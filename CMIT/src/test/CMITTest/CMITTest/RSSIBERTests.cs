﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CMITApp;

namespace CMITTest
{
    [TestClass]
    public class RSSIBERTests
    {
        [TestMethod]
        public void Test_RSSIBERParse()
        {
            CSTagTypes.Packets.CommandResponsePacket cmdResponse = new CSTagTypes.Packets.CommandResponsePacket();
            cmdResponse.cmiuPacketHeader = new CSTagTypes.Tags.PackerHeader();
            cmdResponse.cmiuPacketHeader.cellularRssiAndBer = 5378;
            
            cmdResponse.packetType = CSTagTypes.Packets.PacketTypeId.E_PACKET_TYPE_RESPONSE;

            String parsed = CMITApp.ResponsePackets.Parse(cmdResponse);
            Assert.IsTrue(parsed.Contains("RSSI: 21 & BER: 2"));
           
        }
    }
}
