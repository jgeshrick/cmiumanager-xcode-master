﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using CMITApp;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace CMITTest
{
    [TestClass]
    public class FileVersionTests
    {
        [TestMethod]
        public void TestLoadImageFileVersions()
        {
            // Load test file
            string[] hexFileLines = GetTestFile();

            FileVersions fvTest = new FileVersions();

            bool success = fvTest.LoadFileImageVersions(hexFileLines.ToList());

            // Test if the file was properly parsed
            Assert.IsTrue(success);

            
        }

        [TestMethod]
        public void TestApplicationFileVersionsParse()
        {
            // Load test file
            string[] hexFileLines = GetTestFile();

            FileVersions fvTest = new FileVersions();

            bool success = fvTest.LoadFileImageVersions(hexFileLines.ToList());

            // Test if the file was properly parsed
            Assert.IsTrue(success);

            // Test values
            string testMajor = "12";
            string testMinor = "34";
            string testDate = "00150911";
            string testBuild = "00000056";

            string parsedMajor = fvTest.ApplicationImage.VersionData[(int)FileVersions.VersionIndex.Version_Major].Value;
            string parsedMinor = fvTest.ApplicationImage.VersionData[(int)FileVersions.VersionIndex.Version_Minor].Value;
            string parsedDate = fvTest.ApplicationImage.VersionData[(int)FileVersions.VersionIndex.Version_YearMonthDay].Value;
            string parsedBuild = fvTest.ApplicationImage.VersionData[(int)FileVersions.VersionIndex.Version_Build].Value;

            Assert.AreEqual(testMajor, parsedMajor, "Major versions do not match");
            Assert.AreEqual(testMinor, parsedMinor, "Minor versions do not match");            
            Assert.AreEqual(testDate, parsedDate, "Date versions do not match");
            Assert.AreEqual(testBuild, parsedBuild, "Build versions do not match");
        }


        [TestMethod]
        public void TestBLEConfigFileVersionsParse()
        {
            // Load test file
            string[] hexFileLines = GetTestFile();

            FileVersions fvTest = new FileVersions();

            bool success = fvTest.LoadFileImageVersions(hexFileLines.ToList());

            // Test if the file was properly parsed
            Assert.IsTrue(success);

            // Test values
            string testMajor = "12";
            string testMinor = "34";
            string testDate = "00150911";
            string testBuild = "00000056";

            string parsedMajor = fvTest.BLEConfigurationImage.VersionData[(int)FileVersions.VersionIndex.Version_Major].Value;
            string parsedMinor = fvTest.BLEConfigurationImage.VersionData[(int)FileVersions.VersionIndex.Version_Minor].Value;
            string parsedDate = fvTest.BLEConfigurationImage.VersionData[(int)FileVersions.VersionIndex.Version_YearMonthDay].Value;
            string parsedBuild = fvTest.BLEConfigurationImage.VersionData[(int)FileVersions.VersionIndex.Version_Build].Value;

            Assert.AreEqual(testMajor, parsedMajor, "Major versions do not match");
            Assert.AreEqual(testMinor, parsedMinor, "Minor versions do not match");
            Assert.AreEqual(testDate, parsedDate, "Date versions do not match");
            Assert.AreEqual(testBuild, parsedBuild, "Build versions do not match");
        }

        [TestMethod]
        public void TestBootloaderFileVersionsParse()
        {
            // Load test file
            string[] hexFileLines = GetTestFile();

            FileVersions fvTest = new FileVersions();

            bool success = fvTest.LoadFileImageVersions(hexFileLines.ToList());

            // Test if the file was properly parsed
            Assert.IsTrue(success);

            // Test values
            string testMajor = "12";
            string testMinor = "34";
            string testDate = "00150911";
            string testBuild = "00000056";
            string testID = "999999999";

            string parsedMajor = fvTest.BootloaderImage.VersionData[(int)FileVersions.VersionIndex.Version_Major].Value;
            string parsedMinor = fvTest.BootloaderImage.VersionData[(int)FileVersions.VersionIndex.Version_Minor].Value;
            string parsedDate = fvTest.BootloaderImage.VersionData[(int)FileVersions.VersionIndex.Version_YearMonthDay].Value;
            string parsedBuild = fvTest.BootloaderImage.VersionData[(int)FileVersions.VersionIndex.Version_Build].Value;
            string parsedID = fvTest.BootloaderImage.VersionData[(int)FileVersions.VersionIndex.Version_Build + 1].Value;

            Assert.AreEqual(testMajor, parsedMajor, "Major versions do not match");
            Assert.AreEqual(testMinor, parsedMinor, "Minor versions do not match");            
            Assert.AreEqual(testDate, parsedDate, "Date versions do not match");
            Assert.AreEqual(testBuild, parsedBuild, "Build versions do not match");
            Assert.AreEqual(testID, parsedID, "ID's do not match");
        }

        [TestMethod]
        public void TestConfigurationFileVersionsParse()
        {
            // Load test file
            string[] hexFileLines = GetTestFile();

            //FileVersions fvTest = new FileVersions();
            ConfigurationFile cfgFile = new ConfigurationFile();

            cfgFile.LoadConfigurationFromFile(hexFileLines.ToList());

            // Test values
            string testMajor = "12";
            string testMinor = "34";
            string testDate = "00150911";
            string testBuild = "00000056";

            string testNormalARB = "111";
            string testNormalModeDetailedCfg = "111";
            string testNormalIntervalData = "111";
            string testNormalCellConnection = "11";
            string testSwipeARB = "11";
            string testSwipeStateTimer = "1";
            string testServerMQTTPort = "1111";
            string testServerMQTTAddress = "11.111.111.11";
            string testServerHTTPPort = "11";
            string testServerHTTPAddress = "11.111.111.11";
            string testBLEDebugOutput = "1";
            string testBLEStartupActive = "111";
            string testBLELinkActiveTimeout = "111";
            string testHWVersion = "1";
            
            string parsedNormalModeARB = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.NormalMode_ARBReadInterval].Value;
            string parsedNormalDetailedCfg = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.NormalMode_ARBReadInterval].Value;
            string parsedNormalIntervalData = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.NormalMode_IntervalDataPacketInterval].Value;
            string parsedNormalCellConnection = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.NormalMode_CellConnectionInterval].Value;
            string parsedSwipeARB = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.Swipe_ARBReadInterval].Value;
            string parsedSwipeStateTimer = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.Swipe_StateTimer].Value;
            string parsedServerMQTTPort = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.Server_MQTTPortNumber].Value;
            string parsedServerMQTTAddress = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.Server_MQTTAddress].Value;
            parsedServerMQTTAddress = parsedServerMQTTAddress.TrimEnd('\0');
            string parsedServerHTTPPort = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.Server_HTTPPortNumber].Value;
            string parsedServerHTTPAddress = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.Server_HTTPAddress].Value;
            parsedServerHTTPAddress = parsedServerHTTPAddress.TrimEnd('\0');
            string parsedBLEDebugOutput = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.BLE_DebugOutputBitmap].Value;
            string parsedBLELinkStartupActive = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.BLE_LinkStartupActiveDuration].Value;
            string parsedBLELinkActiveTimeout = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.BLE_LinkActiveTimeout].Value;
            string parsedHWVersion = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.Hardware_Version].Value;

            string parsedMajor = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.Version_Major].Value;
            string parsedMinor = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.Version_Minor].Value;
            string parsedDate = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.Version_YearMonthDay].Value;
            string parsedBuild = cfgFile.Settings[(int)ConfigurationSettings.SettingIndex.Version_Build].Value;

            Assert.AreEqual(testNormalARB, parsedNormalModeARB, "Normal ARB intervals do not match");
            Assert.AreEqual(testNormalModeDetailedCfg, parsedNormalDetailedCfg, "Normal Detailed Cfg intervals do not match");
            Assert.AreEqual(testNormalIntervalData, parsedNormalIntervalData, "Normal Interval Data intervals do not match");
            Assert.AreEqual(testNormalCellConnection, parsedNormalCellConnection, "Normal Cell Connection intervals do not match");
            Assert.AreEqual(testSwipeARB, parsedSwipeARB, "Swipe ARB intervals do not match");
            Assert.AreEqual(testSwipeStateTimer, parsedSwipeStateTimer, "Swipe State Timer intervals do not match");
            Assert.AreEqual(testServerMQTTPort, parsedServerMQTTPort, "Server MQTT Ports do not match");
            Assert.AreEqual(testServerMQTTAddress, parsedServerMQTTAddress, "Server MQTT Adresses do not match");
            Assert.AreEqual(testServerHTTPPort, parsedServerHTTPPort, "Server HTTP Ports do not match");
            Assert.AreEqual(testServerHTTPAddress, parsedServerHTTPAddress, "Server HTTP Adresses do not match");
            Assert.AreEqual(testBLEDebugOutput, parsedBLEDebugOutput, "BLE Output Bitmap do not match");
            Assert.AreEqual(testBLELinkActiveTimeout, parsedBLELinkActiveTimeout, "BLE Link Active Timeouts do not match");
            Assert.AreEqual(testBLEStartupActive, parsedBLELinkStartupActive, "BLE Link Startup Active do not match");
            Assert.AreEqual(testHWVersion, parsedHWVersion, "HW Versions do not match");

            Assert.AreEqual(testMinor, parsedMinor, "Minor versions do not match");
            Assert.AreEqual(testMajor, parsedMajor, "Major versions do not match");
            Assert.AreEqual(testDate, parsedDate, "Date versions do not match");
            Assert.AreEqual(testBuild, parsedBuild, "Build versions do not match");
        }

        [TestMethod]
        public void TestFileVersionsGetUserInput()
        {
            // Load test file
            string[] hexFileLines = GetTestFile();

            //FileVersions fvTest = new FileVersions();
            ConfigurationFile cfgFile = new ConfigurationFile();            
            FileVersions fvTest = new FileVersions();

            cfgFile.LoadConfigurationFromFile(hexFileLines.ToList());
            bool success = fvTest.LoadFileImageVersions(hexFileLines.ToList());

            // Test if the file was properly parsed
            Assert.IsTrue(success);

            // Test values
            string testMajor = "12";
            string testMinor = "34";
            string testDate = "00150911";
            string testBuild = "00000056";

            fvTest.ApplicationImage.VersionData[(int)FileVersions.VersionIndex.Version_Major].Value = testMajor;
            fvTest.ApplicationImage.VersionData[(int)FileVersions.VersionIndex.Version_Minor].Value = testMinor;
            fvTest.ApplicationImage.VersionData[(int)FileVersions.VersionIndex.Version_YearMonthDay].Value = testDate;
            fvTest.ApplicationImage.VersionData[(int)FileVersions.VersionIndex.Version_Build].Value = testBuild;

            fvTest.BootloaderImage.VersionData[(int)FileVersions.VersionIndex.Version_Major].Value = testMajor;
            fvTest.BootloaderImage.VersionData[(int)FileVersions.VersionIndex.Version_Minor].Value = testMinor;
            fvTest.BootloaderImage.VersionData[(int)FileVersions.VersionIndex.Version_YearMonthDay].Value = testDate;
            fvTest.BootloaderImage.VersionData[(int)FileVersions.VersionIndex.Version_Build].Value = testBuild;

            fvTest.BLEConfigurationImage.VersionData[(int)FileVersions.VersionIndex.Version_Major].Value = testMajor;
            fvTest.BLEConfigurationImage.VersionData[(int)FileVersions.VersionIndex.Version_Minor].Value = testMinor;
            fvTest.BLEConfigurationImage.VersionData[(int)FileVersions.VersionIndex.Version_YearMonthDay].Value = testDate;
            fvTest.BLEConfigurationImage.VersionData[(int)FileVersions.VersionIndex.Version_Build].Value = testBuild;

            // Prepare the files
            // Get the file with updated versions and config
            List<byte> config = cfgFile.GetConfigDataWithUserUpdates();
            Assert.IsNotNull(config);
            List<string> file = fvTest.GetHexFileWithUpdatedVersions(config);
            Assert.IsNotNull(file);

            for(int index = 0; index < file.Count; index++)
            {
                string original = hexFileLines[index];
                string updated = file[index];
                Assert.AreEqual(original, updated);
            }
            
            
        }


        // Helper Methods

        private string[] GetTestFile()
        {
            // Open the Intel Hex File and read in all lines
            const string fileandpath = "test.hex";
            string[] hexFileLines;
            hexFileLines = File.ReadAllLines(fileandpath);
            return hexFileLines;
        }
    }
}
