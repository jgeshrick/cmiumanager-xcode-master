﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using CMITApp;
using System.Collections.Generic;
using System.Linq;

namespace CMITTest
{
    [TestClass]
    public class DeviceTests
    {
        [TestMethod]
        public void Test_Device_Create()
        {
            string[] hexFileLines = GetTestFile();

            //Device cmiu = new Device(hexFileLines.ToList());
            Device cmiuDeviceMemory = new Device(hexFileLines.ToList());

            Assert.IsFalse(cmiuDeviceMemory.ErrorEncountered);
        }

        [TestMethod]
        public void Test_Device_Application_Parse()
        {
            string[] hexFileLines = GetTestFile();

            //Device cmiu = new Device(hexFileLines.ToList());
            Device cmiuDeviceMemory = new Device(hexFileLines.ToList());

            Assert.IsFalse(cmiuDeviceMemory.ErrorEncountered);

            // Test values
            string testMajor = "12";
            string testMinor = "34";
            string testDate = "00160505";
            string testBuild = "00000517";

            
            string parsedMajor = cmiuDeviceMemory.ApplicationImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Major].Value;
            string parsedMinor = cmiuDeviceMemory.ApplicationImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Minor].Value;
            string parsedDate = cmiuDeviceMemory.ApplicationImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_YearMonthDay].Value;
            string parsedBuild = cmiuDeviceMemory.ApplicationImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Build].Value;

            Assert.AreEqual(testMajor, parsedMajor, "Major versions do not match");
            Assert.AreEqual(testMinor, parsedMinor, "Minor versions do not match");
            Assert.AreEqual(testDate, parsedDate, "Date versions do not match");
            Assert.AreEqual(testBuild, parsedBuild, "Build versions do not match");
        }

        [TestMethod]
        public void Test_Device_BLEConfig_Parse()
        {
            string[] hexFileLines = GetTestFile();

            //Device cmiu = new Device(hexFileLines.ToList());
            Device cmiuDeviceMemory = new Device(hexFileLines.ToList());

            Assert.IsFalse(cmiuDeviceMemory.ErrorEncountered);

            // Test values
            string testMajor = "12";
            string testMinor = "34";
            string testDate = "00160505";
            string testBuild = "00000517";


            string parsedMajor = cmiuDeviceMemory.BLEConfigImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Major].Value;
            string parsedMinor = cmiuDeviceMemory.BLEConfigImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Minor].Value;
            string parsedDate = cmiuDeviceMemory.BLEConfigImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_YearMonthDay].Value;
            string parsedBuild = cmiuDeviceMemory.BLEConfigImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Build].Value;

            Assert.AreEqual(testMajor, parsedMajor, "Major versions do not match");
            Assert.AreEqual(testMinor, parsedMinor, "Minor versions do not match");
            Assert.AreEqual(testDate, parsedDate, "Date versions do not match");
            Assert.AreEqual(testBuild, parsedBuild, "Build versions do not match");
        }


        [TestMethod]
        public void Test_Device_Bootloader_Parse()
        {
            string[] hexFileLines = GetTestFile();

            //Device cmiu = new Device(hexFileLines.ToList());
            Device cmiuDeviceMemory = new Device(hexFileLines.ToList());

            Assert.IsFalse(cmiuDeviceMemory.ErrorEncountered);

            // Test values
            string testMajor = "12";
            string testMinor = "34";
            string testDate = "00160505";
            string testBuild = "00000393";
            string testID = "400000200";


            string parsedMajor = cmiuDeviceMemory.BootloaderImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Major].Value;
            string parsedMinor = cmiuDeviceMemory.BootloaderImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Minor].Value;
            string parsedDate = cmiuDeviceMemory.BootloaderImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_YearMonthDay].Value;
            string parsedBuild = cmiuDeviceMemory.BootloaderImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Build].Value;
            string parsedID = cmiuDeviceMemory.BootloaderImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Build + 1].Value;

            Assert.AreEqual(testMajor, parsedMajor, "Major versions do not match");
            Assert.AreEqual(testMinor, parsedMinor, "Minor versions do not match");
            Assert.AreEqual(testDate, parsedDate, "Date versions do not match");
            Assert.AreEqual(testBuild, parsedBuild, "Build versions do not match");
            Assert.AreEqual(testID, parsedID, "ID's do not match");
        }

        [TestMethod]
        public void Test_Device_Configuration_Parse()
        {
            // Load test file
            string[] hexFileLines = GetTestFile();

            //Device cmiu = new Device(hexFileLines.ToList());
            Device cmiuDeviceMemory = new Device(hexFileLines.ToList());

            Assert.IsFalse(cmiuDeviceMemory.ErrorEncountered);

            // Test values
            string testMajor = "12";
            string testMinor = "34";
            string testDate = "00160505";
            string testBuild = "00000517";

            string testNormalARB = "111";
            string testNormalModeDetailedCfg = "1111";
            string testNormalIntervalData = "111";
            string testNormalCellCallInInterval = "111";
            string testNormalCellCallInOffset = "1";
            string testNormalCellCallInWindow = "1";
            string testNormalDatalogInt = "111";

            string testSwipeARB = "11";
            string testSwipeStateTimer = "1111";

            string testServerMQTTPort = "1111";
            string testServerMQTTAddress = "broker.prod.mdce";
            string testServerHTTPPort = "11";
            string testServerHTTPAddress = "broker.prod.mdce";

            string testBLEDebugOutput = "1";
            string testBLEStartupActive = "111";
            string testBLELinkActiveTimeout = "111";
            string testHWVersion = "1";

            string testCarrierContextATT = "1";
            string testCarrierContextVZW = "1";
            string testCarrierATTAPN = "neptune-nonprod01.com.attz";
            string testCarrierVZWAPN = "neptune-nonprod01.com.vzwz";

            string parsedNormalModeARB = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.NormalMode_ARBReadInterval].Value;
            string parsedNormalDetailedCfg = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.NormalMode_DetailedConfigPacketInterval].Value;
            string parsedNormalIntervalData = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.NormalMode_IntervalDataPacketInterval].Value;
            string parsedNormalCellCallInInt = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.NormalMode_CellCallInIntervalSecs].Value;
            string parsedNormalCellCallInOffset = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.NormalMode_CellCallInOffsetMins].Value;
            string parsedNormalCellCallInWindow = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.NormalMode_CellCallInWindowQtrHrs].Value;
            string parsedNormalDatalogInt = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.NormalMode_DatalogIntervalSecs].Value;

            string parsedSwipeARB = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.Swipe_ARBReadInterval].Value;
            string parsedSwipeStateTimer = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.Swipe_StateTimer].Value;
            string parsedServerMQTTPort = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.Server_MQTTPortNumber].Value;
            string parsedServerMQTTAddress = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.Server_MQTTAddress].Value;
            parsedServerMQTTAddress = parsedServerMQTTAddress.TrimEnd('\0');
            string parsedServerHTTPPort = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.Server_HTTPPortNumber].Value;
            string parsedServerHTTPAddress = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.Server_HTTPAddress].Value;
            parsedServerHTTPAddress = parsedServerHTTPAddress.TrimEnd('\0');
            string parsedBLEDebugOutput = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.BLE_DebugOutputBitmap].Value;
            string parsedBLELinkStartupActive = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.BLE_LinkStartupActiveDuration].Value;
            string parsedBLELinkActiveTimeout = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.BLE_LinkActiveTimeout].Value;
            string parsedHWVersion = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.Hardware_Version].Value;
            string parsedCarrierContextATT = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.CarrierConfig_ATTContextID].Value;
            string parsedCarrierContextVZW = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.CarrierConfig_VZWContextID].Value;
            string parsedCarrierATTAPN = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.CarrierConfig_ATTAPN].Value;
            string parsedCarrierVZWAPN = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.CarrierConfig_VZWAPN].Value;

            string parsedMajor = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.Version_Major].Value;
            string parsedMinor = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.Version_Minor].Value;
            string parsedDate = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.Version_YearMonthDay].Value;
            string parsedBuild = cmiuDeviceMemory.ConfigurationImage.versionData.VersionData[(int)ImageConfiguration.SettingIndex.Version_Build].Value;

            Assert.AreEqual(testNormalARB, parsedNormalModeARB, "Normal ARB intervals do not match");
            Assert.AreEqual(testNormalModeDetailedCfg, parsedNormalDetailedCfg, "Normal Detailed Cfg intervals do not match");
            Assert.AreEqual(testNormalIntervalData, parsedNormalIntervalData, "Normal Interval Data intervals do not match");
            Assert.AreEqual(testNormalCellCallInInterval, parsedNormalCellCallInInt, "Normal Cell Call In Intervals do not match");
            Assert.AreEqual(testNormalCellCallInOffset, parsedNormalCellCallInOffset, "Normal Cell Call In Offset does not match");
            Assert.AreEqual(testNormalCellCallInWindow, parsedNormalCellCallInWindow, "Normal Cell Call In Window does not match");
            Assert.AreEqual(testNormalDatalogInt, parsedNormalDatalogInt, "Normal Datalog Interval does not match");
            
            Assert.AreEqual(testSwipeARB, parsedSwipeARB, "Swipe ARB intervals do not match");
            Assert.AreEqual(testSwipeStateTimer, parsedSwipeStateTimer, "Swipe State Timer intervals do not match");
           
            Assert.AreEqual(testServerMQTTPort, parsedServerMQTTPort, "Server MQTT Ports do not match");
            Assert.AreEqual(testServerMQTTAddress, parsedServerMQTTAddress, "Server MQTT Adresses do not match");
            Assert.AreEqual(testServerHTTPPort, parsedServerHTTPPort, "Server HTTP Ports do not match");
            Assert.AreEqual(testServerHTTPAddress, parsedServerHTTPAddress, "Server HTTP Adresses do not match");
            
            Assert.AreEqual(testBLEDebugOutput, parsedBLEDebugOutput, "BLE Output Bitmap do not match");
            Assert.AreEqual(testBLELinkActiveTimeout, parsedBLELinkActiveTimeout, "BLE Link Active Timeouts do not match");
            Assert.AreEqual(testBLEStartupActive, parsedBLELinkStartupActive, "BLE Link Startup Active do not match");
            
            Assert.AreEqual(testHWVersion, parsedHWVersion, "HW Versions do not match");

            Assert.AreEqual(testCarrierContextATT, parsedCarrierContextATT, "Carrier Context ID ATT does not match");
            Assert.AreEqual(testCarrierContextVZW, parsedCarrierContextVZW, "Carrier Context ID VZW does not match");
            Assert.AreEqual(testCarrierATTAPN, parsedCarrierATTAPN, "Carrier ATT APN does not match");
            Assert.AreEqual(testCarrierVZWAPN, parsedCarrierVZWAPN, "Carrier VZW APN does not match");

            Assert.AreEqual(testMinor, parsedMinor, "Minor versions do not match");
            Assert.AreEqual(testMajor, parsedMajor, "Major versions do not match");
            Assert.AreEqual(testDate, parsedDate, "Date versions do not match");
            Assert.AreEqual(testBuild, parsedBuild, "Build versions do not match");
        }

        [TestMethod]
        public void Test_Device_GetUpdatedFile()
        {
            // Load test file
            string[] hexFileLines = GetTestFile();

            //Device cmiu = new Device(hexFileLines.ToList());
            Device cmiuDeviceMemory = new Device(hexFileLines.ToList());

            // Test if the file was properly parsed
            Assert.IsFalse(cmiuDeviceMemory.ErrorEncountered);

            // Test values
            string testMajor = "12";
            string testMinor = "34";
            string testDate = "00160505";
            string testBuild = "00000517";
            string testBuildBL = "00000393";

            cmiuDeviceMemory.ApplicationImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Major].Value = testMajor;
            cmiuDeviceMemory.ApplicationImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Minor].Value = testMinor;
            cmiuDeviceMemory.ApplicationImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_YearMonthDay].Value = testDate;
            cmiuDeviceMemory.ApplicationImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Build].Value = testBuild;

            cmiuDeviceMemory.BootloaderImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Major].Value = testMajor;
            cmiuDeviceMemory.BootloaderImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Minor].Value = testMinor;
            cmiuDeviceMemory.BootloaderImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_YearMonthDay].Value = testDate;
            cmiuDeviceMemory.BootloaderImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Build].Value = testBuildBL;

            cmiuDeviceMemory.BLEConfigImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Major].Value = testMajor;
            cmiuDeviceMemory.BLEConfigImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Minor].Value = testMinor;
            cmiuDeviceMemory.BLEConfigImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_YearMonthDay].Value = testDate;
            cmiuDeviceMemory.BLEConfigImage.versionData.VersionData[(int)FileVersions.VersionIndex.Version_Build].Value = testBuild;

            // Prepare the files
            List<string> file = cmiuDeviceMemory.GetUpdatedHexFile();

            Assert.IsFalse(cmiuDeviceMemory.ErrorEncountered);

            for (int index = 0; index < file.Count; index++)
            {
                string original = hexFileLines[index];
                string updated = file[index];
                Assert.AreEqual(original, updated);
            }


        }



        // Helper Methods
        private string[] GetTestFile()
        {
            // Open the Intel Hex File and read in all lines
            const string fileandpath = "test.hex";
            string[] hexFileLines;
            hexFileLines = File.ReadAllLines(fileandpath);
            return hexFileLines;
        }
    }
}
