﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PacketHandlerLibrary;

namespace nRFUart
{
    class PacketHandlerTest
    {
        public UInt32 MakeFirmwareImagePacket(Byte[] outputData)
        {
            PacketHandlerLibrary.BuildPacket builder = new BuildPacket();

            String fileName = "C:\\TestImage.hex";

            return builder.MakeFirmwareImagePacket(fileName, outputData);

        }
        
    }
}
