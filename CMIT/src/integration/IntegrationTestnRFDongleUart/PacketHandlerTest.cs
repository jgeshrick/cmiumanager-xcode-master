﻿/*****************************************************************************
******************************************************************************
**
**    Neptune Technology Group
**    Copyright 2016 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PacketHandlerLibrary;

namespace IntegrationTestnRFDongleUart
{
    class TestImageHexFilePacketCreator
    {
        // This is a wrapper method for bulding a test firmware image packet
        public UInt32 MakeFirmwareImagePacket(Byte[] outputData)
        {
            PacketHandlerLibrary.BuildPacket builder = new BuildPacket();

            String fileName = "C:\\TestImage.hex";

            return builder.MakeFirmwareImagePacket(fileName, outputData);

        }
        
    }
}
