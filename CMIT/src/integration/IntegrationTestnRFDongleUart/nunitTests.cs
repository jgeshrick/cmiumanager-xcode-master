/*****************************************************************************
******************************************************************************
**
**    Neptune Technology Group
**    Copyright 2016 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using nRFDongleUartLib;
using Nordicsemi;
using NUnit.Framework;


///
/// Tests run by NUnit 2.6.4 in test explorer or via TeamCity Nunit runner
/// Note: requires Nordic dongle to be plugged in and working, and a Server simulator up and 
/// running and in range, taht echoes back messages.
/// 
/// Note: If the auto tests fail (hangs)- check dongle is working and that it's not waiting on a 
/// dialog for new software update.
namespace IntegrationTestnRFDongleUart
{

    [TestFixture]
     public partial class Program
    {
     
        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            SetupHardware();
        }

        [TestFixtureTearDown]
        public void TestFixtureTeardown()
        {
            TeardownHardware();
        }

        /// <summary>
        /// Check nothing has been hacked for development purposes - typically a forced Addr to connect to
        /// </summary>
        [Test]
        public static void IsNotDevelopmentCode()
        {
            string connectAddressString = Environment.GetEnvironmentVariable("INT_TEST_NRFDONGLE");
            Assert.False(connectAddressString.Contains("C6482F913B8D"));
        }

        /// <summary>
        /// Begin a file transfer, wait until completion.
        /// Server (CMIU Simulator) sends a magic ASYNC message when complete
        /// This tells this test to stop and analyse the result.
        /// Note. Use dummy data (not filesystem), since test server won't have it.
        /// </summary>
        [Test]
        public static void FileTransfer()
        {
            Byte[] temp = new Byte[1024];
            UInt32 numToSend;
            const Int32 testSleepTimeMs = 500;
            UInt32 timeOutCnt = 0;
            const UInt32 constTimeoutToCompleteTest = 60000 / (UInt32)testSleepTimeMs;
            TestImageHexFilePacketCreator tpbpTest = new TestImageHexFilePacketCreator();

            numToSend = tpbpTest.MakeFirmwareImagePacket(temp);
            Array.Resize<Byte>(ref temp, (Int32)numToSend);

            controller.SendFirmwareFileRequest(temp);
            fileDownloadTestResult = -1;

            // Wait for the test to complete (signal via setting fileDownloadTestResult)
            while ((fileDownloadTestResult < 0) && (timeOutCnt <= constTimeoutToCompleteTest))
            {
                Thread.Sleep(testSleepTimeMs);
                timeOutCnt ++;
            }

            // Complete with no error.
            Assert.True(0 == fileDownloadTestResult);
        }

        /// <summary>
        /// Short pings run forever for extended time. Comment out for normal tests, only for debug.
        /// </summary>
////   [Test]
        public static void ShortPingTestForever()
        {
            Console.WriteLine("ShortPingTest Forever:");
            UInt32 numPings = 0xFFFFFFFF;
            bool result = DoPings(numPings, PingSoakType.ShortOnly);
            Assert.True(result);
        }
     
        /// <summary>
        /// Long pings run forever for extended time. Comment out for normal tests, only for debug.
        /// </summary>
////    [Test]
        public static void LongPingTestForever()
        {
            Console.WriteLine("LongPingTest Forever:");
            UInt32 numPings = 0xFFFFFFFF;
            const UInt32 fixedPaddingLength = 246;
            //Console.WriteLine("Fixed length PingTest: Count = {0} Len = {1}", numPings, fixedPaddingLength);
            bool result = DoPings(numPings, PingSoakType.FixedLength, fixedPaddingLength);
            Assert.True(result);
        }

        // Test to prove NUnit test framework is operational
        [Test]
        public static void ReferenceTest()
        {
            Console.WriteLine("ReferenceTest");
            Assert.True(true);
        }


        // See Log file See C:\ProgramData\Nordic Semiconductor\MasterEmulator\log.txt
        /// <summary>
        /// Short pings
        /// </summary>
        [Test]
        public static void ShortPingTest()
        {
            Console.WriteLine("ShortPingTest:");
            UInt32 numPings = 5;
            bool result = DoPings(numPings, PingSoakType.ShortOnly);
            Assert.True(result);
        }


        /// <summary>
        ///   embedded timestamp, no padding
        /// </summary>
        [Test]
        public static void ExtendedPingTest()
        {
            Console.WriteLine("ExtendedPingTest:");
            UInt32 numPings = 5;
            bool result = DoPings(numPings, PingSoakType.ExtendedPing);
            Assert.True(result);
            Assert.AreEqual(numPings, pingsReceivedCount);
        }


        /// <summary>
        /// Mixture of short and extended (have embedded timestamp) random length pings
        /// </summary>
        [Test]
        public static void MixPingTest()
        {
            Console.WriteLine("MixPingTest:");
            UInt32 numPings = 5;
            bool result = DoPings(numPings, PingSoakType.Mixed);
            Assert.True(result);
            Assert.AreEqual(numPings, pingsReceivedCount);
        }


        /// <summary>
        /// Fixed payload len of 0
        /// </summary>
        [Test]
        public static void FixedLengthPingTest0()
        {           
            UInt32 numPings = 2;
            const UInt32 fixedPaddingLength = 0;
            Console.WriteLine("Fixed length PingTest: Count = {0} Len = {1}", numPings, fixedPaddingLength);

            bool result = DoPings(numPings, PingSoakType.FixedLength, fixedPaddingLength);
            Assert.True(result);

        }

        /// <summary>
        /// Fixed payload len of 1
        /// </summary>
        [Test]
        public static void FixedLengthPingTest1()
        {
            UInt32 numPings = 2;
            const UInt32 fixedPaddingLength = 1;
            Console.WriteLine("Fixed length PingTest: Count = {0} Len = {1}", numPings, fixedPaddingLength);

            bool result = DoPings(numPings, PingSoakType.FixedLength, fixedPaddingLength);
            Assert.True(result);
        }  
        
        
        /// <summary>
        /// Increasing payload len of 0-constMaxPayloadForTest 
        /// </summary>
        [Test]
        public static void IncreasingLengthPingTest()
        {
            UInt32 numPings = 1;
            const UInt32 constMaxPayloadForTest = 246;

              for (fixedPaddingLength = 0; fixedPaddingLength <= constMaxPayloadForTest; fixedPaddingLength++)
              {
                  Console.WriteLine("PingTest: Count = {0} Len = {1}", numPings, fixedPaddingLength);
                  bool result = DoPings(numPings, PingSoakType.FixedLength, fixedPaddingLength);
                  Assert.True(result);
              }
        }


        /// <summary>
        /// Fixed payload len of 128
        /// </summary>
        [Test]
        public static void FixedLengthPingTest128()
        {
            UInt32 numPings = 200;
            const UInt32 fixedPaddingLength = 128;
          //  Console.WriteLine("Fixed length PingTest: Count = {0} Len = {1}", numPings, fixedPaddingLength);
            bool result = DoPings(numPings, PingSoakType.FixedLength, fixedPaddingLength);
            Assert.True(result);
        }  /// <summary>
       

        /// <summary>
        /// Fixed payload len of constMaxPayloadForTest
        /// </summary>
        [Test]
        public static void FixedLengthPingTestMaximumPayload()
        {
            UInt32 numPings = 200; 

            const UInt32 constMaxPayloadForTest = 246;
            Console.WriteLine("Fixed length PingTest: Count = {0} Len = {1}", numPings, constMaxPayloadForTest);
            bool result = DoPings(numPings, PingSoakType.FixedLength, constMaxPayloadForTest);
            Assert.True(result);
        }


        /// <summary>
        /// randomised payload len
        /// </summary>
        [Test]
        public static void RandomisedLengthPingTest()
        {
            Console.WriteLine("Randomised length PingTest:");
            UInt32 numPings = 300;
            const UInt32 constMaxPayloadForTest = 246;

            bool result = DoPings(numPings, PingSoakType.RandomLengths, 0, constMaxPayloadForTest);
            Assert.True(result);
        }
    }
}
