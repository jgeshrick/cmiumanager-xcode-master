﻿
using System;
using System.IO;
using System.Windows;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;
using System.Text;
 
 

namespace nRFDongleUartLib
{

    /// <summary>
    /// This class is a C# wrapper around the HTTPServer Applet Controller DLL
    ///  Note. To enable debugging DLL, in the projects Debug settings, Configuration Debug, Enable unmanaged code debugging. 
    /// </summary>
    static class HttpServerApplet
    {
        const UInt32 constTickIntervalMs = 100; 
        private static System.Timers.Timer tickTimer;
        static Byte[] responseDataBuffer = new Byte[8192];
        public delegate void OnSendResponse(byte[] response);
        public static event OnSendResponse onSendResponse;



        // trunk\CMIT\target\nRFDongleUartTestHarness\bin\Debug or Release
        private const String constLibFileName = "HttpServerLib.dll";

       // static GCHandle gh;

        /// <summary>
        /// Inits the DLL
        /// </summary>
        [DllImport(constLibFileName, CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void HttpServerTestAppletInit();

        /// <summary>
        /// Tick function. Call at tickInterval
        /// </summary>
        /// <param name="tickIntervalMs"></param>
        [DllImport(constLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern void HttpServerTestAppletTick(UInt32 tickIntervalMs);
        
        /// <summary>
        /// Send received bytes (i.e requests) into server
        /// </summary>
        /// <param name="tickIntervalMs"></param>
        [DllImport(constLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern void HttpServerReceiveData([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] Byte[]   data, UInt32 dataBytes);
 
        // <summary>
        /// Test to create a dummy request with the given args
        /// </summary>
        /// <param name="tickIntervalMs"></param>
        [DllImport(constLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern void HttpServerTestGenerateRequest([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] Byte[] url,
            [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] Byte[] hostName,
                UInt32 firstRangeByte, UInt32 lastRangeByte );

        // <summary>
        /// Get the response from the server ringbuffer, so we can send them
        /// </summary>
        /// <param name="tickIntervalMs"></param>
        [DllImport(constLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern UInt32 HttpServerGetResponseBytes([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] Byte[] data, UInt32 arraySize);

        /// <summary>
        /// Init the UART Controller DLL 
        /// </summary>
        public static void Init()
        {
        HttpServerTestAppletInit ();

            tickTimer = new System.Timers.Timer();
            tickTimer.Interval = constTickIntervalMs;
            tickTimer.Elapsed += new ElapsedEventHandler(httpSvr_TickTimer_Elapsed);
            tickTimer.Enabled = true;

            System.Diagnostics.Debug.WriteLine("HttpServerTestApplet Initialised");
        }


        /// <summary>
        /// sends bytes to the server
        /// </summary>
        public static void HandleRequestData(byte[] data, UInt32 numBytes)
        {
            HttpServerReceiveData(data, numBytes);
        }

        /// <summary>
        /// Make a dummy request and sends to server engine
        /// </summary>
        public static void SendTestRequest()
        {
            String sUrl = "c:\\work\\testimage.hex";
            String sHost = "myHost";
            UInt32 firstRangeByte = 1;
            UInt32 lastRangeByte = 8;
            Byte[] bUrl = Encoding.ASCII.GetBytes(sUrl);
            Byte[] bHost = Encoding.ASCII.GetBytes(sHost);
            HttpServerTestGenerateRequest(bUrl, bHost, firstRangeByte, lastRangeByte);
        }


        /// <summary>
        /// On tick, 
        /// </summary>
        /// <param name="sender">Not used</param>
        /// <param name="e">Not used</param>
        private static void httpSvr_TickTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            HttpServerTestAppletTick(constTickIntervalMs);

            UInt32 numBytesToSend = HttpServerGetResponseBytes(responseDataBuffer, (UInt32)responseDataBuffer.GetLength(0));
            if (numBytesToSend >0)
            {
                // BTLE Send
                String s = Encoding.ASCII.GetString(responseDataBuffer, 0 , (int)numBytesToSend);
                System.Diagnostics.Debug.WriteLine("HttpServer Served " + s);
                Byte[] bR = new byte[ numBytesToSend] ;// ( responseDataBuffer.
                Array.Copy(responseDataBuffer, bR, numBytesToSend);

                var v = onSendResponse;
                if (null != v)
                {
                    onSendResponse(bR);
                }
            }
        }

    }


}