﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using nRFDongleUartLib;
using UartDataController;
using Nordicsemi;
using NUnit.Framework;





//
// This test expects an env var  INT_TEST_NRFDONGLE to be set to the MAC addr (eg C0D035F6FF0C) of the simu to use.
// Use Dos SET command or define in TeamCity.
namespace IntegrationTestnRFDongleUart
{
    enum PingSoakType
    {
        ShortOnly,
        ExtendedPing,
        Mixed,
        FixedLength,
        RandomLengths
    };


     

    /// <summary>
    /// TestFixture tells nunit this is the app to run
    /// </summary>
    [TestFixture]
    public partial class Program
    {
        // Set true to use DEW simulator ending "B8D"
        static bool forcedAddrForDuncansHardware = false;

        static private nRFUartController controller;
        static private bool controllerInitialised = false;
        static private bool controllerConnected = false;
        static private bool uartAvailable = false;
        static UInt32 pingsReceivedCount = 0;
        static Random repeatableRandomValue = new Random(0); // Use same sequence for each run
        static UInt32 fixedPaddingLength = 0;
        static Int32 fileDownloadTestResult = -1;

        /// <summary>
        /// Set up Nordic Dongle and make connection to target (simulator). Nunit uses Env Var to set target address
        /// </summary>
        static void SetupHardware()
        {
            if (controller != null)
            {
                controller.Close();
            } 
            Console.WriteLine("NUnit setupHardware: Initialising dongle (may take a few seconds)");

            // To test env var use these...   
            // Environment.SetEnvironmentVariable("INT_TEST_NRFDONGLE", "C0D035F6FF0C"); // Int test on Nethuns

            if (forcedAddrForDuncansHardware)
            {
                Environment.SetEnvironmentVariable("INT_TEST_NRFDONGLE", "C6482F913B8D"); // Duncan's dev board
                Console.WriteLine("WARNING Env Var Overwritten in code for development!");
            }

            // Environment.SetEnvironmentVariable("INT_TEST_USE_BONDING", "0");

            // Read Env Var to get address of simulator to run against. For NUnit usage where a cmd line is not possible.
            string connectAddressString = Environment.GetEnvironmentVariable("INT_TEST_NRFDONGLE");
            string useBondingString     = Environment.GetEnvironmentVariable("INT_TEST_USE_BONDING"); // 0 for no, 1 for yes

            if (connectAddressString == string.Empty)
            {
                Console.WriteLine("Env Var INT_TEST_NRFDONGLE NOT FOUND. Set this env var to lock to a given board");
            }
            else
            {
                Console.WriteLine("Env Var INT_TEST_NRFDONGLE={0}.", connectAddressString);
            }

            if (IsValidHexAddress(connectAddressString))
            {
                Console.WriteLine("Env Var [INT_TEST_NRFDONGLE] Found! Connection locked to DevBoard having address:[{0}]", connectAddressString);
                Console.WriteLine("Warning - test will fail if this device is not visible");
            }
            else
            {
                Console.WriteLine("Env Var INT_TEST_NRFDONGLE: {0} is not a valid hex address and will be ignored.", connectAddressString);
                Console.WriteLine("Will connect to first simulator seen");
            }
   
            if (useBondingString == string.Empty)
            {
                Console.WriteLine("Env Var INT_TEST_USE_BONDING NOT FOUND. Set this env var 0 or 1 to use bonding");
            }
            else
            {
                Console.WriteLine("Env Var INT_TEST_USE_BONDING={0}.", useBondingString);
            }

            bool wantBonded = useBondingString == "1";

            BeginConnectionHelper (connectAddressString,  wantBonded);
        }


         /// <summary>
         /// Close down at end of tests
         /// </summary>
        public void TeardownHardware()
        {
            Console.WriteLine("Teardown disconnecting...");

            Thread.Sleep(1000);
            controller.InitiateDisconnect();

            while (controllerConnected)
            {
                Thread.Sleep(50);
            }
            
            controller.DeleteBond();

            Console.WriteLine("Teardown disconnected.");
            controller.MessageReceived -= OnUarcMessageReceived;
            controller.Close();

            Thread.Sleep(1000);
            Console.WriteLine("Teardown complete");
        }

        /// <summary>
        /// Helper to initialise and start connection.
        /// </summary>
        /// <param name="connectAddressString"></param>
        /// <param name="wantBonded"></param>
        static void BeginConnectionHelper (string connectAddressString, bool wantBonded)
        {
            const Int32 tickIntervalMs = 500;
            const Int32 constTimeoutToInitialise = 10000;
            Int32 timerForInit = 0;

            Console.WriteLine("Initialising dongle (may take a few seconds)");
            Initialise();

            while (!controllerInitialised)
            {
                Console.Write(".");
                Thread.Sleep(tickIntervalMs);
                timerForInit += tickIntervalMs;
                if (timerForInit > constTimeoutToInitialise)
                {
                    throw new Exception(string.Format("Dongle failed to init in {0}s. Not plugged in or hung", constTimeoutToInitialise/1000));
                }
            }

            Console.WriteLine("Initiating Connection...");
            Console.WriteLine("Use Bonded connection? (env var INT_TEST_USE_BONDING = 0|1 or cmd line -b) = {0}", wantBonded ? "YES" : "NO");
            
             if (connectAddressString != string.Empty)
             {
                 if (IsValidHexAddress(connectAddressString))
                 {
                    //controller.SetConnectAddress(connectAddressString);
                    controller.ConnectToDeviceWithAddress(connectAddressString);
                 }
             }

             const bool doAutoConnect = true;
            controller.InitiateConnection(doAutoConnect, wantBonded);

            while (!controllerConnected)
            {
                Thread.Sleep(50);
            }

            Console.Write("Waiting for UART pipe to be established.");
            while (!uartAvailable)
            {
                Console.Write(".");
                Thread.Sleep(tickIntervalMs);
            }

            if (wantBonded)
            {
                int timeoutMs = 0;
                Console.Write("Waiting up to 10s for BONDING.");
                while (!controller.hasBondedOk && timeoutMs < 10000)
                {
                    timeoutMs += tickIntervalMs;
                    Console.Write(".");
                    Thread.Sleep(tickIntervalMs);
                }
                if (controller.hasBondedOk)
                {
                    Console.WriteLine("BONDING OK in {0}ms", timeoutMs);
                }
                else
                {
                    Console.WriteLine("BONDING Timed out in 10s");
                }
            }
        }

        /// <summary>
        /// Main App runs as console app and requires user input to quit.
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            bool wantBonded = false;
            Console.WriteLine("IntegrationTestnRFDongleUart test.");
            string connectAddress = "";

            if (args.Length > 0)
            {
                foreach (string s  in args)
                {
                    if (s.ToUpper().Contains("-B"))
                    {
                        wantBonded = true;
                    }
               
                    if (IsValidHexAddress(s))
                    {
                        Console.WriteLine("CommandLine Arg found! Connection locked to DevBoard having address:[{0}]", s);
                        Console.WriteLine("Warning - test will fail if this device is not visible");
                       connectAddress = s;
                    }
                }
            }
            else
            {
                Console.WriteLine("No command line arg suppplied. Test will connect to first DevBoard seen.");
                Console.WriteLine("To connect to a specific board, specify a Hex address in the command line. e.g 12D035F6FFAA");
                Console.WriteLine("To specify bonded connection use -b");
            }

            /*
                        Console.WriteLine("Hit any key to connect to the BtleServer.");
                        ConsoleKeyInfo key = Console.ReadKey();
            */
            
            BeginConnectionHelper(connectAddress, wantBonded);


            DoPings(5, PingSoakType.ShortOnly);
            DoPings(30000, PingSoakType.Mixed);

            Console.WriteLine("Done. Disconnecting...");
            controller.InitiateDisconnect();

            while (controllerConnected)
            {
                Thread.Sleep(50);
            }

            Console.WriteLine("Disconnected.");
            Console.WriteLine("Hit any key to exit.");
            Console.ReadKey();
        }


        /// <summary>
        /// Simple helper to determine if supplied string is a BTLE Hex address
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private static bool IsValidHexAddress(string s)
        {
            if (s != null && s != string.Empty)
            {
                return System.Text.RegularExpressions.Regex.IsMatch(s, @"\A\b[0-9a-fA-F]+\b\Z");
            }
            return false;
        }


        /// <summary>
        /// Execute Multiple pings of given type
        /// </summary>
        /// <param name="numberOfPingsToSend"></param>
        /// <param name="pingSoakType"></param>
        /// minPaddingSize if random, is the smallest no of paddng bytes
        /// maxPaddingSize is the largest, note - this number is additional to the timestamp, so cannot be as high as the max payload len.
        /// <returns>True if passed all pings, else false</returns>
        static private bool DoPings(UInt32 numberOfPingsToSend, 
            PingSoakType pingSoakType,
            UInt32 minPaddingSize = 0,
            UInt32 maxPaddingSize = 100)
        {
            const UInt32 constMaxWaitForResponseMs = 3000;
            double maxBytesPerSec = 0.0;
            double minBytesPerSec = 1e9;
            double meanBytesPerSecAcc = 0.0;
            bool result = false;
            bool testAborted = false;
            UInt32 pingSendCount = 0;
            DateTime testStartTime = DateTime.Now;
            bool pingIsExtended = false;
            UInt32 pingPaddingByteCount = 0;
            PingType pingType;
            pingsReceivedCount = 0;

            Console.WriteLine("Doing Pings...Number = {0}, Type={1}", 
                numberOfPingsToSend, 
                pingSoakType.ToString());

            while (pingSendCount < numberOfPingsToSend)
            {
                pingSendCount++;

                if (testAborted)
                    break;

                TimeSpan tsSinceStart = DateTime.Now - testStartTime;

                //  Choice of minimal length fixed PINGs or mix of short and extended
                switch (pingSoakType)
                {
                    default:
                    case PingSoakType.ShortOnly:
                        {
                            // All short "PING" payload
                            pingType = PingType.ShortPings;
                            pingIsExtended = false;
                            pingPaddingByteCount = 0;

                            Console.Write("\n{2, 6:D6}ms Tx Short Ping {0} of {1} ", 
                                pingSendCount,
                                numberOfPingsToSend, 
                                (UInt32)tsSinceStart.TotalMilliseconds);
                        }
                    break;

                    case PingSoakType.ExtendedPing:
                    {
                        //  extended have timestamps , no padding
                        pingIsExtended = true;
                        pingType = PingType.ExtendedPing;
                        pingPaddingByteCount = 0;

                        Console.Write("\n{2, 6:D6}ms Tx {4} Ping {0} of {1},  PaddingCount = {3 :D3} ",
                            pingSendCount,
                            numberOfPingsToSend,
                            (UInt32)tsSinceStart.TotalMilliseconds,
                            pingPaddingByteCount,
                            pingIsExtended ? "Extended" : " Short  ");
                    }
                    break;
                    case PingSoakType.Mixed:
                    {
                        // Randomly some extended, some short.
                        pingIsExtended = (bool)(repeatableRandomValue.Next(2) == 1);
                        pingType = pingIsExtended ? PingType.ExtendedPing : PingType.ShortPings;
                        pingPaddingByteCount = pingIsExtended ? (UInt32)repeatableRandomValue.Next((Int32)minPaddingSize, (Int32)maxPaddingSize) : 0;

                        Console.Write("\n{2, 6:D6}ms Tx {4} Ping {0} of {1},  PaddingCount = {3 :D3} ",
                            pingSendCount,
                            numberOfPingsToSend,
                            (UInt32)tsSinceStart.TotalMilliseconds,
                            pingPaddingByteCount,
                            pingIsExtended ? "Extended" : " Short  ");
                    }
                    break;

                    // Fixed len payload (of minPaddingSize)
                    case PingSoakType.FixedLength:
                        {
                        pingType = PingType.FixedPayloadLength;
                        pingPaddingByteCount = minPaddingSize;
                        
                        Console.Write("\n{2, 6:D6}ms Tx {4} Ping {0} of {1},  PaddingCount = {3 :D3} ",
                          pingSendCount,
                          numberOfPingsToSend,
                          (UInt32)tsSinceStart.TotalMilliseconds,
                          pingPaddingByteCount,
                          " Fixed  ");
                         }
                        break;

                    case PingSoakType.RandomLengths:
                        {
                            pingType = PingType.FixedPayloadLength;
                            pingPaddingByteCount = (UInt32)repeatableRandomValue.Next((Int32)minPaddingSize, (Int32)maxPaddingSize);

                            Console.Write("\n{2, 6:D6}ms Tx {4} Ping {0} of {1},  PaddingCount = {3 :D3} ",
                              pingSendCount,
                              numberOfPingsToSend,
                              (UInt32)tsSinceStart.TotalMilliseconds,
                              pingPaddingByteCount,
                              " Random ");
                        }
                        break;
                }


                DateTime dtPingSentTime = DateTime.Now;

                //
                // Send the ping
                //
                Int32 numBytesSent = controller.SendPingPacket(pingSendCount, pingType, pingPaddingByteCount);
                if (0 == numBytesSent)
                {
                    Console.WriteLine("Error SendPingPacket Can't send");
                    testAborted = true;
                    break;
                }

                TimeSpan tsRoundtripTime = TimeSpan.Zero;

                //
                // Wait for response
                //
                while (pingsReceivedCount < pingSendCount)
                {
                    tsRoundtripTime = DateTime.Now - dtPingSentTime;
                    bool responseTimeout = (tsRoundtripTime.TotalMilliseconds > constMaxWaitForResponseMs);

                    if (!controllerConnected || !uartAvailable || responseTimeout)
                    {
                        if (responseTimeout)
                        {
                            Console.WriteLine("\nNo response from Ping within {0}ms Took {1}ms", 
                                constMaxWaitForResponseMs, 
                                tsRoundtripTime.Milliseconds);
                        }

                        testAborted = true;
                        break;
                    }
                 
                    Thread.Sleep(4);
                } // while wait for response

                Console.WriteLine("Ping Roundtrip in {0}ms, Sent={1} Rcvd={2}", 
                    tsRoundtripTime.Milliseconds, 
                    pingSendCount, 
                    pingsReceivedCount);

                //
                // Do Stats
                //
                if (tsRoundtripTime.TotalMilliseconds > 10.0) // sanity
                {
                    double bytesPerSec = numBytesSent * 1000 / tsRoundtripTime.TotalMilliseconds;
                    //Console.W
                    meanBytesPerSecAcc += bytesPerSec;
                    if (bytesPerSec > maxBytesPerSec) maxBytesPerSec = bytesPerSec;
                    if (bytesPerSec < minBytesPerSec) minBytesPerSec = bytesPerSec;
                }
               
            }

            
            TimeSpan elapsedTestTimeMs = DateTime.Now - testStartTime;
            if ((pingSendCount == numberOfPingsToSend) && !testAborted && (pingSendCount == pingsReceivedCount))
            {
                Console.WriteLine("\nPassed test with {0} pings in {1}s.",
                                  pingSendCount,
                                  elapsedTestTimeMs.ToString());
                result = true;
            }
            else
            {
                Console.WriteLine("\nFailed test at ping {0} (Rx'd {1}) of {2} after {3}s.",
                                  pingSendCount,
                                  pingsReceivedCount,
                                  numberOfPingsToSend,
                                  elapsedTestTimeMs.ToString());
                result = false;
            }

            Console.WriteLine("\nMin BytesPerSec = {0}; Max BytesPerSec = {1} Mean = {2}.",
                               minBytesPerSec,
                               maxBytesPerSec,
                               meanBytesPerSecAcc / pingSendCount);
            return (result);
        }


        /// <summary>
        /// Init the dongle and app
        /// </summary>
        static private void Initialise()
        {
            if (controller != null)
            {
             throw new AssertionException("Controller not null");
            }

            controller = new nRFUartController();
            controllerInitialised = false; // set on completion in OnControllerInitialized

            // Register event handler methods for all nRFUartController events
            controller.LogMessage += OnLogMessage;
            controller.Initialized += OnControllerInitialized;
            controller.Scanning += OnScanning;
            controller.ScanningCanceled += OnScanningCanceled;
            controller.Connecting += OnConnecting;
            controller.ConnectionCanceled += OnConnectionCanceled;
            controller.Connected += OnConnected;
            controller.PipeDiscoveryCompleted += OnControllerPipeDiscoveryCompleted;
            controller.Disconnected += OnDisconnected;
            controller.SendDataStarted += OnSendDataStarted;
            controller.SendDataCompleted += OnSendDataCompleted;
            controller.SendDataReadyToSend += controller_SendDataReadyToSend;
            controller.ProgressUpdated += OnProgressUpdated;
            controller.MessageReceived += OnUarcMessageReceived;
            controller.MessageSendFailed += OnUarcMessageFailed;
            controller.DeviceDiscovered += controller_DeviceDiscovered;
            controller.OnErrorInitialization += controller_OnErrorInitialization;
            controller.OnErrorConnection += controller_OnErrorConnection;

            controller.Initialize();

            pingsReceivedCount = 0;

             HttpServerApplet.Init();
             HttpServerApplet.onSendResponse += HttpServerApplet_onSendResponse;

        }

        static void controller_SendDataReadyToSend(object sender, OutputReceivedEventArgs e)
        {
            // Do nothing
        }

        static void controller_OnErrorConnection(object sender, EventArgs e)
        {
            Console.WriteLine("\nConnection failed");
        }

        /// <summary>
        /// Envent Handler Dongle not initied (not plugged in?)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void controller_OnErrorInitialization(object sender, EventArgs e)
        {
            Console.WriteLine("\nDongle Init failed - Plugged in?");
        }

        static void controller_DeviceDiscovered(object sender, OnDeviceDiscoveredArgs e)
        {
          //  throw new NotImplementedException();
            Console.WriteLine("\nDevice Discovered {0}", e.device.ToString());
        }

        static void HttpServerApplet_onSendResponse(byte[] response)
        {
            controller.SendHttpResponsePacket(response);
        }

    #region nRFDongleUart Event Handlers

        static void OnLogMessage(object sender, OutputReceivedEventArgs e)
        {
            Console.WriteLine(e.Message);
        }

        static void OnControllerInitialized(object sender, EventArgs e)
        {
            controllerInitialised = true;
            Console.WriteLine("\r\nDongle initialised.");
        }

        static void OnScanning(object sender, EventArgs e)
        {
            Console.WriteLine("Scanning for BtleServer...");
        }

        static void OnScanningCanceled(object sender, EventArgs e)
        {
            Console.WriteLine("\r\nStopped scanning.");
        }

        static void OnConnecting(object sender, EventArgs e)
        {
            // Called when the dongle initiates a connection to another device
        }

        static void OnConnectionCanceled(object sender, EventArgs e)
        {
            Console.WriteLine("\r\nConnection cancelled.");
        }

        static void OnConnected(object sender, EventArgs e)
        {
            controllerConnected = true;
            Console.WriteLine("Connected.");
        }

        static void OnControllerPipeDiscoveryCompleted(object sender, EventArgs e)
        {
            uartAvailable = true;
            Console.WriteLine("\r\nUART link established.");
        }

        static void OnDisconnected(object sender, EventArgs e)
        {
            controllerConnected = false;
            uartAvailable = false;
        }

        static void OnSendDataStarted(object sender, EventArgs e)
        {
            // Called when data transfer is initiated
        }

        static void OnSendDataCompleted(object sender, EventArgs e)
        {
            // Called when data transfer completes
        }

        static void OnProgressUpdated(object sender, Nordicsemi.ValueEventArgs<int> e)
        {
            int progress = e.Value;
        }


        /// <summary>
        /// Called when a valid msg is received from the UARC layer
        /// </summary>
        /// <param name="m"></param>
        static void OnUarcMessageReceived(UarcMessage m)
        {
            // File xfr requests
            if (m.type == UarcMessageType.UARC_MT_HTTP_CMD)
            {
                HttpServerApplet.HandleRequestData(m.payload, (UInt32)m.payload.Length);
            }
            // File xfr results sent as ASYNC
            if (m.type == UarcMessageType.UARC_MT_ASYNC)
            {
                string s = System.Text.Encoding.ASCII.GetString(m.payload);
               
                if (s.ToUpper().Contains("$FILE_DOWNLOAD_BEGIN$"))
                {
                    Console.WriteLine("Start FL test");
                } 
                
                if (s.ToUpper().Contains("$FILE_DOWNLOAD_RESULTS$"))
                {
                    ProcessFileDownloadTestResults(s);
                }


                Console.WriteLine(" Rx'd ASYNC {0}  \n",  s);
            }


            // Called when the UartController successfully decodes a message
            if (m.type == UarcMessageType.UARC_MT_GNIP)
            {
                pingsReceivedCount++;
                string s = System.Text.Encoding.ASCII.GetString(m.payload);
                
                char[] delimiters = { ' ' };
                string[] tokens = s.Split(delimiters);

                if (tokens.Length >2)
                {
                    UInt32 rxIndex = Convert.ToUInt32(tokens[1]);
                    DateTime txTime = DateTime.Parse(tokens[2]);

                    TimeSpan roundTripTime = DateTime.Now - txTime;

                    Console.WriteLine(" Rx'd PingRxCount {0} Index {1} in {2}ms\n",
                        pingsReceivedCount, 
                        rxIndex, 
                        roundTripTime.Milliseconds);
                }
                else
                {
                    Console.WriteLine(" Rx'd PingRxCount = {0}.", pingsReceivedCount);
                }
            }
        }

        static void OnUarcMessageFailed()
        {
            string s = "Tx Packet FAILED"; // String.Format("Rx Packet FAILED", m.payload.Length) + payloadString;

            Console.WriteLine(s);
        }


        static void ProcessFileDownloadTestResults(string s)
        {
            char[] delimiters = { ',',';' };
            string[] tokens = s.Split(delimiters);

            Console.WriteLine("File Download ErrorCode      = {0}.",    tokens[1]);
            Console.WriteLine("File Download NumBytes       = {0}.",    tokens[2]);
            Console.WriteLine("File Download time           = {0}s.",   tokens[3]);
            Console.WriteLine("File Download ReqSIze        = {0}.",    tokens[4]);
            Console.WriteLine("File Download Total Length   = {0}.",    tokens[5]);
            Console.WriteLine("File Download Num Reqs       = {0}.",    tokens[6]);
            Console.WriteLine("File Download Num Failed Req = {0}.",    tokens[7]);

            // Setting this (non-negative) stops the test waiting for completion.
            fileDownloadTestResult = Int32.Parse(tokens[1]);
        }


    #endregion
    }
}
