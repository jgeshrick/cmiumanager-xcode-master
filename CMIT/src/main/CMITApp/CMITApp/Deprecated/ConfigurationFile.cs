﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntelHexFileHelper;

namespace CMITApp
{

    public class ConfigurationFileErrorArgs : EventArgs
    {
        public string Error { get; set; }

        public ConfigurationFileErrorArgs(string error)
        {
            Error = error;
        }
    }
    
    public class ConfigurationFile
    {
        public ObservableCollection<ConfigurationSettings> Settings { get; set; }
        public event EventHandler<ConfigurationFileErrorArgs> UserInputParseErrorOccurred;

        private List<byte> HexFileData = new List<byte>();

        // Configuration Settings
        public const int CONFIG_START_ADDRESS = 0x6800;
        public const int CONFIG_BYTESIZE = 2048;
        public const int VERSION_BYTESIZE = 12;

        public const int NORMAL_ARBREADINTERVAL_ADDRESS = CONFIG_START_ADDRESS;
        public const int NORMAL_ARBREADINTERVAL_BYTESIZE = 4;

        public const int NORMAL_DETAILEDCONFIGPACKETINTERVAL_ADDRESS = NORMAL_ARBREADINTERVAL_ADDRESS + NORMAL_ARBREADINTERVAL_BYTESIZE;
        public const int NORMAL_DETAILEDCONFIGPACKET_BYTESIZE = 4;

        public const int NORMAL_INTDATAPACKETINTERVAL_ADDRESS = NORMAL_DETAILEDCONFIGPACKETINTERVAL_ADDRESS + NORMAL_DETAILEDCONFIGPACKET_BYTESIZE;
        public const int NORMAL_INTDATAPACKETINTERVAL_BYTESIZE = 4;

        public const int NORMAL_CELLCONNECTIONINTERVAL_ADDRESS = NORMAL_INTDATAPACKETINTERVAL_ADDRESS + NORMAL_INTDATAPACKETINTERVAL_BYTESIZE;
        public const int NORMAL_CELLCONNECTIONINTERVAL_BYTESIZE = 4;

        // SWIPE MODE

        public const int SWIPE_ARBREADINTERVAL_ADDRESS = NORMAL_CELLCONNECTIONINTERVAL_ADDRESS + NORMAL_CELLCONNECTIONINTERVAL_BYTESIZE;
        public const int SWIPE_ARBREADINTERVAL_BYTESIZE = 4;

        public const int SWIPE_STATETIMER_ADDRESS = SWIPE_ARBREADINTERVAL_ADDRESS + SWIPE_ARBREADINTERVAL_BYTESIZE;
        public const int SWIPE_STATETIMER_BYTESIZE = 4;

        //SERVER

        public const int SERVER_MQTTPORTNUMBER_ADDRESS = SWIPE_STATETIMER_ADDRESS + SWIPE_STATETIMER_BYTESIZE;
        public const int SERVER_MQTTPORTNUMBER_BYTESIZE = 4;

        public const int SERVER_MQTTSERVERADDRESS_ADDRESS = SERVER_MQTTPORTNUMBER_ADDRESS + SERVER_MQTTPORTNUMBER_BYTESIZE;
        public const int SERVER_MQTTSERVERADDRESS_BYTESIZE = 32;

        public const int SERVER_HTTPPORTNUMBER_ADDRESS = SERVER_MQTTSERVERADDRESS_ADDRESS + SERVER_MQTTSERVERADDRESS_BYTESIZE;
        public const int SERVER_HTTPPORTNUMBER_BYTESIZE = 4;

        public const int SERVER_HTTPSERVERADDRESS_ADDRESS = SERVER_HTTPPORTNUMBER_ADDRESS + SERVER_HTTPPORTNUMBER_BYTESIZE;
        public const int SERVER_HTTPSERVERADDRESS_BYTESIZE = 32;

        // BLE

        public const int BLE_DEBUGOUTPUTBITMAP_ADDRESS = SERVER_HTTPSERVERADDRESS_ADDRESS + SERVER_HTTPSERVERADDRESS_BYTESIZE;
        public const int BLE_DEBUGOUTPUTBITMAP_BYTESIZE = 2;

        public const int BLE_LINKSTARTUPACTIVEDURATION_ADDRESS = BLE_DEBUGOUTPUTBITMAP_ADDRESS + BLE_DEBUGOUTPUTBITMAP_BYTESIZE;
        public const int BLE_LINKSTARTUPACTIVEDURATION_BYTESIZE = 2;

        public const int BLE_LINKACTIVETIMEOUT_ADDRESS = BLE_LINKSTARTUPACTIVEDURATION_ADDRESS + BLE_LINKSTARTUPACTIVEDURATION_BYTESIZE;
        public const int BLE_LINKACTIVETIMEOUT_BYTESIZE = 2;

        // Other

        public const int HARDWAREVERSION_ADDRESS = BLE_LINKACTIVETIMEOUT_ADDRESS + BLE_LINKACTIVETIMEOUT_BYTESIZE;
        public const int HARDWAREVERSION_BYTESIZE = 2;

        // Version
        public const int VERSION_START_ADDRESS = (CONFIG_START_ADDRESS + CONFIG_BYTESIZE) - CRC_BYTESIZE - VERSION_BYTESIZE;

        public const int VERSION_DONT_CARE_ADDRESS = VERSION_START_ADDRESS;
        public const int VERSION_DONT_CARE_BYTESIZE = 2;

        public const int VERSION_MAJOR_ADDRESS = VERSION_START_ADDRESS + VERSION_DONT_CARE_BYTESIZE;
        public const int VERSION_MAJOR_BYTESIZE = 1;

        public const int VERSION_MINOR_ADDRESS = VERSION_MAJOR_ADDRESS + VERSION_MAJOR_BYTESIZE;
        public const int VERSION_MINOR_BYTESIZE = 1;

        public const int VERSION_YEARMONTHDAY_ADDRESS = VERSION_MINOR_ADDRESS + VERSION_MINOR_BYTESIZE;
        public const int VERSION_YEARMONTHDAY_BYTESIZE = 4;

        public const int VERSION_BUILD_ADDRESS = VERSION_YEARMONTHDAY_ADDRESS + VERSION_YEARMONTHDAY_BYTESIZE;
        public const int VERSION_BUILD_BYTESIZE = 4;

        public const int CRC_ADDRESS = VERSION_BUILD_ADDRESS + VERSION_BUILD_BYTESIZE;
        public const int CRC_BYTESIZE = 4;

        /// <summary>
        /// Constructor creates a new settings list
        /// </summary>
        public ConfigurationFile()
        {
            Settings = new ObservableCollection<ConfigurationSettings>();
            foreach(string name in ConfigurationSettings.SettingsNames)
            {
                ConfigurationSettings setting = new ConfigurationSettings();
                setting.Name = name;
                setting.Value = "";
                Settings.Add(setting);
            }
        }

        /// <summary>
        /// This function loads the configuration settings from the file
        /// </summary>
        /// <param name="file"></param>
        public void LoadConfigurationFromFile(List<string> file)
        {
            List<IntelHex> hex = IntelHex.ParseToIntelHex(file);

            // Clear HexFileData
            HexFileData.Clear();

            // Load the data
            foreach (IntelHex line in hex)
            {
                if(line.RecordType == 0x00)
                    HexFileData.AddRange(line.Data);
            }

            Settings[(int)ConfigurationSettings.SettingIndex.NormalMode_ARBReadInterval].Value = Get32BitNumberAtAddress(NORMAL_ARBREADINTERVAL_ADDRESS);
            Settings[(int)ConfigurationSettings.SettingIndex.NormalMode_DetailedConfigPacketInterval].Value = Get32BitNumberAtAddress(NORMAL_DETAILEDCONFIGPACKETINTERVAL_ADDRESS);
            Settings[(int)ConfigurationSettings.SettingIndex.NormalMode_IntervalDataPacketInterval].Value = Get32BitNumberAtAddress(NORMAL_INTDATAPACKETINTERVAL_ADDRESS);
            Settings[(int)ConfigurationSettings.SettingIndex.NormalMode_CellConnectionInterval].Value = Get32BitNumberAtAddress(NORMAL_CELLCONNECTIONINTERVAL_ADDRESS);
            Settings[(int)ConfigurationSettings.SettingIndex.Swipe_ARBReadInterval].Value = Get32BitNumberAtAddress(SWIPE_ARBREADINTERVAL_ADDRESS);
            Settings[(int)ConfigurationSettings.SettingIndex.Swipe_StateTimer].Value = Get32BitNumberAtAddress(SWIPE_STATETIMER_ADDRESS);
            Settings[(int)ConfigurationSettings.SettingIndex.Server_MQTTPortNumber].Value = Get32BitNumberAtAddress(SERVER_MQTTPORTNUMBER_ADDRESS);
            Settings[(int)ConfigurationSettings.SettingIndex.Server_MQTTAddress].Value = GetIPAddressAtAddress(SERVER_MQTTSERVERADDRESS_ADDRESS, SERVER_MQTTSERVERADDRESS_BYTESIZE);
            Settings[(int)ConfigurationSettings.SettingIndex.Server_HTTPPortNumber].Value = Get32BitNumberAtAddress(SERVER_HTTPPORTNUMBER_ADDRESS);
            Settings[(int)ConfigurationSettings.SettingIndex.Server_HTTPAddress].Value = GetIPAddressAtAddress(SERVER_HTTPSERVERADDRESS_ADDRESS, SERVER_HTTPSERVERADDRESS_BYTESIZE);
            Settings[(int)ConfigurationSettings.SettingIndex.BLE_DebugOutputBitmap].Value = Get16BitNumberAtAddress(BLE_DEBUGOUTPUTBITMAP_ADDRESS);
            Settings[(int)ConfigurationSettings.SettingIndex.BLE_LinkStartupActiveDuration].Value = Get16BitNumberAtAddress(BLE_LINKSTARTUPACTIVEDURATION_ADDRESS);
            Settings[(int)ConfigurationSettings.SettingIndex.BLE_LinkActiveTimeout].Value = Get16BitNumberAtAddress(BLE_LINKACTIVETIMEOUT_ADDRESS);
            Settings[(int)ConfigurationSettings.SettingIndex.Hardware_Version].Value = Get16BitNumberAtAddress(HARDWAREVERSION_ADDRESS);
            Settings[(int)ConfigurationSettings.SettingIndex.Version_Major].Value = GetBCDValueAtAddressForByteSize(VERSION_MAJOR_ADDRESS, VERSION_MAJOR_BYTESIZE);
            Settings[(int)ConfigurationSettings.SettingIndex.Version_Minor].Value = GetBCDValueAtAddressForByteSize(VERSION_MINOR_ADDRESS, VERSION_MINOR_BYTESIZE);
            Settings[(int)ConfigurationSettings.SettingIndex.Version_YearMonthDay].Value = GetBCDValueAtAddressForByteSize(VERSION_YEARMONTHDAY_ADDRESS, VERSION_YEARMONTHDAY_BYTESIZE);
            Settings[(int)ConfigurationSettings.SettingIndex.Version_Build].Value = GetBCDValueAtAddressForByteSize(VERSION_BUILD_ADDRESS, VERSION_BUILD_BYTESIZE);
        }

        public List<byte> GetConfigDataWithUserUpdates()
        {
            if(UpdateHexFileData())
            {
                List<byte> configData = HexFileData.GetRange(CONFIG_START_ADDRESS, CONFIG_BYTESIZE);
                return configData;
            }
            else
            {
                return null;
            }
            
        }

        /// <summary>
        /// This function returns a fully combined file with the config updated
        /// </summary>
        /// <returns></returns>
        public List<string> GetHexFileWithUpdatedConfig()
        {
            if(UpdateHexFileData())
            {
                List<string> hexLines = IntelHex.CreateFile(0, HexFileData.Count, ref HexFileData, 0);
                return hexLines;
            }
            else
            {
                return null;
            }
            
        }

        // Update a 32bit setting in Hex Data, checks to see if the value passed can be converted properly
        private bool Update32BitSettingInHexData(string setting, int address, int byteSize)
        {
            int number = -1;
            bool success = Int32.TryParse(setting, out number);
            if(success)
            {
                List<byte> bytes = DataHelper.Convert32BitValueToBytes(number);
                HexFileData.RemoveRange(address, byteSize);
                HexFileData.InsertRange(address, bytes);
            }

            return success;
        }

        // Update a 16bit setting in Hex Data, checks to see if the value passed can be converted properly
        private bool Update16BitSettingInHexData(string setting, int address, int byteSize)
        {
            Int16 number = -1;
            bool success = Int16.TryParse(setting, out number);
            if (success)
            {
                List<byte> bytes = DataHelper.Convert16BitValueToBytes(number);
                HexFileData.RemoveRange(address, byteSize);
                HexFileData.InsertRange(address, bytes);
            }

            return success;
        }

        // Update the IP address
        // TODO: Add check to see if its an actual IP address
        private bool UpdateIPAddressInHexData(string setting, int address, int byteSize)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(setting);
            HexFileData.RemoveRange(address, byteSize);
            HexFileData.InsertRange(address, byteArray);
            return true;
        }

        private bool UpdateHexFileData()
        {
            string setting = Settings[(int)ConfigurationSettings.SettingIndex.NormalMode_ARBReadInterval].Value;
            if (!Update32BitSettingInHexData(setting, NORMAL_ARBREADINTERVAL_ADDRESS, NORMAL_ARBREADINTERVAL_BYTESIZE))
            {
                return false;
            }

            setting = Settings[(int)ConfigurationSettings.SettingIndex.NormalMode_DetailedConfigPacketInterval].Value;
            if (!Update32BitSettingInHexData(setting, NORMAL_DETAILEDCONFIGPACKETINTERVAL_ADDRESS, SWIPE_ARBREADINTERVAL_BYTESIZE))
            {
                return false;
            }

            setting = Settings[(int)ConfigurationSettings.SettingIndex.NormalMode_IntervalDataPacketInterval].Value;
            if (!Update32BitSettingInHexData(setting, NORMAL_INTDATAPACKETINTERVAL_ADDRESS, NORMAL_INTDATAPACKETINTERVAL_BYTESIZE))
            {
                return false;
            }

            setting = Settings[(int)ConfigurationSettings.SettingIndex.NormalMode_CellConnectionInterval].Value;
            if (!Update32BitSettingInHexData(setting, NORMAL_CELLCONNECTIONINTERVAL_ADDRESS, NORMAL_CELLCONNECTIONINTERVAL_BYTESIZE))
            {
                return false;
            }

            setting = Settings[(int)ConfigurationSettings.SettingIndex.Swipe_ARBReadInterval].Value;
            if (!Update32BitSettingInHexData(setting, SWIPE_ARBREADINTERVAL_ADDRESS, SWIPE_ARBREADINTERVAL_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }
            
            setting = Settings[(int)ConfigurationSettings.SettingIndex.Swipe_StateTimer].Value;
            if (!Update32BitSettingInHexData(setting, SWIPE_STATETIMER_ADDRESS, SWIPE_STATETIMER_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }            

            setting = Settings[(int)ConfigurationSettings.SettingIndex.Server_MQTTPortNumber].Value;
            if (!Update32BitSettingInHexData(setting, SERVER_MQTTPORTNUMBER_ADDRESS, SERVER_MQTTPORTNUMBER_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = Settings[(int)ConfigurationSettings.SettingIndex.Server_MQTTAddress].Value;
            if (!UpdateIPAddressInHexData(setting, SERVER_MQTTSERVERADDRESS_ADDRESS, SERVER_MQTTSERVERADDRESS_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = Settings[(int)ConfigurationSettings.SettingIndex.Server_HTTPPortNumber].Value;
            if (!Update32BitSettingInHexData(setting, SERVER_HTTPPORTNUMBER_ADDRESS, SERVER_HTTPPORTNUMBER_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = Settings[(int)ConfigurationSettings.SettingIndex.Server_HTTPAddress].Value;
            if (!UpdateIPAddressInHexData(setting, SERVER_HTTPSERVERADDRESS_ADDRESS, SERVER_HTTPSERVERADDRESS_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = Settings[(int)ConfigurationSettings.SettingIndex.Hardware_Version].Value;
            if (!Update16BitSettingInHexData(setting, HARDWAREVERSION_ADDRESS, HARDWAREVERSION_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = Settings[(int)ConfigurationSettings.SettingIndex.BLE_DebugOutputBitmap].Value;
            if (!Update16BitSettingInHexData(setting, BLE_DEBUGOUTPUTBITMAP_ADDRESS, BLE_DEBUGOUTPUTBITMAP_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = Settings[(int)ConfigurationSettings.SettingIndex.BLE_LinkStartupActiveDuration].Value;
            if (!Update16BitSettingInHexData(setting, BLE_LINKSTARTUPACTIVEDURATION_ADDRESS, BLE_LINKSTARTUPACTIVEDURATION_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = Settings[(int)ConfigurationSettings.SettingIndex.BLE_LinkActiveTimeout].Value;
            if (!Update16BitSettingInHexData(setting, BLE_LINKACTIVETIMEOUT_ADDRESS, BLE_LINKACTIVETIMEOUT_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            string value = Settings[(int)ConfigurationSettings.SettingIndex.Version_Major].Value;
            int number = Int32.Parse(value);
            List<byte> bytes = DataHelper.Convert8BitToBcdByte(number).ToList();
            HexFileData.RemoveRange(VERSION_MAJOR_ADDRESS, VERSION_MAJOR_BYTESIZE);
            HexFileData.InsertRange(VERSION_MAJOR_ADDRESS, bytes);

            value = Settings[(int)ConfigurationSettings.SettingIndex.Version_Minor].Value;
            number = Int32.Parse(value);
            bytes = DataHelper.Convert8BitToBcdByte(number).ToList();
            HexFileData.RemoveRange(VERSION_MINOR_ADDRESS, VERSION_MINOR_BYTESIZE);
            HexFileData.InsertRange(VERSION_MINOR_ADDRESS, bytes);

            value = Settings[(int)ConfigurationSettings.SettingIndex.Version_YearMonthDay].Value;
            number = Int32.Parse(value);
            bytes = DataHelper.Convert32BitToBcdBytes(number).ToList();
            HexFileData.RemoveRange(VERSION_YEARMONTHDAY_ADDRESS, VERSION_YEARMONTHDAY_BYTESIZE);
            HexFileData.InsertRange(VERSION_YEARMONTHDAY_ADDRESS, bytes);

            value = Settings[(int)ConfigurationSettings.SettingIndex.Version_Build].Value;
            number = Int32.Parse(value);
            bytes = DataHelper.Convert32BitToBcdBytes(number).ToList();
            HexFileData.RemoveRange(VERSION_BUILD_ADDRESS, VERSION_BUILD_BYTESIZE);
            HexFileData.InsertRange(VERSION_BUILD_ADDRESS, bytes);

            // Recalculate the CRC32 for the image
            RecalculateCRC32();

            return true;
        }

        // Recalculates the CRC32 for the config and places it in the data list
        private void RecalculateCRC32()
        {
            List<byte> config = HexFileData.GetRange(CONFIG_START_ADDRESS, CONFIG_BYTESIZE - CRC_BYTESIZE);
            UInt32 crc = CRC.CalculateCRC32(config);
            byte[] bytes = BitConverter.GetBytes(crc);
            Array.Reverse(bytes, 0, bytes.Length);
            HexFileData.RemoveRange(CRC_ADDRESS, CRC_BYTESIZE);
            HexFileData.InsertRange(CRC_ADDRESS, bytes);
        }

        // Error parsing input
        private void ParseInputError(string badInput)
        {
            string error = "ERROR Parsing user input for field: " + badInput;
            UserInputParseErrorOccurred(this, new ConfigurationFileErrorArgs(error));
        }

        // Settings Get Methods
        private string Get32BitNumberAtAddress(int address)
        {
            int numBytes = 4;
            int interval = DataHelper.ConvertBytesTo32Bit(HexFileData.GetRange(address, numBytes));
            return interval.ToString();
        }

        private string Get16BitNumberAtAddress(int address)
        {
            int numBytes = 2;
            int interval = DataHelper.ConvertBytesTo16Bit(HexFileData.GetRange(address, numBytes));
            return interval.ToString();
        }

        private string GetIPAddressAtAddress(int address, int length)
        {
            return System.Text.Encoding.ASCII.GetString(HexFileData.GetRange(address, length).ToArray());
        }

        private string GetBCDValueAtAddressForByteSize(int address, int byteSize)
        {
            return DataHelper.ConvertBytesToBCDString(HexFileData.GetRange(address, byteSize));
        }

    }
}
