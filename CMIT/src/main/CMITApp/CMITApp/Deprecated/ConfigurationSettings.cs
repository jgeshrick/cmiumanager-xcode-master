﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntelHexFileHelper;

namespace CMITApp
{
    public class ConfigurationSettings
    {
        public string Name { get; set; }
        public string Value { get; set; }

        // NOTE: To add an entry to the config table:
        // Add the name to the name list "SettingsNames" the order matters
        // Add the index to the "SettingIndex", this must match the index of the name
        // Then update the Config File for address and byte size info.
        // Appropriate functions for getting those values will need to be added as well

        // Settings Names
        public static string[] SettingsNames = { "Normal Mode - ARB Read Interval", "Normal Mode - Detailed Config Packet Interval", 
                                                "Normal Mode - Interval Data Packet Interval", "Normal Mode - Cell Connection Interval", 
                                                "Swipe Mode - ARB Read Interval", "Swipe Mode - State Swipe Timer", 
                                                "Server Config - MQTT Server Port Number", "Server Config - MQTT Server Address",
                                                "Server Config - HTTP Server Port Number", "Server Config - HTTP Server Address",
                                                "BLE - Debug Output Bitmap", "BLE - Link Startup Active Duration", "BLE - Link Active Timeout",
                                                "Hardware Version",
                                                "Version - Major", "Version - Minor", "Version - Year Day Month", "Version - Build"};

        public enum SettingIndex
        {
            NormalMode_ARBReadInterval = 0,
            NormalMode_DetailedConfigPacketInterval = 1,
            NormalMode_IntervalDataPacketInterval,
            NormalMode_CellConnectionInterval,
            Swipe_ARBReadInterval,
            Swipe_StateTimer,
            Server_MQTTPortNumber,
            Server_MQTTAddress,
            Server_HTTPPortNumber,
            Server_HTTPAddress,
            BLE_DebugOutputBitmap,
            BLE_LinkStartupActiveDuration,
            BLE_LinkActiveTimeout,
            Hardware_Version,
            Version_Major,
            Version_Minor,
            Version_YearMonthDay,
            Version_Build
        }

    }
}
