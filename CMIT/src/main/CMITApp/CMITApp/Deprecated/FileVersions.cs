﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntelHexFileHelper;

namespace CMITApp
{
    public class FileVersions
    {

        // List of the different versions
        public ImageVersionData ApplicationImage { get; set; }
        public ImageVersionData BootloaderImage { get; set; }
        public ImageVersionData BLEConfigurationImage { get; set; }
        public ImageVersionData EncryptionImage { get; set; }
        public ImageVersionData ConfigurationImage { get; set; }

        public ImageVersionData CurrentlyVeiwedImage { get; set; }

        private List<byte> HexFileData;
        private string[] VersionNames = {"Version - Major", "Version - Minor", "Version - Year Day Month", "Version - Build"};
        public enum VersionIndex
        {
            Version_Major = 0,
            Version_Minor = 1,
            Version_YearMonthDay = 2,
            Version_Build = 3
        }

        public const int VERSION_DONTCARE_BYTESIZE = 2;
        public const int VERSION_MAJOR_BYTESIZE = 1;
        public const int VERSION_MINOR_BYTESIZE = 1;
        public const int VERSION_YEARMONTHDAY_BYTESIZE = 4;
        public const int VERSION_BUILD_BYTESIZE = 4;
        private const int CRC_BYTESIZE = 4;
        private const int INFO_BLOCK_SIZE = VERSION_DONTCARE_BYTESIZE + 
                                            VERSION_MAJOR_BYTESIZE +
                                            VERSION_MINOR_BYTESIZE +
                                            VERSION_YEARMONTHDAY_BYTESIZE +
                                            VERSION_BUILD_BYTESIZE +
                                            CRC_BYTESIZE;
        
        private const int FILE_BYTESIZE = 0x40000;

        // Where each version is located in each image
        private const int BOOTLOADER_START_ADDRESS = 0x0000;
        private const int BOOTLOADER_END_ADDRESS = 0x67FF;
        private const int BOOTLODER_BYTESIZE = (BOOTLOADER_END_ADDRESS - BOOTLOADER_START_ADDRESS) + 1;
        private const int BOOTLOADER_VERSIONSTART_ADDRESS = (BOOTLOADER_END_ADDRESS - (INFO_BLOCK_SIZE - 1));
        private const int BOOTLOADER_CRC_STARTADDRESS = (BOOTLOADER_END_ADDRESS - CRC_BYTESIZE) + 1;
        private const int BOOTLODER_CRCDATA_BYTESIZE = BOOTLODER_BYTESIZE - CRC_BYTESIZE;
        private const int BOOTLOADER_ID_STARTADDRESS = 0x6400;
        private const int BOOTLOADER_ID_PUB_STARTADDRESS = 0x641C;
        private const int BOOTLOADER_ID_SUB_STARTADDRESS = 0x643D;
        private const int BOOTLOADER_ID_CLIENT_STARTADDRESS = 0x6455;
        private const int BOOTLOADER_ID_BYTESIZE = 4;
        private const int BOOTLOADER_ID_ASCII_BYTESIZE = 9;

        private const int APPLICATION_START_ADDRESS = 0x8000;
        private const int APPLICATION_END_ADDRESS = 0x3FFFF;
        private const int APPLICATION_BYTESIZE = (APPLICATION_END_ADDRESS - APPLICATION_START_ADDRESS) + 1;
        private const int APPLICATION_VERSIONSTART_ADDRESS = (APPLICATION_END_ADDRESS - (INFO_BLOCK_SIZE - 1));
        private const int APPLICATION_CRC_STARTADDRESS = (APPLICATION_END_ADDRESS - CRC_BYTESIZE) + 1;
        private const int APPLICATION_CRCDATA_BYTESIZE = APPLICATION_BYTESIZE - CRC_BYTESIZE;

        private const int CONFIGURATION_END_ADDRESS = 0x6FFF;
        private const int CONFIGURATION_VERSIONSTART_ADDRESS = (CONFIGURATION_END_ADDRESS - (INFO_BLOCK_SIZE - 1));
        private const int CONFIGURATION_CRC_STARTADDRESS = CONFIGURATION_END_ADDRESS - CRC_BYTESIZE;

        private const int ENCRYPTION_START_ADDRESS = 0x7000;
        private const int ENCRYPTION_END_ADDRESS = 0x77FF;
        private const int ENCRYPTION_BYTESIZE = (ENCRYPTION_END_ADDRESS - ENCRYPTION_START_ADDRESS) + 1;
        private const int ENCRYPTION_VERSIONSTART_ADDRESS = (ENCRYPTION_END_ADDRESS - (INFO_BLOCK_SIZE - 1));
        private const int ENCRYPTION_CRC_STARTADDRESS = (ENCRYPTION_END_ADDRESS - CRC_BYTESIZE) + 1;
        private const int ENCRYPTION_CRCDATA_BYTESIZE = ENCRYPTION_BYTESIZE - CRC_BYTESIZE;

        private const int BLE_CONFIG_START_ADDRESS = 0x7800;
        private const int BLECONFIG_END_ADDRESS = 0x7FFF;
        private const int BLECONFIG_BYTESIZE = (BLECONFIG_END_ADDRESS - BLE_CONFIG_START_ADDRESS) + 1;
        private const int BLECONFIG_VERSIONSTART_ADDRESS = (BLECONFIG_END_ADDRESS - (INFO_BLOCK_SIZE - 1));
        private const int BLECONFIG_CRC_STARTADDRESS = (BLECONFIG_END_ADDRESS - CRC_BYTESIZE) + 1;
        private const int BLECONFIG_CRCDATA_BYTESIZE = BLECONFIG_BYTESIZE - CRC_BYTESIZE;


        

        /// <summary>
        /// Function to load all version imformation from the file
        /// </summary>
        /// <param name="file"></param>
        public bool LoadFileImageVersions(List<string> file)
        {
            ApplicationImage = new ImageVersionData();
            BootloaderImage = new ImageVersionData();
            BLEConfigurationImage = new ImageVersionData();
            EncryptionImage = new ImageVersionData();
            ConfigurationImage = new ImageVersionData();
            CurrentlyVeiwedImage = new ImageVersionData();

            HexFileData = new List<byte>();
            List<IntelHex> hex = IntelHex.ParseToIntelHex(file);

            if(!ValidateInputHexFile(hex))
            {
                return false;
            }

            // Clear HexFileData
            HexFileData.Clear();

            // Load the data
            foreach (IntelHex line in hex)
            {
                if(line.RecordType == 0x00)
                    HexFileData.AddRange(line.Data);
            }

            GetBootloaderFromFile();
            GetApplicationFromFile();
            GetBLEConfigFromFile();
            GetEncryptionFromFile();

            return true;
        }

        private bool ValidateInputHexFile(List<IntelHex> file)
        {
            List<byte> data = new List<byte>();
            
            // Validate the checksum of each line
            foreach(IntelHex line in file)
            {
                if(line.LineChecksum != (IntelHexFileHelper.Checksum.CalculateChecksum(line)))
                {
                    return false;
                }

                if (line.RecordType == 0x00)
                    data.AddRange(line.Data);
            }

            // Validate the number of bytes
            if(data.Count != FILE_BYTESIZE)
            {
                return false;
            }

            return true;
        }


        public List<string> GetHexFileWithUpdatedVersions(List<byte> updatedConfig)
        {
            // Load each image and insert the user data and CRC for each image
            List<ImageVersionData> versions = new List<ImageVersionData>();
            versions.Add(ApplicationImage);
            versions.Add(BootloaderImage);
            versions.Add(BLEConfigurationImage);
            //versions.Add(EncryptionImage); //  Removing this for now as its parsing would fail  because there is nothing there !!!
            foreach (ImageVersionData image in versions)
            {
                GetImageVersionFromInput(image);
            }

            // Update the ID
            UpdateImageDataIDWithUserInput(BootloaderImage);

            // HexFileData now contains updated user data for versions

            // Get the configuration data
            HexFileData.RemoveRange(ConfigurationFile.CONFIG_START_ADDRESS, ConfigurationFile.CONFIG_BYTESIZE);
            HexFileData.InsertRange(ConfigurationFile.CONFIG_START_ADDRESS, updatedConfig);

            // Convert to string lines for file output
            List<string> hexLines = IntelHex.CreateFile(0, HexFileData.Count, ref HexFileData, 0);
            return hexLines;

        }

        public List<byte> GetDataOfCurrentlySelectedImage(List<byte> updatedConfig)
        {

            if(CurrentlyVeiwedImage == ConfigurationImage)
            {
                // Get the configuration data
                HexFileData.RemoveRange(ConfigurationFile.CONFIG_START_ADDRESS, ConfigurationFile.CONFIG_BYTESIZE);
                HexFileData.InsertRange(ConfigurationFile.CONFIG_START_ADDRESS, updatedConfig);
                return updatedConfig;
            }
            else
            {
                GetImageVersionFromInput(CurrentlyVeiwedImage);
                int imageSize = (CurrentlyVeiwedImage.CRCStartAddress + CRC_BYTESIZE) - CurrentlyVeiwedImage.ImageStartAddress;
                return HexFileData.GetRange(CurrentlyVeiwedImage.ImageStartAddress, imageSize);
            }
            
        }


        private void GetBootloaderFromFile()
        {
            BootloaderImage.VersionStartAddress = BOOTLOADER_VERSIONSTART_ADDRESS;
            BootloaderImage.ImageStartAddress = BOOTLOADER_START_ADDRESS;
            BootloaderImage.CRCDataSize = BOOTLODER_CRCDATA_BYTESIZE;
            BootloaderImage.CRCStartAddress = BOOTLOADER_CRC_STARTADDRESS;
            
            for(int index = 0; index < VersionNames.Count(); index++)
            {
                VersionInfo version = new VersionInfo();
                version.Name = VersionNames[index];
                BootloaderImage.VersionData.Add(version);
            }

            BootloaderImage.VersionData[(int)VersionIndex.Version_Major].Value = GetMajorVersion(BOOTLOADER_VERSIONSTART_ADDRESS);
            BootloaderImage.VersionData[(int)VersionIndex.Version_Minor].Value = GetMinorVersion(BOOTLOADER_VERSIONSTART_ADDRESS);
            BootloaderImage.VersionData[(int)VersionIndex.Version_YearMonthDay].Value = GetYearMonthDayVersion(BOOTLOADER_VERSIONSTART_ADDRESS);
            BootloaderImage.VersionData[(int)VersionIndex.Version_Build].Value = GetBuildVersion(BOOTLOADER_VERSIONSTART_ADDRESS);

            // Add ID
            VersionInfo idinfo = new VersionInfo();
            idinfo.Name = "CMIU ID";
            idinfo.Value = GetID();
            BootloaderImage.VersionData.Add(idinfo);
        }

        private void GetApplicationFromFile()
        {
            ApplicationImage.VersionStartAddress = APPLICATION_VERSIONSTART_ADDRESS;
            ApplicationImage.ImageStartAddress = APPLICATION_START_ADDRESS;
            ApplicationImage.CRCDataSize = APPLICATION_CRCDATA_BYTESIZE;
            ApplicationImage.CRCStartAddress = APPLICATION_CRC_STARTADDRESS;

            for(int index = 0; index < VersionNames.Count(); index++)
            {
                VersionInfo version = new VersionInfo();
                version.Name = VersionNames[index];
                ApplicationImage.VersionData.Add(version);
            }

            ApplicationImage.VersionData[(int)VersionIndex.Version_Major].Value = GetMajorVersion(APPLICATION_VERSIONSTART_ADDRESS);
            ApplicationImage.VersionData[(int)VersionIndex.Version_Minor].Value = GetMinorVersion(APPLICATION_VERSIONSTART_ADDRESS);
            ApplicationImage.VersionData[(int)VersionIndex.Version_YearMonthDay].Value = GetYearMonthDayVersion(APPLICATION_VERSIONSTART_ADDRESS);
            ApplicationImage.VersionData[(int)VersionIndex.Version_Build].Value = GetBuildVersion(APPLICATION_VERSIONSTART_ADDRESS);

        }

        private void GetBLEConfigFromFile()
        {
            BLEConfigurationImage.VersionStartAddress = BLECONFIG_VERSIONSTART_ADDRESS;
            BLEConfigurationImage.ImageStartAddress = BLE_CONFIG_START_ADDRESS;
            BLEConfigurationImage.CRCDataSize = BLECONFIG_CRCDATA_BYTESIZE;
            BLEConfigurationImage.CRCStartAddress = BLECONFIG_CRC_STARTADDRESS;

            for(int index = 0; index < VersionNames.Count(); index++)
            {
                VersionInfo version = new VersionInfo();
                version.Name = VersionNames[index];
                BLEConfigurationImage.VersionData.Add(version);
            }

            BLEConfigurationImage.VersionData[(int)VersionIndex.Version_Major].Value = GetMajorVersion(BLECONFIG_VERSIONSTART_ADDRESS);
            BLEConfigurationImage.VersionData[(int)VersionIndex.Version_Minor].Value = GetMinorVersion(BLECONFIG_VERSIONSTART_ADDRESS);
            BLEConfigurationImage.VersionData[(int)VersionIndex.Version_YearMonthDay].Value = GetYearMonthDayVersion(BLECONFIG_VERSIONSTART_ADDRESS);
            BLEConfigurationImage.VersionData[(int)VersionIndex.Version_Build].Value = GetBuildVersion(BLECONFIG_VERSIONSTART_ADDRESS);

        }

        // THIS NEEDS UPDATING ONCE ENC IMAGE IS USED !!!!
        private void GetEncryptionFromFile()
        {
            EncryptionImage.VersionStartAddress = ENCRYPTION_VERSIONSTART_ADDRESS;

            for(int index = 0; index < VersionNames.Count(); index++)
            {
                VersionInfo version = new VersionInfo();
                version.Name = VersionNames[index];
                EncryptionImage.VersionData.Add(version);
            }

            EncryptionImage.VersionData[(int)VersionIndex.Version_Major].Value = GetMajorVersion(ENCRYPTION_VERSIONSTART_ADDRESS);
            EncryptionImage.VersionData[(int)VersionIndex.Version_Minor].Value = GetMinorVersion(ENCRYPTION_VERSIONSTART_ADDRESS);
            EncryptionImage.VersionData[(int)VersionIndex.Version_YearMonthDay].Value = GetYearMonthDayVersion(ENCRYPTION_VERSIONSTART_ADDRESS);
            EncryptionImage.VersionData[(int)VersionIndex.Version_Build].Value = GetBuildVersion(ENCRYPTION_VERSIONSTART_ADDRESS);

        }

       
        private void GetImageVersionFromInput(ImageVersionData image)
        {
            int startAddress = image.VersionStartAddress + VERSION_DONTCARE_BYTESIZE;

            string value = image.VersionData[(int)VersionIndex.Version_Major].Value;
            int number = Int32.Parse(value);
            //List<byte> bytes = DataHelper.Convert16BitToBcdBytes(number).ToList();
            List<byte> bytes = DataHelper.Convert8BitToBcdByte(number).ToList();
            HexFileData.RemoveRange(startAddress, VERSION_MAJOR_BYTESIZE);
            HexFileData.InsertRange(startAddress, bytes);
            startAddress += VERSION_MAJOR_BYTESIZE;

            value = image.VersionData[(int)VersionIndex.Version_Minor].Value;
            number = Int32.Parse(value);
            bytes = DataHelper.Convert8BitToBcdByte(number).ToList();
            HexFileData.RemoveRange(startAddress, VERSION_MINOR_BYTESIZE);
            HexFileData.InsertRange(startAddress, bytes);
            startAddress += VERSION_MINOR_BYTESIZE;

            value = image.VersionData[(int)VersionIndex.Version_YearMonthDay].Value;
            number = Int32.Parse(value);
            bytes = DataHelper.Convert32BitToBcdBytes(number).ToList();
            HexFileData.RemoveRange(startAddress, VERSION_YEARMONTHDAY_BYTESIZE);
            HexFileData.InsertRange(startAddress, bytes);
            startAddress += VERSION_YEARMONTHDAY_BYTESIZE;

            value = image.VersionData[(int)VersionIndex.Version_Build].Value;
            number = Int32.Parse(value);
            bytes = DataHelper.Convert32BitToBcdBytes(number).ToList();
            HexFileData.RemoveRange(startAddress, VERSION_BUILD_BYTESIZE);
            HexFileData.InsertRange(startAddress, bytes);
            startAddress += VERSION_BUILD_BYTESIZE;

            // CRC32 for image
            RecalculateCRC32(image);
        }

        private void UpdateImageDataIDWithUserInput(ImageVersionData image)
        {
            string idString = image.VersionData[4].Value;
            // Convert the ID to bytes
            int idNumber = Int32.Parse(idString);
            List<byte> idBytes = DataHelper.Convert32BitValueToBytes(idNumber);

            // Replace the binary ID
            HexFileData.RemoveRange(BOOTLOADER_ID_STARTADDRESS, BOOTLOADER_ID_BYTESIZE);
            HexFileData.InsertRange(BOOTLOADER_ID_STARTADDRESS, idBytes);

            // Replace the ASCII IDs
            byte[] asciiIDBytes = Encoding.ASCII.GetBytes(idString);
            HexFileData.RemoveRange(BOOTLOADER_ID_PUB_STARTADDRESS, BOOTLOADER_ID_ASCII_BYTESIZE);
            HexFileData.InsertRange(BOOTLOADER_ID_PUB_STARTADDRESS, asciiIDBytes);
            HexFileData.RemoveRange(BOOTLOADER_ID_SUB_STARTADDRESS, BOOTLOADER_ID_ASCII_BYTESIZE);
            HexFileData.InsertRange(BOOTLOADER_ID_SUB_STARTADDRESS, asciiIDBytes);
            HexFileData.RemoveRange(BOOTLOADER_ID_CLIENT_STARTADDRESS, BOOTLOADER_ID_ASCII_BYTESIZE);
            HexFileData.InsertRange(BOOTLOADER_ID_CLIENT_STARTADDRESS, asciiIDBytes);

            // CRC32 for image
            RecalculateCRC32(image);
        }

        // Recalculates the CRC32 for the config and places it in the data list
        private void RecalculateCRC32(ImageVersionData image)
        {
            List<byte> config = HexFileData.GetRange(image.ImageStartAddress, image.CRCDataSize);
            UInt32 crc = CRC.CalculateCRC32(config);
            byte[] bytes = BitConverter.GetBytes(crc);
            Array.Reverse(bytes, 0, bytes.Length);
            HexFileData.RemoveRange(image.CRCStartAddress, 4);
            HexFileData.InsertRange(image.CRCStartAddress, bytes);
        }

       
        private string GetMajorVersion(int versionStartAddr)
        {
            return DataHelper.ConvertBytesToBCDString(HexFileData.GetRange(versionStartAddr + VERSION_DONTCARE_BYTESIZE, VERSION_MAJOR_BYTESIZE));
        }

        private string GetMinorVersion(int versionStartAddr)
        {

            int addr = versionStartAddr + VERSION_MAJOR_BYTESIZE + VERSION_DONTCARE_BYTESIZE;
            return DataHelper.ConvertBytesToBCDString(HexFileData.GetRange(addr, VERSION_MINOR_BYTESIZE));
        }

        private string GetYearMonthDayVersion(int versionStartAddr)
        {
            int addr = versionStartAddr + VERSION_MAJOR_BYTESIZE + VERSION_MINOR_BYTESIZE + VERSION_DONTCARE_BYTESIZE;
            return DataHelper.ConvertBytesToBCDString(HexFileData.GetRange(addr, VERSION_YEARMONTHDAY_BYTESIZE));
        }

        private string GetBuildVersion(int versionStartAddr)
        {
            int addr = versionStartAddr + VERSION_MAJOR_BYTESIZE + VERSION_MINOR_BYTESIZE + VERSION_YEARMONTHDAY_BYTESIZE + VERSION_DONTCARE_BYTESIZE;
            return DataHelper.ConvertBytesToBCDString(HexFileData.GetRange(addr, VERSION_BUILD_BYTESIZE));
        }

        private string GetID()
        {
            //int addr = BOOTLOADER_ID_STARTADDRESS;
            List<byte> idBytes = HexFileData.GetRange(BOOTLOADER_ID_STARTADDRESS, BOOTLOADER_ID_BYTESIZE);

            List<byte> id2Bytes = HexFileData.GetRange(BOOTLOADER_ID_PUB_STARTADDRESS, BOOTLOADER_ID_ASCII_BYTESIZE);
            List<byte> id3Bytes = HexFileData.GetRange(BOOTLOADER_ID_SUB_STARTADDRESS, BOOTLOADER_ID_ASCII_BYTESIZE);
            List<byte> id4Bytes = HexFileData.GetRange(BOOTLOADER_ID_CLIENT_STARTADDRESS, BOOTLOADER_ID_ASCII_BYTESIZE);

            string id =  DataHelper.ConvertBinaryIDToString(HexFileData.GetRange(BOOTLOADER_ID_STARTADDRESS, BOOTLOADER_ID_BYTESIZE));
            if(id.Length == 10)
            {
                id = id.Remove(0, 1);
                return id;
            }
            else
            {
                // Problem 
                return null;
            }
        }
    }
}
