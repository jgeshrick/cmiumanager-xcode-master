﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CMITApp
{
    /// <summary>
    /// Interaction logic for HotKeyWindow.xaml
    /// </summary>
    public partial class HotKeyWindow : Window
    {
        public System.Windows.Input.Key hkKey;

        public HotKeyWindow()
        {
            InitializeComponent();
            

        }

        private void btnHKCreate_Click(object sender, RoutedEventArgs e)
        {
            string input = tbHKInput.Text;
            if(IsValidAsKey(input))
            {
                KeyConverter converter = new KeyConverter();
                hkKey = (Key)converter.ConvertFromString(input);
            }
            else
            {
                hkKey = Key.None;
            }
                       
            this.Close();
        }

        private bool IsValidAsKey(string input)
        {
            if (input.Length > 1 ||
                input.Length < 1 ||
                !char.IsLetter(input[0]))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
