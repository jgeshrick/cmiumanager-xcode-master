﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMITApp
{
    public class DataHelper
    {
        // Converts byte list to a hex string with spacing
        public static string ConvertBytesToHexString(List<byte> bytes)
        {
            string result = "";
            foreach (byte data in bytes)
            {
                result += data.ToString("X2" + " ");
            }
            return result;
        }

        // Converts byte list to a hex string with spacing
        public static string ConvertBytesToBCDString(List<byte> bytes)
        {
            string result = "";
            bytes.Reverse();
            foreach (byte data in bytes)
            {
                result += data.ToString("X2");
            }
            return result;
        }

        //
        public static byte[] Convert32BitToBcdBytes(int value)
        {
            if (value < 0 || value > 99999999)
                throw new ArgumentOutOfRangeException("value");
            byte[] ret = new byte[4];
            for (int i = 0; i < 4; i++)
            {
                ret[i] = (byte)(value % 10);
                value /= 10;
                ret[i] |= (byte)((value % 10) << 4);
                value /= 10;
            }
            return ret;
        }

        public static byte[] Convert16BitToBcdBytes(int value)
        {
            if (value < 0 || value > 99999)
                throw new ArgumentOutOfRangeException("value");
            byte[] ret = new byte[2];
            for (int i = 0; i < 2; i++)
            {
                ret[i] = (byte)(value % 10);
                value /= 10;
                ret[i] |= (byte)((value % 10) << 4);
                value /= 10;
            }
            return ret;
        }

        public static byte[] Convert8BitToBcdByte(int value)
        {
            if (value < 0 || value > 99)
                throw new ArgumentOutOfRangeException("value");
            byte[] ret = new byte[1];
            for (int i = 0; i < 1; i++)
            {
                ret[i] = (byte)(value % 10);
                value /= 10;
                ret[i] |= (byte)((value % 10) << 4);
                value /= 10;
            }
            return ret;
        }


        // Converts bytes to a string
        public static string ConvertBytesToString(List<byte> data, int numDigits)
        {
            string result = "";
            for (int count = 0; count < numDigits; count++)
            {
                result += data[count].ToString();
            }

            return result;
        }

        // Takes an int value and places in a 2 byte buffer
        public static List<byte> Convert16BitValueToBytes(Int16 value)
        {
            //byte numLSB = (byte)(value & 0xFF);
            //byte numMSB = (byte)((value >> 8) & 0xFF);
            //List<byte> twobytes = new List<byte>();
            //twobytes.Add(numMSB);
            //twobytes.Add(numLSB);
            //return twobytes;
            return BitConverter.GetBytes(value).ToList();
        }

        // Takes an int value and places in a 2 byte buffer
        public static List<byte> Convert32BitValueToBytes(int value)
        {
            return BitConverter.GetBytes(value).ToList();
        }

        // Converts bytes to a 32 bit value
        public static Int32 ConvertBytesTo32Bit(List<byte> bytes)
        {
            Int32 value = 0;
            value = BitConverter.ToInt32(bytes.ToArray(), 0);
            return value;
        }

        // Converts bytes to a 16 bit value
        public static Int16 ConvertBytesTo16Bit(List<byte> bytes)
        {
            Int16 value = 0;
            value = BitConverter.ToInt16(bytes.ToArray(), 0);
            return value;
        }

        // Convert even parity ASCII to a string
        public static string ConvertEvenParityASCIIToString(List<byte> evenParity)
        {

            for (int count = 0; count < evenParity.Count; count++)
            {
                evenParity[count] = (byte)(evenParity[count] & 0x7F);
            }

            return ASCIIEncoding.ASCII.GetString(evenParity.ToArray());
        }

        // Converts a 10 digit string ID to bytes
        public static List<byte> ConvertStringIDToBytes(string strID)
        {
            List<byte> idbytes = new List<byte>();
            int ID = Convert.ToInt32(strID);
            idbytes.Add((byte)(ID & 0x000000ff));
            idbytes.Add((byte)((ID >> 8) & 0x000000ff));
            idbytes.Add((byte)((ID >> 16) & 0x000000ff));
            idbytes.Add((byte)((ID >> 24) & 0x000000ff));
            idbytes.Reverse();
            return idbytes;
        }

        // Converts a 4 byte binary ID to 10 digit string
        public static string ConvertBinaryIDToString(List<byte> binID)
        {
            long id = ((((long)binID[3]) << 24) |
                   (((long)binID[2]) << 16) |
                   (((long)binID[1]) << 8) |
                   (((long)binID[0]) << 0));
            return id.ToString("0000000000");
        }


        public static string RemoveAnyNewlines(string str)
        {
            return str.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
        }

    }
}
