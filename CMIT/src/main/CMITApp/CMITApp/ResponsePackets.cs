﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;
using CSTagTypes;

namespace CMITApp
{
    public class ResponsePackets
    {
        public static string Parse(CSTagTypes.Packets.ResponsePacket response)
        {
            string parsed = "";
            
            switch(response.packetType)
            {
                case Packets.PacketTypeId.E_PACKET_TYPE_RESPONSE:
                    parsed += "COMMAND RESPONSE PACKET RX: \r";                    
                    TagHelper.GetAllTagContents(response, ref parsed, 0);
                    ParseRSSIValue(response, ref parsed);
                    CSTagTypes.Packets.CommandResponsePacket cmdResp = new Packets.CommandResponsePacket();
                    cmdResp = (CSTagTypes.Packets.CommandResponsePacket)response;
                    if(cmdResp.command == CSTagTypes.CommandData.CommandIds.E_COMMAND_GET_CAN_DATA)
                    {
                        CSTagTypes.Packets.CANDataResponsePacket can = new Packets.CANDataResponsePacket();
                        can = (CSTagTypes.Packets.CANDataResponsePacket)cmdResp;
                        string parsedCAN = ParseCANDataForLogging(can);
                        LogFile.AppendToCANDataFile(parsedCAN);
                    }
                    
                    break;
                case Packets.PacketTypeId.E_PACKET_TYPE_DETAILED_CONFIG:
                    parsed += "DETAILED CONFIG PACKET RX: \r";
                    TagHelper.GetAllTagContents(response, ref parsed, 0);
                    break;
                case Packets.PacketTypeId.E_PACKET_TYPE_INTERVAL_DATA:
                    parsed += "INTERVAL DATA PACKET RX: \r";
                    TagHelper.GetAllTagContents(response, ref parsed, 0);
                    break;
                case Packets.PacketTypeId.E_PACKET_TYPE_BASIC_CONFIG:
                    parsed += "BASIC INTERVAL PACKET RX: \r";
                    TagHelper.GetAllTagContents(response, ref parsed, 0);
                    break;
            }

            return parsed;
        }

        private static string ParseCANDataForLogging(CSTagTypes.Packets.CANDataResponsePacket canPacket)
        {
            string parsed = "";
            const string delimiter = " | ";

            parsed += Convert.ToString(canPacket.cmiuID) + delimiter;

            if(canPacket.IMEI != null)
            {
                parsed += canPacket.IMEI;
            }
            parsed += delimiter;
            

            if(canPacket.ICCID != null)
            {
                parsed += canPacket.ICCID;
            }
            parsed += delimiter;

            if(canPacket.MSISDN != null)
            {
                parsed += canPacket.MSISDN;
            }
            parsed += delimiter;

            if(canPacket.IMSI != null)
            {
                parsed += canPacket.IMSI;
            }
            parsed += delimiter;

            if(canPacket.APN != null)
            {
                parsed += canPacket.APN;
            }

            return parsed;
        }

        private static void ParseRSSIValue(CSTagTypes.Packets.ResponsePacket response, ref string parsedResponse)
        {
            // Covert to hex
            // Get upper byte (RSSI)
            // Get lower byte (BER)
            UInt16 rssiBER = response.cmiuPacketHeader.cellularRssiAndBer;
            String rssi = GetRSSIValue(rssiBER);
            String ber = GetBERValue(rssiBER);
            String parsedRSSIBER = "RSSI: " + rssi + " & BER: " + ber;
            
            // Replace in the parsed response. This feels hacky.
            String strRSSIBER = "cellularRssiAndBer: " + rssiBER.ToString();
            parsedResponse = parsedResponse.Replace(strRSSIBER, parsedRSSIBER);
        }

        private static string GetRSSIValue(UInt16 rssiBER)
        {
            String hexRSSIBER = rssiBER.ToString("X4");
            String hexRSSI = hexRSSIBER.Substring(0, 2);
            UInt16 decRSSI = UInt16.Parse(hexRSSI, System.Globalization.NumberStyles.HexNumber);
            return decRSSI.ToString();
        }

        private static string GetBERValue(UInt16 rssiBER)
        {
            String hexRSSIBER = rssiBER.ToString("X4");
            String hexBER = hexRSSIBER.Substring(2, 2);
            UInt16 decBER = UInt16.Parse(hexBER, System.Globalization.NumberStyles.HexNumber);
            return decBER.ToString();
        }
    }
}
