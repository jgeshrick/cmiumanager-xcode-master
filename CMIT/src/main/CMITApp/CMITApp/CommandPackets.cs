﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections.ObjectModel;
using System.ComponentModel;
using CMIUCommandLibrary;

namespace CMITApp
{
    public class CommandPackets
    {
        public ObservableCollection<CommandParameter> CommandPacketParameters { get; set; }
        private List<CommandData> CommandList;
        public string ParsedCommand;
        private List<dynamic> HotKeyFunctions = new List<dynamic>();
        private List<HotKeyCommand> HotKeyCommands = new List<HotKeyCommand>();
            
        private enum CommandHandlerOption
        {
            Send,
            CreateHotKey,
            SendHotKey
        }

        // Build command list
        public CommandPackets()
        {
            CommandPacketParameters = new ObservableCollection<CommandParameter>();
            CommandList = new List<CommandData>();
            List<string> paramList = new List<string>();

            CommandData readDevice = new CommandData();
            readDevice.Name = "Read Connected Device";
            CSTagTypes.CommandData.CommandParameters_ReadConnectedDevices readParams = new CSTagTypes.CommandData.CommandParameters_ReadConnectedDevices();
            readParams.readDeviceTag = new CSTagTypes.Tags.ReadConnectedDevice();
            readParams.startDelayTag = new CSTagTypes.Tags.CommandStartDelay();
            readDevice.ParameterType = readParams;
            GetParameterList(readDevice.ParameterType, ref paramList);
            readDevice.Parameters = paramList.ToArray();
            readDevice.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(readDevice);
            paramList.Clear();

            CommandData readMemory = new CommandData();
            readMemory.Name = "Read Memory";
            CSTagTypes.CommandData.CommandParameters_ReadMemory memParams = new CSTagTypes.CommandData.CommandParameters_ReadMemory();
            memParams.readMemoryTag = new CSTagTypes.Tags.ReadMemory();
            readMemory.ParameterType = memParams;
            GetParameterList(readMemory.ParameterType, ref paramList);
            readMemory.Parameters = paramList.ToArray();
            readMemory.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(readMemory);
            paramList.Clear();

            CommandData lteTest = new CommandData();
            lteTest.Name = "LTE Carrier Assert";
            CSTagTypes.CommandData.CommandParameters_LTECarrierAssert lteParams = new CSTagTypes.CommandData.CommandParameters_LTECarrierAssert();
            lteParams.durationTag = new CSTagTypes.Tags.CommandDuration();
            lteParams.rfTestTag = new CSTagTypes.Tags.RFTestMode();
            lteParams.startDelayTag = new CSTagTypes.Tags.CommandStartDelay();
            lteTest.ParameterType = lteParams;
            GetParameterList(lteTest.ParameterType, ref paramList);
            lteTest.Parameters = paramList.ToArray();
            lteTest.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(lteTest);
            paramList.Clear();

            CommandData btleTest = new CommandData();
            btleTest.Name = "BTLE Carrier Assert";
            CSTagTypes.CommandData.CommandParameters_BTLECarrierAssert btleParams = new CSTagTypes.CommandData.CommandParameters_BTLECarrierAssert();
            btleParams.durationTag = new CSTagTypes.Tags.CommandDuration();
            btleParams.rfTestTag = new CSTagTypes.Tags.BLE_RFTestMode();
            btleParams.startDelayTag = new CSTagTypes.Tags.CommandStartDelay();
            btleTest.ParameterType = btleParams;
            GetParameterList(btleTest.ParameterType, ref paramList);
            btleTest.Parameters = paramList.ToArray();
            btleTest.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(btleTest);
            paramList.Clear();

            CommandData power = new CommandData();
            power.Name = "Set Power Mode";
            CSTagTypes.CommandData.CommandParameters_SetPowerMode powerParams = new CSTagTypes.CommandData.CommandParameters_SetPowerMode();
            powerParams.durationTag = new CSTagTypes.Tags.CommandDuration();
            powerParams.powerModeBitMaskTag = new CSTagTypes.Tags.SetPowerMode();
            powerParams.startDelayTag = new CSTagTypes.Tags.CommandStartDelay();
            power.ParameterType = powerParams;
            GetParameterList(power.ParameterType, ref paramList);
            power.Parameters = paramList.ToArray();
            power.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(power);
            paramList.Clear();

            CommandData toggleModem = new CommandData();
            toggleModem.Name = "Toggle LTE Modem";
            CSTagTypes.CommandData.CommandParameters_ToggleLTEModem modemParams = new CSTagTypes.CommandData.CommandParameters_ToggleLTEModem();
            modemParams.durationTag = new CSTagTypes.Tags.CommandDuration();
            modemParams.modemTag = new CSTagTypes.Tags.ToggleModem();
            modemParams.startDelayTag = new CSTagTypes.Tags.CommandStartDelay();
            toggleModem.ParameterType = modemParams;
            GetParameterList(toggleModem.ParameterType, ref paramList);
            toggleModem.Parameters = paramList.ToArray();
            toggleModem.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(toggleModem);
            paramList.Clear();

            CommandData publishPacket = new CommandData();
            publishPacket.Name = "Publish Specified Packet";
            CSTagTypes.CommandData.CommandParameters_PublishRequestedPacket publishParams = new CSTagTypes.CommandData.CommandParameters_PublishRequestedPacket();
            publishParams.destinationTag = new CSTagTypes.Tags.PublishPacketDestination();
            publishParams.packetTypeTag = new CSTagTypes.Tags.PublishPacketType();
            publishPacket.ParameterType = publishParams;
            GetParameterList(publishPacket.ParameterType, ref paramList);
            publishPacket.Parameters = paramList.ToArray();
            publishPacket.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(publishPacket);
            paramList.Clear();

            CommandData eraseMem = new CommandData();
            eraseMem.Name = "Erase Memory Image";
            CSTagTypes.CommandData.CommandParameters_EraseMemoryImage eraseParams = new CSTagTypes.CommandData.CommandParameters_EraseMemoryImage();
            eraseParams.memoryImageBitmaskTag = new CSTagTypes.Tags.EraseMemoryImage();
            eraseMem.ParameterType = eraseParams;
            GetParameterList(eraseMem.ParameterType, ref paramList);
            eraseMem.Parameters = paramList.ToArray();
            eraseMem.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(eraseMem);
            paramList.Clear();

            CommandData recrepInt = new CommandData();
            recrepInt.Name = "Set Recording and Reporting Intervals";
            CSTagTypes.CommandData.CommandParameters_SetRecordingReportingIntervals recrepParams = new CSTagTypes.CommandData.CommandParameters_SetRecordingReportingIntervals();
            recrepParams.intervalsTag = new CSTagTypes.Tags.RecordingReportingInterval();
            recrepInt.ParameterType = recrepParams;
            GetParameterList(recrepInt.ParameterType, ref paramList);
            recrepInt.Parameters = paramList.ToArray();
            recrepInt.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(recrepInt);
            paramList.Clear();

            CommandData sleepSec = new CommandData();
            sleepSec.Name = "Sleep Seconds";
            CSTagTypes.CommandData.CommandParameters_SleepSeconds sleepParams = new CSTagTypes.CommandData.CommandParameters_SleepSeconds();
            sleepParams.startDelayTag = new CSTagTypes.Tags.CommandStartDelay();
            sleepParams.durationTag = new CSTagTypes.Tags.CommandDuration();
            sleepSec.ParameterType = sleepParams;
            GetParameterList(sleepSec.ParameterType, ref paramList);
            sleepSec.Parameters = paramList.ToArray();
            sleepSec.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(sleepSec);
            paramList.Clear();

            CommandData getCAN = new CommandData();
            getCAN.Name = "Get CAN+ Data";
            CSTagTypes.CommandData.CommandParameters_GetCANData canParams = new CSTagTypes.CommandData.CommandParameters_GetCANData();
            getCAN.ParameterType = canParams;
            getCAN.Parameters = null;
            getCAN.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(getCAN);

            CommandData getDebug = new CommandData();
            getDebug.Name = "Get Debug Log";
            CSTagTypes.CommandData.CommandParameters_RequestDebugLog debugParams = new CSTagTypes.CommandData.CommandParameters_RequestDebugLog();
            getDebug.ParameterType = debugParams;
            getDebug.Parameters = null;
            getDebug.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(getDebug);

            CommandData getCMIUSignal = new CommandData();
            getCMIUSignal.Name = "Get CMIU Signal Quality";
            CSTagTypes.CommandData.CommandParameters_CMIUSignalQuality signalParams = new CSTagTypes.CommandData.CommandParameters_CMIUSignalQuality();
            getCMIUSignal.ParameterType = signalParams;
            getCMIUSignal.Parameters = null;
            getCMIUSignal.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(getCMIUSignal);

            CommandData modemFOTA = new CommandData();
            modemFOTA.Name = "Set Modem FOTA";
            CSTagTypes.CommandData.CommandParameters_ModemFOTA fota = new CSTagTypes.CommandData.CommandParameters_ModemFOTA();
            modemFOTA.ParameterType = fota;
            fota.ftpDNS = new CSTagTypes.Tags.FTPDNS();
            fota.ftpPassword = new CSTagTypes.Tags.FTPPassword();
            fota.ftpPortNumber = new CSTagTypes.Tags.FTPPortNumber();
            fota.ftpUsername = new CSTagTypes.Tags.FTPUsername();
            fota.imageFilename = new CSTagTypes.Tags.ImageFilename();
            fota.newImageVersion = new CSTagTypes.Tags.NewImageVersionInfo();
            GetParameterList(modemFOTA.ParameterType, ref paramList);
            modemFOTA.Parameters = paramList.ToArray();
            modemFOTA.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(modemFOTA);
            paramList.Clear();

            CommandData apnGet = new CommandData();
            apnGet.Name = "Get CMIU APN";
            CSTagTypes.CommandData.CommandParameters_APN_Get apnGetParams = new CSTagTypes.CommandData.CommandParameters_APN_Get();
            apnGet.ParameterType = apnGetParams;
            apnGet.Parameters = null;
            apnGet.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(apnGet);
            paramList.Clear();

            CommandData apnSet = new CommandData();
            apnSet.Name = "Set CMIU APN";
            CSTagTypes.CommandData.CommandParameters_APN_Set apnSetParams = new CSTagTypes.CommandData.CommandParameters_APN_Set();
            apnSet.ParameterType = apnSetParams;
            apnSetParams.cmiuAPN = new CSTagTypes.Tags.CMIUAPN();
            GetParameterList(apnSet.ParameterType, ref paramList);
            apnSet.Parameters = paramList.ToArray(); ;
            apnSet.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(apnSet);
            paramList.Clear();

            CommandData runConnectivityTest = new CommandData();
            runConnectivityTest.Name = "Run Connectivity Test";
            CSTagTypes.CommandData.CommandParameters_RunConnectivityTest runConnectivityTestParams = new CSTagTypes.CommandData.CommandParameters_RunConnectivityTest();
            runConnectivityTest.ParameterType = runConnectivityTestParams;
            runConnectivityTestParams.packetInstigator = new CSTagTypes.Tags.PacketInstigator();
            GetParameterList(runConnectivityTest.ParameterType, ref paramList);
            runConnectivityTest.Parameters = paramList.ToArray();
            runConnectivityTest.CommandParameters = new ObservableCollection<CommandParameter>();
            CommandList.Add(runConnectivityTest);
            paramList.Clear();
        }

        // Retrieve the command names 
        public string[] GetCommandNames()
        {
            List<string> names = new List<string>();
            foreach (CommandData cmd in CommandList)
            {
                names.Add(cmd.Name);
            }

            return names.ToArray();
        }

        // Update the parameters for the command requested
        public void UpdateParametersForCommand(int commandIndex)
        {
            // Check if the command index is valid
            if (commandIndex >= CommandList.Count)
            {
                return;
            }

            // Reset the command parameters
            CommandPacketParameters = new ObservableCollection<CommandParameter>();

            // Get the command
            CommandData command = CommandList[commandIndex];

            // Check if the command has any parameters
            if (command.Parameters == null)
            {
                return;
            }

            if (CommandList[commandIndex].CommandParameters.Count == 0)
            {
                foreach (string name in command.Parameters)
                {
                    CommandParameter parameter = new CommandParameter();
                    parameter.Name = name;
                    parameter.Value = "0";
                    CommandPacketParameters.Add(parameter);
                }
            }
            else
            {
                // Get the saved parameters
                CommandPacketParameters = CommandList[commandIndex].CommandParameters;
            }
            

            
        }

        public bool CreateHotKey(ref CMIUCommands cmiuCMDs, System.Windows.Input.Key key, int index, CMIUCommands.CommandInterface cmdInterface)
        {
            // Check if the key already exists
            int check = HotKeyCommands.FindIndex(x => x.hotKey == key);
            if(check >= 0)
            {
                // There is already a hotkey with this key
                return false;
            }
            else
            {
                return CommandPacketHandler(ref cmiuCMDs, cmdInterface, index, CommandHandlerOption.CreateHotKey, key);
            }
            
        }
        

        public bool SendHotKeyCommand(System.Windows.Input.Key key, ref CMIUCommands cmiuCMDs, CMIUCommands.CommandInterface cmdInterface)
        {
            // Get the HotKeyCommand index
            int index = HotKeyCommands.FindIndex(x => x.hotKey == key);
            if(index >= 0)
            {
                return CommandPacketHandler(ref cmiuCMDs, cmdInterface, index, CommandHandlerOption.SendHotKey, System.Windows.Input.Key.Insert);
            }
            else
            {
                return false;
            }
            
        }

        public bool SendCommandPacket(ref CMIUCommands cmiuCMDs, CMIUCommands.CommandInterface cmdInterface, int index)
        {
            return CommandPacketHandler(ref cmiuCMDs, cmdInterface, index, CommandHandlerOption.Send, System.Windows.Input.Key.Insert);
        }



        // This is kind of a terrible function...it's doing too much, but this is to avoid multiple functions with if/else for each CommandParameter type
        private bool CommandPacketHandler(ref CMIUCommands cmiuCMDs, CMIUCommands.CommandInterface cmdInterface, int index, CommandHandlerOption option , System.Windows.Input.Key key)
        {
            
            // If sending a hot key, get the parameter type from the hoy key commands
            // If not, we are sending a command or creating a command based on the current selection, so we get the command data from that
            CSTagTypes.CommandData.CommandPacketParameters parameterType;
            if(option == CommandHandlerOption.SendHotKey)
            {
                parameterType = HotKeyCommands[index].packetParams;
            }
            else
            {
                CommandData command = CommandList[index];
                parameterType = command.ParameterType;

                // Make sure the UI matches the selected command parameters
                if (command.Parameters != null && command.Parameters.Count() != CommandPacketParameters.Count)
                {
                    return false;
                }

                // Set the command parameters
                CommandList[index].CommandParameters = CommandPacketParameters;
            } 

            try
            {
                // Determine which parameter type we are trying to use
                // Once that parameter type is determined, we do different things depeding on the command option
                // If we are sending a hotkey send first and exit
                // If not create the object
                // If we are creating a hot key, add the function to call, then create and add the user parameters and add to the hotkey list
                // If we are sending a command, just send the command with the user parameters

                if (parameterType is CSTagTypes.CommandData.CommandParameters_LTECarrierAssert)
                {
                    CSTagTypes.CommandData.CommandParameters_LTECarrierAssert lte = new CSTagTypes.CommandData.CommandParameters_LTECarrierAssert();

                    
                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = HotKeyFunctions[index](HotKeyCommands[index].cmdInterface, (CSTagTypes.CommandData.CommandParameters_LTECarrierAssert)HotKeyCommands[index].packetParams);
                        ParsedCommand = GetParsedCommand((CSTagTypes.CommandData.CommandParameters_LTECarrierAssert)HotKeyCommands[index].packetParams);
                        return success;
                    }
                    else
                    {                        
                        lte.durationTag = new CSTagTypes.Tags.CommandDuration();
                        lte.rfTestTag = new CSTagTypes.Tags.RFTestMode();
                        lte.startDelayTag = new CSTagTypes.Tags.CommandStartDelay();
                        lte.startDelayTag.startDelay = UInt64.Parse(CommandPacketParameters[0].Value);
                        lte.durationTag.duration = UInt64.Parse(CommandPacketParameters[1].Value);                        
                        lte.rfTestTag.rfBand = Byte.Parse(CommandPacketParameters[2].Value);
                        lte.rfTestTag.rfChannel = UInt16.Parse(CommandPacketParameters[3].Value);
                        lte.rfTestTag.rfPowerLvel = Byte.Parse(CommandPacketParameters[4].Value);
                        ParsedCommand = GetParsedCommand(lte);
                    }
                    
                    
                    if(option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, CSTagTypes.CommandData.CommandParameters_LTECarrierAssert, bool>(cmiuCMDs.PacketSend_Command_LTE_RFTestMode));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = lte;
                        hotKeyCmd.Name = "LTE Carrier Assert";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_LTE_RFTestMode(cmdInterface, lte);
                    }                    
                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_BTLECarrierAssert)
                {
                    CSTagTypes.CommandData.CommandParameters_BTLECarrierAssert btle = new CSTagTypes.CommandData.CommandParameters_BTLECarrierAssert();


                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = HotKeyFunctions[index](HotKeyCommands[index].cmdInterface, (CSTagTypes.CommandData.CommandParameters_BTLECarrierAssert)HotKeyCommands[index].packetParams);
                        ParsedCommand = GetParsedCommand((CSTagTypes.CommandData.CommandParameters_BTLECarrierAssert)HotKeyCommands[index].packetParams);
                        return success;
                    }
                    else
                    {
                        btle.durationTag = new CSTagTypes.Tags.CommandDuration();
                        btle.rfTestTag = new CSTagTypes.Tags.BLE_RFTestMode();
                        btle.startDelayTag = new CSTagTypes.Tags.CommandStartDelay();
                        btle.startDelayTag.startDelay = UInt64.Parse(CommandPacketParameters[0].Value);
                        btle.durationTag.duration = UInt64.Parse(CommandPacketParameters[1].Value);
                        btle.rfTestTag.rfCommand = Byte.Parse(CommandPacketParameters[2].Value);
                        btle.rfTestTag.rfFrequency = Byte.Parse(CommandPacketParameters[3].Value);
                        btle.rfTestTag.rfPacketLength = Byte.Parse(CommandPacketParameters[4].Value);
                        btle.rfTestTag.rfPacketType = Byte.Parse(CommandPacketParameters[5].Value);
                        ParsedCommand = GetParsedCommand(btle);
                    }


                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, CSTagTypes.CommandData.CommandParameters_BTLECarrierAssert, bool>(cmiuCMDs.PacketSend_Command_BTLE_RFTestMode));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = btle;
                        hotKeyCmd.Name = "BTLE Carrier Assert";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_BTLE_RFTestMode(cmdInterface, btle);
                    }
                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_ReadMemory)
                {
                    CSTagTypes.CommandData.CommandParameters_ReadMemory memory = new CSTagTypes.CommandData.CommandParameters_ReadMemory();

                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = HotKeyFunctions[index](HotKeyCommands[index].cmdInterface, (CSTagTypes.CommandData.CommandParameters_ReadMemory)HotKeyCommands[index].packetParams);
                        ParsedCommand = GetParsedCommand((CSTagTypes.CommandData.CommandParameters_ReadMemory)HotKeyCommands[index].packetParams);
                        return success;
                    }
                    else
                    {
                        memory.readMemoryTag = new CSTagTypes.Tags.ReadMemory();
                        memory.readMemoryTag.startAddress = UInt16.Parse(CommandPacketParameters[0].Value);
                        memory.readMemoryTag.numberOfBytes = UInt16.Parse(CommandPacketParameters[1].Value);
                        ParsedCommand = GetParsedCommand(memory);
                    }                    
                    

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, CSTagTypes.CommandData.CommandParameters_ReadMemory, bool>(cmiuCMDs.PacketSend_Command_ReadMemory));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = memory;
                        hotKeyCmd.Name = "Read Memory";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_ReadMemory(cmdInterface, memory);
                    }                    
                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_ReadConnectedDevices)
                {
                    CSTagTypes.CommandData.CommandParameters_ReadConnectedDevices device = new CSTagTypes.CommandData.CommandParameters_ReadConnectedDevices();

                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = HotKeyFunctions[index](HotKeyCommands[index].cmdInterface, (CSTagTypes.CommandData.CommandParameters_ReadConnectedDevices)HotKeyCommands[index].packetParams);
                        ParsedCommand = GetParsedCommand((CSTagTypes.CommandData.CommandParameters_ReadConnectedDevices)HotKeyCommands[index].packetParams);
                        return success;
                    }
                    else
                    {
                        device.startDelayTag = new CSTagTypes.Tags.CommandStartDelay();
                        device.readDeviceTag = new CSTagTypes.Tags.ReadConnectedDevice();
                        device.startDelayTag.startDelay = UInt64.Parse(CommandPacketParameters[0].Value);
                        device.readDeviceTag.pulse = Byte.Parse(CommandPacketParameters[1].Value);
                        device.readDeviceTag.delayBetweenReads = UInt64.Parse(CommandPacketParameters[2].Value);
                        device.readDeviceTag.numberOfRepeats = UInt64.Parse(CommandPacketParameters[3].Value);
                        ParsedCommand = GetParsedCommand(device);
                    }                   


                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, CSTagTypes.CommandData.CommandParameters_ReadConnectedDevices, bool>(cmiuCMDs.PacketSend_Command_ReadConnectedDevices));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = device;
                        hotKeyCmd.Name = "Read Connected Device";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_ReadConnectedDevices(cmdInterface, device);
                    }
                   
                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_SetPowerMode)
                {
                    CSTagTypes.CommandData.CommandParameters_SetPowerMode power = new CSTagTypes.CommandData.CommandParameters_SetPowerMode();
                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = HotKeyFunctions[index](HotKeyCommands[index].cmdInterface, (CSTagTypes.CommandData.CommandParameters_SetPowerMode)HotKeyCommands[index].packetParams);
                        ParsedCommand = GetParsedCommand((CSTagTypes.CommandData.CommandParameters_SetPowerMode)HotKeyCommands[index].packetParams);
                        return success;
                    }
                    else
                    {
                        power.startDelayTag = new CSTagTypes.Tags.CommandStartDelay();
                        power.durationTag = new CSTagTypes.Tags.CommandDuration();
                        power.powerModeBitMaskTag = new CSTagTypes.Tags.SetPowerMode();
                        power.startDelayTag.startDelay = UInt64.Parse(CommandPacketParameters[0].Value);
                        power.durationTag.duration = UInt64.Parse(CommandPacketParameters[1].Value);
                        power.powerModeBitMaskTag.powerMode = Byte.Parse(CommandPacketParameters[2].Value);
                        ParsedCommand = GetParsedCommand(power);
                    }

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, CSTagTypes.CommandData.CommandParameters_SetPowerMode, bool>(cmiuCMDs.PacketSend_Command_SetPowerMode));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = power;
                        hotKeyCmd.Name = "Set Power Mode";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_SetPowerMode(cmdInterface, power);
                    }

                    
                    
                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_ToggleLTEModem)
                {
                    CSTagTypes.CommandData.CommandParameters_ToggleLTEModem modem = new CSTagTypes.CommandData.CommandParameters_ToggleLTEModem();
                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = HotKeyFunctions[index](HotKeyCommands[index].cmdInterface, (CSTagTypes.CommandData.CommandParameters_ToggleLTEModem)HotKeyCommands[index].packetParams);
                        ParsedCommand = GetParsedCommand((CSTagTypes.CommandData.CommandParameters_ToggleLTEModem)HotKeyCommands[index].packetParams);
                        return success;
                    }
                    else
                    {
                        modem.startDelayTag = new CSTagTypes.Tags.CommandStartDelay();
                        modem.durationTag = new CSTagTypes.Tags.CommandDuration();
                        modem.modemTag = new CSTagTypes.Tags.ToggleModem();
                        modem.startDelayTag.startDelay = UInt64.Parse(CommandPacketParameters[0].Value);
                        modem.durationTag.duration = UInt64.Parse(CommandPacketParameters[1].Value);
                        modem.modemTag.modemStatus = Byte.Parse(CommandPacketParameters[2].Value);
                        ParsedCommand = GetParsedCommand(modem);
                    }

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, CSTagTypes.CommandData.CommandParameters_ToggleLTEModem, bool>(cmiuCMDs.PacketSend_Command_ToggleModem));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = modem;
                        hotKeyCmd.Name = "Toggle LTE Modem";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_ToggleModem(cmdInterface, modem);
                    }
                    
                    
                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_PublishRequestedPacket)
                {
                    CSTagTypes.CommandData.CommandParameters_PublishRequestedPacket publish = new CSTagTypes.CommandData.CommandParameters_PublishRequestedPacket();
                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = HotKeyFunctions[index](HotKeyCommands[index].cmdInterface, (CSTagTypes.CommandData.CommandParameters_PublishRequestedPacket)HotKeyCommands[index].packetParams);
                        ParsedCommand = GetParsedCommand((CSTagTypes.CommandData.CommandParameters_PublishRequestedPacket)HotKeyCommands[index].packetParams);
                        return success;
                    }
                    else
                    {
                        publish.packetTypeTag = new CSTagTypes.Tags.PublishPacketType();
                        publish.destinationTag = new CSTagTypes.Tags.PublishPacketDestination();
                        publish.packetTypeTag.packetType = Byte.Parse(CommandPacketParameters[0].Value);
                        publish.destinationTag.destination = Byte.Parse(CommandPacketParameters[1].Value);
                        ParsedCommand = GetParsedCommand(publish);
                    }

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, CSTagTypes.CommandData.CommandParameters_PublishRequestedPacket, bool>(cmiuCMDs.PacketSend_Command_PublishRequestedPacket));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = publish;
                        hotKeyCmd.Name = "Publish Requested Packet";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_PublishRequestedPacket(cmdInterface, publish);
                    }
                    
                   
                }

                if(parameterType is CSTagTypes.CommandData.CommandParameters_EraseMemoryImage)
                {
                    CSTagTypes.CommandData.CommandParameters_EraseMemoryImage erase = new CSTagTypes.CommandData.CommandParameters_EraseMemoryImage();
                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = HotKeyFunctions[index](HotKeyCommands[index].cmdInterface, (CSTagTypes.CommandData.CommandParameters_EraseMemoryImage)HotKeyCommands[index].packetParams);
                        ParsedCommand = GetParsedCommand((CSTagTypes.CommandData.CommandParameters_EraseMemoryImage)HotKeyCommands[index].packetParams);
                        return success;
                    }
                    else
                    {
                        erase.memoryImageBitmaskTag = new CSTagTypes.Tags.EraseMemoryImage();
                        erase.memoryImageBitmaskTag.memoryImageBitmask = UInt16.Parse(CommandPacketParameters[0].Value);
                        ParsedCommand = GetParsedCommand(erase);
                    }

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, CSTagTypes.CommandData.CommandParameters_EraseMemoryImage, bool>(cmiuCMDs.PacketSend_Command_EraseMemoryImage));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = erase;
                        hotKeyCmd.Name = "Erase Memory Image";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_EraseMemoryImage(cmdInterface, erase);
                    }
                    
                    
                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_SetRecordingReportingIntervals)
                {
                    CSTagTypes.CommandData.CommandParameters_SetRecordingReportingIntervals intervals = new CSTagTypes.CommandData.CommandParameters_SetRecordingReportingIntervals();
                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = HotKeyFunctions[index](HotKeyCommands[index].cmdInterface, (CSTagTypes.CommandData.CommandParameters_SetRecordingReportingIntervals)HotKeyCommands[index].packetParams);
                        ParsedCommand = GetParsedCommand((CSTagTypes.CommandData.CommandParameters_SetRecordingReportingIntervals)HotKeyCommands[index].packetParams);
                        return success;
                    }
                    else
                    {
                        intervals.intervalsTag = new CSTagTypes.Tags.RecordingReportingInterval();
                        intervals.intervalsTag.reportingStartMins = UInt16.Parse(CommandPacketParameters[0].Value);
                        intervals.intervalsTag.reportingIntervalHours = Byte.Parse(CommandPacketParameters[1].Value);
                        intervals.intervalsTag.reportingRetries = Byte.Parse(CommandPacketParameters[2].Value);
                        intervals.intervalsTag.reportingTransmitWindowsMins = Byte.Parse(CommandPacketParameters[3].Value);
                        intervals.intervalsTag.reportingQuietStartMins = UInt16.Parse(CommandPacketParameters[4].Value);
                        intervals.intervalsTag.reportingQuietEndMins = UInt16.Parse(CommandPacketParameters[5].Value);
                        intervals.intervalsTag.recordingStartMins = UInt16.Parse(CommandPacketParameters[6].Value);
                        intervals.intervalsTag.recordingIntervalMins = Byte.Parse(CommandPacketParameters[7].Value);
                        ParsedCommand = GetParsedCommand(intervals);
                    }

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, CSTagTypes.CommandData.CommandParameters_SetRecordingReportingIntervals, bool>(cmiuCMDs.PacketSend_Command_SetRecordingReportingIntervals));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = intervals;
                        hotKeyCmd.Name = "Set Recording Reporting Intervals";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_SetRecordingReportingIntervals(cmdInterface, intervals);
                    }
                    
                    
                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_SleepSeconds)
                {
                    CSTagTypes.CommandData.CommandParameters_SleepSeconds sleep = new CSTagTypes.CommandData.CommandParameters_SleepSeconds();
                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = HotKeyFunctions[index](HotKeyCommands[index].cmdInterface, (CSTagTypes.CommandData.CommandParameters_SleepSeconds)HotKeyCommands[index].packetParams);
                        ParsedCommand = GetParsedCommand((CSTagTypes.CommandData.CommandParameters_SleepSeconds)HotKeyCommands[index].packetParams);
                        return success;
                    }
                    else
                    {
                        sleep.startDelayTag = new CSTagTypes.Tags.CommandStartDelay();
                        sleep.durationTag = new CSTagTypes.Tags.CommandDuration();
                        sleep.startDelayTag.startDelay = UInt64.Parse(CommandPacketParameters[0].Value);
                        sleep.durationTag.duration = UInt64.Parse(CommandPacketParameters[1].Value);
                        ParsedCommand = GetParsedCommand(sleep);
                    }

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, CSTagTypes.CommandData.CommandParameters_SleepSeconds, bool>(cmiuCMDs.PacketSend_Command_SleepSeconds));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = sleep;
                        hotKeyCmd.Name = "Sleep Seconds";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_SleepSeconds(cmdInterface, sleep);
                    }
                    
                    
                }


                if(parameterType is CSTagTypes.CommandData.CommandParameters_GetCANData)
                {
                    ParsedCommand = "\r Get CAN+ Data \r";
                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = cmiuCMDs.PacketSend_Command_GetCANData(HotKeyCommands[index].cmdInterface);
                        return success;
                    }

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, bool>(cmiuCMDs.PacketSend_Command_GetCANData));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = new CSTagTypes.CommandData.CommandParameters_GetCANData();
                        hotKeyCmd.Name = "Get CAN+ Data";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_GetCANData(cmdInterface);
                    }

                    
                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_RequestDebugLog)
                {
                    ParsedCommand = "\r Get Debug Log \r";

                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = cmiuCMDs.PacketSend_Command_GetDebugLog(HotKeyCommands[index].cmdInterface);
                        return success;
                    }

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, bool>(cmiuCMDs.PacketSend_Command_GetDebugLog));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = new CSTagTypes.CommandData.CommandParameters_RequestDebugLog();
                        hotKeyCmd.Name = "Get Debug Log";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_GetDebugLog(cmdInterface);
                    }
                    
                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_CMIUSignalQuality)
                {
                    ParsedCommand = "\r Get CMIU Signal Quality \r";

                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = cmiuCMDs.PacketSend_Command_GetCMIUSignalQuality(HotKeyCommands[index].cmdInterface);
                        return success;
                    }

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, bool>(cmiuCMDs.PacketSend_Command_GetCMIUSignalQuality));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = new CSTagTypes.CommandData.CommandParameters_CMIUSignalQuality();
                        hotKeyCmd.Name = "Get CMIU Signal Quality";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_GetCMIUSignalQuality(cmdInterface);
                    }
                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_ModemFOTA)
                {
                    CSTagTypes.CommandData.CommandParameters_ModemFOTA fota = new CSTagTypes.CommandData.CommandParameters_ModemFOTA();
                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = HotKeyFunctions[index](HotKeyCommands[index].cmdInterface, (CSTagTypes.CommandData.CommandParameters_ModemFOTA)HotKeyCommands[index].packetParams);
                        ParsedCommand = GetParsedCommand((CSTagTypes.CommandData.CommandParameters_ModemFOTA)HotKeyCommands[index].packetParams);
                        return success;
                    }
                    else
                    {
                        fota.ftpDNS = new CSTagTypes.Tags.FTPDNS();
                        fota.ftpPortNumber = new CSTagTypes.Tags.FTPPortNumber();
                        fota.ftpUsername = new CSTagTypes.Tags.FTPUsername();
                        fota.ftpPassword = new CSTagTypes.Tags.FTPPassword();
                        fota.imageFilename = new CSTagTypes.Tags.ImageFilename();
                        fota.newImageVersion = new CSTagTypes.Tags.NewImageVersionInfo();

                        fota.ftpDNS.ftpDNS = CommandPacketParameters[0].Value;
                        fota.ftpPortNumber.ftpPortNumber = UInt16.Parse(CommandPacketParameters[1].Value);
                        fota.ftpUsername.ftpUsername = CommandPacketParameters[2].Value;
                        fota.ftpPassword.ftpPassword = CommandPacketParameters[3].Value;
                        fota.imageFilename.imageFilename = CommandPacketParameters[4].Value;
                        fota.newImageVersion.newImageVerionInfo = CommandPacketParameters[5].Value;

                        ParsedCommand = GetParsedCommand(fota);
                    }

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, CSTagTypes.CommandData.CommandParameters_ModemFOTA, bool>(cmiuCMDs.PacketSend_Command_SetModemFOTA));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = fota;
                        hotKeyCmd.Name = "Set Modem FOTA";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_SetModemFOTA(cmdInterface, fota);
                    }

                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_APN_Get)
                {
                    ParsedCommand = "\r Get CMIU APN \r";

                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = cmiuCMDs.PacketSend_Command_CMIUAPN_Get(HotKeyCommands[index].cmdInterface);
                        return success;
                    }

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, bool>(cmiuCMDs.PacketSend_Command_CMIUAPN_Get));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = new CSTagTypes.CommandData.CommandParameters_APN_Get();
                        hotKeyCmd.Name = "Get CMIU APN";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_CMIUAPN_Get(cmdInterface);
                    }
                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_APN_Set)
                {
                    CSTagTypes.CommandData.CommandParameters_APN_Set apn = new CSTagTypes.CommandData.CommandParameters_APN_Set();
                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = HotKeyFunctions[index](HotKeyCommands[index].cmdInterface, (CSTagTypes.CommandData.CommandParameters_APN_Set)HotKeyCommands[index].packetParams);
                        ParsedCommand = GetParsedCommand((CSTagTypes.CommandData.CommandParameters_APN_Set)HotKeyCommands[index].packetParams);
                        return success;
                    }
                    else
                    {
                        apn.cmiuAPN = new CSTagTypes.Tags.CMIUAPN();


                        apn.cmiuAPN.cmiuAPN = CommandPacketParameters[0].Value;

                        ParsedCommand = GetParsedCommand(apn);
                    }

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, CSTagTypes.CommandData.CommandParameters_APN_Set, bool>(cmiuCMDs.PacketSend_Command_CMIUAPN_Set));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = apn;
                        hotKeyCmd.Name = "Set CMIU APN";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_CMIUAPN_Set(cmdInterface, apn);
                    }

                }

                if (parameterType is CSTagTypes.CommandData.CommandParameters_RunConnectivityTest)
                {
                    CSTagTypes.CommandData.CommandParameters_RunConnectivityTest runConnectivityTest = new CSTagTypes.CommandData.CommandParameters_RunConnectivityTest();
                    if (option == CommandHandlerOption.SendHotKey)
                    {
                        bool success = HotKeyFunctions[index](HotKeyCommands[index].cmdInterface, (CSTagTypes.CommandData.CommandParameters_RunConnectivityTest)HotKeyCommands[index].packetParams);
                        ParsedCommand = GetParsedCommand((CSTagTypes.CommandData.CommandParameters_RunConnectivityTest)HotKeyCommands[index].packetParams);
                        return success;
                    }
                    else
                    {
                        runConnectivityTest.packetInstigator = new CSTagTypes.Tags.PacketInstigator();
                        runConnectivityTest.packetInstigator.packetInstigator = (CSTagTypes.Tags.E_PACKET_INSTIGATOR)Byte.Parse(CommandPacketParameters[0].Value);
                        ParsedCommand = GetParsedCommand(runConnectivityTest);
                    }

                    if (option == CommandHandlerOption.CreateHotKey)
                    {
                        HotKeyFunctions.Add(new Func<CMIUCommandLibrary.CMIUCommands.CommandInterface, CSTagTypes.CommandData.CommandParameters_RunConnectivityTest, bool>(cmiuCMDs.PacketSend_Command_RunConnectivityTest));
                        HotKeyCommand hotKeyCmd = new HotKeyCommand();
                        hotKeyCmd.cmdInterface = cmdInterface;
                        hotKeyCmd.hotKey = key;
                        hotKeyCmd.packetParams = runConnectivityTest;
                        hotKeyCmd.Name = "Run Connectivity Test";
                        HotKeyCommands.Add(hotKeyCmd);
                        return true;
                    }
                    else
                    {
                        return cmiuCMDs.PacketSend_Command_RunConnectivityTest(cmdInterface, runConnectivityTest);
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;

        }

        public bool IsHotKey(System.Windows.Input.Key key)
        {

            int check = HotKeyCommands.FindIndex(x => x.hotKey == key);
            if (check >= 0)
            {
                // There is already a hotkey with this key
                return true;
            }
            else
            {
                return false;
            }
        }



        private void GetParameterList(object parameters, ref List<string> paramList)
        {
            
            var fields = parameters.GetType().GetFields();

            // TESTING
            //var things = parameters.GetType().GetProperties();
            //if(things.Length > 0)
            //{
            //    var nameAttr = things[0].GetCustomAttributes(typeof(DisplayNameAttribute), true);
            //    if (nameAttr.Length > 0)
            //    {
            //        string namey = (nameAttr[0] as DisplayNameAttribute).DisplayName;
            //    }

            //    var maxAttr = things[0].GetCustomAttributes(typeof(CSTagTypes.MaxValueAttribute), true);
            //    if (nameAttr.Length > 0)
            //    {
            //        UInt64 max = (maxAttr[0] as CSTagTypes.MaxValueAttribute).maxValue;
            //    }
            //}

            foreach (var name in fields)
            {
                if (name.FieldType.BaseType == typeof(CSTagTypes.Tags.Tag))
                {
                    var tagNames = name.GetValue(parameters);
                    if (tagNames != null)
                    {
                        GetParameterList(tagNames, ref paramList);
                    }

                }
                else
                {
                    paramList.Add(name.Name);
                }
            }
        }


        private string GetParsedCommand(CSTagTypes.CommandData.CommandPacketParameters cmd)
        {
            if (cmd != null)
            {
                string parsed = "\r" + cmd.GetType().Name + "\r";
                TagHelper.GetAllTagContents(cmd, ref parsed, 0);
                return parsed;
            }

            return null;

        }
    }


    class CommandData
    {
        public string Name;
        public CSTagTypes.CommandData.CommandPacketParameters ParameterType;
        public string[] Parameters;
        public ObservableCollection<CommandParameter> CommandParameters;
    }
}
