﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CMITApp
{
    class LogFile
    {
        private static bool loggingEnabled = false;        
        private static string logPath = string.Empty;

        private static bool canDataLogging = false;
        private static string canDatalogPath = string.Empty;

        private static bool respLoggingEnabled = false;
        private static string respLogPath = string.Empty;

        private static Object logLock = new Object();
        private static Object canDataLock = new Object();
        private static Object respLogLock = new Object();

        public static void CreateLogFile(string path, string text)
        {
            lock(logLock)
            {
                File.AppendAllText(path, text);
            }            
        }

        public static void AppendToFile(string text)
        {
            lock (logLock)
            {
                if (loggingEnabled && logPath != string.Empty)
                {
                    TextWriter logger = new StreamWriter(logPath, true);
                    text = text + Environment.NewLine;
                    logger.WriteLine(text);
                    logger.Flush();
                    logger.Dispose();
                }
            }
                
        }

        public static void EnableLogging(string path)
        {
            logPath = path;
            loggingEnabled = true;
        }

        public static void EnableLogging()
        {
            loggingEnabled = true;
        }

        public static void DisableLogging()
        {
            loggingEnabled = false;
        }

        public static string GetLogPath()
        {
            return logPath;
        }

        // CAN Data Logging
        public static void EnableCANLogging(string path)
        {
            canDatalogPath = path;
            canDataLogging = true;
        }

        public static void EnableCANLogging()
        {
            canDataLogging = true;
        }

        public static void DisableCANLogging()
        {
            canDataLogging = false;
        }

        public static string GetCANDataLogPath()
        {
            return canDatalogPath;
        }

        public static void AppendToCANDataFile(string text)
        {
            lock (canDataLock)
            {
                if (canDataLogging && canDatalogPath != string.Empty)
                {
                    TextWriter logger = new StreamWriter(canDatalogPath, true);
                    text = text + Environment.NewLine;
                    logger.WriteLine(text);
                    logger.Flush();
                    logger.Dispose();
                }
            }
        }

        // Response Logging
        public static void EnableRespLogging(string path)
        {
            respLogPath = path;
            respLoggingEnabled = true;
        }

        public static void EnableRespLogging()
        {
            respLoggingEnabled = true;
        }

        public static void DisableRespLogging()
        {
            respLoggingEnabled = false;
        }

        public static string GetResponseLogPath()
        {
            return respLogPath;
        }

        public static void CreateRespLogFile(string path, string text)
        {
            lock (logLock)
            {
                File.AppendAllText(path, text);
            }
        }

        public static void AppendToRespLogFile(string text)
        {
            lock (respLogLock)
            {
                if (respLoggingEnabled && respLogPath != string.Empty)
                {
                    TextWriter logger = new StreamWriter(respLogPath, true);
                    text = text + Environment.NewLine;
                    logger.WriteLine(text);
                    logger.Flush();
                    logger.Dispose();
                }
            }
        }
    }
}
