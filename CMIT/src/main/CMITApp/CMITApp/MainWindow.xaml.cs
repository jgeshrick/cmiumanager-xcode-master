﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Threading;
using System.IO;
using Microsoft.Win32;
using CMIUCommandLibrary;
using IntelHexFileHelper;
using CSTagTypes;

namespace CMITApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // CMIU Command Library
        CMIUCommands cmiuCMDs;

        // Public variables
        public ObservableCollection<CMIUDeviceBTLEInfo> discoveredCMIUs { get; set; }
        public CMIUDeviceBTLEInfo selectedCMIU { get; set; }
        public int selectedCMIUIndex = -1;
        public CommandPackets commandPackets = new CommandPackets();

        // Private Variables
        private bool isScrollToBottomEnabled = true;

        // Lock required for UI Threading and Observable Collectoin
        private static object _lock = new object();

        // Device Memory
        Device cmiuDeviceMemory;

        // File location of input file
        private string InputFileLocation;

        // Command Interface
        private bool isToggleBTLE = true;
        CMIUCommandLibrary.CMIUCommands.CommandInterface selectedCmdInterface = CMIUCommands.CommandInterface.Bluetooth;

        private const string DETECT = "Detect";
        private const string DETECTED = "Detected";
        private const string CONNECT = "Connect";
        private const string CONNECTING = "Connecting...";
        private const string DISCONNECT = "Disconnect";
        private const string DISCONNECTED = "Disconnected";
        private const string DETECTING = "Detecting...";
        private const string UNDETECTED = "Undetected";
        private const string NOTFOUND = "Not Found";
        private const string SCAN = "Scan";
        private const string SCANNING = "Scanning";
        private const string CONNECTED = "Connected";

        public MainWindow()
        {
            InitializeComponent();
            LogInitalMessage();
            InitializeCommandLibrary();
            SetupUI();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                cmiuCMDs.BTLE_Dongle_Disconnect();
                cmiuCMDs.Serial_SerialPort_Close();
                cmiuCMDs.JLink_Disconnect();
                LogFile.DisableLogging();
                LogFile.DisableRespLogging();
                LogFile.DisableCANLogging();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
            }

            base.OnClosing(e);
        }

        #region Setup
        void SetupUI ()
        {
            DataContext = this;
            btnConfigProgram.IsEnabled = false;
            btnSaveFile.IsEnabled = false;
            btnSaveImage.IsEnabled = false;

            // Bluetooth Setup
            discoveredCMIUs = new ObservableCollection<CMIUDeviceBTLEInfo>();
            lstBxDiscoveredCMIUDevices.ItemsSource = discoveredCMIUs;
            btnBTConnect.IsEnabled = true;
            btnBTConnect.Content = DETECT;
            //btnBTScan.Content = DETECT;
            txtBTDongleStatus.Text = UNDETECTED;

            // Image Data Setup
            lstImageData.ItemsSource = null;

            // UART Setup
            btnUARTDisconnect.IsEnabled = false;

            // JLink Setup
            cmbJLinkSpeeds.ItemsSource = cmiuCMDs.JLink_Speeds_Get();
            cmbJLinkSpeeds.SelectedIndex = 4;
            //BindingOperations.EnableCollectionSynchronization(discoveredCMIUs, _lock);
            lstBxDiscoveredCMIUDevices.DataContext = discoveredCMIUs;
            
        }

        void configFile_UserInputParseErrorOccurred(object sender, ConfigurationFileErrorArgs e)
        {
            LogToConsole(e.Error);
        }

        void LogInitalMessage()
        {
            // Print the version of the file
            string version = "CMIT App Version: " + GetVersion();
            this.Title = version;
            LogToConsole(version);
            LogToConsole("Initializing Command Library...please wait...");
        }

        string GetVersion()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fileVersion = FileVersionInfo.GetVersionInfo(assembly.Location);
            return fileVersion.FileVersion;
        }
       
        #endregion


        #region Logging
        void LogToConsole(string text)
        {
            /* Need to call Invoke since method will be called from a background thread. */
            Dispatcher.BeginInvoke((Action)delegate()
            {
                string timestamp = DateTime.Now.ToString("HH:mm:ss.ff");
                text = String.Format("[{0}] {1}", timestamp, text);
                text += "\r";
                rtbConsole.AppendText(text);
                LogFile.AppendToFile(text);

                // Now, minimize console size to help prevent CMIT from crashing
                ReduceConsoleLog(rtbConsole, 90000);

                // Only scroll if enabled
                if(isScrollToBottomEnabled)
                {
                    rtbConsole.ScrollToEnd();
                }
                    
            });
        }

        void LogToCmdResponses(string text)
        {
            /* Need to call Invoke since method will be called from a background thread. */
            Dispatcher.BeginInvoke((Action)delegate()
            {
                string timestamp = DateTime.Now.ToString("HH:mm:ss.ff");
                text = String.Format("[{0}] {1}", timestamp, text);
                text += "\r";
                rtbCmdResp.AppendText(text);
                LogFile.AppendToRespLogFile(text);

                // Now, minimize console size to help prevent CMIT from crashing
                ReduceConsoleLog(rtbCmdResp, 90000);

                // Only scroll if enabled
                if (isScrollToBottomEnabled)
                {
                    rtbCmdResp.ScrollToEnd();
                }

            });
        }

        /**
         * This routine is used to prevent the console from getting too big 
         * (and causing a memory problem). It grabs all the text from the
         * rich text box (that is passed to it), then removes the first
         * line until the length is less than the passed max length.
         *
         * After the text has been properly reduced, the console text is
         * replaced with the newly reduced text.
         *
         * */
        void ReduceConsoleLog(RichTextBox rtb, int maxLength)
        {
            string new_text = new TextRange(rtb.Document.ContentStart, rtb.Document.ContentEnd).Text;
            bool textReduced = false;

            char[] anyOf = new char[1];
            anyOf[0] = '\r';

            if (new_text.Length > (maxLength*10))
            {
                while (new_text.Length > maxLength)
                {
                    int index = new_text.IndexOfAny(anyOf);
                    ++index; // We want to move forward one position
                    int remainder = new_text.Length - index;
                    new_text = new_text.Substring((index), (remainder));
                    rtb.LineUp(); // This do to keep up with big logs
                    textReduced = true;
                }

                if (true == textReduced)
                {
                    rtb.Document.Blocks.Clear();
                    rtb.AppendText(new_text);
                }
            }

        }
        #endregion



        #region Commands Setup

        void InitializeCommandLibrary()
        {
            cmiuCMDs = new CMIUCommands();

            cmiuCMDs.OnLogInfo += cmiuCMDs_OnLogInfo;
            cmiuCMDs.OnBTLE_LogMessage += cmiuCMDs_OnLogInfo;
            cmiuCMDs.OnSerial_LogMessage += cmiuCMDs_OnLogInfo;
            cmiuCMDs.OnBTLE_DongleInitialized += cmiuCMDs_DongleInitialized;
            cmiuCMDs.OnBTLE_DongleInitializationFailed += cmiuCMDs_DongleInitializationFailed;
            cmiuCMDs.OnBTLE_CMIUConnected += cmiuCMDs_CMIUConnected;
            cmiuCMDs.OnBTLE_CMIUDisconnected += cmiuCMDs_OnBTLE_CMIUDisconnected;
            cmiuCMDs.OnBTLE_CMIUsDiscovered += cmiuCMDs_CMIUsDiscovered;
            cmiuCMDs.OnErrorEvent += cmiuCMDs_OnErrorEvent;
            cmiuCMDs.OnBTLE_ResponsePacketReceived += cmiuCMDs_OnBTLE_ResponsePacketReceived;
            cmiuCMDs.OnSerial_ResponsePacketReceived += cmiuCMDs_OnSerial_ResponsePacketReceived;
            
            cmiuCMDs.CommandLibrary_Setup();
            LogToConsole("Command Library Initialized.");
        }

        


        void InitializerfUartController()
        {
            cmiuCMDs.BTLE_Dongle_Initialize();
        }

        #endregion

        #region Internal Helper Methods

        // Doing this because the tab control UI style is being stupid
        private void resetAllImageTabButtons()
        {
            SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(0xFF, 0xBC, 0xBC, 0xBC));
            btnImageApp.Background = brush;
            btnImageBLEConfig.Background = brush;
            btnImageBootloader.Background = brush;
            btnImageConfig.Background = brush;
            btnImageEncryption.Background = brush;
        }

        // Updates the image info listbox to the passed image
        private void UpdateImageFieldsListBox(ImageData image)
        {
            lstImageData.ItemsSource = null;

            // Make sure the file versions/images have been created
            if (image != null)
            {
                lstImageData.ItemsSource = image.versionData.VersionData;
                //fileVersions.CurrentlyVeiwedImage = image;
            }
        }

        private bool CanSendCommand()
        {
            if(isToggleBTLE && cmiuCMDs.BTLE_CMIU_IsConnected())
            {
                return true;
            }

            if(!isToggleBTLE && cmiuCMDs.Serial_SerialPort_IsOpen())
            {
                return true;
            }

            return false;
        }

        #endregion

        #region CMIU Command Library Events

        // Event called when dongle is initialized
        void cmiuCMDs_DongleInitialized(object sender, EventArgs e)
        {
            LogToConsole("RF Dongle Detected and Initialized");
            Dispatcher.BeginInvoke((Action)delegate()
            {
                txtBTDongleStatus.Text = DETECTED;
                btnBTConnect.Content = SCAN;
            });
        }

        // Event called when dongle initialization failed
        void cmiuCMDs_DongleInitializationFailed(object sender, EventArgs e)
        {
            LogToConsole("ERROR: Dongle failed to initialize. Please check the connection.");

            Dispatcher.BeginInvoke((Action)delegate()
            {
                txtBTDongleStatus.Text = NOTFOUND;
                btnBTConnect.Content = DETECT;
            });
        }

        // Event called when the CMIUs discovered is sent
        void cmiuCMDs_CMIUsDiscovered(object sender, CMIUDevicesBTLEInfoArgs e)
        {
            // Get the discovered CMIU and display it in a list 
            Task.Factory.StartNew(() =>
            {
                if (e.Devices.Count == 0)
                {
                    // There are no devices, clear out the device list
                    Dispatcher.BeginInvoke((Action)delegate()
                    {
                        lstBxDiscoveredCMIUDevices.ItemsSource = null;
                    });
                }
                else
                {
                    discoveredCMIUs = new ObservableCollection<CMIUDeviceBTLEInfo>(e.Devices);

                    // Update the selected index now that the itemsource has been updated
                    Dispatcher.BeginInvoke((Action)delegate()
                    {
                        lstBxDiscoveredCMIUDevices.ItemsSource = discoveredCMIUs;
                        lstBxDiscoveredCMIUDevices.SelectedIndex = selectedCMIUIndex;
                    });
                }
            });

            Dispatcher.BeginInvoke((Action)delegate()
            {
                if (!btnBTConnect.IsEnabled)
                    btnBTConnect.IsEnabled = true;
            });
        }

        // Event called when logging info is sent
        void cmiuCMDs_OnLogInfo(object sender, LogInfo e)
        {
            LogToConsole(e.Message);
        }

        // Event triggered when CMIU is connected to
        void cmiuCMDs_CMIUConnected(object sender, EventArgs e)
        {
            LogToConsole("Connected to CMIU!");
            Dispatcher.BeginInvoke((Action)delegate()
            {
                btnBTConnect.Content = DISCONNECT;
                txtBTDongleStatus.Text = CONNECTED;
            });
        }

        // Event triggered when disconnected from the CMIU
        void cmiuCMDs_OnBTLE_CMIUDisconnected(object sender, EventArgs e)
        {
            LogToConsole("Disconnected to CMIU!");
            Dispatcher.BeginInvoke((Action)delegate()
            {
                btnBTConnect.Content = SCAN;
                btnBTConnect.IsEnabled = true;
                txtBTDongleStatus.Text = DISCONNECTED;
                lstBxDiscoveredCMIUDevices.ItemsSource = null;                
            });
        }

        void cmiuCMDs_OnSerial_ResponsePacketReceived(object sender, PacketResponseInfo e)
        {
            string resp = ResponsePackets.Parse(e.Response);
            LogToCmdResponses(resp);
        }

        void cmiuCMDs_OnBTLE_ResponsePacketReceived(object sender, PacketResponseInfo e)
        {
            string resp = ResponsePackets.Parse(e.Response);
            LogToCmdResponses(resp);
        }


        void cmiuCMDs_OnErrorEvent(object sender, ErrorInfo e)
        {
            LogToConsole("ERROR: " + e.ErrorMessage + " ERROR CODE: " + e.ErrorCode.ToString());
        }

        #endregion


        #region GUI Methods
 

        private void lstBxDiscoveredCMIUDevices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = lstBxDiscoveredCMIUDevices.SelectedIndex;

            // When user makes selection, or if the selection is being set after an itemsource update, save it
            if (lstBxDiscoveredCMIUDevices.SelectedIndex >= 0)
            {
                selectedCMIUIndex = index;
                selectedCMIU = discoveredCMIUs[index];
            }

            // Whenever this get reset because the itemsource is updated, set it to what the user selected
            if (index == -1)
            {
                lstBxDiscoveredCMIUDevices.SelectedIndex = selectedCMIUIndex;
            }
        }

        private void cmbBoxSerialPorts_DropDownOpened(object sender, EventArgs e)
        {
            string[] ports = cmiuCMDs.Serial_SerialPortNames_Get();
            if (ports != null)
            {
                cmbBoxSerialPorts.ItemsSource = ports;
            }
        }

        private void cmbBoxSerialPorts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(cmbBoxSerialPorts.SelectedIndex >= 0)
            {
                string portName = cmbBoxSerialPorts.SelectedValue.ToString();
                if (cmiuCMDs.Serial_SerialPort_Open(portName))
                {
                    LogToConsole("Serial UART connection opened.");
                    btnUARTDisconnect.IsEnabled = true;
                }
                else
                {
                    LogToConsole("ERROR: Unable to open serial UART connection.");
                    btnUARTDisconnect.IsEnabled = false;
                }
            }
        }

        private void btnDisconnectSerial_Click(object sender, RoutedEventArgs e)
        {
            cmiuCMDs.Serial_SerialPort_Close();
            cmbBoxSerialPorts.SelectedIndex = -1;
        }

        private void btnJLinkProgram_Click(object sender, RoutedEventArgs e)
        {
            LogToConsole("Programming CMIU...");

            string filePath = txtJLinkFilePath.Text;

            // Check if the file path has been set
            if (!File.Exists(filePath))
            {
                // Error
                LogToConsole("ERROR - Please select a file first.");
                return;
            }

            // Program
            string speed = cmbJLinkSpeeds.SelectedValue.ToString();

            // Configure JLink settings
            cmiuCMDs.JLink_ConfigureSpeed(speed);

            if (cmiuCMDs.JLink_Device_ProgramWithFile(filePath))
            {
                LogToConsole("Success programming CMIU.");
            }
        }

        private void btnJLinkBrowse_Click(object sender, RoutedEventArgs e)
        {
            //Open file dialog
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Please select a Hex file";
            fdlg.Filter = "hex files (*.hex)|*.hex";
            fdlg.FilterIndex = 2;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == true)
            {
                string fileandpath = fdlg.FileName;
                txtJLinkFilePath.Text = fileandpath;                
            }
        }

        private void btnBTConnect_Click(object sender, RoutedEventArgs e)
        {
            if (btnBTConnect.Content.ToString() == DETECT)
            {
                LogToConsole("Detecting dongle...");
                txtBTDongleStatus.Text = DETECTING;
                btnBTConnect.Content = DETECTING;
                cmiuCMDs.BTLE_Dongle_Initialize();
            }
            else if (btnBTConnect.Content.ToString() == SCAN)
            {
                LogToConsole("Scanning for CMIUs..");
                txtBTDongleStatus.Text = SCANNING;
                btnBTConnect.Content = CONNECT;
                cmiuCMDs.BLTE_Dongle_ScanForCMIUs();
            }
            else if (btnBTConnect.Content.ToString() == CONNECT)
            {
                if (selectedCMIUIndex < 0)
                {
                    LogToConsole("ERROR: Please select a CMIU to connect to.");
                }
                else
                {
                    if (btnBTConnect.Content.ToString() == CONNECT)
                    {
                        LogToConsole("Connecting to CMIU: " + selectedCMIU.DeviceName);
                        btnBTConnect.Content = DISCONNECT;
                        txtBTDongleStatus.Text = CONNECTING;
                        cmiuCMDs.BTLE_CMIU_ConnectToCMIUID(selectedCMIU.DeviceName);
                    }                    
                }
            }
            else if (btnBTConnect.Content.ToString() == DISCONNECT)
            {
                LogToConsole("Disconnecting from CMIU");
                cmiuCMDs.BTLE_CMIU_Disconnect();
                btnBTConnect.Content = CONNECT;
                txtBTDongleStatus.Text = DETECTED;
            }
            
            
        }

        private void btnJLinkRead_Click(object sender, RoutedEventArgs e)
        {
            LogToConsole("Reading Device");

            // Program
            string speed = cmbJLinkSpeeds.SelectedValue.ToString();

            // Configure JLink settings
            cmiuCMDs.JLink_ConfigureSpeed(speed);

            // Read Code from Device
            string[] intelHexLines = cmiuCMDs.JLink_Device_ReadCode();
            LogToConsole("Device Read Successfully");

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "hex files (*.hex)|*.hex";
            saveFileDialog.Title = "Select Hex file name.";
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == true)
            {
                string filename = saveFileDialog.FileName;
                File.WriteAllLines(filename, intelHexLines);
                LogToConsole("File saved to " + filename);
            }
            else
            {
                LogToConsole("File not saved :(");
            }


        }

        private void btnConfigBrowse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Open the hex file
                string fileandpath;
                OpenFileDialog fdlg = new OpenFileDialog();
                fdlg.Title = "Please select a Hex file";
                fdlg.Filter = "hex files (*.hex)|*.hex";
                fdlg.FilterIndex = 2;
                fdlg.RestoreDirectory = true;
                if (fdlg.ShowDialog() == true)
                {
                    fileandpath = fdlg.FileName;
                    InputFileLocation = fileandpath;
                }
                else
                {
                    return;
                }

                // Get the Hex File
                string[] hexFileLines;
                hexFileLines = File.ReadAllLines(fileandpath);

                //Device cmiu = new Device(hexFileLines.ToList());
                cmiuDeviceMemory = new Device(hexFileLines.ToList());

                // Update the item source
                lstImageData.ItemsSource = null;
                //lstImageData.ItemsSource = fileVersions.ApplicationImage.VersionData;
                lstImageData.ItemsSource = cmiuDeviceMemory.ApplicationImage.versionData.VersionData;

                btnImageApp_Click(null, null);

                btnConfigProgram.IsEnabled = true;
                btnSaveFile.IsEnabled = true;
                btnSaveImage.IsEnabled = true;
            }
            catch (Exception ex)
            {
                LogToConsole(string.Format("Exception when attempting to read file: {0}", ex.Message));
            }
        }

        private void btnConfigRead_Click(object sender, RoutedEventArgs e)
        {
            LogToConsole("Reading Device...");

            // Program
            string speed = "4000";

            // Configure JLink settings
            cmiuCMDs.JLink_ConfigureSpeed(speed);

            // Read Code from Device
            string[] intelHexLines = cmiuCMDs.JLink_Device_ReadCode();

            // New device memory
            cmiuDeviceMemory = new Device(intelHexLines.ToList());

            // Update the item source
            lstImageData.ItemsSource = null;
            //lstImageData.ItemsSource = fileVersions.ApplicationImage.VersionData;
            lstImageData.ItemsSource = cmiuDeviceMemory.ApplicationImage.versionData.VersionData;

            btnImageApp_Click(null, null);

            btnSaveFile.IsEnabled = true;
            btnSaveImage.IsEnabled = true;

            LogToConsole("Device read and loaded.");
           
        }

        // Save the whole file with updated data
        private void btnSaveFile_Click(object sender, RoutedEventArgs e)
        {

            List<string> updatedHexFile = cmiuDeviceMemory.GetUpdatedHexFile();
            if(cmiuDeviceMemory.ErrorEncountered)
            {
                LogToConsole("ERROR: There is a problem with the input for the versions or settings. Please check your input.");
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "hex files (*.hex)|*.hex";
            saveFileDialog.Title = "Select Hex file name.";
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == true)
            {
                string filename = saveFileDialog.FileName;
                File.WriteAllLines(filename, updatedHexFile);
                LogToConsole("File saved to " + filename);
            }
            else
            {
                LogToConsole("File not saved :(");
            }
        }

        // Get just the current image
        private void btnSaveImage_Click(object sender, RoutedEventArgs e)
        {
            LogToConsole("Not supported yet.");            
        }


        // Methods for image tabs
        private void btnImageApp_Click(object sender, RoutedEventArgs e)
        {
            resetAllImageTabButtons();
            btnImageApp.Background = Brushes.White;
            if(cmiuDeviceMemory == null)
            {
                return;
            }
            UpdateImageFieldsListBox(cmiuDeviceMemory.ApplicationImage);
        }

        private void btnImageBootloader_Click(object sender, RoutedEventArgs e)
        {
            resetAllImageTabButtons();
            btnImageBootloader.Background = Brushes.White;
            if (cmiuDeviceMemory == null)
            {
                return;
            }
            UpdateImageFieldsListBox(cmiuDeviceMemory.BootloaderImage);
        }

        private void btnImageConfig_Click(object sender, RoutedEventArgs e)
        {
            resetAllImageTabButtons();
            btnImageConfig.Background = Brushes.White;
            if (cmiuDeviceMemory == null)
            {
                return;
            }
            UpdateImageFieldsListBox(cmiuDeviceMemory.ConfigurationImage);
        
        }

        private void btnImageBLEConfig_Click(object sender, RoutedEventArgs e)
        {
            resetAllImageTabButtons();
            btnImageBLEConfig.Background = Brushes.White;
            if (cmiuDeviceMemory == null)
            {
                return;
            }
            UpdateImageFieldsListBox(cmiuDeviceMemory.BLEConfigImage);
        }

        private void btnImageEncryption_Click(object sender, RoutedEventArgs e)
        {
            resetAllImageTabButtons();
            btnImageEncryption.Background = Brushes.White;
        }

        private void btnBTTest_Click(object sender, RoutedEventArgs e)
        {
            cmiuCMDs.SendCommand_Ping(CMIUCommands.CommandInterface.Bluetooth);
        }

        private void menuItemClearLog_Click(object sender, RoutedEventArgs e)
        {
            rtbConsole.Document.Blocks.Clear();
            rtbCmdResp.Document.Blocks.Clear();
        }

        private void menuItemScrollToggle_Checked(object sender, RoutedEventArgs e)
        {
            isScrollToBottomEnabled = false;
        }

        private void menuItemScrollToggle_Unchecked(object sender, RoutedEventArgs e)
        {
            isScrollToBottomEnabled = true;
        }

        private void menuItemSaveToLogFile_Checked(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "txt files (*.txt)|*.txt";
            saveFileDialog.Title = "Select log file name.";
            saveFileDialog.RestoreDirectory = false;

            if (saveFileDialog.ShowDialog() == true)
            {
                LogFile.EnableLogging(saveFileDialog.FileName);
            }
        }

        private void menuItemSaveToLogFile_Unchecked(object sender, RoutedEventArgs e)
        {
            LogFile.DisableLogging();
        }

        private void menuItemSaveRespToLogFile_Checked(object sender, RoutedEventArgs e)
        {
            if (LogFile.GetResponseLogPath() == string.Empty)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "txt files (*.txt)|*.txt";
                saveFileDialog.Title = "Select response log file name.";
                saveFileDialog.RestoreDirectory = false;

                if (saveFileDialog.ShowDialog() == true)
                {
                    LogFile.EnableRespLogging(saveFileDialog.FileName);
                }
            }
            else
            {
                LogFile.EnableRespLogging();
            }
        }

        private void menuItemSaveRespToLogFile_Unchecked(object sender, RoutedEventArgs e)
        {
            LogFile.DisableRespLogging();
        }

        private void menuItemCreateLogFile_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "txt files (*.txt)|*.txt";
            saveFileDialog.Title = "Select log file name.";
            saveFileDialog.RestoreDirectory = false;

            if (saveFileDialog.ShowDialog() == true)
            {

                var textRange = new TextRange(rtbConsole.Document.ContentStart, rtbConsole.Document.ContentEnd);
                string text = textRange.Text;
                LogFile.CreateLogFile(saveFileDialog.FileName, text);
            }
        }

        private void menuItemCreateLogFromResponse_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "txt files (*.txt)|*.txt";
            saveFileDialog.Title = "Select response log file name.";
            saveFileDialog.RestoreDirectory = false;

            if (saveFileDialog.ShowDialog() == true)
            {

                var textRange = new TextRange(rtbCmdResp.Document.ContentStart, rtbCmdResp.Document.ContentEnd);
                string text = textRange.Text;
                LogFile.CreateRespLogFile(saveFileDialog.FileName, text);
            }
        }

        private void menuItemExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }        

        private void btnTest_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnConfigProgram_Click(object sender, RoutedEventArgs e)
        {
            LogToConsole("Creating file...");

            try
            {
                List<string> file = cmiuDeviceMemory.GetUpdatedHexFile();
                if(cmiuDeviceMemory.ErrorEncountered)
                {
                    LogToConsole("ERROR: There is a problem with your configuration settings. Please check your input!");
                    return;
                }

                string templocation = System.IO.Path.GetTempPath();
                string tempfilename = templocation + "cmiu_temp.hex";
                if (File.Exists(tempfilename))
                {
                    File.Delete(tempfilename);
                }
                File.WriteAllLines(tempfilename, file);

                LogToConsole("File created, programming device...");
                if (!cmiuCMDs.JLink_Device_ProgramWithFile(tempfilename))
                {
                    LogToConsole("ERROR: Device failed to program.");
                }
                else
                {
                    LogToConsole("Device programmed successfully");
                }

                File.Delete(tempfilename);
            }
            catch (Exception ex)
            {
                LogToConsole(string.Format("Exception in when attempting to update device: {0}", ex.Message));
            }
        }

        #endregion

        


        private void cmbBoxBTLECommands_DropDownOpened(object sender, EventArgs e)
        {
            string[] commands = commandPackets.GetCommandNames();
            cmbBoxBTLECommands.ItemsSource = commands;
        }

        private void cmbBoxBTLECommands_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            commandPackets.UpdateParametersForCommand(cmbBoxBTLECommands.SelectedIndex);
            lstBTLECommands.ItemsSource = null;
            lstBTLECommands.ItemsSource = commandPackets.CommandPacketParameters;
        }

        private void btnBTSendCommand_Click(object sender, RoutedEventArgs e)
        {
            // Check if we can send a command
            //if(!CanSendCommand())
            //{
            //    LogToCmdResponses("ERROR: Please check your connection with the interface (BTLE/Serial) you are attempting to send the command on");
            //    return;
            //}

            if(cmbBoxBTLECommands.SelectedIndex < 0)
            {
                LogToCmdResponses("ERROR: Please select a command.");
                return;
            }

            // Send user selected command
            bool success = commandPackets.SendCommandPacket(ref this.cmiuCMDs, selectedCmdInterface, cmbBoxBTLECommands.SelectedIndex);
            if (!success)
            {
                LogToCmdResponses("ERROR: Error building command. Please check your input");
            }
            else
            {
                LogToCmdResponses("COMMAND SENT: " + commandPackets.ParsedCommand);
            }
        }

        private void btnCreateHK_Click(object sender, RoutedEventArgs e)
        {

            if (cmbBoxBTLECommands.SelectedIndex < 0)
            {
                LogToCmdResponses("ERROR: Please select a command.");
                return;
            }

            var hkWindow = new HotKeyWindow();
            hkWindow.ShowDialog();

            System.Windows.Input.Key key = hkWindow.hkKey;
            if(key == Key.None)
            {
                LogToCmdResponses("ERROR: Hotkey creation cancelled or the hotkey was invalid. Please only enter a single letter character.");
                return;
            }


            // Send user selected command
            bool success = commandPackets.CreateHotKey(ref cmiuCMDs, key, cmbBoxBTLECommands.SelectedIndex, selectedCmdInterface);
            if (!success)
            {
                LogToCmdResponses("ERROR: Error building Hotkey command. Please check your input");
            }
            else
            {
                LogToCmdResponses("Hotkey Created: " + commandPackets.ParsedCommand);
            }
        }

        private void toggleBar_MouseUp(object sender, MouseButtonEventArgs e)
        {
            TranslateTransform transform = new TranslateTransform();            
            transform.Y = 0;
           

            if(isToggleBTLE)
            {
                transform.X = -toggleBG.RenderSize.Width + toggleBar.RenderSize.Width;
                selectedCmdInterface = CMIUCommands.CommandInterface.UART;
            }
            else
            {
                transform.X = 0;
                selectedCmdInterface = CMIUCommands.CommandInterface.Bluetooth;
            }

            isToggleBTLE = !isToggleBTLE;
            toggleBar.RenderTransform = transform;
            
        }

        private void menuItemSaveCANDataToLogFile_Checked(object sender, RoutedEventArgs e)
        {
            if (LogFile.GetCANDataLogPath() == string.Empty)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "txt files (*.txt)|*.txt";
                saveFileDialog.Title = "Select log file name.";
                saveFileDialog.RestoreDirectory = false;

                if (saveFileDialog.ShowDialog() == true)
                {
                    LogFile.EnableCANLogging(saveFileDialog.FileName);
                }
            }
            else
            {
                LogFile.EnableCANLogging();
            }
        }

        private void menuItemSaveCANDataToLogFile_Unchecked(object sender, RoutedEventArgs e)
        {
            LogFile.DisableCANLogging();
        }

        private void MainWindowCMIT_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (commandPackets.IsHotKey(e.Key))
            {
                bool success = commandPackets.SendHotKeyCommand(e.Key, ref cmiuCMDs, selectedCmdInterface);

                if(success)
                {
                    LogToCmdResponses("Hotkey Command Sent: " + commandPackets.ParsedCommand);
                }
                else
                {
                    LogToCmdResponses("ERROR: Error building Hotkey command. Please check your input");
                }
            }
        }


        

    }
}
