﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntelHexFileHelper;

namespace CMITApp
{
    public class Device
    {
        // Private properties
        private List<byte> hexFileData = new List<byte>();

        // Public properties
        public bool ErrorEncountered = false;

        // CMIU has the following images
        public ImageData ApplicationImage;
        public ImageData BLEConfigImage;
        public ImageBootloader BootloaderImage;
        public ImageConfiguration ConfigurationImage;

        // Constructor
        public Device(List<string> inputFile)
        {
            // Process the input data
            List<IntelHex> hex = IntelHex.ParseToIntelHex(inputFile);
            if (!ValidateInputHexFile(hex))
            {
                ErrorEncountered = true;
                return;
            }
            LoadHexFileData(hex);

            // Load the images
            LoadApplication();
            LoadBLEConfig();
            LoadBootloader();
            LoadConfiguration();
        }

        private void LoadApplication()
        {
            ImageVersionData versionData = new ImageVersionData();
            versionData.ImageStartAddress = ImageConstants.APPLICATION_START_ADDRESS;
            versionData.ImageByteSize = ImageConstants.APPLICATION_BYTESIZE;
            versionData.VersionStartAddress = ImageConstants.APPLICATION_VERSIONSTART_ADDRESS;
            versionData.CRCStartAddress = ImageConstants.APPLICATION_CRC_STARTADDRESS;
            versionData.CRCDataSize = ImageConstants.APPLICATION_CRCDATA_BYTESIZE;
            ApplicationImage = new ImageData(versionData, hexFileData);
        }

        private void LoadBLEConfig()
        {
            ImageVersionData versionData = new ImageVersionData();
            versionData.ImageStartAddress = ImageConstants.BLECONFIG_START_ADDRESS;
            versionData.ImageByteSize = ImageConstants.BLECONFIG_BYTESIZE;
            versionData.VersionStartAddress = ImageConstants.BLECONFIG_VERSIONSTART_ADDRESS;
            versionData.CRCStartAddress = ImageConstants.BLECONFIG_CRC_STARTADDRESS;
            versionData.CRCDataSize = ImageConstants.BLECONFIG_CRCDATA_BYTESIZE;
            BLEConfigImage = new ImageData(versionData, hexFileData);
        }

        private void LoadBootloader()
        {
            ImageVersionData versionData = new ImageVersionData();
            versionData.ImageStartAddress = ImageConstants.BOOTLOADER_START_ADDRESS;
            versionData.VersionStartAddress = ImageConstants.BOOTLOADER_VERSIONSTART_ADDRESS;
            versionData.ImageByteSize = ImageConstants.BOOTLODER_BYTESIZE;
            versionData.CRCStartAddress = ImageConstants.BOOTLOADER_CRC_STARTADDRESS;
            versionData.CRCDataSize = ImageConstants.BOOTLOADER_CRCDATA_BYTESIZE;
            BootloaderImage = new ImageBootloader(versionData, hexFileData);
        }

        private void LoadConfiguration()
        {
            ImageVersionData versionData = new ImageVersionData();
            versionData.ImageStartAddress = ImageConstants.CONFIG_START_ADDRESS;
            versionData.VersionStartAddress = ImageConstants.CONFIGURATION_VERSIONSTART_ADDRESS;
            versionData.ImageByteSize = ImageConstants.CONFIGURATION_BYTESIZE;
            versionData.CRCStartAddress = ImageConstants.CONFIGURATION_CRC_STARTADDRESS;
            versionData.CRCDataSize = ImageConstants.CONFIGURATION_CRCDATA_BYTESIZE;
            ConfigurationImage = new ImageConfiguration(versionData, hexFileData);
        }

        public List<string> GetUpdatedHexFile()
        {
            // Get updates for each image for the raw byte data
            ApplicationImage.UpdateHexDataWithInput((int)ImageData.VersionIndex.Version_Major);
            BLEConfigImage.UpdateHexDataWithInput((int)ImageData.VersionIndex.Version_Major);
            BootloaderImage.UpdateHexDataWithInput((int)ImageData.VersionIndex.Version_Major);
            ConfigurationImage.UpdateHexDataWithInput((int)ImageData.VersionIndex.Version_Major);
            
            // Replace the byte data for each image

            // Get new application image
            List<byte> image = ApplicationImage.GetImageDataBytes();
            hexFileData.RemoveRange(ImageConstants.APPLICATION_START_ADDRESS, ImageConstants.APPLICATION_BYTESIZE);
            hexFileData.InsertRange(ImageConstants.APPLICATION_START_ADDRESS, image);

            // Get new ble configuration image
            image = BLEConfigImage.GetImageDataBytes();
            hexFileData.RemoveRange(ImageConstants.BLECONFIG_START_ADDRESS, ImageConstants.BLECONFIG_BYTESIZE);
            hexFileData.InsertRange(ImageConstants.BLECONFIG_START_ADDRESS, image);

            // Convert byte data to intel hex
            List<string> hexFile = IntelHex.CreateFile(0, hexFileData.Count, ref hexFileData, 0);
            return hexFile;
        }


        private void LoadHexFileData(List<IntelHex> intelHex)
        {
            // Clear HexFileData
            hexFileData.Clear();

            // Load the data
            foreach (IntelHex line in intelHex)
            {
                if (line.RecordType == 0x00)
                {
                    hexFileData.AddRange(line.Data);
                }                    
            }
        }

        private bool ValidateInputHexFile(List<IntelHex> file)
        {
            List<byte> data = new List<byte>();

            // Validate the checksum of each line
            foreach (IntelHex line in file)
            {
                if (line.LineChecksum != (IntelHexFileHelper.Checksum.CalculateChecksum(line)))
                {
                    return false;
                }

                if (line.RecordType == 0x00)
                    data.AddRange(line.Data);
            }

            // Validate the number of bytes
            if (data.Count != ImageConstants.FILE_BYTESIZE)
            {
                return false;
            }

            return true;
        }
    }
}
