﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMITApp
{
    class CRC
    {
        public static UInt32 CalculateCRC32(List<byte> dataReadyForCRC)
        {
            int index = 0;
            int NumberOfBytes = dataReadyForCRC.Count;
            UInt32 CRC = 0xFFFFFFFF;

            while (NumberOfBytes-- > 0)
            {
                CRC ^= Convert.ToUInt32(dataReadyForCRC[index++]);
                for (int count = 0; count < 8; count++)
                {
                    CRC = (CRC >> 1) ^ (((CRC & 0x0000001) > 0) ? 0xEDB88320 : 0x00000000);
                }
            }

            return CRC;
        }
    }
}
