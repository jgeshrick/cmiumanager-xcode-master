﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMITApp
{
    public class ImageBootloader :ImageData
    {
        
        // Constructor
        public ImageBootloader(ImageVersionData version, List<byte> inputData) : base(version, inputData)
        {
            // Add the ID
            VersionInfo idinfo = new VersionInfo();
            idinfo.Name = "CMIU ID";
            idinfo.Value = GetID();
            base.versionData.VersionData.Add(idinfo);
        }

        // Pulls the ID out of memory
        private string GetID()
        {
            //int addr = BOOTLOADER_ID_STARTADDRESS;
            List<byte> idBytes = base.hexFileData.GetRange(ImageConstants.BOOTLOADER_ID_STARTADDRESS, ImageConstants.BOOTLOADER_ID_BYTESIZE);

            string id = DataHelper.ConvertBinaryIDToString(base.hexFileData.GetRange(ImageConstants.BOOTLOADER_ID_STARTADDRESS, ImageConstants.BOOTLOADER_ID_BYTESIZE));
            if (id.Length == 10)
            {
                id = id.Remove(0, 1);
                return id;
            }
            else
            {
                // ERROR
                // TODO: Add error handling to base class
                return null;
            }
        }

        public override void UpdateHexDataWithInput(int versionIndex)
        {
            base.UpdateHexDataWithInput(versionIndex);
            UpdateImageDataIDWithUserInput();
            base.RecalculateCRC32(base.versionData);
        }

        private void UpdateImageDataIDWithUserInput()
        {
            string idString = base.versionData.VersionData[base.versionData.VersionData.Count-1].Value.TrimEnd();
            idString = DataHelper.RemoveAnyNewlines(idString);

            // Convert the ID to bytes
            int idNumber = Int32.Parse(idString);
            List<byte> idBytes = DataHelper.Convert32BitValueToBytes(idNumber);

            // Replace the binary ID
            hexFileData.RemoveRange(ImageConstants.BOOTLOADER_ID_STARTADDRESS, ImageConstants.BOOTLOADER_ID_BYTESIZE);
            hexFileData.InsertRange(ImageConstants.BOOTLOADER_ID_STARTADDRESS, idBytes);

            // Replace the ASCII IDs
            byte[] asciiIDBytes = Encoding.ASCII.GetBytes(idString);
            hexFileData.RemoveRange(ImageConstants.BOOTLOADER_ID_PUB_STARTADDRESS, ImageConstants.BOOTLOADER_ID_ASCII_BYTESIZE);
            hexFileData.InsertRange(ImageConstants.BOOTLOADER_ID_PUB_STARTADDRESS, asciiIDBytes);
            hexFileData.RemoveRange(ImageConstants.BOOTLOADER_ID_SUB_STARTADDRESS, ImageConstants.BOOTLOADER_ID_ASCII_BYTESIZE);
            hexFileData.InsertRange(ImageConstants.BOOTLOADER_ID_SUB_STARTADDRESS, asciiIDBytes);
            hexFileData.RemoveRange(ImageConstants.BOOTLOADER_ID_CLIENT_STARTADDRESS, ImageConstants.BOOTLOADER_ID_ASCII_BYTESIZE);
            hexFileData.InsertRange(ImageConstants.BOOTLOADER_ID_CLIENT_STARTADDRESS, asciiIDBytes);
        }
    }
}
