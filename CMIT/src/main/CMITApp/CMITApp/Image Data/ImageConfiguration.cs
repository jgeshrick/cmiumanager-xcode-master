﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMITApp
{
    public class ImageConfiguration : ImageData
    {
        

        // Settings Names
        public static string[] SettingsNames = { "Normal Mode - ARB Read Interval Secs", 
                                                    "Normal Mode - Detailed Config Packet Interval Secs", 
                                                    "Normal Mode - Interval Data Packet Interval Secs", 
                                                    "Normal Mode - Cell Call In Interval Secs", 
                                                    "Normal Mode - Cell Call In Offset Mins",
                                                    "Normal Mode - Cell Call In Window QtrHrs",
                                                    "Normal Mode - Datalog Intrerval Secs",
                                                    "Swipe Mode - ARB Read Interval", 
                                                    "Swipe Mode - State Swipe Timer", 
                                                    "Server Config - MQTT Server Port Number", 
                                                    "Server Config - MQTT Server Address",
                                                    "Server Config - HTTP Server Port Number", 
                                                    "Server Config - HTTP Server Address",
                                                    "BLE - Debug Output Bitmap", 
                                                    "BLE - Link Startup Active Duration", 
                                                    "BLE - Link Active Timeout",
                                                    "Hardware Version",
                                                    "Carrier Config - ATT Context ID",
                                                    "Carrier Config - VZW Context ID",
                                                    "Carrier Config - ATT APN",
                                                    "Carrier Config - VZW APN"};

        public enum SettingIndex
        {
            NormalMode_ARBReadInterval = 0,
            NormalMode_DetailedConfigPacketInterval = 1,
            NormalMode_IntervalDataPacketInterval,
            NormalMode_CellCallInIntervalSecs,
            NormalMode_CellCallInOffsetMins,
            NormalMode_CellCallInWindowQtrHrs,
            NormalMode_DatalogIntervalSecs,
            Swipe_ARBReadInterval,
            Swipe_StateTimer,
            Server_MQTTPortNumber,
            Server_MQTTAddress,
            Server_HTTPPortNumber,
            Server_HTTPAddress,
            BLE_DebugOutputBitmap,
            BLE_LinkStartupActiveDuration,
            BLE_LinkActiveTimeout,
            Hardware_Version,
            CarrierConfig_ATTContextID,
            CarrierConfig_VZWContextID,
            CarrierConfig_ATTAPN,
            CarrierConfig_VZWAPN,
            Version_Major,
            Version_Minor,
            Version_YearMonthDay,
            Version_Build
        }


        public ImageConfiguration(ImageVersionData version, List<byte> inputData) : base(version, inputData)
        {
            // Add all of the settings
            int index = 0;

            // ARB Read
            VersionInfo arb = new VersionInfo();
            arb.Name = SettingsNames[index];
            arb.Value = base.Get32BitNumberAtAddress(ImageConstants.NORMAL_ARBREADINTERVAL_ADDRESS);
            base.versionData.VersionData.Insert(index, arb);
            index++;

            // Detailed Config
            VersionInfo detailed = new VersionInfo();
            detailed.Name = SettingsNames[index];
            detailed.Value = base.Get32BitNumberAtAddress(ImageConstants.NORMAL_DETAILEDCONFIGPACKETINTERVAL_ADDRESS);
            base.versionData.VersionData.Insert(index, detailed);
            index++;

            // Interval Data Packet
            VersionInfo interval = new VersionInfo();
            interval.Name = SettingsNames[index];
            interval.Value = base.Get32BitNumberAtAddress(ImageConstants.NORMAL_INTDATAPACKETINTERVAL_ADDRESS);
            base.versionData.VersionData.Insert(index, interval);
            index++;

            // Cell Call In Interval
            VersionInfo cell = new VersionInfo();
            cell.Name = SettingsNames[index];
            cell.Value = base.Get32BitNumberAtAddress(ImageConstants.NORMAL_CELLCALLININTERVAL_ADDRESS);
            base.versionData.VersionData.Insert(index, cell);
            index++;

            // Cell Call In Offset
            VersionInfo offset = new VersionInfo();
            offset.Name = SettingsNames[index];
            offset.Value = base.Get16BitNumberAtAddress(ImageConstants.NORMAL_CELLCALLINOFFSET_ADDRESS);
            base.versionData.VersionData.Insert(index, offset);
            index++;

            // Cell Call In Window
            VersionInfo window = new VersionInfo();
            window.Name = SettingsNames[index];
            window.Value = base.Get16BitNumberAtAddress(ImageConstants.NORMAL_CELLCALLINWINDOW_ADDRESS);
            base.versionData.VersionData.Insert(index, window);
            index++;

            // Datalog Interval
            VersionInfo dl = new VersionInfo();
            dl.Name = SettingsNames[index];
            dl.Value = base.Get32BitNumberAtAddress(ImageConstants.NORMAL_DATALOGINTERVAL_ADDRESS);
            base.versionData.VersionData.Insert(index, dl);
            index++;

            // Swipe ARB
            VersionInfo swipearb = new VersionInfo();
            swipearb.Name = SettingsNames[index];
            swipearb.Value = base.Get32BitNumberAtAddress(ImageConstants.SWIPE_ARBREADINTERVAL_ADDRESS);
            base.versionData.VersionData.Insert(index, swipearb);
            index++;

            // Swipe State
            VersionInfo swipestate = new VersionInfo();
            swipestate.Name = SettingsNames[index];
            swipestate.Value = base.Get32BitNumberAtAddress(ImageConstants.SWIPE_STATETIMER_ADDRESS);
            base.versionData.VersionData.Insert(index, swipestate);
            index++;

            // MQTT Port
            VersionInfo mqttport = new VersionInfo();
            mqttport.Name = SettingsNames[index];
            mqttport.Value = base.Get32BitNumberAtAddress(ImageConstants.SERVER_MQTTPORTNUMBER_ADDRESS);
            base.versionData.VersionData.Insert(index, mqttport);
            index++;

            // MQTT Address
            VersionInfo mqttaddr = new VersionInfo();
            mqttaddr.Name = SettingsNames[index];
            mqttaddr.Value = base.GetIPAddressAtAddress(ImageConstants.SERVER_MQTTSERVERADDRESS_ADDRESS, ImageConstants.SERVER_MQTTSERVERADDRESS_BYTESIZE);
            base.versionData.VersionData.Insert(index, mqttaddr);
            index++;

            // HTTP Port
            VersionInfo htttport = new VersionInfo();
            htttport.Name = SettingsNames[index];
            htttport.Value = base.Get32BitNumberAtAddress(ImageConstants.SERVER_HTTPPORTNUMBER_ADDRESS);
            base.versionData.VersionData.Insert(index, htttport);
            index++;

            // HTTP Address
            VersionInfo httpaddr = new VersionInfo();
            httpaddr.Name = SettingsNames[index];
            httpaddr.Value = base.GetIPAddressAtAddress(ImageConstants.SERVER_HTTPSERVERADDRESS_ADDRESS, ImageConstants.SERVER_HTTPSERVERADDRESS_BYTESIZE);
            base.versionData.VersionData.Insert(index, httpaddr);
            index++;

            // BLE Debug
            VersionInfo bledebug = new VersionInfo();
            bledebug.Name = SettingsNames[index];
            bledebug.Value = base.Get16BitNumberAtAddress(ImageConstants.BLE_DEBUGOUTPUTBITMAP_ADDRESS);
            base.versionData.VersionData.Insert(index, bledebug);
            index++;

            // BLE Link Startup
            VersionInfo blelink = new VersionInfo();
            blelink.Name = SettingsNames[index];
            blelink.Value = base.Get16BitNumberAtAddress(ImageConstants.BLE_LINKSTARTUPACTIVEDURATION_ADDRESS);
            base.versionData.VersionData.Insert(index, blelink);
            index++;

            // BLE Active Timeout
            VersionInfo bletimeout = new VersionInfo();
            bletimeout.Name = SettingsNames[index];
            bletimeout.Value = base.Get16BitNumberAtAddress(ImageConstants.BLE_LINKACTIVETIMEOUT_ADDRESS);
            base.versionData.VersionData.Insert(index, bletimeout);
            index++;

            // HW Version
            VersionInfo hw = new VersionInfo();
            hw.Name = SettingsNames[index];
            hw.Value = base.Get16BitNumberAtAddress(ImageConstants.HARDWAREVERSION_ADDRESS);
            base.versionData.VersionData.Insert(index, hw);
            index++;

            // Carrier Config ATT Context ID
            VersionInfo attcontext = new VersionInfo();
            attcontext.Name = SettingsNames[index];
            attcontext.Value = base.Get16BitNumberAtAddress(ImageConstants.CARRIERCONFIG_ATTCONTEXTID_ADDRESS);
            base.versionData.VersionData.Insert(index, attcontext);
            index++;

            // Carrier Config VZW Context ID
            VersionInfo vzwcontext = new VersionInfo();
            vzwcontext.Name = SettingsNames[index];
            vzwcontext.Value = base.Get16BitNumberAtAddress(ImageConstants.CARRIERCONFIG_VZWCONTEXTID_ADDRESS);
            base.versionData.VersionData.Insert(index, vzwcontext);
            index++;

            // Carrier Config ATT APN
            VersionInfo attapn = new VersionInfo();
            attapn.Name = SettingsNames[index];
            attapn.Value = base.GetIPAddressAtAddress(ImageConstants.CARRIERCONFIG_ATTAPN_ADDRESS, ImageConstants.CARRIERCONFIG_ATTAPN_BYTESIZE);
            base.versionData.VersionData.Insert(index, attapn);
            index++;

            // Carrier Config VZW APN
            VersionInfo vzwapn = new VersionInfo();
            vzwapn.Name = SettingsNames[index];
            vzwapn.Value = base.GetIPAddressAtAddress(ImageConstants.CARRIERCONFIG_VZWAPN_ADDRESS, ImageConstants.CARRIERCONFIG_VZWAPN_BYTESIZE);
            base.versionData.VersionData.Insert(index, vzwapn);
            index++;
        }

        public override void UpdateHexDataWithInput(int versionIndex)
        {
            base.UpdateHexDataWithInput((int)SettingIndex.Version_Major);
            UpdateConfigHexDataWithInput();
            base.RecalculateCRC32(base.versionData);
        }

        private bool UpdateConfigHexDataWithInput()
        {
            string setting = base.versionData.VersionData[(int)SettingIndex.NormalMode_ARBReadInterval].Value;
            if (!Update32BitSettingInHexData(setting, ImageConstants.NORMAL_ARBREADINTERVAL_ADDRESS, ImageConstants.NORMAL_ARBREADINTERVAL_BYTESIZE))
            {
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.NormalMode_DetailedConfigPacketInterval].Value;
            if (!Update32BitSettingInHexData(setting, ImageConstants.NORMAL_DETAILEDCONFIGPACKETINTERVAL_ADDRESS, ImageConstants.SWIPE_ARBREADINTERVAL_BYTESIZE))
            {
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.NormalMode_IntervalDataPacketInterval].Value;
            if (!Update32BitSettingInHexData(setting, ImageConstants.NORMAL_INTDATAPACKETINTERVAL_ADDRESS, ImageConstants.NORMAL_INTDATAPACKETINTERVAL_BYTESIZE))
            {
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.NormalMode_CellCallInIntervalSecs].Value;
            if (!Update32BitSettingInHexData(setting, ImageConstants.NORMAL_CELLCALLININTERVAL_ADDRESS, ImageConstants.NORMAL_CELLCALLINININTERVAL_BYTESIZE))
            {
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.NormalMode_CellCallInOffsetMins].Value;
            if (!Update16BitSettingInHexData(setting, ImageConstants.NORMAL_CELLCALLINOFFSET_ADDRESS, ImageConstants.NORMAL_CELLCALLINOFFSET_BYTESIZE))
            {
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.NormalMode_CellCallInWindowQtrHrs].Value;
            if (!Update16BitSettingInHexData(setting, ImageConstants.NORMAL_CELLCALLINWINDOW_ADDRESS, ImageConstants.NORMAL_CELLCALLINWINDOW_BYTESIZE))
            {
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.NormalMode_DatalogIntervalSecs].Value;
            if (!Update32BitSettingInHexData(setting, ImageConstants.NORMAL_DATALOGINTERVAL_ADDRESS, ImageConstants.NORMAL_DATALOGINTERVAL_BYTESIZE))
            {
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.Swipe_ARBReadInterval].Value;
            if (!Update32BitSettingInHexData(setting, ImageConstants.SWIPE_ARBREADINTERVAL_ADDRESS, ImageConstants.SWIPE_ARBREADINTERVAL_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.Swipe_StateTimer].Value;
            if (!Update32BitSettingInHexData(setting, ImageConstants.SWIPE_STATETIMER_ADDRESS, ImageConstants.SWIPE_STATETIMER_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.Server_MQTTPortNumber].Value;
            if (!Update32BitSettingInHexData(setting, ImageConstants.SERVER_MQTTPORTNUMBER_ADDRESS, ImageConstants.SERVER_MQTTPORTNUMBER_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.Server_MQTTAddress].Value;
            if (!UpdateIPAddressInHexData(setting, ImageConstants.SERVER_MQTTSERVERADDRESS_ADDRESS, ImageConstants.SERVER_MQTTSERVERADDRESS_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.Server_HTTPPortNumber].Value;
            if (!Update32BitSettingInHexData(setting, ImageConstants.SERVER_HTTPPORTNUMBER_ADDRESS, ImageConstants.SERVER_HTTPPORTNUMBER_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.Server_HTTPAddress].Value;
            if (!UpdateIPAddressInHexData(setting, ImageConstants.SERVER_HTTPSERVERADDRESS_ADDRESS, ImageConstants.SERVER_HTTPSERVERADDRESS_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.Hardware_Version].Value;
            if (!Update16BitSettingInHexData(setting, ImageConstants.HARDWAREVERSION_ADDRESS, ImageConstants.HARDWAREVERSION_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.BLE_DebugOutputBitmap].Value;
            if (!Update16BitSettingInHexData(setting, ImageConstants.BLE_DEBUGOUTPUTBITMAP_ADDRESS, ImageConstants.BLE_DEBUGOUTPUTBITMAP_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.BLE_LinkStartupActiveDuration].Value;
            if (!Update16BitSettingInHexData(setting, ImageConstants.BLE_LINKSTARTUPACTIVEDURATION_ADDRESS, ImageConstants.BLE_LINKSTARTUPACTIVEDURATION_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.BLE_LinkActiveTimeout].Value;
            if (!Update16BitSettingInHexData(setting, ImageConstants.BLE_LINKACTIVETIMEOUT_ADDRESS, ImageConstants.BLE_LINKACTIVETIMEOUT_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.CarrierConfig_ATTContextID].Value;
            if (!Update16BitSettingInHexData(setting, ImageConstants.CARRIERCONFIG_ATTCONTEXTID_ADDRESS, ImageConstants.CARRIERCONFIG_ATTCONTEXTID_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.CarrierConfig_VZWContextID].Value;
            if (!Update16BitSettingInHexData(setting, ImageConstants.CARRIERCONFIG_VZWCONTEXTID_ADDRESS, ImageConstants.CARRIERCONFIG_VZWCONTEXTID_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.CarrierConfig_ATTAPN].Value;
            if (!UpdateIPAddressInHexData(setting, ImageConstants.CARRIERCONFIG_ATTAPN_ADDRESS, ImageConstants.CARRIERCONFIG_ATTAPN_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            setting = base.versionData.VersionData[(int)SettingIndex.CarrierConfig_VZWAPN].Value;
            if (!UpdateIPAddressInHexData(setting, ImageConstants.CARRIERCONFIG_VZWAPN_ADDRESS, ImageConstants.CARRIERCONFIG_VZWAPN_BYTESIZE))
            {
                // ERROR!!!!!!!!!!
                return false;
            }

            return true;
        }
    }
}
