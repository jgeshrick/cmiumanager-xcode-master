﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMITApp
{
    public class ImageVersionData
    {
        public ObservableCollection<VersionInfo> VersionData { get; set; }
        public int VersionStartAddress { get; set; }
        public int ImageStartAddress { get; set; }
        public int ImageByteSize { get; set; }
        public int CRCStartAddress { get; set; }
        public int CRCDataSize { get; set; }

        public ImageVersionData()
        {
            VersionData = new ObservableCollection<VersionInfo>();
        }
    }
}
