﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMITApp
{
    public class ImageData
    {
        protected List<byte> hexFileData;
        public ImageVersionData versionData;
        private string[] VersionNames = { "Version - Major", "Version - Minor", "Version - Year Day Month", "Version - Build" };
        public enum VersionIndex
        {
            Version_Major = 0,
            Version_Minor = 1,
            Version_YearMonthDay = 2,
            Version_Build = 3
        }  


        // Constructor
        public ImageData(ImageVersionData version, List<byte> inputData)
        {
            hexFileData = new List<byte>();
            hexFileData = inputData;
            versionData = new ImageVersionData();
            versionData = version;
            GetVersionDataForImage();
        }

        // TODO: Add error checking
        public virtual void UpdateHexDataWithInput(int versionIndex)
        {
            int versionStartAddress = versionData.VersionStartAddress + ImageConstants.VERSION_DONT_CARE_BYTESIZE;
            string value = versionData.VersionData[versionIndex].Value;
            int number = Int32.Parse(value);
            List<byte> bytes = DataHelper.Convert8BitToBcdByte(number).ToList();
            hexFileData.RemoveRange(versionStartAddress, ImageConstants.VERSION_MAJOR_BYTESIZE);
            hexFileData.InsertRange(versionStartAddress, bytes);
            versionStartAddress += ImageConstants.VERSION_MAJOR_BYTESIZE;
            versionIndex++;

            value = versionData.VersionData[versionIndex].Value;
            number = Int32.Parse(value);
            bytes = DataHelper.Convert8BitToBcdByte(number).ToList();
            hexFileData.RemoveRange(versionStartAddress, ImageConstants.VERSION_MINOR_BYTESIZE);
            hexFileData.InsertRange(versionStartAddress, bytes);
            versionStartAddress += ImageConstants.VERSION_MINOR_BYTESIZE;
            versionIndex++;

            value = versionData.VersionData[versionIndex].Value;
            number = Int32.Parse(value);
            bytes = DataHelper.Convert32BitToBcdBytes(number).ToList();
            hexFileData.RemoveRange(versionStartAddress, ImageConstants.VERSION_YEARMONTHDAY_BYTESIZE);
            hexFileData.InsertRange(versionStartAddress, bytes);
            versionStartAddress += ImageConstants.VERSION_YEARMONTHDAY_BYTESIZE;
            versionIndex++;

            value = versionData.VersionData[versionIndex].Value;
            number = Int32.Parse(value);
            bytes = DataHelper.Convert32BitToBcdBytes(number).ToList();
            hexFileData.RemoveRange(versionStartAddress, ImageConstants.VERSION_BUILD_BYTESIZE);
            hexFileData.InsertRange(versionStartAddress, bytes);

            RecalculateCRC32(this.versionData);
        }

        public List<byte> GetImageDataBytes()
        {
            return hexFileData.GetRange(versionData.ImageStartAddress, versionData.ImageByteSize);
        }


        private void GetVersionDataForImage()
        {
            int startAddress = versionData.VersionStartAddress;
            for (int index = 0; index < VersionNames.Count(); index++)
            {
                VersionInfo version = new VersionInfo();
                version.Name = VersionNames[index];
                versionData.VersionData.Add(version);
            }

            versionData.VersionData[(int)VersionIndex.Version_Major].Value = GetMajorVersion(startAddress);
            versionData.VersionData[(int)VersionIndex.Version_Minor].Value = GetMinorVersion(startAddress);
            versionData.VersionData[(int)VersionIndex.Version_YearMonthDay].Value = GetYearMonthDayVersion(startAddress);
            versionData.VersionData[(int)VersionIndex.Version_Build].Value = GetBuildVersion(startAddress);
        }

        // Version Helpers

        protected string GetMajorVersion(int versionStartAddr)
        {
            return DataHelper.ConvertBytesToBCDString(hexFileData.GetRange(versionStartAddr + ImageConstants.VERSION_DONTCARE_BYTESIZE, ImageConstants.VERSION_MAJOR_BYTESIZE));
        }

        protected string GetMinorVersion(int versionStartAddr)
        {

            int addr = versionStartAddr + ImageConstants.VERSION_MAJOR_BYTESIZE + ImageConstants.VERSION_DONTCARE_BYTESIZE;
            return DataHelper.ConvertBytesToBCDString(hexFileData.GetRange(addr, ImageConstants.VERSION_MINOR_BYTESIZE));
        }

        protected string GetYearMonthDayVersion(int versionStartAddr)
        {
            int addr = versionStartAddr + ImageConstants.VERSION_MAJOR_BYTESIZE + ImageConstants.VERSION_MINOR_BYTESIZE + ImageConstants.VERSION_DONTCARE_BYTESIZE;
            return DataHelper.ConvertBytesToBCDString(hexFileData.GetRange(addr, ImageConstants.VERSION_YEARMONTHDAY_BYTESIZE));
        }

        protected string GetBuildVersion(int versionStartAddr)
        {
            int addr = versionStartAddr + ImageConstants.VERSION_MAJOR_BYTESIZE + ImageConstants.VERSION_MINOR_BYTESIZE + ImageConstants.VERSION_YEARMONTHDAY_BYTESIZE + ImageConstants.VERSION_DONTCARE_BYTESIZE;
            return DataHelper.ConvertBytesToBCDString(hexFileData.GetRange(addr, ImageConstants.VERSION_BUILD_BYTESIZE));
        }

        // Settings Get Methods
        protected string Get32BitNumberAtAddress(int address)
        {
            int numBytes = 4;
            int interval = DataHelper.ConvertBytesTo32Bit(hexFileData.GetRange(address, numBytes));
            return interval.ToString();
        }

        protected string Get16BitNumberAtAddress(int address)
        {
            int numBytes = 2;
            int interval = DataHelper.ConvertBytesTo16Bit(hexFileData.GetRange(address, numBytes));
            return interval.ToString();
        }

        protected string Get8BitNumberAtAddress(int address)
        {
            int interval = hexFileData[address];
            return interval.ToString();
        }

        protected string GetIPAddressAtAddress(int address, int length)
        {
            string ip = System.Text.Encoding.ASCII.GetString(hexFileData.GetRange(address, length).ToArray());
            ip = ip.TrimEnd(new char[] { '\0' });
            return ip;
        }

        protected string GetBCDValueAtAddressForByteSize(int address, int byteSize)
        {
            return DataHelper.ConvertBytesToBCDString(hexFileData.GetRange(address, byteSize));
        }

        // Helper Methods

        // Update a 32bit setting in Hex Data, checks to see if the value passed can be converted properly
        protected bool Update32BitSettingInHexData(string setting, int address, int byteSize)
        {
            int number = -1;
            bool success = Int32.TryParse(setting, out number);
            if (success)
            {
                List<byte> bytes = DataHelper.Convert32BitValueToBytes(number);
                hexFileData.RemoveRange(address, byteSize);
                hexFileData.InsertRange(address, bytes);
            }

            return success;
        }

        // Update a 16bit setting in Hex Data, checks to see if the value passed can be converted properly
        protected bool Update16BitSettingInHexData(string setting, int address, int byteSize)
        {
            Int16 number = -1;
            bool success = Int16.TryParse(setting, out number);
            if (success)
            {
                List<byte> bytes = DataHelper.Convert16BitValueToBytes(number);
                hexFileData.RemoveRange(address, byteSize);
                hexFileData.InsertRange(address, bytes);
            }

            return success;
        }

        // Update byte setting in Hex Data, checks to see if the value passed can be converted properly
        protected bool Update8BitSettingInHexData(string setting, int address, int byteSize)
        {
            Byte number = 0x00;
            bool success = Byte.TryParse(setting, out number);
            if (success)
            {
                hexFileData[address] = number;
            }

            return success;
        }

        // Update the IP address
        // TODO: Add check to see if its an actual IP address
        protected bool UpdateIPAddressInHexData(string setting, int address, int byteSize)
        {
            // Get the ip as bytes
            byte[] settingsBytes = Encoding.ASCII.GetBytes(setting);

            // Prefill an array
            byte[] byteArray = Enumerable.Repeat((byte)'\0', byteSize - settingsBytes.Count()).ToArray();

            List<byte> newIP = new List<byte>();
            newIP.AddRange(settingsBytes);
            newIP.AddRange(byteArray);

            hexFileData.RemoveRange(address, newIP.Count());
            hexFileData.InsertRange(address, newIP);
            return true;
        }

        // Recalculates the CRC32 for the config and places it in the data list
        protected void RecalculateCRC32(ImageVersionData image)
        {
            List<byte> imgBytes = hexFileData.GetRange(image.ImageStartAddress, image.CRCDataSize);
            UInt32 crc = CRC.CalculateCRC32(imgBytes);
            byte[] bytes = BitConverter.GetBytes(crc);
            Array.Reverse(bytes, 0, bytes.Length);
            hexFileData.RemoveRange(image.CRCStartAddress, ImageConstants.CRC_BYTESIZE);
            hexFileData.InsertRange(image.CRCStartAddress, bytes);
        }
    }
}
