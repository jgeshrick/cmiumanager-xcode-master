﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMITApp
{
    public static class TagHelper
    {
        // This function recusively prints out the object contents using reflection
        public static string GetAllTagContents(object obj, ref string parsed, int indentLevel)
        {
            var fields = obj.GetType().GetFields();
            foreach (var name in fields)
            {
                // If null just print its null
                if (name.GetValue(obj) == null)
                {
                    parsed += name.Name + ": null \r";
                }
                else
                {
                    Type valueType = name.GetValue(obj).GetType();
                    // There is something to print
                    if (valueType.BaseType == typeof(CSTagTypes.Tags.Tag))
                    {
                        // The thing to print is an object. 
                        // Recursively call this with an indent to get the member of this object
                        // Then reset the indent level
                        parsed += name.Name + " -> \r";
                        indentLevel++;
                        GetAllTagContents(name.GetValue(obj), ref parsed, indentLevel);
                        indentLevel--;
                    }
                    else if (valueType.IsArray)
                    {
                        var expectedType = typeof(UInt32);
                        if (expectedType.IsAssignableFrom(valueType.GetElementType()))
                        {
                            string tab = "";
                            UInt32[] array = (UInt32[])name.GetValue(obj);
                            for (int count = 0; count < indentLevel; count++)
                            {
                                tab += "    ";
                            }
                            for (int count = 0; count < array.Length; count++)
                            {
                                parsed += tab + name.Name + "[" + count + "]" + ": " + array[count] + "\r";
                            }
                        }
                    }
                    else
                    {
                        // Print the field and add an indent at the indent level
                        string tab = "";
                        for (int count = 0; count < indentLevel; count++)
                        {
                            tab += "    ";
                        }
                        parsed += tab + name.Name + ": " + name.GetValue(obj) + "\r";
                    }

                }

            }

            return parsed;
        }
    }
}
