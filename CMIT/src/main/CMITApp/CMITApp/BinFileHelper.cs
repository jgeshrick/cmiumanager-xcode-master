﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace CMITApp
{
    class BinFileHelper
    {
        /// <summary>
        /// Write out a portion of the combined data list as a stand-alone, addressless binary file
        /// </summary>
        /// <param name="path">path to which to write the file (overwrites existing file)</param>
        /// <param name="start">Address on target (not index into TargetMemory)</param>
        /// <param name="length">length of block to write out</param>
        public static void WriteBinaryFile(string path, List<byte> data)
        {
            FileStream binfile = File.Open(path, FileMode.OpenOrCreate, FileAccess.Write);
            foreach (byte by in data)
            {
                binfile.WriteByte(by);
            }
            binfile.Close();
        }
    }
}
