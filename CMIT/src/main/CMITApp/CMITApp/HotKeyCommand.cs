﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTagTypes;
using CMIUCommandLibrary;

namespace CMITApp
{
    class HotKeyCommand
    {
        // Letter
        public System.Windows.Input.Key hotKey;

        // Name
        public string Name;

        // Parameters
        public CSTagTypes.CommandData.CommandPacketParameters packetParams;
        public CMIUCommandLibrary.CMIUCommands.CommandInterface cmdInterface;
    }
}
