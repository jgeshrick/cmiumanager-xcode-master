﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTagTypes;

namespace CMIUCommandLibrary
{
    /// <summary>
    /// Provides data for the Logging event.
    /// </summary>
    public class LogInfo : EventArgs
    {
        /// <summary>
        /// The string message to log
        /// </summary>
        public string Message { get; set; }

        public LogInfo(string message)
        {
            Message = message;
        }
    }

    /// <summary>
    /// Provides data for recevied payload data
    /// </summary>
    public class MessagePayloadData : EventArgs
    {
        /// <summary>
        /// The raw byte payload data of a message
        /// </summary>
        public byte[] Payload { get; set; }

        public MessagePayloadData(byte[] payload)
        {
            Payload = payload;
        }
    }

    /// <summary>
    /// Class data for CMIU Bluetooth Device Info
    /// </summary>
    public class CMIUDeviceBTLEInfo
    {
        /// <summary>
        /// Hex device address
        /// </summary>
        public string DeviceAddress { get; set; }
        /// <summary>
        /// CMIU ID, e.g. NC999999999
        /// </summary>
        public string DeviceName { get; set; }
        /// <summary>
        /// RSSI level as a string
        /// </summary>
        public string RSSI { get; set; }
        /// <summary>
        /// UUID of the device
        /// </summary>
        public string UUID { get; set; }
        /// <summary>
        /// Date time the device was last heard from
        /// </summary>
        public string LastHeard { get; set; }
    }

    /// <summary>
    /// Event arguments returning all of the devices that have been discovered.
    /// This is a list of CMIUDeviceBTLEInfo objects
    /// </summary>
    public class CMIUDevicesBTLEInfoArgs : EventArgs
    {
        /// <summary>
        /// List of CMIUDeviceBTLEInfo devices that are connectable
        /// </summary>
        public List<CMIUDeviceBTLEInfo> Devices { get; set; }

        public CMIUDevicesBTLEInfoArgs(List<CMIUDeviceBTLEInfo> devices)
        {
            Devices = devices;
        }
    }

    /// <summary>
    /// This event argument contains the response packet type. This event is raised when a message is received and successfully parsed.
    /// </summary>
    public class PacketResponseInfo : EventArgs
    {
        /// <summary>
        /// The response packet.  It is a parent class, so the packet type should be checked 
        /// to see what the actual response packet is.
        /// </summary>
        public CSTagTypes.Packets.ResponsePacket Response { get; set; }

        public PacketResponseInfo(CSTagTypes.Packets.ResponsePacket response)
        {
            Response = response;
        }
    }

    /// <summary>
    /// Provides data for an error event.
    /// </summary>
    public class ErrorInfo : EventArgs
    {
        /// <summary>
        /// The string message to for the error
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// The integer Error Code enum
        /// </summary>
        public int ErrorCode { get; set; }

        public ErrorInfo(int code, string message)
        {
            ErrorCode = code;
            ErrorMessage = message;
        }
    }
}
