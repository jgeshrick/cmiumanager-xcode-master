﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PacketHandlerLibrary;
using CSTagTypes;

namespace CMIUCommandLibrary
{
    public static class CommandPackets
    {
        public static byte[] PacketBuild_Command_FirmwareUpdate(string filePath)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket builder = new BuildPacket();
            uint packetLength = builder.MakeFirmwareImagePacket(filePath, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_PublishRequestedPacket(CSTagTypes.CommandData.CommandParameters_PublishRequestedPacket publishPacket)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_PublishRequestedPacket(publishPacket, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_ReadConnectedDevices(CSTagTypes.CommandData.CommandParameters_ReadConnectedDevices readData)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_ReadConnectedDevices(readData, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_ReadMemory(CSTagTypes.CommandData.CommandParameters_ReadMemory memory)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_ReadMemory(memory, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_LTERFTestMode(CSTagTypes.CommandData.CommandParameters_LTECarrierAssert lteRFTest)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_LTE_RFTestMode(lteRFTest, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_BTLERFTestMode(CSTagTypes.CommandData.CommandParameters_BTLECarrierAssert btleRFTest)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_BTLE_RFTestMode(btleRFTest, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_SetPowerMode(CSTagTypes.CommandData.CommandParameters_SetPowerMode powerMode)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_SetPowerMode(powerMode, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_ToggleModem(CSTagTypes.CommandData.CommandParameters_ToggleLTEModem modem)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_ToggleModem(modem, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_EraseMemoryImage(CSTagTypes.CommandData.CommandParameters_EraseMemoryImage erase)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_EraseMemoryImage(erase, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_SetRecordingReportingIntervals(CSTagTypes.CommandData.CommandParameters_SetRecordingReportingIntervals intervals)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_SetRecordingReportingIntervals(intervals, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_SleepSeconds(CSTagTypes.CommandData.CommandParameters_SleepSeconds sleep)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_SleepSeconds(sleep, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_GetCANData()
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_GetCANData(packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_GetDebugLog()
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_GetDebugLog(packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_GetCMIUSignalQuality()
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_GetCMIUSignalQuality(packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_SetModemFOTA(CSTagTypes.CommandData.CommandParameters_ModemFOTA fota)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_ModemFOTA(fota, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_APN_Get()
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_CMIUAPN_Get(packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_APN_Set(CSTagTypes.CommandData.CommandParameters_APN_Set apn)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_CMIUAPN_Set(apn, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }

        public static byte[] PacketBuild_Command_RunConnectivityTest(CSTagTypes.CommandData.CommandParameters_RunConnectivityTest packetInstigator)
        {
            Byte[] packetData = new Byte[1024];
            PacketHandlerLibrary.BuildPacket packetHandler = new PacketHandlerLibrary.BuildPacket();
            uint packetLength = packetHandler.Packet_Command_RunConnectivityTest(packetInstigator, packetData);
            Array.Resize<Byte>(ref packetData, (int)packetLength);
            return packetData;
        }
    }
}
