﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMIUCommandLibrary
{
    /// <summary>
    /// Error Handler contains error code information as well as the Error object for reporting an error
    /// </summary>
    class ErrorHandler
    {
        /// <summary>
        /// Error Codes used in the Error reported object
        /// </summary>
        public enum ErrorCodes
        {            
            /// <summary>
            /// Error in the setup of the library, definitely not good
            /// </summary>
            Error_CommandLibrary_Setup = -100,
            /// <summary>
            /// Error when setting the "state" the BTLE dongle should be in - Initialized, Scanning, Connecting, etc.
            /// </summary>
            Error_BTLE_Dongle_State = -200,
            /// <summary>
            /// Error with the connection of the BTLE dongle with the computer
            /// </summary>
            Error_BTLE_Initialization = -201,
            /// <summary>
            /// Error with the connection to the CMIU device
            /// </summary>
            Error_BTLE_Device_Connection = -202,
            /// <summary>
            /// Error with the connection to the CMIU device
            /// </summary>
            Error_BTLE_SendDataFailure = -203,
            /// <summary>
            /// Error when configuring the JLink device
            /// </summary>
            Error_JLink_Configuration = -300,
            /// <summary>
            /// Error when programming a device
            /// </summary>
            Error_JLink_Programming = -301,
            /// <summary>
            /// Error when reading a device
            /// </summary>
            Error_JLink_Reading = -302,
            /// <summary>
            /// Error when opening a serial port
            /// </summary>
            Error_Serial_Open = -400,
            /// <summary>
            /// Error when reading data from the serial port
            /// </summary>
            Error_Serial_Read = -401,
            /// <summary>
            /// Error when sending data on the serial port
            /// </summary>
            Error_Serial_Send = -402
        }

    }
}
