﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Reflection;
using nRFDongleUartLib;
using Nordicsemi;
using pUartLibrary;
using UartDataController;
using PacketHandlerLibrary;

/// <summary>
/// CMIU Command Library
/// This supports Nordic RF Dongle, Virtual Serial Port, and JLink Interface commands
/// </summary>
namespace CMIUCommandLibrary
{
    

    /// <summary>
    /// This is the class containing all of the available commands
    /// </summary>
    public class CMIUCommands
    {
        
        private nRFUartController rfInterface;
        private pSerialUart serialUart;
        private JLinkInterface jLink;
        private List<CMIUDeviceBTLEInfo> discoveredCMIUs;
        private Timer deviceTimer;
        private static Object devicesLock = new Object();
        private const int CMIU_ADVERTISING_TIMEOUT_SECONDS = 5;
        //private SupportedCommands supportedCommands;

        //private PacketHandlerLibrary.CCommandParameters[] supportedCommandParameters = { };

        /// <summary>
        /// CommandInterface enum
        /// Either Bluetooth or UART
        /// </summary>
        public enum CommandInterface
        {
            Bluetooth = 1,
            UART = 2
        }

        #region Public Functions

        /// <summary>
        /// This function should be called before calling any other commands. This is the setup function for the entire library
        /// </summary>
        public void CommandLibrary_Setup()
        {
            // Setup supported commands
            //supportedCommands = new SupportedCommands();

            // Setup physical serial uart
            serialUart = new pSerialUart();
            //serialUart.SerialDataRxd += OnSerialDataRxd;
            serialUart.OnSerialDataEvent += OnSerialDataRxd;
            serialUart.OnError_OpenPort += serialUart_OnError_OpenPort;
            serialUart.OnError_SendData += serialUart_OnError_SendData;
            serialUart.MessageReceived += serialUart_MessageReceived;

            // Setup JLink
            jLink = new JLinkInterface();

            // Setup RF Dongle
            rfInterface = new nRFUartController();
            rfInterface.DebugMessagesEnabled = false;
            discoveredCMIUs = new List<CMIUDeviceBTLEInfo>();
            
            /* Registering event handler methods for all nRFUartController events. */
            rfInterface.LogMessage += OnLogMessage;
            rfInterface.Initialized += OnControllerInitialized;            
            rfInterface.Scanning += OnScanning;
            rfInterface.ScanningCanceled += OnScanningCanceled;
            rfInterface.Connecting += OnConnecting;
            rfInterface.ConnectionCanceled += OnConnectionCanceled;
            rfInterface.Connected += OnConnected;
            rfInterface.PipeDiscoveryCompleted += OnControllerPipeDiscoveryCompleted;
            rfInterface.Disconnected += OnDisconnected;
            rfInterface.SendDataStarted += OnSendDataStarted;
            rfInterface.SendDataCompleted += OnSendDataCompleted;
            rfInterface.SendDataFailed += rfInterface_SendDataFailed;
            rfInterface.SendDataReadyToSend += rfInterface_SendDataReadyToSend;
            rfInterface.MessageReceived += rfInterface_MessageReceived;
            rfInterface.DeviceDiscovered += rfInterface_DeviceDiscovered;
            rfInterface.ProgressUpdated += rfInterface_ProgressUpdated;
            rfInterface.OnErrorDongleState += rfInterface_OnErrorDongleState;
            rfInterface.OnErrorInitialization += rfInterface_OnErrorInitialization;
            rfInterface.OnErrorConnection += rfInterface_OnErrorConnection;
        }



        ///////////////////////////////////////////////////
        //
        // Error Handling
        //
        ///////////////////////////////////////////////////

        void rfInterface_OnErrorConnection(object sender, EventArgs e)
        {
            OnErrorEvent(this, new ErrorInfo((int)ErrorHandler.ErrorCodes.Error_BTLE_Device_Connection, ErrorHandler.ErrorCodes.Error_BTLE_Device_Connection.ToString()));
        }

        void rfInterface_OnErrorInitialization(object sender, EventArgs e)
        {
            OnBTLE_DongleInitializationFailed(this, EventArgs.Empty);
            OnErrorEvent(this, new ErrorInfo((int)ErrorHandler.ErrorCodes.Error_BTLE_Initialization, ErrorHandler.ErrorCodes.Error_BTLE_Initialization.ToString()));
        }

        void rfInterface_OnErrorDongleState(object sender, EventArgs e)
        {
            OnErrorEvent(this, new ErrorInfo((int)ErrorHandler.ErrorCodes.Error_BTLE_Dongle_State, ErrorHandler.ErrorCodes.Error_BTLE_Dongle_State.ToString()));
        }

        void serialUart_OnError_SendData(object sender, EventArgs e)
        {
            OnErrorEvent(this, new ErrorInfo((int)ErrorHandler.ErrorCodes.Error_Serial_Send, ErrorHandler.ErrorCodes.Error_Serial_Send.ToString()));
        }

        void serialUart_OnError_OpenPort(object sender, EventArgs e)
        {
            OnErrorEvent(this, new ErrorInfo((int)ErrorHandler.ErrorCodes.Error_Serial_Open, ErrorHandler.ErrorCodes.Error_Serial_Open.ToString()));
        }


        ///////////////////////////////////////////////////
        //
        // BLE Functions
        //
        ///////////////////////////////////////////////////


        /// <summary>
        /// This function initializes the RF Dongle, this can take some time
        /// </summary>
        public void BTLE_Dongle_Initialize()
        {
            rfInterface.Initialize();
        }

        /// <summary>
        /// Function to disconnect dongle
        /// </summary>
        public void BTLE_Dongle_Disconnect()
        {
            rfInterface.Close();
            deviceTimer.Enabled = false;
        }

        /// <summary>
        /// Function to scan for CMIUs
        /// </summary>
        public void BLTE_Dongle_ScanForCMIUs()
        {
            rfInterface.ScanForDevices();
            deviceTimer = new Timer();
            deviceTimer.Interval = 1000;
            deviceTimer.Elapsed += deviceTimer_Elapsed;
            deviceTimer.Enabled = true;
        }

        /// <summary>
        /// Function to determine if connected to a CMIU
        /// </summary>
        /// <returns></returns>
        public bool BTLE_CMIU_IsConnected()
        {
            return rfInterface.IsConnected();
        }

        /// <summary>
        /// Function to disconnect from connected CMIU
        /// </summary>
        public void BTLE_CMIU_Disconnect()
        {
            rfInterface.InitiateDisconnect();
        }        

        /// <summary>
        /// Function to connect to first CMIU discovered "auto connect"
        /// </summary>
        public void BTLE_CMIU_AutoConnect()
        {
            rfInterface.InitiateConnection();
            deviceTimer.Enabled = false;
        }        

        /// <summary>
        /// Function to connect to specified device by the device name (CMIU ID)
        /// </summary>
        /// <param name="cmiuID">string CMIU ID</param>
        public void BTLE_CMIU_ConnectToCMIUID(string cmiuID)
        {
            rfInterface.ConnectToDeviceWithName(cmiuID);
            deviceTimer.Enabled = false;
        }


        ///////////////////////////////////////////////////
        //
        // Command Functions
        //
        ///////////////////////////////////////////////////


        /// <summary>
        /// Sends a Ping command
        /// </summary>
        /// <param name="type">CommandInterface type i.e. BTLE or Serial</param>
        public bool SendCommand_Ping(CommandInterface type)
        {
            if (type == CommandInterface.Bluetooth)
            {
                return rfInterface.SendPingPacket();
            }                
            else
            {
                serialUart.SendPingPacket();
                return true;
            }                

        }


        /// <summary>
        /// Sends a Transmit Detailed Config Packet command packet over BTLE
        /// </summary>
        /// <param name="cmdInterface">CommandInterface type i.e. BTLE or Serial</param>
        /// <returns>returns status of packet building and sending</returns>
        public bool PacketSend_Command_PublishRequestedPacket(CommandInterface cmdInterface, CSTagTypes.CommandData.CommandParameters_PublishRequestedPacket publishPacket)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_PublishRequestedPacket(publishPacket);
            if(cmdInterface == CommandInterface.Bluetooth)
            {
                
                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }            
        }

        /// <summary>
        /// Sends a Command to Read Connected Devices
        /// </summary>
        /// <param name="cmdInterface">CommandInterface type i.e. BTLE or Serial</param>
        /// <param name="device">Read Connected Device data</param>
        /// <returns>returns status of packet building and sending</returns>
        public bool PacketSend_Command_ReadConnectedDevices(CommandInterface cmdInterface, CSTagTypes.CommandData.CommandParameters_ReadConnectedDevices device)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_ReadConnectedDevices(device);
            if (cmdInterface == CommandInterface.Bluetooth)
            {
                
                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to Read Memory
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_ReadMemory(CommandInterface cmdInterface, CSTagTypes.CommandData.CommandParameters_ReadMemory read)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_ReadMemory(read);
            if (cmdInterface == CommandInterface.Bluetooth)
            {
                
                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to RF Test Mode LTE
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_LTE_RFTestMode(CommandInterface cmdInterface, CSTagTypes.CommandData.CommandParameters_LTECarrierAssert rf)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_LTERFTestMode(rf);
            if (cmdInterface == CommandInterface.Bluetooth)
            {
                
                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to RF Test Mode BTLE
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_BTLE_RFTestMode(CommandInterface cmdInterface, CSTagTypes.CommandData.CommandParameters_BTLECarrierAssert rf)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_BTLERFTestMode(rf);
            if (cmdInterface == CommandInterface.Bluetooth)
            {

                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to Set Power Mode
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_SetPowerMode(CommandInterface cmdInterface, CSTagTypes.CommandData.CommandParameters_SetPowerMode power)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_SetPowerMode(power);
            if (cmdInterface == CommandInterface.Bluetooth)
            {                
                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to Toggle Modem
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_ToggleModem(CommandInterface cmdInterface, CSTagTypes.CommandData.CommandParameters_ToggleLTEModem toggle)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_ToggleModem(toggle);
            if (cmdInterface == CommandInterface.Bluetooth)
            {
                
                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to Erase Memory Image
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_EraseMemoryImage(CommandInterface cmdInterface, CSTagTypes.CommandData.CommandParameters_EraseMemoryImage erase)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_EraseMemoryImage(erase);
            if (cmdInterface == CommandInterface.Bluetooth)
            {

                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to Set Recording Reporting Intervals
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_SetRecordingReportingIntervals(CommandInterface cmdInterface, CSTagTypes.CommandData.CommandParameters_SetRecordingReportingIntervals intervals)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_SetRecordingReportingIntervals(intervals);
            if (cmdInterface == CommandInterface.Bluetooth)
            {

                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to Sleep for X seconds
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_SleepSeconds(CommandInterface cmdInterface, CSTagTypes.CommandData.CommandParameters_SleepSeconds sleep)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_SleepSeconds(sleep);
            if (cmdInterface == CommandInterface.Bluetooth)
            {

                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to Get CAN Data
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_GetCANData(CommandInterface cmdInterface)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_GetCANData();
            if (cmdInterface == CommandInterface.Bluetooth)
            {

                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to Get Debug Log
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_GetDebugLog(CommandInterface cmdInterface)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_GetDebugLog();
            if (cmdInterface == CommandInterface.Bluetooth)
            {

                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to Get CMIU Signal Quality
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_GetCMIUSignalQuality(CommandInterface cmdInterface)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_GetCMIUSignalQuality();
            if (cmdInterface == CommandInterface.Bluetooth)
            {

                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to Set Modem FOTA
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_SetModemFOTA(CommandInterface cmdInterface, CSTagTypes.CommandData.CommandParameters_ModemFOTA fota)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_SetModemFOTA(fota);
            if (cmdInterface == CommandInterface.Bluetooth)
            {

                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to Get CMIU APN
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_CMIUAPN_Get(CommandInterface cmdInterface)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_APN_Get();
            if (cmdInterface == CommandInterface.Bluetooth)
            {

                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to Set CMIU APN
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_CMIUAPN_Set(CommandInterface cmdInterface, CSTagTypes.CommandData.CommandParameters_APN_Set apn)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_APN_Set(apn);
            if (cmdInterface == CommandInterface.Bluetooth)
            {

                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        /// <summary>
        /// Sends a Command to run a connectivity test against MDCE
        /// </summary>
        /// <returns></returns>
        public bool PacketSend_Command_RunConnectivityTest(CommandInterface cmdInterface, CSTagTypes.CommandData.CommandParameters_RunConnectivityTest packetInstigator)
        {
            Byte[] packetPayload = CommandPackets.PacketBuild_Command_RunConnectivityTest(packetInstigator);
            if (cmdInterface == CommandInterface.Bluetooth)
            {

                bool success = rfInterface.SendCmdPacket(packetPayload, 5000);
                return success;
            }
            else
            {
                bool success = serialUart.SendCmdPacket(packetPayload, 5000);
                return success;
            }
        }

        



        ///////////////////////////////////////////////////
        //
        // Serial Port Functions
        //
        ///////////////////////////////////////////////////

        /// <summary>
        /// Get the list of available serial ports
        /// </summary>
        /// <returns></returns>
        public string[] Serial_SerialPortNames_Get()
        {
            return serialUart.GetAvailableSerialPorts();
        }

        /// <summary>
        /// Function to open a serial port using the serial port name
        /// </summary>
        /// <param name="portName">string name of the port to open i.e COM2</param>
        /// <returns>boolean success or failure</returns>
        public bool Serial_SerialPort_Open(string portName)
        {
            return serialUart.OpenSerialPortWithName(portName);
        }

        /// <summary>
        /// Function to get open/close status of the serial port
        /// </summary>
        /// <returns>true if open, false if closed</returns>
        public bool Serial_SerialPort_IsOpen()
        {
            return serialUart.SerialPortIsOpen();
        }

        /// <summary>
        /// Function to close the serial port
        /// </summary>
        public void Serial_SerialPort_Close()
        {
            serialUart.CloseSerialPort();
        }



        ///////////////////////////////////////////////////
        //
        // JLink Function
        //
        ///////////////////////////////////////////////////

        /// <summary>
        /// Function to get available JLink speeds
        /// </summary>
        /// <returns>string array of speeds</returns>
        public string[] JLink_Speeds_Get()
        {
            return jLink.JLinkSpeeds;
        }
        /// <summary>
        /// Function to setup the JLink connection. This must be called first.
        /// </summary>
        /// <param name="speed">string - speed of the JLink: 400, 1000, 2000, 3000, or 4000</param>
        public void JLink_ConfigureSpeed(string speed)
        {
            jLink.Speed = speed;
        }

        /// <summary>
        /// Function to program the CMIU with a hex file using the JLink 
        /// Requires "ConfigureJLink" function to be called at least once before this is called
        /// </summary>
        /// <param name="filePath">Full file path of the hex file</param>
        /// <returns>bool - Pass or Fail</returns>
        public bool JLink_Device_ProgramWithFile(string filePath)
        {
            // Attempt to connect
            if(!JLinkConnect())
            {
                return false;
            }

            jLink.FilePath = filePath;
            if(!JLinkProgramDevice())
            {
                return false;
            }

            // Disconnect once completed
            jLink.CloseJLink();

            return true;
        }

        /// <summary>
        /// Function to read all of the raw data from the device flash memory
        /// Requires "ConfigureJLink" function to be called at least once before this is called 
        /// </summary>
        public string[] JLink_Device_ReadCode()
        {
            // Attempt to connect
            if (!JLinkConnect())
            {
                return null;
            }

            List<string> intelHex = jLink.ReadFlash();
            return intelHex.ToArray();
        }

        /// <summary>
        /// Function to close the JLink Connection
        /// </summary>
        public void JLink_Disconnect()
        {
            jLink.CloseJLink();
        }

        #endregion

        #region Public Events
        /// <summary>
        /// Event used for logging info
        /// </summary>
        public event EventHandler<LogInfo> OnLogInfo;

        /// <summary>
        /// Event triggered when BTLE message is recevied
        /// </summary>
        public event EventHandler<LogInfo> OnBTLE_LogMessage;

        /// <summary>
        /// Event triggered when Serial message is recevied
        /// </summary>
        public event EventHandler<LogInfo> OnSerial_LogMessage;

        /// <summary>
        /// Event triggered when dongle has been initailized
        /// </summary>
        public event EventHandler<EventArgs> OnBTLE_DongleInitialized;

        /// <summary>
        /// Event triggered when the dongle fails to initialize. This could be due to the dongle not being plugged in.
        /// </summary>
        public event EventHandler<EventArgs> OnBTLE_DongleInitializationFailed;

        /// <summary>
        /// Event triggered when CMIU has been connected to
        /// </summary>
        public event EventHandler<EventArgs> OnBTLE_CMIUConnected;

        /// <summary>
        /// Event triggered when CMIU has been disconnected
        /// </summary>
        public event EventHandler<EventArgs> OnBTLE_CMIUDisconnected;

        /// <summary>
        /// Event triggered every 1 second that returns all of the CMIUs that have been discovered 
        /// and have been heard from within the timeout period (5 seconds)
        /// </summary>
        public event EventHandler<CMIUDevicesBTLEInfoArgs> OnBTLE_CMIUsDiscovered;

        /// <summary>
        /// Event triggered when a response packet is received and successfully parsed over BTLE.
        /// </summary>
        public event EventHandler<PacketResponseInfo> OnBTLE_ResponsePacketReceived;

        /// <summary>
        /// Event triggered when a response packet is received and successfully parsed over BTLE.
        /// </summary>
        public event EventHandler<PacketResponseInfo> OnSerial_ResponsePacketReceived;

        /// <summary>
        /// Event triggered when an error is encoutered. This will contain the error code as defined in the 
        /// ErrorHandler.ErrorCodes enum as well as the enum name and any other info.
        /// </summary>
        public event EventHandler<ErrorInfo> OnErrorEvent;



        #endregion
        
        
        #region Internal Private Methods
        

        // Logging method
        private void AddToLog(string message)
        {
            OnLogInfo(this, new LogInfo(message));
        }

        // Parses and formats rf messages received
        private string ParseReceivedMessage(CommandInterface cmdInterface, UarcMessage message)
        {
            string parsed = "";
            if (cmdInterface == CommandInterface.Bluetooth)
                parsed += "BTLE RX ";
            else
                parsed += "SERIAL RX ";


            if (message.type == UarcMessageType.UARC_MT_ASYNC)
            {
                parsed += "ASYNC: ";
                string convertedPayload = Encoding.ASCII.GetString(message.payload);
                parsed += convertedPayload;
                parsed = parsed.TrimEnd(new char[] {'\r', '\n'});
                return parsed;
            }
            else if (message.type == UarcMessageType.UARC_MT_RESP)
            {
                PacketHandlerLibrary.ParsePacket parser = new ParsePacket();
                CSTagTypes.Packets.ResponsePacket response = parser.ParseMessagePayload(message.payload);
                if(response != null)
                {
                    if (cmdInterface == CommandInterface.Bluetooth)
                        OnBTLE_ResponsePacketReceived(this, new PacketResponseInfo(response));
                    else
                        OnSerial_ResponsePacketReceived(this, new PacketResponseInfo(response));
                }
                return "RESPONSE PACKET RECEIVED.";
            }
            else if (message.type == UarcMessageType.UARC_MT_HEARTBEAT)
            {
                // Return an empty string. We don't need lots of text to display the heartbeat.
                return "";
            }
            else
            {
                //AddToLog("ERROR Unrecognized message received.");
                return "ERROR Unrecognized message received.";
            }

        }

        // Parses and formats serial messages received
        private string ParseSerialMessage(string message)
        {
            string parsed = "SERIAL RX ASYNC: " + message;
            return parsed;
        }

        // Function to called on timer interval to manage what devices should be sent as discovered
        private void deviceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            List<CMIUDeviceBTLEInfo> remove = new List<CMIUDeviceBTLEInfo>();

            // Check what needs to be removed
            foreach (CMIUDeviceBTLEInfo device in discoveredCMIUs)
            {
                // See if the last time the device was read excedes the timeout
                DateTime last = Convert.ToDateTime(device.LastHeard);
                DateTime now = DateTime.Now;
                var diff = (now - last).TotalSeconds;
                if (diff > CMIU_ADVERTISING_TIMEOUT_SECONDS)
                {
                    remove.Add(device);
                }
            }

            // Remove the devices that have timed out
            foreach (CMIUDeviceBTLEInfo device in remove)
            {
                lock (devicesLock)
                {
                    discoveredCMIUs.Remove(device);
                }                
            }

            OnBTLE_CMIUsDiscovered(this, new CMIUDevicesBTLEInfoArgs(discoveredCMIUs.ToList()));
        }

        // Connect to JLink
        private bool JLinkConnect()
        {
            jLink.ClearError();

            // Attempt to connect
            if (jLink.OpenJlink() != JLinkInterface.JLinkReturnValues.JLINK_OPERATION_SUCEEDED)
            {
                return false;
            }

            // Get the serial number
            string device = jLink.GetSerialNumber();

            // Check speed
            if(jLink.Speed == null)
            {
                jLink.Speed = jLink.JLinkSpeeds.Last();
            }

            if(!jLink.JLinkSpeeds.Contains(jLink.Speed))
            {
                return false;
            }

            // Configure JLink connection
            if (jLink.InitializeJLink(jLink.Speed, JLinkInterface.CURRENT_DEVICE) != JLinkInterface.JLinkReturnValues.JLINK_OPERATION_SUCEEDED)
            {
                return false;
            }

            return true;
        }

        // Program a CMIU with JLink
        private bool JLinkProgramDevice()
        {
            jLink.ClearError();

            // Attempt to halt target
            bool success = jLink.HaltTarget();
            if (!success)
            {
                return false;
            }

            // Erase the flash first
            if (jLink.EraseFlash() != JLinkInterface.JLinkReturnValues.JLINK_OPERATION_SUCEEDED)
            {
                return false;
            }

            // Program the device, reset the device, then go
            if((jLink.JLinkProgramFlash(jLink.FilePath)) != JLinkInterface.JLinkReturnValues.JLINK_OPERATION_SUCEEDED)
            {
                return false;
            }

            return true;
        }
        #endregion

        #region RF Interface Events

        private void OnLogMessage(object sender, OutputReceivedEventArgs e)
        {
            //OnLogInfo(this, new LogInfo(e.Message));
        }

        void rfInterface_MessageReceived(UarcMessage m)
        {
            // Parse and notify the message received
            string message = ParseReceivedMessage(CommandInterface.Bluetooth, m);
            if (message.Equals(""))
            {
                //Don't display an empty string.
            }
            else
            {
                OnBTLE_LogMessage(this, new LogInfo(message));
            }
        }

        private void OnControllerInitialized(object sender, EventArgs e)
        {
            OnBTLE_DongleInitialized(this, EventArgs.Empty);
        }

        private void OnScanning(object sender, EventArgs e)
        {

        }

        private void rfInterface_DeviceDiscovered(object sender, OnDeviceDiscoveredArgs e)
        {
            CMIUDeviceBTLEInfo cmiu = new CMIUDeviceBTLEInfo();
            cmiu.DeviceAddress = e.device.DeviceAddress.ToString();

            IDictionary<DeviceInfoType, string> deviceInfo = e.device.DeviceInfo;

            cmiu.UUID = "UUID";
            cmiu.RSSI = "N/A";
            cmiu.DeviceName = "N/A";
            cmiu.LastHeard = DateTime.Now.ToString();

            if (deviceInfo.ContainsKey(DeviceInfoType.CompleteLocalName))
            {
                // TODO validate the name/CMIU id
                cmiu.DeviceName = deviceInfo[DeviceInfoType.CompleteLocalName].ToString();
            }
            else
            {
                // There is no CMIU ID
                return;
            }

            if(deviceInfo.ContainsKey(DeviceInfoType.Rssi))
            {
                cmiu.RSSI = deviceInfo[DeviceInfoType.Rssi].ToString();
            }                            

            // Check if a device should be added/updated in list
            lock (devicesLock)
            {
                bool containsDevice = discoveredCMIUs.Any(item => item.DeviceAddress == cmiu.DeviceAddress);
                if (containsDevice)
                {
                    // Update item already in list
                    int index = discoveredCMIUs.IndexOf(discoveredCMIUs.Where(item => item.DeviceAddress == cmiu.DeviceAddress).FirstOrDefault());
                    discoveredCMIUs[index] = cmiu;
                }
                else
                {
                    // Add it
                    discoveredCMIUs.Add(cmiu);
                }
            }
            

            //CMIUDiscovered(this, new CMIUDeviceBTLEInfoArgs(cmiu));
        }

        private void OnScanningCanceled(object sender, EventArgs e)
        {
            AddToLog("Stopped scanning");
        }

        private void OnConnectionCanceled(object sender, EventArgs e)
        {
            AddToLog("Connection cancelled");
            //rfInterface.status = rfUARTInterface.Status.Initialized;
        }

        private void OnConnecting(object sender, EventArgs e)
        {
            AddToLog("Connecting...");
            //rfInterface.status = rfUARTInterface.Status.Connecting;
        }

        private void OnConnected(object sender, EventArgs e)
        {
            //isControllerConnected = true;
            //AddToLog("Connected!");
            //rfInterface.status = rfUARTInterface.Status.Connected;
            OnBTLE_CMIUConnected(this, EventArgs.Empty);
        }

        private void OnControllerPipeDiscoveryCompleted(object sender, EventArgs e)
        {
            //AddToLog("Ready to send");
        }

        private void OnSendDataStarted(object sender, EventArgs e)
        {
            AddToLog("Started Sending Packet...");
        }

        private void OnSendDataCompleted(object sender, EventArgs e)
        {
            AddToLog("Packet Sent Successfully");
        }

        void rfInterface_SendDataFailed(object sender, EventArgs e)
        {
            OnErrorEvent(this, new ErrorInfo((int)ErrorHandler.ErrorCodes.Error_BTLE_SendDataFailure, "FAILED TO SEND PACKET"));
        }

        private void OnDisconnected(object sender, EventArgs e)
        {
            OnBTLE_CMIUDisconnected(this, EventArgs.Empty);
        }

        void rfInterface_SendDataReadyToSend(object sender, OutputReceivedEventArgs e)
        {
            AddToLog("Packet data being sent being sent: " + e.Message);
        }

        void rfInterface_ProgressUpdated(object sender, ValueEventArgs<int> e)
        {
            //throw new NotImplementedException();
        }

        #endregion

        
        #region Serial Uart Events

        void serialUart_MessageReceived(UarcMessage m)
        {
            //throw new NotImplementedException();
            string message = ParseReceivedMessage(CommandInterface.UART, m);
            if (message.Equals(""))
            {
                //Don't display an empty string.
            }
            else
            {
                OnSerial_LogMessage(this, new LogInfo(message));
            }
        }

        private void OnSerialDataRxd(object sender, SerialDataEventArgs e)
        {
            //throw new NotImplementedException();
            string message = "SENDING COMMAND: " + e.Message;
            AddToLog(message);
        }

        #endregion
    }
}
