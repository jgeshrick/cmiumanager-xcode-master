﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using IntelHexFileHelper;

namespace CMIUCommandLibrary
{
    class JLinkInterface
    {
        public const string CURRENT_DEVICE = "EFM32LG332F256";


        #region JLINK Speeds
        public string[] JLinkSpeeds = { "400", "1000", "2000", "3000", "4000" };
        #endregion

        #region Error Defs
        public enum JLinkReturnValues
        {
            JLINK_OPERATION_SUCEEDED = 0,
            JLINK_NOT_FOUND,
            JLINK_WAS_NOT_SELECTED,
            JLINK_SELECT_OPERATION_FAILED,
            JLINK_OPEN_ERROR,
            SELECT_MCU_DEVICE_ERROR,
            HEX_FILE_CREATION_ERROR,

            // Published J-Link Global Errors
            JLINK_ERR_EMU_NO_CONNECTION = -256, // (0xFFFFFF00) No connection to emulator / Connection to emulator lost
            JLINK_ERR_EMU_COMM_ERROR = -257, // (0xFFFFFEFF) Emulator communication error (host-interface module reproted error)
            JLINK_ERR_DLL_NOT_OPEN = -258, // (0xFFFFFEFE) DLL has not been opened but needs to be (JLINKARM_Open() needs to be called first)
            JLINK_ERR_VCC_FAILURE = -259, // (0xFFFFFEFD) Target system has no power (Measured VTref < 1V)
            JLINK_ERR_INVALID_HANDLE = -260, // (0xFFFFFEFC) File handle / memory area handle needed for operation, but given handle is not valid
            JLINK_ERR_NO_CPU_FOUND = -261, // (0xFFFFFEFB) Could not find supported CPU
            JLINK_ERR_EMU_FEATURE_NOT_SUPPORTED = -262, // (0xFFFFFEFA) Emulator does not support the selected feature (Usually returned by functions which need specific emulator capabilities)
            JLINK_ERR_EMU_NO_MEMORY = -263, // (0xFFFFFEF9) Emulator does not have enough memory to perform the requested operation
            JLINK_ERR_TIF_STATUS_ERROR = -264, // (0xFFFFFEF8) Things such as "TCK is low but should be high"
            JLINK_ERR_FLASH_PROG_COMPARE_FAILED = -265,
            JLINK_ERR_FLASH_PROG_PROGRAM_FAILED = -266,
            JLINK_ERR_FLASH_PROG_VERIFY_FAILED = -267,

            // TO DO:
            // JLINK returns this if the Hex file is corrupt and an attempt 
            // is made to program the target flash.
            // It should return JLINK_ERR_FLASH_PROG_VERIFY_FAILED instead.
            JLINK_ERR_OPEN_FILE_FAILED = -268,
            JLINK_ERR_UNKNOWN_FILE_FORMAT = -269,
            JLINK_ERR_WRITE_TARGET_MEMORY_FAILED = -270
        }
        #endregion

        #region Class Constants

        const UInt32 JLINKARM_TIF_SWD = 1;
        const int JLINKARM_HOSTIF_USB = 1;

        #endregion

        #region DLL Methods Definitions

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt16 JLINKARM_EMU_SelectByUSBSN(UInt32 SerialNo);

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern string JLINKARM_Open();

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int JLINKARM_ExecCommand(string sIn, string error, Int16 len);

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int JLINKARM_TIF_Select(UInt32 targetif);

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int JLINK_GetSN();

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int JLINKARM_SetSpeed(UInt32 speed);

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int JLINKARM_Connect();

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int JLINK_EraseChip();

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int JLINKARM_BeginDownload(UInt32 startaddress);

        // Note: This uses StdCall, not Cdecl.
        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int JLINK_DownloadFile(string hexfile, UInt32 startaddress);

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int JLINKARM_EndDownload();

        // Note: The UM08002_JLinkDLL.pdf does not say what value is returned on succeszs or failure.
        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int JLINKARM_Reset();

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte JLINKARM_Halt();

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte JLINKARM_IsOpen();

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void JLINKARM_Go();

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void JLINKARM_Close();

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void JLINKARM_ClrError();

        [DllImport("JLinkARM.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int JLINKARM_ReadMemEx(UInt32 Addr, UInt32 NumBytes, byte[] pData, UInt32 AccessWidth);

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 0x40000)]
        public byte[] pData = new byte[0x40000];

        #endregion

        #region Public Properties

        public string Speed;
        public string FilePath;

        #endregion

        #region Public Functions

        /// <summary>
        /// This must be called first before other J-Link calls.
        /// If more than one J-link emulator is connected to the PC,
        /// this method will display a form that lets the user select the
        /// J-Link emulator that they want to use.
        /// If only one J-Link emulator is found, it will use it.
        /// ReturnSerNo() can then be called to get the J-Link serial number
        /// that was selected.
        /// </summary>
        /// <returns>
        /// ErrorDefs.JLinkErrors.JLINK_OPERATION_SUCEEDED is returned on success.
        /// A ErrorDefs.JLinkErrors defined error will be returned otherwise.
        /// </returns>
        public JLinkReturnValues OpenJlink()
        {
            JLinkReturnValues retVal = JLinkReturnValues.JLINK_OPERATION_SUCEEDED;

            // See if the J-Link device can be opened.
            string test = "";
            test = OpenJLinkArm();

            if (test != null)
            {
                retVal = JLinkReturnValues.JLINK_OPEN_ERROR;
            }

            return retVal;
        }

        /// <summary>
        /// Resturns the serila number that J-Link returned after the J-Link was opened.
        /// If more than one J-link emulator is connected to the PC, a from will be displayed
        /// that allows the use to select the desired J-Link emulator.
        /// </summary>
        /// <returns></returns>
        public string GetSerialNumber()
        {
            return JLINK_GetSN().ToString();
        }

        /// <summary>
        /// Initialize J-Link - Find J-Link based on serial number and establish a USB connection to it.
        /// Prepare it to accept commands.
        /// </summary>
        /// <returns>
        /// 0 = success. anything else retuns an error.
        /// Note: Multiple returns are used here to avoid a large nested set of methods.
        /// </returns>
        public JLinkReturnValues InitializeJLink(string speed, string device)
        {
            //Speed = speed;
            JLinkReturnValues retVal = JLinkReturnValues.JLINK_OPERATION_SUCEEDED;

            // Select the MCU Device.       
            if (SelectMCU(device) == false)
            {
                retVal = JLinkReturnValues.SELECT_MCU_DEVICE_ERROR;
                return retVal;
            }

            // Select the Target Interface type.
            // This type will always be used for this project
            retVal = SelectTargetIfType(JLINKARM_TIF_SWD);

            // Select the speed at which communications with the target takes place.
            // This will be selected by a listBox.

            UInt32 spd = UInt32.Parse(speed);
            SetJLinkCommSpeed(spd);

            // Connect
            // This can return a variety of errors which are interpreted by the caller.
            return JlinkConnect();
        }

        /// <summary>
        /// Function to program and run device
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public JLinkReturnValues JLinkProgramFlash(string path)
        {
            JLinkReset();
            JLinkReturnValues retVal = JLinkProgramTargetFlash(path, 0);
            if (retVal == JLinkReturnValues.JLINK_OPERATION_SUCEEDED)
            {
                JLinkReset();
                JLinkGo();
            }

            return retVal;
        }

        /// <summary>
        /// Close the JLink Connection if it is already open.
        /// </summary>
        public void CloseJLink()
        {
            if (IsJLinkOpen() == true)
            {
                JLINKARM_Close();
            }
        }

        /// <summary>
        /// Check is the JLink is open.
        /// </summary>
        /// <returns>returns true if JLink has already been opened.</returns>
        public bool IsJLinkOpen()
        {
            bool result = true;
            if (JLINKARM_IsOpen() == 0)
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Halt the Target
        /// </summary>
        /// <returns>
        /// true if target has been halted.
        /// false otherwise.
        /// </returns>
        public bool HaltTarget()
        {
            bool result = false;

            if (JLINKARM_Halt() == 0)
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        ///  Use the J-Link emulator to erase the Flash Memory in the target.
        /// </summary>
        /// <returns>
        /// true if the erase process was successful.
        /// false otherwise.
        /// </returns>
        public JLinkReturnValues EraseFlash()
        {
            JLinkReturnValues retVal = (JLinkReturnValues)JLINK_EraseChip();
            return retVal;
        }

        /// <summary>
        /// Clear any JLink errors
        /// </summary>
        public void ClearError()
        {
            JLINKARM_ClrError();
        }

        /// <summary>
        /// Function to read flash from a device and save as a list of formatted intel hex files
        /// </summary>
        /// <returns></returns>
        public List<string> ReadFlash()
        {
            UInt32 memsize = 0x40000; // 256k       
            UInt32 length = (UInt32)JLINKARM_ReadMemEx(0, memsize, pData, 0);
            // Code now exists in pData
            // Convert this to hex file
            List<byte> deviceMemory = new List<byte>();
            deviceMemory.AddRange(pData);
            List<string> intelHexLines = IntelHex.CreateFile((int)0, (int)memsize,ref deviceMemory, (int)0);
            return intelHexLines;
        }

        #endregion


        #region Wrappers ForJLinkARM Dll Exported Functions

        /// <summary>
        /// Identify J-Link unit by serial number.
        /// </summary>
        /// <param name="serno"></param>
        /// <returns> bool true if found. Otherwise false</returns>
        private bool SelectJLinkDevice(UInt32 serno)
        {
            bool result = false;

            // See if a J-Link Emulator can be found.
            if (JLINKARM_EMU_SelectByUSBSN(serno) == 0)
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        /// Open the J-Link. 
        /// </summary>
        /// <returns>Returns null on succes or an error message other wise</returns>
        private string OpenJLinkArm()
        {
            return (JLINKARM_Open());

        }

        /// <summary>
        /// Select the J-Link device based on the serial number
        /// </summary>
        /// <returns>False on error. True otherwise.</returns>
        private bool SelectMCU(string device)
        {
            bool result = false;
            // Select the MCU Device.
            string s = "device = " + device;
            char[] ar = new char[80];

            // TO DO: This needs improvement. If strings in C# are a set size, this may not be necessary.
            //string errormsg = "                                                                                            ";
            // Not sure if a call back and PInvoke is required to pass back the error string.
            string errormsg = "";
            short slen = (short)errormsg.Length;
            int test = JLINKARM_ExecCommand(s, errormsg, slen);

            //int to = errormsg.Split(' ');
            if (errormsg.Length == 0)
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        ///  Select the target interface type.
        ///  For the Cortex M3 it will always be the JLINKARM_TIF_SWD type.
        /// </summary>
        /// <param name="iftype">uint32 iftype</param>
        private JLinkReturnValues SelectTargetIfType(UInt32 iftype)
        {
            return (JLinkReturnValues)JLINKARM_TIF_Select(JLINKARM_TIF_SWD);
        }

        /// <summary>
        /// Reset the JLink device.
        /// </summary>

        private JLinkReturnValues JLinkReset()
        {
            return (JLinkReturnValues)JLINKARM_Reset();
        }

        /// <summary>
        /// Set the connection speed
        /// </summary>
        /// <param name="speed">uint32 speed of connection</param>
        private JLinkReturnValues SetJLinkCommSpeed(UInt32 speed)
        {
            return (JLinkReturnValues)JLINKARM_SetSpeed(speed);
        }


        


        /// <summary>
        /// Attempt to connect to the target.
        /// </summary>
        /// <returns>
        /// 0 = SUCCESS;
        /// Non zero - various erros
        /// </returns>
        private JLinkReturnValues JlinkConnect()
        {
            return (JLinkReturnValues)JLINKARM_Connect();
        }

        

        /// <summary>
        /// Tell JLink to run program
        /// </summary>
        private void JLinkGo()
        {
            JLINKARM_Go();
        }

        /// <summary>
        /// Program 
        /// </summary>
        /// <param name="fileandpath"></param>
        /// <returns>
        /// If return value is greater or equal to 0 Flash programming was successful.
        /// If return value is less than 0 Flash programming faile.
        /// </returns>
        private JLinkReturnValues JLinkProgramTargetFlash(string fileandpath, UInt32 addr)
        {

            // Clear any errors.
            ClearError();
            int beginDLSuccess = JLINKARM_BeginDownload(0);             // Indicates start of flash download
            if (IsError(beginDLSuccess))
                return (JLinkReturnValues)beginDLSuccess;

            int dlSuccess = JLINK_DownloadFile(fileandpath, addr);      // Load the application binary to address 0
            if (IsError(dlSuccess))
                return (JLinkReturnValues)dlSuccess;

            int endDLSuccess = JLINKARM_EndDownload();                  // Indicates end of flash download

            return (JLinkReturnValues)dlSuccess;
        }

        // Pass values are 0 ro greater.
        // Fail values are negative
        private bool IsError(int returnValue)
        {
            if(returnValue >= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        
        #endregion
    }
}
