﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTagTypes
{
    public class CommandData
    {

        /// The E_COMMAND_ID enum indicates the command byte used in Tag Number 32
        public enum CommandIds
        {
            /** 0 Update image/s */
            E_COMMAND_UPDATE_IMAGE = 0x00,
            /** 1	Reboot CMIU */
            E_COMMAND_REBOOT = 0x01,
            /** 2	Mag swipe Emulation */
            E_COMMAND_MAGSWIPE = 0x02,
            /** 3	Enter Sleep state */
            E_COMMAND_SLEEP = 0x03,
            /** 4	Enter normal operating mode from sleep state */
            E_COMMAND_WAKE = 0x04,
            /** 5	Enter operation mode */
            E_COMMAND_ENTER_OPERATION = 0x05,
            /** 6	Erase Memory */
            E_COMMAND_ERASE_MEMORY_IMAGE = 0x06,
            /** 7	Set MQTT Broker Address */
            E_COMMAND_SET_BROKER_ADDRESS = 0x07,
            /** 8	Set Fallback MQTT Broker Address */
            E_COMMAND_SET_FALLBACK_BROKER_ADDRESS = 0x08,
            /** 9	Set Current Time and Date */
            E_COMMAND_SET_CLOCK = 0x09,
            /** 10	Set the Recording and Reporting Intervals */
            E_COMMAND_SET_RECORDING_AND_REPORTING_INTERVALS = 0x0a,
            /** 11	Publish Detailed Config Packet */
            E_COMMAND_PUBLISH_REQESTED_PACKET = 0x0b,
            /** 12	Publish Basic Config Packet */
            E_COMMAND_PUBLISH_BASIC_CONFIG_PACKET = 0x0c,
            /** 13	Publish Interval Data Packet */
            E_COMMAND_PUBLISH_INTERVAL_PACKET = 0x0d,
            /** 14   Read Connected Devices **/
            E_COMMAND_READ_CONNECTED_DEVICES = 0x0e,
            /** 15	Read Device Memory */
            E_COMMAND_READ_MEMORY = 0x0f,
            /** 16	Sleep for X seconds */
            E_COMMAND_SLEEP_SECONDS = 0x10,
            /** 17	Power Mode */
            E_COMMAND_SELECT_POWER_MODE = 0x11,
            /** 18	AT Passthrough */
            E_COMMAND_AT_PASSTHROUGH = 0x12,
            /** 19	VSense */
            E_COMMAND_VSENSE_MEASUREMENT = 0x13,
            /** 20	Carrier Assert */
            E_COMMAND_LTE_CARRIER_ASSERT = 0x14,
            /** 21	LTE RX Test Mode */
            E_COMMAND_LTE_RX_TEST_MODE = 0x15,
            /** 22	Get CAN+ Data */
            E_COMMAND_GET_CAN_DATA = 0x16,
            /** 23	Set Micro EM */
            E_COMMAND_SET_MICRO_EM = 0x17,
            /** 24	BTLE Advertising */
            E_COMMAND_BTLE_ADVERTISING = 0x18,
            /** 25	BTLE Carrier Assert */
            E_COMMAND_BTLE_CARRIER_ASSERT = 0x19,
            /** 26	BTLE RX Test Mode */
            E_COMMAND_BTLE_RX_TEST_MODE = 0x1A,
            /** 27	Assert 32kHz on GPIO */
            E_COMMAND_32KHZ_GPIO = 0x1B,
            /** 28	Register on LTE Network */
            E_COMMAND_REGISTER_ONLTE_NETWORK = 0x1C,
            /** 29	Toggle LTE Modem */
            E_COMMAND_TOGGLE_LTE_MODEM = 0x1D,
            /** 30	Get Debug Log */
            E_COMMAND_GET_DEBUG_LOG = 0x1E,
            /** 31 Get CMIU Signal **/
            E_COMMAND_GET_CMIU_SIGNAL_QUALITY = 0x1F,
            /** 32	Modem FOTA command */
            E_COMMAND_MODEM_FOTA = 0x20,
            /** 33 CMIU APN Get command **/
            E_COMMAND_APN_GET = 0x21,
            /** 34 CMIU APN Set command **/
            E_COMMAND_APN_SET = 0x22,
            /** 35 Run Connectivity Test against MDCE **/
            E_COMMAND_RUN_CONNECTIVITY_TEST = 0x23,

            E_COMMAND_COUNT
        }

        /// <summary>
        /// Command Packet
        /// </summary>
        public abstract class CommandPacketParameters
        {

        }

        /// <summary>
        /// Command 6
        /// </summary>
        public class CommandParameters_EraseMemoryImage : CommandPacketParameters
        {
            public Tags.EraseMemoryImage memoryImageBitmaskTag;
        }

        /// <summary>
        /// Command 10
        /// </summary>
        public class CommandParameters_SetRecordingReportingIntervals : CommandPacketParameters
        {
            public Tags.RecordingReportingInterval intervalsTag;
        }

        /// <summary>
        /// Command 11
        /// </summary>
        public class CommandParameters_PublishRequestedPacket : CommandPacketParameters
        {
            public Tags.PublishPacketType packetTypeTag;
            public Tags.PublishPacketDestination destinationTag;
        }

        /// <summary>
        /// Command 14
        /// </summary>
        public class CommandParameters_ReadConnectedDevices : CommandPacketParameters
        {
            public Tags.CommandStartDelay startDelayTag;
            public Tags.ReadConnectedDevice readDeviceTag;
        }

        /// <summary>
        /// Command 15
        /// </summary>
        public class CommandParameters_ReadMemory : CommandPacketParameters
        {
            public Tags.ReadMemory readMemoryTag;
        }

        /// <summary>
        /// Command 16
        /// </summary>
        public class CommandParameters_SleepSeconds : CommandPacketParameters
        {
            public Tags.CommandStartDelay startDelayTag;
            public Tags.CommandDuration durationTag;
        }

        /// <summary>
        /// Command 17
        /// </summary>
        public class CommandParameters_SetPowerMode : CommandPacketParameters
        {
            public Tags.CommandStartDelay startDelayTag;
            public Tags.CommandDuration durationTag;
            public Tags.SetPowerMode powerModeBitMaskTag;
        }

        /// <summary>
        /// Command 20
        /// </summary>
        public class CommandParameters_LTECarrierAssert : CommandPacketParameters
        {
            public Tags.CommandStartDelay startDelayTag;
            public Tags.CommandDuration durationTag;
            public Tags.RFTestMode rfTestTag;
        }

        /// <summary>
        /// Command 22
        /// </summary>
        public class CommandParameters_GetCANData : CommandPacketParameters
        {
        }

        /// <summary>
        /// Command 25
        /// </summary>
        public class CommandParameters_BTLECarrierAssert : CommandPacketParameters
        {
            public Tags.CommandStartDelay startDelayTag;
            public Tags.CommandDuration durationTag;
            public Tags.BLE_RFTestMode rfTestTag;
        }

        /// <summary>
        /// Command 29
        /// </summary>
        public class CommandParameters_ToggleLTEModem : CommandPacketParameters
        {
            public Tags.CommandStartDelay startDelayTag;
            public Tags.CommandDuration durationTag;
            public Tags.ToggleModem modemTag;
        }

        /// <summary>
        /// Command 30
        /// </summary>
        public class CommandParameters_RequestDebugLog : CommandPacketParameters
        {
        }

        /// <summary>
        /// Command 31
        /// </summary>
        public class CommandParameters_CMIUSignalQuality : CommandPacketParameters
        {
        }

        /// <summary>
        /// Command 32
        /// </summary>
        public class CommandParameters_ModemFOTA : CommandPacketParameters
        {
            public Tags.FTPDNS ftpDNS;
            public Tags.FTPPortNumber ftpPortNumber;
            public Tags.FTPUsername ftpUsername;
            public Tags.FTPPassword ftpPassword;
            public Tags.ImageFilename imageFilename;
            public Tags.NewImageVersionInfo newImageVersion;
        }

        /// <summary>
        /// Command 33
        /// </summary>
        public class CommandParameters_APN_Get : CommandPacketParameters
        {
        }

        /// <summary>
        /// Command 34
        /// </summary>
        public class CommandParameters_APN_Set : CommandPacketParameters
        {
            public Tags.CMIUAPN cmiuAPN;
        }

        /// <summary>
        /// Command 35
        /// </summary>
        public class CommandParameters_RunConnectivityTest : CommandPacketParameters
        {
            public Tags.PacketInstigator packetInstigator;
        }

        
    }
}
