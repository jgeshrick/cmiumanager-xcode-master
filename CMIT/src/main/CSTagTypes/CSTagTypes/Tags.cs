﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.ComponentModel;

namespace CSTagTypes
{
    public class Tags
    {

        /**
        * Definition of the tag data bytes
        */
        [StructLayout(LayoutKind.Sequential)]
        public class TPBPTag
        {
            /** The meaning of the tag */
            public TagNumber tagNumber;
            /** If packet is data, then use this */
            public TagType tagType;
            /** The number of bytes following this tag */
            public UInt32 dataSize;
        } 


        /**
        * Enum of the valid tag number that identifies the tag (must be <= 255) as per ETI 48-00.
        */
        public enum TagNumber
        {
            /** A null tag, used for padding */
            E_TAG_NUMBER_NULL = 0,
            /** Indicates the header data for each packet the CMIU sends */
            E_TAG_NUMBER_CMIU_PACKET_HEADER = 1,
            /** Indicates CMIU configuration data */
            E_TAG_NUMBER_CMIU_CONFIGURATION = 2,
            /** Indicates a response string from the cellular network operator */
            E_TAG_NUMBER_NETWORK_OPERATORS = 3,
            /** Indicates a response string containing the cellular network performance information */
            E_TAG_NUMBER_NETWORK_PERFORMANCE = 4,
            /** Indicates a response string containing the cellular network registration status */
            E_TAG_NUMBER_REGISTRATION_STATUS = 5,
            /** Indicates a response string containing the hosts IP address */
            E_TAG_NUMBER_HOST_ADDRESS = 6,
            /** Indicates a response string containing the IP address of the fallback host */
            E_TAG_NUMBER_FALLBACK_HOST_ADDRESS = 7,
            /** Indicates a response string containing the CMIUs IP address */
            E_TAG_NUMBER_ASSIGNED_CMIU_ADDRESS = 8,
            /** Indicates a response string containing the FTP DNS address */
            E_TAG_NUMBER_FTP_DNS_ADDRESS = 9,
            /** Indicates a response uint16_t containing the FTP port number */
            E_TAG_NUMBER_FTP_PORT_NUMBER = 10,
            /** Indicates a response string containing the FTP user name */
            E_TAG_NUMBER_FTP_USERNAME = 11,
            /** Indicates a response string containing the FTP password */
            E_TAG_NUMBER_FTP_PASSWORD = 12,
            /** Indicates a response string containing the modules FOTA user ID */
            E_TAG_NUMBER_MODULE_FOTA_USER_ID = 13,
            /** Indicates a response string containing the modules FOTA password */
            E_TAG_NUMBER_MODULE_FOTA_PW = 14,
            /** Indicates a response string containing the modems hardware revision  */
            E_TAG_NUMBER_MODEM_HARDWARE_REVISION = 15,
            /** Indicates a response string containing the modems ID code */
            E_TAG_NUMBER_MODEM_MODEL_IDENTIFICATION_CODE = 16,
            /** Indicates a response string containing manufacturers ID code */
            E_TAG_NUMBER_MODEM_MANUFACTURER_ID_CODE = 17,
            /** Indicates a response string containing the modems software revision */
            E_TAG_NUMBER_MODEM_SOFTWARE_REVISION = 18,
            /** Indicates a response string containing modems IMEI number */
            E_TAG_NUMBER_IMEI = 19,
            /** Indicates a response string containing SIMs IMSI number */
            E_TAG_NUMBER_SIM_IMSI = 20,
            /** Indicates a response string containing SIM cards ID number */
            E_TAG_NUMBER_SIM_CARD_ID = 21,
            /** Indicates a response string containing PIN/PUK/PUK2 status of the device */
            E_TAG_NUMBER_PINPUKPUK2_REQUEST_STATUS = 22,
            /** Indicates the CMIUs HW diagnostics information */
            E_TAG_NUMBER_CMIU_HW_DIAGNOSTICS = 23,
            /** Indicates a response string containing the ID of the last BLE user */
            E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_ID = 24,
            /** Indicates the date and time the last BLE user logged in */
            E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_DATE = 25,
            /** Indicates CMIU flags */
            E_TAG_NUMBER_CMIU_FLAGS = 26,
            /** Indicates the the current device configuration settings */
            E_TAG_NUMBER_DEVICE_CONFIGURATION = 27,
            /** Indicates R900 regular interval readings data */
            E_TAG_NUMBER_R900_INTERVAL_DATA = 28,
            /** Indicates an event */
            E_TAG_NUMBER_EVENT = 29,
            /** Indicates a response string containing any error log messages */
            E_TAG_NUMBER_ERROR_LOG = 30,
            /** Indicates the connection timing log data */
            E_TAG_NUMBER_CONNECTION_TIMING_LOG = 31,
            /** Indicates a command or configuration setting */
            E_TAG_NUMBER_COMMAND = 32,
            /**   */
            E_TAG_NUMBER_CMIU_BASIC_CONFIGURATION = 33, //TODO - remove duplicate data in #2
            /**   */
            E_TAG_NUMBER_INTERVAL_RECORDING = 34, //TODO - remove this as it is a duplicate of 27
            /** Indicates the packet header for UART transfers */
            E_TAG_NUMBER_UART_PACKET_HEADER = 35,
            /** Indicates the current time and date */
            E_TAG_NUMBER_CURRENT_TIME = 36,
            /** Indicates a MSISDN request (phone number request) */
            E_TAG_NUMBER_MSISDN_REQUEST = 37,

            /** Indicates a HW Rev*/
            E_TAG_NUMBER_HARDWARE_REVISION = 38,
            /** Indicates a F/W Rev */
            E_TAG_NUMBER_FIRMWARE_REVISION = 39,
            /** Indicates a Bootloader Rev */
            E_TAG_NUMBER_BOOTLOADER_REVISION = 40,
            /** CMIU Config Image revision - operating characteristics*/
            E_TAG_NUMBER_CONFIG_REVISION = 41,
            /** Indicates a ARB Rev */
            E_TAG_NUMBER_ARB_REVISION = 42,
            /** Indicates a Config Rev */
            E_TAG_NUMBER_BLE_CONFIG_REVISION = 43,
            /** Indicates a CMIU Device Info */
            E_TAG_NUMBER_CMIU_INFORMATION = 44,

            /** Firmware Image name*/
            E_TAG_NUMBER_IMAGE = 46,
            /** Firmware Metadata */
            E_TAG_NUMBER_IMAGE_METADATA = 47,

            /** Recording and Reporting Interval */
            E_TAG_NUMBER_RECORDING_REPORTING_INTERVAL = 48,

            /** Image revision for the encryption details */
            E_TAG_NUMBER_ENCRYPTION_CONFIG_REVISION = 49,

            /** An 8-byte Unix timestamp */
            E_TAG_NUMBER_TIMESTAMP = 50,

            /** Error Code */
            E_TAG_NUMBER_ERROR_CODE = 51,

            /** Debug Data */
            E_TAG_NUMBER_DEBUG_DATA = 52,

            /** Command Start Delay */
            E_TAG_NUMBER_COMMAND_STARTDELAY = 53,

            /** Command Duration */
            E_TAG_NUMBER_COMMAND_DURATION = 54,

            /** Command Read Connected Device */
            E_TAG_NUMBER_COMMAND_READCONNECTEDEVICE = 55,

            /** Command Power Mode */
            E_TAG_NUMBER_COMMAND_POWERMODE = 56,

            /** Command Read Memory */
            E_TAG_NUMBER_COMMAND_MEMORY = 57,

            /** Command RF Test Mode */
            E_TAG_NUMBER_COMMAND_RFTESTMODE = 58,

            /** Command Toggle LTE Modem */
            E_TAG_NUMBER_COMMAND_TOGGLELTEMODEM = 59,

            /** R900 - OOK*/
            E_TAG_NUMBER_R900_READINGS_OOK = 60,

            /** R900 - FSK*/
            E_TAG_NUMBER_R900_READINGS_FSK = 61,

            /** Battery V */
            E_TAG_NUMBER_BATTERY_VOLTAGE = 62,

            /** CMIU Temperature */
            E_TAG_NUMBER_CMIU_TEMPERATURE = 63,

            /** Memory Image*/
            E_TAG_NUMBER_MEMORY_IMAGE = 64,

            /** Packet Type */
            E_TAG_NUMBER_PACKET_TYPE = 65,

            /** Destination */
            E_TAG_NUMBER_DESTINATION = 66,

            /** Meta data for packet request command */
            E_TAG_NUMBER_META_DATA_PACKET_REQUEST = 67,


            /** CMIU ID */
            E_TAG_NUMBER_CMIU_ID = 68,

            /** Cellular APN */
            E_TAG_NUMBER_CMIU_APN = 69,
            /** Command BTLE RF Test Mode */
            E_TAG_NUMBER_COMMAND_BTLE_RFTESTMODE = 70,

            /** New image version info for Modem FOTA update */
            E_TAG_NUMBER_MODEM_FOTA_VERSION = 71,

            /** Packet Instigator */
            E_TAG_NUMBER_PACKET_INSTIGATOR = 72,

            /** Tag used to allow extended tag numbers greater than 255 */
            E_TAG_NUMBER_EXTENSION_256 = 250,
            /** Indicates a block of encrypted data */
            E_TAG_NUMBER_SECURE_DATA = 254,
            /** Indicates the end of a packet */
            E_TAG_NUMBER_EOF = 255,

            E_TAG_NUMBER_COUNT
        }



        /**
        * Enum of the possible types of data in a tag  (must be <= 255) 
        */
        public enum TagType
        {
            /** No tag type, such as the EOF tag */
            E_TAG_TYPE_NONE = 0x00,
            /** A Uint8 value follows */
            E_TAG_TYPE_BYTE = 0x01,
            /** A Uint16 value follows */
            E_TAG_TYPE_U16 = 0x02,
            /** A Uint32 value follows */
            E_TAG_TYPE_U32 = 0x03,
            /** A Uint64 value follows */
            E_TAG_TYPE_U64 = 0x04,
            /** A struct based on an array of Uint8 values follows */
            E_TAG_TYPE_BYTE_RECORD = 0x05,
            /** An array of Uint16 values follows */
            E_TAG_TYPE_U16_ARRAY = 0x06,
            /** An array of Uint32 values follows */
            E_TAG_TYPE_U32_ARRAY = 0x07,
            /** An array of chars follows */
            E_TAG_TYPE_CHAR_ARRAY = 0x08,
            /** A struct based on an extended array of Uint8 values follows */
            E_TAG_TYPE_EXTENDED_BYTE_RECORD = 0x09,
            /** An extended array of Uint16 values follows */
            E_TAG_TYPE_EXTENDED_U16_ARRAY = 0x0A,
            /** An extended array of Uint32 values follows */
            E_TAG_TYPE_EXTENDED_U32_ARRAY = 0x0B,
            /** An extended array of chars follows */
            E_TAG_TYPE_EXTENDED_CHAR_ARRAY = 0x0C,
            /** An extended array of secure encrypted data follows */
            E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY = 0x0D,
            /** 10 bytes of image version info follows -
            major (1 byte), minor (1 byte), date (4 bytes), rev (4 bytes) as big-endian BCD */
            E_TAG_TYPE_IMAGE_VERSION_INFO = 0x0E,
            /** An array of Uint8 values follows */
            E_TAG_TYPE_U8_ARRAY = 0x0F,
            /** An extended array of Uint8 values follows */
            E_TAG_TYPE_EXTENDED_U8_ARRAY = 0x10,
            /** An array of 16-byte secure encrypted data blocks follows */
            E_TAG_TYPE_SECURE_BLOCK_ARRAY = 0x11,
            E_TAG_TYPE_COUNT
        }

        /**
        Error type IDs used in tag 51
        */
        public enum E_ERROR_TAG
        {
            /** 0 - No Error */
            E_ERROR_NONE                    =0x00,
            /** 1 - Invalid Command*/
            E_ERROR_INVALID_CMD             =0x01
        }

        /**
        Packet Instigator used in tag 72
        */
        public enum E_PACKET_INSTIGATOR
        {
            /** 0 - Regularly Scheduled Packet */
            E_PACKET_INSTIGATOR_NORMAL = 0x00,
            /** 1 - Connection Vaildation Station (CVS) */
            E_PACKET_INSTIGATOR_CVS = 0x01,
            /** 2 - Final Functional Test Station (FFTS) */
            E_PACKET_INSTIGATOR_FFTS = 0x02
        }

        [StructLayout(LayoutKind.Sequential)]
        public abstract class Tag
        {

        }

        /// <summary>
        /// Tag 69 - CMIU APN
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class CMIUAPN : Tag
        {
            public String cmiuAPN;
        }

        /// <summary>
        /// Tag 9 - FTP DNS
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class FTPDNS : Tag
        {
            public String ftpDNS;
        }

        /// <summary>
        /// Tag 10 - FTP Port Number
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class FTPPortNumber : Tag
        {
            public UInt16 ftpPortNumber;
        }

        /// <summary>
        /// Tag 11 - FTP Username
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class FTPUsername : Tag
        {
            public String ftpUsername;
        }

        /// <summary>
        /// Tag 12 - FTP Password
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class FTPPassword : Tag
        {
            public String ftpPassword;
        }

        /// <summary>
        /// Tag 46 - Image Filename
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class ImageFilename : Tag
        {
            public String imageFilename;
        }

        /// <summary>
        /// Tag 71 - New Image Version Info
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class NewImageVersionInfo : Tag
        {
            public String newImageVerionInfo;
        }

        /// <summary>
        /// Tag 35 - MQTT BLE UART
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class MqttBleUart : Tag
        {
            public Byte sequenceNumber;
            public Byte keyInfo;
            public Byte encryptionMethod;
            public Byte token;
        }

        /// <summary>
        /// Tag 47 - Image meta data
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class ImageMetadata : Tag
        {
            public Byte imageType;
            public UInt32 startAddress;
            public UInt32 imageSize;
        }


        /// <summary>
        /// Tag 14 - Image version
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class ImageVersionInfo : Tag
        {
            public Byte versionMajorBcd;
            public Byte versionMinorBcd;
            public UInt32 versionYearMonthDayBcd;
            public UInt32 versionBuildBcd;
        }

        /// <summary>
        /// Tag 1 - CMIU Packet Header payload to packer
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class PackerHeader : Tag
        {
            public UInt32 cmiuId;
            public Byte sequenceNumber;
            public Byte keyInfo;
            public Byte encryptionMethod;
            public Byte tokenAesCrc;
            public Byte networkFlags;
            public UInt16 cellularRssiAndBer;
            public UInt64 timeAndDate;
        }


        /// <summary>
        /// CMIU Configuration (tag num 2)
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class CMIUConfigurarion : Tag
        {
            public Byte cmiuType;
            public UInt16 hwRevision;

            public UInt16 fwRelease;
            public UInt32 fwRevDate;
            public UInt32 fwBuildNum;

            public UInt16 blRelease;
            public UInt32 blRevDate;
            public UInt32 blBuildNum;

            public UInt16 configRelease;
            public UInt32 configRevDate;
            public UInt32 configBuildNum;

            public UInt64 manufactureDate;
            public UInt16 initialCallInTime;
            public UInt16 callInInterval;
            public UInt16 callInWindow;
            public Byte reportingwindowNRetries;

            public UInt16 quietTimeStartMins;
            public UInt16 quietTimeEndMins;

            public Byte numAttachedDevices;
            public UInt64 installationDate;
            public UInt64 dateLastMagSwipe;
            public Byte magSwipes;
            public UInt16 batteryRemaining;
            public Byte timeErrorLastNetworkTimeAccess;
        }


        /// <summary>
        /// CMIU Diagnostics (tag number 23)
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class CMIUDiagnostics : Tag
        {
            public UInt32 diagnosticsResult;
            public Byte processorResetCounter;
        }

        /// <summary>
        /// Status flags (tag number 26)
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class CMIUStatus : Tag
        {
            public Byte cmiuType;
            public Byte numberOfDevices;
            public UInt16 cmiuFlags;
        }

        /// <summary>
        /// Reported Device Config (tag number 27)
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class ReportedDeviceConfig : Tag
        {
            public Byte deviceNumber;
            public UInt64 attachedDeviceId;
            public UInt16 deviceType;
            public UInt32 currentDeviceData;
            public UInt16 deviceFlags;
        }

        /// <summary>
        /// R900 Interval Data and flags (tag number 28)
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class R900IntervalData : Tag
        {
            /** This will have to be manually initialized when it is used. */
            public UInt32[] record;
        }


        /// <summary>
        /// connection timing log (tag num 31)
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class ConnectionTimingLog : Tag
        {
            public UInt64 dateOfConnection;
            public UInt16 timeFromPowerOnToNetworkRegistration;
            public UInt16 timeFromNetworkRegistrationToContextActivation;
            public UInt16 timeFromContextActivationToServerConnection;
            public UInt16 timeToTransferMessageToServer;
            public UInt16 timeToDisconnectAndShutdown;
        }

        /// <summary>
        /// CMIU Basic Configuration payload to packer (tag num 33)
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class CMIUBasicConfiguration : Tag
        {
            public UInt16 initialCallInTime;
            public Byte callInInterval;
            public Byte callInWindow;
            public Byte reportingwindowNRetries;
            public UInt16 quietTimeStartMins;
            public UInt16 quietTimeEndMins;
            public Byte numAttachedDevices;
            public Byte eventMask;
        }


        /// <summary>
        /// Interval Recording Data (tag number 34)
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class IntervalRecordingData : Tag
        {
            public Byte deviceNumber;
            public Byte intervalFormat;
            public Byte recordingIntervalMin;
            public Byte reportingIntervalHrs;
            public UInt16 intervalStartTime;
            public UInt64 intervalLastRecordDateTime;

        }

        /// <summary>
        /// Recording Reporting Interval (tag num 48)
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class RecordingReportingInterval : Tag
        {
            public UInt16 reportingStartMins;
            public Byte reportingIntervalHours;
            public Byte reportingRetries;
            public Byte reportingTransmitWindowsMins;
            public UInt16 reportingQuietStartMins;
            public UInt16 reportingQuietEndMins;
            public UInt16 recordingStartMins;
            public Byte recordingIntervalMins;
            public Byte numberAttachedDevices;
            public Byte eventMask;
        }



        //////////////////////
        //
        // Version Info
        //
        //////////////////////

        /// <summary>
        /// Struct for E_TAG_TYPE_IMAGE_VERSION_INFO (tag *type* 14), tag numbers 38-43, 49
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class VersionInfo : Tag
        {
            /** Major version as binary coded decimal - e.g. version fifteen = 0x15 */
            public Byte versionMajorBcd;

            /** Minor version as binary coded decimal - e.g. version sixteen = 0x16 */
            public Byte versionMinorBcd;    

            /** Date as binary coded decimal - e.g. 0x150619 for June 19, 2015 */
            public UInt32 versionYearMonthDayBcd;

            /** Build number as binary coded decimal - e.g. 0x1234 for one thousand two hundred thirty four*/
            public UInt32 versionBuildBcd;

        }

        //////////////////////
        //
        // Command Related
        //
        //////////////////////



        /// <summary>
        /// Tag 53
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class CommandStartDelay : Tag
        {
            //[DisplayName("Start Delay (ms)"), MaxValue(5000)]
            //public UInt64 startDelay { get; set; }
            public UInt64 startDelay;
        }

        /// <summary>
        /// Tag 54
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class CommandDuration : Tag
        {
            //[DisplayName("DURATION")]
            public UInt64 duration;
        }

        /// <summary>
        /// Tag *type* 55
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class ReadConnectedDevice : Tag
        {
            public Byte pulse;
            public UInt64 delayBetweenReads;
            public UInt64 numberOfRepeats;
        }

        /// <summary>
        /// Tag *type* 56
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class SetPowerMode : Tag
        {
            public Byte powerMode;
        }

        /// <summary>
        /// Tag *type* 57
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class ReadMemory : Tag
        {
            public UInt16 startAddress;
            public UInt16 numberOfBytes;
        }

        /// <summary>
        /// Tag *type* 58
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class RFTestMode : Tag
        {
            public Byte rfBand;
            public UInt16 rfChannel;
            public Byte rfPowerLvel;
        }

        /// <summary>
        /// Tag *type* 59
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class ToggleModem : Tag
        {
            public Byte modemStatus;
        }

        /// <summary>
        /// Tag 64
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class EraseMemoryImage : Tag
        {
            public UInt16 memoryImageBitmask;
        }

        /// <summary>
        /// Tag 65
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class PublishPacketType : Tag
        {
            public Byte packetType;
        }

        /// <summary>
        /// Tag 66
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class PublishPacketDestination : Tag
        {
            public Byte destination;
        }

        /// <summary>
        /// Tag *type* 70
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class BLE_RFTestMode : Tag
        {
            public Byte rfCommand;
            public Byte rfFrequency;
            public Byte rfPacketLength;
            public Byte rfPacketType;
        }

        /// <summary>
        /// Tag 72
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class PacketInstigator : Tag
        {
            public E_PACKET_INSTIGATOR packetInstigator;
        }
    }
}
