﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSTagTypes
{
    [System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Property)]
    public class MaxValueAttribute : System.Attribute
    {
        public UInt64 maxValue;

        public MaxValueAttribute(UInt64 max)
        {
            maxValue = max;
        }
    }
}
