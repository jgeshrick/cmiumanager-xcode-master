﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTagTypes
{
    public class Packets
    {
        /// <summary>
        /// Packet types
        /// </summary>
        public enum PacketTypeId
        {
            /** A detailed config packet */
            E_PACKET_TYPE_DETAILED_CONFIG = 0,
            /** Regular meter reading data packet */
            E_PACKET_TYPE_INTERVAL_DATA = 1,
            /** A basic config packet */
            E_PACKET_TYPE_BASIC_CONFIG = 2,
            /** This packet indicates an event has occured */
            E_PACKET_TYPE_EVENT = 3,
            /** This packet is used to send the time */
            E_PACKET_TYPE_TIME = 4,
            /** A command packet */
            E_PACKET_TYPE_COMMAND = 5,
            /** A response packet */
            E_PACKET_TYPE_RESPONSE = 6,

            E_PACKET_TYPE_COUNT
        }

        


        /// <summary>
        /// Response Packet
        /// </summary>
        public abstract class ResponsePacket
        {
            public PacketTypeId packetType;
            public Tags.PackerHeader cmiuPacketHeader;
        }

        /// <summary>
        /// Command Response Packet
        /// </summary>
        public class CommandResponsePacket : ResponsePacket
        {
            public CommandData.CommandIds command;
            public Tags.E_ERROR_TAG error;
        }

        /// <summary>
        /// Detailed Config Packet 
        /// </summary>
        public class DetailedConfigPacket : ResponsePacket
        {
            public Tags.CMIUStatus status;
            public Tags.CMIUConfigurarion config;
            public Tags.IntervalRecordingData intervalRecordingData;
            public Tags.CMIUDiagnostics diagnostics;
            public Tags.ReportedDeviceConfig reportedConfig;
            public Tags.ConnectionTimingLog timingLog;
            public Tags.ImageVersionInfo firmwareRevision;
            public Tags.ImageVersionInfo bootloaderRevision;
            public Tags.ImageVersionInfo configRevision;
            public String networkOperatorStatus;
            public String networkPerformance;
            public String registrationStatus;
            public String mqttBrokerIP;
            public String assignedCMIUIP;
            public String modemHWRevision;
            public String modemModelIDCode;
            public String modemManufIDCode;
            public String modemSWRevisionNumber;
            public String modemSerialNumber;
            public String simIMSI;
            public String simCARDID;
        }

        /// <summary>
        /// Interval Data Packet
        /// </summary>
        public class IntervalDataPacket : ResponsePacket
        {
            public Tags.CMIUStatus status;
            public Tags.ReportedDeviceConfig reportedConfig;
            public Tags.IntervalRecordingData intervalRecordingData;
            public Tags.R900IntervalData r900IntervalData;
        }

        /// <summary>
        /// CAN+ Data Response
        /// </summary>
        public class CANDataResponsePacket : CommandResponsePacket
        {
            public UInt32 cmiuID;
            public String IMEI;
            public String ICCID;
            public String MSISDN;
            public String IMSI;
            public String APN;            
        }

        /// <summary>
        /// Read Connected Devices Response
        /// </summary>
        public class ReadConnectedDevicesResponsePacket : CommandResponsePacket
        {
            public Tags.ReportedDeviceConfig reportedDeviceConfig;
        }

        /// <summary>
        /// Get APN Response
        /// </summary>
        public class APNGetResponsePacket : CommandResponsePacket
        {
            public Tags.CMIUAPN reportedAPN;
        }
    }
}
