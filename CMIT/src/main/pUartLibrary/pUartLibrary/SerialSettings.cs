﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace pUartLibrary
{
    static class SerialSettings
    {
        public const int BaudRate = 230400;
        public const System.IO.Ports.Parity Parity = System.IO.Ports.Parity.None;
        public const int DataBits = 8;
        public const System.IO.Ports.StopBits StopBits = System.IO.Ports.StopBits.One;
        public const int TimeoutMS = 500;
    }
}
