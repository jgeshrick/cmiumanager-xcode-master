﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Diagnostics;
using UartDataController;

namespace pUartLibrary
{
    /// <summary>
    /// Provides data for the DataReceived event.
    /// </summary>
    public class SerialDataEventArgs : EventArgs
    {
        public string Message { get; set; }

        public SerialDataEventArgs(string message)
        {
            Message = message;
        }
    }

    /// <summary>
    /// Library for handling the physical serial port communications
    /// </summary>
    public class pSerialUart
    {
        UartDataController.UartData uartController;
        private static string[] serialPorts;
        private SerialPort serialPort;
        private string serialPortMessage;
        private List<byte> serialData = new List<byte>();
        private const int maxPacketLength = 20;

        public delegate void OnMsgHandler(UarcMessage m);
        public event OnMsgHandler MessageReceived;

        public delegate void OnSendFailHandler();
        public event OnSendFailHandler MessageSendFailed;


        #region Public Events
        public EventHandler<SerialDataEventArgs> OnSerialDataEvent;
        public event EventHandler OnError_OpenPort;
        public event EventHandler OnError_SendData;
        #endregion

        #region Public Functions

        public pSerialUart()
        {
            uartController = new UartData();
            uartController.Init();
            uartController.MessageReceived += OnUarcMessageReceived;
            uartController.MessageFailed += OnSendFail;

            // Init the Uart Controller interface
            //UartDataController.UartData.Init();
            //UartDataController.UartData.MessageReceived += OnUarcMessageReceived;
            //UartDataController.UartData.MessageFailed += OnSendFail;
        }
        /// <summary>
        /// Function to get the list of available serial ports. This can be called to get an updated listing
        /// </summary>
        /// <returns>string[] - An array of strings, each string contains the name of the port</returns>
        public string[] GetAvailableSerialPorts()
        {
            serialPorts = SerialPort.GetPortNames();
            return serialPorts;
        }

        /// <summary>
        /// Functino to connect to a serial port that was returned in the list
        /// </summary>
        /// <param name="portName"></param>
        /// <returns></returns>
        public bool OpenSerialPortWithName(string portName)
        {
            GetAvailableSerialPorts();
            if(serialPorts.Contains(portName))
            {
                return OpenSerialPort(portName);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Function to close the serial port that was opened
        /// </summary>
        public void CloseSerialPort()
        {
            if (serialPort == null)
            {
                return;
            }

            serialPort.DataReceived -= serialPort_DataReceived;
            if(serialPort.IsOpen)
            {
                serialPort.Close();
            }
        }

        /// <summary>
        /// Function to return if the serial port is open
        /// </summary>
        /// <returns></returns>
        public bool SerialPortIsOpen()
        {
            if(serialPort != null)
            {
                return serialPort.IsOpen;
            }

            return false;
            
        }

        public void SendPingPacket()
        {
            byte[] payload = System.Text.Encoding.ASCII.GetBytes("PING");
            Byte[] bytesToSend = uartController.MakePacket(UarcMessageType.UARC_MT_PING, payload);
            StartSendData(bytesToSend);

        }

        public bool SendCmdPacket(Byte[] payload, UInt32 timeoutMs)
        {
            return SendPacket(UarcMessageType.UARC_MT_CMD, payload);
        }

        #endregion

        #region Private Functions
        private bool OpenSerialPort(string portName)
        {
            if (serialPort != null && serialPort.IsOpen)
                serialPort.Close();

            try
            {
                serialPort = new SerialPort();
                serialPort.PortName = portName;
                serialPort.BaudRate = SerialSettings.BaudRate;
                serialPort.Parity = SerialSettings.Parity;
                serialPort.DataBits = SerialSettings.DataBits;
                serialPort.StopBits = SerialSettings.StopBits;
                serialPort.ReadTimeout = SerialSettings.TimeoutMS;

                serialPort.DataReceived += serialPort_DataReceived;
                
                serialPort.Open();
            }
            catch (Exception ex)
            {
                Trace.WriteLine("[pUart] ERROR opening serial port: " + ex.Message);
                OnError_OpenPort(this, EventArgs.Empty);
                return false;
            }

            return true;
        }

        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Read all available bytes
            while(serialPort.BytesToRead > 0)
            {
                SerialDataHandler((byte)serialPort.ReadByte());
            }
        }

        private void SerialDataHandler(byte by)
        {
            // Add to the uart controller
            uartController.OnCharReceived(by);
        }

        /// <summary>
        /// Throw the rx msg up the stack
        /// </summary>
        /// <param name="m">The received message</param>
        void OnUarcMessageReceived(UarcMessage m)
        {
            var h = MessageReceived;
            if (h != null)
            {
                h(m);
            }
        }

        void OnSendFail()
        {
            var h = MessageSendFailed;
            if (h != null)
            {
                h();
            }
        }

        /// <summary>
        /// Are we able to send a packet? 
        /// </summary>
        /// <returns>true if ready to send, otherwise busy. Note: a previous timeout is not a hindrance to sending</returns>
        private bool CanSend()
        {
            UInt32 state = uartController.UartControllerDllGetSendState();

            return ((UInt32)E_UARC_MSG_SEND_STATE.UARC_MSS_BUSY != state);
        }



        private bool SendPacket(UarcMessageType msgType, Byte[] payload)
        {
            bool canSend = CanSend();

            if (canSend)
            {
                Byte[] bytesToSend = uartController.MakePacket(msgType, payload);

                this.StartSendData(bytesToSend);
                uartController.StartSendTimer(100);
            }

            return canSend;
        }


        private void StartSendData(byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException();
            }

            string dataToSend = String.Join(String.Empty, Array.ConvertAll(data, x => (x.ToString("X2") + " ")));
            OnSerialDataEvent(this, new SerialDataEventArgs(dataToSend));
            //Trace.Write(dataToSend);

            IList<byte[]> splitData = SplitDataNoCounter(data, maxPacketLength);

            List<byte> commandData = new List<byte>();
            foreach(byte[] packet in splitData)
            {
                commandData.AddRange(packet);
            }

            serialPort.Write(commandData.ToArray(), 0, commandData.Count);

            //sendData = true;

            /* Starting a task to perform the sending of packets asynchronouly.
             * The SendDataCompleted event will notify the application when ready. */
            //Task.Factory.StartNew(() =>
            //{
            //    try
            //    {
            //        int numberOfPackets = splitData.Count;

            //        for (int i = 0; i < numberOfPackets; i++)
            //        {                        
            //            /* Send one packet of data on the serial port */
            //            serialPort.Write(splitData[i], 0, splitData.Count());                                                
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        //AddToLog("Sending of data failed.");
            //        Trace.WriteLine(ex.ToString());
            //        //SendDataFailed(this, EventArgs.Empty);
            //        return;
            //    }

            //});
        }

        /// <summary>
        /// Split to max length packets without any pre-count --DEW
        /// </summary>
        private IList<byte[]> SplitDataNoCounter(byte[] data, int partSize)
        {
            /* Collection of packets split out from source array. */
            IList<byte[]> packets = new List<byte[]>();

            /* Last index where a packet of full length may start. */
            /* Note const and readonly won't work here since const is compile-time const and */
            /* readonly requires this to be a member. Since SplitDataAndAddCounter is called multiple*/
            /* times, it breaks the readonly usage as well. */
            int lastFullPacketIndex = (data.Length - partSize);

            /* Current index in source array. */
            int index;

            for (index = 0; index < lastFullPacketIndex; index += partSize)
            {
                byte[] packet = new byte[maxPacketLength];
                Array.Copy(data, index, packet, 0, partSize);

                packets.Add(packet);
            }

            /* Special treatment of last packet. */
            int lastPacketPayloadSize = (data.Length - index);
            byte[] lastPacket = new byte[maxPacketLength];
            Array.Copy(data, index, lastPacket, 0, lastPacketPayloadSize);

            packets.Add(lastPacket);

            return packets;
        }

       
        #endregion

    }
}
