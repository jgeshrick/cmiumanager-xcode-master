﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using CSTagTypes;

namespace PacketHandlerLibrary
{
    internal class TPBPHelpers
    {
        static GCHandle ghPack;
        static GCHandle ghParse;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packer"></param>
        /// <param name=" "></param>
        /// <returns></returns>
        public bool AddSecureDataBlock(PacketPacker packer, Byte[] data, UInt32 bufferSize)
        {
            //  Byte[] fileNameBytes = System.Text.Encoding.ASCII.GetBytes(fileName);
            GCHandle ghz;
            ghz = GCHandle.Alloc(data, GCHandleType.Pinned);

            return TPBP.Tpbp_PackerAdd_SecureBlockArray(packer, ghz.AddrOfPinnedObject(), bufferSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packer"></param>
        /// <param name="buffer"></param>
        /// <param name="bufferSize"></param>
        public void PackInit(PacketPacker packer, Byte[] buffer, UInt32 bufferSize)
        {
            // Fix the buffer for use in the DLL
            ghPack = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            // Get a pointer suitable for DLL
            IntPtr pBuf = ghPack.AddrOfPinnedObject();
            TPBP.Tpbp_PackerInit(packer, pBuf, bufferSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packer"></param>
        /// <param name="buffer"></param>
        /// <param name="bufferSize"></param>
        public void ParseInit(PacketParser parser, Byte[] buffer, UInt32 bufferSize)
        {
            // Fix the buffer for use in the DLL
            ghParse = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            // Get a pointer suitable for DLL
            IntPtr pBuf = ghParse.AddrOfPinnedObject();
            TPBP.Tpbp_ParserInit(parser, pBuf, bufferSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packer"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool AddImageFileName(PacketPacker packer, String fileName)
        {
            Byte[] fileNameBytes = System.Text.Encoding.ASCII.GetBytes(fileName);
            GCHandle ghz;
            ghz = GCHandle.Alloc(fileNameBytes, GCHandleType.Pinned);

            return TPBP.Tpbp_PackerAdd_ImageName(packer, ghz.AddrOfPinnedObject());
        }

        public bool AddNewImageFileName(PacketPacker packer, String fileName)
        {
            Byte[] fileNameBytes = System.Text.Encoding.ASCII.GetBytes(fileName);
            GCHandle ghz;
            ghz = GCHandle.Alloc(fileNameBytes, GCHandleType.Pinned);

            return TPBP.Tpbp_PackerAdd_NewImageVersion(packer, ghz.AddrOfPinnedObject());
        }


        public bool AddString(PacketPacker packer, CSTagTypes.Tags.TagNumber tag, String str)
        {
            Byte[] strBytes = System.Text.Encoding.ASCII.GetBytes(str);
            GCHandle ghz;
            ghz = GCHandle.Alloc(strBytes, GCHandleType.Pinned);

            return TPBP.Tpbp_PackerAdd_CharArray(packer, tag, ghz.AddrOfPinnedObject());
        }


        public bool AddFTPPortNumber(PacketPacker packer, CSTagTypes.Tags.FTPPortNumber ftpPortNum)
        {
            return TPBP.Tpbp_PackerAdd_FtpPortNumber(packer, ftpPortNum.ftpPortNumber);
        }

        public bool AddImageMetadata(PacketPacker packer, CSTagTypes.Tags.ImageMetadata imageMetadata)
        {
            return TPBP.Tpbp_PackerAdd_ImageMetadata(packer, imageMetadata);
        }

        public bool AddPacketId(PacketPacker packer, CSTagTypes.Packets.PacketTypeId id)
        {
            return TPBP.Tpbp_Packer_BeginPacket(packer, id);
        }

        public bool AddUartPacketHeader(PacketPacker packer, CSTagTypes.Tags.MqttBleUart mqttBleUartHeader)
        {
            return TPBP.Tpbp_PackerAdd_UartPacketHeader(packer, mqttBleUartHeader);
        }

        public bool AddCommand(PacketPacker packer, CSTagTypes.CommandData.CommandIds command)
        {
            return TPBP.Tpbp_PackerAdd_Command(packer, (Byte)command);
        }

        public bool AddFirmwareRevision(PacketPacker packer, CSTagTypes.Tags.ImageVersionInfo imageVersionInfo)
        {
            return TPBP.Tpbp_PackerAdd_ImageVersionInfoFirmware(packer, imageVersionInfo);
        }

        public bool AddTimeAndDate(PacketPacker packer, UInt64 timeDate)
        {
            return TPBP.Tpbp_PackerAdd_TimeAndDate(packer, timeDate);
        }

        public bool AddReadConnectedDevicesTag(PacketPacker packer, CSTagTypes.Tags.ReadConnectedDevice readDevice)
        {
            return TPBP.Tpbp_PackerAdd_Command_ReadConnectedDevice(packer, readDevice);
        }

        public bool AddPowerModeTag(PacketPacker packer, CSTagTypes.Tags.SetPowerMode powerMode)
        {
            return TPBP.Tpbp_PackerAdd_Command_PowerMode(packer, powerMode.powerMode);
        }

        public bool AddReadMemoryTag(PacketPacker packer, CSTagTypes.Tags.ReadMemory memoryData)
        {
            return TPBP.Tpbp_PackerAdd_Command_ReadMemory(packer, memoryData);
        }

        public bool AddRFTestModeTag(PacketPacker packer, CSTagTypes.Tags.RFTestMode rfTestMode)
        {
            return TPBP.Tpbp_PackerAdd_Command_RFTestMode(packer, rfTestMode);
        }

        public bool AddBleRFTestModeTag(PacketPacker packer, CSTagTypes.Tags.BLE_RFTestMode rfTestMode)
        {
            return TPBP.Tpbp_PackerAdd_Command_Btle_RFTestMode(packer, rfTestMode);
        }


        public bool AddToggleLTEModemTag(PacketPacker packer, CSTagTypes.Tags.ToggleModem modemStatus)
        {
            return TPBP.Tpbp_PackerAdd_Command_ToggleLTEModem(packer, modemStatus.modemStatus);
        }

        public bool AddStartDelayTag(PacketPacker packer, CSTagTypes.Tags.CommandStartDelay startDelay)
        {
            return TPBP.Tpbp_PackerAdd_Command_StartDelay(packer, startDelay.startDelay);
        }

        public bool AddDurationTag(PacketPacker packer, CSTagTypes.Tags.CommandDuration duration)
        {
            return TPBP.Tpbp_PackerAdd_Command_Duration(packer, duration.duration);
        }

        public bool AddPacketTypeTag(PacketPacker packer, CSTagTypes.Tags.PublishPacketType packetTypeTag)
        {
            return TPBP.Tpbp_PackerAdd_PacketTypeTag(packer, packetTypeTag.packetType);
        }

        public bool AddMemoryImageTag(PacketPacker packer, CSTagTypes.Tags.EraseMemoryImage memoryImage)
        {
            return TPBP.Tpbp_PackerAdd_MemoryImageTag(packer, memoryImage.memoryImageBitmask);
        }

        public bool AddPacketDestinationTag(PacketPacker packer, CSTagTypes.Tags.PublishPacketDestination destination)
        {
            return TPBP.Tpbp_PackerAdd_Destination(packer, destination.destination);
        }

        public bool AddReportingRecordingInterval(PacketPacker packer, CSTagTypes.Tags.RecordingReportingInterval intervals)
        {
            return TPBP.Tpbp_PackerAdd_RecordingReportingInterval(packer, intervals);
        }

        public bool AddPacketInstigator(PacketPacker packer, CSTagTypes.CommandData.CommandParameters_RunConnectivityTest packetInstigator)
        {
            return TPBP.Tpbp_PackerAdd_PacketInstigator(packer, (Byte)packetInstigator.packetInstigator.packetInstigator);
        }

        public bool AddEof(PacketPacker packer)
        {
            return TPBP.Tpbp_Packer_EndPacket(packer);
        }

        public bool PadZeroToBlockBoundary(PacketPacker packer)
        {
            return TPBP.Tpbp_PackerPadZeroToBlockBoundary(packer, 16);
        }

        public UInt32 GetPackerByteCount(PacketPacker packer)
        {
            return TPBP.Tpbp_PackerGetCount(packer);
        }

        public CSTagTypes.Packets.PacketTypeId GetPacketType(PacketParser parser)
        {
            return TPBP.Tpbp_ParserRead_PacketTypeId(parser);
        }

        public bool ReadTag(PacketParser parser, CSTagTypes.Tags.TPBPTag tag)
        {
            if (tag == null)
                return false;

            return TPBP.Tpbp_ParserReadTag(parser, tag);
        }

        public bool GetHeaderPacket(PacketParser parser, CSTagTypes.Tags.PackerHeader packetHeader)
        {
            if (packetHeader == null)
                return false;

            return TPBP.Tpbp_ParserRead_CmiuPacketHeader(parser, packetHeader);
        }

        public bool GetNumberOfBytes(PacketParser parser, byte[] buffer, UInt32 numberOfBytes)
        {
            return TPBP.Tpbp_ParserGetBytes(parser, buffer, numberOfBytes);
        }

        public bool FindTag(PacketParser parser, CSTagTypes.Tags.TagNumber tagNumber, CSTagTypes.Tags.TPBPTag tag)
        {
            if (tag == null)
                return false;

            bool status = TPBP.Tpbp_ParserFindTag(parser, tagNumber, tag);
            //parser.readPosition = 0;
            return status;
        }

        public bool GetString(PacketParser parser, CSTagTypes.Tags.TPBPTag tag, ref String output, UInt32 buffer)
        {
            StringBuilder str = new StringBuilder(1024);
            bool success = TPBP.Tpbp_ParserRead_String(parser, tag, str, 1024);
            output = RemoveAnyNewlines(str.ToString());
            return success;
        }

        public bool GetImageVersionInfo(PacketParser parser, CSTagTypes.Tags.ImageVersionInfo imageInfo)
        {
            return TPBP.Tpbp_ParserRead_ImageVersionInfo(parser, imageInfo);
        }

        public bool GetDiagnostics(PacketParser parser, CSTagTypes.Tags.CMIUDiagnostics diagnostics)
        {
            return TPBP.Tpbp_ParserRead_CmiuDiagnostics(parser, diagnostics);
        }

        public bool GetConfiguration(PacketParser parser, CSTagTypes.Tags.CMIUConfigurarion config)
        {
            return TPBP.Tpbp_ParserRead_CmiuConfiguration(parser, config);
        }

        public bool GetStatus(PacketParser parser, CSTagTypes.Tags.CMIUStatus status)
        {
            return TPBP.Tpbp_ParserRead_CmiuStatus(parser, status);
        }

        public bool GetTimingLog(PacketParser parser, CSTagTypes.Tags.ConnectionTimingLog log)
        {
            return TPBP.Tpbp_ParserRead_ConnectionTimingLog(parser, log);
        }

        public bool GetReportedDeviceConfig(PacketParser parser, CSTagTypes.Tags.ReportedDeviceConfig reportedDeviceConfig)
        {
            return TPBP.Tpbp_ParserRead_ReportedDeviceConfig(parser, reportedDeviceConfig);
        }

        public bool GetIntervalRecordingData(PacketParser parser, CSTagTypes.Tags.IntervalRecordingData intervalRecordingData)
        {
            return TPBP.Tpbp_ParserRead_IntervalRecordingConfig(parser, intervalRecordingData);
        }

        public bool GetR900IntervalData(PacketParser parser, CSTagTypes.Tags.R900IntervalData reportedR900IntervalData, UInt32 numBytes)
        {
            UInt32 arraySize = (numBytes/4);
            bool retval = false;
            byte[] buffer = new byte[numBytes];

            retval = TPBP.Tpbp_ParserGetBytes(parser, buffer, numBytes);

            /** If we didn't grab anything, there is no point in trying to populate the
             * record array. */
            if (retval)
            {
                /** Make sure the record array is initialized here as we do it nowhere else. */
                reportedR900IntervalData.record = new UInt32[arraySize];

                /** Grab each set of four bytes and shove them into the correct
                 *  record. */
                for (UInt32 index = 0; index < arraySize; ++index)
                {
                    UInt32 bufferIndex = index * 4;
                    reportedR900IntervalData.record[index] = (UInt32)((buffer[bufferIndex + 0] << 24) |
                                                                     (buffer[bufferIndex + 1] << 16) |
                                                                     (buffer[bufferIndex + 2] << 8) |
                                                                     (buffer[bufferIndex + 3]));
                }
            }

            return retval;
        }

        public bool GetCMIUID(PacketParser parser, ref UInt32 cmiuID)
        {
            return TPBP.Tpbp_ParserRead_CmiuId(parser, ref cmiuID);
        }


        




        //
        // Helper
        //
        private static string RemoveAnyNewlines(string str)
        {
            return str.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
        }
        
    }
}
