﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTagTypes;

namespace PacketHandlerLibrary
{
    public class BuildPacket
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 MakeFirmwareImagePacket(string filePath, Byte[] outputData)
        {
            CommandPacket_Build commandFWImage = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_UPDATE_IMAGE);

            CSTagTypes.Tags.ImageVersionInfo imageVersionInfo = new CSTagTypes.Tags.ImageVersionInfo();
            CSTagTypes.Tags.ImageMetadata imageMetadata = new CSTagTypes.Tags.ImageMetadata();

            imageVersionInfo.versionMajorBcd = 0;
            imageVersionInfo.versionMinorBcd = 1;
            imageVersionInfo.versionYearMonthDayBcd = 0x12341233;
            imageVersionInfo.versionBuildBcd = 0x05;

            imageMetadata.imageType = 0x03;
            imageMetadata.startAddress = 0x1234ABCD;
            imageMetadata.imageSize = 0x1000;

            String fileName = filePath;

            // Secure region
            commandFWImage.Tpbp.AddImageFileName(commandFWImage.securePacker, fileName);
            commandFWImage.Tpbp.AddImageMetadata(commandFWImage.securePacker, imageMetadata);
            commandFWImage.Tpbp.AddFirmwareRevision(commandFWImage.securePacker, imageVersionInfo);

            return commandFWImage.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_ERASE_MEMORY_IMAGE 
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_EraseMemoryImage(CSTagTypes.CommandData.CommandParameters_EraseMemoryImage erase, Byte[] outputData)
        {
            CommandPacket_Build commandEraseMemoryImage = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_ERASE_MEMORY_IMAGE);
            commandEraseMemoryImage.Tpbp.AddMemoryImageTag(commandEraseMemoryImage.securePacker, erase.memoryImageBitmaskTag);
            return commandEraseMemoryImage.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_SET_RECORDING_AND_REPORTING_INTERVALS 
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_SetRecordingReportingIntervals(CSTagTypes.CommandData.CommandParameters_SetRecordingReportingIntervals intervals, Byte[] outputData)
        {
            CommandPacket_Build commandSetRecordingReportingInt = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_SET_RECORDING_AND_REPORTING_INTERVALS);
            commandSetRecordingReportingInt.Tpbp.AddReportingRecordingInterval(commandSetRecordingReportingInt.securePacker, intervals.intervalsTag);
            return commandSetRecordingReportingInt.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_SLEEP_SECONDS 
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_SleepSeconds(CSTagTypes.CommandData.CommandParameters_SleepSeconds sleep, Byte[] outputData)
        {
            CommandPacket_Build commandSleepSeconds = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_SLEEP_SECONDS);
            commandSleepSeconds.Tpbp.AddStartDelayTag(commandSleepSeconds.securePacker, sleep.startDelayTag);
            commandSleepSeconds.Tpbp.AddDurationTag(commandSleepSeconds.securePacker, sleep.durationTag);
            return commandSleepSeconds.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_PUBLISH_DETAILED_CONFIGURATION (=11), to req a DCP
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_PublishRequestedPacket(CSTagTypes.CommandData.CommandParameters_PublishRequestedPacket packet, Byte[] outputData)
        {
            CommandPacket_Build commandRequestPublishDetailedConfigPacket = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_PUBLISH_REQESTED_PACKET);
            commandRequestPublishDetailedConfigPacket.Tpbp.AddPacketTypeTag(commandRequestPublishDetailedConfigPacket.securePacker, packet.packetTypeTag);
            commandRequestPublishDetailedConfigPacket.Tpbp.AddPacketDestinationTag(commandRequestPublishDetailedConfigPacket.securePacker, packet.destinationTag);
            return commandRequestPublishDetailedConfigPacket.GetCompleteCommandPacket(outputData);            
        }

        /// <summary>
        /// Send command E_COMMAND_READ_CONNECTED_DEVICES tag 53
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_ReadConnectedDevices(CSTagTypes.CommandData.CommandParameters_ReadConnectedDevices readDevice, Byte[] outputData)
        {            
            CommandPacket_Build commandReadConnectedDevice = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_READ_CONNECTED_DEVICES);
            commandReadConnectedDevice.Tpbp.AddStartDelayTag(commandReadConnectedDevice.securePacker, readDevice.startDelayTag);
            commandReadConnectedDevice.Tpbp.AddReadConnectedDevicesTag(commandReadConnectedDevice.securePacker, readDevice.readDeviceTag);
            return commandReadConnectedDevice.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_SELECT_POWER_MODE tag 56
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_SetPowerMode(CSTagTypes.CommandData.CommandParameters_SetPowerMode powerMode, Byte[] outputData)
        {
            CommandPacket_Build commandSetPowerMode = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_SELECT_POWER_MODE);
            commandSetPowerMode.Tpbp.AddStartDelayTag(commandSetPowerMode.securePacker, powerMode.startDelayTag);
            commandSetPowerMode.Tpbp.AddDurationTag(commandSetPowerMode.securePacker, powerMode.durationTag);
            commandSetPowerMode.Tpbp.AddPowerModeTag(commandSetPowerMode.securePacker, powerMode.powerModeBitMaskTag);           
            return commandSetPowerMode.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_READ_MEMORY tag 57
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_ReadMemory(CSTagTypes.CommandData.CommandParameters_ReadMemory readMemory, Byte[] outputData)
        {
            CommandPacket_Build commandReadMemory = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_READ_MEMORY);
            commandReadMemory.Tpbp.AddReadMemoryTag(commandReadMemory.securePacker, readMemory.readMemoryTag);
            return commandReadMemory.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_LTE_CARRIER_ASSERT tag 20
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_LTE_RFTestMode(CSTagTypes.CommandData.CommandParameters_LTECarrierAssert lteRFTest, Byte[] outputData)
        {
            CommandPacket_Build commandRFTestMode = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_LTE_CARRIER_ASSERT);
            commandRFTestMode.Tpbp.AddStartDelayTag(commandRFTestMode.securePacker, lteRFTest.startDelayTag);
            commandRFTestMode.Tpbp.AddDurationTag(commandRFTestMode.securePacker, lteRFTest.durationTag);
            commandRFTestMode.Tpbp.AddRFTestModeTag(commandRFTestMode.securePacker, lteRFTest.rfTestTag);            
            return commandRFTestMode.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_BTLE_CARRIER_ASSERT tag 25
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_BTLE_RFTestMode(CSTagTypes.CommandData.CommandParameters_BTLECarrierAssert blteRFTest, Byte[] outputData)
        {
            CommandPacket_Build commandRFTestMode = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_BTLE_CARRIER_ASSERT);
            commandRFTestMode.Tpbp.AddStartDelayTag(commandRFTestMode.securePacker, blteRFTest.startDelayTag);
            commandRFTestMode.Tpbp.AddDurationTag(commandRFTestMode.securePacker, blteRFTest.durationTag);
            commandRFTestMode.Tpbp.AddBleRFTestModeTag(commandRFTestMode.securePacker, blteRFTest.rfTestTag);
            return commandRFTestMode.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_GET_CAN_DATA 
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_GetCANData(Byte[] outputData)
        {
            CommandPacket_Build commandSleepSeconds = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_GET_CAN_DATA);
            return commandSleepSeconds.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_TOGGLE_LTE_MODEM tag 59
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_ToggleModem(CSTagTypes.CommandData.CommandParameters_ToggleLTEModem modem, Byte[] outputData)
        {
            CommandPacket_Build commandToggleModem = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_TOGGLE_LTE_MODEM);
            commandToggleModem.Tpbp.AddStartDelayTag(commandToggleModem.securePacker, modem.startDelayTag);
            commandToggleModem.Tpbp.AddDurationTag(commandToggleModem.securePacker, modem.durationTag);
            commandToggleModem.Tpbp.AddToggleLTEModemTag(commandToggleModem.securePacker, modem.modemTag);            
            return commandToggleModem.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_GET_DEBUG_LOG 
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_GetDebugLog(Byte[] outputData)
        {
            CommandPacket_Build commandGetDebugLog = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_GET_DEBUG_LOG);
            return commandGetDebugLog.GetCompleteCommandPacket(outputData);
        }


        /// <summary>
        /// Send command E_COMMAND_GET_DEBUG_LOG 
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_GetCMIUSignalQuality(Byte[] outputData)
        {
            CommandPacket_Build commandGetSignalQuality = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_GET_CMIU_SIGNAL_QUALITY);
            return commandGetSignalQuality.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_MODEM_FOTA tag 32
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_ModemFOTA(CSTagTypes.CommandData.CommandParameters_ModemFOTA fota, Byte[] outputData)
        {
            CommandPacket_Build commandModemFota = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_MODEM_FOTA);
            commandModemFota.Tpbp.AddString(commandModemFota.securePacker, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_FTP_DNS_ADDRESS, fota.ftpDNS.ftpDNS);
            commandModemFota.Tpbp.AddFTPPortNumber(commandModemFota.securePacker, fota.ftpPortNumber);
            commandModemFota.Tpbp.AddString(commandModemFota.securePacker, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_FTP_USERNAME, fota.ftpUsername.ftpUsername);
            commandModemFota.Tpbp.AddString(commandModemFota.securePacker, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_FTP_PASSWORD, fota.ftpPassword.ftpPassword);
            commandModemFota.Tpbp.AddImageFileName(commandModemFota.securePacker, fota.imageFilename.imageFilename);
            commandModemFota.Tpbp.AddNewImageFileName(commandModemFota.securePacker, fota.newImageVersion.newImageVerionInfo);
            return commandModemFota.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_APN_GET 
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_CMIUAPN_Get(Byte[] outputData)
        {
            CommandPacket_Build commandAPNGet = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_APN_GET);
            return commandAPNGet.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command E_COMMAND_APN_SET 
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_CMIUAPN_Set(CSTagTypes.CommandData.CommandParameters_APN_Set apn, Byte[] outputData)
        {
            CommandPacket_Build commandAPNGet = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_APN_SET);
            commandAPNGet.Tpbp.AddString(commandAPNGet.securePacker, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_CMIU_APN, apn.cmiuAPN.cmiuAPN);
            return commandAPNGet.GetCompleteCommandPacket(outputData);
        }

        /// <summary>
        /// Send command to E_COMMAND_RUN_CONNECTIVITY_TEST
        /// <summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public UInt32 Packet_Command_RunConnectivityTest(CSTagTypes.CommandData.CommandParameters_RunConnectivityTest packetInstigator, Byte[] outputData)
        {
            CommandPacket_Build commandRunConnectivityTest = new CommandPacket_Build(CSTagTypes.CommandData.CommandIds.E_COMMAND_RUN_CONNECTIVITY_TEST);
            commandRunConnectivityTest.Tpbp.AddPacketInstigator(commandRunConnectivityTest.securePacker, packetInstigator);
            return commandRunConnectivityTest.GetCompleteCommandPacket(outputData);
        }

    }
}
