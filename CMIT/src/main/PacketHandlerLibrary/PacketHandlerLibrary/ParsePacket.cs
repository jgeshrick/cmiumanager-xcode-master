﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTagTypes;

namespace PacketHandlerLibrary
{
    public class ParsePacket
    {
        // Need to return return some inherited class that could contain the different responses.
        public CSTagTypes.Packets.ResponsePacket ParseMessagePayload(byte[] messageData)
        {
            TPBPHelpers Tpbp = new TPBPHelpers();
            PacketParser parser = new PacketParser();            

            // Init the parser with the data received
            Tpbp.ParseInit(parser, messageData, (UInt32)messageData.Length);

            // Check the packet type
            CSTagTypes.Packets.PacketTypeId packetType = Tpbp.GetPacketType(parser);

            // Check the packet type
            if (packetType == CSTagTypes.Packets.PacketTypeId.E_PACKET_TYPE_RESPONSE)
            {
                return ParseResponse(Tpbp, parser);                
            }

            if(packetType == CSTagTypes.Packets.PacketTypeId.E_PACKET_TYPE_DETAILED_CONFIG)
            {
                return ParseDetailedConfig(Tpbp, parser);
            }

            if(packetType == CSTagTypes.Packets.PacketTypeId.E_PACKET_TYPE_INTERVAL_DATA)
            {
              return ParseIntervalData(Tpbp, parser);
            }

            return null;

        }


        private CSTagTypes.Packets.ResponsePacket ParseResponse(TPBPHelpers Tpbp, PacketParser parser)  
        {
            // Parse the response packet
            CSTagTypes.Packets.CommandResponsePacket responsePacket = new Packets.CommandResponsePacket();
            CSTagTypes.Tags.TPBPTag tag = new CSTagTypes.Tags.TPBPTag();

            // Set the packet type
            responsePacket.packetType = CSTagTypes.Packets.PacketTypeId.E_PACKET_TYPE_RESPONSE;

            // Get the Header
            if (Tpbp.FindTag(parser, Tags.TagNumber.E_TAG_NUMBER_CMIU_PACKET_HEADER, tag))
            {
                CSTagTypes.Tags.PackerHeader header = new CSTagTypes.Tags.PackerHeader();
                if (Tpbp.GetHeaderPacket(parser, header))
                {
                    responsePacket.cmiuPacketHeader = header;
                }
            }

            // Parse the secure data portion
            // Get Secure data
            if (Tpbp.FindTag(parser, Tags.TagNumber.E_TAG_NUMBER_SECURE_DATA, tag))
            {
                Byte[] secureDataBuffer = new Byte[2000];
                Tpbp.GetNumberOfBytes(parser, secureDataBuffer, tag.dataSize);
                Array.Resize(ref secureDataBuffer, (Int32)tag.dataSize);
                PacketParser secureParser = new PacketParser();
                Tpbp.ParseInit(secureParser, secureDataBuffer, (UInt32)secureDataBuffer.Length);

                // Get the command
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_COMMAND, tag))
                {
                    byte[] commandByte = new byte[tag.dataSize];
                    Tpbp.GetNumberOfBytes(secureParser, commandByte, tag.dataSize);
                    if (commandByte.Count() > 0)
                    {
                        responsePacket.command = (CSTagTypes.CommandData.CommandIds)commandByte[0];

                        // Create CAN Data Response
                        if(responsePacket.command == CommandData.CommandIds.E_COMMAND_GET_CAN_DATA)
                        {
                            return ParseCANDataResponse(Tpbp, secureParser, responsePacket);
                        }

                        if(responsePacket.command == CommandData.CommandIds.E_COMMAND_READ_CONNECTED_DEVICES)
                        {
                            return ParseReadConnectedDevicesResponse(Tpbp, secureParser, responsePacket);
                        }

                        if (responsePacket.command == CommandData.CommandIds.E_COMMAND_APN_GET)
                        {
                            return ParseAPNGetResponse(Tpbp, secureParser, responsePacket);
                        }
                    }

                }

                // Check for error
                GetError(responsePacket, Tpbp, secureParser);
            }


            return responsePacket;
        }


        /// <summary>
        /// Gets the error tag
        /// </summary>
        /// <param name="responsePacket"></param>
        /// <param name="Tpbp"></param>
        /// <param name="secureParser"></param>
        private void GetError(CSTagTypes.Packets.CommandResponsePacket responsePacket, TPBPHelpers Tpbp, PacketParser secureParser)
        {
            CSTagTypes.Tags.TPBPTag tag = new CSTagTypes.Tags.TPBPTag();
            if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_ERROR_CODE, tag))
                {
                    byte[] errorByte = new byte[tag.dataSize];
                    Tpbp.GetNumberOfBytes(secureParser, errorByte, tag.dataSize);
                    if (errorByte.Count() > 0)
                    {
                        responsePacket.error = (CSTagTypes.Tags.E_ERROR_TAG)errorByte[0];
                    }
                }
        }

        /// <summary>
        /// Parse the CAN Data Response
        /// </summary>
        /// <param name="Tpbp"></param>
        /// <param name="secureParser"></param>
        /// <param name="responsePacket"></param>
        /// <returns></returns>
        private CSTagTypes.Packets.CommandResponsePacket ParseCANDataResponse(TPBPHelpers Tpbp, PacketParser secureParser, CSTagTypes.Packets.CommandResponsePacket responsePacket)
        {
            CSTagTypes.Packets.CANDataResponsePacket canDataPacket = new Packets.CANDataResponsePacket();
            CSTagTypes.Tags.TPBPTag tag = new CSTagTypes.Tags.TPBPTag();
            canDataPacket.command = CommandData.CommandIds.E_COMMAND_GET_CAN_DATA;
            canDataPacket.packetType = responsePacket.packetType;
            canDataPacket.cmiuPacketHeader = responsePacket.cmiuPacketHeader;

            // ID
            if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_CMIU_ID, tag))
            {
                UInt32 cmiudID = 0;
                if (Tpbp.GetCMIUID(secureParser, ref cmiudID))
                {
                    canDataPacket.cmiuID = cmiudID;
                }

            }
            secureParser.readPosition = 0;

            // IMEI
            if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_IMEI, tag))
            {
                String str = "";
                if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                {
                    canDataPacket.IMEI = str;
                }

            }
            secureParser.readPosition = 0;

            // ICCID
            if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_SIM_CARD_ID, tag))
            {
                String str = "";
                if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                {
                    canDataPacket.ICCID = str;
                }

            }
            secureParser.readPosition = 0;

            // MSISDN
            if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_MSISDN_REQUEST, tag))
            {
                String str = "";
                if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                {
                    canDataPacket.MSISDN = str;
                }

            }
            secureParser.readPosition = 0;

            // IMSI
            if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_SIM_IMSI, tag))
            {
                String str = "";
                if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                {
                    canDataPacket.IMSI = str;
                }

            }
            secureParser.readPosition = 0;

            // APN
            if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_CMIU_APN, tag))
            {
                String str = "";
                if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                {
                    canDataPacket.APN = str;
                }

            }
            secureParser.readPosition = 0;

            // Check for error
            GetError(canDataPacket, Tpbp, secureParser);

            return canDataPacket;
        }

        /// <summary>
        /// Parse the payload for a detailed config packet
        /// </summary>
        /// <param name="Tpbp"></param>
        /// <param name="parser"></param>
        /// <returns></returns>
        private CSTagTypes.Packets.ResponsePacket ParseDetailedConfig(TPBPHelpers Tpbp, PacketParser parser)
        {
            // Parse detailed config packet
            CSTagTypes.Packets.DetailedConfigPacket configPacket = new Packets.DetailedConfigPacket();
            CSTagTypes.Tags.TPBPTag tag = new CSTagTypes.Tags.TPBPTag();

            configPacket.packetType = CSTagTypes.Packets.PacketTypeId.E_PACKET_TYPE_DETAILED_CONFIG;

            // Get the Header
            if (Tpbp.FindTag(parser, Tags.TagNumber.E_TAG_NUMBER_CMIU_PACKET_HEADER, tag))
            {
                CSTagTypes.Tags.PackerHeader header = new CSTagTypes.Tags.PackerHeader();
                if (Tpbp.GetHeaderPacket(parser, header))
                {
                    configPacket.cmiuPacketHeader = header;
                }
            }

            // Get Secure data
            if (Tpbp.FindTag(parser, Tags.TagNumber.E_TAG_NUMBER_SECURE_DATA, tag))
            {
                Byte[] secureDataBuffer = new Byte[2000];
                Tpbp.GetNumberOfBytes(parser, secureDataBuffer, tag.dataSize);
                Array.Resize(ref secureDataBuffer, (Int32)tag.dataSize);
                PacketParser secureParser = new PacketParser();
                Tpbp.ParseInit(secureParser, secureDataBuffer, (UInt32)secureDataBuffer.Length);



                // FW Rev 
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_FIRMWARE_REVISION, tag))
                {
                    CSTagTypes.Tags.ImageVersionInfo image = new Tags.ImageVersionInfo();
                    if (Tpbp.GetImageVersionInfo(secureParser, image))
                    {
                        configPacket.firmwareRevision = image;
                    }

                }
                secureParser.readPosition = 0;

                // BL Rev
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_BOOTLOADER_REVISION, tag))
                {
                    CSTagTypes.Tags.ImageVersionInfo image = new Tags.ImageVersionInfo();
                    if (Tpbp.GetImageVersionInfo(secureParser, image))
                    {
                        configPacket.bootloaderRevision = image;
                    }
                }
                secureParser.readPosition = 0;

                // Config Rev 
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_CONFIG_REVISION, tag))
                {
                    CSTagTypes.Tags.ImageVersionInfo image = new Tags.ImageVersionInfo();
                    if (Tpbp.GetImageVersionInfo(secureParser, image))
                    {
                        configPacket.configRevision = image;
                    }
                }
                secureParser.readPosition = 0;

                // Config  
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_CMIU_CONFIGURATION, tag))
                {
                    CSTagTypes.Tags.CMIUConfigurarion config = new Tags.CMIUConfigurarion();
                    if (Tpbp.GetConfiguration(secureParser, config))
                    {
                        configPacket.config = config;
                    }
                }
                secureParser.readPosition = 0;

                // Status  
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_CMIU_FLAGS, tag))
                {
                    CSTagTypes.Tags.CMIUStatus status = new Tags.CMIUStatus();
                    if (Tpbp.GetStatus(secureParser, status))
                    {
                        configPacket.status = status;
                    }
                }
                secureParser.readPosition = 0;

                // Network Operators
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_NETWORK_OPERATORS, tag))
                {
                    String str = "";
                    if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                    {
                        configPacket.networkOperatorStatus = str;
                    }

                }
                secureParser.readPosition = 0;

                // Network performance
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_NETWORK_PERFORMANCE, tag))
                {
                    String str = "";
                    if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                    {
                        configPacket.networkPerformance = str;
                    }
                }
                secureParser.readPosition = 0;

                // Registration status
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_REGISTRATION_STATUS, tag))
                {
                    String str = "";
                    if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                    {
                        configPacket.registrationStatus = str;
                    }
                }
                secureParser.readPosition = 0;

                // Assigned CMIU Address
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_ASSIGNED_CMIU_ADDRESS, tag))
                {
                    String str = "";
                    if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                    {
                        configPacket.assignedCMIUIP = str;
                    }
                }
                secureParser.readPosition = 0;

                // Modem HW Rev
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_MODEM_HARDWARE_REVISION, tag))
                {
                    String str = "";
                    if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                    {
                        configPacket.modemHWRevision = str;
                    }
                }
                secureParser.readPosition = 0;

                // Modem Model ID
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_MODEM_MODEL_IDENTIFICATION_CODE, tag))
                {
                    String str = "";
                    if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                    {
                        configPacket.modemModelIDCode = str;
                    }
                }
                secureParser.readPosition = 0;

                // Modem Manf ID
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_MODEM_MANUFACTURER_ID_CODE, tag))
                {
                    String str = "";
                    if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                    {
                        configPacket.modemManufIDCode = str;
                    }
                }
                secureParser.readPosition = 0;

                // Modem SW Rev
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_MODEM_SOFTWARE_REVISION, tag))
                {
                    String str = "";
                    if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                    {
                        configPacket.modemSWRevisionNumber = str;
                    }
                }
                secureParser.readPosition = 0;

                // IMEI
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_IMEI, tag))
                {
                    String str = "";
                    if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                    {
                        //configPacket.
                    }
                }
                secureParser.readPosition = 0;

                // IMSI
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_SIM_IMSI, tag))
                {
                    String str = "";
                    if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                    {
                        configPacket.simIMSI = str;
                    }
                }
                secureParser.readPosition = 0;

                // SIM ID
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_SIM_CARD_ID, tag))
                {
                    String str = "";
                    if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                    {
                        configPacket.simCARDID = str;
                    }
                }
                secureParser.readPosition = 0;

                // Diagnostics
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_CMIU_HW_DIAGNOSTICS, tag))
                {
                    CSTagTypes.Tags.CMIUDiagnostics diagnostics = new Tags.CMIUDiagnostics();
                    if (Tpbp.GetDiagnostics(secureParser, diagnostics))
                    {
                        configPacket.diagnostics = diagnostics;
                    }
                }
                secureParser.readPosition = 0;

                // User 
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_ID, tag))
                {
                    String str = "";
                    if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                    {
                        //configPacket.la = str;
                    }
                }
                secureParser.readPosition = 0;

                // Reported Device Config
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_DEVICE_CONFIGURATION, tag))
                {
                    CSTagTypes.Tags.ReportedDeviceConfig device = new Tags.ReportedDeviceConfig();
                    if (Tpbp.GetReportedDeviceConfig(secureParser, device))
                    {
                        configPacket.reportedConfig = device;
                    }

                }
                secureParser.readPosition = 0;

                // Interval Recording/Reporting 
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_INTERVAL_RECORDING, tag))
                {
                    CSTagTypes.Tags.IntervalRecordingData device = new Tags.IntervalRecordingData();
                    if (Tpbp.GetIntervalRecordingData(secureParser,device))
                    {
                        configPacket.intervalRecordingData = device;
                    }

                }
                secureParser.readPosition = 0;

                // 
                Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_DATE, tag);
                secureParser.readPosition = 0;

                // Error log
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_ERROR_LOG, tag))
                {
                    String str = "";
                    if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                    {
                        //configPacket.er = str;
                    }
                }
                secureParser.readPosition = 0;

                // Timing Log
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_CONNECTION_TIMING_LOG, tag))
                {
                    CSTagTypes.Tags.ConnectionTimingLog log = new Tags.ConnectionTimingLog();
                    if (Tpbp.GetTimingLog(secureParser, log))
                    {
                        configPacket.timingLog = log;
                    }
                }
                secureParser.readPosition = 0;

            }


            return configPacket;
        }

        /// <summary>
        /// Parse the payload for an interval data packet
        /// </summary>
        /// <param name="Tpbp"></param>
        /// <param name="parser"></param>
        /// <returns></returns>
        private CSTagTypes.Packets.ResponsePacket ParseIntervalData(TPBPHelpers Tpbp, PacketParser parser)
        {
            // Parse interval data packet
            CSTagTypes.Packets.IntervalDataPacket configPacket = new Packets.IntervalDataPacket();
            CSTagTypes.Tags.TPBPTag tag = new CSTagTypes.Tags.TPBPTag();

            configPacket.packetType = CSTagTypes.Packets.PacketTypeId.E_PACKET_TYPE_INTERVAL_DATA;

            // Get the Header
            if (Tpbp.FindTag(parser, Tags.TagNumber.E_TAG_NUMBER_CMIU_PACKET_HEADER, tag))
            {
                CSTagTypes.Tags.PackerHeader header = new CSTagTypes.Tags.PackerHeader();
                if (Tpbp.GetHeaderPacket(parser, header))
                {
                    configPacket.cmiuPacketHeader = header;
                }
            }

            // Get Secure data
            if (Tpbp.FindTag(parser, Tags.TagNumber.E_TAG_NUMBER_SECURE_DATA, tag))
            {
                Byte[] secureDataBuffer = new Byte[2000];
                Tpbp.GetNumberOfBytes(parser, secureDataBuffer, tag.dataSize);
                Array.Resize(ref secureDataBuffer, (Int32)tag.dataSize);
                PacketParser secureParser = new PacketParser();
                Tpbp.ParseInit(secureParser, secureDataBuffer, (UInt32)secureDataBuffer.Length);

                // Status  
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_CMIU_FLAGS, tag))
                {
                    CSTagTypes.Tags.CMIUStatus status = new Tags.CMIUStatus();
                    if (Tpbp.GetStatus(secureParser, status))
                    {
                        configPacket.status = status;
                    }
                }
                secureParser.readPosition = 0;

                // Reported Device Config
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_DEVICE_CONFIGURATION, tag))
                {
                    CSTagTypes.Tags.ReportedDeviceConfig device = new Tags.ReportedDeviceConfig();
                    if (Tpbp.GetReportedDeviceConfig(secureParser, device))
                    {
                        configPacket.reportedConfig = device;
                    }

                }
                secureParser.readPosition = 0;

                // tag34 - Interval Recording Configuration
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_INTERVAL_RECORDING, tag))
                {
                    CSTagTypes.Tags.IntervalRecordingData device = new Tags.IntervalRecordingData();
                    if (Tpbp.GetIntervalRecordingData(secureParser, device))
                    {
                        configPacket.intervalRecordingData = device;
                    }

                }
                secureParser.readPosition = 0;

                // tag28 - R900 Interval Data and Flags
                if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_R900_INTERVAL_DATA, tag))
                {
                    CSTagTypes.Tags.R900IntervalData device = new Tags.R900IntervalData();
                    if (Tpbp.GetR900IntervalData(secureParser, device, tag.dataSize))
                    {
                        configPacket.r900IntervalData = device;
                    }

                }
                secureParser.readPosition = 0;


                Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_DATE, tag);
                secureParser.readPosition = 0;
            }

            return configPacket;
        }


        private CSTagTypes.Packets.CommandResponsePacket ParseReadConnectedDevicesResponse(TPBPHelpers Tpbp, PacketParser secureParser, CSTagTypes.Packets.CommandResponsePacket responsePacket)
        {
            CSTagTypes.Packets.ReadConnectedDevicesResponsePacket readDevicesPacket = new Packets.ReadConnectedDevicesResponsePacket();
            CSTagTypes.Tags.TPBPTag tag = new CSTagTypes.Tags.TPBPTag();
            readDevicesPacket.command = CommandData.CommandIds.E_COMMAND_READ_CONNECTED_DEVICES;
            readDevicesPacket.packetType = responsePacket.packetType;
            readDevicesPacket.cmiuPacketHeader = responsePacket.cmiuPacketHeader;

            // Reported Device Config
            if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_DEVICE_CONFIGURATION, tag))
            {
                CSTagTypes.Tags.ReportedDeviceConfig device = new Tags.ReportedDeviceConfig();
                if(Tpbp.GetReportedDeviceConfig(secureParser, device))
                {
                    readDevicesPacket.reportedDeviceConfig = device;
                }               

            }
            secureParser.readPosition = 0;

            // Check for error
            GetError(readDevicesPacket, Tpbp, secureParser);

            return readDevicesPacket;
        }

        private CSTagTypes.Packets.CommandResponsePacket ParseAPNGetResponse(TPBPHelpers Tpbp, PacketParser secureParser, CSTagTypes.Packets.CommandResponsePacket responsePacket)
        {
            CSTagTypes.Packets.APNGetResponsePacket reportedAPNPacket = new Packets.APNGetResponsePacket();
            CSTagTypes.Tags.TPBPTag tag = new CSTagTypes.Tags.TPBPTag();
            reportedAPNPacket.command = CommandData.CommandIds.E_COMMAND_APN_GET;
            reportedAPNPacket.packetType = responsePacket.packetType;
            reportedAPNPacket.cmiuPacketHeader = responsePacket.cmiuPacketHeader;

            // Reported Device Config
            if (Tpbp.FindTag(secureParser, CSTagTypes.Tags.TagNumber.E_TAG_NUMBER_CMIU_APN, tag))
            {
                String str = "";
                if (Tpbp.GetString(secureParser, tag, ref str, tag.dataSize))
                {
                    reportedAPNPacket.reportedAPN = new Tags.CMIUAPN();
                    reportedAPNPacket.reportedAPN.cmiuAPN = str;
                }

            }
            secureParser.readPosition = 0;

            // Check for error
            GetError(reportedAPNPacket, Tpbp, secureParser);

            return reportedAPNPacket;
        }
    }
}
