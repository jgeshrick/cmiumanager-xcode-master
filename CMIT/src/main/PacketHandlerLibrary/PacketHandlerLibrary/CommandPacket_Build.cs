﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTagTypes;

namespace PacketHandlerLibrary
{
    class CommandPacket_Build
    {
        public TPBPHelpers Tpbp = new TPBPHelpers();
        public PacketPacker packer = new PacketPacker();
        public PacketPacker securePacker = new PacketPacker();

        private UInt64 fakeTime = 0;
        private Byte[] buffer = new Byte[2000];
        private Byte[] secureBuffer = new Byte[2000];
        private UInt32 bufferLength;
        private UInt32 secureBufferLength;

        public CommandPacket_Build(CSTagTypes.CommandData.CommandIds command)
        {
            CSTagTypes.Tags.MqttBleUart mqttBleUartHeader = new CSTagTypes.Tags.MqttBleUart();

            mqttBleUartHeader.encryptionMethod = 1;
            mqttBleUartHeader.sequenceNumber = 2;
            mqttBleUartHeader.token = 3;
            mqttBleUartHeader.keyInfo = 4;

            // Init the packers
            Tpbp.PackInit(packer, buffer, (UInt32)buffer.Length);
            Tpbp.PackInit(securePacker, secureBuffer, (UInt32)secureBuffer.Length);


            // Assemble the packet in tag order
            Tpbp.AddPacketId(packer, CSTagTypes.Packets.PacketTypeId.E_PACKET_TYPE_COMMAND); // == 0x05

            // Unsecure tag
            Tpbp.AddUartPacketHeader(packer, mqttBleUartHeader);

            // Add command and time
            Tpbp.AddCommand(securePacker, command);
            Tpbp.AddTimeAndDate(securePacker, fakeTime);
        }


        public UInt32 GetCompleteCommandPacket(Byte[] outputData)
        {

            Tpbp.PadZeroToBlockBoundary(securePacker);

            secureBufferLength = Tpbp.GetPackerByteCount(securePacker);

            Tpbp.AddSecureDataBlock(packer, secureBuffer, secureBufferLength);

            // End of pkt
            Tpbp.AddEof(packer);


            // Send the exact quantity (todo - resize)
            bufferLength = Tpbp.GetPackerByteCount(packer);


            Array.Copy(buffer, outputData, bufferLength);

            return bufferLength;
        }


    }
}
