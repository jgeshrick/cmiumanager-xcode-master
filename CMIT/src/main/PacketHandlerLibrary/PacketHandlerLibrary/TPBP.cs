﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using CSTagTypes;

namespace PacketHandlerLibrary
{
    /// <summary>
    /// Packer object
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    internal class PacketPacker
    {
        IntPtr pOutputBuffer;
        /** The current offset (where next byte will be written) */
        UInt32 writePosition;
        /** The size in bytes of the buffer */
        UInt32 bufferSize;
    }

    /// <summary>
    /// Parser object
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    internal class PacketParser
    {
        IntPtr pInputBuffer;
        /** The current offset (where next byte will be written) */
        public UInt32 readPosition;
        /** The size in bytes of the buffer */
        UInt32 bufferSize;
    }

    /// <summary>
    /// DLL Interface to TPBP
    /// </summary>
    internal static class TPBP
    {
        private const String constTpbpLibFileName = "TpbpLib.dll";

        ///// <summary>
        ///// Inits  
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void Tpbp_PackerInit(PacketPacker packer, IntPtr buffer, UInt32 bufferSize); 


        ///// <summary>
        ///// Gets count   
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt32 Tpbp_PackerGetCount(PacketPacker packer);


        ///// <summary>
        ///// Begin Packet: Add packet ID
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_Packer_BeginPacket(PacketPacker packer, CSTagTypes.Packets.PacketTypeId packetTypeId);

        
        /// <summary>
        /// Add string
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_CharArray(PacketPacker packer, CSTagTypes.Tags.TagNumber tag, IntPtr str);

        

        /// <summary>
        /// Add FTP port number - Tag 10
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_FtpPortNumber(PacketPacker packer, UInt16 ftpPortNumber);


        /// <summary>
        /// Add COMMAND tag to packet  - Tag 32
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_Command(PacketPacker packer, Byte commandByte);



        /// <summary>
        /// Add MQTT BLE tag to packet  - Tag 35
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_UartPacketHeader(PacketPacker packer, CSTagTypes.Tags.MqttBleUart mqttBleUart);

        /// <summary>
        /// Add ImageVersionInfoFirmware tag to packet  - Tag 39
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_ImageVersionInfoFirmware(PacketPacker packer, CSTagTypes.Tags.ImageVersionInfo imageVersionInfo);


        /// <summary>
        /// Add image name tag to packet  - Tag 46
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_ImageName(PacketPacker packer, IntPtr fileName);

        /// <summary>
        /// Add imageMetadata to packet  - Tag 47
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_ImageMetadata(PacketPacker packer, CSTagTypes.Tags.ImageMetadata imageMetadata);

        /// <summary>
        /// Add recording reporting intervals to packet  - Tag 48
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_RecordingReportingInterval(PacketPacker packer, CSTagTypes.Tags.RecordingReportingInterval intervals);

        /// <summary>
        /// Add time Date to packet  - Tag 50
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_TimeAndDate(PacketPacker packer, UInt64 timeDate);

        /// <summary>
        /// Add a start delay tag - Tag 53
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_Command_StartDelay(PacketPacker packer, UInt64 startDelay);

        /// <summary>
        /// Add a start delay tag - Tag 54
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_Command_Duration(PacketPacker packer, UInt64 duration);

        /// <summary>
        /// Add read connected device - Tag 55
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_Command_ReadConnectedDevice(PacketPacker packer, CSTagTypes.Tags.ReadConnectedDevice readDevice);

        /// <summary>
        /// Add power mode - Tag 56
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_Command_PowerMode(PacketPacker packer, Byte powerMode);

        /// <summary>
        /// Add read memory - Tag 57
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_Command_ReadMemory(PacketPacker packer, CSTagTypes.Tags.ReadMemory readMemory);

        /// <summary>
        /// Add RF Test Mode - Tag 58
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_Command_RFTestMode(PacketPacker packer, CSTagTypes.Tags.RFTestMode rfTestMode);

        /// <summary>
        /// Add RF Test Mode - Tag 70
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_Command_Btle_RFTestMode(PacketPacker packer, CSTagTypes.Tags.BLE_RFTestMode rfTestMode);

        /// <summary>
        /// Add Toggle LTE Modem - Tag 59
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_Command_ToggleLTEModem(PacketPacker packer, Byte toggleState);

        /// <summary>
        /// Add New Image Version - Tag 71
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_NewImageVersion(PacketPacker packer, IntPtr fileName);

        /// <summary>
        /// Add Packet Instigator - Tag 72
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_PacketInstigator(PacketPacker packer, Byte packetInstigator);

        


        /// <summary>
        /// End Packet : Add EOF Marker
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_Packer_EndPacket(PacketPacker packer);

        /// <summary>
        /// Add secure data block to packet  - 
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_SecureBlockArray(PacketPacker packer, IntPtr buffer, UInt32 bufferSize);

        /// <summary>
        /// Pad to block boundary 
        /// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerPadZeroToBlockBoundary(PacketPacker packer, UInt32 blockSize);

        ///// <summary>
        ///// Packet Type
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_PacketTypeTag(PacketPacker packer, Byte packetType);

        ///// <summary>
        ///// Packet Type
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_Destination(PacketPacker packer, Byte destination);

        ///// <summary>
        ///// Memory Image
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_PackerAdd_MemoryImageTag(PacketPacker packer, UInt16 memoryImage);

        

        //
        //
        // Parser Functions
        //
        //

        ///// <summary>
        ///// Inits  
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void Tpbp_ParserInit(PacketParser parser, IntPtr buffer, UInt32 bufferSize);


        ///// <summary>
        ///// Gets Packet type  
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern CSTagTypes.Packets.PacketTypeId Tpbp_ParserRead_PacketTypeId(PacketParser parser);   

        ///// <summary>
        ///// Gets tag   
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_ParserReadTag(PacketParser parser, CSTagTypes.Tags.TPBPTag tag);


        ///// <summary>
        ///// Gets tag   
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_ParserRead_CmiuPacketHeader(PacketParser parser, CSTagTypes.Tags.PackerHeader packetHeader);

        ///// <summary>
        ///// Gets bytes   
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_ParserGetBytes(PacketParser parser, byte[] buffer, UInt32 numberOfBytes);

        ///// <summary>
        ///// Finds tag   
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_ParserFindTag(PacketParser parser, CSTagTypes.Tags.TagNumber tagNumber, CSTagTypes.Tags.TPBPTag tag);

        ///// <summary>
        ///// Finds tag   
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_ParserRead_String(PacketParser parser, CSTagTypes.Tags.TPBPTag tag, StringBuilder output, UInt32 buffer);

        ///// <summary>
        ///// Reads the image version info 
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_ParserRead_ImageVersionInfo(PacketParser parser, CSTagTypes.Tags.ImageVersionInfo imageInfo);

        ///// <summary>
        ///// CMIU Diagnostics 
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_ParserRead_CmiuDiagnostics(PacketParser parser, CSTagTypes.Tags.CMIUDiagnostics diagnostics);

        ///// <summary>
        ///// CMIU Config 
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_ParserRead_CmiuConfiguration(PacketParser parser, CSTagTypes.Tags.CMIUConfigurarion config);

        ///// <summary>
        ///// CMIU Status 
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_ParserRead_CmiuStatus(PacketParser parser, CSTagTypes.Tags.CMIUStatus status);

        ///// <summary>
        ///// CMIU Status 
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_ParserRead_ConnectionTimingLog(PacketParser parser, CSTagTypes.Tags.ConnectionTimingLog log);

        ///// <summary>
        ///// CMIU ID 
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_ParserRead_CmiuId(PacketParser parser, ref UInt32 cmiuID); 


        ///// <summary>
        ///// CMIU Reported Device Config 
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_ParserRead_ReportedDeviceConfig(PacketParser parser, CSTagTypes.Tags.ReportedDeviceConfig reportedDeviceConfig);

        ///// <summary>
        ///// CMIU Interval Recording Data
        ///// </summary>
        [DllImport(constTpbpLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Tpbp_ParserRead_IntervalRecordingConfig(PacketParser parser, CSTagTypes.Tags.IntervalRecordingData intervalRecordingData);

    }
}
