﻿/*****************************************************************************
******************************************************************************
**
**         Filename: nRFDongleUart.cs
**    
**          Author: Duncan Willis
**          Created: 05 May 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*****************************************************************************
* @file nRFDongleUart 
*
* @brief C# wrapper class for UART Controller DLL
* 
******************************************************************************/


using System;
using System.IO;
using System.Windows;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;


namespace nRFDongleUartLib
{
    /// <summary>
    /// Define a simple struct for TX and RX of messages
    /// </summary>
    public class UarcMessage
    {
        public Byte[] payload;
        public UarcMessageType type;
    }

    /// <summary>
    /// Used in the Type field of a message - see also UartMessageControllerTypes.h
    /// </summary>
    public enum UarcMessageType
    {
    UARC_MT_NULL        = 0x00,     // Test use
    UARC_MT_CMD         = 0x01,     // Command sent from Master, response is expected
    UARC_MT_PING        = 0x02,     // Ping request
    UARC_MT_HTTP_CMD    = 0x04,     // HTTP Request
    UARC_MT_RESP        = 0x81,     // Response to a command
    UARC_MT_GNIP        = 0x82,     // Rsponse to a ping
    UARC_MT_ASYNC       = 0x83,     // Async message (eg log); No response required.
    UARC_MT_HTTP_RSP    = 0x84,     // HTTP Response
    }



    /// <summary>
    /// The state of the Send Engine
    /// </summary>
    public enum E_UARC_MSG_SEND_STATE
    {
        UARC_MSS_READY_TO_SEND  = 0,        // A command can be sent
        UARC_MSS_BUSY           = 1,        // Waiting for response
        UARC_MSS_TIMEOUT        = 2,        // The last command did not receive a response; a new command can be sent.
    }


    /// <summary>
    /// This class is a C# wrapper around the UART Controller DLL
    /// It is used by the nRFUartController
    /// </summary>
    static class nRFDongleUart
    {
        const UInt32 constTickIntervalMs = 100;
        static Byte[] sendTransferBuffer = new Byte[4096];
        static Byte[] rxBufferManagedMemory = new Byte[4096];
        static GCHandle gh;
        const UInt32 RX_MSG_GET_COUNT = 0;
        const UInt32 RX_MSG_RESET_COUNT = 1;
        private static System.Timers.Timer tickTimer;
        private const String constUartControllerLibFileName = "UartControllerDll.dll";

        public delegate void    OnMsgHandler(UarcMessage m);
        public static event     OnMsgHandler MessageReceived;
        public delegate void    OnMsgFailHandler();
        public static event     OnMsgFailHandler MessageFailed;   
        private static          OnMsgFailHandler msgFailHandler = null;
        

        /// <summary>
        /// Inits the DLL
        /// </summary>
        [DllImport(constUartControllerLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void UartControllerDllInit();

        /// <summary>
        /// Start a timer to wait for response
        /// </summary>
        /// <param name="timeoutMs"></param>
        /// <returns></returns>
        [DllImport(constUartControllerLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt32 UartControllerDllStartSendTimer(UInt32 timeoutMs);

        /// <summary>
        /// Gets the state of the send action, returns E_UARC_MSG_SEND_STATE as uint
        /// </summary>
        /// <param name="dummy">Not used, needed for DLL compatibilty</param>
        /// <returns></returns>
        [DllImport(constUartControllerLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt32 UartControllerDllGetSendState(UInt32 dummy);

        /// <summary>
        /// Assemble a packet of given type into supplied buffer, add CRC32.
        /// </summary>
        /// <param name="type">The enum tyoe as uint</param>
        /// <param name="payload">The data to send</param>
        /// <param name="payloadBytesToSend">Count of data to send in bytes</param>
        /// <param name="pBuffer">The output bytes</param>
        /// <param name="bufferMaxSize">size of the output array</param>
        /// <returns> Number of bytes written or 0 if insufficient mem supplied.</returns>
        [DllImport(constUartControllerLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt32 UartControllerDllMakePacket(UInt32 type,
            IntPtr payload,
            UInt32 payloadBytesToSend,
            IntPtr pBuffer,
            UInt32 bufferMaxSize);

        /// <summary>
        /// Call when we have an incoming byte received over the BTLE
        /// </summary>
        /// <param name="b"></param>
        [DllImport(constUartControllerLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern void UartControllerDllOnRxByte(Byte b);

        /// <summary>
        /// Tick function. Call at tickInterval
        /// </summary>
        /// <param name="tickIntervalMs"></param>
        [DllImport(constUartControllerLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern void UartControllerDllTick(UInt32 tickIntervalMs);

        /// <summary>
        /// Gives an area of memory to the DLL, be used to store a decoded incoming message
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dataBytes"></param>
        [DllImport(constUartControllerLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern void UartControllerDllSetRxBuffer(IntPtr data, UInt32 dataBytes);

        /// <summary>
        /// Send an async message
        /// </summary>
        /// <param name="type"></param>
        /// <param name="data"></param>
        /// <param name="dataBytes"></param>
        [DllImport(constUartControllerLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern void UartControllerDllSend(UInt32 type, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] Byte[] payload, UInt32 payloadLength);

        [DllImport(constUartControllerLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern UInt32 UartControllerDllGetRxMessageByteCount(UInt32 doReset);

        [DllImport(constUartControllerLibFileName, CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern UInt32 UartControllerDllGetRxMessageType(UInt32 dummy);

        /// <summary>
        /// Init the UART Controller DLL 
        /// </summary>
        public static void Init()
        {
            UartControllerDllInit();

            // Fix the buffer for use in the DLL
            gh = GCHandle.Alloc(rxBufferManagedMemory, GCHandleType.Pinned);

            // Get a pointer suitable for DLL
            IntPtr rxPayloadBuffer = gh.AddrOfPinnedObject();
            UartControllerDllSetRxBuffer(rxPayloadBuffer, (UInt32)rxBufferManagedMemory.Length);


            tickTimer = new System.Timers.Timer();
            tickTimer.Interval = constTickIntervalMs;
            tickTimer.Elapsed += new ElapsedEventHandler(tickTimer_Elapsed);
            tickTimer.Enabled = true;
        }


        /// <summary>
        /// On tick, check send state and fire fail handler on timeout
        /// </summary>
        /// <param name="sender">Not used</param>
        /// <param name="e">Not used</param>
        private static void tickTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            UartControllerDllTick(constTickIntervalMs);

            if ((UInt32)E_UARC_MSG_SEND_STATE.UARC_MSS_TIMEOUT == UartControllerDllGetSendState())
            {
                var handler = msgFailHandler;
                if (handler != null)
                {
                    handler();
                    msgFailHandler = null;
                }
            }
        }


        /// <summary>
        /// Begin a timer when a command is sent.
        /// </summary>
        /// <param name="timeoutMs"></param>
        /// <returns></returns>
        public static bool StartSendTimer(UInt32 timeoutMs)
        {
            msgFailHandler = MessageFailed;
            UInt32 isOk = UartControllerDllStartSendTimer(timeoutMs);
            return (isOk == 1);
        }


        /// <summary>
        /// Get the state of the send operation
        /// </summary>
        /// <returns>   UARC_MSS_READY_TO_SEND  = 0,
        ///             UARC_MSS_BUSY           = 1,
        ///             UARC_MSS_TIMEOUT        = 2</returns>  
        public static UInt32 UartControllerDllGetSendState()
        {
            return (UInt32)UartControllerDllGetSendState(0);
        }


        /// <summary>
        /// Make a packet from the given message (overload)
        /// The payload is populated with dummy data to the size given
        /// </summary>
        /// <param name="m">The message to make into bytes</param>
        /// <returns>Raw data for transmisssion over BTLE</returns>
        public static Byte[] MakePacket(UarcMessage m)
        {
            return MakePacket(m.type, m.payload);
        }


        /// <summary>
        /// Make a packet from the given type and payload bytes
        /// </summary>
        /// <param name="type"></param>
        /// <param name="payload"></param>
        /// <returns>Raw data for transmisssion over BTLE</returns>
        public static Byte[] MakePacket(UarcMessageType type, Byte[] payload)
        {
            Byte[] txBufferManaged = new byte[4096]; /// todo max size?
            GCHandle ghTxBuf;
            GCHandle ghPayload;
            IntPtr ipPayload;
            UInt32 payloadCount = 0;
            ghTxBuf = GCHandle.Alloc(txBufferManaged, GCHandleType.Pinned);

            if (payload != null)
            {
                ghPayload = GCHandle.Alloc(payload, GCHandleType.Pinned);
                ipPayload = ghPayload.AddrOfPinnedObject();
                payloadCount = (UInt32)payload.Length;
            }
            else
            {
                ghPayload = GCHandle.Alloc(new byte[1], GCHandleType.Pinned);
                ipPayload = ghPayload.AddrOfPinnedObject();

            }
            // Get a pointer suitable for use with DLL
            IntPtr ipTxBuffer = ghTxBuf.AddrOfPinnedObject();


            // Make the bytes to send
            UInt32 numToSend = UartControllerDllMakePacket((UInt32)type,
                ipPayload,
                payloadCount,
                ipTxBuffer,
                (UInt32)txBufferManaged.Length);

            ghTxBuf.Free();
            ghPayload.Free();

            // Now send the relevant amount.
            Byte[] sendData = new Byte[numToSend];
            Array.Copy(txBufferManaged, sendData, numToSend);

            return sendData;
        }


        /// <summary>
        /// Event handler for received char, called for each char received on BTLE
        /// In turn, once the complete Msg has been received, then call a msg handler
        /// </summary>
        /// <param name="b">The received byte</param>
        public static void OnCharReceived(Byte b)
        {
            // send to DLL for message parsing
            UartControllerDllOnRxByte(b);

            // Has a complete msg been received?
            UInt32 payloadByteCount = UartControllerDllGetRxMessageByteCount(RX_MSG_GET_COUNT);
            if (payloadByteCount > 0)
            {
                // Reset the counter, to allow a new message to be received
                UartControllerDllGetRxMessageByteCount(RX_MSG_RESET_COUNT);
                UarcMessage m = new UarcMessage();
                m.payload = new byte[payloadByteCount];
                Array.Copy(rxBufferManagedMemory, m.payload, payloadByteCount);
                m.type = (UarcMessageType)UartControllerDllGetRxMessageType(0);

                // Call event handler
                MessageReceived(m);
            }
        }
    }
}


