﻿/* Copyright (c) 2013 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT. 
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/* This application is targeted to work with a peripheral loaded with an nRF UART peripheral
 * application.
 */

using System;
using System.IO;
using System.Windows;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Threading;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Nordicsemi;
using UartDataController;


namespace nRFDongleUartLib
{
    /// <summary>
    /// Provides data for the OutputReceived event.
    /// </summary>
    public class OutputReceivedEventArgs : EventArgs
    {
        public string Message { get; set; }

        public OutputReceivedEventArgs(string message)
        {
            Message = message;
        }
    }

    /// <summary>
    /// Provides data for RXd byte event
    /// </summary>
    public class OnRxEventArgs : EventArgs
    {
        public byte by { get; set; }

        public OnRxEventArgs(byte b)
        {
            by = b;
        }
    }

    /// <summary>
    /// Provides the bluetooth devices info when device is discovered
    /// </summary>
    public class OnDeviceDiscoveredArgs : EventArgs
    {
        public BtDevice device { get; set; }

        public OnDeviceDiscoveredArgs(BtDevice dev)
        {
            device = dev;
        }
    }

    public enum PingType
    {
        ShortPings,
        ExtendedPing,
        FixedPayloadLength
    };

    /// <summary>
    /// This class controls all calls to MasterEmulator DLL and implements the nRF UART 
    /// logic.
    /// </summary>
    public class nRFUartController
    {
        /* Event declarations */
        public event EventHandler<OutputReceivedEventArgs> LogMessage;
        public event EventHandler<EventArgs> Initialized;
        public event EventHandler<EventArgs> Scanning;
        public event EventHandler<EventArgs> ScanningCanceled;
        public event EventHandler<EventArgs> Connecting;
        public event EventHandler<EventArgs> ConnectionCanceled;
        public event EventHandler<EventArgs> Connected;
        public event EventHandler<EventArgs> PipeDiscoveryCompleted;
        public event EventHandler<EventArgs> Disconnected;
        public event EventHandler<EventArgs> SendDataStarted;
        public event EventHandler<EventArgs> SendDataCompleted;
        public event EventHandler<EventArgs> SendDataFailed;
        public event EventHandler<OutputReceivedEventArgs> SendDataReadyToSend;
        public event EventHandler<ValueEventArgs<int>> ProgressUpdated;
        public event EventHandler<OnDeviceDiscoveredArgs> DeviceDiscovered;

        public event EventHandler<EventArgs> OnErrorInitialization;
        public event EventHandler<EventArgs> OnErrorDongleState;
        public event EventHandler<EventArgs> OnErrorConnection;

        private event EventHandler<OnRxEventArgs> OnRxByte;

        public delegate void OnMsgHandler(UarcMessage m);
        public event OnMsgHandler MessageReceived;

        public delegate void OnSendFailHandler();
        public event OnSendFailHandler MessageSendFailed;

        /* Public properties */
        public bool DebugMessagesEnabled { get; set; }

        /* Instance variables */
        MasterEmulator                  masterEmulator;
        PipeSetup                       pipeSetup;
        UartDataController.UartData     uartController;
        bool connectionInProgress       = false;
        bool sendData                   = false;
        bool autoConnect                = false;
        bool taskRunning                = false;
        bool useBondedConnection        = false;
        public bool hasBondedOk         = false;
        string deviceNameToConnectTo    = string.Empty;
        string deviceAddressToConnectTo = string.Empty;
        const int maxPacketLength       = 20;
        const int counterFieldLength    = 2;
        const int maxPayloadLength      = maxPacketLength - counterFieldLength;

        DateTime dtStartDiscovery;
        TimeSpan timeToScanForSec = TimeSpan.FromSeconds(5.0);
        //String deviceAddressToConnectTo = string.Empty; //"C6482F913B8D";// 
        static Random randomDataValue = new Random(0); // Use same sequence for each run

        // Event is set to signal completion of send background task
        // Automatically cleared on the WaitOne completing
        // true = initially set; Send is OK to go on first call.
        AutoResetEvent sendThreadEvent = new AutoResetEvent(true); 

        
        ///////////////////////////////////////////////////////////////////////
        //
        // Setup
        //
        ///////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Constructor.
        /// </summary>
        public nRFUartController()
        {
            this.OnRxByte += controller_OnRxByte;

            // Init the Uart Controller interface
            uartController = new UartData();
            uartController.Init();
            uartController.MessageReceived += OnUarcMessageReceived;
            uartController.MessageFailed += OnSendFail;
            uartController.KeepAlive += OnKeepAlive;
            
        }

        

        /// <summary>
        /// Collection of method calls to start and setup MasterEmulator.
        /// The calls are placed in a background task for not blocking the gui thread.
        /// </summary>
        public void Initialize()
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    InitializeMasterEmulator();
                    string device = FindUsbDevice();
                    OpenMasterEmulatorDevice(device);
                    pipeSetup = new PipeSetup(masterEmulator);
                    pipeSetup.PerformPipeSetup();
                    //masterEmulator.LogVerbosity = Verbosity.High;
                    Run();
                    Initialized(this, EventArgs.Empty);
                }
                catch (Exception ex)
                {
                    LogErrorMessage(string.Format("Exception in StartMasterEmulator: {0}", ex.Message),
                    ex.StackTrace);
                    OnErrorInitialization(this, EventArgs.Empty);
                }
            });
        }

        /// <summary>
        /// Tell the api to use the given master emulator device.
        /// </summary>
        /// <param name="device"></param>
        void OpenMasterEmulatorDevice(string device)
        {
            if (masterEmulator.IsOpen)
            {
                return;
            }

            masterEmulator.Open(device);
            masterEmulator.Reset();
        }

        /// <summary>
        /// Create MasterEmulator instance.
        /// </summary>
        void InitializeMasterEmulator()
        {
            if (masterEmulator == null)
            {
                masterEmulator = new MasterEmulator();
                masterEmulator.LogVerbosity = Verbosity.Low;
                RegisterEventHandlers();
            }

        }

        /// <summary>
        /// Register event handlers for MasterEmulator events.
        /// </summary>
        void RegisterEventHandlers()
        {
            masterEmulator.Connected += OnConnected;
            masterEmulator.ConnectionUpdateRequest += OnConnectionUpdateRequest;
            masterEmulator.DataReceived += OnDataReceived;
            masterEmulator.DeviceDiscovered += OnDeviceDiscovered;
            masterEmulator.Disconnected += OnDisconnected;
            masterEmulator.LogMessage += OnLogMessage;
            masterEmulator.PipeError += OnPipeError;
        }

        void OnPipeError(object sender, PipeErrorEventArgs e)
        {
            //throw new NotImplementedException();
            OnErrorConnection(this, EventArgs.Empty);
        }



        ///////////////////////////////////////////////////////////////////////
        //
        // Dongle Methods
        //
        ///////////////////////////////////////////////////////////////////////

        public void ScanForDevices()
        {
            autoConnect = false;
            bool success = StartDeviceDiscovery();
            if(!success)
            {
                OnErrorDongleState(this, EventArgs.Empty);
            }
        }


        /// <summary>
        /// Connect to peer device.
        /// </summary>
        public void InitiateConnection()
        {
            autoConnect = true;
            deviceNameToConnectTo = string.Empty;
            deviceAddressToConnectTo = string.Empty;
            bool success = StartDeviceDiscovery();
        }

        /// <summary>
        /// Connect to peer device.
        /// </summary>
        /// wantBonded indicates if the connection should be bonded once made
        public void InitiateConnection(bool doAutoConnect, bool wantBonded )
        {
            autoConnect = doAutoConnect;///dew
            this.useBondedConnection = wantBonded;
            bool success = StartDeviceDiscovery();
        }

        /// <summary>
        /// Stop scanning for devices.
        /// </summary>
        public void StopScanning()
        {
            if (!masterEmulator.IsDeviceDiscoveryOngoing)
            {
                return;
            }

            bool success = masterEmulator.StopDeviceDiscovery();

            if (success)
            {
                ScanningCanceled(this, EventArgs.Empty);
            }
            else
            {
                OnErrorDongleState(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Function to check if connected to a device
        /// </summary>
        /// <returns></returns>
        public bool IsConnected()
        {
            if(masterEmulator != null)
            {
                return masterEmulator.IsConnected;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Disconnect from peer device.
        /// </summary>
        public void InitiateDisconnect()
        {
            if (!masterEmulator.IsConnected)
            {
                return;
            }

            bool success = masterEmulator.Disconnect();

            if(!success)
            {
                OnErrorDongleState(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool DoBond()
        {
            this.hasBondedOk = masterEmulator.Bond();

            if (hasBondedOk)
            {
                AddToLog("\n***** Bond Ok ******\n");
            }
            else
            {
                AddToLog("\n***** Bond Failed *****\n");
            }

            return hasBondedOk;
        }

        /// <summary>
        /// 
        /// </summary>
        public void DeleteBond()
        {
            masterEmulator.DeleteBondInformation();

            AddToLog("Bond Info Deleted");
        }

        /// <summary>
        /// Sets the time in sec to scan for devices, before permitting a connect
        /// This allows user time to see all devs in the area,
        /// </summary>
        /// <param name="scanTimeSec">Time to scan before connecting</param>
        public void SetScanTimeSec(UInt32 scanTimeSec)
        {
            timeToScanForSec = TimeSpan.FromSeconds((double)scanTimeSec);
        }

        /// <summary>
        /// Close MasterEmulator.
        /// </summary>
        public void Close()
        {
            if (!masterEmulator.IsOpen)
            {
                return;
            }

            masterEmulator.Close();
        }

        /// <summary>
        /// Searching for master emulator devices attached to the pc. 
        /// If more than one is connected it will simply return the first in the list.
        /// </summary>
        /// <returns>Returns the first master emulator device found.</returns>
        string FindUsbDevice()
        {
            /* The UsbDeviceType argument is used for filtering master emulator device types. */
            var devices = masterEmulator.EnumerateUsb(UsbDeviceType.AnyMasterEmulator);

            if (devices.Count > 0)
            {
                return devices[0];
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Sets the MAC (as string eg "C6482F913B8D" )to scan for and connect to if present. 
        /// If empty (default), then conenct to first device seen. 
        /// </summary>
        /// <param name="address">string version of address</param>
        public void SetConnectAddress(String address)
        {
            // Need to convert this to use a name?
            // It makes more sense from the CMIU library side to connect to the CMIU ID
        }

        /// <summary>
        /// Connect to device with the name (CMIU ID)
        /// </summary>
        /// <param name="deviceName">CMIU ID i.e. NC499999998</param>
        public void ConnectToDeviceWithName(string deviceName)
        {
            deviceNameToConnectTo = deviceName;
        }

        /// <summary>
        /// Sets the MAC (as string eg "C6482F913B8D" )to scan for and connect to if present. 
        /// If empty (default), then conenct to first device seen. 
        /// </summary>
        /// <param name="address">string version of address</param>
        public void ConnectToDeviceWithAddress(string address)
        {
            deviceAddressToConnectTo = address;
        }


        /// <summary>
        /// By calling Run, the pipesetup is processed and the stack engine is started.
        /// </summary>
        void Run()
        {
            if (masterEmulator.IsRunning)
            {
                return;
            }

            try
            {
                masterEmulator.Run();
            }
            catch (Exception ex)
            {
                OnErrorDongleState(this, EventArgs.Empty);
            }
           
        }

        /// <summary>
        /// Device discovery is started with the given scan parameters.
        /// By stating active scan, we will be receiving data from both advertising
        /// and scan repsonse packets.
        /// </summary>
        /// <returns></returns>
        bool StartDeviceDiscovery()
        {
            if (!masterEmulator.IsRunning)
            {
                AddToLog("Not ready.");
                return false;
            }

            BtScanParameters scanParameters = new BtScanParameters();
            scanParameters.ScanType = BtScanType.ActiveScanning;
            scanParameters.ScanIntervalMs = 250;
            scanParameters.ScanWindowMs = 50;
            bool startSuccess = masterEmulator.StartDeviceDiscovery(scanParameters);

            if (startSuccess)
            {
                Scanning(this, EventArgs.Empty);
            }
            else
            {
                OnErrorDongleState(this, EventArgs.Empty);
            }

            return startSuccess;
        }

        /// <summary>
        /// Connecting to the given device, and with the given connection parameters.
        /// </summary>
        /// <param name="device">Device to connect to.</param>
        /// <returns>Returns success indication.</returns>
        bool Connect(BtDevice device)
        {
            if (masterEmulator.IsDeviceDiscoveryOngoing)
            {
                bool success = masterEmulator.StopDeviceDiscovery();

                if(!success)
                {
                    OnErrorDongleState(this, EventArgs.Empty);
                }
            }

            string deviceName = GetDeviceName(device.DeviceInfo);
            AddToLog(string.Format("Connecting to {0}, Device name: {1}",                                              
                device.DeviceAddress.ToString(), deviceName));

            BtConnectionParameters connectionParams = new BtConnectionParameters();
#if CONNECT_INTERVAL_LONG
            connectionParams.ConnectionIntervalMs = 60;
#else
            connectionParams.ConnectionIntervalMs = 11.25;
#endif
            connectionParams.ScanIntervalMs = 250;
            connectionParams.ScanWindowMs = 200;


            AddToLog(string.Format("ConnectionInterval = {0}ms", connectionParams.ConnectionIntervalMs));

            bool connectSuccess = masterEmulator.Connect(device.DeviceAddress, connectionParams);
            if(!connectSuccess)
            {
                OnErrorDongleState(this, EventArgs.Empty);
            }

            return connectSuccess;
        }

        /// <summary>
        /// By discovering pipes, the pipe setup we have specified will be matched up
        /// to the remote device's ATT table by ATT service discovery.
        /// </summary>
        void DiscoverPipes()
        {
            bool success = masterEmulator.DiscoverPipes();

            if (!success)
            {
                OnErrorConnection(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Pipes of type PipeType.Receive must be opened before they will start receiving notifications.
        /// This maps to ATT Client Configuration Descriptors.
        /// </summary>
        void OpenRemotePipes()
        {
            var openedPipesEnumeration = masterEmulator.OpenAllRemotePipes();
            List<int> openedPipes = new List<int>(openedPipesEnumeration);
        }


        ///////////////////////////////////////////////////////////////////////
        //
        // Logging Methods
        //
        ///////////////////////////////////////////////////////////////////////
       
        /// <summary>
        ///  Method for adding text to the textbox and logfile.
        ///  When called on the main thread, invoke is not required.
        ///  For other threads, the invoke is required.
        /// </summary>
        /// <param name="message">The message string to add to the log.</param>
        void AddToLog(string message)
        {
            if (LogMessage != null)
            {
                LogMessage(this, new OutputReceivedEventArgs(message));
            }

            /* Writing to trace also, which causes the message to be put in the log file. */
            Trace.WriteLine(message);
        }

        /// <summary>
        /// Convenience method for logging exception messages.
        /// </summary>
        void LogErrorMessage(string errorMessage, string stackTrace)
        {
            AddToLog(errorMessage);
            Trace.WriteLine(stackTrace);
        }

        /// <summary>
        /// Get the path to the log file of MasterEmulator.
        /// </summary>
        /// <returns>Returns path to log file.</returns>
        public string GetLogfilePath()
        {
            return masterEmulator.GetLogFilePath();

        }

        ///////////////////////////////////////////////////////////////////////
        //
        // Events
        //
        ///////////////////////////////////////////////////////////////////////


        /// <summary>
        /// Helper to receive a byte from BTLE and send to parser
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void controller_OnRxByte(object sender, OnRxEventArgs e)
        {
            uartController.OnCharReceived(e.by);
        }



        /// <summary>
        /// Throw the rx msg up the stack
        /// </summary>
        /// <param name="m">The received message</param>
        void OnUarcMessageReceived(UarcMessage m)
        {
            var h = MessageReceived;
            if (h != null)
            {
                h(m);
            }     
        }

        void OnSendFail()
        {
            var h = MessageSendFailed;
            if(h != null)
            {
                h();
            }            
        }

        private void OnKeepAlive()
        {
            if (masterEmulator != null)
            {
                if (masterEmulator.IsConnected)
                {
                    Byte[] payload = new Byte[2];
                    payload[0] = (byte)'H';
                    payload[1] = (byte)'B';

                    //Byte[] bytesToSend = nRFDongleUart.MakePacket(UarcMessageType.UARC_MT_PING, payload);
                    Byte[] bytesToSend = uartController.MakePacket(UarcMessageType.UARC_MT_PING, payload);

                    // Disable keepalive in C# app for now
                    // this.StartSendData(bytesToSend);
                }
            }
        }


        /// <summary>
        /// Event handler for DeviceDiscovered. This handler will be called when devices
        /// are discovered during asynchronous device discovery.
        /// </summary>
        void OnDeviceDiscovered(object sender, ValueEventArgs<BtDevice> arguments)
        {
            /* Avoid call after a connect procedure is being started,
             * and the discovery procedure hasn't yet been stopped. */
            if (connectionInProgress)
            {
                return;
            }

            Task.Factory.StartNew(() =>
            {
                BtDevice device = arguments.Value;

                // If eligible for connection, then notify a device has been discovered
                if (IsEligibleForConnection(device))
                {
                    //Trace.WriteLine("CMIU Discovered: " + device.DeviceAddress);
                    DeviceDiscovered(this, new OnDeviceDiscoveredArgs(device));
                }

                // If autoconnect, attempt to connect to the first device discovered
                if (autoConnect)
                {
                    ConnectToDevice(device);
                }
                else
                {
                    // Check if a device has been requested to connect to
                    if (deviceNameToConnectTo != string.Empty || deviceAddressToConnectTo != string.Empty)
                    {
                        string deviceName = GetNameOfDevice(device);
                        if (deviceName == deviceNameToConnectTo || deviceAddressToConnectTo == device.DeviceAddress.Value)
                        {
                            ConnectToDevice(device);
                        }
                    }
                }
            });

            

            
        }

        /// <summary>
        /// This event handler is called when data has been received on any of our pipes.
        /// </summary>
        void OnDataReceived(object sender, PipeDataEventArgs arguments)
        {
            if (arguments.PipeNumber != pipeSetup.UartTxPipe)
            {
                AddToLog("Received data on unknown pipe.");
                return;
            }

            StringBuilder stringBuffer = new StringBuilder();
            foreach (byte element in arguments.PipeData)
            {
                OnRxByte(sender, new OnRxEventArgs(element));
                stringBuffer.AppendFormat(" 0x{0:X2}", element);
            }

            if (DebugMessagesEnabled)
            {
                AddToLog(string.Format("Data received on pipe number {0}:{1}", arguments.PipeNumber,
                    stringBuffer.ToString()));
            }

            byte[] utf8Array = arguments.PipeData;
            string convertedText = Encoding.UTF8.GetString(utf8Array);

        }

        /// <summary>
        /// This event handler is called when a connection has been successfully established.
        /// </summary>
        void OnConnected(object sender, EventArgs arguments)
        {
            if (Connected != null)
            {
                Connected(this, EventArgs.Empty);
            }

            /* The connection is up, proceed with pipe discovery. 
             * Using a background task in order not to block the event caller. */
            Task.Factory.StartNew(() =>
            {
                try
                {                    
                    DiscoverPipes();
                    OpenRemotePipes();
                    //var props = masterEmulator.GetCharacteristicProperties(pipeSetup.UartTxPipe);
                    //var props2 = masterEmulator.GetCharacteristicProperties(pipeSetup.UartRxPipe);
                    PipeDiscoveryCompleted(this, EventArgs.Empty);
                }
                catch (Exception ex)
                {
                    LogErrorMessage(string.Format("Exception in OnConnected: {0}", ex.Message),
                        ex.StackTrace);
                    if (null != OnErrorConnection)
                    {
                        OnErrorConnection(this, EventArgs.Empty);
                    }
                }
            });
        }

        /// <summary>
        /// This event handler is called when a connection update request has been received.
        /// A connection update must be responded to in two steps: sending a connection update
        /// response, and performing the actual update.
        /// </summary>
        void OnConnectionUpdateRequest(object sender, ConnectionUpdateRequestEventArgs arguments)
        {
            Task.Factory.StartNew(() =>
            {
                masterEmulator.SendConnectionUpdateResponse(arguments.Identifier,
                    ConnectionUpdateResponse.Accepted);
                BtConnectionParameters updateParams = new BtConnectionParameters();
                updateParams.ConnectionIntervalMs = arguments.ConnectionIntervalMinMs;
                updateParams.SupervisionTimeoutMs = arguments.ConnectionSupervisionTimeoutMs;
                updateParams.SlaveLatency = arguments.SlaveLatency;
                masterEmulator.UpdateConnectionParameters(updateParams);
            });
        }

        /// <summary>
        /// This event handler is called when a connection has been terminated.
        /// </summary>
        void OnDisconnected(object sender, ValueEventArgs<DisconnectReason> arguments)
        {
            connectionInProgress = false;
            sendData = false;
            autoConnect = false;
            deviceNameToConnectTo = string.Empty;
            deviceAddressToConnectTo = string.Empty;
            Disconnected(this, EventArgs.Empty);
        }

        /// <summary>
        /// Relay received log message events to the log method.
        /// </summary>
        void OnLogMessage(object sender, ValueEventArgs<string> arguments)
        {
            string message = arguments.Value;

            if (message.Contains("Connected to"))
            {
                /* Don't filter out */
            }
            else if (message.Contains("Disconnected"))
            {
                return;
            }
            else if (!DebugMessagesEnabled)
            {
                return;
            }

            AddToLog(string.Format("{0}", arguments.Value));
        }


        ///////////////////////////////////////////////////////////////////////
        //
        // Helper Methods
        //
        ///////////////////////////////////////////////////////////////////////


        private void ConnectToDevice(BtDevice device)
        {
            if (!IsEligibleForConnection(device))
            {
                ConnectionCanceled(this, EventArgs.Empty);
                return;
            }

            connectionInProgress = true;

            /* Start the connection procedure in a background task to avoid 
             * blocking the event caller. */
            Task.Factory.StartNew(() =>
            {
                try
                {
                    Connecting(this, EventArgs.Empty);

                    bool success = Connect(device);
                    if (!success)
                    {
                        ConnectionCanceled(this, EventArgs.Empty);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    LogErrorMessage(string.Format("Exception in OnDeviceDiscovered: {0}",
                        ex.Message), ex.StackTrace);
                    ConnectionCanceled(this, EventArgs.Empty);
                }
            });
        }

        /// <summary>
        /// Check if a device has the advertising data we are looking for.
        /// </summary>
        bool IsEligibleForConnection(BtDevice device)
        {
            // Check if the UUID is correct
            IDictionary<DeviceInfoType, string> deviceInfo = device.DeviceInfo;

            bool hasServicesCompleteAdField =
                deviceInfo.ContainsKey(DeviceInfoType.ServicesCompleteListUuid128);

            if (!hasServicesCompleteAdField)
            {
                return false;
            }


            const string bleUartUuid = "43370001E80291A76D4050962F8FDB50";
            bool hasHidServiceUuid =
                    deviceInfo[DeviceInfoType.ServicesCompleteListUuid128].Contains(
                    bleUartUuid);

            if (!hasHidServiceUuid)
            {
                return false;
            }

            /* If we have reached here it means all the criterias have passed. */
            return true;
        }

        string GetNameOfDevice(BtDevice device)
        {
            IDictionary<DeviceInfoType, string> deviceInfo = device.DeviceInfo;

            string name = string.Empty;
            bool hasNameField =
                deviceInfo.ContainsKey(DeviceInfoType.CompleteLocalName);

            if(hasNameField)
            {
                name = deviceInfo[DeviceInfoType.CompleteLocalName];
            }

            return name;
        }

        /// <summary>
        /// Extract the device name from the advertising data.
        /// </summary>
        string GetDeviceName(IDictionary<DeviceInfoType, string> deviceInfo)
        {
            string deviceName = string.Empty;
            bool hasNameField = deviceInfo.ContainsKey(DeviceInfoType.CompleteLocalName);
            if (hasNameField)
            {
                deviceName = deviceInfo[DeviceInfoType.CompleteLocalName];
            }
            return deviceName;
        }



        ///////////////////////////////////////////////////////////////////////
        //
        // Sending Data Methods
        //
        ///////////////////////////////////////////////////////////////////////


        /// <summary>
        /// Are we able to send a packet? 
        /// </summary>
        /// <returns>true if ready to send, otherwise busy. Note: a previous timeout is not a hindrance to sending</returns>
        public bool CanSend()
        {
            UInt32 state = uartController.UartControllerDllGetSendState();

            return ((UInt32)E_UARC_MSG_SEND_STATE.UARC_MSS_BUSY != state);
        }


        /// <summary>
        /// Send a packet of type CMD
        /// </summary>
        /// <param name="payload"></param>
        /// <param name="timeoutMs"></param>
        /// <returns>true if we sent, else we are already busy</returns>
        public bool SendPacket(UarcMessageType msgType, Byte[] payload, UInt32 timeoutMs)
        {
            bool canSend = CanSend();

            if (canSend)
            {
                Byte[] bytesToSend = uartController.MakePacket(msgType, payload);

                this.StartSendData(bytesToSend);
                uartController.StartSendTimer(timeoutMs);
            }

            return canSend;
        }        


        /// Send a packet of type UARC_MT_CMD
        /// </summary>
        /// <param name="payload"></param>
        /// <param name="timeoutMs"></param>
        /// <returns>true if we sent, else we are already busy</returns>
        public bool SendCmdPacket(Byte[] payload, UInt32 timeoutMs)
        {
            return SendPacket(UarcMessageType.UARC_MT_CMD, payload, timeoutMs);
        }


        /// Send a packet of type UARC_MT_HTTP_RSP
        /// </summary>
        /// <param name="payload"></param>
        /// <param name="timeoutMs"></param>
        /// <returns>true if we sent, else we are already busy</returns>
        public bool SendHttpResponsePacket(Byte[] payload)
        {
            bool canSend = CanSend();

            if (canSend)
            {
                Byte[] bytesToSend = uartController.MakePacket(UarcMessageType.UARC_MT_HTTP_RSP, payload);

                this.StartSendData(bytesToSend);
            }

            return canSend;
        }

        /// <summary>
        /// Inactivity timer on Rx
        /// </summary>
        /// <returns>Time in ms</returns>
        public UInt32 GetTimeSinceLastRx()
        {
            return uartController.GetTimeSinceLastReceive();
        }

        /// <summary>
        /// Inactivity timer on Tx
        /// </summary>
        /// <returns>Time in ms</returns>
        public UInt32 GetTimeSinceLastTx()
        {
            return uartController.GetTimeSinceLastSend();
        }


        /// <summary>
        /// Send a packet of type PING (has simple payload)
        /// </summary>
        /// <returns>true if we sent, else we are already busy</returns>
        public bool SendPingPacket()
        {
            bool canSend = CanSend();
            byte[] payload = System.Text.Encoding.ASCII.GetBytes("PING");
            if (canSend)
            {
                Byte[] bytesToSend = uartController.MakePacket(UarcMessageType.UARC_MT_PING, payload);

                this.StartSendData(bytesToSend);
            }

            return canSend;
        }

        /// <summary>
        /// Register event handlers for MasterEmulator events.
        /// </summary>
        ///
        /// <returns>true if we sent, else we are already busy</returns>
        /// 
        public Int32 SendPingPacket(UInt32 pingIndex, PingType pingType, UInt32 paddingByteCount)
        {
            Int32 numBytesSent = 0;
            DateTime timeSend = DateTime.Now;
            StringBuilder sb = new StringBuilder();
            byte[] payload;

            bool canSend = CanSend();

            for (int i = 0; i < paddingByteCount; i++)
            {
                sb.Append("?");
            }

            switch (pingType)
            {
                // Has dateTime plus padding text of specified length
                case PingType.ExtendedPing:
                    payload = System.Text.Encoding.ASCII.GetBytes("PING " + pingIndex.ToString() + " " + timeSend.ToString("o", null) + " " + sb.ToString());
                    break;

                // "PING"

                default:
                case PingType.ShortPings:
                    payload = System.Text.Encoding.ASCII.GetBytes("PING ");
                    break;

                // Payload = padding of given size only
                case PingType.FixedPayloadLength:
                    payload = new byte[paddingByteCount];
                    for (int i = 0; i < paddingByteCount; i++)
                    {
                        payload[i] = (byte)randomDataValue.Next(0x21, 0x7F);
                    }

                    break;
            }

            if (canSend)
            {
                Byte[] bytesToSend = uartController.MakePacket(UarcMessageType.UARC_MT_PING, payload);

                this.StartSendData(bytesToSend);

                numBytesSent = bytesToSend.Length;
            }

            return numBytesSent;
        }


        /// <summary>
        /// Send a packet of type UARC_MT_ASYNC
        /// </summary>
        /// <param name="payload"></param>
        /// <param name="timeoutMs"></param>
        /// <returns>true if we sent, else we are already busy</returns>
        public bool SendAsyncPacket(Byte[] payload)
        {
            bool canSend = CanSend();

            if (canSend)
            {
                Byte[] bytesToSend = uartController.MakePacket(UarcMessageType.UARC_MT_ASYNC, payload);

                this.StartSendData(bytesToSend);
            }

            return canSend;
        }



        /// <summary>
        /// Send a packet of type NULL (has no payload)
        /// </summary>
        /// <returns>true if we sent, else we are already busy</returns>
        public bool SendNullPacket()
        {
            bool canSend = CanSend();
            if (canSend)
            {
                Byte[] bytesToSend = uartController.MakePacket(UarcMessageType.UARC_MT_NULL, null);

                this.StartSendData(bytesToSend);
            }

            return canSend;
        }

        /// <summary>
        /// Send msg to simulator to make it request a file
        /// Target will begin to reqest data from out HTTP mini-server.
        /// </summary>
        /// <returns></returns>
        public bool SendFirmwareFileRequest(Byte[] payloadBytes)
        {
            UInt32 timeoutMs = 8000;

            bool canSend = SendCmdPacket(payloadBytes, timeoutMs);

            return (canSend);
        } 

        /// <summary>
        /// Send data to peer device.
        /// </summary>
        /// <param name="value"></param>
        public void SendData(string value)
        {
            byte[] encodedBytes = Encoding.UTF8.GetBytes(value);

            if (encodedBytes.Length > maxPacketLength)
            {
                Array.Resize<byte>(ref encodedBytes, maxPacketLength);
                AddToLog("Max packet size is 20 characters, text is truncated.");
            }

            masterEmulator.SendData(pipeSetup.UartRxPipe, encodedBytes);

            string decodedString = Encoding.UTF8.GetString(encodedBytes);
            AddToLog(string.Format("TX: {0}", decodedString));
        }


        /// <summary>
        /// Start sending a data to the peer device.
        /// </summary>
        /// <param name="data">An arbitrarily large byte array of data to send.</param>
        /// <remarks>The method will continue to send until all data has been
        /// transmitted or the transmission has been stopped by <see cref="StopSendData"/>.
        /// </remarks>
        public void StartSendData(byte[] data)
        {
            const Int32 constSendThreadTimeoutMs = 5000;
            bool printLongSendTimes = false;    // set true for info on long sends
            int numberOfPackets = 0;
            UInt32[] PacketsSendTimes_ms = new UInt32[numberOfPackets];

            // Check if data is valid
            if (data == null)
            {
                throw new ArgumentNullException();
            }

            // FOR DEBUG
            string dataToSend = String.Join(String.Empty, Array.ConvertAll(data, x => (x.ToString("X2") + " ")));
            AddToLog(dataToSend);
            if (null != SendDataReadyToSend)
            {
                SendDataReadyToSend(this, new OutputReceivedEventArgs(dataToSend));
            }

            // Wait for the thread to complete if it already was running. (sendThreadEvent to be set)
            bool eventReceived = sendThreadEvent.WaitOne(constSendThreadTimeoutMs);
            if (!eventReceived)
            {
                Console.WriteLine("ERROR send thread timeout in {0}ms, StartSendData Failed", constSendThreadTimeoutMs);
                return;
            }

            // This should never happen if the Wait worked.
            if (taskRunning)
            {
                Console.WriteLine("ERROR ************** Task already running");
            }

            DateTime dtStart = DateTime.Now;

            IList<byte[]> splitData = SplitDataNoCounter(data, 20);

            sendData = true;

            /* Starting a task to perform the sending of packets asynchronouly.
             * The SendDataCompleted event will notify the application when ready. */
            Task.Factory.StartNew(() =>
            {
                try
                {
                    SendDataStarted(this, EventArgs.Empty);

                    numberOfPackets = splitData.Count;
                    PacketsSendTimes_ms = new UInt32[numberOfPackets];
                    int progressInPercent = 0;

                    for (int i = 0; i < numberOfPackets; i++)
                    {
                        if (!sendData)
                        {
                            break;
                        }

                        /* Send one packet of data on the UartRx pipe. */
                        DateTime dtStartPktSend = DateTime.Now;
                        bool success = masterEmulator.SendData(pipeSetup.UartRxPipe, splitData[i]);
                        if(!success)
                        {
                            SendDataFailed(this, EventArgs.Empty);
                        }
                        TimeSpan tsPktSend = DateTime.Now - dtStartPktSend;
                        PacketsSendTimes_ms[i] = (UInt32)tsPktSend.TotalMilliseconds;

                        int currentProgressInPercent = ((i + 1) * 100) / numberOfPackets;

                        if (currentProgressInPercent > progressInPercent)
                        {
                            progressInPercent = currentProgressInPercent;
                            ProgressUpdated(this, new ValueEventArgs<int>(progressInPercent));
                        }
                    }
                }
                catch (Exception ex)
                {
                    AddToLog("Sending of data failed.");
                    Trace.WriteLine(ex.ToString());
                    SendDataFailed(this, EventArgs.Empty);
                    taskRunning = false;
                    sendThreadEvent.Set(); 
                    return;
                }

                DateTime dtEnd = DateTime.Now;
                TimeSpan ts1 = dtEnd - dtStart;

                // If send took a long time, optionally print time for each pkt.
                if (printLongSendTimes)
                {
                    if (ts1.TotalMilliseconds > 300)
                    {
                        Console.WriteLine("\ntSend = {0}ms\n", ts1.TotalMilliseconds);
                        for (int i = 0; i < numberOfPackets; i++)
                        {
                            Console.WriteLine("T{0} = {1}ms", i, PacketsSendTimes_ms[i]);
                        }
                    }
                }

                SendDataCompleted(this, EventArgs.Empty);

                // Task completed. OK to start another one.
                taskRunning = false;
                sendThreadEvent.Set();  
            });
        }

        /// <summary>
        /// Start to send data to the peer device.
        /// </summary>
        /// <param name="data">A byte array no larger than max packet size.</param>
        /// <param name="numberOfRepetitions">The number of times to repeat the packet.</param>
        public void StartSendData(byte[] data, int numberOfRepetitions)
        {
            if (data == null)
            {
                throw new ArgumentNullException();
            }

            if (data.Length > maxPayloadLength)
            {
                throw new ArgumentException(string.Format("Length of data must not exceed {0}.",
                    maxPayloadLength));
            }

            int totalPacketSize = data.Length * numberOfRepetitions;

            var aggregatedData = new List<byte>(totalPacketSize);

            for (int i = 0; i < numberOfRepetitions; i++)
            {
                aggregatedData.AddRange(data);
            }

            StartSendData(aggregatedData.ToArray());
        }

        /// <summary>
        /// Split to max length packets and leave room for counter values.
        /// Note - not used for UART Controller - do not need counter. See function below.
        /// </summary>
        IList<byte[]> SplitDataAndAddCounter(byte[] data, int partSize)
        {
            /* Collection of packets split out from source array. */
            IList<byte[]> packets = new List<byte[]>();

            /* Counter value, incremented for each new packet. */
            int counter = 0;

            /* Index of counter field in packet. */
            const int counterIndex = 0;

            /* Last index where a packet of full length may start. */
            /* Note const and readonly won't work here since const is compile-time const and */
            /* readonly requires this to be a member. Since SplitDataAndAddCounter is called multiple*/
            /* times, it breaks the readonly usage as well. */
            int lastFullPacketIndex = (data.Length - partSize);

            /* Current index in source array. */
            int index;

            for (index = 0; index < lastFullPacketIndex; index += partSize)
            {
                byte[] packet = new byte[maxPacketLength];
                Array.Copy(data, index, packet, counterFieldLength, partSize);

                InjectCounter(counter, counterIndex, packet);

                packets.Add(packet);

                counter += 1;
            }

            /* Special treatment of last packet. */
            int lastPacketPayloadSize = (data.Length - index);
            byte[] lastPacket = new byte[maxPacketLength];
            Array.Copy(data, index, lastPacket, counterFieldLength, lastPacketPayloadSize);
            InjectCounter(counter, counterIndex, lastPacket);

            packets.Add(lastPacket);

            return packets;
        }



        /// <summary>
        /// Split to max length packets without any pre-count --DEW
        /// </summary>
        IList<byte[]> SplitDataNoCounter(byte[] data, int partSize)
        {
            /* Collection of packets split out from source array. */
            IList<byte[]> packets = new List<byte[]>();

            /* Last index where a packet of full length may start. */
            /* Note const and readonly won't work here since const is compile-time const and */
            /* readonly requires this to be a member. Since SplitDataAndAddCounter is called multiple*/
            /* times, it breaks the readonly usage as well. */
            int lastFullPacketIndex = (data.Length - partSize);

            /* Current index in source array. */
            int index;

            for (index = 0; index < lastFullPacketIndex; index += partSize)
            {
                byte[] packet = new byte[maxPacketLength];
                Array.Copy(data, index, packet, 0, partSize);

                packets.Add(packet);
            }

            /* Special treatment of last packet. */
            int lastPacketPayloadSize = (data.Length - index);
            byte[] lastPacket = new byte[maxPacketLength];
            Array.Copy(data, index, lastPacket, 0, lastPacketPayloadSize);

            packets.Add(lastPacket);

            return packets;
        }


        /// <summary>
        /// Insert 16 bit counter in Least Significant Byte order.
        /// </summary>
        void InjectCounter(int counter, int index, byte[] packet)
        {
            byte leastSignificantByte = (byte)(counter & 0xFF);
            byte mostSignificantByte = (byte)((counter >> 8) & 0xFF);

            packet[index] = leastSignificantByte;
            packet[index + 1] = mostSignificantByte;
        }

        /// <summary>
        /// Signal StartSendData task to cancel sending of data.
        /// </summary>
        public void StopSendData()
        {
            sendData = false;
        }

    }
}
