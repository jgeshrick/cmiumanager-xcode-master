@echo off

:: ========================================================================
:: (c) Sagentia Limited, 2011.  All rights reserved.   
::
::  This software is the property of Sagentia Limited and may 
::  not be copied or reproduced otherwise than on to a single hard disk for 
::  backup or archival purposes.  The source code is confidential  
::  information and must not be disclosed to third parties or used without  
::  the express written permission of Sagentia Limited. 
::  
::   $Workfile:  $  
::   $Author: rmenzies $  
::   $Date:  $  
::   $Revision:  $  
::
::=========================================================================


::
:: Define operating environment
::
SET mappedDrive=L
SET arguments=%*

:DISPLAY_CURRENT_DRIVES
ECHO.
ECHO Checking current drives...
FSUTIL fsinfo drives
IF NOT EXIST %mappedDrive%: GOTO MAP_DRIVE

:DELETE_MAPPED_DRIVE
ECHO.
ECHO Deleting drive %mappedDrive%:...
NET USE /delete %mappedDrive%:
ECHO Checking current drives...
FSUTIL fsinfo drives

:MAP_DRIVE
ECHO.
ECHO (Re-)Mapping drive %mappedDrive%:...
NET USE %mappedDrive%: \\wabash\Keil /user:slbntdom\L900test N3ptun31234
ECHO Checking current drives...
FSUTIL fsinfo drives

:DISPLAY_LICENSE_FILE
ECHO.
ECHO License folder contents:
DIR %mappedDrive%:\License\ARM


::
:: Do the build
::
DoKeilBuild.bat %arguments%