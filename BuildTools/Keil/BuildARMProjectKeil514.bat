@echo off

:: ========================================================================
:: (c) Sagentia Limited, 2011.  All rights reserved.   
::
::  This software is the property of Sagentia Limited and may 
::  not be copied or reproduced otherwise than on to a single hard disk for 
::  backup or archival purposes.  The source code is confidential  
::  information and must not be disclosed to third parties or used without  
::  the express written permission of Sagentia Limited. 
::  
::   $Workfile:  $  
::   $Author: rmenzies $  
::   $Date:  $  
::   $Revision:  $  
::
::=========================================================================


::
:: Define operating environment
::
SET keilExePath=%Keil.Uv5.exe%
IF [%keilExePath%]==[] SET keilExePath="C:\Program Files (x86)\Keil\ARM\v5.14\UV4\UV4.exe"
SET arguments=%*


::
:: Do the build
::
BuildARMProject.bat %arguments%
