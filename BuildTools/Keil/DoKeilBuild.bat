@echo off

:: ========================================================================
:: (c) Sagentia Limited, 2011.  All rights reserved.   
::
::  This software is the property of Sagentia Limited and may 
::  not be copied or reproduced otherwise than on to a single hard disk for 
::  backup or archival purposes.  The source code is confidential  
::  information and must not be disclosed to third parties or used without  
::  the express written permission of Sagentia Limited. 
::  
::   $Workfile:  $  
::   $Author: rmenzies $  
::   $Date:  $  
::   $Revision:  $  
::
::=========================================================================

::
:: Define script return code values
::
SET noError=0
SET invalidArgumentsSpecified=1
SET noKeilOutput=2
SET projectFileNotFound=3
SET keilNotInstalled=4
SET keilBuildFailed=5
SET keilCleanFailed=6
SET badScriptEnvironment=7


::
:: Define Keil-specific return code variables
::
SET keilErrorNoErrorsOrWarnings=0
SET keilErrorWarningsOnly=1
SET keilErrorNonFatalErrors=2
SET keilErrorFatalErrors=3
SET keilErrorNoProjectFileForWriting=11
SET keilErrorDeviceNotFound=12
SET keilErrorProjectFileWriteFailed=13
SET keilErrorXmlImportFailed=15
SET keilBuildErrorLicenseInconsistency=224

::
:: Define operating environment.
::
SET returnCode=%noError%
SET keilReturnCode=%keilErrorNoErrorsOrWarnings%
SET keilOutputLogFileName=KeilBuild.log
SET keilOutputFolder=Build


::
:: Process script arguments:
::
:: * Check that there are enough arguments
:: * Check that the operating environment is valid
:: * Extract the working directory required for the Keil build
:: * Change directory to be with respect to the build directory
::
:PROCESS_SCRIPT_ARGUMENTS
ECHO Arguments are %*
IF [%1]==[] GOTO INVALID_ARGUMENTS
IF NOT EXIST %keilExePath% GOTO KEIL_NOT_FOUND
IF [%keilExePath%]==[] GOTO BAD_SCRIPT_ENVIRONMENT
FOR /F %%A IN ("%1") DO (
	SET keilProjectFileFolder=%%~dpA 
	SET keilProjectFileName=%%~nA%%~xA )
ECHO Project file folder = %keilProjectFileFolder%
ECHO Project file name = %keilProjectFileName%
ECHO Changing working directory to "%keilProjectFileFolder%"...
PUSHD %keilProjectFileFolder%
IF NOT EXIST %keilProjectFileName% GOTO KEIL_PROJECT_FILE_NOT_FOUND
SET keilTargetConfiguration=%2


::
:: Clean the output folders as required.
::
:CHECK_FOR_OUTPUT_FOLDER
SET folderToClean=
IF [%keilTargetConfiguration%]==[] SET folderToClean=%keilOutputFolder%
IF [%folderToClean%] NEQ [] GOTO CLEAN_IF_REQUIRED
SET folderToClean=%keilOutputFolder%\%keilTargetConfiguration%

:CLEAN_IF_REQUIRED
IF NOT EXIST %folderToClean% GOTO PREPARE_BUILD
ECHO Cleaning keil output folder (%folderToClean%)...
ECHO Deleting files...
DEL /S /Q /F %folderToClean% > NUL
IF %ERRORLEVEL% NEQ 0 GOTO KEIL_CLEAN_FAILED
ECHO Removing directory...
RMDIR /S /Q %folderToClean%
IF %ERRORLEVEL% NEQ 0 GOTO KEIL_CLEAN_FAILED


::
:: The rebuild feature of the -z option for the Keil command-line builder doesn't
:: rebuild for all targets (bug?).  To work around this, we delete the output folder
:: directly to force a complete rebuild.
::
:PREPARE_BUILD
IF [%keilTargetConfiguration%]==[] GOTO PREPARE_TO_BUILD_ALL_KEIL_TARGETS
GOTO PREPARE_TO_BUILD_SINGLE_KEIL_TARGET

:PREPARE_TO_BUILD_ALL_KEIL_TARGETS
ECHO Rebuilding all Keil project targets...
SET keilArguments=-z
GOTO DO_KEIL_BUILD

:PREPARE_TO_BUILD_SINGLE_KEIL_TARGET
ECHO Building Keil project target %keilTargetConfiguration%...
SET keilArguments=-t %keilTargetConfiguration%


::
:: Run Keil in command-line mode:
::
:: * Command-line build
:: * Hide the GUI during the build
:: * Write the build output to file
:: * Store the Keil return code
::
:: See http://www.keil.com/support/man/docs/uv4/uv4_commandline.htm for more information.
::
:DO_KEIL_BUILD
SET commandToExecute=%keilExePath% -b -j0 -o %keilOutputLogFileName% %keilArguments% %keilProjectFileName%
ECHO.
ECHO Running %commandToExecute%...
%commandToExecute%
SET keilReturnCode=%ERRORLEVEL%


::
:: Display the output logfile to put it on the command line for user reference
::
:DISPLAY_KEIL_BUILD_OUTPUT
IF NOT EXIST %keilOutputLogFileName% GOTO NO_KEIL_OUTPUT
ECHO.
TYPE %keilOutputLogFileName%
ECHO Keil exited with return code %keilReturnCode%.


::
:: Handle the build return code.
::
:: * Allow warnings for now (Keil returns 1).
:: * On error, display the Keil return codes for the benefit of the user.
:: * If there were errors, parse the build log and look for specific errors
::   worth extracting explicitly for the user and publishing through TeamCity.
::
:PROCESS_KEIL_RETURN_CODE
IF %keilReturnCode% GTR 1 SET returnCode=%keilBuildFailed%
IF %returnCode% EQU %noError% GOTO CLEAN_UP
ECHO Parsing build output in %keilOutputLogFileName%...
SET lastKeilLicenseError=0
FOR /F "tokens=1-6,*" %%a IN (%keilOutputLogFileName%) DO (
	IF "%%c"=="C500:" IF "%%f"=="(%keilBuildErrorLicenseInconsistency%:" SET lastKeilLicenseError=%keilBuildErrorLicenseInconsistency%
)
IF %lastKeilLicenseError% EQU 0 GOTO DISPLAY_KEIL_CODES
IF %TEAMCITY% NEQ 1 GOTO DISPLAY_KEIL_CODES
IF lastKeilLicenseError EQU %keilBuildErrorLicenseInconsistency% ECHO ##teamcity[buildStatus status='FAILURE' text='Keil License Inconsistency ({build.status.text})']


::
:: Show all the known error codes that Keil can return and identify specific
:: build errors that are worth decoding for the user.
::
:DISPLAY_KEIL_CODES
ECHO.
ECHO    Keil Return Codes:    %keilErrorNoErrorsOrWarnings% - No errors or warnings.
ECHO                          %keilErrorWarningsOnly% - Build warnings generated.
ECHO                          %keilErrorNonFatalErrors% - Non-fatal build errors generated.
ECHO                          %keilErrorFatalErrors% - Fatal build errors generated.
ECHO                          %keilErrorNoProjectFileForWriting% - Project file not found for writing.
ECHO                          %keilErrorDeviceNotFound% - Device not found.
ECHO                          %keilErrorProjectFileWriteFailed% - Project file write failed.
ECHO                          %keilErrorXmlImportFailed% - XML import failed.
ECHO.
ECHO    Keil Build Errors:	  %keilBuildErrorLicenseInconsistency% - The Keil license manager thinks that you're
ECHO                                trying a fast one with the license.  This can happen when the license
ECHO                                is accessed on the same machine from different user accounts.  The license
ECHO                                will now be locked out for 3 hours.
GOTO DISPLAY_RETURN_CODES


:: 
:: Handle errors
::
:INVALID_ARGUMENTS
SET returnCode=%invalidArgumentsSpecified%
ECHO.
ECHO Invalid arguments specified, please see usage information below.
GOTO USAGE

:NO_KEIL_OUTPUT
SET returnCode=%noKeilOutput%
GOTO DISPLAY_RETURN_CODES

:KEIL_PROJECT_FILE_NOT_FOUND
ECHO.
ECHO The project file specified (%keilProjectFileName%) could not be found.
SET returnCode=%projectFileNotFound%
GOTO DISPLAY_RETURN_CODES

:KEIL_NOT_FOUND
ECHO.
ECHO The Keil executable (%keilExePath%) could not be found
SET returnCode=%keilNotInstalled%
GOTO DISPLAY_RETURN_CODES

:KEIL_CLEAN_FAILED
ECHO.
ECHO The project output folder could not be cleaned successfully.
SET returnCode=%keilCleanFailed%
GOTO DISPLAY_RETURN_CODES

:BAD_SCRIPT_ENVIRONMENT
SET returnCode=%badScriptEnvironment%
ECHO The script environment is not correctly configured.  Be sure that the
ECHO keilExePath variable has been set outside %0.
GOTO DISPLAY_RETURN_CODES


::
:: Output usage information
::
:USAGE
ECHO.
ECHO                  Usage:   %0 [Keil Project Path] [Keil Target]
ECHO.
ECHO    [Keil Project Path]:   An absolute or relative path to the Keil project file that you want to re-build.
ECHO.
ECHO          [Keil Target]:   Optional.  Provides for re-build of single Keil target (a.k.a build configuration)
ECHO.
ECHO          Example usage:   %0 MetrologyApplication.uvproj MetrologyApplicationDebugLog
ECHO                           %0 RestrictedApplication.uvproj
ECHO.
ECHO            Description:   
ECHO                           
ECHO.


::
:: Output return codes description
::
:DISPLAY_RETURN_CODES
ECHO.
ECHO		Return codes:     %noError% - The operation completed successfully.
ECHO                           %invalidArgumentsSpecified% - Invalid arguments specified.  See usage instructions.
ECHO                           %noKeilOutput% - %keilOutputLogFileName% was not found.
ECHO                           %projectFileNotFound% - %keilProjectFileName% was not found.
ECHO                           %keilNotInstalled% - %keilExePath% was not found.
ECHO                           %keilBuildFailed% - The Keil build failed with return code %keilReturnCode%.
ECHO                               See the Keil return codes for more information.
ECHO                           %keilCleanFailed% - The Keil project output folder (%keilOutputFolder%)
ECHO                               could not be cleaned.
ECHO                           %badScriptEnvironment% - The batch scripting environment has not been
ECHO                               configured correctly.
ECHO.


::
:: Clean up and exit.
:: We don't set ERRORLEVEL directly, this screws up return code handling for all other commands.
:: If the TEAMCITY environment variable is defined as 1 (this is set in build configuration
:: settings under "Properties and Environment Variables"), then exit the entire command process
:: i.e. don't use the /B switch.
::
:CLEAN_UP
ECHO Reverting working directory...
POPD
IF NOT %returnCode%==%noError% GOTO ECHO_ERRORLEVEL
ECHO Keil build executed successfully.
GOTO END

:ECHO_ERRORLEVEL
ECHO Script exiting with return code %returnCode%...

:END
IF [%TEAMCITY%]==[1] EXIT %returnCode%
EXIT /B %returnCode%
