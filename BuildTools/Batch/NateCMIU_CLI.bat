@echo off

:: ========================================================================
:: (c) Sagentia Limited, 2011.  All rights reserved.   
::
::  This software is the property of Sagentia Limited and may 
::  not be copied or reproduced otherwise than on to a single hard disk for 
::  backup or archival purposes.  The source code is confidential  
::  information and must not be disclosed to third parties or used without  
::  the express written permission of Sagentia Limited. 
::  

 
::   $Author: nathan.loden $  
::   $Date: 2016-02-04 $  
::   $Revision: 1 $  
::
::=========================================================================


::
:: Define script return code values
::
SET noError=0


::
:: Define operating environment.
::
SET returnCode=%noError%
SET artifactPath=Artifacts


::
:: Process script arguments:
::
:::PROCESS_SCRIPT_ARGUMENTS
::IF [%1]==[] GOTO ERROR
::IF [%2]==[] GOTO ERROR
::IF [%3]==[] GOTO ERROR
::SET targetPath=%1
::SET targetFileExtension=%2
::SET searchPath=%targetPath%%targetFileExtension%
::SET versionString=%3


::
:: Create output folder if it doesn't already exist
::
cd \
cd SEGGER
jlink.exe -commanderscript downloadblink.jlink
GOTO CLEAN_UP
::
:: Handle Errors
::
:ERROR
ECHO Some error occurred!
SET returnCode=1


::
:: Clean up and exit.
:: We don't set ERRORLEVEL directly, this screws up return code handling for all other commands.
:: If the TEAMCITY environment variable is defined as 1 (this is set in build configuration
:: settings under "Properties and Environment Variables"), then exit the entire command process
:: i.e. don't use the /B switch.
::
:CLEAN_UP
IF NOT %returnCode%==%noError% GOTO ECHO_ERRORLEVEL
ECHO %collatedCount% artifacts collated successfully.
GOTO END

:ECHO_ERRORLEVEL
ECHO Script exiting with return code %returnCode%...

:END
IF [%TEAMCITY%]==[1] EXIT %returnCode%
EXIT /B %returnCode%
