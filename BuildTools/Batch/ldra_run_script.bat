@echo off
setlocal enabledelayedexpansion

:: What will we call this test report?
set myset=CMIU_v%1
echo %myset%

set my_src_dir=%2
if [%my_src_dir%] == [] set my_src_dir=..\..\CMIU\firmware\src\main\c\src
echo my_src_dir

:: Get current local directory. Assume we run this script from BuildTools\Batch
set local_dir=%CD%
echo %local_dir%
:: Firmware source files are relative to BuildTools\Batch like so:
set firmware_source_dir=%local_dir%\%my_src_dir%
echo %firmware_source_dir%

:: Find all of the C source files in the src directory.
set source_c_files=
for %%f in (%firmware_source_dir%\*.c)  do (
    set source_c_files=!source_c_files!/add_set_file="%%f" 
)
echo %source_c_files%

:: Find all of the head files in the src directory.
set source_h_files=
for %%f in (%firmware_source_dir%\*.h)  do (
    set source_h_files=!source_h_files!/add_set_file="%%f" 
)
echo %source_h_files%


cd /D C:\LDRA_Toolsuite

start /wait contestbed %myset% /create_set=group /1q

start /wait contestbed %myset% %source_c_files% \1q

start /wait contestbed %myset% %source_h_files% \1q

start /wait contestbed %myset% /112a36q

cd /D %local_dir%
