
@echo off

SET noError=0
SET returnCode=%noError%
SET exeFile=ThirdPartyTools\NordicSemiconductor\nrf51\bin\mergehex.exe
SET rootFolder=CmiuSimulator\BtleServer
SET nordicSoftcoreFile=%rootFolder%\src\main\resources\s110_nrf51822_7.1.0_softdevice.hex
SET folderOfFilesToMerge=%rootFolder%\target\Artifacts
SET folderForMergedFiles=%rootFolder%\target\MergedArtifacts

:CREATE_OUTPUT_FOLDER
IF EXIST %folderForMergedFiles% GOTO CLEAN_OUTPUT_FOLDER
ECHO Creating output folder %folderForMergedFiles%...
MKDIR %folderForMergedFiles%
IF %ERRORLEVEL% NEQ 0 GOTO ERROR
GOTO MERGE_FILES

:CLEAN_OUTPUT_FOLDER
ECHO Cleaning output folder %folderForMergedFiles% of files...
DEL /S /Q /F %folderForMergedFiles%\*.* > NUL

:MERGE_FILES
ECHO Merging hex files in %folderOfFilesToMerge% with %nordicSoftcoreFile%...
ECHO.
SET mergedCount=0
for /r %%i in (%folderOfFilesToMerge%\*.hex) do (
	ECHO Merging %%~ni...
    %exeFile% -m %nordicSoftcoreFile% "%%i" -o "%folderForMergedFiles%\%%~ni Merged%%~xi"
    SET /A mergedCount+=1
)
GOTO CLEAN_UP

:ERROR
ECHO Error :(

::
:: Clean up and exit.
:: We don't set ERRORLEVEL directly, this screws up return code handling for all other commands.
:: If the TEAMCITY environment variable is defined as 1 (this is set in build configuration
:: settings under "Properties and Environment Variables"), then exit the entire command process
:: i.e. don't use the /B switch.
::
:CLEAN_UP
IF NOT %returnCode%==%noError% GOTO ECHO_ERRORLEVEL
ECHO Merged %mergedCount% files successfully.
GOTO END

:ECHO_ERRORLEVEL
ECHO Script exiting with return code %returnCode%...

:END
IF [%TEAMCITY%]==[1] EXIT %returnCode%
EXIT /B %returnCode%
