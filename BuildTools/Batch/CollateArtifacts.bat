@echo off

:: ========================================================================
:: (c) Sagentia Limited, 2011.  All rights reserved.   
::
::  This software is the property of Sagentia Limited and may 
::  not be copied or reproduced otherwise than on to a single hard disk for 
::  backup or archival purposes.  The source code is confidential  
::  information and must not be disclosed to third parties or used without  
::  the express written permission of Sagentia Limited. 
::  
::   $Workfile: UART_Codec.h $  
::   $Author: rupert.menzies $  
::   $Date: 2011-10-13 18:35:21 +0100 (Thu, 13 Oct 2011) $  
::   $Revision: 2 $  
::
::=========================================================================


::
:: Define script return code values
::
SET noError=0


::
:: Define operating environment.
::
SET returnCode=%noError%
SET artifactPath=Artifacts


::
:: Process script arguments:
::
:PROCESS_SCRIPT_ARGUMENTS
IF [%1]==[] GOTO ERROR
IF [%2]==[] GOTO ERROR
IF [%3]==[] GOTO ERROR
SET targetPath=%1
SET targetFileExtension=%2
SET searchPath=%targetPath%%targetFileExtension%
SET versionString=%3


::
:: Create output folder if it doesn't already exist
::
:CREATE_OUTPUT_FOLDER
IF EXIST %artifactPath% GOTO CLEAN_OUTPUT_FOLDER
ECHO Creating output folder %artifactPath%...
MKDIR %artifactPath%
IF %ERRORLEVEL% NEQ 0 GOTO ERROR
GOTO PREPARE_TO_COLLATE


::
:: The folder did exist, so delete it's contents
::
:CLEAN_OUTPUT_FOLDER
ECHO Cleaning output folder %artifactPath% of %targetFileExtension% files...
DEL /S /Q /F %artifactPath%\*%targetFileExtension% > NUL
IF %ERRORLEVEL% NEQ 0 GOTO ERROR


::
:: Prepare for one of the two possible collate actions depending on whether or
:: not the user specified a version string in the command.  If not, just copy
:: the file into the target folder.
::
:PREPARE_TO_COLLATE
IF [%versionString%]==[] GOTO CONFIGURE_FOR_COPY
:CONFIGURE_FOR_COPY_AND_SUFFIX
SET copyCommand=COPY %%f "%artifactPath%\%%~nf %versionString%%%~xf"
GOTO COLLATE_ARTIFACTS
:CONFIGURE_FOR_COPY
SET copyCommand=COPY %%f %artifactPath%
GOTO COLLATE_ARTIFACTS


::
:: Recurse the working directory looking for all files.  When found,
:: run the pre-configured copy command.
::
:COLLATE_ARTIFACTS
SET collatedCount=0
ECHO Collating artifacts...
FOR /D /r %%d IN (*) DO (
	FOR %%f IN (%%d\%searchPath%) DO (
		%copyCommand%
		SET /A collatedCount+=1 ) )
IF %ERRORLEVEL% NEQ 0 GOTO ERROR
GOTO CLEAN_UP


::
:: Handle Errors
::
:ERROR
ECHO Some error occurred!
SET returnCode=1


::
:: Clean up and exit.
:: We don't set ERRORLEVEL directly, this screws up return code handling for all other commands.
:: If the TEAMCITY environment variable is defined as 1 (this is set in build configuration
:: settings under "Properties and Environment Variables"), then exit the entire command process
:: i.e. don't use the /B switch.
::
:CLEAN_UP
IF NOT %returnCode%==%noError% GOTO ECHO_ERRORLEVEL
ECHO %collatedCount% artifacts collated successfully.
GOTO END

:ECHO_ERRORLEVEL
ECHO Script exiting with return code %returnCode%...

:END
IF [%TEAMCITY%]==[1] EXIT %returnCode%
EXIT /B %returnCode%
