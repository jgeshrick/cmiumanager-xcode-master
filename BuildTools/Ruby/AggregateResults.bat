@echo off

SET rubyExe="ruby.exe"
SET csvOutputFile=AggregatedResults.csv

ECHO Aggregating results to %csvOutputFile%...
CALL %rubyExe% results_aggregator.rb > %csvOutputFile%

ECHO Converting csv to html...
CALL %rubyExe% csv2table.rb %csvOutputFile%