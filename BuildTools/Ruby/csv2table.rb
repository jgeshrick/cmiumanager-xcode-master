#= CSV -> table 変換
# ：CSV ファイルを table タグに変換する
#   第１引数：変換対象の CSV ファイル名
#   第２引数：align を c,l,l,r,r,l のように指定（省略可、デフォルトは "l"）
#---------------------------------------------------------------------------------
#++
require 'csv'

# [CLASS] 引数
class Arg
  def initialize
  end

  # 引数取得
  def get_args
    begin
      # CSV ファイル名取得
      @csv_file = ARGV[0]
      # CSV ファイル存在チェック
      unless File.exist?(@csv_file)
        puts "[ERROR] Can't found #{@csv_file}"
        exit
      end

    rescue => e
      STDERR.puts "[ERROR][#{self.class.name}.get_args] #{e}"
      exit 1
    end
  end

  # CSV ファイル名返却
  def get_csv_file
    return @csv_file
  end

end

# [CLASS] CSV -> table 変換
class Csv2Table
  def initialize(csv_file)
    @csv_file = csv_file
    @html_file = File.dirname(csv_file) + "/" +
      File.basename(csv_file, File.extname(csv_file)) + ".html"
  end

  def success_fail_html(s)
    if s == "SUCCESS"
      return "<span class='success'>#{s}</span>"
    elsif s == "UNKNOWN"
      return "<span class='unknown'>#{s}</span>"
    elsif s == "FAILURE"
      return "<span class='failure'>#{s}</span>"
    end
    return s
  end


  def highlight_test_name_html(s)
    if match = s.match(/(^[A-Za-z_]+) \((.*)\)$/)
      testName, testSet = match.captures
      return "<span class='test-name'>#{testName}</span> (<span class='test-set'>#{testSet}</span>)</span>"
    elsif match = s.match(/(^.*)\.(.*$)/)
      testSet, testName = match.captures
      return "<span class='test-set'>#{testSet}</span>.<span class='test-name'>#{testName}</span>"
    elsif match = s.match(/(^.*)_(.*$)/)
      testSet, testName = match.captures
      return "<span class='test-set'>#{testSet}</span>_<span class='test-name'>#{testName}</span>"
    else
      return "<span class='test-name'>#{s}</span>"
    end
  end
  
  
  # CSV -> table 変換
  def convert
    begin
      # CSV ファイル読み込み
      csv = CSV.open(@csv_file, "r")

      # HTML ファイル書き込み
      html = File.open(@html_file, "w")
      html.puts "<!DOCTYPE html>"
      html.puts "<html>"
      html.puts "  <head>"
      # TODO - add style reference: borders, configure column widths
      html.puts "  <title>Aggregated MSPD Integration and Unit Test Results</title>"
      html.puts "  "
      html.puts "  <style>"
      html.puts "  "
      html.puts "  body {"
      html.puts "      background-color: #434343;"
      html.puts "      color: #efefef;"
      html.puts "      font-family: Arial,sans-serif;"
      html.puts "  }"
      html.puts "  a {"
      html.puts "      color: #ff9900;"
      html.puts "  }"
      html.puts "  a:visited {"
      html.puts "      color: #ef8900;"
      html.puts "  }"
      html.puts "  .content {"
      html.puts "      height: 100%;"
      html.puts "  }"
      html.puts "  table {"
      html.puts "      border-collapse: collapse; table-layout: fixed;"
      html.puts "  }"
      html.puts "  table thead tr th {"
      html.puts "      background-color: #ff9900;"
      html.puts "      border-left: 1px solid black;"
      html.puts "      border-right: 1px solid black;"
      html.puts "      color: white;"
      html.puts "      font-weight: normal;"
      html.puts "      padding: 2px;"
      html.puts "  }"
      html.puts "  table thead tr th.project { width: 20ex;}"
      html.puts "  table thead tr th.build-config { width: 50ex;}"
      html.puts "  table tbody tr th {"
      html.puts "      border-left: 1px solid black;"
      html.puts "      border-right: 1px solid black;"
      html.puts "      color: white;"
      html.puts "      font-weight: normal;"
      html.puts "      padding: 2px;"
      html.puts "  }"
      html.puts "  table tbody tr td {"
      html.puts "      border-left: 1px solid black;"
      html.puts "      border-right: 1px solid black;"
      html.puts "      color: white;"
      html.puts "      font-weight: normal;"
      html.puts "      padding: 2px 1ex 2px 1ex;"
      html.puts "  }"
      html.puts "  "
      html.puts "  span.success {"
      html.puts "      color: #50FF00;"
      html.puts "  }"
      html.puts "  span.failure {"
      html.puts "      color: red;"
      html.puts "  }"
      html.puts "  span.unknown {"
      html.puts "      color: gray;"
      html.puts "  }"
      html.puts "  span.test-name {"
      html.puts "      font-weight: bold; color: #ff9900;"
      html.puts "  }"
      html.puts "  h1 {"
      html.puts "      font-size: 1.8em;"
      html.puts "      text-align: center;"
      html.puts "  }"
      html.puts "  "
      html.puts "  </style>"
      
      
      html.puts "  </head>"
      html.puts "  <body>"
      generationMessage = "<p>These results aggregated on " + Time.now.strftime("%F at %T") + ".</p>"
      html.puts generationMessage
      
      # TODO - add summary of results here
      html.puts "    <table>"
      csv.each_with_index do |row, rowIndex|
        if rowIndex == 0
            html.puts "      <thead><tr>"
            row.each_with_index do |col, i|
              html.puts "        <th>#{col}</th>"
            end
            html.puts "      </tr></thead><tbody>"
            
        else
            html.puts "      <tr>"
            html.print "        <td nowrap>#{row[0]}</td>"
            html.print "        <td nowrap>#{row[1]}</td>"
            html.print "        <td>#{highlight_test_name_html(row[2])}</td>"
            html.print "        <td>#{success_fail_html(row[3])}</td>"
            html.print "        <td>#{row[4]}</td>"
            html.puts "      </tr>"
            
        end
      end
      html.puts "    </tbody></table>"
      html.puts "  </body>"
      html.puts "</html>"
    rescue => e
      STDERR.puts "[ERROR][#{self.class.name}.convert] #{e}"
      exit 1
    end
  end

private

end

begin
  # 引数取得
  obj_args = Arg.new
  obj_args.get_args
  csv_file = obj_args.get_csv_file

  # CSV -> table 変換
  obj_main = Csv2Table.new(csv_file)
  obj_main.convert
rescue => e
  STDERR.puts "[EXCEPTION] #{e}"
  exit 1
end
