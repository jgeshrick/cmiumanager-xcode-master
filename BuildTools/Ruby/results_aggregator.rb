require 'net/http'
require 'json'
require 'csv'

TEAMCITY_ROOT = 'http://teamcity.neptunetg.com:8111'
TEAMCITY_USERNAME = 'test.reporter'
TEAMCITY_PASSWORD = 'test.reporter'

BUILD_CONFIGURATIONS = [
  'Mspd_Trunk_IntegrationTest_IntegrationTests_BleIntegrationTest',
  'Mspd_Trunk_IntegrationTest_IntegrationTests_BtleIntegrationTestWithBonding',
  'Mspd_Common_Trunk_IntegrationTest_PacketCodeCrossCheckJavaAndC',
  'Mspd_Trunk_IntegrationTest_IntegrationTests_MdceIntegrationTest',
  #'Mspd_Trunk_IntegrationTest_IntegrationTests_OnTargetTests',
  'Mspd_Trunk_IntegrationTest_IntegrationTests_CellularNetworkIntegrationTest',
  'Mspd_CmiuManager_IOSRelease_2',
  'Mspd_Mdce_MdceCommonAwsIpc',
  'Mspd_Msc_Trunk_MdceCommonAwsStorage',
  'Mspd_Msc_Trunk_MdceCommonInternalApi',
  'Mspd_Mdce_Trunk_MdceIntegration',
  'Mspd_Mdc_Trunk_MdceMqtt',
  'Mspd_Mdce_MdceScheduler',
  'Mspd_Mdce_Trunk_MdceWeb',
  'Mspd_Trunk_PcTools_UpdateVersion',
  'Mspd_Common_C_TaggedPacketBuilderParser',
  'Mspd_Common_C_TcpSessionManager',
  'Mspd_Common_UartController_2',
  'Mspd_Common_C_Unity',
  'Mspd_Common_C_Utility',
  'Mspd_Common_Java_PacketUtils',
  #'Mspd_Common_Trunk_Python_CmiuSimulator',
]


def request(path)
  uri = URI.parse("#{TEAMCITY_ROOT}/#{path}")

  response = Net::HTTP.start(uri.host, uri.port) do |http|
    req = Net::HTTP::Get.new(uri.request_uri)
    req['Accept'] = 'application/json'
    req.basic_auth(TEAMCITY_USERNAME, TEAMCITY_PASSWORD)
    http.request(req).body
  end
  JSON.parse(response)
end

def fetch_project_info(project_id)
  response = request("app/rest/buildTypes/id:#{project_id}")
  {
    :name => response['name'],
    :project_name => response['projectName'],
  }
end

def fetch_latest_build_id(project_id) 
  response = request("app/rest/buildTypes/id:#{project_id}/builds/?locator=count:1")
  response['build'].first['id']
end


def fetch_tests_from_build(build_id)
  response = request("app/rest/testOccurrences?locator=build:(id:#{build_id})")
  response
end

def create_test_report(project_id)
  project_info = fetch_project_info(project_id)
  latest_build_id = fetch_latest_build_id(project_id)
  tests = fetch_tests_from_build(latest_build_id)

  project_info[:tests] =  tests['testOccurrence'].map do |test|
    {
      :name => test['name'],
      :status => test['status'],
      :duration => test['duration'],
    }
  end

  project_info
end

test_reports = BUILD_CONFIGURATIONS.map{ |project_id| create_test_report(project_id) }

output = CSV.generate do |csv|
  csv << ['TeamCity Project', 'Build Configuration', 'Test Name', 'Status', 'Duration (ms)']
  test_reports.each do |test_report|
    test_report[:tests].each do |test|
      csv << [test_report[:project_name], test_report[:name], test[:name], test[:status], test[:duration]]
    end
  end
end

puts output
