﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="13008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str">This project contains a simple VI that plots random data. The project is used by the Generate Programmatic Build Command Line String.vi example to programmatically create a built application.</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="CCSymbols" Type="Str">OS,Win;CPU,x86;OS_hidden,Win;CPU_hidden,x86</Property>
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Main.vi" Type="VI" URL="../Main.vi"/>
		<Item Name="Post-Build Quit LabVIEW.vi" Type="VI" URL="../Post-Build Quit LabVIEW.vi"/>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="TeamCity" Type="EXE">
				<Property Name="App_INI_aliasGUID" Type="Str">{49C9E7F6-6785-4D8B-AB4B-396A74AFD004}</Property>
				<Property Name="App_INI_GUID" Type="Str">{F203D76A-54DA-40ED-909F-E80014ED823C}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{C949CDBC-3A94-4F9C-A36C-24408B4AE229}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">TeamCity</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeTypedefs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/TeamCity</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/Post-Build Quit LabVIEW.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{2C943B30-43EE-4A11-B8AD-BE3159295390}</Property>
				<Property Name="Bld_userLogFile" Type="Path">../builds/TeamCity Sample Project/TeamCity/TeamCity Sample Project_TeamCity_log.txt</Property>
				<Property Name="Bld_userLogFile.pathType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_version.build" Type="Int">4444</Property>
				<Property Name="Bld_version.major" Type="Int">11</Property>
				<Property Name="Bld_version.minor" Type="Int">22</Property>
				<Property Name="Bld_version.patch" Type="Int">33</Property>
				<Property Name="Destination[0].destName" Type="Str">Application.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/TeamCity/Application.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/TeamCity/data</Property>
				<Property Name="Destination[2].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[2].path" Type="Path">../builds/NI_AB_PROJECTNAME/TeamCity</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Source[0].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyProperties" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{5B7C4D92-B0A9-447E-98EA-9C57E348AC30}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Main.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Sagentia</Property>
				<Property Name="TgtF_fileDescription" Type="Str">asdf</Property>
				<Property Name="TgtF_internalName" Type="Str">TeamCity</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2005 NI</Property>
				<Property Name="TgtF_productName" Type="Str">TeamCity</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{11108EA9-0B5F-4726-AABD-D9D54EE63D58}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Application.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
