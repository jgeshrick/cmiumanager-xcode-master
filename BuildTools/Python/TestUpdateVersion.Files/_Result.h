/************************************************************************* 
 * (c) Sagentia Limited, 2012.  All rights reserved.   
 *  
 * This software is the property of Sagentia Limited and may 
 * not be copied or reproduced otherwise than on to a single hard disk for 
 * backup or archival purposes.  The source code is confidential  
 * information and must not be disclosed to third parties or used without  
 * the express written permission of Sagentia Limited. 
 * 
 *  $Workfile$  
 *  $Author: ams1 $  
 *  $Date: 2013-01-07 12:00:27 +0000 (Mon, 07 Jan 2013) $  
 *  $Revision: 4422 $  
 *
 *************************************************************************/

#ifndef TESTVERSION_H
#define TESTVERSION_H

/* This is based on the way TI's IBL bootloader wants its version.
 * Build numbers (d) in particular are likely to exceed 255, so we just
 * take it mod 255, this should be unambiguous most of the time.
 * The string version below is not affected.
 */
#define VERSION_CLIP(x) ((x) & 0xFF)
#define MAKE_VERSION(a,b,c,d)  ((VERSION_CLIP(a) << 24) | (VERSION_CLIP(b) << 16) | (VERSION_CLIP(c) << 8) | (VERSION_CLIP(d) << 0))


/**
 * @brief
 *  The version number, eg. 1.0.0.15
 */
#define TEST_VERSION MAKE_VERSION(1u,2u,3u,4u)
#define TEST_BUILD_COUNTER 4u
/* Version string */
#define TEST_VERSION_STR  "1.2.3.4"

#endif 

