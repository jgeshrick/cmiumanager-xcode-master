#********************************************************************* 
# (c) Sagentia Ltd., 2012.  All rights reserved.   
#  
# Repository path  : $HeadURL: https://svn.genericsgroup.local/Sagentia/LanguageLibrary/TeamCityBuildAgents/Versioning/UpdateVersion.py $
# Last committed   : $Revision: 125 $
# Last changed by  : $Author: gd4 $
# Last changed date: $Date: 2014-07-25 10:22:30 +0100 (Fri, 25 Jul 2014) $
# ID: $Id: UpdateVersion.py 125 2014-07-25 09:22:30Z gd4 $
#*********************************************************************  
# -*- coding: utf-8 -*-
"""
Updates files on disk with a software version number.  It can read and write
various different kinds of files, including Visual Studio project files,
Win32 resource files, C# Assembly.cs files, general template files (eg. for
C header or source files) and LabView project files.

This is particularly useful in a Continuous Integration context, where it 
can be used to ensure that the software is built with an internal version 
number matching that which the CI system (eg. TeamCity) has assigned 
to the build.

Created on Wed Jan 02 11:58:10 2013

@author: ams1
"""

import argparse
import re
import sys
import os.path
import glob
import uuid

#Exit error conditions - Note that invalid arguments always exit with value 2
ERROR_TYPES = { 
    "filetype_unknown"      : (101, "File type cannot be determined and was not user specified"),
    "file_not_found"        : (102, "%s file not found"),
    "file_read"             : (103, "%s problems reading file"),
    "file_write_open"       : (104, "%s failed to open for writing"),
    "file_write"            : (105, "%s problems writing file"),
    "no_changes_made"       : (106, "No changes made to file.  Is the input file in unicode format?  This is not supported!"),
    "filetype_unsupported"  : (107, "%s file type is not supported"),
    "application_required"  : (108, "An application name is required when a template is specified"),
    "no_wildcards_allowed"  : (109, "Wildcards are not permitted in %s"),
    "invalid_label"         : (110, "Invalid label in template file %%%d$s on line %d"),
    "version_not_bytes"     : (111, "Version number exceeds 255 for byte-style version numbers %d.%d.%d.%d"),
    "product_block_missing" : (112, "The product block was not found.")
         }

class UpdateError(Exception):
    def __init__(self, exitCode, msg):
        self.args = (exitCode,msg)
        self.exitCode = exitCode
        self.msg = msg
        
class UpdateVersion(object):
    """This is a class that encapsulates the functionality used to update the version
       information in a version file used to build code on the Teamcity build server."""

    #Members that are accessable publicly    
    FilePath=None               #Path of files to be processed (may include wild cards)
    Version=None                #Version as a list of numbers
    Template=None               #Template file name (fully qualified path)

    #"Private class members
    __bytesOnly=False           #True if version can only be bytes
    __applicationName=None      #Application name
    __quietMode=False           #Quiet mode (boolean)

    __lines=None                #Lines to be processed

    #Constructor
    def __init__(self, *args, **kwargs):
        self.FilePath = kwargs.pop("filePath", None) #FilePath is now a list of paths
        if self.FilePath == None: self.FilePath = args[0]
        self.Version  = kwargs.pop("version", None)
        if self.Version == None: self.Version = args[1]
        self.__bytesOnly = kwargs.pop("bytesOnly", False)
        self.__applicationName = kwargs.pop("applicationName", None)
        self.Template = kwargs.pop("template", None)
        self.__quietMode = kwargs.pop("quietMode", False)

        self.Warnings = []
        self.PrintWarnings = True
        
    #Used for verbose filtered output
    def Report(self,stuff):
        """Prints stuff if not in quiet mode."""
        if not self.__quietMode: print stuff

    def ReportWarning(self,msg):
        """Print a warning, even in quiet mode, but they can be suppressed for unit testing."""
        if self.PrintWarnings:
            sys.stderr.write(msg + "\n")
        self.Warnings.append(msg)
    
    #Error exit handler
    def RaiseExceptionOnError(self,reason,args):
        """Prints an error message and raises an UpdateError exception"""
        errorCondition = ERROR_TYPES[reason]
        self.Report( "\n" + os.path.basename(__file__) + ": error: " + (errorCondition[1] % args) )
        raise UpdateError(errorCondition[0], errorCondition[1] % args)    

    #Read in the file to be patched
    def __ReadInput(self, filename):
        self.Report( "Opening file %s for reading" % filename )
        try:
            file = open(filename,"r")
        except IOError:
            self.RaiseExceptionOnError("file_not_found", filename)
        with file:
            try:
                self.__lines = file.readlines()
            except IOError:
                self.RaiseExceptionOnError("file_read", filename)
        file.close()
        self.Report( "Read %d lines" % len(self.__lines) )
    
    #Write the lines to the output file
    def __WriteOutput(self, filename):
        self.Report( "Opening file %s for writing" % filename )
        try:
            file = open(filename,"w")
        except IOError:
            self.RaiseExceptionOnError("file_open_write", filename)
        with file:
            try:
                file.writelines(self.__lines)
            except IOError:
                self.RaiseExceptionOnError("file_write", filename)
        file.close()
        self.Report( "%d lines written" % len(self.__lines) )
    
    def __UpdateRcFileVersionBlock(self):
        """Updates the version block in an RC file, for Win32 native applications"""
        matchFixedInfo = re.compile("""(\s*)(FILE|PRODUCT)VERSION\s+(\d+)\,\s*(\d+)\,\s*(\d+)\,\s*(\d+)""")
        matchVersionInfo = re.compile("""(\s*)VALUE\s+\"(File|Product)Version\"\,\s*(\".+\")""")
        commaVer  = ( str(self.Version[0]) + ","
                    + str(self.Version[1]) + ","
                    + str(self.Version[2]) + ","
                    + str(self.Version[3]) )
        dotString = ( '"' + str(self.Version[0]) + "."
                          + str(self.Version[1]) + "."
                          + str(self.Version[2]) + "."
                          + str(self.Version[3]) + '"' )
        changesMade = False
        for lineIdx in xrange(0, len(self.__lines)):
            line = self.__lines[lineIdx]
            #FixedInfo version patch
            m = matchFixedInfo.match(line)
            if m:
                infoType = m.group(2)+"VERSION"
                oldCommaVer = m.group(3)+","+m.group(4)+","+m.group(5)+","+m.group(6)
                self.Report("Line %d: Patched %s %s -> %s" % (lineIdx+1,infoType,oldCommaVer,commaVer))
                self.__lines[lineIdx] = m.group(1) + infoType + " " + commaVer + "\n"
                changesMade = True
            #Language string value version patch
            m = matchVersionInfo.match(line)
            if m: 
                infoType = '"' + m.group(2)+ 'Version"'
                oldVerStr = m.group(3)
                self.Report("Line %d: Patched VALUE %s %s -> %s" % (lineIdx+1,infoType,oldVerStr,dotString))
                self.__lines[lineIdx] = m.group(1) + "VALUE " + infoType + ", " + dotString + "\n"
                changesMade = True
        if not changesMade:
            self.RaiseExceptionOnError( "no_changes_made", () )
    
    def __UpdateAssemblyInfoCsVersionBlock(self):
        """Updates the version block in an AssemblyInfo.cs file, for C# assemblies"""
        matcher = re.compile("""(\s*)\[assembly:\s*(AssemblyVersion|AssemblyFileVersion)\((\".+\")\)\]""")
        dotString = ( '"' + str(self.Version[0]) + "."
                          + str(self.Version[1]) + "."
                          + str(self.Version[2]) + "."
                          + str(self.Version[3]) + '"' )
        changesMade = False
        for lineIdx in xrange(0, len(self.__lines)):
            line = self.__lines[lineIdx]
            m = matcher.match(line)
            if m:
                infoType = m.group(2)
                oldVerStr = m.group(3)
                self.Report("Line %d: Patched %s %s -> %s" % (lineIdx+1,infoType,oldVerStr,dotString))
                self.__lines[lineIdx] = m.group(1) + "[assembly: " + infoType + "(" + dotString + ")]\n"
                changesMade = True
        if not changesMade:
            self.RaiseExceptionOnError( "no_changes_made", () )
    
    def __UpdateTemplateAppAndVersionLabels(self):
        """Updates the lines based on a template file, eg. template.h"""
        matcher = re.compile("""(\%\d+\$s)""")
        replacements = ( self.__applicationName, 
                         str(self.Version[0]), 
                         str(self.Version[1]), 
                         str(self.Version[2]), 
                         str(self.Version[3]) )
        lineNum = 0
        def repl(matchObj):
            label = matchObj.group(0)
            token = int(label.strip("%$s"))-1
            if not token in xrange(0,len(replacements)):
                self.RaiseExceptionOnError("invalid_label", (token+1,lineNum))
            return replacements[token]
        changesMade = False
        for lineIdx in xrange(0, len(self.__lines)):
            line = self.__lines[lineIdx]
            lineNum = lineIdx
            subnResult = matcher.subn(repl,line)
            #If a substitution has been made...
            if subnResult[1]>0:
                self.Report("Line %d: from: %s" % (lineIdx+1, line.strip("\n")))
                self.Report("           to: %s" % subnResult[0].strip("\n"))
                self.__lines[lineIdx] = subnResult[0]
                changesMade = True
        if not changesMade:
            self.RaiseExceptionOnError( "no_changes_made", () )

    def __UpdateVdprojFileVersionBlock(self):
        """Updates the version block in a Visual Studio installer (MSI) project file"""
        matchProduct = re.compile("""\s*\"Product\"""")
        matchOpenBrace = re.compile("""\s*\{""")
        matchCloseBrace = re.compile("""\s*\}""")
        matchProductCode = re.compile("""(\s*)\"ProductCode\"\s*=\s*\"8:\{([0-9A-Fa-f]{8}\-[0-9A-Fa-f]{4}\-[0-9A-Fa-f]{4}\-[0-9A-Fa-f]{4}\-[0-9A-Fa-f]{12})\}\"""")
        matchProductVersion = re.compile("""(\s*)\"ProductVersion\"\s*=\s*\"8:(\d+\.\d+\.\d+)""")
        #Locate the product block
        productBlockStart = None
        productBlockEnd = None
        lineCount = len(self.__lines)
        for lineIdx in xrange(0, lineCount):
            line = self.__lines[lineIdx]
            if matchProduct.match(line) and (lineIdx+1)<lineCount:
                self.Report("Located start of product block (title) on line %d" % (lineIdx+1))
                line = self.__lines[lineIdx+1]                    
                if matchOpenBrace.match(line) and (lineIdx+2)<lineCount:
                    self.Report("Located start of product block (open brace) on line %d" % (lineIdx+2))
                    productBlockStart = lineIdx+2
                    #Now find the end of the block
                    for lineIdx2 in xrange(productBlockStart, lineCount):
                        line = self.__lines[lineIdx2]
                        if matchCloseBrace.match(line):
                            self.Report("Located end of product block (close brace) on line %d" % (lineIdx2+1))
                            productBlockEnd = lineIdx2
                            break
                    break
                else:
                    #Error, should have matched brace here
                    break
        #Check to see that product block was located
        if productBlockStart==None or productBlockEnd==None:
            self.RaiseExceptionOnError("product_block_missing", ())
        #Now update the product code and product version number
        dotString = ( str(self.Version[0]) + "."
                    + str(self.Version[1]) + "."
                    + str(self.Version[2]) )
        uuidStr = str(uuid.uuid4())
        uuidStr = uuidStr.upper()
        productCodeChangesMade = False
        productVersionChangesMade = False
        for lineIdx in xrange(productBlockStart, productBlockEnd):
            line = self.__lines[lineIdx]
            #ProductCode version patch
            m = matchProductCode.match(line)
            if m:
                self.Report("Line %d: Patched ProductCode %s -> %s" % (lineIdx+1,m.group(2),uuidStr))
                self.__lines[lineIdx] = ( m.group(1) + "\"ProductCode\" = \"8:{" 
                                        + uuidStr + "}\"\n" )
                self.Report("New line is: %s" % self.__lines[lineIdx])
                productCodeChangesMade = True
            #ProductVersion version patch
            m = matchProductVersion.match(line)
            if m: 
                self.Report("Line %d: Patched ProductVersion %s -> %s" % (lineIdx+1,m.group(2),dotString))
                self.__lines[lineIdx] = ( m.group(1) + "\"ProductVersion\" = \"8:"  
                                        + dotString + "\"\n" )
                productVersionChangesMade = True
        if (not productCodeChangesMade) or (not productVersionChangesMade):
            self.RaiseExceptionOnError( "no_changes_made", () )

    def __UpdateCsprojApplicationTags(self):
        """Update the ApplicationVersion and ApplicationRevision tags in a Visual Studio 2010 .Csproj file, if present"""
        matchAppVersion = re.compile(r"(\s*<ApplicationVersion>).*(</ApplicationVersion>)",re.IGNORECASE)
        matchAppRevision = re.compile(r"(\s*<ApplicationRevision>).*(</ApplicationRevision>)",re.IGNORECASE)
        changesMade = False
        for lineIdx, line in enumerate(self.__lines):
            if matchAppVersion.match(line):
                newline = matchAppVersion.sub(r"\g<1>{0}.{1}.{2}.{3}\g<2>".format(*tuple(self.Version)),
                                                 line, 1)
                if newline != line:
                    changesMade = True
                    self.__lines[lineIdx] = newline
                    self.Report("New line is: %s" % self.__lines[lineIdx])
            elif matchAppRevision.match(line):
                newline = matchAppRevision.sub(r"\g<1>{0}\g<2>".format(self.Version[3]), line, 1)
                if newline != line:
                    changesMade = True
                    self.__lines[lineIdx] = newline
                    self.Report("New line is: %s" % self.__lines[lineIdx])
        if not changesMade:
            self.RaiseExceptionOnError( "no_changes_made", () )

    def __UpdateLvprojFileVersionBlock(self):
        """Determine if LabView 2011"""
        matchstr = r"\<Project Type=\"Project\" LVVersion=\"11008008\"\>"
        matcher = re.compile(matchstr)
        foundMatch = False

        for line in self.__lines:
            if matcher.search(line) is not None:
                foundMatch = True
                self.__UpdateLvproj2011FileVersionBlock()
                break
        if foundMatch is False:
            self.__UpdateLvprojNon2011FileVersionBlock()
    
    def __UpdateLvprojNon2011FileVersionBlock(self):
        """Update version information in a LabView project file"""
        matchstr = [r"""(<Property +Name="Bld_version.major" +Type="Int">)\d*(</Property>)""",
                    r"""(<Property +Name="Bld_version.minor" +Type="Int">)\d*(</Property>)""",
                    r"""(<Property +Name="Bld_version.patch" +Type="Int">)\d*(</Property>)""",
                    r"""(<Property +Name="Bld_version.build" +Type="Int">)\d*(</Property>)"""]
        tagnames = ["Bld_version.major","Bld_version.minor","Bld_version.patch","Bld_version.build"]
        matchers = [re.compile(s,re.IGNORECASE) for s in matchstr]
        replacements = [r"\g<1>{0}\g<2>".format(i) for i in self.Version]
        counts = [0,0,0,0]        
        
        for j, m in enumerate(matchers):
            for lineIdx, line in enumerate(self.__lines):
                try:
                    newstr, n = m.subn(replacements[j],line,1)
                except Exception, e:
                    print e
                if n > 0:
                    self.__lines[lineIdx] = newstr
                    self.Report("Line %d: Patched %s -> %s" % (lineIdx+1,tagnames[j],self.Version[j]))
                    counts[j] += n
                    break
        
        if sum(counts) == 0:
            self.RaiseExceptionOnError( "no_changes_made", () )

        for j, n in enumerate(counts):
            if n == 0:
                self.ReportWarning("WARNING: could not find property {0}".format(tagnames[j]))

    def __UpdateLvproj2011FileVersionBlock(self):
        """Update version information in a LabView project file"""
        matchstr = [r"""(<Property +Name="TgtF_fileVersion.major" +Type="Int">)\d*(</Property>)""",
                    r"""(<Property +Name="TgtF_fileVersion.minor" +Type="Int">)\d*(</Property>)""",
                    r"""(<Property +Name="TgtF_fileVersion.patch" +Type="Int">)\d*(</Property>)""",
                    r"""(<Property +Name="TgtF_fileVersion.build" +Type="Int">)\d*(</Property>)"""]
        tagnames = ["TgtF_fileVersion.major", "TgtF_fileVersion.minor",
                    "TgtF_fileVersion.patch", "TgtF_fileVersion.build"]
        matchers = [re.compile(s, re.IGNORECASE) for s in matchstr]
        replacements = [r"\g<1>{0}\g<2>".format(i) for i in self.Version]
        counts = [0,0,0,0]

        for j, m in enumerate(matchers):
            for lineIdx, line in enumerate(self.__lines):
                try:
                    newstr, n = m.subn(replacements[j],line,1)
                except Exception, e:
                    print e
                if n > 0:
                    self.__lines[lineIdx] = newstr
                    self.Report("Line %d: Patched %s -> %s" % (lineIdx+1,tagnames[j],self.Version[j]))
                    counts[j] += n
                    break

        if sum(counts) == 0:
            self.RaiseExceptionOnError("no_changes_made", ())

        for j, n in enumerate(counts):
            if n == 0:
                self.ReportWarning("WARNING: could not find property {0}".format(tagnames[j]))
                
     
    #"Public" method for handling the version file updates
    def Update(self):
        """Performs the update of the files to be patched"""
        
        #If there is a template file specified
        if self.Template != None:
            #Must specify an application name for a template file
            if self.__applicationName == None:
                self.RaiseExceptionOnError( "application_required", () )
        
        #Check to see if version values need to be restricted to 0..255
        if self.__bytesOnly:
            versionValNotByte = [ val>255 for val in self.Version ]
            if any(versionValNotByte):
                self.RaiseExceptionOnError( "version_not_bytes", tuple(self.Version) )
                
        #Now see if the file specification yields any files
        fileList = []
        emptySpecs = []
        for filePathSpec in self.FilePath:
            filesMatching = glob.glob( filePathSpec )
            fileList += filesMatching
            if len(filesMatching) == 0:
                emptySpecs.append(filePathSpec)
            
        if len(fileList)==0:
            #The file list cannot be empty if we are not using a template
            if self.Template == None:
                self.RaiseExceptionOnError("file_not_found", self.FilePath)
            #However if using a template then all we need is valid file paths without wildcards
            for filePathSpec in self.FilePath:
                if re.search("\*|\?|\[\d+\-\d+\]", filePathSpec):
                    self.RaiseExceptionOnError("no_wildcards_allowed", self.FilePath)
            fileList = self.FilePath

        elif len(emptySpecs) > 0 and self.Template == None:
            for filePathSpec in emptySpecs:
                self.ReportWarning("WARNING: no files found matching " + filePathSpec)

        #Patch all the files in the file list
        for filepath in fileList:
            
            #Split the filename into its components
            self.Report( "\n" + filepath + ":" )
            basename = os.path.basename(filepath)
            filename_ext = os.path.splitext(basename)
            fileType = filename_ext[1].strip(".")
            
            if self.Template != None:
                #Processing for a template file
                self.__ReadInput(self.Template)
                self.__UpdateTemplateAppAndVersionLabels()
            
            else:
                #Processing for a file to be directly patched
                self.__ReadInput(filepath)
                #Process appropriate file type
                if fileType=="rc":
                    self.__UpdateRcFileVersionBlock()
                elif basename=="AssemblyInfo.cs":
                    self.__UpdateAssemblyInfoCsVersionBlock()
                elif fileType=="vdproj":
                    self.__UpdateVdprojFileVersionBlock()
                elif fileType=="lvproj":  # LabView
                    self.__UpdateLvprojFileVersionBlock()
                elif fileType=="csproj":  # Visual Studio csproj, for one-click publishing
                    self.__UpdateCsprojApplicationTags()
                else:
                    self.RaiseExceptionOnError( "filetype_unsupported", basename )
                
            self.__WriteOutput(filepath)
        
        self.Report( "\nDone." )
        
    #Type used to check for a valid version string N.N.N.N
    @staticmethod
    def version_type(version):
        verStrs = version.split(".")
        if len(verStrs)!=4:
            raise argparse.ArgumentTypeError("Not a valid version string N.N.N.N")
        verVals = [ int(vs) for vs in verStrs ]
        
        # Suppressing 16 bit limit on values for testing
        for ival in verVals:
#            if( (ival<0) | (65535<ival) ):
            if( (ival<0) ):
                raise argparse.ArgumentTypeError("Individual version numbers need to be 0..65535")
        return verVals
    
    @staticmethod
    def InitCommandLineParser( parser ):
        parser.add_argument("file_path", 
                            help="Fully qualified path and name(s) of the file(s) to be patched "
                            "or overwritten from the template. If no template is specified then "
                            "the extension of the file is used to determine the file type. If "
                            "a template file is specified then the extension is ignored.",
                            nargs="+")
        parser.add_argument("-v", "--version", metavar="Maj.Min.Bld.Rev",
                            help="Version string Maj, Min, Bld, and Rev are all unsigned integers. By convention "
                            "Maj & Min are version number of the release, Bld is the build number "
                            "from the build system, and Rev is the revision number from the "
                            "version control system.",
                            type=UpdateVersion.version_type, required=True)
        parser.add_argument("-b", "--bytes_only",
                            help="If specified restricts the range of the version values to be "
                            "from 0 to 255",
                            action="store_true")
        parser.add_argument("-a", "--application", metavar="APPLICATION_NAME",
                            help="The name of the application. This is optional except when a template "
                            "file is specified then it is required.")
        parser.add_argument("-t", "-T", "--template", "--local_template", metavar="TEMPLATE_FILE",
                            help="Updates the version information and application name using "
                            "a template file to overwrite an existing file. The template file "
                            "should contain the following markers: %%1$s application name, "
                            "%%2$s major version, %%3$s minor version, %%4$s build number, and %%5$s "
                            "is the revision number")
        parser.add_argument("-q", "--quiet", help="Suppresses all output reporting (except for usage message)",
                            action="store_true")
        
        
#Main entry point for the module called from the command line
if __name__ == '__main__':

    #Process the command line arguments
    parser = argparse.ArgumentParser(
        description="Updates the version information in file on the filesystem.",
        epilog="This script is intended to be called by the continuous integration system.")
    UpdateVersion.InitCommandLineParser( parser )
    args = parser.parse_args()

    #Create the updater
    updater = UpdateVersion(args.file_path,
                            args.version,
                            bytesOnly=args.bytes_only,
                            applicationName=args.application,
                            template=args.template,
                            quietMode=args.quiet)

    #Process the user input
    try:
        updater.Update()
    except UpdateError as e:
        sys.exit(e.exitCode)
        
    #Exit on successful completion of the script        
    sys.exit(0)
