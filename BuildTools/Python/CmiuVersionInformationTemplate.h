/******************************************************************************
*******************************************************************************
**
**         Filename: CmiuVersionInformation.h
**
**           Author: Brian Arnberg
**          Created: 2015.07.15
**
**     Last Edit By:
**        Last Edit:
**
** Revision History:
**       2015.07.15: Generated file.
**           %4$s: Updated by TeamCity
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/



// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __VERSION_INFORMATION_H
#define __VERSION_INFORMATION_H

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include "typedefs.h"
#include "MemoryMap.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
#define VERSION_MAJOR   0x%2$s  /** Major Version is %2$s BCD */
#define VERSION_MINOR   0x%3$s  /** Minor Version is %3$s BCD */
#define VERSION_YYMMDD  0x%4$s  /** Version build date is (YYMMDD) */
#define VERSION_BUILD   0x%5$s  /** Version build is %5$s BCD */
#define DEFAULT_CRC     0xABCDEF00  /** CRC Placeholder value. */


#endif // __VERSION_INFORMATION_H

