#********************************************************************* 
# (c) Sagentia Ltd., 2012.  All rights reserved.   
#  
# Repository path  : $HeadURL: https://svn.genericsgroup.local/Sagentia/LanguageLibrary/TeamCityBuildAgents/Versioning/SvnUpdateVersion.py $
# Last committed   : $Revision: 131 $
# Last changed by  : $Author: gd4 $
# Last changed date: $Date: 2014-07-31 09:29:35 +0100 (Thu, 31 Jul 2014) $
# ID: $Id: SvnUpdateVersion.py 131 2014-07-31 08:29:35Z gd4 $
#*********************************************************************  
# -*- coding: utf-8 -*-
"""
Extends the UpdateVersion class to perform a Subversion checkout, update files,
then check back into SVN.  This is designed for use in a Continuous Integration
server, and means that after a successful build, the 
repository is updated to match (approximately) what was used, and any personal
builds after an Update from SVN will reflect the numbering on the CI system.

The --suppress option allows some of the version number to be set to 0.
The Sagentia convention is that a Build number of 0 denotes a private build
on a workstation, so the SvnUpdateVersion is usually run with --supress=Bld
"""

import tempfile
import pysvn
import argparse
import UpdateVersion
import os
import sys
import shutil
#import pprint

#Exit error conditions - Note that invalid arguments always exit with value 2
ERROR_TYPES = { 
    "no_svn_repo"   : (201, "No SVN repository was specified"),
    "no_svn_user"   : (202, "No SVN user was specified"),
    "no_svn_pwd"    : (203, "No SVN password was specified"),
    "no_log_msg"    : (204, "No SVN commit log message was specified"),
    "svn_checkout"  : (205, "SVN checkout error on repo %s"),
    "svn_commit"    : (206, "SVN commit failed on folder %s"),
    "template_conflict" : (207, "Cannot specify both -t and -T at the same time!")
         }

class SvnUpdateVersion(UpdateVersion.UpdateVersion):
    """This class updates the version numbers in an SVN tree."""

    #"Private" members
    __svnRepo = None
    __svnUser = None
    __svnPwd  = None
    __suppresses = None
    __logMsg = None
    
    def __init__(self, *args, **kwargs):
        super(SvnUpdateVersion,self).__init__(*args, **kwargs)
        self.__svnRepo = kwargs.pop("svnRepo",None)
        self.__svnUser = kwargs.pop("svnUser",None)
        self.__svnPwd  = kwargs.pop("svnPwd",None)
        self.__suppresses = kwargs.pop("suppresses",None)
        self.__logMsg = kwargs.pop("logMsg",None)
        self.__svnTemplate = kwargs.pop("svnTemplate",None)
        self.__localTemplate = kwargs.pop("localTemplate",None)
        

    #Error exit handler
    def RaiseExceptionOnError(self,reason,args):
        """Prints an error message and raises an UpdateError exception"""
        try:
            errorCondition = ERROR_TYPES[reason]
        except KeyError:
            super(SvnUpdateVersion,self).RaiseExceptionOnError(reason,args)
        self.Report( "\n" + os.path.basename(__file__) + ": error: " + (errorCondition[1] % args) )
        raise UpdateVersion.UpdateError(errorCondition[0], errorCondition[1] % args)    

    def Update(self):
        #Check for complete set of parameters
        if self.__svnRepo == None: self.RaiseExceptionOnError("no_svn_repo", ())
        if self.__svnUser == None: self.RaiseExceptionOnError("no_svn_user", ())
        if self.__svnPwd == None: self.RaiseExceptionOnError("no_svn_pwd", ())
        if self.__logMsg == None: self.RaiseExceptionOnError("no_log_msg", ())
        
        #First suppress any version numbers as required
        if self.__suppresses != None:
            oldVer = self.Version
            newVer = list(oldVer)
            for el in self.__suppresses:
                if el=="Maj": newVer[0]=0
                if el=="Min": newVer[1]=0
                if el=="Bld": newVer[2]=0
                if el=="Rev": newVer[3]=0
            self.Version = tuple(newVer)
            self.Report("Version suppressed. Old=%d.%d.%d.%d, New=%d.%d.%d.%d"
                   % (oldVer[0],oldVer[1],oldVer[2],oldVer[3],
                      self.Version[0],self.Version[1],self.Version[2],self.Version[3]))

        #Now do an SVN checkout of the required folders
        def svn_login( realm, username, may_save ):
            self.Report('SVN log in as user "%s"' % (self.__svnUser))
            #self.Report("SVN log in. User=\"%s\" Password=\"%s\"" % (self.__svnUser, self.__svnPwd))
            return True, self.__svnUser, self.__svnPwd, False
        def svn_ssl_server_trust_prompt( trust_dict ):
            self.Report("SVN trusting SSL certificate")
            return True, trust_dict["failures"], False
        svn = pysvn.Client()
        svn.callback_get_login = svn_login
        svn.callback_ssl_server_trust_prompt = svn_ssl_server_trust_prompt

        #We're going to check out to this in a temporary folder
        tempFolder = tempfile.mkdtemp(prefix="SvnUpdateVersion_")

        if self.__svnTemplate != None and self.__localTemplate != None:
            self.RaiseExceptionOnError("template_conflict",())
        elif self.__localTemplate != None:
            self.Template = os.path.abspath(self.__localTemplate)
        elif self.__svnTemplate != None:
            self.Template = os.path.join(tempFolder, self.__svnTemplate)
        

        self.Report("Checking out %s to folder %s" % (self.__svnRepo, tempFolder))
        try:        
            svn.checkout(self.__svnRepo, tempFolder)
        except pysvn.ClientError:
            #os.rmdir(tempFolder)
            self.RaiseExceptionOnError("svn_checkout",self.__svnRepo)
            
        #Now set the path of the files to be updated
        newFilePath = [ os.path.join(tempFolder, filePathSpec) for filePathSpec in self.FilePath ]        
        self.FilePath = newFilePath
        
        #Invoke the Update on the base class
        self.Report("Updating version files.")
        try:
            super(SvnUpdateVersion,self).Update()
        except UpdateVersion.UpdateError as e:
            shutil.rmtree(tempFolder, ignore_errors=True)
            raise e
            
        #Now we need to commit the changes back to SVN
        self.Report("\nCommitting changes back to SVN")
        try:
            svn.checkin(tempFolder, self.__logMsg)
        except pysvn.ClientError:
            shutil.rmtree(tempFolder, ignore_errors=True)
            self.RaiseExceptionOnError("svn_commit",tempFolder)
        
        #All is done. Clean up the temporary folder
        shutil.rmtree(tempFolder, ignore_errors=True)
        self.Report("Done.")

    @staticmethod
    def InitCommandLineParser( parser ):
        UpdateVersion.UpdateVersion.InitCommandLineParser(parser)
        parser.add_argument("-sr","--svn_repo",
                            help="SVN repository.",
                            required=True)
        parser.add_argument("-su","--svn_user",
                            help="SVN user id",
                            required=True)
        parser.add_argument("-sp","--svn_pwd",
                            help="SVN password",
                            required=True)
        parser.add_argument("-s", "--suppress",
                            help="Suppress one or more of the elements in the "
                            "version number. If an element is suppressed then its "
                            "value is set to zero", choices=("Maj","Min","Bld","Rev"),
                            action="append")
        parser.add_argument("-T", "--local_template", metavar="TEMPLATE_FILE",
                            help="Like -t, but with a template on the local drive, not "
                            "in the SVN checkout.  Path should be either absolute or relative "
                            "to the directory in which this script is run. ")
        parser.add_argument("-l","--log_message",
                            help="Message to be used when committing version changes "
                            "back to SVN.",
                            default=("Version automatically updated by " + os.path.basename(__file__)))

#Main entry point for the module called from the command line
if __name__ == '__main__':

    #Process the command line arguments
    parser = argparse.ArgumentParser(
        description="Updates the version information in files in an SVN repository."
        "Note that paths are taken relative to the new checkout, except for the -T option",
        epilog="This script is intended to be called by the continuous integration system.",
        conflict_handler="resolve")
    SvnUpdateVersion.InitCommandLineParser( parser )
    args = parser.parse_args()
    
    #Create the updater
    updater = SvnUpdateVersion(svnRepo=args.svn_repo,
                               svnUser=args.svn_user,
                               svnPwd=args.svn_pwd,
                               suppresses=args.suppress,
                               logMsg=args.log_message,
                               filePath=args.file_path,
                               version=args.version,
                               bytesOnly=args.bytes_only,
                               applicationName=args.application,
                               svnTemplate=args.template,
                               localTemplate=args.local_template,
                               quietMode=args.quiet)

    #Process the user input
    try:
        updater.Update()
    except UpdateVersion.UpdateError as e:
        sys.exit(e.exitCode)
        
    #Exit on successful completion of the script        
    sys.exit(0)
    
        
