#********************************************************************* 
# (c) Sagentia Ltd., 2012.  All rights reserved.   
#  
# Repository path  : $HeadURL: https://svn.genericsgroup.local/Sagentia/LanguageLibrary/TeamCityBuildAgents/Versioning/TestUpdateVersion.py $
# Last committed   : $Revision: 126 $
# Last changed by  : $Author: gd4 $
# Last changed date: $Date: 2014-07-25 10:23:23 +0100 (Fri, 25 Jul 2014) $
# ID: $Id: TestUpdateVersion.py 126 2014-07-25 09:23:23Z gd4 $
#*********************************************************************  
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 04 17:59:32 2013

@author: ams1
"""

import UpdateVersion as uv
import unittest
import os
import shutil
import re

try:
    from teamcity import underTeamcity
    from teamcity.unittestpy import TeamcityTestRunner
except ImportError:
    def underTeamcity():
        return False

UNIT_TEST_DATA_FOLDER = "TestUpdateVersion.Files"

RC_TEMPLATE_FILENAME = "_Test.rc"
RC_RESULT_FILENAME = "_Result.rc"
ASSEMBLY_CS_TEMPLATE_FILENAME = "_TestAssemblyInfo.cs"    
ASSEMBLY_CS_RESULT_FILENAME = "_ResultAssemblyInfo.cs"
TEMPLATE_TEMPLATE_FILENAME = "_Test.h"
TEMPLATE_RESULT_FILENAME = "_Result.h"
VDPROJ_TEMPLATE_FILENAME = "_Test.vdproj"
VDPROJ_RESULT_FILENAME = "_Result.vdproj"

RC_TEST_SINGLE_FILENAME = "testsingle.rc"
RC_TEST_MULTIPLE_FILENAME = "testmultiple*.rc"
RC_TEST_MULTIPLE_FILENAME_F = "testmultiple%d.rc"
RC_TEST_MULTIPLE_NUMFILES = 5

ASSEMBLY_CS_TEST_FILENAME = "AssemblyInfo.cs"
ASSEMBLY_CS_TEST_MULTIPLE_FOLDER = "test*"
ASSEMBLY_CS_TEST_MULTIPLE_FOLDER_F = "test%d"
ASSEMBLY_CS_TEST_MULTIPLE_NUMFOLDERS = 5

TEMPLATE_TEST_SINGLE_FILENAME = "testsingle.h"
TEMPLATE_TEST_CREATE_FILENAME = "testcreate.h"
TEMPLATE_TEST_MULTIPLE_FILENAME = "testmultiple*.h"
TEMPLATE_TEST_MULTIPLE_FILENAME_F = "testmultiple%d.h"
TEMPLATE_TEST_MULTIPLE_NUMFILES = 5

VDPROJ_TEST_SINGLE_FILENAME = "testsingle.vdproj"

CSPROJ_TEMPLATE_FILENAME = "_OneClickPublish_VS2010.csproj"
CSPROJ_TEST_SINGLE_FILENAME = "_Test.csproj"
CSPROJ_RESULT_FILENAME = "_Result.csproj"

LVPROJ_TEST_SINGLE_FILENAME = "_Test.lvproj"
LVPROJ_RESULT_FILENAME = "_Result.lvproj"

LVPROJ_TEST_NOCHANGE_FILENAME = "_NoChange.lvproj"

NOT_FOUND_FILENAME = "does_not_exist"
UNSUPPORTED_FILETYPE = "_Test.xxx"
NO_CHANGES_MADE_FILENAME = "_Test.xxx"
WILDCARDS_NOT_ALLOWED_FILENAME = "does_not_exist*"
BAD_TEMPLATE_FILENAME1 = "_TestBad1.h"
BAD_TEMPLATE_FILENAME2 = "_TestBad2.h"

QUIET = True

#Unit test class
class TestUpdateVersion(unittest.TestCase):
    
    __testFolderBase = ""   #The base of the test folder
    
    def __compareFiles( self, filename1, filename2, errorMsg, concession=None ):
        """concession is a compiled regular expression"""
        file1 = open(filename1)
        lines1 = file1.readlines()
        file1.close()
        file2 = open(filename2)
        lines2 = file2.readlines()
        file2.close()
        if(len(lines1) != len(lines2)):
            print "Number of lines in files differ"
            self.fail(errorMsg)
        else:
            for lineIdx in xrange(0,len(lines1)):
                line1 = lines1[lineIdx]
                line2 = lines2[lineIdx]
                if( line1 != line2 ):
                    #Check to see if we have a concession
                    if concession != None:
                        if not concession.match(line1):
                            print "Mismatch on line %d" % (lineIdx+1)
                            self.fail(errorMsg)
                    else:
                        print "Mismatch on line %d" % (lineIdx+1)
                        self.fail(errorMsg)
        
    #Set up
    def setUp(self):
        #Perform set-up actions
        scriptDir = os.path.dirname(os.path.realpath(__file__))
        self.__testFolderBase = os.path.join( scriptDir, UNIT_TEST_DATA_FOLDER)
        
        #Copy the test RC file for single test
        srcFilename = os.path.join( self.__testFolderBase, RC_TEMPLATE_FILENAME )
        dstFilename = os.path.join( self.__testFolderBase, RC_TEST_SINGLE_FILENAME )
        shutil.copyfile( srcFilename, dstFilename )
        
        #Copy the test RC file for multiple test
        for filenum in xrange(0, RC_TEST_MULTIPLE_NUMFILES):
            dstFilename = os.path.join( self.__testFolderBase, 
                                       (RC_TEST_MULTIPLE_FILENAME_F % filenum) )
            shutil.copyfile( srcFilename, dstFilename )
            
        # Copy the csproj test        
        srcFilename = os.path.join( self.__testFolderBase, CSPROJ_TEMPLATE_FILENAME )
        dstFilename = os.path.join( self.__testFolderBase, CSPROJ_TEST_SINGLE_FILENAME )
        shutil.copyfile( srcFilename, dstFilename )
        
        #Copy the test AssemblyInfo.cs file for single test
        srcFilename = os.path.join( self.__testFolderBase, ASSEMBLY_CS_TEMPLATE_FILENAME )
        dstFilename = os.path.join( self.__testFolderBase, ASSEMBLY_CS_TEST_FILENAME )
        shutil.copyfile( srcFilename, dstFilename )

        #Copy the test AssemblyInfo.cs for multiple file tests
        # Reuses srcFilename
        for foldernum in xrange(0, ASSEMBLY_CS_TEST_MULTIPLE_NUMFOLDERS):
            foldername = os.path.join( self.__testFolderBase, 
                                       (ASSEMBLY_CS_TEST_MULTIPLE_FOLDER_F % foldernum)  )
            os.mkdir(foldername)
            dstFilename = os.path.join( foldername, ASSEMBLY_CS_TEST_FILENAME )
            shutil.copyfile(srcFilename, dstFilename)
        
        #Copy the test template file for single test
        srcFilename = os.path.join( self.__testFolderBase, TEMPLATE_TEMPLATE_FILENAME )
        dstFilename = os.path.join( self.__testFolderBase, TEMPLATE_TEST_SINGLE_FILENAME )
        shutil.copyfile( srcFilename, dstFilename )
        
        #Copy the test template file for multiple test
        for filenum in xrange(0, TEMPLATE_TEST_MULTIPLE_NUMFILES):
            dstFilename = os.path.join( self.__testFolderBase, 
                                       (TEMPLATE_TEST_MULTIPLE_FILENAME_F % filenum) )
            shutil.copyfile( srcFilename, dstFilename )
            
        #Copy the test VDPROJ file for single test
        srcFilename = os.path.join( self.__testFolderBase, VDPROJ_TEMPLATE_FILENAME )
        dstFilename = os.path.join( self.__testFolderBase, VDPROJ_TEST_SINGLE_FILENAME )
        shutil.copyfile( srcFilename, dstFilename )
        
            
    def tearDown(self):
        #Perform tear-down actions
        filename = os.path.join( self.__testFolderBase, RC_TEST_SINGLE_FILENAME )
        os.remove( filename )
        for filenum in xrange(0, RC_TEST_MULTIPLE_NUMFILES):
            filename = os.path.join( self.__testFolderBase, 
                                       (RC_TEST_MULTIPLE_FILENAME_F % filenum) )
            os.remove( filename )
        filename = os.path.join( self.__testFolderBase, ASSEMBLY_CS_TEST_FILENAME )
        os.remove(filename)
        for foldernum in xrange(0, ASSEMBLY_CS_TEST_MULTIPLE_NUMFOLDERS):
            foldername = os.path.join( self.__testFolderBase, 
                                       (ASSEMBLY_CS_TEST_MULTIPLE_FOLDER_F % foldernum)  )
            filename = os.path.join( foldername, ASSEMBLY_CS_TEST_FILENAME )
            os.remove(filename)
            os.rmdir(foldername)
        filename = os.path.join( self.__testFolderBase, TEMPLATE_TEST_SINGLE_FILENAME )
        os.remove( filename )
        filename = os.path.join( self.__testFolderBase, TEMPLATE_TEST_CREATE_FILENAME )
        if os.path.exists( filename ):
            os.remove(filename)
        filename = os.path.join( self.__testFolderBase, CSPROJ_TEST_SINGLE_FILENAME )
        os.remove( filename )
        for filenum in xrange(0, TEMPLATE_TEST_MULTIPLE_NUMFILES):
            filename = os.path.join( self.__testFolderBase, 
                                       (TEMPLATE_TEST_MULTIPLE_FILENAME_F % filenum) )
            os.remove( filename )
        filename = os.path.join( self.__testFolderBase, VDPROJ_TEST_SINGLE_FILENAME )
        os.remove( filename )
        
    def testRcSingleFilePatch(self):
        testFilename = os.path.join( self.__testFolderBase, RC_TEST_SINGLE_FILENAME )
        filePaths = [testFilename]
        updater = uv.UpdateVersion( filePaths, version=[1,2,3,4], quietMode=QUIET )        
        updater.Update()
        expectedFilename = os.path.join( self.__testFolderBase, RC_RESULT_FILENAME )
        self.__compareFiles(expectedFilename, testFilename, "Patched RC file not as expected")
    
    def testRcWildcardFilePatch(self):
        testFilename = os.path.join( self.__testFolderBase, RC_TEST_MULTIPLE_FILENAME )
        filePaths = [testFilename]
        updater = uv.UpdateVersion( filePaths, version=[1,2,3,4], quietMode=QUIET )        
        updater.Update()
        expectedFilename = os.path.join( self.__testFolderBase, RC_RESULT_FILENAME )
        for filenum in xrange(0, RC_TEST_MULTIPLE_NUMFILES):
            testFilename = os.path.join( self.__testFolderBase, 
                                        (RC_TEST_MULTIPLE_FILENAME_F % filenum) )
            self.__compareFiles(expectedFilename, testFilename, "Patched RC file not as expected")
        
    def testRcMultipleFilePatch(self):
        filePaths = []
        for filenum in xrange(0, RC_TEST_MULTIPLE_NUMFILES):
            testFilename = os.path.join( self.__testFolderBase, 
                                        (RC_TEST_MULTIPLE_FILENAME_F % filenum) )
            filePaths = filePaths + [testFilename]
        updater = uv.UpdateVersion( filePaths, version=[1,2,3,4], quietMode=QUIET )        
        updater.Update()
        expectedFilename = os.path.join( self.__testFolderBase, RC_RESULT_FILENAME )
        for testFilename in filePaths:
            self.__compareFiles(expectedFilename, testFilename, "Patched RC file not as expected")
        
    def testAssemblyCsSingleFilePatch(self):
        testFilename = os.path.join( self.__testFolderBase, ASSEMBLY_CS_TEST_FILENAME )
        filePaths = [testFilename]
        updater = uv.UpdateVersion( filePaths, version=[1,2,3,4], quietMode=QUIET )        
        updater.Update()
        expectedFilename = os.path.join( self.__testFolderBase, ASSEMBLY_CS_RESULT_FILENAME )
        self.__compareFiles(expectedFilename, testFilename, "Patched AssemblyInfo.cs file not as expected")
    
    def testAssemblyCsWildcardFilePatch(self):
        testFilename = os.path.join( self.__testFolderBase, ASSEMBLY_CS_TEST_MULTIPLE_FOLDER )
        testFilename = os.path.join( testFilename, ASSEMBLY_CS_TEST_FILENAME )
        filePaths = [testFilename]
        updater = uv.UpdateVersion( filePaths, version=[1,2,3,4], quietMode=QUIET )        
        updater.Update()
        expectedFilename = os.path.join( self.__testFolderBase, ASSEMBLY_CS_RESULT_FILENAME )
        for foldernum in xrange(0, ASSEMBLY_CS_TEST_MULTIPLE_NUMFOLDERS):
            foldername = os.path.join( self.__testFolderBase, 
                                       (ASSEMBLY_CS_TEST_MULTIPLE_FOLDER_F % foldernum)  )
            testFilename = os.path.join( foldername, ASSEMBLY_CS_TEST_FILENAME )
            self.__compareFiles(expectedFilename, testFilename, "Patched AssemblyInfo.cs file not as expected")
        
    def testAssemblyCsMultipleFilePatch(self):
        filePaths = []
        for foldernum in xrange(0, ASSEMBLY_CS_TEST_MULTIPLE_NUMFOLDERS):
            foldername = os.path.join( self.__testFolderBase, 
                                       (ASSEMBLY_CS_TEST_MULTIPLE_FOLDER_F % foldernum)  )
            testFilename = os.path.join( foldername, ASSEMBLY_CS_TEST_FILENAME )
            filePaths = filePaths + [testFilename]
        updater = uv.UpdateVersion( filePaths, version=[1,2,3,4], quietMode=QUIET )        
        updater.Update()
        expectedFilename = os.path.join( self.__testFolderBase, ASSEMBLY_CS_RESULT_FILENAME )
        for testFilename in filePaths:
            self.__compareFiles(expectedFilename, testFilename, "Patched AssemblyInfo.cs file not as expected")

    def testRcAndAssemblyCsMultipleFilePatch(self):
        filePathsRc = []
        for filenum in xrange(0, RC_TEST_MULTIPLE_NUMFILES):
            testFilename = os.path.join( self.__testFolderBase, 
                                        (RC_TEST_MULTIPLE_FILENAME_F % filenum) )
            filePathsRc = filePathsRc + [testFilename]
        filePathsCs = []
        for foldernum in xrange(0, ASSEMBLY_CS_TEST_MULTIPLE_NUMFOLDERS):
            foldername = os.path.join( self.__testFolderBase, 
                                       (ASSEMBLY_CS_TEST_MULTIPLE_FOLDER_F % foldernum)  )
            testFilename = os.path.join( foldername, ASSEMBLY_CS_TEST_FILENAME )
            filePathsCs = filePathsCs + [testFilename]
        updater = uv.UpdateVersion( filePathsRc + filePathsCs, version=[1,2,3,4], quietMode=QUIET )        
        updater.Update()
        expectedFilename = os.path.join( self.__testFolderBase, RC_RESULT_FILENAME )
        for testFilename in filePathsRc:
            self.__compareFiles(expectedFilename, testFilename, "Patched RC file not as expected")
        expectedFilename = os.path.join( self.__testFolderBase, ASSEMBLY_CS_RESULT_FILENAME )
        for testFilename in filePathsCs:
            self.__compareFiles(expectedFilename, testFilename, "Patched AssemblyInfo.cs file not as expected")

    def testTemplateSingleFilePatch(self):
        testFilename = os.path.join( self.__testFolderBase, TEMPLATE_TEST_SINGLE_FILENAME )
        filePaths = [testFilename]
        template = os.path.join( self.__testFolderBase, TEMPLATE_TEMPLATE_FILENAME )
        updater = uv.UpdateVersion(filePaths, version=[1,2,3,4], applicationName="TEST",
                                   template=template, quietMode=QUIET )        
        updater.Update()
        expectedFilename = os.path.join( self.__testFolderBase, TEMPLATE_RESULT_FILENAME )
        self.__compareFiles(expectedFilename, testFilename, "Patched template file not as expected")
    
    def testTemplateCreateFilePatch(self):
        testFilename = os.path.join( self.__testFolderBase, TEMPLATE_TEST_CREATE_FILENAME )
        filePaths = [testFilename]
        template = os.path.join( self.__testFolderBase, TEMPLATE_TEMPLATE_FILENAME )
        updater = uv.UpdateVersion(filePaths, version=[1,2,3,4], applicationName="TEST",
                                   template=template, quietMode=QUIET )        
        updater.Update()
        expectedFilename = os.path.join( self.__testFolderBase, TEMPLATE_RESULT_FILENAME )
        self.__compareFiles(expectedFilename, testFilename, "Patched template file not as expected")
    
    def testTemplateMultipleFilePatch(self):
        testFilename = os.path.join( self.__testFolderBase, TEMPLATE_TEST_MULTIPLE_FILENAME )
        filePaths = [testFilename]
        template = os.path.join( self.__testFolderBase, TEMPLATE_TEMPLATE_FILENAME )
        updater = uv.UpdateVersion(filePaths, version=[1,2,3,4], applicationName="TEST",
                                   template=template, quietMode=QUIET)        
        updater.Update()
        expectedFilename = os.path.join( self.__testFolderBase, TEMPLATE_RESULT_FILENAME )
        for filenum in xrange(0, TEMPLATE_TEST_MULTIPLE_NUMFILES):
            testFilename = os.path.join( self.__testFolderBase, 
                                        (TEMPLATE_TEST_MULTIPLE_FILENAME_F % filenum) )
            self.__compareFiles(expectedFilename, testFilename, "Patched template file not as expected")
        
    def testCsprojSingleFilePatch(self):
        testFilename = os.path.join( self.__testFolderBase, CSPROJ_TEST_SINGLE_FILENAME )
        filePaths = [testFilename]
        updater = uv.UpdateVersion(filePaths, version=[1,2,3,4], applicationName="TEST",
                                   quietMode=QUIET )        
        updater.Update()
        expectedFilename = os.path.join( self.__testFolderBase, CSPROJ_RESULT_FILENAME )
        self.__compareFiles(expectedFilename, testFilename, "Patched sln file not as expected")
    
    def testFileNotFound(self):
        testFilename = os.path.join( self.__testFolderBase, NOT_FOUND_FILENAME )
        filePaths = [testFilename]
        updater = uv.UpdateVersion( filePaths, version=[1,2,3,4], quietMode=QUIET )        
        updater.PrintWarnings = False
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, uv.ERROR_TYPES["file_not_found"][0] )
        
    def testNoChangesMade(self):
        testFilename = os.path.join( self.__testFolderBase, TEMPLATE_TEST_CREATE_FILENAME )
        filePaths = [testFilename]
        template = os.path.join( self.__testFolderBase, NO_CHANGES_MADE_FILENAME )
        updater = uv.UpdateVersion(filePaths, version=[1,2,3,4], applicationName="TEST",
                                   template=template, quietMode=QUIET )        
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, uv.ERROR_TYPES["no_changes_made"][0] )
    
    def testUnsupportedFileType(self):
        testFilename = os.path.join( self.__testFolderBase, UNSUPPORTED_FILETYPE )
        filePaths = [testFilename]
        updater = uv.UpdateVersion( filePaths, version=[1,2,3,4], quietMode=QUIET )        
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, uv.ERROR_TYPES["filetype_unsupported"][0] )
    
    def testApplicationRequired(self):
        testFilename = os.path.join( self.__testFolderBase, TEMPLATE_TEST_SINGLE_FILENAME )
        filePaths = [testFilename]
        template = os.path.join( self.__testFolderBase, TEMPLATE_TEMPLATE_FILENAME )
        updater = uv.UpdateVersion(filePaths, version=[1,2,3,4],
                                   template=template, quietMode=QUIET )        
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, uv.ERROR_TYPES["application_required"][0] )
    
    def testWildcardsNotAllowed(self):
        testFilename = os.path.join( self.__testFolderBase, WILDCARDS_NOT_ALLOWED_FILENAME )
        filePaths = [testFilename]
        template = os.path.join( self.__testFolderBase, TEMPLATE_TEMPLATE_FILENAME )
        updater = uv.UpdateVersion(filePaths, version=[1,2,3,4], 
                                   template=template, applicationName="TEST", quietMode=QUIET )        
        updater.PrintWarnings = False
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, uv.ERROR_TYPES["no_wildcards_allowed"][0] )
    
    def testWildcardsNotMatched(self):
        testFilename = os.path.join( self.__testFolderBase, CSPROJ_TEST_SINGLE_FILENAME )
        emptyGlobFilename = os.path.join( self.__testFolderBase, WILDCARDS_NOT_ALLOWED_FILENAME )
        filePaths = [testFilename, emptyGlobFilename]
        updater = uv.UpdateVersion(filePaths, version=[1,2,3,4], 
                                   applicationName="TEST", quietMode=QUIET )        
        updater.PrintWarnings = False
        updater.Update()
        self.assertEqual( len(updater.Warnings), 1 )
        self.assertEqual( updater.Warnings[0], "WARNING: no files found matching " + emptyGlobFilename )
    
    def testInvalidLabel(self):
        testFilename = os.path.join( self.__testFolderBase, TEMPLATE_TEST_SINGLE_FILENAME )
        filePaths = [testFilename]
        template = os.path.join( self.__testFolderBase, BAD_TEMPLATE_FILENAME1 )
        updater = uv.UpdateVersion(filePaths, version=[1,2,3,4], applicationName="TEST",
                                   template=template, quietMode=QUIET )        
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception        
        self.assertEqual( e.exitCode, uv.ERROR_TYPES["invalid_label"][0] )
        template = os.path.join( self.__testFolderBase, BAD_TEMPLATE_FILENAME2 )
        updater = uv.UpdateVersion(filePaths, version=[1,2,3,4], applicationName="TEST",
                                   template=template, quietMode=QUIET )        
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, uv.ERROR_TYPES["invalid_label"][0] )

    def testVersionNotBytes(self):
        testFilename = os.path.join( self.__testFolderBase, TEMPLATE_TEST_SINGLE_FILENAME )
        filePaths = [testFilename]
        template = os.path.join( self.__testFolderBase, TEMPLATE_TEMPLATE_FILENAME )
        updater = uv.UpdateVersion(filePaths, version=[256,2,3,4], applicationName="TEST",
                                   template=template, bytesOnly=True, quietMode=QUIET )        
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, uv.ERROR_TYPES["version_not_bytes"][0] )
        updater = uv.UpdateVersion(testFilename, version=[1,256,3,4], applicationName="TEST",
                                   template=template, bytesOnly=True, quietMode=QUIET )        
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, uv.ERROR_TYPES["version_not_bytes"][0] )
        updater = uv.UpdateVersion(testFilename, version=[1,2,256,4], applicationName="TEST",
                                   template=template, bytesOnly=True, quietMode=QUIET )        
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, uv.ERROR_TYPES["version_not_bytes"][0] )
        updater = uv.UpdateVersion(testFilename, version=[1,2,3,256], applicationName="TEST",
                                   template=template, bytesOnly=True, quietMode=QUIET )        
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, uv.ERROR_TYPES["version_not_bytes"][0] )
    
    def testVdprojSingleFilePatch(self):
        testFilename = os.path.join( self.__testFolderBase, VDPROJ_TEST_SINGLE_FILENAME )
        filePaths = [testFilename]
        updater = uv.UpdateVersion( filePaths, version=[1,2,3,4], quietMode=QUIET )        
        updater.Update()
        expectedFilename = os.path.join( self.__testFolderBase, VDPROJ_RESULT_FILENAME )
        matchProductCode = re.compile("""(\s*)\"ProductCode\"\s*=\s*\"8:\{([0-9A-Fa-f]{8}\-[0-9A-Fa-f]{4}\-[0-9A-Fa-f]{4}\-[0-9A-Fa-f]{4}\-[0-9A-Fa-f]{12})\}\"""")
        self.__compareFiles(expectedFilename, testFilename, 
                            "Patched VDPROJ file not as expected",
                            concession=matchProductCode)

    def testLvprojSingleFilePatch(self):
        testFilename = os.path.join( self.__testFolderBase, LVPROJ_TEST_SINGLE_FILENAME )
        filePaths = [testFilename]
        updater = uv.UpdateVersion( filePaths, version=[11,22,33,4444], quietMode=QUIET )        
        updater.Update()
        expectedFilename = os.path.join( self.__testFolderBase, LVPROJ_RESULT_FILENAME )
        #matchProductCode = re.compile(r"""Property Name="Bld_version.(build|major|minor|patch)" """,re.IGNORECASE)
        self.__compareFiles(expectedFilename, testFilename, 
                            "Patched LVPROJ file not as expected")

    def testLvprojNoChangePatch(self):
        testFilename = os.path.join( self.__testFolderBase, LVPROJ_TEST_NOCHANGE_FILENAME )
        filePaths = [testFilename]
        updater = uv.UpdateVersion( filePaths, version=[11,22,33,4444], quietMode=QUIET )        
        with self.assertRaises(uv.UpdateError):
            updater.Update()
    
    
#Run the unit tests        
if __name__ == '__main__':
    if underTeamcity():
        runner = TeamcityTestRunner()
    else:
        runner = unittest.TextTestRunner(verbosity=1)
    unittest.main(testRunner=runner)
