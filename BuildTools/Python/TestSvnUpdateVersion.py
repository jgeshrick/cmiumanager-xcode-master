#********************************************************************* 
# (c) Sagentia Ltd., 2012.  All rights reserved.   
#  
# Repository path  : $HeadURL: https://svn.genericsgroup.local/Sagentia/LanguageLibrary/TeamCityBuildAgents/Versioning/TestSvnUpdateVersion.py $
# Last committed   : $Revision: 125 $
# Last changed by  : $Author: gd4 $
# Last changed date: $Date: 2014-07-25 10:22:30 +0100 (Fri, 25 Jul 2014) $
# ID: $Id: TestSvnUpdateVersion.py 125 2014-07-25 09:22:30Z gd4 $
#*********************************************************************  
# -*- coding: utf-8 -*-
"""
Unit tests of SvnUpdateVersion.py.

Note most of these are "live", they expect a working SVN repo that can be modified!
Ideally one might use a mock pysn instead, but this works well enough.

To run it, provide a repo URL for TestUpdateVersion.Files, username and password, eg.
python TestSvnUpdateVersion.py https://svn.sagentia.com/Sagentia/LanguageLibrary/TeamCityBuildAgents/Versioning/TestUpdateVersion.Files gd4 P@ssw0rd

Created on Mon Jan 07 17:55:38 2013

@author: ams1
"""

import SvnUpdateVersion as suv
import UpdateVersion as uv
import unittest
import pysvn
import argparse
import tempfile
import os
import shutil
import re
import sys

from teamcity import underTeamcity
from teamcity.unittestpy import TeamcityTestRunner

SVN_REPO = None
SVN_USER = None
SVN_PWD  = None

TEST_FILENAME = "_SvnTest.rc"
INIT_FILENAME = "_SvnInit.rc"

QUIET = True

class TestSvnUpdateVersion(unittest.TestCase):

    @staticmethod
    def svn_login( realm, username, may_save ):
        return True, SVN_USER, SVN_PWD, False
        
    @staticmethod
    def svn_ssl_server_trust_prompt( trust_dict ):
        return True, trust_dict["failures"], False
    
    def __checkRcFileVersions(self, fileName, testVersion):
        """
        Parses an RC file for version information. Returns a list of
        tuples containing the versions detected. There should be four
        tuples in the test file that is scanned.
        """
        f = open( fileName, "r" )
        lines = f.readlines()
        f.close()
        matchFixedInfo = re.compile("""(\s*)(FILE|PRODUCT)VERSION\s+(\d+)\,\s*(\d+)\,\s*(\d+)\,\s*(\d+)""")
        matchVersionInfo = re.compile("""(\s*)VALUE\s+\"(File|Product)Version\"\,\s*\"(\d+)\.(\d+).(\d+).(\d+)\"""")
        versions = []
        for line in lines:
            #FixedInfo version patch
            m = matchFixedInfo.match(line)
            if m:
                versionStr = (m.group(3), m.group(4), m.group(5), m.group(6))
                version = ( int(s) for s in versionStr )
                versions.append(version)
            #Language string value version patch
            m = matchVersionInfo.match(line)
            if m: 
                versionStr = (m.group(3), m.group(4), m.group(5), m.group(6))
                version = ( int(s) for s in versionStr )
                versions.append(version)
        versionsMatch = [ (v==testVersion) for v in versions ]
        self.assertTrue( not all(versionsMatch), "Version information not updated correctly"  )        
    
    def setUp(self):

        #Check out a copy of the repo for testing
        self.__svn = pysvn.Client()
        self.__svn.callback_get_login = TestSvnUpdateVersion.svn_login
        self.__svn.callback_ssl_server_trust_prompt = TestSvnUpdateVersion.svn_ssl_server_trust_prompt
        self.__tempFolder = tempfile.mkdtemp(prefix="TestSvnUpdateVersion_")
        self.__svn.checkout(SVN_REPO, self.__tempFolder)
        #Ensure that the test file matches the init file
        testFilename = os.path.join(self.__tempFolder,TEST_FILENAME)
        initFilename = os.path.join(self.__tempFolder,INIT_FILENAME)
        if not os.path.exists(testFilename) or not os.path.exists(initFilename):
            raise IOError("Test files not found")
        shutil.copyfile(initFilename, testFilename)
        self.__svn.checkin(self.__tempFolder, "MSPD-434 NOEMAIL NOEMAIL TestSvnUpdateVersion SVN setUp")
    
    def tearDown(self):
        del(self.__svn)
        shutil.rmtree(self.__tempFolder, ignore_errors=True)
    
    def testSimpleSvnUpdate(self):
        testVersion = (4,5,6,7)
        #Update the test file in SVN
        updater = suv.SvnUpdateVersion(svnRepo=SVN_REPO, svnUser=SVN_USER, 
                                       svnPwd=SVN_PWD, filePath=[TEST_FILENAME],
                                       logMsg="MSPD-434 NOEMAIL NOEMAIL TestSvnUpdateVersion test commit",
                                       version=testVersion, quietMode=QUIET )        
        updater.Update()
        #Now update our checkout of SVN
        self.__svn.update(self.__tempFolder)
        testFilename = os.path.join(self.__tempFolder, TEST_FILENAME)
        self.__checkRcFileVersions(testFilename, testVersion)
    
    def testSuppressMaj(self):
        testVersion = (4,5,6,7)
        suppress = ("Maj",)
        suppressVersion = (0,5,6,7)
        #Update the test file in SVN
        updater = suv.SvnUpdateVersion(svnRepo=SVN_REPO, svnUser=SVN_USER, 
                                       svnPwd=SVN_PWD, filePath=[TEST_FILENAME],
                                       logMsg="MSPD-434 NOEMAIL NOEMAIL TestSvnUpdateVersion test commit",
                                       version=testVersion, quietMode=QUIET,
                                       suppresses=suppress)        
        updater.Update()
        #Now update our checkout of SVN
        self.__svn.update(self.__tempFolder)
        testFilename = os.path.join(self.__tempFolder, TEST_FILENAME)
        self.__checkRcFileVersions(testFilename, suppressVersion)
    
    def testSuppressMin(self):
        testVersion = (4,5,6,7)
        suppress = ("Min",)
        suppressVersion = (4,0,6,7)
        #Update the test file in SVN
        updater = suv.SvnUpdateVersion(svnRepo=SVN_REPO, svnUser=SVN_USER, 
                                       svnPwd=SVN_PWD, filePath=[TEST_FILENAME],
                                       logMsg="MSPD-434 NOEMAIL TestSvnUpdateVersion test commit",
                                       version=testVersion, quietMode=QUIET,
                                       suppresses=suppress)        
        updater.Update()
        #Now update our checkout of SVN
        self.__svn.update(self.__tempFolder)
        testFilename = os.path.join(self.__tempFolder, TEST_FILENAME)
        self.__checkRcFileVersions(testFilename, suppressVersion)
    
    def testSuppressBld(self):
        testVersion = (4,5,6,7)
        suppress = ("Bld",)
        suppressVersion = (4,5,0,7)
        #Update the test file in SVN
        updater = suv.SvnUpdateVersion(svnRepo=SVN_REPO, svnUser=SVN_USER, 
                                       svnPwd=SVN_PWD, filePath=[TEST_FILENAME],
                                       logMsg="MSPD-434 NOEMAIL TestSvnUpdateVersion test commit",
                                       version=testVersion, quietMode=QUIET,
                                       suppresses=suppress)        
        updater.Update()
        #Now update our checkout of SVN
        self.__svn.update(self.__tempFolder)
        testFilename = os.path.join(self.__tempFolder, TEST_FILENAME)
        self.__checkRcFileVersions(testFilename, suppressVersion)
    
    def testSuppressRev(self):
        testVersion = (4,5,6,7)
        suppress = ("Rev",)
        suppressVersion = (4,5,6,0)
        #Update the test file in SVN
        updater = suv.SvnUpdateVersion(svnRepo=SVN_REPO, svnUser=SVN_USER, 
                                       svnPwd=SVN_PWD, filePath=[TEST_FILENAME],
                                       logMsg="MSPD-434 NOEMAIL TestSvnUpdateVersion test commit",
                                       version=testVersion, quietMode=QUIET,
                                       suppresses=suppress)        
        updater.Update()
        #Now update our checkout of SVN
        self.__svn.update(self.__tempFolder)
        testFilename = os.path.join(self.__tempFolder, TEST_FILENAME)
        self.__checkRcFileVersions(testFilename, suppressVersion)
    
    def testSuppressMany(self):
        testVersion = (4,5,6,7)
        suppress = ("Maj","Bld",)
        suppressVersion = (0,5,0,7)
        #Update the test file in SVN
        updater = suv.SvnUpdateVersion(svnRepo=SVN_REPO, svnUser=SVN_USER, 
                                       svnPwd=SVN_PWD, filePath=[TEST_FILENAME],
                                       logMsg="MSPD-434 NOEMAIL TestSvnUpdateVersion test commit",
                                       version=testVersion, quietMode=QUIET,
                                       suppresses=suppress)        
        updater.Update()
        #Now update our checkout of SVN
        self.__svn.update(self.__tempFolder)
        testFilename = os.path.join(self.__tempFolder, TEST_FILENAME)
        self.__checkRcFileVersions(testFilename, suppressVersion)
    
    def testSuppressAll(self):
        testVersion = (4,5,6,7)
        suppress = ("Maj","Min","Bld","Rev")
        suppressVersion = (0,0,0,0)
        #Update the test file in SVN
        updater = suv.SvnUpdateVersion(svnRepo=SVN_REPO, svnUser=SVN_USER, 
                                       svnPwd=SVN_PWD, filePath=[TEST_FILENAME],
                                       logMsg="MSPD-434 NOEMAIL TestSvnUpdateVersion test commit",
                                       version=testVersion, quietMode=QUIET,
                                       suppresses=suppress)        
        updater.Update()
        #Now update our checkout of SVN
        self.__svn.update(self.__tempFolder)
        testFilename = os.path.join(self.__tempFolder, TEST_FILENAME)
        self.__checkRcFileVersions(testFilename, suppressVersion)
    
    def testNoRepo(self):
        testVersion = (4,5,6,7)
        #Update the test file in SVN
        updater = suv.SvnUpdateVersion(svnUser=SVN_USER, 
                                       svnPwd=SVN_PWD, filePath=[TEST_FILENAME],
                                       logMsg="MSPD-434 NOEMAIL TestSvnUpdateVersion test commit",
                                       version=testVersion, quietMode=QUIET )
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, suv.ERROR_TYPES["no_svn_repo"][0] )
    
    def testNoUser(self):
        testVersion = (4,5,6,7)
        #Update the test file in SVN
        updater = suv.SvnUpdateVersion(svnRepo=SVN_REPO, 
                                       svnPwd=SVN_PWD, filePath=[TEST_FILENAME],
                                       logMsg="MSPD-434 NOEMAIL TestSvnUpdateVersion test commit",
                                       version=testVersion, quietMode=QUIET )
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, suv.ERROR_TYPES["no_svn_user"][0] )
    
    def testNoPwd(self):
        testVersion = (4,5,6,7)
        #Update the test file in SVN
        updater = suv.SvnUpdateVersion(svnRepo=SVN_REPO, svnUser=SVN_USER, 
                                       filePath=[TEST_FILENAME],
                                       logMsg="MSPD-434 NOEMAIL TestSvnUpdateVersion test commit",
                                       version=testVersion, quietMode=QUIET )
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, suv.ERROR_TYPES["no_svn_pwd"][0] )
    
    def testNoLogMsg(self):
        testVersion = (4,5,6,7)
        #Update the test file in SVN
        updater = suv.SvnUpdateVersion(svnRepo=SVN_REPO, svnUser=SVN_USER, 
                                       svnPwd=SVN_PWD, filePath=[TEST_FILENAME],
                                       version=testVersion, quietMode=QUIET )
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, suv.ERROR_TYPES["no_log_msg"][0] )
    
    def testTemplateConflict(self):
        testVersion = (4,5,6,7)
        #Update the test file in SVN
        updater = suv.SvnUpdateVersion(svnRepo=SVN_REPO, svnUser=SVN_USER, 
                                       svnPwd=SVN_PWD, filePath=[TEST_FILENAME],
                                       logMsg="MSPD-434 NOEMAIL TestSvnUpdateVersion test commit",
				       svnTemplate="foo", localTemplate="bar",
                                       version=testVersion, quietMode=QUIET )
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, suv.ERROR_TYPES["template_conflict"][0] )
    
    def testInvalidRepo(self):
        testVersion = (4,5,6,7)
        #Update the test file in SVN
        updater = suv.SvnUpdateVersion(svnRepo=SVN_REPO+"xxx", svnUser=SVN_USER, 
                                       svnPwd=SVN_PWD, filePath=[TEST_FILENAME],
                                       logMsg="MSPD-434 NOEMAIL TestSvnUpdateVersion test commit",
                                       version=testVersion, quietMode=QUIET )        
        with self.assertRaises(uv.UpdateError) as cm:
            updater.Update()
        e = cm.exception
        self.assertEqual( e.exitCode, suv.ERROR_TYPES["svn_checkout"][0] )
    
#Run the unit tests        
if __name__ == '__main__':

    #Get SVN repo, user name and password from the command line
    parser = argparse.ArgumentParser(description="Unit test for SvnUpdateVersion")
    parser.add_argument("svn_repo",
                        help="SVN repository. This needs to be the SVN path to the "
                        "Python/TestUpdateVersion.Files folder.")
    parser.add_argument("svn_user",
                        help="SVN user id")
    parser.add_argument("svn_pwd",
                        help="SVN password")
    parser.add_argument('unittest_args', 
                        nargs=argparse.REMAINDER,
                        help="Additional arguments passed to unittest runner")

    args = parser.parse_args()
    
    #Set globals
    SVN_REPO = args.svn_repo
    SVN_USER = args.svn_user
    SVN_PWD  = args.svn_pwd
    
    #Remove elements from sys.argv that have been used
    unittestArgv = [ sys.argv[0] ] + args.unittest_args
    
    #Run the tests
    if underTeamcity():
        runner = TeamcityTestRunner()
    else:
        runner = unittest.TextTestRunner(verbosity=2)
    unittest.main(testRunner=runner, argv=unittestArgv, failfast=True)

