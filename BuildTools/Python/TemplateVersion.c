/**
 * @brief This file is used by TeamCity to auto-generate files containing dynamic
 * version information.
 */
#include "%1$sVersion.h"

/**
 * @brief The version numbers filled in at build time (created by build server)
 */
static const S_IMAGE_VERSION_INFO version%1$s =
{
    0x%2$s,
    0x%3$s,
    0x%4$s,
    0x%5$s
};

/**
 * @brief  Returns the version (created by build server)
 * @return A const pointer to the version
 */
const S_IMAGE_VERSION_INFO* %1$sVersionGet(void)
{
    return(&version%1$s);
}
