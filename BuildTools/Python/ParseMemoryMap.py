#!C:\Python27\python.exe
# This program parses a C-Header file and converts each #define
# to Java properties format. - BWA
#
### I want the following format output:
### print 'BOOTLOADER_START_ADDRESS=0x0000'

import csv
import sys, getopt
import string
import re
import ast

defines = dict()
global keyList


def mysplit(string):
    if len(string) == 1:
        return string
    else:
        helper = re.split("([+-/*])",string.replace(' ', ''))
        helper = [ word for word in helper if word is not ' ' and word is not '' ]
        return helper

def myMath(left,middle,right):
    if middle == '+':
        return left + right
    elif middle == '-':
        return left - right
    elif middle == '/':
        return left / right
    elif middle == '*':
        return left * right
    else:
        raise


def dictReduce(key,listArg):
    print '  ->',
    print listArg
    try:
        length = len(listArg)
    except:
        return int(listArg)
    else:
        if listArg in defines.keys():
            print '    tick',
            print listArg,
            print ' --> ',
            print defines[listArg]
            return dictReduce(key,defines[listArg])
        elif length == 1:
            print '   L1'
            try:
                if str(listArg[0]) in defines.keys():
                    print '    tack',
                    print listArg[0],
                    print ' --> ',
                    print defines[str(listArg[0])]
                    return dictReduce(key,defines[listArg[0]])
                elif str(listArg[0]) in '+-/*':
                    print '    tock -->',
                    print listArg[0]
                    return listArg[0]
                else:
                    print '    tuck',
                    print listArg[0],
                    print ' --> ',
                    print listArg[0]
                    temp = listArg[0]
                    if isinstance(temp, basestring):
                        temp = ast.literal_eval(listArg[0])
                    print '     --> ',
                    print temp
                    return dictReduce(key,temp)

            except Exception, err:
                print '     --> ',
                print Exception, err
                return  listArg
        elif length == 2:
            print '   L2'
            try:
                temp1 = dictReduce(key,listArg[0])
                temp2 = dictReduce(key,listArg[1])
                if isinstance(temp1, list):
                    len1 = len(temp1)
                elif isinstance(temp1, int):
                    len1 = 1
                    temp1 = [temp1]
                else:
                    len1 = 1

                if isinstance(temp2, list):
                    len2 = len(temp2)
                elif isinstance(temp2, int):
                    len2 = 1
                    temp2 = [temp2]
                else:
                    len2 = 1
                sumLen = len1 + len2
                if sumLen == 2:
                    temp = [temp1, temp2]
                    print '    -2->',
                    print temp
                    return temp
                else:
                    temp = temp1 + temp2
                    print '    -M->',
                    print temp
                    return dictReduce(key,temp)
            except Exception, err:
                print "   Wouldn't reduce: ",
                print listArg
                print "   Because of",
                print Exception, err
                return listArg
        elif length == 3:
            print '   L3'
            try:
                left = dictReduce(key,[str(listArg[0])])
                middle = listArg[1]
                right = dictReduce(key,[listArg[2]])
                print "   result: " + str(myMath(left,middle,right))
                return myMath(left,middle,right)
            except:
                print "   This failed: " + str(listArg)
                return listArg
        elif length > 3:
            print "   ---> LOOK AT ME <---   "
            return dictReduce(key,[dictReduce(key,[listArg[0],listArg[1],listArg[2]])] + listArg[3::])
        else:
            return listArg

def main(argv):
    inputFile = ''
    outputFile = ''
    lineKey = '#define'
    outputString = ''
    keyList = []
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except:
        print 'convertHeader.py -i <input File> -o <output File>'
        sys.exit(2)
    for o, a in opts:
        if o == '-h':
            print 'convertHeader.py -i <input File> -o <output File>'
        elif o in ("-i", "--ifile"):
            inputFile = a
        elif o in ("-o", "--ofile"):
            outputFile = a
    print 'Input:  ', inputFile
    print 'Output: ', outputFile
    print ' '

    if inputFile == '' or outputFile == '':
        print 'I need both parameters.'
        print 'convertHeader.py -i <input File> -o <output File>'
        sys.exit(2)

    reader = csv.reader(open(inputFile, "rb"), delimiter=' ', skipinitialspace=True)
    try:
        for line in reader:
            if (lineKey in line) and not any('__attribute' in string for string in line):
                del line[0]
                if len(line) > 1:
                    helper = ''.join(line[1::])
                    helper = [", ".join(x.split()) for x in re.split(r'[()]',helper) if x.strip()]
                    helper = [mysplit(word) for word in helper]
                    keyList += [line[0]]
                    defines[line[0]] = helper
        for each in keyList:
            print each,
            print ' --> ',
            print defines[each]
            defines[each] = dictReduce(each,defines[each])
            print ' --->',
            print defines[each]
        print '\n\n'
        for each in keyList:
            print each + "=0x" + "{0:0{1}X}".format(defines[each],8)
            outputString += each + "=0x" + "{0:0{1}X}".format(defines[each],8) + '\n'
        outFile = open(outputFile,'w')
        outFile.write(outputString)
        outFile.close()
        return (0)
    except csv.Error as e:
        sys.exit('Error: ' % (e))

if __name__ == "__main__":
    main(sys.argv[1:])
