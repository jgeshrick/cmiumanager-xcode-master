//
//  AppDelegate.h
//  Demo
//
//  Created by Administrator on 12/03/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

