//
//  CMIUModeSettings.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 19/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMIUModeSettings.h"

@implementation CMIUMode (CMIUModeSettings)

+(NSDictionary*)getModes
{
    static dispatch_once_t oncePredicate;
    static NSDictionary* modes;

    dispatch_once(&oncePredicate, ^{

        /* ----------------- Modes ------------------
            Dictionary of modes with parameters - Read Interval (mins), Record Interval (mins), Report Interval (hrs), Name
         */

        modes = @{ @"Basic" : [[self alloc] initWithIntervalsRead:15 record:60 report:24 name:@"Basic"],
                   @"Advanced" : [[self alloc] initWithIntervalsRead:15 record:60 report:4 name:@"Advanced"],
                   @"Pro" : [[self alloc] initWithIntervalsRead:15 record:15 report:1 name:@"Pro"]
                };
        });

    return modes;
}

@end



