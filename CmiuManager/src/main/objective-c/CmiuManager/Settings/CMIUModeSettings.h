//
//  CMIUModes.h
//  CmiuManager
//
//  Created by Gamini Obeyesekere on 19/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#ifndef CMIUModeSettings_h
#define CMIUModeSettings_h

#import "CMIUMode.h"

@interface CMIUMode (CMIUModeSettings)

+(NSDictionary*)getModes;

@end

#endif /* CMIUModeSettings_h */
