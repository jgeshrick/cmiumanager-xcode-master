//
//  StatusButton.h
//  CmiuManager
//
//  Created by Matthew Houlden on 17/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorSelectButton.h"

@interface StatusButton : ColorSelectButton

@property (nonatomic) UIImage *image;

@end
