//
//  StatusButton.m
//  CmiuManager
//
//  Created by Matthew Houlden on 17/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import "StatusButton.h"

@interface StatusButton()
{
    UIImageView* _imageView;
}
@end

@implementation StatusButton

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    return [self init_StatusButton];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    return [self init_StatusButton];
}

-(id)init_StatusButton
{
    _imageView = [UIImageView new];
    [self addSubview:_imageView];
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    UILabel *label = self.titleLabel;
    float verticalSpacing = (self.frame.size.height - label.frame.size.height - _imageView.frame.size.height)/3.f;
    
    _imageView.frame = CGRectMake((self.frame.size.width - self.image.size.width)/2.f, verticalSpacing, self.image.size.width, self.image.size.height);
    
    CGRect frame = label.frame;
    frame.origin.y = self.frame.size.height - verticalSpacing - frame.size.height;
    label.frame = frame;
}

-(void)setImage:(UIImage *)image
{
    _imageView.image = image;
    [self setNeedsLayout];
}

-(UIImage*)image
{
    return _imageView.image;
}

@end
