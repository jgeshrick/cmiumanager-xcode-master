//
//  ColorSelectButton.m
//  CmiuManager
//
//  Created by Matthew Houlden on 17/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import "ColorSelectButton.h"

@implementation ColorSelectButton

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (!self.unhighlightedBackgroundColor)
    {
        self.unhighlightedBackgroundColor = self.backgroundColor;
    }
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
    if (highlighted && self.highlightedBackgroundColor)
    {
        self.backgroundColor = self.highlightedBackgroundColor;
    }
    else
    {
        self.backgroundColor = self.unhighlightedBackgroundColor;
    }
}

-(void)setHighlightedBackgroundColor:(UIColor *)highlightedBackgroundColor
{
    _highlightedBackgroundColor = highlightedBackgroundColor;
    
    if (self.highlighted)
    {
        self.backgroundColor = _highlightedBackgroundColor;
    }
}

-(void)setUnhighlightedBackgroundColor:(UIColor *)unhighlightedBackgroundColor
{
    _unhighlightedBackgroundColor = unhighlightedBackgroundColor;
    
    if (!self.highlighted)
    {
        self.backgroundColor = _unhighlightedBackgroundColor;
    }
}

@end
