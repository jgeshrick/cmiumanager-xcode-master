//
//  ColorSelectButton.h
//  CmiuManager
//
//  Created by Matthew Houlden on 17/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorSelectButton : UIButton

@property (nonatomic) UIColor *unhighlightedBackgroundColor;

@property (nonatomic) UIColor *highlightedBackgroundColor;

@end
