//
//  TrafficLightView.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 17/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "TrafficLightView.h"

@interface TrafficLightView ()

@property (weak, nonatomic) UILabel* label;
@property (weak, nonatomic) UIView* container;

@end

@implementation TrafficLightView
{
    CGPathRef axisPath;
    CAShapeLayer* axisLayer;
    
    NSLayoutConstraint* heightConstraint;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib
{
    UILabel* label = [[UILabel alloc] initWithFrame:self.frame];
    
    UIView* container = [[UIView alloc] initWithFrame:self.frame];
    
    _label = label;
    _container = container;
    
    // Configure container
    container.translatesAutoresizingMaskIntoConstraints = false;
    container.layer.cornerRadius = 5.0;
    container.clipsToBounds = true;
    
    // Add container
    [self addSubview:container];
    
    // Set up containers
    [container.leadingAnchor constraintEqualToAnchor:self.leadingAnchor].active = YES;
    [container.trailingAnchor constraintEqualToAnchor:self.trailingAnchor].active = YES;
    [container.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
    
    // Configure label
    label.translatesAutoresizingMaskIntoConstraints = false;

    // Attach
    [container addSubview:label];
    
    // Set up constraints
    [label.centerXAnchor constraintEqualToAnchor:container.centerXAnchor].active = YES;
    [label.centerYAnchor constraintEqualToAnchor:container.centerYAnchor].active = YES;
    
    // Add layer for axes
    [self initAxisLayer];
    
    // Set to default
    [self setLabelForColour:NONE animated:NO];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    // Add axes
    [self drawAxes];
}

-(void)setLabelForColour:(TrafficLightViewColour)colour animated:(BOOL)animated
{
    float heightMultiplier;
    UIColor* uiColour;
    NSString* text;
    
    switch( colour )
    {
            
        case AMBER:
            heightMultiplier = 0.6;
            uiColour = [UIColor yellowColor];
            text = @"Moderate";
            break;
            
        case GREEN:
            heightMultiplier = 0.9;
            uiColour = [UIColor greenColor];
            text = @"Excellent";
            break;
            
        case RED:
            heightMultiplier = 0.3;
            uiColour = [UIColor redColor];
            text = @"Poor";
            break;
            
        case NONE:
        default:
            heightMultiplier = 0.0;
            uiColour = [UIColor whiteColor];
            text = @"";
    }
    
    _label.text = text;

    // Layout after label text size change
    [self setNeedsLayout];
    [self layoutIfNeeded];
    
    // Deactivate old constraint
    heightConstraint.active = false;
    
    heightConstraint = [_container.heightAnchor constraintEqualToAnchor:self.heightAnchor multiplier:heightMultiplier constant:0];
    heightConstraint.active = true;
    
    void(^animationBlock)(void) = ^{
    
        [self layoutIfNeeded];
        _container.backgroundColor = uiColour;

    };
    
    if( animated )
    {
        [UIView animateWithDuration:0.7 animations:animationBlock];
    }
    else
    {
        animationBlock();
    }
}

-(void)setColour:(TrafficLightViewColour)colour
{
    if (![NSThread isMainThread])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setColour:colour];
        });
        return;
    }
    
    [self setLabelForColour:colour animated:YES];
}


-(void)initAxisLayer
{
    const CGFloat axisColour[] = {1.0, 0.0, 0.0, 1.0};
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();;
    
    axisLayer = [CAShapeLayer layer];
    
    axisLayer.fillColor = nil;
    axisLayer.strokeColor = CGColorCreate(colorSpace, axisColour);
    axisLayer.strokeStart = 0.0;
    axisLayer.strokeEnd = 1.0;
    
    [self.layer addSublayer:axisLayer];
    
    CGColorSpaceRelease(colorSpace);
}


-(void)drawAxes
{
    // Create path
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathMoveToPoint(path, nil, 0, 0);
    
    CGPathAddLineToPoint(path, nil, 0, self.bounds.size.height);
    CGPathAddLineToPoint(path, nil, self.bounds.size.width, self.bounds.size.height);
    
    axisLayer.path = path;

    // Release our refernece to path (now owned by layer)
    CGPathRelease(path);
}

@end
