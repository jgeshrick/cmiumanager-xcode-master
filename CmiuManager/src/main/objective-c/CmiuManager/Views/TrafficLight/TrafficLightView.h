//
//  TrafficLightView.h
//  CmiuManager
//
//  Created by UKHARMAC05 on 17/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum { NONE, RED, AMBER, GREEN } TrafficLightViewColour;

@interface TrafficLightView : UIView

-(void)setColour:(TrafficLightViewColour)colour;

@end
