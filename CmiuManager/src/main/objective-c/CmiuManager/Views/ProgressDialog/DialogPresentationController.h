//
//  DialogPresentationController.h
//  CmiuManager
//
//  Created by UKHARMAC05 on 05/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DialogPresentationController : UIPresentationController

@end

@interface DialogTransitionDelegate : NSObject <UIViewControllerTransitioningDelegate>

+(DialogTransitionDelegate*) getInstance;

@end

    
    

