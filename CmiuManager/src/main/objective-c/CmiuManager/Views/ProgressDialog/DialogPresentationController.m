//
//  DialogPresentationController.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 05/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "DialogPresentationController.h"

@implementation DialogPresentationController
{
    UIView* _dimmerView;
}


-(void)presentationTransitionWillBegin
{
    
    // Create background dimming subview
    _dimmerView = [[UIView alloc] initWithFrame:[[self containerView] bounds]];
    
    [_dimmerView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.6]];
    
    [[self containerView] addSubview:_dimmerView];
    
}


-(void)presentationTransitionDidEnd:(BOOL)completed
{
    
    if( !completed )
    {
        [_dimmerView removeFromSuperview];
        
    }
    
}

-(void)dismissalTransitionDidEnd:(BOOL)completed
{
    
    if( completed )
    {
        [_dimmerView removeFromSuperview];
    }
    
}

@end


@implementation DialogTransitionDelegate


-(UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source
{
    return [[DialogPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
    
}


+(DialogTransitionDelegate*) getInstance
{
    static DialogTransitionDelegate* _delegate;
    
    if( !_delegate )
    {
        _delegate = [DialogTransitionDelegate new];
    }
    
    return _delegate;
    
}


@end
