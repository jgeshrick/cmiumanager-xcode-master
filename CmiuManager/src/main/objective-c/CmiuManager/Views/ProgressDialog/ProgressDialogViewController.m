//
//  ProgressDialogViewController.m
//  CmiuManager
//
//  Created by Gamini Obeyesekere on 04/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "ProgressDialogViewController.h"
#import "DialogPresentationController.h"


@implementation ProgressDialogViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    // Set progress bar to zero
    [self setProgress:0];
    
    NSLog(@"Loaded");
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSLog(@"Appeared");
}


-(void)setProgress:(float)progress
{
    self.progressView.progress = progress;
}


-(void)setLabel:(NSString *)label
{
    self.dialogLabel.text = label;
}

-(id)init
{
    self = [self initWithNibName:@"ProgressDialogView" bundle:nil];
    
    [self setModalPresentationStyle:UIModalPresentationCustom];
    
    self.transitioningDelegate = [DialogTransitionDelegate getInstance];
    
    return self;    
}


+(ProgressDialogViewController *)progressControllerWithTitle:(NSString *)title
{
    ProgressDialogViewController* progressController = [[ProgressDialogViewController alloc] init];
    
    [progressController setLabel:title];
    [progressController setProgress:0];
    
    return progressController;
}


@end
