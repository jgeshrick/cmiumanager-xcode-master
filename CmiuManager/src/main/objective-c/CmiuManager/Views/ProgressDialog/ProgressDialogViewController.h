//
//  ProgressDialogViewController.h
//  CmiuManager
//
//  Created by Gamini Obeyesekere on 04/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressDialogViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *dialogLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

+(ProgressDialogViewController*)progressControllerWithTitle:(NSString*) title;

-(void)setProgress:(float) progress;
-(void)setLabel:(NSString*) label;

@end
