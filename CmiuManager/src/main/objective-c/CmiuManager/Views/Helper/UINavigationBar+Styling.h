//
//  UINavigationBar+Styling.h
//  CmiuManager
//
//  Created by Matthew Houlden on 15/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar(Styling)

- (void)applyStyling;

@end
