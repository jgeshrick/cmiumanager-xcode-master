//
//  UINavigationBar+Styling.m
//  CmiuManager
//
//  Created by Matthew Houlden on 15/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import "UINavigationBar+Styling.h"
#import "AppDelegate.h"

@implementation UINavigationBar(Styling)

- (void)applyStyling
{
    self.barTintColor = [AppDelegate darkGray];
    self.tintColor = [UIColor whiteColor];
    self.titleTextAttributes = @{
                                 NSFontAttributeName: [UIFont boldSystemFontOfSize:20.f],
                                 NSForegroundColorAttributeName : [UIColor whiteColor]
                                 };
}

@end
