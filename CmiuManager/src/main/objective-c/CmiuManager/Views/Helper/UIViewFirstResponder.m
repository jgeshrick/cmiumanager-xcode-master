//
//  UIViewFirstResponder.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 30/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "UIViewFirstResponder.h"

@implementation UIView (FirstResponder)

-(UIView*) findFirstResponder
{
    UIView* firstResponder;

    if( self.isFirstResponder )
    {
        return self;
    }
    else
    {
        for( UIView* view in self.subviews )
        {
            firstResponder = [view findFirstResponder];

            if( firstResponder )
            {
                return firstResponder;
            }

        }
    }

    return nil;
}

@end