//
//  UIViewInterfaceTable.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 07/09/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "UIViewInterfaceTable.h"

@implementation UIViewInterfaceTable

-(void)awakeFromNib
{
    [super awakeFromNib];

    // Disable content interaction delay
    if( !self.delaysContentTouches )
    {
        for( UIView* view in self.subviews )
        {
            if( [view isKindOfClass:[UIScrollView class]] )
            {
                ((UIScrollView*)view).delaysContentTouches = NO;
            }
        }
    }
}

@end
