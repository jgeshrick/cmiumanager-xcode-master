//
//  KeyboardScrollView.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 30/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "KeyboardScrollView.h"
#import "UIViewFirstResponder.h"

@implementation KeyboardScrollView
{
    UIEdgeInsets defaultInset;
}

-(void)awakeFromNib
{
    [super awakeFromNib];

    // Add listeners for keyboard
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasHidden:) name:UIKeyboardDidHideNotification object:nil];
}


-(void)keyboardWasShown:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];

    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height + 20, 0.0);

    defaultInset = self.contentInset;

    self.contentInset = contentInsets;
    self.scrollIndicatorInsets = contentInsets;

    // Check if item is active and scroll to make it visible
    UIView* firstResponder = [self findFirstResponder];

    if( firstResponder && firstResponder != self )
    {
        // Find position in our view
        CGRect scrollTarget = [firstResponder.superview convertRect:firstResponder.frame toView:self];

        [self scrollRectToVisible:scrollTarget animated:YES];
    }
}


- (void)keyboardWasHidden:(NSNotification*)notification
{
    self.contentInset = defaultInset;
    self.scrollIndicatorInsets = defaultInset;
}

@end
