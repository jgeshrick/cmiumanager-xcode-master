//
//  AppDelegate.m
//  CmiuManager
//
//  Created by Matthew Houlden on 15/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import "AppDelegate.h"
#import "ASColorUtils.h"
#import "ASCBCentralManager.h"
#import "CMIU.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

+ (UIColor *)mainOrange
{
    return AS_RGB(0xf47b20);
}

+ (UIColor *)pressedOrange
{
    return AS_RGB(0xe55302);
}

+ (UIColor *)black
{
    return AS_RGB(0x231f20);
}

+ (UIColor *)pressedBlack
{
    return AS_RGB(0x848589);
}

+ (UIColor *)darkGray
{
    return AS_RGB(0x848589);
}

+ (UIColor *)lightGray
{
    return AS_RGB(0xc1c2c4);
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Rewrite the Settings bundle
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *appInfo = [[NSBundle mainBundle] infoDictionary];
    NSString *version = appInfo[@"CFBundleShortVersionString"];
    
    [defaults setObject:version forKey:@"app_version"];
    [defaults synchronize];
    
    // Override point for customization after application launch.
    [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    
    
    // Handle launching from a notification
    UILocalNotification *note = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (note) {
        NSString *name = note.userInfo[@"CMIU"];
        if(name)
        {
            // Show the connect screen
        }
    }
    _enteringForeground = YES;
    
    return YES;
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)note
{
    NSString *identifierString = note.userInfo[@"CMIUIdentifier"];
    
    
    if(identifierString)
    {
        // Find the CMIU associated with this identifier
        UINavigationController * nav = (UINavigationController*)_window.rootViewController;
        CMIU *cmiu = (CMIU*)[[ASCBCentralManager sharedCentral] peripheralForIdentifierString:identifierString];
        if(cmiu)
        {
            // Show the connect screen
            [nav.topViewController performSegueWithIdentifier:@"Connect" sender:cmiu];
            return;
        }
        
        NSString *title = [NSString stringWithFormat:@"CMIU %@ is no longer in range", note.userInfo[@"CMIUName"]];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:title preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [nav.topViewController presentViewController:alert animated:YES completion:nil];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    _enteringForeground = NO;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    _enteringForeground = NO;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // We may receive a new advert as we enter the foreground. We don't want to process a local notification for this....
    _enteringForeground = YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [application cancelAllLocalNotifications];
    for(CMIU *cmiu in [[ASCBCentralManager sharedCentral] discoveredPeripherals])
    {
        cmiu.localNotification = nil;
    }
    _enteringForeground = NO;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
