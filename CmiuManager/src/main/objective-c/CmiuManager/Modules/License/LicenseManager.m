//
//  LicenseManager.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 31/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "LicenseManager.h"
#import "KeyChain.h"
#include <CommonCrypto/CommonCryptor.h>

@implementation LicenseManager

-(LicenseMode)getLicenseMode
{
    const char modeByte = 4;
    const char modeMask = 0x80;

    // Get security key
    NSData* ksData = [KeyChain getKey:@"ksData"];

    if( ksData && [self.class crcCheck:ksData] )
    {
        // Get user license mode
        if( ((uint8_t*)ksData.bytes)[modeByte] & modeMask )
        {
            return LICENSE_SUPERUSER;
        }
        else
        {
            return LICENSE_USER;
        }
    }
    else
    {
        return LICENSE_NONE;
    }
}

-(BOOL)registerLicense:(NSString *)license customer:(NSString*)customer
{
    char keyStoreData[] = {0x29, 0x68, 0x70, 0x5D, 0x0A, 0x6B, 0x32, 0x1F, 0x65, 0xBB, 0x81, 0x36, 0xCB, 0xF1, 0xE1, 0x12};

    if( license.length != 32 )
    {
        return NO;
    }

    // Get license bytes from hex string
    NSMutableData* licenseData = [[self parseHexKey:license] mutableCopy];

    // Check for successful hex string parsing
    if( !licenseData )
    {
        return NO;
    }

    NSData* keyStoreKey = [self.class getKeystoreKey];

    // Perform decryption of keystore
    if( CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionECBMode, keyStoreKey.bytes, keyStoreKey.length, nil, keyStoreData, sizeof(keyStoreData), keyStoreData, sizeof(keyStoreData), nil) != noErr )
    {
        @throw [NSException exceptionWithName:@"Cryptography error" reason:@"Could not decrypt key" userInfo:nil];
    }

    // NSLog(@"AES Key: %@", [NSData dataWithBytes:keyStoreData length:sizeof(keyStoreData)]);

    NSMutableData* customerBytes = [NSMutableData dataWithLength:sizeof(keyStoreData)];

    // Generate final customer specific key
    [customer getBytes:customerBytes.mutableBytes maxLength:customerBytes.length usedLength:nil encoding:NSASCIIStringEncoding options:kNilOptions range:NSMakeRange(0, customer.length) remainingRange:nil];

    for( int i = 0; i < sizeof(keyStoreData); i++ )
    {
        keyStoreData[i] ^= ((char*)customerBytes.bytes)[i];
    }

    // Decrypt license key
    if( CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionECBMode, keyStoreData, sizeof(keyStoreData), nil, licenseData.mutableBytes, licenseData.length, licenseData.mutableBytes, licenseData.length, nil) != noErr )
    {
        @throw [NSException exceptionWithName:@"Cryptography error" reason:@"Could not decrypt key" userInfo:nil];
    }

    // Extract license key
    NSData* licenseKey = [licenseData subdataWithRange:NSMakeRange(0, 12)];

    // Check CRC
    if( ![self.class crcCheck:licenseData] )
    {
        return NO;
    }

    NSCalendar* calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];

    // Load date components
    NSDate* licenseDate = [self.class parseDateFromKey:licenseKey];

    // Check for valid date
    if( !licenseDate )
    {
        return NO;
    }

    NSDate* weekFromLicenseDate = [calendar dateByAddingUnit:NSCalendarUnitDay value:7 toDate:licenseDate options:kNilOptions];

    if( [licenseDate compare:[NSDate date]] == NSOrderedDescending || [weekFromLicenseDate compare:[NSDate date]] == NSOrderedAscending )
    {
        NSLog(@"Invalid date");
        NSLog(@"Issued on: %@", licenseDate);
        return NO;
    }

    return [KeyChain setKey:@"ksData" value:licenseData];
}


-(NSData*)parseHexKey:(NSString*)str
{
    // Hex mapping
    const NSOrderedSet* hexMapping = [NSOrderedSet orderedSetWithArray:@[@"A", @"B", @"C", @"D", @"E", @"G", @"H", @"J", @"K", @"L", @"P", @"Q", @"R", @"T", @"U", @"V"]];

    const NSUInteger byteLength = (str.length + 1) / 2;

    NSString* upperString = str.uppercaseString;

    if( upperString.length == 0 || upperString.length != str.length )
    {
        return nil;
    }

    NSMutableData* stringBytes = [NSMutableData dataWithLength:byteLength];
    uint8_t* bytes = stringBytes.mutableBytes;

    // Convert to bytes
    for( int i = 0; i < str.length; i++ )
    {
        NSUInteger hexValue = [hexMapping indexOfObject:[upperString substringWithRange:NSMakeRange(i, 1)]];

        if( hexValue == NSNotFound )
        {
            return nil;
        }

        bytes[i/2] |= (hexValue & 0xf) << 4 * (1 - i % 2);
    }

    return stringBytes;
}

+(NSDate*)parseDateFromKey:(NSData*)key
{
    uint32_t dateValue;

    // Copy 4 bytes for date
    [key getBytes:&dateValue length:sizeof(dateValue)];

    // Convert endian
    dateValue = ntohl(dateValue);

    NSDateComponents* date = [NSDateComponents new];

    // Extract year
    date.year = dateValue / 10000;

    // Extract month
    date.month = (dateValue % 10000U) / 100;

    date.day = (dateValue % 100);

    // Convert to date
    NSCalendar* calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];

    return [calendar dateFromComponents:date];
}

+(NSData*)getKeystoreKey
{
    uint64_t seed = 25;
    NSMutableData* key = [NSMutableData dataWithCapacity:16];

    for( int i = 0; i < 16; i++ )
    {
        seed = seed / 9 + seed * 2;
        char rand = (char)(seed & 0x3f);
        [key appendBytes:&rand length:1];
    }

    return key;
}

+(BOOL)crcCheck:(NSData*) license
{
    uint16_t remainder = 0x0;
    const uint16_t polynomial = 0x1021;
    unsigned char bit;
    uint16_t checksum;
    uint16_t crc;

    // Extract CRC checksum from license
    [license getBytes:&checksum range:NSMakeRange(14, 2)];
    checksum = CFSwapInt16BigToHost(checksum);

    // Extract message from license
    NSData* message = [license subdataWithRange:NSMakeRange(0, 14)];

    // Perform modulo-2 division
    for (int byte = 0; byte < message.length; byte++)
    {
        // Bring the next byte into the remainder.
        remainder ^= ((uint16_t)(((uint8_t*)message.bytes)[byte])) << 8;

        // Perform modulo-2 division, a bit at a time.
        for (bit = 8; bit > 0; bit--)
        {
            if (remainder & 0x8000U)
            {
                remainder = ((remainder) << 1) ^ polynomial;
            }
            else
            {
                remainder = ((remainder) << 1);
            }
        }
    }

    // The final remainder is the CRC result.
    crc = remainder;

    return (crc == checksum);
}

-(BOOL)deactivateLicense
{
    return [KeyChain deleteKey:@"ksData"];
}

+(NSString *)getString:(LicenseMode)mode
{
    switch(mode)
    {
        case LICENSE_USER:
            return @"Basic";

        case LICENSE_SUPERUSER:
            return @"Advanced";

        default:
            return @"Unlicensed";
    }
}

+(LicenseManager *)getLicenseManager
{
    static LicenseManager* licenseManager;
    static dispatch_once_t initOnceToken;

    dispatch_once(&initOnceToken, ^{
        licenseManager = [[self alloc] initSingleton];
    });

    return licenseManager;
}

-(instancetype)init
{
    NSAssert(FALSE, @"LicenseManager should not be initialised directly");

    return nil;
}

-(instancetype)initSingleton
{
    return [super init];
}

@end
