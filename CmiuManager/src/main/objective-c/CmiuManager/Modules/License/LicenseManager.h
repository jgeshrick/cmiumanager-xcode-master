//
//  LicenseManager.h
//  CmiuManager
//
//  Created by UKHARMAC05 on 31/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//
#ifndef LicenseManager_h
#define LicenseManager_h

#import <Foundation/Foundation.h>

typedef enum {
    LICENSE_NONE, LICENSE_USER, LICENSE_SUPERUSER
} LicenseMode;


@interface LicenseManager : NSObject

+(LicenseManager*)getLicenseManager;

-(LicenseMode) getLicenseMode;

-(BOOL)registerLicense:(NSString*)key customer:(NSString*)customer;

-(BOOL)deactivateLicense;

+(NSString*)getString:(LicenseMode)mode;

@end

#endif
