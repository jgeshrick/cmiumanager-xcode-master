//
//  KeyChain.h
//  CmiuManager
//
//  Created by UKHARMAC05 on 31/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyChain : NSObject

+(NSData*)getKey:(NSString*)key;

+(BOOL)setKey:(NSString*)key value:(NSData*)value;

+(BOOL)deleteKey:(NSString*)key; 

@end
