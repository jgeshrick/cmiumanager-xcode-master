//
//  KeyChain.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 31/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "KeyChain.h"
@import Security;

static CFMutableDictionaryRef baseAttributes;
static CFMutableDictionaryRef searchAttributes;

@implementation KeyChain

+(void)initialize
{
    baseAttributes = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);

    // Set up query for generic key/value
    CFDictionaryAddValue(baseAttributes, kSecClass, kSecClassGenericPassword);

    searchAttributes = CFDictionaryCreateMutableCopy(kCFAllocatorDefault, 0, baseAttributes);

    CFDictionaryAddValue(searchAttributes, kSecMatchLimit, kSecMatchLimitOne);
    CFDictionaryAddValue(searchAttributes, kSecReturnData, kCFBooleanTrue);

}

+(BOOL)setKey:(const NSString *)key value:(NSData *)value
{

    OSStatus itemExists = SecItemCopyMatching([self getBaseAttributesForLabel:key], nil); // returns noErr if item found

    // Check if key already exists
    if( itemExists == noErr )
    {
        CFMutableDictionaryRef itemData = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
        [self addDataToDictionary:itemData data:value];

        // Update key
        if( SecItemUpdate(baseAttributes, itemData) == noErr )
        {
            return YES;
        }
    } else if( itemExists == errSecItemNotFound )
    {
        CFMutableDictionaryRef itemData = [self getBaseAttributesForLabel:key];
        [self addDataToDictionary:itemData data:value];

        // Add item
        if( SecItemAdd(itemData, nil) == noErr )
        {
            return YES;
        }
    }

    return NO;
}

+(BOOL)deleteKey:(NSString *)key
{
    if( SecItemDelete([self getBaseAttributesForLabel:key]) == noErr )
    {
        return YES;
    }

    return NO;
}


+(NSData *)getKey:(NSString *)key
{
    CFDataRef result;

    OSStatus err = SecItemCopyMatching([self getSearchAttributesForLabel:key], (CFTypeRef*)&result);

    // Retrieve item from KeyChain
    if( err == noErr )
    {
        return (__bridge_transfer NSData*)result;
    }
    else if( err == errSecItemNotFound )
    {
        return nil;
    }
    else
    {
        @throw [NSException exceptionWithName:@"License error" reason:@"License key error" userInfo:nil];
    }
}

+(void)addDataToDictionary:(CFMutableDictionaryRef)dict data:(NSData*)data
{
     CFDictionaryAddValue(dict, kSecValueData, (__bridge CFTypeRef)data);
}

+(CFMutableDictionaryRef)getSearchAttributesForLabel:(const NSString*)label
{
    CFMutableDictionaryRef dict = CFDictionaryCreateMutableCopy(kCFAllocatorDefault, 0, searchAttributes);
    CFDictionaryAddValue(dict, kSecAttrLabel, (__bridge CFTypeRef)label);

    return dict;
}

+(CFMutableDictionaryRef)getBaseAttributesForLabel:(const NSString*)label
{
    CFMutableDictionaryRef dict = CFDictionaryCreateMutableCopy(kCFAllocatorDefault, 0, baseAttributes);
    CFDictionaryAddValue(dict, kSecAttrLabel, (__bridge CFTypeRef)label);

    return dict;
}

-(instancetype)initInstance
{
    return [super init];
}


+(KeyChain*)getKeychain
{
    static KeyChain* keyChain;
    static dispatch_once_t initOnceToken;

    dispatch_once(&initOnceToken, ^{
        keyChain = [[self alloc] initInstance];
    });

    return keyChain;
}

-(instancetype)init
{
    NSAssert(FALSE, @"LicenseManager should not be initialised directly");

    return nil;
}

@end
