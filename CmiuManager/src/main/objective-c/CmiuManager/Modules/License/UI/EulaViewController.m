//
//  EulaViewController.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 26/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "EulaViewController.h"

@interface EulaViewController ()

@end

@implementation EulaViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Disable next button
    self.nextButton.enabled = false;

    NSError* error;

    NSAttributedString* license = [[NSAttributedString alloc] initWithURL:[[NSBundle mainBundle] URLForResource:@"License" withExtension:@"rtf"] options:@{ NSDocumentTypeDocumentAttribute : NSRTFTextDocumentType } documentAttributes:nil error:&error];

    // Load license
    self.licenseText.attributedText = license;

    if( !license )
    {
        @throw [NSException exceptionWithName:@"LicenseFile" reason:[@"Could not open EULA file: " stringByAppendingString:[error description]] userInfo:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)acceptSwitchChanged:(id)sender {

    // Enable/disable next button accordingly
    self.nextButton.enabled = self.acceptSwitch.on;
}

- (IBAction)nextButtonPress:(id)sender {

    // Check switch state
    if( self.acceptSwitch.on )
    {
        [self performSegueWithIdentifier:@"Next" sender:self];
    }
}
@end
