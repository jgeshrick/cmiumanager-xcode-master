//
//  LicenseNavigationController.h
//  CmiuManager
//
//  Created by UKHARMAC05 on 26/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "LicenseManager.h"

// Licensing callbacks
typedef void (^LicenseComplete)(LicenseMode);
typedef void (^LicenseFailed)(void);

@interface LicenseNavigationController : UINavigationController

@property LicenseComplete licenseComplete;
@property LicenseFailed licenseFailed;

-(void)segueDataLicenseComplete:(LicenseComplete)licenseComplete licenseFailed:(LicenseFailed)licenseFailed;

@end



