//
//  LicenseNavigationController.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 26/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "LicenseNavigationController.h"

@interface LicenseNavigationController ()

@end

@implementation LicenseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Check for segue data
    NSAssert(self.licenseComplete && self.licenseFailed, @"Licensing callbacks undefined");
}

#pragma mark - Navigation

// Data passed during segue
-(void)segueDataLicenseComplete:(LicenseComplete)licenseComplete licenseFailed:(LicenseFailed)licenseFailed
{
    self.licenseComplete = licenseComplete;
    self.licenseFailed = licenseFailed;
}

@end
