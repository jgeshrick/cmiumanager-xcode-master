//
//  LicenseViewController.h
//  CmiuManager
//
//  Created by UKHARMAC05 on 30/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardScrollView.h"

@interface LicenseViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *licenseField;
@property (weak, nonatomic) IBOutlet UITextField *customerField;

- (IBAction)confirmPress:(id)sender;

- (IBAction)dismissTap:(id)sender;

@end
