//
//  LicenseViewController.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 30/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "LicenseViewController.h"
#import "LicenseNavigationController.h"
#import "LicenseManager.h"

@interface LicenseViewController ()

@end

@implementation LicenseViewController
{
    __weak LicenseNavigationController* licenseController;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    NSAssert([[self parentViewController] isMemberOfClass:[LicenseNavigationController class]], @"LicenseViewController not child of LicenseNavigationController");

    // Register license delegate
    licenseController = (LicenseNavigationController*)[self parentViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)confirmPress:(id)sender {

    NSString* licenseKey = self.licenseField.text;
    NSString* customerId = self.customerField.text;

    // Register the license key
    if( [[LicenseManager getLicenseManager] registerLicense:licenseKey customer:customerId] )
    {
        NSLog(@"License valid registered");

        LicenseMode licenseMode = [[LicenseManager getLicenseManager] getLicenseMode];

        NSString* message = [@"Successfully registered as: " stringByAppendingString:[LicenseManager getString:licenseMode]];

        // Pop up success dialog
        UIAlertController* dialog = [UIAlertController alertControllerWithTitle:@"Registration success" message:message preferredStyle:UIAlertControllerStyleAlert];

        [dialog addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
            [licenseController licenseComplete]([[LicenseManager getLicenseManager] getLicenseMode]);
        }]];

        [self presentViewController:dialog animated:YES completion:nil];
    }
    else
    {
        NSLog(@"Invalid license key");

        // Pop up failure dialog
        UIAlertController* dialog = [UIAlertController alertControllerWithTitle:@"Registration failed" message:@"License key invalid" preferredStyle:UIAlertControllerStyleAlert];

        [dialog addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
            [licenseController licenseFailed]();
        }]];

        [self presentViewController:dialog animated:YES completion:nil];
    }
}

- (IBAction)dismissTap:(id)sender {

    [self.licenseField resignFirstResponder];

}
@end
