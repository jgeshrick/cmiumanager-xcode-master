//
//  EulaViewController.h
//  CmiuManager
//
//  Created by UKHARMAC05 on 26/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EulaViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISwitch *acceptSwitch;

@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UITextView *licenseText;

- (IBAction)acceptSwitchChanged:(id)sender;

- (IBAction)nextButtonPress:(id)sender;

@end