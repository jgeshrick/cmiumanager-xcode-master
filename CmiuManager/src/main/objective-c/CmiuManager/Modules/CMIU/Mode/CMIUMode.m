//
//  CMIUMode.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 19/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "CMIUMode.h"
#import "CMIUModeSettings.h"

@implementation CMIUMode

-(id)initWithIntervalsRead:(int)readInterval record:(int)recordInterval report:(int)reportInterval name:(NSString*)name
{
    _readInterval = readInterval;
    _recordInterval = recordInterval;
    _reportInterval = reportInterval;
    
    _name = [name copy];
    
    return self;
}

-(BOOL)isEqual:(id)object
{
    if( object == self )
    {
        return YES;
    }
    
    if( [object isKindOfClass:[CMIUMode class]] )
    {
        CMIUMode* mode = (CMIUMode*) object;

        if( mode.readInterval == self.readInterval &&
           mode.recordInterval == self.recordInterval &&
           mode.reportInterval == self.reportInterval )
        {
            return YES;
            
        }
    }

    return NO;
}

-(NSString*)lookupLabel
{
    CMIUMode* mode = [self lookupMode];

    if( mode )
    {
        return mode.name;
    }
    else
    {
        return @"Unrecognised";
    }
}

-(CMIUMode*)lookupMode
{
    NSArray* modes = [[self.class getModes] allKeysForObject:self];

    NSAssert(modes.count <= 1, @"Multiple CMIU modes for same 2R combination");

    if( modes.count > 0 )
    {
        return [self.class getModeFromLabel:modes[0]];
    }
    else
    {
        return nil;
    }
}

+(NSDictionary *)getModes
{
    @throw [NSException exceptionWithName:@"No Modes" reason:@"No mode settings defined" userInfo:nil];
}

+(CMIUMode*)getModeFromLabel:(NSString *)mode
{
    return [[CMIUMode getModes] objectForKey:mode];
}

+(CMIUMode*)lookupModeForParametersRead:(int)readInterval record:(int)recordInterval report:(int)reportInterval
{
    CMIUMode* mode = [[CMIUMode alloc] initWithIntervalsRead:readInterval record:recordInterval report:reportInterval name:nil];

    return [mode lookupMode];
}

@end

