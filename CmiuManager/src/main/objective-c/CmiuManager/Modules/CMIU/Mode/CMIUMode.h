//
//  CMIUMode.h
//  CmiuManager
//
//  Created by UKHARMAC05 on 19/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMIUMode : NSObject

@property (assign, readonly) int readInterval;
@property (assign, readonly) int recordInterval;
@property (assign, readonly) int reportInterval;
@property (readonly) NSString* name;

-(id)initWithIntervalsRead:(int)readInterval record:(int)recordInterval report:(int)reportInterval name:(NSString*)name;

-(BOOL)isEqual:(id)object;

-(NSString*)lookupLabel;

-(CMIUMode*)lookupMode;

+(CMIUMode*)getModeFromLabel:(NSString *)mode;

+(NSDictionary*)getModes;

+(CMIUMode*)lookupModeForParametersRead:(int)readInterval record:(int)recordInterval report:(int)reportInterval;

@end