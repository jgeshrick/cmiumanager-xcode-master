//
//  DebugLog.h
//  CmiuManager
//
//  Created by Gamini Obeyesekere on 11/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DebugLog : NSObject

-(id)initWithName: (NSString*) name;

-(BOOL) append: (NSString*) data;

-(NSString*) retrieve;

@end
