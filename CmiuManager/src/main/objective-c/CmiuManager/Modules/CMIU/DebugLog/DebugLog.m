//
//  DebugLog.m
//  CmiuManager
//
//  Created by Gamini Obeyesekere on 11/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "DebugLog.h"

@interface DebugLog ()

@property NSString* name;

@end

@implementation DebugLog
{
    NSFileHandle* writeFileHandle;
    NSFileHandle* readFileHandle;
    
    NSURL* backingFile;
}


-(id)init
{
    // Standard initialiser should not be used
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"-init not valid initializer" userInfo:nil];
}


-(id)initWithName: (NSString*) name
{
    self.name = name;
    
    // Generate unique file ID
    backingFile = [[NSURL fileURLWithPath:NSTemporaryDirectory()] URLByAppendingPathComponent:[[NSUUID UUID] UUIDString]];
    
    // Create file
    if( ![[NSFileManager defaultManager] createFileAtPath:backingFile.path contents:[NSData data] attributes:nil] )
    {
        NSLog(@"Could not create debug log file: %@", backingFile.path);
        
        return nil;
    }
    
    writeFileHandle = [NSFileHandle fileHandleForUpdatingURL:backingFile error:nil];
    readFileHandle = [NSFileHandle fileHandleForReadingFromURL:backingFile error:nil];
    
    if( !(writeFileHandle && readFileHandle) )
    {
        NSLog(@"Could not get handle to debug log file");
        return nil;
    }
    
    return self;
}

-(void)dealloc
{
    [writeFileHandle closeFile];
    [readFileHandle closeFile];
    
    // Delete file
    [[NSFileManager defaultManager] removeItemAtURL:backingFile error:nil];
}


-(BOOL)append:(NSString *)text
{
    NSData* stringData = [text dataUsingEncoding:NSUTF8StringEncoding];
    
    if( !stringData )
    {
        NSLog(@"Debug log string contains unsupported characters");
    }

    @try
    {
        @synchronized (self) {
            // Append data to file
            [writeFileHandle writeData:stringData];
        }
        
    } @catch( NSException* e )
    {
        NSLog(@"Could not write to debug log %@", e.reason);
        return NO;
    }
    
    return YES;
}


-(NSString*)retrieve
{
    NSString* debugLogText;
    
    [readFileHandle seekToFileOffset:0];
    
    @synchronized (self) {
        debugLogText = [[NSString alloc] initWithData:[readFileHandle readDataToEndOfFile] encoding:NSUTF8StringEncoding];
        NSLog(@"Log text: %@", debugLogText);
    }
    
    return debugLogText;
}


@end
