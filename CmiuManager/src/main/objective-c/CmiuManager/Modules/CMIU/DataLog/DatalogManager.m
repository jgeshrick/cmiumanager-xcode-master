//
//  DatalogManager.m
//  CmiuManager
//
//  Created by Matthew Houlden on 16/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import "DatalogManager.h"
#import "CMIU.h"

static NSString * const kUserDefaultsDatalogManagerKey = @"kUserDefaultsDatalogManagerKey";

const static float SECONDS_PER_DAY = 60*60*24;
const static float NUMBER_DAYS_UNTIL_EXPIRY = 30.f;

@interface DatalogManager()

@property (nonatomic) NSArray *datalogs;

@end

@implementation DatalogManager

#pragma mark - Singleton

+ (DatalogManager *)datalogManager
{
    static DatalogManager *datalogManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        datalogManager = [self new];
    });
    return datalogManager;
}

#pragma mark - Lifecycle

- (id)init
{
    self = [super init];
    if (self)
    {
        [self _loadDatalogsFromCache];
        [self _deleteExpiredDatalogs];
    }
    return self;
}

#pragma mark - Public methods

- (NSDictionary *)datalogForCMIU:(CMIU *)cmiu
{
    [self _deleteExpiredDatalogs];
    for (NSDictionary *datalog in self.datalogs)
    {
        if ([datalog[@"name"] isEqualToString:cmiu.name])
        {
            return datalog;
        }
    }
    return nil;
}

- (BOOL)hasDatalogForCMIU:(CMIU *)cmiu
{
    return ([self datalogForCMIU:cmiu] != nil);
}

- (void)downloadDatalogForCMIU:(CMIU *)cmiu
{
    if (!cmiu)
    {
        [self.delegate datalogDownloadFailed];
        return;
    }
    
    /*** FAKED ***/
    NSDictionary *fakeDatalog = @{
                                  @"name" : cmiu.name,
                                  @"data" : @[
                                          @{
                                              @"date" : [NSDate date],
                                              @"description" : @"something that happened today"
                                              },
                                          @{
                                              @"date" : [NSDate dateWithTimeIntervalSinceNow:-60*60*24],
                                              @"description" : @"something that happened yesterday"
                                              },
                                          @{
                                              @"date" : [NSDate dateWithTimeIntervalSinceNow:-60*60*24*2],
                                              @"description" : @"something that happened two days ago"
                                              }
                                          ]
                                  };
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(_downloadTimerFire:) userInfo:fakeDatalog repeats:NO];
}

- (void)_downloadTimerFire:(NSTimer *)timer
{
    /*** FAKED ***/
    [self _datalogDownloadComplete:timer.userInfo];
}

- (void)_datalogDownloadComplete:(NSDictionary *)downloadedDatalog
{
    NSMutableArray *mutableDatalogs = [self.datalogs mutableCopy];
    
    // If a datalog already exists for this CMIU then remove it
    for (NSDictionary *datalog in self.datalogs)
    {
        if ([datalog[@"name"] isEqualToString:downloadedDatalog[@"name"]])
        {
            [mutableDatalogs removeObject:datalog];
            break;
        }
    }
    
    // Add the new datalog with a timestamp
    NSMutableDictionary *mutableDownloadedDatalog = [downloadedDatalog mutableCopy];
    [mutableDownloadedDatalog setValue:[NSDate date] forKey:@"timestamp"];
    [mutableDatalogs addObject:mutableDownloadedDatalog];
    
    [self _saveDatalogs:mutableDatalogs];
    [self.delegate datalogDownloadSuccesful];
}

- (void)_datalogDownloadFailed
{
    [self.delegate datalogDownloadFailed];
}

#pragma mark - Properties

- (NSArray *)availableDatalogs
{
    [self _deleteExpiredDatalogs];
    
    NSMutableArray *datalogNames = [NSMutableArray new];
    for (NSDictionary *datalog in self.datalogs)
    {
        [datalogNames addObject:datalog[@"name"]];
    }
    return datalogNames;
}

#pragma mark - Private methods

- (void)_loadDatalogsFromCache
{
    self.datalogs = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsDatalogManagerKey];
}

- (void)_deleteExpiredDatalogs
{
    NSMutableArray *mutableDatalogs = [self.datalogs mutableCopy];

    for (NSDictionary *datalog in self.datalogs)
    {
        NSDate *timestamp = datalog[@"timestamp"];
        if ([self _isExpired:timestamp])
        {
            [mutableDatalogs removeObject:datalog];
        }
    }
    
    [self _saveDatalogs:mutableDatalogs];
}

- (void)_saveDatalogs:(NSArray *)datalogs
{
    if ([self.datalogs isEqualToArray:datalogs])
    {
        return;
    }
    
    self.datalogs = [NSArray arrayWithArray:datalogs];
    [[NSUserDefaults standardUserDefaults] setObject:self.datalogs forKey:kUserDefaultsDatalogManagerKey];
}

- (BOOL)_isExpired:(NSDate *)date
{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:date];
    BOOL expired = (timeInterval > SECONDS_PER_DAY * NUMBER_DAYS_UNTIL_EXPIRY);
    return expired;
}

@end
