//
//  DatalogManager.h
//  CmiuManager
//
//  Created by Matthew Houlden on 16/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CMIU;

@protocol DatalogDownloadDelegate <NSObject>

- (void)datalogDownloadSuccesful;
- (void)datalogDownloadFailed;

@end

@interface DatalogManager : NSObject

+ (DatalogManager *)datalogManager;

- (NSDictionary *)datalogForCMIU:(CMIU *)cmiu;

- (BOOL)hasDatalogForCMIU:(CMIU *)cmiu;

- (void)downloadDatalogForCMIU:(CMIU *)cmiu;

@property (nonatomic, readonly) NSArray *availableDatalogs;

@property (nonatomic, weak) id<DatalogDownloadDelegate> delegate;

@end
