//
//  SignalUtility.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 28/09/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SignalUtility.h"

@implementation SignalUtility

// Extract RSRQ value (dB) from AT+CSQ response
// As per 3.4.4.4.4 in Telit AT Command Reference Guide (r14)
+(CGFloat)extractRsrqFromSignal:(UInt16)signal
{
    // Extract raw RSRQ value stored in AT+CSQ response high byte
    UInt8 rawRsrq = signal & 0xff;
    rawRsrq = MIN(7, rawRsrq);

    // Convert RSRQ byte reading to dB value
    float rsrq = -3.5 - rawRsrq * 2;

    if( rawRsrq >= 4 )
    {
        rsrq -= 0.5;
    }

    if( rawRsrq >= 5 )
    {
        rsrq -= 0.5;
    }

    return rsrq;
}

// Extract RSSI value (dB) from AT+CSQ response
// As per 3.4.4.4.4 in Telit AT Command Reference Guide (r14)
+(CGFloat)extractRssiFromSignal:(UInt16)signal
{
    // Extract raw RSRQ value stored in AT+CSQ response high byte
    UInt8 rssi = (signal >> 8) & 0xff;
    rssi = MIN(31, rssi);

    // Convert RSSI byte reading to dB value
    return -113 + rssi * 2;
}

@end