//
//  SignalUtility.h
//  CmiuManager
//
//  Created by UKHARMAC05 on 28/09/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#ifndef SignalUtility_h
#define SignalUtility_h

#import <UIKit/UIKit.h>

@interface SignalUtility : NSObject

+(CGFloat)extractRsrqFromSignal:(UInt16)signal;
+(CGFloat)extractRssiFromSignal:(UInt16)signal;

@end

#endif /* SignalUtility_h */
