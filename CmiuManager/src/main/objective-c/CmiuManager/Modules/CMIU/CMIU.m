//
//  CMIU.m
//  CmiuManager
//
//  Created by Matthew Houlden on 15/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//
#import "CMIU.h"
#import "ASCBCentralManager.h"
#include "RingBuffer.h"
#include "UartController.h"
#include "Tpbp.h"
#include "HttpServer.h"
#include "SignalUtility.h"

/// // For nRF UART
//#define TX_CHARACTERISTIC @"6E400002-B5A3-F393-E0A9-E50E24DCCA9E"  
//#define RX_CHARACTERISTIC @"6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

#define TX_CHARACTERISTIC @"43370002-E802-91A7-6D40-50962F8FDB50"
#define RX_CHARACTERISTIC @"43370003-E802-91A7-6D40-50962F8FDB50"
    // return [CBUUID UUIDWithString:@"43370001-e802-91a7-6d40-50962f8fdb50"]; 

// We tick the Uart as soon as we receive data, and we never receive more than an L2CAP packet
// (20 bytes) at a time, unless we're negotiating for extended packets -
// so actually 128 is probably excessive
#define RING_BUFFER_SIZE_RX             128

// The HTTP buffer needs enough space for a full request
#define RING_BUFFER_SIZE_RX_HTTP        256

// Maximum length of any received message.
#define RX_PAYLOAD_BUFFER_SIZE                  2048

#define AUTO_DETECT_VALUE                       0x10

#define READ_ERROR_NO_DATA                      (UInt32)0x07000000
#define READ_ERROR_TIMEOUT                      (UInt32)0x07000007
#define READ_ERROR_INVALID                      (UInt32)0x0700000F
#define READ_ERROR_HISTORY                      (UInt32)0x07000010
#define READ_ERROR_UNKNOWN                      (UInt32)0x07FFFFFF

@interface CMIU ()
@property (nonatomic, assign) uint64_t cmiuClock;
@property (nonatomic, strong) NSString *firmwareRevision;
@property (nonatomic, strong) NSString *bootloaderRevision;
@property (nonatomic, assign) CGFloat rssiMostRecent;
@property (nonatomic, assign) CGFloat rsrqMostRecent;
@property (nonatomic, assign) int8_t temperature;
@property (nonatomic, strong) NSString *configRevision;
@property (nonatomic, strong) NSString *mqttBrokerAddress;
@property (nonatomic, strong) NSString *modemSwRevision;
@property (nonatomic, strong) NSString *modemSerialNumber;
@property (nonatomic, strong) NSString *SimImsi;
@property (nonatomic, strong) NSString *SimId;
@end

NSString * const kCMIUUpdated = @"kCMIUUpdated";
NSString * const kCMIUMeterRead = @"kCMIUMeterRead";
NSString * const kCMIUMeterReadComplete = @"kCMIUMeterReadComplete";

NSString * const kCMIUSignalUpdated = @"kCMIUSignalUpdated";
NSString * const kCMIUSignalUpdatedNewValueKey = @"NewValue";

NSString * const kCMIUHeartbeatStatusChanged = @"kCMIUHeartbeatStatusChanged";
NSString * const kCMIUHeartbeatStatusChangedNewValueKey = @"NewValue";

NSString * const kCMIUSignalStatusChanged = @"kCMIUSignalStatusChanged";;
NSString * const kCMIUSignalStatusChangedNewValueKey = @"NewValue";

const static float HEARTBEAT_NORMAL_INTERVAL = 5.0;
const static float HEARTBEAT_SLOW_INTERVAL = 20.0;
const static float SEND_SIGNAL_COMMAND_INTERVAL_INITIAL = 20.0;
const static float SEND_SIGNAL_COMMAND_INTERVAL_ACTIVE = 5.0;
const static float DELAY_SIGNAL_COMMAND_INTERVAL = 0.5;
const static float METER_READ_INTERVAL = 5;// Greater than we expect for 1 interface
const static UInt8 DEFAULT_TRANSMIT_WINDOW = 30; //Minutes

const NSDictionary* meterReadErrorCodes;

@implementation CMIU
{
    NSMutableData *message;
    CMIUResponseHandler _handler;
    NSMutableArray *writeQueue;
    NSMutableDictionary *writeQueueHandlers;
    NSRange writeRange;
    BOOL pendingWrite;
    DATA_PROVIDER _dataProvider;
    NSLock *arrayLock;
    
    // Ring buffer underlying data
    uint8_t                                 rxData[RING_BUFFER_SIZE_RX];
    RingBufferT                             rxRingBuffer;
    uint8_t                                 rxHttpData[RING_BUFFER_SIZE_RX_HTTP];
    RingBufferT                             rxHttpRingBuffer;
    uint8_t                                 txHttpData[HTTP_SERVER_HEADER_BUFFER_SIZE + HTTP_SERVER_CONTENT_BUFFER_SIZE];
    RingBufferT                             txHttpRingBuffer;
    
    // This is where messages get assembled.
    uint8_t                                 payloadBuffer[RX_PAYLOAD_BUFFER_SIZE];
    
    // Cellular RSSI and quality data
    UInt16                                  cellularRssiMostRecent;
    UInt16                                  cellularRssiStore[MAX_RSSI_STORE_SIZE];
    double                                  _signalQualityTime[MAX_RSSI_STORE_SIZE];
    double                                  _lastSignalQualityTime;
    float                                   _sendSignalCommandInterval;
    bool                                    _signalQualityAquisitionFlag;

    // Heartbeat timer interval
    float                                   _heartbeatTimerInterval;
    
    // Device meter measurements
    UInt64                                  meterReadId[MAX_CONNECTED_DEVICES];
    UInt32                                  meterReadValue[MAX_CONNECTED_DEVICES];
    UInt16                                  meterReadCnt;
    UInt16                                  meterReadAttemptCnt;

    bool                                    _meterReadActive;

    UART_CONTROLLER                         uc;
    UARC_MSG_READER                         mr;
    UARC_MSG_WRITER                         mw;
    
    NSTimeInterval                          lastTickTime;
    HTTP_SERVER                             server;
    
    S_RECORDING_REPORTING_INTERVAL recordingReportingInterval;
    S_INTERVAL_RECORDING_CONFIG intervalRecordingConfig;
    CMIUWriteProgressHandler    _httpWriteProgressHandler;
    
    NSTimer *_heartbeatTimeoutTimer;
    NSTimer *_signalTimeoutTimer;
    NSTimer *_sendSignalCommandTimeoutTimer;
    NSTimer *_delaySignalCommandTimeoutTimer;
    NSTimer *_MeterReadTimeoutTimer;
}

+(void)initialize
{
    meterReadErrorCodes = @{ @0x7000000 : @{ @"error" : @"Timeout" ,                        @"code" : @"::::::::" },
                             @0x7000001 : @{ @"error" : @"Parity Error" ,                   @"code" : @"UUUUUUUU" },
                             @0x7000002 : @{ @"error" : @"Checksum Failed" ,                @"code" : @"UUUUUUUU" },
                             @0x7000003 : @{ @"error" : @"Format Error" ,                   @"code" : @"UUUUUUUU" },
                             @0x7000004 : @{ @"error" : @"Unknown Register" ,               @"code" : @"UUUUUUUU" },
                             @0x7000005 : @{ @"error" : @"Mismatched Register Type" ,       @"code" : @"UUUUUUUU" },
                             @0x7000006 : @{ @"error" : @"Mismatched Register ID" ,         @"code" : @"UUUUUUUU" },
                             @0x7000007 : @{ @"error" : @"Message Timeout" ,                @"code" : @"UUUUUUUU" },
                             @0x700000F : @{ @"error" : @"Non-Numeric Reading" ,            @"code" : @"????????" },
                             @0x7000010 : @{ @"error" : @"Reading history not available" ,  @"code" : @"MMMMMMMM" },
                             @0x7FFFFFF : @{ @"error" : @"Unspecified Error" ,              @"code" : @"UUUUUUUU" } };
}

+(void)scanForCMIUs
{
    ASCBCentralManager *manager = [ASCBCentralManager sharedCentral];

    manager.serviceToScanFor = [self uartServiceUUID];
    manager.peripheralSetupBlock = ^(ASCBCentralManager *manager, ASCBPeripheral *objectForReuse, CBPeripheral *peripheral)
    {
        if(objectForReuse)
        {
            assert(objectForReuse.peripheral == peripheral);
            return objectForReuse;
        }
        ASCBPeripheral *cmiu = [[CMIU alloc] initWithPeripheral:peripheral centralManager:manager];
        cmiu.advertTimeout = CMIU_ADVERT_TIMEOUT;
        return cmiu;
    };
}

static void OnReceiveMessage(const struct UARC_MSG* pRxMsg, const void *context)
{
    CMIU *cmiu = (__bridge CMIU*)context;
    [cmiu receivedMessage:pRxMsg];
}

-(id)initWithPeripheral:(CBPeripheral*)peripheral centralManager:(ASCBCentralManager*)centralManager
{
    self = [super initWithPeripheral:peripheral centralManager:centralManager];
    message = nil;
    writeQueue = [NSMutableArray new];
    writeQueueHandlers = [NSMutableDictionary new];
    self.alwaysNotifyCharacteristicKeys = @[  @"RX" ];
    self.characteristicKeyMap = @{  TX_CHARACTERISTIC : @"TX", RX_CHARACTERISTIC : @"RX"};
    __weak id weakSelf = self;

    // Set intial hearbeat period
    _heartbeatTimerInterval = HEARTBEAT_NORMAL_INTERVAL;
    self.heartbeatStatus = CMIU_HeartbeatStatus_Disconnected;
    self.signalStatus = CMIU_SignalStatus_ModemInit;
    self->cellularRssiMostRecent = 0;
    _cellularRssiStoreCount = 0;
    _sendSignalCommandInterval = SEND_SIGNAL_COMMAND_INTERVAL_INITIAL;

    // Reading interval defined in CMIU firmware
    _readingInterval = 15;

    for (int x = 0; x < MAX_CONNECTED_DEVICES; ++x)
    {
        self->meterReadId[x] = 0;
        self->meterReadValue[x] = 0;
    }
    self->meterReadCnt = 0;

    self->arrayLock = [[NSLock alloc] init];
    self.detailConfigSuccess = false;
    [self setHandler:^(ASCBPeripheral *peripheral, NSString *key, NSData *value, NSError *error) {
        // Got some data on the receive channel.
        CMIU *strongSelf = weakSelf;
        if(!error)
        {
            RingBufferWrite(&strongSelf->rxRingBuffer, value.bytes, (uint32_t)value.length);
            [strongSelf tickUart];
        }
        else
        {
            NSLog(@"Error receiving data: %@", error);
        }
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [strongSelf->_activityDelegate cmiuWasActive:strongSelf];
//        });
        

    } forKey:@"RX"];
    [self setWriteHandler:^(ASCBPeripheral *peripheral, NSString *key, NSError *error) {
        
        // Check the TX buffer for any more data.
        
        [weakSelf wroteDataWithError:error];
    } forKey:@"TX"];
    
    
    return self;
}


-(void)postStateChangeNotification
{
    // Run this on the same thread as the other callbacks
    //  dispatch_async([ASCBCentralManager bluetoothQueue], ^(void) {
    // Reset pending write state
    [self clearWriteQueue];
    @synchronized(self) {
        pendingWrite = NO;
        
        if(self.state == ASCBPeripheralState_Connected)
        {
            NSLog(@"Initialising uart and HTTP stack");
            // Reset the UART  on a connection event.
            UART_CONTROLLER_INIT_PARAMS initParams = {0};
            initParams.pRbReceive           = &rxRingBuffer;
            initParams.pMessageReader       = &mr;
            initParams.pfnRxHandler         = &OnReceiveMessage;
            initParams.context              = (__bridge const void*)self;
            
            // Set intial hearbeat period
            _heartbeatTimerInterval = HEARTBEAT_NORMAL_INTERVAL;
            
            // Reset the Cellular RSSI data
            self.signalStatus = CMIU_SignalStatus_ModemInit;
            self->cellularRssiMostRecent = 0;

            for (int x = 0; x < MAX_RSSI_STORE_SIZE; ++x)
            {
                self->cellularRssiStore[x] = 0;
                self->_signalQualityTime[x] = 0;
            }

            _cellularRssiStoreCount = 0;

            self.detailConfigSuccess = false;
            
            RingBufferInit(&rxRingBuffer, rxData, sizeof(rxData));
            
            // Init reader to use the assembly message
            UartMessageReaderInit (&mr, payloadBuffer, sizeof(payloadBuffer));
            UartControllerInit(&uc, &initParams);
            UartControllerTick(&uc, 0);
            lastTickTime = [NSDate timeIntervalSinceReferenceDate];
            
            // Set the signal quality command interval to initial interval
            _sendSignalCommandInterval = SEND_SIGNAL_COMMAND_INTERVAL_INITIAL;
            
            // Stop any old timers
            [self _stopGetSignalTimer];
            [self _stopSendSignalCommandTimer];
            [self _stopMeterReadTimer];
            
            [self _startHeartbeatTimer];
            // Also start the timer used for getting signal strength/quality from modem
        }
        else
        {
            [self _stopHeartbeatTimer];
            [self _stopGetSignalTimer];
            [self _stopSendSignalCommandTimer];
            [self _stopMeterReadTimer];
        }
    }
    
    [self checkWriteQueue :true];
    
    [super postStateChangeNotification];
}

-(void)cancelConnection
{
    [self _stopGetSignalTimer];
    [self _stopSendSignalCommandTimer];
    [self _stopMeterReadTimer];
    
    // Release debug log
    self.debugLog = nil;
    
    [super cancelConnection];
}


-(void)connect
{
    // Initialise debug data log
    self.debugLog = [[DebugLog alloc] initWithName:self.name];
    
    if( !self.debugLog )
    {
        NSLog(@"Error initialising debug message log");
    }
    
    [super connect];
}

-(void)clearWriteQueue
{
    @synchronized(self) {
        [writeQueue removeAllObjects];
        [writeQueueHandlers removeAllObjects];
    }
}

-(void)resetDiscovery
{
    [super resetDiscovery];
    if(_localNotification)
    {
        UIApplication *app = [UIApplication sharedApplication];
        [app cancelLocalNotification:_localNotification];
        self.localNotification = nil;
    }
}

+ (CBUUID *) uartServiceUUID
{
       return [CBUUID UUIDWithString:@"43370001-e802-91a7-6d40-50962f8fdb50"]; 
  //  return [CBUUID UUIDWithString:@"6e400001-b5a3-f393-e0a9-e50e24dcca9e"]; // As per nordic nRFUart demo - works as UART 
}

-(void)tickUart
{
    NSTimeInterval now = [NSDate timeIntervalSinceReferenceDate];
    uint32_t msSinceLastTick = 1000 *(now - lastTickTime);
    lastTickTime = now;
    UartControllerTick(&uc, msSinceLastTick);
}

-(void)startWriting:(NSData*)data handler:(CMIUWriteProgressHandler)handler :(bool) isActiveFlag
{
    @synchronized(self) {
        [writeQueue addObject:data];
        if(handler)
        {
            [writeQueueHandlers setObject:handler forKey:data];
        }
        [self checkWriteQueue: isActiveFlag];
    }
}

-(void)checkWriteQueue:(bool) isActiveFlag
{
    @synchronized(self) {
        if(!writeQueue.count || pendingWrite)
        {
            return;
        }
        if(![self isConnectedOrConnecting])
        {
            [self connect];
            return;
        }
        if(!self.isReadableWriteable)
        {
            return;
        }
        pendingWrite = YES;
        
        // Start writing this one.
        NSData *dataToWrite = [writeQueue objectAtIndex:0];
        writeRange.location = 0;
        writeRange.length = MIN(20, dataToWrite.length);
        
        if (true == isActiveFlag)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_activityDelegate cmiuWasActive:self];
            });
        }
        
        [self writeValue:[dataToWrite subdataWithRange:writeRange] forKey:@"TX"];
    }
}

-(void)wroteDataWithError:(NSError*)error
{
    @synchronized(self) {
        // if write queue is empty, we have cleared it out to cancel everything!
        if(!writeQueue.count)
        {
            pendingWrite = NO;
            return;
        }

        // Assume no error, and continue.
        NSData *dataToWrite = [writeQueue objectAtIndex:0];
        writeRange.location += writeRange.length;
        
        CMIUWriteProgressHandler handler = [writeQueueHandlers objectForKey:dataToWrite];
        // need to figure out error handling
        if(error)
        {
            // Device has gone to sleep??
            // Fail this write.
            
            // This is hacky, do something correct!
            writeRange.location = -1;
        }
        
        
        if(handler)
        {
            handler(writeRange.location, dataToWrite.length);
        }
        
        if(writeRange.location == dataToWrite.length || writeRange.location == -1)
        {
            // Done
            [writeQueue removeObjectAtIndex:0];
            [writeQueueHandlers removeObjectForKey:dataToWrite];
            pendingWrite = NO;
            
            [self checkWriteQueue:true];
        }
        else
        {
            // Write the next chunk
            writeRange.length = MIN(20, dataToWrite.length - writeRange.location);
            [self writeValue:[dataToWrite subdataWithRange:writeRange] forKey:@"TX"];
        }
    }
    
}

-(void)sendGarbage:(size_t)length progressHandler:(CMIUWriteProgressHandler)handler
{
    assert(length %4 == 0);
    uint8_t bytes[] = { 0xDE, 0xAD, 0xBE, 0xEF };
    
    NSMutableData *payload = [NSMutableData dataWithLength:length];
    uint8_t *writeBytes = payload.mutableBytes;
    for(int i = 0; i < payload.length - 4 ; i += 4)
    {
        memcpy(writeBytes+i, bytes, 4);
    }

    
    [self startWriting:payload handler:handler:true];
}



#define TRY(f)   if ((f) == false) { NSLog(@"TRY failure at line %d", __LINE__); break; }

-(NSData*)buildCommand:(uint8_t)commandByte secureDataHandler:(void (^)(S_TPBP_PACKER* packer))secureDataHandler
{

    
    // Format a CMIU message.
    S_TPBP_PACKER   packer;
    uint8_t*        pSecureData;
    uint32_t        numSecureBytes = 0;
    uint32_t        numBytesToSend = 0;
    S_MQTT_BLE_UART     uartHeader = {0,0,0,0};
    
    uint8_t workingBuffer[256];
    
    Tpbp_PackerInit(&packer, workingBuffer, sizeof(workingBuffer));
    
    // Break out with isOk set false if any function returns false, else fall through
    do
    {
        // Begin Packet
        TRY(Tpbp_Packer_BeginPacket(&packer, E_PACKET_TYPE_COMMAND));
        // The UART header must be present, but its values are completely ignored.
        TRY(Tpbp_PackerAdd_UartPacketHeader(&packer, &uartHeader));
        
        // Secure region (we are required to send a secure region, but encryption is not yet supported).
        TRY(Tpbp_Packer_BeginSecurePack(&packer));
        
        TRY(Tpbp_PackerAdd_Command(&packer,  commandByte));
        if(secureDataHandler)
        {
            secureDataHandler(&packer);
        }
        pSecureData = Tpbp_Packer_EndSecurePack(&packer, &numSecureBytes);
        // fakeEncrypt(pSecureData, pSecureData, numSecureBytes); // In-place encryption
        TRY(Tpbp_Packer_EndPacket(&packer));
    } while(NO);
    
    numBytesToSend = Tpbp_PackerGetCount(&packer);
    
    
    
    
    UARC_TX_MSG msg = {UARC_MT_CMD, numBytesToSend, workingBuffer};
    uint32_t len = UartControllerMessageWriterCalculateSize(&msg);
    NSMutableData *encoded = [NSMutableData dataWithLength:len];
    UartControllerMessageWriterWriteBuffer(&mw, &msg, encoded.mutableBytes, (uint32_t)encoded.length);
    
    return encoded;
}

-(NSData*)buildUpdateImage:(NSString*)filePath
{
    NSData *encoded = [self buildCommand:E_COMMAND_UPDATE_IMAGE secureDataHandler:^(S_TPBP_PACKER *packer) {
        S_IMAGE_METADATA imageMetadata;
        S_IMAGE_VERSION_INFO    imageVersionInfo;
        do
        {
            TRY(Tpbp_PackerAdd_UInt64(packer, E_TAG_NUMBER_TIMESTAMP, 0));
            TRY(Tpbp_PackerAdd_CharArray(packer, E_TAG_NUMBER_IMAGE, filePath.UTF8String));
            TRY(Tpbp_PackerAdd_ImageMetadata(packer, &imageMetadata));
            TRY(Tpbp_PackerAdd_ImageVersionInfo(packer, E_TAG_NUMBER_FIRMWARE_REVISION, &imageVersionInfo));
            
        } while(NO);
        
    }];
    return encoded;
}

-(NSData*)buildSetRecordingReportingInterval
{
    NSData *encoded = [self buildCommand:E_COMMAND_SET_RECORDING_AND_REPORTING_INTERVALS secureDataHandler:^(S_TPBP_PACKER *packer) {
        do
        {
            TRY(Tpbp_PackerAdd_RecordingReportingInterval(packer, &recordingReportingInterval));
        } while(NO);
    }];
    return encoded;
}


-(void)updateIntervalConfigRecording:(uint8_t)recording reporting:(uint8_t)reporting start:(uint16_t)start
{
    // Only some values used by the CMIU.
    recordingReportingInterval.recordingIntervalMins = recording; //interval for recording, in minutes
    recordingReportingInterval.reportingIntervalHours = reporting; // reporting
    recordingReportingInterval.reportingStartMins = start; // interval start time, U16 after midnight
    recordingReportingInterval.reportingTransmitWindowsMins = DEFAULT_TRANSMIT_WINDOW;
    
    //Also update tag 34 parameters so it can be used in detailed config packet
    intervalRecordingConfig.recordingIntervalMin = recording; //interval for recording, in minutes
    intervalRecordingConfig.reportingIntervalHrs = reporting; // reporting
    intervalRecordingConfig.intervalStartTime = start; // interval start time, U16 after midnight
    
    NSData *encoded = [self buildSetRecordingReportingInterval];
    [self startWriting:encoded handler:nil:true];
}

-(uint8_t)reportingInterval
{
    return recordingReportingInterval.reportingIntervalHours;
}

-(uint8_t)recordingInterval
{
    return recordingReportingInterval.recordingIntervalMins;
}

-(uint16_t)startReportingTime
{
    return recordingReportingInterval.reportingStartMins;
}



-(void)setReportingInterval:(uint8_t)reportingInterval
{
    recordingReportingInterval.reportingIntervalHours = reportingInterval;
}

-(void)setRecordingInterval:(uint8_t)recordingInterval
{
    recordingReportingInterval.recordingIntervalMins = recordingInterval;
}

-(void)setStartReportingTime:(uint16_t)startReportingTime
{
    recordingReportingInterval.reportingStartMins = startReportingTime;
}

-(NSData*)buildGetSignalQualityCommand
{
    NSData *encoded = [self buildCommand:E_COMMAND_GET_CMIU_SIGNAL_QUALITY secureDataHandler:^(S_TPBP_PACKER *packer) {
        do
        {
            // No tags required.
        } while(NO);
    }];
    return encoded;
}

-(void)_sendGetSignalQualityCommand
{
    //NSLog(@"Sig Quality Command sent\n");
    NSData *encoded = [self buildGetSignalQualityCommand];
    [self startWriting:encoded handler:nil:false];
}

-(NSData*)buildReadConnectedDevicesCommand
{
    UInt64 startDelay = 10;
    S_COMMAND_READCONNECTEDEVICE readDeviceTag;
    readDeviceTag.pulse = (UInt8)AUTO_DETECT_VALUE;
    readDeviceTag.numberOfRepeats = (UInt64)0;
    readDeviceTag.delayBetweenReadsMsec = (UInt64)1500;
    NSData *encoded = [self buildCommand:E_COMMAND_READ_CONNECTED_DEVICES secureDataHandler:^(S_TPBP_PACKER *packer) {
        do
        {
            TRY(Tpbp_PackerAdd_Command_StartDelay(packer, startDelay));
            TRY(Tpbp_PackerAdd_Command_ReadConnectedDevice(packer, &readDeviceTag));
        } while(NO);
    }];
    return encoded;
}

-(void)sendReadConnectedDevicesCommand
{
    // Reset the connected device meter reading arrays
    for (int x = 0; x < MAX_CONNECTED_DEVICES; ++x)
    {
        self->meterReadId[x] = 0;
        self->meterReadValue[x] = 0;
    }
    self->meterReadCnt = 0;
    
    // Start progress timer
    [self _startMeterReadTimer];
    
    // Reset attempt counter
    self->meterReadAttemptCnt = 0;

    _meterReadActive = true;
    
    NSData *encoded = [self buildReadConnectedDevicesCommand];
    [self startWriting:encoded handler:nil :true];
}

-(NSString*)_documentsDir
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *publicDocumentsDir = [paths objectAtIndex:0];
    return publicDocumentsDir;
}

-(UIAlertController*)updateImageWithProgressHandler:(CMIUWriteProgressHandler)handler startedHandler:(StartCallback)started cancelledHandler:(CancelCallback)cancelled
{
    NSString *publicDocumentsDir = [self _documentsDir];
    
    NSError *error;
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:publicDocumentsDir error:&error];
    
    
    NSString *title = nil;
    if(files.count == 0)
    {
        title = @"No files found. Use iTunes to load a firmware image into the application";
    }
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Choose file" message:title preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        cancelled();
    }]];
    
    
    __weak CMIU *weakSelf = self;
    for(NSString *file in files)
    {
        [alert addAction:[UIAlertAction actionWithTitle:file style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            CMIU *strongSelf = weakSelf;
            if(strongSelf)
            {
                // Estimate a timeout based on the size of the file selected.
                NSString *publicDocumentsDir = [strongSelf _documentsDir];
                
                NSString *fullPath = [publicDocumentsDir stringByAppendingPathComponent:file];
                NSError *error = nil;
                NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:fullPath error:&error];
                
                NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
                NSLog(@"Writing %d bytes", fileSizeNumber.intValue);
                // Allow a 50 byte/second transfer rate - which is appalling, but we're only getting about 200 bytes/second inc headers at present.
                started(fileSizeNumber.intValue/50 + 10);
                // We tell the CMIU to use the path relative to the documents dir, because we've set the HTTP Server to use the documents dir as its base path
                [strongSelf updateImage:file withProgressHandler:handler];
            }
        }]];
        
    }
    
    return alert;
    
}

-(void)updateImage:(NSString*)filePath withProgressHandler:(CMIUWriteProgressHandler)handler
{
    NSData *encoded = [self buildUpdateImage:filePath];
    
    // Reset the HTTP stack
    HTTP_SERVER_INIT_PARAMS params;
    RingBufferInit(&rxHttpRingBuffer, rxHttpData, sizeof(rxHttpData));
    RingBufferInit(&txHttpRingBuffer, txHttpData, sizeof(txHttpData));
    
    params.pRbRx = &rxHttpRingBuffer;
    params.pRbTx = &txHttpRingBuffer;
    params.dataProvider = &_dataProvider;
    HttpServerInit(&server, &params);
    
    // Any requests we get the CMIU to make MUST be relative to this path
    NSString* documentsDir = [self _documentsDir];
    if ([documentsDir characterAtIndex:documentsDir.length - 1] != '/')
    {
        documentsDir = [documentsDir stringByAppendingString:@"/"];
    }
    
    bool setBasePathResult = HttpServerSetBasePath(&server, [documentsDir cStringUsingEncoding:NSASCIIStringEncoding]);
    NSAssert(setBasePathResult, @"HttpServerSetBasePath failure");
    
    _httpWriteProgressHandler = handler;
    [self startWriting:encoded handler:handler :true];
    
}


// Added DEW MSPD-1049 - this used on screen refresh. Previously no tags were sent.
// Now we need to sent the type of packet to send (here always a E_PACKET_TYPE_DETAILED_CONFIG) and its destination
// (  E_PACKET_DESTINATION_BTLE = this App, CMIU Manager)
-(NSData*)buildPublishRequestedPacket
{
    NSData *encoded = [self buildCommand:E_COMMAND_PUBLISH_REQUESTED_PACKET secureDataHandler:^(S_TPBP_PACKER *packer) {
        do
        {
            TRY(Tpbp_PackerAdd_PacketTypeTag(packer, E_PACKET_TYPE_DETAILED_CONFIG));
            TRY(Tpbp_PackerAdd_Destination(packer, E_PACKET_DESTINATION_BTLE));
        } while(NO);
    }];
    return encoded;
}


-(void)getConfiguration
{
    NSData *encoded = [self buildPublishRequestedPacket];
    [self startWriting:encoded handler:nil :true];
}



-(void)sendNull
{
    NSData *data = [self encodeString:nil  dataType:UARC_MT_NULL];
    [self startWriting:data handler:nil :true];
}

-(void)sendPing
{
    NSData *data = [self encodeString:nil dataType:UARC_MT_PING];
    [self startWriting:data handler:nil :true];
}

-(void)setResponseHandler:(CMIUResponseHandler)handler
{
    _handler = handler;
}

-(NSData*)encodeString:(NSString*)s dataType:(int)dataType
{
    NSData *data = [s dataUsingEncoding:NSUTF8StringEncoding];
    return [self encodeData:data dataType:dataType];
}


-(void)receivedMessage:(const struct UARC_MSG*)pRxMsg
{
    // Any complete message counts as a heartbeat
    [self _startHeartbeatTimer];
    
    // We expect this to be a complete, reassembled message.
    NSData *data = [NSData dataWithBytes:pRxMsg->pPayload length:pRxMsg->length];
    BOOL done = YES;
    
    switch(pRxMsg->type)
    {
        case UARC_MT_ASYNC:
        {
            
            done = [self UartAsyncHandler:pRxMsg];
            
            // According to nRFDongleUart.cs, Async messages do not require a response. So why does
            // the dev board disconnect after sending a few?
        }
            
            break;
            
        case UARC_MT_CMD:
            NSLog(@"Command");
            break;
            
        case UARC_MT_ERROR:
            NSLog(@"Error");
            break;
            
        case UARC_MT_GNIP:
            NSLog(@"Ping response");
            break;
            
        case UARC_MT_NULL:
            NSLog(@"Null");
            break;
            
        case UARC_MT_PING:
            NSLog(@"Ping");
            break;
            
        case UARC_MT_RESP:
        {
            done = [self UartCommandHandler:pRxMsg];
        }
            break;
            
        case UARC_MT_HTTP_CMD:
        {
            done = [self HttpCommandHandler:pRxMsg];
        }
            break;
            
        case UARC_MT_HEARTBEAT:
            NSLog(@"Heartbeat");
            // No callback.
            done = NO;
            break;
            
            
        default:
            NSLog(@"!!! Unexpected message type !!!");
            break;
    }

    if(_handler && done)
    {
        _handler(pRxMsg->type, data);
    }

}

-(BOOL)HttpCommandHandler:(const struct UARC_MSG*)pRXMsg
{
    //NSData *input = [NSData dataWithBytes:pRXMsg->pPayload length:pRXMsg->length];
    //NSLog(@"%@", input);
    RingBufferWrite(&rxHttpRingBuffer, pRXMsg->pPayload, pRXMsg->length);
    HttpServerProcess(&server, 0);
    
    assert(RingBufferUsed(&rxHttpRingBuffer) == 0);
    // Transmit whatever is in the tx ring buffer.
    uint8_t txBuf[sizeof(txHttpData)];
    uint32_t used = RingBufferUsed(&txHttpRingBuffer);
    RingBufferRead(&txHttpRingBuffer, txBuf, used);
    
    NSData *data = [NSData dataWithBytes:txBuf length:used];
    
    
    // How much are we writing - this is encoded as Content-Length: %d"
    
    NSData *encoded = [self encodeData:data dataType:UARC_MT_HTTP_RSP];

    [self startWriting:encoded handler:nil :true];
    
    
    uint32_t percent = HttpServerGetPercentServed(&server);
    _httpWriteProgressHandler(percent, 100);
    return percent == 100;
}

-(BOOL)UartAsyncHandler:(const struct UARC_MSG*)pRXMsg
{
    S_TPBP_PARSER parser;
    
    BOOL done = NO;
    
    // Init a parser to the received data
    Tpbp_ParserInit(&parser, pRXMsg->pPayload, pRXMsg->length);
    
    NSString *s = [[NSString alloc] initWithBytes:pRXMsg->pPayload length:pRXMsg->length encoding:NSUTF8StringEncoding];
    
    if ([s containsString:@"modem powered off"])
    {
        NSLog(@"Modem Powered off Received");
    }
    
    // Save to log
    [self.debugLog append:s];
     
    // No processing required
    done = YES;
    
    return done;
}

-(BOOL)UartCommandHandler:(const struct UARC_MSG*)pRXMsg
{
    S_TPBP_PARSER parser;
    E_PACKET_TYPE_ID packetType = E_PACKET_TYPE_COUNT; // Default to n/a
    S_TPBP_TAG              tag;
    S_CMIU_PACKET_HEADER    packetHeader;
    S_TPBP_PARSER           secureTagSequenceParser;
    uint8_t                 workingBuffer[1024];
    uint8_t                 secureDataBuffer[1024];

    // Init a parser to the received data
    Tpbp_ParserInit(&parser, pRXMsg->pPayload, pRXMsg->length);
    
    // Read the packet type ID
    BOOL done = NO;
    do
    {
    
        packetType = Tpbp_ParserRead_PacketTypeId(&parser);
        TRY(Tpbp_ParserReadTag(&parser, &tag));
        TRY(tag.tagNumber == E_TAG_NUMBER_CMIU_PACKET_HEADER);
        TRY(Tpbp_ParserRead_CmiuPacketHeader(&parser, &packetHeader));
        TRY(Tpbp_ParserReadTag(&parser, &tag));
        TRY(tag.tagNumber == E_TAG_NUMBER_SECURE_DATA);
        TRY(Tpbp_ParserGetBytes(&parser, workingBuffer, tag.dataSize));
        memcpy(secureDataBuffer, workingBuffer, tag.dataSize);
        // No encryption yet.
        
        // Store cellular Rssi and quality from header to static
        self->cellularRssiMostRecent = packetHeader.cellularRssiAndBer;
        
        Tpbp_ParserInit(&secureTagSequenceParser, secureDataBuffer, tag.dataSize);
        switch(packetType)
        {
            case E_PACKET_TYPE_DETAILED_CONFIG:
                [self HandleDetailedConfig:&secureTagSequenceParser];
                done = YES;
                break;
                
            case E_PACKET_TYPE_RESPONSE:
                done = [self HandleCommandResponse:&secureTagSequenceParser];
                break;
            default:
                assert(0);
                break;
        }
        
        // Ensure we processed everything in the secure area
        TRY(Tpbp_ParserReadTag(&secureTagSequenceParser, &tag));
        TRY(tag.tagNumber == E_TAG_NUMBER_NULL);
    } while(NO);
    if(done)
    {
        [self setCmiuClock:packetHeader.timeAndDate];
        // 64 bit number representing time in seconds, epoch somewhat uncertain but currently believed to be when the device was flashed.
        NSLog(@"CMIU time: %llu", packetHeader.
              timeAndDate);
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kCMIUUpdated object:nil];
        });
    }


    return done;
    
}


-(BOOL)HandleCommandResponse:(S_TPBP_PARSER*)secureTagSequenceParser
{
    S_TPBP_TAG              tag;
    BOOL done = NO;
    S_REPORTED_DEVICE_CONFIG currentDeviceConfig;
    NSString *testDeviceName = @"AB123456";
    NSString *deviceName = self.peripheral.name;
    char                tempString[258];
    
    do
    {
        TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
        TRY(tag.tagNumber == E_TAG_NUMBER_COMMAND);
        E_COMMAND commandId = (E_COMMAND)Tpbp_Unpack(secureTagSequenceParser, tag.dataSize);
        TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
        TRY(tag.tagNumber == E_TAG_NUMBER_ERROR_CODE);
        E_ERROR_TAG errorTag = (E_ERROR_TAG)Tpbp_Unpack(secureTagSequenceParser, tag.dataSize);
        if (E_ERROR_NONE != errorTag)
        {
            NSLog(@"Error received: %d", errorTag);
        }

        switch(commandId)
        {
            case E_COMMAND_PUBLISH_REQUESTED_PACKET:
                NSLog(@"Got detailed config packet");
                break;
                
            case E_COMMAND_UPDATE_IMAGE:
                NSLog(@"Acknowledge update image");
                break;
                
            case E_COMMAND_SET_RECORDING_AND_REPORTING_INTERVALS:
                NSLog(@"Acknowledge set recording reporting");
                done = YES;
                break;
                
            case E_COMMAND_GET_CMIU_SIGNAL_QUALITY:
                NSLog(@"Acknowledge get signal quality");
                done = NO;
                
                if ([testDeviceName isEqualToString:deviceName])// Temporary fix for simulator until it has been updated
                {
                    // We now know the modem is on
                    // Get and store cellular RSSI and sig quality from packet header
                    [self StoreCellularSignalQuality];
                    
                    // Only request new data if aquisition flag is set
                    if ( true == _signalQualityAquisitionFlag)
                    {
                        // Stop existing delay sending command and start a new one
                        [self _stopDelaySignalCommandTimer];
                        [self _startDelaySignalCommandTimer];
                        // Stop existing timer for sending next command and start new one
                        [self _stopSendSignalCommandTimer];
                        [self _startSendSignalCommandTimer];
                    }
                    
                    // Update the get signal quality timer period to active value
                    if (SEND_SIGNAL_COMMAND_INTERVAL_INITIAL == _sendSignalCommandInterval)
                    {
                        self.signalStatus = CMIU_SignalStatus_GoodData;
                        _sendSignalCommandInterval = SEND_SIGNAL_COMMAND_INTERVAL_ACTIVE;
                        // Start the timer to control how long signal quality data is acquired for
                        [self _startGetSignalTimer];
                    }
                }
                else
                {
                    TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
                    TRY(tag.tagNumber == E_TAG_NUMBER_NETWORK_PERFORMANCE);
                    TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, tempString, sizeof(tempString)));
                    
                    // We now know the modem is on
                    // Get and store cellular RSSI and sig quality from packet header
                    [self StoreCellularSignalQuality];
                    
                    // Only request new data if aquisition flag is set
                    if ( true == _signalQualityAquisitionFlag)
                    {
                        // Send next signal quality command
                        [self _sendGetSignalQualityCommand];
                        // Stop existing timer for sending next command and start new one
                        [self _stopSendSignalCommandTimer];
                        [self _startSendSignalCommandTimer];
                    }
                    
                    // Update the get signal quality timer period to active value
                    if (SEND_SIGNAL_COMMAND_INTERVAL_INITIAL == _sendSignalCommandInterval)
                    {
                        self.signalStatus = CMIU_SignalStatus_GoodData;
                        _sendSignalCommandInterval = SEND_SIGNAL_COMMAND_INTERVAL_ACTIVE;
                        // Start the timer to control how long signal quality data is acquired for
                        [self _startGetSignalTimer];
                    }
                }
                
                break;
                
            case E_COMMAND_READ_CONNECTED_DEVICES:
                NSLog(@"Acknowledge Read connected devices");
                
                TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
                TRY(tag.tagNumber == E_TAG_NUMBER_DEVICE_CONFIGURATION);
                TRY(Tpbp_ParserRead_ReportedDeviceConfig(secureTagSequenceParser, &currentDeviceConfig));
                
                // Stop the meter read timer
                [self _stopMeterReadTimer];

                // If we haven't got a reading yet
                if( _meterReadActive )
                {
                    // Check if this is an error reading or not
                    if (READ_ERROR_NO_DATA <= currentDeviceConfig.currentDeviceData && MAX_METER_READ_ATTEMPTS > self->meterReadAttemptCnt)
                    {
                        self->meterReadAttemptCnt++;

                        // No meter on the interface attempted
                        // Increment progess boxes
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[NSNotificationCenter defaultCenter] postNotificationName:kCMIUMeterRead object:nil];
                        });

                        // Restart the meter read timer
                        [self _startMeterReadTimer];
                    }
                    else
                    {
                        // Store reading from meter
                        self->meterReadId[self->meterReadCnt] = currentDeviceConfig.attachedDeviceId;
                        self->meterReadValue[self->meterReadCnt] = currentDeviceConfig.currentDeviceData;

                        // This is a real reading so display it
                        // Assign meter id and reading
                        self->meterReadCnt++;

                        _meterReadActive = false;
                        
                        // Display the meter reading
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[NSNotificationCenter defaultCenter] postNotificationName:kCMIUMeterReadComplete object:nil];
                        });
                    }
                }
                break;
                
            default:
                break;
        }

    } while(NO);
    return done;
}

-(void)StoreCellularSignalQuality
{
    if (self.cellularRssiStoreCount == MAX_RSSI_STORE_SIZE)
    {
        // Shift array back and issue warning
        for( int i = 0; i < self.cellularRssiStoreCount-1; i++ )
        {
            cellularRssiStore[i] = cellularRssiStore[i+1];
            _signalQualityTime[i] = _signalQualityTime[i+1];
        }

        cellularRssiStore[_cellularRssiStoreCount-1] = 0;
        _signalQualityTime[_cellularRssiStoreCount-1] = 0;

        _cellularRssiStoreCount--;

        NSLog(@"WARN: Signal quality data buffer exceeded");
    }

    double currentTime = CACurrentMediaTime();

    // First data point
    if( _cellularRssiStoreCount == 0 )
    {
        _lastSignalQualityTime = currentTime;
    }

    self->_signalQualityTime[self.cellularRssiStoreCount] = currentTime - _lastSignalQualityTime;
    self->cellularRssiStore[self.cellularRssiStoreCount] = self->cellularRssiMostRecent;

    NSLog(@"Storeindex: %d", self.cellularRssiStoreCount);

    // Increment index and trigger update notification
    self.cellularRssiStoreCount++;

    [self notifyRssiStoreIncrement];

    _lastSignalQualityTime = currentTime;
}

-(void)HandleDetailedConfig:(S_TPBP_PARSER*)secureTagSequenceParser
{
    S_TPBP_TAG              tag;
    S_CMIU_STATUS           status;
    S_CMIU_DIAGNOSTICS      diagnostics;
    S_REPORTED_DEVICE_CONFIG     reportedDeviceConfig;
    S_CONNECTION_TIMING_LOG connectionTimingLog;
    S_CMIU_INFORMATION cmiuInformation;
    S_CMIU_BASIC_CONFIGURATION configuration;
    S_IMAGE_VERSION_INFO firmwareVersionInfo;
    S_IMAGE_VERSION_INFO hardwareVersionInfo;
    S_IMAGE_VERSION_INFO bootloaderVersionInfo;
    S_IMAGE_VERSION_INFO configVersionInfo;
    S_IMAGE_VERSION_INFO arbVersionInfo;
    S_IMAGE_VERSION_INFO bleVersionInfo;
    S_IMAGE_VERSION_INFO encryptionVersionInfo;
    char                tempString[258];
    char                errorBuffer[256];
    char                mqttBrokerAddress[256];
    char                modemSwRevision[256];
    char                modemSerialNumber[256];
    char                SimImsi[256];
    char                SimId[256];
    S_CMIU_DIAGNOSTICS DiagnosticData;
    UInt16 batteryVoltage = 0;
    UInt8 temperature = 0;
    
    do
    {
        NSString *testDeviceName = @"AB123456";
        NSString *deviceName = self.peripheral.name;
        
        if ([testDeviceName isEqualToString:deviceName])// Temporary fix for simulator until it has been updated
        {
            //Tags expected from simulator
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 26
            TRY(tag.tagNumber == E_TAG_NUMBER_CMIU_FLAGS);
            TRY(Tpbp_ParserRead_CmiuStatus(secureTagSequenceParser, &status));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_CMIU_BASIC_CONFIGURATION);
            TRY(Tpbp_ParserRead_CmiuBasicConfiguration(secureTagSequenceParser, &configuration));
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_INTERVAL_RECORDING);
            // Need to update our state...
            TRY(Tpbp_ParserRead_IntervalRecordingConfig(secureTagSequenceParser, &intervalRecordingConfig));
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_CMIU_HW_DIAGNOSTICS);
            TRY(Tpbp_ParserRead_CmiuDiagnostics(secureTagSequenceParser, &diagnostics));
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_DEVICE_CONFIGURATION);
            TRY(Tpbp_ParserRead_ReportedDeviceConfig(secureTagSequenceParser, &reportedDeviceConfig));
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_ERROR_LOG);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, errorBuffer, sizeof(errorBuffer)));
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_CONNECTION_TIMING_LOG);
            TRY(Tpbp_ParserRead_ConnectionTimingLog(secureTagSequenceParser, &connectionTimingLog));
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_CMIU_INFORMATION);
            TRY(Tpbp_ParserRead_CmiuInformation(secureTagSequenceParser, &cmiuInformation));
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_HARDWARE_REVISION);
            TRY(Tpbp_ParserRead_ImageVersionInfo(secureTagSequenceParser, &hardwareVersionInfo));
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_FIRMWARE_REVISION);
            TRY(Tpbp_ParserRead_ImageVersionInfo(secureTagSequenceParser, &firmwareVersionInfo));
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_BOOTLOADER_REVISION);
            TRY(Tpbp_ParserRead_ImageVersionInfo(secureTagSequenceParser, &bootloaderVersionInfo));
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_CONFIG_REVISION);
            TRY(Tpbp_ParserRead_ImageVersionInfo(secureTagSequenceParser, &configVersionInfo));
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_ARB_REVISION);
            TRY(Tpbp_ParserRead_ImageVersionInfo(secureTagSequenceParser, &arbVersionInfo));
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_BLE_CONFIG_REVISION);
            TRY(Tpbp_ParserRead_ImageVersionInfo(secureTagSequenceParser, &bleVersionInfo));
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));
            TRY(tag.tagNumber == E_TAG_NUMBER_ENCRYPTION_CONFIG_REVISION);
            TRY(Tpbp_ParserRead_ImageVersionInfo(secureTagSequenceParser, &encryptionVersionInfo));
        }
        else
        {
            //Tags expected from CMIU
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 3
            TRY(tag.tagNumber == E_TAG_NUMBER_NETWORK_OPERATORS);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, tempString, sizeof(tempString)));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 4
            TRY(tag.tagNumber == E_TAG_NUMBER_NETWORK_PERFORMANCE);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, tempString, sizeof(tempString)));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 5
            TRY(tag.tagNumber == E_TAG_NUMBER_REGISTRATION_STATUS);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, tempString, sizeof(tempString)));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 6
            TRY(tag.tagNumber == E_TAG_NUMBER_HOST_ADDRESS);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, mqttBrokerAddress, sizeof(mqttBrokerAddress)));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 8
            TRY(tag.tagNumber == E_TAG_NUMBER_ASSIGNED_CMIU_ADDRESS);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, tempString, sizeof(tempString)));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 15
            TRY(tag.tagNumber == E_TAG_NUMBER_MODEM_HARDWARE_REVISION);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, tempString, sizeof(tempString)));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 16
            TRY(tag.tagNumber == E_TAG_NUMBER_MODEM_MODEL_IDENTIFICATION_CODE);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, tempString, sizeof(tempString)));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 17
            TRY(tag.tagNumber == E_TAG_NUMBER_MODEM_MANUFACTURER_ID_CODE);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, tempString, sizeof(tempString)));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 18
            TRY(tag.tagNumber == E_TAG_NUMBER_MODEM_SOFTWARE_REVISION);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, modemSwRevision, sizeof(modemSwRevision)));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 19
            TRY(tag.tagNumber == E_TAG_NUMBER_IMEI);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, modemSerialNumber, sizeof(modemSerialNumber)));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 20
            TRY(tag.tagNumber == E_TAG_NUMBER_SIM_IMSI);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, SimImsi, sizeof(SimImsi)));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 21
            TRY(tag.tagNumber == E_TAG_NUMBER_SIM_CARD_ID);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, SimId, sizeof(SimId)));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 23
            TRY(tag.tagNumber == E_TAG_NUMBER_CMIU_HW_DIAGNOSTICS);
            TRY(Tpbp_ParserRead_CmiuDiagnostics(secureTagSequenceParser, &DiagnosticData));
        
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 26
            TRY(tag.tagNumber == E_TAG_NUMBER_CMIU_FLAGS);
            TRY(Tpbp_ParserRead_CmiuStatus(secureTagSequenceParser, &status));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 27
            TRY(tag.tagNumber == E_TAG_NUMBER_DEVICE_CONFIGURATION);
            TRY(Tpbp_ParserRead_ReportedDeviceConfig(secureTagSequenceParser, &reportedDeviceConfig));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 30
            TRY(tag.tagNumber == E_TAG_NUMBER_ERROR_LOG);
            TRY(Tpbp_ParserRead_String(secureTagSequenceParser, &tag, errorBuffer, sizeof(errorBuffer)));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 31
            TRY(tag.tagNumber == E_TAG_NUMBER_CONNECTION_TIMING_LOG);
            TRY(Tpbp_ParserRead_ConnectionTimingLog(secureTagSequenceParser, &connectionTimingLog));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 34
            TRY(tag.tagNumber == E_TAG_NUMBER_INTERVAL_RECORDING);
            // Need to update our state...
            TRY(Tpbp_ParserRead_IntervalRecordingConfig(secureTagSequenceParser, &intervalRecordingConfig));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 39
            TRY(tag.tagNumber == E_TAG_NUMBER_FIRMWARE_REVISION);
            TRY(Tpbp_ParserRead_ImageVersionInfo(secureTagSequenceParser, &firmwareVersionInfo));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 40
            TRY(tag.tagNumber == E_TAG_NUMBER_BOOTLOADER_REVISION);
            TRY(Tpbp_ParserRead_ImageVersionInfo(secureTagSequenceParser, &bootloaderVersionInfo));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 41
            TRY(tag.tagNumber == E_TAG_NUMBER_CONFIG_REVISION);
            TRY(Tpbp_ParserRead_ImageVersionInfo(secureTagSequenceParser, &configVersionInfo));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 44
            TRY(tag.tagNumber == E_TAG_NUMBER_CMIU_INFORMATION);
            TRY(Tpbp_ParserRead_CmiuInformation(secureTagSequenceParser, &cmiuInformation));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 48
            TRY(tag.tagNumber == E_TAG_NUMBER_RECORDING_REPORTING_INTERVAL);
            TRY(Tpbp_ParserRead_RecordingReportingInterval(secureTagSequenceParser, &recordingReportingInterval));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 62
            TRY(tag.tagNumber == E_TAG_NUMBER_BATTERY_VOLTAGE);
            TRY(Tpbp_ParserRead_BatteryVoltage(secureTagSequenceParser, &batteryVoltage));
            
            TRY(Tpbp_ParserReadTag(secureTagSequenceParser, &tag));// Tag 63
            TRY(tag.tagNumber == E_TAG_NUMBER_CMIU_TEMPERATURE);
            TRY(Tpbp_ParserRead_CmiuTemperature(secureTagSequenceParser, &temperature));
        }
        
        // Most recent signal quality
        self.rssiMostRecent = [SignalUtility extractRssiFromSignal:self->cellularRssiMostRecent];
        self.rsrqMostRecent = [SignalUtility extractRsrqFromSignal:self->cellularRssiMostRecent];
        
        // Temperature
        self.temperature = (int8_t)temperature;
        
        // Version are coded as BCD.
        NSString *version = [NSString stringWithFormat:@"%d.%d.%06d.%d",
                             [self bcdToUint:firmwareVersionInfo.versionMajorBcd],
                             [self bcdToUint:firmwareVersionInfo.versionMinorBcd],
                             [self bcdToUint:firmwareVersionInfo.versionYearMonthDayBcd],
                             [self bcdToUint:firmwareVersionInfo.versionBuildBcd]];
        self.firmwareRevision = version;
        
        version = [NSString stringWithFormat:@"%d.%d.%06d.%d",
                             [self bcdToUint:bootloaderVersionInfo.versionMajorBcd],
                             [self bcdToUint:bootloaderVersionInfo.versionMinorBcd],
                             [self bcdToUint:bootloaderVersionInfo.versionYearMonthDayBcd],
                             [self bcdToUint:bootloaderVersionInfo.versionBuildBcd]];
        self.bootloaderRevision = version;
        
        version = [NSString stringWithFormat:@"%d.%d.%06d.%d",
                            [self bcdToUint:configVersionInfo.versionMajorBcd],
                            [self bcdToUint:configVersionInfo.versionMinorBcd],
                            [self bcdToUint:configVersionInfo.versionYearMonthDayBcd],
                            [self bcdToUint:configVersionInfo.versionBuildBcd]];
        self.configRevision = version;
        
        // Tag 6
        self.mqttBrokerAddress = [NSString stringWithFormat:@"%s", mqttBrokerAddress];
        
        // Tag 18
        self.modemSwRevision = [NSString stringWithFormat:@"%s", modemSwRevision];
        
        // Tag 19
        self.modemSerialNumber = [NSString stringWithFormat:@"%s", modemSerialNumber];
        
        // Tag 20
        self.SimImsi = [NSString stringWithFormat:@"%s", SimImsi];
        
        // Tag 21
        self.SimId = [NSString stringWithFormat:@"%s", SimId];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kCMIUUpdated object:nil];
        });
        
        self.detailConfigSuccess = true;
        
    } while(NO);
}

-(uint32_t)bcdToUint:(uint32_t)bcd
{
    uint32_t result = 0;
    uint32_t power = 1;
    while(bcd > 0)
    {
        uint32_t units = bcd & 0xf;
        result += units * power;
        power *= 10;
        bcd = bcd >> 4;
    }
    return  result;
}

-(NSData*)encodeData:(NSData*)data dataType:(int)dataType
{
    
    UARC_TX_MSG msg = {dataType, (uint32_t)data.length, data.bytes};
    uint32_t len = UartControllerMessageWriterCalculateSize(&msg);
    NSMutableData *encoded = [NSMutableData dataWithLength:len];
    UartControllerMessageWriterWriteBuffer(&mw, &msg, encoded.mutableBytes, (uint32_t)encoded.length);
    return encoded;
}

-(void)_startMeterReadTimer
{
    if (![NSThread isMainThread])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self _startMeterReadTimer];
        });
        return;
    }
    
    [_MeterReadTimeoutTimer invalidate];
    _MeterReadTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:METER_READ_INTERVAL target:self selector:@selector(_meterReadTimerExpired:) userInfo:nil repeats:NO];
}

-(void)_stopMeterReadTimer
{
    [_MeterReadTimeoutTimer invalidate];
    _MeterReadTimeoutTimer = nil;
}

-(void)_meterReadTimerExpired:(NSTimer*)timer
{
    _meterReadActive = false;

    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kCMIUMeterReadComplete object:nil];
    });
}

-(void)_startHeartbeatTimer
{
    if (![NSThread isMainThread])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self _startHeartbeatTimer];
        });
        return;
    }
    
    [_heartbeatTimeoutTimer invalidate];
    _heartbeatTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:_heartbeatTimerInterval target:self selector:@selector(_heartbeatTimerExpired:) userInfo:nil repeats:NO];
    self.heartbeatStatus = CMIU_HeartbeatStatus_Connected;
}

-(void)_stopHeartbeatTimer
{
    [_heartbeatTimeoutTimer invalidate];
    _heartbeatTimeoutTimer = nil;
    self.heartbeatStatus = CMIU_HeartbeatStatus_Disconnected;
}

-(void)_heartbeatTimerExpired:(NSTimer*)timer
{
    self.heartbeatStatus = CMIU_HeartbeatStatus_LostConnection;
}

-(void)setHeartbeatStatus:(CMIU_HeartbeatStatus_t)heartbeatStatus
{
    if (heartbeatStatus == _heartbeatStatus)
    {
        return;
    }
    
    _heartbeatStatus = heartbeatStatus;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *userinfo = @{ kCMIUHeartbeatStatusChangedNewValueKey: @(heartbeatStatus) };
        [[NSNotificationCenter defaultCenter] postNotificationName:kCMIUHeartbeatStatusChanged object:self userInfo:userinfo];
    });
}

-(void)_startGetSignalTimer
{
    if (![NSThread isMainThread])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self _startGetSignalTimer];
        });
        return;
    }
    
    [_signalTimeoutTimer invalidate];
    _signalTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:GETSIGNAL_INTERVAL target:self selector:@selector(_getSignalTimerExpired:) userInfo:nil repeats:NO];
}

-(void)_stopGetSignalTimer
{
    [_signalTimeoutTimer invalidate];
    _signalTimeoutTimer = nil;
}

-(void)_getSignalTimerExpired:(NSTimer*)timer
{
    // Disable the signal quality aquisition flag
    _signalQualityAquisitionFlag = false;
    
    self.signalStatus = CMIU_SignalStatus_ModemOff;
    //NSLog(@"Get Signal timer expired");
}

-(void)setSignalStatus:(CMIU_SignalStatus_t)signalStatus
{
    if (signalStatus == _signalStatus)
    {
        return;
    }
    
    _signalStatus = signalStatus;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *userinfo = @{ kCMIUSignalStatusChangedNewValueKey: @(signalStatus) };
        [[NSNotificationCenter defaultCenter] postNotificationName:kCMIUSignalStatusChanged object:self userInfo:userinfo];
    });
}

-(void)notifyRssiStoreIncrement
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *userinfo = @{ kCMIUSignalUpdatedNewValueKey: @(self->_cellularRssiStoreCount) };
        [[NSNotificationCenter defaultCenter] postNotificationName:kCMIUSignalUpdated object:self userInfo:userinfo];
    });
}

-(void)_startSendSignalCommandTimer
{
    if (![NSThread isMainThread])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self _startSendSignalCommandTimer];
        });
        return;
    }
    
    [_sendSignalCommandTimeoutTimer invalidate];
    _sendSignalCommandTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:_sendSignalCommandInterval target:self selector:@selector(_sendSignalCommandTimerExpired:) userInfo:nil repeats:NO];
}

-(void)_stopSendSignalCommandTimer
{
    [_sendSignalCommandTimeoutTimer invalidate];
    _sendSignalCommandTimeoutTimer = nil;
}

-(void)_sendSignalCommandTimerExpired:(NSTimer*)timer
{
    //NSLog(@"Send Signal command timer expired");
    //NSLog(@"Initiate get signal quality command");
    // Send the command
    [self _sendGetSignalQualityCommand];
    
    switch (self.signalStatus)
    {
        case CMIU_SignalStatus_ModemInit:
            [self _startSendSignalCommandTimer];
            //NSLog(@"Send Signal command timer restarted");
            break;
            
        case CMIU_SignalStatus_GoodData:
            [self _startSendSignalCommandTimer];
            //NSLog(@"Send Signal command timer restarted");
            break;
            
        case CMIU_SignalStatus_ModemOff:
            // Stop the timer
            [self _stopSendSignalCommandTimer];
            //NSLog(@"Send Signal command timer stopped");
            break;
    }
}

-(void)_startDelaySignalCommandTimer
{
    if (![NSThread isMainThread])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self _startDelaySignalCommandTimer];
        });
        return;
    }
    
    [_delaySignalCommandTimeoutTimer invalidate];
    _delaySignalCommandTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:DELAY_SIGNAL_COMMAND_INTERVAL target:self selector:@selector(_sendSignalCommandTimerExpired:) userInfo:nil repeats:NO];
}

-(void)_stopDelaySignalCommandTimer
{
    [_delaySignalCommandTimeoutTimer invalidate];
    _delaySignalCommandTimeoutTimer = nil;
}

-(void)_delaySignalCommandTimerExpired:(NSTimer*)timer
{
    // Send the command
    [self _sendGetSignalQualityCommand];
}

-(UInt16*)getCellularRssiStore
{
    return self->cellularRssiStore;
}

-(UInt64*)getMeterReadIds
{
    return self->meterReadId;
}

-(UInt32*)getMeterReadValues
{
    return self->meterReadValue;
}

-(UInt16)getMeterReadCnt
{
    return self->meterReadCnt;
}

-(UInt16)getMeterAttemptCnt
{
    return self->meterReadAttemptCnt;
}

-(CMIUMode *)getMode
{
    return [[CMIUMode alloc] initWithIntervalsRead:self.readingInterval record:self.recordingInterval report:self.reportingInterval name:nil];
}

+(NSString*)getHumanReading:(UInt32)reading
{
    NSDictionary* error = [meterReadErrorCodes objectForKey:@(reading)];

    // Check for error codes
    if( error )
    {
        return [error objectForKey:@"code"];
    }

    return [NSString stringWithFormat:@"%08d", reading];
}

-(NSString*)name
{
    NSString *s = [self localName];
    if(s)
    {
        return s;
    }
    return self.peripheral.name;
}

-(float)getSignalInterval
{
    return SEND_SIGNAL_COMMAND_INTERVAL_ACTIVE;
}

-(void)stopGetSignalQualityTimers
{
    [self _stopGetSignalTimer];
    [self _stopSendSignalCommandTimer];
}

-(void)startAquiringSignalQualityData
{
    // Stop any old timers
    [self _stopGetSignalTimer];
    [self _stopSendSignalCommandTimer];
    
    self.signalStatus = CMIU_SignalStatus_ModemInit;
    _sendSignalCommandInterval = SEND_SIGNAL_COMMAND_INTERVAL_INITIAL;
    
    // Enable the signal quality aquisition flag
    _signalQualityAquisitionFlag = true;

    // Also start the timer used for getting signal strength/quality from modem
    [self _startSendSignalCommandTimer];
    // Send the first command
    [self _sendGetSignalQualityCommand];
}

-(void)clearRssiStore
{
     // Clear the buffer
     for( int i = 0; i < _cellularRssiStoreCount; i++ )
     {
         cellularRssiStore[i] = 0;
     }

     _cellularRssiStoreCount = 0;
}

-(void)stopAquiringSignalQualityData
{
    // Disable the signal quality aquisition flag
    _signalQualityAquisitionFlag = false;
    
    [self _stopGetSignalTimer];
    [self _stopSendSignalCommandTimer];
    [self _stopDelaySignalCommandTimer];
}

-(NSArray*)logReadingTimes
{
    return @[
             @[ @"1 minute", @(1)],
             @[ @"5 minutes", @(5)],
             @[ @"15 minutes", @(15)],
             @[ @"60 minutes", @(60)]
             ];
}

-(NSArray*)sendingTimes
{
    return @[ @[@"1 hour", @(1)],
              @[@"4 hours", @(4)],
              @[@"6 hours", @(6)],
              @[@"24 hours", @(24)]];
}

-(NSArray*)readingTimes
{
    return @[ @[ @"1 minute", @(1)],
              @[ @"5 minutes", @(5)],
              @[ @"15 minutes", @(15)],
              @[ @"60 minutes", @(60)]];
}

-(double*)getSignalQualityTimeDiff
{
    return _signalQualityTime;
}

// Set heartbeat period to normal
-(void)setNormalHeartbeat
{
    // Set hearbeat period
    _heartbeatTimerInterval = HEARTBEAT_NORMAL_INTERVAL;
    [self _stopHeartbeatTimer];
    [self _startHeartbeatTimer];// With new interval
}

// Set heartbeat period to slow
-(void)setSlowHeartbeat
{
    // Set hearbeat period
    _heartbeatTimerInterval = HEARTBEAT_SLOW_INTERVAL;
    [self _stopHeartbeatTimer];
    [self _startHeartbeatTimer];// With new interval
}

-(void)dealloc
{
    [self _stopHeartbeatTimer];
    [self _stopGetSignalTimer];
    [self _stopSendSignalCommandTimer];
    [self _stopMeterReadTimer];
}

@end
