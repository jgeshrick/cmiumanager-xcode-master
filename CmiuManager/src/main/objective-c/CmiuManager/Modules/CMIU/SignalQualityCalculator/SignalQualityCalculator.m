//
//  SignalQualityCalculator.m
//  CmiuManager
//
//  Created by UKHARMAC05 on 17/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "SignalQualityCalculator.h"
#import "CMIU.h"
#import "SignalUtility.h"

const static float thresholdPoorIntermediate = -14;
const static float thresholdIntermediateExcellent = -11;

@implementation SignalQualityCalculator
{
    double squaredSum;
    CGFloat totalSum;
    
    float mean;
    float variance;
    
    float quality;
    
    UInt16 count;
}

-(id)init
{
    squaredSum = 0;
    totalSum = 0;
    count = 0;
    
    return self;
}

-(void)addRssqReading:(CGFloat)reading
{
    @synchronized (self) {
        
        // Add to accumulators
        totalSum += reading;
    
        squaredSum += reading * reading;
        
        count++;
        
        // Calculate statistics
        double totalSumSquared = totalSum * totalSum;
        double multipliedSquaredSum = squaredSum * count;
        
        variance = (multipliedSquaredSum - totalSumSquared) / (count * count);
        mean = totalSum / count;
        
        // Calculate quality metric
        quality = mean - 2 * sqrtf(variance);

//        NSLog(@"Mean %f", mean);
//        NSLog(@"Variance %f", variance);
//        NSLog(@"Quality %f", quality);
    }
    
}

-(SignalQuality)getQuality
{
    if( quality <= thresholdPoorIntermediate )
    {
        return POOR;
    }
    else if( quality < thresholdIntermediateExcellent )
    {
        return MODERATE;
    }
    else
    {
        return EXCELLENT;
    }
}

@end
