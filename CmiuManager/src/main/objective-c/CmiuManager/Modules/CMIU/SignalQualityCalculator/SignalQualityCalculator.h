//
//  SignalQualityCalculator.h
//  CmiuManager
//
//  Created by UKHARMAC05 on 17/08/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum { EXCELLENT, MODERATE, POOR } SignalQuality;

@interface SignalQualityCalculator : NSObject

-(void)addRssqReading:(CGFloat)reading;

-(SignalQuality)getQuality;

@end
