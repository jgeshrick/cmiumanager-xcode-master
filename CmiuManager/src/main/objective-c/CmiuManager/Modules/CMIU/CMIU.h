//
//  CMIU.h
//  CmiuManager
//
//  Created by Matthew Houlden on 15/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASCBPeripheral.h"

// If no adverts are received for this timeout period (in seconds), then the CMIU is assumed to have gone out of range.
#define CMIU_ADVERT_TIMEOUT 10
//
//typedef enum E_UARC_MSG_TYPE
//{
//    UARC_MT_NULL = 0x00,
//    UARC_MT_CMD = 0x01,
//    UARC_MT_PING = 0x02,
//    
//    UARC_MT_ERROR = 0x80,
//    UARC_MT_RESP = 0x81,
//    UARC_MT_GNIP = 0x82,
//    UARC_MT_ASYNC = 0x83,
//    
//}
//E_UARC_MSG_TYPE;
//
#include "UartControllerMessageTypes.h"
#import <UIKit/UIKit.h>
#include "Tpbp.h"
#include "DebugLog.h"
#include "CMIUMode.h"

#define MAX_CONNECTED_DEVICES                   20
#define MIN_RSSI                                -114.0f
#define MIN_RSRQ                                -20.0f
#define MAX_RSSI_STORE_SIZE                     400
#define MAX_METER_READ_ATTEMPTS                     5

typedef  void (^CMIUResponseHandler)(E_UARC_MSG_TYPE type, NSData *data);
typedef  void (^CMIUWriteProgressHandler)(size_t written, size_t length);
typedef  void (^StartCallback)(int timeout);
typedef  void (^CancelCallback)();

typedef enum CMIU_HeartbeatStatus_Tag
{
    CMIU_HeartbeatStatus_Connected,
    CMIU_HeartbeatStatus_LostConnection,
    CMIU_HeartbeatStatus_Disconnected
} CMIU_HeartbeatStatus_t;

typedef enum CMIU_SignalStatus_Tag
{
    CMIU_SignalStatus_ModemInit,
    CMIU_SignalStatus_GoodData,
    CMIU_SignalStatus_ModemOff
} CMIU_SignalStatus_t;

// Posted asynchronously onto the main thread
NSString * const kCMIUUpdated;
NSString * const kCMIUMeterRead;
NSString * const kCMIUMeterReadComplete;

NSString * const kCMIUSignalUpdated;
NSString * const kCMIUSignalUpdatedNewValueKey;

NSString * const kCMIUHeartbeatStatusChanged;
NSString * const kCMIUHeartbeatStatusChangedNewValueKey;

NSString * const kCMIUSignalStatusChanged;
NSString * const kCMIUSignalStatusChangedNewValueKey;

const static float GETSIGNAL_INTERVAL = 60.0;

@class CMIU;

@protocol  CMIUActivityDelegate <NSObject>
-(void)cmiuWasActive:(CMIU*)cmiu;
@end

@interface CMIU : ASCBPeripheral

-(id)initWithPeripheral:(CBPeripheral*)peripheral centralManager:(ASCBCentralManager*)centralManager;


+(void)scanForCMIUs;

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) float getSignalInterval;
//@property (nonatomic, readonly) NSDate *cmiuClock;
@property (nonatomic, readonly) NSString *iccid;
@property (nonatomic, readonly) NSDate *lastReading;
@property (nonatomic, readonly) NSUInteger frequencyBand;
@property (nonatomic, readonly) NSUInteger registering;
@property (nonatomic, readonly) NSUInteger initializing;
@property (readonly) BOOL roaming;
@property (nonatomic, readonly) NSString *carrier;
@property (nonatomic, readonly) NSString *tower;
@property (nonatomic, readonly) uint64_t cmiuClock;
@property (nonatomic, readonly) NSString *firmwareRevision;
@property (nonatomic, readonly) NSString *bootloaderRevision;
@property (nonatomic, readonly) CGFloat rssiMostRecent;
@property (nonatomic, readonly) CGFloat rsrqMostRecent;
@property (nonatomic, readonly) int8_t temperature;
@property (nonatomic, readonly) NSString *configRevision;
@property (nonatomic, readonly) NSString *mqttBrokerAddress;
@property (nonatomic, readonly) NSString *modemSwRevision;
@property (nonatomic, readonly) NSString *modemSerialNumber;
@property (nonatomic, readonly) NSString *SimImsi;
@property (nonatomic, readonly) NSString *SimId;
@property (nonatomic, weak) id<CMIUActivityDelegate> activityDelegate;
@property (nonatomic, readonly) uint8_t readingInterval;
@property (nonatomic, assign) uint8_t recordingInterval;
@property (nonatomic, assign) uint8_t reportingInterval;
@property (nonatomic, assign) uint16_t startReportingTime;
@property (nonatomic, strong) UILocalNotification *localNotification;
@property (nonatomic, assign, readonly) CMIU_HeartbeatStatus_t heartbeatStatus;
@property (nonatomic, assign, readonly) CMIU_SignalStatus_t signalStatus;
@property (atomic, assign) UInt16 cellularRssiStoreCount;
@property (nonatomic, assign) bool detailConfigSuccess;
@property (nonatomic) DebugLog* debugLog;

// messaging
-(void)getConfiguration;
-(void)sendGarbage:(size_t)length progressHandler:(CMIUWriteProgressHandler)handler;
-(UIAlertController*)updateImageWithProgressHandler:(CMIUWriteProgressHandler)handler startedHandler:(StartCallback)started cancelledHandler:(CancelCallback)cancelled;
-(void)sendNull;
-(void)sendPing;
-(void)clearWriteQueue;
-(void)updateIntervalConfigRecording:(uint8_t)recording reporting:(uint8_t)reporting start:(uint16_t)start;
-(void)sendReadConnectedDevicesCommand;

-(UInt16 *)getCellularRssiStore;
-(UInt64*)getMeterReadIds;
-(UInt32*)getMeterReadValues;
-(UInt16)getMeterReadCnt;
-(UInt16)getMeterAttemptCnt;

+(NSString*)getHumanReading:(UInt32)reading;

-(void)stopGetSignalQualityTimers;
-(void)startAquiringSignalQualityData;
-(void)stopAquiringSignalQualityData;

-(void)clearRssiStore;

-(NSArray*)logReadingTimes;

-(NSArray*)sendingTimes;

-(NSArray*)readingTimes;

-(void)setResponseHandler:(CMIUResponseHandler)handler;
-(double*)getSignalQualityTimeDiff;
-(void)setNormalHeartbeat;
-(void)setSlowHeartbeat;

-(CMIUMode*)getMode;

@end
