//
//  TestRunner.m
//  CmiuManager
//
//  Created by Ben Blaukopf on 19/06/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import "TestRunner.h"

@implementation TestRunner
{
    __weak id<TestRunnerDelegate> _delegate;
    __weak CMIU *_cmiu;
    BOOL completedTest;
}




+(NSDictionary*)tests
{
    static NSDictionary *allTests;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        allTests =  @{
                      @"Ping" : @[ ^(TestRunner *runner, CMIU *cmiu, CMIUWriteProgressHandler progressHandler, StartCallback started) { started(5); [cmiu sendPing]; },
                               ^BOOL(E_UARC_MSG_TYPE type, NSData *data) {
                                   if(type == UARC_MT_GNIP)
                                   {
                                       return YES;
                                   }
                                   NSLog(@"Unexpected response to PING");
                                   return NO;
                               }],
//                 @"10kB" : @[ ^(TestRunner *runner, CMIU *cmiu, CMIUWriteProgressHandler progressHandler, StartCallback started)
//                                    {
//                                        started(100);
//                                        __weak TestRunner *weakRunner = runner;
//                                        [cmiu sendGarbage:10240 progressHandler:^(size_t written, size_t length) {
//                                        if(progressHandler)
//                                        {
//                                            progressHandler(written, length);
//                                        }
//                                        // This particular test completes when all data is written.
//                                        if(written == -1)
//                                        {
//                                            [weakRunner completeTest:NO];
//                                        }
//                                        if(written == length)
//                                        {
//                                            [weakRunner completeTest:YES];
//                                        }
//                                    }];
//                                    },
//
//                                ^BOOL(E_UARC_MSG_TYPE type, NSData *data) {
//                                    return NO;
//                                }],
//                 @"Update Image" : @[ ^(TestRunner *runner, CMIU *cmiu, CMIUWriteProgressHandler progressHandler, StartCallback started) {
//                     UIAlertController *alert = [cmiu updateImageWithProgressHandler:progressHandler startedHandler:started cancelledHandler:^{
//                         [runner completeTest:NO];
//                     }];
//                     [runner.presenting presentViewController:alert animated:YES completion:nil];
//                 },
//                                  ^BOOL(E_UARC_MSG_TYPE type, NSData *data) {
//                                      if (type == UARC_MT_HTTP_CMD)
//                                      {
//                                          return YES;
//                                      }
//                                      if(type != UARC_MT_HEARTBEAT)
//                                      {
//                                          NSLog(@"Unexpected response to CMD");
//                                      }
//                                      return NO;
//                                  }]
//
                 };
        
    });
    return allTests;
}

+(NSArray*)testNames
{
    return [[self tests] allKeys];
}

-(id)initWithDelegate:(id<TestRunnerDelegate>)delegate cmiu:(CMIU*)cmiu
{
    self = [super init];
    _cmiu = cmiu;
    _delegate = delegate;
    return self;
    
}

-(void)completeTest:(BOOL)passed
{
    if(![NSThread isMainThread])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self completeTest:passed];
        });
        return;
    }
    if(completedTest)
    {
        return;
    }

    [_delegate completeTest:passed];
    completedTest = YES;
}

-(void)runTest:(NSString*)name withProgressHandler:(CMIUWriteProgressHandler)progressHandler
{
    NSArray *data = [[TestRunner tests] objectForKey:name];
    // Only run one test per test runner
    assert(!completedTest);
    void (^triggerTest)(TestRunner *runner, CMIU *cmiu, CMIUWriteProgressHandler progressHandler, StartCallback started) = data[0];
    BOOL (^completionTest)(E_UARC_MSG_TYPE type, NSData *data) = data[1];
    
    __weak TestRunner *weakSelf = self;
    
    [_cmiu setResponseHandler:^(E_UARC_MSG_TYPE type, NSData *data) {
        if(completionTest(type, data))
        {
            TestRunner *strongSelf = weakSelf;
            [strongSelf completeTest:YES];
        }
    }];
    
    
    
    // Trigger the test
    triggerTest(self, _cmiu, progressHandler, ^(int timeout){
        // Test has started. If no ping response received in timeout seconds, test has failed
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(timeout * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // If test is still running, it has failed.
            TestRunner *strongSelf = weakSelf;
            [strongSelf completeTest:NO];
        });
    });
}

@end
