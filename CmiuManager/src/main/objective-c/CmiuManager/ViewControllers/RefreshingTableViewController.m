//
//  RefreshingTableViewController.m
//  CmiuManager
//
//  Created by Matthew Houlden on 15/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import "RefreshingTableViewController.h"
#import "AppDelegate.h"

const static float DEFAULT_AUTO_REFRESH_INTERVAL = 30.f;

@implementation RefreshingTableViewController
{
    NSTimer *_autoRefreshTimer;
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor whiteColor];
    self.refreshControl.tintColor = [AppDelegate mainOrange];
    [self.refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    self.autoRefreshInterval = DEFAULT_AUTO_REFRESH_INTERVAL;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_applicationWilResignActive) name:UIApplicationWillResignActiveNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self _stopTimer];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
}

#pragma mark - Public methods

- (void)refreshData
{
    [self _stopTimer];
    _isRefreshing = YES;
}

- (void)receivedData:(id)responseObject
{
    [self _startTimer];
    [self.refreshControl endRefreshing];
    _isRefreshing = NO;
}

#pragma mark - Notifications

- (void)_applicationDidBecomeActive
{
    [self refreshData];
}

- (void)_applicationWilResignActive
{
    [self _stopTimer];
}

#pragma mark - Private methods

- (void)_startTimer
{
    if (!_autoRefreshTimer && self.autoRefreshInterval > 0)
    {
        [NSTimer scheduledTimerWithTimeInterval:self.autoRefreshInterval target:self selector:@selector(refreshData) userInfo:nil repeats:NO];
    }
}

- (void)_stopTimer
{
    [_autoRefreshTimer invalidate];
    _autoRefreshTimer = nil;
}

@end
