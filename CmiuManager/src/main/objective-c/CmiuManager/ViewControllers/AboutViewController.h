//
//  AboutViewController.h
//  CmiuManager
//
//  Created by Gamini Obeyesekere on 07/09/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *licenseTypeLabel;

- (IBAction)deactivateLicense:(id)sender;

@end
