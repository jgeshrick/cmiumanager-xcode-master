//
//  ConfigViewController.m
//  CmiuManager
//
//  Created by Matthew Houlden on 16/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import "ConfigViewController.h"
#import "CMIU.h"
#import "AppDelegate.h"
#import "ColorSelectButton.h"
#import "CMIUMode.h"

@interface ConfigViewController () <UITextFieldDelegate>

@end

@implementation ConfigViewController
{
    id _lastResponseObject;
    UIDatePicker *_picker;
    NSTimer *_connectionTimer;
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSAssert(self.cmiu, @"Should have a CMIU set");
    
    self.cmiuLabel.text = self.cmiu.name;
    
    self.currentModeLabel.text = [[self.cmiu getMode] lookupLabel];

    self.sendButton.highlightedBackgroundColor = [AppDelegate pressedOrange];
    [self _enableSlidersAndButtons:YES];
    [self _updateUI];
    
    _picker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    _picker.datePickerMode = UIDatePickerModeTime;
    [_picker addTarget:self action:@selector(pickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    self.pollingStartTimeLabel.inputView = _picker;
    self.pollingStartTimeLabel.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPicker:)];
    [self.view addGestureRecognizer:tap];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    _picker.date = [NSDate dateWithTimeIntervalSinceReferenceDate:_cmiu.startReportingTime * 60];
}

-(void)dismissPicker:(id)sender
{
    [self.view endEditing:YES];
}

-(void)pickerValueChanged:(UIDatePicker*)picker
{
    static NSDateFormatter *dateFormatter;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"HH:mm";
    });

    _pollingStartTimeLabel.text = [dateFormatter stringFromDate:picker.date];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    __weak ConfigViewController *weakSelf = self;
    
    [_cmiu setResponseHandler:^(E_UARC_MSG_TYPE type, NSData *data) {
        // Mark that we have received data.
        dispatch_async(dispatch_get_main_queue(), ^{
            if(type == UARC_MT_RESP)
            {
                // No parsing of response!
                [weakSelf receivedData];
            }
        });
    }];

}


#pragma mark - Events

- (IBAction)basicButtonPressed:(id)sender {
    [self _startSendingConfigData: [CMIUMode getModeFromLabel:@"Basic"]];
}

- (IBAction)advancedButtonPressed:(id)sender {
    [self _startSendingConfigData: [CMIUMode getModeFromLabel:@"Advanced"]];
}

- (IBAction)proButtonPressed:(id)sender {
    [self _startSendingConfigData: [CMIUMode getModeFromLabel:@"Pro"]];
}


#pragma mark - Send data helpers

- (void)_startSendingConfigData:(CMIUMode*)mode
{
    NSAssert(mode, @"Selected mode undefined");

    [self _enableSlidersAndButtons:NO];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sending data\n\n"
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activityIndicator.color = [AppDelegate mainOrange];
    activityIndicator.center = CGPointMake(130.5, 65.5); // MH TODO - magic numbers!!!
    [activityIndicator startAnimating];
    [alertController.view addSubview:activityIndicator];
    [self presentViewController:alertController animated:YES completion:nil];
    
    NSString *srt = _pollingStartTimeLabel.text;
    NSString *hrs = [srt substringToIndex:2];
    NSString *mins = [srt substringFromIndex:3];
    
    uint16_t start = [mins intValue] + [hrs intValue] * 60;
    [_cmiu updateIntervalConfigRecording:mode.recordInterval reporting:mode.reportInterval start:start];

    // Put a timer on this
    _connectionTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(connectionTimerExpired:) userInfo:nil repeats:NO];
    
}

-(void)connectionTimerExpired:(NSTimer*)timer
{
    _connectionTimer = nil;
    [self _failedSendingConfigData];
}

- (void)receivedData
{
    [_connectionTimer invalidate];
    _connectionTimer = nil;

    if ([self.presentedViewController isKindOfClass:[UIAlertController class]])
    {
        __weak ConfigViewController *weakSelf = self;
        [self.presentedViewController dismissViewControllerAnimated:NO
                                                         completion:^{
                                                             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Config data successfully sent"
                                                                                                                                      message:@"CMIU is now resetting\nPlease reconnect when ready"
                                                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                                             [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                                                                                 style:UIAlertActionStyleDefault
                                                                                                               handler:^(UIAlertAction *action){
                                                                                                                   [weakSelf _enableSlidersAndButtons:YES];
                                                                                   [self.cmiu cancelConnection];
                                                                                   [self.navigationController popToRootViewControllerAnimated:YES];
                                                                                                               }]];
                                                             [weakSelf presentViewController:alertController animated:YES completion:nil];
                                                         }];
    }
}

- (void)_failedSendingConfigData
{
    if ([self.presentedViewController isKindOfClass:[UIAlertController class]])
    {
        __weak ConfigViewController *weakSelf = self;
        [self.presentedViewController dismissViewControllerAnimated:NO
                                                         completion:^{
                                                             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Data failed to send"
                                                                                                                                      message:nil
                                                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                                             [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                                                                                 style:UIAlertActionStyleCancel
                                                                                                               handler:^(UIAlertAction *action){
                                                                                                                   [weakSelf _enableSlidersAndButtons:YES];
                                                                                                               }]];
                                                             [weakSelf presentViewController:alertController animated:YES completion:nil];
                                                         }];
    }
}

#pragma mark - UI helpers

- (void)_updateUI
{
    
    uint16_t srt = _cmiu.startReportingTime;
    uint32_t srtmins = srt % 60;
    uint32_t srthours = (srt/60) % 24;
    self.pollingStartTimeLabel.text = [NSString stringWithFormat:@"%02d:%02d", srthours, srtmins];
}

- (void)_enableSlidersAndButtons:(BOOL)enable
{
    _basicButton.enabled = enable;
    _advancedButton.enabled = enable;
    _proButton.enabled = enable;
}

#pragma mark - Other helpers

@end
