//
//  RefreshingTableViewController.h
//  CmiuManager
//
//  Created by Matthew Houlden on 15/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RefreshingTableViewController : UITableViewController

- (void)refreshData;

- (void)receivedData:(id)responseObject;

@property (readonly) BOOL isRefreshing;

// A default value is used if not set explicitly. If set to a value less than or equal to zero, it will not autorefresh.
@property (nonatomic) float autoRefreshInterval;

@end
