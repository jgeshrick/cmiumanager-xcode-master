//
//  LocateViewController.h
//  CmiuManager
//
//  Created by Matthew Houlden on 16/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrafficLightView.h"


@class CMIU;
@interface LocateViewController : UIViewController

@property (nonatomic) CMIU *cmiu;

@property (strong, nonatomic) IBOutlet UILabel *cmiuLabel;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet TrafficLightView *trafficLightView;

@end
