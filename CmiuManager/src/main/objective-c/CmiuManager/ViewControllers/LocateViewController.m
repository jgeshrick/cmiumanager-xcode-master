//
//  LocateViewController.m
//  CmiuManager
//
//  Created by Matthew Houlden on 16/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import "LocateViewController.h"
#import "CMIU.h"
#import "ASGraphView.h"
#import "ASGraphNumberedAxis.h"
#import "AirsourceLog.h"
#import "SignalQualityCalculator.h"
#import "SignalUtility.h"
#import <MessageUI/MessageUI.h>

const static float GRAPH_HEIGHT = 170.f;

const static UInt16 INITIAL_SIGNAL = 0x6363;

@interface GraphStyle : ASGraphStyle
@end

@interface LocateViewController() <MFMailComposeViewControllerDelegate>
@end

@implementation GraphStyle
{
    UIImage *_backgroundImage;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        _backgroundColor =[UIColor whiteColor];//  [UIColor colorWithRed:(6.0f/255.0f)   green:(69.0f/255.0f)  blue:(107.0f/255.0f) alpha:1.0f];
        _labelBorderColor = [UIColor clearColor];
        _highlightColor = [UIColor blueColor];// [UIColor colorWithRed:(2.0f/255.0f) green:(74.0f/255.0f) blue:(120.0f/255.0f) alpha:0.7f];
        _labelColor = [UIColor blackColor];
        _labelBackgroundColor = [UIColor clearColor];
        _gridLineColor = [UIColor redColor];
        _axisColor = [UIColor redColor];
        
        _pathColors[0] = [UIColor blackColor];
        _pathStyle = ASGraphPathStyleSmooth;
        _labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0f];
        
        self.shouldOffsetLabels = true;
        self.pathShouldMask = false;
    }
    return self;
}

@end

@implementation LocateViewController
{
    
    ASGraphView *_graphViewRSRQ;

    double rssiCurrentPlotTime;
    UInt16 rssiCurrentCount;

    UILabel *_yaxisRSRQValue;
    NSUInteger layerIndex;
    NSUInteger layerIndexRSRQ;
    
    int maximumRsrqY;
    int minimumRsrqY;
    
    id<NSObject> _signalQualityObserver;
    id<NSObject> _signalUpdateObserver;
    
    UIActivityIndicatorView *_spinner;
    UInt16 *rssiStoredData;
    double *rssiDataTimeSteps;
    NSLock *arrayLock;
    UIAlertController *_waitingOnModemAlert;
    bool _isActive;
    bool _isVisible;
    bool _dataAcquisitionComplete;

    SignalQualityCalculator* qualityCalculator;
}

static GraphStyle *_style;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Set heartbeat slow
    [_cmiu setSlowHeartbeat];
    
    _isActive = false;
    _dataAcquisitionComplete = false;
    
    NSAssert(self.cmiu, @"Should have a CMIU set");
    
    [self setNormalRightBarButtonItems];
    
    self.cmiuLabel.text = self.cmiu.name;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _style = [GraphStyle new];
    });
    
    _yaxisRSRQValue = [UILabel new];

    // Setup up the plots
    rssiStoredData = [_cmiu getCellularRssiStore];
    rssiDataTimeSteps = [_cmiu getSignalQualityTimeDiff];

    minimumRsrqY = -10 - 15;
    maximumRsrqY = -10 + 15;

    rssiCurrentPlotTime = 0;
    rssiCurrentCount = 0;

    // Clear CMIU RSSI cache
    [_cmiu clearRssiStore];

    // Set up graph with initial data point
    [self setupGraph:INITIAL_SIGNAL :0];

    qualityCalculator = [SignalQualityCalculator new];

    // This notification is made on the main thread.
    _signalQualityObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kCMIUSignalStatusChanged object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self _signalQualityStatusDidChange:[(NSNumber*)note.userInfo[kCMIUSignalStatusChangedNewValueKey] intValue]];
    }];

    _signalUpdateObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kCMIUSignalUpdated object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self _signalDidChange:[(NSNumber*)note.userInfo[kCMIUSignalUpdatedNewValueKey] intValue]];
    }];
}

-(void)clampRsrqMinMax
{
    maximumRsrqY += 5 - abs(maximumRsrqY % 5);
    minimumRsrqY -= abs(minimumRsrqY % 5);
}

-(void)viewDidAppear:(BOOL)animated
{
    _isVisible = true;

    // Only start data aquistion if signal quality email was not active
    if (false == _isActive)
    {
        _isActive = true;

        [_cmiu addObserver:self forKeyPath:@"rssi" options:NSKeyValueObservingOptionNew context:nil];
        
        // Start aquiring Signal Quality data
        [_cmiu startAquiringSignalQualityData];
        
        [self showSpinner];
        
        // Show alert box to indicate waiting for modem to initialise
        _waitingOnModemAlert = [UIAlertController alertControllerWithTitle:nil message:@"Waiting for modem to power on" preferredStyle:UIAlertControllerStyleActionSheet];
        
        [_waitingOnModemAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [self.navigationController.topViewController presentViewController:_waitingOnModemAlert animated:YES completion:nil];
    }

    // Check if data acquisition is complete
    if (true == _dataAcquisitionComplete)
    {
        [self acquisitionCompleteMessage];

        // Reset the flag
        _dataAcquisitionComplete = false;
    }
}

-(void)viewDidDisappear:(BOOL)animated
{

}

-(void)dealloc
{
    // Set heartbeat normal
    [_cmiu setNormalHeartbeat];

    [_cmiu stopAquiringSignalQualityData];
    [_cmiu removeObserver:self forKeyPath:@"rssi"];
    [[NSNotificationCenter defaultCenter] removeObserver:_signalQualityObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:_signalUpdateObserver];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"rssi"])
    {
        [_cmiu refreshRSSI];
    }
}

-(void)plotDataPoint:(UInt16)signalQuality :(double)time
{
    CGPoint rsrqPt;

    //Assign new RSRQ point
    rsrqPt.x = (float)time;
    rsrqPt.y = [SignalUtility extractRsrqFromSignal:signalQuality];
    //NSLog(@"RSRQ: (%f)\n", rsrqPt.y);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if(!_graphViewRSRQ)
        {
            return;
        }
        
        if(maximumRsrqY < rsrqPt.y || minimumRsrqY > (rsrqPt.y - 5))
        {
            maximumRsrqY = MAX(maximumRsrqY, rsrqPt.y);
            minimumRsrqY = MIN(minimumRsrqY, rsrqPt.y-5);
            [self clampRsrqMinMax];
            ASGraphAxis * yAxisRSRQ = [[ASGraphNumberedAxis alloc] initWithPrecision:0
                                                                            rectEdge:CGRectMinXEdge
                                                                                from:minimumRsrqY to:maximumRsrqY
                                                                          ticksEvery:10
                                                                              offset:0];
            [_graphViewRSRQ replaceYAxis:yAxisRSRQ forLayer:layerIndexRSRQ];
        }
        
        [_graphViewRSRQ pushPopPoint:rsrqPt toLayer:0 toPlotIndex:0 pop:NO];
        _graphViewRSRQ.xAxis.minimum = rsrqPt.x - 60;
        _graphViewRSRQ.xAxis.maximum = rsrqPt.x;

        _yaxisRSRQValue.text = [NSString stringWithFormat:@"%.1f dBm", rsrqPt.y];
    });
    
}

-(void)setupGraph:(UInt16)signalQuality :(double)time
{
    float maximumX = 60;
    
    // Create the graph (with a placeholder width)
    NSUInteger width = 250;

    /* RSRQ graph setup */
    // Remove the old one if necessary
    if (_graphViewRSRQ)
    {
        [_graphViewRSRQ removeFromSuperview];
    }
    
    // Create the graph (with a placeholder width)
    // Area of the graph, including axes
    _graphViewRSRQ = [[ASGraphView alloc] initWithFrame:CGRectMake(20, 40 + kOverflowMargin,
                                                                   width,
                                                                   GRAPH_HEIGHT - kOverflowMargin)];
    
    [ASGraphStyle updateStyle:_style];
    
    _graphViewRSRQ.xAxis = [[ASGraphNumberedAxis alloc] initWithPrecision:0
                                                                 rectEdge:CGRectMinYEdge
                                                                     from:0 to:maximumX
                                                               ticksEvery:20
                                                                   offset:60];
    
    ASGraphAxis * yAxisRSRQ = [[ASGraphNumberedAxis alloc] initWithPrecision:0
                                                                    rectEdge:CGRectMinXEdge
                                                                        from:minimumRsrqY to:maximumRsrqY
                                                                  ticksEvery:10
                                                                      offset:0];
    // Add a layer with this Y axis
    layerIndexRSRQ = [_graphViewRSRQ addLayerWithAxis:yAxisRSRQ];
    
    CGPoint first;
    
    [_graphViewRSRQ replaceYAxis:yAxisRSRQ forLayer:layerIndexRSRQ];
    first.x = (float)time;
    first.y = [SignalUtility extractRsrqFromSignal:signalQuality];
    [_graphViewRSRQ addPlot:@[[NSValue valueWithCGPoint:first]] toLayer:0];
    
    // Add the graph to the scroll view
    [self.scrollView addSubview:_graphViewRSRQ];
    //self.scrollView.contentSize = CGSizeMake(width, _graphViewRSRQ.frame.size.height);
    self.scrollView.contentOffset = CGPointZero;
    
    UILabel *yaxisLabelRSRQ = [UILabel new];
    yaxisLabelRSRQ.text = @"LTE RSRQ (dBm)";
    yaxisLabelRSRQ.textColor=[UIColor blackColor];
    yaxisLabelRSRQ.transform = CGAffineTransformMakeRotation(-M_PI/2);
    yaxisLabelRSRQ.bounds = CGRectMake(0, 0, 200, 25);
    yaxisLabelRSRQ.center = CGPointMake(10,85);
    [_scrollView addSubview:yaxisLabelRSRQ];
    
    _yaxisRSRQValue.text = @"--- dBm";
    _yaxisRSRQValue.textColor=[UIColor blackColor];
    _yaxisRSRQValue.frame = CGRectMake(170, 30, 100, 25);
    _yaxisRSRQValue.textAlignment = NSTextAlignmentRight;
    [_scrollView addSubview:_yaxisRSRQValue];
    
    //Plot any points that already exist in the rssi Data Store
    //TODO: [self->plotDataPoint];
    
}

-(void)showSpinner
{
    // Show a spinner
    if(!_spinner)
    {
        _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _spinner.color = [UIColor blueColor];
        _spinner.frame = self.view.bounds;
        [_spinner startAnimating];
    }
    else
    {
        [self.view bringSubviewToFront:_spinner];
    }
    [self.view addSubview:_spinner];
}

-(void)hideSpinner
{
    [_spinner stopAnimating];
    [_spinner removeFromSuperview];
    _spinner = nil;
}

-(void)refreshTrafficLight
{
    TrafficLightViewColour colour;
    
    // Get quality
    switch( [qualityCalculator getQuality] )
    {
        case POOR:
            colour = RED;
            break;
            
        case  MODERATE:
            colour = AMBER;
            break;
            
        case EXCELLENT:
            colour = GREEN;
            break;
            
        default:
            NSLog(@"Unrecognised quality value received");
            colour = RED;
    }
    
    [_trafficLightView setColour:colour];
    
}

-(void)_signalDidChange:(int)count
{
    // Make sure we have not missed a point
    NSAssert(rssiCurrentCount + 1 >= count, @"Missed graph point notification");

    if( rssiCurrentCount < count )
    {
        rssiCurrentCount = count;

        rssiCurrentPlotTime += rssiDataTimeSteps[count-1];

        [self plotDataPoint:rssiStoredData[count-1] :rssiCurrentPlotTime];

        [qualityCalculator addRssqReading:[SignalUtility extractRsrqFromSignal:rssiStoredData[count-1]]];

        dispatch_async(dispatch_get_main_queue(), ^{
            [self refreshTrafficLight];
        });
    }
}

-(void)_signalQualityStatusDidChange:(CMIU_SignalStatus_t)newValue
{
    switch (newValue)
    {
        case CMIU_SignalStatus_ModemInit:
            [self showSpinner];
            break;
            
        case CMIU_SignalStatus_GoodData:
            NSLog(@"Good Signal Data");
            [self hideSpinner];
            [_waitingOnModemAlert dismissViewControllerAnimated:YES completion: nil];
            break;
            
        case CMIU_SignalStatus_ModemOff:
            [self hideSpinner];

            if( _isVisible == true)
            {
                [self acquisitionCompleteMessage];
            }
            else
            {
                // Set flag for data acquisition complete
                _dataAcquisitionComplete = true;
            }
            break;
    }
}

-(void)acquisitionCompleteMessage
{
    // Show alert box to indicate acquisition of signal quality data is complete
    UIAlertController *_signalAquisitionAlert = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"Acquisition of %0.0fs of cellular signal quality complete", GETSIGNAL_INTERVAL] preferredStyle:UIAlertControllerStyleActionSheet];
    
    [_signalAquisitionAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
    [self.navigationController.topViewController presentViewController:_signalAquisitionAlert animated:YES completion:nil];
}

-(void)setNormalRightBarButtonItems
{
    // Navigation bar buttons
    UIBarButtonItem *emailButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-navbar-email"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(_emailSignalButtonPressed)];
    
    self.navigationItem.rightBarButtonItems = @[ emailButton ];
}

- (void)_emailSignalButtonPressed
{
    UInt16 *signalQualityData = [_cmiu getCellularRssiStore];
    CGFloat relativeTime = 0.0;
    CGFloat rssi;
    CGFloat rsrq;
    NSString *subject = [NSString stringWithFormat:@"Cellular signal quality for %@", self.cmiu.name];
    NSString *message = [NSString stringWithFormat:@""];
    int ii;
    double *timeDiff = [_cmiu getSignalQualityTimeDiff];

    // Can't email the data if we don't yet have it
    if (0 == _cmiu.cellularRssiStoreCount)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Show alert box to indicate aquisition of signal quality data is not yet done complete
            UIAlertController *_emailAlert = [UIAlertController alertControllerWithTitle:nil message: @"No signal quality data acquired yet" preferredStyle:UIAlertControllerStyleActionSheet];
            
            [_emailAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
            [self.navigationController.topViewController presentViewController:_emailAlert animated:YES completion:nil];
        });
        return;
    }
    
    message = [message stringByAppendingFormat:
               @"Cellular signal quality data (Time(s), RSSI, RSRQ)\n\n"];
    
    for (ii = 0; ii < _cmiu.cellularRssiStoreCount; ii++)
    {
        if (0 == ii)
        {
            relativeTime = 0.0;
        }
        else
        {
            relativeTime += (float)timeDiff[ii];
        }
        
        rssi = [SignalUtility extractRssiFromSignal:signalQualityData[ii]];
        rsrq = [SignalUtility extractRsrqFromSignal:signalQualityData[ii]];

        NSLog(@"%0.1f, %0.1f, %0.1f\n", relativeTime, rssi, rsrq);
        message = [message stringByAppendingFormat:
                   @"%0.1f, %0.1f, %0.1f\n", relativeTime, rssi, rsrq];
    }
    
   [self _sendEmailWithSubject:subject message:message];
}

- (void)_sendEmailWithSubject:(NSString *)subject message:(NSString *)message
{
    if (![MFMailComposeViewController canSendMail])
    {
        // MH TODO
        return;
    }
    
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
    [controller setSubject:subject];
    [controller setMessageBody:message isHTML:NO];
    controller.navigationBar.barStyle = UIBarStyleDefault;
    controller.mailComposeDelegate = self;
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    // Dismiss the controller
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
