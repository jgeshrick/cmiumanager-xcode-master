//
//  AboutViewController.m
//  CmiuManager
//
//  Created by Gamini Obeyesekere on 07/09/2016.
//  Copyright © 2016 Sagentia. All rights reserved.
//

#import "AboutViewController.h"
#import "LicenseManager.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Set software version
    NSString* versionString = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];

    NSAssert([versionString isKindOfClass:[NSString class]], @"Version string missing or corrupted");

    // Display
    self.versionLabel.text = (NSString*)versionString;

    // Display license type
    self.licenseTypeLabel.text = [LicenseManager getString:[[LicenseManager getLicenseManager] getLicenseMode]];

}

- (IBAction)deactivateLicense:(id)sender {

    // Deactivate license
    if( [[LicenseManager getLicenseManager] deactivateLicense] )
    {
        // Segue back to main screen (and recheck license)
        [self performSegueWithIdentifier:@"licenseDeactivated" sender:nil];
    }
    else
    {
        @throw [NSException exceptionWithName:@"License deactivate" reason:@"Failed to deactivate license" userInfo:nil];
    }

}

@end
