//
//  PeripheralTableViewCell.m
//  CmiuManager
//
//  Created by Ben Blaukopf on 11/06/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import "PeripheralTableViewCell.h"
#import "CMIU.h"

@implementation PeripheralTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setCmiu:(CMIU *)cmiu
{
    _cmiu = cmiu;
    
    self.textLabel.text = cmiu.name;
}

@end
