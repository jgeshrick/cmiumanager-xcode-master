//
//  CMIUConnectViewController.h
//  CmiuManager
//
//  Created by Matthew Houlden on 15/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CMIU, ColorSelectButton;

@protocol CMIUConnectViewControllerDelegate <NSObject>

- (void)cmiuConnectViewControllerDidCancel;
- (void)cmiuConnectViewControllerDidConnectWithCmiu:(CMIU *)cmiu;

@end

@interface CMIUConnectViewController : UIViewController

@property (nonatomic) CMIU *cmiu;

@property (weak, nonatomic) id<CMIUConnectViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UILabel *cmiuLabel;

@property (strong, nonatomic) IBOutlet ColorSelectButton *connectButton;
- (IBAction)connectButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *DebugMessage;

@end
