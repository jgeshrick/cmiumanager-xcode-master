//
//  CMIUStatusTableViewController.m
//  CmiuManager
//
//  Created by Matthew Houlden on 15/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <MessageUI/MessageUI.h>

#import "CMIUStatusTableViewController.h"
#import "CMIU.h"
#import "CMIUMode.h"
#import "LocateViewController.h"
#import "ConfigViewController.h"
#import "StatusButton.h"
#import "AppDelegate.h"
#import "ProgressDialogViewController.h"
#import "TestRunner.h"
#import "DebugLog.h"
#import "LicenseManager.h"
#import "SignalUtility.h"

@interface CMIUStatusTableViewController() <MFMailComposeViewControllerDelegate, TestRunnerDelegate, CMIUActivityDelegate>

@end

@implementation CMIUStatusTableViewController
{
    NSArray*_statusData;
    NSDate *_lastUpdated;
    UIActivityIndicatorView *_spinner;
    UIProgressView *_progress;
    TestRunner *_testRunner;
    id<NSObject> _observer;
    id<NSObject> _meterReadObserver;
    id<NSObject> _meterReadCompleteObserver;
    id<NSObject> _heartbeatObserver;
    id<NSObject> _stateChangeObserver;
    UIView *_grayMask;
    UIAlertController *_heartbeatAlert;
    UIAlertController *_meterReadAlert;
    ProgressDialogViewController *_meterReadProgress;
    UIAlertController *_meterReadResultsAlert;
    NSString* meterReadTimeDate;
}

#pragma mark - Lifecycle
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
    [[NSNotificationCenter defaultCenter] removeObserver:_meterReadObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:_meterReadCompleteObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:_heartbeatObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:_stateChangeObserver];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSAssert(self.cmiu, @"Should have a CMIU set");
    __weak CMIUStatusTableViewController *weakSelf = self;

    
    
    _stateChangeObserver = [[NSNotificationCenter defaultCenter] addObserverForName:ASCBPeripheralStateDidChangeNotification
                                                                            object:nil
                                                                             queue:nil
                                                                        usingBlock:^(NSNotification *note) {
                                                                            CMIUStatusTableViewController *strongSelf = weakSelf;
                                                                            if (strongSelf.cmiu.state == ASCBPeripheralState_Disconnected)
                                                                            {
                                                                                [strongSelf goBack];
                                                                            }
                                                                        }];
    
    
    [self setNormalRightBarButtonItems];

    self.cmiuLabel.text = self.cmiu.name;

    // Grey and disable following buttons if detailed config response not received
    //if (false == _cmiu.detailConfigSuccess)
    //{
    //    [self setButtonsActive:NO];
    //}

    self.cellularButton.unhighlightedBackgroundColor = [UIColor lightGrayColor];
    self.meterButton.unhighlightedBackgroundColor = [UIColor lightGrayColor];
    self.configButton.unhighlightedBackgroundColor = [UIColor lightGrayColor];

    self.cellularButton.highlightedBackgroundColor = [UIColor orangeColor];
    self.meterButton.highlightedBackgroundColor = [UIColor orangeColor];
    self.configButton.highlightedBackgroundColor = [UIColor orangeColor];

    self.cellularButton.enabled = NO;
    self.meterButton.enabled = NO;
    self.configButton.enabled = NO;

    self.configButton.image = [UIImage imageNamed:@"icon-config"];
    self.cellularButton.image = [UIImage imageNamed:@"icon-cellular"];
    self.meterButton.image = [UIImage imageNamed:@"icon-meter"];
    
    // MH TODO - why would we want autorefreshing when this view is meant to time out?
    // Temporarily prevented auto refreshing until we decide what we are doing here...
    self.autoRefreshInterval = 0;

    // Both these notifications are made on the main thread.
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:kCMIUUpdated object:nil queue:nil usingBlock:^(NSNotification *note) {
        CMIUStatusTableViewController* strongSelf = weakSelf;
        [strongSelf _updateTableData];
    }];
    _meterReadObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kCMIUMeterRead object:nil queue:nil usingBlock:^(NSNotification *note) {
        CMIUStatusTableViewController* strongSelf = weakSelf;
        [strongSelf _updateMeterRead];
    }];
    _meterReadCompleteObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kCMIUMeterReadComplete object:nil queue:nil usingBlock:^(NSNotification *note) {
        CMIUStatusTableViewController* strongSelf = weakSelf;
        [strongSelf _updateMeterCompleteRead];
    }];
    _heartbeatObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kCMIUHeartbeatStatusChanged object:nil queue:nil usingBlock:^(NSNotification *note) {
        CMIUStatusTableViewController* strongSelf = weakSelf;
        [strongSelf _heartbeatStatusDidChange:[(NSNumber*)note.userInfo[kCMIUHeartbeatStatusChangedNewValueKey] intValue]];
    }];
    
    [self refreshData];
}

-(void)activateDetailedButtons
{
    // Detailed config packet received - Activate buttons
    self.cellularButton.unhighlightedBackgroundColor = [UIColor orangeColor];
    self.meterButton.unhighlightedBackgroundColor = [UIColor orangeColor];

    self.cellularButton.enabled = YES;
    self.meterButton.enabled = YES;

    if( [[LicenseManager getLicenseManager] getLicenseMode] == LICENSE_SUPERUSER )
    {
        self.configButton.unhighlightedBackgroundColor = [UIColor orangeColor];
        self.configButton.enabled = YES;
    }
}

-(void)setNormalRightBarButtonItems
{
    // Navigation bar buttons
    UIBarButtonItem *emailButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-navbar-email"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(_emailButtonPressed)];
    
    UIBarButtonItem *testButton =  [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-navbar-test"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(_testButtonPressed)];
    
    self.navigationItem.rightBarButtonItems = @[ emailButton, testButton];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupResponseHandler];

    _cmiu.activityDelegate = self;

    // Refresh table data
    [self refreshData];

   // [self.cmiu sendPing];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    _cmiu.activityDelegate = nil;

    if(self.isMovingFromParentViewController)
    {
        [self cleanup];
    }
}

-(void)cmiuWasActive:(CMIU *)cmiu
{
}

#pragma mark - Events

-(void)runTest:(NSString*)name
{
    [self startTest];
    _testRunner = [[TestRunner alloc] initWithDelegate:self cmiu:self.cmiu];
    _testRunner.presenting = self;
    [_testRunner runTest:name withProgressHandler:^(size_t written, size_t length) {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         _progress.progress = ((float)written)/length;
                     });
    }];
    
}

-(void)showSpinner
{
    // Show a spinner
    if(!_spinner)
    {
        _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _spinner.color = [UIColor blueColor];
        _spinner.frame = self.view.bounds;
        [_spinner startAnimating];
    }
    else
    {
        [self.view bringSubviewToFront:_spinner];
    }
    [self.view addSubview:_spinner];
}

-(void)hideSpinner
{
    [_spinner stopAnimating];
    [_spinner removeFromSuperview];
    _spinner = nil;
}

// TestRunnerDelegate
-(void)startTest
{
    [self showSpinner];
    
    // Disable the UI.
    self.view.userInteractionEnabled = NO;

    UIProgressView *progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    CGRect f = progressView.frame;
    f.size.width = self.view.frame.size.width;
    progressView.frame = f;
    progressView.tintColor = [UIColor blueColor];
    [self.view addSubview:progressView];
    _progress = progressView;
    
    
    // Change the right button item to a cancel button
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTest)];

    self.navigationItem.rightBarButtonItems = @[cancelButton];
}

-(void)completeTest:(BOOL)passed
{
    _testRunner = nil;
    [_cmiu clearWriteQueue];
    if(![NSThread isMainThread])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self completeTest:passed];
        });
        return;
    }
    self.view.userInteractionEnabled = YES;

    if(!_spinner)
    {
        return;
    }
    [self hideSpinner];
    [self setupResponseHandler];

    
    NSString *msg = passed ? @"Passed" : @"Failed";
    [_progress removeFromSuperview];
    _progress = nil;
    [self setNormalRightBarButtonItems];
    
    if(_heartbeatAlert)
    {
        UIViewController *presenting = _heartbeatAlert.presentingViewController;
        _heartbeatAlert = nil;
        [presenting dismissViewControllerAnimated:YES completion:^{
            [self showCompletion:msg];
        }];
    }
    else
    {
        [self showCompletion:msg];
    }
    
}

-(void)showCompletion:(NSString*)msg
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ran test" message:msg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)setupResponseHandler
{
    __weak CMIUStatusTableViewController *weakSelf = self;

    [_cmiu setResponseHandler:^(E_UARC_MSG_TYPE type, NSData *data) {
        // Mark that we have received data.
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf receivedData:nil];
        });
    }];
}

-(void)cancelTest
{
    _testRunner = nil;
    [self completeTest:NO];
}

- (void)_testButtonPressed
{
    __weak CMIUStatusTableViewController *weakSelf = self;

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Run test" message:@"Select test" preferredStyle:UIAlertControllerStyleActionSheet];
    
    for(NSString *name in [TestRunner testNames])
    {
        [alert addAction:[UIAlertAction actionWithTitle:name style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [weakSelf runTest:name];
        }]];
        
    }
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Issue Report" style:UIAlertActionStyleDefault handler:^(UIAlertAction *alert) {
        [weakSelf _issueReportButtonPressed];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
     [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)_emailButtonPressed
{
    NSArray* toRecipients = [NSArray arrayWithObjects:@"hhsupp@neptunetg.com", nil];
    UInt16 *signalQualityData = [_cmiu getCellularRssiStore];
    UInt16 maxIndex = 0;
    CGFloat relativeTime = 0.0;
    CGFloat rssi;
    CGFloat rsrq;
    int ii;
    double *timeDiff;
    UInt16 readingsCnt = [_cmiu getMeterReadCnt];
    UInt64 *meterReadIds = [_cmiu getMeterReadIds];
    UInt32 *meterReadValues = [_cmiu getMeterReadValues];
    
    // Can't email the data if we don't yet have it
    if (_statusData.count == 0)
    {
        return;
    }
    
    // Status Data
    NSString *subject = [NSString stringWithFormat:@"Status for %@", self.cmiu.name];
    NSMutableString *message = [NSMutableString new];
    [message appendString:[NSString stringWithFormat:@"Last updated: %@\n\n", [self _timeDayDateMonthYearStringFromDate:_lastUpdated]]];
    for (NSDictionary *dictionary in _statusData)
    {
        [message appendString:[NSString stringWithFormat:@"%@ - %@\n\n", dictionary[@"field"], dictionary[@"value"]]];
    }
    
    // Section separator
    [message appendString:[NSString stringWithFormat:@"---------------------------------\n\n"]];
    
    // Cellular Data
    // Check if data buffer has wrapped
    maxIndex =_cmiu.cellularRssiStoreCount;

    // Get the recorded time diffs for signal quality data
    timeDiff = [_cmiu getSignalQualityTimeDiff];

    // Can't email the data if we don't yet have it
    if (maxIndex == 0)
    {
        [message appendString:[NSString stringWithFormat:@"No signal quality data acquired\n\n"]];
    }
    else
    {
        [message appendString:[NSString stringWithFormat:@"Cellular signal quality data (Time(s), RSSI, RSRQ)\n\n"]];
        
        for (ii = 0; ii < maxIndex; ii++)
        {
            if( ii > 0 )
            {
                relativeTime += (float)timeDiff[ii];
            }

            rssi = [SignalUtility extractRssiFromSignal:signalQualityData[ii]];
            rsrq = [SignalUtility extractRsrqFromSignal:signalQualityData[ii]];

            NSLog(@"%0.1f, %0.1f, %0.1f\n", relativeTime, rssi, rsrq);
            [message appendString:[NSString stringWithFormat: @"%0.1f, %0.1f, %0.1f\n", relativeTime, rssi, rsrq]];
        }
    }

    // Section separator
    [message appendString:[NSString stringWithFormat:@"---------------------------------\n\n"]];
    
    // Connected meter Data
    //Only use first meter read as CMIU currently double reads the meter
    if (readingsCnt > 0)
    {
        readingsCnt = 1;
    }
    
    if (0 == readingsCnt)
    {
        [message appendString:[NSString stringWithFormat:@"No Meter Readings Received\n\n"]];
    }
    else
    {
        [message appendString:[NSString stringWithFormat:@"Meter Reading Time: %@\n\n", meterReadTimeDate]];
        for (int ii = 0;ii < readingsCnt; ii++)
        {
            [message appendString:[NSString stringWithFormat: @"CMIU ID: %@ \nMeter ID: %016llu \nReading: %08d\n\n", self.cmiu.name, meterReadIds[ii], (unsigned int)meterReadValues[ii]]];
        }
    }
    
    NSString* debugLog = [_cmiu.debugLog retrieve];
    
    MFMailComposeViewController *mailController = [MFMailComposeViewController new];
    [mailController setToRecipients:toRecipients];
    [mailController setSubject:subject];
    [mailController setMessageBody:message isHTML:NO];
    [mailController addAttachmentData:[debugLog dataUsingEncoding:NSUTF8StringEncoding] mimeType:@"text/plain" fileName:@"debug_log.txt"];
    
    // Debug log
    //[message appendString:[NSString stringWithFormat:@"Debug log: %@", [_cmiu.debugLog retrieve]]];
    
    [self _sendEmail:mailController];
}

- (void)_issueReportButtonPressed
{
    // Date and time for issue report
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss zzz"];
    NSString* timeDateString = [dateFormatter stringFromDate:[NSDate date]];
    // iOS version
    NSString* iosVersionString = [UIDevice currentDevice].systemVersion;
    NSArray* toRecipients = [NSArray arrayWithObjects:@"hhsupp@neptunetg.com", nil];
    
    NSString *subject = [NSString stringWithFormat:@"Issue Report for %@", self.cmiu.name];
    NSMutableString *message = [NSMutableString new];
    [message appendString:[NSString stringWithFormat:@"Description: <insert issue description here>\n\n\n"]];
    [message appendString:[NSString stringWithFormat:@"Steps to reproduce: <insert issue steps to reproduce here>\n\n\n"]];
    [message appendString:[NSString stringWithFormat:@"Date and Time: %@\n\n\n", timeDateString]];
    [message appendString:[NSString stringWithFormat:@"iOS version: v%@\n\n\n", iosVersionString]];
    [message appendString:[NSString stringWithFormat:@"CMIU version: %@\n\n\n", _cmiu.firmwareRevision ? [NSString stringWithFormat:@"v%@", _cmiu.firmwareRevision] : @"Unknown"]];
    
    [self _sendEmailWithSubject:subject message:message :toRecipients];
}

- (IBAction)configButtonPressed:(id)sender {

    // Check for authentication
    if( [[LicenseManager getLicenseManager] getLicenseMode] == LICENSE_SUPERUSER )
    {
        [self performSegueWithIdentifier:@"Config" sender:self];
    }
}

- (IBAction)meterButtonPressed:(id)sender {
    if ([sender isEqual:self.meterButton])
    {
        // Signal to start sending out get current meter reading command
        [_cmiu sendReadConnectedDevicesCommand];
        
        _meterReadProgress = [ProgressDialogViewController progressControllerWithTitle:@"Reading connected meters..."];
        
        [self.navigationController.topViewController presentViewController:_meterReadProgress animated:YES completion:nil];
        
    }
}

- (void)_updateMeterRead
{
    // Get attempt count
    UInt16 attemptCnt = [_cmiu getMeterAttemptCnt];

    [_meterReadProgress setProgress:((float) attemptCnt / MAX_METER_READ_ATTEMPTS)];
}

- (void)_updateMeterCompleteRead
{
    // Set progress bar to completed
    [_meterReadProgress setProgress:1];
    
    // Show meter readings after delay
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self _showMeterReadings];
    });
}

-(void)_showMeterReadings
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_meterReadProgress dismissViewControllerAnimated:YES completion:nil];
        
        // Get the meter readings
        UInt16 readingsCnt = [_cmiu getMeterReadCnt];
        UInt64 *meterReadIds = [_cmiu getMeterReadIds];
        UInt32 *meterReadValues = [_cmiu getMeterReadValues];
        NSString *meterReadings = [NSString stringWithFormat:@""];
        
        NSLog(@"No. Meter Reads: %d ", readingsCnt);
        if (0 == readingsCnt)
        {
            meterReadings = [meterReadings stringByAppendingFormat:
                             @"No meter readings received"];
        }
        else
        {
            for (int ii = 0;ii < readingsCnt; ii++)
            {
                meterReadings = [meterReadings stringByAppendingFormat:
                                 @"CMIU ID: %@ \nMeter ID: %016llu \nReading: %@\n\n", self.cmiu.name, meterReadIds[ii], [self.cmiu.class getHumanReading:meterReadValues[ii]]];
            }
        }
        
        NSMutableAttributedString *meterTitleAttributed = [[NSMutableAttributedString alloc] initWithString:@"Meter Readings\n"];
        [meterTitleAttributed addAttribute:NSFontAttributeName
                                        value:[UIFont boldSystemFontOfSize:25.0]
                                        range:NSMakeRange(0, meterTitleAttributed.length)];
        
        NSMutableAttributedString *meterReadingsAttributed = [[NSMutableAttributedString alloc] initWithString:meterReadings];
        [meterReadingsAttributed addAttribute:NSFontAttributeName
                                        value:[UIFont systemFontOfSize:18.0]
                                        range:NSMakeRange(0, meterReadings.length)];
        _meterReadResultsAlert = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        [_meterReadResultsAlert setValue:meterTitleAttributed forKey:@"attributedTitle"];
        [_meterReadResultsAlert setValue:meterReadingsAttributed forKey:@"attributedMessage"];
        
        // Date and time for meter reading
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss zzz"];
        NSString* timeDateString = [dateFormatter stringFromDate:[NSDate date]];
        meterReadTimeDate = timeDateString;
        
        __weak CMIUStatusTableViewController *weakSelf = self;
        [_meterReadResultsAlert addAction:[UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction *_meterReadResultsAlert) {
            [weakSelf createMeterReadingEmail: timeDateString];
            }]];
        [_meterReadResultsAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [self.navigationController.topViewController presentViewController:_meterReadResultsAlert animated:YES completion:nil];
    });
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Locate"])
    {
        LocateViewController *locateViewController = (LocateViewController *)segue.destinationViewController;
        locateViewController.cmiu = self.cmiu;
        return;
    }
    
    if ([segue.identifier isEqualToString:@"Config"])
    {
        ConfigViewController *configViewController = (ConfigViewController *)segue.destinationViewController;
        configViewController.cmiu = self.cmiu;
        return;
    }
}


#pragma mark - Public methods

- (void)refreshData
{
    // Try to get data if it's not already doing so
    if (!self.isRefreshing)
    {
        [super refreshData];
        
        [self.cmiu getConfiguration];
    }
}

-(void)_heartbeatStatusDidChange:(CMIU_HeartbeatStatus_t)newValue
{
    switch (newValue)
    {
        case CMIU_HeartbeatStatus_Connected:
            [self _hideHeartbeatAlert];
            break;
            
        case CMIU_HeartbeatStatus_Disconnected:
            break;
            
        case CMIU_HeartbeatStatus_LostConnection:
            [self _showHeartbeatAlert];
            break;
    }
}

-(void)_hideHeartbeatAlert
{
    if(_grayMask)
    {
        [_grayMask removeFromSuperview];
        if(_heartbeatAlert)
        {
            [_heartbeatAlert.presentingViewController dismissViewControllerAnimated:YES completion:^{
                _heartbeatAlert = nil;
            }];
        }
        _grayMask = nil;
        //[self hideSpinner];
    }
}

-(void)_showHeartbeatAlert
{
    [super receivedData:nil];
    
    if(!_grayMask)
    {
        // Gray out the screen while we continue to wait for a heartbeat.
        CGRect f = self.view.frame;
        // Allow scrolling space
        f.origin.y -= 200;
        f.size.height += 400;
        _grayMask = [[UIView alloc] initWithFrame:f];
        [_grayMask setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.3]];
        
        [self.view addSubview:_grayMask];
        __weak CMIUStatusTableViewController *weakSelf = self;
        
        // Due to a bad design decision, we are not necessarily the top most controller.
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Lost communication. Retrying..." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Disconnect" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            CMIUStatusTableViewController *strongSelf = weakSelf;
            [strongSelf goBack];
            
        }]];
        [self.navigationController.topViewController presentViewController:alert animated:YES completion:nil];
        _heartbeatAlert = alert;
        
        //[self showSpinner];
    }
}


-(void)goBack
{
    [self cleanup];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)cleanup
{
    _testRunner = nil;
    [self.cmiu cancelConnection];
    [self _hideHeartbeatAlert];
}

- (void)receivedData:(id)responseObject
{
   [super receivedData:responseObject];
    _lastUpdated = [NSDate date];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _statusData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StatusCell" forIndexPath:indexPath];
    
    NSDictionary *dataDictionary = _statusData[indexPath.row];
    
    UILabel *fieldLabel = (UILabel *)[cell viewWithTag:1];
    fieldLabel.text = dataDictionary[@"field"];
    
    UILabel *valueLabel = (UILabel *)[cell viewWithTag:2];
    valueLabel.text = dataDictionary[@"value"];
    
    return cell;
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    // Dismiss the controller
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table data helpers

- (void)_updateTableData
{
    NSString *recordingInterval;
    NSString *reportingInterval;
    
    // assign 3R intervals (currently only 2Rs)
    int mins = _cmiu.recordingInterval;
    NSArray *times = [_cmiu logReadingTimes];
    for(int i = 0; i < times.count; i++)
    {
        if([times[i][1] intValue] >= mins)
        {
            recordingInterval = times[i][0];
            break;
        }
    }
    int hours = _cmiu.reportingInterval;
    times = [_cmiu sendingTimes];
    for(int i = 0; i < times.count; i++)
    {
        if([times[i][1] intValue] >=hours)
        {
            reportingInterval = times[i][0];
            break;
        }
    }
    
    NSString *modemSwRevision;
    if ([_cmiu.modemSwRevision length] == 0)
    {
        modemSwRevision = @"UnKnown";
    }
    else
    {
        if ([[_cmiu.modemSwRevision substringToIndex:1] isEqualToString:@"#"])
        {
            modemSwRevision = [NSString stringWithFormat:@"v%@", [_cmiu.modemSwRevision substringWithRange:NSMakeRange(7, [_cmiu.modemSwRevision length]-2-7)]];
        }
        else
        {
            modemSwRevision = _cmiu.modemSwRevision;
        }
    }
    
    NSString *modemSerialNumber;
    if ([_cmiu.modemSerialNumber length] == 0)
    {
        modemSerialNumber = @"UnKnown";
    }
    else
    {
        if ([[_cmiu.modemSerialNumber substringToIndex:1] isEqualToString:@"#"])
        {
            modemSerialNumber = [_cmiu.modemSerialNumber substringWithRange:NSMakeRange(7, [_cmiu.modemSerialNumber length]-2-7)];
        }
        else
        {
            modemSerialNumber = _cmiu.modemSerialNumber;
        }
    }
    
    NSString *SimImsi;
    if ([_cmiu.SimImsi length] == 0)
    {
        SimImsi = @"UnKnown";
    }
    else
    {
        if ([[_cmiu.SimImsi substringToIndex:1] isEqualToString:@"#"])
        {
            SimImsi = [_cmiu.SimImsi substringWithRange:NSMakeRange(7, [_cmiu.SimImsi length]-2-7)];
        }
        else
        {
            SimImsi = _cmiu.SimImsi;
        }
    }
    
    NSString *SimId;
    if ([_cmiu.SimId length] == 0)
    {
        SimId = @"UnKnown";
    }
    else
    {
        if ([[_cmiu.SimId substringToIndex:1] isEqualToString:@"#"])
        {
            SimId = [_cmiu.SimId substringWithRange:NSMakeRange(7, [_cmiu.SimId length]-2-7)];
        }
        else
        {
            SimId = _cmiu.SimId;
        }
    }
    
    _statusData = @[
                    @{
                        @"field": @"Mode",
                        @"value": [[_cmiu getMode] lookupLabel]
                        },
                    @{
                        @"field": @"Reporting Interval",
                        @"value":reportingInterval ? reportingInterval : @"Unknown"
                        },
                    @{
                        @"field": @"Recording Interval",
                        @"value":recordingInterval ? recordingInterval : @"Unknown"
                        },
                    @{
                        @"field": @"Reading Interval",
                        @"value":@"15 minutes"
                        },
                    @{
                        @"field": @"CMIU Clock",
                        @"value": [self _timeStringFromDate:_cmiu.cmiuClock]
                        },
                    @{
                        @"field": @"Last RSSI",
                        @"value": (MIN_RSSI == _cmiu.rssiMostRecent) ? @"N/A" : [NSString stringWithFormat:@"%0.0fdBm", _cmiu.rssiMostRecent]
                        },
                    @{
                        @"field": @"Last RSRQ",
                        @"value": (MIN_RSRQ == _cmiu.rsrqMostRecent) ? @"N/A" : [NSString stringWithFormat:@"%0.0fdBm", _cmiu.rsrqMostRecent]
                        },
                    @{
                        @"field": @"Firmware Rev.",
                        @"value":_cmiu.firmwareRevision ? [NSString stringWithFormat:@"v%@", _cmiu.firmwareRevision] : @"Unknown"
                        },
                    @{
                        @"field": @"Bootloader Rev.",
                        @"value":_cmiu.bootloaderRevision ? [NSString stringWithFormat:@"v%@", _cmiu.bootloaderRevision] : @"Unknown"
                        },
                    @{
                        @"field": @"Config Rev.",
                        @"value":_cmiu.configRevision ? [NSString stringWithFormat:@"v%@", _cmiu.configRevision] : @"Unknown"
                        },
                    @{
                        @"field": @"Broker",
                        @"value":_cmiu.mqttBrokerAddress ? _cmiu.mqttBrokerAddress : @"Unknown"
                        },
                    @{
                        @"field": @"Modem Sw Rev.",
                        @"value":_cmiu.modemSwRevision ? modemSwRevision : @"Unknown"
                        },
                    @{
                        @"field": @"Modem S/N",
                        @"value":_cmiu.modemSerialNumber ? modemSerialNumber : @"Unknown"
                        },
                    @{
                        @"field": @"IMSI",
                        @"value":_cmiu.SimImsi ? SimImsi : @"Unknown"
                        },
                    @{
                        @"field": @"ICCID",
                        @"value":_cmiu.SimId ? SimId : @"Unknown"
                        },
                    @{
                        @"field": @"Temperature",
                        @"value": [NSString stringWithFormat:@"%d\u00B0C / %1.0f\u00B0F", _cmiu.temperature, (CGFloat)_cmiu.temperature * 9.0 / 5.0 + 32]
                        }
                    ];
    
    [self.tableView reloadData];

    [self activateDetailedButtons];
}

- (NSString *)_timeStringFromDate:(uint64_t)secs
{
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:secs];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"hh:mm:ss";
    NSString *ret = [dateFormatter stringFromDate:date];;
    return ret;
}

- (NSString *)_dateStringFromDate:(NSDate *)date
{
    if(!date)
    {
        return @"Unknown";
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger dayNumber = [calendar component:NSCalendarUnitWeekday fromDate:date];
    NSString *dayString = dateFormatter.shortWeekdaySymbols[dayNumber - 1];
    
    dateFormatter.dateFormat = @"dd";
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    dateFormatter.dateFormat = @"MM";
    NSUInteger monthNumber = [[dateFormatter stringFromDate:date] integerValue];
    NSString *monthString = dateFormatter.shortMonthSymbols[monthNumber - 1];
    
    dateFormatter.dateFormat = @"yyyy";
    NSString *yearString = [dateFormatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@ %@ %@ %@", dayString, dateString, monthString, yearString];
}

- (NSString *)_stringFromBool:(BOOL)boolean
{
    if (boolean)
    {
        return @"Yes";
    }
    return @"No";
}

#pragma mark - Email helpers

-(void)createMeterReadingEmail: (NSString*)timeDateString
{
    UInt16 readingsCnt = [_cmiu getMeterReadCnt];
    UInt64 *meterReadIds = [_cmiu getMeterReadIds];
    UInt32 *meterReadValues = [_cmiu getMeterReadValues];
    NSString *subject = [NSString stringWithFormat:@"Meter reading for %@", self.cmiu.name];
    NSString *message = [NSString stringWithFormat:@""];
    NSArray* toRecipients = [NSArray arrayWithObjects:@"", nil];
    
    //Only use first meter read as CMIU currently double reads the meter
    if (readingsCnt > 0)
    {
        readingsCnt = 1;
    }
    
    if (0 == readingsCnt)
    {
        message = [message stringByAppendingFormat:
                   @"No Meter Readings Received\n"];
    }
    else
    {
        message = [message stringByAppendingFormat:
                         @"Meter Reading Time: %@\n\n", timeDateString];
        for (int ii = 0;ii < readingsCnt; ii++)
        {
            message = [message stringByAppendingFormat:
                             @"CMIU ID: %@ \nMeter ID: %016llu \nReading: %08d\n\n", self.cmiu.name, meterReadIds[ii], (unsigned int)meterReadValues[ii]];
        }
    }
        
    [self _sendEmailWithSubject:subject message:message :toRecipients];
}

- (NSString *)_timeDayDateMonthYearStringFromDate:(NSDate *)date
{
    if (!date)
    {
        return @"";
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"hh:mm";
    NSString *timeString = [dateFormatter stringFromDate:date];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger dayNumber = [calendar component:NSCalendarUnitWeekday fromDate:date];
    NSString *dayString = dateFormatter.shortWeekdaySymbols[dayNumber - 1];
    
    dateFormatter.dateFormat = @"dd";
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    dateFormatter.dateFormat = @"MM";
    NSUInteger monthNumber = [[dateFormatter stringFromDate:date] integerValue];
    NSString *monthString = dateFormatter.monthSymbols[monthNumber - 1];
    
    dateFormatter.dateFormat = @"yyyy";
    NSString *yearString = [dateFormatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@, %@ %@ %@ %@", timeString, dayString, dateString, monthString, yearString];
}

- (NSString *)_bodyStringFromData:(NSArray *)data
{
    if (!data)
    {
        return @"";
    }
    
    NSMutableString *bodyString = [NSMutableString string];
    
    for (NSDictionary *entry in data)
    {
        NSDate *date = entry[@"date"];
        NSString *dateString = [self _bodyDateStringFromDate:date];
        [bodyString appendString:[NSString stringWithFormat:@"%@\n", dateString]];
        
        NSString *description = entry[@"description"];
        [bodyString appendString:[NSString stringWithFormat:@"%@\n\n", description]];
    }
    
    return bodyString;
}

- (NSString *)_bodyDateStringFromDate:(NSDate *)date
{
    if (!date)
    {
        return @"";
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    dateFormatter.dateFormat = @"MM";
    NSUInteger monthNumber = [[dateFormatter stringFromDate:date] integerValue];
    NSString *monthString = dateFormatter.monthSymbols[monthNumber - 1];
    
    dateFormatter.dateFormat = @"dd";
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    dateFormatter.dateFormat = @"yyyy";
    NSString *yearString = [dateFormatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@ %@ %@", monthString, dateString, yearString];
}

- (void)_sendEmailWithSubject:(NSString *)subject message:(NSString *)message :(NSArray *)toRecipients
{
   
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
    [controller setToRecipients:toRecipients];
    [controller setSubject:subject];
    [controller setMessageBody:message isHTML:NO];
    
    [self _sendEmail:controller];
}

-(void)_sendEmail:(MFMailComposeViewController*) controller
{
    if (![MFMailComposeViewController canSendMail])
    {
        NSLog(@"Email composer not available");
        // MH TODO
        return;
    }
    
    controller.navigationBar.barStyle = UIBarStyleDefault;
    controller.mailComposeDelegate = self;
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - Other helpers


@end
