//
//  PeripheralTableViewCell.h
//  CmiuManager
//
//  Created by Ben Blaukopf on 11/06/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CMIU;
@interface PeripheralTableViewCell : UITableViewCell
@property (nonatomic, strong) CMIU *cmiu;

@end
