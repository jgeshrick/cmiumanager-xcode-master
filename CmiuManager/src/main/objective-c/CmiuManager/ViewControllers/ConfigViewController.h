//
//  ConfigViewController.h
//  CmiuManager
//
//  Created by Matthew Houlden on 16/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CMIU, ColorSelectButton;

@interface ConfigViewController : UIViewController <UIPickerViewDelegate>

@property (nonatomic) CMIU *cmiu;

@property (strong, nonatomic) IBOutlet UILabel *cmiuLabel;

@property (strong, nonatomic) IBOutlet UITextField *pollingStartTimeLabel;

@property (strong, nonatomic) IBOutlet ColorSelectButton *sendButton;

- (IBAction)basicButtonPressed:(id)sender;
- (IBAction)advancedButtonPressed:(id)sender;
- (IBAction)proButtonPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *basicButton;
@property (weak, nonatomic) IBOutlet UIButton *advancedButton;
@property (weak, nonatomic) IBOutlet UIButton *proButton;

@property (weak, nonatomic) IBOutlet UILabel *currentModeLabel;

@end
