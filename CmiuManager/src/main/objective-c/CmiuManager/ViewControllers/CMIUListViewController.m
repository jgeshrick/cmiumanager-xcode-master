//
//  CMIUListViewController.m
//  CmiuManager
//
//  Created by Matthew Houlden on 15/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import "CMIUListViewController.h"
#import "CMIU.h"
#import "CMIUConnectViewController.h"
#import "CMIUStatusTableViewController.h"
#import "UINavigationBar+Styling.h"
#import "AppDelegate.h"
#import "ASCBCentralManager.h"
#import "PeripheralTableViewCell.h"
#import "LicenseNavigationController.h"
#import "LicenseManager.h"

@interface CMIUListViewController () <UISearchBarDelegate, CMIUConnectViewControllerDelegate>

@property (nonatomic) NSArray *cmius;

@end

@implementation CMIUListViewController
{
    NSArray *_filteredCmius;
    NSArray *_lastActiveSet;
    UILocalNotification *_multipleNotification;
    CMIU* lastConnectedCmiu;
    NSTimer* peripheralTimer;
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.navigationController.navigationBar applyStyling];

    // Disable the search bar until we receive some data
    self.cmius = nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Check license status
    [self checkLicense];

    // Clear CMIU list until data received
    self.cmius = nil;

    [CMIU scanForCMIUs];

    ASCBCentralManager *manager = [ASCBCentralManager sharedCentral];
    manager.scanning = YES;

    // Start refreshing list
    peripheralTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updatePeripherals) userInfo:nil repeats:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    // Stop scanning
    [peripheralTimer invalidate];
    peripheralTimer = nil;

    ASCBCentralManager *manager = [ASCBCentralManager sharedCentral];
    manager.scanning = NO;
}

-(IBAction)licenseChanged:(UIStoryboardSegue*)segue
{
    // License state changed - License rechecked in viewWillAppear
}

-(void)checkLicense
{
    // Check license status
    if( [[LicenseManager getLicenseManager] getLicenseMode] != LICENSE_USER && [[LicenseManager getLicenseManager] getLicenseMode] != LICENSE_SUPERUSER )
    {
        // Display licensing screen
        [self performSegueWithIdentifier:@"License" sender:nil];
    }
}

-(void)updatePeripherals
{
    ASCBCentralManager *manager = [ASCBCentralManager sharedCentral];
    
    // Did we find a new one while in the background? If so show a notification.
    UIApplication *app = [UIApplication sharedApplication];

    UIApplicationState state = [app applicationState];
    AppDelegate *appDelegate = (AppDelegate*)app.delegate;
    if(state == UIApplicationStateActive || appDelegate.enteringForeground)
    {
        _lastActiveSet = manager.discoveredPeripherals;
    }
    else
    {
        NSMutableSet *discovered = [[NSMutableSet alloc] initWithArray:manager.discoveredPeripherals];
        for(CMIU *cmiu in _lastActiveSet)
        {
            [discovered removeObject:cmiu];
        }
        
        if(discovered.count)
        {
            
            UILocalNotification* localNotification;
            if(discovered.count == 1)
            {
                CMIU *cmiu = discovered.anyObject;
                if(!cmiu.localNotification)
                {
                    localNotification = [[UILocalNotification alloc] init];
                    localNotification.alertBody = [NSString stringWithFormat:@"CMIU %@ detected", cmiu.name];
                    localNotification.userInfo = @{@"CMIUIdentifier" : cmiu.peripheral.identifier.UUIDString, @"CMIUName" : cmiu.name};
                    cmiu.localNotification = localNotification;
                    _multipleNotification = nil;
                }
            }
            else
            {
                if(!_multipleNotification)
                {
                    localNotification = [[UILocalNotification alloc] init];
                    localNotification.alertBody = @"Multiple CMIUs detected";
                    localNotification.userInfo = nil;
                    _multipleNotification = localNotification;
                }
            }
            if(localNotification)
            {
                [app cancelAllLocalNotifications];
                [app presentLocalNotificationNow:localNotification];
                
            }
        }
    }
    
    NSMutableArray* discoveredCmius = [NSMutableArray arrayWithArray:manager.discoveredPeripherals];
    
    NSUInteger lastConnectedCmiuIndex = [discoveredCmius indexOfObject:lastConnectedCmiu];
    
    // Move last connected CMIU to top of the list
    if( lastConnectedCmiuIndex != NSNotFound )
    {
        [discoveredCmius exchangeObjectAtIndex:lastConnectedCmiuIndex withObjectAtIndex:0];
    }

    self.cmius = [discoveredCmius copy];

    return;
}


#pragma mark - Navigation



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Connect"])
    {
        CMIUConnectViewController *connectViewController = segue.destinationViewController;
        CMIU *cmiu = nil;
        if([sender isKindOfClass:[UIView class]])
        {
            // Identify the selected cell.
            UIView *v = sender;
            while(![v isKindOfClass:[PeripheralTableViewCell class]])
            {
                v = v.superview;
            }
            PeripheralTableViewCell *cell = (PeripheralTableViewCell*)v;
            cmiu = cell.cmiu;
        }
        else if([sender isKindOfClass:[CMIU class]])
        {
            cmiu = sender;
        }
        else
        {
            assert(0);
        }
        
        connectViewController.cmiu = cmiu;

        // Save CMIU
        lastConnectedCmiu = cmiu;

        // Refresh to ensure new peripheral highlighted
        [self updatePeripherals];
        
//        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//        CMIU *cmiu = _filteredCmius[indexPath.row];
//        connectViewController.cmiu = cmiu;
        connectViewController.delegate = self;
        return;
    }
    
    if ([segue.identifier isEqualToString:@"Status"])
    {
        CMIUStatusTableViewController *statusViewController = segue.destinationViewController;
        statusViewController.cmiu = (CMIU *)sender;
        return;
    }

    if ([segue.identifier isEqualToString:@"License"])
    {
        NSAssert([segue.destinationViewController isMemberOfClass:[LicenseNavigationController class]], @"License segue to wrong controller");

        __weak LicenseNavigationController* licenseController = (LicenseNavigationController*)segue.destinationViewController;

        // Register license callbacks
        [licenseController segueDataLicenseComplete:^(LicenseMode mode){

                // Dismiss license view
                [licenseController dismissViewControllerAnimated:YES completion:nil];
            }

            licenseFailed:^{
                // Keep license view open
            }
         ];
    }
}

#pragma mark - Properties

- (void)setCmius:(NSArray *)cmius
{
    _cmius = cmius;
    
    self.searchBar.userInteractionEnabled = (self.cmius.count != 0);
    [self updateFilteredCmius];
}

#pragma mark - UISearchBarDelegate

-(void)updateFilteredCmius
{
    NSString *searchText = _searchBar.text;
    if (searchText.length == 0)
    {
        _filteredCmius = self.cmius;
    }
    else
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", searchText];
        _filteredCmius = [self.cmius filteredArrayUsingPredicate:predicate];
    }
    [self.tableView reloadData];
   
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (![searchBar isEqual:self.searchBar])
    {
        return;
    }
    
    [self updateFilteredCmius];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if (![searchBar isEqual:self.searchBar])
    {
        return;
    }
    
    [searchBar resignFirstResponder];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _filteredCmius.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PeripheralTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CMIUListCell" forIndexPath:indexPath];
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-arrow"]];
    cell.textLabel.textColor = [AppDelegate black];
    cell.contentView.backgroundColor = [UIColor whiteColor];
    
    cell.cmiu = _filteredCmius[indexPath.row];
    
    // Highlight last used cell
    if( [cell.cmiu isEqual:lastConnectedCmiu] )
    {
        //cell.contentView.backgroundColor = [UIColor redColor];
        cell.textLabel.textColor = [UIColor orangeColor];
    }
    
    return cell;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (![scrollView isEqual:self.tableView])
    {
        return;
    }
    
    [self.searchBar resignFirstResponder];
}

#pragma mark - CMIUConnectViewControllerDelegate

- (void)cmiuConnectViewControllerDidConnectWithCmiu:(CMIU *)cmiu
{
    if ([self.presentedViewController isKindOfClass:[CMIUConnectViewController class]])
    {
        [self.presentedViewController dismissViewControllerAnimated:YES
                                                         completion:^{
                                                             [self performSegueWithIdentifier:@"Status" sender:cmiu];
                                                         }];
    }
}

- (void)cmiuConnectViewControllerDidCancel
{
    if ([self.presentedViewController isKindOfClass:[CMIUConnectViewController class]])
    {
        [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}


@end
