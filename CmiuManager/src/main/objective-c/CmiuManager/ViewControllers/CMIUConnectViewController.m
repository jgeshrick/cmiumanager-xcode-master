//
//  CMIUConnectViewController.m
//  CmiuManager
//
//  Created by Matthew Houlden on 15/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import "CMIUConnectViewController.h"
#import "CMIU.h"
#import "UINavigationBar+Styling.h"
#import "ColorSelectButton.h"
#import "AppDelegate.h"

const static float DEBUG_BACKLOG_INTERVAL = 0.75;
const static float PROGRESS_BAR_INCREMENT = 0.01;
const static float NAV_BAR_HEIGHT = 64.0;

@implementation CMIUConnectViewController
{
    void (^stateChangeBlock)(NSNotification *note);
    UIProgressView *_progress;
    NSTimer *_DebugBacklogTimeoutTimer;
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSAssert(self.cmiu, @"Should have a CMIU set");
    
    // Create, style and add the navigation bar
    UINavigationBar *navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    [navigationBar applyStyling];
    [self.view addSubview:navigationBar];
    
    // Set the navigation bar title and button(s)
    UINavigationItem *navigationItem = [[UINavigationItem alloc] initWithTitle:@"CMIU"];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(_cancelButtonPressed)];
    [navigationItem setLeftBarButtonItem:backButton];
    NSArray *barItemArray = [[NSArray alloc] initWithObjects:navigationItem, nil];
    [navigationBar setItems:barItemArray];
    
    __weak CMIUConnectViewController *weakSelf = self;
    
    self.cmiuLabel.text = self.cmiu.name;
    self.connectButton.highlightedBackgroundColor = [AppDelegate pressedOrange];
    
    stateChangeBlock = ^(NSNotification *note) {
        CMIUConnectViewController *strongSelf = weakSelf;
        if(strongSelf.cmiu.state == ASCBPeripheralState_Connected)
        {
            [weakSelf setupResponseHandler];
            //Start the debug log backlog timer
            [weakSelf _startDebugBacklogTimer];
            
            NSLog(@"Begin loading debug backlog");
        }
    };
    [[NSNotificationCenter defaultCenter] addObserverForName:ASCBPeripheralStateDidChangeNotification object:self.cmiu queue:nil usingBlock:stateChangeBlock];
}

-(void) viewDidAppear:(BOOL)animated
{
    // Setup the progress bar
    UIProgressView *progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    CGRect f = progressView.frame;
    f.origin.x = 0.0;
    f.origin.y = NAV_BAR_HEIGHT;
    f.size.width = self.view.frame.size.width;
    progressView.frame = f;
    //progressView.center = self.view.center;
    progressView.tintColor = [UIColor blueColor];
    [self.view addSubview:progressView];
    [progressView bringSubviewToFront:self.view];
    _progress = progressView;
    _progress.progress = 0.0;
}

-(void) viewDidDisappear:(BOOL)animated
{
    [self _stopDebugBacklogTimer];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:stateChangeBlock];
    [self _stopDebugBacklogTimer];
}

-(void)_startDebugBacklogTimer
{
    if (![NSThread isMainThread])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self _startDebugBacklogTimer];
        });
        return;
    }
    
    [_DebugBacklogTimeoutTimer invalidate];
    _DebugBacklogTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:DEBUG_BACKLOG_INTERVAL target:self selector:@selector(_debugBacklogTimerExpired:) userInfo:nil repeats:NO];
}

-(void)_stopDebugBacklogTimer
{
    [_DebugBacklogTimeoutTimer invalidate];
    _DebugBacklogTimeoutTimer = nil;
}

-(void)_debugBacklogTimerExpired:(NSTimer*)timer
{
    __weak CMIUConnectViewController *weakSelf = self;
    CMIUConnectViewController *strongSelf = weakSelf;
    [strongSelf.delegate cmiuConnectViewControllerDidConnectWithCmiu:strongSelf.cmiu];
}

#pragma mark - Events
- (void)receivedData
{
    //Restart the debug log backlog timer
    [self _stopDebugBacklogTimer];
    [self _startDebugBacklogTimer];

    _progress.progress += PROGRESS_BAR_INCREMENT;
    if (_progress.progress >= 1.0)
    {
        __weak CMIUConnectViewController *weakSelf = self;
        CMIUConnectViewController *strongSelf = weakSelf;
        [strongSelf.delegate cmiuConnectViewControllerDidConnectWithCmiu:strongSelf.cmiu];
    }
}

-(void)setupResponseHandler
{
    __weak CMIUConnectViewController *weakSelf = self;
    
    [_cmiu setResponseHandler:^(E_UARC_MSG_TYPE type, NSData *data) {
        // Mark that we have received data.
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf receivedData];
        });
    }];
}

- (void)_cancelButtonPressed
{
    [self.cmiu cancelConnection];
    [self.delegate cmiuConnectViewControllerDidCancel];
}

- (IBAction)connectButtonPressed:(id)sender
{
    self.connectButton.unhighlightedBackgroundColor = [UIColor lightGrayColor];
    self.DebugMessage.text = @"Connecting...";
    
    [self.cmiu connect];
    
}

@end
