//
//  AppDelegate.h
//  CmiuManager
//
//  Created by Matthew Houlden on 15/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (UIColor *)mainOrange;
+ (UIColor *)pressedOrange;
+ (UIColor *)black;
+ (UIColor *)pressedBlack;
+ (UIColor *)darkGray;

@property (nonatomic, readonly) BOOL enteringForeground;
@end

