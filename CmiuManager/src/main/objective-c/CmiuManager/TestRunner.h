//
//  TestRunner.h
//  CmiuManager
//
//  Created by Ben Blaukopf on 19/06/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "UartControllerMessageTypes.h"
#import "CMIU.h"

@protocol TestRunnerDelegate
-(void)completeTest:(BOOL)passed;
@end

@interface TestRunner : NSObject
@property (nonatomic, weak) UIViewController *presenting;
+(NSArray*)testNames;

-(id)initWithDelegate:(id<TestRunnerDelegate>)delegate cmiu:(CMIU*)cmiu;

// If progress handler is non-nil, it will be called back when a packet of data is TXed
-(void)runTest:(NSString*)name withProgressHandler:(CMIUWriteProgressHandler)progressHandler;


@end
