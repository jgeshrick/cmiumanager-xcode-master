//
//  CmiuManagerTests.m
//  CmiuManagerTests
//
//  Created by Matthew Houlden on 15/04/2015.
//  Copyright (c) 2015 Sagentia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "CMIU.h"
#import "ASCBCentralManager.h"
#import "TestRunner.h"

@interface CmiuManagerTests : XCTestCase <TestRunnerDelegate>

@end

@implementation CmiuManagerTests
{
    CMIU *_cmiu;
    BOOL _completed;
    BOOL _passed;
}

- (void)setUp {
    [super setUp];
    _completed = NO;
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    ASCBCentralManager *manager = [ASCBCentralManager sharedCentral];
    [manager addObserver:self forKeyPath:@"discoveredPeripherals" options:NSKeyValueObservingOptionNew context:nil];
    [CMIU scanForCMIUs];

    // Grab a CMIU if we already have one in the peripheral list.
    if(manager.discoveredPeripherals.count > 0)
    {
        _cmiu = manager.discoveredPeripherals[0];
    }
    else
    {
        manager.scanning = YES;
        NSLog(@"Waiting for CMIU");
    }
    
    if(manager.central.state == CBCentralManagerStateUnsupported)
    {
        NSLog(@"Tests not supported on simulator");
        return;
    }
    NSLog(@"Central state is %d", manager.central.state);
    // Start scanning, and wait for a device to be found
    double startTime = CACurrentMediaTime();
    while(!_cmiu)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
        // Allow 10 seconds to find a device
        if(CACurrentMediaTime() - startTime > 10.0)
        {
            XCTAssert(NO, @"Timed out waiting for device advertisement");
            break;
        }
    }
    [self doConnect];
    
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"discoveredPeripherals"])
    {
        ASCBCentralManager *manager = [ASCBCentralManager sharedCentral];
        if(manager.discoveredPeripherals.count)
        {
            manager.scanning = NO;
            _cmiu = [manager.discoveredPeripherals objectAtIndex:0];
        }
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    [_cmiu cancelConnection];
    
    // Wait until disconnected
    while(_cmiu.state != ASCBPeripheralState_Disconnected)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];

    }
    
    // Wait a fraction longer to allow BT to settle
    //[NSThread sleepForTimeInterval:2.0];
}

-(void)doConnect
{
    [_cmiu connect];
    // Restart connection if it fails
    NSLog(@"Waiting for CMIU connection");
    double startTime = CACurrentMediaTime();

    while(_cmiu.state != ASCBPeripheralState_Connected)
    {
        // Allow 10 seconds to find a device
        if(CACurrentMediaTime() - startTime > 10.0)
        {
            XCTAssert(NO, @"Timed out connecting");
        }

        // Check for connection failure
        if(_cmiu.state == ASCBPeripheralState_Disconnected)
        {
            // Connection has failed. Retry
            NSLog(@"Connection failed. Retrying");
            [_cmiu connect];
        }
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    NSLog(@"CMIU connected. Commencing test");
    
    
}

-(void)testPing {
    ASCBCentralManager *manager = [ASCBCentralManager sharedCentral];
    if(manager.central.state == CBCentralManagerStateUnsupported)
    {
        NSLog(@"Tests not supported on simulator");
        return;
    }
    

    TestRunner *test  =[[TestRunner alloc] initWithDelegate:self cmiu:_cmiu];
    [test runTest:@"Ping" withProgressHandler:nil];
    while(!_completed)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
    XCTAssert(_passed, @"Test passed");
    
}

-(void)testCMD8 {
    ASCBCentralManager *manager = [ASCBCentralManager sharedCentral];
    if(manager.central.state == CBCentralManagerStateUnsupported)
    {
        NSLog(@"Tests not supported on simulator");
        return;
    }
    

    TestRunner *test  =[[TestRunner alloc] initWithDelegate:self cmiu:_cmiu];
    [test runTest:@"CMD8" withProgressHandler:nil];
    while(!_completed)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
    XCTAssert(_passed, @"Test passed");
    
}

-(void)testCMD32 {
    ASCBCentralManager *manager = [ASCBCentralManager sharedCentral];
    if(manager.central.state == CBCentralManagerStateUnsupported)
    {
        NSLog(@"Tests not supported on simulator");
        return;
    }
    

    TestRunner *test  =[[TestRunner alloc] initWithDelegate:self cmiu:_cmiu];
    [test runTest:@"CMD32" withProgressHandler:nil];
    while(!_completed)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
    XCTAssert(_passed, @"Test passed");
    
}

-(void)testCMD2048 {
    ASCBCentralManager *manager = [ASCBCentralManager sharedCentral];
    if(manager.central.state == CBCentralManagerStateUnsupported)
    {
        NSLog(@"Tests not supported on simulator");
        return;
    }
    

    TestRunner *test  =[[TestRunner alloc] initWithDelegate:self cmiu:_cmiu];
    [test runTest:@"CMD2048" withProgressHandler:nil];
    while(!_completed)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
    XCTAssert(_passed, @"Test passed");
    
}

-(void)test10KB {
    ASCBCentralManager *manager = [ASCBCentralManager sharedCentral];
    if(manager.central.state == CBCentralManagerStateUnsupported)
    {
        NSLog(@"Tests not supported on simulator");
        return;
    }
    

    TestRunner *test  =[[TestRunner alloc] initWithDelegate:self cmiu:_cmiu];
    static float nextPercent;
    nextPercent = 10.0;
    
    [test runTest:@"10kB" withProgressHandler:^(size_t written, size_t length) {
        float percent = (100.0 * written) / length;
        if(percent > nextPercent)
        {
            NSLog(@"Wrote %g%%", percent);
            nextPercent += 10.0;
        }
    }];
    while(!_completed)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
    XCTAssert(_passed, @"Test passed");
    
}


-(void)completeTest:(BOOL)passed
{
    _completed = YES;
    _passed = passed;
}

@end
