// Copyright (c) Airsource Ltd. 2014.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#import "ASGraphAxis.h"
#import "ASGraphStyle.h"
#import "ASGraphView.h"
@interface ASGraphAxis()
@end

@implementation ASGraphAxis


- (id) init
{
    NSAssert(0, @"Error: Should always initialise with axis details");
    return nil;
}
- (id) initWithCoder:(NSCoder *)aDecoder
{
    NSAssert(0, @"Error: Should always initialise with axis details");
    return nil;
}
- (id) initWithFrame:(CGRect)frame
{
    NSAssert(0, @"Error: Should always initialise with axis details");
    return nil;
}
- (id) initWithDetails:(CGRectEdge)rectEdge
                  from:(CGFloat)minimum to:(CGFloat)maximum
            ticksEvery:(CGFloat)ticksEvery
{
    self = [super initWithFrame:CGRectZero];
    if (self)
    {
        _rectEdge = rectEdge;
        _minimum = minimum;
        _maximum = maximum;
        _margin = 0.0f;
        _ticksEvery = ticksEvery;
        //This switch helps when the parent view is resized.
        switch (_rectEdge)
        {
            case CGRectMinYEdge:
                self.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
                break;
            case CGRectMaxYEdge:
                self.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
                break;
            case CGRectMinXEdge:
                self.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
                break;
            case CGRectMaxXEdge:
                self.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
                break;
        }
        return self;
    }
    return nil;
}

- (void) setGraphRect:(CGRect)graphRect
{
    _graphRect = graphRect;
    CGRect f;
    switch (_rectEdge)
    {
        case CGRectMinYEdge:
            // Position below the provided graph rect
            f = CGRectMake(_graphRect.origin.x - kOverflowMargin, _graphRect.origin.y + _graphRect.size.height,
                                    _graphRect.size.width + 2 * kOverflowMargin, self.margin);
            break;
        case CGRectMaxYEdge:
            // Position above the provided graph rect
            f = CGRectMake(_graphRect.origin.x - kOverflowMargin, _graphRect.origin.y - self.margin,
                                    _graphRect.size.width + 2 * kOverflowMargin, self.margin);
            break;
        case CGRectMinXEdge:
            // Position left of the provided graph rect
            f = CGRectMake(_graphRect.origin.x - self.margin, _graphRect.origin.y - kOverflowMargin,
                                    self.margin, _graphRect.size.height + 2 * kOverflowMargin);
            break;
        case CGRectMaxXEdge:
            // Position right of the provided graph rect
            f = CGRectMake(_graphRect.origin.x + _graphRect.size.width, _graphRect.origin.y - kOverflowMargin,
                                    self.margin, _graphRect.size.height + 2 * kOverflowMargin);
            break;
    }
    self.frame = f;

}

- (void) drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClearRect(context, rect);
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);

    CGContextFillRect(context, rect);
    [self drawAxis:context];
    [self drawTicks:context];
}

- (void) drawTicks:(CGContextRef)context
{
    if (_ticksEvery > 0.0f)
    {
        [self drawTicksOnAxis:context];
    }
}

- (void) drawAxis:(CGContextRef)context //rect:(CGRect)rect
{
    CGRect rect = self.bounds;
    CGFloat w = rect.size.width;
    CGFloat h = rect.size.height;
    switch (_rectEdge)
    {
        case CGRectMinYEdge:
            CGContextMoveToPoint(context, kOverflowMargin, 0);
            CGContextAddLineToPoint(context, w - kOverflowMargin, 0);
            break;
        case CGRectMaxYEdge:
            CGContextMoveToPoint(context, kOverflowMargin,  h);
            CGContextAddLineToPoint(context,  w - kOverflowMargin, h);
            break;
        case CGRectMinXEdge:
            CGContextMoveToPoint(context, w, kOverflowMargin);
            CGContextAddLineToPoint(context, w,  h - kOverflowMargin);
            break;
        case CGRectMaxXEdge:
            CGContextMoveToPoint(context, 0,kOverflowMargin);
            CGContextAddLineToPoint(context, 0, h - kOverflowMargin);
            break;
    }
    [[ASGraphStyle sharedStyle] setAxisStyleOnContext:context];
	CGContextStrokePath(context);
}

- (void) drawTicksOnAxis:(CGContextRef)context
{
    // Determine how to draw ticks
    CGFloat tickSize = 10.0f;
    CGRect tickRect;
    NSUInteger numTicks = ceilf((_maximum  - _minimum)/ _ticksEvery);
    bool horizontal = false;
    switch (_rectEdge)
    {
        case CGRectMinYEdge:
        {
            tickRect = CGRectMake(_graphRect.origin.x,
                                  _graphRect.origin.y + _graphRect.size.height,
                                  _graphRect.size.width ,
                                  tickSize);
            tickRect = CGRectMake(0,0,_graphRect.size.width,tickSize);
            break;
        }
        case CGRectMaxYEdge:
        {
            tickRect = CGRectMake(_graphRect.origin.x + kOverflowMargin,
                                  _graphRect.origin.y,
                                  _graphRect.size.width,
                                  tickSize);
            break;
        }
        case CGRectMinXEdge:
        {
            tickRect = CGRectMake(_graphRect.origin.x - tickSize,
                                  _graphRect.origin.y,
                                  tickSize,
                                  _graphRect.size.height);
            horizontal = true;
            break;
        }
        case CGRectMaxXEdge:
        {
            tickRect = CGRectMake(_graphRect.origin.x + _graphRect.size.width - tickSize,
                                  _graphRect.origin.y ,
                                  tickSize,
                                  _graphRect.size.height);
            horizontal = true;
            break;
        }
    }
    [self drawTicks: context rect:tickRect number:numTicks horizontal:horizontal];
}

- (void) drawTicks:(CGContextRef)context rect:(CGRect)rect number:(NSUInteger)number horizontal:(bool)horizontal
{
    if (!number)
    {
        return;
    }
    if (horizontal)
    {
        CGFloat dy = rect.size.height / (CGFloat)number;
        // Draw horizontal lines
        for (CGFloat y = rect.origin.y + rect.size.height; y >= 0; y -= dy)
        {
            CGContextMoveToPoint(context, rect.origin.x, floor(y)+ kOverflowMargin);
            CGContextAddLineToPoint(context, rect.origin.x + rect.size.width, floor(y)+kOverflowMargin);
        }
    }
    else
    {
        CGFloat dx = rect.size.width / (CGFloat)number;
        // Draw vertical lines
        for (CGFloat x = rect.origin.x ; x <= rect.size.width; x += dx)
        {
            CGContextMoveToPoint(context, floor(x) + kOverflowMargin, rect.origin.y);
            CGContextAddLineToPoint(context, floor(x) + kOverflowMargin, rect.origin.y + rect.size.height);
        }
    }
    [[ASGraphStyle sharedStyle] setAxisStyleOnContext:context];
	CGContextStrokePath(context);
}

@end
