// Copyright (c) Airsource Ltd. 2014.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define kOverflowMargin 9

/**
 * This class defines the parameters and style of a graph axis. It is also a base
 * class for other types of axis
 *
 * TODO: Object structure is imperfect. We are often switching on ASGraphAxisType.
 *       Instead should this be refactored to ASGraphXAxis, ASGraphYAxis e.g.?
 *       Also it would be nice to have a subclass that can take any UIView as labels
 */
@interface ASGraphAxis : UIView

/**
 * This is the designated initialiser.
 *
 * rectEdge:   The location of this axis
 * from:       The minimum value on this axis
 * to:         The maximum value on this axis
 * ticksEvery: The number of divisions between each axis ticks. Set to 0.0f for no ticks.
 */
- (id) initWithDetails:(CGRectEdge)rectEdge
                  from:(CGFloat)minimum to:(CGFloat)maximum
            ticksEvery:(CGFloat)ticksEvery;

/**
 * The graph's frame rectangle
 */
@property (nonatomic, assign) CGRect graphRect;

@property (nonatomic, assign) CGFloat ticksEvery;

/**
 * Draws the axis' ticks into the CG context
 *
 * context:   The CG context
 * graphRect: The graph's frame rectangle
 */
- (void) drawTicks:(CGContextRef)context;

/**
 * The maximum value allowed on this axis (required for plotting paths)
 */
@property (assign, nonatomic) CGFloat maximum;
@property (assign, nonatomic) CGFloat minimum;

/**
 * The edge of the graph that this axis will be drawn
 */
@property (readonly, nonatomic) CGRectEdge rectEdge;

/**
 * The margin in pixels this axis requires (for axis labels e.g.)
 */
@property (nonatomic) CGFloat margin;

@end
