// Copyright (c) Airsource Ltd. 2014.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#import "ASGraphView.h"
#import "ASGraphAxis.h"
#import "ASGraphNumberedAxis.h"
#import "ASGraphLabeledAxis.h"
#import "ASGraphStyle.h"
#import "ASGraphLayer.h"
#import "AirsourceChecks.h"

@interface ASGraphView ()
{
    NSMutableArray *_layers;
    UIEdgeInsets _graphMargins;
    CGFloat _xDistance;
}
@end

@implementation ASGraphView

@synthesize xAxis = _xAxis;


- (id)init
{
    self = [super init];
    return [self init_ASGraphView];
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    return [self init_ASGraphView];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    return [self init_ASGraphView];
}
- (id)init_ASGraphView
{
    //we use redraw mode to keep the graph looking nice.
    [self setContentMode:UIViewContentModeRedraw];
    _xDistance = 1;
    _layers = [NSMutableArray new];
    // Create a default style
    ASGraphStyle *sharedStyle = [ASGraphStyle sharedStyle];
    // Create a default X axis
    _xAxis = [[ASGraphNumberedAxis alloc] initWithPrecision:0
                                                   rectEdge:CGRectMinYEdge
                                                       from:0.0f to:self.bounds.size.width
                                                 ticksEvery:0.0f
                                                     offset:0];
    if (sharedStyle && _xAxis)
    {
        _graphMargins = UIEdgeInsetsZero;
        // Force an update of the graph area
        self.xAxis = _xAxis;
        return self;
    }
    return nil;
}

-(void)replaceYAxis:(ASGraphAxis*)yAxis forLayer:(NSUInteger)layerIndex
{
    ASGraphLayer *layer = _layers[layerIndex];
    ASGraphAxis *oldYAxis = layer.yAxis;
    NSArray *subviews = [self subviews];
    layer.yAxis = yAxis;
    NSUInteger index = [subviews indexOfObject:oldYAxis];
    [oldYAxis removeFromSuperview];
    [self insertSubview:yAxis atIndex:index];
    [self setNeedsDisplay];
}

- (NSUInteger) addLayerWithAxis:(ASGraphAxis *)yAxis
{
    // Create and add the layer
    NSUInteger layerIndex = [_layers count];
    ASGraphLayer *layer = [[ASGraphLayer alloc] initWithAxes:_xAxis
                                                       yAxis:yAxis
                                                  layerIndex:layerIndex];
    [_layers addObject:layer];
        
    // Update the graph area
    if (yAxis.rectEdge == CGRectMinXEdge)
    {
        //TODO: what if two y-axes have margins?
        _graphMargins.left += yAxis.margin;
    }
    else if (yAxis.rectEdge == CGRectMaxXEdge)
    {
        _graphMargins.right += yAxis.margin;
    }
    else
    {
        NSAssert(0, @"Adding layer with x-axes not supported!");
    }
    
    [self addSubview:yAxis];
    [self invalidateIntrinsicContentSize];
    
    return layerIndex;
}

- (void) removeLastLayer
{
    //TODO: we need to worry about the margins here
    [_layers removeLastObject];
    [self invalidateIntrinsicContentSize];
}

- (void) removeAllLayers
{
    //TODO: we need to worry about the margins here
    [_layers removeAllObjects];
    [self invalidateIntrinsicContentSize];
}

- (void) setXAxis:(ASGraphAxis *)xAxis
{
    [_xAxis removeFromSuperview];
    _xAxis = xAxis;
    
    // Update the graph area
    if (xAxis.rectEdge == CGRectMaxYEdge)
    {
        _graphMargins.top  = xAxis.margin;
        _graphMargins.bottom = 0;
    }
    else if (xAxis.rectEdge == CGRectMinYEdge)
    {
        _graphMargins.bottom = xAxis.margin;
        _graphMargins.top = 0;
    }
    else
    {
        NSAssert(0, @"Adding layer with y-axis as x-axis not supported!");
    }
    [_xAxis setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_xAxis];
    [self invalidateIntrinsicContentSize];
}

- (void) clearPlots
{
    for (NSObject * object in _layers)
    {
        ASGraphLayer *layer = ASAssertStaticCast(ASGraphLayer, object);
        [layer clearPlots];
    }
    [self invalidateIntrinsicContentSize];
    [self setNeedsDisplay];
}

- (void) addPlot:(NSArray *) data toLayer:(NSUInteger)layerIndex
{
    ASGraphLayer *layer = ASAssertStaticCastOrNil(ASGraphLayer, _layers[layerIndex]);
    if (layer)
    {
        [layer addPlot:data];
        [self bringSubviewToFront:_xAxis];
        [self invalidateIntrinsicContentSize];
        [self setNeedsDisplay];
    }
}


- (void) pushPopPoint:(CGPoint) point toLayer:(NSUInteger)layerIndex toPlotIndex:(NSUInteger)plotIndex pop:(BOOL)pop
{
    ASGraphLayer *layer = ASAssertStaticCastOrNil(ASGraphLayer, _layers[layerIndex]);
    if (layer)
    {
        [layer pushPopPoint:point toPlotIndex:plotIndex pop:(BOOL)pop];
        [self invalidateIntrinsicContentSize];
        [self setNeedsDisplay];
    }
}

- (void) updateVisibleAreaWithXOffset:(CGFloat)offset width:(CGFloat)width
{
    if (((_xAxis.rectEdge == CGRectMinYEdge || _xAxis.rectEdge == CGRectMaxYEdge)) &&
         [_xAxis isKindOfClass:[ASGraphLabeledAxis class]])
    {
        ASGraphLabeledAxis *labeledAxis = ASAssertStaticCast(ASGraphLabeledAxis, _xAxis);
        [labeledAxis setVisibleX:offset to:offset + width];
        // Redraw the X axis
        [_xAxis setNeedsDisplay];
    }
}

- (void) setDataPointWidth:(CGFloat)width
{
    _xDistance = width;
    [self invalidateIntrinsicContentSize];
}

//Tells the system what width we think we should be (we only have an opinion along the x-axis)
- (CGSize) intrinsicContentSize
{
    return CGSizeMake(_xAxis.maximum * _xDistance, UIViewNoIntrinsicMetric);
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Fill in the background
    CGContextClearRect(context, rect);
    CGContextSetFillColorWithColor(context, [ASGraphStyle sharedStyle].backgroundColor.CGColor);
    CGContextFillRect(context, rect);
    
    // Shrink the rect to allow space for both axes.
    rect = UIEdgeInsetsInsetRect(rect, _graphMargins);
    
    
    [_xAxis setGraphRect:rect];
    // Draw layers above the x-axis
    for (NSObject *object in _layers)
    {
        ASGraphLayer *layer = ASAssertStaticCast(ASGraphLayer, object);
        [layer plot:context graphRect:rect];
    }
}

@end
