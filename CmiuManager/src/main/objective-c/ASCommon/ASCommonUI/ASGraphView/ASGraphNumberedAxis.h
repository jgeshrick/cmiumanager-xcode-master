// Copyright (c) Airsource Ltd. 2014.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#import "ASGraphLabeledAxis.h"

/**
 * A sub-class of an ASGraphLabeledAxis that makes number labels.
 */
@interface ASGraphNumberedAxis : ASGraphLabeledAxis

/**
 * This is the designated initialiser.
 *
 * precisionDP: The number of decimal places to include in the number labels
 * rectEdge:    The location of this axis
 * from:        The minimum value on this axis
 * to:          The maximum value on this axis
 * ticksEvery:  The number of divisions between each axis number labels and ticks
 */
- (id) initWithPrecision:(NSUInteger)precisionDP
                rectEdge:(CGRectEdge)rectEdge
                    from:(CGFloat)minimum to:(CGFloat)maximum
              ticksEvery:(CGFloat)ticksEvery
                  offset:(CGFloat)offset;


@end
