// Copyright (c) Airsource Ltd. 2014.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#import "ASGraphAxis.h"

/**
 * A sub-class of an ASGraphAxis that adds UILabels to the axis.
 *
 * TODO: This currently only supports fixed-size single-line labels.
 */
@interface ASGraphLabeledAxis : ASGraphAxis

/**
 * This is the designated initialiser.
 *
 * labels:    An array of NSStrings for labelling the axis
 * locations: An array of NSNumbers that indicate the positions of the
 *            labels (as axis values, not pixels)
 * rectEdge:  The location of this axis
 * maximum:   The maximum value on this axis
 */
- (id) initWithLabels:(NSArray *)labels
            locations:(NSArray *)locations
             rectEdge:(CGRectEdge)rectEdge
              minimum:(CGFloat)minimum
              maximum:(CGFloat)maximum;

/**
 * Informs a labeled X axis which portion is visible to enable
 * offsetting of label positions
 *
 * start:  The start of the visible area as a pixel value
 * finish: The end of the visible area as a pixel value
 */
-(void)setVisibleX:(CGFloat)start to:(CGFloat)finish;

@end
