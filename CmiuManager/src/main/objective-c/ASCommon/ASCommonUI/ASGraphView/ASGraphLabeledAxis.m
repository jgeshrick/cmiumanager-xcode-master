// Copyright (c) Airsource Ltd. 2014.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#import "ASGraphLabeledAxis.h"
#import "ASGraphStyle.h"
#import <CoreText/CoreText.h>
#import "AirsourceLog.h"

static const CGFloat kTextPadding = 0;
// Fixed label frame width and heights in pixels
static const CGFloat kAxisLabelWidth = 50.0f;
static const CGFloat kAxisLabelHeight = 25.0f;

@interface ASGraphLabeledAxis()
{
    NSArray * _axisLabels;
    NSArray * _labelLocations;
    NSMutableArray * _labelXOffsets;
}
@end

@implementation ASGraphLabeledAxis

- (id) init
{
    NSAssert(0, @"Error: Should always initialise with axis labels");
    return nil;
}
- (id) initWithDetails:(CGRectEdge)rectEdge from:(CGFloat)minimum to:(CGFloat)maximum ticksEvery:(CGFloat)ticksEvery
{
    NSAssert(0, @"Error: Should always initialise with axis labels");
    return nil;
}
- (id) initWithCoder:(NSCoder *)aDecoder
{
    NSAssert(0, @"Error: Should always initialise with axis labels");
    return nil;
}
- (id) initWithFrame:(CGRect)frame
{
    NSAssert(0, @"Error: Should always initialise with axis labels");
    return nil;
}
- (id) initWithLabels:(NSArray *)labels
            locations:(NSArray *)locations
             rectEdge:(CGRectEdge)rectEdge
              minimum:(CGFloat)minimum
              maximum:(CGFloat)maximum
{
    self = [super initWithDetails:rectEdge from:minimum to:maximum ticksEvery:0.0f];
    if (self)
    {
        _axisLabels = [[NSArray alloc] initWithArray:labels];
        _labelLocations = [[NSArray alloc] initWithArray:locations];
        switch (self.rectEdge)
        {
            case CGRectMinYEdge:
            case CGRectMaxYEdge:
                self.margin = kAxisLabelHeight;
                break;
            case CGRectMinXEdge:
            case CGRectMaxXEdge:
                self.margin = kAxisLabelWidth;
                break;
        }
        return self;
    }
    return nil;
}

- (void) drawRect:(CGRect)rect
{
    [super drawRect:rect];
    if (_axisLabels && _labelLocations && ([_axisLabels count] == [_labelLocations count]))
    {
        CGContextRef context = UIGraphicsGetCurrentContext();
        NSUInteger count = _axisLabels.count;
        float range = self.maximum - self.minimum;
        CGFloat scaleX = (self.frame.size.width - 2 * kOverflowMargin)/ range;
        CGFloat scaleY = (self.frame.size.height - 2 * kOverflowMargin)/ range;
        NSString *label;
        
        // Fetch the styles
        ASGraphStyle *style = [ASGraphStyle sharedStyle];
        UIFont *uiFont = style.labelFont;
        CTFontRef font = CTFontCreateWithName( (CFStringRef)uiFont.fontName, uiFont.pointSize, NULL);
        CGColorRef textColor = style.labelColor.CGColor;
        CTTextAlignment textAlignment = kCTCenterTextAlignment;
        
        // Prepare the view for drawing
        CGContextSetTextMatrix(context, CGAffineTransformIdentity);
        CGFloat yOffset = self.frame.size.height + self.frame.origin.y;
        CGContextTranslateCTM(context, 0, yOffset);
        CGContextScaleCTM(context, 1.0, -1.0);
        
        for (NSUInteger i = 0; i < count; i++)
        {
            label = _axisLabels[i];
//            CGFloat w = 0.0f;
//            if (i == (count - 1))
//            {
//                w = scaleX * (self.maximum - [_labelLocations[i] floatValue]);
//            }
//            else
//            {
//                w = scaleX * ([_labelLocations[i+1] floatValue] - [_labelLocations[i] floatValue]);
//            }
            CGFloat x = scaleX * [_labelLocations[i] floatValue];
            CGFloat y = scaleY * [_labelLocations[i] floatValue] - (kAxisLabelHeight / 2.0f);
            CGRect textFrame;
            switch (self.rectEdge)
            {
                case CGRectMinYEdge:
                case CGRectMaxYEdge:
                {
                    textFrame = CGRectMake(x - kAxisLabelWidth/2 + kOverflowMargin, self.frame.origin.y,
                                       kAxisLabelWidth, self.margin);
                    textAlignment = kCTCenterTextAlignment;
                    break;
                }
                case CGRectMinXEdge:
                {
                    textFrame = CGRectMake(self.frame.origin.x, y,
                                       self.margin - 15, kAxisLabelHeight);
                    textAlignment = kCTRightTextAlignment;
                    break;
                }
                case CGRectMaxXEdge:
                {
                    textFrame = CGRectMake(self.frame.origin.x, y,
                                       self.margin, kAxisLabelHeight);
                    textAlignment = kCTLeftTextAlignment;
                    break;
                }
            }
            
            // Create attributed string
            NSDictionary *attributesDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                            (__bridge id)font, (NSString *)kCTFontAttributeName,
                                            textColor, (NSString *)kCTForegroundColorAttributeName,
                                            nil];
            NSAttributedString *stringToDraw = [[NSAttributedString alloc] initWithString:label attributes:attributesDict];
            
            // Create the text
            CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((CFAttributedStringRef)stringToDraw);
            CGMutablePathRef textPath = CGPathCreateMutable();
            CGSize textSize = stringToDraw.size;
            // Manually centre and pad text in frame
            CGRect textRect = CGRectMake(textFrame.origin.x + ((textFrame.size.width - textSize.width) / 2.0f),
                                         textFrame.origin.y + ((textFrame.size.height - textSize.height) / 2.0f),
                                         textSize.width, textSize.height);
            
            switch (textAlignment)
            {
                case kCTLeftTextAlignment:
                    textRect.origin.x = textFrame.origin.x + kTextPadding;
                    break;
                case kCTRightTextAlignment:
                    textRect.origin.x = textFrame.origin.x + textFrame.size.width - textRect.size.width - kTextPadding;
                    break;
                default:
                    if (style.shouldOffsetLabels && _labelXOffsets && (i < _labelXOffsets.count))
                    {
                        textRect.origin.x = textRect.origin.x + [_labelXOffsets[i] floatValue];
                    }
                    break;
            }
            
            // Draw the frame
            // TODO - perhaps switch off anti-aliasing here
            CGMutablePathRef framePath = CGPathCreateMutable();
            CGPathAddRect(framePath, nil, textFrame);
            CGContextAddPath(context, framePath);
            CGContextSetFillColorWithColor(context, style.labelBackgroundColor.CGColor);
            CGContextSetStrokeColorWithColor(context, style.labelBorderColor.CGColor);
            CGContextDrawPath(context, kCGPathFillStroke);
            
            // Draw the text
            CGContextSaveGState(context);
            CGContextSetAllowsFontSubpixelPositioning(context, YES);
            CGContextSetShouldSubpixelPositionFonts(context, YES);
            CGPathAddRect(textPath, nil, textRect);
            CTFrameRef textFramesetterFrame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, 0), textPath, NULL);
            CTFrameDraw(textFramesetterFrame, context);
            CGContextRestoreGState(context);
        }
        // Reset the context
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextTranslateCTM(context, 0, -yOffset);
    }
}

-(void)setVisibleX:(CGFloat)start to:(CGFloat)finish
{
    if ((self.rectEdge == CGRectMinXEdge) || (self.rectEdge == CGRectMaxXEdge))
    {
        Airsource_Log_Debug(@"Label offset feature is only avaiable for a horizontal axis");
        return;
    }
    // Fetch the styles
    ASGraphStyle *style = [ASGraphStyle sharedStyle];
    if (!style.shouldOffsetLabels)
    {
        Airsource_Log_Debug(@"Label offset feature not enabled in the style");
        return;
    }
    
    CGFloat scale = self.frame.size.width / self.maximum;
    _labelXOffsets = [[NSMutableArray alloc] initWithCapacity:_axisLabels.count];
    
    UIFont *uiFont = style.labelFont;
    CTFontRef font = CTFontCreateWithName( (CFStringRef)uiFont.fontName, uiFont.pointSize, NULL);
    CGColorRef textColor = style.labelColor.CGColor;
    
    CGFloat labelFinish;
    for (NSUInteger i = 0; i < _labelLocations.count; i++)
    {
        CGFloat labelStart = scale * [_labelLocations[i] floatValue];
        if (i < (_labelLocations.count - 1))
        {
            labelFinish = scale * [_labelLocations[i+1] floatValue];
        }
        else
        {
            labelFinish = self.frame.size.width;
        }
        CGFloat labelMiddle = (labelStart + labelFinish) / 2.0f;
        // Create attributed string
        NSDictionary *attributesDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                        (__bridge id)font, (NSString *)kCTFontAttributeName,
                                        textColor, (NSString *)kCTForegroundColorAttributeName,
                                        nil];
        NSAttributedString *stringToDraw = [[NSAttributedString alloc] initWithString:_axisLabels[i] attributes:attributesDict];
        CGSize textSize = stringToDraw.size;
        CGFloat textWidth = textSize.width;
        
        CGFloat offset = 0;
        if (labelStart < start)
        {
            if (labelFinish > finish)
            {
                // Label larger than visible
                // Keep label centred in visible space
                offset = ((start - labelStart) - (labelFinish - finish)) / 2;
            }
            else
            {
                if (labelFinish > start)
                {
                    // Just right edge of label is visible
                    if ((labelFinish - start) < (textWidth + (2.0f * kTextPadding)))
                    {
                        // Whole text not visible
                        // Right justify
                        offset = labelMiddle - labelStart - (textWidth / 2.0f) - kTextPadding;
                    }
                    else
                    {
                        // Whole text visible
                        // Centre in visible block
                        offset = (start - labelStart) / 2.0f;
                    }
                }
            }
        }
        else
        {
            if (labelFinish > finish)
            {
                if (labelStart < finish)
                {
                    // Just left edge of label is visible
                    if ((finish - labelStart) < (textWidth + (2.0f * kTextPadding)))
                    {
                        // Whole text not visible
                        // Left justify
                        offset = (textWidth / 2.0f) - labelMiddle + labelStart + kTextPadding;
                    }
                    else
                    {
                        // Whole text visible
                        // Centre in visible block
                        offset = (finish - labelFinish) / 2.0f;
                    }
                }
            }
        }
        _labelXOffsets[i] = [NSNumber numberWithFloat:offset];
    }
}

@end
