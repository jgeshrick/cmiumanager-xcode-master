// Copyright (c) Airsource Ltd. 2014.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#import "ASGraphStyle.h"

@implementation ASGraphStyle

@synthesize backgroundColor = _backgroundColor;
@synthesize highlightColor = _highlightColor;
@synthesize pathColors = _pathColors;
@synthesize axisColor = _axisColor;
@synthesize gridLineColor = _gridLineColor;
@synthesize labelColor = _labelColor;
@synthesize labelBackgroundColor = _labelBackgroundColor;
@synthesize labelBorderColor = _labelBorderColor;
@synthesize labelFont = _labelFont;
@synthesize pathStyle = _pathStyle;
@synthesize shouldOffsetLabels = _shouldOffsetLabels;
@synthesize pathShouldMask = _pathShouldMask;

static ASGraphStyle *sharedStyle = nil;
static dispatch_once_t onceToken;

+ (ASGraphStyle *)sharedStyle
{
    dispatch_once(&onceToken, ^{
        sharedStyle = [[self alloc] init];
    });
    return sharedStyle;
}

+ (void)updateStyle:(ASGraphStyle *)style
{
    [ASGraphStyle sharedStyle];
    if (style)
    {
        sharedStyle = [style init];
    }
}

- (id)init
{
    self = [super init];
    if (self)
    {
        // Configure default style
        _backgroundColor = [UIColor lightGrayColor];
        _highlightColor = [UIColor yellowColor];
        // Default is grey for 5 layers
        _pathColors = [NSMutableArray new];
        for (int i = 0; i < 5; i++)
        {
            [_pathColors addObject:[UIColor grayColor]];
        }
        _axisColor = [UIColor whiteColor];
        _gridLineColor = [UIColor whiteColor];
        _labelColor = [UIColor whiteColor];
        _labelBackgroundColor = [UIColor clearColor];
        _labelBorderColor = [UIColor clearColor];
        _labelFont = [UIFont fontWithName:@"HelveticaNeue" size:16.0f];
        _shouldOffsetLabels = false;
        _pathShouldMask = false;
    }
    return self;
}

- (void) setPathStyleOnContext:(CGContextRef)context forLayerIndex:(NSUInteger)layerIndex
{
    CGContextSetStrokeColorWithColor(context, [self pathColorForLayer:layerIndex].CGColor);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetAllowsAntialiasing(context, YES);
    CGContextSetLineWidth(context, 1.5f);
    CGContextSetShouldAntialias(context, YES);
    CGContextSetMiterLimit(context, 2.0f);
}

- (void) setAxisStyleOnContext:(CGContextRef)context
{
    CGContextSetStrokeColorWithColor(context, [self axisColor].CGColor);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetAllowsAntialiasing(context, YES);
    CGContextSetLineWidth(context, 1.0f);
    CGContextSetShouldAntialias(context, YES);
    CGContextSetMiterLimit(context, 2.0f);
}

- (void) setHorizontalGridlineStyleOnContext:(CGContextRef)context
{
    [self setGridlineStyleOnContext:context];
}

- (void) setVerticalGridlineStyleOnContext:(CGContextRef)context
{
    [self setGridlineStyleOnContext:context];
}

- (void) setGridlineStyleOnContext:(CGContextRef)context
{
    CGContextSetStrokeColorWithColor(context, [self gridLineColor].CGColor);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineWidth(context, 0.01f);
    CGContextSetShouldAntialias(context, NO);
    CGContextSetMiterLimit(context, 1.0f);
}

- (UIImage *) getBackgroundImageToMask:(CGContextRef)context layerIndex:(NSUInteger)layerIndex frame:(CGRect)frame
{
    // Stub for default style
    return nil;
}

- (UIImage *) getInvertedMarkerImageForLayerIndex:(NSUInteger)layerIndex maxSize:(CGSize)maxSize
{
    // Stub for default style
    return nil;
}

- (bool) shouldSetAreaUnderPathOnLayer:(NSUInteger)layerIndex
{
    return false;
}

- (void) setAreaUnderPath:(CGContextRef)context layerIndex:(NSUInteger)layerIndex frame:(CGRect)frame
{
    // Stub for default style
}

- (ASGraphPathStyle) getPathStyle
{
    return ASGraphPathStyleNormal;
}

- (UIColor *)pathColorForLayer:(NSUInteger)layerIndex
{
    UIColor * pathColor = [UIColor whiteColor];
    if (layerIndex < [_pathColors count])
    {
        pathColor = _pathColors[layerIndex];
    }
    return pathColor;
}

@end
