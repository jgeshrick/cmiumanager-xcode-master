// Copyright (c) Airsource Ltd. 2014.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 * Enum of path styles:
 *
 * Normal: sharp corners
 * Smooth: smooth corners
 * Ribbon: a clipped area of constant height
 */
typedef enum
{
    ASGraphPathStyleNormal,
    ASGraphPathStyleSmooth,
    ASGraphPathStyleRibbon
} ASGraphPathStyle;

/**
 * This is a base singleton class for styling. It provides a default style that can
 * be customised via cascading sub-classes.
 *
 * TODO: Currently only one style per application instance is supported at any one
 *       time (although this style can be updated).
 *       Styles should belong to plots or axes or the graphview.
 *
 * For examples see ExampleASGraphStyles
 */
@interface ASGraphStyle : NSObject
{
    UIColor *_backgroundColor;
    UIColor *_highlightColor;
    NSMutableArray *_pathColors;
    UIColor *_axisColor;
    UIColor *_gridLineColor;
    UIColor *_labelColor;
    UIColor *_labelBackgroundColor;
    UIColor *_labelBorderColor;
    UIFont  *_labelFont;
    ASGraphPathStyle _pathStyle;
}

/**
 * Access to the shared style singleton
 */
+ (ASGraphStyle *)sharedStyle;

/**
 * Update the shared style singleton. This should be called by any sub-classes
 * to use their style.
 */
+ (void)updateStyle:(ASGraphStyle *)style;

/**
 * The graph's background colour. Default is light grey.
 */
@property (nonatomic, retain) UIColor * backgroundColor;

/**
 * The graph's highlight colour. Default is yellow.
 */
@property (nonatomic, retain) UIColor * highlightColor;

/**
 * An array of the graph's path colours for plotting on a given layer.
 * Default is mid grey for the first 5 layers.
 *
 * layerIndex: The index of the layer
 */
@property (nonatomic, retain) NSMutableArray * pathColors;

/**
 * The graph's axis colour. Default is white.
 */
@property (nonatomic, retain) UIColor * axisColor;

/**
 * The graph's grid line colour. Default is white.
 */
@property (nonatomic, retain) UIColor * gridLineColor;

/**
 * The graph's axes labels' text colour. Default is white.
 */
@property (nonatomic, retain) UIColor * labelColor;

/**
 * The graph's axes labels' background colour. Default is transparent.
 */
@property (nonatomic, retain) UIColor * labelBackgroundColor;

/**
 * The graph's axes labels' border colour. Default is transparent.
 */
@property (nonatomic, retain) UIColor * labelBorderColor;

/**
 * The graph's axes labels' font. Default is Helvetica Neue, 16pt.
 */
@property (nonatomic, retain) UIFont * labelFont;

/**
 * Determines if the graph should offset the X axis labels. Default is false;
 */
@property (nonatomic, assign) bool shouldOffsetLabels;

/**
 * Sets the path style in the CG context for a given layer.
 *
 * context:    The CG context
 * layerIndex: The index of the layer
 */
- (void) setPathStyleOnContext:(CGContextRef)context forLayerIndex:(NSUInteger)layerIndex;

/**
 * Sets the axis style in the CG context.
 *
 * context: The CG context
 */
- (void) setAxisStyleOnContext:(CGContextRef)context;

/**
 * Sets the horizontal grid line style in the CG context.
 *
 * context: The CG context
 */
- (void) setHorizontalGridlineStyleOnContext:(CGContextRef)context;

/**
 * Sets the vertical grid line style in the CG context.
 *
 * context: The CG context
 */
- (void) setVerticalGridlineStyleOnContext:(CGContextRef)context;

/**
 * Determines if the path should behave as a mask for the area. Default is false.
 */
@property (nonatomic, assign) bool pathShouldMask;

/**
 Gets the background that will be masked.
 @todo is the context really needed?
 @param context    The CG context
 @param layerIndex The index of the layer
 @param frame      The frame to mask. Normally this will be the graph's frame.
 @returns           The background image
 */
- (UIImage *) getBackgroundImageToMask:(CGContextRef)context layerIndex:(NSUInteger)layerIndex frame:(CGRect)frame;

/**
 Gets the marker image to be used for a layer.
 This should be vertically inverted to allow for CGContext's inverted coordinate space.
 The marker image will be shown centered horizontally at the X value of the marker position
 with its bottom edge aligned with the Y value.
 
 @see ASGraphLayer#addPlot:withMarkers: For more information
 
 @param   layerIndex The index of the layer
 @param   maxSize    The maximum size the marker should be. Dimensions maybe be specified as UIViewNoIntrensicMetric meaning no preference
 @returns            The marker image
 */
- (UIImage *) getInvertedMarkerImageForLayerIndex:(NSUInteger)layerIndex maxSize:(CGSize)maxSize;

/**
 * Determines if the area should use clipping to fill the area under the path
 * for a given layer. Default is false.
 *
 * layerIndex: The index of the layer
 * Returns:    True if the style will set the area under the path.
 */
- (bool) shouldSetAreaUnderPathOnLayer:(NSUInteger)layerIndex;

/**
 * Uses clipping to fill the area under the path for a given layer. Default does nothing.
 *
 * context:    The CG context
 * layerIndex: The index of the layer
 * frame:      The frame to clip to. Normally this will be the graph's frame.
 */
- (void) setAreaUnderPath:(CGContextRef)context layerIndex:(NSUInteger)layerIndex frame:(CGRect)frame;

/**
 * The path style (normal, smooth or ribbon). Default is normal.
 */
@property (nonatomic, readonly) ASGraphPathStyle pathStyle;

/**
 * Retrieves the path color for a given layer
 *
 * layerIndex: The index of the layer
 * Returns:    The color of the path
 */
- (UIColor *)pathColorForLayer:(NSUInteger)layerIndex;

@end
