// Copyright (c) Airsource Ltd. 2014.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#import <UIKit/UIKit.h>

/**
 * Graphing Overview
 *
 * This provides simple plotting of multiple traces on a single view.
 * 
 * An ASGraphView has a single X axis and many layers each with their own Y axis.
 * Each layer can have many plots. Plots can be sharp or smooth lines, or shaded
 * or clipped areas. Axes can be blank, labelled or numbered and on any side.
 * Vertical highlighting, gridlines and axis ticks are also supported.
 *
 * An example view controller is provided for testing and demonstration.
 *
 * Graphs are styled by sub-classing the singleton ASGraphStyle. A cascading example 
 * is provided in ExampleASGraph
 *
 * Limitations:
 *
 * This graph is designed for a single draw (on for example ViewWillAppear) and therefore
 * is not optimised for regular redrawing if zooming is required for example.
 *
 * A plot is defined by a series of data points assuming linear integer X values (a
 * 'scatter' plot is not supported.
 *
 * TODO: Dynamic label widths on Y axes
 *       Behaviour of labels with more than one labelled y-axis.
 * TODO: Dynamic data loading (hard). Manage visual area and draw a little bit outside
 * TODO: Dynamic sizing and include scrollview. Tell it how wide each point should be
 *       and it does the rest. We can use invalidateIntrinsicContentSize to do this.
 */

// All includes for client convenience
#import "ASGraphLayer.h"
#import "ASGraphAxis.h"
#import "ASGraphStyle.h"

/**
 * Adds graphing functionality to a UIView. Manages a mutable dictionary of ASGraphLayers.
 *
 * Supports multiple layers, which in turn have multiple plots. Supports gridlines and 
 * vertical highlights.
 */

@interface ASGraphView : UIView

/**
 * This is the main X axis. If it is not set before drawing a default is created
 * with a maximum of the width of the view. (One pixel equals one division of X).
 */
@property (retain, nonatomic) ASGraphAxis *xAxis;

/**
 * Add layers to the graph view for plotting. Each layer has a Y axis that defines
 * style and the maximum Y value.
 * 
 * yAxis: The Y axis to attach to this layer
 * Returns: A layer index that you can use later to add plots to a particular layer
 */
- (NSUInteger) addLayerWithAxis:(ASGraphAxis *)yAxis;

/**
 * Removes the last layer.
 */
- (void) removeLastLayer;

/**
 * Removes all layers
 */
- (void) removeAllLayers;

/**
 * Removes all plots from every layer. Note the layers themselves are not removed
 */
- (void) clearPlots;

/**
 * Adds a plot of data points to a layer.
 *
 * data: An array of NSNumbers to plot. A data value of [NSNull null] is valid and will
 * result in a gap in the plot.
 *
 * layerKey: The key string of the layer to plot onto
 */
- (void) addPlot:(NSArray *)data toLayer:(NSUInteger)layerIndex;

- (void) pushPopPoint:(CGPoint) point toLayer:(NSUInteger)layerIndex toPlotIndex:(NSUInteger)plotIndex pop:(BOOL)pop;

-(void)replaceYAxis:(ASGraphAxis*)yAxis forLayer:(NSUInteger)layerIndex;

/**
 * Sets the width between datapoints in the x direction.
 * Defaults to 1.
 *
 * width: The Width in pixels of each x data point.
 */
- (void) setDataPointWidth:(CGFloat)width;

/**
 * Informs the graph view of the visible horizontal area
 * This is for the purpose of keeping the X-Axis displayed nicely
 *
 * offset: The pixel X offset of the content
 * width:  The pixel width of the visible area
 */
- (void) updateVisibleAreaWithXOffset:(CGFloat)offset width:(CGFloat)width;

@end
