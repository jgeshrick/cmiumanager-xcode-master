// Copyright (c) Airsource Ltd. 2014.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#import "ASGraphNumberedAxis.h"

@implementation ASGraphNumberedAxis

- (id) init
{
    NSAssert(0, @"Error: Must initialise with precision");
    return nil;
}
- (id) initWithCoder:(NSCoder *)aDecoder
{
    NSAssert(0, @"Error: Must initialise with precision");
    return nil;
}
- (id) initWithFrame:(CGRect)frame
{
    NSAssert(0, @"Error: Must initialise with precision");
    return nil;
}
- (id) initWithPrecision:(NSUInteger)precisionDP
                rectEdge:(CGRectEdge)rectEdge
                    from:(CGFloat)minimum to:(CGFloat)maximum
              ticksEvery:(CGFloat)ticksEvery
                  offset:(CGFloat)offset
{
    NSMutableArray * labels = [NSMutableArray new];
    NSMutableArray * locations = [NSMutableArray new];
    if (ticksEvery)
    {
        CGFloat i = 0.0f;
        for (float pos = minimum; pos <= maximum; pos += ticksEvery)
        {
            [labels addObject:[NSString stringWithFormat:@"%.*f", (int)precisionDP, pos-offset]];
                        [locations addObject:[NSNumber numberWithFloat:(i * ticksEvery)]];
            i++;
        }
    }
    self =  [super initWithLabels:labels locations:locations rectEdge:rectEdge minimum:minimum maximum:maximum];
  //  self.ticksEvery = ticksEvery;
    return self;
}

@end
