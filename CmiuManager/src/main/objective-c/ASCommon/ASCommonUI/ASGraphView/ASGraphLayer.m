// Copyright (c) Airsource Ltd. 2014.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#import "ASGraphLayer.h"
#import "ASGraphStyle.h"
#import "ASGraphAxis.h"
#import "ASGraphLabeledAxis.h"
#import "AirsourceChecks.h"

@interface ASGraphLayer()
{
    ASGraphAxis *_xAxis;
    NSMutableArray *_plots;
    NSUInteger _layerIndex;
    NSMutableArray *_segmentPoints;
}
@end

@implementation ASGraphLayer

@synthesize plots=_plots;

- (id) init
{
    NSAssert(0,@"@Error: must create layer with a Y axis");
    return nil;
}
- (id) initWithAxes:(ASGraphAxis *)xAxis yAxis:(ASGraphAxis *)yAxis layerIndex:(NSUInteger)layerIndex;
{
    self = [super init];
    if (self)
    {
        _xAxis = xAxis;
        self.yAxis = yAxis;
        _plots = [NSMutableArray new];
        _layerIndex = layerIndex;
    }
    return self;
}

- (void) clearPlots
{
    [_plots removeAllObjects];
}

- (NSUInteger) addPlot:(NSArray *)data
{
    [_plots addObject:data];
    return [_plots count] - 1;
}

-(void)pushPopPoint:(CGPoint)point toPlotIndex:(NSUInteger)index pop:(BOOL)pop
{
    NSData *plot = _plots[index];
    NSMutableArray *d = [plot mutableCopy];
    if(pop)
    {
        [d removeObjectAtIndex:0];
    }
    [d addObject:[NSValue valueWithCGPoint:point] ];
    [_plots replaceObjectAtIndex:index withObject:d];
    
}

- (void) plot:(CGContextRef)context graphRect:(CGRect)graphRect
{
    // Give the yaxis the same space to draw.
    _yAxis.graphRect = graphRect;
    
    // Plot the data and the markers
    for (NSArray *plot in _plots)
    {
        if (plot)
        {
            [self drawPath:context data:plot graphRect:graphRect];
        }
    }    
}

- (void) drawPath:(CGContextRef)context data:(NSArray *)data graphRect:(CGRect)graphRect
{
    //If the graphrect has zero size in either dimension it results in a bug in the CGImageCreation causing an image of the incorrect
    // size to be generated
    if (!data || [data count] == 0 || graphRect.size.height == 0 || graphRect.size.width == 0)
    {
        return;
    }
    
    // These variables are used during masking to hold the original context and rect passed in.
    CGContextRef cont1 = NULL;
    CGRect rect1;
    ASGraphStyle * style = [ASGraphStyle sharedStyle];
    
    [style setPathStyleOnContext:context forLayerIndex:_layerIndex];
    
    if (style.pathShouldMask)
    {
        //We make ourselves a fresh context for drawing the mask into
        // and we adjust the graphRect accordingly.
        cont1 = context;
        rect1 = graphRect;
        UIGraphicsBeginImageContextWithOptions(graphRect.size, NO, 0.0f);
        context = UIGraphicsGetCurrentContext();
        graphRect.origin = CGPointZero;
    }

    [style setPathStyleOnContext:context forLayerIndex:_layerIndex];
    
    if (style.pathShouldMask)
    {
        // For masking draw black on a white background
        CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
        CGContextFillRect(context, graphRect);
        CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    }
    
    // Draw lines in segments to support the gaps indicated by a data point of NSNull
    bool inSegment = false;
    CGPoint lastPoint;
    CGFloat segmentStartX = 0.0f;
    _segmentPoints = [NSMutableArray new];
    CGFloat xrange = _xAxis.maximum - _xAxis.minimum;
    CGFloat yrange = _yAxis.maximum - _yAxis.minimum;

    for(NSValue *v in data)
    {
        CGPoint pt = [v CGPointValue];

        
        if (isnan(pt.y))
        {
            if (inSegment)
            {
                // End of segment
                [self endSegment:context startX:segmentStartX endX:lastPoint.x graphRect:graphRect];
                inSegment = false;
            }
        }
        else
        {
            //NSValue *v = data[i];
            CGFloat x = graphRect.origin.x +  (pt.x - _xAxis.minimum) *graphRect.size.width / xrange;
            CGFloat y = graphRect.origin.y + graphRect.size.height -  (pt.y - _yAxis.minimum) *graphRect.size.height / yrange;
            CGPoint point = CGPointMake(x, y);
            if (!inSegment)
            {
                // Start of segment
                [_segmentPoints removeAllObjects];
                [self startSegment:context point:point graphRect:graphRect];
                [_segmentPoints addObject:[NSValue valueWithCGPoint:point]];
                segmentStartX = x;
                inSegment = true;
            }
            else
            {
                // Add point
                [self addPointToSegment:context point:point lastPoint:lastPoint];
                [_segmentPoints addObject:[NSValue valueWithCGPoint:point]];
            }
            lastPoint = point;
        }
    }
    if (inSegment)
    {
        // End of segment
        [self endSegment:context startX:segmentStartX endX:lastPoint.x graphRect:graphRect];
    }

    if (style.pathShouldMask)
    {
        // TODO - can this be done without this copy?
        CGImageRef maskImage = CGBitmapContextCreateImage(context);
        CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskImage),
                                            CGImageGetHeight(maskImage),
                                            CGImageGetBitsPerComponent(maskImage),
                                            CGImageGetBitsPerPixel(maskImage),
                                            CGImageGetBytesPerRow(maskImage),
                                            CGImageGetDataProvider(maskImage), NULL, false);
        
        // Fetch the background image
        UIImage * background = [style getBackgroundImageToMask:context
                                                                         layerIndex:_layerIndex
                                                                              frame:graphRect];
        // Mask it
        CGImageRef masked = CGImageCreateWithMask(background.CGImage, mask);
        
        UIGraphicsEndImageContext();
        
        // Recover the passed in context and rect
        context = cont1;
        graphRect = rect1;
        
        // Clear the background
        CGContextSetFillColorWithColor(context, style.backgroundColor.CGColor);
        CGContextFillRect(context, graphRect);
        
        // Draw it
        //TODO: line joins are cropped when the value reaches zero. might be worth scaling in by our line width or something like that here. (or better yet, higher up)
        CGContextSaveGState(context);
        CGFloat scale = -((graphRect.size.height + _xAxis.margin) / graphRect.size.height);
        scale = -1; //SD to JU: was the intention to scale stuff under the X axis here?
            CGContextScaleCTM(context, 1.0, scale);
            CGContextTranslateCTM(context, 0.0, - graphRect.origin.y - graphRect.size.height);
            CGContextDrawImage(context, graphRect, masked);
        CGContextRestoreGState(context);
        
        // Clean up
        CGImageRelease(maskImage);
        CGImageRelease(mask);
        CGImageRelease(masked);
    }
}

#pragma mark Segment drawing helpers

- (void) startSegment:(CGContextRef)context point:(CGPoint)point graphRect:(CGRect)graphRect
{
    CGContextMoveToPoint(context, point.x, point.y);
}

- (void) addPointToSegment:(CGContextRef)context point:(CGPoint)point lastPoint:(CGPoint)lastPoint
{
    if (!isnan(point.x) && ! isnan(point.y))
    {
        if ([ASGraphStyle sharedStyle].pathStyle == ASGraphPathStyleNormal)
        {
            CGContextAddLineToPoint(context, point.x, point.y);
        }
        else
        {
            CGPoint cp1 = CGPointMake((point.x + lastPoint.x) / 2.0f, lastPoint.y);
            CGPoint cp2 = CGPointMake((point.x + lastPoint.x) / 2.0f, point.y);
            CGContextAddCurveToPoint(context, cp1.x, cp1.y, cp2.x, cp2.y, point.x, point.y);
        }
    }
}

- (void) endSegment:(CGContextRef)context startX:(CGFloat)startX endX:(CGFloat)endX graphRect:(CGRect)graphRect
{
    ASGraphStyle * style = [ASGraphStyle sharedStyle];
    if (style.pathStyle == ASGraphPathStyleRibbon)
    {
        CGFloat ribbonHeight = 10.0f;
        // Need to run back through all points in the segment and add another point below it
        // to create the clipped area
        // TODO: Should really use +/- ribbonHeight / 2
        NSUInteger last = [_segmentPoints count] - 1;
        CGPoint firstPoint = [_segmentPoints[0] CGPointValue];
        CGPoint lastPoint = [_segmentPoints[last] CGPointValue];
        for (NSUInteger i = last; i > 0; i--)
        {
            CGPoint sp = [_segmentPoints[i] CGPointValue];
            CGPoint p = CGPointMake(sp.x, sp.y + ribbonHeight);
            [self addPointToSegment:context point:p lastPoint:lastPoint];
            lastPoint = p;
        }
        CGPoint p = CGPointMake(firstPoint.x, firstPoint.y + ribbonHeight);
        [self addPointToSegment:context point:p lastPoint:lastPoint];
        // Set the style
        [style setAreaUnderPath:context
                                          layerIndex:_layerIndex
                                               frame:CGRectMake(startX, graphRect.origin.y, endX,
                                                                graphRect.origin.y + graphRect.size.height)];
    }
    else if ([style shouldSetAreaUnderPathOnLayer:_layerIndex])
    {
        //We save off the path (clipping it deletes it)
        CGPathRef path = CGContextCopyPath(context);
        // Add a point to the end
        CGContextAddLineToPoint(context, endX, graphRect.origin.y + graphRect.size.height);
        // Add a point back to the start
        CGContextAddLineToPoint(context, startX, graphRect.origin.y + graphRect.size.height);
        // Set the style
        [style setAreaUnderPath:context
                                          layerIndex:_layerIndex
                                               frame:CGRectMake(startX, graphRect.origin.y, endX,
                                                                graphRect.origin.y + graphRect.size.height)];
        
        CGContextAddPath(context, path);
    }
    CGContextStrokePath(context);
}

@end
