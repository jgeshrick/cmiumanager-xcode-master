// Copyright (c) Airsource Ltd. 2014.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ASGraphAxis;

/**
 * A graph layer has an associated Y axis and manages a mutable array
 * of plots. A plot is an NSArray of NSNumbers. The view will give us
 * a layer key that can used for styling the layers differently.
 */
@interface ASGraphLayer : NSObject

@property (nonatomic, readonly, strong) NSArray * plots;
@property (nonatomic, strong) ASGraphAxis *yAxis;

/**
 * This is the designated initialiser.
 *
 * xAxis:      The X axis of the graph
 * yAxis:      The Y axis to associate with this layer
 * layerIndex: The index this layer
 */
- (id) initWithAxes:(ASGraphAxis *)xAxis yAxis:(ASGraphAxis *)yAxis layerIndex:(NSUInteger)layerIndex;

/**
 * Clears all plots
 */
- (void) clearPlots;

/**
 * Adds a data plot to this layer
 *
 * data: An array of NSValues for each point
 * return: index of plot
 */
- (NSUInteger) addPlot:(NSArray *)data;


/**
 * Adds a single data point to an existing plot and pops the first point
 *
 * point: CGPoint
 * index: index of plot
 */
-(void)pushPopPoint:(CGPoint)point toPlotIndex:(NSUInteger)index pop:(BOOL)pop;


/**
 * Draws all the plots
 *
 * context:   The CG context to draw into
 * graphRect: The bounding frame of the graph
 */
- (void) plot:(CGContextRef)context graphRect:(CGRect)graphRect;

@end
