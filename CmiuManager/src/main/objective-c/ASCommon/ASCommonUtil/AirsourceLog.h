// Copyright (c) Airsource Ltd. 2009-2010.
// 
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#ifndef Airsource_Log_LEVEL
#define Airsource_Log_LEVEL (!defined(NDEBUG) ? Airsource_Log_LEVEL_DEBUG : Airsource_Log_LEVEL_OOPS)
#endif

#ifndef CONFIG_Airsource_Log_Timing
#define CONFIG_Airsource_Log_Timing (Airsource_Log_LEVEL>Airsource_Log_LEVEL_OOPS)
#endif

#define Airsource_Log_LEVEL_DEBUG 3
#define Airsource_Log_LEVEL_INFO 2
#define Airsource_Log_LEVEL_OOPS 1
#define Airsource_Log_LEVEL_NONE 0

#if Airsource_Log_LEVEL >= Airsource_Log_LEVEL_DEBUG
	#define Airsource_Log_Debug(fmt,...) NSLog(@"DEBUG: %s %d " fmt, __PRETTY_FUNCTION__, __LINE__, ## __VA_ARGS__)
	#define Airsource_Log_Oops(fmt,...)  NSLog(@"OOPS:  %s %d " fmt, __PRETTY_FUNCTION__, __LINE__, ## __VA_ARGS__)
#elif Airsource_Log_LEVEL >= Airsource_Log_LEVEL_OOPS
	#define Airsource_Log_Debug(...) ((void)0)
	#define Airsource_Log_Oops(fmt,...)  NSLog(@"OOPS: " fmt, ## __VA_ARGS__)
#else
	#define Airsource_Log_Debug(...) ((void)0)
	#define Airsource_Log_Debug(...) ((void)0)
#endif

#if CONFIG_Airsource_Log_Timing
	#define Airsource_Log_Timing(...) Airsource_Log_Timing_(__PRETTY_FUNCTION__, __LINE__, @"" __VA_ARGS__)
#else
	#define Airsource_Log_Timing(...) ((void)0)
#endif

#ifndef ASDevLog
#define ASDevLog Airsource_Log_Debug
#endif

#ifndef ASLogNSError
#define ASLogNSError(e) Airsource_Log_Debug(@"%@ desc='%@' fr='%@' rs='%@' ro=%@", e, [e localizedDescription], [e localizedFailureReason], [e localizedRecoverySuggestion], [e localizedRecoveryOptions])
#endif

#if CONFIG_Airsource_Log_Timing && defined(__OBJC__)
@class NSString;

#ifdef __cplusplus
extern "C" {
#endif

void Airsource_Log_Timing_(const char * tag, int line, NSString * fmt, ...) __attribute__((format (__NSString__, 3, 4)));

#ifdef __cplusplus
};
#endif

#endif
