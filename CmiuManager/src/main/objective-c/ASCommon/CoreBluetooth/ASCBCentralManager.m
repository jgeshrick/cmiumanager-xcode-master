//
//  ASCBCentralManager.m
//  ASCommonTest
//
//  Created by John Earl on 17/09/2013.
//
//

#import "ASCBCentralManager.h"


@interface ASCBCentralManager()<CBCentralManagerDelegate>
{
    NSArray *_allPeripherals;
    NSArray *_discoveredPeripherals;
}

@property (nonatomic,strong) CBCentralManager *central;

@property (nonatomic,strong) NSMutableArray *peripherals;

@end

static dispatch_queue_t _bluetoothQueue;

@implementation ASCBCentralManager
@synthesize central=_central;
@synthesize scanning=_scanning;
@synthesize serviceToScanFor=_serviceToScanFor;
@synthesize peripherals=_peripherals;
@synthesize peripheralSetupBlock=_peripheralSetupBlock;

static ASCBCentralManager *_sharedCentral;

+(ASCBCentralManager*)sharedCentral
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedCentral = [ASCBCentralManager new];
    });
    return _sharedCentral;
}

-(id)init_ASCBCentralManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _bluetoothQueue = dispatch_queue_create("ASCBCentralManager", DISPATCH_QUEUE_SERIAL);
    });
    self.peripherals = [NSMutableArray array];
    self.central = [[CBCentralManager alloc] initWithDelegate:self queue:_bluetoothQueue];
    return self;
}

-(id)init
{
    self = [super init];
    return [self init_ASCBCentralManager];
}

+(dispatch_queue_t)bluetoothQueue
{
    return _bluetoothQueue;
}

#pragma mark Helpers


-(void)updatePeripherals
{
    // Should be called only on _bluetoothQueue
    NSMutableArray *a = [NSMutableArray array];
    for (ASCBPeripheral *peripheral in self.peripherals)
    {
        if ([peripheral recentlyDiscovered])
        {
            [a addObject:peripheral];
        }
    }
    NSArray *allPeripherals = [self.peripherals copy];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self willChangeValueForKey:@"allPeripherals"];
        [self willChangeValueForKey:@"discoveredPeripherals"];
        _discoveredPeripherals = a;
        _allPeripherals = allPeripherals;
        [self didChangeValueForKey:@"discoveredPeripherals"];
        [self didChangeValueForKey:@"allPeripherals"];
    });
}

#pragma mark External interface

-(NSArray*)allPeripherals
{
    return _allPeripherals;
}

-(NSArray*)discoveredPeripherals
{
    return _discoveredPeripherals;
}


-(void)setScanning:(bool)scanning
{
    assert([NSThread isMainThread]);
    _scanning = scanning;
    
    dispatch_async(_bluetoothQueue, ^{
        if (self.central.state < CBCentralManagerStatePoweredOn)
        {
            return;
        }
        if (scanning)
        {
            
            NSDictionary *wantDuplicates = nil;
#if 1
            wantDuplicates = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:CBCentralManagerScanOptionAllowDuplicatesKey];
#endif
            ASCB_Log_Debug(@"%@ scanForPeripherals", self.central);
            for (ASCBPeripheral *peripheral in self.peripherals)
            {
                [peripheral resetDiscovery];
            }
            [self updatePeripherals];
            
            CBUUID *uuid = self.serviceToScanFor;
            [self.central scanForPeripheralsWithServices:uuid?[NSArray arrayWithObject:uuid]:nil options:wantDuplicates];
        }
        else
        {
            ASCB_Log_Debug(@"%@ stopScan", self.central);
            [self.central stopScan];
        }
    });
}

-(BOOL)needsToScan
{
    assert([NSThread isMainThread]);
    BOOL anyConnected = NO;
    for (ASCBPeripheral *p in _allPeripherals)
    {
        if (p.isConnectedOrConnecting)
        {
            anyConnected = YES;
            break;
        }
    }
    return !anyConnected;
}

-(ASCBPeripheral*)peripheralForIdentifierString:(NSString*)identifierString
{    
    for(ASCBPeripheral *periph in _discoveredPeripherals)
    {
        if([periph.peripheral.identifier.UUIDString isEqualToString:identifierString])
        {
            return periph;
        }
    }
    return nil;

}

#pragma mark Creating and managing peripheral owners

/*
 * Finds the container object that owns the peripheral, or creates and
 * registers a new one.
 */
-(ASCBPeripheral*)ownerForPeripheral:(CBPeripheral*)peripheral
{
    ASCBPeripheral *reusePeripheral = nil;
    for (ASCBPeripheral *p in self.peripherals)
    {
        if ([p shouldOwnPeripheral:peripheral])
        {
            reusePeripheral = p;
            break;
        }
    }
    
    ASCBPeripheral *newPeripheral = nil;
    if (self.peripheralSetupBlock)
    {
        ASCBPeripheral *p = self.peripheralSetupBlock(self, reusePeripheral, peripheral);
        if (p != reusePeripheral)
        {
            // TODO: probably need to remove the old peripheral object from the peripheral list?
            reusePeripheral = nil;
            newPeripheral = p;
        }
    }
    else
    {
        if (reusePeripheral)
        {
            [reusePeripheral assumeOwnership:peripheral];
        }
        else
        {
            newPeripheral = [[ASCBPeripheral alloc] initWithPeripheral:peripheral centralManager:self];
        }
    }
    
    if (newPeripheral)
    {
        [self.peripherals addObject:newPeripheral];
        [self updatePeripherals];
    }
    
    return newPeripheral?:reusePeripheral;
}

-(void)allPeripheralsDisconnected
{
    for (ASCBPeripheral *p in self.peripherals)
    {
        [p disconnectedDuePowerOff];
    }
    [self updatePeripherals];
}

-(void)invalidatedPeripheralReferences
{
    for (ASCBPeripheral *p in self.peripherals)
    {
        [p invalidated];
    }
    [self.peripherals removeAllObjects];
    [self updatePeripherals];
}

-(void)givePeripheralsOwners:(NSArray*)peripherals
{
    for (CBPeripheral *p in peripherals)
    {
        [self ownerForPeripheral:p];
    }
}


#pragma mark CBCentralManager

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    CBCentralManagerState state = central.state;
    
    ASCB_Log_Debug(@"state -> %d", central.state);
    if (state < CBCentralManagerStatePoweredOn)
    {
        [self allPeripheralsDisconnected];
    }
    
    if (state < CBCentralManagerStatePoweredOff)
    {
        [self invalidatedPeripheralReferences];
    }
    
    if (state == CBCentralManagerStatePoweredOn)
    {
        // Restart scanning if we are meant to be scanning when powered on
        dispatch_async(dispatch_get_main_queue(), ^{
            if (_scanning)
            {
                [self setScanning:YES];
            }
        });
    }
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals
{
    ASCB_Log_Debug(@"%@ %@", central, peripherals);
    [self givePeripheralsOwners:peripherals];
}

- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals
{
    ASCB_Log_Debug(@"%@ %@", central, peripherals);
    [self givePeripheralsOwners:peripherals];
}

-(void)peripheralResetRecentDiscovery
{
    dispatch_async(_bluetoothQueue,
    ^{
        [self updatePeripherals];
    });
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    ASCB_Log_Debug(@"%@ %@ %@ %@", central, peripheral, advertisementData, RSSI);
    ASCBPeripheral *p = [self ownerForPeripheral:peripheral];
    [p setAdvertisementData:advertisementData];
    p.advertisementRSSI = RSSI;
    [p discovered];
    [self updatePeripherals];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    ASCB_Log_Debug(@"%@ %@", central, peripheral);
    ASCBPeripheral *p = [self ownerForPeripheral:peripheral];
    [p connected];
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    ASCB_Log_Debug(@"%@ %@ %@", central, peripheral, error);
    ASCBPeripheral *p = [self ownerForPeripheral:peripheral];
    [p failedToConnectWithError:error];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    ASCB_Log_Debug(@"%@ %@ %@", central, peripheral, error);
    ASCBPeripheral *p = [self ownerForPeripheral:peripheral];
    [p disconnectedWithError:error];
}

@end
