//
//  ASCBCentralManager.h
//  ASCommonTest
//
//  Created by John Earl on 17/09/2013.
//
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "ASCBPeripheral.h"

typedef ASCBPeripheral* (^ASCBPeripheralSetupBlock)(ASCBCentralManager *manager, ASCBPeripheral *objectForReuse, CBPeripheral *peripheral);

@interface ASCBCentralManager : NSObject

+(ASCBCentralManager*)sharedCentral;
+(dispatch_queue_t)bluetoothQueue;

-(CBCentralManager*)central;

/*
 * Use KVO on peripherals or discoveredPeripherals keys to notice changes
 * in these lists while scanning.
 */
-(NSArray*)allPeripherals;
-(NSArray*)discoveredPeripherals;

/*
 * Returns YES if we don't have a currently connected peripheral
 */
-(BOOL)needsToScan;


@property (nonatomic,assign) bool scanning;



/*
 * If non-nil, UUID of a service to scan for in advertising packets
 */
@property (nonatomic,strong) CBUUID *serviceToScanFor;

/*
 * If set, this block should return a configured peripheral object (ASCBPeripheral or a subclass)
 * If not set, an ASCBPeripheral will be set up with default options.
 *
 * The PeripheralSetupBlock will be called on the bluetooth dispatch queue.
 */
@property (nonatomic,strong) ASCBPeripheralSetupBlock peripheralSetupBlock;

-(void)peripheralResetRecentDiscovery;

-(ASCBPeripheral*)peripheralForIdentifierString:(NSString*)identifierString;

@end
