//
//  ASCBPeripheral.m
//  ASCommonTest
//
//  Created by John Earl on 17/09/2013.
//
//

#import "ASCBPeripheral.h"

#import "ASCBCentralManager.h"
#import <mach/mach_time.h>
#import "AirsourceLog.h"

#define ASCB_PERIODICSTATS_PERIOD 4

NSTimeInterval ASCBMachIntervalToTimeInterval(uint64_t startTime)
{
    uint64_t now = mach_absolute_time();
    static mach_timebase_info_data_t    sTimebaseInfo;
    
    if ( sTimebaseInfo.denom == 0 ) {
        (void) mach_timebase_info(&sTimebaseInfo);
    }
    
    // Do the maths. We hope that the multiplication doesn't
    // overflow; the price you pay for working in fixed point.
    
    uint64_t elapsedNano = (now - startTime) * sTimebaseInfo.numer / sTimebaseInfo.denom;
    NSTimeInterval interval = elapsedNano/1000.0/1000.0/1000.0;
    
    return interval;
}

NSString *ASCBStringFromCBUUID(CBUUID *uuid)
{
    return uuid.UUIDString;
}

NSString *ASCBStringFromCBCharacteristicProperties(CBCharacteristicProperties props)
{
    NSMutableArray *propStrs = [NSMutableArray array];
    if (props & CBCharacteristicPropertyBroadcast) [propStrs addObject:@"Broadcast"];
    if (props & CBCharacteristicPropertyRead) [propStrs addObject:@"Read"];
    if (props & CBCharacteristicPropertyWriteWithoutResponse) [propStrs addObject:@"WriteWithoutResponse"];
    if (props & CBCharacteristicPropertyWrite) [propStrs addObject:@"Write"];
    if (props & CBCharacteristicPropertyNotify) [propStrs addObject:@"Notify"];
    if (props & CBCharacteristicPropertyIndicate) [propStrs addObject:@"Indicate"];
    if (props & CBCharacteristicPropertyAuthenticatedSignedWrites) [propStrs addObject:@"AuthenticatedSignedWrites"];
    if (props & CBCharacteristicPropertyExtendedProperties) [propStrs addObject:@"ExtendedProperties"];
    if (props & CBCharacteristicPropertyNotifyEncryptionRequired) [propStrs addObject:@"NotifyEncryptionRequired"];
    if (props & CBCharacteristicPropertyIndicateEncryptionRequired) [propStrs addObject:@"IndicateEncryptionRequired"];
    return [propStrs componentsJoinedByString:@","];
    
}

NSString * const ASCBPeripheralStateDidChangeNotification = @"ASCBPeripheralStateDidChangeNotification";

NSString *ASCBStringForPeripheralState(ASCBPeripheralState state)
{
    switch (state)
    {
        case ASCBPeripheralState_Connected:
            return @"Connected";
        case ASCBPeripheralState_Disconnecting:
            return @"Disconnecting";
        case ASCBPeripheralState_Connecting:
            return @"Connecting";
        case ASCBPeripheralState_Disconnected:
            return @"Disconnected";
        case ASCBPeripheralState_Discovery:
            return @"Discovery";
        case ASCBPeripheralState_DiscoverCharacteristics:
            return @"DiscoverCharacteristics";
        case ASCBPeripheralState_Pairing:
            return @"Pairing";
    }
}

@interface ASCBPeripheral()<CBPeripheralDelegate>
{
    bool _recentlyDiscovered;
    NSMutableArray *_pendingServicesForDiscovery;
    
    NSMutableArray *_pendingCharacteristicsForNotify;
    
    NSMutableDictionary *_discoveredCharacteristics;
    
    NSTimer *_advertTimeoutTimer;
    NSTimer *_connectTimeoutTimer;
    
    uint64_t _statsStartTime;
    uint64_t _statsPacketsIn;
    uint64_t _statsPacketsOut;
    
    uint64_t _stateStartTime;
    
    NSMutableDictionary *_handlers;
    NSMutableDictionary *_writeHandlers;
    NSDictionary *_advertisementData;
}

@property (nonatomic,strong) CBPeripheral *peripheral;
@property (nonatomic,weak) ASCBCentralManager *centralManager;
@property (nonatomic,assign) ASCBPeripheralState state;

@end

@implementation ASCBPeripheral
@synthesize peripheral=_peripheral;
@synthesize centralManager=_centralManager;
@synthesize advertisementRSSI=_advertisementRSSI;
@synthesize state=_state;
@synthesize characteristicKeyMap=_characteristicKeyMap;
@synthesize servicesToDiscover=_servicesToDiscover;
@synthesize alwaysNotifyCharacteristicKeys=_alwaysNotifyCharacteristicKeys;


-(id)initWithPeripheral:(CBPeripheral*)peripheral centralManager:(ASCBCentralManager*)centralManager
{
    self = [super init];
    if (self)
    {
        [self resetInternalState];
        self.centralManager = centralManager;
        self.peripheral = peripheral;
        peripheral.delegate = self;
        _discoveredCharacteristics = [NSMutableDictionary dictionary];
        _handlers = [NSMutableDictionary dictionary];
        _writeHandlers = [NSMutableDictionary dictionary];
    }
    return self;
}

-(void)dealloc
{
    
}

-(void)statsStart
{
    @synchronized(self)
    {
        _statsStartTime = mach_absolute_time();
        _statsPacketsIn = 0;
        _statsPacketsOut = 0;
    }
}

-(NSDictionary*)statsDictionary
{
    NSDictionary *d = nil;
    @synchronized(self)
    {
        NSTimeInterval interval = ASCBMachIntervalToTimeInterval(_statsStartTime);
    
        uint64_t actualPacketsOut = _statsPacketsOut;
    
        d = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:interval],@"interval",[NSNumber numberWithUnsignedLongLong:_statsPacketsIn],@"in",[NSNumber numberWithUnsignedLongLong:actualPacketsOut],@"out",[NSNumber numberWithDouble:(_statsPacketsIn/interval)],@"packetsPerSecond", nil];
    }
    return d;
}


-(void)resetInternalState
{
    @synchronized(self)
    {
        _recentlyDiscovered = NO;
        
        _pendingServicesForDiscovery = nil;
        _pendingCharacteristicsForNotify = nil;
        _discoveredCharacteristics = [NSMutableDictionary dictionary];
        
        [_connectTimeoutTimer invalidate];
        _connectTimeoutTimer = nil;
    }
    
}

-(void)setAdvertisementData:(NSDictionary *)advertisementData
{
    @synchronized(self) {
        _advertisementData = advertisementData;
        
        // Got an advert, so reset the advert time out (if any).
        if(_advertTimeout)
        {
            [_advertTimeoutTimer invalidate];
            _advertTimeoutTimer = [NSTimer timerWithTimeInterval:_advertTimeout target:self selector:@selector(advertTimeout:) userInfo:nil repeats:NO];
            // Background thread won't necessarily be running its runloop, so need to put this on main
            [[NSRunLoop mainRunLoop] addTimer:_advertTimeoutTimer forMode:NSRunLoopCommonModes];
        }
    }
}

-(void)advertTimeout:(NSTimer*)timer
{
    Airsource_Log_Debug(@"Resetting discovery for %@", self);
    [self resetDiscovery];
}

-(void)postStateChangeNotification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ASCBPeripheralStateDidChangeNotification" object:self];
    });
}


// Period of stats reports to console, in seconds
-(void)_periodicStatisticsReport
{
    assert([NSThread isMainThread]);
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_periodicStatisticsReport) object:nil];
    NSLog(@"%@",[self statsDictionary]);
    [self statsStart];
    [self performSelector:@selector(_periodicStatisticsReport) withObject:nil afterDelay:ASCB_PERIODICSTATS_PERIOD];
    
}

-(void)setState:(ASCBPeripheralState)state
{
    @synchronized(self)
    {
        if (_state != state)
        {
            Airsource_Log_Debug(@"%@ -> %@", ASCBStringForPeripheralState(_state), ASCBStringForPeripheralState(state));
            _state = state;
            ASCBCentralManager *centralManager = self.centralManager;
            switch (_state)
            {
                case ASCBPeripheralState_Disconnecting:
                    Airsource_Log_Debug(@"Cancelling BT connection: %@", self.peripheral);
                    [centralManager.central cancelPeripheralConnection:self.peripheral];
                    break;
                case ASCBPeripheralState_Connecting:
                    [centralManager.central connectPeripheral:self.peripheral options:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:1], CBConnectPeripheralOptionNotifyOnDisconnectionKey, nil]];
                    break;
                case ASCBPeripheralState_Disconnected:
                {
#if CONFIG_CHD_BLUETOOTH_STATS
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_periodicStatisticsReport) object:nil];
                    });
#endif
                    [self resetInternalState];
                    break;
                }
                case ASCBPeripheralState_Discovery:
                    [self.peripheral discoverServices:nil];
                    break;
                case ASCBPeripheralState_Pairing:
                {
                    NSMutableArray *notifyCharacteristics = [NSMutableArray array];
                    for (NSString *k in self.alwaysNotifyCharacteristicKeys)
                    {
                        [notifyCharacteristics addObject:[self characteristicForKey:k]];
                    }
                    _pendingCharacteristicsForNotify = notifyCharacteristics;
                    for (CBCharacteristic *c in notifyCharacteristics)
                    {
                        [self.peripheral setNotifyValue:YES forCharacteristic:c];
                    }
                    
                    break;
                }
                case ASCBPeripheralState_Connected:
                {
                    [_connectTimeoutTimer invalidate];
                    _connectTimeoutTimer = nil;
                    
#if CONFIG_ASCB_BLUETOOTH_STATS
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self _periodicStatisticsReport];
                    });
#endif
                    break;
                }
                case ASCBPeripheralState_DiscoverCharacteristics:
                    
                    for (CBService *service in [_pendingServicesForDiscovery copy])
                    {
                        [self.peripheral discoverCharacteristics:nil forService:service];
                    }
                    break;
            }
            
            [self postStateChangeNotification];
        }
        
    }
}

-(ASCBPeripheralState)state
{
    ASCBPeripheralState s;
    @synchronized(self)
    {
        s = _state;
    }
    return s;
}


#pragma mark Messages from Central Manager


-(bool)recentlyDiscovered
{
    @synchronized(self)
    {
        return _recentlyDiscovered;
    }
}

-(void)resetDiscovery
{
    @synchronized(self)
    {
        _recentlyDiscovered = NO;
        [[ASCBCentralManager sharedCentral] peripheralResetRecentDiscovery];
    }
}

-(void)invalidated
{
    @synchronized(self)
    {
        self.state = ASCBPeripheralState_Disconnected;
        self.peripheral = nil;
    }
}


-(void)disconnectedDuePowerOff
{
    @synchronized(self)
    {
        self.state = ASCBPeripheralState_Disconnected;
    }
}


-(bool)shouldOwnPeripheral:(CBPeripheral*)peripheral
{
    if (peripheral == self.peripheral)
    {
        return YES;
    }
    return NO;
}

-(void)assumeOwnership:(CBPeripheral*)peripheral
{
    assert(peripheral == self.peripheral);
}


-(void)discovered
{
    @synchronized(self)
    {
        _recentlyDiscovered = YES;
    }
}

-(void)connected
{
    @synchronized(self)
    {
        if (self.state == ASCBPeripheralState_Connecting)
        {
            self.state = ASCBPeripheralState_Discovery;
        }
    }
    
}

-(bool)isConnectable
{
    @synchronized(self) {
        NSNumber* connectable = [_advertisementData objectForKey:CBAdvertisementDataIsConnectable];
        return connectable.intValue != 0;
    }
}

-(NSString*)localName
{
    @synchronized(self) {
        NSString *name =  [_advertisementData objectForKey:CBAdvertisementDataLocalNameKey];
        return name;
    }
}

-(void)failedToConnectWithError:(NSError*)error
{
    @synchronized(self)
    {
        self.state = ASCBPeripheralState_Disconnected;
    }
}

-(void)disconnectedWithError:(NSError*)error
{
    @synchronized(self)
    {
        self.state = ASCBPeripheralState_Disconnected;
    }
}

#pragma mark connection timeout

-(void)connectTimeout:(id)aSender
{
    assert([NSThread isMainThread]);
    
    @synchronized(self)
    {
        [_connectTimeoutTimer invalidate];
        _connectTimeoutTimer = nil;
        
        if(_state != ASCBPeripheralState_Connected &&
               _state != ASCBPeripheralState_Disconnected)
        {
            self.state = ASCBPeripheralState_Disconnecting;
        }
    }
}

#pragma mark External interface


-(bool)isConnectedOrConnecting
{
    return (self.state > ASCBPeripheralState_Disconnecting);
}


-(void)connectWithTimeout:(NSTimeInterval)timeout
{
    assert([NSThread isMainThread]);
    @synchronized(self)
    {
        [_connectTimeoutTimer invalidate];
        _connectTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:timeout target:self selector:@selector(connectTimeout:) userInfo:nil repeats:NO];
        [self connect];
    }
}

-(void)connect
{
    @synchronized(self)
    {
        [_advertTimeoutTimer invalidate];
        if (self.state <= ASCBPeripheralState_Connecting)
        {
            self.state = ASCBPeripheralState_Connecting;
        }
    }
}
-(void)cancelConnection
{
    @synchronized(self)
    {
        if (_state == ASCBPeripheralState_Connecting)
        {
            self.state = ASCBPeripheralState_Disconnected;
        }
        else if (_state > ASCBPeripheralState_Disconnected)
        {
            self.state = ASCBPeripheralState_Disconnecting;
        }
    }
}

-(CBCharacteristic*)characteristicForKey:(NSString*)key
{
    @synchronized(self)
    {
        return [_discoveredCharacteristics objectForKey:key];
    }
}


-(void)setHandler:(ASCBPeripheralCharacteristicValueHandler)handler forKey:(NSString*)key
{
    @synchronized(self)
    {
        if (!handler)
        {
            [_handlers removeObjectForKey:key];
        }
        else
        {
            [_handlers setObject:handler forKey:key];
        }
    }
}

-(void)setWriteHandler:(ASCBPeripheralCharacteristicWriteHandler)handler forKey:(NSString*)key
{
    @synchronized(self)
    {
        if (!handler)
        {
            [_writeHandlers removeObjectForKey:key];
        }
        else
        {
            [_writeHandlers setObject:handler forKey:key];
        }
    }
}

-(void)readValueForKey:(NSString*)key
{
    @synchronized(self)
    {
        if (self.state < ASCBPeripheralState_Pairing)
        {
            ASCB_Log_Debug(@"Ignoring characteristic read as not yet connected");
            return;
        }
        
        CBCharacteristic *characteristic = [self characteristicForKey:key];
        if(characteristic)
        {
            [self.peripheral readValueForCharacteristic:characteristic];
        }
    }
}

-(BOOL)isReadableWriteable
{
    @synchronized(self)
    {
        return self.state == ASCBPeripheralState_Connected;
    }
}

-(void)writeValue:(NSData*)value forKey:(NSString*)key
{
    @synchronized(self)
    {
        if (self.state < ASCBPeripheralState_Pairing)
        {
            ASCB_Log_Debug(@"Ignoring characteristic write as not yet connected");
            return;
        }
        
        CBCharacteristic *characteristic = [self characteristicForKey:key];
        if(characteristic)
        {
            CBCharacteristicWriteType writeType = CBCharacteristicWriteWithoutResponse;
            ASCBPeripheralCharacteristicWriteHandler writeHandler = [_writeHandlers objectForKey:key];
            if(writeHandler)
            {
                
                writeType = CBCharacteristicWriteWithResponse;
            }
            [self.peripheral writeValue:value forCharacteristic:characteristic type:writeType];
        }
    }
}


#pragma mark CBPeripheralDelegate

- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral
{
    Airsource_Log_Debug(@"%@", peripheral);
}

- (void)peripheralDidInvalidateServices:(CBPeripheral *)peripheral
{
    ASCB_Log_Debug(@"%@", peripheral);
}


-(void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(NSError *)error
{
    ASCB_Log_Debug(@"%@ %@", peripheral, error);
    [self setRssi:RSSI];
}

-(void)setRssi:(NSNumber *)rssi
{
    _rssi = rssi;
}

-(void)refreshRSSI
{
    [_peripheral readRSSI];
}



- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error
{
    ASCB_Log_Debug(@"%@ %@", peripheral, error);
}


- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    NSArray *services = peripheral.services;
    ASCB_Log_Debug(@"%@ %@: %@", peripheral, error, services);
    
    @synchronized(self)
    {
        if (self.state != ASCBPeripheralState_Discovery)
        {
            return;
        }
        
        if ([services count])
        {
            
            if ([self.servicesToDiscover count])
            {
                NSMutableArray *pendingServices = [NSMutableArray array];
                for (CBService *service in services)
                {
                    if ([self.servicesToDiscover containsObject:ASCBStringFromCBUUID(service.UUID)] )
                    {
                        [pendingServices addObject:service];
                    }
                }
                _pendingServicesForDiscovery = pendingServices;
            }
            else
            {
                _pendingServicesForDiscovery = [services mutableCopy];
            }
            
            self.state = ASCBPeripheralState_DiscoverCharacteristics;
        }
        else
        {
            self.state = ASCBPeripheralState_Connected;
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error
{
    ASCB_Log_Debug(@"%@ %@ %@", peripheral, service, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{

    NSArray *characteristics = service.characteristics;
    ASCB_Log_Debug(@"%@ %@ %@: %@", peripheral, service, error, characteristics);
    NSLog(@"S -> %@ (%d)", ASCBStringFromCBUUID(service.UUID), service.isPrimary ? 1 : 0);
    @synchronized(self)
    {
        // Do nothing if we are not connected
        if (self.state <= ASCBPeripheralState_Connecting)
        {
            ASCB_Log_Debug(@"Ignoring characteristic as disconnected or disconnecting");
            return;
        }
        
        for (CBCharacteristic *characteristic in characteristics)
        {
            CBCharacteristicProperties props = characteristic.properties;
            CBUUID *uuid = characteristic.UUID;
            NSString *uuidString = ASCBStringFromCBUUID(uuid);
            NSLog(@"C -> %@ : %@", uuidString, ASCBStringFromCBCharacteristicProperties(props));
            
            NSString *k = [self.characteristicKeyMap objectForKey:uuidString];
            if (k)
            {
                [_discoveredCharacteristics setObject:characteristic forKey:k];
            }
            
        }
        
        [_pendingServicesForDiscovery removeObject:service];
        
        // No state changes if we are not in discover characteristics
        if (self.state != ASCBPeripheralState_DiscoverCharacteristics)
        {
            return;
        }
        
        if (![_pendingServicesForDiscovery count])
        {
            if ([_discoveredCharacteristics count] == [self.characteristicKeyMap count])
            {
                if ([self.alwaysNotifyCharacteristicKeys count])
                {
                    self.state = ASCBPeripheralState_Pairing;
                }
                else
                {
                    self.state = ASCBPeripheralState_Connected;
                }
            }
            else
            {
                ASCB_Log_Debug(@"Did not receive all expected characteristics - disconnecting");
                [self cancelConnection];
            }
        }
    }
    
}



- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    ASCB_Log_Debug(@"%@ %@ %@", peripheral, characteristic, error);
    @synchronized(self)
    {
        if (self.state < ASCBPeripheralState_Connecting)
        {
            ASCB_Log_Debug(@"Ignoring characteristic update as disconnected or disconnecting");
            return;
        }
        if (!error)
        {
            _statsPacketsIn++;
        }
        NSString *key = [self.characteristicKeyMap objectForKey:ASCBStringFromCBUUID(characteristic.UUID)];
        if (key)
        {
            ASCBPeripheralCharacteristicValueHandler handler = [_handlers objectForKey:key];
            if (handler)
            {
                handler(self, key, error?nil:characteristic.value, error);
            }
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    NSString *key = [self.characteristicKeyMap objectForKey:ASCBStringFromCBUUID(characteristic.UUID)];
    if (key)
    {
        ASCBPeripheralCharacteristicWriteHandler handler = [_writeHandlers objectForKey:key];
        if (handler)
        {
            handler(self, key, error);
        }
    }}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (peripheral.state != CBPeripheralStateConnected)
    {
        ASCB_Log_Debug(@"Received notification state update while peripheral not connected");
        return;
    }
    ASCB_Log_Debug(@"%@ %@ %@", peripheral, characteristic, error);
    @synchronized(self)
    {
        if (self.state < ASCBPeripheralState_Connecting)
        {
            ASCB_Log_Debug(@"Ignoring notification as disconnected or disconnecting");
            return;
        }
        
        if (error && [_pendingCharacteristicsForNotify containsObject:characteristic] && self.state == ASCBPeripheralState_Pairing)
        {
            ASCB_Log_Debug(@"Error setting notify on a characteristic during pairing state - disconnecting");
            [self cancelConnection];
            return;
        }
        
        
        [_pendingCharacteristicsForNotify removeObject:characteristic];
        
        if (![_pendingCharacteristicsForNotify count] && self.state == ASCBPeripheralState_Pairing)
        {
            self.state = ASCBPeripheralState_Connected;
        }
    }
    
}



@end
