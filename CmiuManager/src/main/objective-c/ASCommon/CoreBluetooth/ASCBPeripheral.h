//
//  ASCBPeripheral.h
//  ASCommonTest
//
//  Created by John Earl on 17/09/2013.
//
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#if defined(CONFIG_ASCB_VERBOSE_LOG) && CONFIG_ASCB_VERBOSE_LOG
#define ASCB_Log_Mock(fmt,...) NSLog(@"[BT] " fmt, ## __VA_ARGS__)
#define ASCB_Log_Debug(fmt,...) NSLog(@"[BT] %s %d " fmt, __PRETTY_FUNCTION__, __LINE__, ## __VA_ARGS__)
#else
#define ASCB_Log_Mock(...)
#define ASCB_Log_Debug(...)
#endif

#ifndef CONFIG_ASCB_BLUETOOTH_STATS
#define CONFIG_ASCB_BLUETOOTH_STATS 0
#endif

@class ASCBCentralManager;

NSString * const ASCBPeripheralStateDidChangeNotification;
NSString * const ASCBPeripheralButtonPressNotification;

typedef enum
{
    ASCBPeripheralState_Disconnected,
    ASCBPeripheralState_Disconnecting,
    ASCBPeripheralState_Connecting,
    ASCBPeripheralState_Discovery,
    ASCBPeripheralState_DiscoverCharacteristics,
    ASCBPeripheralState_Pairing,
    ASCBPeripheralState_Connected
} ASCBPeripheralState;


NSString *ASCBStringFromCBUUID(CBUUID *uuid);
NSString *ASCBStringFromCBCharacteristicProperties(CBCharacteristicProperties props);
NSString *ASCBStringForPeripheralState(ASCBPeripheralState state);

@class ASCBPeripheral;

typedef void (^ASCBPeripheralCharacteristicValueHandler)(ASCBPeripheral *peripheral, NSString *key, NSData *value, NSError *error);
typedef void (^ASCBPeripheralCharacteristicWriteHandler)(ASCBPeripheral *peripheral, NSString *key, NSError *error);

@interface ASCBPeripheral : NSObject

-(id)initWithPeripheral:(CBPeripheral*)peripheral centralManager:(ASCBCentralManager*)centralManager;

-(CBPeripheral*)peripheral;

// Properties set by ASCBCentralManager
@property (atomic,strong) NSNumber *advertisementRSSI;
@property (atomic, readonly) NSNumber *rssi;



/*
 * When a peripheral is being connected, we will only discover characteristics
 * on services listed in this array. Array should contain strings of the format
 * returned by ASCBStringFromCBUUID()
 */
@property (nonatomic,copy) NSArray *servicesToDiscover;

/*
 * Values: unique strings which can be used to look up discovered characteristics
 *   in ASCBPeripheral
 *
 * Keys: strings of the format returned by ASCBStringFromCBUUID(), which will
 *   be matched against the discovered characteristics.
 *
 * If this is set, it is expected that all the listed characteristics will be found.
 * If not all are found within a suitable timeout, the ASCBPeripheral will cancel
 * the connection rather than proceed to the pairing state.
 *
 */
@property (nonatomic,copy) NSDictionary *characteristicKeyMap;

/*
 * If set, during the Pairing phase the peripheral will set notify on these characteristics
 * Values should be keys matching characteristicKeyMap
 */
@property (nonatomic,copy) NSArray *alwaysNotifyCharacteristicKeys;

-(void)refreshRSSI;



/*
 * Timeout after which we treat a peripheral as no longer recently discovered
 */
@property (nonatomic, assign) NSTimeInterval advertTimeout;

-(void)setAdvertisementData:(NSDictionary *)advertisementData;

// Look up a characteristic by key (as configured in the characteristicKeyMap)
-(CBCharacteristic*)characteristicForKey:(NSString*)key;
-(void)setHandler:(ASCBPeripheralCharacteristicValueHandler)handler forKey:(NSString*)key;
-(void)setWriteHandler:(ASCBPeripheralCharacteristicWriteHandler)handler forKey:(NSString*)key;
-(void)readValueForKey:(NSString*)key;
-(void)writeValue:(NSData*)value forKey:(NSString*)key;

// Threadsafe accessors
-(ASCBPeripheralState) state;
-(bool)isConnectedOrConnecting;
-(bool)recentlyDiscovered;

// Convenience methods
-(bool)isConnectable;
-(NSString*)localName;

// Messages sent by ASCBCentralManager - expected ONLY on the bluetooth queue
-(void)resetDiscovery;
-(void)invalidated;
-(void)disconnectedDuePowerOff;
-(void)discovered;
-(void)connected;
-(void)failedToConnectWithError:(NSError*)error;
-(void)disconnectedWithError:(NSError*)error;
-(bool)shouldOwnPeripheral:(CBPeripheral*)peripheral;
-(void)assumeOwnership:(CBPeripheral*)peripheral;


// Commands to be called externally - could be any thread
-(void)connect;
-(void)connectWithTimeout:(NSTimeInterval)timeout;
-(void)cancelConnection;

-(BOOL)isReadableWriteable;

// Override this in subclasses ONLY to get a notification of a state change of the BACKGROUND THREAD
-(void)postStateChangeNotification;

@end
