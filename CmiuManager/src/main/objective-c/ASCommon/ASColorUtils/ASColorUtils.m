// Copyright (c) Airsource Ltd. 2011.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#import "ASColorUtils.h"

/**
 ** Returns true if "c" is "flat" (not pattern) and opaque (alpha = 1).
 ** Primarily designed to be used for labels in table cells: The label can use the cell's background to avoid an extra composite.
 */
bool ASUIColorIsFlatOpaque(UIColor * c)
{
    CGColorRef cgColor = c.CGColor;
    CGColorSpaceModel model = cgColor ? CGColorSpaceGetModel(CGColorGetColorSpace(cgColor)) : kCGColorSpaceModelUnknown;
    bool alphaIsUsable = !(model == kCGColorSpaceModelPattern || model == kCGColorSpaceModelUnknown);
    CGFloat alpha = alphaIsUsable ? CGColorGetAlpha(cgColor) : -1;
    return alpha == 1;
}

/**
 ** Primarily designed to be used like AS_RGB(0xFF00FF) for magenta.
 */
UIColor * AS_RGB(unsigned rgb888)
{
    uint8_t r = rgb888 >> 16;
    uint8_t g = rgb888 >>  8;
    uint8_t b = rgb888 >>  0;
    return [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:1];
}

UIColor * AS_RGBA(unsigned rgba8888)
{
    uint8_t r = rgba8888 >> 24;
    uint8_t g = rgba8888 >>  16;
    uint8_t b = rgba8888 >>  8;
    uint8_t a = rgba8888 >> 0;
    return [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a/255.0f];
}

int AS_RGBA_FromColor(UIColor *color)
{
    CGFloat values[4];
    [color getRed:values green:values+1 blue:values+2 alpha:values+3];
    
    uint8_t r = values[0]*255.0f;
    uint8_t g = values[1]*255.0f;
    uint8_t b = values[2]*255.0f;
    uint8_t a = values[3]*255.0f;
    return (r << 24) + (g << 16) + (b << 8) + a;
}
