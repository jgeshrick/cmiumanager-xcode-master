// Copyright (c) Airsource Ltd. 2011.
//
// Airsource hereby grants to the user a worldwide, irrevocable,
// royalty-free, non-exclusive license to use this pre-existing
// intellectual property component for their own business purposes,
// limited in extent to that which is needed for the user to exploit work
// results completed by Airsource.
//
// If you have received this component apart from work produced by
// Airsource you are not authorised to use this component in your
// software. Please contact Airsource on consulting@airsource.co.uk in
// order to discuss your intended use.
//
// Any unauthorised copying of this file constitutes an infringement
// of copyright. All rights reserved.

#import <UIKit/UIKit.h>

#ifdef __cplusplus
extern "C" {
#endif
    
    bool ASUIColorIsFlatOpaque(UIColor * c);
    UIColor * AS_RGB(unsigned rgb888);
    UIColor * AS_RGBA(unsigned rgba8888);
    
    
    int AS_RGBA_FromColor(UIColor *color);
    
#ifdef __cplusplus
}
#endif
