﻿/*
 * Created by Ranorex
 * User: Gallop SQA
 * Date: 2-1-2016
 * Time: 12:40 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
 #region Namespaces
using System;
using Ranorex.Core.Testing;
using DateTime = System.DateTime;

#endregion

namespace DeviceConnect
{
    /// <summary>
    /// Description of EndReport.
    /// </summary>
    [TestModule("83B35F58-6D35-4DD2-8F40-3AB7FAAAC654", ModuleType.UserCode, 1)]
    public class EndReport : ITestModule
    {
    	 public static DateTime _suiteEnd;
    	 public static string _resultsZipFolderPath;
        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
           HtmlReports._suiteEnd = DateTime.Now;
           var ts2 = HtmlReports._suiteEnd.Subtract(HtmlReports._suiteStart);
           var elapsedTime = string.Format("{0:0#}:{1:0#}:{2:0#}:{3:00#}", ts2.Hours, ts2.Minutes, ts2.Seconds, ts2.Milliseconds);
           HtmlReports.ElapsedTime = elapsedTime;
           HtmlReports.CreateHtmlSummaryReport();

        }
    }
}
