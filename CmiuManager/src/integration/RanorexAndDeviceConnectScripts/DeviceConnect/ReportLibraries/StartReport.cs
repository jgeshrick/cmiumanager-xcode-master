﻿/*
 * Created by Ranorex
 * User: Gallop SQA
 * Date: 2-1-2016
 * Time: 11:58 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

#region Namespaces
using System;
using System.Configuration;
using System.IO;

using Ranorex.Core.Testing;
using DateTime = System.DateTime;

#endregion

namespace DeviceConnect
{
	/// <summary>
	/// Description of StartReport.
	/// </summary>
	[TestModule("DC1B4D0F-F096-4F01-931A-E9C09E04D005", ModuleType.UserCode, 1)]
	public class StartReport : ITestModule
	{
		public static string GetResultsFolder= "Results";
		public static string GetResultsSummaryFolder = "ResultsSummary";
		public static string GetResultsFilename = "Results_SummaryReport_stamp.html";
		public static string GetDetailResultsFilename = "Results_DetailedReport_stamp.html";
		public static string _summaryReportDirectory;
		public static string _projectDirectory;
		public static string _logosDirectory;
		
		private string _suiteResults;
		private string _detailResultsFileName;
		// private string _summaryReportDirectory;
		
		/// <summary>
		/// Performs the playback of actions in this module.
		/// </summary>
		/// <remarks>You should not call this method directly, instead pass the module
		/// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
		/// that will in turn invoke this method.</remarks>
		void ITestModule.Run()
		{
			string strCurrentDateTime = System.DateTime.Now.ToString("yyyy.MM.dd-HH.mm.ss");
			string strReportSummaryDirectory = GetResultsSummaryFolder+ "_" + strCurrentDateTime;
			//Returns the path of "Binaries" folder
			_projectDirectory = Directory.GetCurrentDirectory();
			//_projectDirectory = ConfigurationManager.AppSettings["ProjectFolder"];
			Directory.SetCurrentDirectory(_projectDirectory);
			
			//Returns the 'Images' folder of 'Binaries'
			_logosDirectory = Path.Combine(_projectDirectory,"Images");
			
			//Creating 'Results' folder in Binaries
			if (!Directory.Exists(GetResultsFolder)) Directory.CreateDirectory(GetResultsFolder);
			
			//Creating 'ResultsSummary' folder under 'Results' folder
			if (!Directory.Exists(Path.Combine(GetResultsFolder, strReportSummaryDirectory))) Directory.CreateDirectory(Path.Combine(GetResultsFolder, strReportSummaryDirectory));
			
			_suiteResults = Path.Combine(GetResultsFolder, strReportSummaryDirectory) + "\\" + GetResultsFilename.Replace("stamp", strCurrentDateTime);
			_summaryReportDirectory = Path.Combine(GetResultsFolder, strReportSummaryDirectory);
			HtmlReports._suiteStart = DateTime.Now;
			_detailResultsFileName = Path.Combine(GetResultsFolder, strReportSummaryDirectory) + "\\" + GetDetailResultsFilename.Replace("stamp", strCurrentDateTime);
			HtmlReports.detailreportfilename = _detailResultsFileName;
			HtmlReports.summaryreportfilename = _suiteResults;
			HtmlReports.CreateDetailedReport();
			
		}
	}
}
