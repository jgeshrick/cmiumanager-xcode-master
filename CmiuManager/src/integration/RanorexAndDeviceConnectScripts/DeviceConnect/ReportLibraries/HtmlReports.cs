﻿/*
 * Created by Ranorex
 * User: Gallop SQA
 * Date: 2-1-2016
 * Time: 12:40 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

using Ranorex;
using DateTime = System.DateTime;

namespace DeviceConnect
{
	/// <summary>
	/// Description of HtmlReports.
	/// </summary>
	public static class HtmlReports
	{		
        //public static string buildVersion = ConfigurationManager.AppSettings["buildVersion"].ToString();
        public static string browser = ConfigurationManager.AppSettings["browser"].ToString();         
        public static string screenshotPath;
        public static string screenshotName;
        public static string detailreportfilename = string.Empty;
        public static int testcaseCount = 0;
        public static Dictionary<string, Hashtable> dictStatus = new Dictionary<string, Hashtable>();
        public static DateTime _suiteStart;
        public static DateTime _suiteEnd;
        internal static int totalFailed = 0; 
        internal static int totalPassed = 0;
        
        internal static int totalCriticalExecuted = 0;
        internal static int totalCriticalPassed = 0;
        internal static int totalCriticalFailed = 0;
        
        internal static int totalHighExecuted = 0;
        internal static int totalHighPassed = 0;
        internal static int totalHighFailed = 0;
        
        internal static int totalMediumExecuted = 0;
        internal static int totalMediumPassed = 0;
        internal static int totalMediumFailed = 0;
        
        internal static int totalLowExecuted = 0;
        internal static int totalLowPassed = 0;
        internal static int totalLowFailed = 0;
        
        public static string summaryreportfilename = string.Empty;
        public static bool isRecordFailureTest = false;
        public static string ElapsedTime = string.Empty;

        public static void CreateDetailedReport()
        {

            StreamWriter sw = new StreamWriter(detailreportfilename, true);
            sw.Write("<html>");
            sw.Write("<head><title>Detailed Report</title></head>");
            sw.Write("<body>");
            sw.Write("<table border='0' cellspacing='1' cellpadding='1' width='100%'>");
            sw.Write("<tr><td align='left' width='40%'><h4 align='left'><font face='arial'  color='#153e7e' size='4'><b>NEPTUNE TECHNOLOGY GROUP</b></font></h4></td>");
            sw.Write("<td align='center' width='20%'><h4 align='center'><font face='arial'  color='#153e7e' size='5'><b>Detailed Report</b></font></h4></td>");
            //Logo -- 
            //sw.Write("<td align='right' width='40%'><img src='../../Images/CompanyLogo.png' alt='Cigniti' height='50' width='125'/></td></tr>");
            sw.Write("<td align='right' width='40%'><h4 align='right'><font face='arial'  color='#153e7e' size='4'><b>GALLOP SOLUTIONS</b></font></h4></td>");
            sw.Write("</table>");
            sw.Flush();
            sw.Close();

        }
		
		public static void CreateHtmlSummaryReport()
        {
            try
            {
                foreach (KeyValuePair<string, Hashtable> keyPair in dictStatus)
                {
                    if (keyPair.Value["ScriptResult"].ToString().ToLower().Equals("passed"))
                    {
                        totalPassed += 1;
                    }
                    else if (keyPair.Value["ScriptResult"].ToString().ToLower().Equals("failed"))
                    {
                        totalFailed += 1;
                    }
                    
                    // Total Critical Priority Test Cases
                    if (keyPair.Value["Priority"].ToString().ToLower().Equals("critical"))
                    {
                    	totalCriticalExecuted += 1;
                    	if (keyPair.Value["ScriptResult"].ToString().ToLower().Equals("passed"))
                    	{
                        	totalCriticalPassed += 1;
                    	}
                    	else if (keyPair.Value["ScriptResult"].ToString().ToLower().Equals("failed"))
                    	{
                        	totalCriticalFailed += 1;
                    	}
                    	
                    }
                    
					// Total High Priority Test Cases
                    if (keyPair.Value["Priority"].ToString().ToLower().Equals("high"))
                    {
                    	totalHighExecuted += 1;
                    	if (keyPair.Value["ScriptResult"].ToString().ToLower().Equals("passed"))
                    	{
                        	totalHighPassed += 1;
                    	}
                    	else if (keyPair.Value["ScriptResult"].ToString().ToLower().Equals("failed"))
                    	{
                        	totalHighFailed += 1;
                    	}
                    	
                    }
                    // Total Medium Priority Test Cases
                    if (keyPair.Value["Priority"].ToString().ToLower().Equals("medium"))
                    {
                    	totalMediumExecuted += 1;
                    	if (keyPair.Value["ScriptResult"].ToString().ToLower().Equals("passed"))
                    	{
                        	totalMediumPassed += 1;
                    	}
                    	else if (keyPair.Value["ScriptResult"].ToString().ToLower().Equals("failed"))
                    	{
                        	totalMediumFailed += 1;
                    	}
                    	
                    }
                    // Total Low Priority Test Cases
                    if (keyPair.Value["Priority"].ToString().ToLower().Equals("low"))
                    {
                    	totalLowExecuted += 1;
                    	if (keyPair.Value["ScriptResult"].ToString().ToLower().Equals("passed"))
                    	{
                        	totalLowPassed += 1;
                    	}
                    	else if (keyPair.Value["ScriptResult"].ToString().ToLower().Equals("failed"))
                    	{
                        	totalLowFailed += 1;
                    	}
                    	
                    }
                }
            	//var summaryreportfilename = strSummaryResultsFile;
            	StreamWriter sw = new StreamWriter(summaryreportfilename, true);              
                sw.Write("<html>");
                sw.Write("<head><title>Execution Summary Report</title>");
                sw.Write("<script type='text/javascript' src='https://www.google.com/jsapi'></script>");
                sw.Write("<script type='text/javascript'>");
                sw.Write("google.load('visualization', '1', { packages: ['corechart'] }); google.setOnLoadCallback(drawChart); function drawChart() {");
                sw.Write("var data = google.visualization.arrayToDataTable([['Status', 'Count'], ['PASS', "+totalPassed.ToString()+"], ['FAIL', "+totalFailed.ToString()+"]]);");
                sw.Write("var options = { title: 'Test Cases Status Chart', is3D: true, chartArea: { left: 0, top: 0, width: '100%', height: '100%' }, colors: ['green', 'red', 'blue'], }; var chart = new ");
                sw.Write("google.visualization.PieChart(document.getElementById('piechart')); chart.draw(data, options); }");
                sw.Write("</script>");
                sw.Write("</head>");
                sw.Write("<body>");
                sw.Write("<table border='0' cellspacing='1' cellpadding='1' width='100%'>");
                sw.Write("<tr><td align='left' width='40%'><h4 align='left'><font face='arial'  color='#153e7e' size='4'><b>NEPTUNE TECHNOLOGY GROUP</b></font></h4></td>");
                //sw.Write("<tr><td align='left' height='40' width='280'><img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAV4AAABQCAMAAABmiO3sAAABJlBMVEX///8HfMEAesAAAAAAd78LfcH7+/sAdb4Adr8Ac734+Pjr6+tVVVXo6Oja2tpaWlrw8PDNzc3j4+Ovr6+Li4ttqNTw+Ps0NDTFxcU+Pj6/v7+Js9l7e3sLCwvw+Pxusdp4eHjY6/aampqSkpKpqalqamrl8vkpKSlISEgZGRlYWFiDg4OdnZ2rq6u3t7cvLy84ise70ehTotOmzObG4PBCQkJkZGSUwOA0j8pcp9W62ewNhMU6ls32hx6Bt9wWFhb2gwCYx+Rjns+IvuCXu91So9PrhilagJJdfp370Kj4rGnnhjcAfrb70rT/8+mcmo9DgJgzeqKygVTonVL8xZH/4MWcsLWagnT95tH5sHnNhUbqhiOqg3H3mkDu49mgg13Im3zmwJ/Iog+bAAAQZUlEQVR4nO1ci2LiSHYtJCGQBELiIR7GgHjTwhjzlBGmDcxsZpLNJLOZSXY3u3n8/0+k6lbpBbLb2O7u6YnOTLuRVCqko6tT594qN0IxYsSIESNGjBgxYsSIESNGjBgxfp9o9DEaX/sqfpdYHEuHrY2xPTT3/df1YZgE/cit/8dozLdcCII9fw0tRwGfyu3Z1lrCPUnr97pIUXyvnr4szPU0xXGJEDhhu7m4I+Mg4DMdk26JW9Jn8h2i1+ibi8V8N397T18e/bUtnHBLCU5eHHeLKTlvy7bMNN4SZm+S8r65ma+vSlvbEVLct0jvZssClxNSEkFKcHckLuV3jYM3kXK14Ui2hNdyYphzPBjYUyfJwQUlE1P11x9/fGVvXwnGvUS4xNc/3TbnCzwSLfYl22HRnL6Mm/6MnJc22CbphZsuLr6mxubYdAT8mNlzTlIkfnh8fPy26DW3KaoDh32ABmNzoHLB2ReNJgtCqPC9u0XI4Q6Xa4O52MyvZjbn8srA/ffjw+M3NbptbGBR2N6djD8NeM0TCemi2FuTZyW5Eb+D1+KVvuHH//2fv/0cJjjxnw8Pf39db18HEG0JTjpGRNg98Cs0L+iOOgXXNxgHErzO5doA+OmnX3/5tz/9zHE+u/+Kg/db0oYFld1ppAHrT7mgC3gJTIk8kBKT3g1I7yXnn+PXf596/HJ/enx4+OlN3X1RsNidmZFHGyXhUnqPxIh57insIl59kTPGb+KP//Tw8I/fjvSa8CoL26dc/zx9Kb0k3jmbqQF1EdKbcwp8mZTef/jDw+M/v7W3LwaDRmfCeOK4uE6R3PiU3sW6OZsSzJr7E8VeJIhvKLlbyShtaMybWzh7t3g6EI15055O7RktfPRtGOC4v+GB7c+fvK3fSuK8lsCQRSsDhrFzMA1O8OXuL+45iZhRWpVISd+Hxi3iFLiEpw3ERaRCvtnc25LEThckZx56PBtIaWbIWNyzRvgLSuTyFgIJ3p//+vD4lxB3+UqnredDu+RMudq9gATaj8qgPN1GW+a0S/o0wZQmn5ZGEWf5GP7LbWxm0knyLATZb4BTcCsMIlg+J6AN/bktBU/npPvAURFSvNTCbCYEv03KJk2I/HI/YFsW8g16uTce51ohesdVTdZQ7RIiEOrxDJEPRu1m8U+F5/OX9Am+Syi93PMbuyTNNHBYkeBiabPvOjYODJRsyyTBK9z7/bPcm0tw3tnCzOe3AY/D3pPyB/kK1iRNtGZBUuL/eAxrg5YZy/hn+KaLLfwj33s5DQT4JbD4crHdVqOOtnkd/xR1/RLVmQNXF5jSPk1AuKQz3c5m22mSMXTvtfhOCOYUTSHoIrCroOTi8/HpSfdsP+dYSDT9JaqQtGcz9gVcEj+/Bh4l//hX7BuCF6R/9N9WtVPv4jdbVHOZmqq0MrWJmCdcZQkzMg5mcdKyKoQfXatYFj5Rbtfbst+Zymey8EErZj7AAe0mN2xNRFSp8qv6RJQ7HQ0p7Y5mZXIFaNgdDutWvRBJVuMAwbt7MbvGFuhySq5gmk0Hbn/q8U+cAseF6g2ui0BGU4Kj9g72iPMZKADni8c9kwQusV3DeLCZ0YSS2Oi5kPjhNKfIDr1b645a1qqqI6W4rFqVSbVqteTCCpM45vGPbhkhK1csDlq46bA1tIqYzkGv2Br50erSm7/my7f8MI/kIT8q87yqYc0YfSzKGhEHtcxf8wOe72B2h/wwx/O37Ui2NjCscy92TSJNcEMVYNiVcNxNcApeMNN6g5thGE36ru+8gbQBrjAheR0mWDz7VXz6vnBbkzh04V8wveGcYsJ3ICAxJ0RsrZzMxKFYJ4SVVaT0ePwI6phOMgDqI0JvlaiJmMMMoZYvIi69Pb6nqkN+jDT+tqbWJjK6KfNjPY+062tMb5WvVnSL0F/jM3nN4uuRbFFTln55wrtJhmMNGITarkcvrTfcsa0jsOW++zsgW9oE5AuGVj/nXtDKkhR04XvYRwTMtKX/ejyrN6irQWaM/27nYGugufSSH0qmgtRM7wOOMzdIb/GHIeEV6bfkSrSlZxUYvWr1+kbNF/mVpvHXFhzNL0F7XXrx9+X5so66fBELB7+KZIvaBu5JU3aKxgyGo3B7SHoFdygDp8DZjB2oN3jVhz0ogxM6XaS222absJVIHoMuvC+59DYOs5/+HpFTyIUBjsBVi3zODpUgvajbQuOOMhArZRzVijUcZj4Sem/Iscn1sorB9BZ59FYG1EIMs6hzzfMZTKwappd8JvTq/EDN5vhOJF07UN77s4yiYRj4f/9Pg7UwyQSnfQw3nkP4uTtpvWHn1RsCLoI+TOeksrEP0gtCwDkn1WVw5pTeufiXyHqDMqqgHryiConSAL21pbysoWXhpicifVCf6NkBoRfMV2FUmBQKhYo3uHn0XlsdjBt8QLdusfY+Ra9cJ49hFe2FodhwPo1gHkoncBPciBlf4wqcgjt4kScWyCk4v95g3IOGnjwcdCcF6J07wYQvSC91N7s++iW63jCooTa8or44WEBvdlhZaqizKk6QaBGV1Xx6K6OTvnxxCHhmPYOdMKP0jN7WoFDTo9k16Z2dacMRp2RBpJ8zbv0tZBHuG0DrDazLPhgTiR67g2HUPnXYlF5WeqeP6iS+Df8yNw30S2S9oYI5y8NNt8vu0Nalitga4qjW+KqKZGIWUJH36JWHVrgbf2irTlS9XkPaaqLpVX6C8kO+rqtn9Cr8qN3tFiL53T2RUtink8WH83OJZMBSk03I2m0S4BTYFnURVBv62yjqcJaW9oc2qC5xiZMmYIWFA35pDHypv57UG9Rcr9teXRO6uqN6dwU2C+jNXrfG2B8UgIvhCkty7doaf+gNPO3FGd+HcZdqdohe7McIJkiDvzMKMXc8X5fD9Koo+4GqdFQevY2eRlg4p/SGU2bDXNytd7OZ7eD/bLC9gqcNcGpUveEuuq5p3AdY34RchAs6E3rFtv58Ir3apG1Z7Qplq2iNIX3TYbtStDCTyhg+u3vG8gRzMWE2ItvBJ/u+V2sXqA7LhU6xXVBwHtIpFm9IcMqTYhGLQXes4VSjQx5C50ZBH/j6eDz+wLfOyKWF8rNxBJOS9mSBZkxCUG0X+9I2mfYKOjQJc31UP7Sigc1awJYICUTqrGIPSTAzI+KORHLqVIlAYRLuI/6NVMIosjTa21HGFxzVeUIs7pseZsmTkNvMnASrA3jkYud8JQa69ExaX/LFB9Y6eIbNB9Qk2ONphEydCzpd8oqJ5i8B/rqoV+oDvnJ+aJ6Muptwk1D9AJkziSaxnCCw2UWvIAC4SgX1FYKRoxlGMx1pUqj+p9fweDbU1J3EJ73MCPn/LeCmin3xaDiJOLQHauyn6ujIrR8k3bFvnqTTmoJ9uPpuDtjTYnlQG05dBA07J8rzIjqMuvOc3xNTlzwJU5p3CG+dTPpcyNdqtYocdeSKe8IVeIBEQLin4dTY0fzVaS5860uLFp5T4CLqDX5PUDcIYwEBfqD9RbYxwcdIzwTBbxRXQrQc+jgG6wdrmkE3+8GX94oLFntBDXxt4PwhaQ9P5jRfwElwoCaxCTkEF3OaWV58d18djN5nKg52IEeYSxGvNw0tt5wjOkGlaAR8A6UxygOS3VPa5j5KP6i9ebYsIquqmv/UFE0++AJX2iI+6xNnvBVUe5NPLx8LrlfoU5tx0hgyMb/cRdqnI+sNdILo9KsawDpb+EvrDadZHUwFCs1n7FjeWrZWS+vpBgTiKDj6FDIi0vkA4XL27Iw3g85UnL2NHuhAxXKEY9SobuxC9YamEORwHXAdDTrmnY5sVLkd0b+c05yiAVngc65MbmXyWlb9VCx2g7NFhZyIlG7gXvTocvibAIXap9WBrjtjh41I88kKXO51Qri6QxN1HQI91o+k1wQx4eheMVwaYoBk5Nn1aerAizwRiTV3xljT2W5Rq8l4H4SqLMNnoJft0VWyp2PBppinx98HUIxNpCPnMftHOm/ARn5mPsPjd2MXrjeEXUQi4CJoNndSLWvYJNo59kaY9nm9QYSLEJ5dXJkv6+7Hj9btajiEwtaq3CtDmbC2HK6qbSwONTJzgT/3FEpvfigj2RqscrcW6lyPqss86t7mVuXWRRPtz2EOlT6srmdPrD/fsslyNgu5izBxZglcFResN5yub3Dj9UDPD/Jk0sh0Z4nvoEV4vQV11c7z5f72bY0xwvfySC4ucRxaqyzKZsjsz6ArI0VBYhnnVRo2qGpVZ/RmZDQhExd5Fcm9lpJF8qBLtiJN7KvAajdCKXwDjeOWTeHit5syQucUQtG7t2kGd1JvYE3orMXU7Zg6h+DYtqAB7f3+xY6mLMFZoF0yIB5PQu5+XFqgBFDHypdVHKZQFMfxWWTTNEAvoN0O0HvNAqtORkZ5FJV8vQEbthyES95vGoYoikajsThuE1BLoBO0rBBLJ2m4HQs/sbF33EUKrjDSeoNrUKnraLrPY04bJ+f0fsT+90KIXexM6PMU7A09RzTZpPQLfnFGz/GEmQEMX60CmoxuJoVJoZxHGbYkhNKr1KzWsuPTi6xREcQa6EU3o5z+frELE7+MX0Hi7Nn9bEtXJ8Ftlghdbo5wZFxu12TJznrGpRj/3u2L4VI4uIjEnfdVbHmENLvD5+9nHFAXWJW5oUKFd6a2e9xkPhMEyvaL7qRAyrw+vQOrWCxaXS1M77jaK3ZyQXpRpVXuZV16kdpZZi5aJvIJNJre71mxVTdsQyqZEI2Ou5wh6T8HQUpzZJGNnTzXBt9FBGctECnlsg5SEqxPg752vhLM/CVP5DtSKTqp/FzSE8IAD11QSZdzNVQru7tzRfo3oVeBGZ5OiF5MaW/l0Uvqv9WLFjl9AuJRCizlcu9PsucNeNndegPWwfC6ME7g1nTg8XIKCDZvK7RK8rwD3IPkBCfkgf2pE1q8JgT5fx4aKQmCQui8gvJVV2k7Q0oioVevKmTdxwm9sO6BTssRZKvvm82Z9w5H1nN5vCUcukpkk0wLgbmbncO5i8M4ITnFuVkJR2Ha8w1NMkHn2VZ6LJSlYR8t+IXi5DZ0cE79rVFKuG0Ezim9JHTzbTWrTD6SejZfnSg60VY05mtKtkLYLq9URdeROKgg8bqTVTNk0vwmQ+ktdLJZfdhCWKzzWVHuqdl8ffjeCdxifdg6ThqrruPYhyv3V4PMq6urdSB6Fjvb4SQpSdqQJTTG/IqARaC4vgq0F7+DY+HY668P0EOC9LAJW2iv3jAv2Y6Ar8Q+7F5WQM+2M5lhD7KuwbiXy3UhKCe9TKZHtEAp5jIfcCx3cFDqvUyuUMN7dazI2hgnxq3McFUkd1DMZFSxlRtmrM9QimiYi8UdxmLx3O9ViyZptHm2zSe/J7oHmlNA6dmANgvz5Ws2NUWh7/kgKypuUiAqinfYSxT8wy7cU5GiidDTi7/1W0JkveFiDN5zVPodga51k168GOsJjGJ6I0G1wfl0w+fRfrdywe8LsAYi/fJVxjEuAZ2tTL9VG2JEow818238T/d8HsxTwdJQjHfGNrDWIcZ7w6C15Pgfivo82JNqRTrWhs+EfXO3a76wwhDjYhiAr30VMWLEiBEjRowYMWLEeCn+D6GouIZoyQVQAAAAAElFTkSuQmCC' alt='Logo'/></td>");
                 sw.Write("<td align='center'><h4 align='center'><font face='arial' color='#153e7e' size='5'><b>Execution Summary Report</b></font></h4></td>");
                 sw.Write("<td align='right' width='40%'><h4 align='right'><font face='arial'  color='#153e7e' size='4'><b>GALLOP SOLUTIONS</b></font></h4></td></tr>");
                //sw.Write("<td align='right' height='50' width='150'><img src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDw8NDxAVDxUPDxUQFRUVFRAVEBAQFRUXFxURFRUYHSggGBolGxcVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGxAQGzElICY3LTc3Ly8rLy0wMDI2MC0tNS0tLS0vKy0tLS0tLS0tLS0tLSstKystLS0tNS0tLS0rLf/AABEIAJ8BPAMBEQACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAQYEBQcDAgj/xABSEAABAwIDBQMFCAsOBwEAAAABAAIDBBEFEiEGBxMxQSJRYRQycYGhF1JUkZOxwdEIFSMzQlVicpKy0yQ0NUNzg5SiwsPS4eLwJjZFU3SE8Rb/xAAaAQEAAgMBAAAAAAAAAAAAAAAABAUCAwYB/8QALBEBAAICAQQCAQMCBwAAAAAAAAEDAgQRBRIhMRNBMhQiQmGRBiNRcYHB8P/aAAwDAQACEQMRAD8A7igICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICCEBAQEBBKAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAghAQEBAQSgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAghBKAgICCEEoIQSgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIIQSgICCEEoCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICCEBAKD5Lh3ryZiPZxyBw715GUfRxL6WQICCUBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBB8krwYuIYhHAwvkcG+HU+harr8Kse7OWyqnO2eMYVKv2te45YhkB5d/pXPbHV7Mp4q8QuqelxjHNjWPxeV2pkJ9aqM778585T/dPjTrj1i+4MUlH8Y7TxKYXXRPMZz/AHeZ6lc/Te0GOSttn7QPUq61+o3Y8d/mFZdo4Tz2rFSVbZACD6uqvadjC2OYVVlWVc8SyFva30gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIMXEaxkEb5XkANF/SegWFmcYYzlLLDCc8oxj7cur8Tlrp9LnM7Kxova3QrlNiyzZs4dXTRhq18z/yttDg9NRQmaqcCSO0SQAPyWjvVrVoVUVTld7U9u5btWxjTClVItI7LmDSbtzCxLe9UN9cY5TxDpKJ5rj7lk0jCSANSenetVeM5ZcQwtmMceZXzDuBLG2BzcrmNAsdHXtzC6miKrK4wmPLl7vlrz74nxL5dTOgeCDcdPqWrKjKjLmPTOLcbo4n23UEmZoKtKs+7FAzx7cnrdbGJdAQLoCBdAQEBAQLoJQRdBKAgi6AgXQSghAugXQLoF0C6BdAQSghACCUEXQc33oYseJFRtOg+6P9XIe1VPUrJ47YXnR6ImZsl97AUoax9a/pmAPvWtGp+dYdOoxxxm3L6OrXzlnFOLMhkbUGTE6v7zCcsMffl1Lz4kn2L3xZzdZ+MeoJjKmI1qfyn3LV176qva6q4QbHHo0DziO8Kv2cLdiPkiPELCj4NPL4u7nKfbzwKCWR+eEAujGfXl/vVRdWmzPPmuPTZu2V4Y9tnqVuo6gVbSHN4c0XzhXlVn6jHjjjOFFdXOvl484S2lK/jRkOGrTlPpHVTK5+XDz7Q84+LPx6lNEbEtWNM9uXD2yOY5ZNRJkY99r5Wl1u+ymI7jfu9MEhY6hIa15aSJbmwNr2yfSg6hsttLTYlTippnXB0c02zMd70j/fNB77RYsKKkqKwtz+TxPlLb2zZWk2v05IKvu13gjGTU/cOB5PkPn5swfm/JHvUGg2n30xUtVJSU9KajhPdG52fL2mmxygNNxcINX7u8v4sPyrv2aCHb+JALnDSP5137NBdt3W3pxiOpkMHB8nLdA/NmDs3XKPeoKTJv5cHOa3D72cf443t8mgn3d5fxaflHfs0EHfzINXYcQOv3V37NBeaPeFBPhNRi0EZd5O1xdESAczW5rZrcvGyCix7+ZHebhxd6JSf7tAfv6e3R2HZfTKR/doPo79pbX+1p5f9x1vj4aCxbvt6X22qzSGl4Foy/NxM3LpbKEB29IjGftP5LpxeFxOJre1/Ny/Sg6YgxcTquDDNPbNwonSW5XytJtf1IKTu43inGJqmE03A4FjfPnzAlwH4It5qCvY9vsFJVTUpos/CflzcW19L8sh70F62K21pMWiMkBLXN8+N1s7f8kG/rJ+HFJLa/Djc+3flBNvYgoGwm877a1j6LyXg5A45s+a+XN0yi3JBh7Y74oqGqdRw0xqXRmzznygHnYANN+aDSe7vL+LT8q79mgg7+JBqcNI/nXfs0Fx3a7wzjT52+T8DgtB8/PcG35IQX0IJQfK8HDtvarNiVSPekN9Q/8Aipd6ObHUdJjiiP6ytXF4WDnL+FFb9J1j7CpNn7NKeP8A3lXVx8nUOZ+p/wCkbTC0OH0bdGvAv3alRtr8MMI9J3T/ADZddPuG9rsaZSVUNEWgRGKx8CeX0qRlsRTbjVP48INWnOzRlfz+6JaWrhkw6rDoz9zkdpbkWk+b6rqsswz1L4nCfErGvPDe1pjP8ob6uIjraeVunGAv43CnXz2bOGUfyVtEd+tnjP8AFtqVwFTKwdWh3rt/kplfi7LGELPzTjlLykmyzEeK1WZdtjbGPNfLPr/vMv8AJu+YqxQX5i3Z4DBiOJT0k7czXslsfePFyHD1gIPeCWv2VxMtcHOizC9ieHPEdbjpmtfn1CDs20+OQV+z9dVU7szX0ct/fNPDN2kd6ChfY3/9S/Mi/vUFJwTHYcOx2oq54nTMZUVALGhrnG7nAGziAg6N7tOF/i6b5OD/ABoPeh3n4RXzRUT8NcRUSNj+6QwOZdxsCRmPeg6JhGzlFQNmNLC2ETWLw0WBDQbWHIcyg49h+8DZ2ke4Q4S4kE9oxU7nWv0JddBtvdowv8XTfJwf40Gj213oYfXUM9JDQyRPlaA1zmQgN172uJQWPcfgYmwmqgqojw6iUts64ztygHx6oOlYRs7RUbctNTRQ6aljGBzvSQLlBxLf60DEqGwA0+liDt+F0sbqSBro2uBp4wQWggjIL3CDGw/ZOgpqg1dPTRwSFpaTG1rAQedwEHCZf+cP/cP6hQfpJBrto/3lV/8AjS/qOQcW+x0/fmI/ms/WkQVynw+Gp2nkp52CSOSpyub3izUE7Q4LW7MYk2qpy7hFzsjgTZ8dxeKT2HXqPBB2vZ/a2DFsMlqIuy/yeQSRnzmPDCCPEX5HxCDkm4r+GZvzZP7aDT12LxUO0c1VLGZWRzglrQ0k6MOgOiC/e7Rhf4um+Tg/xoPun3sYTVPjpnYa8iZ4j7cUBbdxtc9ooOkYFs1RUT3zUkDIDM0ZsgDWkCxHZGgQboIJQfK8HCd5sBixSY8hK0PHjoL/ADqr28Z7uXS9Ks/yuP8ARYMIm8qwl0YNyI3t8czSSPmC3xj8mtOMe0C2Zo3oyn09sULqqgp6uK5fS3a8fhAt1vZRNjHvpjOPcJ2nMU7WVOf45+pem0dVT1tLHXNeBJGMjmXs49bW9RWrb7Lq4s+27p+FutflRMc4z9tns9iEFbTthqiM1MQ4EmxLW6A36rZq2V3Vxhd7xRN+i3VunOn1k9aWq8urhJH96pvwumgXmGX6jZ7v44vLK/0mr2z+Wf03ODy8aaoqB5ubhjuOUC5HrJU3Wy77MrPpXbOPZXhX9tWaviVBy63dYKD3/JfxCf8AF2UcytGIfeZf5N3zK8Ub87bjDbG3397L/aQdv222Tp8VpX08ws6xMcn4Ub+hv3X6IPzrVSYhgLq3DZWuEdXE6Mg/e3tcC0Ss6XsTy7kF3+xv54kPyYvX99QU7B62modoKiXEY7xNnqA5rmZ9S52Xs2N0HR/dC2W+DM/o3+lBD95mzcNpYaUOew3blgDXX7wS0WQWbYLboYzHWOEJibAGgXPadmD+difeoOSbkaKGoxiZk8bJW+SyODXAObm4kQvY+koO/DZfD/gcHyTPqQP/AMvh/wADg+Sj+pBs4IGRgMY0MaOTWgAD1BB6FB+f9/8A/CdD+b9LEHdMF/e1P/IR/qBBmIPzJtBXtotqZKuoBayOp4h0u7IWkXA9KDqfu1YN7+T5OT6kGJjG+PCJaaoia+QmSF7B9zkHac0gdPFBWvscmuNTiEgBylsettL3ebX9Y+NBo8Hd/wAWemq+gIP0DtDgkFfA+mqGB7XjS/NrvfA9Cg/OGK4fiOzFa/JmMMofGHc4p4ntIAd0DgCD01CDa7hJM2LyP5ZmPdbuuHmyDXYlVU9JtJNNWx3ibOHOBbmu2zdctteRQX87w9lvgzP6N/oQfL95GzMYzx0rS5urbU4Bv6S0ILNu73gtxmWojZCYWwNa5pNsxuQLGxIQXsIJQRZBz3fBgBnp21kbSX057VusR5+3KfUo2xX3Qsum7Hx59s+pc92Hx8QScJ7rMlIsTya4/Xp8aj61nZlxKy6nqzdh34e4XPNLQyvqIW8aCXWaIC5He9tvBbM65pynLHzjKFVbXt1xXZPGePqfSv7RT0WZklE42kBL2E6Md0FvWVV7WFc/g6Dp2WxGPGxHr1LBpKhoe3MSGkgOsbHL1UTHH93lOzxnt4iOZ+v93Q6SqbMwUOGNLY9BLOQdG83AE6F3RWuP7sfipj37lzNtc1ZzfuTzl9YtjjmJR4fTCmiPbc0gDrrzcfatmzbhrVfHj7RtPXs3L/ky9MDYiAyyGY+azQHvcVE6ZXlnnNkpnVrIrwiuPcrw5twQdQRYq+c611DgNHTvMsMDI3m/aF768+qDZIMPEMKp6kATxNltyzDl60HzhuD01Lm8nhbFntmy31te3P0lBrsX2Mw2sk4tRSskeebu0CfTYoML3N8G+Bs/Sk+tA9zfBvgTPjk+tBt8E2co6FsjaWERCUjMAXG9r25nxKDxwfZKgo5XVFNTtikc0tLgXE5SQSNT3gfEg3iCLoJCAUGkxrZWhrZGTVMAlezRpJdpy7j4BBuImBjWsaLBoDQO4AWAQfV0GlxrZSgrXB9TTtlcBa5zA28bFBrfc2wb4Gz9KT60D3NsG+BM/Sk+tBvsHwemo4+DTRNhbe9hfU+JOvRB5x7P0bZvKRAwSXzZ9b370GyQY9bQwztyTRtkb3EXQYuH4DR07zJBAyN1rXF72+NBi4zslh9a8SVNMyRw0zdoH2FBr/c2wb4E345PrQBu2wb4E345PrQbXAtmKKgLzSQCEvADrFxuB6Sg3AQSgIPOaIPDmuFw4FpHeDzXnvwc8TzDgO8LYyXDpn1EXap5H5hYaxE82nwv1UK2rieXR6G9GURjl7fGzO2r6cCKYcSMfhDz2g/P7FjXfOE+We50vG7nLDxMsbG8UZPUySxgNaSABy011soV3GWXMQttGrKqmMc55YrZf9/QtHam4+VwwPbqSlo/JWxhz2k5XE2AaTfUW1OqlV7M14TjEKna6Njs7HyzPj7Y+D0tTidQdS4k9t55NHh6lGxozvz5lJ2bqNKrtj69Q7FhWHsp4mxM5N9p71fU1RXh2w4i+7K7Oc8mVKDY2te2l+V1tanOKLH8bdiz8Lf5LaICVzgJLmAkEAa+dYhBa9ucYkoMOqayIBz4WZgDfLckBBgbA4tW1cbpqqamlDmtLRAHgsvfR+YnVB4bcbRVkNTR4dQNj41WJHZ5MxaxsYbplGpJzd45IN1slW1c9KySug8nmBcx7Rq05SRmHgbX9aCqYtt/JT1GM07jE3yGFj4MxN5HOjY6xHpcUFx2YxB9VR09TIA10rMxA5XuQg53Tbzqg1GK0z2Rg0kLpID2u2WWu136TUHRNmcRdU0FJVyAB09LFO4DkC9gcQPjQc8rN4GIh9RXRshFHSV/kTmHPx5AXiPiB3Ido3tbog6bJVfucztH8SZQDy83MAg0mwGPS4hR+VTBrXcZ8dm3tZtrfOgxt4G0lRR+SU9I1hmrZjEx0l8jA1pcXEDU9NEEbv8AaSorPLKerawTUNQ6ne5lwyQtNs4B1F+7VB4babSVkVZS4XQ8Ns1Qx0ueXMWMYwOJGUWvfL39UG92Tr6melElXEIZA9zSGm7XAcnjwKCpbVbfTYbV1VPOxpBpxLScwZJcxBjP9X40F3wGSd9LTyVIAlfEx0gbfKHloJAv4oM9BzrHdqcSkrqukw5sIGHxMlkEmYvmLw5wjZa2Um1rm/NBecKqHywRySs4T3sDnMvctPddBy6o3j4iyqlsyB8UdeKThgvFQ4OLvug6WGX2oOpYjWiGnlqSPvcJky+ht7IOc7ObdYhJNQSVTIvJ8Ue5kQZn4sJa97AHE6OuWX6c0Fy26xmSgw+oq4gHPiYSA69r2J6INTS7VzvxKioixgZU0RqHc8wfdmg8O0UGTvI2inw6limpwwvlqo4AX3yNz37Rt0FkGJsBtVU1k9ZR1bYi+lLTxIS7hPDmtcAL9e0gvAQSgIIKDznha9rmPaHNcLFpAIIPQhJ8kcxPMOdbQ7pqeVxkpH+Tk65NeGT6uXqCj568T6Wut1WyqOMvMKbV7tMViNmMZMO9r7H+sAouWtl9LenrOvP5eEU+77FXGxgDPFzxb2XWH6TOfpInrWrEeFrwPdWQWurJQRzLGXsfAnRbcNL7lA2P8QZccVRx/V0XDcNhpoxFDGI2jo0AX8Tbmp2GOOMcRDnrbrLcu7OeZZdlk1/SUFQpsCqG49UYiWt4MlK2MG/azgNBFvUUGZvAwmatwyrpIADJLHZoJsLgg2v6kGv3dYfU08boZ8OgocrWjPE5jjORe7nWaPDmg8dusErnVlBidDGyd9IJGuic8sLxIG2IdYgWy+1BvNj4K1lK37YPDpnOc51iSGBziQy552Fh6kFMxzYKWpqscqJIIpPK4GCmc6xc2RsUbeo7OrSgvWytDJT0VNBL58ceV1jcXueqDl+P7tqyelq8kbBMa0yxHNYuhcBmaTbS+UaIOo7N0L4KCkppAA6KljicByDmsDTb1hBzOt2MxX914ZHFG6mq8RFX5QZCDEziiUs4eW5OlvSg6pJSHyZ0ANzwOED0vky3QUzdrQ4pRNNDU0rGwh75BMJbuJNrDJl7h3oMzeNgVVUmhqqNjZZKKcycNzsoka5paRexQRu6wGqpnV1XWMbFJX1T5+G12bhBx0aTYA9NUGHt/s5Vy1lPiFLAyqyQSU8kLn8MuZK17SQ6xGmf2INluwwSooaDgVLQxxmfIGB2bIx2WzL+Figx9vdlpa6rwueONj200zzKXHUMcG2tprqEF3QEHJ9tNkMQdU4m6mpmVceKQRsJMgY6nljaWtfYjtW7J59EHQNk6CWmoaWnm1fHEGu6jMPHqg5pVbuqwvqatkTBUHE2zxSB1nCmu/OL27i3RB1TFKJ1RSy05IDpYSwnoHFvP0XQc02b2RxTjYZT1MLIoMKke8StkzOnOd722bbTzra9yC+bb4K+vw+po43Br5YyGl18uaxteyCp7KbP4m/EIK2tgjpW0lJ5M0Nk4jpSS3t6NAA7PtQbreds9PiNJDTwBrrVcUjw42BiF848eaDeYDgdPRRCKngZACBmyNaMzrC5NuaDaBBKAgICCCgWQAgBBKAgIIQEBAQEBAQEBAQEBAQEBAQEBBKCCgICAglBCAgICCUBAQEBAQEBAQEBAQEBAQEEIJQEBAQQglAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBACAgICAghBKAgICAgICAgICAgICAgICAghBKAgICAgICAgICAgICAghBKCEEoCCEEoIQSgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICCEEoCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIIQSgICAghBKAgICAgICAgICAgICAgICAgICAg//9k=' alt='Cigniti' height='50' width='150'/></td></tr>");
                sw.Write("</table><br/>");
                sw.Write("<br/>");
                sw.Write("<table border='0' cellspacing='1' cellpadding='1' width='100%'><tr><td colspan='3'>");
                sw.Write("<table border='1' align='center' cellspacing='1' cellpadding='1' width='35%' style='font-family:Arial'>");
                sw.Write("<tr><td colspan='3' align='center' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='2'><b>Environment</b></font></td></tr>");
                //sw.Write("<tr><td width='40%' align='left' bgcolor='#153e7e'><font color='#e0e0e0' face='arial' size=1.85><b>Build Version</b></font></td>");
                //sw.Write("<td width='60%' align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + buildVersion + "</b></font></td></tr>");
                sw.Write("<tr><td width='40%' align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Run Date&Time</b></font></td>");
                sw.Write("<td width='60%' align='left'style='color:#153e7e'><font face='arial' size='1.85'><b>" + _suiteStart.ToString("MM/dd/yy HH:mm:ss.fff") + "</b></font></td></tr>");
                sw.Write("<tr><td width='40%' align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Host Name</b></font></td>");
                sw.Write("<td width='60%' align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + HostName() + "</b></font></td></tr>");
                sw.Write("<tr><td width='40%' align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>OS Details</b></font></td>");
                sw.Write("<td width='60%' align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + osEnvironment() + "</b></font></td></tr>");
                sw.Write("<tr><td width='40%' align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Browser Name</b></font></td>");
                sw.Write("<td width='60%' align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + browser.ToString() + "</b></font></td></tr>");
                sw.Write("</table><hr/>");
                sw.Write("</td></tr>");
                sw.Write("<tr><td style='width:30%'>");
                //Create Summary report Second table
                sw.Write("<table border=1 cellspacing='1' cellpadding='1' width='100%' style='font-family:Arial' align='left' >");
                sw.Write("<tr><td colspan='3' align='center' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='2'><b>Execution Status</b></font></td></tr>");

                //sw.Write("<tr><td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Suite</b></font></td>");
                //sw.Write("<td width=150 align='left'><font face='arial' size='1.85'><b>" + suiteName + "</b></font></td></tr>");

                sw.Write("<tr><td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Started</b></font></td>");
                sw.Write("<td width=150 align='left'><font face='arial' size='1.85'><b>" +  _suiteStart.ToString("MM/dd/yy HH:mm:ss.fff") + "</b></font></td></tr>");

                sw.Write("<tr><td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Ended</b></font></td>");
                sw.Write("<td width=150 align='left'><font face='arial' size='1.85'><b>" + _suiteEnd.ToString("MM/dd/yy HH:mm:ss.fff") + "</b></font></td></tr>");

                sw.Write("<tr><td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Elapsed</b></font></td>");
                sw.Write("<td width=150 align='left'><font face='arial' size='1.85'><b>" +ElapsedTime + "</b></font></td></tr>");

                sw.Write("<tr><td width=150 align='left'bgcolor='#153E7E' ><font color='#e0e0e0' face='arial' size='1.85'><b>No. of  Scripts</b></font></td>");
                //sw.Write("<td width=150 align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + totalTests.ToString() + "</b></font></td></tr>")
                sw.Write("<td width=150 align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + dictStatus.Count.ToString() + "</b></font></td></tr>");
                sw.Write("<tr><td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>No. of  Scripts Passed</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + totalPassed.ToString() + "</b></font></td></tr>");
                sw.Write("<tr><td width=150 align='left'bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>No. of  Scripts Failed</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:red' ><font face='arial' size='1.85'><b>" + totalFailed.ToString() + "</b></font></td></tr>");
                //sw.Write("<tr><td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>No. of  Scripts Incomplete</b></font></td>");
                //sw.Write("<td width=150 align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + totalIncomplete.ToString() + "</b></font></td></tr>");
                //sw.Write("<tr><td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>No. of  Scripts Rerun</b></font></td>");
                //sw.Write("<td width=150 align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + totalRerun.ToString() + "</b></font></td></tr>");
                sw.Write("</table>");
                sw.Write("</td><td id='piechart' style='padding: 50px 10px 10px 80px;height: 169px;width:20%'>");
                
                sw.Write("<td style='width:30%'>");
                //Create Summary report Second table
                sw.Write("<table border=1 cellspacing='1' cellpadding='1' width='100%' style='font-family:Arial' align='left' >");
                sw.Write("<tr><td colspan='4' align='center' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='2'><b>Priority Wise Status</b></font></td></tr>");
                sw.Write("<tr><td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Priority</b></font></td>");
                sw.Write("<td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Executed</b></font></td>");
                sw.Write("<td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Passed</b></font></td>");
                sw.Write("<td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Failed</b></font></td>");
                
                sw.Write("<tr><td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Critical</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + totalCriticalExecuted.ToString() + "</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + totalCriticalPassed.ToString() + "</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:red' ><font face='arial' size='1.85'><b>" + totalCriticalFailed.ToString() + "</b></font></td></tr>");
                                
                sw.Write("<tr><td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>High</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + totalHighExecuted.ToString() + "</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + totalHighPassed.ToString() + "</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:red' ><font face='arial' size='1.85'><b>" + totalHighFailed.ToString() + "</b></font></td></tr>");
                                
                sw.Write("<tr><td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Medium</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + totalMediumExecuted.ToString() + "</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + totalMediumPassed.ToString() + "</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:red' ><font face='arial' size='1.85'><b>" + totalMediumFailed.ToString() + "</b></font></td></tr>");
                
                sw.Write("<tr><td width=150 align='left' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='1.85'><b>Low</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + totalLowExecuted.ToString() + "</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:#153e7e'><font face='arial' size='1.85'><b>" + totalLowPassed.ToString() + "</b></font></td>");
                sw.Write("<td width=150 align='left' style='color:red' ><font face='arial' size='1.85'><b>" + totalLowFailed.ToString() + "</b></font></td></tr>");

                sw.Write("</table>");
                //sw.Write("/<td>");
                sw.Write("</tr><tr><td colspan='3'><br/><br/></td></tr><tr><td colspan='3'></td></tr>");
                
                //priority wise status
//                sw.Write("<table border=1 cellspacing='1' cellpadding='1' width='100%' style='font-family:Arial' align='right' >");
//                sw.Write("<tr><td colspan='3' align='center' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='2'><b>Priority Wise Status</b></font></td></tr>");
                
                
                
                //Create Summary report third table
                sw.Write("<table border='1' cellspacing='1' cellpadding='1' width='75%' style='font-family:Arial' align='center'>");
                
                if(isRecordFailureTest)
                {
                    sw.Write("<tr><td colspan='5' align='center' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='2'><b>Summary Report</b></font></td>");
                }
                else{
                    sw.Write("<tr><td colspan='4' align='center' bgcolor='#153E7E'><font color='#e0e0e0' face='arial' size='2'><b>Summary Report</b></font></td>");
                }
                sw.Write("</tr>");
                sw.Write("<tr><td align='center' width='12%'><font color='#153e7e' face='arial' size='2.25'><b>Script Module</b></font></td>" + "<td align='center'><font color='#153e7e' face='arial' size='2.25'><b>Script Name</b></font></td>" + "<td align='center' width='9%'><font color='#153e7e' face='arial' size='2.25'><b>Status</b></font></td>"+ "<td align='center' width='9%'><font color='#153e7e' face='arial' size='2.25'><b>Priority</b></font></td>");
                    //"<td align='center' width='12%'><font color='#153e7e' face='arial' size='2.25'><b>Times Rerun</b></font></td>");

                //if(isRecordFailureTest)
                //{
                //    sw.Write("<td align='center' width='12%'><font color='#153e7e' face='arial' size='2.25'><b>Record</b></font></td>");
                //}
                sw.Write("</tr>");
               
                foreach (KeyValuePair<string, Hashtable> tcscript in dictStatus)
                {
                    if (dictStatus.Count > 0)
                    {
                        sw.Write("<tr>");
                        Hashtable tcscriptdetails = (Hashtable)tcscript.Value;

                        if (tcscriptdetails.ContainsKey("ModuleName"))
                        {
                            sw.Write("<td><font color='#153e7e' size='1' face='arial'><b>" + tcscriptdetails["ModuleName"].ToString() + "</b></font></td>");
                        }
                        if (tcscriptdetails.ContainsKey("TestcaseName"))
                        {
                            sw.Write("<td><font color='#153e7e' size='1' face='Arial'><b>" + tcscriptdetails["TestcaseName"].ToString().Substring(tcscriptdetails["TestcaseName"].ToString().LastIndexOf(".") + 1) + "</b></font></td>");
                        }
                        if (tcscriptdetails.ContainsKey("ScriptResult"))
                        {
                            string detailreportfile = detailreportfilename.Substring(detailreportfilename.LastIndexOf('\\') + 1);
                            if (tcscriptdetails["ScriptResult"].ToString().ToLower() == "passed")
                            {
                                
                            	sw.Write("<td bgcolor='green' align='center'><font color='#FEFCFF' size='1' face='Arial'><b><a style='color:#FEFCFF' href='" + detailreportfile + "'>Pass</a></b></font></td>");
                            	//<a href="http://www.myhtmltutorials.com/jump.html">Jumping Inside Pages - HTML Tutorials</a>
                            }
                            else if (tcscriptdetails["ScriptResult"].ToString().ToLower() == "failed")
                            {
                                
                                sw.Write("<td bgcolor='red' align='center'><font color='#FEFCFF' size='1' face='Arial'><b><a style='color:#FEFCFF' href='" + detailreportfile + "'>Fail</a></b></font></td>");
                            }
                        }
                        if (tcscriptdetails.ContainsKey("Priority"))
                        {
                        	sw.Write("<td align='center'><font color='#153e7e' size='1' face='arial'><b>" + tcscriptdetails["Priority"].ToString() + "</b></font></td>");
                        }
                       
                       
                    }
                    sw.Write("</tr>");
                }
                sw.Write("</table>");
                sw.Write("</td></tr>");
                sw.Write("</table></body></html>");
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
                Report.Error(ex.Message.ToString() + "Stack Trace:" + ex.StackTrace.ToString());
                throw ex;
            }
        }
		
        // return environmental details
        public static string osEnvironment()
        {

            return "Current suite executed on : " + Environment.OSVersion;
        }

        public static string HostName()
        {
            return "Current suite executed on : " + Environment.MachineName;
        }
        
        public static void SuccessReport(string strStepName, string strStepDes)
        {
            HtmlReports.onSuccess(strStepName, strStepDes);
            Report.Success(strStepName, strStepDes);
        }

        public static void onSuccess(string strStepName, string strStepDes)
        {
            StreamWriter sw = new StreamWriter(detailreportfilename, true);
            sw.Write("<tr><td width='30%'><font color='#153e7e' size='1' face='Arial'><b>" + strStepName + "</b></font></td><td><font color='#153e7e' size='1' face='Arial'><b>" + strStepDes + "</b></font></td>" + "<td width='10%' bgcolor='green' align='center'><font color='white' size='1' face='Arial'><b>Passed</b></font></td></tr>");
            sw.Flush();
            sw.Close();
        }

        public static void FailureReport(string strStepName, string strStepDes)
        {

            Rectangle bounds = Screen.GetBounds(Point.Empty);
            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
                }

                screenshotName = strStepName.Trim().Replace(" ","").Replace("'","").Replace("\"","").Replace(":","")+"_" + System.DateTime.Now.ToString("yyyy.MM.dd-HH.mm.ss");
                screenshotPath = detailreportfilename.Substring(0, detailreportfilename.LastIndexOf("\\")) + "//" + screenshotName+".jpeg";
                bitmap.Save(screenshotPath, ImageFormat.Jpeg);
                
            }
            HtmlReports.onFailure(strStepName, strStepDes);
            Report.Failure(strStepName, strStepDes);
        }

        public static void CaptureElementScreenshot(Ranorex.Core.Element element,bool ishighlight,string strStepName,string strStepDes)
        {
        	//string projectLoc = Directory.GetCurrentDirectory();//.Substring(0,Directory.GetCurrentDirectory().LastIndexOf("\\"));
        	//string RptImgFolderpath = Path.Combine(projectLoc,"images_"+Ranorex.Core.Reporting.TestReport.ReportFilename.Replace(".rxlog",""));
        	element.EnsureVisible();
			element.Focus();
			Delay.Milliseconds(4000);
        	Bitmap bitmap = Ranorex.Imaging.CaptureDesktopImageWithSurroundingArea(element,null,Color.Red);
        	screenshotName = strStepName.Trim().Replace(" ","").Replace("'","").Replace("\"","").Replace(":","")+"_" + System.DateTime.Now.ToString("yyyy.MM.dd-HH.mm.ss");
        	screenshotPath = detailreportfilename.Substring(0, detailreportfilename.LastIndexOf("\\")) + "//" + screenshotName+".jpeg";
            bitmap.Save(screenshotPath, ImageFormat.Jpeg);

        	HtmlReports.onFailure(strStepName, strStepDes);
        }
        public static void onFailure(string strStepName, string strStepDes)
        {
            StreamWriter sw = new StreamWriter(detailreportfilename, true);
            sw.Write("<tr><td width='30%'><font color='#153e7e' size='1' face='Arial'><b>" + strStepName + "</b></font></td><td><font color='#153e7e' size='1' face='Arial'><b>" + strStepDes + "</b></font></td>" + "<td width='10%' bgcolor='red' align='center'><font color='white' size='1' face='Arial'><b><a href='" + screenshotName + ".Jpeg'" + ">Failed</a></b></font></td></tr>");
            sw.Flush();
            sw.Close();
        }

        public static void testHeader(string strModuleName,string strTestName)
        //public static void testHeader()
        {
            testcaseCount++;

            StreamWriter sw = new StreamWriter(detailreportfilename, true);

            sw.Write("<div id='Script" + testcaseCount.ToString() + "'><table border='1' cellspacing='1' cellpadding='1' width='100%'>");
            sw.Write("<tr><td colspan='3'><h4 align='center'><font color='black' size='4' face='Arial'><b>" + strModuleName + " - " + strTestName.Substring(strTestName.LastIndexOf(".") + 1) + "</b></font></h4></td></tr>");
            sw.Write("<tr><td bgcolor='#153e7e' width='30%' align='center' valign='middle'><font color='#e0e0e0' size='2' face='Arial'><b>Step</b></td>"
                    + "<td bgcolor='#153e7e' align='center' valign='middle'><font color='#e0e0e0' size='2' face='Arial'><b>Description</b></font></td>"
                    + "<td bgcolor='#153e7e' align='center' valign='middle' width='10%'><font color='#e0e0e0' size='2' face='Arial'><b>Status</b></font></td></tr>");            
            sw.Flush();
            sw.Close();
        }
        public static void testHeaderEnd()
        {
            StreamWriter sw = new StreamWriter(detailreportfilename, true);
            sw.Write("</table></div><br/><br/>");
            sw.Flush();
            sw.Close();
        }
        public static void testFooter()
        {
            StreamWriter sw = new StreamWriter(detailreportfilename, true);
            sw.Write("</body></html>");
            sw.Flush();
            sw.Close();
        }

        public static void CreateReportSetupExecCleanupSection(string strtype)
        {
        	
            StreamWriter sw = new StreamWriter(detailreportfilename, true);      
            sw.Write("<tr><td align='center' width='30%' colspan='3'><font color='#153e7e' size='1' face='Arial'><b>" + strtype + "</b></font></td></tr>");
            sw.Flush();
            sw.Close();
            Report.Info(strtype,strtype);
        }

	}
}
