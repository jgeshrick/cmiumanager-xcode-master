﻿/*
 * Created by Ranorex
 * User: IN00179
 * Date: 5/22/2016
 * Time: 4:38 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections;
using Ranorex;
using Ranorex.Core.Testing;

namespace DeviceConnect
{
	/// <summary>
	/// Description of Instrument_And_Deploy_IOS_App.
	/// </summary>
	[TestModule("44DEF882-A110-418F-8F1A-7E5DAD351B5F", ModuleType.UserCode, 1)]
	public class Instrument_And_Deploy_IOS_App : ITestModule
	{
		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		public Instrument_And_Deploy_IOS_App()
		{
			// Do not delete - a parameterless constructor is required!
		}

		#region Variables
		string currentTestCaseName = string.Empty;
		bool ScriptResult = true;
		Hashtable tblScripStatus = new Hashtable();
		string priority = "High";
		#endregion
		/// <summary>
		/// Performs the playback of actions in this module.
		/// </summary>
		/// <remarks>You should not call this method directly, instead pass the module
		/// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
		/// that will in turn invoke this method.</remarks>
		void ITestModule.Run()
		{
			Mouse.DefaultMoveTime = 300;
			Keyboard.DefaultKeyPressTime = 100;
			Delay.SpeedFactor = 1.0;
			
			#region Before Test
			Type type = this.GetType();
			currentTestCaseName = type.Name;
			string moduleName = "Deploy iOS App";
			HtmlReports.testHeader(moduleName, currentTestCaseName);
			#endregion
			Setup();
			ScriptResult = Exec();
			Cleanup();
			
			#region After Test
			tblScripStatus.Add("ModuleName", moduleName);
			tblScripStatus.Add("TestcaseName", currentTestCaseName);
			tblScripStatus.Add("ScriptResult", ScriptResult ? "Passed" : "Failed");
			tblScripStatus.Add("Priority",priority);
			HtmlReports.dictStatus.Add(currentTestCaseName, tblScripStatus);
			#endregion
		}
		
		
		#region Setup
		protected void Setup()
		{
			HtmlReports.CreateReportSetupExecCleanupSection("Setup");
		}
		#endregion
		
		#region Execution
		protected bool Exec()
		{
			HtmlReports.CreateReportSetupExecCleanupSection("Execution");
			
			if(Deployment.Deploy_iOS_App()) //invoke the actual script execution
			{
				HtmlReports.SuccessReport("Deployment","Instrumented and deployed iOS App successfully");
				return true;
			}
			else {
				HtmlReports.FailureReport("Deployment","Failed to instrument and deploy iOS App");
				return false;
			}
			
		}
		#endregion
		
		#region Cleanup
		/// <summary>
		/// This is for cleanup the testdata after testscript execution is completed
		/// </summary>
		protected void Cleanup()
		{
			HtmlReports.CreateReportSetupExecCleanupSection("Cleanup");
			
		}
		#endregion
	}
}
