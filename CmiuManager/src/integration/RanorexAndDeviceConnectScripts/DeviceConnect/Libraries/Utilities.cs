﻿/*
 * Created by Ranorex
 * User: IN00179
 * Date: 5/22/2016
 * Time: 4:28 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Configuration;
using System.Diagnostics;
using Ranorex;

namespace DeviceConnect
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	public static class Utilities
	{

		/// <summary>
		/// Read appSettings section of app.config configuration file
		/// </summary>
		/// <param name="Key">appSettings key</param>
		/// <returns>appSettings value</returns>
		public static string fn_ReadConfigFile(string Key)
		{
			try
			{
				return ConfigurationManager.AppSettings[Key].ToString();
			}
			catch(Exception ex)
			{
				Console.WriteLine("Unable to read config file with key: {0} \nException: {1}",Key,ex.Message);
				return null;
			}
		}
		
		/// <summary>
		/// Method to close browser
		/// </summary>
		/// <param name="strBrowser"></param>
		/// <returns></returns>
		public static bool CloseBrowser(string strBrowser)
		{
			try
			{
				Delay.Milliseconds(500);
				Host.Local.KillBrowser(strBrowser);
				Delay.Milliseconds(500);
				HtmlReports.SuccessReport("Close Browser","Sucessfully closed the browser: "+strBrowser.ToUpper());
			
			}
			
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message.ToString() + "Stack Trace:" + ex.StackTrace.ToString());
				return false;
			}
			return true;
			
		}
				
	}
}
