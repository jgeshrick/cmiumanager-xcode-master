﻿/*
 * Created by Ranorex
 * User: IN00179
 * Date: 5/25/2016
 * Time: 8:56 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using Ranorex;

namespace DeviceConnect
{
	/// <summary>
	/// Description of Deployment.
	/// </summary>
	public class Deployment
	{
		/// <summary>
		/// Method to deploy iOS App using deviceConnect.
		/// </summary>
		/// <returns></returns>
		public static bool Deploy_iOS_App()
		{
			try
			{
				Ranorex.Controls.ProgressForm.Hide();
				#region Read from config file
				string browser = Utilities.fn_ReadConfigFile("Browser");
				string url = Utilities.fn_ReadConfigFile("URL");
				string username = Utilities.fn_ReadConfigFile("UserName");
				string password = Utilities.fn_ReadConfigFile("Password");
				string deviceName = Utilities.fn_ReadConfigFile("DeviceName");
				string appName = Utilities.fn_ReadConfigFile("AppName");
				string appPath = Directory.GetCurrentDirectory()+"\\"+Utilities.fn_ReadConfigFile("AppFileName");
				string instrumentedAppPath = Directory.GetCurrentDirectory()+"\\"+Utilities.fn_ReadConfigFile("InstrumentedAppFileName");
				string p12CertificatePath = Directory.GetCurrentDirectory()+"\\"+Utilities.fn_ReadConfigFile("P12CertificateFileName");
				string p12Password = Utilities.fn_ReadConfigFile("P12CertificatePassword");
				string provisioningProfilePath = Directory.GetCurrentDirectory()+"\\"+Utilities.fn_ReadConfigFile("ProvisioningProfileFileName");
				
				/*string appPath = Utilities.fn_ReadConfigFile("AppPath");
				string instrumentedAppPath = Utilities.fn_ReadConfigFile("InstrumentedAppPath");
				string p12CertificatePath = Utilities.fn_ReadConfigFile("P12CertificatePath");
				string p12Password = Utilities.fn_ReadConfigFile("P12CertificatePassword");
				string provisioningProfilePath = Utilities.fn_ReadConfigFile("ProvisioningProfilePath");
				 */
				string ranorexPath = Utilities.fn_ReadConfigFile("RanorexPath");
				string commandFilePath= Directory.GetCurrentDirectory()+"\\command.bat";
				
				string[] deviceList = deviceName.Split(',');
				string cmdText = (char)(34)+ranorexPath+"\\Bin\\Ranorex.Instrumentation.exe"+(char)(34)+" /pagename:ios -ip:"+(char)(34)+appPath+(char)(34)+" -sd:True -o:"+(char)(34)+instrumentedAppPath+(char)(34)+" -k:"+(char)(34)+p12CertificatePath+(char)(34)+" -pw:"+(char)(34)+p12Password+(char)(34)+" -pp:"+(char)(34)+provisioningProfilePath+(char)(34);
				#endregion
				
				#region Instrumentation

				if(File.Exists(commandFilePath))
				{
					File.Delete(commandFilePath);
				}
				
				StreamWriter sw = new StreamWriter(commandFilePath);
				sw.WriteLine(cmdText);
				sw.Close();
				Process.Start(commandFilePath).WaitForExit(600000);
				if(File.Exists(commandFilePath))
				{
					File.Delete(commandFilePath);
				}
				
				if(File.Exists(instrumentedAppPath))
				{
					HtmlReports.SuccessReport("Instrumentation","Instrumented the app successfully");
				}
				else
				{
					HtmlReports.FailureReport("Instrumentation","Failed to instrument the app");
					Validate.Fail("Unable to instrument the app!");
				}
				#endregion
				
				#region Launch Browser & Login to device Connect
				if(Host.Local.OpenBrowser(url,browser,true,true)!= 0)
				{
					HtmlReports.SuccessReport("Launch URL","deviceConnect URL launched successfully");
				}
				else
				{
					HtmlReports.FailureReport("Launch URL","Failed to Launch deviceConnect URL");
					Validate.Fail("Failed to Launch deviceConnect URL");
				}
				
				
				var repo = DeviceConnect.Repository.DeviceConnectRepository.Instance;
				var deviceConnectMobileLabs = repo.DeviceConnectMobileLabs;
				
				deviceConnectMobileLabs.txtUsernameInfo.WaitForExists(60000);
				
				//Login screen -- Username
				if(deviceConnectMobileLabs.txtUsernameInfo.Exists(2000))
				{
					deviceConnectMobileLabs.txtUsername.Value = username;
					HtmlReports.SuccessReport("Login","UserName entered successfully");
				}
				else
				{
					HtmlReports.FailureReport("Login","Failed to find username field");
					Validate.Fail("Unable to find UserName field");
				}
				
				
				//Login screen -- Password
				if(deviceConnectMobileLabs.txtPasswordInfo.Exists(2000))
				{
					deviceConnectMobileLabs.txtPassword.Value = password;
					HtmlReports.SuccessReport("Login","Password entered successfully");
				}
				else
				{
					HtmlReports.FailureReport("Login","Failed to find Password field");
					Validate.Fail("Unable to find Password field");
				}
				
				
				//Login screen -- Login button
				if(deviceConnectMobileLabs.btnLoginInfo.Exists(2000))
				{
					deviceConnectMobileLabs.btnLogin.PerformClick();
					HtmlReports.SuccessReport("Login","Login button clicked successfully");
				}
				else
				{
					HtmlReports.FailureReport("Login","Failed to find Login button");
					Validate.Fail("Unable to find Login button");
				}
				
				deviceConnectMobileLabs.btnDevicesInfo.WaitForExists(60000);
				#endregion
				
				//loop through device list
				foreach (string device in deviceList)
				{
					#region Check the status of device in deviceConnect
					//Check device availability enter device name in search box
					if(deviceConnectMobileLabs.txtboxSearchInfo.Exists(5000))
					{
						deviceConnectMobileLabs.txtboxSearch.Value="";
						deviceConnectMobileLabs.txtboxSearch.PressKeys(device);
						HtmlReports.SuccessReport("Devices","Searched for device successfully");
					}
					else
					{
						HtmlReports.FailureReport("Devices","Failed to find search text box");
						Validate.Fail("Unable to find search text box");
					}
					//check Availability status of device
					if(deviceConnectMobileLabs.txtAvailableInfo.Exists(5000) || deviceConnectMobileLabs.txtInUseInfo.Exists(5000))
					{
						HtmlReports.SuccessReport("Devices","Device is found and not offline");
					}
					else
					{
						HtmlReports.FailureReport("Devices","Failed to find device or it is offline");
						Validate.Fail("Unable to find device or it is offline");
					}
					
					#endregion
				}
				
				#region Upload Application in deviceConnect
				
				//click Applications tab
				if(deviceConnectMobileLabs.btnApplicationsInfo.Exists(2000))
				{
					deviceConnectMobileLabs.btnApplications.PerformClick();
					HtmlReports.SuccessReport("Applications","Clicked the Applications tab successfully");
				}
				else
				{
					HtmlReports.FailureReport("Applications","Failed to find Applications tab");
					Validate.Fail("Unable to find Applications tab");
				}
				
				//Click Upload Application button
				if(deviceConnectMobileLabs.btnUploadApplicationInfo.Exists(2000))
				{
					deviceConnectMobileLabs.btnUploadApplication.Click();
					HtmlReports.SuccessReport("Applications","Clicked the Upload Application button successfully");
				}
				else
				{
					HtmlReports.FailureReport("Applications","Failed to find Upload Application button");
					Validate.Fail("Unable to find Upload Application button");
				}
				
				var fileUploadDialog = repo.FileUploadDialog;
				fileUploadDialog.txtFileNameInfo.WaitForExists(20000);

				//Enter instrumented file name
				if(fileUploadDialog.txtFileNameInfo.Exists(2000))
				{
					fileUploadDialog.txtFileName.TextValue=instrumentedAppPath;
					HtmlReports.SuccessReport("Applications","Entered file name successfully");
				}
				else
				{
					HtmlReports.FailureReport("Applications","Failed to find File upload dialog box");
					Validate.Fail("Unable to find File upload dialog box");
				}
				
				//upload the instrumented app
				if(fileUploadDialog.btnOpenInfo.Exists(2000))
				{
					fileUploadDialog.btnOpen.Click();
					HtmlReports.SuccessReport("Applications","Opened the file successfully");
				}
				else
				{
					HtmlReports.FailureReport("Applications","Failed to find the Open button");
					Validate.Fail("Unable to find the Open button");
				}
				
				
				//Wait for success message at the bottom of the screen
				deviceConnectMobileLabs.txtSuccessInfo.WaitForExists(120000);
				HtmlReports.SuccessReport("Applications","Uploaded the application successfully");
				
				#endregion
				
				//loop through device list
				foreach (string device in deviceList)
				{
					#region Uninstall existing app
					
					//Click on Devices tab
					if(deviceConnectMobileLabs.btnDevicesInfo.Exists(2000))
					{
						deviceConnectMobileLabs.btnDevices.PerformClick();
						HtmlReports.SuccessReport("Devices","Clicked the Devices tab successfully");
					}
					else
					{
						HtmlReports.FailureReport("Devices","Failed to find the Devices tab");
						Validate.Fail("Unable to find Devices tab");
					}
					
					
					//Enter device name in search box
					if(deviceConnectMobileLabs.txtboxSearchInfo.Exists(2000))
					{
						deviceConnectMobileLabs.txtboxSearch.Value="";
						deviceConnectMobileLabs.txtboxSearch.PressKeys(device);
						HtmlReports.SuccessReport("Devices","Searched for device successfully");
					}
					else
					{
						HtmlReports.FailureReport("Devices","Failed to find search text box");
						Validate.Fail("Unable to find search text box");
					}
					
					//check Availability status of device
					if(deviceConnectMobileLabs.txtAvailableInfo.Exists(5000) || deviceConnectMobileLabs.txtInUseInfo.Exists(5000))
					{
						HtmlReports.SuccessReport("Devices","Device is found and is not offline");
					}
					else
					{
						HtmlReports.FailureReport("Devices","Failed to find device or it is offline");
						Validate.Fail("Unable to find device or it is offline");
					}
					
					deviceConnectMobileLabs.tblDevicesListInfo.WaitForExists(30000);
					
					var tblDevicesList = repo.DeviceConnectMobileLabs.tblDevicesList;
					
					string xPath = "/dom[@caption~'^deviceConnect']//table//a[@innertext~'"+device+"']";
					IList<Ranorex.ATag> searchResults = deviceConnectMobileLabs.pageBody.Find<Ranorex.ATag>(xPath,120000);
					if(searchResults.Count == 0)
					{
						HtmlReports.FailureReport("Devices","Unable to find device: "+device);
						Validate.Fail("Unable to find the device:"+device);
					}
					
					foreach (Ranorex.ATag dev in searchResults)
					{
						if(dev.InnerText.Contains(device))
							dev.PerformClick();
						HtmlReports.SuccessReport("Devices","Opened device successfully");
						break;
					}
					
					
					var tblInstalledList = repo.DeviceConnectMobileLabs.tblInstalledList;
					var tblAvailableList = repo.DeviceConnectMobileLabs.tblAvailableList;

					//Search through Installed App list and un-install the existing app
					xPath = "/dom[@caption~'^deviceConnect']//table[@tagname='table'][1]//tr[@innertext~'.']";
					IList<Ranorex.TrTag> searchRows = deviceConnectMobileLabs.tblInstalledList.Find<Ranorex.TrTag>(xPath,120000);
					int found=0, rownum=0;
					foreach (Ranorex.TrTag row in searchRows)
					{
						IList<Ranorex.TdTag> searchColumns = row.FindChildren<Ranorex.TdTag>();
						foreach(Ranorex.TdTag col in searchColumns)
						{
							if ((col != null) && (col.Title != null))
							{
								if(col.Title.Contains(appName))
								{
									//xPath = row.GetPath()+"//button[@innertext~'Uninstall']";
									xPath = "/dom[@caption~'^deviceConnect']//table[@tagname='table'][1]//tr["+rownum+"]//button[@innertext~'Uninstall']";
									Ranorex.ButtonTag btnUninstall= null;
									bool buttonAvailable = deviceConnectMobileLabs.tblInstalledList.TryFindSingle<Ranorex.ButtonTag>(xPath,120000, out btnUninstall);
									if(buttonAvailable)
									{
										btnUninstall.PerformClick();
										HtmlReports.SuccessReport("Devices","Clicked Uninstall button for "+appName+" successfully");
										found=1;
										break;
									}
									else
									{
										HtmlReports.FailureReport("Devices","Unable to find Uninstall button for app: "+appName);
										Validate.Fail("Unable to find uninstall button for app:"+appName);
									}
									
								}
							}
							
						}
						rownum=rownum+1;
						if(found==1) break;
					}

					if(found==1)
					{
						deviceConnectMobileLabs.btnContinue.PerformClick();
						deviceConnectMobileLabs.txtSuccessInfo.WaitForExists(300000);
						HtmlReports.SuccessReport("Devices","Uninstalled the app successfully");
					}
					#endregion
					
					#region Install the new app
					
					//Search through Available App list and install the app
					xPath = "/dom[@caption~'^deviceConnect']//table[@tagname='table'][2]//tr[@innertext~'.']";
					searchRows = deviceConnectMobileLabs.tblAvailableList.Find<Ranorex.TrTag>(xPath,120000);
					found=0;rownum=0;
					foreach (Ranorex.TrTag row in searchRows)
					{
						IList<Ranorex.TdTag> searchColumns = row.FindChildren<Ranorex.TdTag>();
						foreach(Ranorex.TdTag col in searchColumns)
						{
							if ((col != null) && (col.Title != null))
							{
								if(col.Title.Contains(appName))
								{
									//xPath = row.GetPath()+"//button[@innertext~'Install']";
									xPath = "/dom[@caption~'^deviceConnect']//table[@tagname='table'][2]//tr["+rownum+"]//button[@innertext~'Install']";
									Ranorex.ButtonTag btnInstall= null;
									bool buttonAvailable = deviceConnectMobileLabs.tblAvailableList.TryFindSingle<Ranorex.ButtonTag>(xPath,120000, out btnInstall);
									if(buttonAvailable)
									{
										btnInstall.PerformClick();
										deviceConnectMobileLabs.txtSuccessInfo.WaitForExists(600000);
										HtmlReports.SuccessReport("Devices","Install "+appName+" app successfully");
										found=1;
										break;
									}
									else
									{
										HtmlReports.FailureReport("Devices","Unable to find Install button for app: "+appName);
										Validate.Fail("Unable to find Install button for app:"+appName);
									}
									
								}
							}
							
						}
						rownum=rownum+1;
						if(found==1) break;
					}
					
					Delay.Seconds(10);
					if(found==0)
					{
						HtmlReports.FailureReport("Devices","Unable to Install app: "+appName);
						Validate.Fail("Unable to Install app:"+appName);
					}
					#endregion
				}
				#region Logout of deviceConnect
				
				//Logout of deviceConnect
				if(deviceConnectMobileLabs.btnLogoutInfo.Exists(5000))
				{
					deviceConnectMobileLabs.btnLogout.PerformClick();
					HtmlReports.SuccessReport("Devices","Logged out of deviceConnect successfully");
				}
				
				Utilities.CloseBrowser(browser);
				#endregion
				
				return true;
			}
			catch(Exception ex)
			{
				Console.WriteLine("Failed to deploy iOS App.\nException: "+ex.Message);
				HtmlReports.FailureReport("Exception","Failed to deploy iOS App. Exception: "+ex.Message);
				return false;
			}
		}

	}
}
