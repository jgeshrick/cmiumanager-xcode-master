# DeviceConnect produces test reports in date and timestamped folders.
# This script finds the latest reports and copies them to a location where teamcity will pick them up to publish.

# Remove old generic files if they exist
Remove-Item ".\Results_DetailedReport.html"
Remove-Item ".\Results_SummaryReport.html"

$items = Get-ChildITem -Path ".\Working\Results" # Folder containing reports
$dst = "." # Destination folder
$maxDate = Get-Date "1/1/2000 11:11:11" # Initialise datetime object, value should be well in the past
foreach ($item in $items) # Loop through items in the source directory
{
	if ($item.Attributes -eq "Directory") # Search for sub-directories only
	{
		$dirName = $item.Name # Get the sub-directory name (does not include the path)
		$dirName = $dirName.Replace("ResultsSummary_", "") # Trim characters not part of the timestamp
		$dirDate = [datetime]::ParseExact($dirName, 'yyyy.MM.dd-HH.mm.ss', $null) # Convert timestamp string to datetime object
		if ($dirDate -gt $maxDate) # Compare directory timestamp to the previous maximum date
		{
			$maxDate = $dirDate # Set new maximum date
			$src = $item.FullName # Get full path of the folder
		}
	}
}
#Write-Host $maxDate # FOR DEBUGGING: Write maximum date in the console
#Write-Host $src # FOR DEBUGGING: Write full path of newest folder to console
Get-ChildItem $src -Filter "*.html" | Copy-Item -Destination $dst # Copy all html files from the folder to the destination directory

# Rename report files to generic name
Get-ChildItem $dst -Filter "Results_DetailedReport*" | Rename-Item -NewName "Results_DetailedReport.html"
Get-ChildItem $dst -Filter "Results_SummaryReport*" | Rename-Item -NewName "Results_SummaryReport.html"
