﻿# DeviceConnect produces test output in the wrong format for TeamCity.
# This script reformats the output.
# See here for the format required for TeamCity: https://confluence.jetbrains.com/display/TCD9/Build+Script+Interaction+with+TeamCity#BuildScriptInteractionwithTeamCity-Supportedtestservicemessages

param([string]$file="deviceconnect_test_output.txt")

$reader = [System.IO.File]::OpenText($file)
try {
    $currentTest = ""
    while($null -ne ($line = $reader.ReadLine())) {
        if ($line -match "\[Info\s+\]\[Test\]: Test Suite '(.+)' started") {
            Write-Host "##teamcity[testSuiteStarted name='$($matches[1])']"
        } elseif ($line -match "\[Info\s+\]\[Test\]: Test Case '(.+)' started") {
            Write-Host "##teamcity[testSuiteStarted name='$($matches[1])']"
        } elseif ($line -match "\[Info\s+\]\[Test\]: Test Module '(.+)' started") {
            Write-Host "##teamcity[testStarted name='$($matches[1])']"
            $currentTest = $matches[1]
        } elseif ($line -match "\[.+\]\[Test\]: Test Suite '(.+)' completed.*") {
            Write-Host "##teamcity[testSuiteFinished name='$($matches[1])']"
        } elseif ($line -match "\[.+\]\[Test\]: Test Case '(.+)' completed.*") {
            Write-Host "##teamcity[testSuiteFinished name='$($matches[1])']"
        } elseif ($line -match "\[.+\]\[Test\]: Test Module '(.+)' completed.*") {
            Write-Host "##teamcity[testFinished name='$($matches[1])']"
            $currentTest = ""
        } elseif (($line -match "\[Failure\s+\]\[(.+)\]:\s*(.*)") -and ($currentTest -ne "")) {
	    $safeLine = $line.Replace("|","||").Replace("[","|[").Replace("]","|]").Replace("'","|'")
            Write-Host "##teamcity[testFailed name='$($currentTest)' message='$($safeLine)']"
        } elseif (($line -match "\[Error\s+\]\[(.+)\]:\s*(.*)") -and ($currentTest -ne "")) {
	    $safeLine = $line.Replace("|","||").Replace("[","|[").Replace("]","|]").Replace("'","|'")
            Write-Host "##teamcity[testFailed name='$($currentTest)' message='$($safeLine)']"
        } else {
            Write-Host $line
        }
    }
}
finally {
    $reader.Close()
}

