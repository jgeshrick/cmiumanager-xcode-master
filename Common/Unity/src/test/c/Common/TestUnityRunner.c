/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @defgroup RingBufferUnitTest 
 *
 * @brief Unit Test for the @ref RingBuffer  
 *
 */

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup RingBufferUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test runner for the @ref RingBuffer
 * @author Duncan Willis
 * @date 2015.03.24  
 * @version 1.0
 */
 

#include "unity.h"
#include "TeamCity.h"
#include "TestUnity.h"


extern void setUp(void);
extern void tearDown(void);



/** This function is used by unity */
void resetTest(void)
{
    tearDown();
    setUp();
}


/**
** Runs the unit tests for unity
**/
int TestUnity(void)
{
    int testFailures = 0;
 
    Unity.TestFile = "Unity.c";
    UnityBegin();

    printf("Testing module %s\n", Unity.TestFile);

    // Todo: replace line numbers with __LINE__?
//    RUN_TEST(testUnitySizeInitializationReminder, 63);
    RUN_TEST(testTrue, 93);
    RUN_TEST(testFalse, 100);
    RUN_TEST(testPreviousPass, 107);
    RUN_TEST(testNotVanilla, 112);
    RUN_TEST(testNotTrue, 119);
    RUN_TEST(testNotFalse, 126);
    RUN_TEST(testNotUnless, 133);
    RUN_TEST(testNotNotEqual, 140);
    RUN_TEST(testFail, 147);
    RUN_TEST(testIsNull, 154);
    RUN_TEST(testIsNullShouldFailIfNot, 163);
    RUN_TEST(testNotNullShouldFailIfNULL, 172);
    RUN_TEST(testIgnore, 181);
    RUN_TEST(testIgnoreMessage, 189);
    RUN_TEST(testNotEqualInts, 197);
    RUN_TEST(testNotEqualInt8s, 204);
    RUN_TEST(testNotEqualInt16s, 211);
    RUN_TEST(testNotEqualInt32s, 218);
    RUN_TEST(testNotEqualBits, 225);
    RUN_TEST(testNotEqualUInts, 232);
    RUN_TEST(testNotEqualUInt8s, 244);
    RUN_TEST(testNotEqualUInt16s, 256);
    RUN_TEST(testNotEqualUInt32s, 268);
    RUN_TEST(testNotEqualHex8s, 280);
    RUN_TEST(testNotEqualHex8sIfSigned, 292);
    RUN_TEST(testNotEqualHex16s, 304);
    RUN_TEST(testNotEqualHex16sIfSigned, 316);
    RUN_TEST(testNotEqualHex32s, 328);
    RUN_TEST(testNotEqualHex32sIfSigned, 340);
    RUN_TEST(testEqualInts, 352);
    RUN_TEST(testEqualInt8s, 372);
    RUN_TEST(testEqualInt8sWhenThereAreDifferencesOutside8Bits, 391);
    RUN_TEST(testEqualInt16s, 397);
    RUN_TEST(testEqualInt16sNegatives, 416);
    RUN_TEST(testEqualInt16sWhenThereAreDifferencesOutside16Bits, 435);
/*
    RUN_TEST(testEqualInt32s, 441);
    RUN_TEST(testEqualInt32sNegatives, 460);
*/
    RUN_TEST(testEqualUints, 480);
    RUN_TEST(testEqualUint8s, 501);
    RUN_TEST(testEqualUint8sWhenThereAreDifferencesOutside8Bits, 520);
    RUN_TEST(testEqualUint16s, 526);
    RUN_TEST(testEqualUint16sWhenThereAreDifferencesOutside16Bits, 545);
//    RUN_TEST(testEqualUint32s, 551);
    RUN_TEST(testNotEqual, 570);
    RUN_TEST(testEqualHex8s, 581);
    RUN_TEST(testEqualHex8sWhenThereAreDifferencesOutside8Bits, 600);
    RUN_TEST(testEqualHex8sNegatives, 606);
    RUN_TEST(testEqualHex16s, 625);
    RUN_TEST(testEqualHex16sWhenThereAreDifferencesOutside16Bits, 644);
    RUN_TEST(testEqualHex32s, 650);
    RUN_TEST(testEqualBits, 669);
/*
    RUN_TEST(testNotEqualBitHigh, 685);
    RUN_TEST(testNotEqualBitLow, 694);
    RUN_TEST(testNotEqualBitsHigh, 703);
    RUN_TEST(testNotEqualBitsLow, 714);
*/
    RUN_TEST(testEqualShorts, 724);
    RUN_TEST(testEqualUShorts, 744);
    RUN_TEST(testEqualChars, 764);
    RUN_TEST(testEqualUChars, 784);
    RUN_TEST(testEqualPointers, 803);
    RUN_TEST(testNotEqualPointers, 820);
    RUN_TEST(testIntsWithinDelta, 827);
//    RUN_TEST(testIntsWithinDeltaAndCustomMessage, 839);
    RUN_TEST(testIntsNotWithinDelta, 851);
//    RUN_TEST(testIntsNotWithinDeltaAndCustomMessage, 858);
    RUN_TEST(testUIntsWithinDelta, 865);
//    RUN_TEST(testUIntsWithinDeltaAndCustomMessage, 872);
    RUN_TEST(testUIntsNotWithinDelta, 879);
//    RUN_TEST(testUIntsNotWithinDeltaAndCustomMessage, 886);
    RUN_TEST(testUIntsNotWithinDeltaEvenThoughASignedIntWouldPassSmallFirst, 893);
//    RUN_TEST(testUIntsNotWithinDeltaEvenThoughASignedIntWouldPassSmallFirstAndCustomMessage, 900);
    RUN_TEST(testUIntsNotWithinDeltaEvenThoughASignedIntWouldPassBigFirst, 907);
//    RUN_TEST(testUIntsNotWithinDeltaEvenThoughASignedIntWouldPassBigFirstAndCustomMessage, 914);
    RUN_TEST(testHEX32sWithinDelta, 921);
//    RUN_TEST(testHEX32sWithinDeltaAndCustomMessage, 928);
    RUN_TEST(testHEX32sNotWithinDelta, 935);
//    RUN_TEST(testHEX32sNotWithinDeltaAndCustomMessage, 942);
    RUN_TEST(testHEX32sNotWithinDeltaEvenThoughASignedIntWouldPass, 949);
//    RUN_TEST(testHEX32sNotWithinDeltaEvenThoughASignedIntWouldPassAndCustomMessage, 956);
    RUN_TEST(testHEX16sWithinDelta, 963);
//    RUN_TEST(testHEX16sWithinDeltaAndCustomMessage, 970);
    RUN_TEST(testHEX16sWithinDeltaWhenThereAreDifferenceOutsideOf16Bits, 977);
//    RUN_TEST(testHEX16sWithinDeltaWhenThereAreDifferenceOutsideOf16BitsAndCustomMessage, 982);
    RUN_TEST(testHEX16sNotWithinDelta, 987);
//    RUN_TEST(testHEX16sNotWithinDeltaAndCustomMessage, 994);
    RUN_TEST(testHEX8sWithinDelta, 1001);
//    RUN_TEST(testHEX8sWithinDeltaAndCustomMessage, 1008);
    RUN_TEST(testHEX8sWithinDeltaWhenThereAreDifferenceOutsideOf8Bits, 1015);
//    RUN_TEST(testHEX8sWithinDeltaWhenThereAreDifferenceOutsideOf8BitsAndCustomMessage, 1020);
    RUN_TEST(testHEX8sNotWithinDelta, 1025);
/*
    RUN_TEST(testHEX8sNotWithinDeltaAndCustomMessage, 1032);
    RUN_TEST(testUINT32sWithinDelta, 1041);
    RUN_TEST(testUINT32sWithinDeltaAndCustomMessage, 1048);
    RUN_TEST(testUINT32sNotWithinDelta, 1055);
    RUN_TEST(testUINT32sNotWithinDeltaAndCustomMessage, 1062);
    RUN_TEST(testUINT32sNotWithinDeltaEvenThoughASignedIntWouldPass, 1069);
    RUN_TEST(testUINT32sNotWithinDeltaEvenThoughASignedIntWouldPassAndCustomMessage, 1076);
    RUN_TEST(testUINT16sWithinDelta, 1083);
    RUN_TEST(testUINT16sWithinDeltaAndCustomMessage, 1090);
    RUN_TEST(testUINT16sWithinDeltaWhenThereAreDifferenceOutsideOf16Bits, 1097);
    RUN_TEST(testUINT16sWithinDeltaWhenThereAreDifferenceOutsideOf16BitsAndCustomMessage, 1102);
    RUN_TEST(testUINT16sNotWithinDelta, 1107);
    RUN_TEST(testUINT16sNotWithinDeltaAndCustomMessage, 1114);
    RUN_TEST(testUINT8sWithinDelta, 1121);
    RUN_TEST(testUINT8sWithinDeltaAndCustomMessage, 1128);
    RUN_TEST(testUINT8sWithinDeltaWhenThereAreDifferenceOutsideOf8Bits, 1135);
    RUN_TEST(testUINT8sWithinDeltaWhenThereAreDifferenceOutsideOf8BitsAndCustomMessage, 1140);
    RUN_TEST(testUINT8sNotWithinDelta, 1145);
    RUN_TEST(testUINT8sNotWithinDeltaAndCustomMessage, 1152);
    RUN_TEST(testINT32sWithinDelta, 1159);
    RUN_TEST(testINT32sWithinDeltaAndCustomMessage, 1166);
    RUN_TEST(testINT32sNotWithinDelta, 1171);
    RUN_TEST(testINT32sNotWithinDeltaAndCustomMessage, 1178);
    RUN_TEST(testINT16sWithinDelta, 1185);
    RUN_TEST(testINT16sWithinDeltaAndCustomMessage, 1192);
    RUN_TEST(testINT16sWithinDeltaWhenThereAreDifferenceOutsideOf16Bits, 1197);
    RUN_TEST(testINT16sWithinDeltaWhenThereAreDifferenceOutsideOf16BitsAndCustomMessage, 1202);
    RUN_TEST(testINT16sNotWithinDelta, 1207);
    RUN_TEST(testINT16sNotWithinDeltaAndCustomMessage, 1214);
    RUN_TEST(testINT8sWithinDelta, 1221);
    RUN_TEST(testINT8sWithinDeltaAndCustomMessage, 1228);
    RUN_TEST(testINT8sWithinDeltaWhenThereAreDifferenceOutsideOf8Bits, 1233);
    RUN_TEST(testINT8sWithinDeltaWhenThereAreDifferenceOutsideOf8BitsAndCustomMessage, 1238);
    RUN_TEST(testINT8sNotWithinDelta, 1243);
    RUN_TEST(testINT8sNotWithinDeltaAndCustomMessage, 1250);
*/
#ifndef __XC8
    RUN_TEST(testEqualStrings, 1257);
    RUN_TEST(testEqualStringsWithCarriageReturnsAndLineFeeds, 1268);
    RUN_TEST(testNotEqualString1, 1279);
    RUN_TEST(testNotEqualString2, 1286);
    RUN_TEST(testNotEqualString3, 1293);
    RUN_TEST(testNotEqualString4, 1300);
    RUN_TEST(testNotEqualString5, 1307);
    RUN_TEST(testNotEqualString_ExpectedStringIsNull, 1316);
    RUN_TEST(testNotEqualString_ActualStringIsNull, 1323);
    RUN_TEST(testEqualStringArrays, 1330);
    RUN_TEST(testNotEqualStringArray1, 1341);
    RUN_TEST(testNotEqualStringArray2, 1351);
    RUN_TEST(testNotEqualStringArray3, 1361);
    RUN_TEST(testNotEqualStringArray4, 1371);
    RUN_TEST(testNotEqualStringArray5, 1381);
    RUN_TEST(testNotEqualStringArray6, 1391);
    RUN_TEST(testEqualStringArrayIfBothNulls, 1401);
    RUN_TEST(testEqualMemory, 1409);
    RUN_TEST(testNotEqualMemory1, 1421);
    RUN_TEST(testNotEqualMemory2, 1428);
    RUN_TEST(testNotEqualMemory3, 1435);
    RUN_TEST(testNotEqualMemory4, 1442);
#endif
    RUN_TEST(testEqualIntArrays, 1449);
    RUN_TEST(testNotEqualIntArraysNullExpected, 1463);
    RUN_TEST(testNotEqualIntArraysNullActual, 1473);
    RUN_TEST(testNotEqualIntArrays1, 1483);
    RUN_TEST(testNotEqualIntArrays2, 1493);
    RUN_TEST(testNotEqualIntArrays3, 1503);
#ifndef __XC8
    RUN_TEST(testEqualPtrArrays, 1513);
    RUN_TEST(testNotEqualPtrArraysNullExpected, 1543);
    RUN_TEST(testNotEqualPtrArraysNullActual, 1558);
    RUN_TEST(testNotEqualPtrArrays1, 1573);
    RUN_TEST(testNotEqualPtrArrays2, 1596);
    RUN_TEST(testNotEqualPtrArrays3, 1619);
#endif
    RUN_TEST(testEqualInt8Arrays, 1642);
    RUN_TEST(testNotEqualInt8Arrays, 1656);
    RUN_TEST(testEqualUIntArrays, 1666);
    RUN_TEST(testNotEqualUIntArrays1, 1680);
    RUN_TEST(testNotEqualUIntArrays2, 1690);
    RUN_TEST(testNotEqualUIntArrays3, 1700);
    RUN_TEST(testEqualInt16Arrays, 1710);
    RUN_TEST(testNotEqualInt16Arrays, 1724);
/*
    RUN_TEST(testEqualInt32Arrays, 1734);
    RUN_TEST(testNotEqualInt32Arrays, 1748);
    RUN_TEST(testEqualUINT8Arrays, 1758);
    RUN_TEST(testNotEqualUINT8Arrays1, 1772);
    RUN_TEST(testNotEqualUINT8Arrays2, 1782);
    RUN_TEST(testNotEqualUINT8Arrays3, 1792);
*/
    RUN_TEST(testEqualUINT16Arrays, 1803);
    RUN_TEST(testNotEqualUINT16Arrays1, 1817);
    RUN_TEST(testNotEqualUINT16Arrays2, 1827);
    RUN_TEST(testNotEqualUINT16Arrays3, 1837);
/*
    RUN_TEST(testEqualUINT32Arrays, 1847);
    RUN_TEST(testNotEqualUINT32Arrays1, 1861);
    RUN_TEST(testNotEqualUINT32Arrays2, 1871);
    RUN_TEST(testNotEqualUINT32Arrays3, 1881);
*/
    RUN_TEST(testEqualHEXArrays, 1891);
    RUN_TEST(testNotEqualHEXArrays1, 1905);
    RUN_TEST(testNotEqualHEXArrays2, 1915);
    RUN_TEST(testNotEqualHEXArrays3, 1925);
/*
    RUN_TEST(testEqualHEX32Arrays, 1935);
    RUN_TEST(testNotEqualHEX32Arrays1, 1949);
    RUN_TEST(testNotEqualHEX32Arrays2, 1959);
    RUN_TEST(testNotEqualHEX32Arrays3, 1969);
*/
    RUN_TEST(testEqualHEX16Arrays, 1979);
    RUN_TEST(testNotEqualHEX16Arrays1, 1993);
    RUN_TEST(testNotEqualHEX16Arrays2, 2003);
    RUN_TEST(testNotEqualHEX16Arrays3, 2013);
    RUN_TEST(testEqualHEX8Arrays, 2023);
    RUN_TEST(testNotEqualHEX8Arrays1, 2037);
    RUN_TEST(testNotEqualHEX8Arrays2, 2047);
    RUN_TEST(testNotEqualHEX8Arrays3, 2057);
#ifndef __XC8
    RUN_TEST(testEqualMemoryArrays, 2067);
    RUN_TEST(testNotEqualMemoryArraysExpectedNull, 2081);
    RUN_TEST(testNotEqualMemoryArraysActualNull, 2091);
    RUN_TEST(testNotEqualMemoryArrays1, 2101);
    RUN_TEST(testNotEqualMemoryArrays2, 2111);
    RUN_TEST(testNotEqualMemoryArrays3, 2121);
#endif
    RUN_TEST(testProtection, 2131);
    RUN_TEST(testIgnoredAndThenFailInTearDown, 2149);
    RUN_TEST(testEqualHex64s, 2157);
/*
    RUN_TEST(testEqualUint64s, 2180);
    RUN_TEST(testEqualInt64s, 2203);
*/
    RUN_TEST(testNotEqualHex64s, 2227);
/*
    RUN_TEST(testNotEqualUint64s, 2243);
    RUN_TEST(testNotEqualInt64s, 2259);
*/
    RUN_TEST(testNotEqualHex64sIfSigned, 2275);
    RUN_TEST(testHEX64sWithinDelta, 2291);
    RUN_TEST(testHEX64sNotWithinDelta, 2302);
    RUN_TEST(testHEX64sNotWithinDeltaEvenThoughASignedIntWouldPass, 2313);
/*
    RUN_TEST(testUINT64sWithinDelta, 2324);
    RUN_TEST(testUINT64sNotWithinDelta, 2335);
    RUN_TEST(testUINT64sNotWithinDeltaEvenThoughASignedIntWouldPass, 2346);
    RUN_TEST(testINT64sWithinDelta, 2357);
    RUN_TEST(testINT64sNotWithinDelta, 2368);
*/
    RUN_TEST(testEqualHEX64Arrays, 2379);
/*
    RUN_TEST(testEqualUint64Arrays, 2397);
    RUN_TEST(testEqualInt64Arrays, 2415);
*/
    RUN_TEST(testNotEqualHEX64Arrays1, 2434);
    RUN_TEST(testNotEqualHEX64Arrays2, 2448);
/*
    RUN_TEST(testNotEqualUint64Arrays, 2462);
    RUN_TEST(testNotEqualInt64Arrays, 2476);
*/
/* The code will not use floats or doubles */
/* Floats and double are excluded so that it fits in the  available ROM */
#ifndef UNITY_EXCLUDE_FLOAT
    RUN_TEST(testFloatsWithinDelta, 2491);
    RUN_TEST(testFloatsNotWithinDelta, 2503);
    RUN_TEST(testFloatsEqual, 2514);
    RUN_TEST(testFloatsNotEqual, 2526);
    RUN_TEST(testFloatsNotEqualNegative1, 2537);
    RUN_TEST(testFloatsNotEqualNegative2, 2548);
/*
    RUN_TEST(testFloatsNotEqualActualNaN, 2559);
    RUN_TEST(testFloatsNotEqualExpectedNaN, 2570);
    RUN_TEST(testFloatsNotEqualBothNaN, 2581);
    RUN_TEST(testFloatsNotEqualInfNaN, 2592);
    RUN_TEST(testFloatsNotEqualNaNInf, 2603);
    RUN_TEST(testFloatsNotEqualActualInf, 2614);
    RUN_TEST(testFloatsNotEqualExpectedInf, 2625);
    RUN_TEST(testFloatsNotEqualBothInf, 2636);
    RUN_TEST(testFloatsNotEqualPlusMinusInf, 2647);
    RUN_TEST(testFloatIsPosInf1, 2658);
    RUN_TEST(testFloatIsPosInf2, 2667);
    RUN_TEST(testFloatIsNegInf1, 2678);
    RUN_TEST(testFloatIsNegInf2, 2687);
    RUN_TEST(testFloatIsNotPosInf1, 2698);
    RUN_TEST(testFloatIsNotPosInf2, 2709);
    RUN_TEST(testFloatIsNotNegInf, 2718);
    RUN_TEST(testFloatIsNan1, 2729);
    RUN_TEST(testFloatIsNan2, 2738);
    RUN_TEST(testFloatIsNotNan1, 2749);
    RUN_TEST(testFloatIsNotNan2, 2760);
    RUN_TEST(testFloatInfIsNotNan, 2769);
    RUN_TEST(testFloatNanIsNotInf, 2780);
    RUN_TEST(testFloatIsDeterminate1, 2791);
    RUN_TEST(testFloatIsDeterminate2, 2802);
    RUN_TEST(testFloatIsNotDeterminate1, 2813);
    RUN_TEST(testFloatIsNotDeterminate2, 2824);
*/
    RUN_TEST(testEqualFloatArrays, 2835);
    RUN_TEST(testNotEqualFloatArraysExpectedNull, 2853);
    RUN_TEST(testNotEqualFloatArraysActualNull, 2867);
    RUN_TEST(testNotEqualFloatArrays1, 2881);
    RUN_TEST(testNotEqualFloatArrays2, 2895);
    RUN_TEST(testNotEqualFloatArrays3, 2909);
    RUN_TEST(testNotEqualFloatArraysNegative1, 2923);
    RUN_TEST(testNotEqualFloatArraysNegative2, 2937);
    RUN_TEST(testNotEqualFloatArraysNegative3, 2951);
/*
    RUN_TEST(testNotEqualFloatArraysNaN, 2965);
    RUN_TEST(testNotEqualFloatArraysInf, 2979);
*/
#endif
#ifndef UNITY_EXCLUDE_DOUBLE
    RUN_TEST(testDoublesWithinDelta, 2995);
    RUN_TEST(testDoublesNotWithinDelta, 3007);
    RUN_TEST(testDoublesEqual, 3019);
    RUN_TEST(testDoublesNotEqual, 3031);
    RUN_TEST(testDoublesNotEqualNegative1, 3042);
    RUN_TEST(testDoublesNotEqualNegative2, 3053);
    RUN_TEST(testDoublesNotEqualActualNaN, 3064);
    RUN_TEST(testDoublesNotEqualExpectedNaN, 3075);
    RUN_TEST(testDoublesNotEqualBothNaN, 3086);
    RUN_TEST(testDoublesNotEqualInfNaN, 3097);
    RUN_TEST(testDoublesNotEqualNaNInf, 3108);
    RUN_TEST(testDoublesNotEqualActualInf, 3119);
    RUN_TEST(testDoublesNotEqualExpectedInf, 3130);
    RUN_TEST(testDoublesNotEqualBothInf, 3141);
    RUN_TEST(testDoublesNotEqualPlusMinusInf, 3152);
    RUN_TEST(testDoubleIsPosInf1, 3163);
    RUN_TEST(testDoubleIsPosInf2, 3172);
    RUN_TEST(testDoubleIsNegInf1, 3183);
    RUN_TEST(testDoubleIsNegInf2, 3192);
    RUN_TEST(testDoubleIsNotPosInf1, 3203);
    RUN_TEST(testDoubleIsNotPosInf2, 3214);
    RUN_TEST(testDoubleIsNotNegInf, 3223);
    RUN_TEST(testDoubleIsNan1, 3234);
    RUN_TEST(testDoubleIsNan2, 3243);
    RUN_TEST(testDoubleIsNotNan1, 3254);
    RUN_TEST(testDoubleIsNotNan2, 3265);
    RUN_TEST(testDoubleInfIsNotNan, 3274);
    RUN_TEST(testDoubleNanIsNotInf, 3285);
    RUN_TEST(testDoubleIsDeterminate1, 3296);
    RUN_TEST(testDoubleIsDeterminate2, 3307);
    RUN_TEST(testDoubleIsNotDeterminate1, 3318);
    RUN_TEST(testDoubleIsNotDeterminate2, 3329);
    RUN_TEST(testEqualDoubleArrays, 3340);
    RUN_TEST(testNotEqualDoubleArraysExpectedNull, 3358);
    RUN_TEST(testNotEqualDoubleArraysActualNull, 3372);
    RUN_TEST(testNotEqualDoubleArrays1, 3386);
    RUN_TEST(testNotEqualDoubleArrays2, 3400);
    RUN_TEST(testNotEqualDoubleArrays3, 3414);
    RUN_TEST(testNotEqualDoubleArraysNegative1, 3428);
    RUN_TEST(testNotEqualDoubleArraysNegative2, 3442);
    RUN_TEST(testNotEqualDoubleArraysNegative3, 3456);
    RUN_TEST(testNotEqualDoubleArraysNaN, 3470);
    RUN_TEST(testNotEqualDoubleArraysInf, 3484);
#endif
    testFailures = UnityEnd();

    return testFailures;
}

/**
 * @}
 * @}
 */
