/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @defgroup UnityUnitTest 
 *
 * @brief Unit Test for the @ref unity framework
 *
 */

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup UnityUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test runner for the @ref Unity
 * @author Rupert Menzies
 * @date 2015.07.29
 */
 
#ifndef TEST_UNITY_RUNNER_H
#define TEST_UNITY_RUNNER_H

/**
** Runs the unit tests for the unity framework
**/
int TestUnity(void);

#endif

/**
 * @}
 * @}
 */
