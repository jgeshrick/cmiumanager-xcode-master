echo off

echo Running pre-build checks in directory %cd%
echo.

set /a bad=0

echo listing dependencies - please check against latest versions
for /f %%f in ('dir /S /B pom.xml 2^>nul') do (
	echo   -- Inspecting file: %%f ...
	find ".version>" %%f | find /V "<!--"
)
echo.


echo checking for snapshot dependencies in pom.xml files
for /f %%f in ('dir /S /B pom.xml 2^>nul') do (
	echo   -- Inspecting file: %%f ...
	type %%f | find ".version>" | find "SNAPSHOT" | find /V "<!--" > sn.txt
	for %%S in (sn.txt) do if %%~zS gtr 0 (
		echo ** SNAPSHOT VERSIONS FOUND IN %%f **
		type sn.txt
		set /a bad = bad + 1
	)
	del sn.txt
)
echo.

echo checking for env files not "local-dev"
for /f %%f in ('dir /S /B mdce-env.properties 2^>nul ^| find /V "environments"  ^| find /V "target"') do (
	echo   -- Inspecting file: %%f ...	
	type %%f | find "env.name" | find /V "#" | find /V "local-dev" > sn.txt
	type %%f | find "aws.dynamo.endpoint" | find /V "#" | find /V "localhost" >> sn.txt
	for %%S in (sn.txt) do if %%~zS gtr 0 (
		echo ** PROBLEMS WITH %%f - env should be local-dev, dynamo should be pointing at localhost **
		type sn.txt
		set /a bad = bad + 1
	)
	
	del sn.txt
)
echo.


echo checking for AWS credentials
for /f %%f in ('dir /S /B *credentials.properties 2^>nul ^| find /V "target"') do (
	echo   -- Inspecting file: %%f ...
	type %%f | find "Key" | find /V "dummy" > sn.txt
	for %%S in (sn.txt) do if %%~zS gtr 0 (
		echo ** PROBLEMS WITH %%f - replace key values with "dummy" before commit **
		type %%f
		set /a bad = bad + 1
	)
	
	del sn.txt
)
echo.

echo checking for Java files without copyright
for /f %%f in ('dir /S /B *.java 2^>nul  ^| find /V "target"') do (

	find /C "Copyright" %%f | find ": 0" > sn.txt
	for %%S in (sn.txt) do if %%~zS gtr 0 (
		echo ** PROBLEM WITH %%f - no copyright **
		set /a bad = bad + 1
	)
	
	del sn.txt
)
echo.

echo %bad% problem files found
echo.

set ERRORLEVEL=%bad%
exit %bad%
