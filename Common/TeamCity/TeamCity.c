/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2013 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/
#include <stdio.h>
#include <string.h>

#include "TeamCity.h"
#include "unity.h"
#define TEAMCITY_MESSAGE_SIZE_MAX 256u

extern void setUp(void);
extern void tearDown(void);

////////////////////////////////////////////////////////////////////////////////
/// @addtogroup TeamCity
/// 
/// @{
////////////////////////////////////////////////////////////////////////////////
void TeamCityConcludeTest(const char* FuncName);
////////////////////////////////////////////////////////////////////////////////
///
/// Prints a standard TeamCity-formatted message to stdout.
///
/// @param [in] report 
///
/// @param [in] testName
///
/// @details This function should be used to generate special TeamCity markup
///          that TeamCity itself parse in order to report test results through
///          the web interface.  
///
////////////////////////////////////////////////////////////////////////////////
void PrintTeamCityTestMessage(char *report, char *testName)
{
    // Constant definitions
    const char *teamcityPrefix = "##teamcity[";
    const char *teamcitySuffix = "']\n";    
    const uint8_t prefixLength = strlen(teamcityPrefix);
    const uint8_t suffixLength = strlen(teamcitySuffix);
    
    // Local variables
    uint32_t reportLength;
    uint32_t nameLength;
    char messageBuffer[TEAMCITY_MESSAGE_SIZE_MAX];
    char *messageStart = messageBuffer;
    
    // Check size of input strings, return if there's an error
    reportLength = strlen(report);
    nameLength = strlen(testName);
    if(prefixLength + reportLength + nameLength + suffixLength >= TEAMCITY_MESSAGE_SIZE_MAX)
        return;

    // Generate the TeamCity message string
    strcpy(messageStart, teamcityPrefix);
    messageStart += prefixLength;
    strcpy(messageStart, report);
    messageStart += reportLength;
    strcpy(messageStart, testName);
    messageStart += nameLength;
    strcpy(messageStart, teamcitySuffix);

    // Print out the buffer contents using the macro so that printing can be
    // disabled at compile-time
    NT_TEAMCITY_PRINT(messageBuffer);
}

void TeamCityTestRun(TeamCityTestFunction Func, const char* FuncName, const int FuncLineNum)
{
    Unity.CurrentTestName = FuncName;
    Unity.CurrentTestLineNumber = FuncLineNum;
    Unity.NumberOfTests++; 
    NT_TEAMCITY_TEST_START(FuncName);
    if (TEST_PROTECT())
    {
        setUp();
        Func();
    }
    if (TEST_PROTECT() && !(Unity.CurrentTestIgnored))
    {
        tearDown();
    }
    TeamCityConcludeTest(FuncName);
    NT_TEAMCITY_TEST_FINISH(FuncName);
}

void TeamCityConcludeTest(const char* FuncName)
{
    if (Unity.CurrentTestIgnored)
    {
        Unity.TestIgnores++;
		NT_TEAMCITY_TEST_IGNORE(FuncName);
    }
    else if (!Unity.CurrentTestFailed)
    {
         // test passed so do nothing
    }
    else
    {
        Unity.TestFailures++;
        NT_TEAMCITY_TEST_FAIL(FuncName);
    }

    Unity.CurrentTestFailed = 0;
    Unity.CurrentTestIgnored = 0;
}

////////////////////////////////////////////////////////////////////////////////
/// @} // Close the Doxygen group.
////////////////////////////////////////////////////////////////////////////////
