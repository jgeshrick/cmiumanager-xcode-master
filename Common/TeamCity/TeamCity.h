/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2013 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

#ifndef TEAM_CITY_H
#define TEAM_CITY_H

#include "CommonTypes.h"

#ifdef INTEGRATION_TEST
#include "DebugUart.h"
#endif //INTEGRATION_TEST

////////////////////////////////////////////////////////////////////////////////
/// @addtogroup TeamCity
///
/// TeamCity integration functions and macros
///
/// @{
////////////////////////////////////////////////////////////////////////////////



/**
 * @file
 *
 * @brief API for Team City Unity Test helper module
 * @author Rohan Kamdar
 * @date 2013.04.23 (23rd April 2013)
 * @copyright Sagentia Limited, 2013
 * @version 1.0
 */

void PrintTeamCityTestMessage(char *report, char *testName);
typedef void (*TeamCityTestFunction)(void);
#define TRUE 1
#define FALSE 0

void TeamCityTestRun(TeamCityTestFunction Func, const char* FuncName, const int FuncLineNum);

// to run unity only, comment out this line.
#define TEAMCITY

////////////////////////////////////////////////////////////////////////////////
/// @brief Print macro that is disabled if TEAMCITY is not defined.  This
///        allows non-TeamCity testing to suppress printing TeamCity results
///        messages which easily clutter up the console.
////////////////////////////////////////////////////////////////////////////////
#ifdef TEAMCITY
   
   #ifdef INTEGRATION_TEST
        #define NT_TEAMCITY_PRINT(message)          DbgUart_PutStr(&messageBuffer); DbgUart_Continue();
    #else
        #define NT_TEAMCITY_PRINT(message)          ( printf(message) )
    #endif
    
    #undef RUN_TEST
    #define RUN_TEST(func, line_num) TeamCityTestRun(func, #func, line_num)
#else
    #define NT_TEAMCITY_PRINT(message)
#endif

////////////////////////////////////////////////////////////////////////////////
/// @brief Helper macros to generate the selection of test messages that
///        TeamCity can parse in order to display test results.
////////////////////////////////////////////////////////////////////////////////
#define NT_TEAMCITY_TEST_START(testName)    ( PrintTeamCityTestMessage( "testStarted name='", (char*)testName ) )
#define NT_TEAMCITY_TEST_FINISH(testName)   ( PrintTeamCityTestMessage( "testFinished name='", (char*)testName ) )
#define NT_TEAMCITY_TEST_IGNORE(testName)   ( PrintTeamCityTestMessage( "testIgnored name='", (char*)testName ) )
#define NT_TEAMCITY_TEST_FAIL(testName)     ( PrintTeamCityTestMessage( "testFailed name='", (char*)testName ) )
#define NT_TEST_START()                     ( NT_TEAMCITY_TEST_START(__FUNCTION__) )
#define NT_TEST_END()                       ( NT_TEAMCITY_TEST_FINISH(__FUNCTION__) )
#define NT_TEST_IGNORE()                    ( NT_TEAMCITY_TEST_IGNORE(__FUNCTION__) )


////////////////////////////////////////////////////////////////////////////////
/// @} // Close the Doxygen group.
////////////////////////////////////////////////////////////////////////////////

#endif
