/**************************************************************************//**
 * @file
 * @brief UART/LEUART/RS232 example for EFM32GG_DK3750 development kit
 * @version 3.20.12
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2014 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include <stdio.h>
#include <stdbool.h>
#include "em_device.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "bsp.h"
#include "bsp_trace.h"
#include "retargetserial.h"

/* Counts 1ms timeTicks */
volatile uint32_t msTicks;

/* Local prototypes */
void Delay(uint32_t dlyTicks);

/**************************************************************************//**
 * @brief SysTick_Handler
 * Interrupt Service Routine for system tick counter
 *****************************************************************************/
void SysTick_Handler(void)
{
    msTicks++;       /* increment counter necessary in Delay()*/
}

/**************************************************************************//**
 * @brief Delays number of msTick Systicks (typically 1 ms)
 * @param dlyTicks Number of ticks to delay
 *****************************************************************************/
void Delay(uint32_t dlyTicks)
{
    uint32_t curTicks;

    curTicks = msTicks;
    while ((msTicks - curTicks) < dlyTicks) ;
}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
    SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000);

    /* Initialize DK board register access */
    BSP_Init(BSP_INIT_DEFAULT);

    /* Initialize USART and map LF to CRLF */
    RETARGET_SerialInit();
    RETARGET_SerialCrLf(1);

    while (1)
    {
      BSP_LedsSet(0xff00);
      printf("Left\n");
      Delay(500);
      
      BSP_LedsSet(0x00ff);
      printf("Right\n");
      Delay(500);
    }
}
