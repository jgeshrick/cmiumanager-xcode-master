﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelHexFileHelper
{
    public class Constants
    {
        public const int RECORD_TYPE_DATA = 0;
        public const int RECORD_TYPE_EOF = 1;
        public const int RECORD_TYPE_EXTENDED_SEGMENT_ADDRESS = 2;
        public const int RECORD_TYPE_START_SEGMENT_ADDRESS = 3;
        public const int RECORD_TYPE_EXTENDED_LINEAR_ADDRESS = 4;
        public const int RECORD_TYPE_START_LINEAR_ADDRESS = 5;
        public const int RECORD_DATA_BYTES_PER_LINE = 16;
        public const string END_OF_FILE = ":00000001FF";
    }
}
