﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelHexFileHelper
{
    public class IntelHex
    {
        /*
         *  Intel Hex Format:
         *  :   10  0100    00  214601360121470136007EFE09D21901    40
         *  A   B    C      D                  E                    F
         *  
         *  A: Start Code
         *  B: Byte Count
         *  C: Address
         *  D: Record Type
         *  E: Data
         *  F: Checksum
         */

        public string StartCode = ":";
        public byte ByteCount;

        // NB. Address is NOT an absolute address but is relative to the address specified by the most recent
        // Extended Segment Address or Extended Linear Address address (or 0 if none has been found).
        public UInt16 Address;
        public byte RecordType;
        public List<byte> Data = new List<byte>();
        public byte LineChecksum;

        public IntelHex()
        {
        }

        /// <summary>
        /// This function creates a 'list of strings' representation of an Intel Hex file from a list
        /// of bytes representing the memory on thhe target device and an address range.
        /// Note that Extended Linear Address records are added as necessary to allow addresses above
        /// 0xFFFF to be used.
        /// </summary>
        /// <param name="startAddress">Address on target (not index into targetMemory)</param>
        /// <param name="bytesToCopy">Length of memory region</param>
        /// <param name="targetMemory">Complete target memory</param>
        /// <param name="targetMemoryStartAddress">Target address associated with start of memory area</param>
        /// <returns>List of string representing Intel Hex file</returns>
        public static List<String> CreateFile(Int32 startAddress, Int32 bytesToCopy, ref List<byte> targetMemory, Int32 targetMemoryStartAddress)
        {
            List<IntelHex> fileList = new List<IntelHex>();
            Int32 memoryAddress = startAddress;
            bool useExtendedLinearAddresses = false;

            // Only add Extended Linear Address record if we have max. address > 64k
            if ((startAddress + bytesToCopy) > 0x10000)
            {
                useExtendedLinearAddresses = true;
            }

            // Create a list of IntelHex objects representing the file content. We start with an Extended
            // Linear Address record (if required) and follow with Data records and any further Extended  
            // Linear Address records as required.
            while (bytesToCopy > 0)
            {
                // Address in file is represented as 4 bytes (0x0000 to 0xFFFF). We use Extended Linear
                // Address records to support addresses out of this range.
                UInt16 fileAddress = (UInt16)(memoryAddress % 0x10000);

                if (useExtendedLinearAddresses)
                {
                    // Second test forces Extended Linear Address record on first line
                    if ((fileAddress == 0) || (fileList.Count == 0))
                    {
                        IntelHex baseAddressLine = new IntelHex();
                        baseAddressLine.Address = 0;
                        baseAddressLine.ByteCount = 2;
                        baseAddressLine.RecordType = Constants.RECORD_TYPE_EXTENDED_LINEAR_ADDRESS;

                        // Upper 16 bits as big-endian
                        UInt16 upper16Bits = (UInt16)(memoryAddress >> 16);
                        byte[] lengthBytes = BitConverter.GetBytes(upper16Bits);
                        Array.Reverse(lengthBytes);

                        baseAddressLine.Data.AddRange(lengthBytes);

                        // Add checksum
                        baseAddressLine.LineChecksum = Checksum.CalculateChecksum(baseAddressLine);

                        // Add to file
                        fileList.Add(baseAddressLine);
                    }
                }

                IntelHex line = new IntelHex();

                line.RecordType = Constants.RECORD_TYPE_DATA;
                line.Address = fileAddress;
                line.ByteCount = Constants.RECORD_DATA_BYTES_PER_LINE;
                if (bytesToCopy < Constants.RECORD_DATA_BYTES_PER_LINE)
                {
                    line.ByteCount = (byte)bytesToCopy;
                }

                for (byte dataCount = 0; dataCount < line.ByteCount; dataCount++)
                {
                    line.Data.Add(targetMemory[memoryAddress - targetMemoryStartAddress]);
                    ++memoryAddress;

                    // If necessary truncate this record such that we will recognise an
                    // address rollover and insert an Extended Linear Address record (note
                    // that this is to handle the case where the combination of start address 
                    // and number of bytes per line means that the address of the first
                    // byte on the line will not result in a fileAddress of 0).
                    if ((memoryAddress % 0x10000) == 0)
                    {
                        line.ByteCount = (byte)(dataCount + 1);
                        break;
                    }
                }

                // Generate checksum and append IntelHex object to file
                line.LineChecksum = Checksum.CalculateChecksum(line);
                fileList.Add(line);

                bytesToCopy -= line.ByteCount;
            }

            List<string> strList = ParseIntelHexToString(fileList);

            // Finally add the EOF marker
            strList.Add(Constants.END_OF_FILE);

            return strList;
        }


        /// <summary>
        /// This function parses a string file to a list of IntelHex objects
        /// </summary>
        /// <param name="strFile">list of strings where each entry is a line from an Intel Hex format file</param>
        /// <returns>List of IntelHex objects</returns>
        public static List<IntelHex> ParseToIntelHex(List<string> strFile)
        {
            List<IntelHex> ParsedFile = new List<IntelHex>();
            foreach (string strLine in strFile)
            {
                IntelHex ihLine = ConvertToIntelHexLine(strLine);
                ParsedFile.Add(ihLine);
            }

            return ParsedFile;
        }

        /// <summary>
        /// Converts a line from an Intel Hex format file to an IntelHex object
        /// </summary>
        /// <param name="str_line">Line from Intel Hex format file</param>
        /// <returns>IntelHex object</returns>
        private static IntelHex ConvertToIntelHexLine(string str_line)
        {
            const int STARTCODE_INDEX = 0;
            const int BYTECOUNT_INDEX = 1;
            const int ADDRESS_INDEX = 3;
            const int RECORDTYPE_INDEX = 7;
            const int DATA_INDEX = 9;

            const int HEX_INPUT = 16;

            IntelHex ih_line = new IntelHex();

            try
            {
                // Start Code
                ih_line.StartCode = str_line.Substring(STARTCODE_INDEX, 1);
                // Byte Count
                ih_line.ByteCount = Convert.ToByte(str_line.Substring(BYTECOUNT_INDEX, 2), HEX_INPUT);
                // Address
                ih_line.Address = Convert.ToUInt16(str_line.Substring(ADDRESS_INDEX, 4), HEX_INPUT);
                // Record Type
                ih_line.RecordType = Convert.ToByte(str_line.Substring(RECORDTYPE_INDEX, 2), HEX_INPUT);
                // Data
                // Set the index used to find the byte to add
                int dataindex = DATA_INDEX;
                // Move through the data adding a byte at a time for the number of bytes
                for (int bycount = 0; bycount < ih_line.ByteCount; bycount++)
                {
                    ih_line.Data.Add(Convert.ToByte(str_line.Substring(dataindex, 2), HEX_INPUT));
                    // Increase substring index by 2, the byte width
                    dataindex += 2;
                }
                // Checksum
                // dataindex now points at the checksum
                ih_line.LineChecksum = Convert.ToByte(str_line.Substring(dataindex, 2), HEX_INPUT);
            }
            catch (Exception ex)
            {
                //Error.Error_Found = true;
                //Error.Error_Message = "Error when converting file to intel hex format. Details: " + ex.Message;
            }

            return ih_line;
        }


        /// <summary>
        /// This function converts an IntelHex object list into a string list
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private static List<string> ParseIntelHexToString(List<IntelHex> file)
        {
            List<string> ParsedFile = new List<string>();

            foreach (IntelHex ihline in file)
            {
                // Only use lower 16 bits of address
                UInt16 address = (UInt16)(ihline.Address % 0x10000);

                string strline = "";
                strline += ihline.StartCode;
                strline += ihline.ByteCount.ToString("X2");

                strline += address.ToString("X4");
                strline += ihline.RecordType.ToString("X2");
                foreach (byte data_byte in ihline.Data)
                {
                    strline += data_byte.ToString("X2");
                }
                strline += ihline.LineChecksum.ToString("X2");
                ParsedFile.Add(strline);
            }

            return ParsedFile;
        }
    }
}
