﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelHexFileHelper
{
    public class Checksum
    {
        public static byte CalculateChecksum(IntelHex line)
        {
            int CalculatedChecksum = 0;

            // Byte count
            CalculatedChecksum += line.ByteCount;
            // Address
            byte[] addressbytes = BitConverter.GetBytes(line.Address);
            CalculatedChecksum += addressbytes[0];
            CalculatedChecksum += addressbytes[1];
            // Record Type
            CalculatedChecksum += line.RecordType;
            // Data            
            foreach (byte data_byte in line.Data)
            {
                CalculatedChecksum += data_byte;
            }

            CalculatedChecksum &= 0xFF;

            CalculatedChecksum = (0x100 - CalculatedChecksum);

            return (byte)CalculatedChecksum;
        }
    }
}
