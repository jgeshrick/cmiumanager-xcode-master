/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;


/**
 * Test SQS services with local SQS mode set to true. This will bypass AWS SQS and use our own stub SQS instead.
 */
public class SqsMdceIpcPublishServiceLocalTest
{
    private static final String queueName = "test-message";

    private SqsMdceIpcPublishService awsPubQueueService;
    private SqsMdceIpcSubscribeService awsSubQueueService;

    @Before
    public void setUp() throws Exception
    {
        final Properties junitEnvProperties = new Properties();
        junitEnvProperties.setProperty("env.name", "dev0");
        junitEnvProperties.setProperty("mdce.scheduler.localMode", "true");

        AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider("/dev/aws-local-credential.properties");

        awsPubQueueService = new SqsMdceIpcPublishService(credentialsProvider, junitEnvProperties);
        awsSubQueueService = new SqsMdceIpcSubscribeService(credentialsProvider, junitEnvProperties);
    }

    @Test
    public void testSendMessage() throws Exception
    {
        TestAwsQueueReceiver testReceiver1 = new TestAwsQueueReceiver();
        awsSubQueueService.register(testReceiver1, queueName, Duration.ZERO, 100);
        testReceiver1.setMessage("");

        final String messageToSend = "unit test message. create time: " + LocalDateTime.now();

        System.out.println("-> " + messageToSend);

        awsPubQueueService.sendMessage(queueName, messageToSend);

        //we have to do a few polls as first poll might not get the message
        final Instant timeout = Instant.now().plus(Duration.ofSeconds(1L));
        while (Instant.now().isBefore(timeout))
        {
            if (messageToSend.equals(testReceiver1.getMessage()))
            {
                break;
            }
            Thread.sleep(10L);
        }

        assertEquals(messageToSend, testReceiver1.getMessage());

    }

    /**
     * A queue observer for the unit test
     */
    private class TestAwsQueueReceiver implements AwsQueueReceiver
    {
        private volatile String message;

        @Override
        public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
        {
            this.message = message;
            System.out.println("<- " + message);

            return true;
        }

        public String getMessage()
        {
            return message;
        }

        public void setMessage(String message)
        {
            this.message = message;
        }
    }
}