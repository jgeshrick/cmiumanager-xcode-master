/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.*;


/**
 * Test SQS services.
 */
@Ignore("Need to access AWS SQS service")
public class AwsQueueServiceImplTest
{
    private static final String queueName = "test-message";

    MdceIpcSubscribeService awsSubQueueService;
    MdceIpcPublishService awsPubQueueService;

    @Before
    public void setUp() throws Exception
    {
        final Properties junitEnvProperties = new Properties();
        junitEnvProperties.setProperty("env.name", "dev0");

        AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider("/dev/aws-local-credential.properties");

        awsSubQueueService = new SqsMdceIpcSubscribeService(credentialsProvider, junitEnvProperties);
        awsPubQueueService = new SqsMdceIpcPublishService(credentialsProvider, junitEnvProperties);
    }

    @Test
    public void testRegisterUnregisterObserver()
    {
        TestAwsQueueReceiver testReceiver1 = new TestAwsQueueReceiver();
        TestAwsQueueReceiver testReceiver2 = new TestAwsQueueReceiver();

        assertFalse(awsSubQueueService.isRegistered(testReceiver1, queueName));
        assertFalse(awsSubQueueService.isRegistered(testReceiver2, queueName));

        awsSubQueueService.register(testReceiver1, queueName, Duration.of(100L, ChronoUnit.DAYS), 100);
        assertTrue(awsSubQueueService.isRegistered(testReceiver1, queueName));
        assertFalse(awsSubQueueService.isRegistered(testReceiver2, queueName));

        assertFalse(awsSubQueueService.isRegistered(testReceiver1, "unregistered-queue"));


        assertTrue(awsSubQueueService.isRegistered(testReceiver1, queueName));

        awsSubQueueService.unregister(queueName);
        assertFalse(awsSubQueueService.isRegistered(testReceiver1, queueName));
        assertFalse(awsSubQueueService.isRegistered(testReceiver2, queueName));
    }

    @Test
    public void testRegisterSecondObserverReplacesLastObserver()
    {
        TestAwsQueueReceiver testReceiver1 = new TestAwsQueueReceiver();
        TestAwsQueueReceiver testReceiver2 = new TestAwsQueueReceiver();

        awsSubQueueService.register(testReceiver1, queueName, Duration.of(100L, ChronoUnit.DAYS), 100);
        assertTrue(awsSubQueueService.isRegistered(testReceiver1, queueName));
        assertFalse(awsSubQueueService.isRegistered(testReceiver2, queueName));

        awsSubQueueService.register(testReceiver2, queueName, Duration.of(100L, ChronoUnit.DAYS), 100);
        assertFalse(awsSubQueueService.isRegistered(testReceiver1, queueName));
        assertTrue(awsSubQueueService.isRegistered(testReceiver2, queueName));

        awsSubQueueService.unregister(queueName);
        assertFalse(awsSubQueueService.isRegistered(testReceiver1, queueName));
        assertFalse(awsSubQueueService.isRegistered(testReceiver2, queueName));
    }


    @Test
    public void testSendMessage() throws Exception
    {
        TestAwsQueueReceiver testReceiver1 = new TestAwsQueueReceiver();
        awsSubQueueService.register(testReceiver1, queueName, Duration.ZERO, 100);
        testReceiver1.setMessage("");

        final String messageToSend = "unit test message. create time: " + LocalDateTime.now();

        System.out.println("-> " + messageToSend);

        String messageId = awsPubQueueService.sendMessage(queueName, messageToSend);

        //Wait for the polling thread to have a chance to run
        Thread.sleep(100L);

        assertEquals(messageToSend, testReceiver1.getMessage());
        assertEquals(messageId, testReceiver1.getMessageId());

    }

    /**
     * A queue observer for the unit test
     */
    private class TestAwsQueueReceiver implements AwsQueueReceiver
    {
        private String message;
        private String messageId;

        @Override
        public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
        {
            this.message = message;
            this.messageId = messageId;

            System.out.println("<- " + message);

            return true;
        }

        public String getMessage()
        {
            return message;
        }

        public void setMessage(String message)
        {
            this.message = message;
        }

        public String getMessageId()
        {
            return messageId;
        }
    }


}