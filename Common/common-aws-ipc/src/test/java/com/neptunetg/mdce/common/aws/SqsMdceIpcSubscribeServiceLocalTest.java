/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Test SQS services with local SQS mode set to true. This will bypass AWS SQS and use our own stub SQS instead.
 */
public class SqsMdceIpcSubscribeServiceLocalTest
{
    private static final String queueName = "test-message";

    private SqsMdceIpcSubscribeService awsQueueService;

    @Before
    public void setUp() throws Exception
    {
        final Properties junitEnvProperties = new Properties();
        junitEnvProperties.setProperty("env.name", "dev0");
        junitEnvProperties.setProperty("mdce.scheduler.localMode", "true");

        AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider("/dev/aws-local-credential.properties");

        awsQueueService = new SqsMdceIpcSubscribeService(credentialsProvider, junitEnvProperties);
    }

    @Test
    public void testRegisterUnregisterObserver() throws Exception
    {
        TestAwsQueueReceiver testReceiver1 = new TestAwsQueueReceiver();
        TestAwsQueueReceiver testReceiver2 = new TestAwsQueueReceiver();

        assertFalse(awsQueueService.isRegistered(testReceiver1, queueName));
        assertFalse(awsQueueService.isRegistered(testReceiver2, queueName));

        awsQueueService.register(testReceiver1, queueName, Duration.of(100L, ChronoUnit.DAYS), 100);
        assertTrue(awsQueueService.isRegistered(testReceiver1, queueName));
        assertFalse(awsQueueService.isRegistered(testReceiver2, queueName));

        assertFalse(awsQueueService.isRegistered(testReceiver1, "unregistered-queue"));


        assertTrue(awsQueueService.isRegistered(testReceiver1, queueName));

        awsQueueService.unregister(queueName);

        //we have to do a few polls as first poll might not get the message
        Thread.sleep(100L);

        assertFalse(awsQueueService.isRegistered(testReceiver1, queueName));
        assertFalse(awsQueueService.isRegistered(testReceiver2, queueName));
    }

    @Test
    public void testRegisterSecondObserverReplacesLastObserver()
    {
        TestAwsQueueReceiver testReceiver1 = new TestAwsQueueReceiver();
        TestAwsQueueReceiver testReceiver2 = new TestAwsQueueReceiver();

        awsQueueService.register(testReceiver1, queueName, Duration.of(100L, ChronoUnit.DAYS), 100);
        assertTrue(awsQueueService.isRegistered(testReceiver1, queueName));
        assertFalse(awsQueueService.isRegistered(testReceiver2, queueName));

        awsQueueService.register(testReceiver2, queueName, Duration.of(100L, ChronoUnit.DAYS), 100);
        assertFalse(awsQueueService.isRegistered(testReceiver1, queueName));
        assertTrue(awsQueueService.isRegistered(testReceiver2, queueName));

        awsQueueService.unregister(queueName);
        assertFalse(awsQueueService.isRegistered(testReceiver1, queueName));
        assertFalse(awsQueueService.isRegistered(testReceiver2, queueName));
    }

    /**
     * A queue observer for the unit test
     */
    private class TestAwsQueueReceiver implements AwsQueueReceiver
    {
        private String message;

        @Override
        public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
        {
            this.message = message;
            System.out.println("<- " + message);

            return true;
        }

        public String getMessage()
        {
            return message;
        }

        public void setMessage(String message)
        {
            this.message = message;
        }
    }
}