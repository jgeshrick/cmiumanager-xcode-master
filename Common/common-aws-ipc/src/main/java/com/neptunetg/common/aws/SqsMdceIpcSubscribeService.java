/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.text.ParseException;
import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Allows client code to subscribe to SQS queues.  Clients must poll on their own schedule
 */
@Service
public class SqsMdceIpcSubscribeService extends SqsMdceIpcServiceBase implements MdceIpcSubscribeService
{
    private static final Logger log = LoggerFactory.getLogger(SqsMdceIpcSubscribeService.class);

    @Autowired
    public SqsMdceIpcSubscribeService(AWSCredentialsProvider credentialsProvider,
                                      @Qualifier("envProperties") Properties envProperties) throws ParseException
    {
        super(credentialsProvider, envProperties);
    }


    @Override
    public void register(AwsQueueReceiver observer, String queueName, Duration pollInterval, int messageBatchSize)
    {

        log.info("Subscribing to SQS queue {} to process {} messages every {}, observer {}", queueName, messageBatchSize, observer, pollInterval);

        SqsQueue awsQueue = queueMap.getOrDefault(queueName, null);

        //create a queue if it is not present
        if (awsQueue == null)
        {
            log.info("Subscribing to SQS queue {} which does not exist.  Creating queue.", queueName);
            awsQueue = new SqsQueue(queueName, getFullyQualifiedQueueName(envName, queueName), amazonSQS);
            queueMap.put(queueName, awsQueue);
        }

        awsQueue.startPolling(observer, pollInterval, messageBatchSize);
    }

    @Override
    public void unregister(String queueName)
    {
        log.info("Unsubscribing from SQS queue {}", queueName);
        final SqsQueue awsQueue = queueMap.get(queueName);
        awsQueue.stopPolling();
        log.info("Successfully unsubscribed from SQS queue {}", queueName);
    }

    @Override
    public boolean isRegistered(AwsQueueReceiver observer, String queueName)
    {
        final SqsQueue awsQueue = queueMap.getOrDefault(queueName, null);

        return (awsQueue != null) && (awsQueue.getObserver() == observer);
    }


    @PreDestroy
    public void dispose()
    {
        log.info("Shutting down SqsMdceIpcSubscribeService.  Stopping polling all queues.");
        synchronized (this.queueMap)
        {
            this.queueMap.keySet().parallelStream().forEach(queueName -> this.unregister(queueName));
        }
    }
}
