/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.aws;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A SQS queue observer for a SQS queue.
 */
public interface AwsQueueReceiver
{
    /**
     * Handler called by MdceIpcSubscribeService to process a SQS message
     * @param message the message body
     * @param messageId the message id
     * @param messageReceipt message receipt
     * @param cancellationToken If the message handler is long-running, it should check this object from time to time.  If true,
     *                          it should stop doing what it is doing and return false.
     * @return if return true, the message will be deleted, else message will not be deleted from SQS.
     */
    boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken);

}
