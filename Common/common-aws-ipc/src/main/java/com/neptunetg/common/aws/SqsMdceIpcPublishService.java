/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015, 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.sqs.model.PurgeQueueRequest;
import com.amazonaws.services.sqs.model.QueueDoesNotExistException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Properties;

/**
 * Encapsulate Aws SQS service and sending message to a queue.
 */
@Service
public class SqsMdceIpcPublishService extends SqsMdceIpcServiceBase implements MdceIpcPublishService
{
    private static final Logger log = LoggerFactory.getLogger(SqsMdceIpcPublishService.class);

    @Autowired
    public SqsMdceIpcPublishService(AWSCredentialsProvider credentialsProvider,
                                    @Qualifier("envProperties") Properties envProperties) throws ParseException
    {
        super(credentialsProvider, envProperties);
    }

    @Override
    public String sendMessage(String queueName, String message)
    {
        SqsQueue awsQueue = queueMap.getOrDefault(queueName, null);

        //create a queue if it is not present
        if (awsQueue == null)
        {

            log.info("Message queued to SQS queue {} which does not exist.  Creating queue.", queueName);
            awsQueue = new SqsQueue(queueName, getFullyQualifiedQueueName(envName, queueName), amazonSQS);
            queueMap.put(queueName, awsQueue);
        }

        final String ret = awsQueue.sendMessage(message);
        log.debug("Message queued to SQS queue {}, message: {}", queueName, message);
        return ret;
    }

    /**
     * Only one PurgeQueue operation on a queue is allowed every 60 seconds
     *
     * @param queueName name of the queue
     */
    @Override
    public void purgeQueue(String queueName)
    {
        try
        {
            PurgeQueueRequest purgeQueueRequest = new PurgeQueueRequest(getQueueUrl(queueName));
            amazonSQS.purgeQueue(purgeQueueRequest);
        }
        catch (QueueDoesNotExistException ex)
        {
            log.debug("Non-existent queue " + queueName + " at purge", ex);
        }
    }


}
