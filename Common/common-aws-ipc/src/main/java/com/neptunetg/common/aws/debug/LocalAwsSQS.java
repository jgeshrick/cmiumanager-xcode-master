/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.aws.debug;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonWebServiceRequest;
import com.amazonaws.ResponseMetadata;
import com.amazonaws.regions.Region;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.AddPermissionRequest;
import com.amazonaws.services.sqs.model.ChangeMessageVisibilityBatchRequest;
import com.amazonaws.services.sqs.model.ChangeMessageVisibilityBatchRequestEntry;
import com.amazonaws.services.sqs.model.ChangeMessageVisibilityBatchResult;
import com.amazonaws.services.sqs.model.ChangeMessageVisibilityRequest;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.DeleteMessageBatchRequest;
import com.amazonaws.services.sqs.model.DeleteMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.DeleteMessageBatchResult;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.ListDeadLetterSourceQueuesRequest;
import com.amazonaws.services.sqs.model.ListDeadLetterSourceQueuesResult;
import com.amazonaws.services.sqs.model.ListQueuesRequest;
import com.amazonaws.services.sqs.model.ListQueuesResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.PurgeQueueRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.amazonaws.services.sqs.model.RemovePermissionRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.SendMessageBatchResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.time.Instant;
import java.util.*;

/**
 * A stub SQS with minimum function implemented for local testing purpose.
 */
public class LocalAwsSQS implements AmazonSQS
{
    private static final Logger log = LoggerFactory.getLogger(LocalAwsSQS.class);

    private static LocalAwsSQS instance;
    //Local SQS exchanges
    private final File messageExchangeFile;
    private final ObjectMapper jackson = new ObjectMapper();


    private LocalAwsSQS(String folderForMessageFile)
    {
        this.messageExchangeFile = new File(folderForMessageFile, "LocalAwsSQS.dat");
        log.info("Local AWS fake SQS exchanging messages via file " + this.messageExchangeFile.getAbsolutePath());
    }

    public static synchronized LocalAwsSQS createOrGetExisting(String folderForMessageFile)
    {
        if (instance == null) {
            instance = new LocalAwsSQS(folderForMessageFile);
        }
        return instance;
    }

    private synchronized LockedFileInfo lockAndLoadQueues()
    {
        log.trace("  Loading SQS file " + this.messageExchangeFile);
        RandomAccessFile openFile = null;
        FileLock fileLock = null;
        try
        {
            if (!messageExchangeFile.exists())
            {
                final Map<String, List<Map<String, String>>> ret = new HashMap<>();
                jackson.writeValue(messageExchangeFile, ret);
            }
            openFile = new RandomAccessFile(messageExchangeFile, "rws");
            boolean waitedToLock = false;
            boolean warnedOnWait = false;
            long waitedSince = 0L;
            while (true)
            {
                String waitedMessage;
                try
                {
                    fileLock = openFile.getChannel().tryLock();
                    if (fileLock != null)
                    {
                        break;
                    }
                    waitedMessage = "  Waiting to lock file " + this.messageExchangeFile.getAbsolutePath();
                }
                catch (OverlappingFileLockException e)
                {
                    waitedMessage = "  Waiting to lock file " + this.messageExchangeFile.getAbsolutePath() + "; tryLock threw " + e;
                }

                if (!waitedToLock)
                {
                    waitedToLock = true;
                    waitedSince = System.currentTimeMillis();
                }
                else if (!warnedOnWait && System.currentTimeMillis() > waitedSince + 500L)
                {
                    log.warn(waitedMessage);
                    warnedOnWait = true;
                }

                try
                {
                    Thread.sleep(100L);
                }
                catch (InterruptedException e)
                {
                    log.warn("  Sleep interrupted... continuing");
                }
            }
            if (warnedOnWait)
            {
                log.warn("  Successfully locked file " + this.messageExchangeFile.getAbsolutePath() + " after waiting " + (System.currentTimeMillis() - waitedSince) / 1000.0 + " seconds");
            }

            final byte[] rawData = new byte[(int) openFile.length()];
            final Map<String, List<Map<String, String>>> ret;
            if (rawData.length == 0)
            {
                ret = new HashMap<>();
            }
            else
            {
                openFile.readFully(rawData);
                ret = jackson.readValue(rawData, Map.class);
            }

            if (ret.size() > 0 && log.isTraceEnabled())
            {
                final StringBuilder logMessage = new StringBuilder("  Loaded ").append(ret.size()).append(" queues from SQS file ").append(this.messageExchangeFile);
                appendQueueSizeInfo(logMessage, ret);
                log.trace(logMessage.toString());
            }

            return new LockedFileInfo(openFile, fileLock, ret);
        }
        catch (Exception e)
        {
            if (fileLock != null)
            {
                try
                {
                    fileLock.close();
                }
                catch (Exception e2)
                {
                    log.error("  Error releasing file lock " + messageExchangeFile, e);
                }

            }
            if (openFile != null)
            {
                try
                {
                    openFile.close();
                }
                catch (Exception e2)
                {
                    log.error("  Error closing file " + messageExchangeFile, e);
                }
                openFile = null;
            }
            throw new RuntimeException("Can't read SQS file " + messageExchangeFile + "!", e);
        }
    }

    private void appendQueueSizeInfo(StringBuilder logMessage, Map<String, List<Map<String, String>>> newQueueData)
    {
        for (String queueName : newQueueData.keySet())
        {
            logMessage.append(", ").append(queueName).append(": ").append(newQueueData.get(queueName).size()).append(" messages");
        }
    }


    private Message mapToMessage(Map<String, String> m)
    {
        final Message message = new Message();
        message.setMessageId(m.get("messageId"));
        message.setBody(m.get("messageBody"));
        message.setReceiptHandle(m.get("receiptHandle"));
        return message;
    }


    private Map<String, String> messageToMap(Message message)
    {
        final Map<String, String> m = new HashMap<>();
        m.put("messageId", message.getMessageId());
        m.put("messageBody", message.getBody());
        m.put("receiptHandle", message.getReceiptHandle());
        return m;
    }


    @Override
    public void setEndpoint(String endpoint) throws IllegalArgumentException
    {
        //do nothing
    }

    @Override
    public void setRegion(Region region) throws IllegalArgumentException
    {
        //do nothing
    }

    @Override
    public void setQueueAttributes(SetQueueAttributesRequest setQueueAttributesRequest) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public ChangeMessageVisibilityBatchResult changeMessageVisibilityBatch(ChangeMessageVisibilityBatchRequest changeMessageVisibilityBatchRequest) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public void changeMessageVisibility(ChangeMessageVisibilityRequest changeMessageVisibilityRequest) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public GetQueueUrlResult getQueueUrl(GetQueueUrlRequest getQueueUrlRequest) throws AmazonClientException
    {
        GetQueueUrlResult result = new GetQueueUrlResult();
        result.setQueueUrl(getQueueUrlRequest.getQueueName());
        return result;
    }

    @Override
    public void removePermission(RemovePermissionRequest removePermissionRequest) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public GetQueueAttributesResult getQueueAttributes(GetQueueAttributesRequest getQueueAttributesRequest) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public SendMessageBatchResult sendMessageBatch(SendMessageBatchRequest sendMessageBatchRequest) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public void purgeQueue(PurgeQueueRequest purgeQueueRequest) throws AmazonClientException
    {
        try (final LockedFileInfo file = lockAndLoadQueues())
        {
            file.queues.get(purgeQueueRequest.getQueueUrl()).clear();
        }
    }

    @Override
    public ListDeadLetterSourceQueuesResult listDeadLetterSourceQueues(ListDeadLetterSourceQueuesRequest listDeadLetterSourceQueuesRequest) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public void deleteQueue(DeleteQueueRequest deleteQueueRequest) throws AmazonClientException
    {
        try (final LockedFileInfo file = lockAndLoadQueues())
        {
            file.queues.remove(deleteQueueRequest.getQueueUrl());
        }
    }

    @Override
    public SendMessageResult sendMessage(SendMessageRequest sendMessageRequest) throws AmazonClientException
    {
        String queueUrl = sendMessageRequest.getQueueUrl();
        String messageId = queueUrl + "-" + Instant.now();

        log.info("Sending message with ID " + messageId + " to queue " + queueUrl);

        Message message = new Message();
        message.setMessageId(messageId);
        message.setBody(sendMessageRequest.getMessageBody());
        message.setReceiptHandle(messageId);

        try (final LockedFileInfo file = lockAndLoadQueues())
        {
            file.queues.get(queueUrl).add(messageToMap(message));
        }

        SendMessageResult result = new SendMessageResult();
        result.setMessageId(messageId);

        log.info("Successfully sent message with ID " + messageId + " to queue " + queueUrl);

        return result;
    }

    @Override
    public ReceiveMessageResult receiveMessage(ReceiveMessageRequest receiveMessageRequest) throws AmazonClientException
    {
        Integer maxMessagesInteger = receiveMessageRequest.getMaxNumberOfMessages();
        if (maxMessagesInteger != null && maxMessagesInteger.intValue() > 10)
        {
            throw new IllegalArgumentException("max messages cannot be > 10 in ReceiveMessageRequest");
        }

        final String queueUrl = receiveMessageRequest.getQueueUrl();
        log.trace("Checking for messages on queue " + queueUrl);

        ReceiveMessageResult result = new ReceiveMessageResult();
        try (final LockedFileInfo file = lockAndLoadQueues())
        {
            final List<Message> newMessages = new ArrayList<>();

            int messagesToGet = maxMessagesInteger == null ? 10 : maxMessagesInteger.intValue();

            for (Map<String, String> m : file.queues.get(queueUrl))
            {
                final Message message = mapToMessage(m);
                log.debug("Received new message with ID " + message.getMessageId() + " on queue " + queueUrl);
                newMessages.add(message);
                messagesToGet--;
                if (messagesToGet <= 0)
                {
                    break;
                }
            }
            final int size = newMessages.size();
            if (size > 0 && log.isDebugEnabled())
            {
                log.debug("Received " + size + " messages on queue " + queueUrl);
            }
            result.setMessages(newMessages);
        }

        return result;
    }

    @Override
    public ListQueuesResult listQueues(ListQueuesRequest listQueuesRequest) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public DeleteMessageBatchResult deleteMessageBatch(DeleteMessageBatchRequest deleteMessageBatchRequest) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public CreateQueueResult createQueue(CreateQueueRequest createQueueRequest) throws AmazonClientException
    {
        final String queueName = createQueueRequest.getQueueName();
        log.debug("Creating queue " + queueName);

        CreateQueueResult result = new CreateQueueResult();
        result.setQueueUrl(queueName);
        try (final LockedFileInfo file = lockAndLoadQueues())
        {
            if (!file.queues.containsKey(queueName))
            {
                file.queues.put(queueName, new ArrayList<>());
            }
        }

        log.debug("Successfully created queue " + queueName);

        return result;
    }

    @Override
    public void addPermission(AddPermissionRequest addPermissionRequest) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public void deleteMessage(DeleteMessageRequest deleteMessageRequest) throws AmazonClientException
    {
        log.debug("Deleting messages from queue " + deleteMessageRequest.getQueueUrl());

        try (final LockedFileInfo file = lockAndLoadQueues())
        {
            List<Map<String, String>> messageList = file.queues.get(deleteMessageRequest.getQueueUrl());

            for (Iterator<Map<String, String>> entryIterator = messageList.iterator(); entryIterator.hasNext(); )
            {
                Map<String, String> message = entryIterator.next();
                if (deleteMessageRequest.getReceiptHandle().equals(mapToMessage(message).getReceiptHandle()))
                {
                    entryIterator.remove();
                }
            }
        }

        log.debug("Finished deleting messages from queue " + deleteMessageRequest.getQueueUrl());

    }

    @Override
    public ListQueuesResult listQueues() throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public void setQueueAttributes(String queueUrl, Map<String, String> attributes) throws AmazonClientException
    {
        throw new NotImplementedException();

    }

    @Override
    public ChangeMessageVisibilityBatchResult changeMessageVisibilityBatch(String queueUrl, List<ChangeMessageVisibilityBatchRequestEntry> entries) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public void changeMessageVisibility(String queueUrl, String receiptHandle, Integer visibilityTimeout) throws AmazonClientException
    {
        //do nothing
    }

    @Override
    public GetQueueUrlResult getQueueUrl(String queueName) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public void removePermission(String queueUrl, String label) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public GetQueueAttributesResult getQueueAttributes(String queueUrl, List<String> attributeNames) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public SendMessageBatchResult sendMessageBatch(String queueUrl, List<SendMessageBatchRequestEntry> entries) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public void deleteQueue(String queueUrl) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public SendMessageResult sendMessage(String queueUrl, String messageBody) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public ReceiveMessageResult receiveMessage(String queueUrl) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public ListQueuesResult listQueues(String queueNamePrefix) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public DeleteMessageBatchResult deleteMessageBatch(String queueUrl, List<DeleteMessageBatchRequestEntry> entries) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public CreateQueueResult createQueue(String queueName) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public void addPermission(String queueUrl, String label, List<String> aWSAccountIds, List<String> actions) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public void deleteMessage(String queueUrl, String receiptHandle) throws AmazonClientException
    {
        throw new NotImplementedException();
    }

    @Override
    public void shutdown()
    {
        throw new NotImplementedException();
    }

    @Override
    public ResponseMetadata getCachedResponseMetadata(AmazonWebServiceRequest request)
    {
        throw new NotImplementedException();
    }


    private final class LockedFileInfo implements AutoCloseable
    {
        private final RandomAccessFile file;
        private final FileLock fileLock;
        private final Map<String, List<Map<String, String>>> queues;

        public LockedFileInfo(RandomAccessFile file, FileLock fileLock, Map<String, List<Map<String, String>>> queues)
        {
            this.file = file;
            this.fileLock = fileLock;
            this.queues = queues;
        }

        @Override
        public void close()
        {
            log.trace("  Saving SQS file " + messageExchangeFile);

            try
            {
                final byte[] rawData = jackson.writerWithDefaultPrettyPrinter().writeValueAsBytes(queues);

                file.seek(0L);
                file.setLength(0L);
                file.write(rawData);

                fileLock.close();
                file.close();

                if (log.isTraceEnabled()) {
                    final StringBuilder logMessage = new StringBuilder("  Saved ").append(queues.size()).append(" queues to SQS file ").append(messageExchangeFile);
                    appendQueueSizeInfo(logMessage, queues);

                    log.trace(logMessage.toString());
                }
            }
            catch (Exception e)
            {
                log.error("  Couldn't save the fake SQS... oh well...", e);
            }

        }
    }

}
