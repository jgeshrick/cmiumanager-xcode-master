/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.aws;

/**
 * Interface for implementing an AWS SQS management and monitoring service.
 */
public interface MdceIpcPublishService extends MdceIpcService
{

    /**
     * Send an SQS message to a queue
     * @param queueName name of the queue
     * @param message message to send
     * @return message id of the send message
     */
    String sendMessage(String queueName, String message);

}
