/*
 *  Neptune Technology Group
 *  Copyright 2015, 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.PurgeQueueRequest;
import com.amazonaws.services.sqs.model.QueueDoesNotExistException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import com.neptunetg.common.aws.debug.LocalAwsSQS;

/**
 * Encapsulate AWS SQS service and sending message to a queue.
 */
public class SqsMdceIpcServiceBase implements MdceIpcService
{
    private static final Logger log = LoggerFactory.getLogger(SqsMdceIpcServiceBase.class);

    private static final String PROPERTY_ENV_NAME = "env.name";
    private static final String PROPERTY_LOCAL_SQS_MODE = "mdce.scheduler.localMode";
    //use this folder to store the message exchange file as it's easy to find
    private static final String PROPERTY_MDCE_LOG_FOLDER = "mdce.log.folder";

    protected final AmazonSQS amazonSQS;
    protected final String envName;
    protected final Map<String, SqsQueue> queueMap;

    public SqsMdceIpcServiceBase(AWSCredentialsProvider credentialsProvider,
                                 Properties envProperties) throws ParseException
    {

        this.envName = envProperties.getProperty(PROPERTY_ENV_NAME);
        boolean isLocalMode = parseBoolean(envProperties.getProperty(PROPERTY_LOCAL_SQS_MODE, "false"));

        this.queueMap = new ConcurrentHashMap<>();

        if (isLocalMode)
        {
            this.amazonSQS = LocalAwsSQS.createOrGetExisting(envProperties.getProperty(PROPERTY_MDCE_LOG_FOLDER));
            log.warn(getClass().getSimpleName() + " is running in SQL local mode.  Messages will be exchanged using a shared file not SQS.");
        }
        else
        {
            this.amazonSQS = new AmazonSQSClient(credentialsProvider);
            this.amazonSQS.setRegion(Region.getRegion(Regions.US_WEST_2));
        }
    }

    private static boolean parseBoolean(String s) throws ParseException
    {
        if (Boolean.TRUE.toString().equals(s))
        {
            return true;
        }
        else if (Boolean.FALSE.toString().equals(s))
        {
            return false;
        }
        else
        {
            throw new ParseException("Boolean " + s, 0);
        }
    }


    /**
     * Only one PurgeQueue operation on a queue is allowed every 60 seconds
     *
     * @param queueName name of the queue
     */
    @Override
    public void purgeQueue(String queueName)
    {
        try
        {
            PurgeQueueRequest purgeQueueRequest = new PurgeQueueRequest(getQueueUrl(queueName));
            amazonSQS.purgeQueue(purgeQueueRequest);
        }
        catch (QueueDoesNotExistException ex)
        {
            log.debug(ex.toString());
        }
    }

    /**
     * Construct the full queue name by prefixing with the environment name
     *
     * @param envPrefix the environment name
     * @param queueName the queue name
     * @return the public AWS SQS queue name
     */
    protected static String getFullyQualifiedQueueName(String envPrefix, String queueName)
    {
        return envPrefix + "-" + queueName;
    }

    /**
     * returns the queueurl for for sqs queue if you pass in a name
     *
     * @param queueName name of the queue
     * @return url of the queue
     */
    protected String getQueueUrl(String queueName)
    {
        GetQueueUrlRequest getQueueUrlRequest = new GetQueueUrlRequest(getFullyQualifiedQueueName(this.envName, queueName));
        return this.amazonSQS.getQueueUrl(getQueueUrlRequest).getQueueUrl();
    }

}
