/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.aws;

import java.time.Duration;

/**
 * Interface for implementing an AWS SQS management and monitoring service.
 */
public interface MdceIpcSubscribeService extends MdceIpcService
{

    /**
     * Register a listener to a queueName
     * @param observer the listener
     * @param queueName the queue name to monitor
     * @param pollInterval how frequently to check the queue.  Any new messages in the queue will be passed to the listener
     * @param messageBatchSize the maximum number of messages to process at each poll.  If messages cannot be processed as fast
     *                         as they arrive, messages are processed continuously.  Otherwise up to messageBatchSize messages
     *                         are processed every pollInterval.
     */
    void register(AwsQueueReceiver observer, String queueName, Duration pollInterval, int messageBatchSize);

    /**
     * Unregister a listener to a queueName.  Blocks until the polling has finished.  The listener may be called again in the interim.
     * @param queueName the queue name to unregister
     */
    void unregister(String queueName);

    /**
     * Check if the observer has been registered to the queue
     * @param observer listener
     * @param queueName queue name
     * @return true if registered
     */
    boolean isRegistered(AwsQueueReceiver observer, String queueName);

}
