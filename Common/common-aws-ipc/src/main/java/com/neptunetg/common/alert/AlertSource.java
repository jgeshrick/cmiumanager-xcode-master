/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.alert;

/**
 * Class to represent alert sources.  This should be the only place where alert sources are defined.
 * All alert sources must also be listed in http://gontg/ntgprojects/10065-1/Project%20Documents/Documents/10065-1_DI04_0002%20Alert%20Scheme%20Definition/10065-1_DI04_0002%20Alert%20Scheme%20Definition.docx
 *
 */
public class AlertSource
{
    private final String alertId;

    private AlertSource(String alertId)
    {
        this.alertId = alertId;
    }

    public static final AlertSource AWS_DYNAMODB_BACKUP = new AlertSource("AWS/DYNAMODB/BACKUP");
    public static final AlertSource AWS_DYNAMODB_MAINTENANCE_ERROR = new AlertSource("AWS/DynamoDB/Maintenance-Error");
    public static final AlertSource AWS_EC2_DB_FAILED_ADD = new AlertSource("AWS/EC2/Database/Failed to add packet");
    public static final AlertSource MQTT_FAILED_CONNECTION = new AlertSource("AWS/EC2/MQTT/Failed Connection");
    public static final AlertSource MQTT_MISSING_DIRECTORY = new AlertSource("AWS/EC2/MQTT/Missing Directory");

    //TODO: this is badly named.  It's actually an issue with a SIM with a known ICCID.  But not clear what condition would actually cause this
    public static final AlertSource SIM_DETAILS_UPDATE_UKNOWN_ERROR = new AlertSource("MNO/SIM/MISMATCH");

    public static AlertSource cloudwatchAlarm(String alarmName)
    {
        return new AlertSource("AWS/EC2/" + alarmName);
    }

    public static AlertSource miuClaimConflict(int miuId, int newSiteId, int existingSiteId)
    {
        return new AlertSource("MIU/Ownership/" + miuId + "/Conflict/" + newSiteId + "/" + existingSiteId);
    }

    public static AlertSource cellularHighUsage(int cmiuId)
    {
        return new AlertSource("CMIU/" + cmiuId + "/Data/Cellular/High-usage");
    }

    public static AlertSource cellularService(int cmiuId)
    {
        return new AlertSource("CMIU/" + cmiuId + "/Data/Cellular/Service");
    }

    public static AlertSource cellularNoConnection(int cmiuId)
    {
        return new AlertSource("CMIU/" + cmiuId + "/Data/Cellular/No-connection");
    }

    public static AlertSource cmiuConfigMismatch(int cmiuId)
    {
        return new AlertSource("CMIU/" + cmiuId + "/Config/Mismatch");
    }

    //TODO: overlap with cmiuCellularConfigMismatch
    public static AlertSource cmiuCellularConfigMismatchIccidOrImsi(int cmiuId)
    {
        return new AlertSource("CMIU/" + cmiuId + "/Reported-sim-details-mismatch");
    }

    //TODO: overlap with cmiuCellularConfigMismatchIccidOrImsi
    public static AlertSource cmiuCellularConfigMismatch(int cmiuId)
    {
        return new AlertSource("CMIU/" + cmiuId + "/CellularConfig/Mismatch");
    }

    public static AlertSource cmiuCellularConfigConflict(int cmiuId1, int cmiuId2)
    {
        return new AlertSource("CMIU/" + cmiuId1 + "/CellularConfig/Conflict/" + cmiuId2);
    }

    public static AlertSource cmiuLifecycleStateStuck(int cmiuId)
    {
        return new AlertSource("CMIU/" + cmiuId + "/Lifecycle-state/Stuck");
    }

    public static AlertSource cmiuReportsNonOfficialMode(int cmiuId)
    {
        return new AlertSource("CMIU/" + cmiuId + "/Mode/NonOfficial");
    }

    public static AlertSource cmiuLifecycleUnexpectedTransition(int cmiuId)
    {
        return new AlertSource("CMIU/" + cmiuId + "/Lifecycle-state/Unexpected-transition");
    }

    public static AlertSource simMsisdnMismatch(String iccid)
    {
        return new AlertSource("SIM/ICCID/" + iccid + "/MSISDN/mismatch");
    }

    public static AlertSource l900NetworkMismatch(int lmiuId)
    {
        return new AlertSource("L900/" + lmiuId + "/network/mismatch");
    }

    public static AlertSource l900EuiMismatch(int lmiuId)
    {
        return new AlertSource("L900/" + lmiuId + "/eui/mismatch");
    }

    public static AlertSource siteConnectionIssue(int siteId)
    {
        return new AlertSource("SITE/" + siteId + "/connection");
    }

    public String getAlertId()
    {
        return alertId;
    }

    @Override
    public String toString()
    {
        return alertId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        AlertSource that = (AlertSource) o;

        return alertId.equals(that.alertId);

    }

    @Override
    public int hashCode()
    {
        return alertId.hashCode();
    }
}
