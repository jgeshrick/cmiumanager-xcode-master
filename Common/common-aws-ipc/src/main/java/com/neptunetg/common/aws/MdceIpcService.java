/*
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 */

package com.neptunetg.common.aws;

/**
 * Base for IPC
 */
public interface MdceIpcService
{
    String SEND_COMMAND_QUEUE = "send-command";
    String MANAGE_NOSQL_QUEUE = "manage-nosql";
    String CHECK_CELLULAR_BILLING = "check-cellular-billing";
    String SUBSCRIBE_MIU_ALL_BROKERS = "subscribe-miu-all-brokers";
    String CLEAR_MIU_MESSAGES = "clear-miu-messages";
    String CLOUDWATCH_ALARMS = "cloudwatch";
    String ALERTS = "alerts";
    String MANAGE_OLD_ALERT = "manage-old-alert";
    String DATA_PIPELINE = "datapipeline";
    String CHECK_PENDING_CELLULAR_STATE = "check-pending-cellular-state";
    String CHECK_CMIU_LIFECYCLE_STATE = "check-cmiu-lifecycle-state";
    String CHECK_SIM_LIST = "check-sim-list";
    String CHECK_CMIU_MODE = "check-cmiu-mode";
    String LORA_PACKETS_RECEIVED = "lora-packets-received";
    String LORA_PACKETS_TO_SEND = "lora-packets-to-send";
    String CMIU_CHECK_REPORTED_SIM_AND_MODEM_DETAILS = "cmiu-check-reported-sim-and-modem-details";
    String CHECK_SITE_COMMUNICATION = "check-site-communication";

    /**
     * Remove all message from the queue
     * @param queueName the queue name
     */
    void purgeQueue(String queueName);


}
