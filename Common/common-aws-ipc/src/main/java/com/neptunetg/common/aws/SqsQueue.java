/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015, 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.aws;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.ChangeMessageVisibilityRequest;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.QueueNameExistsException;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Internal class to encapsulate the queue name and association with a registered queue observer.
 * Note there is only one registered observer per queue.
 */
class SqsQueue implements Runnable
{
    private static final Logger log = LoggerFactory.getLogger(SqsQueue.class);

    private static final Duration VISIBILITY_TIMEOUT = Duration.ofSeconds(60L);  //60s wait for receiver of the message to act on and then delete this message
    private static final Duration MESSAGE_RETENTION_PERIOD = Duration.ofDays(4L);  //message retained for max of 4 days
    private static final int MAXIMUM_MESSAGE_SIZE_BYTES = 2048;  //2048 bytes message size

    private final String name;
    private final String url;
    private final AmazonSQS amazonSQS;
    private final AtomicBoolean terminate = new AtomicBoolean(true);

    private AwsQueueReceiver observer;

    private Thread pollThread;
    private Duration pollInterval;
    private int messageBatchSize;


    SqsQueue(String queueName, String fullyQualifiedName, AmazonSQS amazonSQS)
    {
        this.name = queueName;
        this.amazonSQS = amazonSQS;
        this.url = createQueue(fullyQualifiedName, amazonSQS);
        if (this.amazonSQS == null)
        {
            throw new RuntimeException("Null SQS");
        }
    }


    /**
     * Creates a queue in your region and returns the url of the queue
     *
     * @return url of the queue
     */
    private static String createQueue(String fullyQualifiedName, AmazonSQS amazonSQS)
    {
        final Map<String, String> attributes = new HashMap<>();
        attributes.put("VisibilityTimeout", Long.toString(VISIBILITY_TIMEOUT.getSeconds()));
        attributes.put("MessageRetentionPeriod", Long.toString(MESSAGE_RETENTION_PERIOD.getSeconds()));
        attributes.put("MaximumMessageSize", Integer.toString(MAXIMUM_MESSAGE_SIZE_BYTES));

        final CreateQueueRequest createQueueRequest = new CreateQueueRequest(fullyQualifiedName)
                .withAttributes(attributes);

        try
        {
            return amazonSQS.createQueue(createQueueRequest).getQueueUrl();
        }
        catch (QueueNameExistsException ex)
        {
            log.debug("Queue name {} already exist before create. Using existing name", fullyQualifiedName);
            return amazonSQS.getQueueUrl(fullyQualifiedName).getQueueUrl();
        }
    }

    AwsQueueReceiver getObserver()
    {
        return this.observer;
    }

    /**
     * Commence polling
     * @param observer Callback
     * @param pollInterval Polling interval
     */
    void startPolling(AwsQueueReceiver observer, Duration pollInterval, int messageBatchSize)
    {
        this.terminate.set(true);
        awaitThreadTermination();

        this.observer = observer;
        this.pollInterval = pollInterval;
        this.messageBatchSize = messageBatchSize;
        this.terminate.set(false);
        this.pollThread = new Thread(this, "SQS poll thread for queue " + this.name);

        this.pollThread.start();
    }

    /**
     * Terminate polling
     */
    void stopPolling()
    {
        this.terminate.set(true);
        if (this.pollThread != null)
        {
            this.pollThread.interrupt();
        }
        awaitThreadTermination();
    }

    private void awaitThreadTermination()
    {
        while (true)
        {
            final Thread existingPollThread;

            existingPollThread = this.pollThread;
            if (existingPollThread == null || !existingPollThread.isAlive())
            {
                break;
            }

            try
            {
                existingPollThread.join(); //wait for thread to exit
            }
            catch (InterruptedException e)
            {
                Thread.interrupted();
            }
        }
        this.pollThread = null;
    }


    /**
     * Called by parent class to send a message to the internal queue
     *
     * @param message message to send
     * @return message id
     */
    String sendMessage(String message)
    {
        SendMessageResult messageResult = amazonSQS.sendMessage(new SendMessageRequest(url, message));

        return messageResult.getMessageId();
    }

    /**
     * run loop
     */
    @Override
    public void run()
    {
        while (!terminate.get())
        {
            try
            {


                final Instant nextTick = Instant.now().plus(this.pollInterval);

                final int messagesProcessed = checkAndProcessMessageQueue();

                final Duration pauseTime = Duration.between(Instant.now(), nextTick);

                if (log.isDebugEnabled() && messagesProcessed > 0)
                {
                    log.debug("Processed " + messagesProcessed + " from SQS queue " + this.name + " in " + this.pollInterval.minus(pauseTime));
                }

                if (!pauseTime.isNegative() && !pauseTime.isZero() && !terminate.get())
                {
                    Thread.sleep(pauseTime.toMillis());
                }
            }
            catch (InterruptedException e)
            {
                Thread.interrupted();
                // go back to the top of the loop and check terminate() again
            }
            catch (Exception e)
            {
                log.error("Uncaught exception in SQS polling loop for queue " + this.name, e);
            }
        }
        this.observer = null;
    }


    /**
     * Called by the polling loop to check queue for available message and notify the observer to process
     * the message.
     *
     * @return number of messages processed by the observer
     */
    private int checkAndProcessMessageQueue()
    {

        int messageProcessed = 0;
        int messageFetched = 0;

        if (observer != null)
        {
            int remainingMessagesInBatch = this.messageBatchSize;

            while (remainingMessagesInBatch > 0 && !this.terminate.get())
            {

                final ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(url)
                        .withWaitTimeSeconds(1);

                if (remainingMessagesInBatch > 10)
                {
                    receiveMessageRequest.setMaxNumberOfMessages(10);
                    remainingMessagesInBatch -= 10;
                }
                else
                {
                    receiveMessageRequest.setMaxNumberOfMessages(remainingMessagesInBatch);
                    remainingMessagesInBatch = 0;
                }

                //poll SQS for messages
                try
                {
                    final ReceiveMessageResult receiveMessageResult = amazonSQS.receiveMessage(receiveMessageRequest);
                    final List<Message> messages = receiveMessageResult.getMessages();

                    if (messages == null || messages.isEmpty())
                    {
                        break;
                    }
                    messageFetched += messages.size();
                    for (Message message : messages)
                    {
                        if (this.terminate.get())
                        {
                            break;
                        }
                        try
                        {
                            consumeMessage(message);

                            messageProcessed++;

                        } catch (Exception ex)
                        {
                            //error in process the message, do not remove the message from SQS queue so that it will be
                            //make available again
                            log.error("Error processing received SQS message " + message.getMessageId() + " from queue " + this.name, ex);
                        }

                    }

                } catch (Exception ex)
                {
                    log.error("Error receiving message from SQS queue " + this.name, ex);
                }
            }
            if (messageProcessed > 0)
            {
                log.debug("SQS {} - processed {} out of {} fetched, maximum batch size is {}", name, messageProcessed, messageFetched, this.messageBatchSize);
            }

        }

        return messageProcessed;

    }

    private void consumeMessage(Message message)
    {
        log.debug("Received message from SQS queue {}: {}", this.name, message.getMessageId());
        boolean deleteMessage = observer.processReceivedMessage(message.getBody(), message.getMessageId(), message.getReceiptHandle(), this.terminate);

        if (deleteMessage)
        {
            log.debug("Delete message from SQS queue {}: {}", this.name, message.getMessageId());
            amazonSQS.deleteMessage(new DeleteMessageRequest(url, message.getReceiptHandle()));
        }
        else
        {
            log.debug("Return message to SQS queue {} immediately: {}", this.name, message.getMessageId());

            ChangeMessageVisibilityRequest changeMessageVisibilityRequest =
                    new ChangeMessageVisibilityRequest(this.url, message.getReceiptHandle(), 0);

            amazonSQS.changeMessageVisibility(changeMessageVisibilityRequest);
        }

    }
}
