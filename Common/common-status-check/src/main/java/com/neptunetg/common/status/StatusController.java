/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.status;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * Provides service for checking that an application is up
 */
@Controller
public class StatusController
{
    @Value("#{envProperties['server.name']}")
    String serverName;

    @RequestMapping(value = "/debug/application/status", method = RequestMethod.GET)
    @ResponseBody
    public String getStatus()
    {
        return "{\"status\":\"up\", \"server\":\"" + serverName + "\"}";
    }
}
