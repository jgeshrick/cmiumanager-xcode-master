/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns;

import com.neptunetg.common.cns.verizonwns.InclusiveDateTimeRangeBuilder;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_common.InclusiveDateTimeRange;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;

/**
 * Tests for InclusiveDateTimeRangeBuilder
 */
public class InclusiveDateTimeRangeBuilderTest
{

    @Test
    public void testGetDateTimeRange() throws Exception
    {
        InclusiveDateTimeRange dateTimeRange = InclusiveDateTimeRangeBuilder.fromDateRange("1/2/1970", "12/31/2000");
        assertEquals(1970, dateTimeRange.getEarliest().get(Calendar.YEAR));
        assertEquals(0, dateTimeRange.getEarliest().get(Calendar.MONTH));
        assertEquals(2, dateTimeRange.getEarliest().get(Calendar.DAY_OF_MONTH));

        assertEquals(2000, dateTimeRange.getLatest().get(Calendar.YEAR));
        assertEquals(11, dateTimeRange.getLatest().get(Calendar.MONTH));
        assertEquals(31, dateTimeRange.getLatest().get(Calendar.DAY_OF_MONTH));
    }

    @Test
    public void testFromDaysAgo() throws Exception
    {
        int noOfDaysBefore = 10;

        Calendar calNow = Calendar.getInstance();
        InclusiveDateTimeRange dateTimeRange = InclusiveDateTimeRangeBuilder.fromDaysAgo(noOfDaysBefore);

        assertEquals(calNow.get(Calendar.YEAR), dateTimeRange.getLatest().get(Calendar.YEAR));
        assertEquals(calNow.get(Calendar.MONTH), dateTimeRange.getLatest().get(Calendar.MONTH));
        assertEquals(calNow.get(Calendar.DAY_OF_MONTH), dateTimeRange.getLatest().get(Calendar.DAY_OF_MONTH));

        //subtract the number of days away to get to the from date.
        calNow.add(Calendar.DAY_OF_MONTH, -noOfDaysBefore);

        assertEquals(calNow.get(Calendar.YEAR), dateTimeRange.getEarliest().get(Calendar.YEAR));
        assertEquals(calNow.get(Calendar.MONTH), dateTimeRange.getEarliest().get(Calendar.MONTH));
        assertEquals(calNow.get(Calendar.DAY_OF_MONTH), dateTimeRange.getEarliest().get(Calendar.DAY_OF_MONTH));
    }

    @Test
    public void testGetRangeFromZoneDateTime()
    {
        ZonedDateTime fromDateTime = ZonedDateTime.now();
        ZonedDateTime toDateTime = fromDateTime.plusDays(1);

        InclusiveDateTimeRange dateTimeRange = InclusiveDateTimeRangeBuilder.fromDateRange(fromDateTime, toDateTime);

        assertEquals(fromDateTime.toInstant(), dateTimeRange.getEarliest().toInstant());
        assertEquals(toDateTime.toInstant(), dateTimeRange.getLatest().toInstant());

    }
}