/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns.jasper;

import com.neptunetg.common.cns.CellularStatus;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test transformation of SIm status between MDCE definition and Jasper
 */
public class JasperSimStatusTest
{

    @Test
    public void testFromString() throws Exception
    {
        testTransformStringToEnum("TEST_READY_NAME", JasperSimStatus.TEST_READY_NAME, CellularStatus.ACTIVE);
        testTransformStringToEnum("test_ready_name", JasperSimStatus.TEST_READY_NAME, CellularStatus.ACTIVE);

        testTransformStringToEnum("INVENTORY_NAME", JasperSimStatus.INVENTORY_NAME, CellularStatus.PRE_ACTIVE);
        testTransformStringToEnum("inventory_name", JasperSimStatus.INVENTORY_NAME, CellularStatus.PRE_ACTIVE);

        testTransformStringToEnum("TRIAL_NAME", JasperSimStatus.TRIAL_NAME, CellularStatus.PRE_ACTIVE);
        testTransformStringToEnum("trial_name", JasperSimStatus.TRIAL_NAME, CellularStatus.PRE_ACTIVE);

        testTransformStringToEnum("ACTIVATION_READY_NAME", JasperSimStatus.ACTIVATION_READY_NAME, CellularStatus.ACTIVE);
        testTransformStringToEnum("activation_ready_name", JasperSimStatus.ACTIVATION_READY_NAME, CellularStatus.ACTIVE);

        testTransformStringToEnum("ACTIVATED_NAME", JasperSimStatus.ACTIVATED_NAME, CellularStatus.ACTIVE);
        testTransformStringToEnum("activated_name", JasperSimStatus.ACTIVATED_NAME, CellularStatus.ACTIVE);

        testTransformStringToEnum("DEACTIVATED_NAME", JasperSimStatus.DEACTIVATED_NAME, CellularStatus.RETIRED);

        testTransformStringToEnum("RETIRED_NAME", JasperSimStatus.RETIRED_NAME, CellularStatus.RETIRED);
        testTransformStringToEnum("REPLACED_NAME", JasperSimStatus.REPLACED_NAME, CellularStatus.RETIRED);
        testTransformStringToEnum("PURGED_NAME", JasperSimStatus.PURGED_NAME, CellularStatus.RETIRED);
        testTransformStringToEnum("REPLACED_NAME", JasperSimStatus.REPLACED_NAME, CellularStatus.RETIRED);

        testTransformStringToEnum("unrecognised string", JasperSimStatus.UNKNOWN_STATUS, CellularStatus.UNKNOWN);

    }

    @Test
    public void testFromCellularStatusValidStatus() throws Exception
    {
        assertEquals(JasperSimStatus.INVENTORY_NAME, JasperSimStatus.fromCellularStatus(CellularStatus.PRE_ACTIVE));
        assertEquals(JasperSimStatus.ACTIVATED_NAME, JasperSimStatus.fromCellularStatus(CellularStatus.ACTIVE));
        assertEquals(JasperSimStatus.DEACTIVATED_NAME, JasperSimStatus.fromCellularStatus(CellularStatus.DE_ACTIVE));
        assertEquals(JasperSimStatus.RETIRED_NAME, JasperSimStatus.fromCellularStatus(CellularStatus.RETIRED));
    }

    private void testTransformStringToEnum(final String jasperStatus,
                                           final JasperSimStatus expectedJasperSimStatus,
                                           final CellularStatus expectedCellularStatus)
    {
        JasperSimStatus jasper = JasperSimStatus.fromString(jasperStatus);

        assertEquals(expectedJasperSimStatus, jasper);
        assertEquals(expectedCellularStatus, jasper.toCellularStatus());
    }


}