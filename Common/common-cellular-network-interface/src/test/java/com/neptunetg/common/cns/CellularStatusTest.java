/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CellularStatusTest
{

    @Test
    public void testFromString() throws Exception
    {
        final String PRE_ACTIVE = "PRE_ACTIVE";
        final String ACTIVE = "ACTIVE";
        final String DE_ACTIVE = "DE_ACTIVE";
        final String RETIRED = "RETIRED";
        final String UNKNOWN = "UNKNOWN";

        assertEquals(PRE_ACTIVE, CellularStatus.fromString(PRE_ACTIVE).name());
        assertEquals(ACTIVE, CellularStatus.fromString(ACTIVE).name());
        assertEquals(DE_ACTIVE, CellularStatus.fromString(DE_ACTIVE).name());
        assertEquals(RETIRED, CellularStatus.fromString(RETIRED).name());
        assertEquals(UNKNOWN, CellularStatus.fromString("this is an unknown state").name());

        //test insert with other cases
        assertEquals(ACTIVE, CellularStatus.fromString("aCtIvE").name());
    }
}