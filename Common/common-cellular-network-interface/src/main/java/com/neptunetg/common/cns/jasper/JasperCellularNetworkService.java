/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns.jasper;

import com.jasperwireless.api.BillingServiceStub;
import com.jasperwireless.api.TerminalServiceStub;
import com.jasperwireless.api.ws.schema.DataUsageDetailType;
import com.jasperwireless.api.ws.schema.EditTerminalRequest;
import com.jasperwireless.api.ws.schema.EditTerminalRequestParamGroup;
import com.jasperwireless.api.ws.schema.EditTerminalResponse;
import com.jasperwireless.api.ws.schema.GetModifiedTerminalsRequest;
import com.jasperwireless.api.ws.schema.GetModifiedTerminalsResponse;
import com.jasperwireless.api.ws.schema.GetSessionInfoRequest;
import com.jasperwireless.api.ws.schema.GetSessionInfoResponse;
import com.jasperwireless.api.ws.schema.GetTerminalAuditTrailRequest;
import com.jasperwireless.api.ws.schema.GetTerminalAuditTrailResponse;
import com.jasperwireless.api.ws.schema.GetTerminalDetailsRequest;
import com.jasperwireless.api.ws.schema.GetTerminalDetailsResponse;
import com.jasperwireless.api.ws.schema.GetTerminalLatestRegistrationRequest;
import com.jasperwireless.api.ws.schema.GetTerminalLatestRegistrationRequestParamGroup;
import com.jasperwireless.api.ws.schema.GetTerminalLatestRegistrationResponse;
import com.jasperwireless.api.ws.schema.GetTerminalUsageDataDetailsRequest;
import com.jasperwireless.api.ws.schema.GetTerminalUsageDataDetailsResponse;
import com.jasperwireless.api.ws.schema.GetTerminalUsageRequest;
import com.jasperwireless.api.ws.schema.GetTerminalUsageResponse;
import com.jasperwireless.api.ws.schema.Iccids_type3;
import com.jasperwireless.api.ws.schema.RegistrationInfo_type0;
import com.jasperwireless.api.ws.schema.RequestType;
import com.jasperwireless.api.ws.schema.SessionInfoType;
import com.jasperwireless.api.ws.schema.TerminalAuditTrailType;
import com.jasperwireless.api.ws.schema.TerminalChangeType;
import com.jasperwireless.api.ws.schema.TerminalType;
import com.jasperwireless.api.ws.schema.Terminals_type1;
import com.jasperwireless.api.ws.schema.UsageDetails_type0;
import com.neptunetg.common.cns.*;
import com.neptunetg.common.cns.model.ConnectionEventDetail;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.common.cns.model.DeviceProvisioningEvent;
import com.neptunetg.common.cns.model.UsageHistory;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ConfigurationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ObjectInputStream;
import java.rmi.RemoteException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of CNS using Jasper API
 */
public class JasperCellularNetworkService implements CellularNetworkService
{

    private static final Logger log = LoggerFactory.getLogger(JasperCellularNetworkService.class);

    private static final String AXIS_FAULT_MESSAGE_DEVICE_NOT_FOUND  = "100100";
    private static final String AXIS_FAULT_MESSAGE_INVALID_ICCID     = "100101";
    private static final String AXIS_FAULT_MESSAGE_NO_TERMINAL_USAGE = "200200";

    private static Set<CellularStatus> acceptableCellularStates = new HashSet();

    private static final int KILO_BITS = 1024;

    private static final String JASPER_API_VERSION = "5.90";

    private final ConfigurationContext configContext;
    private final CnsAccountDetails    accountDetails;
    private final long                 accountId;

    public JasperCellularNetworkService(ConfigurationContext configContext, CnsAccountDetails accountDetails)
    {
        this.configContext = configContext;
        this.accountDetails = accountDetails;

        this.accountId = Long.valueOf(accountDetails.getAccountName());

        acceptableCellularStates.add(CellularStatus.ACTIVE);
        acceptableCellularStates.add(CellularStatus.PRE_ACTIVE);
        acceptableCellularStates.add(CellularStatus.RETIRED);
    }

    /**
     * Callback registration/un-registration is not available through API
     *
     * @return false
     */
    @Override
    public boolean registerCallback()
    {
        //no callback available with jasper
        return false;
    }

    /**
     * Callback registration/unregistration is not available through API
     *
     * @return false
     */
    @Override
    public boolean unregisterCallback()
    {
        return false;
    }

    @Override
    public boolean getAllowedLifecycleState(CellularStatus cellularState)
    {
        return acceptableCellularStates.contains(cellularState);
    }

    @Override
    public DeviceCellularInformation getDeviceInformation(String iccid)
            throws CnsException
    {
        final TerminalType terminal = getTerminalDetails(iccid);

        if (terminal == null) //device not found
        {
            return null;
        }

        final DeviceCellularInformation deviceInformation = extractDeviceCellularInformation(terminal);

        final SessionInfoType[] sessionInfoArray = getDeviceActiveSession(iccid);

        if (sessionInfoArray != null && sessionInfoArray.length > 0)
        {
            //if sessionInfoArray has some elements, it is currently connected
            deviceInformation.setConnected(true);
            deviceInformation.setIpAddress(sessionInfoArray[0].getIpAddress());
        }
        else
        {
            deviceInformation.setConnected(false);
        }

        //attempt to retrieve device last connection date
        try
        {
            final Instant lastConnectionDate = getDeviceLastConnectionDate(terminal.getImsi());
            if (lastConnectionDate != null)
            {
                //set last connection date
                deviceInformation.setLastConnectionDate(Date.from(lastConnectionDate));
            }
        }
        catch (Exception e)
        {
            //failure to get last connection date
            log.warn("Cannot get device last connection date for device with ICCID " + iccid + " (Device might not have connected to network at all). " +
                    "Device last connection date is not filled in.  " + e.getMessage()); //don't log the whole stack trace here because org.apache.axis2.transport.http.HTTPSender.sendViaPost() "helpfully" logs it at info level
        }

        return deviceInformation;
    }

    /**
     * Call Jasper GetTerminalDetails API for one device
     *
     * @param iccid iccid for the cellular device
     * @return Jasper TerminalType object
     * @throws CnsException
     */
    private TerminalType getTerminalDetails(final String iccid)
            throws CnsException {
        try {
            TerminalServiceStub terminalService = new TerminalServiceStub(configContext, JasperServiceEndpoint.TERMINAL_SERVICE);
            terminalService._getServiceClient().addHeader(getSecurityToken(this.accountDetails));

            GetTerminalDetailsRequest request = createRequest(new GetTerminalDetailsRequest());

            Iccids_type3 iccids = new Iccids_type3();
            iccids.addIccid(iccid);
            request.setIccids(iccids);

            GetTerminalDetailsResponse response = null;
            DeviceCellularInformation deviceInformation = null;

            response = terminalService.getTerminalDetails(request);
            Terminals_type1 terminals = response.getTerminals();

            //we expect it to return 0 or 1 terminals
            if (terminals.getTerminal().length > 0)
            {
                return terminals.getTerminal()[0];
            }
            else
            {
                throw new CnsException("No terminal details is retrieved for ICCID " + iccid);
            }
        }
        catch (AxisFault axisFault)
        {
            if (axisFaultIsDeviceNotFound(axisFault)) //special value for device not found
            {
                return null;
            }
            else
            {
                throw new CnsException("Error getting terminal details from Jasper for ICCID " + iccid +
                        "; SOAP Fault is: " + axisFault.getFaultDetailElement().toString(), axisFault);
            }
        }
        catch (RemoteException e)
        {
            throw new CnsException("Error getting terminal details from Jasper for ICCID " + iccid, e);
        }
    }

    /**
     * This is an experimental method to investigate the value of the "get terminal usage" Jasper API
     * Retrieves the terminal usage of a given month (completed billing cycle) for the terminal identified by ICCID.
     * This seems to be returning billable data?? FIXME: seems to be return No terminal usage SOAP error: 200200.
     *
     * @param iccid
     * @param cycleStartDate
     */
    private void getTerminalUsage(final String iccid, final Date cycleStartDate)
    {
        try
        {
            BillingServiceStub service = new BillingServiceStub(configContext, JasperServiceEndpoint.BILLING_SERVICE);
            service._getServiceClient().addHeader(getSecurityToken(this.accountDetails));

            GetTerminalUsageRequest request = createRequest(new GetTerminalUsageRequest());

            request.setIccid(iccid);
            request.setCycleStartDate(cycleStartDate);

            GetTerminalUsageResponse response = service.getTerminalUsage(request);

            log.debug("{} {}, {}", response.getCycleStartDate(), response.getBillable(), response.getBillableDataVolume());

        }
        catch (AxisFault axisFault)
        {
            if (axisFault.getMessage().equals("200200"))
            {
                log.warn("GetTerminalUsage request cannot retrieve usage for device: {} for date {}", iccid, cycleStartDate);
            }
            else
            {
                log.error("Error getting Axis service client", axisFault);
                //throw new CnsException("Error getting terminal usage from Jasper for iccid " + iccid, axisFault);
            }
        }
        catch (RemoteException e)
        {
            log.error("Error calling Jasper service", e);
            //throw new CnsException("Error getting terminal usage from Jasper for iccid " + iccid, e);
        }
    }


    /**
     * Utility to change a device sim state
     *
     * @param iccid
     * @param targetState
     */
    private String changeDeviceProvisioningState(String iccid, JasperSimStatus targetState) throws CnsException
    {
        TerminalServiceStub terminalService;

        try
        {
            terminalService = new TerminalServiceStub(configContext, JasperServiceEndpoint.TERMINAL_SERVICE);
            terminalService._getServiceClient().addHeader(getSecurityToken(this.accountDetails));

            //create request object
            EditTerminalRequestParamGroup requestParamGroup = new EditTerminalRequestParamGroup();
            requestParamGroup.setIccid(iccid);

            TerminalChangeType terminalChangeType = new TerminalChangeType();
            terminalChangeType.setTerminalChangeType(3);    //SIM STATUS, refer to JASPER xsd element: <xs:complexType name="TerminalChangeType">

            requestParamGroup.setChangeType(terminalChangeType);
            requestParamGroup.setTargetValue(targetState.name());

            EditTerminalRequest request = createRequest(new EditTerminalRequest());
            request.setEditTerminalRequestParamGroup(requestParamGroup);

            EditTerminalResponse response = terminalService.editTerminal(request);

            return request.getMessageId();
        }
        catch (AxisFault axisFault)
        {
            throw new CnsException("Error getting Axis service client when putting device with ICCID " + iccid + " into state " + targetState, axisFault);
        }
        catch (RemoteException e)
        {
            throw new CnsException("Error calling SOAP service when putting device with ICCID " + iccid + " into state " + targetState, e);
        }
    }

    /**
     * Insert jasper request header to class
     *
     * @param newRequest a new request
     * @param <T>        Jasper Request
     * @return subclass of requesttype with message id, version and license key inserted.
     */
    private <T extends RequestType> T createRequest(T newRequest)
    {
        newRequest.setMessageId(Instant.now().toString());
        newRequest.setVersion(JASPER_API_VERSION);
        newRequest.setLicenseKey(accountDetails.getLicenseKey());

        return newRequest;
    }

    /**
     * Extract data from Jasper terminalInfo and convert to to a MDCE class.
     * Note: devInfo.setBillingCycleEndDate and devInfo.setLastConnectionDate is not set and need to be set
     * externally after calling other Jasper API.
     *
     * @param terminalInfo the device information object obtained from WNS query
     * @return our MDCE generic class containing device information
     */
    private static DeviceCellularInformation extractDeviceCellularInformation(TerminalType terminalInfo)
    {
        if (terminalInfo == null)
        {
            return null;
        }
        DeviceCellularInformation devInfo = new DeviceCellularInformation();

        devInfo.setIccid(terminalInfo.getIccid());
        devInfo.setImei(terminalInfo.getImei());
        devInfo.setMsisdn(terminalInfo.getMsisdn());

        devInfo.setAccountName(String.valueOf(terminalInfo.getAccountId()));
        devInfo.setDeviceGroupName("N/A");  //group name field is not present in jasper
        devInfo.setCreateAt(terminalInfo.getDateAdded().getTime());

        devInfo.setIpAddress(terminalInfo.getFixedIpAddress());

        if (terminalInfo.getDateActivated() != null)
        {
            devInfo.setLastActivationDate(terminalInfo.getDateActivated().getTime());
        }

        devInfo.setLastActivationBy("N/A");    //TODO check

        //get custom fields . Note only getting first 2 fields
        devInfo.setCustomFields(
                terminalInfo.getCustom1() + ", " + terminalInfo.getCustom2() + ", " +
                        terminalInfo.getCustomerCustom1() + ", " + terminalInfo.getCustomerCustom2());

        devInfo.setCarrierName(terminalInfo.getCustomer()); //TODO check this is the carrier name, or else I don't think this field is available in Jasper
        devInfo.setServicePlan(terminalInfo.getRatePlan());
        devInfo.setDeviceState(JasperSimStatus.fromString(terminalInfo.getStatus()).toCellularStatus().name());   //TODO enum is preferred to string?
        return devInfo;
    }


    @Override
    public String getDeviceProvisioningStatus(String iccid) throws CnsException
    {
        final DeviceCellularInformation deviceCellularInformation = getDeviceInformation(iccid);
        if (deviceCellularInformation != null)
        {
            return deviceCellularInformation.getDeviceState();  //FIXME, need to standardise the state.. maybe make it into an enum rather than string
        }
        else
        {
            return null;
        }
    }

    /**
     * This is returning entries of usage history based on current month.
     *
     * @param iccid device iccid
     * @return usage history
     */
    @Override
    public List<UsageHistory> getDeviceUsageHistory(final String iccid) throws CnsException
    {
        BillingServiceStub service;
        try
        {
            service = new BillingServiceStub(configContext, JasperServiceEndpoint.BILLING_SERVICE);
            service._getServiceClient().addHeader(getSecurityToken(this.accountDetails));


            GetTerminalUsageDataDetailsRequest request = createRequest(new GetTerminalUsageDataDetailsRequest());

            request.setIccid(iccid);

            //get for current month?
            request.setCycleStartDate(Date.from(Instant.now().truncatedTo(ChronoUnit.DAYS)));

            GetTerminalUsageDataDetailsResponse response = service.getTerminalUsageDataDetails(request);

            return Arrays.stream(response.getUsageDetails().getUsageDetail())
                    .map(dataUsageDetailType -> {
                        UsageHistory usageHistory = new UsageHistory(iccid);
                        usageHistory.setTimeStamp(dataUsageDetailType.getSessionStartTime().getTime());
                        usageHistory.setBytesUsed(dataUsageDetailType.getDataVolume().longValue() * KILO_BITS);  //data volume is in KB, converting to Bytes

                        return usageHistory;
                    })
                    .collect(Collectors.toList());
        }
        catch (AxisFault axisFault)
        {
            if (axisFaultIsDeviceNotFound(axisFault))
            {
                return Collections.emptyList();
            }
            else
            {
                throw new CnsException("Error getting device usage history from Jasper for ICCID " + iccid +
                        "; SOAP Fault is: " + axisFault.getFaultDetailElement().toString(), axisFault);
            }
        }
        catch (RemoteException e)
        {
            throw new CnsException("Error getting device usage history from Jasper for ICCID " + iccid, e);
        }
    }

    /**
     * Get device usage history for a particular day
     * @param iccid device iccid
     * @param usageDate date where the usage occurs
     * @return usage history
     */
    @Override
    public List<UsageHistory> getDeviceUsageHistory(String iccid, LocalDate usageDate) throws CnsException
    {
        final Instant startInstant = usageDate.atStartOfDay().atZone(ZoneId.of("UTC")).toInstant();

        // getTerminalUsage(iccid, Date.from(startInstant)); TODO: investigate if this is any help

        return getMonthlyTerminalDataUsage(iccid, startInstant)
            .stream()
            .filter(usageHistory -> usageHistory.getTimeStamp().toInstant().truncatedTo(ChronoUnit.DAYS).equals(startInstant))
            .collect(Collectors.toList());
    }

    /**
     * Retrieves the terminal data usage of a given month. The return result contains both billable and non-billable usages.
     *
     * @param iccid iccid of the terminal
     * @param month the date where the data usage for the month within will be retrieved
     */
    private List<UsageHistory> getMonthlyTerminalDataUsage(final String iccid, Instant month) throws CnsException
    {
        try
        {
            BillingServiceStub service = new BillingServiceStub(configContext, JasperServiceEndpoint.BILLING_SERVICE);
            service._getServiceClient().addHeader(getSecurityToken(this.accountDetails));

            GetTerminalUsageDataDetailsRequest request = createRequest(new GetTerminalUsageDataDetailsRequest());

            request.setIccid(iccid);

            //set to first day of the billing cycle
            request.setCycleStartDate(Date.from(month));

            log.debug("Getting terminal usage data for device: {} for cycleStartDate {}", iccid, month);

            GetTerminalUsageDataDetailsResponse response = service.getTerminalUsageDataDetails(request);

            // usageDetails may be null if no history is available - log as info
            UsageDetails_type0 usageDetails = response.getUsageDetails();

            if (usageDetails == null)
            {
                log.warn("No monthly terminal data usage from Jasper for device: {} for cycleStartDate {} (usageDetails == null)", iccid, month);
                return new ArrayList<>();
            }

            DataUsageDetailType[] usageDetailArray = usageDetails.getUsageDetail();

            if (usageDetails == null)
            {
                log.warn("No monthly terminal data usage from Jasper for device: {} for cycleStartDate {} (usageDetailArray == null)", iccid, month);
                return new ArrayList<>();
            }

            return Arrays.stream(usageDetailArray)
                    .map(dataUsageDetailType -> {
                        UsageHistory usageHistory = new UsageHistory(iccid);
                        usageHistory.setTimeStamp(dataUsageDetailType.getSessionStartTime().getTime());
                        usageHistory.setBytesUsed(dataUsageDetailType.getDataVolume().longValue() * KILO_BITS);  //data volume is in KB, converting to Bytes

                        return usageHistory;
                    })
                    .collect(Collectors.toList());
        }
        catch (AxisFault axisFault)
        {
            if (axisFaultIsDeviceNotFound(axisFault)) //includes 200200 - "no terminal usage"
            {
                return Collections.emptyList();
            }
            else
            {
                throw new CnsException("Error getting monthly terminal data usage from Jasper for ICCID " + iccid +
                        "; SOAP Fault is: " + axisFault.getFaultDetailElement().toString(), axisFault);
            }
        }
        catch (RemoteException e)
        {
            throw new CnsException("Error getting monthly terminal data usage from Jasper for ICCID " + iccid, e);
        }


    }

    @Override
    public List<UsageHistory> getDeviceUsageHistory(String iccid, ZonedDateTime fromDateTime, ZonedDateTime toDateTime) throws CnsException
    {
        List<UsageHistory> usageHistoryList = new ArrayList<>();

        //initialise first month to retrieve usage history
        ZonedDateTime monthToRetrieveUsage = fromDateTime;

        do
        {
            //attempt to call GetTerminalUsage request to see whether that returns any useful data...
            // FIXME: to investigate difference between GetTerminalUsage and GetTerminalDataUsage
            // getTerminalUsage(iccid, Date.from(monthToRetrieveUsage.toInstant()));

            List<UsageHistory> monthlyUsage = getMonthlyTerminalDataUsage(iccid, monthToRetrieveUsage.toInstant());
            usageHistoryList.addAll(monthlyUsage);

            //move a month forward
            monthToRetrieveUsage = monthToRetrieveUsage.plusMonths(1);
        }
        while (toDateTime.isAfter(monthToRetrieveUsage));

        return usageHistoryList.stream()
                .filter(usageHistory -> !usageHistory.getTimeStamp().toInstant().isBefore(fromDateTime.toInstant()))
                .collect(Collectors.toList());
    }

    /**
     * Get device data usage for current month. This would include unbilled data
     *
     * @param iccid iccid of the cellular device
     * @return current data usage in bytes
     */
    @Override
    public long getDeviceCurrentMonthDataUsageBytes(String iccid)
    {

        try
        {
            final TerminalType terminal = getTerminalDetails(iccid);
            if (terminal == null) //device not found
            {
                return 0L;
            }
            return terminal.getMonthToDateDataUsage().longValue() * KILO_BITS * KILO_BITS;
        }
        catch (CnsException e)
        {
            log.error("Error retrieving data usage for current month", e);
            return 0L;   //FIXME should we return other values
        }
    }

    /**
     * From jasper apidoc, the EditTerminalRequestParamGroup/changeType for SIM status change is 3. The corresponding value
     * is listed below:
     *
     * @param iccid iccid of the CMIU
     * @return request id
     */
    @Override
    public String activateDevice(String iccid, String imei) throws CnsException
    {
        return changeDeviceProvisioningState(iccid, JasperSimStatus.ACTIVATED_NAME);
    }

    @Override
    public String suspendDevice(String iccid) throws CnsException
    {
        return changeDeviceProvisioningState(iccid, JasperSimStatus.RETIRED_NAME);
    }

    @Override
    public String deactivateDevice(String iccid) throws CnsException
    {
        return changeDeviceProvisioningState(iccid, JasperSimStatus.DEACTIVATED_NAME);
    }

    @Override
    public String restoreDevice(String iccid) throws CnsException
    {
        return changeDeviceProvisioningState(iccid, JasperSimStatus.ACTIVATED_NAME);
    }

    /**
     * Device connection event obtained through Jasper GetSessionInfoRequest??
     *
     * @param iccid       device iccid
     * @param fromDaysAgo days ago to retrieve the information
     * @return null
     */
    @Override
    public List<ConnectionEventDetail> getDeviceConnectionEvents(String iccid, int fromDaysAgo) throws CnsException
    {
        ZonedDateTime toDate = ZonedDateTime.now();
        ZonedDateTime fromDate = toDate.minus(fromDaysAgo, ChronoUnit.DAYS);

        return getDeviceConnectionEvents(iccid, fromDate, toDate);
    }

    /**
     * Device connection event is not available through Jasper API
     *
     * @param iccid    device iccid
     * @param fromDate from date
     * @param toDate   to date
     * @return null
     */
    @Override
    public List<ConnectionEventDetail> getDeviceConnectionEvents(String iccid, Date fromDate, Date toDate) throws CnsException
    {
        return getDeviceConnectionEvents(iccid, ZonedDateTime.ofInstant(fromDate.toInstant(), ZoneId.of("UTC")),
                ZonedDateTime.ofInstant(toDate.toInstant(), ZoneId.of("UTC")));
    }

    /**
     * Retrieve device usage history from Jasper API as an indication of connection events.
     *
     * @param iccid    device iccid
     * @param fromDate from date
     * @param toDate   to date
     * @return connection event detail list
     */
    private List<ConnectionEventDetail> getDeviceConnectionEvents(final String iccid, final ZonedDateTime fromDate, final ZonedDateTime toDate) throws CnsException
    {
        List<UsageHistory> usageHistoryList = getDeviceUsageHistory(iccid, fromDate, toDate);

        return usageHistoryList.stream()
                .map(usageHistory ->
                {
                    ConnectionEventDetail connectionEventDetail = new ConnectionEventDetail(iccid);
                    connectionEventDetail.setOccurredAt(usageHistory.getTimeStamp());
                    connectionEventDetail.setBytesUsed(usageHistory.getBytesUsed());

                    return connectionEventDetail;
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<DeviceCellularInformation> getDeviceList() throws CnsException
    {
        try
        {
            TerminalServiceStub service = new TerminalServiceStub(configContext, JasperServiceEndpoint.TERMINAL_SERVICE);
            service._getServiceClient().addHeader(getSecurityToken(this.accountDetails));

            GetModifiedTerminalsRequest request = createRequest(new GetModifiedTerminalsRequest());

            request.setAccountId(accountId);

            GetModifiedTerminalsResponse response = service.getModifiedTerminals(request);

            ArrayDeque<String> iccids = new ArrayDeque<>(Arrays.asList(response.getIccids().getIccid()));

            if (!iccids.isEmpty())
            {
                log.debug("Retrieving terminal details for {} devices", iccids.size());

                final List<DeviceCellularInformation> ret = new ArrayList<>(iccids.size());

                //Batch up iccids, and request details for each
                while (!iccids.isEmpty())
                {
                    Iccids_type3 iccidsBatch = new Iccids_type3();
                    int count = 0;

                    //Cannot request details for more than 50 iccids at once
                    while (count < 50 && !iccids.isEmpty())
                    {
                        iccidsBatch.addIccid(iccids.removeFirst());
                        count++;
                    }

                    if (count > 0)
                    {
                        //Get details for the batch of iccids
                        GetTerminalDetailsRequest detailsRequest = createRequest(new GetTerminalDetailsRequest());
                        detailsRequest.setIccids(iccidsBatch);
                        GetTerminalDetailsResponse detailsResponse = service.getTerminalDetails(detailsRequest);
                        Terminals_type1 terminals = detailsResponse.getTerminals();

                        for (TerminalType terminal : terminals.getTerminal())
                        {
                            ret.add(extractDeviceCellularInformation(terminal));
                        }
                    }
                }

                return ret;
            }
            else
            {
                return Collections.emptyList();
            }
        }
        catch (AxisFault axisFault)
        {
            throw new CnsException("Axis2 error while getting Jasper device list", axisFault);
        }
        catch (RemoteException e)
        {
            throw new CnsException("Remote exception while getting Jasper device list", e);
        }

    }

    /**
     * Device provisioning event is not available in Jasper API.. we will be using audit trial to retrieve audit events, which will contain
     * device provisioning history.
     *
     * @param iccid         device iccid
     * @param daysOfHistory how many days back to look
     * @return provisioning event
     */
    @Override
    public List<DeviceProvisioningEvent> getDeviceProvisioningEvents(String iccid, int daysOfHistory) throws CnsException
    {
        final TerminalServiceStub service;
        try
        {
            service = new TerminalServiceStub(configContext, JasperServiceEndpoint.TERMINAL_SERVICE);

            service._getServiceClient().addHeader(getSecurityToken(this.accountDetails));
            GetTerminalAuditTrailRequest request = createRequest(new GetTerminalAuditTrailRequest());
            request.setIccid(iccid);

            Calendar calendar = Calendar.getInstance();
            List<DeviceProvisioningEvent> deviceProvisioningEventList = new ArrayList<>();

            // If we want more then 60 days of history we will have to
            // split the days into 60 day chunks make a call for each and
            // account for the remaining days if any
            if (daysOfHistory > 60)
            {
                int iterationsOf60DayPeriods = daysOfHistory / 60;
                int remainingDays = daysOfHistory % 60;

                boolean isFirst60Days = true;

                for (int i = 0; iterationsOf60DayPeriods > i; i++)
                {
                    // Makes sure to get first 60 days
                    if (!isFirst60Days)
                    {
                        calendar.add(Calendar.DAY_OF_MONTH, -60);
                    }
                    isFirst60Days = false;;
                    request.setDate(calendar);

                    List<DeviceProvisioningEvent> events = executeGetDeviceProvisioningEvents(service, request);
                    if (!events.isEmpty())
                    {
                        deviceProvisioningEventList.addAll(events);
                    }
                }

                // Account for remaining days
                if (remainingDays > 0)
                {
                    calendar.add(Calendar.DAY_OF_MONTH, -60);
                    request.setDate(calendar);
                    request.setDaysOfHistory(remainingDays);
                    deviceProvisioningEventList.addAll(executeGetDeviceProvisioningEvents(service, request));
                }
            }
            else
            {
                request.setDaysOfHistory(daysOfHistory);
                request.setDate(calendar);
                deviceProvisioningEventList.addAll(executeGetDeviceProvisioningEvents(service, request));
            }

            return deviceProvisioningEventList;
        }
        catch (AxisFault axisFault)
        {
            if (axisFaultIsDeviceNotFound(axisFault))
            {
                return Collections.emptyList();
            }
            else
            {
                throw new CnsException("Error getting device provisioning events from Jasper for ICCID " + iccid +
                        "; SOAP Fault is: " + axisFault.getFaultDetailElement().toString(), axisFault);
            }
        }
        catch (RemoteException e)
        {
            throw new CnsException("Error getting device provisioning events from Jasper for ICCID " + iccid, e);
        }

    }

    /**
     * Gets Device Provisioning Events as a list
     *
     * @param service TerminalServiceStub
     * @param request GetTerminalAuditTrailRequest
     * @return List<DeviceProvisioningEvent>
     * @throws RemoteException
     */
    private static List<DeviceProvisioningEvent> executeGetDeviceProvisioningEvents(TerminalServiceStub service, GetTerminalAuditTrailRequest request) throws RemoteException
    {
        GetTerminalAuditTrailResponse response = service.getTerminalAuditTrail(request);


        TerminalAuditTrailType[] auditTrails = response.getTerminalAuditTrails().getTerminalAuditTrail();

        if (auditTrails == null)
        {
            return Collections.emptyList();
        }
        else
        {
            return Arrays.stream(auditTrails).map(terminalAuditTrailType ->
                    {
                        DeviceProvisioningEvent event = new DeviceProvisioningEvent();
                        event.setTimeStamp(terminalAuditTrailType.getEffectiveDate().getTime());
                        event.setAction(String.format("Field: %s -> from %s to %s by %s. Status: %s",
                                terminalAuditTrailType.getField(),
                                terminalAuditTrailType.getPriorValue(), terminalAuditTrailType.getValue(),
                                terminalAuditTrailType.getUserName(), terminalAuditTrailType.getStatus()));

                        return event;
                    })
                    .collect(Collectors.toList());
        }
    }

    /**
     * Jasper access credential cannot be changed via API
     *
     * @throws CnsException will not be thrown
     */
    @Override

    public String changeAccessCredential(String oldPassword) throws CnsException
    {
        return null;
    }

    @Override
    public String getBillingCycleUsageData(ZonedDateTime fromZoneDate, ZonedDateTime toZoneDate, String... iccidList)
    {
        //TODO implement GetTerminalUsage

        //For VZW, this sent a request for an async callback.. so the return string is the message id identifying the request..
        return null;
    }

    /**
     * Find the device information for cellular device with values matching the device identifier.
     *
     * @param deviceIdentifierKind  iccid, msisdn, or imei
     * @param deviceIdentifierValue values specified by the device identifier kind
     * @return device cellular information if a matching device is found, else null
     */
    @Override
    public DeviceCellularInformation getDeviceInformationFromIdentifier(DeviceIdentifierKind deviceIdentifierKind, String deviceIdentifierValue) throws CnsException
    {
        if (deviceIdentifierKind == DeviceIdentifierKind.ICCID)
        {
            try
            {
                return getDeviceInformation(deviceIdentifierValue);
            }
            catch (CnsException e)
            {
                log.debug("Error getting device information for ICCID " + deviceIdentifierValue, e);
                return null;
            }
        }
        else
        {
            //we need to find out the device from a list of all terminals
            List<DeviceCellularInformation> deviceList = getDeviceList();

            Optional<DeviceCellularInformation> device = deviceList.stream().filter(deviceCellularInformation -> {
                boolean foundDevice = false;

                switch (deviceIdentifierKind)
                {
                    case IMEI:
                        foundDevice = deviceCellularInformation.getImei().equalsIgnoreCase(deviceIdentifierValue);
                        break;

                    case MSISDN:
                        foundDevice = deviceCellularInformation.getMsisdn().equalsIgnoreCase(deviceIdentifierValue);
                        break;

                    //Ignore case statement for ICCID, since that has already processed by calling getDeviceInformation directly.
                }

                return foundDevice;
            }).findFirst();

            if (device.isPresent())
            {
                return getDeviceInformation(device.get().getIccid());
            }
            else
            {
                log.debug("No matching device found for device: {} = {}", deviceIdentifierKind, deviceIdentifierValue);
            }
        }

        return null;    //no matching device found or error
    }

    /**
     * Use SessionInfoRequest to find out whether the device is currently connected.
     *
     * @param iccid iccid of the terminal
     * @return array containing current information about the session, null if device is not active
     */
    private SessionInfoType[] getDeviceActiveSession(final String iccid)
    {
        final TerminalServiceStub service;
        try
        {
            service = new TerminalServiceStub(configContext, JasperServiceEndpoint.TERMINAL_SERVICE);

            service._getServiceClient().addHeader(getSecurityToken(this.accountDetails));

            GetSessionInfoRequest request = createRequest(new GetSessionInfoRequest());

            request.setIccid(new String[]{iccid});

            GetSessionInfoResponse response = service.getSessionInfo(request);

            return response.getSessionInfo().getSession();  //maybe return null if no session is active

        }
        catch (AxisFault axisFault)
        {
            log.error("Error getting jasper service", axisFault);
        }
        catch (RemoteException e)
        {
            log.error("Error executing request", e);
        }


        return null;
    }

    /**
     * Get the date time when the device last connect and establish a data session
     *
     * @param imsi IMSI of device
     * @return Instant representing the last connection date
     * @throws RemoteException, AxisFault
     */
    private Instant getDeviceLastConnectionDate(final String imsi) throws RemoteException
    {
        final TerminalServiceStub service;

        service = new TerminalServiceStub(configContext, JasperServiceEndpoint.TERMINAL_SERVICE);

        service._getServiceClient().addHeader(getSecurityToken(this.accountDetails));

        GetTerminalLatestRegistrationRequest request = createRequest(new GetTerminalLatestRegistrationRequest());

        GetTerminalLatestRegistrationRequestParamGroup paramGroup = new GetTerminalLatestRegistrationRequestParamGroup();
        paramGroup.setImsi(imsi);

        request.setGetTerminalLatestRegistrationRequestParamGroup(paramGroup);

        GetTerminalLatestRegistrationResponse response = service.getTerminalLatestRegistration(request);

        RegistrationInfo_type0 registrationInfo = response.getGetTerminalLatestRegistrationResponseParamGroup().getRegistrationInfo();

        return registrationInfo.getEventDate().toInstant();

    }

    /**
     * Generate login credentials to be inserted into SOAP header for all Jasper Request
     *
     * @param accountDetails username and password for accessing the Jasper account
     * @return SOAP element with the account detail populated for adding to the header
     */
    private static OMElement getSecurityToken(CnsAccountDetails accountDetails)
    {

        OMFactory factory = OMAbstractFactory.getOMFactory();

        OMNamespace namespaceWSSE = factory
                .createOMNamespace(
                        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "wsse");

        OMElement element = factory.createOMElement("Security", namespaceWSSE);

        OMAttribute attribute = factory.createOMAttribute("mustUnderstand", null, "1");

        element.addAttribute(attribute);


        OMElement element2 = factory.createOMElement("UsernameToken", namespaceWSSE);

        OMNamespace namespaceWSU = factory
                .createOMNamespace(
                        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd",
                        "wsu");

        attribute = factory.createOMAttribute("Id", namespaceWSU,
                "UsernameToken-1");

        element2.addAttribute(attribute);

        element.addChild(element2);

        OMElement element3 = factory.createOMElement("Username", namespaceWSSE);

        element3.setText(accountDetails.getUsername());

        OMElement element4 = factory.createOMElement("Password", namespaceWSSE);

        attribute = factory
                .createOMAttribute(
                        "Type",
                        null,
                        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");

        element4.setText(accountDetails.getPassword());

        element2.addChild(element3);
        element2.addChild(element4);

        return element;
    }


    private static boolean axisFaultIsDeviceNotFound(AxisFault axisFault)
    {
        final String message = axisFault.getMessage(); //message = code indicating fault
        switch (message)
        {
            case AXIS_FAULT_MESSAGE_DEVICE_NOT_FOUND:
            case AXIS_FAULT_MESSAGE_INVALID_ICCID:
            case AXIS_FAULT_MESSAGE_NO_TERMINAL_USAGE:
                return true;
            default:
                return false;
        }
    }
}
