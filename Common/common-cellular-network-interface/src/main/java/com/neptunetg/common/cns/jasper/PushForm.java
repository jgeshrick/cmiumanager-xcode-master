/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns.jasper;

/**
 * POJO for Jasper Push notification.
 * see https://jcc.jasperwireless.com/provision/secure/apidoc/pushApi.html for details
 */
public class PushForm
{
    private String eventId;
    private String eventType;
    private String timestamp;

    /**
     * HmacSHA1 signature supplied by Jasper Push Notification
     */
    private String signature;

    /**
     * HmacSHA256 signature supplied by Jasper Push Notification
     */
    private String signature2;

    private String data;

    public String getEventId()
    {
        return eventId;
    }

    public void setEventId(String eventId)
    {
        this.eventId = eventId;
    }

    public String getEventType()
    {
        return eventType;
    }

    public void setEventType(String eventType)
    {
        this.eventType = eventType;
    }

    public String getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(String timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getSignature()
    {
        return signature;
    }

    public void setSignature(String signature)
    {
        this.signature = signature;
    }

    public String getData()
    {
        return data;
    }

    public void setData(String data)
    {
        this.data = data;
    }

    public String getSignature2()
    {
        return signature2;
    }

    public void setSignature2(String signature2)
    {
        this.signature2 = signature2;
    }

    @Override
    public String toString()
    {
        return "Event id: " + eventId +
                " Event type: " + eventType +
                " Timestamp: " + timestamp +
                " Signature: " + signature +
                " Signature2: " + signature2 +
                " Data: " + data;
    }
}
