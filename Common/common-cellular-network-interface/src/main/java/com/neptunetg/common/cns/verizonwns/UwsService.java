/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns.verizonwns;

import com.verizonapi.service.AccountServiceStub;
import com.verizonapi.service.CallbackRegistrationServiceStub;
import com.verizonapi.service.CarrierServiceStub;
import com.verizonapi.service.DeviceGroupServiceStub;
import com.verizonapi.service.DeviceServiceStub;
import com.verizonapi.service.EnhancedConnectivityServiceStub;
import com.verizonapi.service.SessionServiceStub;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.soap.impl.llom.soap11.SOAP11Factory;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ConfigurationContext;

/**
 * Helper for generating commonly used objects for Verizon UWS API.
 */
public final class UwsService
{
    private static final long TIMEOUT_MS = 60000;

    /**
     * ConfigurationContext is cached to avoid
     * JAR files accumulating:
     * https://issues.apache.org/jira/browse/AXIS2-3919?focusedCommentId=13687774&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-13687774
     */
    private final ConfigurationContext configContext;

    public UwsService(ConfigurationContext configContext)
    {
            this.configContext = configContext;
    }

    public SessionServiceStub createSessionService() throws AxisFault
    {
        return new SessionServiceStub(configContext, UwsSoapDefinitions.getSessionServiceEndpoint());
    }

    public SessionServiceStub createSessionService(String sessionToken) throws AxisFault
    {
        return initialiseServiceStub(new SessionServiceStub(configContext, UwsSoapDefinitions.getSessionServiceEndpoint()),
                sessionToken);
    }

    public DeviceServiceStub createDeviceService(String sessionToken) throws AxisFault
    {
        DeviceServiceStub stub = new DeviceServiceStub(configContext, UwsSoapDefinitions.getDeviceServiceEndpoint());

        return initialiseServiceStub(stub, sessionToken);
    }

    public CarrierServiceStub createCarrierService(String sessionToken) throws AxisFault
    {
        return initialiseServiceStub(new CarrierServiceStub(configContext, UwsSoapDefinitions.getCarrierServiceEndpoint()),
                sessionToken);
    }

    public AccountServiceStub createAccountService(String sessionToken) throws AxisFault
    {
        return initialiseServiceStub(new AccountServiceStub(configContext, UwsSoapDefinitions.getAccountServiceEndpoint()),
                sessionToken);
    }

    public CallbackRegistrationServiceStub createCallbackRegistrationService(String sessionToken) throws AxisFault
    {
        return initialiseServiceStub(new CallbackRegistrationServiceStub(configContext, UwsSoapDefinitions.getCallbackRegistrationServiceEndpoint()),
                sessionToken);
    }

    public DeviceGroupServiceStub createDeviceGroupService(String sessionToken) throws AxisFault
    {
        return initialiseServiceStub(new DeviceGroupServiceStub(configContext, UwsSoapDefinitions.getDeviceGroupServiceEndpoint()),
                sessionToken);
    }

    public EnhancedConnectivityServiceStub createEnhancedConnectivityService(String sessionToken) throws AxisFault
    {
        return initialiseServiceStub(new EnhancedConnectivityServiceStub(configContext, UwsSoapDefinitions.getEnhancedConnectivityServiceEndpoint()),
                sessionToken);
    }

    private static <S extends org.apache.axis2.client.Stub> S initialiseServiceStub(S serviceStub, String sessionToken)
    {
        serviceStub._getServiceClient().addHeader(getSessionTokenOMElement(sessionToken));

        //extend timeout from default 30s
        serviceStub._getServiceClient().getOptions().setTimeOutInMilliSeconds(TIMEOUT_MS);

        return serviceStub;
    }

    /**
     * Utility to insert a session token into the header of a UWS SOAP request
     * @param sessionToken the session token generated from a UWS API login.
     * @return SOAP xml element
     */
    private static OMElement getSessionTokenOMElement(String sessionToken)
    {
        // Add session token to request header
        SOAP11Factory f = new SOAP11Factory();
        OMElement e = f.createOMElement("token", f.createOMNamespace(UwsSoapDefinitions.SOAP_ACTION, "v2"));
        e.setText(sessionToken);
        return e;
    }

}
