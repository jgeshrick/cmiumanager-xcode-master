/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns.verizonwns;

import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_common.ArrayOfDeviceIdentifier;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_common.ArrayOfDeviceIdentifierCollection;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_common.DeviceIdentifier;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_common.DeviceIdentifierCollection;

/**
 * A builder for building device identifier and collections required for DeviceIdentifiers parameter in Verizon UWS
 * request API.
 */
public final class DeviceIdentifierBuilder
{
    private ArrayOfDeviceIdentifier deviceIdentifierArray = new ArrayOfDeviceIdentifier();

    /**
     * Add a MSISDN DeviceIdentifier to the existing array.
     * @param iccidList    the iccid or a list of iccid
     * @return return the same object to enable chaining.
     */
    public DeviceIdentifierBuilder withIccid(String... iccidList)
    {
        for(String iccid : iccidList)
        {
            this.createAndAddDeviceIdentifier("ICCID", iccid);
        }

        return this;
    }

    /**
     * Add a MSISDN DeviceIdentifier to the existing array.
     * @param imei    the imei
     * @return return the same object to enable chaining.
     */
    public DeviceIdentifierBuilder withImei(String imei)
    {
        this.createAndAddDeviceIdentifier("IMEI", imei);
        return this;
    }

    /**
     * Add a MSISDN DeviceIdentifier to the existing array.
     * @param esn    the esn
     * @return return the same object to enable chaining.
     */
    public DeviceIdentifierBuilder withEsn(String esn)
    {
        this.createAndAddDeviceIdentifier("ESN", esn);
        return this;
    }

    /**
     * Add a MSISDN DeviceIdentifier to the existing array.
     * @param mdn    the mdn
     * @return return the same object to enable chaining.
     */
    public DeviceIdentifierBuilder withMdn(String mdn)
    {
        this.createAndAddDeviceIdentifier("MDN", mdn);
        return this;
    }

    /**
     * Add a MSISDN DeviceIdentifier to the existing array.
     * @param meid    the meid
     * @return return the same object to enable chaining.
     */
    public DeviceIdentifierBuilder withMeid(String meid)
    {
        this.createAndAddDeviceIdentifier("MEID", meid);
        return this;
    }

    /**
     * Add a MSISDN DeviceIdentifier to the existing array.
     * @param msisdn MSISDN
     * @return return the same object to enable chaining.
     */
    public DeviceIdentifierBuilder withMsisdn(String msisdn)
    {
        this.createAndAddDeviceIdentifier("MSISDN", msisdn);
        return this;
    }

    /**
     * Internal function to create a new DeviceIdentifier object based on supplied kind and identifier and add
     * to existing array.
     * @param kind The type of the device identifier
     * @param identifier value of the device identifier
     */
    private void createAndAddDeviceIdentifier(String kind, String identifier)
    {
        this.deviceIdentifierArray.addDeviceIdentifier(createDeviceIdentifier(kind, identifier));
    }

    /**
     * Collate the array of DeviceIdentifier constructed so far and return as a new Collection.
     * @return A newly created object based on previously created DeviceIdentifier
     */
    public ArrayOfDeviceIdentifierCollection asCollection()
    {
        // A DeviceIdentifierCollection holds the arrays of device IDs for all devices in the request
        DeviceIdentifierCollection deviceIdCollection = new DeviceIdentifierCollection();
        deviceIdCollection.setDeviceIdentifiers(this.deviceIdentifierArray);

        ArrayOfDeviceIdentifierCollection deviceList = new ArrayOfDeviceIdentifierCollection();
        deviceList.addDeviceIdentifierCollection(deviceIdCollection);

        return deviceList;
    }

    public ArrayOfDeviceIdentifier asArray()
    {
        return this.deviceIdentifierArray;
    }

    /**
     * Generate a DeviceIdentifier object with kind as ICCID
     * @param iccid the iccid of the device
     * @return new DeviceIdentifier object with ICCID defined.
     */
    public static DeviceIdentifier fromIccid(String iccid)
    {
        return createDeviceIdentifier("ICCID", iccid);
    }

    public static DeviceIdentifier fromImei(String imei)
    {
        return createDeviceIdentifier("IMEI", imei);
    }

    public static DeviceIdentifier fromMsisdn(String msisdn)
    {
        return createDeviceIdentifier("MSISDN", msisdn);
    }


    /**
     * Shorthand to generate a DeviceIdentifier object.
     * @param kind The type of the device identifier. Valid types are ESN, ICCID, IMEI, MDN, MEID, MSISDN
     * @param identifier The value of the device identifier.
     * @return A DeviceIdentifier object with kind and identifier field defined as per supplied parameter.
     */
    private static DeviceIdentifier createDeviceIdentifier(String kind, String identifier)
    {
        DeviceIdentifier deviceIdentifier = new DeviceIdentifier();
        deviceIdentifier.setKind(kind);
        deviceIdentifier.setIdentifier(identifier);
        return deviceIdentifier;
    }

}
