/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns.verizonwns;

import com.neptunetg.common.cns.*;
import com.neptunetg.common.cns.model.ConnectionEventDetail;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.common.cns.model.DeviceProvisioningEvent;
import com.neptunetg.common.cns.model.UsageHistory;
import com.nphase.unifiedwebservice.v2.ChangeDeviceState;
import com.nphase.unifiedwebservice.v2.ChangeDeviceStateResponse;
import com.nphase.unifiedwebservice.v2.GetAggregateDeviceUsage;
import com.nphase.unifiedwebservice.v2.GetAggregateDeviceUsageResponse;
import com.nphase.unifiedwebservice.v2.GetDeviceConnectionHistory;
import com.nphase.unifiedwebservice.v2.GetDeviceConnectionHistoryResponse;
import com.nphase.unifiedwebservice.v2.GetDeviceInformation;
import com.nphase.unifiedwebservice.v2.GetDeviceList;
import com.nphase.unifiedwebservice.v2.GetDeviceListResponse;
import com.nphase.unifiedwebservice.v2.GetDeviceProvisioningHistory;
import com.nphase.unifiedwebservice.v2.GetDeviceProvisioningHistoryResponse;
import com.nphase.unifiedwebservice.v2.GetDeviceUsageHistory;
import com.nphase.unifiedwebservice.v2.GetDeviceUsageHistoryResponse;
import com.nphase.unifiedwebservice.v2.GetNewPassword;
import com.nphase.unifiedwebservice.v2.GetNewPasswordResponse;
import com.nphase.unifiedwebservice.v2.LogIn;
import com.nphase.unifiedwebservice.v2.LogInResponse;
import com.nphase.unifiedwebservice.v2.LogOut;
import com.nphase.unifiedwebservice.v2.LogOutResponse;
import com.nphase.unifiedwebservice.v2.RegisterCallback;
import com.nphase.unifiedwebservice.v2.RegisterCallbackResponse;
import com.nphase.unifiedwebservice.v2.UnregisterCallback;
import com.nphase.unifiedwebservice.v2.UnregisterCallbackResponse;
import com.verizonapi.service.AccountServiceStub;
import com.verizonapi.service.CallbackRegistrationServiceStub;
import com.verizonapi.service.CarrierServiceStub;
import com.verizonapi.service.DeviceServiceStub;
import com.verizonapi.service.SessionServiceStub;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.databinding.ADBException;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_accountservice.GetNewPasswordRequest;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_callbackregistrationservice.RegisterCallbackRequest;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_callbackregistrationservice.UnregisterCallbackRequest;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_carrierservice.ActivateDeviceRequest;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_carrierservice.ChangeDeviceStateRequest;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_carrierservice.DeactivateDeviceRequest;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_carrierservice.RestoreDeviceRequest;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_carrierservice.SuspendDeviceRequest;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_common.ArrayOfDeviceIdentifier;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_common.ArrayOfDeviceIdentifierCollection;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_common.DeviceIdentifier;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_common.InclusiveDateTimeRange;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_deviceservice.*;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_sessionservice.LogInRequest;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_sessionservice.LogOutRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.rmi.RemoteException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of CNS using Verizon WNS v2
 */
public class VerizonCellularNetworkService implements CellularNetworkService
{
    private static final String AXIS_FAULT_DEVICE_NOT_FOUND = "DeviceService.REQUEST_FAILED.DeviceNotFound";

    private static final Logger log = LoggerFactory.getLogger(VerizonCellularNetworkService.class);

    private static Set<CellularStatus> acceptableCellularStates = new HashSet();

    /**
     * The Verizon account name where all device all be operating. This assumes there is only one account.
     */
    private final String uwsAccount;

    /**
     * The callback url that will be registered with this WNS API account.
     */
    private final String uwsCallbackUrl;

    private final UwsService uwsService;

    //reuse the sessionToken until it has expired. If that is the obtain a new sessionToken again.
    private String sessionToken = "";
    private String uwsUsername;
    private String uwsPassword;
    private String uwsServicePlan;

    public VerizonCellularNetworkService(
            ConfigurationContext configContext,
            CnsAccountDetails accountDetails)
    {
        this.uwsService = new UwsService(configContext);
        this.uwsAccount = accountDetails.getAccountName();
        this.uwsUsername = accountDetails.getUsername();
        this.uwsPassword = accountDetails.getPassword();
        this.uwsCallbackUrl = accountDetails.getCallbackUrl();
        this.uwsServicePlan = accountDetails.getServicePlan();
        acceptableCellularStates.add(CellularStatus.ACTIVE);
        acceptableCellularStates.add(CellularStatus.DE_ACTIVE);
        acceptableCellularStates.add(CellularStatus.PRE_ACTIVE);
        acceptableCellularStates.add(CellularStatus.RETIRED);
    }

    /**
     * Convert a nphase axis class containing device information to a MDCE class.
     *
     * @param deviceInfo the device information object obtained from WNS query.  May be null
     * @return our MDCE generic class containing device information, or null if argument was null
     */
    private static DeviceCellularInformation extractDeviceCellularInformation(DeviceInformation deviceInfo)
    {
        if (deviceInfo == null)
        {
            return null;
        }

        DeviceCellularInformation devInfo = new DeviceCellularInformation();

        Map<String, String> deviceIdentifierMap = Arrays.stream(deviceInfo.getDeviceIdentifiers().getDeviceIdentifier())
                .collect(Collectors.toMap(DeviceIdentifier::getKind, DeviceIdentifier::getIdentifier));

        devInfo.setIccid(deviceIdentifierMap.get("iccId"));
        devInfo.setImei(deviceIdentifierMap.get("imei"));
        devInfo.setMsisdn(deviceIdentifierMap.get("msisdn"));

        devInfo.setAccountName(deviceInfo.getAccountName());
        devInfo.setDeviceGroupName(String.join(",", deviceInfo.getDeviceGroupName().getString()));
        devInfo.setCreateAt(deviceInfo.getCreatedAt().getTime());

        devInfo.setIpAddress(deviceInfo.getIPAddress());

        if (deviceInfo.getLastActivationDate() != null)
        {
            devInfo.setLastActivationDate(deviceInfo.getLastActivationDate().getTime());
        }

        devInfo.setLastActivationBy(deviceInfo.getLastActivationBy());

        if (deviceInfo.getBillingCycleEndDate() != null)
        {
            devInfo.setBillingCycleEndDate(deviceInfo.getBillingCycleEndDate().getTime());
        }

        devInfo.setConnected(deviceInfo.getConnected());

        if (deviceInfo.getLastConnectionDate() != null)
        {
            devInfo.setLastConnectionDate(deviceInfo.getLastConnectionDate().getTime());
        }

        if (deviceInfo.getCustomFields() != null)
        {
            Map<String, String> customFieldMap = Arrays.stream(deviceInfo.getCustomFields().getCustomFieldObj())
                    .collect(Collectors.toMap(CustomFieldObj::getName, CustomFieldObj::getValue));
            devInfo.setCustomFields(customFieldMap.toString());
        }

        devInfo.setCarrierName(deviceInfo.getCarrierInformation().getCarrierInformation()[0].getCarrierName());
        devInfo.setServicePlan(deviceInfo.getCarrierInformation().getCarrierInformation()[0].getServicePlan());
        devInfo.setDeviceState(deviceInfo.getCarrierInformation().getCarrierInformation()[0].getState());
        return devInfo;
    }

    /**
     * Use to authenticate and retrieve a session token.
     * @param username The username to login to the UWS account
     * @param password The password to login to the UWS account
     *
     * @return true if login success
     */
    private boolean logIn(String username, String password)
    {
        boolean loginSuccess = false;
        LogInRequest request = new LogInRequest();

        request.setUsername(username);
        request.setPassword(password);

        LogIn loginObj = new LogIn();
        loginObj.setInput(request);

        try
        {
            SessionServiceStub sessionServiceStub = uwsService.createSessionService();

            // Send the request and get the response
            LogInResponse response = sessionServiceStub.logIn(loginObj);

            // Store the session token from the response
            this.sessionToken = response.getOutput().getSessionToken();

            log.debug("Login session token is {}", this.sessionToken);

            loginSuccess = true;
        }
        catch (RemoteException e)
        {
            log.error("Error executing login, {}, {}", e.getMessage(), e);
        }

        return loginSuccess;
    }

    private void logOut()
    {

        LogOutRequest request = new LogOutRequest();
        request.setSessionToken(this.sessionToken);

        LogOut logoutObj = new LogOut();
        logoutObj.setInput(request);

        try
        {
            SessionServiceStub sessionServiceStub = uwsService.createSessionService(this.sessionToken);

            // Send the request using the SessionServiceStub
            LogOutResponse response = sessionServiceStub.logOut(logoutObj);

        }
        catch (RemoteException e)
        {
            log.error("Error executing login, {}, {}", e.getMessage(), e);
        }
    }

    @Override
    public String getDeviceProvisioningStatus(String iccid) throws CnsException
    {
        final DeviceCellularInformation deviceCellularInfo  = this.getDeviceInformation(iccid);
        if (deviceCellularInfo == null)
        {
            return null;
        }
        else
        {
            return deviceCellularInfo.getDeviceState();
        }
    }

    @Override
    public List<UsageHistory> getDeviceUsageHistory(String iccid) throws CnsException
    {
        InclusiveDateTimeRange dateRange = InclusiveDateTimeRangeBuilder.fromDaysAgo(30);   //todo: set days ago

        return getUsageHistories(iccid, dateRange);
    }

    /**
     * Get device usage history for a specific date (UTC) only.
     */
    @Override
    public List<UsageHistory> getDeviceUsageHistory(String iccid, LocalDate usageDate) throws CnsException
    {
        Instant startInstant = usageDate.atStartOfDay().atZone(ZoneId.of("GMT")).toInstant();
        Instant endInstant = startInstant.plus(1, ChronoUnit.DAYS).minusMillis(1);

        return getUsageHistories(iccid, InclusiveDateTimeRangeBuilder.fromDateRange(
                Date.from(startInstant),Date.from(endInstant)));
    }

    @Override
    public List<UsageHistory> getDeviceUsageHistory(String iccid, ZonedDateTime fromDateTime, ZonedDateTime toDateTime) throws CnsException
    {
        return getUsageHistories(iccid, InclusiveDateTimeRangeBuilder.fromDateRange(fromDateTime, toDateTime));
    }

    @Override
    public long getDeviceCurrentMonthDataUsageBytes(String iccid) throws CnsException
    {
        ZonedDateTime dateTimeNow = ZonedDateTime.now();
        ZonedDateTime dateTimeStartOfMonth = dateTimeNow.with(TemporalAdjusters.firstDayOfMonth());

        //calculate start of month and get data usage
        List<UsageHistory> usageHistoryList = getUsageHistories(iccid, InclusiveDateTimeRangeBuilder.fromDateRange(dateTimeStartOfMonth, dateTimeNow));

        if (usageHistoryList != null)
        {
            return usageHistoryList.stream().mapToLong(UsageHistory::getBytesUsed).sum();
        }

        return 0;
    }

    private List<UsageHistory> getUsageHistories(final String iccid, InclusiveDateTimeRange dateRange) throws CnsException
    {
        VzwRequestResponseFunction<List<UsageHistory>> getDeviceUsageHistoryFunc = () ->
        {
            DeviceServiceStub deviceService = uwsService.createDeviceService(this.sessionToken);

            GetDeviceUsageHistory deviceUsageHistory = new GetDeviceUsageHistory();

            GetDeviceUsageHistoryRequest request = new GetDeviceUsageHistoryRequest();


            request.setDevice(DeviceIdentifierBuilder.fromIccid(iccid));
            request.setTimestampFilter(dateRange);

            deviceUsageHistory.setInput(request);

            GetDeviceUsageHistoryResponse response = null;
            List<UsageHistory> aggregateDeviceUsageList = new ArrayList<>();
            do
            {
                response = deviceService.getDeviceUsageHistory(deviceUsageHistory);

                final ArrayOfDeviceUsage usageHistoryArray = response.getOutput().getUsageHistory();
                if (usageHistoryArray != null && usageHistoryArray.getDeviceUsage() != null)
                {
                    List<UsageHistory> newDeviceUsageList = Arrays.stream(usageHistoryArray.getDeviceUsage())
                            .map(e ->
                            {
                                UsageHistory usageHistory = new UsageHistory(iccid);
                                usageHistory.setBytesUsed(e.getBytesUsed());
                                usageHistory.setServicePlan(e.getServicePlan());
                                usageHistory.setTimeStamp(e.getTimestamp().getTime());
                                usageHistory.setSmsUsed(e.getSmsUsed());
                                return usageHistory;
                            })
                            .collect(Collectors.toList());

                    if (!newDeviceUsageList.isEmpty())
                    {
                        aggregateDeviceUsageList.addAll(newDeviceUsageList);

                        Date lastRecordDate = newDeviceUsageList.get(newDeviceUsageList.size() - 1).getTimeStamp();
                        dateRange.setEarliest(InclusiveDateTimeRangeBuilder.toCalendar(lastRecordDate.getTime() + 1L));
                    }

                }
                else
                {
                    log.info("No monthly terminal data usage from Verizon for device: {} for cycleStartDate {}.", iccid, dateRange.getEarliest().toString());
                }
            }
            while(!response.getOutput().getIsComplete());   //repeat until all records to the date range have been retrieved

            return aggregateDeviceUsageList;
        };

        try
        {
            return executeWnsApiWithLoginRetry(getDeviceUsageHistoryFunc);
        }
        catch (AxisFault axisFault)
        {
            log.error("Exception while querying device usage history for " + iccid, axisFault);
            if (AXIS_FAULT_DEVICE_NOT_FOUND.equals(axisFault.getFaultCode().getLocalPart()))
            {
                //device not found
                return Collections.emptyList();
            }
            else
            {
                throw new CnsException("Error getting usage history for ICCID " + iccid, axisFault);
            }
        }
        catch (Exception e)
        {
            throw new CnsException("Error getting usage history for ICCID " + iccid, e);
        }
    }

    @Override
    public String activateDevice(final String iccid, final String imei) throws CnsException
    {
        VzwRequestResponseFunction<String> activateDeviceFunc = () ->
        {
            ArrayOfDeviceIdentifierCollection deviceIdentifierCollection = new DeviceIdentifierBuilder()
                    .withIccid(iccid)
                    .withImei(imei)
                    .asCollection();

            // Create request to change the device state to active
            ActivateDeviceRequest adr = new ActivateDeviceRequest();
            adr.setServicePlan(this.uwsServicePlan);    //required
            adr.setMdnZipCode("36078");                 //required

            ChangeDeviceStateRequest deviceStateRequest = new ChangeDeviceStateRequest();
            deviceStateRequest.setAccountName(this.uwsAccount);
            deviceStateRequest.setDeviceList(deviceIdentifierCollection);
            deviceStateRequest.setActivate(adr);

            ChangeDeviceStateResponse response = changeDeviceStateRequest(deviceStateRequest);
            String xml = response.getOMElement(null, OMAbstractFactory.getOMFactory()).toStringWithConsume();

            // For tutorial, display request ID
            log.debug("Activate request for device (iccid={}), acknowledged with request ID {}, XML used: {}",
                    iccid, response.getOutput().getRequestId(), xml);

            return xml;
        };

        try
        {
            return executeWnsApiWithLoginRetry(activateDeviceFunc);
        }
        catch (Exception e)
        {
            throw new CnsException("Error while activating VZW device iccid=" + iccid, e);
        }
    }

    @Override
    public String suspendDevice(String iccid) throws CnsException
    {
        VzwRequestResponseFunction<String> activateDeviceFunc = () ->
        {
            ArrayOfDeviceIdentifierCollection deviceIdentifierCollection = new DeviceIdentifierBuilder()
                    .withIccid(iccid)
                    .asCollection();

            // Create request to change the device state
            ChangeDeviceStateRequest deviceStateRequest = new ChangeDeviceStateRequest();
            deviceStateRequest.setDeviceList(deviceIdentifierCollection);
            deviceStateRequest.setSuspend(new SuspendDeviceRequest());

            String requestId = changeDeviceStateRequest(deviceStateRequest).getOutput().getRequestId();

            // For tutorial, display request ID
            log.info("Suspend request for device (iccid={}) acknowledged with request ID {}", iccid,  requestId);

            return requestId;
        };

        try
        {
            return executeWnsApiWithLoginRetry(activateDeviceFunc);
        }
        catch (Exception e)
        {
            throw new CnsException("Error while suspending VZW device iccid=" + iccid, e);
        }
    }

    @Override
    public String deactivateDevice(String iccid) throws CnsException
    {
        VzwRequestResponseFunction<String> activateDeviceFunc = () ->
        {
            ArrayOfDeviceIdentifierCollection deviceIdentifierCollection = new DeviceIdentifierBuilder()
                    .withIccid(iccid)
                    .asCollection();

            // Create request to change the device state
            ChangeDeviceStateRequest deviceStateRequest = new ChangeDeviceStateRequest();
            deviceStateRequest.setDeviceList(deviceIdentifierCollection);
            DeactivateDeviceRequest deactivateRequest = new DeactivateDeviceRequest();
            deactivateRequest.setReasonCode("FF");
            deactivateRequest.setEtfWaiver(false);

            deviceStateRequest.setDeactivate(deactivateRequest);

            String requestId = changeDeviceStateRequest(deviceStateRequest).getOutput().getRequestId();

            // For tutorial, display request ID
            log.info("Deactivate request for device (iccid={}) acknowledged with request ID {}", iccid, requestId);

            return requestId;
        };

        try
        {
            return executeWnsApiWithLoginRetry(activateDeviceFunc);
        }
        catch (Exception e)
        {
            throw new CnsException("Error while deactivating VZW device iccid=" + iccid, e);
        }
    }

    @Override
    public String restoreDevice(String iccid) throws CnsException
    {
        VzwRequestResponseFunction<String> activateDeviceFunc = () ->
        {
            ArrayOfDeviceIdentifierCollection deviceIdentifierCollection = new DeviceIdentifierBuilder()
                    .withIccid(iccid)
                    .asCollection();

            // Create request to change the device state
            ChangeDeviceStateRequest deviceStateRequest = new ChangeDeviceStateRequest();
            deviceStateRequest.setDeviceList(deviceIdentifierCollection);
            deviceStateRequest.setRestore(new RestoreDeviceRequest());

            String requestId = changeDeviceStateRequest(deviceStateRequest).getOutput().getRequestId();

            // For tutorial, display request ID
            log.info("Restore request for device (iccid={}) acknowledged with request ID {}", iccid,  requestId);

            return requestId;
        };

        try
        {
            return executeWnsApiWithLoginRetry(activateDeviceFunc);
        }
        catch (Exception e)
        {
            throw new CnsException("Error while restoring VZW device iccid=" + iccid, e);
        }
    }

    private ChangeDeviceStateResponse changeDeviceStateRequest(ChangeDeviceStateRequest deviceStateChangeRequest) throws RemoteException, XMLStreamException
    {
        ChangeDeviceState changeDeviceState = new ChangeDeviceState();
        changeDeviceState.setInput(deviceStateChangeRequest);

        CarrierServiceStub carrierServiceStub = uwsService.createCarrierService(this.sessionToken);

        // Send the request to change device state
        return carrierServiceStub.changeDeviceState(changeDeviceState);
    }

    @Override
    public List<ConnectionEventDetail> getDeviceConnectionEvents(String iccid, int fromDaysAgo) throws CnsException
    {
        InclusiveDateTimeRange occuredAtFilter = InclusiveDateTimeRangeBuilder.fromDaysAgo(fromDaysAgo);    //todo: set proper date range

        return this.getDeviceConnectionEvents(iccid, occuredAtFilter);
    }

    /**
     * Retrieve device connection event by splitting the request into multiple requests with smaller date range.
     * @param iccid iccid of the cmiu
     * @param dateTimeRange date range for the event
     * @return all retrieved connection event records between the date range
     */
    private List<ConnectionEventDetail> getDeviceConnectionEvents(String iccid, InclusiveDateTimeRange dateTimeRange) throws CnsException
    {
        Instant fromDate = dateTimeRange.getEarliest().toInstant();
        Instant toDate = dateTimeRange.getLatest().toInstant();

        if (fromDate.plus(1, ChronoUnit.DAYS).isBefore(toDate)) //date range is more than 1 day
        {
            //split into multiple queries with smaller date range
            Instant currentDate = fromDate;

            List<ConnectionEventDetail> allRecords = new ArrayList<>();

            while(!currentDate.isAfter(toDate))
            {
                InclusiveDateTimeRange shortdateRange = InclusiveDateTimeRangeBuilder.fromDateRange(
                        Date.from(currentDate),
                        Date.from(currentDate.plus(1, ChronoUnit.DAYS).minusMillis(1)));

                List<ConnectionEventDetail> dailyRecords = getDeviceConnectionEventsExec(iccid, shortdateRange);

                if (dailyRecords != null)
                {
                    allRecords.addAll(dailyRecords);
                }

                currentDate = currentDate.plus(1, ChronoUnit.DAYS);

            }

            return allRecords;
        }
        else
        {
            //get data from the entire date range at one go
            return getDeviceConnectionEventsExec(iccid, dateTimeRange);
        }
    }

    private List<ConnectionEventDetail> getDeviceConnectionEventsExec(final String iccid, InclusiveDateTimeRange dateTimeRange) throws CnsException
    {
        final String dateRangeDebug = String.format("%s, %s",
                dateTimeRange.getEarliest().toInstant().atZone(ZoneId.of("GMT")),
                dateTimeRange.getLatest().toInstant().atZone(ZoneId.of("GMT")));

        VzwRequestResponseFunction<List<ConnectionEventDetail>> getDeviceConnectionEventsFunc = () ->
            {
                class ConnectionEvent
                {
                    public Map<String, String> attributes;
                    public Date occurredAt;
                }

                GetDeviceConnectionHistoryRequest request = new GetDeviceConnectionHistoryRequest();
                request.setDevice(DeviceIdentifierBuilder.fromIccid(iccid));
                request.setOccurredAtFilter(dateTimeRange);

                GetDeviceConnectionHistory deviceConnectionHistory = new GetDeviceConnectionHistory();
                deviceConnectionHistory.setInput(request);

                DeviceServiceStub deviceService = uwsService.createDeviceService(this.sessionToken);
                GetDeviceConnectionHistoryResponse response = null;
                List<ConnectionEventDetail> aggregrateConnectionEventDetails = new ArrayList<>();

                int run = 0;    //internal counter to log how many times the SOAP API is being called to retrieve all data

                do
                {

                    log.info("Run {} - Get device connection history for {} between {} - {}",
                            ++run,
                            iccid,
                            dateTimeRange.getEarliest().toInstant().atZone(ZoneId.of("GMT")),
                            dateTimeRange.getLatest().toInstant().atZone(ZoneId.of("GMT")));

                    response = deviceService.getDeviceConnectionHistory(deviceConnectionHistory);

                    //get a list of connection event details from the response
                    if (response.getOutput().getConnectionHistory() != null)
                    {
                        List<ConnectionEventDetail> connectionEventDetails = Arrays.stream(response.getOutput().getConnectionHistory().getDeviceConnectionEvent())   //get deviceConnectionEvent[]
                                .map(e ->
                                {
                                    ConnectionEvent connectionEvent = new ConnectionEvent();

                                    //transform connection event attribute into a String map to ease extraction of values into our POJO
                                    Map<String, String> connectionEventAttributesMap = Arrays
                                            .stream(e.getConnectionEventAttributes().getConnectionEventAttribute())
                                            .collect(Collectors.toMap(ConnectionEventAttribute::getKey,
                                                    ConnectionEventAttribute::getValue));

                                    connectionEvent.attributes = connectionEventAttributesMap;
                                    connectionEvent.occurredAt = e.getOccurredAt().getTime();

                                    return connectionEvent;

                                })
                                .filter(e -> e.attributes.get("AccountStatusType").equals("Stop"))
                                .map(e -> {

                                    Map<String, String> connectionEventAttributesMap = e.attributes;

                                    ConnectionEventDetail connectionEventDetail = new ConnectionEventDetail(iccid);
                                    connectionEventDetail.setOccurredAt(e.occurredAt);
                                    connectionEventDetail.setDuration(Integer.parseInt(connectionEventAttributesMap.get("AccountSessionTime")));
                                    connectionEventDetail.setBytesUsed(Integer.parseInt(connectionEventAttributesMap.get("BytesUsed")));
                                    connectionEventDetail.setInputBytes(Integer.parseInt(connectionEventAttributesMap.get("InputBytes")));
                                    connectionEventDetail.setInputPackets(Integer.parseInt(connectionEventAttributesMap.get("AccountInputPackets")));
                                    connectionEventDetail.setOutputBytes(Integer.parseInt(connectionEventAttributesMap.get("OutputBytes")));
                                    connectionEventDetail.setOutputPackets(Integer.parseInt(connectionEventAttributesMap.get("AccountOutputPackets")));
                                    connectionEventDetail.setAttributes(connectionEventAttributesMap.toString());

                                    return connectionEventDetail;
                                })
                                .collect(Collectors.toList());

                        Date lastRecordDate = connectionEventDetails.get(connectionEventDetails.size() - 1).getOccurredAt();
                        dateTimeRange.setEarliest(InclusiveDateTimeRangeBuilder.toCalendar(lastRecordDate.getTime() + 1));

                        aggregrateConnectionEventDetails.addAll(connectionEventDetails);
                    }
                }
                while(!response.getOutput().getIsComplete());

                log.info("{} records retrieved for device iccid {} between date {}",
                        aggregrateConnectionEventDetails.size(), iccid, dateRangeDebug);

                return aggregrateConnectionEventDetails;
            };

        try
        {
            return executeWnsApiWithLoginRetry(getDeviceConnectionEventsFunc);
        }
        catch (AxisFault axisFault)
        {
            if (AXIS_FAULT_DEVICE_NOT_FOUND.equals(axisFault.getFaultCode().getLocalPart()))
            {
                //device not found
                return Collections.emptyList();
            }
            else
            {
                throw new CnsException("Error getting device connection events for ICCID " + iccid, axisFault);
            }
        }
        catch (Exception e)
        {
            throw new CnsException("Error getting device connection events for ICCID " + iccid, e);
        }
    }

    @Override
    public List<ConnectionEventDetail> getDeviceConnectionEvents(String iccid, Date fromDate, Date toDate) throws CnsException
    {
        final InclusiveDateTimeRange occuredAtFilter = InclusiveDateTimeRangeBuilder.fromDateRange(fromDate, toDate);

        return this.getDeviceConnectionEvents(iccid, occuredAtFilter);
    }

    @Override
    public List<DeviceCellularInformation> getDeviceList() throws CnsException
    {
        VzwRequestResponseFunction<List<DeviceCellularInformation>> getDeviceListFunc;
        getDeviceListFunc = () ->
        {
            DeviceServiceStub deviceService = uwsService.createDeviceService(this.sessionToken);

            List<DeviceCellularInformation> genericDeviceList = new ArrayList<>();

            GetDeviceList getDeviceList = new GetDeviceList();
            GetDeviceListRequest deviceListRequest = new GetDeviceListRequest();

            Calendar earliest = Calendar.getInstance();
            earliest.set(Calendar.YEAR, 1980); //Assume no Verizon Sims created before 1980

            Calendar latest = Calendar.getInstance();
            latest.add(Calendar.HOUR, 3); //Push the latest time into the future to insure we get all SIMs

            boolean isComplete = false;

            do
            {
                InclusiveDateTimeRange createdAtRange = new InclusiveDateTimeRange();
                createdAtRange.setEarliest(earliest);
                createdAtRange.setLatest(latest);
                deviceListRequest.setCreatedAtFilter(createdAtRange);

                getDeviceList.setInput(deviceListRequest);

                GetDeviceListResponse response = deviceService.getDeviceList(getDeviceList);
                isComplete = response.getOutput().getIsComplete();

                genericDeviceList.addAll(Arrays.stream(response.getOutput().getDevices().getDeviceInformation())
                        .map(VerizonCellularNetworkService::extractDeviceCellularInformation)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList()));

                earliest.setTime(genericDeviceList.get(genericDeviceList.size() - 1).getCreateAt());
                earliest.add(Calendar.MILLISECOND, 1);
            }
            while(!isComplete && genericDeviceList.size() != 0);

            return genericDeviceList;
        };

        try
        {
            return executeWnsApiWithLoginRetry(getDeviceListFunc);
        }
        catch (Exception e)
        {
            throw new CnsException("Error getting device list from Verizon", e);
        }
    }

    @Override
    public List<DeviceProvisioningEvent> getDeviceProvisioningEvents(String iccid, int daysOfHistory) throws CnsException
    {
        VzwRequestResponseFunction<List<DeviceProvisioningEvent>> getDeviceProvisioningEventFunc = () ->
        {
            DeviceServiceStub deviceService = uwsService.createDeviceService(this.sessionToken);

            GetDeviceProvisioningHistory deviceProvisioningHistory = new GetDeviceProvisioningHistory();
            GetDeviceProvisioningHistoryRequest request = new GetDeviceProvisioningHistoryRequest();
            request.setDevice(DeviceIdentifierBuilder.fromIccid(iccid));

            request.setOccurredAtFilter(InclusiveDateTimeRangeBuilder.fromDaysAgo(daysOfHistory));

            deviceProvisioningHistory.setInput(request);

            GetDeviceProvisioningHistoryResponse response = deviceService.getDeviceProvisioningHistory(deviceProvisioningHistory);

            List<DeviceProvisioningEvent> events = Collections.EMPTY_LIST;

            if (response.getOutput().getProvisioningHistory() != null)
            {
                events = Arrays.stream(response.getOutput().getProvisioningHistory().getProvisioningEvent())
                        .map(e ->
                        {
                            DeviceProvisioningEvent provisioningEvent = new DeviceProvisioningEvent();
                            provisioningEvent.setServicePlan(e.getServicePlan());
                            provisioningEvent.setMsisdn(e.getMsisdn());
                            provisioningEvent.setSuccess(e.getStatus().toLowerCase().equals("success"));
                            provisioningEvent.setTimeStamp(e.getOccurredAt().getTime());
                            provisioningEvent.setAction(e.getEventType());

                            return provisioningEvent;
                        })
                        .collect(Collectors.toList());
            }

            return events;
        };

        try
        {
            return executeWnsApiWithLoginRetry(getDeviceProvisioningEventFunc);
        }
        catch (AxisFault axisFault)
        {
            if (AXIS_FAULT_DEVICE_NOT_FOUND.equals(axisFault.getFaultCode().getLocalPart()))
            {
                //device not found
                return Collections.emptyList();
            }
            else
            {
                throw new CnsException("Error getting device provisioning events for ICCID " + iccid, axisFault);
            }
        }
        catch (Exception e)
        {
            throw new CnsException("Error getting device provisioning events for ICCID " + iccid, e);
        }
    }

    @Override
    public boolean registerCallback()
    {

        return Arrays.asList(
                    "CarrierService",   //Asynchronous responses from all Carrier Service APIs: ChangeDeviceCustomFields, ChangeDeviceIdentifier,
                                        // ChangeDeviceServicePlan, and ChangeDeviceState.
                    "DeviceUsage",      //Callback messages about device usage from Device Service APIs GetAggregateDeviceUsage and GetRatedDeviceUsage.
                    "ExternalProvisioningChanges"  //Device provisioning changes
                )
                .stream()
                .map(e -> this.registerCallback(e, this.uwsCallbackUrl))
                .allMatch(opsResult -> opsResult);
    }

    /**
     * Register callback
     * @param serviceName See https://m2mdeveloper.verizon.com/mediawiki/index.php/WNS2:Callback_Registration_Service for list of service name.
     * @param callbackUrl callback url to register to.
     */
    private boolean registerCallback(String serviceName, String callbackUrl)
    {
        VzwRequestResponseFunction<Boolean> registerCallbackFunc = () ->
        {
            RegisterCallbackRequest request = new RegisterCallbackRequest();
            request.setAccountName(this.uwsAccount);
            request.setServiceName(serviceName);
            request.setUrl(callbackUrl);

            RegisterCallback callback = new RegisterCallback();
            callback.setInput(request);

            // Create a new CallbackRegistrationService object
            CallbackRegistrationServiceStub callbackRegistrationServiceStub = uwsService.createCallbackRegistrationService(this.sessionToken);

            RegisterCallbackResponse response = callbackRegistrationServiceStub.registerCallback(callback);

            log.debug("Callback registered for service: {} to {}", response.getOutput().getServiceName(), callbackUrl);

            return true;
        };

        try
        {
            Boolean result = executeWnsApiWithLoginRetry(registerCallbackFunc);

            return Boolean.TRUE.equals(result);
        }
        catch (Exception e)
        {
            log.error("Error in registerCallbackFunc: ", e);
        }

        return false;
    }

    /**
     * Unregister to all callback services.
     * @return true only if all services are successfully unregistered, this is the case only if ALL services it
     * is attempting to unregister are currently registered and successfully unregistered.
     */
    @Override
    public boolean unregisterCallback()
    {
        //unregister exhaustively all callbacks
        Optional<Boolean> result = Arrays.asList(
                "CarrierService",
                "DevicePRLInformation",
                "DeviceUsage",
                "EnhancedConnectivityService",
                "SMSDeliveryConfirmation",
                "ExternalProvisioningChanges",
                "PromoChanges",
                "ResumeTrackingNotification",
                "EnhancedConnectivityService"
        )
                .stream()
                .map(this::unregisterCallback)
                .reduce((acc, item) -> acc && item);    //return true only if all unregister calls are successful.

        return result.isPresent() && result.get();
    }

    @Override
    public boolean getAllowedLifecycleState(CellularStatus cellularState) {
        return acceptableCellularStates.contains(cellularState);
    }

    private boolean unregisterCallback(String service)
    {
        VzwRequestResponseFunction<Boolean> unregisterCallbackFunc = () ->
        {
            UnregisterCallbackRequest request = new UnregisterCallbackRequest();
            request.setAccountName(this.uwsAccount);
            request.setServiceName(service);

            UnregisterCallback callback = new UnregisterCallback();
            callback.setInput(request);

            // Create a new CallbackRegistrationService object
            CallbackRegistrationServiceStub callbackRegistrationServiceStub = uwsService.createCallbackRegistrationService(this.sessionToken);

            UnregisterCallbackResponse response = callbackRegistrationServiceStub.unregisterCallback(callback);

            log.debug("Callback unregistered for service: " + response.getOutput().getServiceName());

            return Boolean.TRUE;
        };

        try
        {
            Boolean result = executeWnsApiWithLoginRetry(unregisterCallbackFunc);

            return result != null && result;
        }
        catch (Exception e)
        {
            log.error("Error in unregisterCallbackFunc: ", e);
            return false;
        }
    }

    @Override
    public DeviceCellularInformation getDeviceInformation(final String iccid) throws CnsException
    {
        return this.getDeviceInformationFromIdentifier(DeviceIdentifierKind.ICCID, iccid);
    }

    @Override
    public String changeAccessCredential(String oldPassword) throws CnsException
    {
        VzwRequestResponseFunction<String> getNewPasswordFunc;
        getNewPasswordFunc = () ->
        {
            AccountServiceStub accountService = uwsService.createAccountService(this.sessionToken);

            GetNewPassword getNewPassword = new GetNewPassword();
            GetNewPasswordRequest request = new GetNewPasswordRequest();
            request.setOldPassword(oldPassword);

            getNewPassword.setInput(request);

            GetNewPasswordResponse response = accountService.getNewPassword(getNewPassword);

            return response.getOutput().getNewPassword();
        };

        try
        {
            return executeWnsApiWithLoginRetry(getNewPasswordFunc);
        }
        catch (Exception e)
        {
            throw new CnsException("Error in changeAccessCredential", e);
        }
    }

    /**
     * A wrapper for executing Verizon WNS apis function. This takes care of performing the login to obtain a session token,
     * if it has expired. This method is also synchronised to prevent multiple invocations of the methods to interleave.
     * @param wnsApiFunction the lambda function for executing the WNS api
     * @param <R> The return parameter from the lambda function
     * @return The return object return by the lambda function
     * @throws Exception Remote exception
     */
    private synchronized <R> R executeWnsApiWithLoginRetry(VzwRequestResponseFunction<R> wnsApiFunction) throws Exception
    {
        final int maxLoginRetries = 1;
        int numberOfAttempts = 0;

        boolean retryWnsApiAfterLogin;
        //check we have sessionToken

        Exception lastException;
        do
        {
            retryWnsApiAfterLogin = false;

            try
            {
                log.debug("Executing api, attempt: #{}", numberOfAttempts);
                return wnsApiFunction.execute();
            }
            catch (AxisFault e1)
            {
                final String faultCodeValue = e1.getFaultCode().getLocalPart();

                if (faultCodeValue.contains("SessionToken.Expired") ||
                        faultCodeValue.contains("SessionToken.Invalid"))
                {
                    log.debug("Attempting login...");
                    //call login again
                    retryWnsApiAfterLogin = this.logIn(this.uwsUsername, this.uwsPassword);
                }
                lastException = e1;
            }

        }
        while (retryWnsApiAfterLogin && (numberOfAttempts++ < maxLoginRetries));

        throw lastException;
    }

    @Override
    public String getBillingCycleUsageData(ZonedDateTime fromZoneDate, ZonedDateTime toZoneDate, String... iccidList) throws CnsException
    {
        VzwRequestResponseFunction<String> getRatedDeviceUsageFunc = () ->
        {
            // Create a request
            GetAggregateDeviceUsage aggregateDeviceUsage = new GetAggregateDeviceUsage();

            ArrayOfDeviceIdentifier deviceIdentifierArray = new DeviceIdentifierBuilder()
                    .withIccid(iccidList)
                    .asArray();

            GetAggregateDeviceUsageRequest request = new GetAggregateDeviceUsageRequest();
            request.setDeviceList(deviceIdentifierArray);
            request.setTimestampFilter(InclusiveDateTimeRangeBuilder.fromTimeRange(fromZoneDate, toZoneDate));
            aggregateDeviceUsage.setInput(request);

            DeviceServiceStub deviceServiceStub = uwsService.createDeviceService(this.sessionToken);

            // Make the request
            GetAggregateDeviceUsageResponse response = deviceServiceStub.getAggregateDeviceUsage(aggregateDeviceUsage);

            //wait for callback, which will be handled by SoapEndPoint and result submitted to RDS
            //TODO to implement callback backend for receiving the callback data wrt this request


            return response.getOutput().getRequestId();
        };

        try
        {
            return executeWnsApiWithLoginRetry(getRatedDeviceUsageFunc);
        }
        catch (Exception e)
        {
            throw new CnsException("Error getting aggregate device usage for date range " + fromZoneDate + " - " + toZoneDate +
                    " for ICCIDs " + String.join(", ", iccidList), e);
        }

    }

    /**
     * Query a device information from a given device identifier
     * @param deviceIdentifierKind the kind of device identifier, valid value "iccid", "msisdn", "imei", case not important
     * @param deviceIdentifierValue the value of the device identifier
     * @return Device cellular information related the specific device
     */
    @Override
    public DeviceCellularInformation getDeviceInformationFromIdentifier(DeviceIdentifierKind deviceIdentifierKind, String deviceIdentifierValue)
        throws CnsException
    {
        VzwRequestResponseFunction<DeviceCellularInformation> getDeviceInformationFunc = () ->
        {
            // Create a device information request
            GetDeviceInformationRequest deviceInformationRequest = new GetDeviceInformationRequest();

            switch (deviceIdentifierKind)
            {
                case ICCID:
                    deviceInformationRequest.setDevice(DeviceIdentifierBuilder.fromIccid(deviceIdentifierValue));
                    break;

                case IMEI:
                    deviceInformationRequest.setDevice(DeviceIdentifierBuilder.fromImei(deviceIdentifierValue));
                    break;

                case MSISDN:
                    deviceInformationRequest.setDevice(DeviceIdentifierBuilder.fromMsisdn(deviceIdentifierValue));
                    break;
            }

            GetDeviceInformation deviceInformation = new GetDeviceInformation();
            deviceInformation.setInput(deviceInformationRequest);

            DeviceServiceStub deviceServiceStub = uwsService.createDeviceService(this.sessionToken);

            // Make the request
            final com.nphase.unifiedwebservice.v2.GetDeviceInformationResponse response = deviceServiceStub.getDeviceInformation(deviceInformation);

            if (response == null)
            {
                return null;
            }

            final org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_deviceservice.GetDeviceInformationResponse output = response.getOutput();
            if (output == null)
            {
                return null;
            }

            final DeviceInformation device = response.getOutput().getDevice();

            if (device == null)
            {
                return null;
            }

            final DeviceCellularInformation devInfo = extractDeviceCellularInformation(device);

            // Find the device state
            final ArrayOfCarrierInformation arrayOfCarrierInformation = device.getCarrierInformation();

            if (arrayOfCarrierInformation != null)
            {
                final CarrierInformation[] carrierInformation = arrayOfCarrierInformation.getCarrierInformation();
                if (carrierInformation != null && carrierInformation.length > 0)
                {
                    CarrierInformation carrierInfo = carrierInformation[0];
                    log.info("Carrier info: name:{}, state:{}, service plan:{}", carrierInfo.getCarrierName(), carrierInfo.getState(), carrierInfo.getServicePlan());
                }
            }

            return devInfo;
        };

        try
        {
            return executeWnsApiWithLoginRetry(getDeviceInformationFunc);
        }
        catch (Exception e)
        {
            throw new CnsException("Error getting cellular device information for " + deviceIdentifierKind.format(deviceIdentifierValue), e);
        }
    }

    //define a functional interface
    @FunctionalInterface
    private interface VzwRequestResponseFunction<R>
    {
        R execute() throws Exception;
    }

}
