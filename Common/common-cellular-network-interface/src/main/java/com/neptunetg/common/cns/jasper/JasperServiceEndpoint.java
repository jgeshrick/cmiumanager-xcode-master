/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns.jasper;

/**
 * SOAP Address for Jasper wireless service production.
 */
public final class JasperServiceEndpoint
{
    private static final String JASPER_WIRELESS_SERVICE_BASE_URL = "https://api.jasperwireless.com/ws/service/";
    public static final String TERMINAL_SERVICE = JASPER_WIRELESS_SERVICE_BASE_URL + "terminal";
    public static final String BILLING_SERVICE = JASPER_WIRELESS_SERVICE_BASE_URL + "billing";
    public static final String ECHO_SERVICE = JASPER_WIRELESS_SERVICE_BASE_URL + "echo";
    public static final String ALERT_SERVICE = JASPER_WIRELESS_SERVICE_BASE_URL + "Alert";
    public static final String NETWORK_ACCESS_SERVICE = JASPER_WIRELESS_SERVICE_BASE_URL + "networkaccess";
    public static final String EVENT_PLAN_SERVICE = JASPER_WIRELESS_SERVICE_BASE_URL + "eventplan";
    public static final String ORDER_SERVICE = JASPER_WIRELESS_SERVICE_BASE_URL + "order";
}
