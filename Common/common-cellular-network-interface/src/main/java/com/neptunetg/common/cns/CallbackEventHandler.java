/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Interface for handling cellular network callback.
 * The handlers are use to handling 1) SIM state change; 2) Usage notification 3) Usage Limit 4) Pool usage limit
 */
public abstract class CallbackEventHandler
{
    private static final Logger logger = LoggerFactory.getLogger(CallbackEventHandler.class);

    //function handler for saving any device state change to backend
    private TriFunction<DeviceIdentifierKind, String, String, Boolean> deviceStateChangeHandler;

    //function handler for device data usage current monthly threshold
    private BiFunction<String, Integer, Boolean> deviceUsageNotificationHandler;

    //function handler when pool data usage has exceed a limit
    private Function<Integer, Boolean> pooledDataUsageLimitHandler;

    //function handler when device daily (24h) usage has exceed a certain threshold
    private BiFunction<String, Integer, Boolean> deviceDailyUsageLimitHandler;

    //function handler when the device has not connect for the last 24h
    private Function<String, Boolean> noConnectionNotificationHandler;

    /**
     * Function to handle device sim state (in Jasper)/provision state (in Verizon) update.
     * @param functionHandler takes new device msisdn as string in first argument, device state as string in second
     *                        argrument and returns true if it is processed
     */
    public void setDeviceStateChangeHandler(TriFunction<DeviceIdentifierKind, String, String, Boolean> functionHandler)
    {
        this.deviceStateChangeHandler = functionHandler;
    }

    /**
     * Function to handle device usage reports.
     * @param functionHandler takes usage in bytes as input and return true if it is processed
     */
    public void setDeviceUsageNotificationHandler(BiFunction<String, Integer, Boolean> functionHandler)
    {
        this.deviceUsageNotificationHandler = functionHandler;
    }

    /**
     * Function to handle device pooled data usage limit notification
     * @param functionHandler takes usage in bytes as input and return true if it is processed
     */
    public void setPooledDataUsageLimitHandler(Function<Integer, Boolean> functionHandler)
    {
        this.pooledDataUsageLimitHandler = functionHandler;
    }

    public Function<String, Boolean> getNoConnectionNotificationHandler()
    {
        return noConnectionNotificationHandler;
    }

    public Function<Integer, Boolean> getPooledDataUsageLimitHandler()
    {
        return pooledDataUsageLimitHandler;
    }

    public BiFunction<String, Integer, Boolean> getDeviceDailyUsageLimitHandler()
    {
        return deviceDailyUsageLimitHandler;
    }

    public void setDeviceDailyUsageLimitHandler(BiFunction<String, Integer, Boolean> deviceDailyUsageLimitHandler)
    {
        this.deviceDailyUsageLimitHandler = deviceDailyUsageLimitHandler;
    }

    public void setNoConnectionNotificationHandler(Function<String, Boolean> noConnectionNotificationHandler)
    {
        this.noConnectionNotificationHandler = noConnectionNotificationHandler;
    }

    /**
     * execute device state change handler if it has been registered
     * @param deviceIdentifierKind the cellular device identifier kind, permitted values are iccid and msisdn
     * @param deviceIdentifierValue the value corresponding to the device identifier kind
     * @param currentState current device state
     * @return true if handler is executed successfully
     */
    protected boolean executeDeviceStateChangeHandler(final DeviceIdentifierKind deviceIdentifierKind, final String deviceIdentifierValue, final String currentState)
    {
        if (isFunctionHandlerDefined(deviceStateChangeHandler, "Cellular network service device state change callback"))
        {
            deviceStateChangeHandler.apply(deviceIdentifierKind, deviceIdentifierValue, currentState);
            return true;
        }

        return false;

    }

    protected boolean executeDeviceUsageNotification(final String iccid, final int useBytes)
    {
        if (isFunctionHandlerDefined(deviceUsageNotificationHandler, "Cycle to date device data usage warning"))
        {
            deviceUsageNotificationHandler.apply(iccid, useBytes);
            return true;
        }

        return false;
    }

    /**
     * The device has been offline for a set period of time which cause an alert to be pushed from the CNS.
     * @param iccid iccid of the device
     * @return return true if the handler is executed
     */
    protected boolean executeDeviceNoConnectionNotification(final String iccid)
    {
        if (isFunctionHandlerDefined(noConnectionNotificationHandler, "No device connection in past 24H"))
        {
            noConnectionNotificationHandler.apply(iccid);
            return true;
        }

        return false;
    }

    protected boolean executeDeviceDailyUsageLimitHandler(final String iccid, final int dataUsageBytes)
    {
        if (isFunctionHandlerDefined(deviceDailyUsageLimitHandler, "Past 24 hour usage limit"))
        {
            deviceDailyUsageLimitHandler.apply(iccid, dataUsageBytes);   //TODO check data usage is bytes not KB
            return true;
        }

        return false;
    }

    protected boolean executePooledDataUsageLimitHandler(final int pooledDataUsageBytes)
    {
        if (isFunctionHandlerDefined(pooledDataUsageLimitHandler, "Pooled data usage limit"))
        {
            pooledDataUsageLimitHandler.apply(pooledDataUsageBytes);   //TODO check data usage is bytes not KB
            return true;
        }

        return false;
    }

    private boolean isFunctionHandlerDefined(final Object functionHandler, final String handlerDescription)
    {
        if (functionHandler == null)
        {
            logger.warn("No function defined to handle: " + handlerDescription);
            return false;
        }

        return true;
    }

    /**
     * Functional interface that accepts 3 arguments as input argument and return a result
     * @param <In1>
     * @param <In2>
     * @param <In3>
     * @param <Out>
     */
    @FunctionalInterface
    public interface TriFunction<In1, In2, In3, Out>
    {
        public Out apply(In1 arg1, In2 arg2, In3 arg3 );
    }
}
