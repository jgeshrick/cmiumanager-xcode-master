/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns.verizonwns;

import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_common.InclusiveDateTimeRange;
import org.datacontract.schemas._2004._07.nphase_unifiedwebservice_apis_v2_contract_common.TimeRange;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * Builder to help generate InclusiveDateTimeRange object.
 */
public class InclusiveDateTimeRangeBuilder
{
    private long fromUnixTime;
    private long toUnixTime;

    /**
     * Generate dateTimeRange using a fromDate and a toDate
     * @param fromDate start date of the range in MM/dd/yyyy
     * @param toDate end date of the range in MM/dd/yyyy
     * @return the dateTimeRange object with earliest and latest field set.
     * @throws ParseException cannot parse the supply to or from date string
     */
    public static InclusiveDateTimeRange fromDateRange(String fromDate, String toDate) throws ParseException
    {
        final String dateFormat = "MM/dd/yyyy";

        Calendar calStart = Calendar.getInstance();
        calStart.setTime(new SimpleDateFormat(dateFormat).parse(fromDate));

        Calendar calEnd = Calendar.getInstance();
        calEnd.setTime(new SimpleDateFormat(dateFormat).parse(toDate));

        InclusiveDateTimeRange dateRange = new InclusiveDateTimeRange();
        dateRange.setEarliest(calStart);
        dateRange.setLatest(calEnd);

        return dateRange;
    }

    /**
     * Generate dateTimeRange based on a range
     * @param fromDate state date of the range
     * @param toDate end date of the range
     * @return the dateTimeRange object with earliest and latest field set.
     */
    public static InclusiveDateTimeRange fromDateRange(Date fromDate, Date toDate)
    {
        Calendar calStart = Calendar.getInstance();
        calStart.setTime(fromDate);

        Calendar calEnd = Calendar.getInstance();
        calEnd.setTime(toDate);

        InclusiveDateTimeRange dateRange = new InclusiveDateTimeRange();
        dateRange.setEarliest(calStart);
        dateRange.setLatest(calEnd);

        return dateRange;
    }

    public static InclusiveDateTimeRange fromDateRange(ZonedDateTime fromDate, ZonedDateTime toDate)
    {
        return fromDateRange(
                Date.from(fromDate.toInstant()),
                Date.from(toDate.toInstant())
        );
    }

    public static TimeRange fromTimeRange(ZonedDateTime fromDate, ZonedDateTime toDate)
    {
        Calendar calStart = Calendar.getInstance();
        calStart.setTime(Date.from(fromDate.toInstant()));

        Calendar calEnd = Calendar.getInstance();
        calEnd.setTime(Date.from(toDate.toInstant()));

        TimeRange timeRange = new TimeRange();
        timeRange.setStart(calStart);
        timeRange.setEnd(calEnd);

        return timeRange;
    }

    /**
     * Generate dateTime with range from a specified number of days ago to today.
     * @param daysAgo the number of day ago from today
     * @return the dateTimeRange object with earliest and latest field set.
     */
    public static InclusiveDateTimeRange fromDaysAgo(int daysAgo)
    {
        Calendar calNow = Calendar.getInstance();

        Calendar calStart = (Calendar) calNow.clone();
        calStart.add(Calendar.DAY_OF_MONTH, -daysAgo);

        InclusiveDateTimeRange dateRange = new InclusiveDateTimeRange();
        dateRange.setEarliest(calStart);
        dateRange.setLatest(calNow);

        return dateRange;
    }

    /**
     * Set the start time based on unix time. Can be chained together to build InclusiveDateTimeRange.
     * @param fromTime start time, epoch time in milliseconds
     * @return the builder object
     */
    public InclusiveDateTimeRangeBuilder fromUnixTime(long fromTime)
    {
        this.fromUnixTime = fromTime;
        return this;
    }

    /**
     * Set the end time based on unix time. Can be chained together to build InclusiveDateTimeRange.
     * @param toTime end time epoch time in milliseconds
     * @return the builder object
     */
    public InclusiveDateTimeRangeBuilder toUnixTime(long toTime)
    {
        this.fromUnixTime = toTime;
        return this;
    }

    /**
     * Build dateTimeRange from object.
     * @return DateTimeRange for Verizon m2m platform.
     */
    public InclusiveDateTimeRange toInclusiveDateTimeRange()
    {
        InclusiveDateTimeRange dateRange = new InclusiveDateTimeRange();

        Calendar calStart = Calendar.getInstance();
        calStart.setTimeInMillis(this.fromUnixTime);
        dateRange.setEarliest(calStart);

        Calendar calEnd = Calendar.getInstance();
        calEnd.setTimeInMillis(this.toUnixTime);
        dateRange.setEarliest(calEnd);

        return dateRange;
    }

    /**
     * Get a calendar object from epoch time in milliseconds
     * @param epochTimeMs Epoch time in milliseconds
     * @return Calendar object
     */
    public static Calendar toCalendar(long epochTimeMs)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(epochTimeMs);
        return cal;
    }


}
