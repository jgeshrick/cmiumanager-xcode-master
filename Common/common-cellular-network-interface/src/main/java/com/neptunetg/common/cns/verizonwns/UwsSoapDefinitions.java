/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns.verizonwns;

/**
 * A single point to define Verizon SOAP url and endpoints
 */
public final class UwsSoapDefinitions
{
    private static final String SOAP_ADDRESS = "https://uws.apps.nphase.com/api/v2/";
    //private static final String SOAP_ADDRESS = "http://localhost:8088/"; //use for local testing without connecting to server
    public static final String SOAP_ACTION = "http://nphase.com/unifiedwebservice/v2";

    private static final String SVC_SESSION_SERVICE = "SessionService.svc?wsdl";
    private static final String SVC_ACCOUNT_SERVICE = "AccountService.svc?wsdl";
    private static final String SVC_CALLBACK_REGISTRATION_SERVICE = "CallbackRegistrationService.svc?wsdl";
    private static final String SVC_CARRIER_SERVICE = "CarrierService.svc?wsdl";
    private static final String SVC_DEVICE_GROUP_SERVICE = "DeviceGroupService.svc?wsdl";
    private static final String SVC_DEVICE_SERVICE = "DeviceService.svc?wsdl";
    private static final String SVC_ENHANCED_CONNECTIVITY_SERVICE = "EnhancedConnectivityService.svc?wsdl";

    public static final String SESSION_SERVICE_END_POINT = SOAP_ADDRESS + SVC_SESSION_SERVICE;

    /**
     * Get the url endpoint for Verizon UWS session service
     * @return url to UWS service
     */
    public static String getSessionServiceEndpoint()
    {
        return SESSION_SERVICE_END_POINT;
    }

    /**
     * Get the url endpoint for Verizon UWS account service
     * @return url to UWS service
     */
    public static String getAccountServiceEndpoint()
    {
        return SOAP_ADDRESS + SVC_ACCOUNT_SERVICE;
    }

    /**
     * Get the url endpoint for Verizon UWS callback service
     * @return url to UWS service
     */
    public static String getCallbackRegistrationServiceEndpoint()
    {
        return SOAP_ADDRESS + SVC_CALLBACK_REGISTRATION_SERVICE;
    }

    /**
     * Get the url endpoint for Verizon UWS carrier service
     * @return url to UWS service
     */
    public static String getCarrierServiceEndpoint()
    {
        return SOAP_ADDRESS + SVC_CARRIER_SERVICE;
    }

    /**
     * Get the url endpoint for Verizon UWS device group service
     * @return url to UWS service
     */
    public static String getDeviceGroupServiceEndpoint()
    {
        return SOAP_ADDRESS + SVC_DEVICE_GROUP_SERVICE;
    }

    /**
     * Get the url endpoint for Verizon UWS device service
     * @return url to UWS service
     */
    public static String getDeviceServiceEndpoint()
    {
        return SOAP_ADDRESS + SVC_DEVICE_SERVICE;
    }

    /**
     * Get the url endpoint for Verizon UWS device service
     * @return url to UWS service
     */
    public static String getEnhancedConnectivityServiceEndpoint()
    {
        return SOAP_ADDRESS + SVC_ENHANCED_CONNECTIVITY_SERVICE;
    }


}
