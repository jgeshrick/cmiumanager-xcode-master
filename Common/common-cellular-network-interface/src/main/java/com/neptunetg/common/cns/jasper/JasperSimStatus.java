/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns.jasper;

import com.neptunetg.common.cns.CellularStatus;
import com.neptunetg.common.cns.CnsException;

/**
 * enum representation of Jasper API SimStatus for editTerminal and GetTerminalDetailsResponse
 */
public enum JasperSimStatus
{
    TEST_READY_NAME(CellularStatus.ACTIVE),
    INVENTORY_NAME(CellularStatus.PRE_ACTIVE),
    TRIAL_NAME(CellularStatus.PRE_ACTIVE),
    ACTIVATION_READY_NAME(CellularStatus.ACTIVE),
    ACTIVATED_NAME(CellularStatus.ACTIVE),
    DEACTIVATED_NAME(CellularStatus.RETIRED), // Was DE_ACTIVE
    RETIRED_NAME(CellularStatus.RETIRED),
    PURGED_NAME(CellularStatus.RETIRED),
    REPLACED_NAME(CellularStatus.RETIRED),
    UNKNOWN_STATUS(CellularStatus.UNKNOWN);


    private final CellularStatus cellularStatus;

    private JasperSimStatus(CellularStatus cellularStatus)
    {
        this.cellularStatus = cellularStatus;
    }

    /**
     * Get Jasper Sim status enum from string
     * @param simStatus sim status string input
     * @return JasperSimStatus enumeration for use in EditTerminals Jasper API
     */
    public static JasperSimStatus fromString(String simStatus)
    {
        for (JasperSimStatus status : JasperSimStatus.values())
        {
            if (status.toString().equalsIgnoreCase(simStatus))
            {
                return status;
            }
        }

        return UNKNOWN_STATUS;
    }

    public static JasperSimStatus fromCellularStatus(CellularStatus status) throws CnsException
    {
        switch (status)
        {
            case PRE_ACTIVE:
                return INVENTORY_NAME;

            case ACTIVE:
                return ACTIVATED_NAME;

            case DE_ACTIVE:
                return DEACTIVATED_NAME;

            case RETIRED:
                return RETIRED_NAME;

            default:
            throw new CnsException("Cannot transform CellularStatus " + status + " into Jasper Sim Status");
        }
    }

    /**
     * Return the equivalent state representation
     * @return cellular status common to mdce
     */
    public CellularStatus toCellularStatus()
    {
        return cellularStatus;
    }
}
