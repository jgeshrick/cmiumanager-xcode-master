/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.cns.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Cellular device daily usage history.
 */
public class UsageHistory
{
    private final String iccid;
    private Date timeStamp;         //!< The date and time when the event occurred
    private String servicePlan;     //!< The service plan in effect when the event occurred.
    private long bytesUsed;         //!< The number of bytes that the device sent or received as a result of this device usage event.
    private int smsUsed;            //!< the number of SMS messages that occurred as a results of this device usage event.

    @JsonCreator
    public UsageHistory(@JsonProperty("iccid") String iccid)
    {
        this.iccid = iccid;
    }

    public String getIccid()
    {
        return this.iccid;
    }

    public Date getTimeStamp()
    {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp)
    {
        this.timeStamp = timeStamp;
    }

    public String getServicePlan()
    {
        return servicePlan;
    }

    public void setServicePlan(String servicePlan)
    {
        this.servicePlan = servicePlan;
    }

    public long getBytesUsed()
    {
        return bytesUsed;
    }

    public void setBytesUsed(long bytesUsed)
    {
        this.bytesUsed = bytesUsed;
    }

    public int getSmsUsed()
    {
        return smsUsed;
    }

    public void setSmsUsed(int smsUsed)
    {
        this.smsUsed = smsUsed;
    }

    public String toCsv()
    {
        return String.format("%s,%s,%s,%s,%d,%d",
                iccid,
                ZonedDateTime.ofInstant(timeStamp.toInstant(), ZoneId.of("GMT")).format(DateTimeFormatter.ISO_ZONED_DATE_TIME),
                ZonedDateTime.ofInstant(timeStamp.toInstant(), ZoneId.of(ZoneId.SHORT_IDS.get("CST"))).format(DateTimeFormatter.ISO_ZONED_DATE_TIME),
                servicePlan, bytesUsed, smsUsed);
    }

    public static String getCsvHeader()
    {
        return "ICCID, Date (GMT}, Date (CST), Service plan, Bytes used, SMS used";
    }
}
