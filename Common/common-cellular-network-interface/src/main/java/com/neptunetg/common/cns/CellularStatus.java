/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns;

/**
 * Describe the network status of the cellular device.
 * Jasper and Verizon each have their own name and different device provisioning cycle which are not
 * exactly one-to-one mapped.
 */
public enum CellularStatus
{
    UNKNOWN,    //unknown status
    PRE_ACTIVE, //device has been added and only active on the network
    ACTIVE,     //device can connect to network and establish data connection
    DE_ACTIVE,   //device cannot establish data connection
    RETIRED;     //device cannot connect to network, preparing to be suspended/taken out of commission

    public static CellularStatus fromString(final String status)
    {
        for(CellularStatus cellularStatus: CellularStatus.values())
        {
            if (cellularStatus.name().equalsIgnoreCase(status))
            {
                return cellularStatus;
            }
        }

        //cannot find matching status
        return UNKNOWN;
    }

}
