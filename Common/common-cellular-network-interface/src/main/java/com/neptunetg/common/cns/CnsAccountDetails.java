/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns;

/**
 * Information to be passed to the corresponding cellular network service for authenticating with the CNS API
 * and connecting to the respective API accounts.
 */
public class CnsAccountDetails
{
    private final String username;
    private final String password;
    private final String accountName;   //VZW account name
    private final String callbackUrl;   //VZW callback url setup
    private final String servicePlan;   //VZW service plan number
    private final String licenseKey;    //for holding Jasper license key


    public CnsAccountDetails(String username, String password, String accountName, String servicePlan,
                             String licenseKey, String callbackUrl)
    {
        this.username = username;
        this.password = password;
        this.accountName = accountName;
        this.licenseKey = licenseKey;
        this.servicePlan = servicePlan;
        this.callbackUrl = callbackUrl;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }

    public String getAccountName()
    {
        return accountName;
    }

    public String getServicePlan()
    {
        return servicePlan;
    }

    public String getCallbackUrl()
    {
        return callbackUrl;
    }

    public String getLicenseKey()
    {
        return licenseKey;
    }
}
