/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.cns.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Cellular device information
 */
public class DeviceCellularInformation
{
    private String iccid;
    private String imei;
    private String msisdn;

    @JsonProperty("account_name")
    private String accountName;

    @JsonProperty("device_group_name")
    private String deviceGroupName;

    @JsonProperty("createAt")
    private Date createAt;

    @JsonProperty("ip_address")
    private String ipAddress;

    @JsonProperty("last_activate_date")
    private Date lastActivationDate;

    @JsonIgnore
    private String lastActivationBy;

    @JsonProperty("billing_cycle_end_date")
    private Date billingCycleEndDate;

    @JsonProperty("connected")
    private boolean connected;

    @JsonProperty("last_connection_date")
    private Date lastConnectionDate;

    @JsonIgnore
    private String customFields;

    @JsonIgnore
    private String carrierName;

    @JsonIgnore
    private String servicePlan;

    @JsonProperty("device_state")
    private String deviceState;

    public String getIccid()
    {
        return iccid;
    }

    public void setIccid(String iccid)
    {
        this.iccid = iccid;
    }

    public String getImei()
    {
        return imei;
    }

    public void setImei(String imei)
    {
        this.imei = imei;
    }

    public String getMsisdn()
    {
        return msisdn;
    }

    public void setMsisdn(String msisdn)
    {
        this.msisdn = msisdn;
    }

    public String getAccountName()
    {
        return accountName;
    }

    public void setAccountName(String accountName)
    {
        this.accountName = accountName;
    }

    public String getDeviceGroupName()
    {
        return deviceGroupName;
    }

    public void setDeviceGroupName(String deviceGroupName)
    {
        this.deviceGroupName = deviceGroupName;
    }

    public Date getCreateAt()
    {
        return createAt;
    }

    public void setCreateAt(Date createAt)
    {
        this.createAt = createAt;
    }

    public String getIpAddress()
    {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    public Date getLastActivationDate()
    {
        return lastActivationDate;
    }

    public void setLastActivationDate(Date lastActivationDate)
    {
        this.lastActivationDate = lastActivationDate;
    }

    public String getLastActivationBy()
    {
        return lastActivationBy;
    }

    public void setLastActivationBy(String lastActivationBy)
    {
        this.lastActivationBy = lastActivationBy;
    }

    public Date getBillingCycleEndDate()
    {
        return billingCycleEndDate;
    }

    public void setBillingCycleEndDate(Date billingCycleEndDate)
    {
        this.billingCycleEndDate = billingCycleEndDate;
    }

    public boolean isConnected()
    {
        return connected;
    }

    public void setConnected(boolean connected)
    {
        this.connected = connected;
    }

    public Date getLastConnectionDate()
    {
        return lastConnectionDate;
    }

    public void setLastConnectionDate(Date lastConnectionDate)
    {
        this.lastConnectionDate = lastConnectionDate;
    }

    public String getCustomFields()
    {
        return customFields;
    }

    public void setCustomFields(String customFields)
    {
        this.customFields = customFields;
    }

    public String getCarrierName()
    {
        return carrierName;
    }

    public void setCarrierName(String carrierName)
    {
        this.carrierName = carrierName;
    }

    public String getServicePlan()
    {
        return servicePlan;
    }

    public void setServicePlan(String servicePlan)
    {
        this.servicePlan = servicePlan;
    }

    public String getDeviceState()
    {
        return deviceState;
    }

    public void setDeviceState(String deviceState)
    {
        this.deviceState = deviceState;
    }

    @Override
    public String toString()
    {
        return "DeviceCellularInformation{" +
                "iccid='" + iccid + '\'' +
                ", imei='" + imei + '\'' +
                ", msisdn='" + msisdn + '\'' +
                ", accountName='" + accountName + '\'' +
                ", deviceGroupName='" + deviceGroupName + '\'' +
                ", createAt=" + createAt +
                ", ipAddress='" + ipAddress + '\'' +
                ", lastActivationDate=" + lastActivationDate +
                ", lastActivationBy='" + lastActivationBy + '\'' +
                ", billingCycleEndDate=" + billingCycleEndDate +
                ", connected=" + connected +
                ", lastConnectionDate=" + lastConnectionDate +
                ", customFields='" + customFields + '\'' +
                ", carrierName='" + carrierName + '\'' +
                ", servicePlan='" + servicePlan + '\'' +
                ", deviceState='" + deviceState + '\'' +
                '}';
    }
}
