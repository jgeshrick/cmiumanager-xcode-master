/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns;

import com.neptunetg.common.cns.model.ConnectionEventDetail;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.common.cns.model.DeviceProvisioningEvent;
import com.neptunetg.common.cns.model.UsageHistory;

import java.rmi.RemoteException;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

/**
 * Define the interface for CNS.
 */
public interface CellularNetworkService
{
    /**
     * Register Verizon m2m callback. Note only 1 server instance should register to callback. The registration IP
     * should be the load balancer.
     * @return true on success
     */
    boolean registerCallback();

    /**
     * Unregister all callback from Verizon m2m server.
     * @return true on success
     */
    boolean unregisterCallback();

    /**
     * //TODO Fill this in
     */
    boolean getAllowedLifecycleState(CellularStatus cellularState);

    /**
     * Retrieve a cellular device detail, such as identifier (MSISDN etc), IP address etc
     * @param iccid    iccid of the device
     * @return device cellular detail, or null if device not found
     * @throws CnsException if an error occurred
     */
    DeviceCellularInformation getDeviceInformation(String iccid) throws CnsException;

    /**
     * Get device cellular information from an cellular identifier iccid, imei etc
     * @param deviceIdentifierKind iccid, imei, msisdn etc
     * @param deviceIdentifierValue the value corresponding to the identifier kind
     * @return device cellular detail, or null if device not found
     * @throws CnsException if an error occurred
     */
    DeviceCellularInformation getDeviceInformationFromIdentifier(DeviceIdentifierKind deviceIdentifierKind, String deviceIdentifierValue) throws CnsException;

    /**
     * Get the provisioning status - activate, suspended etc.
     * @param iccid    iccid of the device
     * @return provisioning status, or null if device not found
     * @throws CnsException if an error occurred
     */
    String getDeviceProvisioningStatus(String iccid) throws CnsException;

    /**
     * Retrieve usage history
     * @param iccid    iccid of the device
     * @return usage history, empty list if device does not exist
     * @throws CnsException if something else goes wrong other than that the device does not exist
     */
    List<UsageHistory> getDeviceUsageHistory(String iccid) throws CnsException;

    /**
     * Get device usage history
     * @param iccid    iccid of the device
     * @param usageDate usage date
     * @return device usage history, empty list if device does not exist
     * @throws CnsException if something else goes wrong other than that the device does not exist
     */
    List<UsageHistory> getDeviceUsageHistory(String iccid, LocalDate usageDate) throws CnsException;

    /**
     * Get device usage history from a date range
     * @param iccid iccid of the device
     * @param fromDateTime from date
     * @param toDateTime to date
     * @return device usage history, empty list if device does not exist
     * @throws CnsException if something else goes wrong other than that the device does not exist
     */
    List<UsageHistory> getDeviceUsageHistory(String iccid, ZonedDateTime fromDateTime, ZonedDateTime toDateTime) throws CnsException;

    /**
     * Get current device data usage for the current month to date.
     * @param iccid iccid of the cellular device
     * @return device data usage in bytes, or 0L if device does not exist
     * @throws CnsException if something else goes wrong other than that the device does not exist
     */
    long getDeviceCurrentMonthDataUsageBytes(String iccid) throws CnsException;

    /**
     * Change the device state to activate
     * @param iccid iccid fo the device
     * @return XML used for request if available
     * @throws CnsException if something goes wrong, including that the device does not exist
     */
    String activateDevice(String iccid, String imei) throws CnsException;

    /**
     * Change the device state to suspended state
     * @param iccid iccid fo the device
     * @return request id if CNS provides it
     * @throws CnsException if something goes wrong, including that the device does not exist
     */
    String suspendDevice(String iccid) throws CnsException;

    /**
     * Change the device state to deactivate state
     * @param iccid iccid fo the device
     * @return request id if CNS provides it
     * @throws CnsException if something goes wrong, including that the device does not exist
     */
    String deactivateDevice(String iccid) throws CnsException;

    /**
     * Change the device state from suspended to activated
     * @param iccid iccid fo the device
     * @return request id if CNS provides it
     * @throws CnsException if something goes wrong, including that the device does not exist
     */
    String restoreDevice(String iccid) throws CnsException;

    /**
     * Get connection history of a device
     * @param iccid iccid of the device
     * @param fromDaysAgo days ago
     * @return connection history, empty list if device does not exist
     * @throws CnsException if something else goes wrong other than that the device does not exist
     */
    List<ConnectionEventDetail> getDeviceConnectionEvents(String iccid, int fromDaysAgo) throws CnsException;

    /**
     * Get connection history of a device
     * @param iccid iccid of the device
     * @param fromDate from date
     * @param toDate to date
     * @return connection history, empty list if device does not exist
     * @throws CnsException if something else goes wrong other than that the device does not exist
     */
    List<ConnectionEventDetail> getDeviceConnectionEvents(String iccid, Date fromDate, Date toDate) throws CnsException;

    /**
     * Get a list of devices in the account
     * @return device list
     * @throws CnsException error in calling CNS api
     */
    List<DeviceCellularInformation> getDeviceList() throws CnsException;

    /**
     * Get device state change history
     * @param iccid iccid of the device
     * @param daysOfHistory how many days of history
     * @return device state change history, empty list if device does not exist
     * @throws CnsException if something else goes wrong other than that the device does not exist
     */
    List<DeviceProvisioningEvent> getDeviceProvisioningEvents(String iccid, int daysOfHistory) throws CnsException;

    /**
     * Change account password (for UWS only)
     * @param oldPassword old password
     * @return new password
     * @throws CnsException error in calling CNS api
     */
    String changeAccessCredential(String oldPassword) throws CnsException;

    /**
     * Queue a request for billing cycle usage data from a list of iccid. The result is returned via a callback. This is
     * for UWS only
     * @param fromZoneDate from date
     * @param toZoneDate to date
     * @param iccidList list of iccid
     * @return request id
     * @throws CnsException if something goes wrong
     */
    String getBillingCycleUsageData(ZonedDateTime fromZoneDate, ZonedDateTime toZoneDate, String... iccidList) throws CnsException;

}
