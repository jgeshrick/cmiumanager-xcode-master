/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.cns.model;

import java.util.Date;

/**
 * Cellular device daily usage history.
 */
public class DeviceProvisioningEvent
{
    private Date timeStamp;         //!< The date and time when the event occurred
    private Boolean success;        //!< Whether the provisioning event is successful.
    private String msisdn;          //!< The MSISDN assigned to the device
    private String action;  //!< the provision action.
    private String servicePlan;     //!< The service plan in effect when the event occurred.

    public Date getTimeStamp()
    {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp)
    {
        this.timeStamp = timeStamp;
    }

    public Boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(Boolean success)
    {
        this.success = success;
    }

    public String getMsisdn()
    {
        return msisdn;
    }

    public void setMsisdn(String msisdn)
    {
        this.msisdn = msisdn;
    }

    public String getServicePlan()
    {
        return servicePlan;
    }

    public void setServicePlan(String servicePlan)
    {
        this.servicePlan = servicePlan;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }
}
