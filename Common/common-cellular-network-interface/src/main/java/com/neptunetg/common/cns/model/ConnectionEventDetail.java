/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.cns.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Holds details for a device connection event.
 * This is loosely mapped to the Verizon DeviceConnectionEvent to collate ConnectionEventAttribute associated to
 * a DeviceConnectionEvent.
 */
public class ConnectionEventDetail
{
    private final String iccid;
    private Date occurredAt;
    private long durationSeconds;
    private long bytesUsed;
    private long inputBytes;
    private long outputBytes;
    private long inputPackets;
    private long outputPackets;

    /**
     * Encapsulate all connection attributes retrieved from the m2m platform, in key, value pair.
     */
    private String attributes;

    @JsonCreator
    public ConnectionEventDetail(@JsonProperty("iccid") String iccid)
    {
        this.iccid = iccid;
    }

    public String getIccid()
    {
        return iccid;
    }

    public Date getOccurredAt()
    {
        return occurredAt;
    }

    public void setOccurredAt(Date occurredAt)
    {
        this.occurredAt = occurredAt;
    }

    public long getBytesUsed()
    {
        return bytesUsed;
    }

    public void setBytesUsed(long bytesUsed)
    {
        this.bytesUsed = bytesUsed;
    }

    public long getInputBytes()
    {
        return inputBytes;
    }

    public void setInputBytes(long inputBytes)
    {
        this.inputBytes = inputBytes;
    }

    public long getOutputBytes()
    {
        return outputBytes;
    }

    public void setOutputBytes(long outputBytes)
    {
        this.outputBytes = outputBytes;
    }

    public long getInputPackets()
    {
        return inputPackets;
    }

    public void setInputPackets(long inputPackets)
    {
        this.inputPackets = inputPackets;
    }

    public long getOutputPackets()
    {
        return outputPackets;
    }

    public void setOutputPackets(long outputPackets)
    {
        this.outputPackets = outputPackets;
    }

    public long getDuration() { return durationSeconds; }

    public void setDuration(long durationSeconds) { this.durationSeconds = durationSeconds; }

    public String getAttributes()
    {
        return attributes;
    }

    public void setAttributes(String attributes)
    {
        this.attributes = attributes;
    }

    public String toCsv()
    {
        String result =  String.format("%s, %s,%s, %d, %d,%d,%d,%d, %d, %s",
                iccid,
                ZonedDateTime.ofInstant(occurredAt.toInstant(), ZoneId.of("GMT")).format(DateTimeFormatter.ISO_ZONED_DATE_TIME),
                ZonedDateTime.ofInstant(occurredAt.toInstant(), ZoneId.of(ZoneId.SHORT_IDS.get("CST"))).format(DateTimeFormatter.ISO_ZONED_DATE_TIME),
                durationSeconds,
                bytesUsed,
                inputBytes,
                outputBytes,
                inputPackets,
                outputPackets,
                attributes);

        return result;
    }

    public static String getCsvHeader()
    {
        return "ICCID, Date (GMT), Date (CST), Duration (s), Bytes used, Input bytes, output bytes, Input packets, Output packets, Attributes";
    }

}
