/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns;

/**
 * General exception for CellularNetworkInterface class
 */
public class CnsException extends Exception
{
    public CnsException(String message)
    {
        super(message);
    }

    public CnsException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
