/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns.jasper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sml1 on 04/02/2016.
 */
public enum JasperEventType
{
    RecentDataUsage("RECENT_DATA_USAGE_(24 HOURS)"),
    MonthlyPooledDataUsage("MONTHLY_POOLED_DATA_USAGE"),
    NoConnection("SESSION_NO_CONNECTION"),
    TooManyConnections("TOO_MANY_CONNECTIONS_(CYCLE_TO_DATE)"),
    NumberOfSessionConnections("NUMBER_OF_SESSION_CONNECTIONS_(24_HOURS)"),
    CycleToDateUsage("CTD_USAGE"),
    SimStateChange("SIM_STATE_CHANGE"),
    SimRatePlanChange("SIM_RATE_PLAN_CHANGE"),
    IMEIChange("IMEI_CHANGE"),
    SessionStart("SESSION_START"),
    SessionStop("SESSION_STOP"),
    Past24HDataUsageExceed("PAST24H_DATA_USAGE_EXCEEDED"),
    UnknownEventType("Unknown");

    private final String eventTypeIdentifier;

    private static final Logger logger = LoggerFactory.getLogger(JasperAutomationNotificationControllerBase.class);
    private JasperEventType(String eventTypeIdentifier)
    {
        this.eventTypeIdentifier = eventTypeIdentifier;
    }

    public String getEventTypeIdentifier()
    {
        return eventTypeIdentifier;
    }

    public static JasperEventType fromEventTypeString(final String eventType)
    {
        for (JasperEventType et : JasperEventType.values())
        {
            if (et.getEventTypeIdentifier().equalsIgnoreCase(eventType))
            {
                return et;
            }
        }

        logger.error("Cannot match incoming Jasper event type: " + eventType);
        return UnknownEventType;
    }

    public boolean isEqual(final String eventType)
    {
        return this.getEventTypeIdentifier().equalsIgnoreCase(eventType);
    }


}
