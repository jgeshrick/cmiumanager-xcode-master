/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns;

import com.neptunetg.common.cns.jasper.JasperCellularNetworkService;
import com.neptunetg.common.cns.verizonwns.VerizonCellularNetworkService;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;

/**
 * Create and return an implementor of CellularNetworkService interface to caller.
 */
public final class CellularNetworkServiceFactory
{
    private final ConfigurationContext configContext;

    public CellularNetworkServiceFactory()
    {
        try
        {
            this.configContext = ConfigurationContextFactory.createConfigurationContextFromFileSystem(null, null);
        }
        catch (Exception e)
        {
            throw new Error("Cannot initialize CNS factory", e);
        }
    }


    public CellularNetworkService getCellularNetworkService(
            final ServiceProviderName serviceProvider,
            final CnsAccountDetails accountDetails)
    {
        switch (serviceProvider)
        {
            case VERIZON:
                return new VerizonCellularNetworkService(configContext, accountDetails);

            case JASPER:
                return new JasperCellularNetworkService(configContext, accountDetails);

            default:
                throw new IllegalArgumentException("Invalid cellular service provider: " + serviceProvider);
        }
    }

    /**
     * Specify the cellular network service provider that the factory is going to create.
     */
    public enum ServiceProviderName
    {
        VERIZON,    //Verizon UWS service
        JASPER,     //JASPER service
    }


}
