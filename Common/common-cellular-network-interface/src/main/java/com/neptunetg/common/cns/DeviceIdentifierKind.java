/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns;

/**
 * Enum for identifier for cellular device, either iccid, imei or msisdn
 */
public enum DeviceIdentifierKind
{
    UNKNOWN,
    ICCID,
    IMEI,
    MSISDN;

    /**
     * Get enum from device identifier string
     * @param deviceIdentifierString iccid, imei or msisdn, ignore casing
     * @return enum it it matches the input string, else unidentified
     */
    public static DeviceIdentifierKind from(final String deviceIdentifierString)
    {
        for(DeviceIdentifierKind deviceIdentifier: DeviceIdentifierKind.values())
        {
            if (deviceIdentifier.name().equalsIgnoreCase(deviceIdentifierString))
            {
                return deviceIdentifier;
            }
        }

        throw new RuntimeException("Unrecognised cellular device identifier string " + deviceIdentifierString);

    }

    public boolean isUnknown()
    {
        return UNKNOWN.equals(this);
    }

    public boolean isModemProperty()
    {
        return IMEI.equals(this);
    }

    public boolean isSimProperty()
    {
        return ICCID.equals(this) || MSISDN.equals(this);
    }

    public String format(String id)
    {
        return name() + " " + (id == null ? "(null)" : id.trim());
    }

}
