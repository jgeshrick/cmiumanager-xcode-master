/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns.verizonwns;

import com.neptunetg.common.cns.CallbackEventHandler;
import com.neptunetg.common.cns.DeviceIdentifierKind;
import com.neptunetg.common.cns.MnoCallbackException;
import com.verizon.callback.CallbackData;
import com.verizon.callback.CallbackRequest;
import com.verizon.callback.CallbackResponse;
import com.verizon.callback.CarrierServiceCallbackRequest;
import com.verizon.callback.ChangeDeviceStateResponse;
import com.verizon.callback.DeviceIdentifier;
import com.verizon.callback.DeviceUsage;
import com.verizon.callback.ExternalProvisioningChange;
import com.verizon.callback.SoapFault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;

import javax.xml.bind.JAXBElement;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Base class implementation for handling Verizon UWS callback.
 */
public abstract class VerizonCallbackSoapControllerBase extends CallbackEventHandler
{
    private static final Logger log = LoggerFactory.getLogger(VerizonCallbackSoapControllerBase.class);

    public CallbackResponse processCallbackRequest(@RequestPayload CallbackRequest request) throws MnoCallbackException
    {
        log.debug("VZW callback: \n {} \n", request);

        CallbackResponse callbackResponse = new CallbackResponse();
        callbackResponse.setRequestId(request.getRequestId());

        //process the data
        boolean isSuccess = processCallback(request);

        //TODO, do not return response if the callback is not successful.

        return callbackResponse;
    }

    /**
     * Process SOAP callback from Verizon UWS.
     * @param request Callback request deserialize from Verizon SOAP callback.
     * @throws MnoCallbackException
     * @return true if the callback has been handled successfully
     */
    private boolean processCallback(CallbackRequest request) throws MnoCallbackException
    {
        final String CALLBACK_SERVICE_DEVICE_USAGE = "Usage";
        final String CALLBACK_SERVICE_DEVICE_STATE_CHANGE = "ChangeDeviceState";
        final String CALLBACK_SERVICE_EXTERNAL_PROVISIONING_CHANGE = "ExternalProvisioningChange";

        CallbackData callbackData = request.getData().getData();
        String callbackService = getCallbackService(callbackData);
        String comment = callbackData.getComment().getValue();
        String faultDescription = getCallbackFault(callbackData);

        String message = "Callback received. " +
                "; Type: " + callbackService +
                "; Fault: " + faultDescription +
                "; Comment: " + comment +
                "; Request id: " + request.getRequestId();

        if (request.getDevice() != null)
        {
            message += "; Device: " + request.getDevice().getKind() + " = " + request.getDevice().getIdentifier();
        }

        log.debug(message);

        boolean result = false;

        if (faultDescription.isEmpty())
        {
            //device usage
            if (callbackService.contains(CALLBACK_SERVICE_DEVICE_USAGE))
            {
                log.info("Callback: " + CALLBACK_SERVICE_DEVICE_USAGE );
                result = processDeviceUsage(callbackData);
            }
            else if (callbackService.contains(CALLBACK_SERVICE_DEVICE_STATE_CHANGE))
            {
                log.info("Callback: " + CALLBACK_SERVICE_DEVICE_STATE_CHANGE );
                result = processDeviceStateChange(request, callbackData);
            }
            else if (callbackService.contains(CALLBACK_SERVICE_EXTERNAL_PROVISIONING_CHANGE))
            {
                log.info("Callback: " + CALLBACK_SERVICE_EXTERNAL_PROVISIONING_CHANGE);
                result = processExternalProvisioning(callbackData);
            }
        }
        else
        {
            log.error("Callback with fault" + faultDescription );
        }

        return result;

    }

    /**
     * External provisioning data change.
     * @param callbackData Verizon UWS data deserialised from SOAP callback
     * @return true if the callback has been handled successfully.
     * @throws MnoCallbackException
     */
    private boolean processExternalProvisioning(CallbackData callbackData) throws MnoCallbackException
    {
        ExternalProvisioningChange extProvChange = callbackData.getExternalProvisioningChange().getValue();
        Map<String, String> di = toDeviceIdentifierMap(extProvChange.getDeviceInfo().getValue().getDeviceIdentifiers());

        ensureDeviceIdentifiersContainsMsisdn(di);

        return executeDeviceStateChangeHandler(
                DeviceIdentifierKind.MSISDN,
                di.get("msisdn"),
                extProvChange.getDeviceInfo().getValue().getCarrierInformation().get(0).getState());

    }

    private boolean processDeviceStateChange(CallbackRequest request, CallbackData callbackData) throws MnoCallbackException
    {
        String newProvisionState;
        String deviceMsisdn;
        Map<String, String> deviceIdentifierMap = toDeviceIdentifierMap(request.getDevice());

        ensureDeviceIdentifiersContainsMsisdn(deviceIdentifierMap);

        ChangeDeviceStateResponse changeDeviceState = callbackData.getCarrierService().getValue().getChangeDeviceState().getValue();
        if (isElementDefined(changeDeviceState.getActivate()))
        {
            //activated
            Map<String, String> di = toDeviceIdentifierMap(changeDeviceState.getActivate().getValue().getDeviceIdentifierCollection());
            ensureDeviceIdentifiersContainsMsisdn(di);

            //todo: we are not sure what deviceIdentifier kind will be included in the activate callback!
            return executeDeviceStateChangeHandler(DeviceIdentifierKind.MSISDN, di.get("msisdn"),"activated");

        }
        else if (isElementDefined(changeDeviceState.getDeactivate()))
        {
            //deactivated
            //FIXME miuRepo.changeCellularDeviceState(deviceIdentifierMap.get("msisdn"), "deactivated");
            return executeDeviceStateChangeHandler(DeviceIdentifierKind.MSISDN, deviceIdentifierMap.get("msisdn"), "deactivated");
        }
        else if (isElementDefined(changeDeviceState.getRestore()))
        {
            //device restored
            //FIXME miuRepo.changeCellularDeviceState(deviceIdentifierMap.get("msisdn"), "deactivated");
            return executeDeviceStateChangeHandler(DeviceIdentifierKind.MSISDN, deviceIdentifierMap.get("msisdn"), "restore");
        }
        else if (isElementDefined(changeDeviceState.getSuspend()))
        {
            //device suspend
            //FIXME miuRepo.changeCellularDeviceState(deviceIdentifierMap.get("msisdn"), "suspended");
            return executeDeviceStateChangeHandler(DeviceIdentifierKind.MSISDN, deviceIdentifierMap.get("msisdn"), "suspended");
        }

        return false;
    }

    /**
     * Process UWS device usage callback.
     * @param callbackData UWS callback data
     * @return true if successful.
     */
    private boolean processDeviceUsage(CallbackData callbackData)
    {
        boolean isAllSuccess = true;

        //go through all device list and update database
        for(DeviceUsage e : callbackData.getUsage())
        {
            Map<String, String> di = toDeviceIdentifierMap(e.getDevice());

            if (di.containsKey("iccid"))
            {
                //todo, check startDate and endDate are not more than 1 day

                //assuming the time is UTC
                Date endDate = Date.from(
                        LocalDateTime.parse(
                                e.getEndDate().getValue(),
                                DateTimeFormatter.ISO_LOCAL_DATE_TIME).atOffset(ZoneOffset.UTC).toInstant()); //fixme, using endDate, check against startDate

                log.info("Inserting data usage for device iccid: {}, date: {} - {}, bytesUsed: {}",
                        di.get("iccid"),
                        e.getStartDate().getValue(), e.getEndDate().getValue(),
                        e.getDataUsage().getValue());

                isAllSuccess &= executeDeviceUsageNotification(di.get("iccid"), Integer.parseInt(e.getDataUsage().getValue()));
            }
            else
            {
                log.error("Cannot find iccid for updating data usage");
                isAllSuccess = false;
            }
        }

        return isAllSuccess;
    }

    /**
     * Check whether the deviceIdentifier send through the callback request contains msisdn.
     * @param deviceIdentifierMap
     * @throws MnoCallbackException
     */
    private void ensureDeviceIdentifiersContainsMsisdn(Map<String, String> deviceIdentifierMap) throws MnoCallbackException
    {
        if (!deviceIdentifierMap.containsKey("msisdn"))
        {
            throw new MnoCallbackException("Cannot find device identifier: msisdn. Available device identifier are " +
                    deviceIdentifierMap.toString());
        }
    }

    /**
     * Turn deviceIdentifier list into a map. The key is converted to lowercase since Verizon callback does not seems
     * to be consistent with the casing.
     */
    private static Map<String, String> toDeviceIdentifierMap(List<DeviceIdentifier> deviceIdentifiers)
    {
        return deviceIdentifiers.stream()
                .collect(Collectors.toMap(k -> k.getKind().toLowerCase(), DeviceIdentifier::getIdentifier));
    }

    /**
     * Turn device identifier into a map.The keys are converted to lowercase since Verizon callback does not seems
     * to be consistent with the casing.
     */
    private static Map<String, String> toDeviceIdentifierMap(DeviceIdentifier deviceIdentifier)
    {
        Map<String, String> map = new HashMap<>(1);
        map.put(deviceIdentifier.getKind().toLowerCase(), deviceIdentifier.getIdentifier());
        return map;
    }

    /**
     * Traverse the callbackData tree to find out the callback type.
     * @param callbackData CallbackData contained within a Verison UWS CallbackRequest.
     * @return String describing the callback request type.
     */
    private static String getCallbackService(CallbackData callbackData)
    {
        String service = "?";
        String detail = "?";

        //carrier service callback
        if (isElementDefined(callbackData.getCarrierService()))
        {
            service = getElementName(callbackData.getCarrierService());
            CarrierServiceCallbackRequest request = callbackData.getCarrierService().getValue();
            if (isElementDefined(request.getChangeDeviceCustomFields()))
            {
                detail = getElementName(request.getChangeDeviceCustomFields());
            }
            else if (isElementDefined(request.getChangeDeviceFeatureCodes()))
            {
                detail = getElementName(request.getChangeDeviceFeatureCodes());
            }
            else if (isElementDefined(request.getChangeDeviceCustomFields()))
            {
                detail = getElementName(request.getChangeDeviceCustomFields());
            }
            else if (isElementDefined(request.getChangeDeviceIdentifier()))
            {
                detail = getElementName(request.getChangeDeviceIdentifier());
            }
            else if (isElementDefined(request.getChangeDeviceServicePlan()))
            {
                detail = getElementName(request.getChangeDeviceServicePlan());
            }
            else if (isElementDefined(request.getChangeDeviceState()))
            {
                detail = getElementName(request.getChangeDeviceState());
            }
        }

        //ExternalProvisioningChanges - eg device activated, suspend
        if (isElementDefined(callbackData.getExternalProvisioningChange()))
        {
            service = getElementName(callbackData.getExternalProvisioningChange());
            ExternalProvisioningChange request = callbackData.getExternalProvisioningChange().getValue();
            detail = request.getChange().getValue();
        }

        //DeviceUsage
        if (callbackData.getUsage() != null && !callbackData.getUsage().isEmpty())
        {
            return "Usage";
        }

        //DevicePRLInformation
        if (isElementDefined(callbackData.getPRLInformation()))
        {
            service = getElementName(callbackData.getPRLInformation());
            detail = callbackData.getPRLInformation().getValue().getPRLVersion().getValue();
        }

        //SMSDeliveryConfirmation
        if (isElementDefined(callbackData.getSMSDelivery()))
        {
            service = getElementName(callbackData.getSMSDelivery());
            detail = callbackData.getSMSDelivery().getValue().getConfirmation().getValue();
        }

        //PromoChange
        if (isElementDefined(callbackData.getPromoChange()))
        {
            service = getElementName(callbackData.getPromoChange());
            detail = callbackData.getPromoChange().getValue().getChange().getValue() +
                    callbackData.getPromoChange().getValue().getChangeDate().getValue();
        }

        return service + " - " + detail;
    }

    /**
     * Check whether a JAXBElement is defined.
     * @param element the element to check
     * @return true if element is defined and contains value.
     */
    private static boolean isElementDefined(JAXBElement element)
    {
        return (element != null) &&
                !element.isNil() &&
                (element.getValue() != null);
    }

    private static String getElementName(JAXBElement element)
    {
        return element.getValue().getClass().getSimpleName();
    }

    /**
     * Get fault description if any
     */
    private static String getCallbackFault(CallbackData callbackData)
    {
        String faultMessage = "";

        if (isElementDefined(callbackData.getFault()))
        {
            SoapFault fault = callbackData.getFault().getValue();
            faultMessage = String.format("Fault: faultcode:%s, faultstring:%s, faultfactor:%s, detail:%s ",
                    fault.getFaultcode(), fault.getFaultstring(), fault.getFaultactor(), fault.getDetail());
        }

        return faultMessage;
    }
}
