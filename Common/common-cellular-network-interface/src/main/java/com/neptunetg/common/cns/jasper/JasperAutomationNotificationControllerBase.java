/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.cns.jasper;

import com.jasperwireless.api.ws.schema.CtdUsageInfoType;
import com.jasperwireless.api.ws.schema.DailySessionConnectionInfoType;
import com.jasperwireless.api.ws.schema.MonthlyDataUsageInfoType;
import com.jasperwireless.api.ws.schema.NoConnectionInfoType;
import com.jasperwireless.api.ws.schema.Past24HDataUsageInfoType;
import com.jasperwireless.api.ws.schema.SessionInfoType;
import com.jasperwireless.api.ws.schema.SimImeiChangeInfoType;
import com.jasperwireless.api.ws.schema.SimRatePlanChangeInfoType;
import com.jasperwireless.api.ws.schema.SimStateChangeInfoType;
import com.jasperwireless.api.ws.schema.TooManyCtdConnectionInfoType;
import com.neptunetg.common.cns.CallbackEventHandler;
import com.neptunetg.common.cns.DeviceIdentifierKind;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.StringReader;

/**
 * To receive Jasper Control Center Push event. This is to be implemented by a concrete controller in mdce-integration.
 */
@RestController
@RequestMapping(value = "cns/jasper")
public abstract class JasperAutomationNotificationControllerBase extends CallbackEventHandler
{
    private static final Logger logger = LoggerFactory.getLogger(JasperAutomationNotificationControllerBase.class);
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
    private static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";
    protected static final String API_SECRET_KEY = "neptune";

    @RequestMapping(value = "sim-state-change", method = RequestMethod.POST)
    public void notifySimStateChange(PushForm pushForm) throws JasperPushNotificationException
    {
        boolean isSuccess = false;

        logger.debug("Received notification {}", pushForm);

        //verify signature
        if (!isValidSignature(pushForm.getTimestamp(), pushForm.getSignature(), HMAC_SHA1_ALGORITHM))
        {
            throw new JasperPushNotificationException("Invalid signature");
        }

        if (JasperEventType.SimStateChange.isEqual(pushForm.getEventType()))
        {
            try
            {

                SimStateChangeInfoType simStateChangeInfoType = SimStateChangeInfoType.Factory.parse(getXmlReader(pushForm.getData()));
                logger.debug("Sim state change event iccid: {}, from {} to {} at {}",
                        simStateChangeInfoType.getIccid(), simStateChangeInfoType.getPreviousState(),
                        simStateChangeInfoType.getCurrentState(), simStateChangeInfoType.getDateChanged());

                isSuccess = executeDeviceStateChangeHandler(DeviceIdentifierKind.ICCID,
                        simStateChangeInfoType.getIccid(),
                        simStateChangeInfoType.getCurrentState());
            }
            catch (Exception e)
            {
                logger.error("Error executing function", e);
            }
        }

        if (!isSuccess)
        {
            throw new JasperPushNotificationException("Error in executing callback handler in response to the Push API");
        }
    }

    @RequestMapping(value = "usage-monitoring", method = RequestMethod.POST)
    public void notifyUsageDataExceed(PushForm pushForm) throws Exception
    {
        logger.debug("Received notification {}", pushForm);

        //verify signature
        if (!isValidSignature(pushForm.getTimestamp(), pushForm.getSignature(), HMAC_SHA1_ALGORITHM))
        {
            throw new JasperPushNotificationException("Invalid signature");
        }

        //TODO test session start/stop only, we need to find out the xml type for data usage limit
        JasperEventType eventType = JasperEventType.fromEventTypeString(pushForm.getEventType());

        logger.debug("Notification event type {}", eventType);

        //valid ok, check event type and process data according to event type
        processEventNotification(pushForm, eventType);
    }

    @RequestMapping(value = "session", method = RequestMethod.POST)
    public void notifySession(PushForm pushForm) throws JasperPushNotificationException
    {
        logger.debug("Received session notification {}", pushForm);

        //verify signature
        if (!isValidSignature(pushForm.getTimestamp(), pushForm.getSignature(), HMAC_SHA1_ALGORITHM))
        {
            throw new JasperPushNotificationException("Invalid signature");
        }

        JasperEventType eventType = JasperEventType.fromEventTypeString(pushForm.getEventType());
        logger.debug("Notification event type {}", eventType);

    }


    /**
     * Parse the xml string in the data field in the notification form and deserialise into the expected class. Call the handler
     * to process the notification.
     * @param pushForm form POSTED by Jasper Automation API
     * @param eventType type of event
     * @return true if the event handler is executed successfully
     * @throws Exception
     */
    private boolean processEventNotification(PushForm pushForm, JasperEventType eventType) throws Exception
    {
        boolean success = false;

        switch (eventType)
        {
            case RecentDataUsage:   //deliberate fallthrough, FIXME: This might not contain the correct eventType!
            case Past24HDataUsageExceed:
                Past24HDataUsageInfoType past24hDataUsageInfoType = Past24HDataUsageInfoType.Factory.parse(getXmlReader(pushForm.getData()));

                success = executeDeviceDailyUsageLimitHandler(past24hDataUsageInfoType.getIccid(), Integer.valueOf(past24hDataUsageInfoType.getDataUsage()));
                break;

            case MonthlyPooledDataUsage:
                MonthlyDataUsageInfoType monthlyDataUsageInfoType = MonthlyDataUsageInfoType.Factory.parse(getXmlReader(pushForm.getData()));
                success = executePooledDataUsageLimitHandler(monthlyDataUsageInfoType.getTotalActualZoneUsage().intValue());
                break;

            case NoConnection:
                NoConnectionInfoType noConnectionInfoType = NoConnectionInfoType.Factory.parse(getXmlReader(pushForm.getData()));

                success = executeDeviceNoConnectionNotification(noConnectionInfoType.getIccid());
                break;

            case TooManyConnections:
                TooManyCtdConnectionInfoType tooManyCtdConnectionInfoType = TooManyCtdConnectionInfoType.Factory.parse(getXmlReader(pushForm.getData()));
                break;

            case NumberOfSessionConnections:
                DailySessionConnectionInfoType dailySessionConnectionInfoType = DailySessionConnectionInfoType.Factory.parse(getXmlReader(pushForm.getData()));
                break;

            case CycleToDateUsage:
                CtdUsageInfoType ctdUsageInfoType = CtdUsageInfoType.Factory.parse(getXmlReader(pushForm.getData()));
                success = executeDeviceUsageNotification(ctdUsageInfoType.getIccid(), Integer.valueOf(ctdUsageInfoType.getDataUsage()));
                break;

            case SimStateChange:
                SimStateChangeInfoType simStateChangeInfoType = SimStateChangeInfoType.Factory.parse(getXmlReader(pushForm.getData()));
                success = executeDeviceStateChangeHandler(
                        DeviceIdentifierKind.ICCID,
                        simStateChangeInfoType.getIccid(),
                        simStateChangeInfoType.getCurrentState());
                break;

            case SimRatePlanChange:
                //todo do nothing for the moment
                SimRatePlanChangeInfoType simRatePlanChangeInfoType = SimRatePlanChangeInfoType.Factory.parse(getXmlReader(pushForm.getData()));
                break;

            case IMEIChange:
                //todo do nothing for the moment
                SimImeiChangeInfoType simImeiChangeInfoType = SimImeiChangeInfoType.Factory.parse(getXmlReader(pushForm.getData()));
                break;

            case SessionStart:
                processSessionEvent(pushForm.getData());
                break;

            case SessionStop:
                processSessionEvent(pushForm.getData());
                break;

            default:
                logger.warn("Callback for this event type: {} is ignore  in this method", eventType);
                break;
        }

        return success;
    }

    /**
     * Test function to output session event
     * @param data xml data
     * @throws Exception
     */
    private void processSessionEvent(final String data) throws Exception
    {
        SessionInfoType sessionInfo = SessionInfoType.Factory.parse(getXmlReader(data));
        logger.debug("Session event {} {} {}", sessionInfo.getIccid(), sessionInfo.getDateSessionStarted(), sessionInfo.getDateSessionEnded());
    }

    /**
     * Given an xml string, create a stream reader for deserialising into an Axis object
     * @param xmlData xml string
     * @return streamreader
     * @throws XMLStreamException
     */
    private static XMLStreamReader getXmlReader(final String xmlData) throws XMLStreamException
    {
        XMLInputFactory factory = XMLInputFactory.newInstance(); // Or newFactory()
        return factory.createXMLStreamReader(new StringReader(xmlData));
    }

    /**
     * Validate signature
     *
     * @param timestamp timestamp string included in the Jasper automation API request
     * @param signature signature string included in the Jasper automation API request
     * @param algorithm HmacSHA1 or HmacSHA256
     * @return true if signature is validated
     */
    private static boolean isValidSignature(final String timestamp, final String signature, final String algorithm)
    {
        if (API_SECRET_KEY.length() > 0)
        {
            try
            {
                SecretKeySpec keySpec = new SecretKeySpec(API_SECRET_KEY.getBytes(), algorithm);
                Mac mac = Mac.getInstance(algorithm);
                mac.init(keySpec);
                String expectedSignature = new String(Base64.encodeBase64(mac.doFinal(timestamp.getBytes())));
                if (expectedSignature.equals(signature))
                {
                    return true;
                }
                else
                {
                    logger.error("Invalid signature: " + signature + " does not match expected signature: " + expectedSignature);
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.error("Error verifying signature: " + e);
                return false;
            }
        }
        else
        {
            return true;    //no shared secret
        }
    }


}
