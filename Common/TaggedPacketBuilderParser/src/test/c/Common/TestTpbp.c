/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup TpbpUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test for the @ref Tagged Packet Builder Parser
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 */
 
 

#include <stdint.h>
#include <stdio.h>
#include "CommonTypes.h"
#include "unity.h"
#include "Tpbp.h"
#include "TpbpPackerHelpers.h"
#include "TpbpParserHelpers.h"
#include "TestTpbp.h"
static uint8_t s_TestNo = 0u;


// General purpose packer and its buffer; 

uint8_t         packBuffer [BUFFER_SIZE + 50];
uint8_t         parseBuffer[BUFFER_SIZE + 50];
S_TPBP_PACKER   packer;
S_TPBP_PARSER   parser; 

/**
 * Reset the unit and print the test number before each test.
 */
void setUp(void)
{
    Tpbp_PackerInit( &packer, packBuffer, BUFFER_SIZE);   
    Tpbp_ParserInit( &parser, packBuffer, BUFFER_SIZE);

    ++s_TestNo;
    printf("Test %3d.\n",s_TestNo);
}


/**
 * Reset the state after each test.
 */
void tearDown(void)
{
}


void Tpbp_TestStructSizes(void)
{    
    // We need to ensure that there is no inadvertent change of the fixed size packets
    TEST_ASSERT_EQUAL_UINT32(3,    S_TPBP_TAG_ARRAY_PACKED_SIZE);
  	TEST_ASSERT_EQUAL_UINT32(2,    S_TPBP_TAG_INTEGER_PACKED_SIZE);
    TEST_ASSERT_EQUAL_UINT32(1,    S_TPBP_TAG_NO_TYPE_PACKED_SIZE);
    TEST_ASSERT_EQUAL_UINT32(19,   S_CMIU_PACKET_HEADER_PACKED_SIZE);
    TEST_ASSERT_EQUAL_UINT32(73,   S_CMIU_CONFIGURATION_PACKED_SIZE);
}


// Little-endian systems (PC or  ARM Cortex M3), 
// store the least significant byte in the smallest address
void Tpbp_TestEndiannessIsLittle(void)
{
    bool                isOk = 0;
    uint32_t            testWord = 0x1234ABCD;
    uint8_t*            pTestWord = (uint8_t*)(&testWord);
  
    // 32 bit word cast to 4 bytes
    isOk = Tpbp_PackerAddBytes(&packer, pTestWord, 4);
    TEST_ASSERT_TRUE(isOk);
    
    TEST_ASSERT_EQUAL_UINT8(0xCD, packBuffer[0]);    
    TEST_ASSERT_EQUAL_UINT8(0xAB, packBuffer[1]);
    TEST_ASSERT_EQUAL_UINT8(0x34, packBuffer[2]);
    TEST_ASSERT_EQUAL_UINT8(0x12, packBuffer[3]);
}


void Tpbp_TestTagTypeEnum(void)
{
    TEST_ASSERT_EQUAL(0,  E_TAG_TYPE_NONE);
    TEST_ASSERT_EQUAL(1,  E_TAG_TYPE_BYTE);
    TEST_ASSERT_EQUAL(2,  E_TAG_TYPE_U16);
    TEST_ASSERT_EQUAL(3,  E_TAG_TYPE_U32);
    TEST_ASSERT_EQUAL(4,  E_TAG_TYPE_U64);
    TEST_ASSERT_EQUAL(5,  E_TAG_TYPE_BYTE_RECORD);
    TEST_ASSERT_EQUAL(6,  E_TAG_TYPE_U16_ARRAY);
    TEST_ASSERT_EQUAL(7,  E_TAG_TYPE_U32_ARRAY);
    TEST_ASSERT_EQUAL(8,  E_TAG_TYPE_CHAR_ARRAY);
    TEST_ASSERT_EQUAL(9,  E_TAG_TYPE_EXTENDED_BYTE_RECORD);
    TEST_ASSERT_EQUAL(10, E_TAG_TYPE_EXTENDED_U16_ARRAY);
    TEST_ASSERT_EQUAL(11, E_TAG_TYPE_EXTENDED_U32_ARRAY);
    TEST_ASSERT_EQUAL(12, E_TAG_TYPE_EXTENDED_CHAR_ARRAY);
    TEST_ASSERT_EQUAL(13, E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY);
    TEST_ASSERT_EQUAL(14, E_TAG_TYPE_IMAGE_VERSION_INFO);
    TEST_ASSERT_EQUAL(15, E_TAG_TYPE_U8_ARRAY);
    TEST_ASSERT_EQUAL(16, E_TAG_TYPE_EXTENDED_U8_ARRAY);

    // Must be a byte value
    TEST_ASSERT_TRUE(E_TAG_TYPE_COUNT <= 256);
}


void Tpbp_TestUrlLength(void)
{
    TEST_ASSERT_EQUAL(256, TPBP_URL_BUFFER_LENGTH);
}


void Tpbp_TestTagNumberEnum(void)
{
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CMIU_PACKET_HEADER             == 1);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CMIU_CONFIGURATION             == 2);

     /** A null tag, used for padding */
    TEST_ASSERT_TRUE(E_TAG_NUMBER_NULL                           == 0);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CMIU_PACKET_HEADER             == 1);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CMIU_CONFIGURATION             == 2);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_NETWORK_OPERATORS              == 3);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_NETWORK_PERFORMANCE            == 4);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_REGISTRATION_STATUS            == 5);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_HOST_ADDRESS                   == 6);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_FALLBACK_HOST_ADDRESS          == 7);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_ASSIGNED_CMIU_ADDRESS          == 8);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_FTP_DNS_ADDRESS                == 9);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_FTP_PORT_NUMBER                == 10);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_FTP_USERNAME                   == 11);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_FTP_PASSWORD                   == 12);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_MODEM_HARDWARE_REVISION        == 15);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_MODEM_MODEL_IDENTIFICATION_CODE== 16);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_MODEM_MANUFACTURER_ID_CODE     == 17);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_MODEM_SOFTWARE_REVISION        == 18);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_IMEI                           == 19);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_SIM_IMSI                       == 20);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_SIM_CARD_ID                    == 21);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_PINPUKPUK2_REQUEST_STATUS      == 22);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CMIU_HW_DIAGNOSTICS            == 23);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_ID     == 24);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_DATE   == 25);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CMIU_FLAGS                     == 26);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_DEVICE_CONFIGURATION           == 27);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_R900_INTERVAL_DATA             == 28);
    /** Indicates an event */
    TEST_ASSERT_TRUE(E_TAG_NUMBER_EVENT                          == 29);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_ERROR_LOG                      == 30);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CONNECTION_TIMING_LOG          == 31);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_COMMAND                        == 32);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CMIU_BASIC_CONFIGURATION       == 33); 
    TEST_ASSERT_TRUE(E_TAG_NUMBER_INTERVAL_RECORDING             == 34); 
    TEST_ASSERT_TRUE(E_TAG_NUMBER_UART_PACKET_HEADER             == 35);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CURRENT_TIME                   == 36);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_MSISDN_REQUEST                 == 37);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_HARDWARE_REVISION              == 38);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_FIRMWARE_REVISION              == 39);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_BOOTLOADER_REVISION            == 40);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CONFIG_REVISION                == 41);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_ARB_REVISION                   == 42);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_BLE_CONFIG_REVISION            == 43);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CMIU_INFORMATION               == 44);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_IMAGE                          == 46);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_IMAGE_METADATA                 == 47);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_RECORDING_REPORTING_INTERVAL   == 48);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_ENCRYPTION_CONFIG_REVISION     == 49);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_TIMESTAMP                      == 50);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_ERROR_CODE                     == 51);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_MODEM_FOTA_VERSION             == 71);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_EXTENSION_256                  == 250);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_SECURE_DATA                    == 254);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_EOF                            == 255);

    // Must be a byte value
    TEST_ASSERT_TRUE(E_TAG_NUMBER_COUNT <= 256);
}


void Tpbp_TestPackerInit(void)
{
    uint8_t            zeroes[BUFFER_SIZE] = {0};
    const uint8_t      constDummyVal = 127;
    int i;
    
    // Prefill data buffer
    for (i = 0; i < sizeof(packBuffer); i++)
    {
        packBuffer[i] = constDummyVal;
    }

    // Init to garbage values
    packer.bufferSize = 3;
    packer.pOutputBuffer = (uint8_t*)1234;
    packer.writePosition = 1;
    
    Tpbp_PackerInit( &packer, packBuffer, BUFFER_SIZE); 

    // Ensure properly initialised
    TEST_ASSERT_EQUAL_PTR(packBuffer,       packer.pOutputBuffer);
    TEST_ASSERT_EQUAL_UINT32(BUFFER_SIZE,   packer.bufferSize);
    TEST_ASSERT_EQUAL_UINT32(0,             packer.writePosition);

    // Ensure buffer is cleared 
    TEST_ASSERT_EQUAL_INT8_ARRAY(zeroes,    packer.pOutputBuffer, BUFFER_SIZE);

    // ... but does not run over when clearing
    TEST_ASSERT_EQUAL_UINT8(constDummyVal, packBuffer[BUFFER_SIZE]);
}


//  Check that packer is zeroed
void Tpbp_TestPackerReset(void)
{
    S_TPBP_PACKER       packer; 
    uint32_t            i;

    Tpbp_PackerInit( &packer, packBuffer, BUFFER_SIZE);   

    for(i = 0; i < BUFFER_SIZE; i++)
    {
        packBuffer[i] = 0xFA;
    }

    Tpbp_PackerReset(&packer);   

    TEST_ASSERT_EQUAL_PTR(packBuffer,       packer.pOutputBuffer);
    TEST_ASSERT_EQUAL_UINT32(BUFFER_SIZE,   packer.bufferSize);
    TEST_ASSERT_EQUAL_UINT32(0,             packer.writePosition);

    for(i = 0; i < BUFFER_SIZE; i++)
    {
        TEST_ASSERT_EQUAL_UINT8(0,              packBuffer[i]);
    }
}



// Test for the count of bytes contained in buffer
void Tpbp_TestPackerGetCount(void) 
{
    uint32_t count = 0;
    uint32_t n = 0; 

    // Should be empty (cleared in Setup()
    count = Tpbp_PackerGetCount(&packer);
    TEST_ASSERT_EQUAL_UINT32(0, count);

    // Add 4 bytes
    Tpbp_Pack(&packer, n, 4);
  
    // Noww should be 4
    count = Tpbp_PackerGetCount(&packer);
    TEST_ASSERT_EQUAL_UINT32(4, count);
}


// Test for the amount of available bytes in buffer
void Tpbp_TestPackerGetAvailable(void)
{
    uint32_t            available = 0;
    uint32_t            n = 0;  
    Tpbp_PackerInit( &packer, packBuffer, BUFFER_SIZE);

    available = Tpbp_PackerGetAvailable(&packer);
    TEST_ASSERT_EQUAL_UINT32(BUFFER_SIZE, available);

    // Add 4 bytes
    Tpbp_Pack(&packer, n, 4);
  
    available = Tpbp_PackerGetAvailable(&packer);
    TEST_ASSERT_EQUAL_UINT32(BUFFER_SIZE - 4, available);
}


void Tpbp_TestTpbp_PackerAdd_PacketTypeId(void)
{
    // Force to 1 byte buffer
    Tpbp_PackerInit( &packer, packBuffer, 1u);

    TEST_ASSERT_TRUE(Tpbp_Packer_BeginPacket(&packer, (E_PACKET_TYPE_ID)3u));
    TEST_ASSERT_EQUAL_UINT8(3u, packBuffer[0]);
    TEST_ASSERT_EQUAL_UINT32(1u, packer.writePosition);
    TEST_ASSERT_FALSE(Tpbp_Packer_BeginPacket(&packer, (E_PACKET_TYPE_ID)5u)); //only a 1 byte buffer so no room for more
}


void Tpbp_TestAddBytes(void)
{
    uint8_t            testBytes[BUFFER_SIZE] = {0xA5};
    uint32_t           count = 0;
    bool               isOk = 0; 

    count =  Tpbp_PackerGetCount(&packer);
    TEST_ASSERT_EQUAL_UINT32(0, count);

    // Fill it half way
    isOk = Tpbp_PackerAddBytes(&packer, testBytes, BUFFER_SIZE / 2);
    count =  Tpbp_PackerGetCount(&packer);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL_UINT32(BUFFER_SIZE / 2, count);
    TEST_ASSERT_EQUAL_INT8_ARRAY(testBytes, packer.pOutputBuffer, BUFFER_SIZE / 2);

    // fill second half...
    isOk = Tpbp_PackerAddBytes(&packer, testBytes, BUFFER_SIZE / 2);
    count =  Tpbp_PackerGetCount(&packer);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL_UINT32(BUFFER_SIZE, count);    
    TEST_ASSERT_EQUAL_INT8_ARRAY(testBytes,  &(packer.pOutputBuffer[BUFFER_SIZE / 2]), BUFFER_SIZE / 2);

    // Another byte must NOT be accepted...
    packBuffer[BUFFER_SIZE] = 0x9A;                 // This must not be overwritten
    isOk = Tpbp_PackerAddBytes(&packer, testBytes, 1);
    count =  Tpbp_PackerGetCount(&packer);
    TEST_ASSERT_EQUAL_UINT32(0, isOk);              // Must indicate failure
    TEST_ASSERT_EQUAL_UINT32(BUFFER_SIZE, count);   // Must NOT change

    // ...Check we did not write beyond array bound
    TEST_ASSERT_EQUAL_UINT8(0x9A, packBuffer[BUFFER_SIZE]);
}


void Tpbp_TestTpbp_PackTagHelper(void)
{
    bool            isOk = 0;

    // Populate a tag with some values for testing
    S_TPBP_TAG          tag;
    tag.tagNumber       = (E_TAG_NUMBER)3; 
    tag.tagType         = (E_TAG_TYPE)5;
    tag.dataSize        = 99; 

    // Add a tag.
    isOk = Tpbp_PackerTagHelper(&packer, &tag);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_EQUAL_UINT8(tag.tagNumber,  packBuffer[0]);    
    TEST_ASSERT_EQUAL_UINT8(tag.tagType,    packBuffer[1]);
    TEST_ASSERT_EQUAL_UINT8(tag.dataSize,   packBuffer[2]);

    // Repeat add of tag, to check we only added 3 bytes
    tag.tagNumber       = (E_TAG_NUMBER)7; 
    tag.tagType         = (E_TAG_TYPE)8;
    tag.dataSize        = 101;

    isOk = Tpbp_PackerTagHelper(&packer, &tag);
    TEST_ASSERT_TRUE(isOk);
 
    TEST_ASSERT_EQUAL_UINT8(tag.tagNumber,  packBuffer[3]);    
    TEST_ASSERT_EQUAL_UINT8(tag.tagType,    packBuffer[4]);
    TEST_ASSERT_EQUAL_UINT8(tag.dataSize,   packBuffer[5]);

    // Add a tag with a high tag number
    tag.tagNumber       = (E_TAG_NUMBER)507u; 
	tag.tagType         = E_TAG_TYPE_BYTE;
    tag.dataSize        = 1u;

	isOk = Tpbp_PackerTagHelper(&packer, &tag);
    TEST_ASSERT_TRUE(isOk);
 
	TEST_ASSERT_EQUAL_UINT8(E_TAG_NUMBER_EXTENSION_256, packBuffer[6]);    
    TEST_ASSERT_EQUAL_UINT8(507u-256u,					packBuffer[7]);    
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_BYTE,			packBuffer[8]);
	TEST_ASSERT_EQUAL_UINT32(9u,						packer.writePosition);

    // Add a secure block array tag
	tag.tagNumber       = E_TAG_NUMBER_SECURE_DATA; 
	tag.tagType         = E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY;
    tag.dataSize        = 32u; //two blocks

	isOk = Tpbp_PackerTagHelper(&packer, &tag);
    TEST_ASSERT_TRUE(isOk);
 
	TEST_ASSERT_EQUAL_UINT8(E_TAG_NUMBER_SECURE_DATA,	packBuffer[9]);    
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY,	packBuffer[10]);    
    TEST_ASSERT_EQUAL_UINT8(2u,							packBuffer[11]);
	TEST_ASSERT_EQUAL_UINT8(0u,							packBuffer[12]);
	TEST_ASSERT_EQUAL_UINT32(13u,						packer.writePosition);
}


void Tpbp_TestTpbp_PackerPadZeroToBlockBoundary(void)
{
	uint8_t            testBytes[34] = {0xA5};
    uint32_t           count = 0u;
    bool               isOk = 0;
	uint8_t            allZeros[32] = {0}; //compiler infers 0 for the 31 unspecified elements

    Tpbp_PackerInit( &packer, packBuffer, 34);

    count =  Tpbp_PackerGetCount(&packer);
    TEST_ASSERT_EQUAL_UINT32(0, count);

    // Pad to fill block = nop because on boundary 0
    isOk = Tpbp_PackerPadZeroToBlockBoundary(&packer, 16);
	TEST_ASSERT_TRUE(isOk);

	count =  Tpbp_PackerGetCount(&packer);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL_UINT32(0u, count);

	//add a byte
	isOk = Tpbp_PackerAddBytes(&packer, testBytes, 1);
	
	count =  Tpbp_PackerGetCount(&packer);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL_UINT32(1u, count);
	
    // Pad to fill block = 15 zero bytes inserted
    isOk = Tpbp_PackerPadZeroToBlockBoundary(&packer, 16);
	TEST_ASSERT_TRUE(isOk);

	count =  Tpbp_PackerGetCount(&packer);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL_UINT32(16u, count);

	TEST_ASSERT_EQUAL_INT8_ARRAY(allZeros, packer.pOutputBuffer + 1, 15);
	
	//add 15 bytes
	isOk = Tpbp_PackerAddBytes(&packer, testBytes, 15);
	
	count =  Tpbp_PackerGetCount(&packer);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL_UINT32(31u, count);

    // Pad to fill block = 1 zero byte inserted
    isOk = Tpbp_PackerPadZeroToBlockBoundary(&packer, 16);
	TEST_ASSERT_TRUE(isOk);

	count =  Tpbp_PackerGetCount(&packer);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL_UINT32(32u, count);

	TEST_ASSERT_EQUAL_INT8_ARRAY(allZeros, packer.pOutputBuffer + 31, 1);

	//add a byte
	isOk = Tpbp_PackerAddBytes(&packer, testBytes, 1);
	
	count =  Tpbp_PackerGetCount(&packer);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL_UINT32(33u, count);

    // Pad to fill block - not enough capacity
    isOk = Tpbp_PackerPadZeroToBlockBoundary(&packer, 16);
	TEST_ASSERT_FALSE(isOk);
}


// Test pack integer
void Tpbp_TestTpbp_Pack(void)
{
    Tpbp_PackerInit( &packer, packBuffer, 1u+2u+4u);   

    TEST_ASSERT_TRUE(Tpbp_Pack(&packer, 129u, 1u));
    TEST_ASSERT_EQUAL_UINT8(129u, packBuffer[0]);

    TEST_ASSERT_TRUE(Tpbp_Pack(&packer, 65535u, 2u));
    TEST_ASSERT_EQUAL_UINT16(65535u, *((uint16_t*)(&(packBuffer[1]))));

    TEST_ASSERT_TRUE(Tpbp_Pack(&packer, 65536u, 4u));
    TEST_ASSERT_EQUAL_UINT32(65536u, *((uint32_t*)(&(packBuffer[3]))));

    TEST_ASSERT_FALSE(Tpbp_Pack(&packer, 129u, 1u)); //buffer full now
    TEST_ASSERT_FALSE(Tpbp_Pack(&packer, 65535u, 2u));
}


// Test pack int64
void Tpbp_TestTpbp_Pack64(void)
{
    const uint32_t* packBuffer32 = (uint32_t*)packBuffer;

    Tpbp_PackerInit( &packer, packBuffer, 10u);   

    TEST_ASSERT_TRUE(Tpbp_Pack64(&packer, 0x123456789ABCDEFuLL));
    TEST_ASSERT_EQUAL_UINT32(0x89ABCDEFu, packBuffer32[0]);
    TEST_ASSERT_EQUAL_UINT32(0x01234567u, packBuffer32[1]);

    TEST_ASSERT_FALSE(Tpbp_Pack64(&packer, 129uLL)); //buffer full now
}


// Test pack of this specific type
void Tpbp_TestTpbp_PackerAdd_CmiuPacketHeader(void)
{
    bool            isOk = 0;
    uint32_t        i = 0;

    S_CMIU_PACKET_HEADER ts;
    ts.timeAndDate          = 0x0123456789ABCDEF;
    ts.cmiuId               = 0xABCD1122; 
    ts.cellularRssiAndBer   = 0xCEBE;
    ts.sequenceNumber       = 0x99;
    ts.keyInfo              = 0x88;
    ts.encryptionMethod     = 0xEE;
    ts.tokenAesCrc          = 0xAC;
    ts.networkFlags         = 0xFF;


    Tpbp_PackerInit( &packer, packBuffer, BUFFER_SIZE);   

    // Add the data 
    isOk = Tpbp_PackerAdd_CmiuPacketHeader(&packer, &ts);
    TEST_ASSERT_TRUE(isOk);
    
    // test the tag, byte by byte.
    i = 0;
    TEST_ASSERT_EQUAL_UINT8(E_TAG_NUMBER_CMIU_PACKET_HEADER,    packBuffer[i++]); 
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_BYTE_RECORD,             packBuffer[i++]);
    TEST_ASSERT_EQUAL_UINT8(S_CMIU_PACKET_HEADER_PACKED_SIZE,   packBuffer[i++]);  

    // cmiuId
    i = CMIU_PKT_OFFSET(0);
    TEST_ASSERT_EQUAL_UINT8(0x22, packBuffer[i + 0]);       
    TEST_ASSERT_EQUAL_UINT8(0x11, packBuffer[i + 1]);
    TEST_ASSERT_EQUAL_UINT8(0xCD, packBuffer[i + 2]);    
    TEST_ASSERT_EQUAL_UINT8(0xAB, packBuffer[i + 3]);
     
    // Seq
    i = CMIU_PKT_OFFSET( 4);
    TEST_ASSERT_EQUAL_UINT8(0x99, packBuffer[i + 0]);
    
    // keyInfo
    i = CMIU_PKT_OFFSET(4 + 1);
    TEST_ASSERT_EQUAL_UINT8(0x88, packBuffer[i + 0]);
    
    // enc method
    i = CMIU_PKT_OFFSET(4 + 1 + 1);
    TEST_ASSERT_EQUAL_UINT8(0xEE, packBuffer[i + 0]);

    // tokenAesCrc;
    i = CMIU_PKT_OFFSET(4 + 1 + 1 + 1);
    TEST_ASSERT_EQUAL_UINT8(0xAC, packBuffer[i + 0]);

    // networkFlags
    i = CMIU_PKT_OFFSET(4 + 1 + 1 + 1 + 1);
    TEST_ASSERT_EQUAL_UINT8(0xFF, packBuffer[i + 0]);
         
    // CellularRssiAndBer
    i = CMIU_PKT_OFFSET(4 + 1 + 1 + 1 + 1 + 1);
    TEST_ASSERT_EQUAL_UINT16(0xBE, packBuffer[i + 0]);    
    TEST_ASSERT_EQUAL_UINT16(0xCE, packBuffer[i + 1]);    

     // Time/date
    i = CMIU_PKT_OFFSET(4 + 1 + 1 + 1 + 1 + 1 + 2);
    TEST_ASSERT_TRUE(0xEF == packBuffer[i + 0]);       
    TEST_ASSERT_EQUAL_UINT8(0xEF, packBuffer[i + 0]);       
    TEST_ASSERT_EQUAL_UINT8(0xCD, packBuffer[i + 1]);
    TEST_ASSERT_EQUAL_UINT8(0xAB, packBuffer[i + 2]);    
    TEST_ASSERT_EQUAL_UINT8(0x89, packBuffer[i + 3]);
    TEST_ASSERT_EQUAL_UINT8(0x67, packBuffer[i + 4]);       
    TEST_ASSERT_EQUAL_UINT8(0x45, packBuffer[i + 5]);
    TEST_ASSERT_EQUAL_UINT8(0x23, packBuffer[i + 6]);    
    TEST_ASSERT_EQUAL_UINT8(0x01, packBuffer[i + 7]);
}

// Test pack of this specific type
void Tpbp_TestTpbp_PackerAdd_CmiuConfiguration(void)
{
    bool            isOk = 0;
    uint32_t        i = 0;
    S_CMIU_CONFIGURATION ts;
 
    ts.cmiuType                             = 0x02;	
    ts.hwRevision                           = 0x1234;
    
    ts.fwRelease                            = 0x1235u;
    ts.fwRevDate                            = 0x12345678u;
    ts.fwBuildNum                           = 0xfedcba98u;

    ts.blRelease                            = 0x2225u;
    ts.blRevDate                            = 0x22242628u;
    ts.blBuildNum                           = 0x2e2c2a28u;

    ts.configRelease                        = 0xF2F5u;
    ts.configRevDate                        = 0xF2F4F6F8u;
    ts.configBuildNum                       = 0xFeFcFaF8u;

    ts.manufactureDate                      = 0x0123456789ABCDEF;
    ts.initialCallInTime                    = 0x0123;	
    ts.callInInterval                       = 0xFEDC;
    ts.callInWindow                         = 0x9876;
    ts.reportingwindowNRetries              = 0xF4;
    ts.quietTimeStartMins                   = 0xCDEF;
    ts.quietTimeEndMins                     = 0xCBFE;  

    ts.numAttachedDevices                   = 0x05;
    ts.installationDate                     = 0x0123456789ABCD99;
    ts.dateLastMagSwipe                     = 0x0123456789ABCD55;
    ts.magSwipes                            = 0xC9;
    ts.batteryRemaining                     = 0xDEAD;
    ts.timeErrorLastNetworkTimeAccess       = 0x7E;
   

    Tpbp_PackerInit( &packer, packBuffer, BUFFER_SIZE);   

    // Add the data 
    isOk = Tpbp_PackerAdd_CmiuConfiguration(&packer, &ts);
    TEST_ASSERT_TRUE(isOk);
    
    // test the tag, byte by byte.
    i = 0;
    TEST_ASSERT_EQUAL_UINT8(E_TAG_NUMBER_CMIU_CONFIGURATION,    packBuffer[i++]); 
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_BYTE_RECORD,             packBuffer[i++]);
    TEST_ASSERT_EQUAL_UINT8(S_CMIU_CONFIGURATION_PACKED_SIZE,   packBuffer[i++]);
    
     // CMIU Type
    i = CMIU_PKT_OFFSET(0);
    TEST_ASSERT_EQUAL_UINT8(0x02, packBuffer[i + 0]);    
    
     // hw rev
    i = CMIU_PKT_OFFSET(1);
    TEST_ASSERT_EQUAL_UINT8(0x34, packBuffer[i + 0]);    
    TEST_ASSERT_EQUAL_UINT8(0x12, packBuffer[i + 1]);

     // fw rev
    i = CMIU_PKT_OFFSET(1+2);
    TEST_ASSERT_EQUAL_UINT8(0x35, packBuffer[i + 0]);    
    TEST_ASSERT_EQUAL_UINT8(0x12, packBuffer[i + 1]);

    TEST_ASSERT_EQUAL_UINT8(0x78, packBuffer[i + 2]);
    TEST_ASSERT_EQUAL_UINT8(0x56, packBuffer[i + 3]);
    TEST_ASSERT_EQUAL_UINT8(0x34, packBuffer[i + 4]);
    TEST_ASSERT_EQUAL_UINT8(0x12, packBuffer[i + 5]);

    TEST_ASSERT_EQUAL_UINT8(0x98, packBuffer[i + 6]);
    TEST_ASSERT_EQUAL_UINT8(0xba, packBuffer[i + 7]);
    TEST_ASSERT_EQUAL_UINT8(0xdc, packBuffer[i + 8]);
    TEST_ASSERT_EQUAL_UINT8(0xfe, packBuffer[i + 9]);

     // bl ver
    i += 10;
    TEST_ASSERT_EQUAL_UINT8(0x25, packBuffer[i + 0]);    
    TEST_ASSERT_EQUAL_UINT8(0x22, packBuffer[i + 1]);

    TEST_ASSERT_EQUAL_UINT8(0x28, packBuffer[i + 2]);
    TEST_ASSERT_EQUAL_UINT8(0x26, packBuffer[i + 3]);
    TEST_ASSERT_EQUAL_UINT8(0x24, packBuffer[i + 4]);
    TEST_ASSERT_EQUAL_UINT8(0x22, packBuffer[i + 5]);

    TEST_ASSERT_EQUAL_UINT8(0x28, packBuffer[i + 6]);
    TEST_ASSERT_EQUAL_UINT8(0x2a, packBuffer[i + 7]);
    TEST_ASSERT_EQUAL_UINT8(0x2c, packBuffer[i + 8]);
    TEST_ASSERT_EQUAL_UINT8(0x2e, packBuffer[i + 9]);
    
     // config ver
    i += 10;
    TEST_ASSERT_EQUAL_UINT8(0xF5, packBuffer[i + 0]);    
    TEST_ASSERT_EQUAL_UINT8(0xF2, packBuffer[i + 1]);

    TEST_ASSERT_EQUAL_UINT8(0xF8, packBuffer[i + 2]);
    TEST_ASSERT_EQUAL_UINT8(0xF6, packBuffer[i + 3]);
    TEST_ASSERT_EQUAL_UINT8(0xF4, packBuffer[i + 4]);
    TEST_ASSERT_EQUAL_UINT8(0xF2, packBuffer[i + 5]);

    TEST_ASSERT_EQUAL_UINT8(0xF8, packBuffer[i + 6]);
    TEST_ASSERT_EQUAL_UINT8(0xFa, packBuffer[i + 7]);
    TEST_ASSERT_EQUAL_UINT8(0xFc, packBuffer[i + 8]);
    TEST_ASSERT_EQUAL_UINT8(0xFe, packBuffer[i + 9]);

    
     // manufactureDate
    i += 10;
    TEST_ASSERT_EQUAL_UINT8(0xEF, packBuffer[i + 0]);       
    TEST_ASSERT_EQUAL_UINT8(0xCD, packBuffer[i + 1]);
    TEST_ASSERT_EQUAL_UINT8(0xAB, packBuffer[i + 2]);    
    TEST_ASSERT_EQUAL_UINT8(0x89, packBuffer[i + 3]);
    TEST_ASSERT_EQUAL_UINT8(0x67, packBuffer[i + 4]);       
    TEST_ASSERT_EQUAL_UINT8(0x45, packBuffer[i + 5]);
    TEST_ASSERT_EQUAL_UINT8(0x23, packBuffer[i + 6]);    
    TEST_ASSERT_EQUAL_UINT8(0x01, packBuffer[i + 7]);
    
    //initialCallInTime
    i += 8;
    TEST_ASSERT_EQUAL_UINT8(0x23, packBuffer[i + 0]);       
    TEST_ASSERT_EQUAL_UINT8(0x01, packBuffer[i + 1]);

    i += 2;
    TEST_ASSERT_EQUAL_UINT8(0xDC, packBuffer[i + 0]); 
    TEST_ASSERT_EQUAL_UINT8(0xFE, packBuffer[i + 1]); 
    TEST_ASSERT_EQUAL_UINT8(0x76, packBuffer[i + 2]); 
    TEST_ASSERT_EQUAL_UINT8(0x98, packBuffer[i + 3]); 
    i += 4;
    TEST_ASSERT_EQUAL_UINT8(0xF4, packBuffer[i + 0]); 

    
    i += 1;
    TEST_ASSERT_EQUAL_UINT8(0xEF, packBuffer[i + 0]);    
    TEST_ASSERT_EQUAL_UINT8(0xCD, packBuffer[i + 1]);
    i += 2;
    TEST_ASSERT_EQUAL_UINT8(0xFE, packBuffer[i + 0]);    
    TEST_ASSERT_EQUAL_UINT8(0xCB, packBuffer[i + 1]);
    i += 2;
    TEST_ASSERT_EQUAL_UINT8(0x05, packBuffer[i + 0]);     
    i += 1;

     // installationDate 
    TEST_ASSERT_EQUAL_UINT8(0x99, packBuffer[i + 0]);       
    TEST_ASSERT_EQUAL_UINT8(0xCD, packBuffer[i + 1]);
    TEST_ASSERT_EQUAL_UINT8(0xAB, packBuffer[i + 2]);    
    TEST_ASSERT_EQUAL_UINT8(0x89, packBuffer[i + 3]);
    TEST_ASSERT_EQUAL_UINT8(0x67, packBuffer[i + 4]);       
    TEST_ASSERT_EQUAL_UINT8(0x45, packBuffer[i + 5]);
    TEST_ASSERT_EQUAL_UINT8(0x23, packBuffer[i + 6]);    
    TEST_ASSERT_EQUAL_UINT8(0x01, packBuffer[i + 7]);
        
    // dateLastMagSwipe
    i += 8;
    TEST_ASSERT_EQUAL_UINT8(0x55, packBuffer[i + 0]);       
    TEST_ASSERT_EQUAL_UINT8(0xCD, packBuffer[i + 1]);
    TEST_ASSERT_EQUAL_UINT8(0xAB, packBuffer[i + 2]);    
    TEST_ASSERT_EQUAL_UINT8(0x89, packBuffer[i + 3]);
    TEST_ASSERT_EQUAL_UINT8(0x67, packBuffer[i + 4]);       
    TEST_ASSERT_EQUAL_UINT8(0x45, packBuffer[i + 5]);
    TEST_ASSERT_EQUAL_UINT8(0x23, packBuffer[i + 6]);    
    TEST_ASSERT_EQUAL_UINT8(0x01, packBuffer[i + 7]);

    i += 8;
    TEST_ASSERT_EQUAL_UINT8(0xC9, packBuffer[i + 0]); 
    
    i += 1;
    TEST_ASSERT_EQUAL_UINT8(0xAD, packBuffer[i + 0]);    
    TEST_ASSERT_EQUAL_UINT8(0xDE, packBuffer[i + 1]);
    i += 2;
    TEST_ASSERT_EQUAL_UINT8(0x7E, packBuffer[i + 0]);
}





void Tpbp_TestTpbp_PackerAdd_Eof(void)
{
    Tpbp_PackerInit( &packer, packBuffer, 1u);

    TEST_ASSERT_TRUE(Tpbp_Packer_EndPacket(&packer));
    TEST_ASSERT_EQUAL_UINT8(E_TAG_NUMBER_EOF, packBuffer[0]);
    TEST_ASSERT_EQUAL_UINT32(1u, packer.writePosition);
    TEST_ASSERT_FALSE(Tpbp_Packer_EndPacket(&packer)); //only a 1 byte buffer so no room for more
}


//Test add an int tag
void Tpbp_TestTpbp_PackerAdd_UInt(void)
{
    static uint8_t         expectedForByte[] = {
        1,
        E_TAG_TYPE_BYTE, 
        0xFE
        };

    static uint8_t         expectedForUInt16[] = {
        2,
        E_TAG_TYPE_U16, 
        0xDC, 0xFE
        };

    static uint8_t         expectedForUInt32[] = {
        4,
        E_TAG_TYPE_U32, 
        0x98, 0xBA, 0xDC, 0xFE
        };
    
    uint32_t bufferPos = 0;

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_UInt(&packer, (E_TAG_NUMBER)1u, 0xFEu, 1u));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expectedForByte), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expectedForByte, &(packBuffer[bufferPos]), sizeof(expectedForByte));

    bufferPos += sizeof(expectedForByte);

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_UInt(&packer, (E_TAG_NUMBER)2u, 0xFEDCu, 2u));
    TEST_ASSERT_EQUAL_UINT32(bufferPos + sizeof(expectedForUInt16), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expectedForUInt16, &(packBuffer[bufferPos]), sizeof(expectedForUInt16));
    
    bufferPos += sizeof(expectedForUInt16);

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_UInt(&packer, (E_TAG_NUMBER)4u, 0xFEDCBA98u, 4u));
    TEST_ASSERT_EQUAL_UINT32(bufferPos + sizeof(expectedForUInt32), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expectedForUInt32, &(packBuffer[bufferPos]), sizeof(expectedForUInt32));
}


// Test add an int64 tag
void Tpbp_TestTpbp_PackerAdd_UInt64(void)
{
    static uint8_t         expected[] = {
        8,
        E_TAG_TYPE_U64, 
        0x10, 0x32, 0x54, 0x76, 0x98, 0xBA, 0xDC, 0xFE
        };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_UInt64(&packer, (E_TAG_NUMBER)8u, 0xFEDCBA9876543210u));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));

}

// Test pack a char array
void Tpbp_TestTpbp_PackerAdd_CharArray(void)
{
    static uint8_t         expectedForShortString[] = {
        1,
        E_TAG_TYPE_CHAR_ARRAY,
        5,
        'H','e','l','l','o'
        };

    static uint8_t         expectedForLongString[] = {
        2,
        E_TAG_TYPE_EXTENDED_CHAR_ARRAY, 
        4, 1, //4 + 1 x 256 = 260
        'H','e','l','l','o', 'H','e','l','l','o', //10 chars
        'H','e','l','l','o', 'H','e','l','l','o', //20 chars
        'H','e','l','l','o', 'H','e','l','l','o', //30 chars
        'H','e','l','l','o', 'H','e','l','l','o', //40 chars
        'H','e','l','l','o', 'H','e','l','l','o', //50 chars
        'H','e','l','l','o', 'H','e','l','l','o', //60 chars
        'H','e','l','l','o', 'H','e','l','l','o', //70 chars
        'H','e','l','l','o', 'H','e','l','l','o', //80 chars
        'H','e','l','l','o', 'H','e','l','l','o', //90 chars
        'H','e','l','l','o', 'H','e','l','l','o', //100 chars
        'H','e','l','l','o', 'H','e','l','l','o', //110 chars
        'H','e','l','l','o', 'H','e','l','l','o', //120 chars
        'H','e','l','l','o', 'H','e','l','l','o', //130 chars
        'H','e','l','l','o', 'H','e','l','l','o', //140 chars
        'H','e','l','l','o', 'H','e','l','l','o', //150 chars
        'H','e','l','l','o', 'H','e','l','l','o', //160 chars
        'H','e','l','l','o', 'H','e','l','l','o', //170 chars
        'H','e','l','l','o', 'H','e','l','l','o', //180 chars
        'H','e','l','l','o', 'H','e','l','l','o', //190 chars
        'H','e','l','l','o', 'H','e','l','l','o', //200 chars
        'H','e','l','l','o', 'H','e','l','l','o', //210 chars
        'H','e','l','l','o', 'H','e','l','l','o', //220 chars
        'H','e','l','l','o', 'H','e','l','l','o', //230 chars
        'H','e','l','l','o', 'H','e','l','l','o', //240 chars
        'H','e','l','l','o', 'H','e','l','l','o', //250 chars
        'H','e','l','l','o', 'H','e','l','l','o'  //260 chars
        };

    static const char* string260Chars =
        "HelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHello" //100 characters
        "HelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHello" //200 characters
        "HelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHello"; //60 characters
    
    uint8_t bufferPos = 0u;  

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_CharArray(&packer, (E_TAG_NUMBER)1u, "Hello"));

    TEST_ASSERT_EQUAL_UINT32(sizeof(expectedForShortString), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expectedForShortString, &(packBuffer[bufferPos]), sizeof(expectedForShortString));

    bufferPos += sizeof(expectedForShortString);

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_CharArray(&packer, (E_TAG_NUMBER)2u, string260Chars));

    TEST_ASSERT_EQUAL_UINT32(bufferPos + sizeof(expectedForLongString), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expectedForLongString, &(packBuffer[bufferPos]), sizeof(expectedForLongString));

}


// Test pack a U8 array, having a 16 bit length(of elements) field 
void Tpbp_TestTpbp_PackerAdd_ExtU8Array(void)
{
#define NUM_ELEMS       (260)           // = more than 255 elems
#define NUM_ELEM_BYTES  (NUM_ELEMS * 1)

    uint32_t        i;
    static uint8_t elems[NUM_ELEMS];
    static uint8_t  expected [1+1+2 + NUM_ELEM_BYTES] = 
    {
        7,
        E_TAG_TYPE_EXTENDED_U8_ARRAY,
        4, 1, //4 + 1 x 256 = 260 = NUM_ELEMS
    };

    for (i = 0; i < NUM_ELEM_BYTES; i++)
    {
        expected[i + 1+1+2] = 0xA5;
    }

    for (i = 0; i < NUM_ELEMS; i++)
    {
        elems[i] = 0xA5;
    }

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_ExtendedUint8Array(&packer, (E_TAG_NUMBER)7u, elems, NUM_ELEMS));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}



// Test pack a U16 array, having a 16 bit length(of elements) field 
void Tpbp_TestTpbp_PackerAdd_ExtU16Array(void)
{
#define NUM_ELEMS       (260)           // = more than 255 elem
#undef NUM_ELEM_BYTES
#define NUM_ELEM_BYTES  (NUM_ELEMS * 2)

    uint32_t        i;
    static uint16_t elems[NUM_ELEMS];
    static uint8_t  expected [1+1+2 + NUM_ELEM_BYTES] = 
    {
        7,
        E_TAG_TYPE_EXTENDED_U16_ARRAY,
        4, 1, //4 + 1 x 256 = 260 = NUM_ELEMS
    };

    for (i = 0; i < NUM_ELEM_BYTES; i++)
    {
        expected[i + 1+1+2] = 0xA5;
    }

    for (i = 0; i < NUM_ELEMS; i++)
    {
        elems[i] = 0xA5A5;
    }

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_ExtendedUint16Array(&packer, (E_TAG_NUMBER)7u, elems, NUM_ELEMS));

    TEST_ASSERT_EQUAL_U8_ARRAY(expected , packBuffer, sizeof(expected));
}


// Test pack a U32 array, having a 16 bit length(of elements) field 
void Tpbp_TestTpbp_PackerAdd_ExtU32Array(void)
{
#define NUM_ELEMS       (260)           // = more than 255 elems
#undef NUM_ELEM_BYTES
#define NUM_ELEM_BYTES  (NUM_ELEMS * 4)

    uint32_t        i;
    uint8_t         bufferPos = 0u;
    static uint32_t elems[NUM_ELEMS];
    static uint8_t  expected [1+1+2 + NUM_ELEM_BYTES] = 
    {
        7,
        E_TAG_TYPE_EXTENDED_U32_ARRAY,
        4, 1, //4 + 1 x 256 = 260 = NUM_ELEMS
    };

    for (i = 0; i < NUM_ELEM_BYTES; i++)
    {
        expected[i + 1+1+2] = 0xA5;
    }

    for (i = 0; i < NUM_ELEMS; i++)
    {
        elems[i] = 0xA5A5A5A5;
    }

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_ExtendedUint32Array(&packer, (E_TAG_NUMBER)7u, elems, NUM_ELEMS));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected , &(packBuffer[bufferPos]), sizeof(expected));
}


// Test pack of secure block array
void Tpbp_TestTpbp_PackerAdd_SecureBlockArray(void)
{
    bool            isOk = 0;
	const uint8_t	encryptedData[] = {1, 2, 3, 4, 5, 6, 7, 8,
		9, 10, 11, 12, 13, 14, 15, 16};
  
    // Add the data 
    isOk = Tpbp_PackerAdd_SecureBlockArray(&packer, encryptedData, 16);

	TEST_ASSERT_TRUE(isOk);
    
    // test the tag, byte by byte.
	TEST_ASSERT_EQUAL_UINT8(E_TAG_NUMBER_SECURE_DATA,				packBuffer[0]); 
	TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY, packBuffer[1]);
    TEST_ASSERT_EQUAL_UINT8(1u,										packBuffer[2]);
    TEST_ASSERT_EQUAL_UINT8(0u,										packBuffer[3]);
	TEST_ASSERT_EQUAL_U8_ARRAY(encryptedData,					packBuffer + 4, 16);

}



// Test pack of Image Version Info
void Tpbp_TestTpbp_PackerAdd_ImageVersionInfo(void)
{
    bool            isOk = 0;
    S_IMAGE_VERSION_INFO versionInfo;
    uint8_t expected[] = {0x12, 0x34, 0x00, 0x15, 0x06, 0x19, 0x00, 0x01, 0x23, 0x45};

    versionInfo.versionMajorBcd = 0x12u;
    versionInfo.versionMinorBcd = 0x34u;
    versionInfo.versionYearMonthDayBcd = 0x00150619u;
    versionInfo.versionBuildBcd = 0x12345;

    // Add the data 
    isOk = Tpbp_PackerAdd_ImageVersionInfo(&packer, (E_TAG_NUMBER)102u, &versionInfo);

	TEST_ASSERT_TRUE(isOk);
    
    // test the tag, byte by byte.
	TEST_ASSERT_EQUAL_UINT8(102u,           				packBuffer[0]); 
	TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_IMAGE_VERSION_INFO,  packBuffer[1]);
	TEST_ASSERT_EQUAL_U8_ARRAY(expected,					packBuffer + 2, 10);

}




//------------- Parser tests

void Tpbp_TestParserInit(void)
{
    S_TPBP_PARSER   parser;

    // Init to garbage values
    parser.bufferSize = 3;
    parser.pInputBuffer = (uint8_t*)1234;
    parser.readPosition = 1; 

    Tpbp_ParserInit( &parser, parseBuffer, BUFFER_SIZE);

    // Ensure properly initialised
    TEST_ASSERT_EQUAL_PTR(parseBuffer,      parser.pInputBuffer);
    TEST_ASSERT_EQUAL_UINT32(BUFFER_SIZE,   parser.bufferSize);
    TEST_ASSERT_EQUAL_UINT32(0,             parser.readPosition);
}


void Tpbp_TestParserGetBytes(void)
{
    uint8_t         parseBufferLocal[4] = {4,3,2,1 }; 
    uint8_t         testBuffer[4] = {99,99,99,99}; 
    S_TPBP_PARSER   parser;
    bool            isOk = false;

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));
    
    // Read first byte
    isOk = Tpbp_ParserGetBytes( &parser, testBuffer, 1);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL_UINT8(4,             testBuffer[0]); // This must have got filled
    TEST_ASSERT_EQUAL_UINT8(99,            testBuffer[1]); // This must NOT have got filled
    TEST_ASSERT_EQUAL_UINT8(99,            testBuffer[2]); // This must NOT have got filled
    TEST_ASSERT_EQUAL_UINT8(99,            testBuffer[3]); // This must NOT have got filled
    
    // Read another byte
    isOk = Tpbp_ParserGetBytes( &parser, testBuffer, 1);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL_UINT8(3,             testBuffer[0]); // This must have got filled
    TEST_ASSERT_EQUAL_UINT8(99,            testBuffer[1]); // This must NOT have got filled
    TEST_ASSERT_EQUAL_UINT8(99,            testBuffer[2]); // This must NOT have got filled
    TEST_ASSERT_EQUAL_UINT8(99,            testBuffer[3]); // This must NOT have got filled

    // Read two remaining bytes
    isOk = Tpbp_ParserGetBytes( &parser, testBuffer, 2);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL_UINT8(2,             testBuffer[0]); // This must have got filled
    TEST_ASSERT_EQUAL_UINT8(1,             testBuffer[1]); // This must have got filled
    TEST_ASSERT_EQUAL_UINT8(99,            testBuffer[2]); // This must NOT have got filled
    TEST_ASSERT_EQUAL_UINT8(99,            testBuffer[3]); // This must NOT have got filled

    // Try to read another byte (beyond end)
    testBuffer[0] = 99; // reset the test buffer
    testBuffer[1] = 99;
    isOk = Tpbp_ParserGetBytes( &parser, testBuffer, 1);
    TEST_ASSERT_EQUAL_UINT32(0, isOk);              // Must indicate FAIL 

    // Buffer should be unchanged
    TEST_ASSERT_EQUAL_UINT8(99,            testBuffer[0]); 
    TEST_ASSERT_EQUAL_UINT8(99,            testBuffer[1]); 
    TEST_ASSERT_EQUAL_UINT8(99,            testBuffer[2]); 
    TEST_ASSERT_EQUAL_UINT8(99,            testBuffer[3]);
}



void Tpbp_TestParserReadTag(void)
{
    S_TPBP_PARSER   parser;
    bool            isOk = false;
    uint8_t         packBufferLocal[2 * S_TPBP_TAG_ARRAY_PACKED_SIZE]; // 2 tags, exactly.
 	uint8_t			packetDataToParse[] = {
		1, E_TAG_TYPE_BYTE, 1,
		2, E_TAG_TYPE_U16, 2, 0,
		3, E_TAG_TYPE_U32, 3, 0, 0, 0,
		4, E_TAG_TYPE_U64, 4, 0, 0, 0, 0, 0, 0, 0,
		5, E_TAG_TYPE_U8_ARRAY, 2, 5, 6,   //2 byte elements
		6, E_TAG_TYPE_U16_ARRAY, 2, 7, 0, 8, 0, //2 short elements
		7, E_TAG_TYPE_U32_ARRAY, 2, 9, 0, 0, 0, 10, 0, 0, 0,  //2 int elements
		8, E_TAG_TYPE_CHAR_ARRAY, 2, 11, 12, //2 char elements
		9, E_TAG_TYPE_EXTENDED_U8_ARRAY, 2, 0, 13, 14, //2 byte elements
		10, E_TAG_TYPE_EXTENDED_U16_ARRAY, 2, 0, 15, 0, 16, 0, //2 short elements
		11, E_TAG_TYPE_EXTENDED_U32_ARRAY, 2, 0, 17, 0, 0, 0, 18, 0, 0, 0, //2 int elements
		12, E_TAG_TYPE_EXTENDED_CHAR_ARRAY, 2, 0, 19, 20, //2 char elements
		E_TAG_NUMBER_SECURE_DATA, E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY, 2, 0,  //2 secure blocks of 16
			21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, //first block
			37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52 //second block
	};

	uint8_t			secureDataTag[] = {
		E_TAG_NUMBER_SECURE_DATA, E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY, 19, 0  //19 secure blocks of 16
	};

	uint8_t			bigTagNumber[] = {
		E_TAG_NUMBER_EXTENSION_256, 251, E_TAG_TYPE_BYTE, 87  //Tag number 256 + 251
	};

	uint8_t         parseBuffer8[32];
	uint16_t*		parseBuffer16 = (uint16_t*)parseBuffer8;
	uint32_t*		parseBuffer32 = (uint32_t*)parseBuffer8;


    // Populate a tag with some values for testing
    S_TPBP_TAG tag;
    S_TPBP_TAG tagRead = {(E_TAG_NUMBER)0};
    tag.tagNumber       = (E_TAG_NUMBER)1; 
    tag.tagType         = (E_TAG_TYPE)5;
    tag.dataSize        = 99;

    // Create a packet with a couple of tags in.
    Tpbp_PackerInit( &packer, packBufferLocal, 2 * S_TPBP_TAG_ARRAY_PACKED_SIZE);   

    // Add a couple of tags to buffer.
    isOk = Tpbp_PackerTagHelper(&packer, &tag);
    TEST_ASSERT_TRUE(isOk); 
    tag.tagNumber = (E_TAG_NUMBER)2; 
    isOk = Tpbp_PackerTagHelper(&packer, &tag);    
    TEST_ASSERT_TRUE(isOk); 


    Tpbp_ParserInit( &parser, packBufferLocal, sizeof(packBufferLocal));
    
    // Read first tag
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(1,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(tag.tagType,        tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(tag.dataSize,       tagRead.dataSize);

    
    // Read 2nd tag
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(2,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(tag.tagType,        tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(tag.dataSize,       tagRead.dataSize);

    // Should be empty - read should fail
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_EQUAL_UINT32(0, isOk);


	//now read packetDataToParse

	Tpbp_ParserInit( &parser, packetDataToParse, sizeof(packetDataToParse));


    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(1,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_BYTE,    tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(1,					tagRead.dataSize);

	Tpbp_ParserGetBytes(&parser, parseBuffer8, tagRead.dataSize);
	TEST_ASSERT_EQUAL_UINT8(1u,					parseBuffer8[0]);


    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(2,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_U16,     tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(2,					tagRead.dataSize);

	Tpbp_ParserGetBytes(&parser, parseBuffer8,  tagRead.dataSize);
	TEST_ASSERT_EQUAL_UINT16(2u,				parseBuffer16[0]);


    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(3,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_U32,     tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(4,					tagRead.dataSize);

	Tpbp_ParserGetBytes(&parser, parseBuffer8,  tagRead.dataSize);
	TEST_ASSERT_EQUAL_UINT32(3u,				parseBuffer32[0]);
	

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(4,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_U64,     tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(8u,					tagRead.dataSize);

	Tpbp_ParserGetBytes(&parser, parseBuffer8,  tagRead.dataSize);
	TEST_ASSERT_EQUAL_UINT32(4u,				parseBuffer32[0]);
	TEST_ASSERT_EQUAL_UINT32(0u,				parseBuffer32[1]);

	
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(5,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_U8_ARRAY, tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(2,					tagRead.dataSize);

	Tpbp_ParserGetBytes(&parser, parseBuffer8, tagRead.dataSize);
	TEST_ASSERT_EQUAL_UINT8(5u,					parseBuffer8[0]);
	TEST_ASSERT_EQUAL_UINT8(6u,					parseBuffer8[1]);


    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(6,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_U16_ARRAY, tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(4,					tagRead.dataSize);

	Tpbp_ParserGetBytes(&parser, parseBuffer8, tagRead.dataSize);
	TEST_ASSERT_EQUAL_UINT8(7u,					parseBuffer16[0]);
	TEST_ASSERT_EQUAL_UINT8(8u,					parseBuffer16[1]);


    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(7,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_U32_ARRAY, tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(8,					tagRead.dataSize);

	Tpbp_ParserGetBytes(&parser, parseBuffer8, tagRead.dataSize);
	TEST_ASSERT_EQUAL_UINT8(9u,					parseBuffer32[0]);
	TEST_ASSERT_EQUAL_UINT8(10u,				parseBuffer32[1]);

	
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(8,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_CHAR_ARRAY, tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(2,					tagRead.dataSize);

	Tpbp_ParserGetBytes(&parser, parseBuffer8, tagRead.dataSize);
	TEST_ASSERT_EQUAL_UINT8(11u,				parseBuffer8[0]);
	TEST_ASSERT_EQUAL_UINT8(12u,				parseBuffer8[1]);

	
	
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(9,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_EXTENDED_U8_ARRAY, tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(2,					tagRead.dataSize);

	Tpbp_ParserGetBytes(&parser, parseBuffer8, tagRead.dataSize);
	TEST_ASSERT_EQUAL_UINT8(13u,				parseBuffer8[0]);
	TEST_ASSERT_EQUAL_UINT8(14u,				parseBuffer8[1]);

	
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(10,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_EXTENDED_U16_ARRAY, tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(4,					tagRead.dataSize);

	Tpbp_ParserGetBytes(&parser, parseBuffer8, tagRead.dataSize);
	TEST_ASSERT_EQUAL_UINT8(15u,				parseBuffer16[0]);
	TEST_ASSERT_EQUAL_UINT8(16u,				parseBuffer16[1]);

	
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(11,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_EXTENDED_U32_ARRAY, tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(8,					tagRead.dataSize);

	Tpbp_ParserGetBytes(&parser, parseBuffer8, tagRead.dataSize);
	TEST_ASSERT_EQUAL_UINT8(17u,				parseBuffer32[0]);
	TEST_ASSERT_EQUAL_UINT8(18u,				parseBuffer32[1]);

	
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(12,                  tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_EXTENDED_CHAR_ARRAY, tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(2,					tagRead.dataSize);

	Tpbp_ParserGetBytes(&parser, parseBuffer8, tagRead.dataSize);
	TEST_ASSERT_EQUAL_UINT8(19u,				parseBuffer8[0]);
	TEST_ASSERT_EQUAL_UINT8(20u,				parseBuffer8[1]);

	
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(E_TAG_NUMBER_SECURE_DATA, tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY, tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(32,					tagRead.dataSize);

	Tpbp_ParserGetBytes(&parser, parseBuffer8, tagRead.dataSize);
	TEST_ASSERT_EQUAL_UINT8(21u,				parseBuffer8[0]);
	TEST_ASSERT_EQUAL_UINT8(22u,				parseBuffer8[1]);
	TEST_ASSERT_EQUAL_UINT8(52u,				parseBuffer8[31]);

	
	//now read secureDataTag

	Tpbp_ParserInit( &parser, secureDataTag, sizeof(secureDataTag));

	isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(E_TAG_NUMBER_SECURE_DATA, tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY, tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(19*16,				tagRead.dataSize);


	//now read bigTagNumber

	Tpbp_ParserInit( &parser, bigTagNumber, sizeof(bigTagNumber));

	isOk = Tpbp_ParserReadTag(&parser, &tagRead);    
    TEST_ASSERT_TRUE(isOk); 
    TEST_ASSERT_EQUAL_UINT8(507,				tagRead.tagNumber);
	TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_BYTE, tagRead.tagType);
    TEST_ASSERT_EQUAL_UINT8(1,					tagRead.dataSize);
}


void Tpbp_TestParserSkipTagData(void)
{
    S_TPBP_PARSER   parser;
    S_TPBP_TAG      tag;

    static const uint8_t parseBufferLocal[] = {
        E_TAG_NUMBER_EXTENSION_256, 44, E_TAG_TYPE_BYTE_RECORD, 1,
        0xFF,
        E_TAG_NUMBER_EXTENSION_256, 144, E_TAG_TYPE_CHAR_ARRAY, 8,
        'F', 'o', 'u', 'n', 'd', ' ', 'i', 't'
        };

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));
    
    TEST_ASSERT_TRUE(Tpbp_ParserReadTag(&parser, &tag));
    TEST_ASSERT_TRUE(Tpbp_ParserSkipTagData(&parser, &tag));
    TEST_ASSERT_TRUE(Tpbp_ParserReadTag(&parser, &tag));
    TEST_ASSERT_EQUAL_UINT32(400u, tag.tagNumber);
    TEST_ASSERT_EQUAL_UINT32(E_TAG_TYPE_CHAR_ARRAY, tag.tagType);

    TEST_ASSERT_TRUE(Tpbp_ParserGetBytes(&parser, parseBuffer, tag.dataSize));
    
    TEST_ASSERT_EQUAL_U8_ARRAY("Found it", parseBuffer, 8);
}


void Tpbp_TestParserFindTag(void)
{
    S_TPBP_PARSER   parser;
    S_TPBP_TAG      tag;

    static const uint8_t parseBufferLocal[] = {
        E_TAG_NUMBER_EXTENSION_256, 44, E_TAG_TYPE_U8_ARRAY, 1, 0,
        0xFF,
        E_TAG_NUMBER_EXTENSION_256, 144, E_TAG_TYPE_CHAR_ARRAY, 8,
        'F', 'o', 'u', 'n', 'd', ' ', 'i', 't'
        };

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));
    
    TEST_ASSERT_TRUE(Tpbp_ParserFindTag(&parser, (E_TAG_NUMBER)400u, &tag));

    TEST_ASSERT_EQUAL_UINT32(400u, tag.tagNumber);
    TEST_ASSERT_EQUAL_UINT32(E_TAG_TYPE_CHAR_ARRAY, tag.tagType);

    TEST_ASSERT_TRUE(Tpbp_ParserGetBytes(&parser, parseBuffer, tag.dataSize));
    
    TEST_ASSERT_EQUAL_U8_ARRAY("Found it", parseBuffer, 8);
}


void Tpbp_TestParserUnpack32(void)
{
    uint8_t         parseBufferLocal[4] = {4,3,2,1 };
    S_TPBP_PARSER   parser;

    uint32_t n32 = 0;

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));
    
    // Read a 32 bit number  
    n32 = Tpbp_Unpack(&parser, 4); 
    TEST_ASSERT_EQUAL_UINT32(0x01020304, n32);  
}


void Tpbp_TestParserUnpack16(void)
{
    uint8_t         parseBufferLocal[4] = {4,3,2,1 };
    S_TPBP_PARSER   parser;

    uint32_t n32 = 0;

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));
    
    // Read a 16 bit number (will be in LS word) 
    n32 = Tpbp_Unpack(&parser,  2); 
    TEST_ASSERT_EQUAL_UINT16(0x0304, (uint16_t)n32);  
    
    n32 = Tpbp_Unpack(&parser,  2); 
    TEST_ASSERT_EQUAL_UINT16(0x0102, (uint16_t)n32);  
}


void Tpbp_TestParserUnpack8(void)
{
    uint8_t         parseBufferLocal[4] = {4,3,2,1 };
    S_TPBP_PARSER   parser;

    uint32_t n32 = 0;

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));
    
    // Read a 8 bit number (will be in LS byte) 
    n32 = Tpbp_Unpack(&parser, 1); 
    TEST_ASSERT_EQUAL_UINT8(0x04, (uint8_t)n32);  

    n32 = Tpbp_Unpack(&parser, 1); 
    TEST_ASSERT_EQUAL_UINT8(0x03, (uint8_t)n32);  
    
    n32 = Tpbp_Unpack(&parser, 1); 
    TEST_ASSERT_EQUAL_UINT8(0x02, (uint8_t)n32);  
    
    n32 = Tpbp_Unpack(&parser,  1); 
    TEST_ASSERT_EQUAL_UINT8(0x01, (uint8_t)n32);  
}


void Tpbp_TestParserUnpack64(void)
{
    uint8_t         parseBufferLocal[8] = {8,7,6,5,4,3,2,1 };
    S_TPBP_PARSER   parser;

    uint64_t n64 = 0;

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));
    
    // Read a 64 bit number  
    n64 = Tpbp_Unpack64(&parser);
    TEST_ASSERT_EQUAL_UINT32(0x01020304, (uint32_t)(n64 >> 32));  
    TEST_ASSERT_EQUAL_UINT32(0x05060708, (uint32_t)(n64 >>  0));
}


void Tpbp_TestParserRead_ImageVersionInfo(void)
{
    S_TPBP_PARSER   parser;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    uint8_t         data[] = {0xa1, 0x0e, 0x12, 0x34, 0x20, 0x15, 0x06, 0x19, 0x00, 0x12, 0x34, 0x56};
    S_IMAGE_VERSION_INFO imageVersionInfoRead;
    
    Tpbp_ParserInit(&parser, data, sizeof(data));
    
    TEST_ASSERT_TRUE(Tpbp_ParserReadTag(&parser, &tagRead));
    TEST_ASSERT_EQUAL_UINT32((E_TAG_NUMBER)0xa1, tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT32(E_TAG_TYPE_IMAGE_VERSION_INFO, tagRead.tagType);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_ImageVersionInfo(&parser, &imageVersionInfoRead));
    TEST_ASSERT_EQUAL_UINT8(0x12u, imageVersionInfoRead.versionMajorBcd);
    TEST_ASSERT_EQUAL_UINT8(0x34u, imageVersionInfoRead.versionMinorBcd);
    TEST_ASSERT_EQUAL_UINT32(0x20150619u, imageVersionInfoRead.versionYearMonthDayBcd);
    TEST_ASSERT_EQUAL_UINT8(0x123456, imageVersionInfoRead.versionBuildBcd);
}


void Tpbp_TestParserRead_String(void)
{
    S_TPBP_PARSER   parser;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    char*           stringBuffer = (char*)parseBuffer;
    uint8_t         dataCharArray[] = {46, E_TAG_TYPE_CHAR_ARRAY, 11, 'h', 't', 't', 'p', ':', '/', '/', 'a', 'b', 'c', 'd', 0xFF};
    uint8_t         dataExtendedCharArray[] = {46, E_TAG_TYPE_EXTENDED_CHAR_ARRAY, 11, 0, 'h', 't', 't', 'p', ':', '/', '/', 'a', 'b', 'c', 'd', 0xFF};

    Tpbp_ParserInit(&parser, dataCharArray, sizeof(dataCharArray));
    
    TEST_ASSERT_TRUE(Tpbp_ParserReadTag(&parser, &tagRead));
    TEST_ASSERT_EQUAL_UINT32((E_TAG_NUMBER)46, tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT32(E_TAG_TYPE_CHAR_ARRAY, tagRead.tagType);

    TEST_ASSERT_FALSE(Tpbp_ParserRead_String(&parser, &tagRead, stringBuffer, 11)); //buffer too short
    TEST_ASSERT_TRUE(Tpbp_ParserRead_String(&parser, &tagRead, stringBuffer, 12)); //buffer long enough
    TEST_ASSERT_EQUAL_STRING("http://abcd", stringBuffer);

    TEST_ASSERT_TRUE(Tpbp_ParserReadTag(&parser, &tagRead));
    TEST_ASSERT_EQUAL_UINT32(E_TAG_NUMBER_EOF, tagRead.tagNumber);

    Tpbp_ParserInit(&parser, dataExtendedCharArray, sizeof(dataExtendedCharArray));
    
    TEST_ASSERT_TRUE(Tpbp_ParserReadTag(&parser, &tagRead));
    TEST_ASSERT_EQUAL_UINT32((E_TAG_NUMBER)46, tagRead.tagNumber);
    TEST_ASSERT_EQUAL_UINT32(E_TAG_TYPE_EXTENDED_CHAR_ARRAY, tagRead.tagType);

    TEST_ASSERT_FALSE(Tpbp_ParserRead_String(&parser, &tagRead, stringBuffer, 11)); //buffer too short
    TEST_ASSERT_TRUE(Tpbp_ParserRead_String(&parser, &tagRead, stringBuffer, 12)); //buffer long enough
    TEST_ASSERT_EQUAL_STRING("http://abcd", stringBuffer);

    TEST_ASSERT_TRUE(Tpbp_ParserReadTag(&parser, &tagRead));
    TEST_ASSERT_EQUAL_UINT32(E_TAG_NUMBER_EOF, tagRead.tagNumber);
}





// Start and end a secure region
void Tpbp_TestSecureBegin(void)
{     
    bool            isOk = false;
    S_TPBP_PACKER   packer; 
    const uint8_t   myBytes[2] = {0xAA, 0x55};
    uint8_t*        pSecureData;
    const uint32_t  tagLength = 4;// Extended Secure tag has 2 byte length field
    uint32_t        numSecureBytes = 0;

    Tpbp_PackerInit( &packer, packBuffer, BUFFER_SIZE);   

    isOk = Tpbp_Packer_BeginSecurePack(&packer);
    
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_EQUAL_UINT8(E_TAG_NUMBER_SECURE_DATA,               packBuffer[0]);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY, packBuffer[1]);
    TEST_ASSERT_EQUAL_UINT8(0, packBuffer[2]);

    TEST_ASSERT_EQUAL_UINT8(tagLength, packer.writePosition);
    TEST_ASSERT_EQUAL_UINT8(tagLength, packer.secureDataPosition);

    // Write 2 bytes
    isOk = Tpbp_PackerAddBytes(&packer, myBytes, sizeof(myBytes));
    
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL_UINT8(tagLength + sizeof(myBytes), packer.writePosition);
    TEST_ASSERT_EQUAL_UINT8(tagLength,                   packer.secureDataPosition);
    TEST_ASSERT_EQUAL_U8_ARRAY(myBytes, 
         &packer.pOutputBuffer[packer.secureDataPosition],
         sizeof(myBytes));

    pSecureData = Tpbp_Packer_EndSecurePack(&packer, &numSecureBytes);
    
    // Should now be padded to 16 bytes
    TEST_ASSERT_TRUE(NULL != pSecureData);
    TEST_ASSERT_EQUAL_UINT32(16, numSecureBytes);
    TEST_ASSERT_EQUAL_U8_ARRAY(myBytes, pSecureData, sizeof(myBytes));
    
    TEST_ASSERT_EQUAL_UINT8(tagLength + 16, packer.writePosition);

    // Tag should be undisturbed but with length in blocks (=1) now set.
    TEST_ASSERT_EQUAL_UINT8(E_TAG_NUMBER_SECURE_DATA,               packBuffer[0]);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY, packBuffer[1]);
    TEST_ASSERT_EQUAL_UINT8(1,                                      packBuffer[2]);
    TEST_ASSERT_EQUAL_UINT8(0,                                      packBuffer[3]);
}

// Same as prev test but check edge case of no secure data
void Tpbp_TestSecureBeginNoData(void)
{
    bool            isOk = false;
    S_TPBP_PACKER   packer;
    uint8_t*        pSecureData;
    const uint32_t  tagLength = 4;// Extended Secure tag has 2 byte length field
    uint32_t        numSecureBytes = 0;

    Tpbp_PackerInit( &packer, packBuffer, BUFFER_SIZE);   

    isOk = Tpbp_Packer_BeginSecurePack(&packer);
    TEST_ASSERT_TRUE(isOk);
    
    pSecureData = Tpbp_Packer_EndSecurePack(&packer, &numSecureBytes);

    // Should be 0
    TEST_ASSERT_TRUE(NULL != pSecureData);
    TEST_ASSERT_EQUAL_UINT32(0, numSecureBytes); 
    
    TEST_ASSERT_EQUAL_UINT8(tagLength + 0, packer.writePosition);

    // Tag should be undisturbed but with length in blocks (=0)
    TEST_ASSERT_EQUAL_UINT8(E_TAG_NUMBER_SECURE_DATA,               packBuffer[0]);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY, packBuffer[1]);
    TEST_ASSERT_EQUAL_UINT8(0,                                      packBuffer[2]);
    TEST_ASSERT_EQUAL_UINT8(0,                                      packBuffer[3]);
}


// Start and end a secure region, with 32 bytes so no padding, 2 blocks
void Tpbp_TestSecureBeginAlreadyPadded(void)
{     
    bool            isOk = false;
    S_TPBP_PACKER   packer; 
    const uint8_t   myBytes[32] = {0};
    uint8_t*        pSecureData;
    const uint32_t  tagLength = 4;// Extended Secure tag has 2 byte length field
    uint32_t        numSecureBytes = 0;

    Tpbp_PackerInit( &packer, packBuffer, BUFFER_SIZE);   

    isOk = Tpbp_Packer_BeginSecurePack(&packer); 
    TEST_ASSERT_TRUE(isOk);

    // Write 32 bytes
    isOk = Tpbp_PackerAddBytes(&packer, myBytes, sizeof(myBytes));
    
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL_UINT8(tagLength + sizeof(myBytes), packer.writePosition);
    TEST_ASSERT_EQUAL_UINT8(tagLength,                   packer.secureDataPosition);

    pSecureData = Tpbp_Packer_EndSecurePack(&packer, &numSecureBytes);
    
    // Should still be 32 bytes
    TEST_ASSERT_TRUE(NULL != pSecureData);
    TEST_ASSERT_EQUAL_UINT32(32, numSecureBytes);
    TEST_ASSERT_EQUAL_U8_ARRAY(myBytes, pSecureData, sizeof(myBytes));
    TEST_ASSERT_EQUAL_UINT8(tagLength + 32, packer.writePosition);

    // Tag should be undisturbed but with length in blocks (=2) now set.
    TEST_ASSERT_EQUAL_UINT8(E_TAG_NUMBER_SECURE_DATA,               packBuffer[0]);
    TEST_ASSERT_EQUAL_UINT8(E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY, packBuffer[1]);
    TEST_ASSERT_EQUAL_UINT8(2,                                      packBuffer[2]);
    TEST_ASSERT_EQUAL_UINT8(0,                                      packBuffer[3]);
}


/**
 * @}
 * @}
 */
