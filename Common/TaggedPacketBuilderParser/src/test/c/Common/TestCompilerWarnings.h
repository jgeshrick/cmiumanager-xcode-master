/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/


/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup TpbpUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test for the @ref Tagged Packet Builder Parser - for compiler quirks or unwanted/unwarranted warnings
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 */


#ifndef __TPBP_COMPILER_WARNINGS_H__
#define __TPBP_COMPILER_WARNINGS_H__



void Tpbp_TestCompilerFpInStruct(void);


#endif
