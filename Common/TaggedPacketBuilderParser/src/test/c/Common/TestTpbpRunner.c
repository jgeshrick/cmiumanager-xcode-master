/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @defgroup TpbpUnitTest 
 *
 * @brief Unit Test for the @ref Tagged Packet Builder Parser  
 *
 */

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup TpbpUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test runner for the @ref Tagged Packet Builder Parser
 * @author Duncan Willis
 * @date 2015.03.05  
 * @version 1.0
 */
 

#include "unity.h"
#include "TeamCity.h"
#include "TestCompilerWarnings.h"
#include "TestTpbp.h"
#include "Tpbp.h"


extern void setUp(void);
extern void tearDown(void);


/** This function is used by unity */
void resetTest(void)
{
    tearDown();
    setUp();
}


/**
** Runs the unit tests for module Tagged Packet Builder Parser
**/
int TestTpbp(void)
{
    int testFailures = 0;
 
    Unity.TestFile = "TestTpbp.c";
    UnityBegin();

    printf("Testing module %s\n", Unity.TestFile);

    // Compiler Warnings test
    RUN_TEST(Tpbp_TestCompilerFpInStruct,                   __LINE__);
        
    // General tests
    RUN_TEST(Tpbp_TestStructSizes,                          __LINE__);
    RUN_TEST(Tpbp_TestEndiannessIsLittle,                   __LINE__);
    RUN_TEST(Tpbp_TestTagTypeEnum,                          __LINE__);
    RUN_TEST(Tpbp_TestUrlLength,                            __LINE__);     
    RUN_TEST(Tpbp_TestTagNumberEnum,                        __LINE__);

    // General Packer tests
    RUN_TEST(Tpbp_TestPackerInit,                           __LINE__);
    RUN_TEST(Tpbp_TestPackerReset,                          __LINE__);
    RUN_TEST(Tpbp_TestPackerGetCount,                       __LINE__);
    RUN_TEST(Tpbp_TestPackerGetAvailable,                   __LINE__);
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_PacketTypeId,          __LINE__);
    RUN_TEST(Tpbp_TestAddBytes,                             __LINE__);
	RUN_TEST(Tpbp_TestTpbp_PackTagHelper,                   __LINE__);
    RUN_TEST(Tpbp_TestTpbp_PackerPadZeroToBlockBoundary,    __LINE__);
    RUN_TEST(Tpbp_TestTpbp_Pack,                            __LINE__);
    RUN_TEST(Tpbp_TestTpbp_Pack64,                          __LINE__);

    // Tag Packer tests
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_CmiuPacketHeader,      __LINE__); // tag num 1
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_CmiuConfiguration,     __LINE__); // tag num 2
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_CmiuStatus,            __LINE__);
    RUN_TEST(Tpbp_Test_NetworkOps,                          __LINE__);
    RUN_TEST(Tpbp_Test_NetworkPerformance,                  __LINE__);
    RUN_TEST(Tpbp_Test_RegistrationStatus,                  __LINE__);
    RUN_TEST(Tpbp_Test_MqttBroker,                          __LINE__);
    RUN_TEST(Tpbp_Test_FallbackMqttBroker,                  __LINE__);
    RUN_TEST(Tpbp_Test_AssignedCmiuAddress,                 __LINE__);
    RUN_TEST(Tpbp_TestPackerAdd_FtpPortNumber,              __LINE__);
    RUN_TEST(Tpbp_Test_ModemHardwareRevision,               __LINE__);
    RUN_TEST(Tpbp_Test_ModemModelId,                        __LINE__);
    RUN_TEST(Tpbp_Test_ModemManufacturerId,                 __LINE__);
    RUN_TEST(Tpbp_Test_ModemSoftwareRevision,               __LINE__);
    RUN_TEST(Tpbp_Test_ModemSerialNumber,                   __LINE__);
    RUN_TEST(Tpbp_Test_ModemSimImsiNumber,                  __LINE__);
    RUN_TEST(Tpbp_Test_ModemSimId,                          __LINE__);
    RUN_TEST(Tpbp_Test_PinPukPuk2,                          __LINE__);
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_RevisionHardware,      __LINE__);
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_ReportedDeviceConfig,  __LINE__);
    
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_CmiuBasicConfiguration,__LINE__); // tag num 33
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_IntervalRecordingConfig,__LINE__);// tag num 34
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_UartPacketHeader,      __LINE__); // tag num 35
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_RevisionHardware,      __LINE__); // tag num 38
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_RevisionFirmware,      __LINE__); // tag num 39
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_RevisionBootloader,    __LINE__); // tag num 40
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_RevisionConfig,        __LINE__); // tag num 41
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_RevisionArb,           __LINE__); // tag num 42
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_RevisionBle,           __LINE__); // tag num 43
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_RevisionEncryption,    __LINE__); // tag num 49
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_CmiuInformation,       __LINE__); // tag num 44
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_ConnectionTimingLog,   __LINE__);
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_CmiuDiagnostics,       __LINE__);
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_ImageName,             __LINE__);  // Tag Num 46
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_ImageMetaData,         __LINE__);  // Tag Num 47
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_RecordingReporting,    __LINE__);  // Tag Num 48
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_TimeDate,              __LINE__);  // Tag Num 50
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_ErrorCode,             __LINE__);  // Tag Num 51
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_debugData,             __LINE__);  // Tag Num 52
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_StartDelay,            __LINE__);  // Tag Num 53
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_Command_Duration,      __LINE__);  // Tag Num 54
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_Command_ReadConnectedDevice,  __LINE__); // tag Num 55
    RUN_TEST( Tpbp_TestTpbp_PackerAdd_Command_PowerMode,    __LINE__); // tag Num 56
    RUN_TEST( Tpbp_TestTpbp_PackerAdd_Command_ReadMemory,   __LINE__); // tag Num 57
    RUN_TEST( Tpbp_TestTpbp_PackerAdd_Command_RFTestMode,   __LINE__); // tag Num 58
    RUN_TEST( Tpbp_TestTpbp_PackerAdd_Command_ToggleLTEModem, __LINE__); // tag Num 59


    
    RUN_TEST( Tpbp_TestPackerAdd_BatteryVoltage,            __LINE__);      // 62
    RUN_TEST( Tpbp_TestPackerAdd_CmiuTemperature,           __LINE__);      // 63
    RUN_TEST( Tpbp_TestPackerAdd_MemImage,                  __LINE__);      // 64
    RUN_TEST( Tpbp_TestPackerAdd_PktType,                   __LINE__);      // 65
    RUN_TEST( Tpbp_TestPackerAdd_Destination,               __LINE__);      // 66
    RUN_TEST( Tpbp_TestPackerAdd_MetaDataForPacket,         __LINE__);      // 67
    RUN_TEST( Tpbp_TestPackerAdd_CmiuId,                    __LINE__);      // 68
    RUN_TEST( Tpbp_TestPackerAdd_Apn,                       __LINE__);      // 69
    RUN_TEST( Tpbp_TestPackerAdd_ApnExtended,               __LINE__);      // 69
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_Command_Btle_RFTestMode,__LINE__);     // 70
    RUN_TEST(Tpbp_TestPackerAdd_NewImageVersion,            __LINE__);      // 71
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_Eof,                   __LINE__);
	RUN_TEST(Tpbp_TestTpbp_PackerAdd_UInt,                  __LINE__);
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_UInt64,                __LINE__);
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_CharArray,             __LINE__);
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_ExtU8Array,            __LINE__);
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_ExtU16Array,           __LINE__);
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_ExtU32Array,           __LINE__);
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_SecureBlockArray,      __LINE__);
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_ImageVersionInfo,      __LINE__);

    // Parser tests
    RUN_TEST(Tpbp_TestParserInit,                           __LINE__);    
    RUN_TEST(Tpbp_TestParserGetBytes,                       __LINE__);
    RUN_TEST(Tpbp_TestParserReadTag,                        __LINE__);
    RUN_TEST(Tpbp_TestParserSkipTagData,                    __LINE__);
    RUN_TEST(Tpbp_TestParserFindTag,                        __LINE__);
    RUN_TEST(Tpbp_TestParserUnpack32,                       __LINE__);
    RUN_TEST(Tpbp_TestParserUnpack16,                       __LINE__);
    RUN_TEST(Tpbp_TestParserUnpack8,                        __LINE__);
    RUN_TEST(Tpbp_TestParserUnpack64,                       __LINE__);
    RUN_TEST(Tpbp_TestParserRead_ImageVersionInfo,          __LINE__);
    RUN_TEST(Tpbp_TestParserRead_String,                    __LINE__);
   
    // Tag Parsers
    RUN_TEST(Tpbp_TestParserRead_CmiuPacketHeader,          __LINE__); // tag num 1
    RUN_TEST(Tpbp_TestParserRead_CmiuConfiguration,         __LINE__); // tag num 2
    RUN_TEST(Tpbp_TestParserRead_CmiuDiagnostics,           __LINE__); // tag num 23
    RUN_TEST(Tpbp_TestParserRead_CmiuStatus,                __LINE__); // tag num 26
    RUN_TEST(Tpbp_TestParserRead_ReportedDeviceConfig,      __LINE__); // tag num 27    
    RUN_TEST(Tpbp_TestParserRead_ConnectionTimingLog,       __LINE__); // tag num 31
    RUN_TEST(Tpbp_TestTpbp_PackerAdd_Command,               __LINE__); // tag num 32 
    RUN_TEST(Tpbp_TestParserRead_CmiuBasicConfiguration,    __LINE__); // tag num 33  
    RUN_TEST(Tpbp_TestParserRead_IntervalRecordingConfig,   __LINE__); // tag num 34  
    RUN_TEST(Tpbp_TestParserRead_UartPacketHeader,          __LINE__); // tag num 35  
    RUN_TEST(Tpbp_TestParserRead_RevisionHardware,          __LINE__); // tag num 38
    RUN_TEST(Tpbp_TestParserRead_RevisionFirmware,          __LINE__); // tag num 39
    RUN_TEST(Tpbp_TestParserRead_RevisionBootloader,        __LINE__); // tag num 40
    RUN_TEST(Tpbp_TestParserRead_RevisionConfig,            __LINE__); // tag num 41
    RUN_TEST(Tpbp_TestParserRead_RevisionArb,               __LINE__); // tag num 42
    RUN_TEST(Tpbp_TestParserRead_RevisionBle,               __LINE__); // tag num 43
    RUN_TEST(Tpbp_TestParserRead_RevisionEncryption,        __LINE__); // tag num 49
    RUN_TEST(Tpbp_TestParserRead_CmiuInformation,           __LINE__); // tag num 44
    RUN_TEST(Tpbp_TestParserRead_ImageName,                 __LINE__); // tag num 46
    RUN_TEST(Tpbp_TestParserRead_ImageMetaData,             __LINE__); // tag num 47
    RUN_TEST(Tpbp_TestParserRead_RecordingReporting,        __LINE__); // tag num 48
    RUN_TEST(Tpbp_TestParserRead_Time,                      __LINE__); // General
    RUN_TEST(Tpbp_TestParserRead_ErrorCode,                 __LINE__); // tag num 51
    RUN_TEST(Tpbp_TestParserRead_StartDelay,                __LINE__); // tag num 53
    RUN_TEST(Tpbp_TestParserRead_Command_Duration,          __LINE__); // tag num 54
    RUN_TEST(Tpbp_TestParserRead_Command_ReadConnectedDevice,__LINE__);// tag num 55
    RUN_TEST(Tpbp_TestParserRead_Command_PowerMode,         __LINE__); // tag num 56
    RUN_TEST(Tpbp_TestParserRead_Command_ReadMemory,        __LINE__); // tag num 57
    RUN_TEST(Tpbp_TestParserRead_Command_RFTestMode,        __LINE__); // tag num 58
    RUN_TEST(Tpbp_TestParserRead_Command_ToggleLTEModem,    __LINE__); // tag num 59
    RUN_TEST(Tpbp_TestParserRead_BatteryVoltage,            __LINE__); // tag num 62
    RUN_TEST(Tpbp_TestParserRead_CmiuTemperature,           __LINE__); // tag num 63
    RUN_TEST(Tpbp_TestParserRead_MemoryImageTag,            __LINE__); // tag num 64
    RUN_TEST(Tpbp_TestParserRead_PacketTypeTag,             __LINE__); // tag num 65
    RUN_TEST(Tpbp_TestParserRead_Destination,               __LINE__); // tag num 66
    RUN_TEST(Tpbp_TestParserRead_MetadataForPacket,         __LINE__); // tag num 67
    RUN_TEST(Tpbp_TestParserRead_CmiuId,                    __LINE__); // tag num 68
    RUN_TEST(Tpbp_TestParserRead_Apn,                       __LINE__); // tag num 69
    RUN_TEST(Tpbp_TestParserRead_ApnExtended,               __LINE__); // tag num 69
    RUN_TEST(Tpbp_TestParserRead_Command_Btle_RFTestMode,   __LINE__); // tag num 70

    RUN_TEST(Tpbp_TestSecureBegin,                          __LINE__); 
    RUN_TEST(Tpbp_TestSecureBeginNoData,                    __LINE__); 
    RUN_TEST(Tpbp_TestSecureBeginAlreadyPadded,             __LINE__);    
    testFailures = UnityEnd();

    return testFailures;
}


/**
 * @}
 * @}
 */

