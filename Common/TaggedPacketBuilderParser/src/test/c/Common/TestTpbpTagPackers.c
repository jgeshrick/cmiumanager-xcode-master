/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup TpbpUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief *** Packers for specific tags ***  
 * @author Duncan Willis
 * @date 2015.10.29
 * @version 1.0
 */


#include <stdint.h>
#include <stdio.h>
#include "CommonTypes.h"
#include "unity.h"
#include "Tpbp.h"
#include "TpbpPackerHelpers.h"
#include "TpbpParserHelpers.h"
#include "TestTpbp.h"

extern uint8_t         packBuffer[];
extern uint8_t         parseBuffer[];
extern S_TPBP_PACKER   packer;
extern S_TPBP_PARSER   parser; 


// Checks both Pack and Parse of a string based tag. Takes the packer function as arg
// The parser is alwys a string read; and the tag number.
static void Tpbp_TestTpbp_StringBasedTagsHelper(bool(*pfn)(S_TPBP_PACKER*, const char*), E_TAG_NUMBER tagNumber)
{
    char stringBuffer[10]; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    static uint8_t  expected[] = {'a', 'b', 'c', '\0'};
    

    pfn(&packer, "abc");
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_EQUAL(tagNumber, tagRead.tagNumber);
    TEST_ASSERT_TRUE(Tpbp_ParserRead_String(&parser, &tagRead, stringBuffer, sizeof(stringBuffer)));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, stringBuffer, sizeof(expected));
}

// 3
void Tpbp_Test_NetworkOps(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_NetworkOperators, (E_TAG_NUMBER)3);
}

// 4
void Tpbp_Test_NetworkPerformance(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_NetworkPerformance, (E_TAG_NUMBER)4);
}


// 5
void Tpbp_Test_RegistrationStatus(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_RegistrationStatus, (E_TAG_NUMBER)5);
}

void Tpbp_Test_MqttBroker(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_MqttBrokerAddress, (E_TAG_NUMBER)6);
}

void Tpbp_Test_FallbackMqttBroker(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_FallbackMqttBrokerAddress, (E_TAG_NUMBER)7);
}

void Tpbp_Test_AssignedCmiuAddress(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_AssignedCmiuAddress, (E_TAG_NUMBER)8);
}

// 10
void Tpbp_TestPackerAdd_FtpPortNumber(void)
{
    const uint16_t testVal = 0x1234;

    static uint8_t         expected[] = {
        10,
        E_TAG_TYPE_U16,
        0x34, 0x12
    };

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_FtpPortNumber(&packer, testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}

void Tpbp_Test_ModemHardwareRevision(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_ModemHardwareRevision, (E_TAG_NUMBER)15);
}


void Tpbp_Test_ModemModelId(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_ModemModelId, (E_TAG_NUMBER)16);
}

void Tpbp_Test_ModemManufacturerId(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_ModemManufacturerId, (E_TAG_NUMBER)17);
}


void Tpbp_Test_ModemSoftwareRevision(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_ModemSoftwareRevision, (E_TAG_NUMBER)18);
}

void Tpbp_Test_ModemSerialNumber(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_ModemSerialNumber, (E_TAG_NUMBER)19);
}


void Tpbp_Test_ModemSimImsiNumber(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_ModemSimImsiNumber, (E_TAG_NUMBER)20);
}


void Tpbp_Test_ModemSimId(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_ModemSimId, (E_TAG_NUMBER)21);
}


void Tpbp_Test_PinPukPuk2(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAddPinPukPuk2, (E_TAG_NUMBER)22);
}


void Tpbp_TestTpbp_PackerAdd_ConnectionTimingLog(void)
{
    S_CONNECTION_TIMING_LOG timingLog;

    static uint8_t         expected[] = {
        E_TAG_NUMBER_CONNECTION_TIMING_LOG,
        E_TAG_TYPE_BYTE_RECORD, 
        S_CONNECTION_TIMING_LOG_PACKED_SIZE,
        0xEF, 0xCD, 0xAB, 0x89, 0x67, 0x45, 0x23, 0x01,
        53, 0, 54, 0, 55, 0, 56, 0, 57, 0
        };

    timingLog.dateOfConnection                                  = 0x123456789ABCDEFuLL;
    timingLog.timeFromPowerOnToNetworkRegistration              = 53u;
    timingLog.timeFromNetworkRegistrationToContextActivation    = 54u;
    timingLog.timeFromContextActivationToServerConnection       = 55u;
    timingLog.timeToTransferMessageToServer                     = 56u;
    timingLog.timeToDisconnectAndShutdown                       = 57u;

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_ConnectionTimingLog(&packer, &timingLog));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// tag number 23
void Tpbp_TestTpbp_PackerAdd_CmiuDiagnostics(void)
{
    S_CMIU_DIAGNOSTICS diagnostics;

    static uint8_t         expected[] = {
        E_TAG_NUMBER_CMIU_HW_DIAGNOSTICS,
        E_TAG_TYPE_BYTE_RECORD, 
        S_CMIU_DIAGNOSTICS_PACKED_SIZE,
        48, 0, 0, 0,
        49
        };
    
    diagnostics.diagnosticsResult = 48u;
    diagnostics.processorResetCounter = 49u;

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_CmiuDiagnostics(&packer, &diagnostics));

    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// Status / flags type 26
void Tpbp_TestTpbp_PackerAdd_CmiuStatus(void)
{
    S_CMIU_STATUS status;

    static uint8_t         expected[] = {
        E_TAG_NUMBER_CMIU_FLAGS,
        E_TAG_TYPE_BYTE_RECORD, 
        S_CMIU_STATUS_PACKED_SIZE,
        0x41,
        3u,
        0x67,0xA5
        };
     
    status.cmiuType = 0x41;
    status.numberOfDevices = 3;
    status.cmiuFlags = 0xA567; 

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_CmiuStatus(&packer, &status));

    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


//  REPORTED_DEVICE_CONFIG type 27
void Tpbp_TestTpbp_PackerAdd_ReportedDeviceConfig(void)
{
    S_REPORTED_DEVICE_CONFIG cnf;

    static uint8_t         expected[] = {
        E_TAG_NUMBER_DEVICE_CONFIGURATION,
        E_TAG_TYPE_BYTE_RECORD, 
        S_REPORTED_DEVICE_CONFIG_PACKED_SIZE,
        0x56,
        0xEF, 0xCD, 0xAB, 0x89, 0x67, 0x45, 0x23, 0x01,
        0xee,0xDD,
        0x01, 0xF5, 0x67, 0x1B,
        0xFF, 0xD3
    };

    cnf.deviceNumber        = 0x56;
    cnf.attachedDeviceId    = 0x0123456789ABCDEFull;
    cnf.deviceType          = 0xddee;
    cnf.currentDeviceData	= 0x1b67f501;
    cnf.deviceFlags         = 0xd3ff;

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_ReportedDeviceConfig(&packer, &cnf));

    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// Command  type 32
void Tpbp_TestTpbp_PackerAdd_Command(void)
{
    static uint8_t expected[] = {
        32,
        E_TAG_TYPE_BYTE,          
        0xAA // the command
    };

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Command(&packer, 0xAA));
    TEST_ASSERT_EQUAL_UINT32(3, Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}



// CmiuBasicConfiguration  type 33
void Tpbp_TestTpbp_PackerAdd_CmiuBasicConfiguration(void)
{
    S_CMIU_BASIC_CONFIGURATION bc;

    static uint8_t         expected[] = {
        E_TAG_NUMBER_CMIU_BASIC_CONFIGURATION,
        E_TAG_TYPE_BYTE_RECORD, 
        S_CMIU_BASIC_CONFIGURATION_PACKED_SIZE,
        0x34,0x12,
        0xC1,
        0xC2,
        0x02,
        0x22,0x11,
        0x44,0x33,
        0x05,
        0xAA
    };

    Tpbp_PackerInit( &packer, packBuffer, BUFFER_SIZE); 
    bc.initialCallInTime        = 0x1234;
    bc.callInInterval           = 0xC1;
    bc.callInWindow             = 0xC2;
    bc.reportingwindowNRetries  = 0x02;
    bc.quietTimeStartMins       = 0x1122;
    bc.quietTimeEndMins         = 0x3344;
    bc.numAttachedDevices       = 0x05;
    bc.eventMask                = 0xAA;
 
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_CmiuBasicConfiguration(&packer, &bc));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}

//  IntervalRecordingConfig  type 34
void Tpbp_TestTpbp_PackerAdd_IntervalRecordingConfig(void)
{
    S_INTERVAL_RECORDING_CONFIG cnf;

    static uint8_t         expected[] = {
        E_TAG_NUMBER_INTERVAL_RECORDING,
        E_TAG_TYPE_BYTE_RECORD, 
        S_INTERVAL_RECORDING_CONFIG_PACKED_SIZE,
        0x56,
        0x67,
        0xE4,
        0x8A,
        0x79,0xA6,
        0xEF, 0xCD, 0xAB, 0x89, 0x67, 0x45, 0x23, 0x01
    };

    Tpbp_PackerInit( &packer, packBuffer, BUFFER_SIZE);   
    cnf.deviceNumber         = 0x56;
    cnf.intervalFormat       = 0x67;
    cnf.recordingIntervalMin = 0xE4;
    cnf.reportingIntervalHrs = 0x8A;
    cnf.intervalStartTime    = 0xA679;
    cnf.intervalLastRecordDateTime = 0x0123456789ABCDEFull;
 
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_IntervalRecordingConfig(&packer, &cnf));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// UartPacketHeader Tag Number 35
void Tpbp_TestTpbp_PackerAdd_UartPacketHeader(void)
{
    S_MQTT_BLE_UART bc;

    static uint8_t         expected[] = {
        E_TAG_NUMBER_UART_PACKET_HEADER,
        E_TAG_TYPE_BYTE_RECORD, 
        S_MQTT_BLE_UART_PACKED_SIZE,
        0x01,
        0xEE,
        0xFE,
        0x33
    };

    bc.sequenceNumber    = 0x01;
    bc.keyInfo           = 0xEE;
    bc.encryptionMethod  = 0xFE;
    bc.token             = 0x33;

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_UartPacketHeader(&packer, &bc));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}



// Revision - helper for all similar tags  
// Rev has no length field, fixed 10 byte array.
static void Tpbp_TestTpbp_PackerAdd_RevisionHelper(uint8_t tagNumber, bool (*pfn)(S_TPBP_PACKER*,const S_IMAGE_VERSION_INFO* )  )
{
    S_IMAGE_VERSION_INFO rev;

    static uint8_t expected[] = {
        0,
        E_TAG_TYPE_IMAGE_VERSION_INFO,
        0x01,
        0x02, 
        0x15,0x04,0x21,0x0,
        0x16,0x04,0x21,0x3};

    expected[0] = tagNumber;

    rev.versionMajorBcd = 0x01;
    rev.versionMinorBcd = 0x02;
    rev.versionYearMonthDayBcd = 0x15042100;
    rev.versionBuildBcd        = 0x16042103;
 
    TEST_ASSERT_TRUE(pfn(&packer, &rev));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


//  hw Revision  type 38  
void Tpbp_TestTpbp_PackerAdd_RevisionHardware(void)
{   
    Tpbp_TestTpbp_PackerAdd_RevisionHelper(E_TAG_NUMBER_HARDWARE_REVISION,Tpbp_PackerAdd_ImageVersionInfoHardware); 
}
//   fw ImageVersionInfo  type 39  
void Tpbp_TestTpbp_PackerAdd_RevisionFirmware(void)
{   
    Tpbp_TestTpbp_PackerAdd_RevisionHelper(E_TAG_NUMBER_FIRMWARE_REVISION,Tpbp_PackerAdd_ImageVersionInfoFirmware); 
}
//  Revision  type 40  
void Tpbp_TestTpbp_PackerAdd_RevisionBootloader(void)
{   
    Tpbp_TestTpbp_PackerAdd_RevisionHelper(E_TAG_NUMBER_BOOTLOADER_REVISION,Tpbp_PackerAdd_ImageVersionInfoBootloader); 
}
//  Revision  type 41  
void Tpbp_TestTpbp_PackerAdd_RevisionConfig(void)
{   
    Tpbp_TestTpbp_PackerAdd_RevisionHelper(E_TAG_NUMBER_CONFIG_REVISION,Tpbp_PackerAdd_ImageVersionInfoConfig); 
}
//  Revision  type 42  
void Tpbp_TestTpbp_PackerAdd_RevisionArb(void)
{   
    Tpbp_TestTpbp_PackerAdd_RevisionHelper(E_TAG_NUMBER_ARB_REVISION,Tpbp_PackerAdd_ImageVersionInfoArb); 
}
//  Revision  type 43  
void Tpbp_TestTpbp_PackerAdd_RevisionBle(void)
{   
    Tpbp_TestTpbp_PackerAdd_RevisionHelper(E_TAG_NUMBER_BLE_CONFIG_REVISION,Tpbp_PackerAdd_ImageVersionInfoBle); 
}
//  Revision  type 49
void Tpbp_TestTpbp_PackerAdd_RevisionEncryption(void)
{   
    Tpbp_TestTpbp_PackerAdd_RevisionHelper(E_TAG_NUMBER_ENCRYPTION_CONFIG_REVISION,Tpbp_PackerAdd_ImageVersionInfoEncryption); 
}

//  Info  type 44
void Tpbp_TestTpbp_PackerAdd_CmiuInformation(void)
{
    S_CMIU_INFORMATION info;

     static uint8_t         expected[] = {
        E_TAG_NUMBER_CMIU_INFORMATION,
        E_TAG_TYPE_BYTE_RECORD,
        S_CMIU_INFORMATION_PACKED_SIZE,        
        0xEF,0xCD,0xAB,0x89,0x67,0x45,0x23,0x01,    // manu date
        0x99,0xCD,0xAB,0x89,0x67,0x45,0x23,0x02,    // install date
        0x55,0xCD,0xAB,0x89,0x67,0x45,0x23,0x03,    // last swipe date
        0xC9,                                       // mag swipe count
        0xAD, 0xDE,                                 // Battery 
        0x67,0x45,0x23,0x01,                        // time error
        };

    info.manufactureDate                      = 0x0123456789ABCDEF;
    info.installationDate                     = 0x0223456789ABCD99;
    info.dateLastMagSwipe                     = 0x0323456789ABCD55;
    info.magSwipes                            = 0xC9;
    info.batteryRemaining                     = 0xDEAD;
    info.timeErrorLastNetworkTimeAccess       = 0x01234567;

    // Add the data 
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_CmiuInformation(&packer, &info)); 
    
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


//  Image Name  type 46
void Tpbp_TestTpbp_PackerAdd_ImageName(void)
{
    const char* fileName = "myfile"; 
    static uint8_t         expected[] = {
        E_TAG_NUMBER_IMAGE,
        E_TAG_TYPE_EXTENDED_CHAR_ARRAY, 
        6, 0,      // length as 16 bit num
        'm','y','f','i','l','e' };

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_ImageName(&packer, fileName));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


//  Image Meta data  type 47
void Tpbp_TestTpbp_PackerAdd_ImageMetaData(void)
{
    S_IMAGE_METADATA md;

    static uint8_t         expected[] = {
        E_TAG_NUMBER_IMAGE_METADATA,
        E_TAG_TYPE_BYTE_RECORD, 
        S_IMAGE_METADATA_PACKED_SIZE,
        3,
        0x65, 0x87, 0x99, 0x77,
        0xDC, 0xAB, 0x34, 0x12
    };

    md.imageSize = 0x1234ABDC;
    md.imageType = (E_IMAGE_TYPE)3;
    md.startAddress = 0x77998765;
 
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_ImageMetadata(&packer, &md));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// Recording Reporting type 48
void Tpbp_TestTpbp_PackerAdd_RecordingReporting(void)
{
    S_RECORDING_REPORTING_INTERVAL rec;

    static uint8_t         expected[] = {
        E_TAG_NUMBER_RECORDING_REPORTING_INTERVAL,
        E_TAG_TYPE_BYTE_RECORD, 
        S_READING_RECORDING_PACKED_SIZE,
        0x41,0x40,
        3u,4,5,
        0x67,0xA5,
        0x68,0xA5,
        0x69,0xA5,
        6,
        1,
        0
        };
    rec.reportingStartMins              = 0x4041;
    rec.reportingIntervalHours          = 3;
    rec.reportingRetries                = 4;
    rec.reportingTransmitWindowsMins    = 5;
    rec.reportingQuietStartMins         = 0xA567;
    rec.reportingQuietEndMins           = 0xA568;
    rec.recordingStartMins              = 0xA569;
    rec.recordingIntervalMins           = 6;
    rec.recordingNumberAttachedDevices  = 1;
    rec.recordingEventMaskForDevice     = 0;

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_RecordingReportingInterval(&packer, &rec));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}

// Time and Date 50
void Tpbp_TestTpbp_PackerAdd_TimeDate(void)
{
   const uint64_t timeDate = 0x12345678AABBCCDD;

   static uint8_t         expected[] = {
        50, // Tag num
        E_TAG_TYPE_U64, 
        0xDD, 0xCC, 0xBB, 0xAA,
        0x78, 0x56, 0x34, 0x12
        };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_TimeAndDate(&packer, timeDate));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}

// Num 51
void Tpbp_TestTpbp_PackerAdd_ErrorCode(void)
{
    static uint8_t         expected[] = {
        51, // Tag num = E_TAG_NUMBER_ERROR_CODE
        E_TAG_TYPE_U16, 
        0x34, 0x12
     };

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_ErrorCode(&packer, 0x1234));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// E_TAG_NUMBER_DEBUG_DATA Num 52
void Tpbp_TestTpbp_PackerAdd_debugData(void)
{  
    const char* shortString = "abc";

    static uint8_t         expected[] = {
        52, // Tag num = E_TAG_NUMBER_DEBUG_DATA
        E_TAG_TYPE_EXTENDED_CHAR_ARRAY, 
        3, 0, // 16 bit extended length = strlen(shortString), ls first
        'a', 'b', 'c'
     };

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_DebugData( &packer, shortString));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// E_TAG_NUMBER_COMMAND_STARTDELAY Num 53
void Tpbp_TestTpbp_PackerAdd_StartDelay(void)
{
    uint64_t delayVal = 0x12345678AABBCCDD;
    static uint8_t         expected[] = {
        53,  // E_TAG_NUMBER_COMMAND_STARTDELAY
        E_TAG_TYPE_U64, 
        0xDD, 0xCC, 0xBB, 0xAA,
        0x78, 0x56, 0x34, 0x12
        };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Command_StartDelay(&packer, delayVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// E_TAG_NUMBER_COMMAND_DURATION Num 54
void Tpbp_TestTpbp_PackerAdd_Command_Duration(void)
{
    uint64_t val = 0x12345678AABBCCDD;
    static uint8_t         expected[] = {
        54,  // E_TAG_NUMBER_COMMAND_DURATION
        E_TAG_TYPE_U64, 
        0xDD, 0xCC, 0xBB, 0xAA,
        0x78, 0x56, 0x34, 0x12
        };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Command_Duration(&packer, val));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// S_COMMAND_READCONNECTEDEVICE Num 55
void Tpbp_TestTpbp_PackerAdd_Command_ReadConnectedDevice(void)
{
    const S_COMMAND_READCONNECTEDEVICE testVal = {0x01, 
        0x11223344AABBCCDD, 
        0x010203040A0B0C0D};

    static uint8_t         expected[] = {
        55,  // S_COMMAND_READCONNECTEDEVICE
        E_TAG_TYPE_BYTE_RECORD, 
        S_COMMAND_READCONNECTEDDEVICE_PACKED_SIZE,
        0x01,
        0xDD, 0xCC, 0xBB, 0xAA, 0x44, 0x33, 0x22, 0x11,
        0x0D, 0x0C, 0x0B, 0x0A, 0x04, 0x03, 0x02, 0x01,
        };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Command_ReadConnectedDevice(&packer, &testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}

//  Power Mode Num 56
void Tpbp_TestTpbp_PackerAdd_Command_PowerMode(void)
{
    const uint8_t testVal = 0xA5;

    static uint8_t         expected[] = {
        56,  // E_TAG_NUMBER_COMMAND_POWERMODE
        E_TAG_TYPE_BYTE, 
        0xA5
    };

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Command_PowerMode(&packer, testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// Tpbp_PackerAdd_Command_ReadMemory 57
void Tpbp_TestTpbp_PackerAdd_Command_ReadMemory(void)
{
    const S_COMMAND_MEMORY testVal = {0x8a, 0x54a01234, 0xabcd};

    static uint8_t         expected[] = {
        57,  // Tpbp_PackerAdd_Command_ReadMemory
        E_TAG_TYPE_BYTE_RECORD,
        S_COMMAND_MEMORY_PACKED_SIZE,
        0x8a,
        0x34, 0x12, 0xa0, 0x54,
        0xcd, 0xab
    };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Command_ReadMemory(&packer, &testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}

// LTE rf test mode 58
void Tpbp_TestTpbp_PackerAdd_Command_RFTestMode(void)
{
    const S_COMMAND_RFTESTMODE testVal = {1, 0x5678, 3};

    static uint8_t         expected[] = {
        58,  // rf test mode 
        E_TAG_TYPE_BYTE_RECORD,
        S_COMMAND_RFTESTMODE_PACKED_SIZE,
        1,
        0x78, 0x56,
        3
    };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Command_RFTestMode(&packer, &testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// 59
void Tpbp_TestTpbp_PackerAdd_Command_ToggleLTEModem(void)
{
    const uint8_t testVal = 0xA5;

    static uint8_t         expected[] = {
        59,  // E_TAG_NUMBER_COMMAND_POWERMODE
        E_TAG_TYPE_BYTE, 
        0xA5
    };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Command_ToggleLTEModem(&packer, testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// 60 & 61 Not applicable for CMIU
 

// 62
void Tpbp_TestPackerAdd_BatteryVoltage(void)
{  
    const uint16_t testVal = 0x1234;

    static uint8_t         expected[] = {
        62,  
        E_TAG_TYPE_U16,
        0x34, 0x12 
    };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_BatteryVoltage(&packer, testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}   


// 63
void Tpbp_TestPackerAdd_CmiuTemperature(void)
{    
    const uint8_t testVal = 0xAB;

    static uint8_t expected[] = {
        63,  
        E_TAG_TYPE_BYTE,
        0xAB 
    };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Cmiu_Temperature(&packer, (int8_t)testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}  

// 64
void Tpbp_TestPackerAdd_MemImage(void)
{    
    const uint16_t testVal = 0xABCD;

    static uint8_t expected[] = {
        64,  
        E_TAG_TYPE_U16,
        0xCD, 0xAB 
    };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_MemoryImageTag(&packer, testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}         

  // 65
void Tpbp_TestPackerAdd_PktType(void)
{    
    const uint8_t testVal = 0xAD;

    static uint8_t expected[] = {
        65,  
        E_TAG_TYPE_BYTE,
        0xAD 
    };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_PacketTypeTag(&packer, testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}     

// 66
void Tpbp_TestPackerAdd_Destination(void)
{   
    const uint8_t testVal = 0xDE;

    static uint8_t expected[] = {
        66,  
        E_TAG_TYPE_BYTE,
        0xDE 
    };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Destination(&packer, testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// 67
void Tpbp_TestPackerAdd_MetaDataForPacket(void)
{
    const uint64_t testStart    = 0x1234567890ABCDEF;
    const uint64_t testEnd      = 0x0123456789ABCDEF;

    static uint8_t expected[] = {
        67,  
        E_TAG_TYPE_U64_ARRAY,
        2, // num elements (of U64s)
        0xEF, 0xCD, 0xAB, 0x90, 0x78, 0x56, 0x34, 0x12,
        0xEF, 0xCD, 0xAB, 0x89, 0x67, 0x45, 0x23, 0x01,        
    };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_MetadataForPacket(&packer, testStart, testEnd));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected)); 
}


// 68
void Tpbp_TestPackerAdd_CmiuId(void)
{
    const uint32_t testVal = 0xDEAD1234;
    static uint8_t expected[] = {
        68,  
        E_TAG_TYPE_U32,
        0x34, 0x12, 0xAD, 0xDE 
    };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_CmiuId(&packer, testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}


// 69
void Tpbp_TestPackerAdd_Apn(void)
{
    const char* testVal = "myApn";
    char  testValLong[256 + 1]; 

    static uint8_t expected[] = {
        69,  
        E_TAG_TYPE_CHAR_ARRAY,
        5, // = strlen(testVal)- no null term
        'm','y','A','p','n'
    }; 
    
    // For APN >255, which is never going to happen, so that's why we test for it...
    static uint8_t expectedLong[1+1+2+256] = {
        69,  
        E_TAG_TYPE_EXTENDED_CHAR_ARRAY,
        0,1  // = 256 as uint16
        // +256 bytes of 'A' filled in below...
    };
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Apn(&packer, testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
 
    // 256 chars, plus a null term (which won't be placed in pkt)
    memset(testValLong, 'A', sizeof(testValLong));
    testValLong[256] = '\0';

    memset(&expectedLong[4], 'A', 256);

    Tpbp_PackerInit( &packer, packBuffer, BUFFER_SIZE); 
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Apn(&packer, testValLong));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expectedLong), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expectedLong, packBuffer, sizeof(expectedLong));
}


// 69
// For APN >255, which is never going to happen, so that's why we test for it...
void Tpbp_TestPackerAdd_ApnExtended(void)
{
    char  testValLong[256 + 1]; 
      
    static uint8_t expectedLong[1+1+2+256] = {
        69,  
        E_TAG_TYPE_EXTENDED_CHAR_ARRAY,
        0,1  // = 256 as uint16
        // +256 bytes of 'A' filled in below...
    };
     
    // 256 chars, plus a null term (which won't be placed in pkt)
    memset(testValLong, 'A', sizeof(testValLong));
    testValLong[256] = '\0';

    memset(&expectedLong[4], 'A', 256); 
    
    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Apn(&packer, testValLong));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expectedLong), Tpbp_PackerGetCount(&packer));    
    TEST_ASSERT_EQUAL_U8_ARRAY(expectedLong, packBuffer, sizeof(expectedLong));
}


// BTLE rf test mode 70
void Tpbp_TestTpbp_PackerAdd_Command_Btle_RFTestMode(void)
{
    const S_COMMAND_BTLE_RFTESTMODE testVal = { 1, 7, 20, 3 };

    static uint8_t         expected[] = {
        70,  // BTLE rf test mode 
        E_TAG_TYPE_BYTE_RECORD,
        S_COMMAND_RFTESTMODE_BTLE_PACKED_SIZE,
        1,
        7,
        20,
        3
    };

    TEST_ASSERT_TRUE(Tpbp_PackerAdd_Command_Btle_RFTestMode(&packer, &testVal));
    TEST_ASSERT_EQUAL_UINT32(sizeof(expected), Tpbp_PackerGetCount(&packer));
    TEST_ASSERT_EQUAL_U8_ARRAY(expected, packBuffer, sizeof(expected));
}

void Tpbp_TestPackerAdd_NewImageVersion(void)
{
    Tpbp_TestTpbp_StringBasedTagsHelper(Tpbp_PackerAdd_NewImageVersion, (E_TAG_NUMBER)71);
}
