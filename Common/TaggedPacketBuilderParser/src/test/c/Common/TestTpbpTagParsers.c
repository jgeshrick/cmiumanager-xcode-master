/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup TpbpUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief *** Parsers for specific tags ***  
 * @author Duncan Willis
 * @date 2015.10.29
 * @version 1.0
 */


#include <stdint.h>
#include <stdio.h>
#include "CommonTypes.h"
#include "unity.h"
#include "Tpbp.h"
#include "TpbpPackerHelpers.h"
#include "TpbpParserHelpers.h"
#include "TestTpbp.h"

extern uint8_t         packBuffer[];
extern uint8_t         parseBuffer[];
extern S_TPBP_PACKER   packer;
extern S_TPBP_PARSER   parser; 



// tag num 1
void Tpbp_TestParserRead_CmiuPacketHeader(void)
{
    bool            isOk = false;
    uint32_t        bytesRemaining = 0;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};

    // Populate a packet with some values for testing
    S_CMIU_PACKET_HEADER CmiuPkt;
 
    S_CMIU_PACKET_HEADER CmiuPktRead = {0};
    CmiuPkt.cellularRssiAndBer = 0x1234;
    CmiuPkt.cmiuId = 0x76543210;
    CmiuPkt.encryptionMethod = 2;
    CmiuPkt.keyInfo = 3;
    CmiuPkt.networkFlags = 0x04;
    CmiuPkt.sequenceNumber = 0x05;
    CmiuPkt.timeAndDate = 0x12345678ABCDEF01;
    CmiuPkt.tokenAesCrc= 0x06;

    // Create a packet with 2 packets
    Tpbp_PackerInit( &packer, packBuffer, BUFFER_SIZE);   

    // Add a couple of pkts to buffer 
    isOk = Tpbp_PackerAdd_CmiuPacketHeader(&packer, &CmiuPkt);
    TEST_ASSERT_TRUE(isOk); 
    isOk = Tpbp_PackerAdd_CmiuPacketHeader(&packer, &CmiuPkt);   
    TEST_ASSERT_TRUE(isOk);

    Tpbp_ParserInit( &parser, packBuffer, BUFFER_SIZE);
    
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    isOk = Tpbp_ParserRead_CmiuPacketHeader(&parser, &CmiuPktRead);    
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_EQUAL_UINT16(CmiuPkt.cellularRssiAndBer,    CmiuPktRead.cellularRssiAndBer);
    TEST_ASSERT_EQUAL_UINT32(CmiuPkt.cmiuId,                CmiuPktRead.cmiuId);
    TEST_ASSERT_EQUAL_UINT8(CmiuPkt.encryptionMethod,       CmiuPktRead.encryptionMethod);
    TEST_ASSERT_EQUAL_UINT8(CmiuPkt.keyInfo,                CmiuPktRead.keyInfo);
    TEST_ASSERT_EQUAL_UINT8(CmiuPkt.networkFlags,           CmiuPktRead.networkFlags);
    TEST_ASSERT_EQUAL_UINT8(CmiuPkt.sequenceNumber,         CmiuPktRead.sequenceNumber);
    TEST_ASSERT_TRUE(CmiuPkt.timeAndDate ==                 CmiuPktRead.timeAndDate);
    TEST_ASSERT_EQUAL_UINT8(CmiuPkt.tokenAesCrc,            CmiuPktRead.tokenAesCrc);
   
     // Read tag and 2nd pkt
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    isOk = Tpbp_ParserRead_CmiuPacketHeader(&parser, &CmiuPktRead);    
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_EQUAL_UINT16(CmiuPkt.cellularRssiAndBer,    CmiuPktRead.cellularRssiAndBer);
    TEST_ASSERT_EQUAL_UINT32(CmiuPkt.cmiuId,                CmiuPktRead.cmiuId);
    TEST_ASSERT_EQUAL_UINT8(CmiuPkt.encryptionMethod,       CmiuPktRead.encryptionMethod);
    TEST_ASSERT_EQUAL_UINT8(CmiuPkt.keyInfo,                CmiuPktRead.keyInfo);
    TEST_ASSERT_EQUAL_UINT8(CmiuPkt.networkFlags,           CmiuPktRead.networkFlags);
    TEST_ASSERT_EQUAL_UINT8(CmiuPkt.sequenceNumber,         CmiuPktRead.sequenceNumber);
    TEST_ASSERT_TRUE(CmiuPkt.timeAndDate ==                 CmiuPktRead.timeAndDate);
    TEST_ASSERT_EQUAL_UINT8(CmiuPkt.tokenAesCrc,            CmiuPktRead.tokenAesCrc);
 
    // Should now be empty 
    bytesRemaining = Tpbp_ParserGetBytesRemaining(&parser);
    TEST_ASSERT_EQUAL_UINT32(BUFFER_SIZE - 2 * ( 3 + S_CMIU_PACKET_HEADER_PACKED_SIZE),
                                bytesRemaining);
}


// tag num 2
void Tpbp_TestParserRead_CmiuConfiguration(void)
{
    S_TPBP_PARSER   parser;
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
 
    S_CMIU_CONFIGURATION ts;
    S_CMIU_CONFIGURATION cmiuPktRead;
 
    ts.cmiuType                             = 0x02;	
    ts.hwRevision                           = 0x1234;
    ts.fwRelease                            = 0x1235u;
    ts.fwRevDate                            = 0x12345678u;
    ts.fwBuildNum                           = 0xfedcba98u;
    ts.blRelease                            = 0x2225u;
    ts.blRevDate                            = 0x22242628u;
    ts.blBuildNum                           = 0x2e2c2a28u;
    ts.configRelease                        = 0xF2F5u;
    ts.configRevDate                        = 0xF2F4F6F8u;
    ts.configBuildNum                       = 0xFeFcFaF8u;

    ts.manufactureDate                      = 0x0123456789ABCDEF;
    ts.initialCallInTime                    = 0x0123;	
    ts.callInInterval                       = 0x06;
    ts.callInWindow                         = 0xC4;
    ts.reportingwindowNRetries              = 0xF4;
    ts.quietTimeStartMins                   = 0xCDEF;
    ts.quietTimeEndMins                     = 0xCBFE;   
    ts.numAttachedDevices                   = 0x05;
    ts.installationDate                     = 0x0123456789ABCD99;
    ts.dateLastMagSwipe                     = 0x0123456789ABCD55;
    ts.magSwipes                            = 0xC9;
    ts.batteryRemaining                     = 0xDEAD;
    ts.timeErrorLastNetworkTimeAccess       = 0x7E;
   
    // Add the data 
    isOk = Tpbp_PackerAdd_CmiuConfiguration(&packer, &ts);
    TEST_ASSERT_TRUE(isOk);

    // Init a parser
    Tpbp_ParserInit( &parser, packBuffer, BUFFER_SIZE);
    

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
        TEST_ASSERT_TRUE(isOk);

    isOk = Tpbp_ParserRead_CmiuConfiguration(&parser, &cmiuPktRead);    
    TEST_ASSERT_TRUE(isOk);
    
    TEST_ASSERT_TRUE(ts.cmiuType                             == cmiuPktRead.cmiuType);	
    TEST_ASSERT_TRUE(ts.hwRevision                           == cmiuPktRead.hwRevision);

    TEST_ASSERT_EQUAL_UINT16(0x1235u, ts.fwRelease);
    TEST_ASSERT_EQUAL_UINT32(0x12345678u, ts.fwRevDate);
    TEST_ASSERT_EQUAL_UINT32(0xfedcba98u, ts.fwBuildNum);

    TEST_ASSERT_EQUAL_UINT16(0x2225u, ts.blRelease);
    TEST_ASSERT_EQUAL_UINT32(0x22242628u, ts.blRevDate);
    TEST_ASSERT_EQUAL_UINT32(0x2e2c2a28u, ts.blBuildNum);

    TEST_ASSERT_EQUAL_UINT16(0xF2F5u, ts.configRelease);
    TEST_ASSERT_EQUAL_UINT32(0xF2F4F6F8u, ts.configRevDate);
    TEST_ASSERT_EQUAL_UINT32(0xFeFcFaF8u, ts.configBuildNum);

    TEST_ASSERT_TRUE(ts.manufactureDate                      == cmiuPktRead.manufactureDate);
    TEST_ASSERT_TRUE(ts.initialCallInTime                    == cmiuPktRead.initialCallInTime);
    TEST_ASSERT_TRUE(ts.callInInterval                       == cmiuPktRead.callInInterval);
    TEST_ASSERT_TRUE(ts.callInWindow                         == cmiuPktRead.callInWindow);
    TEST_ASSERT_TRUE(ts.reportingwindowNRetries              == cmiuPktRead.reportingwindowNRetries);
    TEST_ASSERT_TRUE(ts.quietTimeStartMins                   == cmiuPktRead.quietTimeStartMins);
    TEST_ASSERT_TRUE(ts.quietTimeEndMins                     == cmiuPktRead.quietTimeEndMins);  
    TEST_ASSERT_TRUE(ts.numAttachedDevices                   == cmiuPktRead.numAttachedDevices);
    TEST_ASSERT_TRUE(ts.installationDate                     == cmiuPktRead.installationDate);
    TEST_ASSERT_TRUE(ts.dateLastMagSwipe                     == cmiuPktRead.dateLastMagSwipe);
    TEST_ASSERT_TRUE(ts.magSwipes                            == cmiuPktRead.magSwipes);
    TEST_ASSERT_TRUE(ts.batteryRemaining                     == cmiuPktRead.batteryRemaining);
    TEST_ASSERT_TRUE(ts.timeErrorLastNetworkTimeAccess       == cmiuPktRead.timeErrorLastNetworkTimeAccess);
}

// 10
void Tpbp_TestParserRead_FtpPortNumber(void)
{
    bool            isOk = false;
    S_TPBP_TAG      tagRead = { (E_TAG_NUMBER)0 };
    uint16_t        value;
    const static uint8_t parseBufferLocal[] = {
        10,
        E_TAG_TYPE_U16,
        0x98, 0x76,
    };

    Tpbp_ParserInit(&parser, parseBufferLocal, sizeof(parseBufferLocal));

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_BATTERY_VOLTAGE == tagRead.tagNumber);
    TEST_ASSERT_TRUE(E_TAG_TYPE_U16 == tagRead.tagType);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_FtpPortNumber(&parser, &value));
    TEST_ASSERT_EQUAL_UINT16(0x7698, value);
}

// Tag num 23
void Tpbp_TestParserRead_CmiuDiagnostics(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    S_CMIU_DIAGNOSTICS diagnostics;
    const static uint8_t parseBufferLocal[] = {
        E_TAG_NUMBER_CMIU_HW_DIAGNOSTICS,
        E_TAG_TYPE_BYTE_RECORD, 
        S_CMIU_DIAGNOSTICS_PACKED_SIZE,
        48, 0, 0, 0,
        49
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_CmiuDiagnostics(&parser, &diagnostics));

    TEST_ASSERT_EQUAL_UINT32(48u, diagnostics.diagnosticsResult);
    TEST_ASSERT_EQUAL_UINT8(49u, diagnostics.processorResetCounter);
}


// Tag num 26
void Tpbp_TestParserRead_CmiuStatus(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    S_CMIU_STATUS   status;
    const static uint8_t parseBufferLocal[] = {
        E_TAG_NUMBER_DEVICE_CONFIGURATION,
        E_TAG_TYPE_BYTE_RECORD, 
        S_CMIU_STATUS_PACKED_SIZE,
        0xCC, 
        0x09,
        0x23, 0x01
        };

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_CmiuStatus(&parser, &status));
    TEST_ASSERT_EQUAL_UINT8(0xCC,               status.cmiuType);  
    TEST_ASSERT_EQUAL_UINT8(0x09,               status.numberOfDevices);  
    TEST_ASSERT_EQUAL_UINT16(0x0123,            status.cmiuFlags); 
}


// Tag num 27
void Tpbp_TestParserRead_ReportedDeviceConfig(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    S_REPORTED_DEVICE_CONFIG cnf;
    const static uint8_t parseBufferLocal[] = {
        E_TAG_NUMBER_DEVICE_CONFIGURATION,
        E_TAG_TYPE_BYTE_RECORD, 
        S_REPORTED_DEVICE_CONFIG_PACKED_SIZE,
        0xDD, 
        0xFF, 0xCD, 0xAB, 0x89, 0x67, 0x45, 0x23, 0x01,
        0x23, 0x01,
        0x52, 0xA4, 0x00, 0x7E,
        0x98, 0x76
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_ReportedDeviceConfig(&parser, &cnf));
    TEST_ASSERT_EQUAL_UINT8(0xDD,               cnf.deviceNumber);    
    TEST_ASSERT_TRUE(0x0123456789ABCDFFull ==   cnf.attachedDeviceId);
    TEST_ASSERT_EQUAL_UINT16(0x0123,            cnf.deviceType);
    TEST_ASSERT_EQUAL_UINT16(0x7E00A452,		cnf.currentDeviceData);
    TEST_ASSERT_EQUAL_UINT16(0x7698,            cnf.deviceFlags);
}


// tag num 31
void Tpbp_TestParserRead_ConnectionTimingLog(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    S_CONNECTION_TIMING_LOG timingLog;
    const static uint8_t parseBufferLocal[] = {
        E_TAG_NUMBER_CONNECTION_TIMING_LOG,
        E_TAG_TYPE_BYTE_RECORD, 
        S_CONNECTION_TIMING_LOG_PACKED_SIZE,
        0xEF, 0xCD, 0xAB, 0x89, 0x67, 0x45, 0x23, 0x01,
        53, 0, 54, 0, 55, 0, 56, 0, 57, 0
        };

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    isOk = Tpbp_ParserRead_ConnectionTimingLog(&parser, &timingLog);    
    TEST_ASSERT_TRUE(isOk);
    
    TEST_ASSERT_EQUAL_UINT32(0x89ABCDEFu, (uint32_t)timingLog.dateOfConnection);
    TEST_ASSERT_EQUAL_UINT32(0x01234567u, (uint32_t)(timingLog.dateOfConnection >> 32));

    TEST_ASSERT_EQUAL_UINT16(53u, timingLog.timeFromPowerOnToNetworkRegistration);
    TEST_ASSERT_EQUAL_UINT16(54u, timingLog.timeFromNetworkRegistrationToContextActivation);
    TEST_ASSERT_EQUAL_UINT16(55u, timingLog.timeFromContextActivationToServerConnection);
    TEST_ASSERT_EQUAL_UINT16(56u, timingLog.timeToTransferMessageToServer);
    TEST_ASSERT_EQUAL_UINT16(57u, timingLog.timeToDisconnectAndShutdown);
}


// Tag num 33
void Tpbp_TestParserRead_CmiuBasicConfiguration(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    S_CMIU_BASIC_CONFIGURATION bc;

    const static uint8_t parseBufferLocal[] = {
        E_TAG_NUMBER_CMIU_BASIC_CONFIGURATION,
        E_TAG_TYPE_BYTE_RECORD, 
        S_CMIU_BASIC_CONFIGURATION_PACKED_SIZE,
        0x2E, 0x2D, 
        0x3d,
        0x4d,
        0x5d,
        0xEE, 0xFF,
        0xAA, 0xA5,
        3,
        0xAB
        };
    

    Tpbp_ParserInit(&parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_CmiuBasicConfiguration(&parser, &bc));
    
    TEST_ASSERT_EQUAL_UINT16(0x2D2E,            bc.initialCallInTime);
    TEST_ASSERT_EQUAL_UINT8(0x3D,               bc.callInInterval);
    TEST_ASSERT_EQUAL_UINT8(0x4D,               bc.callInWindow);
    TEST_ASSERT_EQUAL_UINT8(0x5D,               bc.reportingwindowNRetries);
    TEST_ASSERT_EQUAL_UINT16(0xFFEE,            bc.quietTimeStartMins);
    TEST_ASSERT_EQUAL_UINT16(0xA5AA,            bc.quietTimeEndMins);    
    TEST_ASSERT_EQUAL_UINT8(3,                  bc.numAttachedDevices);
    TEST_ASSERT_EQUAL_UINT8(0xAB,               bc.eventMask);
}

// Tag num 34
void Tpbp_TestParserRead_IntervalRecordingConfig(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    S_INTERVAL_RECORDING_CONFIG cnf;
    const static uint8_t parseBufferLocal[] = {
        E_TAG_NUMBER_INTERVAL_RECORDING,
        E_TAG_TYPE_BYTE_RECORD, 
        S_INTERVAL_RECORDING_CONFIG_PACKED_SIZE,
        0xDD, 
        0x03,
        0x01,
        0x02,
        0x98, 0x76,
        0xEF, 0xCD, 0xAB, 0x89, 0x67, 0x45, 0x23, 0x01,
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_IntervalRecordingConfig(&parser, &cnf));
    
    TEST_ASSERT_EQUAL_UINT8(0xDD,           cnf.deviceNumber);
    TEST_ASSERT_EQUAL_UINT8(0x03,           cnf.intervalFormat);
    TEST_ASSERT_EQUAL_UINT8(0x01,           cnf.recordingIntervalMin);
    TEST_ASSERT_EQUAL_UINT8(0x02,           cnf.reportingIntervalHrs);
    TEST_ASSERT_EQUAL_UINT16(0x7698,        cnf.intervalStartTime);    
    TEST_ASSERT_TRUE(0x0123456789ABCDEFull == cnf.intervalLastRecordDateTime);
}


// Tag num 35
void Tpbp_TestParserRead_UartPacketHeader(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    S_MQTT_BLE_UART bl;
    const static uint8_t parseBufferLocal[] = {
        E_TAG_NUMBER_UART_PACKET_HEADER,
        E_TAG_TYPE_BYTE_RECORD, 
        S_MQTT_BLE_UART_PACKED_SIZE,
        0x2D, 
        0x3d,
        0x4d,
        0x5d
        };

    Tpbp_ParserInit(&parser, parseBufferLocal, sizeof(parseBufferLocal));   

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_UartPacketHeader(&parser, &bl));
    
    TEST_ASSERT_EQUAL_UINT8(0x2D,           bl.sequenceNumber);
    TEST_ASSERT_EQUAL_UINT8(0x3D,           bl.keyInfo);
    TEST_ASSERT_EQUAL_UINT8(0x4D,           bl.encryptionMethod);
    TEST_ASSERT_EQUAL_UINT8(0x5D,           bl.token);
}


// Revision Parser - helper for all similar tags  
// Rev has no length field, fixed 10 byte array.
static void Tpbp_TestTpbp_ParserRead_RevisionHelper(uint8_t tagNumber )
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
 
    S_IMAGE_VERSION_INFO rev;

    static uint8_t parseBufferLocal[] = {
        0,
        E_TAG_TYPE_IMAGE_VERSION_INFO,
        0x01,
        0x02,
        0x15, 0x06,0x21,0x0,
        0x03,0x12, 0x34, 0x56};

    parseBufferLocal[0] = tagNumber;
 
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
     
    TEST_ASSERT_TRUE(Tpbp_ParserRead_ImageVersionInfo(&parser, &rev));
    
    TEST_ASSERT_EQUAL_UINT8(0x01,               rev.versionMajorBcd);
    TEST_ASSERT_EQUAL_UINT8(0x02,               rev.versionMinorBcd);
    TEST_ASSERT_EQUAL_UINT32(0x15062100,        rev.versionYearMonthDayBcd);
    TEST_ASSERT_EQUAL_UINT32(0x03123456,        rev.versionBuildBcd);
}


//  HW Revision  Tag Number 38  
void Tpbp_TestParserRead_RevisionHardware(void)
{   
    Tpbp_TestTpbp_ParserRead_RevisionHelper(E_TAG_NUMBER_HARDWARE_REVISION); 
}

// Tag Number 39
void Tpbp_TestParserRead_RevisionFirmware(void)
{   
    Tpbp_TestTpbp_ParserRead_RevisionHelper(E_TAG_NUMBER_FIRMWARE_REVISION); 
}

// Tag Number 40
void Tpbp_TestParserRead_RevisionBootloader(void)
{   
    Tpbp_TestTpbp_ParserRead_RevisionHelper(E_TAG_NUMBER_BOOTLOADER_REVISION); 
}

// Tag Number 41
void Tpbp_TestParserRead_RevisionConfig(void)
{   
    Tpbp_TestTpbp_ParserRead_RevisionHelper(E_TAG_NUMBER_CONFIG_REVISION); 
}

// Tag Number 42
void Tpbp_TestParserRead_RevisionArb(void)
{   
    Tpbp_TestTpbp_ParserRead_RevisionHelper(E_TAG_NUMBER_ARB_REVISION); 
}

// Tag Number 43
void Tpbp_TestParserRead_RevisionBle(void)
{   
    Tpbp_TestTpbp_ParserRead_RevisionHelper(E_TAG_NUMBER_BLE_CONFIG_REVISION); 
}

// Tag Number 49
void Tpbp_TestParserRead_RevisionEncryption(void)
{   
    Tpbp_TestTpbp_ParserRead_RevisionHelper(E_TAG_NUMBER_ENCRYPTION_CONFIG_REVISION); 
} 


// Info Tag Number 44
void Tpbp_TestParserRead_CmiuInformation(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_CMIU_INFORMATION info;
    
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};

     static const uint8_t         parseBufferLocal[] = {
        E_TAG_NUMBER_CMIU_INFORMATION,
        E_TAG_TYPE_BYTE_RECORD,
        S_CMIU_INFORMATION_PACKED_SIZE,        
        0xEF,0xCD,0xAB,0x89,0x67,0x45,0x23,0x01,    // manu date
        0x99,0xCD,0xAB,0x89,0x67,0x45,0x23,0x02,    // install date
        0x55,0xCD,0xAB,0x89,0x67,0x45,0x23,0x03,    // last swipe date
        0xC9,                                       // mag swipe count
        0xAD, 0xDE,                                 // Battery 
        0x67,0x45,0x23,0x01,                        // time error
        };


    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
     
    TEST_ASSERT_TRUE(Tpbp_ParserRead_CmiuInformation(&parser, &info));
   
    TEST_ASSERT_TRUE(info.manufactureDate                      == 0x0123456789ABCDEF);
    TEST_ASSERT_TRUE(info.installationDate                     == 0x0223456789ABCD99);
    TEST_ASSERT_TRUE(info.dateLastMagSwipe                     == 0x0323456789ABCD55);
    TEST_ASSERT_TRUE(info.magSwipes                            == 0xC9);
    TEST_ASSERT_TRUE(info.batteryRemaining                     == 0xDEAD);
    TEST_ASSERT_TRUE(info.timeErrorLastNetworkTimeAccess       == 0x01234567);
}


// Tag num 46
void Tpbp_TestParserRead_ImageName(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    char fileName[20] = {0};
    char fileNameAbc[] = "abc";

    const static uint8_t parseBufferLocal[] = {
        E_TAG_NUMBER_IMAGE,
        E_TAG_TYPE_EXTENDED_CHAR_ARRAY, 
        0x3, 0x0,
        'a', 'b', 'c'};

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    fileName[3] = 'z'; // should be overwritten with null terminator
    fileName[4] = 'x'; // should NOT be overwritten
    TEST_ASSERT_TRUE(Tpbp_ParserRead_ImageName(&parser, &tagRead, fileName, sizeof(fileName)));
    
    TEST_ASSERT_EQUAL_U8_ARRAY(fileNameAbc,         fileName, 3);
    TEST_ASSERT_EQUAL_UINT8(0,                      fileName[3]);
    TEST_ASSERT_EQUAL_UINT8('x',                    fileName[4]);
    
    // Repeat test, this time with output buffer too small
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
  
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    // Buffer too small - should return false.
    TEST_ASSERT_FALSE(Tpbp_ParserRead_ImageName(&parser, &tagRead, fileName, 2));  
}


// Tag num 47
void Tpbp_TestParserRead_ImageMetaData(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    S_IMAGE_METADATA md;
    const static uint8_t parseBufferLocal[] = {
        47,  //Tag num 47
        E_TAG_TYPE_BYTE_RECORD, 
        S_IMAGE_METADATA_PACKED_SIZE,
        0x03,
        0x01,0x2,0x3,0x4,
        0x11,0x22,0x33,0x44
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_ImageMetadata(&parser, &md));
    
    TEST_ASSERT_EQUAL_UINT8(0x03,           md.imageType); 
    TEST_ASSERT_EQUAL_UINT32(0x04030201,    md.startAddress);
    TEST_ASSERT_EQUAL_UINT32(0x44332211,    md.imageSize);
}


// Tag num 48
void Tpbp_TestParserRead_RecordingReporting(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    S_RECORDING_REPORTING_INTERVAL rec;

    const static uint8_t parseBufferLocal[] = {
        48,  //Tag num 48
        E_TAG_TYPE_BYTE_RECORD, 
        S_READING_RECORDING_PACKED_SIZE,
        0x41,0x40,
        3u,4,5,
        0x67,0xA5,
        0x68,0xA5,
        0x69,0xA5,
        6,
        1,
        0
        };

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_RecordingReportingInterval(&parser, &rec));
    
    TEST_ASSERT_EQUAL_UINT16(0x4041,        rec.reportingStartMins); 
    TEST_ASSERT_EQUAL_UINT8(0x03,           rec.reportingIntervalHours);
    TEST_ASSERT_EQUAL_UINT8(0x04,           rec.reportingRetries);
    TEST_ASSERT_EQUAL_UINT8(0x05,           rec.reportingTransmitWindowsMins);
    TEST_ASSERT_EQUAL_UINT16(0xA567,        rec.reportingQuietStartMins);
    TEST_ASSERT_EQUAL_UINT8(0xA568,         rec.reportingQuietEndMins);
    TEST_ASSERT_EQUAL_UINT16(0xA569,        rec.recordingStartMins);     
    TEST_ASSERT_EQUAL_UINT8(0x6,            rec.recordingIntervalMins);
    TEST_ASSERT_EQUAL_UINT8(0x1,            rec.recordingNumberAttachedDevices);
    TEST_ASSERT_EQUAL_UINT8(0x0,            rec.recordingEventMaskForDevice);
}

 
void Tpbp_TestParserRead_Time(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    uint64_t time = 0; 

    const static uint8_t parseBufferLocal[] = {
        0xDD, 0xCC, 0xBB, 0xAA,
        0x44, 0x33, 0x22, 0x11
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserRead_Time(&parser, &time);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(0x11223344AABBCCDD == time);
}


// 51
void Tpbp_TestParserRead_ErrorCode(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    uint16_t        val = 0;

    const static uint8_t parseBufferLocal[] = {
        51, // Tag num = E_TAG_NUMBER_ERROR_CODE
        E_TAG_TYPE_U16, 
        0x34, 0x12
        };

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_ErrorCode(&parser, &val));
    TEST_ASSERT_EQUAL_UINT16(0x1234, val);
}


// 53
void Tpbp_TestParserRead_StartDelay(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    uint64_t        val = 0;

    const static uint8_t parseBufferLocal[] = {
        53,  //Tag num 53
        E_TAG_TYPE_U64, 
        0xDD, 0xCC, 0xBB, 0xAA,
        0x78, 0x56, 0x34, 0x12
        };

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_Command_StartDelay(&parser, &val));
    TEST_ASSERT_TRUE(0x12345678AABBCCDD == val);
}


// 54
void Tpbp_TestParserRead_Command_Duration(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    uint64_t        val = 0;

    const static uint8_t parseBufferLocal[] = {
        54,  //Tag num 54
        E_TAG_TYPE_U64, 
        0xDD, 0xCC, 0xBB, 0xAA,
        0x78, 0x56, 0x34, 0x12
        };

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_Command_Duration(&parser, &val));
    TEST_ASSERT_TRUE(0x12345678AABBCCDD == val);
}


// 55
void Tpbp_TestParserRead_Command_ReadConnectedDevice(void)
    {
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    S_COMMAND_READCONNECTEDEVICE rec;

    const static uint8_t parseBufferLocal[] = {
        55,  //Tag num 55
        E_TAG_TYPE_BYTE_RECORD, 
        S_COMMAND_READCONNECTEDDEVICE_PACKED_SIZE,
        0x03,
        0xDD, 0xCC, 0xBB, 0xAA, 0x44, 0x33, 0x22, 0x11,
        0x0D, 0x0C, 0x0B, 0x0A, 0x04, 0x03, 0x02, 0x01,
        };

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_Command_ReadConnectedDevice(&parser, &rec));
    
    TEST_ASSERT_EQUAL_UINT8(0x03,           rec.pulse);
    
    TEST_ASSERT_TRUE(0x11223344AABBCCDD == rec.delayBetweenReadsMsec);
    TEST_ASSERT_TRUE(0x010203040A0B0C0D == rec.numberOfRepeats);
}

// 56
void Tpbp_TestParserRead_Command_PowerMode(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    uint8_t         val = 0;

    const static uint8_t parseBufferLocal[] = {
        56,
        E_TAG_TYPE_BYTE, 
         0x12
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_Command_PowerMode(&parser, &val));
    TEST_ASSERT_EQUAL_UINT8(0x12, val);
}


// 57
void Tpbp_TestParserRead_Command_ReadMemory(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    S_COMMAND_MEMORY rec;

    const static uint8_t parseBufferLocal[] = {
        57,  //Tag num 57
        E_TAG_TYPE_BYTE_RECORD, 
        S_COMMAND_MEMORY_PACKED_SIZE,
        0x93,
        0x41,0x40,0x34, 0xBF,
        0xAA,0xBB,
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_TRUE(Tpbp_ParserRead_Command_ReadMemory(&parser, &rec));
    TEST_ASSERT_EQUAL_UINT16(0x93, rec.memoryDesignator);
    TEST_ASSERT_EQUAL_UINT16(0xBF344041, rec.startAddress);
    TEST_ASSERT_EQUAL_UINT16(0xBBAA, rec.numberOfBytes);
}


// 58
void Tpbp_TestParserRead_Command_RFTestMode(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    S_COMMAND_RFTESTMODE rec;

    const static uint8_t parseBufferLocal[] = {
        58,  //Tag num 58
        E_TAG_TYPE_BYTE_RECORD, 
        S_COMMAND_RFTESTMODE_PACKED_SIZE,
        1,
        0x34, 0x12,
        3
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
    
    TEST_ASSERT_TRUE(58                     ==    tagRead.tagNumber);
    TEST_ASSERT_TRUE(E_TAG_TYPE_BYTE_RECORD ==    tagRead.tagType);
    TEST_ASSERT_TRUE(1+2+1                  ==    tagRead.dataSize);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_Command_RFTestMode(&parser, &rec));
    TEST_ASSERT_EQUAL_UINT8(1,  rec.rfBand);
    TEST_ASSERT_EQUAL_UINT8(0x1234, rec.rfChannel);
    TEST_ASSERT_EQUAL_UINT8(3,  rec.rfPowerLevel);
}


// 59
void Tpbp_TestParserRead_Command_ToggleLTEModem(void)
{
    S_TPBP_PARSER   parser; 
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    uint8_t         val = 0;

    const static uint8_t parseBufferLocal[] = {
        56,
        E_TAG_TYPE_BYTE, 
         0xAC
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_Command_ToggleLTEModem(&parser, &val));
    TEST_ASSERT_EQUAL_UINT8(0xAC, val);
}


// 62
void Tpbp_TestParserRead_BatteryVoltage(void)
{
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    uint16_t        value;
    const static uint8_t parseBufferLocal[] = {
        62,
        E_TAG_TYPE_U16,
        0x98, 0x76,
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_BATTERY_VOLTAGE == tagRead.tagNumber );
    TEST_ASSERT_TRUE(E_TAG_TYPE_U16 == tagRead.tagType);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_BatteryVoltage(&parser, &value));
    TEST_ASSERT_EQUAL_UINT16(0x7698,        value);    
}

void Tpbp_TestParserRead_CmiuTemperature(void)
{
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    uint8_t         value;
    const static uint8_t parseBufferLocal[] = {
        63,
        E_TAG_TYPE_BYTE,
        0xBA,
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CMIU_TEMPERATURE == tagRead.tagNumber );
    TEST_ASSERT_TRUE(E_TAG_TYPE_BYTE == tagRead.tagType);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_CmiuTemperature(&parser, &value));
    TEST_ASSERT_EQUAL_UINT8(0xBA,        value);  
}


// 64
void Tpbp_TestParserRead_MemoryImageTag(void)
{
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    uint16_t        value;
    const static uint8_t parseBufferLocal[] = {
        64,
        E_TAG_TYPE_U16,
        0x98, 0x76,
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_MEMORY_IMAGE == tagRead.tagNumber );
    TEST_ASSERT_TRUE(E_TAG_TYPE_U16 == tagRead.tagType);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_MemoryImageTag(&parser, &value));
    TEST_ASSERT_EQUAL_UINT16(0x7698,        value);
}


// 65
void Tpbp_TestParserRead_PacketTypeTag(void)
{
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    uint8_t         value;
    const static uint8_t parseBufferLocal[] = {
        65,
        E_TAG_TYPE_BYTE,
        0xCA,
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_PACKET_TYPE == tagRead.tagNumber );
    TEST_ASSERT_TRUE(E_TAG_TYPE_BYTE == tagRead.tagType);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_Command_PacketType(&parser, &value));
    TEST_ASSERT_EQUAL_UINT8(0xCA,        value);  
}


// 66
void Tpbp_TestParserRead_Destination(void)
{
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    uint8_t         value;
    const static uint8_t parseBufferLocal[] = {
        66,
        E_TAG_TYPE_BYTE,
        0xDE,
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_DESTINATION == tagRead.tagNumber );
    TEST_ASSERT_TRUE(E_TAG_TYPE_BYTE == tagRead.tagType);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_Command_Destination(&parser, &value));
    TEST_ASSERT_EQUAL_UINT8(0xDE,        value);
}


// 67
void Tpbp_TestParserRead_MetadataForPacket(void)
{
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    uint64_t        startValue;
    uint64_t        endValue;
    const static uint8_t parseBufferLocal[] = {
        67,
        E_TAG_TYPE_U64_ARRAY,
        2, //  8 + 8,                  // num bytes following
        0xFF, 0xFF, 0xEE, 0xEE, 0xDD, 0xDD, 0xCC, 0xCC,
        0xFF, 0xFF, 0xEE, 0xEE, 0xDD, 0xDD, 0xCC, 0xC0,
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_META_DATA_PACKET_REQUEST == tagRead.tagNumber );
    TEST_ASSERT_TRUE(E_TAG_TYPE_U64_ARRAY == tagRead.tagType);
    TEST_ASSERT_TRUE(16 == tagRead.dataSize);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_MetadataForPacket(&parser, &startValue, & endValue));
    TEST_ASSERT_TRUE(0xCCCCDDDDEEEEFFFF ==     startValue);    
    TEST_ASSERT_TRUE(0xC0CCDDDDEEEEFFFF ==     endValue);
}


// 68
void Tpbp_TestParserRead_CmiuId(void)
{    
    bool            isOk = false;
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    uint32_t        value;
    const static uint8_t parseBufferLocal[] = {
        68,
        E_TAG_TYPE_U32,
        0xCD, 0xBA,0x01,0x02
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CMIU_ID == tagRead.tagNumber );
    TEST_ASSERT_TRUE(E_TAG_TYPE_U32 == tagRead.tagType);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_CmiuId(&parser, &value));
    TEST_ASSERT_EQUAL_UINT32(0x0201BACD,        value);
}


// 69
void Tpbp_TestParserRead_Apn(void)
{
    bool            isOk = false;
    char            apnBuffer[3 + 1];
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    const static uint8_t parseBufferLocal[] = {
        69,
        E_TAG_TYPE_CHAR_ARRAY,
        3,
        'A', 'P', 'N',
        };
    
    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CMIU_APN == tagRead.tagNumber );
    TEST_ASSERT_TRUE(E_TAG_TYPE_CHAR_ARRAY == tagRead.tagType);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_Apn(&parser, &tagRead, apnBuffer, sizeof(apnBuffer)));
    TEST_ASSERT_EQUAL_MEMORY(&parseBufferLocal[3], apnBuffer, 3);
}


// 69 with extended data
void Tpbp_TestParserRead_ApnExtended(void)
{
    bool            isOk = false;
    char            apnBuffer[256 + 1];
    S_TPBP_TAG      tagRead = {(E_TAG_NUMBER)0};
    static uint8_t parseBufferLocal[1+1+2+256] = {
        69,
        E_TAG_TYPE_EXTENDED_CHAR_ARRAY,
        0, 1, // = 256 as U16
         
        };
    
    memset(&parseBufferLocal[4], 'Z', 256);

    Tpbp_ParserInit( &parser, parseBufferLocal, sizeof(parseBufferLocal));   
        
    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);
    TEST_ASSERT_TRUE(E_TAG_NUMBER_CMIU_APN == tagRead.tagNumber );
    TEST_ASSERT_TRUE(E_TAG_TYPE_EXTENDED_CHAR_ARRAY == tagRead.tagType);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_Apn(&parser, &tagRead, apnBuffer, sizeof(apnBuffer)));
    TEST_ASSERT_EQUAL_MEMORY(&parseBufferLocal[4], apnBuffer, 256);
}

// 70
void Tpbp_TestParserRead_Command_Btle_RFTestMode(void)
{
    S_TPBP_PARSER   parser;
    bool            isOk = false;
    S_TPBP_TAG      tagRead = { (E_TAG_NUMBER)0 };
    S_COMMAND_BTLE_RFTESTMODE rec;

    const static uint8_t parseBufferLocal[] = {
        70,  //Tag num  
        E_TAG_TYPE_BYTE_RECORD,
        S_COMMAND_RFTESTMODE_BTLE_PACKED_SIZE,
        1,
        7,
        20,
        3
    };

    Tpbp_ParserInit(&parser, parseBufferLocal, sizeof(parseBufferLocal));

    isOk = Tpbp_ParserReadTag(&parser, &tagRead);
    TEST_ASSERT_TRUE(isOk);

    TEST_ASSERT_TRUE(70 == tagRead.tagNumber);
    TEST_ASSERT_TRUE(E_TAG_TYPE_BYTE_RECORD == tagRead.tagType);
    TEST_ASSERT_TRUE(1 + 1 + 1 + 1== tagRead.dataSize);

    TEST_ASSERT_TRUE(Tpbp_ParserRead_Command_Btle_RFTestMode(&parser, &rec));
    TEST_ASSERT_EQUAL_UINT8(1, rec.rfCommand);
    TEST_ASSERT_EQUAL_UINT8(7, rec.rfFrequency);
    TEST_ASSERT_EQUAL_UINT8(20, rec.rfPacketLength);
    TEST_ASSERT_EQUAL_UINT8(3, rec.rfPacketType);
}
