/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/


/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup TpbpUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test for the @ref Tagged Packet Builder Parser
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 */


#ifndef __TPBPUNITTEST_H__
#define __TPBPUNITTEST_H__

#define         BUFFER_SIZE 2000

void Tpbp_TestStructSizes(void);
void Tpbp_TestEndiannessIsLittle(void);
void Tpbp_TestUrlLength(void);
void Tpbp_TestTagTypeEnum(void);
void Tpbp_TestTagNumberEnum(void);

void Tpbp_TestPackerInit(void); 
void Tpbp_TestPackerReset(void);
void Tpbp_TestPackerGetCount(void);
void Tpbp_TestPackerGetAvailable(void);
void Tpbp_TestTpbp_PackerAdd_PacketTypeId(void);
void Tpbp_TestAddBytes(void); 
void Tpbp_TestTpbp_PackTagHelper(void);
void Tpbp_TestTpbp_PackerPadZeroToBlockBoundary(void);
void Tpbp_TestTpbp_Pack(void);
void Tpbp_TestTpbp_Pack64(void);

void Tpbp_Test_NetworkOps(void);
void Tpbp_Test_NetworkPerformance(void);
void Tpbp_Test_RegistrationStatus(void);
void Tpbp_Test_MqttBroker(void);
void Tpbp_Test_FallbackMqttBroker(void);
void Tpbp_Test_AssignedCmiuAddress(void);
void Tpbp_TestPackerAdd_FtpPortNumber(void);
void Tpbp_Test_ModemHardwareRevision(void);
void Tpbp_Test_ModemModelId(void);
void Tpbp_Test_ModemManufacturerId(void);
void Tpbp_Test_ModemSoftwareRevision(void);
void Tpbp_Test_ModemSerialNumber(void);
void Tpbp_Test_ModemSimImsiNumber(void);
void Tpbp_Test_ModemSimId(void);
void Tpbp_Test_PinPukPuk2(void);

void Tpbp_TestTpbp_PackerAdd_CmiuPacketHeader(void);
void Tpbp_TestTpbp_PackerAdd_CmiuConfiguration(void);
void Tpbp_TestTpbp_PackerAdd_ConnectionTimingLog(void);
void Tpbp_TestTpbp_PackerAdd_CmiuDiagnostics(void);
void Tpbp_TestTpbp_PackerAdd_CmiuStatus(void);
void Tpbp_TestTpbp_PackerAdd_ReportedDeviceConfig(void);
void Tpbp_TestTpbp_PackerAdd_RevisionHardware(void);
void Tpbp_TestTpbp_PackerAdd_Command(void);
void Tpbp_TestTpbp_PackerAdd_CmiuBasicConfiguration(void);
void Tpbp_TestTpbp_PackerAdd_IntervalRecordingConfig(void);
void Tpbp_TestTpbp_PackerAdd_UartPacketHeader(void);
void Tpbp_TestTpbp_PackerAdd_RevisionHardware(void);
void Tpbp_TestTpbp_PackerAdd_RevisionFirmware(void);
void Tpbp_TestTpbp_PackerAdd_RevisionBootloader(void);
void Tpbp_TestTpbp_PackerAdd_RevisionConfig(void);
void Tpbp_TestTpbp_PackerAdd_RevisionArb(void);
void Tpbp_TestTpbp_PackerAdd_RevisionBle(void);
void Tpbp_TestTpbp_PackerAdd_RevisionEncryption(void);
void Tpbp_TestTpbp_PackerAdd_CmiuInformation(void);
void Tpbp_TestTpbp_PackerAdd_ImageName(void);
void Tpbp_TestTpbp_PackerAdd_ImageMetaData(void);
void Tpbp_TestTpbp_PackerAdd_RecordingReporting(void);
void Tpbp_TestTpbp_PackerAdd_TimeDate(void);
void Tpbp_TestTpbp_PackerAdd_ErrorCode(void);
void Tpbp_TestTpbp_PackerAdd_debugData(void);
void Tpbp_TestTpbp_PackerAdd_StartDelay(void);
void Tpbp_TestTpbp_PackerAdd_Command_Duration(void);
void Tpbp_TestTpbp_PackerAdd_Command_ReadConnectedDevice(void);
void Tpbp_TestTpbp_PackerAdd_Command_PowerMode(void);
void Tpbp_TestTpbp_PackerAdd_Command_ReadMemory(void);
void Tpbp_TestTpbp_PackerAdd_Command_RFTestMode(void);
void Tpbp_TestTpbp_PackerAdd_Command_ToggleLTEModem(void);


void Tpbp_TestPackerAdd_R900_ReadingsOOK(void); // 60
void Tpbp_TestPackerAdd_R900_ReadingsFSK(void); // 61
void Tpbp_TestPackerAdd_BatteryVoltage(void);   // 62
void Tpbp_TestPackerAdd_CmiuTemperature(void);  // 63
void Tpbp_TestPackerAdd_MemImage(void);         // 64
void Tpbp_TestPackerAdd_PktType(void);          // 65
void Tpbp_TestPackerAdd_Destination(void);      // 66
void Tpbp_TestPackerAdd_MetaDataForPacket(void);// 67
void Tpbp_TestPackerAdd_CmiuId(void);           // 68
void Tpbp_TestPackerAdd_Apn(void);              // 69
void Tpbp_TestPackerAdd_ApnExtended(void);      // 69
void Tpbp_TestTpbp_PackerAdd_Command_Btle_RFTestMode(void);
void Tpbp_TestPackerAdd_NewImageVersion(void);  // 71

void Tpbp_TestTpbp_PackerAdd_Eof(void);

void Tpbp_TestTpbp_PackerAdd_UInt(void);
void Tpbp_TestTpbp_PackerAdd_UInt64(void);
void Tpbp_TestTpbp_PackerAdd_CharArray(void);
void Tpbp_TestTpbp_PackerAdd_ExtU8Array(void);
void Tpbp_TestTpbp_PackerAdd_ExtU16Array(void);
void Tpbp_TestTpbp_PackerAdd_ExtU32Array(void);
void Tpbp_TestTpbp_PackerAdd_SecureBlockArray(void);
void Tpbp_TestTpbp_PackerAdd_ImageVersionInfo(void);

void Tpbp_TestParserInit(void); 
void Tpbp_TestParserGetBytes(void);
void Tpbp_TestParserReadTag(void);
void Tpbp_TestParserSkipTagData(void);
void Tpbp_TestParserFindTag(void);
void Tpbp_TestParserUnpack32(void);
void Tpbp_TestParserUnpack16(void);
void Tpbp_TestParserUnpack8(void);
void Tpbp_TestParserUnpack64(void);
void Tpbp_TestParserRead_ImageVersionInfo(void);
void Tpbp_TestParserRead_String(void);
void Tpbp_TestParserRead_CmiuPacketHeader(void);
void Tpbp_TestParserRead_CmiuConfiguration(void);
void Tpbp_TestParserRead_FtpPortNumber(void);
void Tpbp_TestParserRead_CmiuDiagnostics(void);
void Tpbp_TestParserRead_CmiuStatus(void);
void Tpbp_TestParserRead_ConnectionTimingLog(void);
void Tpbp_TestParserRead_ReportedDeviceConfig(void);
void Tpbp_TestParserRead_CmiuBasicConfiguration(void);
void Tpbp_TestParserRead_IntervalRecordingConfig(void);
void Tpbp_TestParserRead_UartPacketHeader(void);
void Tpbp_TestParserRead_RevisionHardware(void);
void Tpbp_TestParserRead_RevisionFirmware(void);
void Tpbp_TestParserRead_RevisionBootloader(void);
void Tpbp_TestParserRead_RevisionConfig(void);
void Tpbp_TestParserRead_RevisionArb(void);
void Tpbp_TestParserRead_RevisionBle(void);
void Tpbp_TestParserRead_RevisionEncryption(void);
void Tpbp_TestParserRead_CmiuInformation(void);
void Tpbp_TestParserRead_ImageName(void);
void Tpbp_TestParserRead_ImageMetaData(void);
void Tpbp_TestParserRead_RecordingReporting(void);
void Tpbp_TestParserRead_Time(void);
void Tpbp_TestParserRead_ErrorCode(void);
void Tpbp_TestParserRead_debugData(void);
void Tpbp_TestParserRead_StartDelay(void);
void Tpbp_TestParserRead_Command_Duration(void);
void Tpbp_TestParserRead_Command_ReadConnectedDevice(void);
void Tpbp_TestParserRead_Command_PowerMode(void);
void Tpbp_TestParserRead_Command_ReadMemory(void);
void Tpbp_TestParserRead_Command_RFTestMode(void);
void Tpbp_TestParserRead_Command_ToggleLTEModem(void);
void Tpbp_TestParserRead_BatteryVoltage(void);
void Tpbp_TestParserRead_CmiuTemperature(void);
void Tpbp_TestParserRead_MemoryImageTag(void);
void Tpbp_TestParserRead_PacketTypeTag(void);
void Tpbp_TestParserRead_Destination(void);
void Tpbp_TestParserRead_MetadataForPacket(void);
void Tpbp_TestParserRead_CmiuId(void);
void Tpbp_TestParserRead_Apn(void);
void Tpbp_TestParserRead_ApnExtended(void);
void Tpbp_TestParserRead_Command_Btle_RFTestMode(void);
void Tpbp_TestSecureBegin(void);
void Tpbp_TestSecureBeginNoData(void);
void Tpbp_TestSecureBeginAlreadyPadded(void);
#endif /* __TPBPUNITTEST_H__ */
