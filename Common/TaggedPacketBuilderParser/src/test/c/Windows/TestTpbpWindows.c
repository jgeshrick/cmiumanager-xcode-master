/*****************************************************************************
******************************************************************************
**
**         Filename: TestTpbpWindows.c
**    
**           Author: Duncan Willis
**          Created: 20/04/2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/**
 * @defgroup TpbpUnitTest 
 *
 * @brief Unit Test for the @ref Tagged Packet Builder Parser  
 *
 */

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup TpbpUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Windows-specific application wrapper
 * @author Rupert Menzies
 * @date 2015.04.09
 * @version 1.0
 */


#include "TestTpbpRunner.h"

int main(void)
{
    return TestTpbp();
}

/**
 * @}
 * @}
 */

