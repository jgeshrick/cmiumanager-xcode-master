# 
# Batch file to run ctypesgen (a Python program)which creates a python interface to a DLL given the DLL and its .h file
# Needs Python 2.7 installed.
# Use pythonXY
# See  http://ctypesgen.googlecode.com/svn/trunk
#
# Duncan Willis 
# 5 March 2015
#

echo off
#echo  About to run
c:\work\CTypesGen\ctypesgen.py -l TPBP_DLL.dll .\c\Tpbp.h -o .\..\..\targetPython\TpbpDllInterface.py
copy TPBP_DLL.DLL C:\work\ 
echo Done.
pause
