/******************************************************************************
*******************************************************************************
**
**         Filename: Tpbp.c
**    
**           Author: Duncan Willis
**          Created: 2015.03.05
**
**     Last Edit By: Duncan Willis
**        Last Edit: 2015.04.16
**    
**      Description:  Module to pack and unpack structured data to/from flat memory
**                    for communication links.
**
**                    Usage - Refer to example \Common\TaggedPacketBuilderParser\src\main\c\main.c
**
**                    Packing:
**                    1) Declare a packer: S_TPBP_PACKER packer;
**                    2) Init it to an array of bytes:  Tpbp_PackerInit(&packer, myBuffer, sizeof(myBuffer));
**                    3) Add Packet(s) into it by calling the approriate function(s) with the structure
**                      isOk = Tpbp_PackerAdd_CmiuConfiguration(&packer, pCmiuConfiguration); 
**                    4) When packet(s) added, call   Tpbp_PackerGetCount(&packer) to determine how many bytes
**                      in myBuffer to send.
**
**                     Unpacking
**                    1) Declare a parser S_TPBP_PARSER parser;
**                    2) Point it to the bytes to be parsed:  Tpbp_ParserInit(&parser, packBuffer, sizeof(packBuffer));
**                    3) Read the first tag: isTag = Tpbp_ParserReadTag(&parser, &tag);
**                    4) If OK, switch (tag.tagNumber),  case E_TAG_NUMBER_CMIU_PACKET_HEADER:
**                    5) Read the corresponding packet: isOk = Tpbp_ParserRead_CmiuPacketHeader(&parser, &cmiuPkt);
**                    6) Repeat reading tags until none left
**
** Revision History:
** 
**
**
**      Code Review:
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
 
/**
 * @addtogroup Tpbp
 * @{
 */


/******************************************************************************
 * @file Tpbp.c
 *
 * @brief  Tagged Packet Builder Parser module
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/

#include "TpbpPackerHelpers.h"
#include "TpbpParserHelpers.h"
#include "Tpbp.h"


/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_CMIUPacketHeader
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The data structure to tag and write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified TAG type into the buffer. Tag Num 1
******************************************************************************/
bool Tpbp_PackerAdd_CmiuPacketHeader(S_TPBP_PACKER* const pPacker, const S_CMIU_PACKET_HEADER* const pkt)
{
    PACK_CMIU_PACKET_HEADER(pPacker, pkt);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_CMIUConfiguration
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The S_CMIU_CONFIGURATION to write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the CMIU Configuration (tag num 2) into the buffer.
******************************************************************************/
bool Tpbp_PackerAdd_CmiuConfiguration(S_TPBP_PACKER* const pPacker, 
    const S_CMIU_CONFIGURATION* const data)
{
    PACK_CMIU_CONFIGURATION(pPacker, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_NetworkOperators
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The data structure to tag and write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_NETWORK_OPERATORS TAG 
*  type into the buffer. Tag Num 3
******************************************************************************/
bool Tpbp_PackerAdd_NetworkOperators(S_TPBP_PACKER* const pPacker, const char* const operatorString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_NETWORK_OPERATORS, 
        operatorString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_NetworkPerformance
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The data structure to tag and write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_NETWORK_PERFORMANCE   
*  type into the buffer. Tag Num 4
******************************************************************************/
bool Tpbp_PackerAdd_NetworkPerformance(S_TPBP_PACKER* const pPacker, 
    const char* const performanceString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_NETWORK_PERFORMANCE, 
        performanceString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_RegistrationStatus
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The data structure to tag and write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_REGISTRATION_STATUS   
*  type into the buffer. Tag Num 5
******************************************************************************/
bool Tpbp_PackerAdd_RegistrationStatus(S_TPBP_PACKER* const pPacker, 
    const char* const registrationString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_REGISTRATION_STATUS, 
        registrationString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_MqttBrokerAddress
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The data structure to tag and write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_HOST_ADDRESS   
*  type into the buffer. Tag Num 6
******************************************************************************/
bool Tpbp_PackerAdd_MqttBrokerAddress(S_TPBP_PACKER* const pPacker, 
    const char* const brokerAddressString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_HOST_ADDRESS, 
        brokerAddressString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_FallbackMqttBrokerAddress
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The data structure to tag and write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_FALLBACK_HOST_ADDRESS   
*  type into the buffer. Tag Num 7
******************************************************************************/
bool Tpbp_PackerAdd_FallbackMqttBrokerAddress(S_TPBP_PACKER* const pPacker, 
    const char* const fallbackBrokerAddressString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_FALLBACK_HOST_ADDRESS, 
        fallbackBrokerAddressString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_AssignedCmiuAddress
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The data structure to tag and write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_ASSIGNED_CMIU_ADDRESS   
*  type into the buffer. Tag Num 8
******************************************************************************/
bool Tpbp_PackerAdd_AssignedCmiuAddress(S_TPBP_PACKER* const pPacker, 
    const char* const addressString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_ASSIGNED_CMIU_ADDRESS, 
        addressString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_FtpPortNumber
*
*              Author: Jonny Gloag
*             Created: 17 Feb 2016
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[in]   portNum  16 bit port number
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief Tag Num 10
******************************************************************************/
bool Tpbp_PackerAdd_FtpPortNumber(S_TPBP_PACKER* const pPacker,
    const uint16_t portNum)
{
    return Tpbp_PackerAdd_UInt(pPacker,
        E_TAG_NUMBER_FTP_PORT_NUMBER,
        (uint32_t)portNum,
        sizeof(portNum));
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ModemHardwareRevision
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The data structure to tag and write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_MODEM_HARDWARE_REVISION   
*  type into the buffer. Tag Num 15
******************************************************************************/
bool Tpbp_PackerAdd_ModemHardwareRevision(S_TPBP_PACKER* const pPacker, 
    const char* const revisionString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_MODEM_HARDWARE_REVISION, 
        revisionString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ModemModelId
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The data structure to tag and write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_MODEM_MODEL_IDENTIFICATION_CODE   
*  type into the buffer. Tag Num 16
******************************************************************************/
bool Tpbp_PackerAdd_ModemModelId(S_TPBP_PACKER* const pPacker, 
    const char* const modemIdString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_MODEM_MODEL_IDENTIFICATION_CODE, 
        modemIdString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ModemManufacturerId
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The data structure to tag and write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_MODEM_MANUFACTURER_ID_CODE   
*  type into the buffer. Tag Num 17
******************************************************************************/
bool Tpbp_PackerAdd_ModemManufacturerId(S_TPBP_PACKER* const pPacker, 
    const char* const modemManuString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_MODEM_MANUFACTURER_ID_CODE, 
        modemManuString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ModemSoftwareRevision
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The data structure to tag and write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_MODEM_SOFTWARE_REVISION   
*  type into the buffer. Tag Num 18
******************************************************************************/
bool Tpbp_PackerAdd_ModemSoftwareRevision(S_TPBP_PACKER* const pPacker, 
    const char* const modemSwRevString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_MODEM_SOFTWARE_REVISION, 
        modemSwRevString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ModemSerialNumber
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The data structure to tag and write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_IMEI   
*  type into the buffer. Tag Num 19
******************************************************************************/
bool Tpbp_PackerAdd_ModemSerialNumber(S_TPBP_PACKER* const pPacker, 
    const char* const modemSerialString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_IMEI, 
        modemSerialString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ModemSimImsiNumber
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        modemImsiString  The string to write to the given tag
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_SIM_IMSI   
*  type into the buffer. Tag Num 20
******************************************************************************/
bool Tpbp_PackerAdd_ModemSimImsiNumber(S_TPBP_PACKER* const pPacker, 
    const char* const modemImsiString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_SIM_IMSI, 
        modemImsiString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ModemSimId
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        modemImsiString  The string to write to the given tag
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_SIM_CARD_ID   
*  type into the buffer. Tag Num 21
******************************************************************************/
bool Tpbp_PackerAdd_ModemSimId(S_TPBP_PACKER* const pPacker, 
    const char* const simIdString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_SIM_CARD_ID, 
        simIdString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAddPinPukPuk2
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        modemImsiString  The string to write to the given tag
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_PINPUKPUK2_REQUEST_STATUS   
*  type into the buffer. Tag Num 22
******************************************************************************/
bool Tpbp_PackerAddPinPukPuk2(S_TPBP_PACKER* const pPacker, 
    const char* const pinPukString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_PINPUKPUK2_REQUEST_STATUS, 
        pinPukString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_CmiuDiagnostics
*    
*              Author: Anthony Hayward
*             Created: 13 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker     The Packer object
*  @param[in]       data        The S_CMIU_DIAGNOSTICS to write to the buffer (tag number 23)
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the CMIU Diagnostics (tag number 23) payload to packer  
*     type into the buffer.
******************************************************************************/
bool Tpbp_PackerAdd_CmiuDiagnostics(S_TPBP_PACKER* const pPacker, 
    const S_CMIU_DIAGNOSTICS* const data)
{
    PACK_CMIU_DIAGNOSTICS(pPacker, data);
}



/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAddBleLastUser
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        lastUserString  The string to write to the given tag
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_ID   
*  type into the buffer. Tag Num 24
******************************************************************************/
bool Tpbp_PackerAddBleLastUser(S_TPBP_PACKER* const pPacker, 
    const char* const lastUserString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_ID, 
        lastUserString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_LastLoggedInDate
*
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[out]  timeDate    64 bit time
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Add E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_DATE  (tag Number 25)
******************************************************************************/
bool Tpbp_PackerAdd_LastLoggedInDate(S_TPBP_PACKER* const pPacker, const uint64_t timeDate)
{
   return Tpbp_PackerAdd_TimeAndDateHelper(pPacker, E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_DATE, timeDate);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_CmiuStatus
*    
*              Author: Duncan Willis
*             Created: 8 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The S_CMIU_STATUS  to write to the buffer 
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified type into the buffer.(Tag Number 26)
******************************************************************************/
bool Tpbp_PackerAdd_CmiuStatus(S_TPBP_PACKER* const pPacker, 
    const S_CMIU_STATUS* const data)
{
    PACK_CMIU_STATUS(pPacker, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ReportedDeviceConfig
*    
*              Author: Duncan Willis
*             Created: 8 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The S_REPORTED_DEVICE_CONFIG  to write to the buffer 
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified type into the buffer.(Tag Number 27)
******************************************************************************/
bool Tpbp_PackerAdd_ReportedDeviceConfig(S_TPBP_PACKER* const pPacker, const S_REPORTED_DEVICE_CONFIG* const data)
{
    PACK_DEVICE_CONFIGURATION(pPacker, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_R900IntervalData
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The E_TAG_NUMBER_R900_INTERVAL_DATA  to write to the buffer 
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified type into the buffer.(Tag Number 28)
******************************************************************************/
bool Tpbp_PackerAdd_R900IntervalData(S_TPBP_PACKER* const pPacker, 
    const uint32_t data[], uint32_t numElements)
{
   return Tpbp_PackerAdd_ExtendedUint32Array(pPacker, 
        E_TAG_NUMBER_R900_INTERVAL_DATA,
        data, 
        numElements);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_EventData
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The E_TAG_NUMBER_EVENT  to write to the buffer 
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified type into the buffer.(Tag Number 29)
******************************************************************************/
bool Tpbp_PackerAdd_EventData(S_TPBP_PACKER* const pPacker, 
    const uint8_t data[], uint32_t numElements)
{
    bool isOk;
    ASSERT(numElements <= 255);

    isOk = Tpbp_PackerHelper(pPacker, 
        E_TAG_NUMBER_EVENT,
        E_TAG_TYPE_BYTE_RECORD, 
        numElements);

   if (isOk)
    {
        isOk = Tpbp_PackerAddBytes(pPacker, data, numElements);
    }

   return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAddErrorLog
*    
*              Author: Duncan Willis
*             Created: 9 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        errorLogString  The string to write to the given tag
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified E_TAG_NUMBER_ERROR_LOG   
*  type into the buffer. Tag Num 30
******************************************************************************/
bool Tpbp_PackerAddErrorLog(S_TPBP_PACKER* const pPacker, 
    const char* const errorLogString)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_ERROR_LOG, 
        errorLogString);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ConnectionTimingLog
*    
*              Author: Anthony Hayward
*             Created: 13 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker     The Packer object
*  @param[in]       data        The S_CONNECTION_TIMING_LOG to write to the buffer 
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified type into the buffer.(tag number 31)
******************************************************************************/
bool Tpbp_PackerAdd_ConnectionTimingLog(S_TPBP_PACKER* const pPacker, 
    const S_CONNECTION_TIMING_LOG* const data)
{
    PACK_CONNECTION_TIMING_LOG(pPacker, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_Command
*
*              Author: Duncan Willis
*             Created:
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[out]  commandByte The command ID
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Add a Command tag (tag number 32)
******************************************************************************/
bool Tpbp_PackerAdd_Command(S_TPBP_PACKER* const pPacker, const uint8_t commandByte)
{
    return Tpbp_PackerAdd_UInt(pPacker,
        E_TAG_NUMBER_COMMAND,
        (uint32_t) commandByte, 
        sizeof(commandByte));
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_CMIUBasicConfiguration
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The S_CMIU_BASIC_CONFIGURATION to write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the CMIU Basic Configuration (tag num 33) into the buffer.
******************************************************************************/
bool Tpbp_PackerAdd_CmiuBasicConfiguration(S_TPBP_PACKER* const pPacker, 
    const S_CMIU_BASIC_CONFIGURATION* const data)
{
    PACK_CMIU_BASIC_CONFIGURATION(pPacker, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_IntervalRecordingConfig
*    
*              Author: Duncan Willis
*             Created: 8 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The S_INTERVAL_RECORDING_CONFIG  to write to the buffer 
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified type into the buffer (Tag Number 34)
******************************************************************************/
bool Tpbp_PackerAdd_IntervalRecordingConfig(S_TPBP_PACKER* const pPacker, 
    const S_INTERVAL_RECORDING_CONFIG* const data)
{
    PACK_INTERVAL_RECORDING_CONFIG(pPacker, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_UartPacketHeader
*    
*              Author: Duncan Willis
*             Created: 8 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The S_MQTT_BLE_UART  to write to the buffer (Tag Number 35)
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified type into the buffer.
******************************************************************************/
bool Tpbp_PackerAdd_UartPacketHeader(S_TPBP_PACKER* const pPacker, 
    const S_MQTT_BLE_UART* const data)
{
    PACK_MQTT_BLE_UART(pPacker, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_CurrentTimeDate
*
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[out]  timeDate    64 bit time
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Add E_TAG_NUMBER_CURRENT_TIME  (tag Number 36)
******************************************************************************/
bool Tpbp_PackerAdd_CurrentTimeDate(S_TPBP_PACKER* const pPacker, const uint64_t timeDate)
{
   return Tpbp_PackerAdd_TimeAndDateHelper(pPacker, E_TAG_NUMBER_CURRENT_TIME, timeDate);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_MSISDNRequest
*    
*              Author: Duncan Willis
*             Created: 9 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The data structure to tag and write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified Tpbp_PackerAdd_MSISDNRequest   
*  type into the buffer. Tag Num 37
******************************************************************************/
bool Tpbp_PackerAdd_MSISDNRequest(S_TPBP_PACKER* const pPacker, 
    const char* const misdnRequest)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_MSISDN_REQUEST, 
        misdnRequest);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ImageVersionInfoX - Wrapper functions sharing this header
*    
*              Author: Duncan Willis
*             Created: 30 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        pRevision  The revision to write to the buffer (Tag Number 38-43)
*  @param[in]        revisionByteArray The 
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble revision type (for various tags) into the buffer.
******************************************************************************/
bool Tpbp_PackerAdd_ImageVersionInfoHardware(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision)
{
    return Tpbp_PackerAdd_ImageVersionInfo(pPacker, E_TAG_NUMBER_HARDWARE_REVISION, pRevision);
};

bool Tpbp_PackerAdd_ImageVersionInfoFirmware(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision)
{
    return Tpbp_PackerAdd_ImageVersionInfo(pPacker, E_TAG_NUMBER_FIRMWARE_REVISION, pRevision);
};

bool Tpbp_PackerAdd_ImageVersionInfoBootloader(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision)
{
    return Tpbp_PackerAdd_ImageVersionInfo(pPacker, E_TAG_NUMBER_BOOTLOADER_REVISION, pRevision);
};

bool Tpbp_PackerAdd_ImageVersionInfoConfig(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision)
{
    return Tpbp_PackerAdd_ImageVersionInfo(pPacker, E_TAG_NUMBER_CONFIG_REVISION, pRevision);
};

bool Tpbp_PackerAdd_ImageVersionInfoArb(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision)
{
    return Tpbp_PackerAdd_ImageVersionInfo(pPacker, E_TAG_NUMBER_ARB_REVISION, pRevision);
};

bool Tpbp_PackerAdd_ImageVersionInfoBle(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision)
{
    return Tpbp_PackerAdd_ImageVersionInfo(pPacker, E_TAG_NUMBER_BLE_CONFIG_REVISION, pRevision);
};

bool Tpbp_PackerAdd_ImageVersionInfoEncryption(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision)  
{
    return Tpbp_PackerAdd_ImageVersionInfo(pPacker, E_TAG_NUMBER_ENCRYPTION_CONFIG_REVISION, pRevision);
};


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_CmiuInformation
*    
*              Author: Duncan Willis
*             Created: 1 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The PACK_CMIU_INFORMATION to write (Tag Number 44)
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified type into the buffer.
******************************************************************************/
bool Tpbp_PackerAdd_CmiuInformation(S_TPBP_PACKER* const pPacker,
    const S_CMIU_INFORMATION* const data)
{
    PACK_CMIU_INFORMATION(pPacker, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_Packer_EndPacket
*    
*              Author: Anthony Hayward
*             Created: 20 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker   The Packer object
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Write an EOF tag to the buffer.  This is the final tag
*         in a packet.
******************************************************************************/
bool Tpbp_Packer_EndPacket(S_TPBP_PACKER* const pPacker)
{
    S_TPBP_TAG tag;
    bool isOk;

    tag.tagNumber = E_TAG_NUMBER_EOF;
    tag.tagType = E_TAG_TYPE_NONE;
    tag.dataSize = 0u;

    isOk = Tpbp_PackerTagHelper(pPacker, &tag);

    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_SecureBlockArray
*    
*              Author: Anthony Hayward
*             Created: 14 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker             The Packer object
*  @param[in]       dataForArray        The data to put in the array
*  @param[in]       arrayLengthInBytes  The length of the array in bytes. Must
*                                       be a multiple of 16.
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble Tag Num 254 Secure Data into the buffer.
******************************************************************************/
bool Tpbp_PackerAdd_SecureBlockArray(S_TPBP_PACKER* const pPacker, 
	const uint8_t* const dataForArray, 
	const uint32_t arrayLengthInBytes)
{
    bool isOk;
    S_TPBP_TAG tag;

    if ((arrayLengthInBytes & 15u) != 0u)
    { //not multiple of 16
        isOk = false;
        ASSERT(0);
    }
    else
    {
        tag.tagNumber = E_TAG_NUMBER_SECURE_DATA;
        tag.tagType = E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY;
        tag.dataSize = arrayLengthInBytes;

        isOk = Tpbp_PackerTagHelper(pPacker, &tag);

        if (isOk == true)
        {
            isOk = Tpbp_PackerAddBytes(pPacker, dataForArray, arrayLengthInBytes);
        }
    }
    return isOk;
}



/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ImageVersionInfo
*    
*              Author: Anthony Hayward
*             Created: 19 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker             The Packer object
*  @param[in]       tagNumber           The tag ID of the image version info
*  @param[in]       dataToAdd           The data for the image version info
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to put version info into the buffer.  The version info is stored
*  as big-endian Binary Coded Decimal for readability in hex form
******************************************************************************/
bool Tpbp_PackerAdd_ImageVersionInfo(S_TPBP_PACKER* const pPacker, 
    const E_TAG_NUMBER tagNumber, const S_IMAGE_VERSION_INFO* const dataToAdd)
{
    bool isOk;
    S_TPBP_TAG tag;

    tag.tagNumber = tagNumber;
    tag.tagType = E_TAG_TYPE_IMAGE_VERSION_INFO;

    isOk = Tpbp_PackerTagHelper(pPacker, &tag);

    if (isOk == true)
    {
        if (Tpbp_PackerGetAvailable(pPacker) >= S_IMAGE_VERSION_INFO_PACKED_SIZE)
        {

            isOk &= Tpbp_Pack(pPacker, dataToAdd->versionMajorBcd, 1);
            isOk &= Tpbp_Pack(pPacker, dataToAdd->versionMinorBcd, 1);

            //YYYYMMDD in big endian format
            isOk &= Tpbp_Pack(pPacker, dataToAdd->versionYearMonthDayBcd >> 24, 1);
            isOk &= Tpbp_Pack(pPacker, dataToAdd->versionYearMonthDayBcd >> 16, 1);
            isOk &= Tpbp_Pack(pPacker, dataToAdd->versionYearMonthDayBcd >> 8, 1);
            isOk &= Tpbp_Pack(pPacker, dataToAdd->versionYearMonthDayBcd, 1);

            //build number in big endian format
            isOk &= Tpbp_Pack(pPacker, dataToAdd->versionBuildBcd >> 24, 1);
            isOk &= Tpbp_Pack(pPacker, dataToAdd->versionBuildBcd >> 16, 1);
            isOk &= Tpbp_Pack(pPacker, dataToAdd->versionBuildBcd >> 8, 1);
            isOk &= Tpbp_Pack(pPacker, dataToAdd->versionBuildBcd, 1);
        }
        else
        {
            isOk = false;
        }
    }
    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ImageName
*    
*              Author: Duncan Willis 
*             Created: 30 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker             The Packer object
*  @param[in]       fileName            Null terminated filename string
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief File name tag for firmware download, tag number 46. 
*  The null term is NOT sent.
******************************************************************************/
bool Tpbp_PackerAdd_ImageName(S_TPBP_PACKER* const pPacker, const char* const fileName)
{
    bool isOk;
    S_TPBP_TAG tag;
    uint32_t stringLength;

    ASSERT(NULL != fileName);

    stringLength = strlen(fileName);

    tag.tagNumber   = E_TAG_NUMBER_IMAGE;
    tag.tagType     = E_TAG_TYPE_EXTENDED_CHAR_ARRAY;
    tag.dataSize    = stringLength;

    isOk = Tpbp_PackerTagHelper(pPacker, &tag);

    if (isOk == true)
    {
        if (Tpbp_PackerGetAvailable(pPacker) >= stringLength)
        {
            isOk = Tpbp_PackerAddBytes(pPacker, (const uint8_t*)fileName, stringLength);           
        }
        else
        {
            isOk = false;
        }
    }
    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ImageMetadata
*    
*              Author: Duncan Willis 
*             Created: 30 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker             The Packer object
*  @param[in]       pImageMetadata      The structure to pack
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief File metadata firmware download, tag number 47
******************************************************************************/
bool Tpbp_PackerAdd_ImageMetadata(S_TPBP_PACKER* const pPacker, 
    const S_IMAGE_METADATA* const pImageMetadata)
{
   PACK_IMAGE_METADATA(pPacker, pImageMetadata);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_RecordingReportingInterval
*    
*              Author: Duncan Willis
*             Created: 7 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker     The Packer object
*  @param[in]       data        The S_RECORDING_REPORTING_INTERVAL to write to the buffer (tag number 23)
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the CMIU Diagnostics (tag number 48) payload to packer  
*     type into the buffer.
******************************************************************************/
bool Tpbp_PackerAdd_RecordingReportingInterval(S_TPBP_PACKER* const pPacker, 
    const S_RECORDING_REPORTING_INTERVAL* const data)
{
    PACK_RECORDING_REPORTING_INTERVAL(pPacker, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_TimeAndDate
*
*              Author:
*             Created:
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[out]      timeDate    64 bit time
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Add TIMESTAMP (tag Number 50)
******************************************************************************/
bool Tpbp_PackerAdd_TimeAndDate(S_TPBP_PACKER* const pPacker, const uint64_t timeDate)
{
   return Tpbp_PackerAdd_TimeAndDateHelper(pPacker, E_TAG_NUMBER_TIMESTAMP, timeDate);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ErrorCode
*
*              Author:
*             Created:
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[out]  errorCode   16 bit ecode
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Add E_TAG_NUMBER_ERROR_CODE (tag Number 51)
******************************************************************************/
bool Tpbp_PackerAdd_ErrorCode(S_TPBP_PACKER* const pPacker, 
    const uint16_t errorCode)
{
   return Tpbp_PackerAdd_UInt(pPacker,
       E_TAG_NUMBER_ERROR_CODE,
       (uint32_t) errorCode, 
       sizeof(uint16_t));
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_Packer_BeginSecurePack
*
*              Author: Duncan Willis
*             Created: 8 July 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pPacker             The Packer object
*  @return True if tag successfully written else false (insufficient space)
*
*  @brief Used to prepare to write tags in secure region.
*  Use in conjuction with EndSecurePack()
*
******************************************************************************/
bool Tpbp_Packer_BeginSecurePack(S_TPBP_PACKER* const pPacker)
{
	bool isOk;
	S_TPBP_TAG tag;

	tag.tagNumber = E_TAG_NUMBER_SECURE_DATA;
	tag.tagType = E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY;
	tag.dataSize = 0;    // Will be populated in EndSecurePack()

	isOk = Tpbp_PackerTagHelper(pPacker, &tag);

	// Retain start of secure area
	pPacker->secureDataPosition = pPacker->writePosition;

	return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_Packer_EndSecurePack
*
*              Author: Duncan Willis
*             Created: 8 July 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pPacker            The Packer object
*  @param[out]  pNumSecureBytes        Number of secure bytes to encrypt
*  @return NULL for failure, else pointer to start of secure bytes
*
*  @brief Used to finalise the secure area. Pad to 128 bit size (16 bytes multiple)
*  and output the pointers to the region, so it can be encrypted in place.
*
******************************************************************************/
uint8_t* Tpbp_Packer_EndSecurePack(S_TPBP_PACKER* const pPacker, uint32_t* pNumSecureBytes)
{
	bool isOk;
	uint8_t* pSecureBytes = NULL;
	uint32_t numberSecureBytesWritten = pPacker->writePosition - pPacker->secureDataPosition;
	uint32_t numberPaddedSecureBytesWritten;
	uint32_t blockSize = 16;
	uint32_t numBlocks;

	ASSERT(pPacker->writePosition >= pPacker->secureDataPosition);

	// Pad secure area to 16 byte multiple
	isOk = Tpbp_PackerPadZeroHelper(pPacker, blockSize, numberSecureBytesWritten);

	if (isOk)
	{
		// Now work out the toatal number of secure bytes including padding
		numberPaddedSecureBytesWritten = pPacker->writePosition - pPacker->secureDataPosition;
		numBlocks = numberPaddedSecureBytesWritten / blockSize;

		ASSERT(numberPaddedSecureBytesWritten % blockSize == 0);
		ASSERT(pPacker->secureDataPosition >= 4); // Must have a 4 byte tag (E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY)
		ASSERT(numBlocks <= 255);

		// Now we know the length, write it into the tag.
		// Last byte of tag is *block* count  - this immediately precedes the secure data.
		// We previously set it to zero, so check it still is.
		ASSERT(0 == pPacker->pOutputBuffer[pPacker->secureDataPosition - 2]);
		ASSERT(0 == pPacker->pOutputBuffer[pPacker->secureDataPosition - 1]);
		pPacker->pOutputBuffer[pPacker->secureDataPosition - 2] = (uint8_t)numBlocks;
		pPacker->pOutputBuffer[pPacker->secureDataPosition - 1] = (uint8_t)(numBlocks >> 8);

		if (NULL != pNumSecureBytes)
		{
			*pNumSecureBytes = numberPaddedSecureBytesWritten;
		}

		pSecureBytes = &(pPacker->pOutputBuffer[pPacker->secureDataPosition]);
	}

	return pSecureBytes;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_DebugData
*
*              Author: Nick Sinas
*             Created: 22 Sept 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pPacker             The Packer object
*  @param[in]       debugData           Null terminated debug data
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Debug data to send. Used with tag number 52.
*  The null term is NOT sent.
******************************************************************************/
bool Tpbp_PackerAdd_DebugData(S_TPBP_PACKER* const pPacker, const char* const debugData)
{
	bool isOk;
	S_TPBP_TAG tag;
	uint32_t stringLength;

	ASSERT(NULL != debugData);

	stringLength = strlen(debugData);

	tag.tagNumber = E_TAG_NUMBER_DEBUG_DATA;
	tag.tagType = E_TAG_TYPE_EXTENDED_CHAR_ARRAY;
	tag.dataSize = stringLength;

	isOk = Tpbp_PackerTagHelper(pPacker, &tag);

	if (isOk == true)
	{
		if (Tpbp_PackerGetAvailable(pPacker) >= stringLength)
		{
			isOk = Tpbp_PackerAddBytes(pPacker, (const uint8_t*)debugData, stringLength);
		}
		else
		{
			isOk = false;
		}
	}
	return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_Command_StartDelay
*
*              Author: Nick Sinas
*             Created:
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]		pParser		The Packer object.
*  @param[out]      startDelay  64 bit start delay in ms
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Time delay before command should start (tag Number 53)
******************************************************************************/
bool Tpbp_PackerAdd_Command_StartDelay(S_TPBP_PACKER* const pPacker, const uint64_t startDelay)
{
    return Tpbp_PackerAdd_UInt64(pPacker, E_TAG_NUMBER_COMMAND_STARTDELAY, startDelay);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_Command_Duration
*
*              Author: Nick Sinas
*             Created:
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]		pParser		The Packer object.
*  @param[out]      duration    64 bit duration in ms
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Time that a command should take (tag Number 54)
******************************************************************************/
bool Tpbp_PackerAdd_Command_Duration(S_TPBP_PACKER* const pPacker, const uint64_t duration)
{
	return Tpbp_PackerAdd_UInt64(pPacker, E_TAG_NUMBER_COMMAND_DURATION, duration);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_Command_ReadConnectedDevice
*
*              Author: Nick Sinas
*             Created: 22 Sept 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The S_COMMAND_READCONNECTEDEVICE  to write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to pack the read connected device command.(Tag Number 55)
******************************************************************************/
bool Tpbp_PackerAdd_Command_ReadConnectedDevice(S_TPBP_PACKER* const pPacker, const S_COMMAND_READCONNECTEDEVICE* const data)
{
	PACK_COMMAND_READCONNECTEDDEVICE(pPacker, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_Command_PowerMode
*
*              Author: Nick Sinas
*             Created:
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]		pParser		The Packer object.
*  @param[out]      powerMode    64 bit duration in ms
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Time that a command should take (tag Number 56)
******************************************************************************/
bool Tpbp_PackerAdd_Command_PowerMode(S_TPBP_PACKER* const pPacker, const uint8_t powerMode)
{
    return Tpbp_PackerAdd_UInt(pPacker,
        E_TAG_NUMBER_COMMAND_POWERMODE,
        (uint32_t) powerMode, 
        sizeof(powerMode));
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_Command_ReadMemory
*
*              Author: Nick Sinas
*             Created: 22 Sept 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The S_COMMAND_MEMORY  to write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to pack the read memory command.(Tag Number 57)
******************************************************************************/
bool Tpbp_PackerAdd_Command_ReadMemory(S_TPBP_PACKER* const pPacker, const S_COMMAND_MEMORY* const data)
{
	PACK_COMMAND_MEMORY(pPacker, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_Command_RFTestMode
*
*              Author: Nick Sinas
*             Created: 22 Sept 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The S_COMMAND_RFTESTMODE  to write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to pack the rf test mode command.(Tag Number 58)
******************************************************************************/
bool Tpbp_PackerAdd_Command_RFTestMode(S_TPBP_PACKER* const pPacker, const S_COMMAND_RFTESTMODE* const data)
{
	PACK_COMMAND_RFTESTMODE(pPacker, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_Command_ToggleLTEModem
*
*              Author: Nick Sinas
*             Created:
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]		pParser		The Packer object.
*  @param[out]      toggleState    byte state modem should be
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief State of LTE modem, 1->ON, 0->OFF (tag Number 59)
******************************************************************************/
bool Tpbp_PackerAdd_Command_ToggleLTEModem(S_TPBP_PACKER* const pPacker, const uint8_t toggleState)
{
    return Tpbp_PackerAdd_UInt(pPacker,
        E_TAG_NUMBER_COMMAND_TOGGLELTEMODEM,
        (uint32_t) toggleState, 
        sizeof(toggleState));
}

 


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_BatteryVoltage
*
*              Author: Duncan Willis    
*             Created: 28 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[in]   adcValue 16 bit raw adc value
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief Tag Num 62
******************************************************************************/
bool Tpbp_PackerAdd_BatteryVoltage(S_TPBP_PACKER* const pPacker,     
    const uint16_t adcValue)
{
     return Tpbp_PackerAdd_UInt(pPacker,
       E_TAG_NUMBER_BATTERY_VOLTAGE,
       (uint32_t) adcValue, 
       sizeof(adcValue));
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_Cmiu_Temperature
*
*              Author: Duncan Willis    
*             Created: 28 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[in]   signed  8 bit temperatureCelsius 
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief Tag Num 63
******************************************************************************/
bool Tpbp_PackerAdd_Cmiu_Temperature(S_TPBP_PACKER* const pPacker,     
    const int8_t temperatureCelsius)
{
    return Tpbp_PackerAdd_UInt(pPacker,
        E_TAG_NUMBER_CMIU_TEMPERATURE,
        (uint32_t) temperatureCelsius, 
        sizeof(temperatureCelsius));
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_MemoryImageTag
*
*              Author: Duncan Willis    
*             Created: 28 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[in]   memoryImageTag. 16 bit bitmask
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief Tag Num 64
******************************************************************************/
bool Tpbp_PackerAdd_MemoryImageTag(S_TPBP_PACKER* const pPacker,     
    const uint16_t memoryImageTag)
{
    return Tpbp_PackerAdd_UInt(pPacker,
        E_TAG_NUMBER_MEMORY_IMAGE,
        (uint32_t) memoryImageTag, 
        sizeof(memoryImageTag));
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_PacketTypeTag
*
*              Author: Duncan Willis    
*             Created: 28 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[in]   8 bit Packet type value from the table.
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief Tag Num 65
******************************************************************************/
bool Tpbp_PackerAdd_PacketTypeTag(S_TPBP_PACKER* const pPacker,     
    const uint8_t packetTypeTag)
{
    return Tpbp_PackerAdd_UInt(pPacker,
        E_TAG_NUMBER_PACKET_TYPE,
        (uint32_t) packetTypeTag, 
        sizeof(packetTypeTag));
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_Destination
*
*              Author: Duncan Willis    
*             Created: 28 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[in]   8 bit Destination.Destination for response:
* Value      Destination
* 00-         Cellular MQTT
* 01-         BLE
* 02-         UART
* 03-      
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief Tag Num 66
******************************************************************************/
bool Tpbp_PackerAdd_Destination(S_TPBP_PACKER* const pPacker,     
    const uint8_t destination)
{
    return Tpbp_PackerAdd_UInt(pPacker,
        E_TAG_NUMBER_DESTINATION,
        (uint32_t) destination, 
        sizeof(destination));
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_MetadataForPacket
*
*              Author: Duncan Willis    
*             Created: 28 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[in]    
*  @return true if OK, else false if insufficient space available.
*
*  @brief Tag Num 67
******************************************************************************/
bool Tpbp_PackerAdd_MetadataForPacket(S_TPBP_PACKER* const pPacker,     
    const uint64_t startTime, const uint64_t endTime)
{
    bool isOk;

    isOk = Tpbp_Pack(pPacker, E_TAG_NUMBER_META_DATA_PACKET_REQUEST, 1);

    if (isOk == true)
    {
        isOk = Tpbp_Pack(pPacker, E_TAG_TYPE_U64_ARRAY, 1);
    }
    if (isOk == true)
    {
        isOk = Tpbp_Pack(pPacker, 2, 1); // There follows 2 elements of U64 in array
    }

    if (isOk == true)
    {
        isOk = Tpbp_Pack64(pPacker, startTime);
    }   
    
    if (isOk == true)
    {
        isOk = Tpbp_Pack64(pPacker, endTime);
    }

    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_CmiuId
*
*              Author: Duncan Willis    
*             Created: 28 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pPacker  The Packer object.
*  @param[in]   32 bit CMIU ID
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief Tag Num 68
******************************************************************************/
bool Tpbp_PackerAdd_CmiuId(S_TPBP_PACKER* const pPacker,     
    const uint32_t cmiuId)
{
    return Tpbp_PackerAdd_UInt(pPacker,
        E_TAG_NUMBER_CMIU_ID,
        (uint32_t) cmiuId, 
        sizeof(cmiuId));
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_Apn
*    
*              Author: Duncan Willis 
*             Created: 28 Oct 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker        The Packer object
*  @param[in]       apn            Null terminated APN string
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief File name tag for APN (Access Point Name), tag number 69. 
*  The null term is NOT sent.
******************************************************************************/
bool Tpbp_PackerAdd_Apn(S_TPBP_PACKER* const pPacker, const char* const apn)
{
    return Tpbp_PackerAdd_CharArray(pPacker, 
        E_TAG_NUMBER_CMIU_APN, 
        apn);
}



/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_Command_Btle_RFTestMode
*
*              Author: Duncan Willis
*             Created: 5 Nov 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The S_COMMAND_BTLE_RFTESTMODE  to write to the buffer
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to pack the rf test mode command.(Tag Number 70)
******************************************************************************/
bool Tpbp_PackerAdd_Command_Btle_RFTestMode(S_TPBP_PACKER* const pPacker, 
    const S_COMMAND_BTLE_RFTESTMODE* const data)
{
    PACK_COMMAND_BTLE_RFTESTMODE(pPacker, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_NewImageVersion
*
*              Author: Jonny Gloag
*             Created: 22 Feb 2016
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]   pPacker     The Packer object
*  @param[in]        imageString The new image version
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to pack the New image version information.(Tag Number 71)
******************************************************************************/
bool Tpbp_PackerAdd_NewImageVersion(S_TPBP_PACKER* const pPacker,
    const char* const imageString)
{
    return Tpbp_PackerAdd_CharArray(pPacker,
        E_TAG_NUMBER_MODEM_FOTA_VERSION,
        imageString);
}

/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_PacketInstigator
*
*              Author: Brian Arnberg
*             Created: 2016.06.20
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[in]   uint8    Packet Instigator Source
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief Pack Tag Num 72
******************************************************************************/
bool Tpbp_PackerAdd_PacketInstigator(S_TPBP_PACKER* const pPacker,
    const uint8_t packetInstigator)
{
    return Tpbp_PackerAdd_UInt(pPacker,
        E_TAG_NUMBER_PACKET_INSTIGATOR,
        (uint32_t)packetInstigator,
        sizeof(packetInstigator));
}



///
///  ******** PARSER functions below  **********
///




/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_ImageVersionInfo
*    
*              Author: Anthony Hayward
*             Created: 19 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      data     A structure to get the data
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the specified type from the parser.
******************************************************************************/
bool Tpbp_ParserRead_ImageVersionInfo(S_TPBP_PARSER* const pParser, S_IMAGE_VERSION_INFO* const data)
{
    bool isOk;
    uint32_t value;

    if (Tpbp_ParserGetBytesRemaining(pParser) >= S_IMAGE_VERSION_INFO_PACKED_SIZE)
    {
        data->versionMajorBcd = (uint8_t) Tpbp_Unpack(pParser, 1);
        data->versionMinorBcd = (uint8_t) Tpbp_Unpack(pParser, 1);

        value  = Tpbp_Unpack(pParser, 1) << 24;
        value += Tpbp_Unpack(pParser, 1) << 16;
        value += Tpbp_Unpack(pParser, 1) << 8;
        value += Tpbp_Unpack(pParser, 1);

        data->versionYearMonthDayBcd = value;

        value  = Tpbp_Unpack(pParser, 1) << 24;
        value += Tpbp_Unpack(pParser, 1) << 16;
        value += Tpbp_Unpack(pParser, 1) << 8;
        value += Tpbp_Unpack(pParser, 1);

        data->versionBuildBcd = value;

        isOk = true;
    }
    else
    {
        isOk = false;
    }
    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_String
*    
*              Author: Anthony Hayward
*             Created: 5 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser         The Parser object.  Should be positioned
*                                   after the tag introduction, at the start
*                                   of the character data in the char array tag.
*  @param[in]       pTag            The tag, to get the length of the array.
*  @param[in]       pStringBuffer   String buffer into which to read the string
*  @param[in]       bufferSize      Length of string buffer in bytes
*
*  @return true if OK, else false if text buffer is not big enough to
*  hold the string and null termination
*
*  @brief Read a text string from an [extended] char array tag and copy
*  it to the buffer as a null-terminated string
******************************************************************************/
bool Tpbp_ParserRead_String(S_TPBP_PARSER* const pParser, 
    const S_TPBP_TAG* const pTag, 
    char* const pStringBuffer, 
    const uint32_t bufferSize)
{
    bool isOk = false;
    const uint32_t stringLength = pTag->dataSize;
    const uint32_t requiredBufferSize = stringLength + 1u;
        
    if (requiredBufferSize <= bufferSize)
    {
        isOk = Tpbp_ParserGetBytes(pParser, (uint8_t*)pStringBuffer, stringLength);
        pStringBuffer[stringLength] = 0;
    }

    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerRead_CMIUPacketHeader
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      data     A structure to get the data
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the specified type from the parser.
******************************************************************************/
bool Tpbp_ParserRead_CmiuPacketHeader(S_TPBP_PARSER* const pParser, S_CMIU_PACKET_HEADER* const data)
{
      UNPACK_CMIU_PACKET_HEADER(pParser, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerRead_CMIUPacketHeader
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      data     A structure to get the data
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of S_CMIU_CONFIGURATION from the parser. (Tag number 2)
******************************************************************************/
bool Tpbp_ParserRead_CmiuConfiguration(S_TPBP_PARSER* const pParser, 
    S_CMIU_CONFIGURATION* const data)
{
    UNPACK_CMIU_CONFIGURATION(pParser, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_FtpPortNumber
*
*              Author: Jonny Gloag
*             Created: 17 Feb 2016
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pPortNum, 16 bit port number
*  @return true if OK, else false if insufficient data available.
*
*  @brief ftp Port Number, port Number.  (Tag number 10)
*
******************************************************************************/
bool Tpbp_ParserRead_FtpPortNumber(S_TPBP_PARSER* const pParser,
    uint16_t* const pPortNum)
{
    return Tpbp_ParseUInt16(pParser, pPortNum);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_CmiuDiagnostics
*    
*              Author: Anthony Hayward
*             Created: 13 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      data     A structure to get the data
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of S_CMIU_DIAGNOSTICS from the parser.(Tag number 23)
******************************************************************************/
bool Tpbp_ParserRead_CmiuDiagnostics(S_TPBP_PARSER* const pParser,
    S_CMIU_DIAGNOSTICS* const data)
{
    UNPACK_CMIU_DIAGNOSTICS(pParser, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_CmiuStatus
*    
*              Author: Duncan Willis
*             Created: 8 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      data     A structure to get the S_CMIU_STATUS FLAGS data 
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of S_CMIU_STATUS from the parser.(Tag number 26)
******************************************************************************/
bool Tpbp_ParserRead_CmiuStatus(S_TPBP_PARSER* const pParser, 
    S_CMIU_STATUS* const data)
{
    UNPACK_CMIU_STATUS(pParser, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_ReportedDeviceConfig
*    
*              Author: Duncan Willis
*             Created: 8 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      data     A structure to get the S_REPORTED_DEVICE_CONFIG data 
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the specified type from the parser.(Tag number 27)
******************************************************************************/
bool Tpbp_ParserRead_ReportedDeviceConfig(S_TPBP_PARSER* const pParser, 
    S_REPORTED_DEVICE_CONFIG* const data)
{
    UNPACK_DEVICE_CONFIGURATION(pParser, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_ConnectionTimingLog
*    
*              Author: Anthony Hayward
*             Created: 13 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pParser  The Parser object.
*  @param[out]       data     A structure to get the data
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the specified type from the parser.(Tag number 31)
******************************************************************************/
bool Tpbp_ParserRead_ConnectionTimingLog(S_TPBP_PARSER* const pParser, 
    S_CONNECTION_TIMING_LOG* const data)
{
    UNPACK_CONNECTION_TIMING_LOG(pParser, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_CmiuBasicConfiguration
*    
*              Author: Duncan Willis
*             Created: 3 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      data     A structure to get the S_CMIU_BASIC_CONFIGURATION data 
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the specified type from the parser.(Tag number 33)
******************************************************************************/
 bool  Tpbp_ParserRead_CmiuBasicConfiguration(S_TPBP_PARSER* const pParser, 
     S_CMIU_BASIC_CONFIGURATION* const data)
 {
     UNPACK_CMIU_BASIC_CONFIGURATION(pParser, data);
 }


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_IntervalRecordingConfig
*    
*              Author: Duncan Willis
*             Created: 8 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      data     A structure to get the S_INTERVAL_RECORDING_CONFIG data 
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the specified type from the parser.(Tag number 34)
******************************************************************************/
bool Tpbp_ParserRead_IntervalRecordingConfig(S_TPBP_PARSER* const pParser, 
    S_INTERVAL_RECORDING_CONFIG* const data)
{
    UNPACK_INTERVAL_RECORDING_CONFIG(pParser, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_UartPacketHeader
*    
*              Author: Duncan Willis
*             Created: 8 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      data     A structure to get the  MQTT_BLE_UART data 
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the specified type from the parser. (Tag number 35)
******************************************************************************/
bool Tpbp_ParserRead_UartPacketHeader(S_TPBP_PARSER* const pParser, 
    S_MQTT_BLE_UART* const data)
{
        UNPACK_MQTT_BLE_UART(pParser, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_CmiuInformation
*    
*              Author: Duncan Willis
*             Created: 1 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker    The Packer object
*  @param[in]        data       The PACK_CMIU_INFORMATION to read 
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Used to assemble the specified type into the buffer. (Tag Number 44)
******************************************************************************/
bool Tpbp_ParserRead_CmiuInformation(S_TPBP_PARSER* const pParser,
    S_CMIU_INFORMATION* const data)
{
    UNPACK_CMIU_INFORMATION(pParser, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_ImageName
*    
*              Author: Duncan Willis
*             Created: 30 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out] pParser      The Parser object.
*  @param[in]      ptag         The Tag, to get the length 
*  @param[out]     fileName     The output file name 
*  @param[in]     maxLength     The size of the buffer given 
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the image file name type from the parser.(Tag number 46)
******************************************************************************/
bool Tpbp_ParserRead_ImageName(S_TPBP_PARSER* const pParser, 
    const S_TPBP_TAG* const pTag, 
    char* const fileName, uint32_t maxLength)
{
    return Tpbp_ParserRead_String(pParser, 
        pTag,
        fileName,
        maxLength);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_ImageMetadata
*    
*              Author: Duncan Willis
*             Created: 30 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pImageMetadata     The output structure   (Tag number 47)
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the specified type from the parser.
******************************************************************************/
bool Tpbp_ParserRead_ImageMetadata(S_TPBP_PARSER* const pParser, 
    S_IMAGE_METADATA* const pImageMetadata)
{
    UNPACK_IMAGE_METADATA(pParser, pImageMetadata);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_RecordingReportingInterval
*    
*              Author: Duncan Willis
*             Created: 7 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker     The Packer object
*  @param[out]      data        The S_RECORDING_REPORTING_INTERVAL to read
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the specified type from the parser. (Tag number 48)
******************************************************************************/
bool Tpbp_ParserRead_RecordingReportingInterval(S_TPBP_PARSER* const pParser, 
    S_RECORDING_REPORTING_INTERVAL* const data)
{
    UNPACK_RECORDING_REPORTING_INTERVAL(pParser, data);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_Time
*    
*              Author: Duncan Willis
*             Created: 6 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pErrorCode  The output time  
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a tag of 64 bit time type from the parser. (Tag number 25, 36, 50)
*
******************************************************************************/
bool Tpbp_ParserRead_Time(S_TPBP_PARSER* const pParser, 
    uint64_t* const pTime)
{
    return Tpbp_ParseUInt64(pParser, pTime);
}
 

/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_ErrorCode
*    
*              Author: Duncan Willis
*             Created: 30 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pErrorCode  The output ecode  
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a tag of ErrorCode type from the parser. (Tag number 51)
*
******************************************************************************/
bool Tpbp_ParserRead_ErrorCode(S_TPBP_PARSER* const pParser, 
    uint16_t* const pErrorCode)
{
    return Tpbp_ParseUInt16(pParser, pErrorCode);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_Command_StartDelay
*
*              Author: Nick Sinas
*             Created: 22 Sept 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pStartDelay  The start delay time
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a tag of 64 bit start delay (Tag number 53)
*
******************************************************************************/
bool Tpbp_ParserRead_Command_StartDelay(S_TPBP_PARSER* const pParser, uint64_t* const pStartDelay)
{
    return Tpbp_ParseUInt64(pParser, pStartDelay);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_Command_Duration
*
*              Author: Nick Sinas
*             Created: 22 Sept 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pDuration  The command duration time
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a tag of 64 bit command duration (Tag number 54)
*
******************************************************************************/
bool Tpbp_ParserRead_Command_Duration(S_TPBP_PARSER* const pParser,	uint64_t* const pDuration)
{
    return Tpbp_ParseUInt64(pParser, pDuration);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_Command_ReadConnectedDevice
*
*              Author: Nick Sinas
*             Created: 22 Sept 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pCmdReadDevice     Device Read Structure   (Tag number 55)
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the specified type from the parser.
******************************************************************************/
bool Tpbp_ParserRead_Command_ReadConnectedDevice(S_TPBP_PARSER* const pParser, 
    S_COMMAND_READCONNECTEDEVICE* const pCmdReadDevice)
{
	UNPACK_COMMAND_READCONNECTEDDEVICE(pParser, pCmdReadDevice);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_Command_PowerMode
*
*              Author: Nick Sinas
*             Created: 22 Sept 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pPowerMode  The command power mode
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a tag of Power Mode (Tag number 56)
*
******************************************************************************/
bool Tpbp_ParserRead_Command_PowerMode(S_TPBP_PARSER* const pParser, uint8_t* const pPowerMode)
{
    return Tpbp_ParseByte(pParser, pPowerMode);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_Command_ReadMemory
*
*              Author: Nick Sinas
*             Created: 22 Sept 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pCmdReadMemory     The memory structure   (Tag number 57)
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the specified type from the parser.
******************************************************************************/
bool Tpbp_ParserRead_Command_ReadMemory(S_TPBP_PARSER* const pParser, S_COMMAND_MEMORY* const pCmdReadMemory)
{
	UNPACK_COMMAND_MEMORY(pParser, pCmdReadMemory);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_Command_RFTestMode
*
*              Author: Nick Sinas
*             Created: 22 Sept 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pCmdReadMemory     The rf test mode structure   (Tag number 58)
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the specified type from the parser.
******************************************************************************/
bool Tpbp_ParserRead_Command_RFTestMode(S_TPBP_PARSER* const pParser, S_COMMAND_RFTESTMODE* const pCmdRFTestMode)
{
	UNPACK_COMMAND_RFTESTMODE(pParser, pCmdRFTestMode);
}

/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_ToggleLTEModem
*
*              Author: Nick Sinas
*             Created: 22 Sept 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pModemToggle  Toggle of the lte modem
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a tag of 64 bit command duration (Tag number 59)
*
******************************************************************************/
bool Tpbp_ParserRead_Command_ToggleLTEModem(S_TPBP_PARSER* const pParser, uint8_t* const pModemToggle)
{
    return Tpbp_ParseByte(pParser, pModemToggle);
}
 

/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_BatteryVoltage
*
*              Author: Duncan Willis
*             Created: 28 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      raw ADC, 16 bit
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a voltage, raw ADC.  (Tag number 62)
*
******************************************************************************/
bool Tpbp_ParserRead_BatteryVoltage(S_TPBP_PARSER* const pParser, 
    uint16_t* const pAdcValue)
{
	return Tpbp_ParseUInt16(pParser, pAdcValue);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_CmiuTemperature
*
*              Author: Duncan Willis
*             Created: 28 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      temperatureCelsius, 8 bit deg C
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a temperatureCelsius.  (Tag number 63)
*
******************************************************************************/
bool Tpbp_ParserRead_CmiuTemperature(S_TPBP_PARSER* const pParser, 
    uint8_t* const pTemperatureCelsius)
{
	return Tpbp_ParseByte(pParser, pTemperatureCelsius);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_MemoryImageTag
*
*              Author: Duncan Willis
*             Created: 28 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pMemoryImageTag   
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a memoryImageTag. 16 bit bitmask (Tag number 64)
*
******************************************************************************/
bool Tpbp_ParserRead_MemoryImageTag(S_TPBP_PARSER* const pParser, uint16_t* const pMemoryImageTag)
{
	return Tpbp_ParseUInt16(pParser, pMemoryImageTag);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_Command_PacketType
*
*              Author: Barry Huggins
*             Created: 21 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pPacketType  The packet type to be published
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a tag of 8 bit packet type (Tag number 65)
*
******************************************************************************/
bool Tpbp_ParserRead_Command_PacketType(S_TPBP_PARSER* const pParser, 
    uint8_t* const pPacketType)
{
    return Tpbp_ParseByte(pParser, pPacketType);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_Command_Destination
*
*              Author: Barry Huggins
*             Created: 21 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pDestination  The destination for the packet to be published
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a tag of 8 bit destination (Tag number 66)
*
******************************************************************************/
bool Tpbp_ParserRead_Command_Destination(S_TPBP_PARSER* const pParser, 
    uint8_t* const pDestination)
{
    return Tpbp_ParseByte(pParser, pDestination);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_MetadataForPacket
*
*              Author: Duncan Willis 
*             Created: 21 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser     The Parser object.
*  @param[out]      pStartTime  Start Time value
*  @param[out]      pEndTime    End Time value
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a MetadataForPacket (Tag number 67)
*
******************************************************************************/
bool Tpbp_ParserRead_MetadataForPacket(S_TPBP_PARSER* const pParser, 
    uint64_t* const pStartTime, 
    uint64_t* const pEndTime)
{
    bool isOk = false;

    if (Tpbp_ParserGetBytesRemaining(pParser) >= (2 * sizeof(uint64_t)))
    {
        Tpbp_ParseUInt64(pParser, pStartTime);
        Tpbp_ParseUInt64(pParser, pEndTime);
        isOk = true;
    }
 
    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_CmiuId
*
*              Author: Duncan Willis
*             Created: 28 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pCmiuId   
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read an ID. 32 bit value. (Tag number 68)
*
******************************************************************************/
bool Tpbp_ParserRead_CmiuId(S_TPBP_PARSER* const pParser, uint32_t* const pCmiuId)
{
	return Tpbp_ParseUInt32(pParser, pCmiuId);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_CmiuId
*
*              Author: Duncan Willis
*             Created: 28 Oct 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pApnBuffer Buffer to copy APN String to
*  @param[in]       bufferSize size of string buffer
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read an APN String.(Tag number 69)
*
******************************************************************************/
DLL_EXPORT bool      Tpbp_ParserRead_Apn(S_TPBP_PARSER* const pParser, 
    const S_TPBP_TAG* const pTag, 
    char* const pApnBuffer,
    const uint32_t bufferSize)
{
    return Tpbp_ParserRead_String(pParser, 
        pTag,
        pApnBuffer,
        bufferSize);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_Command_Btle_RFTestMode
*
*              Author: Duncan Willis    
*             Created: 5 Nov 2015
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pCmdReadMemory     The BTLE rf test mode structure (Tag number 70)
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a packet of the specified type from the parser.
******************************************************************************/
bool Tpbp_ParserRead_Command_Btle_RFTestMode(S_TPBP_PARSER* const pParser, 
    S_COMMAND_BTLE_RFTESTMODE* const pCmdRFTestMode)
{
    UNPACK_COMMAND_BTLE_RFTESTMODE(pParser, pCmdRFTestMode);
}

/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_PacketInstigator
*
*              Author: Brian Arnberg
*             Created: 2016.06.21
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in, out]  pParser  The Parser object.
*  @param[out]      pDestination  The destination for the packet to be published
*  @return true if OK, else false if insufficient data available.
*
*  @brief Read a tag of 8 bit destination (Tag number 72)
*
******************************************************************************/
bool Tpbp_ParserRead_PacketInstigator(S_TPBP_PARSER* const pParser,
    uint8_t* const pPacketInstigator)
{
    return Tpbp_ParseByte(pParser, pPacketInstigator);
}

/**
 * @}
*/
