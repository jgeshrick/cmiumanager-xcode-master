/******************************************************************************
*******************************************************************************
**
**         Filename: Tpbp.h
**    
**           Author: Duncan Willis
**          Created: 2015.03.05
**
**     Last Edit By: Duncan Willis
**        Last Edit: 2015.04.16
**    
**
** Revision History:
** 
**
**
**      Code Review:
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/**
 * @addtogroup Tpbp 
 * @{
 */

/******************************************************************************
 * @file TpbpPackerHelpers.h
 *
 * @brief  Tagged Packet Builder Parser module
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/


#ifndef __TPBP_PACKER_H__
#define __TPBP_PACKER_H__


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <string.h>
#include "CommonTypes.h"
#include "TagTypes.h"


/**
* An object to manage packing
*/
typedef struct S_TPBP_PACKER
{
    /** The pointer to the start of the memory buffer */
    uint8_t*        pOutputBuffer;
    /** The current offset (where next byte will be written) */
    uint32_t        writePosition;
    /** The size in bytes of the buffer */
    uint32_t        bufferSize;
    
    /** The offset of the first secure byte, if used */
    uint32_t        secureDataPosition;
} 
S_TPBP_PACKER;


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/

 bool Tpbp_PackerHelper(S_TPBP_PACKER* const pPacker, 
    const E_TAG_NUMBER eTagNumber,
    const E_TAG_TYPE eTagType, 
    const uint8_t packedSize);

 bool Tpbp_PackerAdd_ExtendedArrayHelper(S_TPBP_PACKER* const pPacker, 
    const E_TAG_NUMBER tagNumber, 
    const E_TAG_TYPE    tagType, 
    const void* const dataToAdd, 
    uint32_t numElements, 
    uint32_t elementSize);

/**
* Packer functions
*/
DLL_EXPORT void      Tpbp_PackerInit(S_TPBP_PACKER* const pPacker, uint8_t* const pBuffer, const uint32_t bufferSize);
DLL_EXPORT void      Tpbp_PackerReset(S_TPBP_PACKER* const pPacker);
DLL_EXPORT uint32_t  Tpbp_PackerGetCount(const S_TPBP_PACKER* const pPacker);
DLL_EXPORT uint32_t  Tpbp_PackerGetAvailable(const S_TPBP_PACKER* const pPacker);
DLL_EXPORT bool      Tpbp_PackerAddBytes(S_TPBP_PACKER* const pPacker, const uint8_t* const bytes, const uint32_t numBytes);
DLL_EXPORT bool      Tpbp_PackerAdd_UInt(S_TPBP_PACKER* const pPacker, const E_TAG_NUMBER tagNumber, const uint32_t data, const uint32_t numBytes);
DLL_EXPORT bool      Tpbp_PackerAdd_UInt64(S_TPBP_PACKER* const pPacker, const E_TAG_NUMBER tagNumber, const uint64_t data);
DLL_EXPORT bool      Tpbp_PackerAdd_CharArray(S_TPBP_PACKER* const pPacker, const E_TAG_NUMBER tagNumber, const char* const stringToAdd);
DLL_EXPORT bool      Tpbp_PackerAdd_ExtendedUint8Array(S_TPBP_PACKER* const pPacker, const E_TAG_NUMBER tagNumber, const uint8_t* const dataToAdd, uint32_t numElements);
DLL_EXPORT bool      Tpbp_PackerAdd_ExtendedUint16Array(S_TPBP_PACKER* const pPacker, const E_TAG_NUMBER tagNumber,  const uint16_t* const dataToAdd, uint32_t numElements);
DLL_EXPORT bool      Tpbp_PackerAdd_ExtendedUint32Array(S_TPBP_PACKER* const pPacker, const E_TAG_NUMBER tagNumber, const uint32_t* const dataToAdd, uint32_t numElements);
DLL_EXPORT bool      Tpbp_PackerTagHelper(S_TPBP_PACKER* const pPacker, const S_TPBP_TAG* const pTag);
DLL_EXPORT bool      Tpbp_PackerPadZeroToBlockBoundary(S_TPBP_PACKER* const pPacker, const uint32_t blockSize);
DLL_EXPORT bool      Tpbp_PackerPadZeroHelper(S_TPBP_PACKER* const pPacker, const uint32_t blockSize, const uint32_t numBytes);
DLL_EXPORT bool      Tpbp_Pack(S_TPBP_PACKER* const pPacker, const uint32_t n, const uint32_t numBytes);
DLL_EXPORT bool      Tpbp_PackMultiple(S_TPBP_PACKER* const pPacker, 
                            const void* pData, 
                            const uint32_t elementSize, 
                            const uint32_t numElements);
DLL_EXPORT bool      Tpbp_Pack64(S_TPBP_PACKER* const pPacker, const uint64_t n);


bool Tpbp_PackerAdd_TimeAndDateHelper(S_TPBP_PACKER* const pPacker, 
    E_TAG_NUMBER eTagNumber, 
    const uint64_t timeDate);

#endif

/**
 * @}
 */
