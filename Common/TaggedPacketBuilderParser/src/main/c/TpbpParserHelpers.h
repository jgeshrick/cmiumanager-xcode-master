/******************************************************************************
*******************************************************************************
**
**         Filename: TpbpParserHelpers.h
**    
**           Author: Duncan Willis
**          Created: 2015.03.05
**
**     Last Edit By: Duncan Willis
**        Last Edit: 2015.04.16
**    
**
** Revision History:
** 
**
**
**      Code Review:
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/**
 * @addtogroup Tpbp 
 * @{
 */

/******************************************************************************
 * @file TpbpParserHelpers.h
 *
 * @brief  Tagged Packet Builder Parser module
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/


#ifndef __TPBP_PARSER_H__
#define __TPBP_PARSER_H__


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <string.h>
#include "CommonTypes.h"
#include "TagTypes.h"



/*===========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                        */
/*===========================================================================*/

/**
* An object to manage parsing (unpacking)
*/
typedef struct S_TPBP_PARSER
{
    /** The pointer to the start of the memory buffer */
    const uint8_t*  pInputBuffer;
    /** The current offset (where next byte will be read) */
    uint32_t        readPosition;
    /** The size in bytes of the buffer */
    uint32_t        bufferSize;
}
S_TPBP_PARSER;


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/


/**
* Parser functions
*/

DLL_EXPORT void      Tpbp_ParserInit(S_TPBP_PARSER* const pParser, const uint8_t* const pBuffer, const uint32_t bufferSize);
DLL_EXPORT uint32_t  Tpbp_ParserGetBytesRemaining(const S_TPBP_PARSER* const pParser); 
DLL_EXPORT bool      Tpbp_ParserGetBytes(S_TPBP_PARSER* const pParser, uint8_t* const pBuffer, const uint32_t numBytes);
DLL_EXPORT bool      Tpbp_ParserReadTag(S_TPBP_PARSER* const pParser, S_TPBP_TAG* const pTag);
DLL_EXPORT bool      Tpbp_ParserSkipTagData(S_TPBP_PARSER* const pParser, const S_TPBP_TAG* const pTag);
DLL_EXPORT bool      Tpbp_ParserFindTag(S_TPBP_PARSER* const pParser, const E_TAG_NUMBER desiredTagNumber, S_TPBP_TAG* const pTagFound);

DLL_EXPORT uint32_t  Tpbp_Unpack(S_TPBP_PARSER* const pParser, const uint32_t numBytes);
DLL_EXPORT uint64_t  Tpbp_Unpack64(S_TPBP_PARSER* const pParser);

DLL_EXPORT bool     Tpbp_ParseByte  (S_TPBP_PARSER* const pParser, uint8_t*  const pValue);
DLL_EXPORT bool     Tpbp_ParseUInt16(S_TPBP_PARSER* const pParser, uint16_t* const pValue);
DLL_EXPORT bool     Tpbp_ParseUInt32(S_TPBP_PARSER* const pParser, uint32_t* const pValue);
DLL_EXPORT bool     Tpbp_ParseUInt64(S_TPBP_PARSER* const pParser, uint64_t* const pValue);
 

#endif

/**
 * @}
 */
