/******************************************************************************
*******************************************************************************
**
**         Filename: Tpbp.h
**    
**           Author: Duncan Willis
**          Created: 2015.03.05
**
**     Last Edit By: Duncan Willis
**        Last Edit: 2015.04.16
**    
**
** Revision History:
** 
**
**
**      Code Review:
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/**
 * @addtogroup Tpbp 
 * @{
 */

/******************************************************************************
 * @file Tpbp.h
 *
 * @brief  Tagged Packet Builder Parser module
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/


#ifndef __TPBP_H__
#define __TPBP_H__


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/



#include "CommonTypes.h"
#include "TagTypes.h"
#include "TpbpPackerHelpers.h"
#include "TpbpParserHelpers.h"



/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/


#define S_TPBP_TAG_ARRAY_PACKED_SIZE    (3)
#define S_TPBP_TAG_INTEGER_PACKED_SIZE  (2)
#define S_TPBP_TAG_NO_TYPE_PACKED_SIZE  (1)

/* The byte offset (from start of tagged data) to the 
** given element (i.e. skips the tag)
*/
#define CMIU_PKT_OFFSET(n)              ((n) + S_TPBP_TAG_ARRAY_PACKED_SIZE)
 
/*
** The max length of URL used in image update
*/
#define TPBP_MAX_IMAGE_FILE_URL_LENGTH   (255)
#define TPBP_URL_BUFFER_LENGTH           (TPBP_MAX_IMAGE_FILE_URL_LENGTH + 1) // null term


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/

/**  NOTE - Refer to TpbpPackerHelpers and TpbpParserHelpers for general functions - e.g. initilisation **/

// Start to pack a packet beginning with its type.
DLL_EXPORT bool      Tpbp_Packer_BeginPacket(S_TPBP_PACKER* const pPacker, const E_PACKET_TYPE_ID packetTypeId);


// Tag Packers
DLL_EXPORT bool      Tpbp_PackerAdd_CmiuPacketHeader(S_TPBP_PACKER* const pPacker, const S_CMIU_PACKET_HEADER* const data);                             // 1
DLL_EXPORT bool      Tpbp_PackerAdd_CmiuConfiguration(S_TPBP_PACKER* const pPacker, const S_CMIU_CONFIGURATION* const data);                            // 2
DLL_EXPORT bool      Tpbp_PackerAdd_NetworkOperators(S_TPBP_PACKER* const pPacker, const char* const operatorString);                                   // 3
DLL_EXPORT bool      Tpbp_PackerAdd_NetworkPerformance(S_TPBP_PACKER* const pPacker, const char* const performanceString);                              // 4
DLL_EXPORT bool      Tpbp_PackerAdd_RegistrationStatus(S_TPBP_PACKER* const pPacker, const char* const registrationString);                             // 5
DLL_EXPORT bool      Tpbp_PackerAdd_MqttBrokerAddress(S_TPBP_PACKER* const pPacker,   const char* const brokerAddressString);                           // 6 
DLL_EXPORT bool      Tpbp_PackerAdd_FallbackMqttBrokerAddress(S_TPBP_PACKER* const pPacker,   const char* const fallbackBokerAddressString);            // 7 
DLL_EXPORT bool      Tpbp_PackerAdd_AssignedCmiuAddress(S_TPBP_PACKER* const pPacker, const char* const addressString);                                 // 8
DLL_EXPORT bool      Tpbp_PackerAdd_FtpPortNumber(S_TPBP_PACKER* const pPacker, const uint16_t portNum);                                                // 10
DLL_EXPORT bool      Tpbp_PackerAdd_ModemHardwareRevision(S_TPBP_PACKER* const pPacker, const char* const revisionString);                              // 15
DLL_EXPORT bool      Tpbp_PackerAdd_ModemModelId(S_TPBP_PACKER* const pPacker, const char* const modemIdString);                                        // 16
DLL_EXPORT bool      Tpbp_PackerAdd_ModemManufacturerId(S_TPBP_PACKER* const pPacker, const char* const modemManuString);                               // 17
DLL_EXPORT bool      Tpbp_PackerAdd_ModemSoftwareRevision(S_TPBP_PACKER* const pPacker,   const char* const modemSwRevString);                          // 18
DLL_EXPORT bool      Tpbp_PackerAdd_ModemSerialNumber(S_TPBP_PACKER* const pPacker,  const char* const modemSerialString);                              // 19
DLL_EXPORT bool      Tpbp_PackerAdd_ModemSimImsiNumber(S_TPBP_PACKER* const pPacker, const char* const modemImsiString);                                // 20
DLL_EXPORT bool      Tpbp_PackerAdd_ModemSimId(S_TPBP_PACKER* const pPacker,  const char* const simIdString);                                           // 21
DLL_EXPORT bool      Tpbp_PackerAddPinPukPuk2(S_TPBP_PACKER* const pPacker, const char* const pinPukString);                                            // 22
DLL_EXPORT bool		 Tpbp_PackerAdd_CmiuDiagnostics(S_TPBP_PACKER* const pPacker, const S_CMIU_DIAGNOSTICS* const data);                                // 23
DLL_EXPORT bool      Tpbp_PackerAddBleLastUser(S_TPBP_PACKER* const pPacker, const char* const lastUserString);                                         // 24
DLL_EXPORT bool      Tpbp_PackerAdd_LastLoggedInDate(S_TPBP_PACKER* const pPacker, const uint64_t timeDate);                                            // 25
DLL_EXPORT bool      Tpbp_PackerAdd_CmiuStatus(S_TPBP_PACKER* const pPacker, const S_CMIU_STATUS* const data);                                          // 26
DLL_EXPORT bool      Tpbp_PackerAdd_ReportedDeviceConfig(S_TPBP_PACKER* const pPacker, const S_REPORTED_DEVICE_CONFIG* const data);                     // 27
DLL_EXPORT bool      Tpbp_PackerAdd_R900IntervalData(S_TPBP_PACKER* const pPacker, const uint32_t  data[], uint32_t numElements);                       // 28
DLL_EXPORT bool      Tpbp_PackerAdd_EventData(S_TPBP_PACKER* const pPacker, const uint8_t  data[], uint32_t numElements);                               // 29
DLL_EXPORT bool      Tpbp_PackerAddErrorLog(S_TPBP_PACKER* const pPacker, const char* const errorLogString);                                            // 30
DLL_EXPORT bool		 Tpbp_PackerAdd_ConnectionTimingLog(S_TPBP_PACKER* const pPacker, const S_CONNECTION_TIMING_LOG* const data);                       // 31
DLL_EXPORT bool      Tpbp_PackerAdd_Command(S_TPBP_PACKER* const pPacker, const uint8_t commandByte);                                                   // 32
DLL_EXPORT bool      Tpbp_PackerAdd_CmiuBasicConfiguration(S_TPBP_PACKER* const pPacker, const S_CMIU_BASIC_CONFIGURATION* const data);                 // 33
DLL_EXPORT bool      Tpbp_PackerAdd_IntervalRecordingConfig(S_TPBP_PACKER* const pPacker, const S_INTERVAL_RECORDING_CONFIG* const data);               // 34
DLL_EXPORT bool      Tpbp_PackerAdd_UartPacketHeader(S_TPBP_PACKER* const pPacker, const S_MQTT_BLE_UART* const data);                                  // 35
DLL_EXPORT bool      Tpbp_PackerAdd_CurrentTimeDate(S_TPBP_PACKER* const pPacker, const uint64_t timeDate);                                             // 36
DLL_EXPORT bool      Tpbp_PackerAdd_MSISDNRequest(S_TPBP_PACKER* const pPacker, const char* const misdnRequest);                                        // 37
DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoHardware(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision);                // 38
DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoFirmware(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision);                // 39
DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoBootloader(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision);              // 40
DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoConfig(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision);                  // 41
DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoArb(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision);                     // 42
DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoBle(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision);                     // 43
DLL_EXPORT bool      Tpbp_PackerAdd_CmiuInformation(S_TPBP_PACKER* const pPacker, const S_CMIU_INFORMATION* const data);                                // 44
DLL_EXPORT bool      Tpbp_PackerAdd_ImageName(S_TPBP_PACKER* const pPacker, const char* const fileName);                                                // 46
DLL_EXPORT bool      Tpbp_PackerAdd_ImageMetadata(S_TPBP_PACKER* const pPacker, const S_IMAGE_METADATA* const pImageMetadata);                          // 47
DLL_EXPORT bool      Tpbp_PackerAdd_RecordingReportingInterval(S_TPBP_PACKER* const pPacker, const S_RECORDING_REPORTING_INTERVAL* const data);         // 48
DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoEncryption(S_TPBP_PACKER* const pPacker, const S_IMAGE_VERSION_INFO* const pRevision);              // 49
DLL_EXPORT bool      Tpbp_PackerAdd_TimeAndDate(S_TPBP_PACKER* const pPacker, const uint64_t timeDate);                                                 // 50
DLL_EXPORT bool      Tpbp_PackerAdd_ErrorCode(S_TPBP_PACKER* const pPacker, const uint16_t errorCode);                                                  // 51
DLL_EXPORT bool		 Tpbp_PackerAdd_DebugData(S_TPBP_PACKER* const pPacker, const char* const debugData);												// 52
DLL_EXPORT bool		 Tpbp_PackerAdd_Command_StartDelay(S_TPBP_PACKER* const pPacker, const uint64_t startDelay);										// 53
DLL_EXPORT bool		 Tpbp_PackerAdd_Command_Duration(S_TPBP_PACKER* const pPacker, const uint64_t duration);											// 54
DLL_EXPORT bool      Tpbp_PackerAdd_Command_ReadConnectedDevice(S_TPBP_PACKER* const pPacker, const S_COMMAND_READCONNECTEDEVICE* const data);          // 55
DLL_EXPORT bool		 Tpbp_PackerAdd_Command_PowerMode(S_TPBP_PACKER* const pPacker, const uint8_t powerMode);											// 56
DLL_EXPORT bool		 Tpbp_PackerAdd_Command_ReadMemory(S_TPBP_PACKER* const pPacker, const S_COMMAND_MEMORY* const data);								// 57
DLL_EXPORT bool      Tpbp_PackerAdd_Command_RFTestMode(S_TPBP_PACKER* const pPacker, const S_COMMAND_RFTESTMODE* const data);							// 58
DLL_EXPORT bool      Tpbp_PackerAdd_Command_ToggleLTEModem(S_TPBP_PACKER* const pPacker, const uint8_t toggleState);									// 59
DLL_EXPORT bool      Tpbp_PackerAdd_R900_Readings_OOK(S_TPBP_PACKER* const pPacker, const uint8_t transferPacketdata[], uint32_t numPackets);           // 60
DLL_EXPORT bool      Tpbp_PackerAdd_R900_Readings_FSK(S_TPBP_PACKER* const pPacker, const uint8_t transferPacketdata[], uint32_t numPackets);           // 61
DLL_EXPORT bool      Tpbp_PackerAdd_BatteryVoltage(S_TPBP_PACKER* const pPacker, const uint16_t adcValue);                                              // 62
DLL_EXPORT bool      Tpbp_PackerAdd_Cmiu_Temperature(S_TPBP_PACKER* const pPacker, const int8_t temperatureCelsius);                                    // 63
DLL_EXPORT bool      Tpbp_PackerAdd_MemoryImageTag(S_TPBP_PACKER* const pPacker, const uint16_t memoryImageTag);                                        // 64
DLL_EXPORT bool      Tpbp_PackerAdd_PacketTypeTag(S_TPBP_PACKER* const pPacker, const uint8_t packetTypeTag);                                           // 65
DLL_EXPORT bool      Tpbp_PackerAdd_Destination(S_TPBP_PACKER* const pPacker, const uint8_t destination);                                               // 66
DLL_EXPORT bool      Tpbp_PackerAdd_MetadataForPacket(S_TPBP_PACKER* const pPacker, const uint64_t startTime, const uint64_t endTime);                  // 67

DLL_EXPORT bool      Tpbp_PackerAdd_CmiuId(S_TPBP_PACKER* const pPacker,  const uint32_t cmiuId);                                                       // 68
DLL_EXPORT bool      Tpbp_PackerAdd_Apn(S_TPBP_PACKER* const pPacker, const char* const apn);                                                           // 69
DLL_EXPORT bool      Tpbp_PackerAdd_Command_Btle_RFTestMode(S_TPBP_PACKER* const pPacker, const S_COMMAND_BTLE_RFTESTMODE* const data);                 // 70
DLL_EXPORT bool      Tpbp_PackerAdd_NewImageVersion(S_TPBP_PACKER* const pPacker, const char* const imageString);                                       // 71
DLL_EXPORT bool      Tpbp_PackerAdd_PacketInstigator(S_TPBP_PACKER* const pPacker, const uint8_t packetInstigator);                                     // 72
DLL_EXPORT bool		 Tpbp_PackerAdd_SecureBlockArray(S_TPBP_PACKER* const pPacker, const uint8_t* const dataForArray, const uint32_t arrayLengthInBytes);//254

// End packing a packet with EOF marker.
DLL_EXPORT bool		 Tpbp_Packer_EndPacket(S_TPBP_PACKER* const pPacker);                                                                                  // 255



DLL_EXPORT E_PACKET_TYPE_ID Tpbp_ParserRead_PacketTypeId(S_TPBP_PARSER* const pParser);
DLL_EXPORT bool		 Tpbp_ParserRead_ImageVersionInfo(S_TPBP_PARSER* const pParser, S_IMAGE_VERSION_INFO* const data);
DLL_EXPORT bool      Tpbp_ParserRead_String(S_TPBP_PARSER* const pParser, const S_TPBP_TAG* const pTag, char* const pStringBuffer, const uint32_t bufferSize);
DLL_EXPORT bool      Tpbp_ParserRead_CmiuPacketHeader(S_TPBP_PARSER* const pParser, S_CMIU_PACKET_HEADER* const data);                                  // 1
DLL_EXPORT bool      Tpbp_ParserRead_CmiuConfiguration(S_TPBP_PARSER* const pParser, S_CMIU_CONFIGURATION* const data);                                 // 2 
DLL_EXPORT bool      Tpbp_ParserRead_FtpPortNumber(S_TPBP_PARSER* const pParser, uint16_t* const pPortNum);                                             // 10
DLL_EXPORT bool      Tpbp_ParserRead_CmiuDiagnostics(S_TPBP_PARSER* const pParser, S_CMIU_DIAGNOSTICS* const data);                                     // 23
DLL_EXPORT bool      Tpbp_ParserRead_CmiuStatus(S_TPBP_PARSER* const pParser, S_CMIU_STATUS* const data);                                               // 26
DLL_EXPORT bool      Tpbp_ParserRead_ReportedDeviceConfig(S_TPBP_PARSER* const pPacker, S_REPORTED_DEVICE_CONFIG* const data);                          // 27
DLL_EXPORT bool      Tpbp_ParserRead_ConnectionTimingLog(S_TPBP_PARSER* const pParser, S_CONNECTION_TIMING_LOG* const data);                            // 31
DLL_EXPORT bool      Tpbp_ParserRead_CmiuBasicConfiguration(S_TPBP_PARSER* const pParser, S_CMIU_BASIC_CONFIGURATION* const data);                      // 33
DLL_EXPORT bool      Tpbp_ParserRead_IntervalRecordingConfig(S_TPBP_PARSER* const pParser, S_INTERVAL_RECORDING_CONFIG* const data);                    // 34
DLL_EXPORT bool      Tpbp_ParserRead_UartPacketHeader(S_TPBP_PARSER* const pParser, S_MQTT_BLE_UART* const data);                                       // 35
DLL_EXPORT bool      Tpbp_ParserRead_CmiuInformation(S_TPBP_PARSER* const pParser, S_CMIU_INFORMATION* const data);                                     // 44
DLL_EXPORT bool      Tpbp_ParserRead_ImageName(S_TPBP_PARSER* const pParser, const S_TPBP_TAG* const pTag, char* const fileName, uint32_t maxLength);   // 46
DLL_EXPORT bool      Tpbp_ParserRead_ImageMetadata(S_TPBP_PARSER* const pParser, S_IMAGE_METADATA* const pImageMetadata);                               // 47
DLL_EXPORT bool      Tpbp_ParserRead_RecordingReportingInterval(S_TPBP_PARSER* const pParser, S_RECORDING_REPORTING_INTERVAL* const data);              // 48
DLL_EXPORT bool      Tpbp_ParserRead_Time(S_TPBP_PARSER* const pParser, uint64_t* const pTime);                                                         // 50, and all 64bit time tag nums, 25, 36, 50
DLL_EXPORT bool      Tpbp_ParserRead_ErrorCode(S_TPBP_PARSER* const pParser, uint16_t* const pErrorCode);                                               // 51
DLL_EXPORT bool		 Tpbp_ParserRead_Command_StartDelay(S_TPBP_PARSER* const pParser, uint64_t* const pStartDelay);										// 53
DLL_EXPORT bool      Tpbp_ParserRead_Command_Duration(S_TPBP_PARSER* const pParser, uint64_t* const pDuration);											// 54
DLL_EXPORT bool		 Tpbp_ParserRead_Command_ReadConnectedDevice(S_TPBP_PARSER* const pParser, S_COMMAND_READCONNECTEDEVICE* const pCmdReadDevice);		// 55
DLL_EXPORT bool		 Tpbp_ParserRead_Command_PowerMode(S_TPBP_PARSER* const pParser, uint8_t* const pPowerMode);										// 56
DLL_EXPORT bool		 Tpbp_ParserRead_Command_ReadMemory(S_TPBP_PARSER* const pParser, S_COMMAND_MEMORY* const pCmdReadMemory);							// 57
DLL_EXPORT bool		 Tpbp_ParserRead_Command_RFTestMode(S_TPBP_PARSER* const pParser, S_COMMAND_RFTESTMODE* const pCmdRFTestMode);						// 58
DLL_EXPORT bool		 Tpbp_ParserRead_Command_ToggleLTEModem(S_TPBP_PARSER* const pParser, uint8_t* const pModemToggle);									// 59
DLL_EXPORT bool      Tpbp_ParserRead_BatteryVoltage(S_TPBP_PARSER* const pParser, uint16_t* const pAdcValue);                                           // 62
DLL_EXPORT bool      Tpbp_ParserRead_CmiuTemperature(S_TPBP_PARSER* const pParser, uint8_t* const pTemperatureCelsius);                                 // 63
DLL_EXPORT bool      Tpbp_ParserRead_MemoryImageTag(S_TPBP_PARSER* const pParser, uint16_t* const pMemoryImageTag);                                     // 64
DLL_EXPORT bool      Tpbp_ParserRead_Command_PacketType(S_TPBP_PARSER* const pParser, uint8_t* const pPacketType);                                      // 65
DLL_EXPORT bool      Tpbp_ParserRead_Command_Destination(S_TPBP_PARSER* const pParser, uint8_t* const pPacketType);                                     // 66
DLL_EXPORT bool      Tpbp_ParserRead_MetadataForPacket(S_TPBP_PARSER* const pParser, uint64_t* const pStartTime, uint64_t* const pEndTime);             // 67
DLL_EXPORT bool      Tpbp_ParserRead_CmiuId(S_TPBP_PARSER* const pParser, uint32_t* const pCmiuId);                                                     // 68
DLL_EXPORT bool      Tpbp_ParserRead_Apn(S_TPBP_PARSER* const pParser, const S_TPBP_TAG* const pTag, char* const pApnBuffer, const uint32_t bufferSize);// 68
DLL_EXPORT bool      Tpbp_ParserRead_Command_Btle_RFTestMode(S_TPBP_PARSER* const pParser, S_COMMAND_BTLE_RFTESTMODE* const pCmdRFTestMode);            // 70
DLL_EXPORT bool      Tpbp_ParserRead_PacketInstigator(S_TPBP_PARSER* const pParser, uint8_t* const pPacketInstigator);                                  // 72
DLL_EXPORT bool      Tpbp_Packer_BeginSecurePack(S_TPBP_PACKER* const pPacker);
DLL_EXPORT uint8_t*  Tpbp_Packer_EndSecurePack(S_TPBP_PACKER* const pPacker, uint32_t* pNumSecureBytes);


// Helper
DLL_EXPORT bool		 Tpbp_PackerAdd_ImageVersionInfo(S_TPBP_PACKER* const pPacker, const E_TAG_NUMBER tagNumber, const S_IMAGE_VERSION_INFO* const dataToAdd);

#endif //  __TPBP_H__

/**
 * @}
 */
