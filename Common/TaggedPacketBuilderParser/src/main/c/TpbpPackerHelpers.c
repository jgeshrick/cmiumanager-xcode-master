/******************************************************************************
*******************************************************************************
**
**         Filename: Tpbp.c
**    
**           Author: Duncan Willis
**          Created: 2015.03.05
**
**     Last Edit By: Duncan Willis
**        Last Edit: 2015.04.16
**    
**      Description:  Module to pack and unpack structured data to/from flat memory
**                    for communication links.
**
**                    Usage - Refer to example \Common\TaggedPacketBuilderParser\src\main\c\main.c
**
**                    Packing:
**                    1) Declare a packer: S_TPBP_PACKER packer;
**                    2) Init it to an array of bytes:  Tpbp_PackerInit(&packer, myBuffer, sizeof(myBuffer));
**                    3) Add Packet(s) into it by calling the approriate function(s) with the structure
**                      isOk = Tpbp_PackerAdd_CmiuConfiguration(&packer, pCmiuConfiguration); 
**                    4) When packet(s) added, call   Tpbp_PackerGetCount(&packer) to determine how many bytes
**                      in myBuffer to send.
**
**                     Unpacking
**                    1) Declare a parser S_TPBP_PARSER parser;
**                    2) Point it to the bytes to be parsed:  Tpbp_ParserInit(&parser, packBuffer, sizeof(packBuffer));
**                    3) Read the first tag: isTag = Tpbp_ParserReadTag(&parser, &tag);
**                    4) If OK, switch (tag.tagNumber),  case E_TAG_NUMBER_CMIU_PACKET_HEADER:
**                    5) Read the corresponding packet: isOk = Tpbp_ParserRead_CmiuPacketHeader(&parser, &cmiuPkt);
**                    6) Repeat reading tags until none left
**
** Revision History:
** 
**
**
**      Code Review:
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
 
/**
 * @addtogroup Tpbp
 * @{
 */


/******************************************************************************
 * @file Tpbp.c
 *
 * @brief  Tagged Packet Builder Parser module
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <string.h>
#include "Tpbp.h"


/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/

  
/******************************************************************************
*  @note  
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerInit
*    
*              Author: Duncan Willis
*             Created: 9 March 2015
*
*        Last Edit By:
*           Last Edit: 
*
*  @param[in, out]   pPacker     The Packer object
*  @param[in, out]   pBuffer     The output buffer in which to assemble data
*  @param[in]        bufferSize  The size of the above  buffer in bytes
*
*  @return None
*
*  @brief Init a packer object to start writing into the specified 
*         buffer. The buffer is zeroed.
*
******************************************************************************/
void Tpbp_PackerInit(S_TPBP_PACKER* const pPacker, uint8_t* const pBuffer, const uint32_t bufferSize)
{
    ASSERT(NULL != pPacker);
    ASSERT(NULL != pBuffer);

    pPacker->bufferSize     = bufferSize;
    pPacker->pOutputBuffer  = pBuffer;
    Tpbp_PackerReset(pPacker);
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerReset
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker   The Packer object
*
*  @return None
*
*  @brief Reset an already initialized packer object to the
*         start of the buffer
*
******************************************************************************/
void Tpbp_PackerReset(S_TPBP_PACKER* const pPacker)
{
    ASSERT(NULL != pPacker);
    memset(pPacker->pOutputBuffer, 0, pPacker->bufferSize);
    pPacker->writePosition = 0;
    pPacker->secureDataPosition = 0;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerGetCount
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker  The Packer object
*
*  @return The number of bytes written so far into the buffer.
*
*  @brief Used to determine the number of bytes to send once assembly
*         is complete.
******************************************************************************/
uint32_t Tpbp_PackerGetCount(const S_TPBP_PACKER* const pPacker)
{
     ASSERT(NULL != pPacker);
     return pPacker->writePosition;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerGetAvailable
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker   The Packer object.
*
*  @return The number of bytes free in buffer
*
*  @brief Used to determine if proposed pack has enough space
******************************************************************************/
uint32_t Tpbp_PackerGetAvailable(const S_TPBP_PACKER* const pPacker)
{
    uint32_t numFreeBytes = 0;
    ASSERT(NULL != pPacker);
    numFreeBytes = pPacker->bufferSize - pPacker->writePosition;
    return numFreeBytes;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_Packer_BeginPacket
*    
*              Author: Anthony Hayward
*             Created: 20 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pPacker        The Packer object.
*  @param[in]        packetTypeId   The packet type ID byte
*
*  @return true if there was space to add the packet type ID byte
*
*  @brief Add a packet type ID.  This is the first byte of
*         a packet.
******************************************************************************/
bool Tpbp_Packer_BeginPacket(S_TPBP_PACKER* const pPacker, const E_PACKET_TYPE_ID packetTypeId)
{
    bool isOk;
    isOk = Tpbp_Pack(pPacker, packetTypeId, 1u);
    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerHelper
*    
*              Author: Duncan Willis
*             Created: 10 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker     The Packer object.
*  @param[in]       eTagNumber  The Tag Number - what it is.
*  @param[in]       eTagType    The Type of the data 
*  @param[in]       packedSize  The number of packed bytes following the tag bytes
*
*  @return True if tag bytes written OK and that enough room
*                      exists for the packed structure to come.
*
*  @brief Prepends the tag byes and checks space in preparation
*         for packing.
******************************************************************************/
 bool Tpbp_PackerHelper(S_TPBP_PACKER* const pPacker, 
    const E_TAG_NUMBER eTagNumber,
    const E_TAG_TYPE eTagType, 
    const uint8_t packedSize)
{
    bool isOk = false;
    S_TPBP_TAG tag;
    uint32_t available = 0;

    ASSERT(NULL != pPacker);
 
    tag.tagNumber   = eTagNumber;
    tag.tagType     = eTagType;
    tag.dataSize    = packedSize;

    isOk = Tpbp_PackerTagHelper(pPacker, &tag);

    available = Tpbp_PackerGetAvailable(pPacker);

    return ((true == isOk) && (available >= tag.dataSize)) ? true : false;
}
 
 
  
/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_UInt
*    
*              Author: Anthony Hayward
*             Created: 17 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker      The Packer object
*  @param[in]       tagNumber    The tag number
*  @param[in]       data         The integer value to add
*  @param[in]       numBytes     The number of bytes in the integer (1, 2 or 4)
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Add a UInt8, UInt16 or UInt32 tag
******************************************************************************/
bool Tpbp_PackerAdd_UInt(S_TPBP_PACKER* const pPacker, 
    const E_TAG_NUMBER tagNumber, 
    const uint32_t data, const uint32_t numBytes)
{
    bool isOk;
    S_TPBP_TAG tag;

    tag.tagNumber = tagNumber;

    switch (numBytes)
    {
        case sizeof(uint8_t):  {tag.tagType = E_TAG_TYPE_BYTE; break;}
        case sizeof(uint16_t): {tag.tagType = E_TAG_TYPE_U16; break;}
        case sizeof(uint32_t): {tag.tagType = E_TAG_TYPE_U32; break;}
        default: {isOk = false; break;}
    }

    tag.dataSize = numBytes;
    isOk = Tpbp_PackerTagHelper(pPacker, &tag);

    if (isOk == true)
    {
        if (Tpbp_PackerGetAvailable(pPacker) >= numBytes)
        {
            Tpbp_Pack(pPacker, data, numBytes);
        }
        else
        {
            isOk = false;
        }
    }
    return isOk;
}

/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_UInt64
*    
*              Author: Anthony Hayward
*             Created: 17 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker      The Packer object
*  @param[in]       tagNumber    The tag number
*  @param[in]       data         The integer value to add
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Add a UInt64 tag
******************************************************************************/
bool Tpbp_PackerAdd_UInt64(S_TPBP_PACKER* const pPacker, 
    const E_TAG_NUMBER tagNumber, 
    const uint64_t data)
{
    bool isOk;
    S_TPBP_TAG tag;

    tag.tagNumber = tagNumber;
    tag.tagType = E_TAG_TYPE_U64;
    tag.dataSize = sizeof(uint64_t);

    isOk = Tpbp_PackerTagHelper(pPacker, &tag);

    if (isOk == true)
    {
        if (Tpbp_PackerGetAvailable(pPacker) >= sizeof(uint64_t))
        {
            Tpbp_Pack64(pPacker, data);
        }
        else
        {
            isOk = false;
        }
    }
    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_CharArray
*    
*              Author: Anthony Hayward
*             Created: 17 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker      The Packer object
*  @param[in]       tagNumber    The tag number
*  @param[in]       stringToAdd  Null terminated string to add as string array
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Add a CharArray tag.  Will automatically use Extended
*         char array for long strings.
******************************************************************************/
bool Tpbp_PackerAdd_CharArray(S_TPBP_PACKER* const pPacker, 
    const E_TAG_NUMBER tagNumber, 
    const char* const stringToAdd)
{
    bool isOk;
    S_TPBP_TAG tag;
    uint32_t stringLength;
    const uint32_t largestInterestingStringLength = Tpbp_PackerGetAvailable(pPacker) + 1u;
    const uint8_t* const stringBytes = (const uint8_t*)stringToAdd;

    ASSERT(NULL != stringToAdd);
    stringLength = strnlen(stringToAdd, largestInterestingStringLength);

    tag.tagNumber = tagNumber;
    tag.dataSize = stringLength;
    if (stringLength > 255) //need extended char array
    {
        tag.tagType = E_TAG_TYPE_EXTENDED_CHAR_ARRAY;
    }
    else
    {
        tag.tagType = E_TAG_TYPE_CHAR_ARRAY;
    }
    isOk = Tpbp_PackerTagHelper(pPacker, &tag);
    
    if (isOk == true)
    {
        if (stringLength > Tpbp_PackerGetAvailable(pPacker))
        { //String won't fit
            isOk = false;
            ASSERT(0);
        }
        else
        {
            isOk = Tpbp_PackerAddBytes(pPacker, stringBytes, stringLength);
        }
    }

    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ExtendedUint32Array
*    
*              Author: Duncan Willis
*             Created: 9 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker      The Packer object
*  @param[in]       tagNumber    The tag number
*  @param[in]       dataToAdd    Array of U8s to add  
*  @param[in]       numElements  Array count 
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Add a E_TAG_TYPE_EXTENDED_U8_ARRAY
******************************************************************************/
bool Tpbp_PackerAdd_ExtendedUint8Array(S_TPBP_PACKER* const pPacker, 
    const E_TAG_NUMBER tagNumber, 
    const uint8_t* const dataToAdd, 
    uint32_t numElements)
{
    return Tpbp_PackerAdd_ExtendedArrayHelper(pPacker, 
        tagNumber,
        E_TAG_TYPE_EXTENDED_U8_ARRAY,
        dataToAdd, 
        numElements, 
        sizeof(uint8_t));
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ExtendedUint16Array
*    
*              Author: Duncan Willis
*             Created: 9 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker      The Packer object
*  @param[in]       tagNumber    The tag number
*  @param[in]       dataToAdd    Array of U16s to add  
*  @param[in]       numElements  Array count 
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Add a E_TAG_TYPE_EXTENDED_U16_ARRAY
******************************************************************************/
bool Tpbp_PackerAdd_ExtendedUint16Array(S_TPBP_PACKER* const pPacker, 
    const E_TAG_NUMBER tagNumber, 
    const uint16_t* const dataToAdd, 
    uint32_t numElements)
{
    return Tpbp_PackerAdd_ExtendedArrayHelper(pPacker, 
        tagNumber,
        E_TAG_TYPE_EXTENDED_U16_ARRAY,
        dataToAdd, 
        numElements, 
        sizeof(uint16_t));
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ExtendedUint32Array
*    
*              Author: Duncan Willis
*             Created: 9 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker      The Packer object
*  @param[in]       tagNumber    The tag number
*  @param[in]       dataToAdd    Array of U32s to add  
*  @param[in]       numElements  Array count 
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Add a E_TAG_TYPE_EXTENDED_U32_ARRAY
******************************************************************************/
bool Tpbp_PackerAdd_ExtendedUint32Array(S_TPBP_PACKER* const pPacker, 
    const E_TAG_NUMBER tagNumber, 
    const uint32_t* const dataToAdd, 
    uint32_t numElements)
{
    return Tpbp_PackerAdd_ExtendedArrayHelper(pPacker, 
        tagNumber,
        E_TAG_TYPE_EXTENDED_U32_ARRAY,
        dataToAdd, 
        numElements, 
        sizeof(uint32_t));
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAddBytes
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker  The Packer object.
*  @param[in]       bytes    The bytes to write to the buffer.
*  @param[in]       numBytes The number of bytes to write to the buffer.
*
*  @return true if OK, else false if insufficient space.
*
*  @brief Helper to add bytes into buffer.
******************************************************************************/
bool Tpbp_PackerAddBytes(S_TPBP_PACKER* const pPacker, const uint8_t* const bytes, const uint32_t numBytes)
{
    uint32_t numFreeBytes = pPacker->bufferSize - pPacker->writePosition;
    bool isOk = false;

    ASSERT(NULL != pPacker);
    ASSERT(NULL != bytes);

    if (numFreeBytes >= numBytes)
    {
        (void) memcpy(&(pPacker->pOutputBuffer[pPacker->writePosition]), bytes, numBytes);
        pPacker->writePosition += numBytes;
        isOk = true;
    }

    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerPadZeroToBlockBoundary
*    
*              Author: Anthony Hayward
*             Created: 14 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker     The Packer object.
*  @param[in]       blockSize   Size of the blocks in bytes.
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief Append zeros until the length of the data 
*         (pPacker->writePosition) is a multiple of blockSize
******************************************************************************/
bool Tpbp_PackerPadZeroToBlockBoundary(S_TPBP_PACKER* const pPacker, const uint32_t blockSize)
{
    bool isOk;
    const uint32_t numBytes = Tpbp_PackerGetCount(pPacker);
    
    isOk = Tpbp_PackerPadZeroHelper(pPacker, blockSize, numBytes);
    
    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerPadZeroHelper
*    
*              Author: Anthony Hayward
*             Created: 14 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker     The Packer object.
*  @param[in]       blockSize   Size of the blocks in bytes.
*  @param[in]       numBytes    Number of bytes currently existing.
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief Append zeros until the length of the data 
*         (pPacker->writePosition) is a multiple of blockSize
******************************************************************************/
bool Tpbp_PackerPadZeroHelper(S_TPBP_PACKER* const pPacker, 
    const uint32_t blockSize, 
        const uint32_t numBytes)
{
    const uint32_t numFreeBytes = Tpbp_PackerGetAvailable(pPacker);

    //number of bytes in the final block if partially full - 0 otherwise
    const uint32_t bytesInPartiallyFullBlock = numBytes % blockSize;

    //number of bytes to complete the final block if partially full - blockSize otherwise
    const uint32_t bytesToFillBlock = blockSize - bytesInPartiallyFullBlock;

    bool isOk;

    ASSERT(NULL != pPacker);
    ASSERT(blockSize > 0u);

    if (bytesInPartiallyFullBlock == 0u)
    { //no padding required
        isOk = true;
    }
    else if (numFreeBytes >= bytesToFillBlock)
    { //add bytesToFillBlock zeroes
        (void) memset(&(pPacker->pOutputBuffer[pPacker->writePosition]), 0, bytesToFillBlock);
        pPacker->writePosition += bytesToFillBlock;
        isOk = true;
    }
    else
    { //not enough space to pad
        isOk = false;
    }

    return isOk;
}

/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerTagHelper
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker  The Packer object.
*  @param[in]       pTag     A tag structure to write to the buffer.
*
*  @return true if OK, else false if insufficient space.
*
*  @brief: Helper to prepend a tag into the buffer.
******************************************************************************/
bool Tpbp_PackerTagHelper(S_TPBP_PACKER* const pPacker, const S_TPBP_TAG* const pTag)
{
    bool isOk = true;
    uint32_t numFreeBytes = pPacker->bufferSize - pPacker->writePosition;
    uint32_t tagIdTemp = pTag->tagNumber;
    uint32_t arrayElementCount;

    ASSERT(NULL != pPacker);
    ASSERT(NULL != pTag);

    while (tagIdTemp > 255u)
    {
        if (numFreeBytes < 1u)
        {
            isOk = false;
            break;
        }
        PACK_U8(pPacker, E_TAG_NUMBER_EXTENSION_256);
        tagIdTemp -= 256u;
        numFreeBytes--;
    }

    if (isOk == true)
    {
        if (numFreeBytes < 1u)
        {
            isOk = false; //no room for tag ID
        }
        else
        {
            PACK_U8(pPacker, tagIdTemp);
            numFreeBytes--;

            if (pTag->tagType == E_TAG_TYPE_NONE)
            {
                isOk = true; //finished writing tag marker
            }
            else
            {
                if (numFreeBytes < 1u)
                {
                    isOk = false;
                }
                else 
                {
                    PACK_U8(pPacker, pTag->tagType);
                    numFreeBytes--;

                    //get the count of array elements
                    switch (pTag->tagType)
                    {
                    case E_TAG_TYPE_BYTE_RECORD:
                    case E_TAG_TYPE_U8_ARRAY:
                    case E_TAG_TYPE_CHAR_ARRAY:
                    case E_TAG_TYPE_EXTENDED_BYTE_RECORD:
                    case E_TAG_TYPE_EXTENDED_U8_ARRAY:
                    case E_TAG_TYPE_EXTENDED_CHAR_ARRAY:
                        {
                        arrayElementCount = pTag->dataSize / sizeof(uint8_t);
                        }
                        break;

                    case E_TAG_TYPE_U16_ARRAY:
                    case E_TAG_TYPE_EXTENDED_U16_ARRAY:
                        {
                        arrayElementCount = pTag->dataSize / sizeof(uint16_t);
                        }
                        break;

                    case E_TAG_TYPE_U32_ARRAY:
                    case E_TAG_TYPE_EXTENDED_U32_ARRAY:
                        {
                        arrayElementCount = pTag->dataSize / sizeof(uint32_t);
                        }
                        break;

                    case E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY:
                        {
                        arrayElementCount = pTag->dataSize / 16u;
                        }
                        break;

                    default:
                        {
                        arrayElementCount = 1u;
                        }
                    }
        
                    //write the number of array elements to the tag header
                    switch (pTag->tagType)
                    {
                    case E_TAG_TYPE_BYTE_RECORD:
                    case E_TAG_TYPE_U8_ARRAY:
                    case E_TAG_TYPE_CHAR_ARRAY:
                    case E_TAG_TYPE_U16_ARRAY:
                    case E_TAG_TYPE_U32_ARRAY:
                        {
                            if (numFreeBytes >= 1u) 
                            {
                                PACK_U8(pPacker, arrayElementCount);
                                numFreeBytes--;
                                isOk = true;
                            }
                            else
                            {
                                isOk = false;
                            }
                        }
                        break;

                    case E_TAG_TYPE_EXTENDED_BYTE_RECORD:
                    case E_TAG_TYPE_EXTENDED_U8_ARRAY:
                    case E_TAG_TYPE_EXTENDED_CHAR_ARRAY:
                    case E_TAG_TYPE_EXTENDED_U16_ARRAY:
                    case E_TAG_TYPE_EXTENDED_U32_ARRAY:
                    case E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY:
                        {
                            if (numFreeBytes >= 2u) 
                            {
                                PACK_U16(pPacker, arrayElementCount);
                                numFreeBytes -= 2;
                                isOk = true;
                            }
                            else
                            {
                                isOk = false;
                            }
                        }
                        break;
    
                    default:
                        {
                            isOk = true;
                            //do nothing - no element count for this tag
                        }
                        break;
                    }

                } //endif enough space to write tag type
            } //endif tagType != NONE
        } //endif enough space to write tagID
    }

    return isOk;
}
 

/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_Pack
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker  The Packer object.
*  @param[in]       n        The number to write (up to 32 bit)
*  @param[in]       numBytes The number of bytes in n; 1,2 or 4
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief Write a word of given size to buffer, in little endian format
******************************************************************************/
bool Tpbp_Pack(S_TPBP_PACKER* const pPacker, const uint32_t n, const uint32_t numBytes)
{
    bool isOk = false;
    uint8_t bytes[4];
    uint32_t i;
	uint32_t valueTemp = n;

    ASSERT(NULL != pPacker);

    // Little endian = lowest number in highest addr
    if (numBytes <= 4)
    {
        for (i = 0; i < numBytes; i++)
        {
            bytes[i] = (uint8_t)(valueTemp & 0xFFul);
            valueTemp >>= 8;
        }
    }
    else
    {
        ASSERT(0);
    }

    isOk  = Tpbp_PackerAddBytes(pPacker, bytes, numBytes);

    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_Pack
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker  The Packer object.
*  @param[in]       pData        The array of numbers to write
*  @param[in]       elementSize The size of element in array in bytes in n; 1,2 or 4
*  @param[in]       numElements The count of element in array 
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief Write array words of given size to buffer, in little endian format
******************************************************************************/
DLL_EXPORT bool Tpbp_PackMultiple(S_TPBP_PACKER* const pPacker, 
    const void* pData, 
    const uint32_t elementSize, 
    const uint32_t numElements)
{
    const uint8_t* p8;
    const uint16_t* p16;
    const uint32_t* p32;
    uint32_t i;
    bool isOk = true;

    switch (elementSize)
    {
    case 1:
        {
            p8 = (uint8_t*)pData;
            for (i = 0; (i < numElements) && isOk; i++)
            {
                Tpbp_Pack(pPacker, (uint32_t)(*p8), elementSize);
                p8++;
            }
        }
        break;

    case 2:
        {
            p16 = (uint16_t*)pData;
            for (i = 0; (i < numElements) && isOk; i++)
            {
                Tpbp_Pack(pPacker, (uint32_t)(*p16), elementSize);
                p16++;
            }
        }
        break;    

    case 4:
        {
            p32 = (uint32_t*)pData;
            for (i = 0; (i < numElements) && isOk; i++)
            {
                Tpbp_Pack(pPacker, (uint32_t)(*p32), elementSize);
                p32++;
            }
        }
        break;

    default:
        ASSERT(0);
        break;
    }

    return isOk;
}



/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_Pack64
*    
*              Author: Anthony Hayward
*             Created: 14 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker  The Packer object.
*  @param[in]       n        The number to write
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief Write a 64-bit integer of given size to buffer, in little endian format
******************************************************************************/
bool Tpbp_Pack64(S_TPBP_PACKER* const pPacker, const uint64_t n)
{
    bool isOk = false;
    uint8_t bytes[8];

    ASSERT(NULL != pPacker);

    bytes[0] = (uint8_t)((n >> (8 * 0)) & 0xFFul);
    bytes[1] = (uint8_t)((n >> (8 * 1)) & 0xFFul);
    bytes[2] = (uint8_t)((n >> (8 * 2)) & 0xFFul);
    bytes[3] = (uint8_t)((n >> (8 * 3)) & 0xFFul);
    bytes[4] = (uint8_t)((n >> (8 * 4)) & 0xFFul);
    bytes[5] = (uint8_t)((n >> (8 * 5)) & 0xFFul);
    bytes[6] = (uint8_t)((n >> (8 * 6)) & 0xFFul);
    bytes[7] = (uint8_t)((n >> (8 * 7)) & 0xFFul);
    
    isOk  = Tpbp_PackerAddBytes(pPacker, bytes, 8u);

    return isOk;
}

/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_ExtendedArrayHelper
*    
*              Author: Duncan Willis
*             Created: 9 June 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker      The Packer object
*  @param[in]       tagNumber    The tag number
*  @param[in]       tagType      The tag type
*  @param[in]       dataToAdd    Array to add  
*  @param[in]       numElements  Array count 
*  @param[in]       elementSize  Array element size 1, 2 or 4 
*
*  @return True if successfully written else false (insufficient space)
*
*  @brief Helper to tag and pack data of given size - EXTENDED format (2 byte length)
******************************************************************************/
bool Tpbp_PackerAdd_ExtendedArrayHelper(S_TPBP_PACKER* const pPacker, 
    const E_TAG_NUMBER tagNumber, 
    const E_TAG_TYPE    tagType, 
    const void* const dataToAdd, 
    uint32_t numElements, 
    uint32_t elementSize)
{
    bool isOk;
    S_TPBP_TAG tag;

    tag.tagNumber   = tagNumber;
    tag.dataSize    = numElements * elementSize;

    switch (elementSize)
    {
    case sizeof(uint8_t):
        tag.tagType     = E_TAG_TYPE_EXTENDED_U8_ARRAY;
        break;    
    case sizeof(uint16_t):
        tag.tagType     = E_TAG_TYPE_EXTENDED_U16_ARRAY;
        break;    
    case sizeof(uint32_t):
        tag.tagType     = E_TAG_TYPE_EXTENDED_U32_ARRAY;
        break;
    default: 
        ASSERT(0);
        break;
    }
         
    isOk = Tpbp_PackerTagHelper(pPacker, &tag);

    if (isOk == true)
    {
        if (tag.dataSize > Tpbp_PackerGetAvailable(pPacker))
        {
            isOk = false;
            ASSERT(0);
        }
        else
        {
            isOk = Tpbp_PackMultiple(pPacker, (void*)dataToAdd, elementSize, numElements);
        }
    }

    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_PackerAdd_TimeAndDate
*
*              Author:
*             Created:
*
*        Last Edit By:
*           Last Edit:
*
*  @param[in,]  pParser  The Packer object.
*  @param[in,]  eTagNumber  The Tag Number  .
*  @param[in]   timeDate    64 bit time
*
*  @return true if OK, else false if insufficient space available.
*
*  @brief
******************************************************************************/
bool Tpbp_PackerAdd_TimeAndDateHelper(S_TPBP_PACKER* const pPacker, 
    E_TAG_NUMBER eTagNumber, 
    const uint64_t timeDate)
{
    return Tpbp_PackerAdd_UInt64(pPacker, eTagNumber,timeDate);
}





/**
 * @}
*/




