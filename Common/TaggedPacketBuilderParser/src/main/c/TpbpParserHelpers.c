/******************************************************************************
*******************************************************************************
**
**         Filename: TpbpParserHelpers.c
**    
**           Author: Duncan Willis
**          Created: 2015.06.30
**
**     Last Edit By: Duncan Willis
**        Last Edit: 2015.07.01
**    
**      Description:  Module to pack and unpack structured data to/from flat memory
**                    for communication links.
**
**                    Usage - Refer to example \Common\TaggedPacketBuilderParser\src\main\c\main.c
**
**                    Packing:
**                    1) Declare a packer: S_TPBP_PACKER packer;
**                    2) Init it to an array of bytes:  Tpbp_PackerInit(&packer, myBuffer, sizeof(myBuffer));
**                    3) Add Packet(s) into it by calling the approriate function(s) with the structure
**                      isOk = Tpbp_PackerAdd_CmiuConfiguration(&packer, pCmiuConfiguration); 
**                    4) When packet(s) added, call   Tpbp_PackerGetCount(&packer) to determine how many bytes
**                      in myBuffer to send.
**
**                     Unpacking
**                    1) Declare a parser S_TPBP_PARSER parser;
**                    2) Point it to the bytes to be parsed:  Tpbp_ParserInit(&parser, packBuffer, sizeof(packBuffer));
**                    3) Read the first tag: isTag = Tpbp_ParserReadTag(&parser, &tag);
**                    4) If OK, switch (tag.tagNumber),  case E_TAG_NUMBER_CMIU_PACKET_HEADER:
**                    5) Read the corresponding packet: isOk = Tpbp_ParserRead_CmiuPacketHeader(&parser, &cmiuPkt);
**                    6) Repeat reading tags until none left
**
** Revision History:
** 
**
**
**      Code Review:
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
 
/**
 * @addtogroup Tpbp
 * @{
 */


/******************************************************************************
 * @file TpbpParserHelpers.c
 *
 * @brief  Tagged Packet Builder Parser module
 * @author Duncan Willis
 * @date 2015.07.01
 * @version 1.0
 *****************************************************************************/


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <string.h>
#include "Tpbp.h"


/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserInit
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker     The Parser object.
*  @param[in]       pBuffer     A buffer to be parsed
*  @param[in]       bufferSize  The number of bytes in the input buffer.
*
*  @return none.
*
*  @brief Initialize a Parser object.
******************************************************************************/
void Tpbp_ParserInit(S_TPBP_PARSER* const pParser, 
    const uint8_t* const pBuffer, 
    const uint32_t bufferSize)
{
    ASSERT(NULL != pParser);
    ASSERT(NULL != pBuffer);

    pParser->bufferSize     = bufferSize;
    pParser->pInputBuffer   = pBuffer;
    pParser->readPosition   = 0;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserGetBytesRemaining
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker  The Parser object.
*
*  @return number of bytes still to be read.
*
*  @brief Gets the amount of bytes remaining to be read from the 
*         buffer
******************************************************************************/
uint32_t Tpbp_ParserGetBytesRemaining(const S_TPBP_PARSER* const pParser)
{  
    uint32_t bytesRemaining = 0;

    ASSERT(NULL != pParser);
    bytesRemaining = pParser->bufferSize - pParser->readPosition;

    return bytesRemaining;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserGetBytes
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser   The Parser object.
*  @param[out]      pBuffer   A buffer to get the data
*  @param[in]       numBytes  The number of bytes to get.
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief If sufficient bytes available, reads numBytes from
*         the parser's inputbuffer into pBuffer.
******************************************************************************/
bool Tpbp_ParserGetBytes(S_TPBP_PARSER* const pParser, 
    uint8_t* const pBuffer, 
    const uint32_t numBytes)
{
    uint32_t numUnreadBytes = Tpbp_ParserGetBytesRemaining(pParser); //pParser->bufferSize - pParser->readPosition;
    bool isOk = false;
   
    ASSERT(NULL != pParser);
    ASSERT(NULL != pBuffer);

    if (numUnreadBytes >= numBytes)
    {
        memcpy(pBuffer, &(pParser->pInputBuffer[pParser->readPosition]),  numBytes);
        pParser->readPosition += numBytes;
        isOk = true;
    }

    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserRead_PacketTypeId
*    
*              Author: Duncan Willis
*             Created: 3 July 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]   pParser        The Parser object. 
*
*  @return The packet type ID byte
*
*  @brief Read a packet type ID.  This is the first byte of  a packet.
******************************************************************************/
E_PACKET_TYPE_ID Tpbp_ParserRead_PacketTypeId(S_TPBP_PARSER* const pParser)
{
    uint32_t numUnreadBytes = 0;
    E_PACKET_TYPE_ID packetTypeOut = E_PACKET_TYPE_COUNT; // Default to n/a

    ASSERT(NULL != pParser);
    
    numUnreadBytes = pParser->bufferSize - pParser->readPosition;
    if (numUnreadBytes >= 1)
    {
        packetTypeOut = (E_PACKET_TYPE_ID) Tpbp_Unpack(pParser, 1);
    }

    return packetTypeOut;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserReadTag
*    
*              Author: Duncan Willis
*             Created: 6 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker  The Parser object.
*  @param[out]      pTag     A structure to get the tag
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief If sufficient bytes available, reads numBytes from
*         the parser's inputbuffer into pBuffer.
******************************************************************************/
bool Tpbp_ParserReadTag(S_TPBP_PARSER* const pParser, S_TPBP_TAG* const pTag)
{
    bool        isOk = false;
	uint32_t    arrayElements = 0;
	uint8_t     tempTagId = 0;
    uint32_t    computeTagId = 0;
    uint32_t    computeTagType = 0;

    ASSERT(NULL != pParser);
    ASSERT(NULL != pTag); 

    do
    {
        isOk = Tpbp_ParserGetBytes(pParser, &tempTagId, 1);
        if (tempTagId == E_TAG_NUMBER_EXTENSION_256) 
        {
            computeTagId += 256u;
        }
        else
        {
            computeTagId += tempTagId;
            break;
        }
    } while (isOk);

    pTag->tagNumber = (E_TAG_NUMBER)computeTagId;

    if( pTag->tagNumber == E_TAG_NUMBER_NULL ||
        pTag->tagNumber == E_TAG_NUMBER_EOF	)
    { 
        pTag->dataSize = 0u;
        pTag->tagType = E_TAG_TYPE_NONE;
    }
    else
    {
        if (Tpbp_ParserGetBytesRemaining(pParser) < 1)
        {
            isOk = false;
        }
        else
        {
            computeTagType = Tpbp_Unpack(pParser, 1);
            pTag->tagType = (E_TAG_TYPE)computeTagType;

            //get the number of array elements
            switch (pTag->tagType)
            {
            case E_TAG_TYPE_BYTE_RECORD:
            case E_TAG_TYPE_CHAR_ARRAY:
            case E_TAG_TYPE_U8_ARRAY:
            case E_TAG_TYPE_U16_ARRAY:
            case E_TAG_TYPE_U32_ARRAY:
            case E_TAG_TYPE_U64_ARRAY:
            case E_TAG_TYPE_SECURE_BLOCK_ARRAY:
                {
                    if (Tpbp_ParserGetBytesRemaining(pParser) >= 1) 
                    {
                        arrayElements = Tpbp_Unpack(pParser, 1);
                    }
                    else
                    {
                        arrayElements = 0u;
                        isOk = false;
                    }
                }
                break;

            case E_TAG_TYPE_EXTENDED_BYTE_RECORD:
            case E_TAG_TYPE_EXTENDED_U8_ARRAY:
            case E_TAG_TYPE_EXTENDED_CHAR_ARRAY:
            case E_TAG_TYPE_EXTENDED_U16_ARRAY:
            case E_TAG_TYPE_EXTENDED_U32_ARRAY:
            case E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY:
                {
                    if (Tpbp_ParserGetBytesRemaining(pParser) >= 2) 
                    {
                        arrayElements = Tpbp_Unpack(pParser, 2);
                    }
                    else
                    {
                        arrayElements = 0u;
                        isOk = false;
                    }
                }
                break;
    
            default:
                {
                    arrayElements = 0u;
                }
                break;
            }

            switch (pTag->tagType)
            {
            case E_TAG_TYPE_BYTE_RECORD:
            case E_TAG_TYPE_CHAR_ARRAY:
            case E_TAG_TYPE_U8_ARRAY:
            case E_TAG_TYPE_EXTENDED_BYTE_RECORD:
            case E_TAG_TYPE_EXTENDED_CHAR_ARRAY:
            case E_TAG_TYPE_EXTENDED_U8_ARRAY:
                {
                    pTag->dataSize = arrayElements * 1u;
                }
                break;

            case E_TAG_TYPE_U16_ARRAY:
            case E_TAG_TYPE_EXTENDED_U16_ARRAY:
                {
                    pTag->dataSize = arrayElements * 2u;
                }
                break;

            case E_TAG_TYPE_U32_ARRAY:
            case E_TAG_TYPE_EXTENDED_U32_ARRAY:
                {
                    pTag->dataSize = arrayElements * 4u;
                }
                break;

            case E_TAG_TYPE_U64_ARRAY:
                {
                    pTag->dataSize = arrayElements * 8u;
                }
                break;

            case E_TAG_TYPE_SECURE_BLOCK_ARRAY:
            case E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY:
                {
                    pTag->dataSize = arrayElements * 16u;
                }
                break;
    
            case E_TAG_TYPE_BYTE:
                {
                    pTag->dataSize = 1u;
                }
                break;

            case E_TAG_TYPE_U16:
                {
                    pTag->dataSize = 2u;
                }
                break;

            case E_TAG_TYPE_U32:
                {
                    pTag->dataSize = 4u;
                }
                break;

            case E_TAG_TYPE_U64:
                {
                    pTag->dataSize = 8u;
                }
                break;

            case E_TAG_TYPE_IMAGE_VERSION_INFO:
                {
                    pTag->dataSize = S_IMAGE_VERSION_INFO_PACKED_SIZE;
                }
                break;

            default:
                {
                    pTag->dataSize = 0u;
                    isOk = false;
                }
                break;
            }
        } //endif enough bytes in buffer to read tag type
    }

    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserSkipTagData
*    
*              Author: Anthony Hayward
*             Created: 17 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker  The Parser object.
*  @param[in]       pTag     The tag preamble just read
*
*  @return true if OK, else false if insufficient data available.
*
*  @brief Skip to next tag.  Call after Tpbp_ParserReadTag.
******************************************************************************/
bool Tpbp_ParserSkipTagData(S_TPBP_PARSER* const pParser, const S_TPBP_TAG* const pTag)
{
    uint32_t    numUnreadBytes = pParser->bufferSize - pParser->readPosition;
    bool        isOk = false;
   
    ASSERT(NULL != pParser);
    
    if (numUnreadBytes >= pTag->dataSize)
    {
        pParser->readPosition += pTag->dataSize;
        isOk = true;
    }

    return isOk;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_ParserFindTag
*    
*              Author: Anthony Hayward
*             Created: 17 April 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pPacker           The Parser object.  Modified to point to
*                                     the start of the data of the desired tag 
*                                     if found, otherwise not modified
*  @param[in]       desiredTagNumber  The tag to find
*  @param[out]      pTagFound         Populated with tag if found
*
*  @return true if found at position >= start, false otherwise
*
*  @brief Find a tag of the required number starting at the current
*         position.
******************************************************************************/
bool Tpbp_ParserFindTag(S_TPBP_PARSER* const pParser, const E_TAG_NUMBER desiredTagNumber, S_TPBP_TAG* const pTagFound)
{
    bool found = false;
    bool isOk;
    bool keepSearching = true;
    S_TPBP_PARSER searchParser;
    S_TPBP_TAG tag;

    (void) memcpy(&searchParser, pParser, sizeof(S_TPBP_PARSER));

    pTagFound->tagNumber = E_TAG_NUMBER_NULL;
    pTagFound->tagType = E_TAG_TYPE_NONE;
    pTagFound->dataSize = 0;

    while (keepSearching)
    {
        isOk = Tpbp_ParserReadTag(&searchParser, &tag);

        if (isOk != true)
        {
            keepSearching = false;
        }
        else
        {
            if (tag.tagNumber == desiredTagNumber)
            {
                found = true;
                keepSearching = false;
                (void) memcpy(pTagFound, &tag, sizeof(S_TPBP_TAG));
                (void) memcpy(pParser, &searchParser, sizeof(S_TPBP_PARSER));
            }
            else
            {
                Tpbp_ParserSkipTagData(&searchParser, &tag);
            }
        }
    }
    return found;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_Unpack
*    
*              Author: Duncan Willis
*             Created: 10 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser   The Parser object.
*  @param[in]       numBytes  The number of bytes in n; 1, 2 or 4
*
*  @return The number read (up to 32 bit), LS aligned
*
*  @brief Read a word of given size from buffer, in little endian format
*                      If less that a 32bit, then bytes are bottom aligned
******************************************************************************/
uint32_t Tpbp_Unpack(S_TPBP_PARSER* const pParser, const uint32_t numBytes)
{
    bool isOk = false;
    uint8_t  bytes[4];
    uint32_t returnValue = 0;
    uint32_t temp = 0;
    uint32_t s = 0;
    uint32_t i;
    
    ASSERT(NULL  != pParser); 

    // Little endian = lowest byte in highest addr
    if (numBytes <= 4)
    {
        isOk = Tpbp_ParserGetBytes(pParser, bytes,  numBytes);

        if (true == isOk)
        {
            for (i = 0; i < numBytes; i++)
            {
                temp = (uint32_t)(bytes[i]);
                temp <<= s;
                s += 8;
                returnValue |= temp;
            }
        }
    }
    else
    {
        ASSERT(0);
    }

    return returnValue;
}


/******************************************************************************
*  @note
*            Filename: Tpbp.c
*       Function Name: Tpbp_Unpack64
*    
*              Author: Duncan Willis
*             Created: 10 March 2015
*
*        Last Edit By: 
*           Last Edit: 
*    
*  @param[in, out]  pParser  The Parser object. 
*
*  @return a 64 bit word (0 if insufficient data available).
*
*  @brief Read a 64 bit word. Note the amount of bytes in the 
*         parser should be prechecked before calling this, 
*         Otherwise an assertion will fail.
******************************************************************************/
uint64_t Tpbp_Unpack64(S_TPBP_PARSER* const pParser)
{
    uint32_t available = 0;
    uint32_t nl = 0;
    uint32_t nh = 0;
    uint64_t result = 0;

    ASSERT(NULL  != pParser);

    available = Tpbp_ParserGetBytesRemaining(pParser);

    if (available >= 8)
    {
        nl = Tpbp_Unpack(pParser, 4);
        nh = Tpbp_Unpack(pParser, 4);
        result  = ((uint64_t)nh) << 32;
        result |= (uint64_t)nl;
    }
    else
    {
        ASSERT(0);
    }

    return result;
}


bool Tpbp_ParseByte(S_TPBP_PARSER* const pParser, 
    uint8_t* const pValue)
{
    bool            isOk = false;
    uint32_t        value;
    const uint32_t  numBytes = sizeof(uint8_t);

    if (Tpbp_ParserGetBytesRemaining(pParser) >= numBytes)
    {
        value = Tpbp_Unpack(pParser, numBytes);
        *pValue = (uint8_t)value;
        isOk = true;
    } 
    
    return isOk;
}


bool Tpbp_ParseUInt16(S_TPBP_PARSER* const pParser, 
    uint16_t* const pValue)
{
    bool            isOk = false;
    uint32_t        value;
    const uint32_t  numBytes = sizeof(uint16_t);

    if (Tpbp_ParserGetBytesRemaining(pParser) >= numBytes)
    {
        value = Tpbp_Unpack(pParser, numBytes);
        *pValue = (uint16_t)value;
        isOk = true;
    }
    
    return isOk;
}

bool Tpbp_ParseUInt32(S_TPBP_PARSER* const pParser, 
    uint32_t* const pValue)
{
    bool            isOk = false;
    uint32_t        value;
    const uint32_t  numBytes = sizeof(uint32_t);

    if (Tpbp_ParserGetBytesRemaining(pParser) >= numBytes)
    {
        value = Tpbp_Unpack(pParser, numBytes);
        *pValue = (uint32_t)value;
        isOk = true;
    }
    
    return isOk;
}


bool Tpbp_ParseUInt64(S_TPBP_PARSER* const pParser, 
    uint64_t* const pValue)
{
    bool            isOk = false;
    const uint32_t  numBytes = sizeof(uint64_t);

    if (Tpbp_ParserGetBytesRemaining(pParser) >= numBytes)
    {
        *pValue = Tpbp_Unpack64(pParser);
        isOk = true;
    }
    
    return isOk;
}


/**
 * @}
*/




