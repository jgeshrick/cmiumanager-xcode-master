/*****************************************************************************
******************************************************************************
**
**         Filename: IntegrationTestTpbpWindows.c
**    
**           Author: Duncan Willis
**          Created: 20/04/2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file IntegrationTestTpbpWindows.c
*
* @brief Runner for Windows test
* 
******************************************************************************/

/**
 * @addtogroup Tpbp 
 * @{
 */

#include <stdio.h>
#include "IntegrationTestTpbp.h"

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{    
    return(IntegrationTestTpbp());
}

/**
 * @}
*/

