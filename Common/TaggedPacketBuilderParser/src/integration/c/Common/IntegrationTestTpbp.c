/*****************************************************************************
******************************************************************************
**
**         Filename: IntegrationTestTpbp.h
**    
**           Author: Duncan Willis
**          Created: 20/04/2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file IntegrationTestTpbp.c
*
* @brief Based on following definition from ETI 48-01
* 
* 1	CMIU packet header 	Uint8 Array
* 254	Begin secure data 	Secure Data array
* 2	CMIU configuration	Uint8 Array
* 3	Network operator status	Char Array
* 4	Network performance 	Char Array
* 5	Registration status	Char Array
* 6	Host  IP address 	Char Array
* 7	Fall back Host IP address 	Char Array
* 8	Assigned CMIU IP address	Char Array
* 9	FTP DNS address	Char Array
* 10	FTP port number	uint16_t
* 11	FTP user name	Char Array
* 12	FTP password	Char Array
* 13	Not defined
* 14	Not defined
* 15	Modem hardware revision	Char Array
* 16	The device model identification code	Char Array
* 17	Device manufacturer identification code	Char Array
* 18	Device software revision number	Char Array
* 19	The product serial number, identified as the IMEI of the mobile	Char Array
* 37	MSISDN request	Char Array
* 20	Sim IMSI	Char Array
* 21	Sim Card ID	Char Array
* 22	PIN/PUK/PUK2 request status of the device 	Char Array
* 23	CMIU Diagnostics	Uint8 Array
* 24	BLE last user logged in ID	Char Array
* 25	BLE last user logged in date	Uint64
* 31	Connection timing log	Uint8 Array
* 0	Null Filler	Uint8
* 255	EOF 	Uint8
* 
******************************************************************************/

/**
 * @addtogroup Tpbp 
 * @{
 */

/**
 * @file
 *
 * @brief  Tagged Packet Builder Parser module. For Win32 or ARM target hardware.
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 */

#include <stdio.h>
#include "unity.h"
#include "TeamCity.h"
#include "Tpbp.h"

/*===========================================================================*/
/*  V A R I A B L E S                                                        */
/*===========================================================================*/

/* size of buffer used to build packets, in bytes */
#define TAGGED_PACKET_BUFFER_SIZE 1024

/* Buffer for assembling packed tags */
static uint8_t packetBuffer[TAGGED_PACKET_BUFFER_SIZE];

/* Buffer for assembling packed tags within secure data block */
static uint8_t secureDataBuffer[TAGGED_PACKET_BUFFER_SIZE];

/* Buffer for encrypted secureDataBUffer */
static uint8_t encryptedSecureDataBuffer[TAGGED_PACKET_BUFFER_SIZE];

// Instantiate a packer object for the packet as a whole
static S_TPBP_PACKER packetPacker;

// Instantiate a packer object for the secure data block contents
static S_TPBP_PACKER secureDataPacker;

// Instantiate a Parser object
static S_TPBP_PARSER parser;


// Example structure to write
static S_CMIU_PACKET_HEADER cmiuPacketHeaderIn;

static S_CMIU_CONFIGURATION cmiuConfigurationIn;

static S_CMIU_DIAGNOSTICS cmiuDiagnosticsIn;

static S_CONNECTION_TIMING_LOG connectionTimingLogIn;

static S_TPBP_TAG tag;


//for consistency check
static S_CMIU_PACKET_HEADER cmiuPacketHeaderOut;

static char textBuffer[TAGGED_PACKET_BUFFER_SIZE];


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/
static void Tpbp_BuildAndParseDetailedConfigurationPacket(void);
static void Tpbp_BuildAndParseIntervalDataPacket(void);
static void Tpbp_ParseCommandPacket(void);
static void fakeEncrypt(const uint8_t* const dataIn, uint8_t* const encryptedDataOut, const uint32_t byteCount);
static void fakeDecrypt(const uint8_t* const encryptedDataIn, uint8_t* const dataOut, const uint32_t byteCount);
static uint32_t readAndPrintTagSequence(S_TPBP_PARSER* const parser);
static void makeCmiuPacketheader(S_CMIU_PACKET_HEADER* p);
static bool Tpbp_MakeIntervalDataPacket(S_TPBP_PACKER* const pPacker, 
    S_TPBP_PACKER* const  pSecurePacker,
    const S_CMIU_PACKET_HEADER* const pCmiuPacketHeader,
    const S_CMIU_STATUS* const pCmiuStatusFlags,
    const S_REPORTED_DEVICE_CONFIG* const pReportedDeviceConfig,
    const S_INTERVAL_RECORDING_CONFIG*  pIntervalRecordingConfig,
    const uint32_t r900Data[],
    uint32_t const r900DataNumElements);


/******************************************************************************
 * @brief
 *   Unity integration functions
 *
 *****************************************************************************/
void setUp(void)
{
}

void tearDown(void)
{
}

void resetTest(void)
{
    tearDown();
    setUp();
}


/******************************************************************************
 * @brief
 *   Tpbp integration test runner
 *
 * @return Number of failed tests
 *****************************************************************************/
int IntegrationTestTpbp(void)
{
    int testFailures = 0;

    Unity.TestFile = "IntegrationTestTpbp.c";
    UnityBegin();

    printf("Testing module %s\n", Unity.TestFile);

    RUN_TEST(Tpbp_BuildAndParseDetailedConfigurationPacket, __LINE__);
    RUN_TEST(Tpbp_BuildAndParseIntervalDataPacket,          __LINE__);

    RUN_TEST(Tpbp_ParseCommandPacket,                       __LINE__);

    return testFailures;
}


static bool HandleImageUpdateCommand(S_TPBP_PARSER* secureTagSequenceParser)
{
    bool                    isOk                    = true;
    S_TPBP_TAG              tag;
    S_IMAGE_METADATA        imageMetadata;
    S_IMAGE_VERSION_INFO    imageVersionInfo;
    uint64_t                applyAtTime;
    const char*             imageUrl;

    isOk &= Tpbp_ParserFindTag(secureTagSequenceParser, E_TAG_NUMBER_TIMESTAMP, &tag);
    
    if (isOk == true)
    {
        applyAtTime = Tpbp_Unpack64(secureTagSequenceParser);
        isOk &= Tpbp_ParserFindTag(secureTagSequenceParser, E_TAG_NUMBER_IMAGE, &tag);
    }
    if (isOk == true)
    {
        isOk &= Tpbp_ParserRead_String(secureTagSequenceParser, &tag, textBuffer, sizeof(textBuffer));
        imageUrl = textBuffer;
    }
    if (isOk == true)
    {
        isOk &= Tpbp_ParserFindTag(secureTagSequenceParser, E_TAG_NUMBER_IMAGE_METADATA, &tag);
    }
    if (isOk == true)
    {
        isOk &= Tpbp_ParserRead_ImageMetadata(secureTagSequenceParser, &imageMetadata);
    }
    if (isOk == true)
    {
        isOk &= Tpbp_ParserFindTag(secureTagSequenceParser, E_TAG_NUMBER_FIRMWARE_REVISION, &tag);
    }
    if (isOk == true)
    {
        isOk &= Tpbp_ParserRead_ImageVersionInfo(secureTagSequenceParser, &imageVersionInfo);
    }
    //the rest is just 0s so ignore

    printf("Image update packet parsed successfully? %s\n", isOk ? "Yes" : "No");

    if (isOk == true)
    {
        printf("Image update to be applied at UNIX time: %lld, download from URL %s, image type %d, start address %d, length %d, version %x.%x.%x.%x\n", 
            applyAtTime, imageUrl, imageMetadata.imageType, imageMetadata.startAddress, imageMetadata.imageSize,
            imageVersionInfo.versionMajorBcd, imageVersionInfo.versionMinorBcd, imageVersionInfo.versionYearMonthDayBcd, imageVersionInfo.versionBuildBcd);
    }
    return isOk;
}


static bool HandleCommandPacket(S_TPBP_PARSER* const outerPacketParser)
{    

    bool                isOk                    = true;
    S_TPBP_TAG          tag;
    S_MQTT_BLE_UART     packetHeader;
    uint32_t            secureDataSizeInBytes;
    S_TPBP_PARSER       secureTagSequenceParser;
    E_COMMAND           commandId               = E_COMMAND_COUNT;
 

    if (isOk == true)
    {
        isOk &= Tpbp_ParserFindTag(outerPacketParser, E_TAG_NUMBER_UART_PACKET_HEADER, &tag);
    }
    if (isOk == true)
    {
        isOk &= Tpbp_ParserRead_UartPacketHeader(outerPacketParser, &packetHeader);
    }
    if (isOk == true)
    {
        isOk &= Tpbp_ParserFindTag(outerPacketParser, E_TAG_NUMBER_SECURE_DATA, &tag);
    }
    if (isOk == true)
    {
        isOk &= Tpbp_ParserGetBytes(outerPacketParser, encryptedSecureDataBuffer, tag.dataSize);
                    
        if (isOk)
        {
            secureDataSizeInBytes = tag.dataSize;
        }
    }
    if (isOk == true)
    {
        // "Decrypt" secure data (actually just copy)
        memcpy(secureDataBuffer, encryptedSecureDataBuffer, secureDataSizeInBytes);

        // Init a parser to the decrypted data
        Tpbp_ParserInit(&secureTagSequenceParser, secureDataBuffer, secureDataSizeInBytes);

        isOk &= Tpbp_ParserFindTag(&secureTagSequenceParser, E_TAG_NUMBER_COMMAND, &tag);
    }
    if (isOk == true)
    {
        // Get command number
        
        commandId = (E_COMMAND)Tpbp_Unpack(&secureTagSequenceParser, tag.dataSize);
    }
    if (isOk == true)
    {
        switch (commandId)
        {
        case E_COMMAND_UPDATE_IMAGE:
            isOk &= HandleImageUpdateCommand(&secureTagSequenceParser);
            break;
        default:
            isOk = false;
            break;
        }
    }
    
    printf("Command packet parsed successfully? %s\n", isOk ? "Yes" : "No");

    return isOk;

}


static void Tpbp_ParseCommandPacket(void)
{
    
    const uint8_t commandPacketData1[] = {5, //(+1)
        35, 5, 4, 0, 0, 0, 0, //header (+8)
        254, 0x11, 4, /*#16 byte blocks =4 (64 bytes) (+11) */
        32, 1, 0,   //image update (+3)
        50, 4, 0, 0, 0, 0, 0, 0, 0, 0, //apply immediately (+13)
        46, 8, 11, 'h', 't', 't', 'p', ':', '/', '/', 'a', 'b', 'c', 'd', //(+27)
        47, 5, 9, 0, /*FW image*/ 0, 0, 0, 0, /*write to +0*/ 12, 0, 0, 0, /*12 bytes*/ //(+39)
        39, 0x0e, 0x01, 0x02, 0x00, 0x15, 0x03, 0x07, 0x00, 0x00, 0x12, 0x34, //1.2.150307.1234 //(+51)
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //(+64)
        255
    };

    const uint8_t commandPacketData2[] = { //packet build by the mdce-integration application using Java packet-utils
        0x05,
        0x23,
        0x05,
        0x04,
        0x04,
        0x00,
        0x00,
        0x00,
        0xfe,
        0x11,
        0x05,
        0x20,
        0x01,
        0x00,
        0x32,
        0x04,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x2e,
        0x0c,
        0x25,
        0x00,
        0x68,
        0x74,
        0x74,
        0x70,
        0x3a,
        0x2f,
        0x2f,
        0x31,
        0x30,
        0x2e,
        0x31,
        0x32,
        0x30,
        0x2e,
        0x30,
        0x2e,
        0x31,
        0x37,
        0x34,
        0x2f,
        0x4e,
        0x65,
        0x70,
        0x74,
        0x75,
        0x6e,
        0x65,
        0x50,
        0x72,
        0x6f,
        0x74,
        0x6f,
        0x32,
        0x2e,
        0x62,
        0x69,
        0x6e,
        0x2f,
        0x05,
        0x09,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x27,
        0x0e,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0xff
    };

    S_TPBP_PARSER parser;
    E_PACKET_TYPE_ID packetTypeOut;
    bool isOk = true;

    //first packet

    Tpbp_ParserInit(&parser, commandPacketData1, sizeof(commandPacketData1)); 

    packetTypeOut = Tpbp_ParserRead_PacketTypeId(&parser);
    
    if (packetTypeOut == E_PACKET_TYPE_COMMAND)
    {

        isOk &= HandleCommandPacket(&parser);
    }
    else
    {
        isOk = false;
    }

    printf("Packet 1 parsed successfully? %s\n", isOk ? "Yes" : "No");

    //second packet

    isOk = true;

    Tpbp_ParserInit(&parser, commandPacketData2, sizeof(commandPacketData2)); 

    packetTypeOut = Tpbp_ParserRead_PacketTypeId(&parser);
    
    if (packetTypeOut == E_PACKET_TYPE_COMMAND)
    {

        isOk &= HandleCommandPacket(&parser);
    }
    else
    {
        isOk = false;
    }

    printf("Packet 2 parsed successfully? %s\n", isOk ? "Yes" : "No");

}



/******************************************************************************
 * @brief
 *   Test to build and parse a detailed configuration packet
 *
 * @note
 *   Builds and parsers a complete detailed configuration packet, and performs
 *   placeholder "encryption" for reference.
 *
 * @return None
 *****************************************************************************/
void Tpbp_BuildAndParseDetailedConfigurationPacket(void)
{
    bool isOk = false;
    int packetTypeOut = -1;
    uint32_t secureDataSizeInBytes;

    Tpbp_PackerInit(&packetPacker, packetBuffer, sizeof(packetBuffer));

    //add packet type ID
    isOk = Tpbp_Packer_BeginPacket(&packetPacker, E_PACKET_TYPE_DETAILED_CONFIG);

    if (isOk == true)
    {
        makeCmiuPacketheader(&cmiuPacketHeaderIn);

        //append packet header tag
        isOk &= Tpbp_PackerAdd_CmiuPacketHeader(&packetPacker, &cmiuPacketHeaderIn);
    }
    if (isOk == true)
    {

        //remaining tags are within secure block so prepare a second packer to 
        //prepare the tags prior to encryption

        Tpbp_PackerInit(&secureDataPacker, secureDataBuffer, sizeof(secureDataBuffer));

        //prepare data for the CMIU configuration tag
        cmiuConfigurationIn.cmiuType = 10u;
        cmiuConfigurationIn.hwRevision = 11u;


        cmiuConfigurationIn.fwRelease                            = 0x1235u;
        cmiuConfigurationIn.fwRevDate                            = 0x12345678u;
        cmiuConfigurationIn.fwBuildNum                           = 0xfedcba98u;

        cmiuConfigurationIn.blRelease                            = 0x2225u;
        cmiuConfigurationIn.blRevDate                            = 0x22242628u;
        cmiuConfigurationIn.blBuildNum                           = 0x2e2c2a28u;

        cmiuConfigurationIn.configRelease                        = 0xF2F5u;
        cmiuConfigurationIn.configRevDate                        = 0xF2F4F6F8u;
        cmiuConfigurationIn.configBuildNum                       = 0xFeFcFaF8u;

        cmiuConfigurationIn.manufactureDate = 14u;
        cmiuConfigurationIn.initialCallInTime = 15u;
        cmiuConfigurationIn.callInInterval = 16u;
        cmiuConfigurationIn.callInWindow = 17u;
        cmiuConfigurationIn.reportingwindowNRetries = 18u;
        cmiuConfigurationIn.quietTimeStartMins = 19u;
        cmiuConfigurationIn.quietTimeEndMins = 20u;
        cmiuConfigurationIn.numAttachedDevices = 21u;
        cmiuConfigurationIn.installationDate = 22u;
        cmiuConfigurationIn.dateLastMagSwipe = 23u;
        cmiuConfigurationIn.magSwipes = 24u;
        cmiuConfigurationIn.batteryRemaining = 25u;
        cmiuConfigurationIn.timeErrorLastNetworkTimeAccess = 26uLL;

        // Add CMIU configuration tag at start of secure buffer
        isOk &= Tpbp_PackerAdd_CmiuConfiguration(&secureDataPacker, &cmiuConfigurationIn); 
    }
    //add 21 char array tags to the secure buffer
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_NETWORK_OPERATORS, "3:27");
    }
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_NETWORK_PERFORMANCE, "4:28");
    }
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_REGISTRATION_STATUS, "5:29");
    }
    if (isOk == true)
    {        
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_HOST_ADDRESS, "6:30");
    }
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_FALLBACK_HOST_ADDRESS, "7:31");
    }
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_ASSIGNED_CMIU_ADDRESS, "8:32");
    }
    if (isOk == true)
    {
        Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_FTP_DNS_ADDRESS, "9:33");
    }
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_FTP_PORT_NUMBER, "10:34");
    }
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_FTP_USERNAME, "11:35");
    }
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_FTP_PASSWORD, "12:36");
    }
    if (isOk == true)
    {        
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_MODEM_HARDWARE_REVISION, "15:39");
    }
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_MODEM_MODEL_IDENTIFICATION_CODE, "16:40");
    }
    if (isOk == true)
    {
        Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_MODEM_MANUFACTURER_ID_CODE, "17:41");
    }
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_MODEM_SOFTWARE_REVISION, "18:42");
    }
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_IMEI, "19:43");
    }
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_MSISDN_REQUEST, "37:44");
    }
    if (isOk == true)
    {
        Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_SIM_IMSI, "20:45");
    }
    if (isOk == true)
    {        
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_SIM_CARD_ID, "21:46");
    }
    if (isOk == true)
    {        
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_PINPUKPUK2_REQUEST_STATUS, "22:47");
    }
    if (isOk == true)
    {
        
        //prepare data for CMIU Diagnostics tag
        cmiuDiagnosticsIn.diagnosticsResult = 48u;
        cmiuDiagnosticsIn.processorResetCounter = 49u;

        //append CMIU Diagnostics tag to secure data section
        isOk &= Tpbp_PackerAdd_CmiuDiagnostics(&secureDataPacker, &cmiuDiagnosticsIn); 
    }
    if (isOk == true)
    {
        
        //add BLE info
        isOk &= Tpbp_PackerAdd_CharArray(&secureDataPacker, E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_ID, "24:50");
        isOk &= Tpbp_PackerAdd_UInt64(&secureDataPacker, E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_DATE, 51uLL);
    }
    if (isOk == true)
    {
        
        //prepare data for connection timing log
        connectionTimingLogIn.dateOfConnection = 52uLL;
        connectionTimingLogIn.timeFromPowerOnToNetworkRegistration = 53u;
        connectionTimingLogIn.timeFromNetworkRegistrationToContextActivation = 54u;
        connectionTimingLogIn.timeFromContextActivationToServerConnection = 55u;
        connectionTimingLogIn.timeToTransferMessageToServer = 56u;
        connectionTimingLogIn.timeToDisconnectAndShutdown = 57u;

        //append connection timing log to secure data section
        isOk &= Tpbp_PackerAdd_ConnectionTimingLog(&secureDataPacker, &connectionTimingLogIn);
    }
    if (isOk == true)
    {
        
        //pad secure data section to multiple of 16 bytes
        isOk &= Tpbp_PackerPadZeroToBlockBoundary(&secureDataPacker, 16u);
    }
    if (isOk == true)
    {
        
        secureDataSizeInBytes = Tpbp_PackerGetCount(&secureDataPacker);

        //fake encrypt the data
        fakeEncrypt(secureDataBuffer, encryptedSecureDataBuffer, secureDataSizeInBytes);

        //append the secure data block to the packet data

        isOk &= Tpbp_PackerAdd_SecureBlockArray(&packetPacker, encryptedSecureDataBuffer, secureDataSizeInBytes);
    }
    if (isOk == true)
    {
        
        //append EOF tag
        isOk &= Tpbp_Packer_EndPacket(&packetPacker);
    }

    printf("Packet creation okay? %s\n", isOk ? "yes" : "no");

    //packet is now done.  Now read it back

    (void) memset(encryptedSecureDataBuffer, 0, sizeof(encryptedSecureDataBuffer));
    (void) memset(secureDataBuffer, 0, sizeof(secureDataBuffer));
    isOk = true;

    // Init a parser to the newly-written packet data buffer
    Tpbp_ParserInit(&parser, packetBuffer, sizeof(packetBuffer));

    //read the packet type ID

    packetTypeOut = Tpbp_Unpack(&parser, 1);
    
    printf("Parsing packet: Packet type %d\n", packetTypeOut);

    printf("---begin packet data tags---\n");

    secureDataSizeInBytes = readAndPrintTagSequence(&parser);

    printf("---end packet data tags---\n");

    if (secureDataSizeInBytes > 0u)
    {

        printf("Found secure data block of %d bytes\n---begin secure data tags---\n", secureDataSizeInBytes);

        //have secure data
        fakeDecrypt(encryptedSecureDataBuffer, secureDataBuffer, secureDataSizeInBytes);

        // Init a parser to the decrypted data
        Tpbp_ParserInit(&parser, secureDataBuffer, secureDataSizeInBytes);

        //print the tags in the secure data
        readAndPrintTagSequence(&parser);

        printf("---end secure data tags---\n");

        // Init a parser to the decrypted data
        Tpbp_ParserInit(&parser, secureDataBuffer, secureDataSizeInBytes);

        //find hardware revision
        if (Tpbp_ParserFindTag(&parser, E_TAG_NUMBER_MODEM_HARDWARE_REVISION, &tag))
        {
            Tpbp_ParserGetBytes(&parser, (uint8_t*)textBuffer, tag.dataSize);
            textBuffer[tag.dataSize] = 0;
            
            printf("modem hardware version found by search: %s\n", textBuffer);

        }
    }
}

/******************************************************************************
 * @brief
 *   Test to build and parse an Interval Data Packet; Packet Type = 1 (E_PACKET_TYPE_INTERVAL_DATA)
 *
 * @note
 *   Builds and parsers a complete Interval Data Packet, and performs
 *   placeholder "encryption" for reference.
 *
 * @return None
 *****************************************************************************/
void Tpbp_BuildAndParseIntervalDataPacket(void)
{
    bool isOk = false;
    int                                 packetTypeOut = -1;
    uint32_t                            secureDataSizeInBytes;
    static S_CMIU_STATUS                cmiuStatusFlags;
    S_CMIU_STATUS                       readbackStatus;
    static S_REPORTED_DEVICE_CONFIG     reportedDeviceConfig;
    static S_INTERVAL_RECORDING_CONFIG  intervalRecordingConfig;  
    uint32_t r900Data[4] = {1,2,3,4};
    
    Tpbp_PackerInit(&packetPacker, packetBuffer, sizeof(packetBuffer));
    Tpbp_PackerInit(&secureDataPacker, secureDataBuffer, sizeof(secureDataBuffer));

    //Set up some Tags' data
    makeCmiuPacketheader(&cmiuPacketHeaderIn);

    cmiuStatusFlags.cmiuType                = 0x11;
    cmiuStatusFlags.numberOfDevices         = 0x22;
    cmiuStatusFlags.cmiuFlags               = 0x3344;

    reportedDeviceConfig.deviceNumber       = 0xDD;
    reportedDeviceConfig.attachedDeviceId   = 0x0123456789ABCDEFull;
    reportedDeviceConfig.deviceType         = 0x12EF;
    reportedDeviceConfig.deviceFlags        = 0xCABD;

    intervalRecordingConfig.deviceNumber         = 0xAD;
    intervalRecordingConfig.intervalFormat       = 0x45;
    intervalRecordingConfig.recordingIntervalMin = 0x46;
    intervalRecordingConfig.reportingIntervalHrs = 0x47;
    intervalRecordingConfig.intervalStartTime    = 0x4849;
    intervalRecordingConfig.intervalLastRecordDateTime = 0X0123456789ABCDEfull;

    isOk = Tpbp_MakeIntervalDataPacket(&packetPacker, &secureDataPacker,
        &cmiuPacketHeaderIn,
        &cmiuStatusFlags,
        &reportedDeviceConfig,
        &intervalRecordingConfig,
        r900Data ,
        sizeof(r900Data)/sizeof(uint32_t));

    printf("Packet creation okay? %s\n", isOk ? "yes" : "no");

    // packet is now made.  Now read it back

    (void) memset(encryptedSecureDataBuffer, 0, sizeof(encryptedSecureDataBuffer));
    (void) memset(secureDataBuffer, 0, sizeof(secureDataBuffer));
    isOk = true;

    // Init a parser to the newly-written packet data buffer
    Tpbp_ParserInit(&parser, packetBuffer, sizeof(packetBuffer));

    // Read the packet type ID
    packetTypeOut = Tpbp_Unpack(&parser, 1);
    
    printf("\nParsing packet: Packet type %d\n", packetTypeOut);

    printf("---begin packet data tags---\n");

    secureDataSizeInBytes = readAndPrintTagSequence(&parser);

    printf("---end packet data tags---\n\n");

    if (secureDataSizeInBytes > 0u)
    {
        printf("Found secure data block of %d bytes\n---begin secure data tags---\n", secureDataSizeInBytes);

        // Decrypt secure data
        fakeDecrypt(encryptedSecureDataBuffer, secureDataBuffer, secureDataSizeInBytes);

        // Init a parser to the decrypted data
        Tpbp_ParserInit(&parser, secureDataBuffer, secureDataSizeInBytes);

        // print the tags in the secure data
        readAndPrintTagSequence(&parser);

        printf("---end secure data tags---\n\n");

        // Init a parser to the decrypted data
        Tpbp_ParserInit(&parser, secureDataBuffer, secureDataSizeInBytes);

        // find CMIU Status (cmiu type field)
        if (Tpbp_ParserFindTag(&parser, E_TAG_NUMBER_CMIU_FLAGS, &tag))
        {
            Tpbp_ParserRead_CmiuStatus(&parser, &readbackStatus);

            printf("CMIU Flags tag found by search: cmiuType = 0x%02x\n", readbackStatus.cmiuType);
        }
    }
}


static uint32_t readAndPrintTagSequence(S_TPBP_PARSER* const pParser)
{
    bool isDone = false;
    bool isTag 	= false;
    bool isOk = true;
    uint32_t secureDataLength = 0u;

        // Read tags in turn until done.
    while (false == isDone)
    {
        isTag = Tpbp_ParserReadTag(pParser, &tag);

        if (true == isTag)
        {
            printf("Tag %d type %d\n", tag.tagNumber, tag.tagType);
            switch(tag.tagNumber)
            {
            case E_TAG_NUMBER_CMIU_PACKET_HEADER:
                isOk = Tpbp_ParserRead_CmiuPacketHeader(pParser, &cmiuPacketHeaderOut);
                if (isOk)
                {
                    // HandleCmuiHeaderPkt(cmiuPkt); //call a type specific handler
                }
                else
                {
                    isDone = true; // Bad error!
                }
                break;

            case E_TAG_NUMBER_SECURE_DATA:

                isOk = Tpbp_ParserGetBytes(pParser, encryptedSecureDataBuffer, tag.dataSize);
                if (isOk)
                {
                    secureDataLength = tag.dataSize;
                }

                break;

            case E_TAG_NUMBER_EOF:

                isDone = true;
                break;

            default:

                isOk = Tpbp_ParserSkipTagData(pParser, &tag);
                break;
            }
            if (isOk != true)
            {
                isDone = true;
            }
        }
        else
        {
            isDone = true;
        }
    }
    return secureDataLength;

}

/*
Placeholder for AES256 encryption method
*/
static void fakeEncrypt(const uint8_t* const dataIn, uint8_t* const encryptedDataOut, const uint32_t byteCount)
{
    uint32_t i;
    for (i = 0u; i < byteCount; i++)
    {
        encryptedDataOut[i] = dataIn[i] ^ 0x55;
    }
}

/*
Placeholder for AES256 decryption method
*/
static void fakeDecrypt(const uint8_t* const encryptedDataIn, uint8_t* const dataOut, const uint32_t byteCount)
{
    uint32_t i;
    for (i = 0u; i < byteCount; i++)
    {
        dataOut[i] = encryptedDataIn[i] ^ 0x55;
    }
}


// helper to populate a CMIU Packet header for test
static void makeCmiuPacketheader(S_CMIU_PACKET_HEADER* p)
{
        
        p->cmiuId = 1u;  
        p->sequenceNumber = 3u;  
        p->keyInfo = 4u; 
        p->encryptionMethod = 5u; 
        p->tokenAesCrc = 6u;  
        p->networkFlags = 7u; 
        p->cellularRssiAndBer = 8u;
        p->timeAndDate = 9ull; 
}



/******************************************************************************
 * @brief
 *   Test to build and parse an Interval Data Packet; 
 *   Packet Type = 1 (E_PACKET_TYPE_INTERVAL_DATA)
 *
 * @note
 *   Builds and parsers a complete Interval Data Packet , and performs
 *   placeholder "encryption" for reference.
 *
 * @return true if packed OK, else false
 *****************************************************************************/
static bool Tpbp_MakeIntervalDataPacket(S_TPBP_PACKER* const pPacker, 
    S_TPBP_PACKER* const  pSecurePacker,
    const S_CMIU_PACKET_HEADER* const pCmiuPacketHeader,
    const S_CMIU_STATUS* const pCmiuStatusFlags,
    const S_REPORTED_DEVICE_CONFIG* const pReportedDeviceConfig,
    const S_INTERVAL_RECORDING_CONFIG*  pIntervalRecordingConfig,
    const uint32_t r900Data[],
    uint32_t const r900DataNumElements)
{
    bool        isOk = false;
    uint32_t    secureDataSizeInBytes; 

    // add packet type ID
    isOk = Tpbp_Packer_BeginPacket(pPacker, E_PACKET_TYPE_INTERVAL_DATA);
    
    // Make and append packet header tag
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_CmiuPacketHeader(pPacker, pCmiuPacketHeader);
    }

    // remaining tags are within secure block so prepare a second packer to 
    // prepare the tags prior to encryption
    // Tag Number 26; CMIU Flags (STATUS)
    if (isOk == true)
    { 
        isOk &= Tpbp_PackerAdd_CmiuStatus(pSecurePacker, pCmiuStatusFlags);
    }

    // Tag Number 27; Reported Dev Config
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_ReportedDeviceConfig(pSecurePacker, pReportedDeviceConfig );
    }

    // Tag Number 34; Interval Recording Config
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_IntervalRecordingConfig(pSecurePacker, pIntervalRecordingConfig );
    }

    // Add Tag Number 28; r900 Interval Recording Config
    if (isOk == true)
    {
        isOk &= Tpbp_PackerAdd_ExtendedUint32Array(pSecurePacker, E_TAG_NUMBER_R900_INTERVAL_DATA, r900Data, r900DataNumElements);
    }

    if (isOk == true)
    {
        // pad secure data section to multiple of 16 bytes
        isOk &= Tpbp_PackerPadZeroToBlockBoundary(pSecurePacker, 16u);
    }

    if (isOk == true)
    {
        secureDataSizeInBytes = Tpbp_PackerGetCount(pSecurePacker);

        // fake encrypt the data
        fakeEncrypt(secureDataBuffer, encryptedSecureDataBuffer, secureDataSizeInBytes);

        // append the secure data block to the packet data
        isOk &= Tpbp_PackerAdd_SecureBlockArray(pPacker, encryptedSecureDataBuffer, secureDataSizeInBytes);
    }
       
    // append EOF tag
    if (isOk == true)
    {
        isOk &= Tpbp_Packer_EndPacket(pPacker);
    }
    
    return isOk;
}

/**
 * @}
 * @}
*/
