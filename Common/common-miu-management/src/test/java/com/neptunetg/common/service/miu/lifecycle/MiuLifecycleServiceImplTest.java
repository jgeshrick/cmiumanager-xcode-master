/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.service.miu.lifecycle;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.JdbcMiuLifecycleRepo;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

/**
 * Created by WJD1 on 20/05/2016.
 * Tests for the lifecycle service class
 */
public class MiuLifecycleServiceImplTest
{
    private EmbeddedDatabase dataSource;
    private JdbcTemplate db;
    private JdbcMiuLifecycleRepo jdbcCmiuLifecycleRepo;
    private MiuLifecycleServiceImpl service;
    private static final MiuId cmiuId = MiuId.valueOf(400000200);
    private static final MiuId extraCmiuId = MiuId.valueOf(400000400);
    private static final MiuId nonExistentCmiuId = MiuId.valueOf(12341234);

    @Before
    public void setUp()
    {
        this.dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("db/create-lifecycle-test-db.sql")
                .build();

        this.db = new JdbcTemplate(dataSource);
        this.jdbcCmiuLifecycleRepo = new JdbcMiuLifecycleRepo(this.db);
        this.service = new MiuLifecycleServiceImpl(this.jdbcCmiuLifecycleRepo);
    }

    @Test
    public void testGetLifecycleStateFromNonExistentCmiu()
    {
        assertNull(service.getMiuLifecycleState(nonExistentCmiuId));
    }

    @Test
    public void testGetLifecycleStateWhereStateIsNull()
    {
        assertNull(service.getMiuLifecycleState(cmiuId).getLifecycleState());
    }

    @Test
    public void testSetAndGetLifecycleState()
    {
        assertTrue(service.setMiuLifecycleState(cmiuId, MiuLifecycleStateEnum.ACTIVATED));
        assertEquals(MiuLifecycleStateEnum.ACTIVATED,
                service.getMiuLifecycleState(cmiuId).getLifecycleState());
    }

    @Test
    public void testSetDisallowedLifecycleState()
    {
        assertTrue(service.setMiuLifecycleState(cmiuId, MiuLifecycleStateEnum.DEAD));
        assertFalse(service.setMiuLifecycleState(cmiuId, MiuLifecycleStateEnum.ACTIVATED));
        assertEquals(MiuLifecycleStateEnum.DEAD, service.getMiuLifecycleState(cmiuId).getLifecycleState());
    }

    @Test
    public void testSetAndGetLifecycleStateForNonExistentCmiu()
    {
        assertFalse(service.setMiuLifecycleState(nonExistentCmiuId, MiuLifecycleStateEnum.ACTIVATED));
        assertNull(service.getMiuLifecycleState(nonExistentCmiuId));
    }

    @Test
    public void testSetAndGetLifecycleHistory() throws InterruptedException
    {
        EnumSet<MiuLifecycleStateEnum> lifecycleSet = EnumSet.of(MiuLifecycleStateEnum.ACTIVATED,
                MiuLifecycleStateEnum.HEARD,
                MiuLifecycleStateEnum.PREPREPOT,
                MiuLifecycleStateEnum.PREPOT,
                MiuLifecycleStateEnum.POSTPOT,
                MiuLifecycleStateEnum.CONFIRMED,
                MiuLifecycleStateEnum.SCANNED,
                MiuLifecycleStateEnum.SHIPPED,
                MiuLifecycleStateEnum.UNCLAIMED,
                MiuLifecycleStateEnum.CLAIMED,
                MiuLifecycleStateEnum.RMAED,
                MiuLifecycleStateEnum.DEAD);

        for(MiuLifecycleStateEnum enumVal : lifecycleSet)
        {
            service.setMiuLifecycleState(cmiuId, enumVal);
            Thread.sleep(5);
        }

        List<MiuLifecycleState> lifecycleStateHistory = service.getMiuLifecycleStateHistory(cmiuId);
        Instant previousInstant = lifecycleStateHistory.get(0).getTransitionInstant().minus(10, ChronoUnit.MILLIS);

        Iterator<MiuLifecycleStateEnum> enumSetIterator = lifecycleSet.iterator();

        for(MiuLifecycleState lifecycleState : lifecycleStateHistory)
        {
            assertTrue(lifecycleState.getTransitionInstant().isAfter(previousInstant));
            previousInstant = lifecycleState.getTransitionInstant();

            assertEquals(cmiuId, lifecycleState.getMiuId());

            assertEquals(enumSetIterator.next(), lifecycleState.getLifecycleState());
        }
    }

    @Test
    public void testGetLifecycleHistoryFromNonExistentCmiu()
    {
        assertNull(service.getMiuLifecycleStateHistory(nonExistentCmiuId));
    }

    @Test
    public void testGetLifecycleHistoryFromCmiuWithNoHistory()
    {
        assertEquals(0, service.getMiuLifecycleStateHistory(cmiuId).size());
    }

    @Test
    public void testGetCmiusInLifecycleStateBefore() throws InterruptedException
    {
        assertTrue(service.setMiuLifecycleState(cmiuId, MiuLifecycleStateEnum.ACTIVATED));
        assertTrue(service.setMiuLifecycleState(extraCmiuId, MiuLifecycleStateEnum.HEARD));
        Thread.sleep(5);
        List<MiuLifecycleState> stateList = service.getCmiusInLifecycleStateBefore(MiuLifecycleStateEnum.ACTIVATED,
                Instant.now());
        assertEquals(1, stateList.size());
        assertEquals(cmiuId, stateList.get(0).getMiuId());
        assertEquals(MiuLifecycleStateEnum.ACTIVATED, stateList.get(0).getLifecycleState());
    }

    @Test
    public void testGetCmiusInLifecycleStateBeforeWhenAllNullStates() throws InterruptedException
    {
        assertEquals(0, service.getCmiusInLifecycleStateBefore(MiuLifecycleStateEnum.ACTIVATED, Instant.now()).size());
    }

    @Test
    public void testGetCmiusInLifecycleStateBeforeWhenNoneBefore() throws InterruptedException
    {
        Instant beforeTransition = Instant.now().minus(10, ChronoUnit.MILLIS);
        assertTrue(service.setMiuLifecycleState(cmiuId, MiuLifecycleStateEnum.ACTIVATED));
        assertTrue(service.setMiuLifecycleState(extraCmiuId, MiuLifecycleStateEnum.HEARD));
        assertTrue(service.getCmiusInLifecycleStateBefore(
                MiuLifecycleStateEnum.ACTIVATED, beforeTransition).size() == 0);
    }

    @Test
    public void testGetCmiusInLifecycleStateWithPreviousStateSetBefore() throws InterruptedException
    {
        assertTrue(service.setMiuLifecycleState(cmiuId, MiuLifecycleStateEnum.ACTIVATED));
        Thread.sleep(2);
        Instant afterStateTransistion = Instant.now();
        Thread.sleep(2);
        assertTrue(service.setMiuLifecycleState(cmiuId, MiuLifecycleStateEnum.POSTPOT));

        assertTrue(service.setMiuLifecycleState(extraCmiuId, MiuLifecycleStateEnum.ACTIVATED));
        assertTrue(service.setMiuLifecycleState(extraCmiuId, MiuLifecycleStateEnum.POSTPOT));

        List<MiuLifecycleStateTransistion> cmiusBeforeInstant = service.getMiusInLifecycleStateWithPreviousStateSetBefore(
                MiuLifecycleStateEnum.POSTPOT,
                MiuLifecycleStateEnum.ACTIVATED, afterStateTransistion);

        assertEquals(1, cmiusBeforeInstant.size());
        assertEquals(cmiuId, cmiusBeforeInstant.get(0).getCurrentMiuLifecycleState().getMiuId());
        assertEquals(cmiuId, cmiusBeforeInstant.get(0).getPreviousMiuLifecycleState().getMiuId());
        assertTrue(cmiusBeforeInstant.get(0).getCurrentMiuLifecycleState().getTransitionInstant().isAfter(afterStateTransistion));
        assertTrue(cmiusBeforeInstant.get(0).getPreviousMiuLifecycleState().getTransitionInstant().isBefore(afterStateTransistion));
        assertTrue(MiuLifecycleStateEnum.POSTPOT.equals(cmiusBeforeInstant.get(0).getCurrentMiuLifecycleState().getLifecycleState()));
        assertTrue(MiuLifecycleStateEnum.ACTIVATED.equals(cmiusBeforeInstant.get(0).getPreviousMiuLifecycleState().getLifecycleState()));
    }

    @Test
    public void testGetCmiusInLifecycleStateWithPreviousStateSetBeforeWhenEmpty() throws InterruptedException
    {
        List<MiuLifecycleStateTransistion> cmiusBeforeInstant = service.getMiusInLifecycleStateWithPreviousStateSetBefore(
                MiuLifecycleStateEnum.POSTPOT,
                MiuLifecycleStateEnum.ACTIVATED, Instant.now());

        assertEquals(0, cmiusBeforeInstant.size());
    }

    @Test
    public void testGetCmiusInLifecycleStateWithPreviousStateSetBeforeWhenNoneBefore() throws InterruptedException
    {
        Instant beforeTransition = Instant.now().minus(10, ChronoUnit.MILLIS);
        assertTrue(service.setMiuLifecycleState(cmiuId, MiuLifecycleStateEnum.ACTIVATED));
        assertTrue(service.setMiuLifecycleState(cmiuId, MiuLifecycleStateEnum.POSTPOT));

        List<MiuLifecycleStateTransistion> cmiusBeforeInstant = service.getMiusInLifecycleStateWithPreviousStateSetBefore(MiuLifecycleStateEnum.POSTPOT, MiuLifecycleStateEnum.ACTIVATED, beforeTransition);

        assertEquals(0, cmiusBeforeInstant.size());
    }

    @After
    public void tearDown()
    {
        this.dataSource.shutdown();
    }
}



























