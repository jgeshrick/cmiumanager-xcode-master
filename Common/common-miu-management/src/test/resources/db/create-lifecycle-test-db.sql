--Skeleton database for testing the lifecycle state handling code

CREATE SCHEMA mdce;

CREATE TABLE IF NOT EXISTS mdce.miu_details
(
  miu_id INT UNSIGNED NOT NULL,
  PRIMARY KEY(miu_id),
  state varchar(20), --State stored with varchar as H2 doesn't support ENUM
  last_state_change_timestamp DATETIME
)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS mdce.miu_lifecycle_state
(
  miu_id INT UNSIGNED NOT NULL,
  miu_lifecycle_state_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  timestamp DATETIME,
  PRIMARY KEY (miu_lifecycle_state_id),
  state varchar(20), --State stored with varchar as H2 doesn't support ENUM
  CONSTRAINT fk_miu_lifecycle_state_miu_details1
    FOREIGN KEY (miu_id)
    REFERENCES miu_details (miu_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB;

INSERT INTO mdce.miu_details (miu_id) VALUES (400000200);
INSERT INTO mdce.miu_details (miu_id) VALUES (400000400);
