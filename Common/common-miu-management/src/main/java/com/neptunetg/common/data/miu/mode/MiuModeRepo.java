/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.mode;


import com.neptunetg.common.data.miu.MiuId;

/**
 * Class to get current and reported MIU Mode
 */
public interface MiuModeRepo
{
    /**
     * Get the current mode of the MIU
     * @param miuId the miu ID to get the mode for
     * @return The mode of the Miu
     */
    MiuMode getMiuCurrentMode(MiuId miuId);

    /**
     * Get the reported mode of the MIU
     * @param miuId the miu ID to get the mode for
     * @param recInt the recording interval reported by the Miu
     * @param repInt the reporting interval reported by the Miu
     * @return The mode of the Miu
     */
    MiuMode getReportedMode(MiuId miuId, int recInt, int repInt);
}

