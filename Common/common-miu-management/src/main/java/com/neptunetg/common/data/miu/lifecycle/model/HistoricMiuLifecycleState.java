/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.lifecycle.model;

import com.neptunetg.common.data.miu.MiuId;

import java.time.Instant;

/**
 * Models an MIU lifecycle state for addition to the lifecycle history.
 */
public class HistoricMiuLifecycleState
{
    /**
     * A client-supplied identity used to track the status of the operation.
     */
    private int referenceId;
    private MiuId miuId;
    private MiuLifecycleStateEnum lifecycleState;
    private Instant transitionInstant;

    public int getReferenceId()
    {
        return referenceId;
    }

    public void setReferenceId(int referenceId)
    {
        this.referenceId = referenceId;
    }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public void setMiuId(MiuId miuId)
    {
        this.miuId = miuId;
    }

    public MiuLifecycleStateEnum getLifecycleState()
    {
        return lifecycleState;
    }

    public void setLifecycleState(MiuLifecycleStateEnum lifecycleState)
    {
        this.lifecycleState = lifecycleState;
    }

    public Instant getTransitionInstant()
    {
        return transitionInstant;
    }

    public void setTransitionInstant(Instant transitionInstant)
    {
        this.transitionInstant = transitionInstant;
    }
}
