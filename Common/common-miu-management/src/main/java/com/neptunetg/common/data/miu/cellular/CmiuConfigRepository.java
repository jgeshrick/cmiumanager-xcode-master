/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.miu.cellular;

import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;

import java.time.Instant;
import java.util.List;

/**
 * Interface for CMIU config set management
 */
public interface CmiuConfigRepository
{

    ////////////////////////// CONFIG SET DEFINITIONS ///////////////////////

    /**
     * Get a config set by ID
     * @param configSetId ID of the config set in the DB
     * @return The config set with the ID
     */
    ConfigSet getConfigSetFromId(long configSetId);


    /**
     * Add a new config set to the RDS
     * @param newConfigSet description of the new config set
     * @return number of rows updated
     */
    long addNewConfigSet(ConfigSet newConfigSet);

    /**
     * Update the name of config set
     * @param newConfigSetDefinition contains the new config set name
     * @return number of rows updated
     */
    int updateConfigSetName(ConfigSet newConfigSetDefinition);

    /**
     * Get the list of config set
     * @return config set list
     */
    List<ConfigSet> getConfigSetList();


    ////////////////////////// CONFIG SET ASSOCIATION TO CMIU ////////////////////

    /**
     * Get current config for the cmiu
     * @param miuId the miu id
     * @return config set definition
     */
    CmiuConfigSetAssociation getCurrentCmiuConfig(MiuId miuId) throws MiuDataException;

    /**
     * Get the config for all CMIUs with a given site ID assignment
     * @param siteId Site ID owning the CMIUs
     * @return all associations
     */
    List<CmiuConfigSetAssociation> getCurrentCmiuConfigCollection(int siteId);

    /**
     * Return all CMIU config regardless from all sites
     */
    List<CmiuConfigSetAssociation> getCurrentCmiuConfigCollection();

    /**
     * Update the CMIU configuration.  If necessary, a new configuration is created in
     * CMIU_CONFIG_SET.  The configuration is written to the CMIU_CONFIG_MGMT table,
     * and CMIU_CONFIG_HISTORY is written
     * @param miuId MIU affected
     * @param configSetDefinition Definition of config set
     * @param reportedByCmiu If true, this configuration was reported by the CMIU, otherwise
     *                       it was assigned in mdce-web
     * @param oldConfigSet The old config set for this CMIU if it exists.  May be null.
     * @param changeNote Information about where this change came from
     * @return the config set, updated with the database ID if it was not already present
     */
    ConfigSet updateCmiuConfig(MiuId miuId, ConfigSet configSetDefinition, ConfigSet oldConfigSet, boolean reportedByCmiu, String changeNote);



    ////////////////////////// CONFIG MANAGEMENT FOR CMIU ////////////////////

    List<CmiuConfigSetManagement> getCmiuConfigChangeInProcess();

    List<CmiuConfigSetManagement> getCmiuConfigChangeCompleted(int numberOfDaysAgo);

    /**
     * Get a row in the cmiu config mgmt table corresponding to the miu
     * @param cmiuId the miu id to retrieve the config management
     * @return MIU config mgmt entry
     */
    CmiuConfigSetManagement getCmiuConfigMgmt(MiuId cmiuId);



    ////////////////////////// CONFIG HISTORY ////////////////////

    /**
     * Get a list of config history in descending date order using the supplied query options.
     * @param queryOptions Defines options for filtering the results.
     * @return list of config history
     */
    List<CmiuConfigHistoryEntry> getConfigHistory(MiuConfigHistoryQueryOptions queryOptions);

    /**
     * Get a single page of configuration history starting after the supplied sequence number.
     * @param lastSequenceId The last received sequence number. When null, results begin from the first sequence
     *                       number; when non-null, results begin at the first sequence number that follows the
     *                       supplied value.
     * @return list of config history
     */
    List<SequentialCmiuConfigHistoryEntry> getSequentialConfigHistory(Integer lastSequenceId);
}
