/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.data.miu;

import java.lang.Integer;
import java.lang.String;

/**
 * Represents an MIU type - 1 = CMIU, 2 = L900, 50 = R900.  Value Object pattern.
 */
public enum MiuType
{
    CMIU(1, "CMIU"),
    L900(2, "L900"),
    R900(50, "R900");

    private final int id;
    private final String title;

    private MiuType(int id, String title)
    {
        this.id = id;
        this.title = title;
    }

    public static MiuType valueOf(int id)
    {
        for (MiuType t : values())
        {
            if (t.id == id)
            {
                return t;
            }
        }
        return null;
    }

    public static MiuType fromString(String sqlEnumString)
    {
        for(MiuType miuType : MiuType.values())
        {
            if(miuType.toString().equals(sqlEnumString))
            {
                return miuType;
            }
        }

        return null;
    }

    public Integer numericWrapperValue()
    {
        return Integer.valueOf(id);
    }


    public int numericValue()
    {
        return id;
    }

    public static String displayString(MiuType mt)
    {
        if (mt != null)
        {
            return mt.title;
        }
        else
        {
            return "(unknown)";
        }
    }

}
