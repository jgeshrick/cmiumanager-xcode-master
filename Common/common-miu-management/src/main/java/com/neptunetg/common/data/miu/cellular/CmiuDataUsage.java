/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.cellular;

import com.neptunetg.common.data.miu.MiuId;

import java.time.Instant;
import java.time.LocalDate;

/**
 * DTO for MiuDataUsage
 */
public class CmiuDataUsage
{
    private final MiuId miuId;
    private final LocalDate dataUsageDate;
    private final Instant dataUsageDatetime;
    private final int byteUsedToday;
    private final int byteUsedMonthToDate;

    public CmiuDataUsage(MiuId miuId, LocalDate dataUsageDate, Instant dataUsageDatetime, int byteUsedToday, int byteUsedMonthToDate)
    {
        this.miuId = miuId;
        this.dataUsageDate = dataUsageDate;
        this.dataUsageDatetime = dataUsageDatetime;
        this.byteUsedToday = byteUsedToday;
        this.byteUsedMonthToDate = byteUsedMonthToDate;
    }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public LocalDate getDataUsageDate()
    {
        return dataUsageDate;
    }

    public Instant getDataUsageDatetime()
    {
        return dataUsageDatetime;
    }

    public int getByteUsedToday()
    {
        return byteUsedToday;
    }

    public int getByteUsedMonthToDate()
    {
        return byteUsedMonthToDate;
    }
}

