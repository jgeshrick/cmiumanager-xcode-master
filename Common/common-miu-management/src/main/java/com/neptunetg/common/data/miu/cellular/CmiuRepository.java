/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.cellular;


import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;

import java.lang.String;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

/**
 * Repository for managing cellular MIUs
 */
public interface CmiuRepository
{

    List<CellularDeviceDetail> getCellularDeviceList() throws MiuDataException;

    /**
     * Get miu cellular details
     * @param miuId the miu id to retrieve the result
     * @return miu cellular details or null if not known
     */
    CellularDeviceDetail getCellularConfig(MiuId miuId) throws MiuDataException;

    /**
     * Given a cellular identifier, such as iccid, msisdn, retrieve the cellular device detail
     * @param deviceId iccid, msisdn, imei etc
     * @return may be null if cellular config is unknown
     */
    CellularDeviceDetail getCellularConfigFromCellularDeviceIdentifier(CellularDeviceId deviceId) throws MiuDataException;

    /**
     * Update CMIU cellular details
     * @param miuId MIU ID
     * @param cellularDeviceDetail Details to be inserted or updated
     * @throws MiuDataException If something goes wrong.  Check isConflict() on this exception to see if there was
     * an ID conflict
     */
    void updateOrInsertMiuCellularDetails(MiuId miuId, CellularDeviceDetail cellularDeviceDetail) throws MiuDataException;

    /**
     * Insert cellular usage data.
     * @param iccid iccid of the cellular device
     * @param usageDate Date of usage
     * @param bytesUsedDay      Bytes used today
     * @param bytesUsedMonth Bytes used this month
     * @throws MiuDataException
     */
    void saveMiuDataUsage(String iccid, LocalDate usageDate, Instant usageDatetime, int bytesUsedDay, long bytesUsedMonth) throws MiuDataException;

    CmiuDataUsage getLatestMiuDataUsage(MiuId miuId);

    /**
     * Change the cellular provisioning state of the device
     * @param deviceId     identify the cellular device identifier: iccid, msisdn or imei
     * @param currentProvisioningState the provisioning state of the cellular device
     * @throws MiuDataException
     */
    void changeCellularDeviceState(CellularDeviceId deviceId, String currentProvisioningState) throws MiuDataException;

    CmiuRevisionSet getCurrentRevisionSet(MiuId cmiuId);

    CmiuRevisionSet getRevisionSetAtTime(MiuId cmiuId, Instant timeStamp);

    void addRevisionSet(CmiuRevisionSet cmiuRevisionSet);

    List<String> getAllCmiuModes();

}