/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.miu.cellular;

import com.neptunetg.common.data.miu.MiuId;

import java.time.Instant;

/**
 * An association between an MIU and several config sets, with timestamps
 */
public class CmiuConfigSetManagement
{
    private long id;
    private MiuId cmiuId;
    private ConfigSet currentConfig;
    private Instant currentConfigApplyDate;
    private ConfigSet defaultConfig;
    private ConfigSet plannedConfig;
    private Instant plannedConfigApplyDate;
    private Instant plannedConfigRevertDate;
    private ConfigSet reportedConfig;
    private Instant reportedConfigDate;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public MiuId getCmiuId()
    {
        return cmiuId;
    }

    public void setCmiuId(MiuId cmiuId)
    {
        this.cmiuId = cmiuId;
    }

    public ConfigSet getCurrentConfig()
    {
        return currentConfig;
    }

    public void setCurrentConfig(ConfigSet currentConfig)
    {
        this.currentConfig = currentConfig;
    }

    public Instant getCurrentConfigApplyDate()
    {
        return currentConfigApplyDate;
    }

    public void setCurrentConfigApplyDate(Instant currentConfigApplyDate)
    {
        this.currentConfigApplyDate = currentConfigApplyDate;
    }

    public ConfigSet getDefaultConfig()
    {
        return defaultConfig;
    }

    public void setDefaultConfig(ConfigSet defaultConfig)
    {
        this.defaultConfig = defaultConfig;
    }

    public ConfigSet getPlannedConfig()
    {
        return plannedConfig;
    }

    public void setPlannedConfig(ConfigSet plannedConfig)
    {
        this.plannedConfig = plannedConfig;
    }

    public Instant getPlannedConfigApplyDate()
    {
        return plannedConfigApplyDate;
    }

    public void setPlannedConfigApplyDate(Instant plannedConfigApplyDate)
    {
        this.plannedConfigApplyDate = plannedConfigApplyDate;
    }

    public Instant getPlannedConfigRevertDate()
    {
        return plannedConfigRevertDate;
    }

    public void setPlannedConfigRevertDate(Instant plannedConfigRevertDate)
    {
        this.plannedConfigRevertDate = plannedConfigRevertDate;
    }

    public ConfigSet getReportedConfig()
    {
        return reportedConfig;
    }

    public void setReportedConfig(ConfigSet reportedConfig)
    {
        this.reportedConfig = reportedConfig;
    }

    public Instant getReportedConfigDate()
    {
        return reportedConfigDate;
    }

    public void setReportedConfigDate(Instant reportedConfigDate)
    {
        this.reportedConfigDate = reportedConfigDate;
    }

    public boolean isReportedConfigMismatchToCurrentConfig()
    {
        if (this.currentConfig == null && this.reportedConfig == null)
        {
            return false;
        }
        if (this.currentConfig == null || this.reportedConfig == null)
        {
            return true;
        }
        //neither is null
        return !this.currentConfig.hasSameConfig(this.reportedConfig);
    }


}
