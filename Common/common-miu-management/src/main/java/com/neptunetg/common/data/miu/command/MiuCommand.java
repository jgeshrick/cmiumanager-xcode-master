/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.command;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;

import java.time.Instant;

/**
 * Object to represent an entry from RDS miu command
 */
public class MiuCommand
{
    private int commandId;
    private MiuId miuId;
    private int commandType; //TODO: change to emum?  See com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest in common-internal-api
    private byte[] commandParams;
    private Instant commandEnteredTime;
    private Instant commandFulfilledTime;
    private String status;
    private String sourceDeviceType;
    private MiuType targetDeviceType;
    private String userName;

    public int getCommandId()
    {
        return commandId;
    }

    public void setCommandId(int commandId)
    {
        this.commandId = commandId;
    }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public void setMiuId(MiuId miuId)
    {
        this.miuId = miuId;
    }

    public int getCommandType()
    {
        return commandType;
    }

    public void setCommandType(int commandNumber)
    {
        this.commandType = commandNumber;
    }

    public byte[] getCommandParams()
    {
        return commandParams;
    }

    public void setCommandParams(byte[] commandParams)
    {
        this.commandParams = commandParams;
    }

    public Instant getCommandEnteredTime()
    {
        return commandEnteredTime;
    }

    public void setCommandEnteredTime(Instant commandEnteredTime)
    {
        this.commandEnteredTime = commandEnteredTime;
    }

    public Instant getCommandFulfilledTime()
    {
        return commandFulfilledTime;
    }

    public void setCommandFulfilledTime(Instant commandFulfilledTime)
    {
        this.commandFulfilledTime = commandFulfilledTime;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public MiuType getTargetDeviceType()
    {
        return targetDeviceType;
    }

    public void setTargetDeviceType(MiuType targetDeviceType)
    {
        this.targetDeviceType = targetDeviceType;
    }

    public void setTargetDeviceType(String targetDeviceType)
    {
        this.targetDeviceType = MiuType.fromString(targetDeviceType);
    }

    public String getSourceDeviceType()
    {
        return sourceDeviceType;
    }

    public void setSourceDeviceType(String sourceDeviceType)
    {
        this.sourceDeviceType = sourceDeviceType;
    }

    public String getUserName() { return userName; }

    public void setUserName(String userName) { this.userName = userName; }

}
