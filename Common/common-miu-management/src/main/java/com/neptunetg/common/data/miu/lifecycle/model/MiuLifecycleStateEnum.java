/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.miu.lifecycle.model;

/**
 * The lifecycle states of L900 and CMIUs
 */
public enum MiuLifecycleStateEnum
{
    NEWBORN("Newborn"),
    PREACTIVATING("PreActivating"),
    PREACTIVATED("PreActivated"),
    ACTIVATED("Activated"),
    HEARD("Heard"),
    TESTJOINED("TestJoined"),
    PREPREPOT("PrePrePot"),
    PREPOT("PrePot"),
    POSTPOT("PostPot"),
    CONFIRMED("Confirmed"),
    DEVELOPMENT("Development"),
    SCANNED("Scanned"),
    SHIPPED("Shipped"),
    UNCLAIMED("Unclaimed"),
    CLAIMED("Claimed"),
    RMAED("RmaEd"),
    DEAD("Dead"),
    ROGUE("Rogue"),
    AGING("Aging"),
    SUSPENDED("Suspended"),
    PRESUSPENDED("PreSuspended"),
    DEATHBED("DeathBed");

    private final String sqlString;

    private MiuLifecycleStateEnum(String sqlEnumString)
    {
        this.sqlString = sqlEnumString;
    }

    public static MiuLifecycleStateEnum fromStringValue(String sqlEnumString)
    {
        for(MiuLifecycleStateEnum state : MiuLifecycleStateEnum.values())
        {
            if(state.getStringValue().equals(sqlEnumString))
            {
                return state;
            }
        }

        return null;
    }

    public String getStringValue()
    {
        return this.sqlString;
    }

}
