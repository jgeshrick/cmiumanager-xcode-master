/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015, 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.miu.heard;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.util.JdbcDateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.neptunetg.common.data.util.PreparedStatementSetterFromBatch;

import java.lang.Override;import java.lang.String;
import java.time.Instant;

import static com.neptunetg.common.data.util.JdbcDateUtil.getInstant;

/**
 * MIU Repository for MySql database
 */
@Profile("jdbc") //only create this bean if "jdbc" Spring profile is active
@Repository
public class JdbcMiuUpdateRepository implements MiuUpdateRepository
{
    private static final Logger log = LoggerFactory.getLogger(JdbcMiuUpdateRepository.class);

    private static final String UPDATE_MIU_LAST_HEARD_TIME_SQL = "UPDATE miu_details " +
            "SET last_heard_time = ?, first_insert_date = COALESCE(first_insert_date, ?), last_insert_date = ? " +
            "WHERE miu_id = ?";

    private static final String INSERT_MIU_WITH_LAST_HEARD_TIME_SQL_PATTERN = "INSERT INTO miu_details (last_heard_time, first_insert_date, last_insert_date," +
            "miu_id, " +
            "site_id, miu_type, " +
            "meter_active) " +
            "VALUES(?, ?, ?," +
            "?," +
            "0, %d, 'N')";

    private static final String INSERT_MIU_STATS_ALL_SQL = "INSERT INTO miu_heard_packets (miu_id, heard_time, miu_packet_count, " +   //1, 2, 3
            "avg_rssi, min_rssi, max_rssi, " +      // 4, 5, 6
            "ber, rsrq, processor_reset_counter, " + // 7, 8, 9
            "register_time, register_time_to_activate_context, register_time_connected, " + // 10, 11, 12
            "register_time_to_transfer_packet, disconnect_time, " +  //13, 14
            "mag_swipe_counter, battery_capacity, battery_voltage, temperature, " + //15, 16, 17, 18
            "snr, datarate, txpower, freq) " + //19, 20, 21, 22
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String INSERT_MIU_STATS_RSSI_SQL = "INSERT INTO miu_heard_packets (miu_id, heard_time, miu_packet_count, avg_rssi, min_rssi, max_rssi) " +
            "VALUES (?, ?, ?, ?, ?, ?)";

    // fields
    private final JdbcTemplate db;

    @Autowired
    public JdbcMiuUpdateRepository(JdbcTemplate db)
    {
        this.db = db;
    }


    private int updateLastHeardTimeMiuDetailsTable(MiuId miuId, MiuType miuType, Instant lastHeardTime,
                                                   Instant insertTime)
    {

        if (log.isDebugEnabled())
        {
            log.debug("Updating last heard time for MIU " + miuId + " to " + lastHeardTime);
        }

        final Object lastHeardTimeToBind = JdbcDateUtil.bindDate(lastHeardTime);
        final Object insertTimeToBind = JdbcDateUtil.bindDate(insertTime);
        int ret = this.db.update(UPDATE_MIU_LAST_HEARD_TIME_SQL, lastHeardTimeToBind, lastHeardTimeToBind, insertTimeToBind, miuId.numericWrapperValue());

        if (ret == 0) //not in the miu_details table, so add this MIU
        {
            log.warn("Received MQTT message from MIU {} that did not exist in miu_details table.  MIU will be added with meter_active='N'", miuId);

            final String query = String.format(INSERT_MIU_WITH_LAST_HEARD_TIME_SQL_PATTERN, miuType.numericWrapperValue());
            ret = this.db.update(query, lastHeardTimeToBind, lastHeardTimeToBind, insertTimeToBind, miuId.numericWrapperValue());
        }

        if (log.isDebugEnabled())
        {
            log.debug("Finished updating last heard time for MIU " + miuId + " to " + lastHeardTime);
        }
        return ret;
    }


    @Override
    @Transactional
    public void updateMiuHeardTimeAndRssiBatch(BatchPreparedStatementSetter miuIdLastHeardTime,
                                               BatchPreparedStatementSetter miuIdLastHeardTimeRssi,
                                               MiuType miuType)
    {

        if (log.isDebugEnabled())
        {
            log.debug("Updating last heard time for " + miuIdLastHeardTime.getBatchSize() + " MIUs");
        }

        final int[] updatedRows = this.db.batchUpdate(UPDATE_MIU_LAST_HEARD_TIME_SQL, miuIdLastHeardTime);

        final String query = String.format(INSERT_MIU_WITH_LAST_HEARD_TIME_SQL_PATTERN, miuType.numericWrapperValue());

        final PreparedStatementSetterFromBatch pss = new PreparedStatementSetterFromBatch(miuIdLastHeardTime);

        int newMiuCount = 0;

        for(int i=0; i < miuIdLastHeardTime.getBatchSize(); i++)
        {
            if(updatedRows[i] == 0)
            {
                newMiuCount++;

                pss.setRowIndex(i);
                this.db.update(query, pss);
            }
        }

        if (newMiuCount > 0)
        {
            log.warn("Attempted to update last heard time for " + newMiuCount + " MIUs that did not exist in miu_details table.  They have been added with meter_active='N'");
        }

        this.db.batchUpdate(INSERT_MIU_STATS_RSSI_SQL, miuIdLastHeardTimeRssi);

        if (log.isDebugEnabled())
        {
            log.debug("Finished updating last heard time for " + miuIdLastHeardTime.getBatchSize() + " MIUs");
        }

    }


    @Override
    public int updateMiuHeardAllStats(MiuId miuId, MiuType miuType, Instant heardTime, Instant insertTime,
                                      PreparedStatementSetter packetStats)
    {
        final int ret = updateLastHeardTimeMiuDetailsTable(miuId, miuType, heardTime, insertTime);

        final String query = INSERT_MIU_STATS_ALL_SQL;

        this.db.update(query, packetStats);

        return ret;
    }



}
