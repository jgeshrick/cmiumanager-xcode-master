/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.cellular;

import java.time.Instant;

/**
 * Contains details of a single CMIU mode change event with a sequence ID for change tracking.
 */
public class SequentialCmiuConfigHistoryEntry
{
    private int sequenceId;
    private int miuId;
    private boolean receivedFromMiu;
    private Instant timestamp;
    private int siteId;
    private Integer recordingIntervalMinutes;
    private Integer reportingIntervalMinutes;

    public SequentialCmiuConfigHistoryEntry(int sequenceId, int miuId, boolean receivedFromMiu, Instant timestamp,
                                            int siteId, Integer recordingIntervalMinutes, Integer reportingIntervalMinutes)
    {
        this.sequenceId = sequenceId;
        this.miuId = miuId;
        this.receivedFromMiu = receivedFromMiu;
        this.timestamp = timestamp;
        this.siteId = siteId;
        this.recordingIntervalMinutes = recordingIntervalMinutes;
        this.reportingIntervalMinutes = reportingIntervalMinutes;
    }

    public int getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(int sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public int getMiuId()
    {
        return miuId;
    }

    public void setMiuId(int miuId)
    {
        this.miuId = miuId;
    }

    public boolean isReceivedFromMiu()
    {
        return receivedFromMiu;
    }

    public void setReceivedFromMiu(boolean receivedFromMiu)
    {
        this.receivedFromMiu = receivedFromMiu;
    }

    public Instant getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public Integer getRecordingIntervalMinutes()
    {
        return recordingIntervalMinutes;
    }

    public void setRecordingIntervalMinutes(Integer recordingIntervalMinutes)
    {
        this.recordingIntervalMinutes = recordingIntervalMinutes;
    }

    public Integer getReportingIntervalMinutes()
    {
        return reportingIntervalMinutes;
    }

    public void setReportingIntervalMinutes(Integer reportingIntervalMinutes)
    {
        this.reportingIntervalMinutes = reportingIntervalMinutes;
    }
}
