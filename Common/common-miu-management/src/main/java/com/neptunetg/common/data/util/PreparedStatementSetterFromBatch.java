/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.util;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * This object wraps a BatchPreparedStatementSetter and allows values to be set from
 * an individual row of the batch.  Call setRowIndex and then pass this object
 * to jdbcTemplate.update().
 */
public class PreparedStatementSetterFromBatch implements PreparedStatementSetter
{
    private final BatchPreparedStatementSetter batchPreparedStatementSetter;
    private int rowIndex;

    public PreparedStatementSetterFromBatch(BatchPreparedStatementSetter batchPreparedStatementSetter)
    {
        this.batchPreparedStatementSetter = batchPreparedStatementSetter;
    }

    public int getRowIndex()
    {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex)
    {
        this.rowIndex = rowIndex;
    }

    @Override
    public void setValues(PreparedStatement preparedStatement) throws SQLException
    {
        this.batchPreparedStatementSetter.setValues(preparedStatement, this.rowIndex);
    }
}
