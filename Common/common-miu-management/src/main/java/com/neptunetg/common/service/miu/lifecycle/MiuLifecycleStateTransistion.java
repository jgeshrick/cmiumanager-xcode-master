/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.service.miu.lifecycle;

import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;

/**
 * Created by WJD1 on 23/05/2016.
 * A class to contain the current lifecycle state, and a previous one for comparison
 */
public class MiuLifecycleStateTransistion
{
    private MiuLifecycleState currentMiuLifecycleState;
    private MiuLifecycleState previousMiuLifecycleState;

    public MiuLifecycleState getCurrentMiuLifecycleState()
    {
        return currentMiuLifecycleState;
    }

    public void setCurrentMiuLifecycleState(MiuLifecycleState currentMiuLifecycleState)
    {
        this.currentMiuLifecycleState = currentMiuLifecycleState;
    }

    public MiuLifecycleState getPreviousMiuLifecycleState()
    {
        return previousMiuLifecycleState;
    }

    public void setPreviousMiuLifecycleState(MiuLifecycleState previousMiuLifecycleState)
    {
        this.previousMiuLifecycleState = previousMiuLifecycleState;
    }
}
