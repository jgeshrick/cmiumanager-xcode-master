/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.common.data.miu.cellular;

import org.springframework.util.StringUtils;

/**
 * Database equivalent of a pair (DeviceIdentifierKind, String)
 */
public class CellularDeviceId
{
    private enum DeviceIdentifierKind
    {
        ICCID,
        IMEI,
        MSISDN;
    }
    private final DeviceIdentifierKind kind;
    private final String value;

    private CellularDeviceId(DeviceIdentifierKind kind, String value)
    {
        this.kind = kind;
        this.value = StringUtils.trimWhitespace(value);
    }

    public static CellularDeviceId iccid(String iccid)
    {
        return new CellularDeviceId(DeviceIdentifierKind.ICCID, iccid);
    }

    public static CellularDeviceId imei(String imei)
    {
        return new CellularDeviceId(DeviceIdentifierKind.IMEI, imei);
    }

    public static CellularDeviceId msisdn(String msisdn)
    {
        return new CellularDeviceId(DeviceIdentifierKind.MSISDN, msisdn);
    }

    public boolean isIccid()
    {
        return DeviceIdentifierKind.ICCID.equals(kind);
    }

    public boolean isImei()
    {
        return DeviceIdentifierKind.IMEI.equals(kind);
    }

    public boolean isMsisdn()
    {
        return DeviceIdentifierKind.MSISDN.equals(kind);
    }

    public String getValue()
    {
        return value;
    }

    public String getColumnName()
    {
        return  kind.name().toLowerCase();
    }

    @Override
    public String toString()
    {
        return kind + " " + value;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final CellularDeviceId that = (CellularDeviceId) o;

        if (kind != that.kind) return false;
        return !(value != null ? !value.equals(that.value) : that.value != null);

    }

    @Override
    public int hashCode()
    {
        int result = kind != null ? kind.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
