/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.service.miu.lifecycle;

import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;

import java.util.*;

/**
 * Created by WJD1 on 19/05/2016.
 * Class for checking if a CMIU lifecycle state transition is allowed
 */
public class CheckCmiuLifecycleStateTransitions
{
    Map<MiuLifecycleStateEnum, EnumSet<MiuLifecycleStateEnum>> disallowedLifecycleStateTransitions;

    //Add disallowed lifecycle state transitions here
    final EnumSet<MiuLifecycleStateEnum> DISALLOWED_STATES_FROM_DEAD = EnumSet.allOf(MiuLifecycleStateEnum.class);

    final EnumSet<MiuLifecycleStateEnum> DISALLOWED_STATES_FROM_POSTPOT =
            EnumSet.range(MiuLifecycleStateEnum.PREACTIVATED,
                          MiuLifecycleStateEnum.PREPOT);

    final EnumSet<MiuLifecycleStateEnum> DISALLOWED_STATES_FROM_SHIPPED =
            EnumSet.range(MiuLifecycleStateEnum.SCANNED,
                          MiuLifecycleStateEnum.SHIPPED);

    final EnumSet<MiuLifecycleStateEnum> DISALLOWED_STATES_FROM_SCANNED = EnumSet.of(MiuLifecycleStateEnum.SCANNED);

    final EnumSet<MiuLifecycleStateEnum> DISALLOWED_STATES_FROM_CLAIMED =
            EnumSet.range(MiuLifecycleStateEnum.SCANNED,
                    MiuLifecycleStateEnum.SHIPPED);


    public CheckCmiuLifecycleStateTransitions()
    {
        disallowedLifecycleStateTransitions = new HashMap<>();
        disallowedLifecycleStateTransitions.put(MiuLifecycleStateEnum.DEAD, DISALLOWED_STATES_FROM_DEAD);
        disallowedLifecycleStateTransitions.put(MiuLifecycleStateEnum.POSTPOT, DISALLOWED_STATES_FROM_POSTPOT);
        disallowedLifecycleStateTransitions.put(MiuLifecycleStateEnum.SHIPPED, DISALLOWED_STATES_FROM_SHIPPED);
        disallowedLifecycleStateTransitions.put(MiuLifecycleStateEnum.SCANNED, DISALLOWED_STATES_FROM_SCANNED);
        disallowedLifecycleStateTransitions.put(MiuLifecycleStateEnum.CLAIMED, DISALLOWED_STATES_FROM_CLAIMED);
    }


    boolean checkLifecycleStateTransition(MiuLifecycleStateEnum currentLifecycleState,
                                          MiuLifecycleStateEnum newLifecycleState)
    {
        if(disallowedLifecycleStateTransitions.containsKey(currentLifecycleState))
        {
            if(disallowedLifecycleStateTransitions.get(currentLifecycleState).contains(newLifecycleState))
            {
                return false;
            }
        }

        return true;
    }
}
