/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.miu.lora;

/**
 * Enum to represent a LoRa network such as Senet
 */
public enum LoraNetwork
{
    SENET("Senet"),
    ACTILITY("Actility");

    private final String sqlString;

    private LoraNetwork(String sqlEnumString)
    {
        this.sqlString = sqlEnumString;
    }

    public static LoraNetwork fromStringValue(String sqlEnumString)
    {
        for(LoraNetwork state : LoraNetwork.values())
        {
            if(state.getStringValue().equals(sqlEnumString))
            {
                return state;
            }
        }

        return null;
    }

    public String getStringValue()
    {
        return this.sqlString;
    }
}
