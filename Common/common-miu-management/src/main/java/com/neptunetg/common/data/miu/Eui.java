/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.miu;

import org.springframework.util.StringUtils;

import javax.xml.bind.DatatypeConverter;
import java.io.Serializable;
import java.nio.ByteBuffer;

/**
 * Created by WJD1 on 27/05/2016.
 * Class to represent a EUI for L900.  Value Object pattern.
 *
 * The EUI is a 8 byte, globally unique identifier. It consists of:
 *
 * 3xBytes = OUI (organizationally unique identifier)
 * 1xByte  = In this case is assigned by Neptune, and has no meaning. It could be anything
 * 4xBytes = The MIU ID
 *
 * Due to the unpredictably assigned 4th byte in the EUI, the MIU ID can be extracted
 * from the EUI, but an EUI cannot be generated from the MIU ID (even though the OUI
 * is known).
 */
public final class Eui implements Serializable
{
    public static final int OUI = 0xCC3ADF;

    private final String eui;

    public Eui(String eui)
    {
        this.eui = StringUtils.hasText(eui) ? eui.trim() : "";
    }

    public static Eui valueOf(String eui)
    {
        return new Eui(eui);
    }

    public String getEui()
    {
        return eui;
    }

    public MiuId extractMiuId()
    {
        final ByteBuffer euiBytes = ByteBuffer.wrap(DatatypeConverter.parseHexBinary(eui)); //big endian
        final int ouiVal = 0xFFFFFF & (euiBytes.getInt() >> 8);

        if(ouiVal != OUI)
        {
            throw new IllegalArgumentException(
                    String.format("Cannot extract MIUID from EUI - expected OUI %x, got %x", OUI, ouiVal));
        }

        final int miuIdVal = euiBytes.getInt();

        return MiuId.valueOf(miuIdVal);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Eui eui1 = (Eui) o;

        return !(eui != null ? !eui.equals(eui1.eui) : eui1.eui != null);

    }

    @Override
    public int hashCode()
    {
        return eui != null ? eui.hashCode() : 0;
    }

    @Override
    public String toString()
    {
        return eui;
    }
}
