/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015, 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.miu.cellular;

import com.neptunetg.common.data.audit.CmiuConfigAuditLogRepository;
import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static com.neptunetg.common.data.util.JdbcDateUtil.bindDate;
import static com.neptunetg.common.data.util.JdbcDateUtil.getInstant;

@Repository
@Profile("jdbc") //only create this bean if "jdbc" Spring profile is active
public class JdbcCmiuConfigRepository implements CmiuConfigRepository
{
    private static final String AUDIT_SECTION = "RDS";

    private static final String GET_CONFIG_HISTORY_SQL =
            "SELECT cch.*, md.site_id, md.meter_active, md.last_heard_time, md.miu_type, ccs.*, " +
                "sd.iccid, sd.msisdn, sd.sim_cellular_network, ld.eui, ld.lora_network " +
                "FROM mdce.cmiu_config_history as cch " +
                "LEFT JOIN mdce.miu_details as md on cch.miu_id = md.miu_id " +
                "LEFT JOIN mdce.sim_details as sd on sd.miu_id = md.miu_id " +
                "LEFT JOIN mdce.lora_device_details as ld on ld.miu_id = md.miu_id " +
                "LEFT JOIN mdce.cmiu_config_set as ccs on cch.cmiu_config = ccs.id ";

    private static final String WHERE_CCH_APPLIES_FROM =
            "cch.`timestamp` >= (" +
                "SELECT cch_inner.`timestamp` " +
                "FROM cmiu_config_history cch_inner " +
                "WHERE cch_inner.miu_id = cch.miu_id AND cch_inner.`timestamp` <= ? " +
                "ORDER BY cch_inner.`timestamp` DESC " +
                "LIMIT 1) ";

    private static final String WHERE_CCH_APPLIES_UNTIL = "cch.`timestamp` <= ? ";

    private static final String WHERE_CCH_MIU_ID = "cch.miu_id = ? ";

    private static final String WHERE_CCH_MIU_ID_FROM = "cch.miu_id >= ? ";

    private static final String WHERE_CCH_MIU_ID_TO = "cch.miu_id <= ? ";

    private static final String WHERE_MD_SITE_ID = "md.site_id = ? ";

    private static final String WHERE_MD_METER_ACTIVE = "md.meter_active = ? ";

    private static final String WHERE_RECEIVED_FROM_MIU = "cch.received_from_cmiu = ? ";

    private static final String ORDER_BY_CCH_TIMESTAMP_DESC = " ORDER BY cch.timestamp DESC ";

    private static final String GET_CMIU_CONFIG_SET_ASSOCIATION_SQL = "SELECT cs.*, md.meter_active as meter_active, md.miu_id as miu_id, md.site_id as site_id " +
            " FROM mdce.cmiu_config_set as cs " +
            " INNER JOIN mdce.cmiu_config_mgmt as ccm on cs.id = ccm.current_config " +
            " INNER JOIN mdce.miu_details as md on ccm.miu_id = md.miu_id ";
    private static final String UPDATE_CMIU_CONFIG_MGMT_CURRENT_CONFIG = "UPDATE mdce.cmiu_config_mgmt SET current_config=?, current_config_apply_date=? WHERE miu_id =?; ";

    private static final String UPDATE_CMIU_CONFIG_MGMT_REPORTED_CONFIG = "UPDATE mdce.cmiu_config_mgmt SET reported_config=?, reported_config_date=? WHERE miu_id =?; ";

    private static final String INSERT_CONFIG_HISTORY = "INSERT INTO mdce.cmiu_config_history (miu_id, cmiu_config, timestamp, received_from_cmiu, change_note, site_id, billable) " +
            " VALUES (?, ?, ?, ?, ?, (SELECT md.site_id FROM miu_details md WHERE md.miu_id = ?), (SELECT md.meter_active FROM miu_details md WHERE md.miu_id = ?))";

    private static final String WHERE_MD_MIU_ID = " WHERE md.miu_id = ?";

    private static final String INSERT_CMIU_CONFIG_MGMT = "INSERT INTO mdce.cmiu_config_mgmt " +
            " (miu_id, current_config, current_config_apply_date, reported_config, reported_config_date, default_config) " +
            " VALUES (?, ?, ?, ?, ?, ?)";

    private static final String FIND_MATCHING_CONFIG_SET = "SELECT cs.* FROM cmiu_config_set cs " +
            "WHERE cs.reporting_start_mins=? AND cs.reporting_interval_mins=? AND cs.reporting_number_of_retries=? AND cs.reporting_transmit_windows_mins=? AND" +
            " cs.reporting_quiet_start_mins=? AND cs.reporting_quiet_end_mins=? AND " +
            " cs.recording_start_time_mins=? AND cs.recording_interval_mins=?";

    private static final String GET_SEQUENTIAL_CONFIG_HISTORY_SQL =
            "SELECT cch.*, ccs.* " +
                    "FROM mdce.cmiu_config_history as cch " +
                    "INNER JOIN mdce.cmiu_config_set as ccs on cch.cmiu_config = ccs.id ";

    private static final String WHERE_SEQUENCE_AFTER =
            "cch.cmiu_config_history_id > ? ";

    private static final String ORDER_BY_SEQUENCE =
            "ORDER BY cch.cmiu_config_history_id ";

    private static final String PAGE_SIZE =
            "LIMIT 1000 ";

    private static final String DEFAULT_CMIU_MODE_UNKNOWN = "Unknown";

    private static final Logger log = LoggerFactory.getLogger(JdbcCmiuConfigRepository.class);

    private final JdbcTemplate db;

    private final CmiuConfigAuditLogRepository auditRepo;

    @Autowired
    public JdbcCmiuConfigRepository(JdbcTemplate db, CmiuConfigAuditLogRepository auditRepo)
    {
        this.db = db;
        this.auditRepo = auditRepo;
    }

    /**
     * Return config set information for ID
     */
    @Override
    public ConfigSet getConfigSetFromId(long configSetId)
    {
        try
        {
            final String sql = "SELECT * FROM mdce.cmiu_config_set WHERE id = ?";
            return this.db.queryForObject(sql, getCmiuConfigSetRowMapper(), Long.valueOf(configSetId));
        }
        catch (EmptyResultDataAccessException ex)
        {
            return null;
        }

    }

    /**
     * Add a new config set row to the cmiu config set table
     *
     * @param newConfigSet contains the new config set parameters
     * @return id of row inserted
     */
    @Override
    public long addNewConfigSet(ConfigSet newConfigSet)
    {
        assert (newConfigSet != null);
        String insertSql = "INSERT INTO mdce.cmiu_config_set (name, billing_plan_id, " +
                " reporting_start_mins, reporting_interval_mins, " +
                " reporting_number_of_retries, reporting_transmit_windows_mins, " +
                " reporting_quiet_start_mins, reporting_quiet_end_mins, " +
                " recording_start_time_mins, recording_interval_mins, cmiu_mode_name) " +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.db.update(connection -> {
                    PreparedStatement ps = connection.prepareStatement(insertSql, new String[]{"id"});
                    ps.setString(1, sanitiseInput(newConfigSet.getName()));
                    ps.setInt(2, 1);
                    ps.setInt(3, newConfigSet.getReportingStartMins());
                    ps.setInt(4, newConfigSet.getReportingIntervalMins());
                    ps.setInt(5, newConfigSet.getReportingNumberOfRetries());
                    ps.setInt(6, newConfigSet.getReportingTransmitWindowMins());
                    ps.setInt(7, newConfigSet.getReportingQuietStartMins());
                    ps.setInt(8, newConfigSet.getReportingQuietEndMins());
                    ps.setInt(9, newConfigSet.getRecordingStartTimeMins());
                    ps.setInt(10, newConfigSet.getRecordingIntervalMins());
                    ps.setString(11, sanitiseInput(newConfigSet.getCmiuModeName()));

                    return ps;
                },
                keyHolder);

        auditRepo.logConfigSetCreated(newConfigSet, AUDIT_SECTION);

        return keyHolder.getKey().intValue();
    }

    /**
     * Change the parameters in existing config set, matched by config set id. Current only change the config set name.
     *
     * @param newConfigSetDefinition contains new parameter definition
     * @return number of rows affected, expects 1
     */
    @Override
    public int updateConfigSetName(ConfigSet newConfigSetDefinition)
    {
        String query = "UPDATE mdce.cmiu_config_set SET name = ?, cmiu_mode_name = ?, official_mode = ? WHERE id = ?";
        return this.db.update(query, newConfigSetDefinition.getName(), newConfigSetDefinition.getCmiuModeName(),
                newConfigSetDefinition.getOfficialMode(), newConfigSetDefinition.getId());
    }

    /**
     * Get all available config set
     *
     * @return
     */
    @Override
    public List<ConfigSet> getConfigSetList()
    {
        return this.db.query("SELECT * FROM mdce.cmiu_config_set", getCmiuConfigSetRowMapper());
    }

    /**
     * Return current cmiu config set of a cmiu
     */
    @Override
    public CmiuConfigSetAssociation getCurrentCmiuConfig(MiuId miuId) throws MiuDataException
    {
        try
        {
            return this.db.queryForObject(GET_CMIU_CONFIG_SET_ASSOCIATION_SQL + WHERE_MD_MIU_ID,
                    (rs, rownumber)-> mapToCmiuConfigSetAssociation(rs),
                    miuId.numericWrapperValue());

        }
        catch (EmptyResultDataAccessException ex)
        {
            throw new MiuDataException("No CmiuConfigSet found for miuId " + miuId, ex);
        }
    }

    /**
     * Return a list of current config set of all miu matching the siteId
     */
    @Override
    public List<CmiuConfigSetAssociation> getCurrentCmiuConfigCollection(int siteId)
    {
        try
        {
            return this.db.query(GET_CMIU_CONFIG_SET_ASSOCIATION_SQL + "WHERE " + WHERE_MD_SITE_ID, (rs, rownumber) -> mapToCmiuConfigSetAssociation(rs), Integer.valueOf(siteId));
        }
        catch (EmptyResultDataAccessException ex)
        {
            log.warn("EmptyResultDataAccessException caught doing inner join but returning null");
            return null;
        }
    }

    /**
     * Return all CMIU config regardless from all sites
     */
    @Override
    public List<CmiuConfigSetAssociation> getCurrentCmiuConfigCollection()
    {
        return this.db.query(GET_CMIU_CONFIG_SET_ASSOCIATION_SQL, (rs, rownumber) -> mapToCmiuConfigSetAssociation(rs));
    }

    @Override
    public ConfigSet updateCmiuConfig(MiuId miuId, ConfigSet newConf, ConfigSet oldConf,
                                      boolean reportedByCmiu, String changeNote)
    {
        if (newConf == null)
        {
            return null;
        }

        //If oldConf is null set the default values
        if(oldConf == null)
        {
            //Set oldConf as default values
            oldConf = new ConfigSet();

            oldConf.setRecordingStartTimeMins(0);
            oldConf.setRecordingIntervalMins(60);
            oldConf.setReportingStartMins(0);
            oldConf.setReportingIntervalMins(24 * 60);
            oldConf.setReportingNumberOfRetries(2);
            oldConf.setReportingTransmitWindowMins(120);
            oldConf.setReportingQuietStartMins(0);
            oldConf.setReportingQuietEndMins(0);
        }

        //Replace null values with the old config values
        newConf.setRecordingStartTimeMins(newConf.getRecordingStartTimeMins() != null ? newConf.getRecordingStartTimeMins() : oldConf.getRecordingStartTimeMins());
        newConf.setRecordingIntervalMins(newConf.getRecordingIntervalMins() != null ? newConf.getRecordingIntervalMins() : oldConf.getRecordingIntervalMins());
        newConf.setReportingStartMins(newConf.getReportingStartMins() != null ? newConf.getReportingStartMins() : oldConf.getReportingStartMins());
        newConf.setReportingIntervalMins(newConf.getReportingIntervalMins() != null ? newConf.getReportingIntervalMins() : oldConf.getReportingIntervalMins());
        newConf.setReportingNumberOfRetries(newConf.getReportingNumberOfRetries() != null ? newConf.getReportingNumberOfRetries() : oldConf.getReportingNumberOfRetries());
        newConf.setReportingTransmitWindowMins(newConf.getReportingTransmitWindowMins() != null ? newConf.getReportingTransmitWindowMins() : oldConf.getReportingTransmitWindowMins());
        newConf.setReportingQuietStartMins(newConf.getReportingQuietStartMins() != null ? newConf.getReportingQuietStartMins() : oldConf.getReportingQuietStartMins());
        newConf.setReportingQuietEndMins(newConf.getReportingQuietEndMins() != null ? newConf.getReportingQuietEndMins() : oldConf.getReportingQuietEndMins());

        ConfigSet configSet = null;

        if (newConf.getId() == 0L) //may or may not be in database
        {
            try
            {
                configSet = this.db.queryForObject(FIND_MATCHING_CONFIG_SET,
                        getCmiuConfigSetRowMapper(),
                        newConf.getValuesAsBindParams()
                );

            }
            catch (EmptyResultDataAccessException e)
            {
                //need to create a new entry
                if (!StringUtils.hasText(newConf.getCmiuModeName()))
                {
                    newConf.setCmiuModeName(DEFAULT_CMIU_MODE_UNKNOWN);
                }

                final long insertedConfigSetId = this.addNewConfigSet(newConf);
                newConf.setId(insertedConfigSetId);
                configSet = newConf;
            }
        }
        else
        {
            configSet = newConf;
        }

        //at this point, configSet has a valid id matching a row in the DB

        final CmiuConfigSetManagement cmiuConfigSetManagement = getCmiuConfigMgmt(miuId);
        boolean isChange = true;
        final Long cmiuConfigSetIdToBind = Long.valueOf(configSet.getId());
        final Instant applyDate = Instant.now();

        if (cmiuConfigSetManagement == null)
        {
            log.debug("Unknown config set received from CMIU " + miuId + " - adding to DB");

            // (miu_id, current_config, current_config_apply_date, reported_config, reported_config_date, default_config)

            this.db.update(INSERT_CMIU_CONFIG_MGMT, miuId.numericWrapperValue(), cmiuConfigSetIdToBind,
                    bindDate(applyDate), cmiuConfigSetIdToBind,
                    reportedByCmiu ? bindDate(applyDate) : null, //don't create a false impression if CMIU has not reported in
                    cmiuConfigSetIdToBind);
        }
        else if (reportedByCmiu && configSet.hasSameConfig(cmiuConfigSetManagement.getReportedConfig()))
        {
            isChange = false;
        }
        else if (!reportedByCmiu && configSet.hasSameConfig(cmiuConfigSetManagement.getCurrentConfig()))
        {
            isChange = false;
        }

        if (isChange)
        {
            log.debug("Updating CMIU " + miuId + " config set to " + cmiuConfigSetIdToBind);
            if (reportedByCmiu)
            {
                this.db.update(UPDATE_CMIU_CONFIG_MGMT_REPORTED_CONFIG, cmiuConfigSetIdToBind, bindDate(applyDate), miuId.numericWrapperValue());
            }
            else
            {
                this.db.update(UPDATE_CMIU_CONFIG_MGMT_CURRENT_CONFIG, cmiuConfigSetIdToBind, bindDate(applyDate), miuId.numericWrapperValue());
            }

            this.db.update(INSERT_CONFIG_HISTORY, miuId.numericWrapperValue(), cmiuConfigSetIdToBind, bindDate(applyDate),
                    Boolean.valueOf(reportedByCmiu), changeNote,
                    miuId.numericWrapperValue(), miuId.numericWrapperValue());
        }

        return configSet;
    }

    @Override
    public List<CmiuConfigSetManagement> getCmiuConfigChangeInProcess()
    {
        List<ConfigSet> allConfigSet = this.getConfigSetList();

        String sql = "SELECT ccm.* FROM mdce.cmiu_config_mgmt AS ccm " +
                " LEFT JOIN  mdce.cmiu_config_set AS cc ON cc.id = ccm.current_config" +
                " LEFT JOIN  mdce.cmiu_config_set AS rc ON rc.id = ccm.reported_config" +
                " WHERE cc.reporting_interval_mins <> rc.reporting_interval_mins OR " +
                "    cc.reporting_number_of_retries <> rc.reporting_number_of_retries OR " +
                "    cc.reporting_start_mins <> rc.reporting_start_mins OR " +
                "    cc.reporting_transmit_windows_mins <> rc.reporting_transmit_windows_mins OR " +
                "    cc.reporting_quiet_start_mins <> rc.reporting_quiet_start_mins OR " +
                "    cc.reporting_quiet_end_mins <> rc.reporting_quiet_end_mins OR " +
                "    cc.recording_start_time_mins <> rc.recording_start_time_mins OR " +
                "    cc.recording_interval_mins <> rc.recording_interval_mins";

        return this.db.query(sql, getCmiuConfigMgmtRowMapper(allConfigSet));

    }

    @Override
    public List<CmiuConfigSetManagement> getCmiuConfigChangeCompleted(int numberOfDaysAgo)
    {
        List<ConfigSet> allConfigSet = this.getConfigSetList();

        String sql = "SELECT ccm.* FROM mdce.cmiu_config_mgmt AS ccm " +
                " LEFT JOIN mdce.cmiu_config_set AS cc ON cc.id = ccm.current_config" +
                " LEFT JOIN mdce.cmiu_config_set AS rc ON rc.id = ccm.reported_config" +
                " WHERE cc.reporting_interval_mins = rc.reporting_interval_mins AND " +
                "    cc.reporting_number_of_retries = rc.reporting_number_of_retries AND " +
                "    cc.reporting_start_mins = rc.reporting_start_mins AND " +
                "    cc.reporting_transmit_windows_mins = rc.reporting_transmit_windows_mins AND " +
                "    cc.reporting_quiet_start_mins = rc.reporting_quiet_start_mins AND " +
                "    cc.reporting_quiet_end_mins = rc.reporting_quiet_end_mins AND " +
                "    cc.recording_start_time_mins = rc.recording_start_time_mins AND " +
                "    cc.recording_interval_mins = rc.recording_interval_mins AND" +
                "    ccm.reported_config_date > DATE_SUB(NOW(), INTERVAL ? DAY )";

        return this.db.query(sql, getCmiuConfigMgmtRowMapper(allConfigSet), numberOfDaysAgo);

    }

    /**
     * Retrieve the current, planned, reported and default config set of a CMIU
     */
    @Override
    public CmiuConfigSetManagement getCmiuConfigMgmt(MiuId cmiuId)
    {
        List<ConfigSet> allConfigSet = this.getConfigSetList();

        String queryCmiuMgmt = "SELECT * FROM mdce.cmiu_config_mgmt WHERE miu_id = ?";

        try
        {
            CmiuConfigSetManagement cmiuConfigMgmt = this.db.queryForObject(queryCmiuMgmt, new Object[]{cmiuId.numericWrapperValue()},
                    getCmiuConfigMgmtRowMapper(allConfigSet));

            return cmiuConfigMgmt;
        }
        catch (EmptyResultDataAccessException ex)
        {
            return null;
        }

    }

    @Override
    public List<CmiuConfigHistoryEntry> getConfigHistory(final MiuConfigHistoryQueryOptions queryOptions)
    {
        Map<String, Object> whereClauses = new HashMap<>();

        if (queryOptions.getStartDate() != null)
        {
            whereClauses.put(WHERE_CCH_APPLIES_FROM, queryOptions.getStartDate());
        }

        if (queryOptions.getEndDate() != null)
        {
            whereClauses.put(WHERE_CCH_APPLIES_UNTIL, queryOptions.getEndDate());
        }

        if (queryOptions.getMiuId() != null)
        {
            whereClauses.put(WHERE_CCH_MIU_ID, queryOptions.getMiuId().numericWrapperValue());
        }

        if (queryOptions.getFromMiuId() != null && queryOptions.getFromMiuId().numericWrapperValue() != 0)
        {
            whereClauses.put(WHERE_CCH_MIU_ID_FROM, queryOptions.getFromMiuId().numericWrapperValue());
        }

        if (queryOptions.getToMiuId() != null && queryOptions.getToMiuId().numericWrapperValue() != 0)
        {
            whereClauses.put(WHERE_CCH_MIU_ID_TO, queryOptions.getToMiuId().numericWrapperValue());
        }

        if (queryOptions.getSiteId() != null && queryOptions.getSiteId().intValue() != 0)
        {
            whereClauses.put(WHERE_MD_SITE_ID, queryOptions.getSiteId());
        }

        if (queryOptions.getMeterActive() != null)
        {
            whereClauses.put(WHERE_MD_METER_ACTIVE, queryOptions.getMeterActive() ? "Y" : "N");
        }

        if (queryOptions.getReceivedFromMiu() != null)
        {
            whereClauses.put(WHERE_RECEIVED_FROM_MIU, queryOptions.getReceivedFromMiu() ? 1 : 0);
        }

        String sql = GET_CONFIG_HISTORY_SQL +
                (whereClauses.isEmpty() ? "" : "WHERE ") +
                whereClauses.entrySet().stream().map(Map.Entry::getKey).collect(Collectors.joining("AND ")) +
                ORDER_BY_CCH_TIMESTAMP_DESC;

        Object[] parameters =
                whereClauses.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList()).toArray();

        List<CmiuConfigHistoryEntry> result = this.db.query(sql, this::configHistoryRowMapper, parameters);

        return result;
    }

    @Override
    public List<SequentialCmiuConfigHistoryEntry> getSequentialConfigHistory(final Integer lastSequenceId)
    {
        String whereClause;
        Object[] parameters;
        if (lastSequenceId != null)
        {
            whereClause = "WHERE " + WHERE_SEQUENCE_AFTER;
            parameters = new Object[] {lastSequenceId};
        }
        else
        {
            whereClause = "";
            parameters = new Object[0];
        }

        String sql =
                GET_SEQUENTIAL_CONFIG_HISTORY_SQL +
                whereClause +
                ORDER_BY_SEQUENCE +
                PAGE_SIZE;

        List<SequentialCmiuConfigHistoryEntry> result = this.db.query(sql, this::sequentialConfigHistoryRowMapper, parameters);

        return result;
    }

    private static ConfigSet readCmiuConfigSet(ResultSet rs, long configSetId) throws SQLException
    {
        final ConfigSet configSet = new ConfigSet();
        configSet.setId(configSetId);
        configSet.setName(rs.getString("name"));
        configSet.setCmiuModeName(rs.getString("cmiu_mode_name"));
        configSet.setReportingStartMins(rs.getInt("reporting_start_mins"));
        configSet.setReportingIntervalMins(rs.getInt("reporting_interval_mins"));
        configSet.setReportingTransmitWindowMins(rs.getInt("reporting_transmit_windows_mins"));
        configSet.setReportingQuietStartMins(rs.getInt("reporting_quiet_start_mins"));
        configSet.setReportingQuietEndMins(rs.getInt("reporting_quiet_end_mins"));
        configSet.setReportingNumberOfRetries(rs.getInt("reporting_number_of_retries"));

        configSet.setRecordingStartTimeMins(rs.getInt("recording_start_time_mins"));
        configSet.setRecordingIntervalMins(rs.getInt("recording_interval_mins"));

        configSet.setOfficialMode(rs.getBoolean("official_mode"));



        return configSet;
    }

    private static CmiuConfigSetAssociation readCmiuConfigSetAssociation(ResultSet rs, long configSetId) throws SQLException
    {

        final ConfigSet configSet = readCmiuConfigSet(rs, configSetId);

        final int siteId = rs.getInt("site_id");

        final MiuId miuId = MiuId.valueOf(rs.getInt("miu_id"));

        final String activeValue = rs.getString("meter_active");

        return new CmiuConfigSetAssociation(miuId, siteId, activeValue, configSet);

    }

    /**
     * Short hand to extract the config set object from a List.
     *
     * @param configSets  the list of config set to search
     * @param configSetId the id of the config set where the object will be matched to
     * @return ConfigSet object if found, null if no matching id found
     */
    private static ConfigSet findConfigSetFromList(final List<ConfigSet> configSets, int configSetId)
    {
        Optional<ConfigSet> foundConfigSet = configSets
                .stream()
                .filter(c -> c.getId() == configSetId)
                .findFirst();

        if (foundConfigSet.isPresent())
        {
            return foundConfigSet.get();
        }

        return null;
    }

    private RowMapper<ConfigSet> getCmiuConfigSetRowMapper()
    {
        return (rs, rownumber) -> readCmiuConfigSet(rs, rs.getLong("id"));
    }

    private RowMapper<CmiuConfigSetManagement> getCmiuConfigMgmtRowMapper(List<ConfigSet> allConfigSet)
    {
        return (rs, rowNum) -> {
            CmiuConfigSetManagement mappedObject = new CmiuConfigSetManagement();
            mappedObject.setCmiuId(MiuId.valueOf(rs.getInt("miu_id")));
            mappedObject.setCurrentConfigApplyDate(getInstant(rs, "current_config_apply_date"));
            mappedObject.setPlannedConfigApplyDate(getInstant(rs, "planned_config_apply_date"));
            mappedObject.setPlannedConfigRevertDate(getInstant(rs, "planned_config_revert_date"));
            mappedObject.setReportedConfigDate(getInstant(rs, "reported_config_date"));

            mappedObject.setCurrentConfig(findConfigSetFromList(allConfigSet, rs.getInt("current_config")));
            mappedObject.setPlannedConfig(findConfigSetFromList(allConfigSet, rs.getInt("planned_config")));
            mappedObject.setDefaultConfig(findConfigSetFromList(allConfigSet, rs.getInt("default_config")));
            mappedObject.setReportedConfig(findConfigSetFromList(allConfigSet, rs.getInt("reported_config")));

            return mappedObject;
        };
    }

    private String sanitiseInput(String systemId)
    {
        if (systemId == null || systemId.equalsIgnoreCase("null"))
        {
            return "";
        }
        return systemId;
    }

    /**
     * Row mapper for mapping SQL query object to POJO
     *
     * @throws SQLException
     */
    private CmiuConfigHistoryEntry configHistoryRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        final String changeNote = rs.getString("change_note");
        final MiuId miuId = MiuId.valueOf(rs.getInt("miu_id"));
        final Integer miuTypeId = rs.getObject("miu_type") != null ? rs.getInt("miu_type") : null;
        final MiuType miuType = miuTypeId != null ? MiuType.valueOf(miuTypeId) : null;
        final Instant dateTime = getInstant(rs, "timestamp");
        final boolean receivedFromMiu = rs.getBoolean("received_from_cmiu");
        final int siteId = rs.getInt("site_id");
        final boolean billable = "Y".equals(rs.getString("billable"));
        final Instant lastHeardTime = getInstant(rs, "last_heard_time");
        final String iccid = rs.getString("iccid");
        final String msisdn = rs.getString("msisdn");
        final String networkProvider = getNetworkProvider(miuType, rs);
        final String eui = rs.getString("eui");
        final boolean meterActive = rs.getBoolean("meter_active");

        final ConfigSet configSet = readCmiuConfigSet(rs, rs.getLong("cmiu_config"));

        return new CmiuConfigHistoryEntry(
                miuId, miuType, dateTime, receivedFromMiu, changeNote, siteId, billable, lastHeardTime, iccid, msisdn,
                networkProvider, eui, configSet, meterActive);
    }

    /**
     * Row mapper for mapping SQL query object to POJO
     *
     * @throws SQLException
     */
    private SequentialCmiuConfigHistoryEntry sequentialConfigHistoryRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        final int sequenceId = rs.getInt("cmiu_config_history_id");
        final MiuId miuId = MiuId.valueOf(rs.getInt("miu_id"));
        final Instant dateTime = getInstant(rs, "timestamp");
        final boolean receivedFromMiu = rs.getBoolean("received_from_cmiu");
        final int siteId = rs.getInt("site_id");
        final Integer recordingIntevalMinutes = rs.getObject("recording_interval_mins") != null ?
                        rs.getInt("recording_interval_mins") : null;
        final Integer reportingIntevalMinutes = rs.getObject("reporting_interval_mins") != null ?
                        rs.getInt("reporting_interval_mins") : null;

        return new SequentialCmiuConfigHistoryEntry(
                sequenceId, miuId.numericValue(), receivedFromMiu, dateTime, siteId, recordingIntevalMinutes, reportingIntevalMinutes);
    }

    private String getNetworkProvider(MiuType miuType, ResultSet rs) throws SQLException
    {
        String networkProvider;

        if (miuType == MiuType.L900)
        {
            networkProvider = rs.getString("lora_network");
        }
        else if (miuType == MiuType.CMIU)
        {
            networkProvider = rs.getString("sim_cellular_network");
        }
        else
        {
            networkProvider = null;
        }

        return networkProvider;
    }

    /**
     * Helper for mapping SQL query of cmiu_config_set
     *
     * @param rs result set
     * @throws SQLException
     */
    private CmiuConfigSetAssociation mapToCmiuConfigSetAssociation(ResultSet rs) throws SQLException
    {
        return readCmiuConfigSetAssociation(rs, rs.getLong("id"));
    }
}
