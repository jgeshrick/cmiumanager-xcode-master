/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.domain.mqtt;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;

/**
 * Interface for subscribing MIUs to MQTT queues
 */
public interface MiuMqttSubscriptionManager
{

    /**
     * If this is a CMIU, send a message for it to be subscribed on all brokers
     *
     * @param miuId MIU ID
     * @param type  Type of MIU
     */
    void subscribeMiuMqttIfApplicable(MiuId miuId, MiuType type);

}
