/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.miu.cellular;

import com.neptunetg.common.data.miu.MiuId;

/**
 * An association between an MIU and a config set
 */
public class CmiuConfigSetAssociation
{

    private final MiuId miuId;
    private final int siteId;
    private final boolean billable;
    private final ConfigSet configSet;


    public CmiuConfigSetAssociation(MiuId miuId, int siteId, String billableString, ConfigSet configSet)
    {
        this.miuId = miuId;
        this.siteId = siteId;
        this.billable = "Y".equalsIgnoreCase(billableString);
        this.configSet = configSet;
    }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public boolean isBillable()
    {
        return billable;
    }

    public String isBillableAsString()
    {
        return this.billable ? "Y" : "N";
    }

    public ConfigSet getConfigSet()
    {
        return configSet;
    }

    public String toString() {
        return "Name: " + this.configSet.getName() + " [" + this.configSet.getId() +
                "] , Site id: " + this.getSiteId() + ", Recording: [" + this.configSet.getRecordingStartTimeMins() + ", " + this.configSet.getRecordingIntervalMins() + "]" +
                ", Reporting: [" + this.configSet.getReportingStartMins() + ", " + this.configSet.getReportingIntervalMins() + "]";
    }
}
