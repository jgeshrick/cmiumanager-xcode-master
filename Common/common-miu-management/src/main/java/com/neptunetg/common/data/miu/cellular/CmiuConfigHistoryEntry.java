/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */
package com.neptunetg.common.data.miu.cellular;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;

import java.time.Instant;

/**
 * Models a row in CMIU_CONFIG_HISTORY
 */
public class CmiuConfigHistoryEntry
{
    private final MiuId miuId;
    private final MiuType miuType;
    private final Instant dateTime;
    private final boolean receivedFromCmiu;
    private final String changeNote;
    private final int siteId;
    private final boolean billable;
    private final Instant lastHeardTime;
    private final String iccid;
    private final String msisdn;
    private final String networkProvider;
    private final String eui;
    private final ConfigSet cmiuConfigSet;
    private boolean meterActive;

    public CmiuConfigHistoryEntry(
            MiuId miuId, MiuType miuType, Instant dateTime, boolean receivedFromCmiu, String changeNote, int siteId,
            boolean billable, Instant lastHeardTime, String iccid, String msisdn, String networkProvider, String eui,
            ConfigSet cmiuConfigSet, boolean meterActive)
    {
        this.miuId = miuId;
        this.miuType = miuType;
        this.dateTime = dateTime;
        this.receivedFromCmiu = receivedFromCmiu;
        this.changeNote = changeNote;
        this.siteId = siteId;
        this.billable = billable;
        this.lastHeardTime = lastHeardTime;
        this.iccid = iccid;
        this.msisdn = msisdn;
        this.networkProvider = networkProvider;
        this.eui = eui;
        this.cmiuConfigSet = cmiuConfigSet;
        this.meterActive = meterActive;
    }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public Instant getDateTime()
    {
        return dateTime;
    }

    public boolean isReceivedFromCmiu()
    {
        return receivedFromCmiu;
    }

    public String getChangeNote()
    {
        return changeNote;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public ConfigSet getCmiuConfigSet()
    {
        return cmiuConfigSet;
    }

    public boolean isBillable()
    {
        return billable;
    }

    public Instant getLastHeardTime()
    {
        return lastHeardTime;
    }

    public String getIccid()
    {
        return iccid;
    }

    public String getMsisdn()
    {
        return msisdn;
    }

    public String getNetworkProvider()
    {
        return networkProvider;
    }

    public MiuType getMiuType()
    {
        return miuType;
    }

    public String getEui()
    {
        return eui;
    }

    public boolean isMeterActive()
    {
        return meterActive;
    }

    public void setMeterActive(boolean meterActive)
    {
        this.meterActive = meterActive;
    }
}
