/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.command;

/**
 * MIU details with additional information useful for search/filter/display.
 */
public class MiuCommandDetail extends MiuCommand
{
    private String networkProvider;

    public String getNetworkProvider()
    {
        return networkProvider;
    }

    public void setNetworkProvider(String networkProvider)
    {
        this.networkProvider = networkProvider;
    }
}
