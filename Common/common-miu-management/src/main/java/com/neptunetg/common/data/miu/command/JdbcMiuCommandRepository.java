/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.command;

import com.neptunetg.common.data.miu.MiuId;

import com.neptunetg.common.data.miu.MiuType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.lang.Override;import java.lang.String;import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static com.neptunetg.common.data.util.JdbcDateUtil.*;

/**
 * MIU command repository for MySql database
 */
@Profile("jdbc") //only create this bean if "jdbc" Spring profile is active
@Repository
public class JdbcMiuCommandRepository implements MiuCommandRepository
{

    private static final String MIU_ID_CONST = "miu_id";
    private static final String SQL_SELECT_MIU_COMMAND_DETAIL =
            "SELECT " +
                "mc.miu_command_id, mc.miu_id, mc.command_type, mc.command_params, mc.command_entered_time, " +
                "mc.command_fulfilled_time, mc.status, mc.source_device_type, mc.target_device_type, mc.user_name, " +
                "sd.sim_details_id, COALESCE(sd.sim_cellular_network, ld.lora_network) AS network_provider " +
            "FROM miu_commands mc " +
            "LEFT OUTER JOIN sim_details sd ON sd.miu_id = mc.miu_id " +
            "LEFT OUTER JOIN lora_device_details ld ON ld.miu_id = mc.miu_id ";

    // fields
    private final JdbcTemplate db;

    @Autowired
    public JdbcMiuCommandRepository(JdbcTemplate db)
    {
        this.db = db;
    }


    @Override
    public List<MiuCommandDetail> getCommandHistory(MiuId miuId)
    {
        final String sql = SQL_SELECT_MIU_COMMAND_DETAIL + "WHERE mc.miu_id = ?";
        List<MiuCommandDetail> result = this.db.query(sql,
                JdbcMiuCommandRepository::miuCommandDetailMapper, miuId.numericWrapperValue());    //retrieve all records

        return result;
    }

    @Override
    public List<MiuCommandDetail> getInProgressCommand()
    {
        String sql = SQL_SELECT_MIU_COMMAND_DETAIL + "WHERE mc.status = ? OR mc.status = ?";
        List<MiuCommandDetail> result = this.db.query(sql,
                JdbcMiuCommandRepository::miuCommandDetailMapper,
                CommandLifeCycle.CREATED.getSqlString(), CommandLifeCycle.QUEUED.getSqlString());

        return result;
    }

    @Override
    public List<MiuCommandDetail> getReceivedCommand()
    {
        String sql = SQL_SELECT_MIU_COMMAND_DETAIL + "WHERE mc.status = ?";
        List<MiuCommandDetail> result = this.db.query(sql,
                JdbcMiuCommandRepository::miuCommandDetailMapper,
                CommandLifeCycle.ACCEPTED.getSqlString());

        return result;
    }

    @Override
    public List<MiuCommandDetail> getCompletedCommand(int numberOfDaysAgo)
    {
        //Get rows from miu_commands with command fulfilled time later (greater) than the specific number of days ago.
        String sql = SQL_SELECT_MIU_COMMAND_DETAIL + "WHERE mc.status = ? AND mc.command_fulfilled_time > DATE_SUB(NOW(), INTERVAL ? DAY )";
        List<MiuCommandDetail> result = this.db.query(sql,
                JdbcMiuCommandRepository::miuCommandDetailMapper,
                CommandLifeCycle.COMMITTED.getSqlString(), numberOfDaysAgo);

        return result;
    }

    @Override
    public List<MiuCommandDetail> getRejectedCommand(int numberOfDaysAgo)
    {
        String sql = SQL_SELECT_MIU_COMMAND_DETAIL + "WHERE mc.status = ? AND mc.command_fulfilled_time > DATE_SUB(NOW(), INTERVAL ? DAY )";
        List<MiuCommandDetail> result = this.db.query(sql,
                JdbcMiuCommandRepository::miuCommandDetailMapper,
                CommandLifeCycle.REJECTED.getSqlString(), numberOfDaysAgo);

        return result;
    }


    @Override
    public List<MiuCommandDetail> getMiuCommandsByState(CommandLifeCycle commandLifecycle, MiuType miuType)
    {
        String sql = SQL_SELECT_MIU_COMMAND_DETAIL + "WHERE mc.status = ? AND target_device_type = ?";

        return this.db.query(sql, JdbcMiuCommandRepository::miuCommandDetailMapper,
                commandLifecycle.getSqlString(),
                miuType.name());
    }

    /**
     * Return a list of firmware update commands which are pending
     * @param miuId the miu to which the firmware update command is sent
     * @return list of pending firmware update commands
     */
    @Override
    public List<MiuCommandDetail> getPendingCommands(MiuId miuId)
    {
        String sql = SQL_SELECT_MIU_COMMAND_DETAIL + "WHERE (mc.status = ? OR mc.status = ?) AND mc.miu_id = ?";

        return this.db.query(sql, JdbcMiuCommandRepository::miuCommandDetailMapper,
                CommandLifeCycle.QUEUED.getSqlString(),
                CommandLifeCycle.ACCEPTED.getSqlString(),
                miuId.numericWrapperValue());
    }


    /**
     * Return a specific MIU command based on row ID
     */
    @Override
    public MiuCommand getMiuCommandById(long commandId)
    {
        String sql = SQL_SELECT_MIU_COMMAND_DETAIL + "WHERE mc.miu_command_id = ?";

        return this.db.queryForObject(sql, JdbcMiuCommandRepository::miuCommandDetailMapper, commandId);

    }


    /**
     * Map row retrieved from miu_commands to MiuCommand object
     *
     * @param rs     record set
     * @param rowNum row number
     * @return MiuCommand object
     * @throws SQLException
     */
    private static MiuCommandDetail miuCommandDetailMapper(ResultSet rs, int rowNum) throws SQLException
    {
        final MiuCommandDetail miuCommand = new MiuCommandDetail();
        miuCommand.setCommandId(rs.getInt("miu_command_id"));
        miuCommand.setMiuId(MiuId.valueOf(rs.getInt(MIU_ID_CONST)));
        miuCommand.setCommandType(rs.getInt("command_type"));

        miuCommand.setCommandEnteredTime(getInstant(rs, "command_entered_time"));
        miuCommand.setCommandFulfilledTime(getInstant(rs, "command_fulfilled_time"));
        miuCommand.setCommandParams(rs.getBytes("command_params"));
        miuCommand.setStatus(rs.getString("status"));

        miuCommand.setSourceDeviceType(rs.getString("source_device_type"));
        miuCommand.setTargetDeviceType(rs.getString("target_device_type"));

        miuCommand.setUserName(rs.getString("user_name"));

        miuCommand.setNetworkProvider(rs.getString("network_provider"));

        return miuCommand;
    }

    @Override
    public int updateMiuCommandsState(int commandId, CommandLifeCycle targetState)
    {
        String sql = "UPDATE mdce.miu_commands SET status = ? WHERE miu_command_id=?";
        return this.db.update(sql, targetState.getSqlString(), commandId);
    }

    @Override
    public int updateMiuCommandsStateByMiuId(MiuId miuId, CommandLifeCycle fromState, CommandLifeCycle toState)
    {
        String sql = "UPDATE mdce.miu_commands SET status = ? WHERE miu_id=? AND status=?";
        return this.db.update(sql, toState.getSqlString(), miuId.numericWrapperValue(), fromState.getSqlString());
    }

    /**
     * Update the status of first (earliest) command status by matching miuid, current state to new state
     * @param miuId miu id of the matching row
     * @return number of rows updated, expected 1
     */
    @Override
    public int updateMiuCommandsStateToCommit(MiuId miuId, int cmdId)
    {
        //update only 1 row, sorted by the earliest.
        String sql = "UPDATE mdce.miu_commands " +
                "SET status = ?, command_fulfilled_time = NOW() " +
                "WHERE miu_id=? AND (status=? OR status=?) AND command_type=? ORDER BY command_entered_time LIMIT 1";
        return this.db.update(sql, CommandLifeCycle.COMMITTED.getSqlString(), miuId.numericWrapperValue(),
                CommandLifeCycle.ACCEPTED.getSqlString(), CommandLifeCycle.QUEUED.getSqlString(), cmdId);
    }

    /**
     * Update the status of first (earliest) command status by matching miuid, current state to new state
     * @param miuId miu id of the matching row
     * @return number of rows updated, expected 1
     */
    @Override
    public int updateMiuCommandsStateToAccepted(MiuId miuId, int cmdId)
    {
        //update only 1 row, sorted by the earliest.
        String sql = "UPDATE mdce.miu_commands " +
                "SET status = ?" +
                "WHERE miu_id=? AND status=? AND command_type=? ORDER BY command_entered_time LIMIT 1";
        return this.db.update(sql, CommandLifeCycle.ACCEPTED.getSqlString(), miuId.numericWrapperValue(),
                CommandLifeCycle.QUEUED.getSqlString(), cmdId);
    }


    /**
     * Update the status of first (earliest) command status by matching miuid, current state to new state
     * @param miuId miu id of the matching row
     * @return number of rows updated, expected 1
     */
    @Override
    public int updateMiuCommandsStateToRejected(MiuId miuId, int cmdId)
    {
        //update only 1 row, sorted by the earliest.
        String sql = "UPDATE mdce.miu_commands SET status = ?, command_fulfilled_time = NOW() " +
                "WHERE miu_id=? AND status=? AND command_type=? " +
                "ORDER BY command_entered_time LIMIT 1";
        return this.db.update(sql, CommandLifeCycle.REJECTED.getSqlString(), miuId.numericWrapperValue(), CommandLifeCycle.QUEUED.getSqlString(), cmdId);
    }


    @Override
    public int createMiuCommand(MiuCommand command)
    {
        String query = "INSERT INTO mdce.miu_commands (miu_id, command_type, command_params, " +
                "command_entered_time, status," +
                "source_device_type, target_device_type," +
                "user_name)" +
                " VALUES (?, ?, ?, " +
                "?, ?," +
                "?, ?," +
                "?)";

        String targetDevice = null;

        if(command.getTargetDeviceType() != null)
        {
            targetDevice = command.getTargetDeviceType().toString();
        }

        return this.db.update(query,
                command.getMiuId().numericWrapperValue(),
                command.getCommandType(), command.getCommandParams(),
                bindDate(command.getCommandEnteredTime()), CommandLifeCycle.CREATED.getSqlString(),
                command.getSourceDeviceType(), targetDevice,
                command.getUserName()
        );

    }

    /**
     * Change all states of a given miu from Create or Queued to Recalled
     *
     * @param miuId miu for state change
     * @return number of rows modified
     */
    @Override
    public int recallCreatedAndQueuedMiuCommand(MiuId miuId)
    {
        String sql = "UPDATE mdce.miu_commands SET status = ? WHERE miu_id = ? AND ( status = ? OR status = ? )";
        return this.db.update(sql, CommandLifeCycle.RECALLED.getSqlString(), miuId.numericWrapperValue(),
                CommandLifeCycle.CREATED.getSqlString(), CommandLifeCycle.QUEUED.getSqlString());
    }


    /**
     * Set command listed as Accepted by an MIU as Rejected
     *
     * @param commandId the command to apply the command state change
     * @return number of rows changed
     */
    @Override
    public int rejectAcceptedMiuCommand(long commandId)
    {
        String sql = "UPDATE mdce.miu_commands SET status = ?, command_fulfilled_time = NOW() WHERE miu_command_id = ? AND status = ?";
        return this.db.update(sql, CommandLifeCycle.REJECTED.getSqlString(), commandId, CommandLifeCycle.ACCEPTED.getSqlString());
    }

}
