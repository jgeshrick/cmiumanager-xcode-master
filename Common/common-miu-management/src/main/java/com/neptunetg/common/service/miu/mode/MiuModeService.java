/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.service.miu.mode;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.mode.MiuMode;

/**
 * Created by dcloud on 10/06/2017.
 * Service for getting reported and current mode for cmius
 */
public interface MiuModeService
{
    /**
     * Get the current mode that Miu is in
     * @param miuId the miu ID to get the mode for
     * @return The mode of the MIU
     */
    MiuMode getMiuCurrentMode(MiuId miuId);

    /**
     * Get the reported mode the Miu is in
     * @param miuId the miu ID to get the mode for
     * @param modeInfo MiuMode object that contains the reporting and recording interval for looking up reported mode
     * @return The mode of the MIU
     */
    MiuMode getMiuReportedMode(MiuId miuId, MiuMode modeInfo);

}
