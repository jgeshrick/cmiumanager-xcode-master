/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.lifecycle;

import com.neptunetg.common.data.miu.lifecycle.model.AddMiuLifecycleStateResult;
import com.neptunetg.common.data.miu.lifecycle.model.HistoricMiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.data.util.JdbcDateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.lang.Override;
import java.lang.String;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.neptunetg.common.data.miu.MiuId;
import org.springframework.util.StringUtils;

import static com.neptunetg.common.data.util.JdbcDateUtil.bindDate;
import static com.neptunetg.common.data.util.JdbcDateUtil.getInstant;

/**
 * Class to get and set MIU state to the SQL database
 */
@Profile("jdbc") //only create this bean if "jdbc" Spring profile is active
@Repository
public class JdbcMiuLifecycleRepo implements MiuLifecycleRepo
{
    private static final Integer ZERO = Integer.valueOf(0);

    private static final Logger log = LoggerFactory.getLogger(JdbcMiuLifecycleRepo.class);

    private static final String SELECT_MIU_LIFECYCLE_STATE =
            "SELECT `miu_lifecycle_state_id`, `miu_id`, `state`, `timestamp` FROM mdce.miu_lifecycle_state ";

    private static final String WHERE_SEQUENCE_AFTER =
            "miu_lifecycle_state_id > ? ";

    private static final String ORDER_BY_SEQUENCE =
            "ORDER BY miu_lifecycle_state_id ";

    private static final String PAGE_SIZE =
            "LIMIT 1000 ";

    private static final String INSERT_MIU_LIFECYCLE_STATE =
            "INSERT INTO mdce.miu_lifecycle_state (`miu_id`, `timestamp`, `state`) VALUES (?, ?, ?) ";

    private static final String SELECT_MIU_IDS_IN_LIST =
            "SELECT miu_id FROM mdce.miu_details WHERE miu_id = ? ";

    private static final String SELECT_IS_HISTORIC_LIFECYCLE_STATE_CURRENT =
            "SELECT COALESCE(MAX(current_state = last_historic_state), 0) " +
                    "FROM ( " +
                    "SELECT " +
                    "md.state AS current_state, ls.state last_historic_state " +
                    "FROM miu_details md " +
                    "LEFT OUTER JOIN miu_lifecycle_state ls ON ls.miu_id = md.miu_id " +
                    "WHERE md.miu_id = ? " +
                    "ORDER BY ls.timestamp DESC " +
                    "LIMIT 1 " +
                    ") AS state_comparison ";

    private static final String UPDATE_MIU_LIFECYCLE_STATE =
            "UPDATE miu_details " +
                    "SET " +
                    "  state = ?, " +
                    "  last_state_change_timestamp = NOW() " +
                    "WHERE " +
                    "  miu_id = ? ";

    private final JdbcTemplate db;

    @Autowired
    public JdbcMiuLifecycleRepo(JdbcTemplate db)
    {
        this.db = db;
    }

    @Override
    public MiuLifecycleState getMiuLifecycleState(MiuId miuId)
    {
        String sql = "SELECT miu_id, state, last_state_change_timestamp FROM mdce.miu_details WHERE miu_id=?";

        try
        {
            return this.db.queryForObject(sql, this::miuLifecycleStateFromMiuDetailsRowMapper, miuId.numericValue());
        }
        catch(DataAccessException e)
        {
            log.debug("Could not get lifecycle state for miu: {}", miuId.toString());
        }

        return null;
    }

    @Override
    public boolean setMiuLifecycleState(MiuId miuId, MiuLifecycleStateEnum miuLifecycleState)
    {
        String sql = "UPDATE mdce.miu_details SET state = ?, last_state_change_timestamp = ? WHERE miu_id = ?";
        Date date = new Date();

        int rowsUpdated = this.db.update(sql, miuLifecycleState.getStringValue(), JdbcDateUtil.bindDate(date),
                miuId.numericValue());

        if(rowsUpdated == 0)
        {
            log.debug("Attempt to change MIU {} state, but MIU doesn't exist", miuId.numericValue());
            return false;
        }

        sql = "INSERT INTO mdce.miu_lifecycle_state (miu_id, timestamp, state) VALUES (?, ?, ?)";

        this.db.update(sql, miuId.numericValue(), JdbcDateUtil.bindDate(date), miuLifecycleState.getStringValue());

        return true;
    }

    @Override
    public List<MiuLifecycleState> getMiuLifecycleStateHistory(MiuId miuId)
    {
        String sql = "SELECT miu_lifecycle_state_id, miu_id, state, timestamp FROM mdce.miu_lifecycle_state WHERE miu_id = ? " +
                "ORDER BY timestamp ASC";

        return this.db.query(sql, this::miuLifecycleStateFromMiuLifecycleStateRowMapper, miuId.numericWrapperValue());
    }

    @Override
    public List<MiuLifecycleState> getMiusInLifecycleState(MiuLifecycleStateEnum miuLifecycleState)
    {
        String sql = "SELECT miu_id, state, last_state_change_timestamp FROM mdce.miu_details WHERE state = ?";

        return this.db.query(sql, this::miuLifecycleStateFromMiuDetailsRowMapper, miuLifecycleState.getStringValue());
    }

    @Override
    public MiuLifecycleState getPreviousLifecycleStateForMiu(MiuId miuId,
                                                             MiuLifecycleStateEnum previousMiuLifecycleState)
    {
        String sql = "SELECT miu_lifecycle_state_id, miu_id, state, timestamp FROM mdce.miu_lifecycle_state WHERE miu_id = ? AND state = ? " +
                "ORDER BY timestamp DESC LIMIT 1";

        List<MiuLifecycleState> miuLifecycleStates = this.db.query(sql,
                this::miuLifecycleStateFromMiuLifecycleStateRowMapper,
                miuId.numericValue(), previousMiuLifecycleState.getStringValue());

        if(miuLifecycleStates == null || miuLifecycleStates.isEmpty())
        {
            return null;
        }

        return miuLifecycleStates.get(0);
    }

    @Override
    public List<MiuLifecycleState> getMiusInLifecycleStateForLongerThan(MiuLifecycleStateEnum miuLifecycleState,
                                                                        Instant maximumTransistionInstant)
    {
        String sql = "SELECT miu_id, state, last_state_change_timestamp FROM mdce.miu_details WHERE state = ? " +
                "AND last_state_change_timestamp <= ?";

        return this.db.query(sql, this::miuLifecycleStateFromMiuDetailsRowMapper, miuLifecycleState.getStringValue(), bindDate(maximumTransistionInstant));
    }

    @Override
    public List<MiuLifecycleState> getMiusInLifecycleStateForLongerThan(Duration intervalDuration)
    {
        String sql = "SELECT * FROM miu_lifecycle_state "
                    + "WHERE miu_id IN"
                    + "(SELECT miu_id FROM miu_details "
                    +   "WHERE state IN ('PreActivated', 'Activated', 'Heard', 'PrePrePot', 'PrePot', 'PostPot', 'Confirmed')) "
                    + "AND state = 'PreActivated' AND timestamp < curdate() - interval ? day;";

        return this.db.query(sql, this::miuLifecycleStateFromMiuLifecycleStateRowMapper, Long.valueOf(intervalDuration.getSeconds()));
    }

    @Override
    public List<MiuLifecycleState> getMiuLifecycleStates(Integer lastSequenceId)
    {
        String whereClause;
        Object[] parameters;
        if (lastSequenceId != null)
        {
            whereClause = "WHERE " + WHERE_SEQUENCE_AFTER;
            parameters = new Object[] {lastSequenceId};
        }
        else
        {
            whereClause = "";
            parameters = new Object[0];
        }

        String sql =
                SELECT_MIU_LIFECYCLE_STATE +
                        whereClause +
                        ORDER_BY_SEQUENCE +
                        PAGE_SIZE;

        List<MiuLifecycleState> result = this.db.query(sql, this::miuLifecycleStateFromMiuLifecycleStateRowMapper, parameters);

        return result;
    }

    @Override
    public List<AddMiuLifecycleStateResult> addMiuLifecycleStates(List<HistoricMiuLifecycleState> historicStates)
    {
        // Check that the MIU ID is known
        List<MiuId> miuIds = historicStates.stream().map(s -> s.getMiuId()).collect(Collectors.toList());

        List<Integer> idParameters = new ArrayList<>();
        String sql = "";
        for (MiuId miuId : miuIds)
        {
            if (StringUtils.hasText(sql))
            {
                sql += "UNION ";
            }

            sql += SELECT_MIU_IDS_IN_LIST;
            idParameters.add(miuId.numericWrapperValue());
        }

        List<Integer> knownMiuIds = this.db.query(sql, this::miuIdRowMapper, idParameters.toArray());

        List<AddMiuLifecycleStateResult> resultList = new ArrayList<>();

        // Add the lifecycle states
        for (HistoricMiuLifecycleState historicState : historicStates)
        {
            AddMiuLifecycleStateResult result = new AddMiuLifecycleStateResult(historicState.getReferenceId());

            if (!knownMiuIds.contains(historicState.getMiuId().numericWrapperValue()))
            {
                result.setOk(false);
                result.setError("Unknown MIU ID " + historicState.getMiuId().toString());
            }
            else
            {
                Object[] parameters = new Object[] {
                        historicState.getMiuId().numericWrapperValue(),
                        Date.from(historicState.getTransitionInstant()),
                        historicState.getLifecycleState().getStringValue()
                };

                try
                {
                    this.db.update(INSERT_MIU_LIFECYCLE_STATE, parameters);

                    // Ensure the state in miu_details is up to date
                    Integer isMiuStateUpToDate = this.db.queryForObject(SELECT_IS_HISTORIC_LIFECYCLE_STATE_CURRENT,
                            Integer.class, historicState.getMiuId().numericWrapperValue());

                    if (ZERO.equals(isMiuStateUpToDate))
                    {
                        this.db.update(UPDATE_MIU_LIFECYCLE_STATE,
                                historicState.getLifecycleState().toString(), historicState.getMiuId().numericWrapperValue());
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                result.setOk(true);
            }

            resultList.add(result);
        }

        return resultList;
    }

    private Integer miuIdRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        return rs.getInt("miu_id");
    }

    private MiuLifecycleState miuLifecycleStateFromMiuDetailsRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        MiuLifecycleState miuLifecycleState = new MiuLifecycleState();

        miuLifecycleState.setMiuId(MiuId.valueOf(rs.getInt("miu_id")));
        miuLifecycleState.setTransitionInstant(getInstant(rs, "last_state_change_timestamp"));

        String state = rs.getString("state");

        if(state != null)
        {
            miuLifecycleState.setLifecycleState(MiuLifecycleStateEnum.fromStringValue(state));
        }

        return miuLifecycleState;
    }

    private MiuLifecycleState miuLifecycleStateFromMiuLifecycleStateRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        MiuLifecycleState miuLifecycleState = new MiuLifecycleState();

        miuLifecycleState.setSequenceId(rs.getInt("miu_lifecycle_state_id"));
        miuLifecycleState.setMiuId(MiuId.valueOf(rs.getInt("miu_id")));
        miuLifecycleState.setTransitionInstant(getInstant(rs, "timestamp"));
        miuLifecycleState.setLifecycleState(MiuLifecycleStateEnum.fromStringValue(rs.getString("state")));

        return miuLifecycleState;
    }
}
