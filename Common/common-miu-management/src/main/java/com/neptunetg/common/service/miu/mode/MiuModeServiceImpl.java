/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.service.miu.mode;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.MiuLifecycleRepo;
import com.neptunetg.common.data.miu.lifecycle.model.AddMiuLifecycleStateResult;
import com.neptunetg.common.data.miu.lifecycle.model.HistoricMiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.data.miu.mode.MiuMode;
import com.neptunetg.common.data.miu.mode.MiuModeRepo;
import com.neptunetg.common.service.miu.lifecycle.CheckCmiuLifecycleStateTransitions;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleStateTransistion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by WJD1 on 19/05/2016.
 * Service for setting and getting lifecycle information for CMIUs
 */
@Profile("jdbc") //only create this bean if "jdbc" Spring profile is active
@Service
@Transactional
public class MiuModeServiceImpl implements MiuModeService
{
    private MiuModeRepo miuModeRepo;

    private static final Logger log = LoggerFactory.getLogger(MiuModeServiceImpl.class);

    @Autowired
    public MiuModeServiceImpl(MiuModeRepo miuModeRepo)
    {
        this.miuModeRepo = miuModeRepo;
    }

    @Override
    public MiuMode getMiuCurrentMode(MiuId miuId)
    {
        return miuModeRepo.getMiuCurrentMode(miuId);
    }

    @Override
    public MiuMode getMiuReportedMode(MiuId miuId, MiuMode modeInfo)
    {
        return miuModeRepo.getReportedMode(miuId, modeInfo.getRecordingInteval(), modeInfo.getReportingInterval());
    }

}
