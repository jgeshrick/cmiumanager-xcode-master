/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.lifecycle;


import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.AddMiuLifecycleStateResult;
import com.neptunetg.common.data.miu.lifecycle.model.HistoricMiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

/**
 * Class to get and set MIU lifecycle state to the SQL database
 */
public interface MiuLifecycleRepo
{
    /**
     * Get the current lifecycle state of a MIU
     * @param miuId the miu ID to get the lifecycle state for
     * @return The lifecycle state of the MIU
     */
    MiuLifecycleState getMiuLifecycleState(MiuId miuId);

    /**
     * Set the lifecycle state of a MIU
     * @param miuId The MIU to set the lifecycle state for
     * @param miuLifecycleState The lifecycle state to set the MIU to
     * @return true if successfully set the lifecycle state, false if not
     */
    boolean setMiuLifecycleState(MiuId miuId, MiuLifecycleStateEnum miuLifecycleState);

    /**
     * Get the lifecycle state history for a MIU
     * @param miuId the miu ID to get the lifecycle state history for
     * @return The lifecycle state of the MIU
     */
    List<MiuLifecycleState> getMiuLifecycleStateHistory(MiuId miuId);

    /**
     * Get all MIUs in lifecycle state
     * @param miuLifecycleState The lifecycle state to filter with
     * @return The MIUs which are in the specified lifecycle state
     */
    List<MiuLifecycleState> getMiusInLifecycleState(MiuLifecycleStateEnum miuLifecycleState);

    /**
     * Get the details on a previous lifecycle state for a MIU
     * @param miuId The MIU to find the lifecycle state for
     * @param previousMiuLifecycleState The previous lifecycle state to look for
     * @return Lifecycle state details for that MIU in the specified lifecycle state
     */
    MiuLifecycleState getPreviousLifecycleStateForMiu(MiuId miuId,
                                                      MiuLifecycleStateEnum previousMiuLifecycleState);

    /**
     * Get all MIUs that transitioned to lifecycle state before instant
     * @param miuLifecycleState The lifecycle state to filter with
     * @param maximumTransistionInstant The latest instant the MIU should have transitioned to that lifecycle state
     * @return List of MIUs in that lifecycle state since before maximumTime
     */
    List<MiuLifecycleState> getMiusInLifecycleStateForLongerThan(MiuLifecycleStateEnum miuLifecycleState,
                                                                 Instant maximumTransistionInstant);

    /**
     * Get all MIUs that transitioned to lifecycle state before duration
     * @param intervalDuration how long to look back for state changes
     * @return List of MIUs in that lifecycle state since before intervalDuration
     */
    List<MiuLifecycleState> getMiusInLifecycleStateForLongerThan(Duration intervalDuration);

    /**
     * Gets the lifecycle state history for MIUs starting after the supplied sequence ID.
     * @param lastSequenceId When non-null, the last sequence ID known to the client.
     * @return A list of lifecycle state history entries.
     */
    List<MiuLifecycleState> getMiuLifecycleStates(Integer lastSequenceId);

    /**
     * Attempts to add the supplied MIU lifecycle states to the lifecycle history as a batch operation.
     * @param historicStates A list of MIU lifecycle states to be added to the history.
     * @return A list containing the result of each insert operation.
     */
    List<AddMiuLifecycleStateResult> addMiuLifecycleStates(List<HistoricMiuLifecycleState> historicStates);
}

