/*
 *  Neptune Technology Group
 *  Copyright 2015, 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.cellular;

import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;

import java.lang.Override;import java.lang.String;import java.time.Instant;

/**
 * Represent RDS data model: cellular_device_details
 */
public class CellularDeviceDetail
{
    private MiuId miuId;
    private String apn;
    private String iccid;
    private String imsi;
    private String msisdn;
    private String simMno;
    private int provisioningState;
    private Instant lastUpdateTime;
    private ModemConfig modemConfig;

    public CellularDeviceDetail()
    {
        this.setModemConfig(new ModemConfig());
    }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public void setMiuId(MiuId miuId)
    {
        this.miuId = miuId;
    }

    public String getApn()
    {
        return apn;
    }

    public void setApn(String apn)
    {
        this.apn = apn;
    }

    public String getIccid()
    {
        return iccid;
    }

    public void setIccid(String iccid)
    {
        this.iccid = iccid;
    }

    public String getImsi()
    {
        return imsi;
    }

    public void setImsi(String imsi)
    {
        this.imsi = imsi;
    }

    public String getMsisdn()
    {
        return msisdn;
    }

    public void setMsisdn(String msisdn)
    {
        this.msisdn = msisdn;
    }

    public String getSimMno()
    {
        return simMno;
    }

    public String getMno() throws MiuDataException
    {

        if (simMno == null || simMno.trim().length() == 0)
        {
            return modemConfig.getMno();
        }
        else if (modemConfig.getMno() == null || modemConfig.getMno().trim().length() == 0)
        {
            return simMno;
        }
        else
        {
            if (!modemConfig.getMno().equals(simMno))
            {
                throw new MiuDataException("Cannot determine MNO for CMIU " + miuId + "!  Sim thinks " + simMno + ", modem thinks " + modemConfig.getMno());
            }
            else
            {
                return simMno;
            }
        }
    }

    public void setSimMno(String simMno)
    {
        this.simMno = simMno;
    }

    public int getProvisioningState()
    {
        return provisioningState;
    }

    public void setProvisioningState(int provisioningState)
    {
        this.provisioningState = provisioningState;
    }

    public Instant getLastUpdateTime()
    {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Instant lastUpdateTime)
    {
        this.lastUpdateTime = lastUpdateTime;
    }

    public ModemConfig getModemConfig()
    {
        return modemConfig;
    }

    public void setModemConfig(ModemConfig modemConfig)
    {
        this.modemConfig = modemConfig;
    }

    @Override
    public String toString()
    {
        return "CellularDeviceDetail{" +
                "miuId=" + miuId +
                ", iccid='" + iccid + '\'' +
                ", imsi='" + imsi + '\'' +
                ", msisdn='" + msisdn + '\'' +
                ", simMno='" + simMno + '\'' +
                ", provisioningState=" + provisioningState +
                ", lastUpdateTime=" + lastUpdateTime +
                ", modemConfig=" + modemConfig +
                '}';
    }

    public class ModemConfig
    {
        private String vendor;
        private String firmwareRevision;
        private String imei;
        private String mno;

        public String getVendor()
        {
            return vendor;
        }

        public void setVendor(String vendor)
        {
            this.vendor = vendor;
        }

        public String getFirmwareRevision()
        {
            return firmwareRevision;
        }

        public void setFirmwareRevision(String firmwareRevision)
        {
            this.firmwareRevision = firmwareRevision;
        }

        public String getImei()
        {
            return imei;
        }

        public void setImei(String imei)
        {
            this.imei = imei;
        }

        public String getMno()
        {
            return mno;
        }

        public void setMno(String mno)
        {
            this.mno = mno;
        }

        @Override
        public String toString()
        {
            return "ModemConfig{" +
                    "vendor='" + vendor + '\'' +
                    ", firmwareRevision='" + firmwareRevision + '\'' +
                    ", imei='" + imei + '\'' +
                    ", mno='" + mno + '\'' +
                    '}';
        }
    }
}
