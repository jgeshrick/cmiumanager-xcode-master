/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.lora;

import com.neptunetg.common.data.miu.Eui;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import static com.neptunetg.common.data.util.JdbcDateUtil.getInstant;

/**
 * LoRa Repository for MySql database
 */
@Profile("jdbc") //only create this bean if "jdbc" Spring profile is active
@Repository
public class JdbcLoraRepository implements LoraRepository
{
    private final JdbcTemplate db;

    @Autowired
    public JdbcLoraRepository(JdbcTemplate db)
    {
        this.db = db;
    }

    /**
     * Return the device details for a LoRa device
     *
     * @param miuId
     * @return LoraDeviceDetail if they exist, otherwise null
     */
    public LoraDeviceDetail getLoraDeviceDetailByMiuId(MiuId miuId)
    {
        final String sql = "SELECT * FROM mdce.lora_device_details WHERE miu_id = ?";

        try
        {
            return this.db.queryForObject(sql, (rs, rownum) -> {

                return new LoraDeviceDetail(MiuId.valueOf(rs.getInt("miu_id")),
                        LoraNetwork.fromStringValue(rs.getString("lora_network")),
                        getInstant(rs, "last_update_time"),
                        Eui.valueOf(rs.getString("eui")),
                        Eui.valueOf(rs.getString("app_eui")),
                        rs.getString("app_key"),
                        LoraAntennaType.fromStringValue(rs.getString("antenna_type")));

            }, miuId.numericWrapperValue());
        }
        catch (EmptyResultDataAccessException ex)
        {
            return null;
        }
    }

    /**
     * Return the device details for a LoRa device
     *
     * @param loraDeviceDetailsId
     * @return LoraDeviceDetail if they exist, otherwise null
     */
    public LoraDeviceDetail getLoraDeviceDetailByDeviceDetailsId(long loraDeviceDetailsId)
    {
        final String sql = "SELECT * FROM mdce.lora_device_details WHERE lora_device_details_id = ?";

        try
        {
            return this.db.queryForObject(sql, (rs, rownum) -> {

                return new LoraDeviceDetail(MiuId.valueOf(rs.getInt("miu_id")),
                        LoraNetwork.fromStringValue(rs.getString("lora_network")),
                        getInstant(rs, "last_update_time"),
                        Eui.valueOf(rs.getString("eui")),
                        Eui.valueOf(rs.getString("app_eui")),
                        rs.getString("app_key"),
                        LoraAntennaType.fromStringValue(rs.getString("antenna_type")));

            }, loraDeviceDetailsId);
        }
        catch (EmptyResultDataAccessException ex)
        {
            return null;
        }
    }

    /**
     * Insert or overwrite lora device details
     * Inserts a MIU Details row if it doesn't exist
     * @param loraDeviceDetail
     * @return the number of rows inserted or updated
     */
    public long insertOrOverwriteLoraDeviceDetail(LoraDeviceDetail loraDeviceDetail)
    {
        final MiuType miuType = MiuType.L900;
        final Integer siteId = Integer.valueOf(0); //universal site

        String sql = "UPDATE miu_details SET miu_type = IFNULL(?, miu_type) WHERE miu_id = ?";
        int rowsUpdated = this.db.update(sql, miuType.numericWrapperValue(),
                loraDeviceDetail.getMiuId().numericWrapperValue());

        if (rowsUpdated == 0)
        {
            sql = "INSERT INTO mdce.miu_details (miu_id, site_id, miu_type) VALUES (?,?,?)";
            this.db.update(sql, loraDeviceDetail.getMiuId().numericWrapperValue(), siteId, miuType.numericWrapperValue());
        }

        sql = "INSERT INTO mdce.lora_device_details (miu_id, lora_network, last_update_time, eui, app_eui, app_key, antenna_type) " +
                "VALUES (?, ?, NOW(), ?, ?, ?, ?) ON DUPLICATE KEY UPDATE lora_network = ?, last_update_time = NOW(), " +
                "eui = ?, app_eui = ?, app_key = ?, antenna_type = IFNULL(?, antenna_type)";

        this.db.update(sql, loraDeviceDetail.getMiuId().numericWrapperValue(),
                loraDeviceDetail.getLoraNetwork() == null ? null : loraDeviceDetail.getLoraNetwork().toString(),
                loraDeviceDetail.getEui().toString(),
                loraDeviceDetail.getAppEui() == null ? null : loraDeviceDetail.getAppEui().toString(),
                loraDeviceDetail.getAppKey(),
                loraDeviceDetail.getLoraAntennaType() == null ? null : loraDeviceDetail.getLoraAntennaType().toString(),
                loraDeviceDetail.getLoraNetwork() == null ? null : loraDeviceDetail.getLoraNetwork().toString(),
                loraDeviceDetail.getEui().toString(),
                loraDeviceDetail.getAppEui() == null ? null : loraDeviceDetail.getAppEui().toString(),
                loraDeviceDetail.getAppKey(),
                loraDeviceDetail.getLoraAntennaType() == null ? null : loraDeviceDetail.getLoraAntennaType().toString());

        //Get the row number for the details just inserted
        sql = "SELECT lora_device_details_id FROM mdce.lora_device_details WHERE miu_id = " + loraDeviceDetail.getMiuId();

        return this.db.queryForObject(sql, Long.class);
    }
}
