/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.audit;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.ConfigSet;

/**
 * Audit logging for config sets
 */
public interface CmiuConfigAuditLogRepository
{

    void logMiuChangeConfigSet(MiuId miuId, ConfigSet oldConfigSet, ConfigSet newConfigSet, String requester);

    void logConfigSetChange(ConfigSet oldConfigSet, ConfigSet newConfigSet, String requester);

    void logConfigSetCreated(ConfigSet newConfigSet, String requester);

}
