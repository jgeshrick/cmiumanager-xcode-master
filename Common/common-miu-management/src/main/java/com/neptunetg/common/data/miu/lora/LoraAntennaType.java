/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.miu.lora;

/**
 * Enum to represent the antenna used by a LoRa device
 */
public enum LoraAntennaType
{
    INTERNAL("internal", 0),
    EXTENRAL("external", 1);

    private final String sqlString;
    private final int value;

    private LoraAntennaType(String sqlEnumString, int value)
    {
        this.sqlString = sqlEnumString;
        this.value = value;
    }

    public static LoraAntennaType fromStringValue(String sqlEnumString)
    {
        for(LoraAntennaType state : LoraAntennaType.values())
        {
            if(state.getStringValue().equals(sqlEnumString))
            {
                return state;
            }
        }

        return null;
    }

    public static LoraAntennaType fromIntValue(int value)
    {
        for(LoraAntennaType state : LoraAntennaType.values())
        {
            if(value == (state.getIntValue()))
            {
                return state;
            }
        }

        return null;
    }

    public String getStringValue()
    {
        return this.sqlString;
    }

    public int getIntValue()
    {
        return this.value;
    }
}
