/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.mode;

import com.neptunetg.common.data.miu.MiuId;


/**
 * Created by dcloud on 10/06/2017.
 * Class to represent the reporting/recording mode for a cmiu
 */
public class MiuMode
{
    private MiuId miuId;
    private String currentMode;
    private String reportedMode;
    private boolean officialMode;
    private int recordingInterval;
    private int reportingInterval;

    public String getReportedMode() { return reportedMode; }

    public void setReportedMode(String reportedMode) { this.reportedMode = reportedMode; }

    public void setReportedMode(MiuMode mode) { this.reportedMode = mode.getReportedMode(); }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public void setMiuId(MiuId miuId)
    {
        this.miuId = miuId;
    }

    public String getCurrentMode() { return currentMode; }

    public void setCurrentMode(String mode) { this.currentMode = mode; }

    public void setCurrentMode(MiuMode mode) { this.currentMode = mode.getCurrentMode(); }

    public boolean isOfficialMode() { return officialMode; }

    public void setIsOfficialMode(boolean bool) { this.officialMode = bool; }

    public int getRecordingInteval() { return recordingInterval; }

    public void setRecordingInteval(int recordingInteval) { this.recordingInterval = recordingInteval; }

    public int getReportingInterval() { return reportingInterval; }

    public void setReportingInterval(int reportingInterval) { this.reportingInterval = reportingInterval; }

}
