/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.miu.cellular;

import com.neptunetg.common.data.miu.MiuId;

import java.lang.String;import java.time.Instant;

/**
 * Created by WJD1 on 11/01/2016.
 * Represents the revisions of various
 * firmwares and configurations for a CMIU
 * at a specific instant in time.
 */
public class CmiuRevisionSet
{
    private MiuId miuId;
    private Instant timestamp;
    private String firmwareRevision;
    private String configRevision;
    private String arbRevision;
    private String bleRevision;
    private String encryptionRevision;
    private String telitFirmwareRevision;

    public MiuId getMiuId()
    {
        return miuId;
    }

    public void setMiuId(MiuId miuId)
    {
        this.miuId = miuId;
    }

    public Instant getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getFirmwareRevision()
    {
        return firmwareRevision;
    }

    public void setFirmwareRevision(String firmwareRevision)
    {
        this.firmwareRevision = firmwareRevision;
    }

    public String getConfigRevision()
    {
        return configRevision;
    }

    public void setConfigRevision(String configRevision)
    {
        this.configRevision = configRevision;
    }

    public String getArbRevision()
    {
        return arbRevision;
    }

    public void setArbRevision(String arbRevision)
    {
        this.arbRevision = arbRevision;
    }

    public String getBleRevision()
    {
        return bleRevision;
    }

    public void setBleRevision(String bleRevision)
    {
        this.bleRevision = bleRevision;
    }

    public String getEncryptionRevision()
    {
        return encryptionRevision;
    }

    public void setEncryptionRevision(String encryptionRevision)
    {
        this.encryptionRevision = encryptionRevision;
    }

    public String getTelitFirmwareRevision()
    {
        return telitFirmwareRevision;
    }

    public void setTelitFirmwareRevision(String telitFirmwareRevision)
    {
        this.telitFirmwareRevision = telitFirmwareRevision;
    }
}
