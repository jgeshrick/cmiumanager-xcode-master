/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */
package com.neptunetg.common.data.debug;

import com.neptunetg.common.data.util.JdbcDateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.ObjectUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Checks timezone conversions between the application and the database.
 *
 * On startup, performs an integrity test that insertion and retrieval of
 *
 * As of 26 April 2016, JPA does not support the new java.time types - see https://java.net/jira/browse/JPA_SPEC-63
 * So, any attempts to use types other than Date, Timestamp and String fail.
 */
@Profile("jdbc") //only create this bean if "jdbc" Spring profile is active
@Service
public class JdbcTimezoneChecker
{
    private static final Logger logger = LoggerFactory.getLogger(JdbcTimezoneChecker.class);

    private final JdbcTemplate db;
    private final TransactionTemplate tx;

    @Autowired
    public JdbcTimezoneChecker(JdbcTemplate db, DataSourceTransactionManager transactionManager) throws SQLException
    {
        this.db = db;
        this.tx = new TransactionTemplate(transactionManager);

        logger.info("Starting database timezone check");
        try
        {
            final String issue = tx.execute(this::checkDatabaseDateInsertionAndRetrieval);
            if (issue != null)
            {
                throw new SQLException(issue);
            }
            logger.info("Database timezone check completed successfully");
        }
        catch (Exception e)
        {
            logger.error("Database timezone check failed", e);
            throw e;
        }

    }

    /**
     * A startup test that dates can be inserted into the database and retrieved
     * successfully without becoming corrupted.
     */
    private String checkDatabaseDateInsertionAndRetrieval(TransactionStatus transactionStatus)
    {
        final String deleteSql = "DELETE FROM tz_test";

        db.update(deleteSql);

        final ZonedDateTime timeInWinter = ZonedDateTime.of(2016, 1, 31, 23, 31, 43, 0, ZoneId.systemDefault());
        final Long timeInWinterEpochSecond = Long.valueOf(timeInWinter.toEpochSecond());
        final Date timeInWinterJavaUtilDate = new Date(timeInWinter.toInstant().toEpochMilli());
        final ZonedDateTime timeInSummer = ZonedDateTime.of(2016, 6, 3, 23, 31, 43, 0, ZoneId.systemDefault());
        final Long timeInSummerEpochSecond = Long.valueOf(timeInSummer.toEpochSecond());
        final Date timeInSummerJavaUtilDate = new Date(timeInSummer.toInstant().toEpochMilli());

        final String insertFromUnixtimeSql = "INSERT INTO tz_test(dt_col, ts_col) VALUES (FROM_UNIXTIME(?), FROM_UNIXTIME(?))";

        final List<String> rowDescriptions = new ArrayList<>();

        rowDescriptions.add("INSERT FROM_UNIXTIME - Winter date");
        db.update(insertFromUnixtimeSql, timeInWinterEpochSecond, timeInWinterEpochSecond);
        rowDescriptions.add("INSERT FROM_UNIXTIME - Summer date");
        db.update(insertFromUnixtimeSql, timeInSummerEpochSecond, timeInSummerEpochSecond);

        final String insertFromObjectSql = "INSERT INTO tz_test(dt_col, ts_col) VALUES (?, ?)";

        rowDescriptions.add("INSERT java.util.Date - Winter date");
        db.update(insertFromObjectSql, JdbcDateUtil.bindDate(timeInWinterJavaUtilDate), JdbcDateUtil.bindDate(timeInWinterJavaUtilDate));
        rowDescriptions.add("INSERT java.util.Date - Summer date");
        db.update(insertFromObjectSql, JdbcDateUtil.bindDate(timeInSummerJavaUtilDate), JdbcDateUtil.bindDate(timeInSummerJavaUtilDate));
        rowDescriptions.add("INSERT java.time.ZonedDateTime - Winter date");
        db.update(insertFromObjectSql, JdbcDateUtil.bindDate(timeInWinter), JdbcDateUtil.bindDate(timeInWinter));
        rowDescriptions.add("INSERT java.time.ZonedDateTime - Summer date");
        db.update(insertFromObjectSql, JdbcDateUtil.bindDate(timeInSummer), JdbcDateUtil.bindDate(timeInSummer));
        rowDescriptions.add("INSERT java.time.Instant - Winter date");
        db.update(insertFromObjectSql, JdbcDateUtil.bindDate(timeInWinter.toInstant()), JdbcDateUtil.bindDate(timeInWinter.toInstant()));
        rowDescriptions.add("INSERT java.time.Instant - Summer date");
        db.update(insertFromObjectSql, JdbcDateUtil.bindDate(timeInSummer.toInstant()), JdbcDateUtil.bindDate(timeInSummer.toInstant()));

        final String querySql = "SELECT dt_col, UNIX_TIMESTAMP(dt_col), ts_col, UNIX_TIMESTAMP(ts_col) FROM tz_test";
        final AtomicInteger rowsChecked = new AtomicInteger(0);

        db.query(querySql, (resultSet, rowNum) -> {

            final Long expectedEpochSecond;
            final Date expectedJavaUtilDate;

            if ((rowNum & 1) == 0)
            {
                //winter date
                expectedEpochSecond = timeInWinterEpochSecond;
                expectedJavaUtilDate = timeInWinterJavaUtilDate;
            }
            else
            {
                //summer date
                expectedEpochSecond = timeInSummerEpochSecond;
                expectedJavaUtilDate = timeInSummerJavaUtilDate;
            }
            checkDateRoundTrip(resultSet, rowDescriptions.get(rowNum), "DATETIME", 1, expectedEpochSecond, expectedJavaUtilDate);
            checkDateRoundTrip(resultSet, rowDescriptions.get(rowNum), "TIMESTAMP", 3, expectedEpochSecond, expectedJavaUtilDate);

            rowsChecked.incrementAndGet();
            return rowsChecked;
        });

        if (rowsChecked.get() < rowDescriptions.size())
        {
            return "Database timezone check failed!  Only " + rowsChecked.get() + " date round trips were checked, should be " + rowDescriptions.size();
        }

        return null; //success
    }

    private static void checkDateRoundTrip(ResultSet resultSet, String tableRowDescription, String columnType, int resultSetOffset,
                                    Long expectedEpochSecond, Date expectedJavaUtilDate) throws SQLException
    {
        final Timestamp dateRetrieved = resultSet.getTimestamp(resultSetOffset);
        final String dateRetrievedAsString = resultSet.getString(resultSetOffset);
        final long epochSecondsRetrieved = resultSet.getLong(resultSetOffset + 1);

        if (!expectedJavaUtilDate.equals(dateRetrieved))
        {
            throw new SQLException("Database timezone check failed!  For " + tableRowDescription + ", column type " + columnType +
                    ", expected " + expectedJavaUtilDate + "(" + expectedJavaUtilDate.getTime() +
                    ") but getTimestamp returned " + dateRetrieved + "(" + dateRetrieved.getTime() +
                    ") (getString returned "  + dateRetrievedAsString +")");
        }

        if (expectedEpochSecond.longValue() != epochSecondsRetrieved)
        {
            throw new SQLException("Database timezone check failed!  For " + tableRowDescription + ", column type " + columnType +
                    ", expected UNIX_TIMESTAMP retrieved to be " + expectedEpochSecond + " but getLong on UNIX_TIMESTAMP(col) returned " + epochSecondsRetrieved +
                    " (getString for col returned "  + dateRetrievedAsString + ")");
        }

    }


    /**
     * Perform a test of different inserts and retrievals of dates using multiple
     * Java classes and column types.  The result is available in an object
     * that can be interrogated or displayed for debugging purposes.
     * @return Result of tests
     */
    public Result checkTimezoneConversions()
    {

        return tx.execute(this::populateResult);

    }

    //runs within transaction
    private Result populateResult(TransactionStatus transactionStatus)
    {
        final Result ret = new Result();
        final DateCollection dateToInsert = ret.dates[0];

        final String sql = "SELECT @@global.time_zone, @@session.time_zone";

        db.query(sql, (resultSet -> {
            ret.dbGlobalTimeZone = resultSet.getString(1);
            ret.dbSessionTimeZone = resultSet.getString(2);
        }));

        testInsertAndRead("FROM_UNIXTIME(?)", dateToInsert.javaTimeInstant.getEpochSecond(), ret, 1);

        testInsertAndRead("?", dateToInsert.javaUtilDate, ret, 3);

        testInsertAndRead("?", dateToInsert.javaTimeInstant, ret, 5);

        testInsertAndRead("?", dateToInsert.javaTimeLocalDateTime, ret, 7);

        testInsertAndRead("?", dateToInsert.javaTimeZonedDateTime, ret, 9);

        return ret;
    }

    private void testInsertAndRead(String wildcard, Object bindVar, Result ret, int rowNum)
    {

        String sql = "DELETE FROM tz_test";

        db.update(sql);

        try
        {
            sql = "INSERT INTO tz_test(dt_col, ts_col) VALUES (" + wildcard + ", " + wildcard + ")";

            db.update(sql, bindVar, bindVar);

            sql = "SELECT dt_col FROM tz_test";
            String sqlWithUnixTimestamp = "SELECT dt_col, UNIX_TIMESTAMP(dt_col) FROM tz_test";

            ret.dates[rowNum] = getQueryResult(sql, sqlWithUnixTimestamp);

            sql = "SELECT ts_col, UNIX_TIMESTAMP(dt_col) FROM tz_test";
            sqlWithUnixTimestamp = "SELECT ts_col, UNIX_TIMESTAMP(ts_col) FROM tz_test";

            ret.dates[rowNum + 1] = getQueryResult(sql, sqlWithUnixTimestamp);

        }
        catch (Exception e)
        {
            ret.dates[rowNum] = new DateCollection();
            ret.dates[rowNum].exceptions = e.getMessage();

            ret.dates[rowNum + 1] = new DateCollection();
            ret.dates[rowNum + 1].exceptions = e.getMessage();
        }

    }

    private DateCollection getQueryResult(String sql, String sqlWithUnixTimestamp)
    {
        final DateCollection ret = new DateCollection();
        final StringBuilder exceptions = new StringBuilder();

        try
        {
            ret.javaUtilCalendar = db.queryForObject(sql, Calendar.class);
        }
        catch (Exception e)
        {
            exceptions.append("When convert to java.util.Calendar: ").append(e.getMessage()).append("; ");
        }
        try
        {
            ret.javaTimeInstant = db.queryForObject(sql, Instant.class);
        }
        catch (Exception e)
        {
            exceptions.append("When convert to java.time.Instant: ").append(e.getMessage()).append("; ");
        }
        try
        {
            ret.javaTimeLocalDateTime = db.queryForObject(sql, LocalDateTime.class);
        }
        catch (Exception e)
        {
            exceptions.append("When convert to java.time.LocalDateTime: ").append(e.getMessage()).append("; ");
        }
        try
        {
            ret.javaTimeZonedDateTime = db.queryForObject(sql, ZonedDateTime.class);
        }
        catch (Exception e)
        {
            exceptions.append("When convert to java.time.ZonedDateTime: ").append(e.getMessage()).append("; ");
        }
        final Integer rows = db.queryForObject(sqlWithUnixTimestamp, (resultSet, rowIndex) -> {
            ret.javaUtilDate = resultSet.getDate(1);
            ret.javaSqlTimestamp = resultSet.getTimestamp(1);
            ret.stringRepresentation = resultSet.getString(1);
            ret.epochSeconds = resultSet.getLong(2);
            return 1;
        });

        if (rows != Integer.valueOf(1))
        {
            exceptions.append("No rows found for query " + sql + "(" + rows + ")");
        }

        ret.exceptions = exceptions.toString();

        return ret;
    }


    public static class Result
    {

        public static final String[] ROW_TITLE =
                {
                        "date inserted",
                        "insert FROM_UNIXTIME(?) into DATETIME column",
                        "insert FROM_UNIXTIME(?) into TIMESTAMP column",
                        "insert java.util.Date into DATETIME column",
                        "insert java.util.Date into TIMESTAMP column",
                        "insert java.time.Instant into DATETIME column",
                        "insert java.time.Instant into TIMESTAMP column",
                        "insert java.time.LocalDateTime into DATETIME column",
                        "insert java.time.LocalDateTime into TIMESTAMP column",
                        "insert java.time.ZonedDateTime into DATETIME column",
                        "insert java.time.ZonedDateTime into TIMESTAMP column"
                };


        public static final String[] COL_TITLE =
                {
                        "java.util.Date",
                        "java.util.Calendar",
                        "java.sql.Timestamp",
                        "java.time.Instant",
                        "java.time.LocalDateTime",
                        "java.time.ZonedDateTime",
                        "java.lang.String",
                        "SELECT UNIX_TIMESTAMP(col)",
                        "Mapping exceptions"
                };


        private final ZoneId javaTimeZone;
        private String dbGlobalTimeZone;
        private String dbSessionTimeZone;
        private final DateCollection[] dates = new DateCollection[ROW_TITLE.length];

        public Result()
        {
            this.dates[0] = DateCollection.now();
            this.javaTimeZone = dates[0].javaTimeZonedDateTime.getZone();
        }

        public int getRowCount()
        {
            return ROW_TITLE.length;
        }

        public String getRowTitle(int rowIndex)
        {
            return ROW_TITLE[rowIndex];
        }

        public int getColumnCount()
        {
            return COL_TITLE.length;
        }

        public String getColumnTitle(int colIndex)
        {
            return COL_TITLE[colIndex];
        }

        public String getTableEntry(int rowIndex, int columnIndex)
        {
            final DateCollection ret = dates[rowIndex];
            if (ret == null)
            {
                return "null";
            }
            else
            {
                switch (columnIndex)
                {
                    case 0: return ret.getJavaUtilDate();
                    case 1: return ret.getJavaUtilCalendar();
                    case 2: return ret.getJavaSqlTimestamp();
                    case 3: return ret.getJavaTimeInstant();
                    case 4: return ret.getJavaTimeLocalDateTime();
                    case 5: return ret.getJavaTimeZonedDateTime();
                    case 6: return ret.getString();
                    case 7: return ret.getEpochSeconds();
                    case 8: return ret.exceptions;
                    default: return "?";
                }
            }
        }

        public ZoneId getJavaTimeZone()
        {
            return javaTimeZone;
        }

        public String getDbGlobalTimeZone()
        {
            return dbGlobalTimeZone;
        }

        public void setDbGlobalTimeZone(String dbGlobalTimeZone)
        {
            this.dbGlobalTimeZone = dbGlobalTimeZone;
        }

        public String getDbSessionTimeZone()
        {
            return dbSessionTimeZone;
        }

        public void setDbSessionTimeZone(String dbSessionTimeZone)
        {
            this.dbSessionTimeZone = dbSessionTimeZone;
        }
    }

    public static class DateCollection
    {
        private Date javaUtilDate;
        private Calendar javaUtilCalendar;
        private java.sql.Timestamp javaSqlTimestamp;
        private Instant javaTimeInstant;
        private LocalDateTime javaTimeLocalDateTime;
        private ZonedDateTime javaTimeZonedDateTime;
        private Long epochSeconds;
        private String stringRepresentation;
        private String exceptions;

        public DateCollection()
        {
        }

        public DateCollection(Date javaUtilDate, Calendar javaUtilCalendar,
                              java.sql.Timestamp javaSqlTimestamp,
                              Instant javaTimeInstant, LocalDateTime javaTimeLocalDateTime, ZonedDateTime javaTimeZonedDateTime,
                              Long epochSeconds,
                              String stringRepresentation)
        {
            this.javaUtilDate = javaUtilDate;
            this.javaUtilCalendar = javaUtilCalendar;

            this.javaSqlTimestamp = javaSqlTimestamp;

            this.javaTimeInstant = javaTimeInstant;
            this.javaTimeLocalDateTime = javaTimeLocalDateTime;
            this.javaTimeZonedDateTime = javaTimeZonedDateTime;

            this.epochSeconds = epochSeconds;

            this.stringRepresentation = stringRepresentation;
        }

        public static DateCollection now()
        {
            final ZonedDateTime now = ZonedDateTime.now();
            return new DateCollection(Date.from(now.toInstant()),
                    GregorianCalendar.from(now),
                    java.sql.Timestamp.from(now.toInstant()),
                    now.toInstant(),
                    now.toLocalDateTime(),
                    now,
                    now.toEpochSecond(),
                    now.toString());
        }

        @Override
        public String toString()
        {
            return "DateCollection{" +
                    "javaUtilDate=" + javaUtilDate +
                    ", javaUtilCalendar=" + javaUtilCalendar +
                    ", javaSqlTimestamp=" + javaSqlTimestamp +
                    ", javaTimeInstant=" + javaTimeInstant +
                    ", javaTimeLocalDateTime=" + javaTimeLocalDateTime +
                    ", javaTimeZonedDateTime=" + javaTimeZonedDateTime +
                    ", stringRepresentation='" + stringRepresentation + '\'' +
                    '}';
        }

        public boolean datesEqual(DateCollection otherDates)
        {
            return ObjectUtils.nullSafeEquals(javaUtilDate, otherDates.javaUtilDate)
                    && ObjectUtils.nullSafeEquals(javaUtilCalendar, otherDates.javaUtilCalendar)
                    && ObjectUtils.nullSafeEquals(javaSqlTimestamp, otherDates.javaSqlTimestamp)
                    && ObjectUtils.nullSafeEquals(javaTimeInstant, otherDates.javaTimeInstant)
                    && ObjectUtils.nullSafeEquals(javaTimeLocalDateTime, otherDates.javaTimeLocalDateTime)
                    && ObjectUtils.nullSafeEquals(javaTimeZonedDateTime, otherDates.javaTimeZonedDateTime)
                    && ObjectUtils.nullSafeEquals(epochSeconds, otherDates.epochSeconds);
        }

        public String getJavaUtilDate()
        {
            if (javaUtilDate == null)
            {
                return "";
            }
            else
            {
                try
                {
                    return DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG).format(this.javaUtilDate) + " (" + this.javaUtilDate.getTime() + ")";
                }
                catch (Exception e)
                {
                    return "Format exception: " + e.getMessage();
                }
            }
        }


        public String getJavaUtilCalendar()
        {
            if (javaUtilCalendar == null)
            {
                return "";
            }
            else
            {
                try
                {
                    return DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG).format(this.javaUtilCalendar.getTime()) + " (" + this.javaUtilCalendar + ")";
                }
                catch (Exception e)
                {
                    return "Format exception: " + e.getMessage();
                }
            }
        }

        public String getJavaSqlTimestamp()
        {
            if (javaSqlTimestamp == null)
            {
                return "";
            }
            else
            {
                try
                {
                    return DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG).format(this.javaSqlTimestamp) + " (" + this.javaSqlTimestamp + ")";
                }
                catch (Exception e)
                {
                    return "Format exception: " + e.getMessage();
                }
            }
        }


        public String getJavaTimeInstant()
        {
            if (javaTimeInstant == null)
            {
                return "";
            }
            else
            {
                try
                {
                    return DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(this.javaTimeInstant.atZone(ZoneId.systemDefault())) + " (" + this.javaTimeInstant.getEpochSecond() + ")";
                }
                catch (Exception e)
                {
                    return "Format exception: " + e.getMessage();
                }
            }
        }

        public String getJavaTimeLocalDateTime()
        {
            if (javaTimeLocalDateTime == null)
            {
                return "";
            }
            else
            {
                try
                {
                    return DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(this.javaTimeLocalDateTime) + " (" + this.javaTimeLocalDateTime + ")";
                }
                catch (Exception e)
                {
                    return "Format exception: " + e.getMessage();
                }
            }
        }

        public String getJavaTimeZonedDateTime()
        {
            if (javaTimeZonedDateTime == null)
            {
                return "";
            }
            else
            {
                try
                {
                    return DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(this.javaTimeZonedDateTime) + " (" + this.javaTimeZonedDateTime.toInstant().getEpochSecond() + ")";
                }
                catch (Exception e)
                {
                    return "Format exception: " + e.getMessage();
                }
            }
        }


        public String getEpochSeconds()
        {
            if (epochSeconds == null)
            {
                return "";
            }
            else
            {
                return epochSeconds.toString();
            }
        }

        public String getString()
        {
            return stringRepresentation;
        }


    }
}
