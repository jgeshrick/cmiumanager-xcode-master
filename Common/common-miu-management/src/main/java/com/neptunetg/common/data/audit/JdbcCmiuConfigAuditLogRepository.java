/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.audit;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.ConfigSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Config set audit log in RDS
 */
@Repository
@Profile("jdbc") //only create this bean if "jdbc" Spring profile is active
public class JdbcCmiuConfigAuditLogRepository implements CmiuConfigAuditLogRepository
{
    private static final int MAX_STRING_LENGTH = 1024; //this is the size of the varchar(1024) in audit_list.old_values and audit_list.new_values

    private final JdbcTemplate db;

    @Autowired
    public JdbcCmiuConfigAuditLogRepository(JdbcTemplate db)
    {
        this.db = db;
    }

    @Override
    public void logMiuChangeConfigSet(MiuId miuId, ConfigSet oldConfigSet, ConfigSet newConfigSet, String requester)
    {
        String oldConfig = oldConfigSet == null ? "???" : oldConfigSet.toString();

        String newConfig = newConfigSet == null ? "???" : newConfigSet.toString();

        addAuditEntry(CmiuConfigAuditLogEventType.AUDIT_EVENT_TYPE_MIU_CONFIG_CHANGED,
                miuId,
                oldConfig,
                newConfig,
                requester);
    }

    @Override
    public void logConfigSetChange(ConfigSet oldConfigSet, ConfigSet modifiedConfigSet, String requester)
    {

        addAuditEntry(CmiuConfigAuditLogEventType.AUDIT_EVENT_TYPE_CONFIG_CHANGE,
                null, oldConfigSet.toString(), // miu id is not relevant here
                modifiedConfigSet.toString(),
                requester);
    }

    @Override
    public void logConfigSetCreated(ConfigSet newConfigSet, String requester)
    {
        addAuditEntry(CmiuConfigAuditLogEventType.AUDIT_EVENT_TYPE_CONFIG_NEW,
                null,
                "",
                newConfigSet.toString(),
                requester);
    }


    private void addAuditEntry(CmiuConfigAuditLogEventType eventType, MiuId miuId, String oldValue, String newValue, String userName)
    {
        final String insertAuditSql = "INSERT INTO mdce.audit_list " +
                "(audit_time, audit_type, miu_id, old_values, new_values, audit_user_name) VALUES (NOW(),?,?,?,?,?)";

        Integer miuIdParameterValue = miuId == null ? null : miuId.numericValue();

        this.db.update(insertAuditSql, eventType.getId(), miuIdParameterValue,
                truncate(oldValue, MAX_STRING_LENGTH), truncate(newValue, MAX_STRING_LENGTH), userName);

    }


    /**
     * Ensure String length is not longer than requested size.
     *
     * @param value  the string to be truncated.  May be null
     * @param length max allowable length of the string, the rest will be truncated off.
     * @return the truncated string
     */
    private static String truncate(String value, int length)
    {
        if (value == null)
        {
            return null;
        }
        else if (value.length() > length)
        {
            return value.substring(0, length);
        }
        else
        {
            return value;
        }
    }
}
