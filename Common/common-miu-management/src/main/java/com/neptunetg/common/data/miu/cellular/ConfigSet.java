/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.data.miu.cellular;

import com.neptunetg.common.data.miu.MiuId;

/**
 * Represents a row from the CMIU_CONFIG_SET table
 */
public class ConfigSet
{
    private long id;
    private String name;
    private String cmiuModeName;
    private Integer reportingStartMins;
    private Integer reportingIntervalMins;
    private Integer reportingTransmitWindowMins;
    private Integer reportingQuietStartMins;
    private Integer reportingQuietEndMins;
    private Integer reportingNumberOfRetries;
    /**
     * TODO: is this really part of the config set?  Seems to be null always at the moment
     */
    private int reportingSyncWarningMins;
    /**
     * TODO: is this really part of the config set?  Seems to be null always at the moment
     */
    private int reportingSyncErrorMins;
    private Integer recordingStartTimeMins;
    private Integer recordingIntervalMins;

    private boolean officialMode;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getReportingStartMins()
    {
        return reportingStartMins;
    }

    public void setReportingStartMins(Integer reportingStartMins)
    {
        this.reportingStartMins = reportingStartMins;
    }

    public Integer getReportingIntervalMins()
    {
        return reportingIntervalMins;
    }

    public void setReportingIntervalMins(Integer reportingIntervalMins)
    {
        this.reportingIntervalMins = reportingIntervalMins;
    }

    public Integer getReportingTransmitWindowMins()
    {
        return reportingTransmitWindowMins;
    }

    public void setReportingTransmitWindowMins(Integer reportingTransmitWindowMins)
    {
        this.reportingTransmitWindowMins = reportingTransmitWindowMins;
    }

    public Integer getReportingQuietStartMins()
    {
        return reportingQuietStartMins;
    }

    public void setReportingQuietStartMins(Integer reportingQuietStartMins)
    {
        this.reportingQuietStartMins = reportingQuietStartMins;
    }

    public Integer getReportingQuietEndMins()
    {
        return reportingQuietEndMins;
    }

    public void setReportingQuietEndMins(Integer reportingQuietEndMins)
    {
        this.reportingQuietEndMins = reportingQuietEndMins;
    }

    public Integer getReportingNumberOfRetries()
    {
        return reportingNumberOfRetries;
    }

    public void setReportingNumberOfRetries(Integer reportingNumberOfRetries)
    {
        this.reportingNumberOfRetries = reportingNumberOfRetries;
    }

    public Integer getReportingSyncWarningMins()
    {
        return reportingSyncWarningMins;
    }

    public void setReportingSyncWarningMins(Integer reportingSyncWarningMins)
    {
        this.reportingSyncWarningMins = reportingSyncWarningMins;
    }

    public int getReportingSyncErrorMins()
    {
        return reportingSyncErrorMins;
    }

    public void setReportingSyncErrorMins(int reportingSyncErrorMins)
    {
        this.reportingSyncErrorMins = reportingSyncErrorMins;
    }

    public Integer getRecordingStartTimeMins()
    {
        return recordingStartTimeMins;
    }

    public void setRecordingStartTimeMins(Integer recordingStartTimeMins)
    {
        this.recordingStartTimeMins = recordingStartTimeMins;
    }

    public Integer getRecordingIntervalMins()
    {
        return recordingIntervalMins;
    }

    public void setRecordingIntervalMins(Integer recordingIntervalMins)
    {
        this.recordingIntervalMins = recordingIntervalMins;
    }

    public String getCmiuModeName()
    {
        return cmiuModeName;
    }

    public void setCmiuModeName(String cmiuModeName)
    {
        this.cmiuModeName = cmiuModeName;
    }

    public boolean getOfficialMode()
    {
        return officialMode;
    }

    public void setOfficialMode(boolean officialMode)
    {
        this.officialMode = officialMode;
    }

    /**
     * Check that the config set has similar values.
     * @param compareConfigSet the config set to compare
     * @return true if the config value are the same
     */
    public boolean hasSameConfig(ConfigSet compareConfigSet)
    {
        if (compareConfigSet == null)
        {
            return false;
        }
        return compareConfigSet.recordingStartTimeMins == this.recordingStartTimeMins &&
                compareConfigSet.recordingIntervalMins == this.recordingIntervalMins &&
                compareConfigSet.reportingStartMins == this.reportingStartMins &&
                compareConfigSet.reportingIntervalMins == this.reportingIntervalMins &&
                compareConfigSet.reportingNumberOfRetries == this.reportingNumberOfRetries &&
                compareConfigSet.reportingTransmitWindowMins == this.reportingTransmitWindowMins &&
                compareConfigSet.reportingQuietStartMins == this.reportingQuietStartMins &&
                compareConfigSet.reportingQuietEndMins == this.reportingQuietEndMins;

    }

    /**
     * Get a summary of the differences
     * @param compareConfigSet Other config set to compare
     * @param labelForThisConfigSet Display tag for this config set
     * @param labelForOtherConfigSet Display tag for the other config set
     * @return String summary
     */
    public String getDifferencesSummary(ConfigSet compareConfigSet, String labelForThisConfigSet, String labelForOtherConfigSet)
    {
        if (compareConfigSet == null)
        {
            return labelForOtherConfigSet + " is null";
        }
        final StringBuilder ret = new StringBuilder();
        appendDifference(ret, "recording start time", labelForThisConfigSet, this.recordingStartTimeMins, labelForOtherConfigSet, compareConfigSet.recordingStartTimeMins);
        appendDifference(ret, "recording interval", labelForThisConfigSet, this.recordingIntervalMins, labelForOtherConfigSet, compareConfigSet.recordingIntervalMins);
        appendDifference(ret, "reporting start time", labelForThisConfigSet, this.reportingStartMins, labelForOtherConfigSet, compareConfigSet.reportingStartMins);
        appendDifference(ret, "reporting interval", labelForThisConfigSet, this.reportingIntervalMins, labelForOtherConfigSet, compareConfigSet.reportingIntervalMins);
        appendDifference(ret, "reporting retries", labelForThisConfigSet, this.reportingNumberOfRetries, labelForOtherConfigSet, compareConfigSet.reportingNumberOfRetries);
        appendDifference(ret, "reporting transmit window", labelForThisConfigSet, this.reportingTransmitWindowMins, labelForOtherConfigSet, compareConfigSet.reportingTransmitWindowMins);
        appendDifference(ret, "reporting quiet time start", labelForThisConfigSet, this.reportingQuietStartMins, labelForOtherConfigSet, compareConfigSet.reportingQuietStartMins);
        appendDifference(ret, "reporting quiet time end", labelForThisConfigSet, this.reportingQuietEndMins, labelForOtherConfigSet, compareConfigSet.reportingQuietEndMins);
        return ret.toString();
    }

    private void appendDifference(StringBuilder ret, String fieldLabel, String labelForThisConfigSet, int valueForThisConfigSet, String labelForOtherConfigSet, int valueForOtherConfigSet)
    {
        if (valueForThisConfigSet != valueForOtherConfigSet)
        {
            if (ret.length() > 0)
            {
                ret.append("; ");
            }
            ret.append(fieldLabel).append(": ")
                    .append(valueForThisConfigSet).append(" (").append(labelForThisConfigSet).append(") ")
                    .append(valueForOtherConfigSet).append(" (").append(labelForOtherConfigSet).append(")");
        }
    }


    /**
     * Get an object array containing the values in the following order:
     * reporting_start_mins, reporting_interval_mins, reporting_number_of_retries, reporting_transmit_windows_mins,
     * reporting_quiet_start_mins, reporting_quiet_end_mins,
     * recording_start_time_mins, recording_interval_mins
     */
    public Object[] getValuesAsBindParams()
    {
        return new Object[]
                {
                        Integer.valueOf(this.reportingStartMins), Integer.valueOf(this.reportingIntervalMins), Integer.valueOf(this.reportingNumberOfRetries), Integer.valueOf(this.reportingTransmitWindowMins),
                        Integer.valueOf(this.reportingQuietStartMins), Integer.valueOf(this.reportingQuietEndMins),
                        Integer.valueOf(this.recordingStartTimeMins), Integer.valueOf(this.recordingIntervalMins)
                };
    }
}

