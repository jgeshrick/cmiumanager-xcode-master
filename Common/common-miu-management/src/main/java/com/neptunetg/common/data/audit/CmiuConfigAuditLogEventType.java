/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.audit;

/**
 * Needs to be consistent with com.neptunetg.mdce.common.internal.audit.AuditEventType in mdce-common-internal-api
 */
public enum CmiuConfigAuditLogEventType
{
    AUDIT_EVENT_TYPE_UNKNOWN(0, "Unknown"),
    AUDIT_EVENT_TYPE_MIU_CONFIG_CHANGED(2, "MIU config changed"),
    AUDIT_EVENT_TYPE_CONFIG_NEW(10, "Config set created"),
    AUDIT_EVENT_TYPE_CONFIG_CHANGE(11, "Config set change");

    private final int id;
    private final String name;

    CmiuConfigAuditLogEventType(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public static CmiuConfigAuditLogEventType fromId(int id)
    {

        for (CmiuConfigAuditLogEventType type : values())
        {
            if(type.getId() == id)
            {
                return type;
            }
        }

        return AUDIT_EVENT_TYPE_UNKNOWN;
    }

    public int getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

}
