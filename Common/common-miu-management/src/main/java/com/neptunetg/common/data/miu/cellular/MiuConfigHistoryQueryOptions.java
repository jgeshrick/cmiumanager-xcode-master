/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.cellular;

import com.neptunetg.common.data.miu.MiuId;

import java.time.Instant;

/**
 * Query options for retrieving MIU configuration history.
 */
public class MiuConfigHistoryQueryOptions
{
    /**
     * When not null, include results for a specific MIU ID only.
     */
    private MiuId miuId;

    /**
     * When not null, include MIU IDs equal to or greater than fromMiuId.
     */
    private MiuId fromMiuId;

    /**
     * When not null, include MIU IDs equal to or less than than toMiuId.
     */
    private MiuId toMiuId;

    /**
     * When not null, include results for a specific Site ID only.
     */
    private Integer siteId;

    /**
     * When not null, include configuration history that applies from a point in time.
     */
    private Instant startDate;

    /**
     * When not null, include configuration history that applies until a point in time.
     */
    private Instant endDate;

    /**
     * When not null, include only configurations with the specified value of meterActive.
     */
    private Boolean meterActive;

    /**
     * When not null, filters by configuration history entries that were either requested by MDCE (false) or confirmed
     * by the MIU (true).
     */
    private Boolean receivedFromMiu;

    public MiuId getMiuId()
    {
        return miuId;
    }

    public void setMiuId(MiuId miuId)
    {
        this.miuId = miuId;
    }

    public Instant getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Instant startDate)
    {
        this.startDate = startDate;
    }

    public Instant getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Instant endDate)
    {
        this.endDate = endDate;
    }

    public Integer getSiteId()
    {
        return siteId;
    }

    public void setSiteId(Integer siteId)
    {
        this.siteId = siteId;
    }

    public MiuId getFromMiuId()
    {
        return fromMiuId;
    }

    public void setFromMiuId(MiuId fromMiuId)
    {
        this.fromMiuId = fromMiuId;
    }

    public MiuId getToMiuId()
    {
        return toMiuId;
    }

    public void setToMiuId(MiuId toMiuId)
    {
        this.toMiuId = toMiuId;
    }

    public Boolean getMeterActive()
    {
        return meterActive;
    }

    public void setMeterActive(Boolean meterActive)
    {
        this.meterActive = meterActive;
    }

    public Boolean getReceivedFromMiu()
    {
        return receivedFromMiu;
    }

    public void setReceivedFromMiu(Boolean receivedFromMiu)
    {
        this.receivedFromMiu = receivedFromMiu;
    }
}
