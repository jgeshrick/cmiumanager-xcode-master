/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.miu.command;

/**
 * Define the lifecycle state of a miu command 
 */
public enum CommandLifeCycle
{
    CREATED("created"),
    QUEUED("queued"),
    COMMITTED("committed"),
    RECALLED("recalled"),
    REJECTED("rejected"),
    CANCELLED("cancelled"),
    ACCEPTED("accepted");

    private final String sqlString;

    private CommandLifeCycle(String sqlEnumString)
    {
        this.sqlString = sqlEnumString;
    }

    public String getSqlString()
    {
        return this.sqlString;
    }

}
