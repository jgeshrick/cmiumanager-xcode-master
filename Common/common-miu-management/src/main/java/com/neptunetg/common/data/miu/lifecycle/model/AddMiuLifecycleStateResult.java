/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.lifecycle.model;

/**
 * Models the result of a batch insert of MIU lifecycle states.
 */
public class AddMiuLifecycleStateResult
{
    /**
     * The client-supplied identity supplied with the original HistoricMiuLifecycleState instance.
     */
    private int referenceId;

    /**
     * Indicates whether the record was successfully inserted.
     */
    private boolean isOk;

    /**
     * Contains details of any error.
     */
    private String error;

    public AddMiuLifecycleStateResult()
    {
    }

    public AddMiuLifecycleStateResult(int referenceId)
    {
        this.referenceId = referenceId;
    }

    public int getReferenceId()
    {
        return referenceId;
    }

    public void setReferenceId(int referenceId)
    {
        this.referenceId = referenceId;
    }

    public boolean isOk()
    {
        return isOk;
    }

    public void setOk(boolean ok)
    {
        isOk = ok;
    }

    public String getError()
    {
        return error;
    }

    public void setError(String error)
    {
        this.error = error;
    }
}
