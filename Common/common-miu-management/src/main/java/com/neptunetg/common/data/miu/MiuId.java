/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.data.miu;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;import java.lang.Integer;import java.lang.Long;import java.lang.Object;import java.lang.Override;import java.lang.String;

/**
 * Represents an MIU ID.  Value Object pattern.
 */
public final class MiuId implements Serializable
{

    private static final long serialVersionUID = 1L;

    private final int miuId;

    public MiuId(int id)
    {
        this.miuId = id;
    }

    public MiuId(long id)
    {
        this.miuId = Long.valueOf(id).intValue();
    }

    /**
     * String-arg constructor supports Spring MVC URL binding
     * @param id number as string
     */
    public MiuId(String id)
    {
        this.miuId = Integer.parseInt(id);
    }

    public int numericValue()
    {
        return miuId;
    }

    public Integer numericWrapperValue()
    {
        return Integer.valueOf(miuId);
    }

    public String toString()
    {
        return Integer.toString(numericValue());
    }

    public static MiuId valueOf(int id)
    {
        return new MiuId(id);
    }

    public static MiuId valueOf(long id)
    {
        return new MiuId((int) id);
    }

    public static MiuId valueOf(Long id)
    {
        if (id == null)
        {
            return null;
        }
        else
        {
            return new MiuId(id.intValue());
        }
    }

    public static MiuId valueOf(Integer id)
    {
        if (id == null)
        {
            return null;
        }
        else
        {
            return new MiuId(id.intValue());
        }
    }

    public static MiuId fromString(String id)
    {
        if (StringUtils.hasText(id))
        {
            int x = Integer.parseInt(id);
            return new MiuId(x);
        }
        else
        {
            return null;
        }
    }

    public boolean matches(long otherNumericMiuId)
    {
        return miuId == otherNumericMiuId;
    }

    public boolean isInCmiuIdRange()
    {
        return 400000000 <= miuId && miuId <= 600000000;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MiuId miuId1 = (MiuId) o;

        return miuId == miuId1.miuId;

    }

    /**
     * More efficient version of ObjectUtils.nullSafeEquals for MIU IDs.
     * @param miuId1 First MIU ID
     * @param miuId2 Second MIU ID
     * @return true if both are null or they are equal.
     */
    public static boolean nullSafeEquals(MiuId miuId1, MiuId miuId2)
    {
        if (miuId1 == null)
        {
            return miuId2 == null;
        }
        return miuId1.miuId == miuId2.miuId;
    }

    @Override
    public int hashCode()
    {
        return miuId;
    }

    public static class ToStringConverter implements com.fasterxml.jackson.databind.util.Converter<MiuId, String>
    {

        @Override
        public String convert(MiuId arg0)
        {
            return arg0.toString();
        }

        @Override
        public JavaType getInputType(TypeFactory arg0)
        {
            return arg0.constructType(MiuId.class);
        }

        @Override
        public JavaType getOutputType(TypeFactory arg0)
        {
            return arg0.constructType(String.class);
        }

    }

}
