/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.service.miu.lifecycle;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.MiuLifecycleRepo;
import com.neptunetg.common.data.miu.lifecycle.model.AddMiuLifecycleStateResult;
import com.neptunetg.common.data.miu.lifecycle.model.HistoricMiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by WJD1 on 19/05/2016.
 * Service for setting and getting lifecycle information for CMIUs
 */
@Profile("jdbc") //only create this bean if "jdbc" Spring profile is active
@Service
@Transactional
public class MiuLifecycleServiceImpl implements MiuLifecycleService
{
    private MiuLifecycleRepo miuLifecycleRepo;

    private static final Logger log = LoggerFactory.getLogger(MiuLifecycleServiceImpl.class);

    private CheckCmiuLifecycleStateTransitions lifecycleStateTransitionChecker = new CheckCmiuLifecycleStateTransitions();

    @Autowired
    public MiuLifecycleServiceImpl(MiuLifecycleRepo miuLifecycleRepo)
    {
        this.miuLifecycleRepo = miuLifecycleRepo;
    }

    @Override
    public MiuLifecycleState getMiuLifecycleState(MiuId miuId)
    {
        return miuLifecycleRepo.getMiuLifecycleState(miuId);
    }

    @Override
    public boolean setMiuLifecycleState(MiuId miuId, MiuLifecycleStateEnum miuLifecycleState)
    {
        MiuLifecycleState currentMiuLifecycleState = miuLifecycleRepo.getMiuLifecycleState(miuId);

        if(currentMiuLifecycleState == null)
        {
            log.debug("Attempt to change CMIU {} state, but CMIU doesn't exist", miuId.numericValue());
            return false;
        }

        MiuLifecycleStateEnum lifecycleState = currentMiuLifecycleState.getLifecycleState();

        if(lifecycleState != null)
        {
            if(miuId.isInCmiuIdRange())
            {
                if (!lifecycleStateTransitionChecker.checkLifecycleStateTransition(
                        lifecycleState, miuLifecycleState))
                {
                    log.debug("Attempt to set CMIU {} to disallowed state from {} to {}",
                            miuId.numericValue(),
                            lifecycleState.name(),
                            miuLifecycleState.name());

                    return false;
                }
            }
            else
            {
                //Todo - check if L900 state transition is allowed. Allow all for now
            }
        }

        return miuLifecycleRepo.setMiuLifecycleState(miuId, miuLifecycleState);
    }

    @Override
    public List<MiuLifecycleState> getMiuLifecycleStateHistory(MiuId miuId)
    {
        if(miuLifecycleRepo.getMiuLifecycleState(miuId) == null)
        {
            return null;
        }

        return miuLifecycleRepo.getMiuLifecycleStateHistory(miuId);
    }

    @Override
    public List<MiuLifecycleState> getCmiusInLifecycleStateBefore(MiuLifecycleStateEnum miuLifecycleState,
                                                                   Instant maximumTransistionInstant)
    {
        return miuLifecycleRepo.getMiusInLifecycleStateForLongerThan(miuLifecycleState, maximumTransistionInstant);
    }

    @Override
    public List<MiuLifecycleStateTransistion> getMiusInLifecycleStateWithPreviousStateSetBefore(
            MiuLifecycleStateEnum currentMiuLifecycleState, MiuLifecycleStateEnum previousMiuLifecycleState,
            Instant maximumTransitionInstant)
    {
        List<MiuLifecycleState> cmiusInLifecycleState =
                miuLifecycleRepo.getMiusInLifecycleState(currentMiuLifecycleState);

        List<MiuLifecycleStateTransistion> cmiusToReturn = new ArrayList<>();

        for(MiuLifecycleState cmiuLifecycleState : cmiusInLifecycleState)
        {
            MiuLifecycleState oldState = miuLifecycleRepo.getPreviousLifecycleStateForMiu(
                    cmiuLifecycleState.getMiuId(),
                    previousMiuLifecycleState);

            if(oldState != null && oldState.getTransitionInstant().isBefore(maximumTransitionInstant))
            {
                MiuLifecycleStateTransistion miuLifecycleStateTransistion = new MiuLifecycleStateTransistion();
                miuLifecycleStateTransistion.setCurrentMiuLifecycleState(cmiuLifecycleState);
                miuLifecycleStateTransistion.setPreviousMiuLifecycleState(oldState);
                cmiusToReturn.add(miuLifecycleStateTransistion);
            }
        }

        return cmiusToReturn;
    }

    @Override
    public List<MiuLifecycleState> getMiusStuckInLifecycleForAlerts(Duration duration)
    {
        return miuLifecycleRepo.getMiusInLifecycleStateForLongerThan(duration);
    }

    @Override
    public boolean checkMiuBpcLifecycleTransition(MiuId miuId, MiuLifecycleStateEnum newLifecycleState)
    {
        //Miu that's passed doesn't contain current Lifecycle state. also doesn't return it in Enum format
        MiuLifecycleState currentState = getMiuLifecycleState(miuId);
        if(currentState != null)
        {
            MiuLifecycleStateEnum currentStateEnum = currentState.getLifecycleState();
            return lifecycleStateTransitionChecker.checkLifecycleStateTransition(currentStateEnum, newLifecycleState);
        }
        else
        {
            log.debug("Error retrieving current lifecycle state");
            return false;
        }

    }

    @Override
    public List<MiuLifecycleState> getMiuLifecycleHistory(Integer lastSequenceId)
    {
        return miuLifecycleRepo.getMiuLifecycleStates(lastSequenceId);
    }

    @Override
    public List<AddMiuLifecycleStateResult> addMiuLifecycleStates(List<HistoricMiuLifecycleState> historicStates)
    {
        return miuLifecycleRepo.addMiuLifecycleStates(historicStates);
    }
}
