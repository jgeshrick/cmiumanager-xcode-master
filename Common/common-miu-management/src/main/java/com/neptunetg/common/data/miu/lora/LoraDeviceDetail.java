/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.lora;

import com.neptunetg.common.data.miu.Eui;
import com.neptunetg.common.data.miu.MiuId;

import java.time.Instant;

/**
 * Represent RDS data model: lora_device_details
 */
public class LoraDeviceDetail
{
    private MiuId miuId;
    private LoraNetwork loraNetwork;
    private Instant lastUpdateTime;
    private Eui eui;
    private Eui appEui;
    private String appKey;
    private LoraAntennaType loraAntennaType;

    public LoraDeviceDetail(MiuId miuId, LoraNetwork loraNetwork, Instant lastUpdateTime, Eui eui,
                            Eui appEui, String appKey, LoraAntennaType loraAntennaType)
    {
        this.miuId = miuId;
        this.loraNetwork = loraNetwork;
        this.lastUpdateTime = lastUpdateTime;
        this.eui = eui;
        this.appEui = appEui;
        this.appKey = appKey;
        this.loraAntennaType = loraAntennaType;
    }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public void setMiuId(MiuId miuId)
    {
        this.miuId = miuId;
    }

    public LoraNetwork getLoraNetwork()
    {
        return loraNetwork;
    }

    public void setLoraNetwork(LoraNetwork loraNetwork)
    {
        this.loraNetwork = loraNetwork;
    }

    public Instant getLastUpdateTime()
    {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Instant lastUpdateTime)
    {
        this.lastUpdateTime = lastUpdateTime;
    }

    public Eui getEui()
    {
        return eui;
    }

    public void setEui(Eui eui)
    {
        this.eui = eui;
    }

    public Eui getAppEui()
    {
        return appEui;
    }

    public void setAppEui(Eui appEui)
    {
        this.appEui = appEui;
    }

    public String getAppKey()
    {
        return appKey;
    }

    public void setAppKey(String appKey)
    {
        this.appKey = appKey;
    }

    public LoraAntennaType getLoraAntennaType()
    {
        return loraAntennaType;
    }

    public void setLoraAntennaType(LoraAntennaType loraAntennaType)
    {
        this.loraAntennaType = loraAntennaType;
    }

    @Override
    public String toString()
    {
        return "LoraDeviceDetails{" +
                "miuId=" + miuId +
                ", loraNetwork=" + loraNetwork +
                ", lastUpdateTime=" + lastUpdateTime +
                ", eui=" + eui +
                ", appEui=" + appEui +
                ", appKey=" + appKey +
                ", antennaType=" + loraAntennaType +
                '}';
    }
}
