/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.lifecycle.model;

import java.time.Instant;

import com.neptunetg.common.data.miu.MiuId;


/**
 * Created by WJD1 on 19/05/2016.
 * Class to represent the lifecycle state for a CMIU or L900
 */
public class MiuLifecycleState
{
    private int sequenceId;
    private MiuId miuId;
    private MiuLifecycleStateEnum lifecycleState;
    private Instant transitionInstant;

    public MiuId getMiuId()
    {
        return miuId;
    }

    public void setMiuId(MiuId miuId)
    {
        this.miuId = miuId;
    }

    public MiuLifecycleStateEnum getLifecycleState()
    {
        return lifecycleState;
    }

    public void setLifecycleState(MiuLifecycleStateEnum lifecycleState)
    {
        this.lifecycleState = lifecycleState;
    }

    public Instant getTransitionInstant()
    {
        return transitionInstant;
    }

    public void setTransitionInstant(Instant transitionInstant)
    {
        this.transitionInstant = transitionInstant;
    }

    public int getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(int sequenceId)
    {
        this.sequenceId = sequenceId;
    }
}
