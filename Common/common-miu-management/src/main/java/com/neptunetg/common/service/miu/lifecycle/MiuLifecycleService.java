/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.service.miu.lifecycle;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.AddMiuLifecycleStateResult;
import com.neptunetg.common.data.miu.lifecycle.model.HistoricMiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

/**
 * Created by WJD1 on 19/05/2016.
 * Service for setting and getting lifecycle information for CMIUs and L900s
 */
public interface MiuLifecycleService
{
    /**
     * Get the current lifecycle state of a CMIU or L900
     * @param miuId the miu ID to get the lifecycle state for
     * @return The lifecycle state of the MIU, unpopulated if no state, null if MIU doesn't exist
     */
    MiuLifecycleState getMiuLifecycleState(MiuId miuId);

    /**
     * Set the lifecycle state of a CMIU or L900
     * @param miuId The CMIU or L900 to set the lifecycle state for
     * @param miuLifecycleState The lifecycle state to set the MIU to
     * @return true if successfully set the lifecycle state, false if disallowed or MIU doesn't exist
     */
    boolean setMiuLifecycleState(MiuId miuId, MiuLifecycleStateEnum miuLifecycleState);

    /**
     * Get the lifecycle state history for a CMIU or L900
     * @param miuId the CMIU or L900 ID to get the lifecycle state history for
     * @return The lifecycle state of the MIU, empty if no history, null if MIU doesn't exist
     */
    List<MiuLifecycleState> getMiuLifecycleStateHistory(MiuId miuId);

    /**
     * Get all CMIUs or L900s that transitioned to lifecycle state before instant
     * @param miuLifecycleState The lifecycle state to filter with
     * @param maximumTransistionInstant The latest instant the MIU should have transitioned to that lifecycle state
     * @return List of MIUs in that lifecycle state since before maximumTime, empty if none
     */
    List<MiuLifecycleState> getCmiusInLifecycleStateBefore(MiuLifecycleStateEnum miuLifecycleState,
                                                            Instant maximumTransistionInstant);

    /**
     * Get a list of MIUs in a specific state, where the transistion instant of a previous state is
     * before the specified maximum transistion instant
     * @param currentMiuLifecycleState The current lifecycle state to filter MIUs by
     * @param previousMiuLifecycleState The previous lifecycle state to check the transistion instant of
     * @param maximumTransitionInstant The maximum transition time for the previous state
     * @return list of objects containing the current state, and previous state. An empty list if no relevant MIUs
     */
    List<MiuLifecycleStateTransistion> getMiusInLifecycleStateWithPreviousStateSetBefore(
            MiuLifecycleStateEnum currentMiuLifecycleState, MiuLifecycleStateEnum previousMiuLifecycleState,
            Instant maximumTransitionInstant);

    /**
     * Get a list of MIUs that are between Preactivated and Confirmed states
     * That are older than X days
     * @param duration of how old to look for state changes
     * @return list of objects containing the Miuid, miu_lifecycle_id, timestamp, and current state
     */
    List<MiuLifecycleState> getMiusStuckInLifecycleForAlerts(Duration duration);

    /**
     * Gets the lifecycle state history for MIUs starting after the supplied sequence ID.
     * @param lastSequenceId When non-null, the last sequence ID known to the client.
     * @return A list of lifecycle state history entries.
     */
    List<MiuLifecycleState> getMiuLifecycleHistory(Integer lastSequenceId);

    /**
     * Attempts to add the supplied MIU lifecycle states to the lifecycle history as a batch operation.
     * @param historicStates A list of MIU lifecycle states to be added to the history.
     * @return A list containing the result of each insert operation.
     */
    List<AddMiuLifecycleStateResult> addMiuLifecycleStates(List<HistoricMiuLifecycleState> historicStates);

    /**
     * Checks to see if the Lifecycle State transition is a valid one
     * @param miuId MiuId of Miu to get current state of
     * @param newLifecycleState lifecycle state to be set
     * @return
     */
    boolean checkMiuBpcLifecycleTransition(MiuId miuId, MiuLifecycleStateEnum newLifecycleState);
}
