/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */
package com.neptunetg.common.data.util;

import org.springframework.jdbc.support.rowset.SqlRowSet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * As of August 2016, JPA does not support the new java.time types - see https://java.net/jira/browse/JPA_SPEC-63
 *
 * This class provides static methods to ensure that these date objects can be bound and retrieved correctly.
 *
 * If JPA adds support for these date types in future, these methods can be simplified.
 */
public class JdbcDateUtil
{
    private JdbcDateUtil()
    {
    }

    /**
     * Bind an Instant to a prepared statement
     * @param instant instant to bind
     * @return java.sql.Timestamp corresponding to the instant, or null if instant is null
     */
    public static Object bindDate(Instant instant)
    {
        if (instant == null)
        {
            return null;
        }
        else
        {
            return Timestamp.from(instant);
        }
    }

    /**
     * Bind an Instant to a prepared statement, or a default value if instant is null
     * @param instant instant to bind
     * @param defaultInstant instant to bind if "instant" is null.
     * @return java.sql.Timestamp corresponding to the instant
     */
    public static Object bindDateWithDefault(Instant instant, Instant defaultInstant)
    {
        if (instant == null)
        {
            if (defaultInstant == null)
            {
                return null;
            }
            else
            {
                return Timestamp.from(instant);
            }
        }
        else
        {
            return Timestamp.from(instant);
        }
    }

    /**
     * Bind a ZonedDateTime to a prepared statement
     * @param zonedDateTime ZonedDateTime to bind
     * @return java.sql.Timestamp corresponding to zonedDateTime, or null if zonedDateTime is null
     */
    public static Object bindDate(ZonedDateTime zonedDateTime)
    {
        if (zonedDateTime == null)
        {
            return null;
        }
        else
        {
            return Timestamp.from(zonedDateTime.toInstant());
        }
    }


    /**
     * Bind a ZonedDateTime to a prepared statement, or a default value if null
     * @param zonedDateTime ZonedDateTime to bind
     * @param defaultZonedDateTime value to bind if zonedDateTime is null
     * @return java.sql.Timestamp corresponding to zonedDateTime, or to defaultZonedDateTime if zonedDateTime is null
     */
    public static Object bindDateWithDefault(ZonedDateTime zonedDateTime, ZonedDateTime defaultZonedDateTime)
    {
        if (zonedDateTime == null)
        {
            if (defaultZonedDateTime == null)
            {
                return null;
            }
            else
            {
                return Timestamp.from(defaultZonedDateTime.toInstant());
            }
        }
        else
        {
            return Timestamp.from(zonedDateTime.toInstant());
        }
    }


    /**
     * Bind a java.util.Date to a prepared statement
     * @param javaUtilDate java.util.Date to bind
     * @return java.sql.Timestamp corresponding to the date, or null if date is null
     */
    public static Object bindDate(Date javaUtilDate)
    {
        if (javaUtilDate == null)
        {
            return null;
        }
        else
        {
            return new Timestamp(javaUtilDate.getTime());
        }
    }

    /**
     * Bind a java.util.Date to a prepared statement, or a default value if null
     * @param javaUtilDate java.util.Date to bind
     * @param  defaultJavaUtilDate value to bind if javaUtilDate is null
     * @return java.sql.Timestamp corresponding to javaUtilDate, or to defaultJavaUtilDate null if javaUtilDate is null
     */
    public static Object bindDateWithDefault(Date javaUtilDate, Date defaultJavaUtilDate)
    {
        if (javaUtilDate == null)
        {
            if (defaultJavaUtilDate == null)
            {
                return null;
            }
            else
            {
                return new Timestamp(defaultJavaUtilDate.getTime());
            }
        }
        else
        {
            return new Timestamp(javaUtilDate.getTime());
        }
    }

    /**
     * Bind a LocalDate to a prepared statement
     * @param localDate local date to bind
     * @return java.sql.Date corresponding to localDate, or null if localDate is null
     */
    public static Object bindDate(LocalDate localDate)
    {
        if (localDate == null)
        {
            return null;
        }
        else
        {
            return java.sql.Date.valueOf(localDate);
        }
    }


    /**
     * Bind a LocalDate to a prepared statement, or a default value if null
     * @param localDate local date to bind
     * @param defaultLocalDate default value
     * @return java.sql.Date corresponding to localDate, or to defaultLocalDate if localDate is null
     */
    public static Object bindDateWithDefault(LocalDate localDate, LocalDate defaultLocalDate)
    {
        if (localDate == null)
        {
            if (defaultLocalDate == null)
            {
                return null;
            }
            else
            {
                return java.sql.Date.valueOf(defaultLocalDate);
            }
        }
        else
        {
            return java.sql.Date.valueOf(localDate);
        }
    }

    /**
     * Get an Instant from a result set
     * @param resultSet Result set to get value from
     * @param index Index of a timestamp column in the result set
     * @return Instant from result set
     * @throws SQLException If reading of result set fails
     */
    public static Instant getInstant(ResultSet resultSet, int index) throws SQLException
    {
        final Timestamp d = resultSet.getTimestamp(index);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.toInstant();
        }
    }

    /**
     * Get an Instant from a result set
     * @param resultSet Result set to get value from
     * @param columnLabel Label of a timestamp column in the result set
     * @return Instant from result set
     * @throws SQLException If reading of result set fails
     */
    public static Instant getInstant(ResultSet resultSet, String columnLabel) throws SQLException
    {
        final Timestamp d = resultSet.getTimestamp(columnLabel);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.toInstant();
        }
    }

    /**
     * Get an Instant from a SqlRowSet
     * @param resultSet Result set to get value from
     * @param index Index of a timestamp column in the result set
     * @return Instant from result set
     * @throws SQLException If reading of result set fails
     */
    public static Instant getInstant(SqlRowSet resultSet, int index) throws SQLException
    {
        final Timestamp d = resultSet.getTimestamp(index);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.toInstant();
        }
    }

    /**
     * Get an Instant from a SqlRowSet
     * @param resultSet Result set to get value from
     * @param columnLabel Label of a timestamp column in the result set
     * @return Instant from result set
     * @throws SQLException If reading of result set fails
     */
    public static Instant getInstant(SqlRowSet resultSet, String columnLabel) throws SQLException
    {
        final Timestamp d = resultSet.getTimestamp(columnLabel);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.toInstant();
        }
    }

    /**
     * Get a ZonedDateTime from a result set
     * @param resultSet Result set to get value from
     * @param index Index of a timestamp column in the result set
     * @param timezone Time zone desired for return value
     * @return Instant from result set
     * @throws SQLException If reading of result set fails
     */
    public static ZonedDateTime getZonedDateTime(ResultSet resultSet, int index, ZoneId timezone) throws SQLException
    {
        final Timestamp d = resultSet.getTimestamp(index);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.toInstant().atZone(timezone);
        }
    }

    /**
     * Get a ZonedDateTime from a result set
     * @param resultSet Result set to get value from
     * @param columnLabel Label of a timestamp column in the result set
     * @param timezone Time zone desired for return value
     * @return Instant from result set
     * @throws SQLException If reading of result set fails
     */
    public static ZonedDateTime getZonedDateTime(ResultSet resultSet, String columnLabel, ZoneId timezone) throws SQLException
    {
        final Timestamp d = resultSet.getTimestamp(columnLabel);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.toInstant().atZone(timezone);
        }
    }

    /**
     * Get a ZonedDateTime from a SqlRowSet
     * @param resultSet Result set to get value from
     * @param index Index of a timestamp column in the result set
     * @param timezone Time zone desired for return value
     * @return Instant from result set
     * @throws SQLException If reading of result set fails
     */
    public static ZonedDateTime getZonedDateTime(SqlRowSet resultSet, int index, ZoneId timezone) throws SQLException
    {
        final Timestamp d = resultSet.getTimestamp(index);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.toInstant().atZone(timezone);
        }
    }

    /**
     * Get a ZonedDateTime from a SqlRowSet
     * @param resultSet Result set to get value from
     * @param columnLabel Label of a timestamp column in the result set
     * @param timezone Time zone desired for return value
     * @return Instant from result set
     * @throws SQLException If reading of result set fails
     */
    public static ZonedDateTime getZonedDateTime(SqlRowSet resultSet, String columnLabel, ZoneId timezone) throws SQLException
    {
        final Timestamp d = resultSet.getTimestamp(columnLabel);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.toInstant().atZone(timezone);
        }
    }

    /**
     * Get a LocalDate from a result set
     * @param resultSet Result set to get value from
     * @param index Index of a timestamp column in the result set
     * @return Instant from result set
     * @throws SQLException If reading of result set fails
     */
    public static LocalDate getLocalDate(ResultSet resultSet, int index) throws SQLException
    {
        final Timestamp d = resultSet.getTimestamp(index);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.toLocalDateTime().toLocalDate();
        }
    }

    /**
     * Get a LocalDate from a result set
     * @param resultSet Result set to get value from
     * @param columnLabel Label of a DATE column in the result set
     * @return Instant from result set
     * @throws SQLException If reading of result set fails
     */
    public static LocalDate getLocalDate(ResultSet resultSet, String columnLabel) throws SQLException
    {
        final Timestamp d = resultSet.getTimestamp(columnLabel);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.toLocalDateTime().toLocalDate();
        }
    }

    /**
     * Get a LocalDate from a SqlRowSet
     * @param resultSet Result set to get value from
     * @param index Index of a timestamp column in the result set
     * @return Instant from result set
     * @throws SQLException If reading of result set fails
     */
    public static LocalDate getLocalDate(SqlRowSet resultSet, int index) throws SQLException
    {
        final Timestamp d = resultSet.getTimestamp(index);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.toLocalDateTime().toLocalDate();
        }
    }

    /**
     * Get a LocalDate from a SqlRowSet
     * @param resultSet Result set to get value from
     * @param columnLabel Label of a DATE column in the result set
     * @return Instant from result set
     * @throws SQLException If reading of result set fails
     */
    public static LocalDate getLocalDate(SqlRowSet resultSet, String columnLabel) throws SQLException
    {
        final Timestamp d = resultSet.getTimestamp(columnLabel);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.toLocalDateTime().toLocalDate();
        }
    }

}
