/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.lora;


import com.neptunetg.common.data.miu.MiuId;

/**
 * Class for accessing LoRa details about a MIU
 */
public interface LoraRepository
{
    /**
     * Return the device details for a LoRa device
     *
     * @param miuId
     * @return LoraDeviceDetail if they exist, otherwise null
     */
    LoraDeviceDetail getLoraDeviceDetailByMiuId(MiuId miuId);


    /**
     * Return the device details for a LoRa device
     *
     * @param loraDeviceDetailsId
     * @return LoraDeviceDetail if they exist, otherwise null
     */
    LoraDeviceDetail getLoraDeviceDetailByDeviceDetailsId(long loraDeviceDetailsId);


    /**
     * Insert or overwrite lora device details
     * Adds new MIU Details if they don't exist
     * @param loraDeviceDetail
     * @return The database id for the LoRa CAN details
     */
    long insertOrOverwriteLoraDeviceDetail(LoraDeviceDetail loraDeviceDetail);
}

