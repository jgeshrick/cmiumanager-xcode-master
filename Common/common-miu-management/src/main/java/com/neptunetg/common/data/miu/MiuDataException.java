/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */
package com.neptunetg.common.data.miu;

/**
 * Represents an issue related to MIU data
 */
public class MiuDataException extends Exception
{
    private final boolean conflict;

    /**
     * Exception with message only
     * @param message The exception message
     */
    public MiuDataException(String message)
    {
        this(message, false);
    }

    /**
     * Exception with message only
     * @param message The exception message
     */
    private MiuDataException(String message, boolean conflict)
    {
        super(message);
        this.conflict = conflict;
    }

    /**
     * Exception with message and wrapped cause
     * @param message Message
     * @param cause Wrapped cause
     */
    public MiuDataException(String message, Throwable cause)
    {
        super(message, cause);
        this.conflict = false;
    }


    /**
     * Exception with message only, caused by a conflict
     * @param message The exception message
     */
    public static MiuDataException conflict(String message)
    {
        return new MiuDataException(message, true);
    }


    /**
     * Was this exception caused by an ID conflict?
     * @return
     */
    public boolean isConflict()
    {
        return conflict;
    }

}
