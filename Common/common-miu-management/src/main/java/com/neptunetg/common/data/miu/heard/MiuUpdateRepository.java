/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015, 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.data.miu.heard;


import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementSetter;

import java.time.Instant;

public interface MiuUpdateRepository
{

    /**
     * Update heard time and RSSI for a batch of MIUs
     * @param miuIdLastHeardTime Object that sets timestamp:1-3 to JdbcDateUtil.bindDate(lastHeardTime), int:4 to MIUID
     * @param miuIdLastHeardTimeRssi Object that sets int:1 to MIUID, long:2 to JdbcDateUtil.bindDate(lastHeardTime), int:3 and double:3 to the RSSI
     * @param miuType
     */
    void updateMiuHeardTimeAndRssiBatch(BatchPreparedStatementSetter miuIdLastHeardTime, BatchPreparedStatementSetter miuIdLastHeardTimeRssi,
                                               MiuType miuType);



    /**
     * Update the last heard time of the MIU, and various statistics about the MIU
     * @param miuId MIU
     * @param miuType MIU type
     * @param heardTime Heard time
     * @param insertTime The time the packet was inserted into DynamoDB
     * @param packetStats Setter for stats.  Must set stats in following position, or null in that position if not available:
     * [1-3] miu_id, heard_time, miu_packet_count, [4-6] avg_rssi, min_rssi, max_rssi, [7-9] ber, rsrq, processor_reset_counter,
     * [10-12] register_time, register_time_to_activate_context, register_time_connected, [13-14] register_time_to_transfer_packet, disconnect_time,
     * [15-18] mag_swipe_counter, battery_capacity, battery_voltage, temperature
     * @return number of records updated
     */
    int updateMiuHeardAllStats(MiuId miuId, MiuType miuType, Instant heardTime, Instant insertTime,
                               PreparedStatementSetter packetStats);

}