/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.cellular;

import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.util.JdbcDateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * MIU Repository for MySql database
 */
@Profile("jdbc") //only create this bean if "jdbc" Spring profile is active
@Repository
public class JdbcCmiuRepository implements CmiuRepository
{
    // fields
    private final JdbcTemplate db;

    @Autowired
    public JdbcCmiuRepository(JdbcTemplate db)
    {
        this.db = db;
    }

    /**
     * Insert a new data usage row into miu_data_usage.
     *
     * @param iccid     the iccid of the cellular device
     * @param usageDate the date of the usage
     * @param bytesUsedDay number of bytes used in this date
     * @param bytesUsedMonth number of bytes used in this month
     */
    @Override
    public void saveMiuDataUsage(String iccid, LocalDate usageDate, Instant usageDatetime, int bytesUsedDay, long bytesUsedMonth) throws MiuDataException
    {
        final Integer miuId = getMiuIdFromIccId(iccid);

        final int recordCount = getRecordCount(usageDate, miuId);

        if (recordCount > 0)
        {
            updateExistingDataUsage(usageDate, bytesUsedDay, bytesUsedMonth, miuId, usageDatetime);
        }
        else
        {
            insertNewDataUsage(usageDate, bytesUsedDay, bytesUsedMonth, miuId, usageDatetime);
        }
    }

    /**
     * Return the latest data usage recorded for the miu.
     *
     * @param miuId
     * @return
     */
    @Override
    public CmiuDataUsage getLatestMiuDataUsage(MiuId miuId)
    {
        final String sql = "SELECT * FROM mdce.miu_data_usage WHERE miu_id = ? ORDER BY data_usage_date DESC LIMIT 1";

        try
        {
            return this.db.queryForObject(sql, (rs, rownum) -> {

                return new CmiuDataUsage(MiuId.valueOf(rs.getInt("miu_id")),
                        JdbcDateUtil.getLocalDate(rs, "data_usage_date"),
                        JdbcDateUtil.getInstant(rs, "data_usage_datetime"),
                        rs.getInt("bytes_used_today"),
                        rs.getInt("bytes_used_month_to_date"));

            }, miuId.numericWrapperValue());
        }
        catch (EmptyResultDataAccessException ex)
        {
            return null;
        }
    }

    @Override
    public void changeCellularDeviceState(CellularDeviceId deviceId, String currentProvisioningState) throws MiuDataException
    {
        final Integer miuId = getMiuIdFromDeviceIdOrThrow(deviceId);

        final String sql = "UPDATE mdce.cellular_device_details SET provisioning_state=?, last_update_time = NOW() " +
                "WHERE miu_id=?";

        //TODO enum provisioning state, temporary using arbitrary value 1
        this.db.update(sql, 1, miuId);
    }

    private Integer getMiuIdFromDeviceIdOrThrow(CellularDeviceId deviceId) throws MiuDataException
    {
        if (deviceId.isMsisdn())
        {
            return getMiuIdFromMsisdnOrThrow(deviceId.getValue());
        }
        else if (deviceId.isIccid())
        {
            return getMiuIdFromIccidOrThrow(deviceId.getValue());
        }
        else
        {
            throw new MiuDataException("Invalid cellular identifier kind for MIU lookup: " + deviceId);
        }
    }

    private Integer getMiuIdFromMsisdnOrThrow(final String msisdn) throws MiuDataException
    {
        try
        {
            String sql = "SELECT miu_id FROM mdce.sim_details WHERE msisdn = ?";
            return this.db.queryForObject(sql, new Object[]{msisdn.trim()}, Integer.class);
        }
        catch (EmptyResultDataAccessException e)
        {
            throw new MiuDataException("No miu with MSISDN=" + msisdn + " found in cellular_device_details table");
        }
    }

    private Integer getMiuIdFromIccidOrThrow(final String iccid) throws MiuDataException
    {
        try
        {
            String sql = "SELECT miu_id FROM mdce.sim_details WHERE iccid = ?";
            return this.db.queryForObject(sql, new Object[]{iccid.trim()}, Integer.class);
        }
        catch (EmptyResultDataAccessException e)
        {
            throw new MiuDataException("No miu with ICCID=" + iccid + " found in sim_details table");
        }
    }

    /**
     * Get a list of all miu which has cellular device registered
     *
     * @return
     */
    @Override
    public List<CellularDeviceDetail> getCellularDeviceList() throws MiuDataException
    {
        return queryCellularAndSimConfig("");

    }


    @Override
    public CellularDeviceDetail getCellularConfig(MiuId miuId) throws MiuDataException
    {
        final List<CellularDeviceDetail> ret = queryCellularAndSimConfig("WHERE cdd.miu_id = ?", miuId.numericWrapperValue());
        if (ret == null || ret.isEmpty())
        {
            return null;
        }
        else if (ret.size() == 1)
        {
            return ret.get(0);
        }
        else
        {
            throw new MiuDataException("Problem getting cellular config for MIU " + miuId + "; " + ret.size() + " rows found");
        }
    }



    @Override
    public CellularDeviceDetail getCellularConfigFromCellularDeviceIdentifier(final CellularDeviceId deviceId) throws MiuDataException
    {
        final List<CellularDeviceDetail> ret = this.queryCellularAndSimConfig("WHERE " + deviceId.getColumnName() + " = ?", deviceId.getValue());

        if (ret == null || ret.isEmpty())
        {
            return null;
        }
        else if (ret.size() == 1)
        {
            return ret.get(0);
        }
        else
        {
            throw new MiuDataException("Problem getting cellular config for " + deviceId + "; " + ret.size() + " rows found");
        }

    }


    private List<CellularDeviceDetail> queryCellularAndSimConfig(String whereClause, Object... bindVars) throws MiuDataException
    {
        final String sql = "SELECT cdd.miu_id as miu_id, provisioning_state, last_update_time," +
                "apn, " +
                "imei, modem_config_firmware, modem_vendor, modem_cellular_network, " +
                "iccid, imsi, msisdn, sim_cellular_network " +
                "FROM cellular_device_details cdd LEFT JOIN sim_details sd on cdd.miu_id = sd.miu_id " + whereClause;
        try
        {
            return this.db.query(sql, bindVars, (rs, rowNum)->{
                final CellularDeviceDetail cellularDeviceDetail = new CellularDeviceDetail();
                cellularDeviceDetail.setMiuId(MiuId.valueOf(rs.getInt("miu_id")));
                cellularDeviceDetail.setProvisioningState(rs.getInt("provisioning_state"));
                cellularDeviceDetail.setLastUpdateTime(JdbcDateUtil.getInstant(rs, "last_update_time"));
                cellularDeviceDetail.setApn(rs.getString("apn"));

                final CellularDeviceDetail.ModemConfig modemConfig = cellularDeviceDetail.getModemConfig();

                modemConfig.setImei(rs.getString("imei"));
                modemConfig.setFirmwareRevision(rs.getString("modem_config_firmware"));
                modemConfig.setVendor(rs.getString("modem_vendor"));
                modemConfig.setMno(rs.getString("modem_cellular_network"));

                cellularDeviceDetail.setIccid(rs.getString("iccid"));
                cellularDeviceDetail.setImsi(rs.getString("imsi"));
                cellularDeviceDetail.setMsisdn(rs.getString("msisdn"));
                cellularDeviceDetail.setSimMno(rs.getString("sim_cellular_network"));
                return cellularDeviceDetail;
            });
        }
        catch (Exception e)
        {
            throw new MiuDataException("Problem getting cellular config for query " + whereClause, e);
        }
    }


    private Integer getMiuIdFromIccId(String iccid) throws MiuDataException
    {
        try
        {
            final String sql = "SELECT miu_id FROM mdce.sim_details WHERE iccid = ?";
            return this.db.queryForObject(sql, new Object[]{iccid}, Integer.class);
        }
        catch (EmptyResultDataAccessException e)
        {
            throw new MiuDataException("Cannot find matching miu id from iccid");
        }
    }

    private int getRecordCount(LocalDate usageDate, Integer miuId)
    {
        final String sql = "SELECT COUNT(ALL *) FROM mdce.miu_data_usage WHERE miu_id = ? AND data_usage_date = ?";
        return this.db.queryForObject(
                sql,
                new Object[]{
                    miuId, JdbcDateUtil.bindDate(usageDate)
                },
                Integer.class);
    }

    private void updateExistingDataUsage(LocalDate usageDate, int bytesUsedDay, long bytesUsedMonth, Integer miuId, Instant usageDatetime)
    {
        final String sql = "UPDATE mdce.miu_data_usage " +
                " SET bytes_used_today = ?,  bytes_used_month_to_date = ? , data_usage_datetime = ?" +
                " WHERE miu_id = ? AND data_usage_date = ?";

        this.db.update(sql, bytesUsedDay, bytesUsedMonth, JdbcDateUtil.bindDate(usageDatetime), miuId, JdbcDateUtil.bindDate(usageDate));
    }

    private void insertNewDataUsage(LocalDate usageDate, int bytesUsedDay, long bytesUsedMonth, Integer miuId, Instant usageDatetime)
    {
        final String sql = "INSERT INTO mdce.miu_data_usage (miu_id, data_usage_date, bytes_used_today, bytes_used_month_to_date, data_usage_datetime) " +
                "VALUES ( " +
                "?, ?, ?, ? ,? )";

        this.db.update(sql, miuId, JdbcDateUtil.bindDate(usageDate), bytesUsedDay, bytesUsedMonth, JdbcDateUtil.bindDate(usageDatetime));
    }



    @Override
    public void updateOrInsertMiuCellularDetails(MiuId miuId, CellularDeviceDetail cellularDeviceDetail) throws MiuDataException
    {
        final MiuType miuType = MiuType.CMIU;
        final Integer siteId = Integer.valueOf(0); //universal site

        String sql = "UPDATE miu_details SET miu_type = IFNULL(?, miu_type) WHERE miu_id = ?";
        int rowsUpdated = this.db.update(sql, miuType.numericWrapperValue(), miuId.numericWrapperValue());

        if (rowsUpdated == 0)
        {
            sql = "INSERT INTO mdce.miu_details (miu_id, site_id, miu_type) VALUES (?,?,?)";
            this.db.update(sql, miuId.numericWrapperValue(), siteId, miuType.numericWrapperValue());
        }


        final CellularDeviceDetail.ModemConfig modemDetails = cellularDeviceDetail.getModemConfig();

        sql = "SELECT miu_id FROM cellular_device_details WHERE imei=? AND imei IS NOT NULL AND miu_id!=? AND miu_id IS NOT NULL";
        SqlRowSet rs = this.db.queryForRowSet(sql, modemDetails.getImei(), miuId.numericValue());

        if(rs.next())
        {
            throw MiuDataException.conflict("IMEI " + modemDetails.getImei() + " already used by CMIU " + rs.getInt(1) + " so cannot be assigned to CMIU" + miuId);
        }

        sql = "UPDATE cellular_device_details SET imei = IFNULL(?, imei), modem_vendor = IFNULL(?, modem_vendor), " +
                "modem_cellular_network = IFNULL(?, modem_cellular_network), apn = IFNULL(?, apn) " +
                "WHERE miu_id = ?";
        rowsUpdated = this.db.update(sql, modemDetails.getImei(), modemDetails.getVendor(),
                modemDetails.getMno(), cellularDeviceDetail.getApn(),
                miuId.numericWrapperValue());

        if (rowsUpdated == 0)
        {
            sql = "INSERT INTO cellular_device_details " +
                    "(miu_id,imei,provisioning_state,last_update_time," +
                    "modem_vendor,modem_config_firmware,modem_cellular_network, apn) " +
                    "VALUES " +
                    "(?, ?, 0, ?, " +
                    "?, NULL, ?, ?);";
            this.db.update(sql,
                    miuId.numericWrapperValue(), modemDetails.getImei(), new Date(),
                    modemDetails.getVendor(), modemDetails.getMno(), cellularDeviceDetail.getApn());
        }

        if(cellularDeviceDetail.getIccid() != null)
        {
            //Find any row where that ICCID is already used
            sql = "SELECT * FROM sim_details WHERE iccid=?";
            rs = this.db.queryForRowSet(sql, cellularDeviceDetail.getIccid());

            if(rs.next())
            {
                int existingMiuId = rs.getInt("miu_id");
                //If another MIU is using that ICCID then throw an exception
                if(existingMiuId != miuId.numericValue() &&
                        existingMiuId != 0)
                {
                    throw MiuDataException.conflict("ICCID " + cellularDeviceDetail.getIccid() + " already used by CMIU " +
                            existingMiuId + " so cannot be assigned to " + miuId);
                }

                //Null any records where the desired MIU is already using a different ICCID
                sql = "UPDATE sim_details SET miu_id=null WHERE miu_id=?";
                this.db.update(sql, miuId.numericValue());

                //Update the row
                sql = "UPDATE sim_details SET miu_id=? WHERE iccid=?";
                this.db.update(sql, miuId.numericValue(), cellularDeviceDetail.getIccid());
            }
            else
            {
                //Null any records where the desired MIU is already using a different ICCID
                sql = "UPDATE sim_details SET miu_id=null WHERE miu_id=?";
                this.db.update(sql, miuId.numericValue());

                //Update the row
                sql = "INSERT sim_details (iccid, miu_id) VALUES (?,?)";
                this.db.update(sql, cellularDeviceDetail.getIccid(), miuId.numericValue());
            }
        }
    }


    @Override
    public CmiuRevisionSet getRevisionSetAtTime(MiuId cmiuId, Instant timeStamp)
    {
        String sql = "SELECT * FROM mdce.cmiu_revision_history WHERE miu_id = ? AND revision_timestamp <= ?" +
                " ORDER BY revision_timestamp DESC LIMIT 1";

        return this.db.query(sql, this::miuRevisionMapper, cmiuId.numericWrapperValue(), Timestamp.from(timeStamp));
    }


    @Override
    public CmiuRevisionSet getCurrentRevisionSet(MiuId cmiuId)
    {
        String sql = "SELECT * FROM (SELECT * FROM mdce.cmiu_revision_history WHERE miu_id = ? " +
                "ORDER BY revision_timestamp DESC) AS t1 LIMIT 1";

        return this.db.query(sql, this::miuRevisionMapper, cmiuId.numericWrapperValue());
    }

    private CmiuRevisionSet miuRevisionMapper(ResultSet rs) throws SQLException
    {
        if(!rs.next())
        {
            return null;
        }

        final CmiuRevisionSet revisionSet = new CmiuRevisionSet();
        revisionSet.setMiuId(MiuId.valueOf(rs.getLong("miu_id")));
        revisionSet.setTimestamp(rs.getTimestamp("revision_timestamp").toInstant());
        revisionSet.setFirmwareRevision(rs.getString("firmware_revision"));
        revisionSet.setConfigRevision(rs.getString("config_revision"));
        revisionSet.setArbRevision(rs.getString("arb_revision"));
        revisionSet.setBleRevision(rs.getString("ble_revision"));
        revisionSet.setEncryptionRevision(rs.getString("encryption_revision"));
        revisionSet.setTelitFirmwareRevision(rs.getString("telit_firmware_revision"));

        return revisionSet;
    }



    public void addRevisionSet(CmiuRevisionSet cmiuRevisionSet)
    {
        final String sql = "INSERT INTO mdce.cmiu_revision_history (`miu_id`, " +
                "`revision_timestamp`, `firmware_revision`, `config_revision`, " +
                "`arb_revision`, `ble_revision`, `encryption_revision`, " +
                "`telit_firmware_revision`) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        this.db.update(sql,
                cmiuRevisionSet.getMiuId().numericWrapperValue(),
                Timestamp.from(cmiuRevisionSet.getTimestamp()),
                cmiuRevisionSet.getFirmwareRevision() == null ? null : cmiuRevisionSet.getFirmwareRevision().toString(),
                cmiuRevisionSet.getConfigRevision() == null ? null : cmiuRevisionSet.getConfigRevision().toString(),
                cmiuRevisionSet.getArbRevision() == null ? null : cmiuRevisionSet.getArbRevision().toString(),
                cmiuRevisionSet.getBleRevision() == null ? null : cmiuRevisionSet.getBleRevision().toString(),
                cmiuRevisionSet.getEncryptionRevision() == null ? null : cmiuRevisionSet.getEncryptionRevision().toString(),
                cmiuRevisionSet.getTelitFirmwareRevision() == null ? null : cmiuRevisionSet.getTelitFirmwareRevision().toString());
    }

    @Override
    public List<String> getAllCmiuModes()
    {
        final String sql = "SELECT cmiu_mode_name " +
                "FROM mdce.cmiu_config_set " +
                "GROUP BY cmiu_mode_name " +
                "ORDER BY cmiu_mode_name ";

        return this.db.queryForList(sql, String.class);
    }

}
