/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.data.miu.mode;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.AddMiuLifecycleStateResult;
import com.neptunetg.common.data.miu.lifecycle.model.HistoricMiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.data.util.JdbcDateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.neptunetg.common.data.util.JdbcDateUtil.bindDate;
import static com.neptunetg.common.data.util.JdbcDateUtil.getInstant;

/**
 * Class to get and set MIU state to the SQL database
 */
@Profile("jdbc") //only create this bean if "jdbc" Spring profile is active
@Repository
public class JdbcMiuModeRepo implements MiuModeRepo
{
    private static final Logger log = LoggerFactory.getLogger(JdbcMiuModeRepo.class);

    private final JdbcTemplate db;

    @Autowired
    public JdbcMiuModeRepo(JdbcTemplate db)
    {
        this.db = db;
    }

    @Override
    public MiuMode getMiuCurrentMode(MiuId miuId)
    {
        String sql = "SELECT miu_id, name, official_mode FROM cmiu_config_mgmt mgmt left join cmiu_config_set st on mgmt.current_config=st.id where miu_id=?";

        try
        {
            return this.db.queryForObject(sql, this::miuModeFromCmiuConfigRowMapper, miuId.numericValue());
        }
        catch(DataAccessException e)
        {
            //TODO CHANGE THIS
            log.debug("Could not get lifecycle state for miu: {}", miuId.toString());
        }

        return null;
    }

    @Override
    public MiuMode getReportedMode(MiuId miuId, int recInt, int repInt)
    {
        String sql = "SELECT miu_id, name, recording_interval_mins, reporting_interval_mins, official_mode FROM cmiu_config_mgmt mgmt " +
                "LEFT JOIN cmiu_config_set st ON mgmt.reported_config=st.id " +
                "WHERE reporting_interval_mins=? AND recording_interval_mins=? AND miu_id=?";

        try
        {
            return this.db.queryForObject(sql, this::miuModeFromCmiuWithIntervalsConfigRowMapper, repInt, recInt, miuId.numericValue());
        }
        catch (DataAccessException e)
        {
            //TODO CHANGE THIS
            log.debug("Could not get lifecycle state for miu: {}", miuId.toString());
        }

        return null;
    }

    private MiuMode miuModeFromCmiuConfigRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        MiuMode miuMode = new MiuMode();

        miuMode.setMiuId(MiuId.valueOf(rs.getInt("miu_id")));
        miuMode.setCurrentMode(rs.getString("name"));
        miuMode.setIsOfficialMode(rs.getBoolean("official_mode"));

        return miuMode;
    }

    private MiuMode miuModeFromCmiuWithIntervalsConfigRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        MiuMode miuMode = new MiuMode();

        miuMode.setMiuId(MiuId.valueOf(rs.getInt("miu_id")));
        miuMode.setReportedMode(rs.getString("name"));
        miuMode.setRecordingInteval(rs.getInt("recording_interval_mins"));
        miuMode.setReportingInterval(rs.getInt("reporting_interval_mins"));
        miuMode.setIsOfficialMode(rs.getBoolean("official_mode"));

        return miuMode;
    }
}
