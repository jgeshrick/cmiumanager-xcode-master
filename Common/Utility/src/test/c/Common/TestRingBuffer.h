/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/


/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup RingBufferUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test for the @ref RingBuffer
 * @author Duncan Willis
 * @date 2015.03.24
 * @version 1.0
 */


#ifndef _RINGBUFFER_UNITTEST_H__
#define _RINGBUFFER_UNITTEST_H__


void RingBuffer_TestStructSize(void);
void RingBuffer_TestInit(void);
void RingBuffer_TestSize(void);
void RingBuffer_TestFree(void);
void RingBuffer_TestFlush(void);
void RingBuffer_TestEmpty(void);
void RingBuffer_TestRW_single(void);
void RingBuffer_TestOverflow(void);
void RingBuffer_TestRW_array(void);
void RingBuffer_TestHighWater(void);
#endif /* _RINGBUFFER_UNITTEST_H__ */

/**
 * @}
 * @}
 */
