/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @defgroup RingBufferUnitTest 
 *
 * @brief Unit Test for the @ref RingBuffer  
 *
 */

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup RingBufferUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test runner for the @ref RingBuffer
 * @author Duncan Willis
 * @date 2015.03.24  
 * @version 1.0
 */
 
#ifndef TEST_TSM_RUNNER_H
#define TEST_TSM_RUNNER_H

/**
** Runs the unit tests for module RingBuffer
**/
int TestRingBuffer(void);

#endif

/**
 * @}
 * @}
 */

