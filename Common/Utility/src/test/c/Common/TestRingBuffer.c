/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup RingBufferUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test for the @ref RingBuffer
 * @author Duncan Willis
 * @date 2015.03.25
 * @version 1.0
 */
 
#include <stdint.h>
#include <stdio.h>

#include "CommonTypes.h"
#include "unity.h"
#include "RingBuffer.h"
#include "TestRingBuffer.h"

static uint8_t s_TestNo = 0u;



// General purpose buffer;  
#define BUF_SIZE 10
static uint8_t myData[BUF_SIZE];

// The amount reported as free in an empty FIFO.
// NOTE this is one less because adding another 
// byte would then cause wr==rd (which means empty)!
#define BUFFER_FREE_REPORT (BUF_SIZE - 1)    


/**
 * Reset the unit and print the test number before each test.
 */
void setUp(void)
{
    ++s_TestNo;
    printf("Test %3d.\n",s_TestNo);
}

/**
 * Reset the state after each test.
 */
void tearDown(void)
{
}



void RingBuffer_TestStructSize(void)
{    
    // Ensure we don't change data sizes
    TEST_ASSERT_EQUAL_UINT32(6*4,  sizeof(RingBufferT));
}


// Init is OK?
void RingBuffer_TestInit(void)
{    
    RingBufferT rb;
    //uint8_t readback;
    
    rb.Overflow = 1;
    rb.pucBuf = NULL;
    rb.ReadIndex = 99;
    rb.WriteIndex = 99;
    rb.Size = 9;

    RingBufferInit(&rb, myData, BUF_SIZE);

    TEST_ASSERT_EQUAL_UINT32(0,  rb.Overflow);
    TEST_ASSERT_EQUAL_PTR(myData,  rb.pucBuf);
    TEST_ASSERT_EQUAL_UINT32(0,  rb.ReadIndex);
    TEST_ASSERT_EQUAL_UINT32(0,  rb.WriteIndex);
    TEST_ASSERT_EQUAL_UINT32(BUF_SIZE,  rb.Size);
}


// Size call
void RingBuffer_TestSize(void)
{    
    RingBufferT rb; 

    RingBufferInit(&rb, myData, BUF_SIZE);

    TEST_ASSERT_EQUAL_UINT32(BUF_SIZE, RingBufferSize(&rb));

    // Push a byte
    RingBufferWriteOne(&rb, (uint8_t) 123);    
    // Size must not change
    TEST_ASSERT_EQUAL_UINT32(BUF_SIZE, RingBufferSize(&rb));
    
    // pop
    (void)RingBufferReadOne(&rb);
    // Size must not change
    TEST_ASSERT_EQUAL_UINT32(BUF_SIZE, RingBufferSize(&rb));
}


// Free call (see note re 1 less than buffer)
void RingBuffer_TestFree(void)
{    
    RingBufferT rb; 

    RingBufferInit(&rb, myData, BUF_SIZE);

    TEST_ASSERT_EQUAL_UINT32(BUF_SIZE - 1, RingBufferFree(&rb));

    // Push a byte
    RingBufferWriteOne(&rb, (uint8_t) 123);
    
    TEST_ASSERT_EQUAL_UINT32(BUF_SIZE - 1 - 1, RingBufferFree(&rb));
}

 
void RingBuffer_TestFlush(void)
{    
    RingBufferT rb; 
    uint32_t i = 0;

    RingBufferInit(&rb, myData, BUF_SIZE);

    // Overfill
    for (i = 0; i < BUF_SIZE+1; i++)
    {
        RingBufferWriteOne(&rb, (uint8_t) 123);
    }

    // Flush, free
    RingBufferFlush(&rb);
    TEST_ASSERT_EQUAL_UINT32(BUFFER_FREE_REPORT, RingBufferFree(&rb));

    TEST_ASSERT_TRUE(rb.ReadIndex == rb.WriteIndex);
}

 
void RingBuffer_TestEmpty(void)
{    
    RingBufferT rb;     
    uint32_t i = 0;

    RingBufferInit(&rb, myData, BUF_SIZE);

     TEST_ASSERT_EQUAL_UINT32(1, RingBufferEmpty(&rb));

    RingBufferWriteOne(&rb, (uint8_t) 123);
    TEST_ASSERT_EQUAL_UINT32(0, RingBufferEmpty(&rb));


    // Make full
    for (i = 0; i < BUF_SIZE - 1 - 1 ; i++)
    {
        RingBufferWriteOne(&rb, (uint8_t) 123);
    }
    
    // ensure still not empty
    TEST_ASSERT_EQUAL_UINT32(0, RingBufferEmpty(&rb));
 
    // Overflow
    RingBufferWriteOne(&rb, (uint8_t) 123);
    TEST_ASSERT_EQUAL_UINT32(0, RingBufferEmpty(&rb));
}


// Read / write one byte; test accessors
void RingBuffer_TestRW_single(void)
{    
    RingBufferT rb;
    uint8_t readback;

    RingBufferInit(&rb, myData, BUF_SIZE);

    // Must be empty, and not full
    TEST_ASSERT_EQUAL_UINT32(1,  RingBufferEmpty(&rb));
    TEST_ASSERT_EQUAL_UINT32(0,  RingBufferFull(&rb));

    // Size accessor?
    TEST_ASSERT_EQUAL_UINT32(BUF_SIZE, RingBufferSize(&rb));
    // Must be fully free. NOTE this is one less because adding another 
    // byte would then cause wr==rd (which means empty)!
    TEST_ASSERT_EQUAL_UINT32(BUFFER_FREE_REPORT, RingBufferFree(&rb));
    // Used must be 0
    TEST_ASSERT_EQUAL_UINT32(0, RingBufferUsed(&rb));

    // Write a byte
    RingBufferWriteOne(&rb, (uint8_t) 99);
     
    // Size must be unchanged
    TEST_ASSERT_EQUAL_UINT32(BUF_SIZE, RingBufferSize(&rb));
    // free is now down one more
    TEST_ASSERT_EQUAL_UINT32(BUFFER_FREE_REPORT - 1, RingBufferFree(&rb));
    // Used must be 1
    TEST_ASSERT_EQUAL_UINT32(1, RingBufferUsed(&rb));

    // Read a byte
    readback = RingBufferReadOne(&rb);
    TEST_ASSERT_EQUAL_UINT8 (99, readback);

    // Size must be unchanged
    TEST_ASSERT_EQUAL_UINT32(BUF_SIZE, RingBufferSize(&rb));
    // Must now be fully free
    TEST_ASSERT_EQUAL_UINT32(BUFFER_FREE_REPORT, RingBufferFree(&rb));
    // Used must be 0
    TEST_ASSERT_EQUAL_UINT32(0, RingBufferUsed(&rb));
}


//
// Note. The ringbuffer has a capacity of one less than the size of the actual data buffer
// This is to avoid the necessity of maintaining a sparate count variable, which can be subject
// to issues reagarding thread safety. The reason being that r==w is the empty state 
// and adding another byte to a full buffer cannot give empty.
void RingBuffer_TestOverflow(void)
{
    RingBufferT rb;
    uint8_t readback;
    uint32_t bytesWritten = 0;
    uint32_t i = 0;

    RingBufferInit(&rb, myData, BUF_SIZE);

    // Fill the fifo (one less than the buf size)
    for (i = 0; i < BUFFER_FREE_REPORT; i++)
    {
    RingBufferWriteOne(&rb, (uint8_t) i);
    bytesWritten++;
    }

    // Ensure we wrote exactly a buf worth     
    TEST_ASSERT_EQUAL_UINT32(BUFFER_FREE_REPORT, bytesWritten);
    TEST_ASSERT_EQUAL_UINT32(BUFFER_FREE_REPORT, RingBufferUsed(&rb));

    // Is full...but not overflowed
    TEST_ASSERT_EQUAL_UINT32(1, RingBufferFull(&rb));
    TEST_ASSERT_EQUAL_UINT32(0, RingBufferHasOverflowed(&rb));
    
    // Nothing free
    TEST_ASSERT_EQUAL_UINT32(0, RingBufferFree(&rb));

    // Push another byte
    RingBufferWriteOne(&rb, (uint8_t) 123);

    // Is still full - and overflowed
    TEST_ASSERT_EQUAL_UINT32(1, RingBufferFull(&rb));
    TEST_ASSERT_EQUAL_UINT32(1, RingBufferHasOverflowed(&rb));

    // Test We overwrote the last element  
    TEST_ASSERT_EQUAL_UINT8 (123, myData[BUF_SIZE-1]);

    // Read pointer should now be ahead of write (we overwrote the oldest byte)
    TEST_ASSERT_EQUAL_UINT32(0, rb.WriteIndex);
    TEST_ASSERT_EQUAL_UINT32(1, rb.ReadIndex);

    // Ensure the oldest data is now '1' (we overwrote the oldest byte)
    readback = RingBufferReadOne(&rb);
    TEST_ASSERT_EQUAL_UINT8 (1, readback);

    // Add one more to chaeck w pointer wraps and the end of buf is not exceeded
    RingBufferWriteOne(&rb, (uint8_t) 77);
    TEST_ASSERT_EQUAL_UINT8 (77, myData[0]);
        // and we are still full
    TEST_ASSERT_EQUAL_UINT32(1, RingBufferFull(&rb));


    // Enssure the r pointer was pushed up by the last write
    readback = RingBufferReadOne(&rb);
    TEST_ASSERT_EQUAL_UINT8 (2, readback);
    // Have one byte, so no longer full!
    TEST_ASSERT_EQUAL_UINT32(0, RingBufferFull(&rb));

    // The overflow flag shoud have remained set    
    TEST_ASSERT_EQUAL_UINT32(1, RingBufferHasOverflowed(&rb));
}


void RingBuffer_TestRW_array(void)
{
    RingBufferT rb;
    uint8_t writeArray[3] = {9,8,7};
    uint8_t readbackArray[3];

    RingBufferInit(&rb, myData, BUF_SIZE);
        
    RingBufferWrite(&rb, writeArray, 3);
    
    TEST_ASSERT_EQUAL_UINT32(1*3, RingBufferUsed(&rb));
    RingBufferWrite(&rb, writeArray, 3);
    
    TEST_ASSERT_EQUAL_UINT32(2*3, RingBufferUsed(&rb));
    
    // Fill to brim 
    RingBufferWrite(&rb, writeArray, 3);    
    TEST_ASSERT_EQUAL_UINT32(3*3, RingBufferUsed(&rb));
    TEST_ASSERT_EQUAL_UINT32(1, RingBufferFull(&rb));
    
     RingBufferRead(&rb, readbackArray, 3);
    TEST_ASSERT_EQUAL_U8_ARRAY(writeArray, readbackArray, 3);
     RingBufferRead(&rb, readbackArray, 3);
    TEST_ASSERT_EQUAL_U8_ARRAY(writeArray, readbackArray, 3);
     RingBufferRead(&rb, readbackArray, 3);
    TEST_ASSERT_EQUAL_U8_ARRAY(writeArray, readbackArray, 3);
}


//  High water, retain nax fullness of ringbuf
void RingBuffer_TestHighWater(void)
{
    RingBufferT rb;
    uint32_t bytesWritten = 0;
    uint32_t i = 0;
    uint8_t sixBytes[6] = {0};

    RingBufferInit(&rb, myData, BUF_SIZE);
 
    
    TEST_ASSERT_EQUAL_UINT32(0, RingBufferGetHighWatermark(&rb));

    RingBufferWriteOne(&rb, (uint8_t) 0);
     
    TEST_ASSERT_EQUAL_UINT32(1, RingBufferGetHighWatermark(&rb));

    // Should stick at 1 after a read
    (void)RingBufferReadOne(&rb);
    TEST_ASSERT_EQUAL_UINT32(1, RingBufferGetHighWatermark(&rb));

     
    // Fill the fifo (one less than the buf size)    
    RingBufferInit(&rb, myData, BUF_SIZE);

    for (i = 0; i < BUFFER_FREE_REPORT; i++)
    {
    RingBufferWriteOne(&rb, 0);
    bytesWritten++;
    }

    TEST_ASSERT_EQUAL_UINT32(bytesWritten, RingBufferGetHighWatermark(&rb));

    // Overflow by one byte
    RingBufferWriteOne(&rb, 0);
    // The overflow flag shoud be set    
    TEST_ASSERT_EQUAL_UINT32(1, RingBufferHasOverflowed(&rb));
    //  HighWatermark should not change
    TEST_ASSERT_EQUAL_UINT32(bytesWritten, RingBufferGetHighWatermark(&rb));
 
    // Init should clear
    RingBufferInit(&rb, myData, BUF_SIZE);
    TEST_ASSERT_EQUAL_UINT32(0, RingBufferGetHighWatermark(&rb));
    
    // test array r/w
    RingBufferWrite(&rb, sixBytes, 6);
    RingBufferRead(&rb, sixBytes, 6);
    
    TEST_ASSERT_EQUAL_UINT32(6, RingBufferGetHighWatermark(&rb));
}
/**
 * @}
 * @}
 */
