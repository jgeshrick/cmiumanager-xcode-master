/******************************************************************************
*******************************************************************************
**
**         Filename: Crc32Fast.h
**
**           Author: Brian Arnberg
**          Created: 2015.06.11
**
**     Last Edit By:
**        Last Edit:
**
**         Comments:
**
** Revision History:
**       2015.06.11: First created (Brian Arnberg)
**       2015.07.09: Moved to Common/Utility and changed the name to
**                   Crc32Fast.h, which overwrites the file that
**                   Duncan Willis wrote using the forward polynomial.
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential
**    property of Neptune Technology Group. The user, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __CRC_32_FAST_H
#define __CRC_32_FAST_H

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
#include <stdint.h>

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
#define CRC_SEED 0xFFFFFFFFUL /** Initialization Value */
#define CRC_ZERO 0x00000000UL /** CRC-32 Zero Value */

#define REVERSE_POLY
#ifndef REVERSE_POLY
    #define CRC_POLY 0x04C11DB7UL /** CRC-32 Polynomial */
#else
    #define CRC_POLY 0xEDB88320UL /** CRC-32 Reversed Polynomial */
#endif


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
uint32_t FastCrc32Word(uint32_t Crc, const uint8_t *dataAddress, uint32_t length);
uint32_t FastCrc32Byte(uint32_t Crc, const uint8_t *dataAddress, uint32_t length);
uint32_t ReverseCrc32(uint32_t incomingCrc32);
/** void makeCrc32Table(void); */

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __CRC_32_FAST_H

