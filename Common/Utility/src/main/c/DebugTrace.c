/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

 
/**
 * @addtogroup DebugTrace
 * @{
 */


/**
 * @file DebugTrace.c
 *
 * @brief Trace functions for debug

 * @author Duncan Willis
 * @date 2015.03.25
 * @version 1.0
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include "Ringbuffer.h"
#include "CommonTypes.h"
#include "DebugTrace.h"
#ifdef __CMIU__
#include "app_cmit_interface.h"
#endif // __CMIU__


// Max len of a debug string
#define TRACE_STRING_MAX_LEN 200

// for ring buffer 
#ifdef SMALL_TRACE_BUFFER
#define TRACE_BUFFER_SIZE 512
#else
#define TRACE_BUFFER_SIZE  4096
#endif

/**
** Private Variables (File Local Variables)
**/
static uint8_t      traceData[TRACE_BUFFER_SIZE];
static RingBufferT  traceRingBuffer;

// In CMIU we cannot use assign at instantiation; MSPD-722
#ifdef __CMIU__
static RingBufferT* pTraceRingBuffer;        // Note: You MUST call DebugTraceInit() before using trace functions 
#else
bool debugLogEnable = true; //will be set to false if this is being called multi-threaded
static RingBufferT* pTraceRingBuffer = NULL; // Module will self-initialise
#endif // __CMIU__


  

/******************************************************************************
**
** @brief Init the trace function
**
** @param None
** @brief This function automatically called on first invocation of trace usage
******************************************************************************/
void DebugTraceInit(void)
{    
    RingBufferInit(&traceRingBuffer, traceData, sizeof(traceData));
    pTraceRingBuffer = &traceRingBuffer; // Now indicate initialised.
}


/**
** @brief Gets the RingBuffer so it can be read from
** @param None
** @return NULL if uninitialised, else the Ringbuffer.
**/
RingBufferT* DebugTraceGetRingBuffer(void)
{
    return pTraceRingBuffer;
}


/******************************************************************************
**
** @brief Send a line for Debug trace to the test harness  
**
** @param fmtStr The null terminated string to send
******************************************************************************/
void DebugTrace(const char* fmtStr, ...)
{
#ifdef __CMIU__
    static char buf[TRACE_STRING_MAX_LEN];
    int len = 0;
    va_list args;  

    // Initialise if not already done. 
    if (NULL == pTraceRingBuffer)
    {        
       DebugTraceInit();
    }

    ASSERT(fmtStr != NULL);
    va_start(args, fmtStr);

    len = vsnprintf(buf, sizeof(buf), (char*)fmtStr, args);
    if (len > 0 && len < sizeof(buf))
    {
        app_cmit_interface_WriteDebugString(buf);
    }

    va_end(args);
#else //not __CMIU__
    static char buf[TRACE_STRING_MAX_LEN];
    uint32_t txCount;
    int len = 0;
    va_list args;  
    if (debugLogEnable == true)
    {
        // Initialise if not already done. 
        if (NULL == pTraceRingBuffer)
        {        
           DebugTraceInit();
        }

        ASSERT(fmtStr != NULL);
        va_start(args, fmtStr);

        len = vsnprintf(buf, sizeof(buf), (char*)fmtStr, args);
        if (len > 0 && len < sizeof(buf))
        {
            txCount = len + 1; // null term  
            RingBufferWrite(pTraceRingBuffer, (const uint8_t*)buf, txCount);
        }

        va_end(args);
    }
#endif
}
  

/******************************************************************************
**
** @brief Send a single char for Debug trace to the test harness  
**
** @param c The char
******************************************************************************/
void DebugPutc(char c)
{
    // Initialise if not already done. 
    if (NULL == pTraceRingBuffer)
    {        
       DebugTraceInit();
    }
 
    RingBufferWrite(pTraceRingBuffer, (const uint8_t*)(&c), 1);
}
  

/******************************************************************************
**    
**  @param traceData. A buffer to receive the trace data into
**  @param traceArraySize The size of the rx buffer
**
**  @eturn The number of chars copied into the buffer (0 if none available)
**
**  @brief This should be polled by the main app, and trace data then emitted e.g. via a 
** debug serial port or h/w dependent interface.
******************************************************************************/
DLL_EXPORT uint32_t DebugTraceGetChars(unsigned char* traceData, uint32_t traceArraySize)
{
    uint32_t txCount = 0;
    RingBufferT* pRb = DebugTraceGetRingBuffer();

    // Must check. Null indicates not inited yet
    if (NULL != pRb)
    {
        // Data in ring buf to send to pc?
        txCount = RingBufferUsed(pRb);

        if (txCount > traceArraySize) 
        {
            txCount = traceArraySize;
        }

        if(txCount > 0)
        {
            RingBufferRead(pRb, traceData, txCount);
        }
    }
    return txCount;
}



/**
 * @}
 */
