/* ****************************************************************************
******************************************************************************
**
**         Filename: RingBuffer.c
**    
**          Author: Duncan Willis
**          Created: 24 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
 


////////////////////////////////////////////////////////////////////////////////
/// @addtogroup 
/// Module containing utilities and common definitions.
/// @{
/// @addtogroup   Ringbuffer
/// @brief Module implementing a ringbuffer for buffered IO.
/// @{
///////////////////////////////////////////////////////////////////////////
 
 
#include <stdint.h>
#include "CommonTypes.h"
#include "Ringbuffer.h"
 



#ifndef NULL
    #define NULL                ((void *)0)
#endif
 

 
//*****************************************************************************
//
// Change the value of a variable atomically.
//
// \param pulVal points to the index whose value is to be modified.
// \param Delta is the number of bytes to increment the index by.
// \param Size is the size of the buffer the index refers to.
//
// This function is used to increment a read or write buffer index that may be
// written in various different contexts. It ensures that the read/modify/write
// sequence is not interrupted and, hence, guards against corruption of the
// variable. The new value is adjusted for buffer wrap.
//
// \return None.
//
//*****************************************************************************
static void
UpdateIndexAtomic(volatile uint32_t *pulVal, uint32_t Delta,
                  uint32_t Size)
{
    //
    // Update the variable value.
    //
    *pulVal += Delta;

    //
    // Correct for wrap. We use a loop here since we don't want to use a
    // modulus operation with interrupts off but we don't want to fail in
    // case Delta is greater than Size (which is extremely unlikely but...)
    //
    while(*pulVal >= Size)
    {
        *pulVal -= Size;
    }
}


///////////////////////////////////////////////////////////////////////////
/// Exported Functions
///////////////////////////////////////////////////////////////////////////

//*****************************************************************************
//
//! Determines whether the ring buffer whose pointers and size are provided
//! is full or not.
//!
//! \param pRingBuffer is the ring buffer object to empty.
//!
//! This function is used to determine whether or not a given ring buffer is
//! full.  The structure is specifically to ensure that we do not see
//! warnings from the compiler related to the order of volatile accesses
//! being undefined.
//!
//! \return Returns \b true if the buffer is full or \b false otherwise.
//
//*****************************************************************************
bool
RingBufferFull(RingBufferT *pRingBuffer)
{
    uint32_t Write;
    uint32_t Read;

    ASSERT(pRingBuffer != NULL);

    //
    // Copy the Read/Write indices for calculation.
    //
    Write = pRingBuffer->WriteIndex;
    Read = pRingBuffer->ReadIndex;

    //
    // Return the full status of the buffer.
    //
    return((((Write + 1) % pRingBuffer->Size) == Read) ? true : false);
}

//*****************************************************************************
//
//! Determines whether the ring buffer whose pointers and size are provided
//! is empty or not.
//!
//! \param pRingBuffer is the ring buffer object to empty.
//!
//! This function is used to determine whether or not a given ring buffer is
//! empty.  The structure is specifically to ensure that we do not see
//! warnings from the compiler related to the order of volatile accesses
//! being undefined.
//!
//! \return Returns \b true if the buffer is empty or \b false otherwise.
//
//*****************************************************************************
bool
RingBufferEmpty(RingBufferT *pRingBuffer)
{
    uint32_t Write;
    uint32_t Read;

    //
    // Check the arguments.
    //
    ASSERT(pRingBuffer != NULL);

    //
    // Copy the Read/Write indices for calculation.
    //
    Write = pRingBuffer->WriteIndex;
    Read = pRingBuffer->ReadIndex;

    //
    // Return the empty status of the buffer.
    //
    return((Write == Read) ? true : false);
}

//*****************************************************************************
//
//! Empties the ring buffer.
//!
//! \param pRingBuffer is the ring buffer object to empty.
//!
//! Discards all data from the ring buffer.
//!
//! \return None.
//
//*****************************************************************************
void
RingBufferFlush(RingBufferT *pRingBuffer)
{  
    //
    // Check the arguments.
    //
    ASSERT(pRingBuffer != NULL);

    //
    // Set the Read/Write pointers to be the same. Do this with interrupts
    // disabled to prevent the possibility of corruption of the read index.
    //
    pRingBuffer->ReadIndex = pRingBuffer->WriteIndex;
    //Reset Overflow flag
    pRingBuffer->Overflow = false;
}



//*****************************************************************************
//
//! Returns number of bytes stored in ring buffer.
//!
//! \param pRingBuffer is the ring buffer object to check.
//!
//! This function returns the number of bytes stored in the ring buffer.
//!
//! \return Returns the number of bytes stored in the ring buffer.
//
//*****************************************************************************
uint32_t
RingBufferUsed(RingBufferT *pRingBuffer)
{
    uint32_t Write;
    uint32_t Read;

    //
    // Check the arguments.
    //
    ASSERT(pRingBuffer != NULL);

    //
    // Copy the Read/Write indices for calculation.
    //
    Write = pRingBuffer->WriteIndex;
    Read = pRingBuffer->ReadIndex;

    //
    // Return the number of bytes contained in the ring buffer.
    //
    return((Write >= Read) ? (Write - Read) :
           (pRingBuffer->Size - (Read - Write)));
}


//*****************************************************************************
//
//! Returns number of bytes available in a ring buffer.
//!
//! \param pRingBuffer is the ring buffer object to check.
//!
//! This function returns the number of bytes available in the ring buffer.
//!
//! \return Returns the number of bytes available in the ring buffer.
//
//*****************************************************************************
uint32_t
RingBufferFree(RingBufferT *pRingBuffer)
{
    ASSERT(pRingBuffer != NULL);

    return((pRingBuffer->Size - 1) - RingBufferUsed(pRingBuffer));
}



//*****************************************************************************
//
//! Returns number of contiguous bytes of data stored in ring buffer ahead of
//! the current read pointer.
//!
//! \param pRingBuffer is the ring buffer object to check.
//!
//! This function returns the number of contiguous bytes of data available in
//! the ring buffer ahead of the current read pointer. This represents the
//! largest block of data which does not straddle the buffer wrap.
//!
//! \return Returns the number of contiguous bytes available.
//
//*****************************************************************************
uint32_t
RingBufferContigUsed(RingBufferT *pRingBuffer)
{
    uint32_t Write;
    uint32_t Read;

    ASSERT(pRingBuffer != NULL);

    Write = pRingBuffer->WriteIndex;
    Read = pRingBuffer->ReadIndex;

    return ((Write >= Read) ? (Write - Read) :
           (pRingBuffer->Size - Read));
}



//*****************************************************************************
//
//! Returns number of contiguous free bytes available in a ring buffer.
//!
//! \param pRingBuffer is the ring buffer object to check.
//!
//! This function returns the number of contiguous free bytes ahead of the
//! current write pointer in the ring buffer.
//!
//! \return Returns the number of contiguous bytes available in the ring
//! buffer.
//
//*****************************************************************************
uint32_t
RingBufferContigFree(RingBufferT *pRingBuffer)
{
    uint32_t Write;
    uint32_t Read;

    ASSERT(pRingBuffer != NULL);

    Write = pRingBuffer->WriteIndex;
    Read = pRingBuffer->ReadIndex;

    if(Read > Write)
    {
        //
        // The read pointer is above the write pointer so the amount of free
        // space is the difference between the two indices minus 1 to account
        // for the buffer full condition (write index one behind read index).
        //
        return((Read - Write) - 1);
    }
    else
    {
        //
        // If the write pointer is above the read pointer, the amount of free
        // space is the size of the buffer minus the write index. We need to
        // add a special-case adjustment if the read index is 0 since we need
        // to leave 1 byte empty to ensure we can tell the difference between
        // the buffer being full and empty.
        //
        return(pRingBuffer->Size - Write - ((Read == 0) ? 1 : 0));
    }
}



//*****************************************************************************
//
//! Return size in bytes of a ring buffer.
//!
//! \param pRingBuffer is the ring buffer object to check.
//!
//! This function returns the size of the ring buffer.
//!
//! \return Returns the size in bytes of the ring buffer.
//
//*****************************************************************************
uint32_t
RingBufferSize(RingBufferT *pRingBuffer)
{
    ASSERT(pRingBuffer != NULL);

    return(pRingBuffer->Size);
}



//*****************************************************************************
//
//! Reads a single byte of data from a ring buffer.
//!
//! \param pRingBuffer points to the ring buffer to be written to.
//!
//! This function reads a single byte of data from a ring buffer.
//!
//! \return The byte read from the ring buffer.
//
//*****************************************************************************
uint8_t
RingBufferReadOne(RingBufferT *pRingBuffer)
{
    uint8_t ucTemp;

    //
    // Check the arguments.
    //
    ASSERT(pRingBuffer != NULL);

    //
    // Verify that space is available in the buffer.
    //
    ASSERT(RingBufferUsed(pRingBuffer) != 0);

    //
    // Write the data byte.
    //
    ucTemp = pRingBuffer->pucBuf[pRingBuffer->ReadIndex];

    //
    // Increment the read index.
    //
    UpdateIndexAtomic(&pRingBuffer->ReadIndex, 1, pRingBuffer->Size);

    //
    // Return the character read.
    //
    return(ucTemp);
}

//*****************************************************************************
//
//! Reads data from a ring buffer.
//!
//! \param pRingBuffer points to the ring buffer to be read from.
//! \param pucData points to where the data should be stored.
//! \param Length is the number of bytes to be read.
//!
//! This function reads a sequence of bytes from a ring buffer.
//!
//! \return None.
//
//*****************************************************************************
void
RingBufferRead(RingBufferT *pRingBuffer, uint8_t *pucData,
               uint32_t Length)
{
    uint32_t Temp;

    //
    // Check the arguments.
    //
    ASSERT(pRingBuffer != NULL);
    ASSERT(pucData != NULL);
    ASSERT(Length != 0);

    //
    // Verify that data is available in the buffer.
    //
    ASSERT(Length <= RingBufferUsed(pRingBuffer));

    //
    // Read the data from the ring buffer.
    //
    for(Temp = 0; Temp < Length; Temp++)
    {
        pucData[Temp] = RingBufferReadOne(pRingBuffer);
    }
}


//*****************************************************************************
//
//! Remove bytes from the ring buffer by advancing the read index.
//!
//! \param pRingBuffer points to the ring buffer from which bytes are to be
//! removed.
//! \param NumBytes is the number of bytes to be removed from the buffer.
//!
//! This function advances the ring buffer read index by a given number of
//! bytes, removing that number of bytes of data from the buffer. If \e
//! NumBytes is larger than the number of bytes currently in the buffer, the
//! buffer is emptied.
//!
//! \return None.
//
//*****************************************************************************
void
RingBufferAdvanceRead(RingBufferT *pRingBuffer,
                      uint32_t NumBytes)
{
    uint32_t Count;

    //
    // Check the arguments.
    //
    ASSERT(pRingBuffer != NULL);

    //
    // Make sure that we are not being asked to remove more data than is
    // there to be removed.
    //
    Count = RingBufferUsed(pRingBuffer);
    Count =  (Count < NumBytes) ? Count : NumBytes;

    //
    // Advance the buffer read index by the required number of bytes.
    //
    UpdateIndexAtomic(&pRingBuffer->ReadIndex, Count,
                      pRingBuffer->Size);
}


//*****************************************************************************
//
//! Add bytes to the ring buffer by advancing the write index.
//!
//! \param pRingBuffer points to the ring buffer to which bytes have been added.
//! \param NumBytes is the number of bytes added to the buffer.
//!
//! This function should be used by clients who wish to add data to the buffer
//! directly rather than via calls to RingBufWrite() or RingBufWriteOne(). It
//! advances the write index by a given number of bytes.  If the \e NumBytes
//! parameter is larger than the amount of free space in the buffer, the
//! read pointer will be advanced to cater for the addition.  Note that this
//! will result in some of the oldest data in the buffer being discarded.
//!
//! \return None.
//
//*****************************************************************************
void
RingBufferAdvanceWrite(RingBufferT *pRingBuffer,
                       uint32_t NumBytes)
{
    uint32_t Count;
   
    //
    // Check the arguments.
    //
    ASSERT(pRingBuffer != NULL);

    //
    // Make sure we were not asked to add a silly number of bytes.
    //
    ASSERT(NumBytes <= pRingBuffer->Size);

    //
    // Determine how much free space we currently think the buffer has.
    //
    Count = RingBufferFree(pRingBuffer);

    //
    // Advance the buffer write index by the required number of bytes and
    // check that we have not run past the read index. Note that we must do
    // this within a critical section (interrupts disabled) to prevent
    // race conditions that could corrupt one or other of the indices.
    //
   
    //
    // Update the write pointer.
    //
    pRingBuffer->WriteIndex += NumBytes;

    //
    // Check and correct for wrap.
    //
    if(pRingBuffer->WriteIndex >= pRingBuffer->Size)
    {
        pRingBuffer->WriteIndex -= pRingBuffer->Size;
    }

    //
    // Did the client add more bytes than the buffer had free space for?
    //
    if(Count < NumBytes)
    {
        //
        // Yes - we need to advance the read pointer to ahead of the write
        // pointer to discard some of the oldest data.
        //
        pRingBuffer->ReadIndex = pRingBuffer->WriteIndex + 1;        
        pRingBuffer->Overflow = true;

        //
        // Correct for buffer wrap if necessary.
        //
        if(pRingBuffer->ReadIndex >= pRingBuffer->Size)
        {
            pRingBuffer->ReadIndex -= pRingBuffer->Size;
        }
    }
}


//*****************************************************************************
//
//! Writes a single byte of data to a ring buffer.
//!
//! \param pRingBuffer points to the ring buffer to be written to.
//! \param ucData is the byte to be written.
//!
//! This function writes a single byte of data into a ring buffer.
//!
//! \return None.
//
//*****************************************************************************
void
RingBufferWriteOne(RingBufferT *pRingBuffer, uint8_t ucData)
{ 
    uint32_t bufferUsed;
    ASSERT(pRingBuffer != NULL);

    if (RingBufferFree(pRingBuffer) == 0) 
    {
            //Note that we have an overflow
            pRingBuffer->Overflow = true;
            UpdateIndexAtomic(&pRingBuffer->ReadIndex, 1, pRingBuffer->Size);
    }

    //
    // Write the data byte.
    //
    pRingBuffer->pucBuf[pRingBuffer->WriteIndex] = ucData;

    //
    // Increment the write index.
    //
    UpdateIndexAtomic(&pRingBuffer->WriteIndex, 1, pRingBuffer->Size);


    // Update the high water level
    bufferUsed = RingBufferUsed(pRingBuffer);

    if (bufferUsed > pRingBuffer->highWatermark)
    {
        pRingBuffer->highWatermark = bufferUsed;
    }
}


//*****************************************************************************
//
//! Writes data to a ring buffer.
//!
//! \param pRingBuffer points to the ring buffer to be written to.
//! \param pucData points to the data to be written.
//! \param Length is the number of bytes to be written.
//!
//! This function write a sequence of bytes into a ring buffer.
//!
//! \return None.
//
//*****************************************************************************
void
RingBufferWrite(RingBufferT *pRingBuffer, const uint8_t *pucData,
                uint32_t Length)
{
    uint32_t Temp;

    //
    // Check the arguments.
    //
    ASSERT(pRingBuffer != NULL);
    ASSERT(pucData != NULL);

    //
    // Write the data into the ring buffer.
    //
    for(Temp = 0; Temp < Length; Temp++)
    {
        RingBufferWriteOne(pRingBuffer, pucData[Temp]);
    }
}


//*****************************************************************************
//
//! Initialize a ring buffer object.
//!
//! \param pRingBuffer points to the ring buffer to be initialized.
//! \param pucBuf points to the data buffer to be used for the ring buffer.
//! \param Size is the size of the buffer in bytes.
//!
//! This function initializes a ring buffer object, preparing it to store data.
//!
//! \return None.
//
//*****************************************************************************
void
RingBufferInit(RingBufferT *pRingBuffer, uint8_t *pucBuf,
               uint32_t Size)
{
    //
    // Check the arguments.
    //
    ASSERT(pRingBuffer != NULL);
    ASSERT(pucBuf != NULL);
    ASSERT(Size != 0);

    //
    // Initialize the ring buffer object.
    //
    pRingBuffer->Size = Size;
    pRingBuffer->pucBuf = pucBuf;
    pRingBuffer->WriteIndex = pRingBuffer->ReadIndex = 0;
    pRingBuffer->Overflow = false;
    pRingBuffer->highWatermark = 0;
}


//*****************************************************************************
//
//! Check if a RingBuffer has overflowed
//!
//! \param pRingBuffer points to the ring buffer to be Checked.
//! \return A bool stating if the ring buffer has overflowed
//
//*****************************************************************************
bool RingBufferHasOverflowed(const RingBufferT* pRingBuffer)
{
    return pRingBuffer->Overflow;
}


//*****************************************************************************
//
//! Get the highest level of fullness since initialisation
//!
//! \param pRingBuffer points to the ring buffer to be checked.
//! \return Number of elements (at highest fullness)
//! \note To clear the highwatermark, re-init the ringbuffer
//*****************************************************************************

uint32_t RingBufferGetHighWatermark(const RingBufferT* pRingBuffer)
{
    return pRingBuffer->highWatermark;
}


/**
 * @}
 * @}
 */
