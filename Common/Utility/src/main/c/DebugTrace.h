/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

 
/**
 * @addtogroup DebugTrace
 * @{
 */


/**
 * @file  DebugTrace.c
 *
 * @brief  Debug trace via a ring buffer. 
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 */


#ifndef _DEBUG_TRACE_H_
#define _DEBUG_TRACE_H_

#include "CommonTypes.h"
#include "RingBuffer.h"

void            DebugTraceInit(void);
RingBufferT*    DebugTraceGetRingBuffer(void);
void            DebugTrace(const char* fmtStr, ...);
void            DebugPutc(char c);

DLL_EXPORT uint32_t        
    DebugTraceGetChars(uint8_t* traceData, uint32_t traceArraySize);


#endif

/**
 * @}
 */

