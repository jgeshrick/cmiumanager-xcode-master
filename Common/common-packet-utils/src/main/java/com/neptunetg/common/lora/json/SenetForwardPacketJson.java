/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.lora.parser.LoraPacketParser;
import com.neptunetg.common.lora.pdu.L900Packet;
import com.neptunetg.common.lora.pdu.PacketId;
import com.neptunetg.common.util.HexUtils;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * A class to represent a packet from Senet
 * These packets are JSON, but arrive from the broker as an array of bytes
 */
public class SenetForwardPacketJson
{
    private final LoraPacketParser packetParser = new LoraPacketParser();

    /**
     * 64-bit Extended Unique Identifier or Node ID
     */
    @JsonProperty("EUI")
    private String eui;

    /**
     * Customer payload data.  11 or 53 bytes.
     */
    @JsonProperty("PDU")
    private String pdu;

    /**
     * PDU as byte sequence
     */
    private byte[] pduBytes;

    /**
     * 64-bit Senet Gateway ID that processed the message
     */
    @JsonProperty("GW")
    private String gw;

    /**
     * Relative Signal Strength Indicator at GW for node message
     */
    @JsonProperty("RSSI")
    private int rssi;

    /**
     * Signal to Noise Ratio
     */
    @JsonProperty("SNR")
    private float snr;

    /**
     * LoRa radio configuration (0-4)
     *
     * Data Rate, Spreading Factor, Bandwidth, Bitrate
         0, SF10, 125 kHz, 980 bps
         1, SF9, 125 kHz, 1760 bps
         2, SF8, 125 kHz, 3125 bps
         3, SF7, 125 kHz, 5470 bps
         4, SF8, 500 kHz, 12500 bps
     */
    @JsonProperty("DataRate")
    private int dataRate;

    /**
     * Node transmit power in dBm
     */
    @JsonProperty("TXPower")
    private int txPower;

    /**
     * Node Transmit Frequency in MHz
     */
    @JsonProperty("FREQ")
    private float freq;

    @JsonProperty("SeqNo")
    private int seqNo;

    @JsonProperty("Port")
    private int port;

    @JsonProperty("TXTime")
    private String txTime;

    public String getEui()
    {
        return eui;
    }

    public void setEui(String eui)
    {
        this.eui = eui;
    }

    public String getPdu()
    {
        return pdu;
    }

    public void setPdu(String pdu)
    {
        this.pdu = pdu;
        this.pduBytes = HexUtils.parseHexToByteArray(pdu);
    }

    public L900Packet parsePdu()
    {
        return packetParser.parsePacket(this.pduBytes);
    }

    @JsonIgnore
    public PacketId getPacketIdFromPdu()
    {
        return PacketId.fromFirstDataByte(this.pduBytes);
    }

    @JsonIgnore
    public byte[] getPduBytes()
    {
        return pduBytes.clone();
    }

    public String getGw()
    {
        return gw;
    }

    public void setGw(String gw)
    {
        this.gw = gw;
    }

    public int getRssi()
    {
        return rssi;
    }

    public void setRssi(int rssi)
    {
        this.rssi = rssi;
    }

    public float getSnr()
    {
        return snr;
    }

    public void setSnr(float snr)
    {
        this.snr = snr;
    }

    public int getDataRate()
    {
        return dataRate;
    }

    public void setDataRate(int dataRate)
    {
        this.dataRate = dataRate;
    }

    public int getTxPower()
    {
        return txPower;
    }

    public void setTxPower(int txPower)
    {
        this.txPower = txPower;
    }

    public float getFreq()
    {
        return freq;
    }

    public void setFreq(float freq)
    {
        this.freq = freq;
    }

    public int getSeqNo()
    {
        return seqNo;
    }

    public void setSeqNo(int seqNo)
    {
        this.seqNo = seqNo;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public String getTxTime()
    {
        return txTime;
    }

    public Instant getTxTimeInstant()
    {
        try
        {
            return ZonedDateTime.parse(txTime, DateTimeFormatter.ISO_DATE_TIME).toInstant();
        }
        catch (DateTimeParseException e)
        {
            return null;
        }
    }

    public void setTxTime(String txTime)
    {
        this.txTime = txTime;
    }
}
