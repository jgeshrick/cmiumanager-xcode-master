/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.substructures;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Represents the (revision, revdate, buildnum) revision information
 */
public class ImageRevisionInfo
{

    public static int SIZE_IN_BYTES = 10;

    private static int POW_2_28 = 268435456; //2^28

    private ByteBuffer myBuffer;
    private int myOffset;

    /**
     *
     * @param major Major version
     * @param minor Minor version
     * @param revDate Revision date as integer in form YYMMDD
     * @param buildNum Build number
     */
    public ImageRevisionInfo(int major, int minor, int revDate, int buildNum)
    {
        myBuffer = ByteBuffer.allocate(SIZE_IN_BYTES).order(ByteOrder.LITTLE_ENDIAN);
        myOffset = 0;

        encodeBcdByteToBuffer(0, major);
        encodeBcdByteToBuffer(1, minor);
        encodeBcdIntToBuffer(2, revDate);
        encodeBcdIntToBuffer(6, buildNum);

    }

    public ImageRevisionInfo(ByteBuffer ambientBuffer, int offset)
    {
        this.myBuffer = ambientBuffer;
        this.myOffset = offset;
    }

    public void embedInExistingBuffer(ByteBuffer existingBuffer, int offset)
    {
        byte[] target = existingBuffer.array();
        byte[] source = this.myBuffer.array();
        System.arraycopy(source, this.myOffset, target, offset, SIZE_IN_BYTES);
        this.myOffset = offset;
        this.myBuffer = existingBuffer;
    }


    /**
     * revision Major.Minor
     */
    @JsonProperty("major")
    public int getMajorRelease()
    {
        return decodeBcdByteFromBuffer(0);
    }

    @JsonProperty("minor")
    public int getMinorRelease()
    {
        return decodeBcdByteFromBuffer(1);
    }

    /**
     * Active revision (YYMMDD)
     */
    @JsonProperty("date")
    public int getRevDate()
    {
        return decodeBcdIntFromBuffer(2);
    }

    /**
     * Active revision build
     */
    @JsonProperty("rev")
    public int getBuildRev()
    {
        return decodeBcdIntFromBuffer(6);
    }


    private void encodeBcdByteToBuffer(int offset, int value)
    {
        int bcdValue = (value % 10) + (value / 10) * 16;
        this.myBuffer.put(myOffset + offset, (byte)bcdValue);
    }

    private void encodeBcdIntToBuffer(int offset, int value)
    {
        long pow16 = 1L;

        long bcdValue = 0L;

        long valueToEncode = ((long)value) & 0xffffffffL; //unsigned int

        while (valueToEncode != 0 && pow16 <= POW_2_28) //2^28, last valid power to add
        {
            bcdValue += (valueToEncode % 10L) * pow16;
            valueToEncode /= 10L;
            pow16 *= 16L;
        }

        this.myBuffer.putInt(myOffset + offset, (int) bcdValue);
    }

    private int decodeBcdByteFromBuffer(int offset)
    {
        int byteValue = this.myBuffer.get(this.myOffset + offset);
        return ((byteValue >> 4) & 0xf) * 10 + (byteValue & 0xf);
    }


    private int decodeBcdIntFromBuffer(int offset)
    {
        int valueToDecode = myBuffer.getInt(myOffset + offset);

        int ret = 0;
        int pow10 = 1;

        for (int nybbleIndex = 0; nybbleIndex < 8; nybbleIndex++)
        {
            ret += (valueToDecode & 0xf) * pow10;
            valueToDecode >>= 4;
            pow10 *= 10;
        }

        return ret;
    }


    @Override
    public String toString()
    {
        return getMajorRelease() + "." + getMinorRelease() + "." + getRevDate() + "." + getBuildRev();
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null || !(o instanceof ImageRevisionInfo))
        {
            return false;
        }
        ImageRevisionInfo other = (ImageRevisionInfo)o;

        for (int i = 0; i < SIZE_IN_BYTES; i++)
        {
            if (myBuffer.get(myOffset + i) != other.myBuffer.get(other.myOffset + 1))
            {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode()
    {
        return myBuffer.getInt(myOffset);
    }
}
