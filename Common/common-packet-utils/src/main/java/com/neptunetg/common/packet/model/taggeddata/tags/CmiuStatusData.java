/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag id 26
 * This tag holds inforamtion about the CMIUs status, and if devices are connected to it
 */
public class CmiuStatusData extends IntegerArrayData
{
    public static int SIZE_IN_BYTES = 4;

    public CmiuStatusData(int cmiuType, int numberOfDevices, int cmiuFlags)
    {
        super(TagId.CmiuStatus, TagDataType.ByteRecord, new byte[SIZE_IN_BYTES], SIZE_IN_BYTES);

        setByte(0, cmiuType);
        setByte(1, numberOfDevices);
        setShort(2, cmiuFlags);
    }

    public CmiuStatusData(TagDataType dataType, byte[] data)
    {
        super(TagId.CmiuStatus, dataType, data, data.length);
    }

    @JsonProperty("cmiu_flags_type")
    public int getCmiuType()
    {
        return getDataBuffer().get(0);
    }

    @JsonProperty("cmiu_flags_num_devices")
    public int getNumberOfDevices()
    {
        return getDataBuffer().get(1);
    }

    @JsonProperty("cmiu_flags_value")
    public int getCmiuFlags()
    {
        return getDataBuffer().getShort(2) & 0xFFFF;
    }

    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Cmiu Status [");
        ret.append("Cmiu Type =").append(getCmiuType());
        ret.append(", Number Of Devices =").append(getNumberOfDevices());
        ret.append(", Cmiu Flags =").append(getCmiuFlags());
        ret.append("]");

        return ret.toString();
    }
}
