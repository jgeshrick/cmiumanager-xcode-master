/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.types;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.util.HexUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

/**
 * Represents an array of 25-byte R900 blocks
 */
/*
 	ETI05-00	Bit 1	Bit 2	Bit 3	Bit 4	Bit 5	Bit 6	Bit 7	Bit 8
Byte 1	DevGrp	Meter Type	Hi/Lo	Read Format
Byte 2	DATA1	Reserved	Not Assigned	Consec. Days No Flow	Back Flow
Byte 2		Tamper	Tamper Count
Byte 2		Number of Digits	Tamper	Tamper - 35 days
Byte 3	DATA2	MSB	Reading
Byte 4	DATA3
Byte 5	DATA4	LSB
Byte 6	ID1	MSB	MIU ID
Byte 7	ID2
Byte 8	ID3
Byte 9	ID4	LSB
Byte 10	DATA5	High order e-coder reading	Days of Leak	Leak Status
Byte 10		Competitor extra digits
Byte 10		Program	Reed Sw	No Flow Days	Unused
Byte 11	DATA1	LSB	Time 1 # of seconds since 01/01/1980 GMT
Byte 12	DATA2
Byte 13	DATA3
Byte 14	DATA4	MSB
Byte 15	DATA1	LSB	Time 2 # of seconds Since 01/01/1980 GMT
Byte 16	DATA2
Byte 17	DATA3
Byte 18	DATA4	MSB
Byte 19	Device Type 0=Neptune R900
Byte 20	DATA1	MSB	RSSI
Byte 21	DATA2	LSB
Byte 22	DATA1   MSB	32 bit Unsigned, Duplicate Packet Received Count, 0 = Only original packet received
Byte 23 DATA2
Byte 24 DATA3
Byte 25 DATA4	LSB
 */
public class R900OokReadingArray extends TaggedDataWithType
{
    public static final long EPOCH_SECONDS_1980 = 315532800L; //time in seconds between epoch and 01/01/1980 00:00 UTC
    private final ByteBuffer data;
    private final int arrayLength;

    /**
     * Create an instance of a tag+data
     * @param tagId the ID of the tag
     * @param readingData List of arrays containing readings
     * @param readingOffsets List of offsets to the arrays
     */
    public R900OokReadingArray(TagId tagId, List<byte[]> readingData, List<Integer> readingOffsets)
    {
        super(tagId, TagDataType.R900OokReadingArray);

        final ByteBuffer buf = ByteBuffer.allocate(readingData.size() * 25);
        this.arrayLength = readingData.size();
        for (int ri = 0; ri < this.arrayLength; ri++)
        {
            buf.put(readingData.get(ri), readingOffsets.get(ri), 25);
        }
        buf.rewind();
        this.data = buf.asReadOnlyBuffer().order(ByteOrder.LITTLE_ENDIAN);
    }

    /**
     * Create an instance of a tag+data
     * @param tagId the ID of the tag
     * @param data data containing back-to-back 25 byte packets.  Must be a multiple of 25 bytes long
     * @param arrayLength Length of array in 25 byte blocks.  Must be data.length/25
     */
    public R900OokReadingArray(TagId tagId, byte[] data, int arrayLength)
    {
        super(tagId, TagDataType.R900OokReadingArray);

        if ((data.length % 25) != 0)
        {
            throw new IllegalArgumentException("Data must be multiple of 25 bytes long for R900 OOK data!");
        }

        this.data = ByteBuffer.wrap(data.clone()).asReadOnlyBuffer().order(ByteOrder.LITTLE_ENDIAN);
        this.arrayLength = arrayLength;
    }

    /**
     * Get the MIU ID of the entry in the array
     * @param index Index of the entry
     * @return MIU ID
     */
    public int getMiuId(int index)
    {
        return data.duplicate().order(ByteOrder.BIG_ENDIAN).getInt(25 * index + 5);
    }

    /**
     * Get the read timestamp of the entry in the array
     * @param index Index of the entry
     * @return the timestamp of the read
     */
    public UnixTimestamp getReadTimestamp(int index)
    {
        final long readDate = getUInt32(index, 10);
        return new UnixTimestamp(readDate + EPOCH_SECONDS_1980); //Add time in seconds between epoch and 01/01/1980 00:00 UTC
    }

    @JsonIgnore
    public UnixTimestamp getOldestPacketBuiltDate()
    {
        long minReadDate = Long.MAX_VALUE;
        for (int index = 0; index < this.arrayLength; index++)
        {
            final long readDate = getUInt32(index, 10);
            if (readDate < minReadDate)
            {
                minReadDate = readDate;
            }
        }
        if (minReadDate == Long.MAX_VALUE)
        {
            return null;
        }
        else
        {
            return new UnixTimestamp(minReadDate + EPOCH_SECONDS_1980); //Add time in seconds between epoch and 01/01/1980 00:00 UTC
        }
    }


    @JsonIgnore
    public UnixTimestamp getNewestPacketBuiltDate()
    {
        long maxReadDate = Long.MIN_VALUE;
        for (int index = 0; index < this.arrayLength; index++)
        {
            final long readDate = getUInt32(index, 10);
            if (readDate > maxReadDate)
            {
                maxReadDate = readDate;
            }
        }
        if (maxReadDate == Long.MIN_VALUE)
        {
            return null;
        }
        else
        {
            return new UnixTimestamp(maxReadDate + EPOCH_SECONDS_1980); //Add time in seconds between epoch and 01/01/1980 00:00 UTC
        }
    }

    @JsonProperty("meta")
    @Override
    public TaggedDataMetadata getMetadata()
    {
        return new TaggedDataMetadata(getTagId(), getDataType(), this.arrayLength);
    }

    @JsonProperty("readings_ook_array")
    public String[] getReadingsOokHex()
    {
        final String[] ret = new String[arrayLength];
        int bufPos = 0;
        for (int i = 0; i < this.arrayLength; i++)
        {
            ret[i] = HexUtils.byteBufferRangeToHex(this.data, bufPos, 25);
            bufPos += 25;
        }
        return ret;
    }

    @JsonIgnore
    public int getReadingCount()
    {
        return this.arrayLength;
    }
    /**
     * Getter that returns a string summary of the tag
     */
    @Override
    public String getTagSummary()
    {
        return getTagId().getTagName() + " (" + (data.capacity() / getDataType().getUnitSize()) + " blocks of 25 bytes each)";
    }

    @Override
    public String toString()
    {
        return getTagSummary();
    }

    @Override
    public byte[] serialize()
    {
        byte[] tagIdAndType = super.serialize();

        final int arrayLengthLength = 1; //1 byte for number of array elements

        final ByteBuffer ret = ByteBuffer.allocate(tagIdAndType.length + data.capacity() + arrayLengthLength)   //add array size
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(tagIdAndType);          //tag number, type

        ret.put((byte) arrayLength); //array size in blocks
        ret.put(data);                  //array content
        return ret.array();
    }


    private long getUInt32(int packetIndex, int byteOffsetInPacket)
    {
        return this.data.getInt(packetIndex * 25 + byteOffsetInPacket) & 0xFFFFFFFFL;
    }

}
