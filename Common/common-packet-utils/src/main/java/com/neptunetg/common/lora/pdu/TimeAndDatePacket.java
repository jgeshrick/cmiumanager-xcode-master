/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.pdu;

import java.time.Instant;

/**
 * Created by WJD1 on 13/07/2016.
 * A class to represent a time and date packet for L900
 */
public class TimeAndDatePacket extends L900Packet
{
    private static int PACKET_SIZE_BYTES = 11;

    public TimeAndDatePacket(Instant timeAndDateEpoch, int updateIdentifier)
    {
        super(PacketId.TIME_AND_DATE, PACKET_SIZE_BYTES);

        setNumber(5, timeAndDateEpoch.getEpochSecond(), 64);
        setNumber(64 + 5, updateIdentifier, 8);
    }

    public TimeAndDatePacket(byte[] packetBytes)
    {
        super(packetBytes);
    }

    public Instant getTimeAndDate()
    {
        return Instant.ofEpochSecond(getNumber(5, 64));
    }

    public int getUpdateIdentifier()
    {
        return (int) getNumber(64 + 5, 8);
    }


    @Override
    public String toString()
    {
        final StringBuilder ret = new StringBuilder("[PDU: TIME/DATE: packetId=");
        ret.append(getPacketId()).append(" time=").append(getTimeAndDate())
                .append(" updateId=").append(getUpdateIdentifier())
                .append(" length=").append(lengthInBytes()).append(" bytes]");
        return ret.toString();
    }
}
