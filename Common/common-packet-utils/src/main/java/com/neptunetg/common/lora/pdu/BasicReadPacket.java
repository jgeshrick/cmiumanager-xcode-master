/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.pdu;

/**
 * Created by WJD1 on 09/08/2016.
 * A class to represent a 11 byte read packet for L900
 * As defined in ETI 05 - 05
 */
public class BasicReadPacket extends L900Packet
{
    private static int PACKET_SIZE_BYTES = 11;

    public BasicReadPacket(int read1, int read2, int read3, int reverseFlowFlag, int continuousLeakFlag)
    {
        super(PacketId.BASIC_READ_PACKET, PACKET_SIZE_BYTES);

        setNumber(5, r900_27BitReadingIntFromReadingVal(read1), 27);
        setNumber(32, r900_27BitReadingIntFromReadingVal(read2), 27);
        setNumber(59, r900_27BitReadingIntFromReadingVal(read3), 27);
        setNumber(86, reverseFlowFlag, 1);
        setNumber(87, continuousLeakFlag, 1);


    }

    public BasicReadPacket(byte[] packetBytes)
    {
        super(packetBytes);
    }

    public int getRead1()
    {
        return readingValFromR900_27BitReading((int)getNumber(5, 27));
    }

    public int getRead2()
    {
        return readingValFromR900_27BitReading((int)getNumber(32, 27));
    }

    public int getRead3()
    {
        return readingValFromR900_27BitReading((int)getNumber(59, 27));
    }

    public int getReverseFlowFlag()
    {
        return (int)getNumber(86, 1);
    }

    public int getContinuousLeakFlag()
    {
        return (int)getNumber(87, 1);
    }

    private int r900_27BitReadingIntFromReadingVal(int readingVal)
    {
        int reading = 0;

        reading |= (readingVal & 0xFF) << 19;
        reading |= ((readingVal >> 8) & 0xFF) << 11;
        reading |= ((readingVal >> 16) & 0xFF) << 3;
        reading |= ((readingVal >> 24) & 0x07);

        return reading;
    }

    private int readingValFromR900_27BitReading(int r900Reading)
    {
        return (((r900Reading >> 19) & 0xFF) +
                (((r900Reading >> 11) & 0xFF) << 8) +
                (((r900Reading >> 3 ) & 0xFF) << 16) +
                (((r900Reading) & 0x07) << 24));
    }

    @Override
    public String toString()
    {
        final StringBuilder ret = new StringBuilder("[PDU: BASIC/READ: packetId=");
        ret.append(getPacketId())
                .append(" read1=").append(getRead1())
                .append(" read2=").append(getRead2())
                .append(" read3=").append(getRead3())
                .append(" ReverseFlowFlag=").append(getReverseFlowFlag())
                .append(" ContinuousLeakFlag=").append(getContinuousLeakFlag())
                .append(" length=").append(lengthInBytes()).append(" bytes]");
        return ret.toString();
    }
}
