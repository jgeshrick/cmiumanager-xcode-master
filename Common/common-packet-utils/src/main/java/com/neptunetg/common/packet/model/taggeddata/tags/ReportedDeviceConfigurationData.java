/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag 27
 * Holds information about about attached devices
 */
public class ReportedDeviceConfigurationData extends IntegerArrayData
{
    public static int SIZE_IN_BYTES = 17;

    public ReportedDeviceConfigurationData(int deviceNumber, long attachedDeviceId, int currentDeviceData, int deviceType, int deviceFlags)
    {
        super(TagId.ReportedDeviceConfiguration, TagDataType.ByteRecord, new byte[SIZE_IN_BYTES], SIZE_IN_BYTES);

        setByte(0, deviceNumber);
        setLong(1, attachedDeviceId);
        setShort(9, deviceType);
        setInt(11, currentDeviceData);
        setShort(15, deviceFlags);
    }

    public ReportedDeviceConfigurationData(TagDataType dataType, byte[] data)
    {
        super(TagId.ReportedDeviceConfiguration, dataType, data, data.length);
    }

    @JsonProperty("device_number")
    public int getDeviceNumber()
    {
        return getDataBuffer().get(0);
    }

    @JsonProperty("attached_device_id")
    public long getAttachedDeviceId()
    {
        return getDataBuffer().getLong(1);
    }

    @JsonProperty("attached_device_type")
    public int getDeviceType()
    {
        return getDataBuffer().getShort(9) & 0xFFFF;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("current_device_data")
    public Integer getCurrentDeviceData()
    {
        if (isInNewFormat())
        {
            return Integer.valueOf(getDataBuffer().getInt(11) & 0xFFFFFFFF);
        }
        else
        {
            return null;
        }
    }

    /**
     * A breaking change was made to this tag in Q4 2015.  It changed from 13 to 17 in length, with
     * currentDeviceData inserted at offset +11.
     */
    private boolean isInNewFormat()
    {
        return getArrayLength() > 13;
    }

    @JsonProperty("attached_device_flags")
    public int getDeviceFlags()
    {
        final int offsetToDeviceFlags;
        if (isInNewFormat())
        {
            offsetToDeviceFlags = 15;
        }
        else
        {
            offsetToDeviceFlags = 11;
        }
        return getDataBuffer().getShort(offsetToDeviceFlags) & 0xFFFF;
    }

    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Reported Device Configuration ");
        if (!isInNewFormat())
        {
            ret.append("(old 13 byte format)");
        }
        ret.append("[ Device Number =").append(getDeviceNumber());
        ret.append(", Attached Device ID =").append(getAttachedDeviceId());
        ret.append(", Device Type =").append(getDeviceType());
        if (isInNewFormat())
        {
            ret.append(", Current Device Data =").append(getCurrentDeviceData());
        }
        ret.append(", Device Flags =").append(getDeviceFlags());
        ret.append("]");

        return ret.toString();
    }
}
