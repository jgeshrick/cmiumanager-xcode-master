/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Created by WJD1 on 01/07/2015.
 * Class to represent a recording and reporting interval tag
 * Can support old and new version of tag which contains two new fields
 * number of attached devices and event mask for device
 * Tag ID 48
 */
public class RecordingAndReportingIntervalData extends IntegerArrayData
{
    public static int SIZE_IN_BYTES = 14;

    public RecordingAndReportingIntervalData(int reportingStartMins,
                                             int reportingIntervalHours,
                                             int reportingNumberOfRetries,
                                             int reportingTransmitWindowsMins,
                                             int reportingQuietStartTimeMins,
                                             int reportingQuietEndTimeMins,
                                             int recordingStartTimeMins,
                                             int recordingIntervalInMins,
                                             int numberOfAttachedDevices,
                                             int eventMaskForDevice)
    {
        super(TagId.RecordingandReportingInterval, TagDataType.ByteRecord, new byte[SIZE_IN_BYTES], SIZE_IN_BYTES);

        setShort(0, reportingStartMins);
        setByte(2, reportingIntervalHours);
        setByte(3, reportingNumberOfRetries);
        setByte(4, reportingTransmitWindowsMins);
        setShort(5, reportingQuietStartTimeMins);
        setShort(7, reportingQuietEndTimeMins);
        setShort(9, recordingStartTimeMins);
        setByte(11, recordingIntervalInMins);
        setByte(12, numberOfAttachedDevices);
        setByte(13, eventMaskForDevice);
    }

    public RecordingAndReportingIntervalData(TagDataType dataType, byte[] data)
    {
        super(TagId.RecordingandReportingInterval, dataType, data, data.length);
    }


    @JsonProperty("reporting_start_mins")
    public int getReportingStartMins()
    {
        return getDataBuffer().getShort(0);
    }

    @JsonProperty("reporting_interval_hours")
    public int getReportingIntervalHours()
    {
        return getDataBuffer().get(2);
    }

    @JsonProperty("reporting_number_of_retries")
    public int getReportingNumberOfRetries()
    {
        return getDataBuffer().get(3);
    }

    @JsonProperty("reporting_transmit_windows_min")
    public int getReportingTransmitWindowsMins()
    {
        return getDataBuffer().get(4);
    }

    @JsonProperty("reporting_quiet_start_mins")
    public int getReportingQuietStartMins()
    {
        return getDataBuffer().getShort(5);
    }

    @JsonProperty("reporting_quiet_end_mins")
    public int getReportingQuietEndMins()
    {
        return getDataBuffer().getShort(7);
    }

    @JsonProperty("recording_start_time_mins")
    public int getRecordingStartTimeMins()
    {
        return getDataBuffer().getShort(9);
    }

    @JsonProperty("recording_interval_mins")
    public int getRecordingIntervalMins()
    {
        return getDataBuffer().get(11);
    }

    @JsonProperty("cmiu_num_devices")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Integer getNumberOfAttachedDevices()
    {
        if(getDataBuffer().limit() < 12)
        {
            return null;
        }
        return Integer.valueOf(getDataBuffer().get(12) & 0xFF);
    }

    @JsonProperty("cmiu_event_mask")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Integer getEventMaskForDevice()
    {
        if(getDataBuffer().limit() < 13)
        {
            return null;
        }
        return Integer.valueOf(getDataBuffer().get(13) & 0xFF);
    }


    /**
     * formats the image meta data into a string
     * @return String with image meta data
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Recording and Reporting Interval");
        ret.append("Reporting Start Time in Mins =").append(getReportingStartMins());
        ret.append(", Reporting Interval in Hours =").append(getReportingIntervalHours());
        ret.append(", Number of Reporting Retries =").append(getReportingNumberOfRetries());
        ret.append(", Reporting Transmit Window in Mins =").append(getReportingTransmitWindowsMins());
        ret.append(", Reporting Quiet Time Start Mins =").append(getReportingQuietStartMins());
        ret.append(", Reporting Quiet Time End Mins =").append(getReportingQuietEndMins());
        ret.append(", Recording Start Time Mins =").append(getReportingStartMins());
        ret.append(", Recording Interval Mins =").append(getRecordingIntervalMins());
        ret.append("]");

        return ret.toString();
    }
}
