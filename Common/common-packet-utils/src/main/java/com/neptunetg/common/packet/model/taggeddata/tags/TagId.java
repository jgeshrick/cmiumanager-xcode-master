/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Tags as defined in ETI-XX (TBC)
 */
public final class TagId
{
    private static final Map<Integer, TagId> LOOKUP_TABLE = new HashMap<>();

    public static final TagId NullTag = new TagId(0, "Null", "Filler byte", false);
    public static final TagId CmiuPacketHeader = new TagId(1, "CMIU packet header", "Tag defining structure of header is used.", true);
    public static final TagId DetailedCmiuConfiguration = new TagId(2, "Detailed CMIU configuration", "(Not to be used post pre-alpha trial)", true);

    public static final TagId NetworkOperators = new TagId(3, "Network Operators Response String", "", true);
    public static final TagId NetworkPerformance = new TagId(4, "Network Performance String", "", true);
    public static final TagId RegistrationStatus = new TagId(5, "Registration Status String", "", true);

    public static final TagId MQTTBrokerIPAddress = new TagId(6, "MQTT Broker IP Address", "", true);
    public static final TagId FallbackMQTTBrokerIPAddress = new TagId(7, "Fallback MQTT Broker IP Address", "", true);
    public static final TagId AssignedCmiuIPAddress = new TagId(8, "CMIU IP Address", "", true);

    public static final TagId ModuleFotaFtpUrl = new TagId(9, "Module FOTA FTP URL", "", true);
    public static final TagId ModuleFotaFtpPort = new TagId(10, "Module FOTA FTP port", "", true);
    public static final TagId ModuleFotaFtpUsername = new TagId(11, "Module FOTA FTP Username", "", true);

    public static final TagId ModuleFotaFtpPassword = new TagId(12, "Module FOTA FTP Password", "", true);

    public static final TagId ModemHardwareRevision = new TagId(15, "Modem Hardware Revision", "", true);
    public static final TagId ModemModelIdentificationCode = new TagId(16, "Modem Model Identification Code", "", true);
    public static final TagId ModemManufacturerIdentificationCode = new TagId(17, "Modem Manufacturer Identification Code", "", true);

    public static final TagId ModemSoftwareRevisionNumber = new TagId(18, "Modem Software Revision Number", "", true);
    public static final TagId ModemSerialNumber = new TagId(19, "Modem Serial Number (mobile IMEI)", "", true);
    public static final TagId SimImsi = new TagId(20, "SIM IMSI Number", "", true);

    public static final TagId SimCardId = new TagId(21, "SIM Card ID", "", true);
    public static final TagId PinPukPukTwoRequestData = new TagId(22, "PIN/PUK/PUK2 Request Device Status", "", true);
    public static final TagId CmiuHwDiagnostics = new TagId(23, "CMIU Hardware Diagnostics", "", true);

    public static final TagId LastBleUserId = new TagId(24, "ID Of Last BLE User", "", true);
    public static final TagId LastBleUserLoginDate = new TagId(25, "Date Of Last BLE Use", "", true);
    public static final TagId CmiuStatus = new TagId(26, "CMIU Status Information", "", true);

    public static final TagId ReportedDeviceConfiguration = new TagId(27, "Device Configuration Information", "", true);
    public static final TagId R900IntervalData = new TagId(28, "R900 Format Interval Data", "", true);
    public static final TagId Event = new TagId(29, "Event Information", "", true);

    public static final TagId ErrorLog = new TagId(30, "Error Log", "", true);
    public static final TagId ConnectionTimingLog = new TagId(31, "Connection Timing Log", "", true);
    public static final TagId Command = new TagId(32, "A Command for the CMIU", "", true);

    public static final TagId CmiuConfiguration = new TagId(33, "CMIU Configuration Data", "", true);
    public static final TagId IntervalRecording = new TagId(34, "Interval Recording Configuration Settings", "", true);
    public static final TagId MQTTPacketHeader = new TagId(35, "packet header for messages sent to CMIU", "Tag defining structure of header send by CMIU.", true);

    public static final TagId CurrentTimeAndDate = new TagId(36, "Current time and date", "Unix date and time format of current time of device", true);
    public static final TagId MsisdnRequest = new TagId(37, "MSISDN request (Phone number request)", "List of phone numbers, Example: AT+CNUM\\n+CNUM: \"PHONENUM1\",\"2173848500\",129\\n", true);
    public static final TagId HardwareRevision = new TagId(38, "Hardware Revision", "", true);

    public static final TagId FirmwareRevision = new TagId(39, "Firmware Revision", "", true);
    public static final TagId BootloaderVersion = new TagId(40, "Bootloader Revision", "", true);
    public static final TagId ConfigRevision = new TagId(41, "Configuration Revision", "", true);

    public static final TagId ArbConfigRevision = new TagId(42, "ARB Configuration Revision", "", true);
    public static final TagId BleConfigRevision = new TagId(43, "BLE Configuration Revision", "", true);
    public static final TagId CmiuInformation = new TagId(44, "CMIU information", "", true);

    public static final TagId DeviceCurrentData = new TagId(45, "Device Current Data", "", true);
    public static final TagId Image = new TagId(46, "URL for Image update", "", true);
    public static final TagId ImageMetaData = new TagId(47, "Meta data for supporting image update", "", true);

    public static final TagId RecordingandReportingInterval = new TagId(48, "Recording and reporting settings", "", true);
    public static final TagId EncryptionConfigRevision = new TagId(49, "The revision of the encryption config image", "", true);
    public static final TagId TimeAndDate = new TagId(50, "Time and Date", "", true);

    public static final TagId Error = new TagId(51, "Error Enum", "", true);

    public static final TagId R900ReadingsOok = new TagId(60, "READINGS_OOK", "R900 data - READINGS OOK", true);
    public static final TagId R900ReadingsFsk = new TagId(61, "READINGS_FSK", "R900 data - READINGS FSK", true);

    public static final TagId BatteryVoltage = new TagId(62, "Battery Voltage", "Battery Voltage at end of last transmission attempt (in milliVolts)", true);
    public static final TagId Temperature = new TagId(63, "Temperature", "Temperature at end of last transmission attempt (CMIU temperature at end of last transmission attempt.  Signed 8-bit value in degrees Celsius)", true);

    public static final TagId PacketType = new TagId(65, "Packet Type", "Packet Type", true);

    public static final TagId NewImageVersionInformation = new TagId(71, "New Image Version Info", "Version information for new modem FOTA image", true);
    public static final TagId PacketInstigator = new TagId(72, "Packet Instigator", "Successful testing", true);

    public static final TagId TagId256Extension = new TagId(250, "Tag ID extension", "Actual tag ID is next byte plus 256", false);
    public static final TagId SecureDataBlockArray = new TagId(254, "Secure data block array", "Block of secure encrypted data", true);
    public static final TagId Eof = new TagId(255, "EOF", "End of record for packet data.Indicates end of tag list. Multiple null tags may be used as filler for the spare data space. No tag type is provided.", false);

    private final int id;

    private final String tagName;

    private final String description;

    private final boolean hasType;

    /**
     * @param id Represents a tag ID (i.e. a byte value which signifies the
     * meaning of the following data.
     */
    private TagId(int id, String tagName, String description, boolean hasType)
    {
        this.id = id;
        this.tagName = tagName;
        this.description = description;
        this.hasType = hasType;
        LOOKUP_TABLE.put(Integer.valueOf(id), this);
    }

    @JsonProperty("id")
    public int getId()
    {
        return id;
    }

    @JsonProperty("tag_name")
    public String getTagName()
    {
        return tagName;
    }

    @JsonIgnore
    public String getDescription()
    {
        return description;
    }

    public boolean hasType()
    {
        return hasType;
    }

    public static TagId forByte(int byteValue)
    {
        Integer key = Integer.valueOf(byteValue & 0xff);

        if (LOOKUP_TABLE.containsKey(key))
        {
            return LOOKUP_TABLE.get(key);
        }

        return new TagId(byteValue, "Unknown tag 0x" + Integer.toHexString(byteValue), "Unknown tag", true);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }

        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TagId tagId = (TagId) o;

        return id == tagId.id;

    }

    @Override
    public int hashCode()
    {
        return id;
    }

    @Override
    public String toString()
    {
        return String.format("%d 0x%02x %s", this.id, this.id, this.tagName);
    }
}
