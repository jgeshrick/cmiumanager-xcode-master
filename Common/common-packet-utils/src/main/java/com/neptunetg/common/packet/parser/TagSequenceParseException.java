/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.parser;

import com.neptunetg.common.util.HexUtils;

import java.nio.ByteBuffer;

/**
 * An error that occurs while parsing a tag sequence
 */
public class TagSequenceParseException extends RuntimeException
{
    public TagSequenceParseException(String message, ByteBuffer partiallyReadBuffer)
    {
        super(createMessage(message, partiallyReadBuffer));
    }

    public TagSequenceParseException(String message, ByteBuffer partiallyReadBuffer, Throwable cause)
    {
        super(createMessage(message, partiallyReadBuffer), cause);
    }


    private static String createMessage(String messageText, ByteBuffer partiallyReadBuffer)
    {
        final StringBuilder ret = new StringBuilder(messageText);
        ret.append(" [at offset +").append(partiallyReadBuffer.position())
                .append(" in buffer of length ").append(partiallyReadBuffer.capacity())
                .append("; raw data in hex: ");

        try
        {
            ret.append(HexUtils.byteBufferToHex(partiallyReadBuffer));
        }
        catch (Exception e)
        {
            ret.append("(not available: ").append(e).append(")");
        }
        ret.append("]");

        return ret.toString();
    }
}
