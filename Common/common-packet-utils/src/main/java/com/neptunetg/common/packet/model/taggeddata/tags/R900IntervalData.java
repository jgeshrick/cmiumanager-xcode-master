/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.substructures.R900Reading;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WJD1 on 01/07/2015.
 * Class to represent a R900 Interval Data Tag
 */
public class R900IntervalData extends IntegerArrayData
{
    List<R900Reading> r900Readings = new ArrayList<R900Reading>();

    public R900IntervalData(TagDataType dataType, byte[] data)
    {
        super(TagId.R900IntervalData, dataType, data, data.length/4);

        for(int i=0; i<data.length; i+=4)
        {
            int r900Entry = (data[i] & 0xFF) +
                    ((data[i+1] & 0xFF) << 8) +
                    ((data[i+2] & 0xFF) << 16) +
                    ((data[i+3] & 0xFF) << 24);
            r900Readings.add(new R900Reading(r900Entry));
        }
    }

    @JsonProperty("readings")
    public List<R900Reading> getReadings()
    {
        return r900Readings;
    }

    /**
     * formats the image meta data into a string
     * @return String with image meta data
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": R900 Interval Data [");
        ret.append(" First Reading=").append(r900Readings.get(0).getReading());
        ret.append(", First Peak Backflow=").append(r900Readings.get(0).getPeakBackflow());
        ret.append(", First Leak Detect=").append(r900Readings.get(0).getLeakDetection());
        ret.append(", Total Readings=").append(r900Readings.size());
        ret.append("]");

        return ret.toString();
    }
}
