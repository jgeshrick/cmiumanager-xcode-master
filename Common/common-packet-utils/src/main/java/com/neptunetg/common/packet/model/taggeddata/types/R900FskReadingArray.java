/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.types;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.util.HexUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

/**
 * Represents an array of 25-byte R900 blocks
 */
/*
From ETI29-01 1.3.21

 	ETI 05-03	Bit 1	Bit 2	Bit 3	Bit 4	Bit 5	Bit 6	Bit 7	Bit 8
Byte 1	 	Connected Device Type	Undefined
Byte 2	DATA1	Undefined	Error Code	35 Day Reverse Flow	24 Hour Reverse Flow	Leak State
Byte 3	DATA2	Battery	Empty Pipe	Excessive Flow	Tamper	Flag Status
Byte 4	DATA1	MSB	Current Reading (Padded with leading 0s since FSK packet only contains 27 bits)
Byte 5	DATA2
Byte 6	DATA3
Byte 7	DATA4	LSB
Byte 8	DATA1	MSB	15 Minute Reading (Padded with leading 0s since FSK packet only contains 27 bits)
Byte 9	DATA2
Byte 10	DATA3
Byte 11	DATA4	LSB
Byte 12	DATA1	MSB	12 Hour Reading (Padded with leading 0s since FSK packet only contains 27 bits)
Byte 13	DATA2
Byte 14	DATA3
Byte 15	DATA4	LSB
Byte 16	ID1	MSB	MIU ID
Byte 17	ID2
Byte 18	ID3
Byte 19	ID4	LSB
Byte 20	DATA1	MSB	Time 1 (# of seconds since 01/01/1980 GMT)
Byte 21	DATA2
Byte 22	DATA3
Byte 23	DATA4	LSB
Byte 24	DATA	 	RSSI
Byte 25	DATA1	Spare Bits	Spare Bit	Undefined
 */
public class R900FskReadingArray extends TaggedDataWithType
{
    public static final long EPOCH_SECONDS_1980 = 315532800L;  //time in seconds between epoch and 01/01/1980 00:00 UTC
    private final ByteBuffer data;
    private final int arrayLength;


    /**
     * Create an instance of a tag+data
     * @param tagId the ID of the tag
     * @param readingData List of arrays containing readings
     * @param readingOffsets List of offsets to the arrays
     */
    public R900FskReadingArray(TagId tagId, List<byte[]> readingData, List<Integer> readingOffsets)
    {
        super(tagId, TagDataType.R900FskReadingArray);

        final ByteBuffer buf = ByteBuffer.allocate(readingData.size() * 25);
        this.arrayLength = readingData.size();
        for (int ri = 0; ri < this.arrayLength; ri++)
        {
            buf.put(readingData.get(ri), readingOffsets.get(ri), 25);
        }
        buf.rewind();
        this.data = buf.asReadOnlyBuffer().order(ByteOrder.LITTLE_ENDIAN);
    }


    /**
     * Create an instance of a tag+data
     * @param tagId the ID of the tag
     * @param data data containing back-to-back 25 byte packets.  Must be a multiple of 25 bytes long
     * @param arrayLength Length of array in 25 byte blocks.  Must be data.length/25
     */
    public R900FskReadingArray(TagId tagId, byte[] data, int arrayLength)
    {
        super(tagId, TagDataType.R900FskReadingArray);

        if ((data.length % 25) != 0)
        {
            throw new IllegalArgumentException("Data must be multiple of 25 bytes long for R900 OOK data!");
        }

        this.data = ByteBuffer.wrap(data.clone()).asReadOnlyBuffer().order(ByteOrder.LITTLE_ENDIAN);
        this.arrayLength = arrayLength;
    }


    @JsonProperty("meta")
    @Override
    public TaggedDataMetadata getMetadata()
    {
        return new TaggedDataMetadata(getTagId(), getDataType(), this.arrayLength);
    }

    @JsonIgnore
    public int getReadingCount()
    {
        return this.arrayLength;
    }


    @JsonProperty("readings_fsk_array")
    public String[] getReadingsFskHex()
    {
        final String[] ret = new String[arrayLength];
        int bufPos = 0;
        for (int i = 0; i < this.arrayLength; i++)
        {
            ret[i] = HexUtils.byteBufferRangeToHex(this.data, bufPos, 25);
            bufPos += 25;
        }
        return ret;
    }


    /**
     * Get the MIU ID of the entry in the array
     * @param index Index of the entry
     * @return MIU ID
     */
    public int getMiuId(int index)
    {
        return data.duplicate().order(ByteOrder.BIG_ENDIAN).getInt(25 * index + 15);
    }

    /**
     * Get the read timestamp of the entry in the array
     * @param index Index of the entry
     * @return the timestamp of the read
     */
    public UnixTimestamp getReadTimestamp(int index)
    {
        final ByteBuffer bigEndian = data.duplicate().order(ByteOrder.BIG_ENDIAN);
        final long readDate = getUInt32(bigEndian, index, 19);
        return new UnixTimestamp(readDate + EPOCH_SECONDS_1980); //Add time in seconds between epoch and 01/01/1980 00:00 UTC
    }



    @JsonIgnore
    public UnixTimestamp getOldestPacketBuiltDate()
    {
        final ByteBuffer bigEndian = data.duplicate().order(ByteOrder.BIG_ENDIAN);
        long minReadDate = Long.MAX_VALUE;
        for (int index = 0; index < this.arrayLength; index++)
        {
            final long readDate = getUInt32(bigEndian, index, 19);
            if (readDate < minReadDate)
            {
                minReadDate = readDate;
            }
        }
        if (minReadDate == Long.MAX_VALUE)
        {
            return null;
        }
        else
        {
            return new UnixTimestamp(minReadDate + EPOCH_SECONDS_1980); //Add time in seconds between epoch and 01/01/1980 00:00 UTC
        }
    }


    @JsonIgnore
    public UnixTimestamp getNewestPacketBuiltDate()
    {
        final ByteBuffer bigEndian = data.duplicate().order(ByteOrder.BIG_ENDIAN);

        long maxReadDate = Long.MIN_VALUE;
        for (int index = 0; index < this.arrayLength; index++)
        {
            final long readDate = getUInt32(bigEndian, index, 19);
            if (readDate > maxReadDate)
            {
                maxReadDate = readDate;
            }
        }
        if (maxReadDate == Long.MIN_VALUE)
        {
            return null;
        }
        else
        {
            return new UnixTimestamp(maxReadDate + EPOCH_SECONDS_1980); //Add time in seconds between epoch and 01/01/1980 00:00 UTC
        }
    }

    /**
     * Getter that returns a string summary of the tag
     */
    @Override
    public String getTagSummary()
    {
        return getTagId().getTagName() + " (" + (data.capacity() / getDataType().getUnitSize()) + " blocks of 25 bytes each)";
    }

    @Override
    public String toString()
    {
        return getTagSummary();
    }

    @Override
    public byte[] serialize()
    {
        byte[] tagIdAndType = super.serialize();

        final int arrayLengthLength = 1; //1 byte for number of array elements

        final ByteBuffer ret = ByteBuffer.allocate(tagIdAndType.length + data.capacity() + arrayLengthLength)   //add array size
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(tagIdAndType);          //tag number, type

        ret.put((byte) arrayLength); //array size in blocks
        ret.put(data);                  //array content
        return ret.array();
    }

    private static long getUInt32(ByteBuffer buf, int packetIndex, int byteOffsetInPacket)
    {
        return buf.getInt(packetIndex * 25 + byteOffsetInPacket) & 0xFFFFFFFFL;
    }

}
