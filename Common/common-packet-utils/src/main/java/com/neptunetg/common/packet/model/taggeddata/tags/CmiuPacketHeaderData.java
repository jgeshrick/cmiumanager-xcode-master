/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Represents tag 1
 * {@code
 CMIU ID	Uint32
 Sequence number	Byte
 Key info	Byte
 Encryption method	Byte
 Token(AES CRC)	Byte
 Network flags	Byte
 Cellular RSSI and BER	UInt16	"First byte =
 <rssi> - received signal strength indication
 0 - (-113) dBm or less
 1 - (-111) dBm
 2..30 - (-109)dBm..(-53)dBm / 2 dBm per step
 31 - (-51)dBm or greater
 99 - not known or not detectable

 Second byte =
 <ber> - bit error rate (in percent)
 0 - less than 0.2%
 1 - 0.2% to 0.4%
 2 - 0.4% to 0.8%
 3 - 0.8% to 1.6%
 4 - 1.6% to 3.2%
 5 - 3.2% to 6.4%
 6 - 6.4% to 12.8%
 7 - more than 12.8%
 99 - not known or not detectable"
 Current Time_&_Date	Uint64	64 bit, time_t  Unix format.
                            (seconds after 1 January 1970)
}
 */

public class CmiuPacketHeaderData extends IntegerArrayData
{

    public static int SIZE_IN_BYTES = 19;

    public CmiuPacketHeaderData(TagDataType dataType, byte[] data)
    {
        super(TagId.CmiuPacketHeader, dataType, data, data.length);
    }

    public CmiuPacketHeaderData(int cmiuId, int sequenceNumber, int keyInfo, int encryptionMethod,
                                int token, int networkFlags, int rssi, int ber, UnixTimestamp currentTimeAndDate) {

        super(TagId.CmiuPacketHeader, TagDataType.ByteRecord, new byte[SIZE_IN_BYTES], SIZE_IN_BYTES);

        setInt(0, cmiuId);
        setByte(4, sequenceNumber);
        setByte(5, keyInfo);
        setByte(6, encryptionMethod);
        setByte(7, token);
        setByte(8, networkFlags);
        setByte(9, rssi);
        setByte(10, ber);
        setLong(11, currentTimeAndDate.getEpochSecond());
    }

    @JsonProperty("cmiu_id")
    public int getCmiuId()
    {
        return getDataBuffer().getInt(0);
    }

    @JsonProperty("sequence_number")
    public int getSequenceNumber()
    {
        return getDataBuffer().get(4);
    }

    @JsonProperty("key_info")
    public int getKeyInfo()
    {
        return getDataBuffer().get(5);
    }

    @JsonProperty("encryption_method")
    public int getEncryptionMethod()
    {
        return getDataBuffer().get(6);
    }

    @JsonProperty("token")
    public int getToken()
    {
        return getDataBuffer().get(7);
    }

    @JsonProperty("network_flags")
    public int getNetworkFlags()
    {
        return getDataBuffer().get(8);
    }

    /**
     * received signal strength indication
     * @return
    0 - (-113) dBm or less
    1 - (-111) dBm
    2..30 - (-109)dBm..(-53)dBm / 2 dBm per step
    31 - (-51)dBm or greater
    99 - not known or not detectable
     */
    @JsonProperty("cellular_rssi")
    public int getRssi()
    {
        return getDataBuffer().get(9);
    }


    /**
     * Get the RSSI as a dBm value
     * @return -113 if the dBm is -113 or less
     * Value between -111 and -53 if dBm is known
     * -51 if the dBm value is -51 or greater
     * null if the dBm is not known or not detectable, or not in the expected range
     */
    @JsonProperty("cellular_rssi_dbm")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Integer getRssiDbm()
    {
        final int dbmIndexValue =  getRssi();
        if (dbmIndexValue <= 31)
        {
            return -113 + 2 * dbmIndexValue;
        }
        else if (dbmIndexValue == 99)
        { //not known or detectable
            return null;
        }
        else
        { //unexpected value
            return null;
        }
    }

    /**
     * bit error rate
     * @return
    0 - less than 0.2%
    1 - 0.2% to 0.4%
    2 - 0.4% to 0.8%
    3 - 0.8% to 1.6%
    4 - 1.6% to 3.2%
    5 - 3.2% to 6.4%
    6 - 6.4% to 12.8%
    7 - more than 12.8%
    99 - not known or not detectable
     */
    @JsonProperty("cellular_ber")
    public int getBer()
    {
        return getDataBuffer().get(10);
    }

    /**
     * Get the bit error rate lower bound of percentage
     *
    0 - less than 0.2%
    1 - 0.2% to 0.4%
    2 - 0.4% to 0.8%
    3 - 0.8% to 1.6%
    4 - 1.6% to 3.2%
    5 - 3.2% to 6.4%
    6 - 6.4% to 12.8%
    7 - more than 12.8%
    99 - not known or not detectable
     * @return Lower bound on BER, null if not known, NaN if not an expected value
     */
    @JsonProperty("cellular_ber_pc_min")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Double getBerPercentageMin()
    {
        final int pcIndexValue = getBer();
        if (pcIndexValue == 0)
        {
            return Double.valueOf(0.0);
        }
        else if (pcIndexValue >= 1 && pcIndexValue <= 7)
        {
            int pow2 = 1 << pcIndexValue; //1->2, 2->4, 3->8...
            return Double.valueOf(0.1 * pow2);
        }
        else if (pcIndexValue == 99)
        { //not known or detectable
            return null;
        }
        else
        { //unexpected value
            return null;
        }
    }


    /**
     * Get the bit error rate lower bound of percentage
     * @return Lower bound on BER, null if not known, NaN if not an expected value
     */
    @JsonProperty("cellular_ber_pc_max")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Double getBerPercentageMax()
    {
        final int pcIndexValue = getBer();
        if (pcIndexValue >= 0 && pcIndexValue <= 6)
        {
            int pow2 = 2 << pcIndexValue; //0->2, 1->4, 2->8, 3->16...
            return Double.valueOf(0.1 * pow2);
        }
        else if (pcIndexValue == 7)
        { //at least 12.8%
            return Double.valueOf(25.6);
        }
        else if (pcIndexValue == 99)
        { //not known or detectable
            return null;
        }
        else
        { //unexpected value
            return null;
        }
    }

    /**
     * 64 bit, time_t  Unix format. (seconds after 1 January 1970)
     * @return Time and date when packet generated
     */
    @JsonProperty("packet_creation_date_time")
    public UnixTimestamp getCurrentTimeAndDate()
    {
        return new UnixTimestamp(getDataBuffer().getLong(11));
    }

    /**
     * formats the packet header data into a string
     * @return String with packet header data
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": CMIU Packet Header [");
        ret.append("CMIU ID=").append(getCmiuId());
        ret.append(", Sequence number=").append(getSequenceNumber());
        ret.append(", Key info=").append(getKeyInfo());
        ret.append(", Encryption method=").append(getEncryptionMethod());
        ret.append(", Token=").append(getToken());
        ret.append(", Network flags=").append(getNetworkFlags());
        ret.append(", RSSI=").append(getRssi());
        ret.append(", BER=").append(getBer());
        ret.append(", Current date and time=").append(getCurrentTimeAndDate());
        ret.append("]");

        return ret.toString();
    }

}
