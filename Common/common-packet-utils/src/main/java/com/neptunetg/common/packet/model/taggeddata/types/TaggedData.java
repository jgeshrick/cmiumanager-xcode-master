/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.types;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;

/**
 * Represents a tag and its data
 * Created by ABH1 on 05/02/2015.
 */
public class TaggedData
{
    private final TagId tag;

    /**
     * Construct a generic tagged data object.
     * Represents a tag ID and possibly some data
     * @param tag = tag ID of the tag.
     */
    public TaggedData(TagId tag)
    {
        this.tag = tag;
    }

    @JsonIgnore
    public String getTagSummary()
    {
        return tag.getTagName() + " (no type)";
    }

    @JsonIgnore
    public TagId getTagId()
    {
        return tag;
    }

    @JsonProperty("meta")
    public TaggedDataMetadata getMetadata()
    {
        return new TaggedDataMetadata(tag, null, null);
    }

    @Override
    public String toString()
    {
        return getTagSummary();
    }

    /**
     * Convert tag to a JSON string.  It consists of a structure {"meta":
     * {"data_type":{"name":"XXX","id":101},"array_length":123,"tag_id":{"id":101,"tag_name":"XXX"}}, prop1, prop2... }
     * @return JSON string
     */
    public String toJson()
    {
        try
        {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        }
        catch (JsonProcessingException e)
        {
            //not expected, so rethrow as unchecked exception
            throw new RuntimeException("Error converting tag object of type " + getClass() + " to JSON", e);
        }
    }

    /**
     * Convert tag to a pretty JSON string.  It consists of a structure {"meta":
     * {"data_type":{"name":"XXX","id":101},"array_length":123,"tag_id":{"id":101,"tag_name":"XXX"}}, prop1, prop2... }
     * with indentation whitespace and carriage returns
     * @return JSON string
     */
    public String toJsonPretty()
    {
        try
        {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        }
        catch (JsonProcessingException e)
        {
            //not expected
            throw new RuntimeException("Error converting tag object of type " + getClass() + " to JSON", e);
        }

    }

    /**
     * Serialize into byte array
     * @return serialized data
     */
    public byte[] serialize()
    {
        return new byte[]{(byte)this.tag.getId()};
    }

}
