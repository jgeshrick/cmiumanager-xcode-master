/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.builder;

import com.neptunetg.common.packet.model.taggeddata.packets.R900MiuReadingsPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.MqttPacketHeaderData;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.R900FskReadingArray;
import com.neptunetg.common.packet.model.taggeddata.types.R900OokReadingArray;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Builds packet type 7
 */
public class R900MiuReadingsPacketBuilder
{

    private final int sequenceNumber;

    private List<byte[]> ookPacketData = new ArrayList<>();
    private List<Integer> ookPacketOffset = new ArrayList<>();
    private List<byte[]> fskPacketData = new ArrayList<>();
    private List<Integer> fskPacketOffset = new ArrayList<>();

    public R900MiuReadingsPacketBuilder(int sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    /**
     * Get the MIU ID of the 25-byte OOK packet starting at the current read position of the byte buffer
     * @param bigEndianByteBuffer Byte buffer.  Must be big-endian, but this is not checked, for efficiency
     * @return MIU ID
     */
    public static int getMiuIdFromOokPacketInBigEndianByteBuffer(ByteBuffer bigEndianByteBuffer)
    {
        //big-endian int32 starting at +5
        return bigEndianByteBuffer.getInt(bigEndianByteBuffer.position() + 5);
    }

    /**
     * Get the MIU ID of the 25-byte FSK packet starting at the current read position of the byte buffer
     * @param bigEndianByteBuffer Byte buffer.  Must be big-endian, but this is not checked, for efficiency
     * @return MIU ID
     */
    public static int getMiuIdFromFskPacketInBigEndianByteBuffer(ByteBuffer bigEndianByteBuffer)
    {
        //big-endian int32 starting at +15
        return bigEndianByteBuffer.getInt(bigEndianByteBuffer.position() + 15);
    }

    /**
     * Get the highest RSSI, lowest and average RSSI value from all the packets in the builder
     * @return Object containing max, min, avg and count of RSSI
     */
    public R900MiuRssiSummary getRssiSummary()
    {
        int rssiMax = Integer.MIN_VALUE;
        int rssiMin = Integer.MAX_VALUE;
        int rssiSum = 0;

        final int ookPacketCount = ookPacketData.size();
        final int fskPacketCount = fskPacketData.size();

        for (int ookI = 0; ookI < ookPacketCount; ookI++)
        {
            //RSSI at +19 big endian Int16 in OOK packet.  See ETI 29-01 1.3.21
            final byte[] arr = ookPacketData.get(ookI);
            final int off = ookPacketOffset.get(ookI) + 19;

            final int rssi = (arr[off] << 8) + (arr[off + 1] & 0xff);
            if (rssi > rssiMax)
            {
                rssiMax = rssi;
            }
            if (rssi < rssiMin)
            {
                rssiMin = rssi;
            }
            rssiSum += rssi;
        }
        for (int fskI = 0; fskI < fskPacketCount; fskI++)
        {
            //RSSI at +23 Int8 in FSK packet.  See ETI 29-01 1.3.21
            final byte[] arr = fskPacketData.get(fskI);
            final int off = fskPacketOffset.get(fskI) + 23;

            final int rssi = arr[off];
            if (rssi > rssiMax)
            {
                rssiMax = rssi;
            }
            if (rssi < rssiMin)
            {
                rssiMin = rssi;
            }
            rssiSum += rssi;
        }
        return new R900MiuRssiSummary(rssiMin, rssiMax, rssiSum, ookPacketCount + fskPacketCount);
    }

    /**
     * Get the number of OOK packets
     * @return Number of OOK packets in the builder
     */
    public int getOokPacketCount()
    {
        return this.ookPacketData.size();
    }

    /**
     * Get the number of FSK packets
     * @return Number of FSK packets in the builder
     */
    public int getFskPacketCount()
    {
        return this.fskPacketData.size();
    }

    /**
     * Add the packet from the buffer.  Only stores a reference to the underlying data byte array of ookPacketBuffer,
     * so the original buffer is retained in memory.
     * @param ookPacketBuffer Buffer containing OOK packet.  Position is advanced by 25 bytes
     */
    public void addOokPacket(ByteBuffer ookPacketBuffer)
    {
        this.ookPacketData.add(ookPacketBuffer.array());
        this.ookPacketOffset.add(Integer.valueOf(ookPacketBuffer.position()));
        ookPacketBuffer.position(ookPacketBuffer.position() + 25);
    }

    /**
     * Add the packet from the buffer.  Only stores a reference to the underlying data byte array of
     * fskPacketBuffer, so the original buffer is retained
     * in memory.
     * @param fskPacketBuffer Buffer containing FSK packet.  Position is advanced by 25 bytes
     */
    public void addFskPacket(ByteBuffer fskPacketBuffer)
    {
        this.fskPacketData.add(fskPacketBuffer.array());
        this.fskPacketOffset.add(Integer.valueOf(fskPacketBuffer.position()));
        fskPacketBuffer.position(fskPacketBuffer.position() + 25);
    }

    private static MqttPacketHeaderData buildMqttPacketHeader(int sequenceNumber)
    {
        //build MqttPacketHeader
        return new MqttPacketHeaderData.Builder()
                .sequenceNumber(sequenceNumber)
                .keyInfo(0)
                .encryptionMethod(0)
                .token(0)
                .build();
    }

    public R900MiuReadingsPacket build()
    {
        final MqttPacketHeaderData headerTag = buildMqttPacketHeader(sequenceNumber);

        final R900OokReadingArray ook = new R900OokReadingArray(TagId.R900ReadingsOok, this.ookPacketData, this.ookPacketOffset);
        final R900FskReadingArray fsk = new R900FskReadingArray(TagId.R900ReadingsFsk, this.fskPacketData, this.fskPacketOffset);

        return new R900MiuReadingsPacket(headerTag, ook, fsk);
    }

}
