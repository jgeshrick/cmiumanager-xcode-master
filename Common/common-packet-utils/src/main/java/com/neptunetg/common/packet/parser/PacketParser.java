/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.packet.parser;

import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.SecureBlockArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Parses raw packet data into a tagged packet object, which contains
 * tagged data objects
 */
public class PacketParser
{
    /**
     * Parses raw packet data into a tagged packet object, which contains
     * tagged data objects
     */
    public static TaggedPacket parseTaggedPacket(byte[] rawPacketData)
    {
        final ByteBuffer byteReader = ByteBuffer.wrap(rawPacketData).asReadOnlyBuffer();
        byteReader.order(ByteOrder.LITTLE_ENDIAN);

        final byte packetTypeValue = byteReader.get();
        final TaggedDataPacketType packetType = TaggedDataPacketType.getPacketType(packetTypeValue);

        final TagSequenceParser tagSequenceParser = new TagSequenceParser();

        final TagSequence tags = tagSequenceParser.parseTagSequence(byteReader);

        return new TaggedPacket(packetType, tags);
    }

    /**
     * Parse a secure block array data to retrieve content in the secure data area
     * @param secureBlockArrayData the SecureData tag containing encrypted contents
     * @return A map of parsed tags in the secure data. The key is the tag id.
     */
    public static Map<Integer, TaggedData> parseSecureTag(final SecureBlockArrayData secureBlockArrayData)
    {
        TreeMap<Integer, TaggedData> tpMap = new TreeMap<>();

        if (secureBlockArrayData != null)
        {
            final ByteBuffer secureData = secureBlockArrayData.getEncryptedData();
            final ByteBuffer decryptedData = secureData.duplicate().order(ByteOrder.LITTLE_ENDIAN);

            final TagSequenceParser secureDataParser = new TagSequenceParser();
            final TagSequence secureTags = secureDataParser.parseTagSequence(decryptedData);

            //Add tags in secured data to the treemap
            for (TaggedData tag : secureTags)
            {
                if (tag.getTagId().getId() > 0)
                {
                    tpMap.put(tag.getTagId().getId(), tag);
                }
            }
        }

        return tpMap;
    }

    /**
     * Parse data in the secure block array into a list of tagged data. Tagged data can be non unique, i.e. tags with
     * same tag id are allowed and will be added to the list in the same order as the block data.
     * @param secureBlockArrayData data in the secure block array
     * @return a list of tagged data.
     */
    public static List<TaggedData> parseNonUniqueSecureTag(final SecureBlockArrayData secureBlockArrayData)
    {
        List<TaggedData> tpList = new ArrayList<>();

        if (secureBlockArrayData != null)
        {
            final ByteBuffer secureData = secureBlockArrayData.getEncryptedData();
            final ByteBuffer decryptedData = secureData.duplicate().order(ByteOrder.LITTLE_ENDIAN);

            final TagSequenceParser secureDataParser = new TagSequenceParser();
            final TagSequence secureTags = secureDataParser.parseTagSequence(decryptedData);

            //Add tags in secured data to the treemap
            for (TaggedData tag : secureTags)
            {
                if (tag.getTagId().getId() > 0)
                {
                    tpList.add(tag);
                }
            }
        }

        return tpList;
    }



}
