/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.types;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Tag type 14.
 * Image version info.  10 bytes big endian binary coded decimal representation of version number; bytes are:
 * 0. major revision
 * 1. minor revision
 * 2-5. date YYMMDD or YYYYMMDD
 * 6-10. build number or counter
 * e.g. version 14.7.150409.12345678 is stored as
 * 0x14  0x07  0x00 0x15 0x04 0x09  0x12 0x34 0x56 0x78
 */
public class ImageVersionInfoData extends TaggedDataWithType
{

    public static int SIZE_IN_BYTES = 10;

    private static int POW_2_28 = 268435456; //2^28

    private final ByteBuffer myBuffer;

    /**
     * @param tag The tag ID
     * @param major Major version
     * @param minor Minor version
     * @param revDate Revision date as integer in form YYMMDD
     * @param buildNum Build number
     */
    public ImageVersionInfoData(TagId tag, int major, int minor, int revDate, int buildNum)
    {
        super(tag, TagDataType.ImageVersionInfo);

        this.myBuffer = ByteBuffer.allocate(SIZE_IN_BYTES).order(ByteOrder.BIG_ENDIAN);

        encodeBcdByteToBuffer(0, major);
        encodeBcdByteToBuffer(1, minor);
        encodeBcdIntToBuffer(2, revDate);
        encodeBcdIntToBuffer(6, buildNum);

    }

    /**
     * Construct using raw data
     * @param tag Tag ID
     * @param data Data for version number - must be 10 bytes
     */
    public ImageVersionInfoData(TagId tag, byte[] data)
    {
        super(tag, TagDataType.ImageVersionInfo);

        if (data.length != SIZE_IN_BYTES)
        {
            throw new IllegalArgumentException("Version info data must be 10 bytes");
        }
        this.myBuffer = ByteBuffer.allocate(SIZE_IN_BYTES).order(ByteOrder.BIG_ENDIAN);
        myBuffer.put(data);
        myBuffer.position(0);
    }


    /**
     * revision Major.Minor
     */
    @JsonProperty("major")
    public int getMajorRelease()
    {
        return decodeBcdByteFromBuffer(0);
    }

    @JsonProperty("minor")
    public int getMinorRelease()
    {
        return decodeBcdByteFromBuffer(1);
    }

    /**
     * Active revision (YYMMDD)
     */
    @JsonProperty("date")
    public int getRevDate()
    {
        return decodeBcdIntFromBuffer(2);
    }

    /**
     * Active revision build
     */
    @JsonProperty("rev")
    public int getBuildRev()
    {
        return decodeBcdIntFromBuffer(6);
    }

    /**
     * Get the full version string
     * @return Version as major.minor.date.rev
     */
    @JsonProperty("version_string")
    public String getVersion()
    {
        return getMajorRelease() + "." + getMinorRelease() + "." + getRevDate() + "." + getBuildRev();
    }

    private void encodeBcdByteToBuffer(int offset, int value)
    {
        int bcdValue = (value % 10) + (value / 10) * 16;
        this.myBuffer.put(offset, (byte)bcdValue);
    }

    private void encodeBcdIntToBuffer(int offset, int value)
    {
        long pow16 = 1L;

        long bcdValue = 0L;

        long valueToEncode = ((long)value) & 0xffffffffL; //unsigned int

        while (valueToEncode != 0 && pow16 <= POW_2_28) //2^28, last valid power to add
        {
            bcdValue += (valueToEncode % 10L) * pow16;
            valueToEncode /= 10L;
            pow16 *= 16L;
        }

        this.myBuffer.putInt(offset, (int) bcdValue);
    }

    private int decodeBcdByteFromBuffer(int offset)
    {
        int byteValue = this.myBuffer.get(offset);
        return ((byteValue >> 4) & 0xf) * 10 + (byteValue & 0xf);
    }


    private int decodeBcdIntFromBuffer(int offset)
    {
        int valueToDecode = myBuffer.getInt(offset);

        int ret = 0;
        int pow10 = 1;

        for (int nybbleIndex = 0; nybbleIndex < 8; nybbleIndex++)
        {
            ret += (valueToDecode & 0xf) * pow10;
            valueToDecode >>= 4;
            pow10 *= 10;
        }

        return ret;
    }


    @Override
    public String toString()
    {
        return getVersion();
    }


    /**
     * Getter that returns a string summary of the tag
     * @return Summary text of tag
     */
    @Override
    public String getTagSummary()
    {
        return getTagId().getTagName() + " " + getVersion();
    }

    /**
     * Get the binary data of the tag
     * @return tagId, 14, then 1 byte major, 1 byte minor, 4 bytes date, 4 bytes rev, in big endian BCD for readability
     */
    @Override
    public byte[] serialize()
    {
        byte[] tagIdAndType = super.serialize();

        final ByteBuffer ret = ByteBuffer.allocate(tagIdAndType.length + SIZE_IN_BYTES)   //add array size
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(tagIdAndType);          //tag number, type

        ret.put(myBuffer);
        return ret.array();
    }

}
