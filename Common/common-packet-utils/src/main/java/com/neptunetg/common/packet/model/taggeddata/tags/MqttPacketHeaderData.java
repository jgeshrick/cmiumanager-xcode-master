/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag 35
 Sequence number	Uint8  	 The sequence number is used for security purposes.
 Key info	Uint8  	The Key Info is  used for security purposes.
 Encryption method	Uint8  	Encryption method is used for security purposes
 Token(AES CRC)	Uint8  	Token is used for security purposes

 */

public class MqttPacketHeaderData extends IntegerArrayData
{
    public static int SIZE_IN_BYTES = 4;

    public MqttPacketHeaderData(int sequenceNumber, int keyInfo, int encryptionMethod, int token)
    {
        super(TagId.MQTTPacketHeader, TagDataType.ByteRecord, new byte[SIZE_IN_BYTES], SIZE_IN_BYTES);

        setByte(0, sequenceNumber);
        setByte(1, keyInfo);
        setByte(2, encryptionMethod);
        setByte(3, token);
    }

    public MqttPacketHeaderData(TagDataType dataType, byte[] data)
    {
        super(TagId.MQTTPacketHeader, dataType, data, data.length);
    }

    @JsonProperty("packet_sequence")
    public int getSequenceNumber()
    {
        return getDataBuffer().get(0);
    }

    @JsonProperty("packet_key")
    public int getKeyInfo()
    {
        return getDataBuffer().get(1);
    }

    @JsonProperty("packet_encryption")
    public int getEncryptionMethod()
    {
        return getDataBuffer().get(2);
    }

    @JsonProperty("packet_token")
    public int getToken()
    {
        return getDataBuffer().get(3);
    }

    /**
     * formats the packet header data into a string
     * @return String with packet header data
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": MQTT Packet Header [");
        ret.append("Sequence number=").append(getSequenceNumber());
        ret.append(", Key info=").append(getKeyInfo());
        ret.append(", Encryption method=").append(getEncryptionMethod());
        ret.append(", Token=").append(getToken());
        ret.append("]");

        return ret.toString();
    }

    /**
     * A builder to build HostPacketHeaderData tag.
     */
    public static class Builder
    {
        private int fieldSetMask = 0;   //a bit mask to check all fields has been set before building the header
        private int sequenceNumber = 0;
        private int keyInfo = 0;
        private int encryptionMethod = 0;
        private int token = 0;

        public Builder sequenceNumber(int val)
        {
            fieldSetMask |= 0x01;
            sequenceNumber = val;
            return this;
        }

        public Builder keyInfo(int val)
        {
            fieldSetMask |= 0x02;
            keyInfo = val;
            return this;
        }

        public Builder encryptionMethod(int val)
        {
            fieldSetMask |= 0x04;
            encryptionMethod = val;
            return this;
        }

        public Builder token(int val)
        {
            fieldSetMask |= 0x08;
            token = val;
            return this;
        }

        public MqttPacketHeaderData build()
        {
            //check all fields has been set
            if ((fieldSetMask & 0x0f) != 0x0f)
            {
                throw new IllegalArgumentException("Undefined fields in MQTTPacketHeaderData");
            }

            byte[] data = {
                    (byte)sequenceNumber,
                    (byte)keyInfo,
                    (byte)encryptionMethod,
                    (byte)token,
            };

            return new MqttPacketHeaderData(TagDataType.ByteRecord, data);
        }
    }

}
