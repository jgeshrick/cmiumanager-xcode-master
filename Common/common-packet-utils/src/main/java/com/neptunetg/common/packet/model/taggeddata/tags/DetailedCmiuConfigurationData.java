/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by WJD1 on 10/02/2015.
 *
 * This tag holds detailed information about the CMIU status and settings
 *
 CMIU Configuration tag name	Byte
 CMIU Configuration tag  type	Byte
 CMIU Configuration byte array size	Byte
 CMIU type	Byte
 Device HW  Revision	Uint16
 Firmware revision (running)	Uint16
 Bootloader version??	Uint16
 Manufacture date	Uint64	64 bit, time_t  Unix format. (seconds after 1 January 1970)
 Initial call in time	Uint64	Time for initial call.All time in UTC.
 Call in interval	Byte	Interval between call in, in hours
 Call in window	Byte	Duration of randomized call in. In 15 minute increments
 Reporting window number of retries	Byte	Number of retries if communications fail
 Number of attached devices	Byte	Number of devices such as dual registers.
 Date of installation	Uint64	64 bit, time_t  Unix format. (seconds after 1 January 1970)
 Date of last mag swipe	Uint64	64 bit, time_t  Unix format. (seconds after 1 January 1970)
 Mag swipe counter	Byte	Counter for each mag swipe instance.
 Estimated battery capacity remaining	Uint16	TBD Battery status indicator
 Time error on last network time access	byte	Signed value in seconds. Current number of seconds of error between RTC and Network time. Maximum value is +/- 127 seconds.

 @deprecated Not to be used post pre-alpha trial

 Tag ID 2
 */
@Deprecated
public class DetailedCmiuConfigurationData extends IntegerArrayData
{

    public static int SIZE_IN_BYTES = 73;

    public DetailedCmiuConfigurationData(TagDataType dataType, byte[] data)
    {
        super(TagId.DetailedCmiuConfiguration, dataType, data, data.length);
    }

    public DetailedCmiuConfigurationData(int cmiuType,
                                 short deviceHwRevision,
                                 ImageRevisionInfo firmwareRevision,
                                 ImageRevisionInfo bootloaderVersion,
                                 ImageRevisionInfo configVersion,
                                 UnixTimestamp manufactureDate,
                                 int initialCallInTime,
                                 int callInInterval, int callInWindow,
                                 int reportingWindowNumberOfRetries,
                                 int minsToReportingWindowQuietTime, int minsToReportingWindowQuietTimeEnd,
                                 int numberOfAttachedDevices,
                                 UnixTimestamp dateOfInstallation,
                                 UnixTimestamp dateOfLastMagSwipe,
                                 int magSwipeCounter,
                                 int estimateBatteryCapacityRemaining,
                                 int timeErrorOnLastNetworkTimeAccess)
    {
        super(TagId.DetailedCmiuConfiguration, TagDataType.ByteRecord, new byte[SIZE_IN_BYTES], SIZE_IN_BYTES);

        setByte(0, cmiuType);
        setShort(1, deviceHwRevision);
        firmwareRevision.embedInExistingBuffer(getDataBuffer(), 3);
        bootloaderVersion.embedInExistingBuffer(getDataBuffer(), 13);
        configVersion.embedInExistingBuffer(getDataBuffer(), 23);
        setLong(33, manufactureDate.getEpochSecond());
        setShort(41, initialCallInTime);
        setShort(43, callInInterval);
        setShort(45, callInWindow);
        setByte(47, reportingWindowNumberOfRetries);
        setShort(48, minsToReportingWindowQuietTime);
        setShort(50, minsToReportingWindowQuietTimeEnd);
        setByte(52, numberOfAttachedDevices);
        setLong(53, dateOfInstallation.getEpochSecond());
        setLong(61, dateOfLastMagSwipe.getEpochSecond());
        setByte(69, magSwipeCounter);
        setShort(70, estimateBatteryCapacityRemaining);
        setByte(72, timeErrorOnLastNetworkTimeAccess);
    }

    @JsonProperty("cmiu_type")
    public int getCmiuType()
    {
        return getDataBuffer().get(0);
    }

    @JsonProperty("cmiu_hw_rev")
    public int getDeviceHwRevision()
    {
        return getDataBuffer().getShort(1);
    }

    @JsonProperty("cmiu_active_fw")
    public ImageRevisionInfo getFirmwareRevision()
    {
        return new ImageRevisionInfo(getDataBuffer(), 3);
    }

    @JsonProperty("cmiu_bootloader")
    public ImageRevisionInfo getBootloaderVersion()
    {
        return new ImageRevisionInfo(getDataBuffer(), 13);
    }

    @JsonProperty("cmiu_active_config")
    public ImageRevisionInfo getConfigVersion()
    {
        return new ImageRevisionInfo(getDataBuffer(), 23);
    }

    /**
     * 64 bit, time_t  Unix format. (seconds after 1 January 1970)
     * @return Time and date when packet generated
     */
    @JsonProperty("cmiu_mfg_date")
    public UnixTimestamp getManufactureDate()
    {
        return new UnixTimestamp(getDataBuffer().getLong(33));
    }

    @JsonProperty("cmiu_reporting_start_time_mins")
    public int getReportingStartTime() { return getDataBuffer().getShort(41); }

    @JsonProperty("cmiu_reporting_interval_mins")
    public int getReportingInterval()
    {
        return getDataBuffer().get(43);
    }

    @JsonProperty("cmiu_reporting_window")
    public int getReportingWindow()
    {
        return getDataBuffer().get(45);
    }

    @JsonProperty("cmiu_reporting_retries")
    public int getReportingWindowNumberOfRetries()
    {
        return getDataBuffer().get(47);
    }

    @JsonProperty("cmiu_reporting_quiet_start_mins")
    public int getMinsToReportingWindowQuietTime()
    {
        return getDataBuffer().getShort(48);
    }

    @JsonProperty("cmiu_reporting_quiet_end_mins")
    public int getMinsToReportingWindowQuietTimeEnd()
    {
        return getDataBuffer().getShort(50);
    }

    @JsonProperty("cmiu_num_devices")
    public int getNumberOfAttachedDevices()
    {
        return getDataBuffer().get(52);
    }

    @JsonProperty("cmiu_install_date")
    public UnixTimestamp getDateOfInstallation()
    {
        return new UnixTimestamp(getDataBuffer().getLong(53));
    }

    @JsonProperty("cmiu_last_mag_swipe_date")
    public UnixTimestamp getDateOfLastMagSwipe()
    {
        return new UnixTimestamp(getDataBuffer().getLong(61));
    }

    @JsonProperty("cmiu_mag_swipe_counter")
    public int getMagSwipeCounter()
    {
        return getDataBuffer().get(69);
    }

    @JsonProperty("cmiu_battery_capacity")
    public int getEstimateBatteryCapacityRemaining()
    {
        return getDataBuffer().getShort(70);
    }

    @JsonProperty("cmiu_time_error")
    public int getTimeErrorOnLastNetworkTimeAccess()
    {
        return getDataBuffer().get(72);
    }

    /**
     * formats the configuration data into a string
     * @return String with configuration data
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Detailed CMIU Configuration [");
        ret.append("CMIU type=").append(getCmiuType());
        ret.append(", Device HW revision=").append(getDeviceHwRevision());
        ret.append(", Firmware revision=").append(getFirmwareRevision());
        ret.append(", Bootloader version=").append(getBootloaderVersion());
        ret.append(", Config version=").append(getConfigVersion());
        ret.append(", manufacture date=").append(getManufactureDate());
        ret.append(", Initial call in time=").append(getReportingStartTime());
        ret.append(", Call in interval=").append(getReportingInterval());
        ret.append(", Call in window=").append(getReportingWindow());
        ret.append(", Reporting window number of retries=").
                append(getReportingWindowNumberOfRetries());
        ret.append(", Reporting window quiet time start in minutes=").
                append(getMinsToReportingWindowQuietTime());
        ret.append(", Reporting window quiet time end in minutes=").
                append(getMinsToReportingWindowQuietTimeEnd());
        ret.append(", Number of attached devices=").
                append(getNumberOfAttachedDevices());
        ret.append(", Data of installation=").append(getDateOfInstallation());
        ret.append(", Date of last mag swipe=").
                append(getDateOfLastMagSwipe());
        ret.append(", Mag swipe counter=").append(getMagSwipeCounter());
        ret.append(", Estimated battery capacity remaining=").
                append(getEstimateBatteryCapacityRemaining());
        ret.append(", Time error on last network time access=").
                append(getTimeErrorOnLastNetworkTimeAccess());
        ret.append("]");

        return ret.toString();
    }


    /**
     * Represents the (revision, revdate, buildnum) revision information
     */
    public static class ImageRevisionInfo
    {

        public static int SIZE_IN_BYTES = 10;

        private static int POW_2_28 = 268435456; //2^28

        private ByteBuffer myBuffer;
        private int myOffset;

        /**
         *
         * @param major Major version
         * @param minor Minor version
         * @param revDate Revision date as integer in form YYMMDD
         * @param buildNum Build number
         */
        public ImageRevisionInfo(int major, int minor, int revDate, int buildNum)
        {
            myBuffer = ByteBuffer.allocate(SIZE_IN_BYTES).order(ByteOrder.LITTLE_ENDIAN);
            myOffset = 0;

            encodeBcdByteToBuffer(0, major);
            encodeBcdByteToBuffer(1, minor);
            encodeBcdIntToBuffer(2, revDate);
            encodeBcdIntToBuffer(6, buildNum);

        }

        public ImageRevisionInfo(ByteBuffer ambientBuffer, int offset)
        {
            this.myBuffer = ambientBuffer;
            this.myOffset = offset;
        }

        public void embedInExistingBuffer(ByteBuffer existingBuffer, int offset)
        {
            byte[] target = existingBuffer.array();
            byte[] source = this.myBuffer.array();
            System.arraycopy(source, this.myOffset, target, offset, SIZE_IN_BYTES);
            this.myOffset = offset;
            this.myBuffer = existingBuffer;
        }


        /**
         * revision Major.Minor
         */
        @JsonProperty("major")
        public int getMajorRelease()
        {
            return decodeBcdByteFromBuffer(0);
        }

        @JsonProperty("minor")
        public int getMinorRelease()
        {
            return decodeBcdByteFromBuffer(1);
        }

        /**
         * Active revision (YYMMDD)
         */
        @JsonProperty("date")
        public int getRevDate()
        {
            return decodeBcdIntFromBuffer(2);
        }

        /**
         * Active revision build
         */
        @JsonProperty("rev")
        public int getBuildRev()
        {
            return decodeBcdIntFromBuffer(6);
        }


        private void encodeBcdByteToBuffer(int offset, int value)
        {
            int bcdValue = (value % 10) + (value / 10) * 16;
            this.myBuffer.put(myOffset + offset, (byte)bcdValue);
        }

        private void encodeBcdIntToBuffer(int offset, int value)
        {
            long pow16 = 1L;

            long bcdValue = 0L;

            long valueToEncode = ((long)value) & 0xffffffffL; //unsigned int

            while (valueToEncode != 0 && pow16 <= POW_2_28) //2^28, last valid power to add
            {
                bcdValue += (valueToEncode % 10L) * pow16;
                valueToEncode /= 10L;
                pow16 *= 16L;
            }

            this.myBuffer.putInt(myOffset + offset, (int) bcdValue);
        }

        private int decodeBcdByteFromBuffer(int offset)
        {
            int byteValue = this.myBuffer.get(this.myOffset + offset);
            return ((byteValue >> 4) & 0xf) * 10 + (byteValue & 0xf);
        }


        private int decodeBcdIntFromBuffer(int offset)
        {
            int valueToDecode = myBuffer.getInt(myOffset + offset);

            int ret = 0;
            int pow10 = 1;

            for (int nybbleIndex = 0; nybbleIndex < 8; nybbleIndex++)
            {
                ret += (valueToDecode & 0xf) * pow10;
                valueToDecode >>= 4;
                pow10 *= 10;
            }

            return ret;
        }


        @Override
        public String toString()
        {
            return getMajorRelease() + "." + getMinorRelease() + "." + getRevDate() + "." + getBuildRev();
        }

        @Override
        public boolean equals(Object o)
        {
            if (o == null || !(o instanceof ImageRevisionInfo))
            {
                return false;
            }
            ImageRevisionInfo other = (ImageRevisionInfo)o;

            for (int i = 0; i < SIZE_IN_BYTES; i++)
            {
                if (myBuffer.get(myOffset + i) != other.myBuffer.get(other.myOffset + 1))
                {
                    return false;
                }
            }
            return true;
        }

        @Override
        public int hashCode()
        {
            return myBuffer.getInt(myOffset);
        }
    }

}
