/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag 31
 */
public class ConnectionTimingLogData extends IntegerArrayData
{

    public static int SIZE_IN_BYTES = 18;

    public ConnectionTimingLogData(UnixTimestamp dateOfConnection, int timeFromPowerOnToNetworkRegistration,
                                   int timeFromNetworkRegistrationToContextActivation, int timeFromContextActivationToServerConnection,
                                   int timeToTransferMessageToServer, int timeToDisconnectAndShutdown)
    {
        super(TagId.ConnectionTimingLog, TagDataType.ByteRecord, new byte[SIZE_IN_BYTES], SIZE_IN_BYTES);

        setLong(0, dateOfConnection.getEpochSecond());
        setShort(8, timeFromPowerOnToNetworkRegistration);
        setShort(10, timeFromNetworkRegistrationToContextActivation);
        setShort(12, timeFromContextActivationToServerConnection);
        setShort(14, timeToTransferMessageToServer);
        setShort(16, timeToDisconnectAndShutdown);
    }
    
    public ConnectionTimingLogData(TagDataType dataType, byte[] data)
    {
        super(TagId.ConnectionTimingLog, dataType, data, data.length);
    }

    /**
     * Time of last connection.
     * @return
     */
    @JsonProperty("last_connection_datetime")
    public UnixTimestamp getDateOfConnection()
    {
        return new UnixTimestamp(getDataBuffer().getLong(0));
    }

    /**
     * Time in milliseconds. 65,535 equals max time exceeded.
     * @return
     */
    @JsonProperty("register_time")
    public int getTimeFromPowerOnToNetworkRegistration()
    {
        return getDataBuffer().getShort(8) & 0xffff;
    }

    /**
     * Time in milliseconds. 65,535 equals max time exceeded.
     * @return
     */
    @JsonProperty("register_time_to_activate_context")
    public int getTimeFromNetworkRegistrationToContextActivation()
    {
        return getDataBuffer().getShort(10) & 0xffff;
    }

    /**
     * Time in milliseconds. 65,535 equals max time exceeded.
     * @return
     */
    @JsonProperty("register_time_connected")
    public int getTimeFromContextActivationToServerConnection()
    {
        return getDataBuffer().getShort(12) & 0xffff;
    }

    /**
     * Time in milliseconds. 65,535 equals max time exceeded.
     * @return
     */
    @JsonProperty("register_time_to_transfer_packet")
    public int getTimeToTransferMessageToServer()
    {
        return getDataBuffer().getShort(14) & 0xffff;
    }

    /**
     * Time in milliseconds. 65,535 equals max time exceeded.
     * @return
     */
    @JsonProperty("disconnect_time")
    public int getTimeToDisconnectAndShutdown()
    {
        return getDataBuffer().getShort(16) & 0xffff;
    }

    /**
     * formats the Connection Timing Log data into a string
     * @return String with connection timing log
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Connection Timing Log [");
        ret.append("Date and Time of Connection=").append(getDateOfConnection());
        ret.append(", Time from power on till registration on the network=").
                append(getTimeFromPowerOnToNetworkRegistration());
        ret.append(", Time from network registration to context activiation=").
                append(getTimeFromNetworkRegistrationToContextActivation());
        ret.append(", Time from context activation till server connection=").
                append(getTimeFromContextActivationToServerConnection());
        ret.append(", Time to transfer a message to the server=").
                append(getTimeToTransferMessageToServer());
        ret.append(", Time to disconnection and shutdown=").
                append(getTimeToDisconnectAndShutdown());
        ret.append("]");

        return ret.toString();
    }

}
