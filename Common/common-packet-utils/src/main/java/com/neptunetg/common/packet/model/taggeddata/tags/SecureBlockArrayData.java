/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedDataMetadata;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedDataWithType;
import com.neptunetg.common.util.HexUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents an array of 16-byte secure blocks
 */
public class SecureBlockArrayData extends TaggedDataWithType
{
    private final ByteBuffer data;
    private final int arrayLength;

    /**
     * Create an instance of a tag+data
     * @param dataType The type of the data (must be secure block array or extended secure block array)
     * @param data  encrypted data - must be a multiple of 16 bytes long
     */
    public SecureBlockArrayData(TagDataType dataType, byte[] data)
    {
        this(dataType, data, data.length / 16);
    }

    /**
     * Create an instance of a tag+data
     * @param dataType The type of the data (must be secure block array or extended secure block array)
     * @param data  encrypted data - must be a multiple of 16 bytes long
     * @param arrayLength Length of array in 16 byte block.  Must be data.length/16
     */
    public SecureBlockArrayData(TagDataType dataType, byte[] data, int arrayLength)
    {
        super(TagId.SecureDataBlockArray, dataType);

        if ((data.length & 15) != 0)
        {
            throw new IllegalArgumentException("Data must be multiple of 16 bytes long for secure block data!");
        }

        this.data = ByteBuffer.wrap(data.clone())
                .order(ByteOrder.LITTLE_ENDIAN)
                .asReadOnlyBuffer();
        this.arrayLength = arrayLength;
    }


    @JsonProperty("meta")
    @Override
    public TaggedDataMetadata getMetadata()
    {
        return new TaggedDataMetadata(getTagId(), getDataType(), this.arrayLength);
    }

    /**
     * Get a copy of the encrypted data
     * @return New array containing the encrypted data
     */
    @JsonIgnore
    public byte[] getEncryptedDataCopy()
    {
        final ByteBuffer encDataBuf = this.data.duplicate();
        encDataBuf.rewind();
        final byte[] encData = new byte[encDataBuf.capacity()];
        encDataBuf.get(encData);
        return encData;
    }


    /**
     * Get a read-only buffer to the encrypted data
     * @return Encrypted data buffer
     */
    @JsonIgnore
    public ByteBuffer getEncryptedData()
    {
        final ByteBuffer encDataBuf = this.data.duplicate().order(ByteOrder.LITTLE_ENDIAN);
        encDataBuf.rewind();
        return encDataBuf;
    }


    @JsonIgnore
    public int getEncryptedDataLengthInBytes()
    {
        return this.data.capacity();
    }

    @JsonProperty("secure_data_value")
    public String getEncryptedDataHex()
    {
        return HexUtils.byteBufferToHex(data);
    }

    /**
     * Getter that returns a string summary of the tag
     */
    @Override
    public String getTagSummary()
    {
        return getTagId().getTagName() + " (" + (data.capacity() / getDataType().getUnitSize()) + " blocks of 16 bytes each)";
    }

    @Override
    public String toString()
    {
        return getTagSummary();
    }

    @Override
    public byte[] serialize()
    {
        byte[] tagIdAndType = super.serialize();
        final int arrayLengthLength = getDataType().getSizeBytesToFollow();

        final ByteBuffer ret = ByteBuffer.allocate(tagIdAndType.length + data.capacity() + arrayLengthLength)   //add array size
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(tagIdAndType);          //tag number, type

        switch (arrayLengthLength) {
            case 1:
                ret.put((byte) arrayLength); //array size in blocks
                break;
            case 2:
                ret.putShort((short) arrayLength); //array size in blocks
                break;
            default:
                throw new UnsupportedOperationException("Can't serialize tag with " + getDataType().getSizeBytesToFollow() + " bytes of length data");
        }
        ret.put(data);                  //array content
        return ret.array();
    }

    /**
     * A builder for building a supplied tag list into the data bytes for the secure block array.
     * @param tagList
     * @return
     */
    public static SecureBlockArrayData build(List<TaggedData> tagList)
    {
        TagSequence secureTagSequence = new TagSequence(new ArrayList<>(tagList));
        byte[] secureDataUnpadded = secureTagSequence.toByteSequence();
        byte[] secureDataPadded = Arrays.copyOf(secureDataUnpadded, (secureDataUnpadded.length / 16 + 1) * 16);

        SecureBlockArrayData secureBlockArrayData = new SecureBlockArrayData(TagDataType.SecureBlockArray, secureDataPadded);

        return secureBlockArrayData;
    }
}
