/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.lora.json;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by WJD1 on 09/06/2016.
 * A class for mapping EUI and PDU to JSON for sending a command to a L900
 */
public class SenetCommandPacketJson
{
    @JsonProperty("EUI")
    String eui;

    @JsonProperty("PDU")
    String pdu;

    public String getEui()
    {
        return eui;
    }

    public void setEui(String eui)
    {
        this.eui = eui;
    }

    public String getPdu()
    {
        return pdu;
    }

    public void setPdu(String pdu)
    {
        this.pdu = pdu;
    }
}
