/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag 45.
 * A now deprecated tag which holds data such as meter readings
 */
public class DeviceCurrentData extends IntegerArrayData
{
    public static int SIZE_IN_BYTES = 6;

    public DeviceCurrentData(int currentDeviceData, int deviceFlags)
    {
        super(TagId.DeviceCurrentData, TagDataType.ByteRecord, new byte[SIZE_IN_BYTES], SIZE_IN_BYTES);

        setInt(0, currentDeviceData);
        setShort(4, deviceFlags);
    }

    public DeviceCurrentData(TagDataType dataType, byte[] data)
    {
        super(TagId.DeviceCurrentData, dataType, data, data.length);
    }

    @JsonProperty("current_device_data")
    public int getCurrentDeviceData()
    {
        return getDataBuffer().getInt(0);
    }

    @JsonProperty("attached_device_flags")
    public int getDeviceFlags()
    {
        return getDataBuffer().getShort(4);
    }

    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Device Current Data [");
        ret.append("Current Device Data =").append(getCurrentDeviceData());
        ret.append(", Device Flags =").append(getDeviceFlags());
        ret.append("]");

        return ret.toString();
    }
}
