/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.packets;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Represents a sequence of tags
 */
public class TagSequence implements Iterable<TaggedData>
{
    private final List<TaggedData> tagList;

    /**
     * @param tagList Represents a packet that consists of a list of tags
     */
    public TagSequence(List<TaggedData> tagList)
    {
        this.tagList = new ArrayList<>(tagList);
    }

    /**
     * This returns the number of tags in the packet
     *
     * @return number of tags
     */
    public int tagCount()
    {
        return tagList.size();
    }

    /**
     * Provides an iterator to allow the tags in the packet to be iterated
     *
     * @return iterator
     */
    @Override
    public Iterator<TaggedData> iterator()
    {

        return tagList.iterator();
    }

    /**
     * Get the raw byte sequence for this tag sequence
     *
     * @return Raw byte sequence
     */
    public byte[] toByteSequence()
    {

        return toByteSequenceInternal(null, 1);
    }

    /**
     * Get the raw byte sequence for this tag sequence, preceded by an initial byte
     *
     * @param initialByte The first byte of the resulting byte array
     * @return Raw byte sequence beginning with initialByte
     */
    public byte[] toByteSequence(byte initialByte)
    {
        return toByteSequenceInternal(Byte.valueOf(initialByte), 1);
    }

    /**
     * Get the raw byte sequence for this tag sequence, padded with 0s to a multiple of tbe block size
     *
     * @param blockSize Block size in bytes size to pad to a full block
     * @return Raw byte sequence
     */
    public byte[] toByteSequencePadToBlockBoundary(int blockSize)
    {

        return toByteSequenceInternal(null, blockSize);
    }


    /**
     * Serialise the taggedPacket into raw byte array.
     *
     * @param initialByte If not null, insert this byte before the tag sequence
     * @param blockSize   Pad with zeroes to this block size
     * @return tagged packet as raw byte
     */
    private byte[] toByteSequenceInternal(Byte initialByte, int blockSize)
    {
        List<byte[]> taggedDataRawDataList = new ArrayList<>();

        //add tagged data
        int taggedDataBytesCount = 0;
        for (TaggedData td : this.tagList)
        {
            //call the serialize function in derived class

            byte[] rawData = td.serialize();
            taggedDataBytesCount += rawData.length;
            taggedDataRawDataList.add(rawData);
        }

        int retSize = taggedDataBytesCount;
        if (initialByte != null)
        {
            retSize++;
        }

        final int partialBlockBytes = retSize % blockSize;
        if (partialBlockBytes > 0)
        {
            retSize += blockSize - partialBlockBytes;
        }

        ByteBuffer byteBuffer = ByteBuffer.allocate(retSize);

        if (initialByte != null)
        {
            byteBuffer.put(initialByte.byteValue());
        }

        //Add serialised tagged data
        for (byte[] td : taggedDataRawDataList)
        {
            byteBuffer.put(td);
        }

        if (partialBlockBytes > 0)
        {
            final byte filler = (byte) TagId.NullTag.getId(); //0
            for (int i = blockSize; i > partialBlockBytes; i--)
            {
                byteBuffer.put(filler);
            }
        }

        return byteBuffer.array();
    }


    /**
     * Convert entire tag sequence to a JSON string.  It consists of a structure {"tags": [tag1, tag2...], "meta": {"packet_type_id": 0, "packet_type_name" : "XXX"}}
     *
     * @return JSON string
     */
    public String toJson()
    {
        try
        {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        }
        catch (JsonProcessingException e)
        {
            //not expected
            throw new RuntimeException("Error converting tagged packet to JSON", e);
        }
    }

    /**
     * Convert entire tag sequence to a pretty JSON string.  It consists of a structure {"tags": [tag1, tag2...], "meta": {"packet_type_id": 0, "packet_type_name" : "XXX"}}
     * with indentation whitespace and carriage returns
     *
     * @return JSON string
     */
    public String toJsonPretty()
    {
        try
        {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e)
        {
            //not expected
            throw new RuntimeException("Error converting tagged packet to JSON", e);
        }

    }

    /**
     * Find the first tag of the given type and return its JSON representation
     *
     * @param taggedDataType Required type
     * @param <T>            Required type
     * @return JSON representation of tag, or null if tag not found
     */
    public <T extends TaggedData> String tagToJson(Class<T> taggedDataType)
    {
        final T tag = findTag(taggedDataType);
        if (tag == null)
        {
            return null;
        } else
        {
            return tag.toJson();
        }
    }

    /**
     * Find the tag with the given ID and return its JSON representation
     *
     * @param requiredTagId  ID of tag to convert to JSON
     * @return JSON representation of tag, or null if tag not found
     */
    public String tagToJson(TagId requiredTagId)
    {
        final TaggedData tag = findTag(requiredTagId, TaggedData.class);
        if (tag == null)
        {
            return null;
        }
        else
        {
            return tag.toJson();
        }
    }

    /**
     * Find the first tag of the given type and return its JSON representation with carriage returns and indents
     *
     * @param taggedDataType Required type
     * @param <T>            Required type
     * @return JSON representation of tag, or null if tag not found
     */
    public <T extends TaggedData> String tagToJsonPretty(Class<T> taggedDataType)
    {
        final T tag = findTag(taggedDataType);
        if (tag == null)
        {
            return null;
        }
        else
        {
            return tag.toJsonPretty();
        }
    }



    /**
     * Find the tag with the given ID and return its JSON representation
     * with carriage returns and indentation
     *
     * @param requiredTagId  ID of tag to convert to JSON
     * @return JSON representation of tag, or null if tag not found
     */
    public String tagToJsonPretty(TagId requiredTagId)
    {
        final TaggedData tag = findTag(requiredTagId, TaggedData.class);
        if (tag == null)
        {
            return null;
        }
        else
        {
            return tag.toJsonPretty();
        }
    }

    /**
     * Extract and return the first tag with the given tag ID and cast it to
     * the requested type
     *
     * @param id             The tag ID
     * @param taggedDataType the tagged data type to be extracted from the tagged packet
     * @param <T>            Tagged data and its subclass
     * @return The extracted tagged data, of type T
     * @throws ClassCastException if a tag is found with the given ID but it does not match the requested type T
     */
    public <T extends TaggedData> T findTag(TagId id, Class<T> taggedDataType) throws ClassCastException
    {
        T returnTag = null;

        if (id != null)
        {
            for (TaggedData taggedData : this)
            {
                if (id.equals(taggedData.getTagId()))
                {
                    returnTag = (T) taggedData;
                    break;
                }
            }
        }

        return returnTag;
    }


    /**
     * Extract and return the first tagged data type found
     * @param taggedDataType the tagged data type to be extracted from the tagged packet
     * @param <T> Tagged data and its subclass
     * @return The extracted tagged data, of type T
     */
    public <T extends TaggedData> T findTag(Class<T> taggedDataType)
    {
        T returnTag = null;

        for(TaggedData taggedData : this)
        {
            //find packetHeader tag
            if (taggedData.getClass().equals(taggedDataType))
            {
                returnTag = (T)taggedData;
                break;
            }
        }

        return returnTag;
    }
}