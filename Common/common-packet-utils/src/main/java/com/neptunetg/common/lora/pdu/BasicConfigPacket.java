/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.pdu;

/**
 * Created by WJD1 on 09/08/2016.
 * A class to represent a basic config packet for L900
 * As defined in ETI 05-05
 */
public class BasicConfigPacket extends L900Packet
{
    private static int PACKET_SIZE_BYTES = 11;

    public BasicConfigPacket(int networkTimeLoss, int l900Errors, int antennaType, int batteryStatus, int magSwipe,
                             int firmwareVersion, int deviceType, long deviceId, int reserved_1,
                             int reverseFlow_35Day, int reverseFlow_24Hour, int leakState,
                             int batteryLife, int emptyPipe, int excessiveFlow, int tamper,
                             int reserved_2, int attachedDeviceAlarmMask)
    {
        super(PacketId.BASIC_CONFIG_PACKET, PACKET_SIZE_BYTES);

        setNumber(5, networkTimeLoss, 2);
        setNumber(7, antennaType, 1);
        setNumber(8, l900Errors, 2);
        setNumber(10, batteryStatus, 2);
        setNumber(12, magSwipe, 1);
        setNumber(13, firmwareVersion, 16);
        setNumber(29, deviceType, 5);
        setNumber(34, deviceId, 34);

        setNumber(68, reserved_1, 1);
        setNumber(69, reverseFlow_35Day, 2);
        setNumber(71, reverseFlow_24Hour, 2);
        setNumber(73, leakState, 2);
        setNumber(75, batteryLife, 2);
        setNumber(77, emptyPipe, 2);
        setNumber(79, excessiveFlow, 2);
        setNumber(81, tamper, 1);
        setNumber(82, reserved_2, 1);

        setNumber(84, attachedDeviceAlarmMask, 4);
    }

    public BasicConfigPacket(byte[] packetBytes)
    {
        super(packetBytes);
    }

    public int getNetworkTimeLoss()
    {
        return (int)getNumber(5, 2);
    }

    public int getL900Errors()
    {
        return (int)getNumber(8, 2);
    }

    public int getAntennaType()
    {
        return (int)getNumber(7, 1);
    }

    public int getBatteryStatus()
    {
        return (int)getNumber(10, 2);
    }

    public int getMagSwipe()
    {
        return (int)getNumber(12, 1);
    }

    public int getFirmwareVersion()
    {
        return (int)getNumber(13, 16);
    }

    public int getDeviceType()
    {
        return (int)getNumber(29, 5);
    }

    public long getDeviceId()
    {
        return getNumber(34, 34);
    }

    public int getReserved_1()
    {
        return (int)getNumber(68, 1);
    }

    public int getReverseFlow_35Day()
    {
        return (int)getNumber(69, 2);
    }

    public int getReverseFlow_24Hour()
    {
        return (int)getNumber(71, 2);
    }

    public int getLeakState()
    {
        return (int)getNumber(73, 2);
    }

    public int getBatteryLife()
    {
        return (int)getNumber(75, 2);
    }

    public int getEmptyPipe()
    {
        return (int)getNumber(77, 2);
    }

    public int getExcessiveFlow()
    {
        return (int)getNumber(79, 2);
    }

    public int getTamper()
    {
        return (int)getNumber(81, 1);
    }

    public int getReserved_2()
    {
        return (int)getNumber(82, 1);
    }

    public int getAttachedDeviceAlarmMask()
    {
        return (int)getNumber(84, 4);
    }

    @Override
    public String toString()
    {
        final StringBuilder ret = new StringBuilder("[PDU: BASIC/CONFIG: packetId=");
        ret.append(getPacketId()).append(" NetworkTimeLoss=").append(getNetworkTimeLoss())
                .append(" L900Errors=").append(getL900Errors())
                .append(" AntennaType=").append(getAntennaType())
                .append(" BatteryStatus=").append(getBatteryStatus())
                .append(" MagSwipe=").append(getMagSwipe())
                .append(" FirmwareVersion=").append(getFirmwareVersion())
                .append(" DeviceType=").append(getDeviceType())
                .append(" DeviceId=").append(getDeviceId())
                .append(" reserved_1=").append(getReserved_1())
                .append(" reverseFlow_35Day=").append(getReverseFlow_35Day())
                .append(" reverseFlow_24Hour=").append(getReverseFlow_24Hour())
                .append(" leakState=").append(getLeakState())
                .append(" batteryLife=").append(getBatteryLife())
                .append(" emptyPipe=").append(getEmptyPipe())
                .append(" excessiveFlow=").append(getExcessiveFlow())
                .append(" tamper=").append(getTamper())
                .append(" reserved_2=").append(getReserved_2())
                .append(" LeakState=").append(getLeakState())
                .append(" AttachedDeviceAlarmMask=").append(getAttachedDeviceAlarmMask())
                .append(" length=").append(lengthInBytes()).append(" bytes]");
        return ret.toString();
    }
}
