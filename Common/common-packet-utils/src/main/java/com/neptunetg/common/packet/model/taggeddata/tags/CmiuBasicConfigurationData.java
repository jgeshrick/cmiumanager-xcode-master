/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Created by WJD1 on 10/06/2015.
 * tag id 33
 * @deprecated Obsolete
 */
@Deprecated
public class CmiuBasicConfigurationData extends IntegerArrayData
{
    public static int SIZE_IN_BYTES = 11;

    public CmiuBasicConfigurationData(int initialCallInTime, int reportingIntervalHours, int callInWindow,
                                      int reportingWindowNumRetries, int quietTimeStartMinutes, int quietTimeEndMinutes,
                                      int numberAttachedDevices, int eventMaskForDevice)
    {
        super(TagId.CmiuConfiguration, TagDataType.ByteRecord, new byte[SIZE_IN_BYTES], SIZE_IN_BYTES);

        setShort(0, initialCallInTime);
        setByte(2, reportingIntervalHours);
        setByte(3, callInWindow);
        setByte(4, reportingWindowNumRetries);
        setShort(5, quietTimeStartMinutes);
        setShort(7, quietTimeEndMinutes);
        setByte(9, numberAttachedDevices);
        setByte(10, eventMaskForDevice);
    }

    public CmiuBasicConfigurationData(TagDataType dataType, byte[] data)
    {
        super(TagId.CmiuConfiguration, dataType, data, data.length);
    }

    public int getReportingStartTime()
    {
        return getDataBuffer().getShort(0);
    }

    public int getReportingIntervalHours()
    {
        return getDataBuffer().get(2);
    }

    public int getReportingWindow()
    {
        return getDataBuffer().get(3);
    }

    public int getReportingWindowNumRetries()
    {
        return getDataBuffer().get(4);
    }

    public int getQuietTimeStartMinutes()
    {
        return getDataBuffer().getShort(5);
    }

    public int getQuietTimeEndMinutes()
    {
        return getDataBuffer().getShort(7);
    }

    public int getNumberAttachedDevices()
    {
        return getDataBuffer().get(9);
    }

    public int getEventMaskForDevice()
    {
        return getDataBuffer().get(10);
    }

    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Cmiu Configuration Data [");
        ret.append(" Initial Call In Time =").append(getReportingStartTime());
        ret.append(", Call In Interval (hours) =").append(getReportingIntervalHours());
        ret.append(", Call In Window =").append(getReportingWindow());
        ret.append(", Reporting Window Number of Retries =").append(getReportingWindowNumRetries());
        ret.append(", Quiet Time Start in Minutes =").append(getQuietTimeStartMinutes());
        ret.append(", Quiet Time End in Minutes =").append(getQuietTimeEndMinutes());
        ret.append(", Number of Attached Devices =").append(getNumberAttachedDevices());
        ret.append(", Event Mask for Devices =").append(getEventMaskForDevice());
        ret.append("]");

        return ret.toString();
    }
}
