/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Created by WJD1 on 01/07/2015.
 * Class to represent a Image Meta Data tag
 * tag id 47
 */
public class ImageMetaData extends IntegerArrayData
{
    public static int SIZE_IN_BYTES = 9;

    public ImageMetaData(int imageType , int startAddress, int size)
    {
        super(TagId.ImageMetaData, TagDataType.ByteRecord, new byte[SIZE_IN_BYTES], SIZE_IN_BYTES);

        setByte(0, imageType);
        setInt(1, startAddress);
        setInt(5, size);
    }

    public ImageMetaData(ImageType imageType , int startAddress, int size)
    {
        this(imageType.getId(), startAddress, size);
    }

    public ImageMetaData(TagDataType dataType, byte[] data)
    {
        super(TagId.ImageMetaData, dataType, data, data.length);
    }

    @JsonProperty("image_type")
    public int getImageType()
    {
        return getDataBuffer().get(0);
    }

    @JsonProperty("image_start_address")
    public int getStartAddress()
    {
        return getDataBuffer().getInt(1);
    }

    @JsonProperty("image_size")
    public int getSize()
    {
        return getDataBuffer().getInt(5);
    }

    /**
     * formats the image meta data into a string
     * @return String with image meta data
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Image Meta Data [");
        ret.append("Image Type =").append(getImageType());
        ret.append(", Image Start Address =").append(getStartAddress());
        ret.append(", Image Size").append(getSize());
        ret.append("]");

        return ret.toString();
    }

    public enum ImageType
    {
        FIRMWARE_IMAGE(0),
        CONFIG_IMAGE(1),
        TELIT_MODULE_IMAGE(2),
        BLE_CONFIG_IMAGE(3),
        ARB_CONFIG_IMAGE(4),
        ENCRYPTION_IMAGE(5),
        BOOTLOADER_IMAGE(6);

        private final int id;
        private ImageType(int id)
        {
            this.id = id;
        }

        public int getId()
        {
            return this.id;
        }
    }
}
