/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.CmdId;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag 26
 */
public class CommandData extends IntegerData
{
    public CommandData(CmdId commandId)
    {
        super(TagId.Command, TagDataType.Byte, commandId.getId());
    }

    @JsonProperty("command_id")
    public int getCommandId()
    {
        return (int)getValue();
    }


    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Command [");
        ret.append("Command ID =").append(getCommandId());
        ret.append("]");

        return ret.toString();
    }
}
