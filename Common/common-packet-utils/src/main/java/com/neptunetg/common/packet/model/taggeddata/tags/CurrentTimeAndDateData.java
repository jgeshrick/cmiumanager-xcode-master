/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Tag 36
 */

public class CurrentTimeAndDateData extends IntegerData
{
    /**
     * Create a packet containing a date
     * @param value Seconds since 1/1/1970
     */
    public CurrentTimeAndDateData(long value)
    {
        super(TagId.CurrentTimeAndDate, TagDataType.UInt64, value);
    }

    /**
     * formats the tag data into a string
     * @return String with time and date data
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Current Time and Date [");
        ret.append("CurrentTimeAndDate=").append(this.getValue());
        ret.append("]");

        return ret.toString();
    }

    @JsonProperty("current_date_time")
    public UnixTimestamp getCurrentDateTime()
    {
        return new UnixTimestamp(getValue());
    }

    @JsonIgnore
    public byte[] getRawBytes()
    {
        ByteBuffer buffer = ByteBuffer.allocate(10).order(ByteOrder.LITTLE_ENDIAN);
        byte[] rawData = {
                (byte) this.getTagId().getId(),     //tag number
                this.getDataType().getId(),  //type
        };

        buffer.put(rawData);
        buffer.putLong(this.getValue());

        return buffer.array();
    }

    /**
     * A builder to build CurrentTimeAndData tag.
     */
    public static class Builder
    {
        public static CurrentTimeAndDateData build()
        {
            return new CurrentTimeAndDateData(
                    UnixTimestamp.getCurrentUnixTimestamp().getEpochSecond());
        }

    }



}
