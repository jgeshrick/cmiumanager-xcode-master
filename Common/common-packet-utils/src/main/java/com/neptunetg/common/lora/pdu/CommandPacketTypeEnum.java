/*
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 */

package com.neptunetg.common.lora.pdu;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * L900 command packet type enum
 */
public enum CommandPacketTypeEnum
{
    Reset(0),
    TimeOffset(1),
    Deactivate(2);

    private static final Map<Integer, CommandPacketTypeEnum> LOOKUP_TABLE = new HashMap<>();

    static
    {
        for(CommandPacketTypeEnum commandPacketType : EnumSet.allOf(CommandPacketTypeEnum.class))
        {
            LOOKUP_TABLE.put(commandPacketType.numericValue(), commandPacketType);
        }
    }

    private int commandIdVal;

    CommandPacketTypeEnum(int commandIdVal)
    {
        this.commandIdVal = commandIdVal;
    }

    public static CommandPacketTypeEnum of(byte commandIdVal)
    {
        for(CommandPacketTypeEnum commandPacketType : CommandPacketTypeEnum.values())
        {
            if(commandPacketType.numericValue() == commandIdVal)
            {
                return commandPacketType;
            }
        }

        return null;
    }

    public int numericValue()
    {
        return this.commandIdVal;
    }

    public static CommandPacketTypeEnum forId(int commandId)
    {
        return LOOKUP_TABLE.get(commandId);
    }
}

