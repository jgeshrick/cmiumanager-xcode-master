/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.substructures;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Represents a R900 Reading
 */
public class R900Reading
{
    public static int SIZE_IN_BYTES = 4;
    private ByteBuffer myBuffer;

    /**
     * Constructor for R900 reading object
     * @param reading r900 reading
     */
    public R900Reading(int reading)
    {
        myBuffer = ByteBuffer.allocate(SIZE_IN_BYTES).order(ByteOrder.LITTLE_ENDIAN);
        myBuffer.putInt(reading);
    }

    @JsonProperty("reading")
    public int getReading()
    {
        int reading = myBuffer.get(3) & 0xFF;
        reading += ((myBuffer.get(2) & 0xFF) << 8);
        reading += ((myBuffer.get(1) & 0xFF) << 16);
        reading += ((myBuffer.get(0) & 0xE0) << 19);

        return reading;
    }

    @JsonProperty("peak_backflow")
    public int getPeakBackflow()
    {
        return (myBuffer.get(0) & 0x0C) >> 2;
    }


   @JsonProperty("leak_detection")
    public int getLeakDetection()
    {
        return (myBuffer.get(0) & 0x03);
    }
}
