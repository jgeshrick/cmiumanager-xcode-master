/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.packet.model.taggeddata.packets;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Packet type byte in a tagged packet as defined in ETI48-00
 */
public class TaggedDataPacketType
{
    private static final Map<Integer, TaggedDataPacketType> KNOWN_PACKET_TYPES = new HashMap<>();

    public static final TaggedDataPacketType DetailedConfigurationStatus = new TaggedDataPacketType(0x00, "Detailed Configuration Status", "Published by the CMIU or transmitted by BLE, for the Host, contains current detailed CMIU configuration and status information.", true);
    public static final TaggedDataPacketType IntervalData = new TaggedDataPacketType(0x01, "Interval Data", "Published by CMIU or BLE for host; contains data information", true);
    public static final TaggedDataPacketType BasicConfigurationStatus = new TaggedDataPacketType(0x02, "Basic Configuration Status", "Published by CMIU or BLE for host; contains current basic CMIU configuration and status information", true);
    public static final TaggedDataPacketType Event = new TaggedDataPacketType(0x03, "Event", "Unscheduled packet published by CMIU to broker upon occurrence of an event", true);
    public static final TaggedDataPacketType TimeOfDay = new TaggedDataPacketType(0x04, "Time of Day", "Published by Broker or transmitted by BLE, for CMIU, contains the current date and time", true);
    public static final TaggedDataPacketType CommandConfiguration = new TaggedDataPacketType(0x05, "Command Configuration", "Published by Broker or transmitted by BLE, for CMIU, contains changes in CMIU configuration or commands to CMIU", true);
    public static final TaggedDataPacketType CommandConfigurationResponse = new TaggedDataPacketType(0x06, "Command Configuration Response", "Published by the CMIU in response to a Command Configuration Packet", true);
    public static final TaggedDataPacketType R900MiuReadings = new TaggedDataPacketType(0x07, "R900 MIU Readings", "This packet is constructed by the MDCE from multiple readings of an R900 MIU", true);

    private final byte id;
    private final String shortDescription;
    private final String longDescription;
    private final boolean known;

    private TaggedDataPacketType(int packetTypeId, String shortDescription, String longDescription, boolean known)
    {
        this.id = (byte)packetTypeId;
        this.shortDescription = shortDescription;
        this.longDescription = longDescription;
        this.known = known;
        if (known) {
            KNOWN_PACKET_TYPES.put(Integer.valueOf(packetTypeId), this);
        }
    }

    @JsonProperty("packet_type_id")
    public byte getId()
    {
        return id;
    }

    @JsonProperty("packet_type_name")
    public String getShortDescription()
    {
        return shortDescription;
    }

    @JsonIgnore
    public String getLongDescription()
    {
        return longDescription;
    }

    @JsonIgnore
    public boolean isKnown()
    {
        return known;
    }

    /**
     * Get TaggedDataPacketType enum from packet id
     * @param packetTypeId the packet id
     * @return the packet type enum
     */
    public static TaggedDataPacketType getPacketType(byte packetTypeId)
    {

        final int packetTypeIdInt = packetTypeId & 0xff;
        final TaggedDataPacketType ret = KNOWN_PACKET_TYPES.get(Integer.valueOf(packetTypeIdInt));

        if (ret == null)
        {
            return new TaggedDataPacketType(packetTypeId, "Unknown " + packetTypeIdInt, "Unknown packet type " + packetTypeIdInt, false);
        }
        else
        {
            return ret;
        }
    }

    @Override
    public String toString()
    {
        return String.format("%d 0x%02x %s", this.id, this.id, this.shortDescription);
    }
}
