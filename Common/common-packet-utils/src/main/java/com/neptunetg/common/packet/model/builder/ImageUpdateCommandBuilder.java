/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.builder;

import com.neptunetg.common.packet.model.taggeddata.tags.ImageData;
import com.neptunetg.common.packet.model.taggeddata.tags.ImageMetaData;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.tags.TimeAndDateData;
import com.neptunetg.common.packet.model.taggeddata.types.ImageVersionInfoData;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Builder to create image update command packet
 */
public class ImageUpdateCommandBuilder
{
    //image address, mqtt broker address, fallback host address
    private String fileName;

    private ImageMetaData.ImageType imageType;

    //for image update
    private int startAddress;
    private int imageSize;
    private int revisionMajor;
    private int revisionMinor;
    private int revisionDate;
    private int revisionBuildNum;

    public ImageUpdateCommandBuilder(ImageMetaData.ImageType imageType)
    {
        this.imageType = imageType;
    }


    public ImageUpdateCommandBuilder withFilename(String filename)
    {
        this.fileName = filename;
        return this;
    }

    public ImageUpdateCommandBuilder withMetaData(int startAddress, int imageSize)
    {
        this.startAddress = startAddress;
        this.imageSize = imageSize;
        return this;
    }

    public ImageUpdateCommandBuilder withRevision(int revisionMajor, int revisionMinor, int revisionDate, int revisionBuildNum)
    {
        this.revisionMajor = revisionMajor;
        this.revisionMinor = revisionMinor;
        this.revisionDate = revisionDate;
        this.revisionBuildNum = revisionBuildNum;

        return this;
    }

    public List<TaggedData> build()
    {
        List<TaggedData> imageUpdateCommandTaggedDataList = new ArrayList<>();
        imageUpdateCommandTaggedDataList.add(new ImageData(fileName));
        imageUpdateCommandTaggedDataList.add(new ImageMetaData(imageType, startAddress, imageSize));

        TagId imageRevisionTagId = null;
        switch (imageType)
        {
            case FIRMWARE_IMAGE:
                imageRevisionTagId = TagId.FirmwareRevision;
                break;

            case CONFIG_IMAGE:
                imageRevisionTagId = TagId.ConfigRevision;
                break;

            case TELIT_MODULE_IMAGE:
                imageRevisionTagId = TagId.FirmwareRevision;    //fixme!! is there a telit module image??
                break;

            case BLE_CONFIG_IMAGE:
                imageRevisionTagId = TagId.BleConfigRevision;
                break;

            case ARB_CONFIG_IMAGE:
                imageRevisionTagId = TagId.ArbConfigRevision;
                break;

            case ENCRYPTION_IMAGE:
                imageRevisionTagId = TagId.EncryptionConfigRevision;
                break;

            case BOOTLOADER_IMAGE:
                imageRevisionTagId = TagId.BootloaderVersion;
                break;
        }

        if (imageRevisionTagId != null)
        {
            imageUpdateCommandTaggedDataList.add(new ImageVersionInfoData(imageRevisionTagId, revisionMajor, revisionMinor, revisionDate, revisionBuildNum));
        }

        return imageUpdateCommandTaggedDataList;
    }

}
