/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.builder;

import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility for building TaggedPacket
 */
public class PacketBuilder<T extends TaggedPacket>
{
    private final List<TaggedData> tagList = new ArrayList<>();
    private final List<TaggedData> secureTagList = new ArrayList<>();
    private final TaggedDataPacketType packetType;

    public PacketBuilder(TaggedDataPacketType packetType)
    {
        this.packetType = packetType;
    }

    public PacketBuilder appendTagData(TaggedData taggedData)
    {
        this.tagList.add(taggedData);
        return this;
    }

    public PacketBuilder appendSecureTagData(TaggedData taggedData)
    {
        this.secureTagList.add(taggedData);
        return this;
    }

    public TaggedPacket buildTaggedPacket()
    {

        return new TaggedPacket(this.packetType, this.tagList, this.secureTagList);
    }


}
