/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2017 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.CharArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag 21
 * SIM Card ID
 */

public class SimCardIdData extends CharArrayData
{
    public SimCardIdData(TagDataType dataType, byte[] data)
    {
        super(TagId.SimCardId, dataType, data);
    }

    /**
     * returns the ICCID as a string
     */
    @JsonProperty("string_value")
    @Override
    public String getAsString()
    {
        return super.getAsString().trim();
    }

    /**
     * returns a summary of the tag as a string
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": SIM card ID [");
        ret.append("ICCID=").append(super.toString());
        ret.append("]");

        return ret.toString();
    }
}
