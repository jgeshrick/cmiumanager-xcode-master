
/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;

import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import static com.neptunetg.common.util.HexUtils.parseHexToByteArray;

/**
 * This is a convenience class for invoking the parser from non-Java environments
 * e.g. Sybase TSQL stored procedures.  All methods are static and inputs and outputs
 * are Java primitives.
 */
public class ParserUtil
{

    /**
     * Convert string data stored as UTF8 hex to a string
     * @param dataHex Hex UTF8 sequence
     * @return String
     */
    public static String convertUtf8HexToString(String dataHex)
    {
        final byte[] data = parseHexToByteArray(dataHex);
        return new String(data, StandardCharsets.UTF_8);
    }

    /**
     * Convert a binary packet to JSON
     * @param packetDataHex Hex string of packet binary data
     * @return JSON string of packet contents
     */
    public static String parsePacketToJson(String packetDataHex)
    {
        final byte[] data = parseHexToByteArray(packetDataHex);
        final TaggedPacket packet = PacketParser.parseTaggedPacket(data);
        return packet.toJson();
    }

    /**
     * Convert a binary packet to JSON
     * @param packetDataHex Hex string of packet binary data
     * @return JSON string of packet contents, including whitespace for readability
     */
    public static String parsePacketToJsonPretty(String packetDataHex)
    {
        final byte[] data = parseHexToByteArray(packetDataHex);
        final TaggedPacket packet = PacketParser.parseTaggedPacket(data);
        return packet.toJsonPretty();
    }

    /**
     * Convert a tag inside a binary packet to JSON
     * @param packetDataHex Hex string of packet binary data
     * @param desiredTagId Tag ID of the tag to be converted to JSON
     * @return JSON string of tag, or null if not found
     */
    public static String parseTagInPacketToJson(String packetDataHex, int desiredTagId)
    {
        final byte[] data = parseHexToByteArray(packetDataHex);
        final TaggedPacket packet = PacketParser.parseTaggedPacket(data);
        final TagId lookupTagId = TagId.forByte(desiredTagId);
        return packet.tagToJson(lookupTagId);
    }

    /**
     * Convert a tag inside a binary packet to JSON
     * @param packetDataHex Hex string of packet binary data
     * @param desiredTagId Tag ID of the tag to be converted to JSON
     * @return JSON string of tag, formatted for readability, or null if not found
     */
    public static String parseTagInPacketToJsonPretty(String packetDataHex, int desiredTagId)
    {
        final byte[] data = parseHexToByteArray(packetDataHex);
        final TaggedPacket packet = PacketParser.parseTaggedPacket(data);
        final TagId lookupTagId = TagId.forByte(desiredTagId);
        return packet.tagToJsonPretty(lookupTagId);
    }

    /**
     * Convert a binary tag sequence to JSON.  A binary tag sequence is like a packet but without
     * the initial packet type ID.
     * @param tagSequenceDataHex Hex data of tag sequence binary data
     * @return JSON representation of tag with required ID, or null if not found
     */
    public static String parseTagSequenceToJson(String tagSequenceDataHex)
    {
        final byte[] data = parseHexToByteArray(tagSequenceDataHex);
        final TagSequenceParser parser = new TagSequenceParser();
        final TagSequence tagSequence = parser.parseTagSequence(data);
        return tagSequence.toJson();
    }

    /**
     * Convert a binary tag sequence to JSON.  A binary tag sequence is like a packet but without
     * the initial packet type ID.
     * @param tagSequenceDataHex Hex data of tag sequence binary data
     * @return JSON representation of tag with required ID, pretty printed or null if not found
     */
    public static String parseTagSequenceToJsonPretty(String tagSequenceDataHex)
    {
        final byte[] data = parseHexToByteArray(tagSequenceDataHex);
        final TagSequenceParser parser = new TagSequenceParser();
        final TagSequence tagSequence = parser.parseTagSequence(data);
        return tagSequence.toJsonPretty();
    }

    /**
     * Convert a tag in a binary tag sequence to JSON.  A binary tag sequence is like a packet but without
     * the initial packet type ID.
     * @param tagSequenceDataHex Hex data of tag sequence binary data
     * @param desiredTagId Tag ID of the tag to be converted to JSON
     * @return JSON representation of tag with required ID, or null if not found
     */
    public static String parseTagToJson(String tagSequenceDataHex, int desiredTagId)
    {
        final byte[] data = parseHexToByteArray(tagSequenceDataHex);
        final TagSequenceParser parser = new TagSequenceParser();
        final TagSequence tagSequence = parser.parseTagSequence(data);
        final TagId lookupTagId = TagId.forByte(desiredTagId);
        return tagSequence.tagToJson(lookupTagId);
    }

    /**
     * Convert a tag in a binary tag sequence to JSON.  A binary tag sequence is like a packet but without
     * the initial packet type ID.
     * @param tagSequenceDataHex Hex data of tag sequence binary data
     * @param desiredTagId Tag ID of the tag to be converted to JSON
     * @return JSON representation of tag with required ID, formatted for readability, or null if not found
     */
    public static String parseTagToJsonPretty(String tagSequenceDataHex, int desiredTagId)
    {
        final byte[] data = parseHexToByteArray(tagSequenceDataHex);
        final TagSequenceParser parser = new TagSequenceParser();
        final TagSequence tagSequence = parser.parseTagSequence(data);
        final TagId lookupTagId = TagId.forByte(desiredTagId);
        return tagSequence.tagToJson(lookupTagId);
    }

    /**
     * Get the version number of this library
     * @return Version number
     */
    public static String getLibraryVersionNumber()
    {
        final Properties versionProperties = new Properties();
        try
        {
            versionProperties.load(ParserUtil.class.getResourceAsStream("/packet-utils-version.properties"));
            return versionProperties.getProperty("library.version");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "(version could not be retrieved due to an error: " + e.getMessage() + ")";
        }
    }

    /**
     * Get information about the JVM that is running.
     *
     * All the system properties are returned - see list here:
     * http://docs.oracle.com/javase/8/docs/api/java/lang/System.html#getProperties--
     * @return JSON representation of JVM details
     */
    public static String getJvmInfo()
    {
        final Map<String, String> props = new TreeMap<>();
        final Properties systemProperties = System.getProperties();
        final Enumeration<?> systemPropertyNames = systemProperties.propertyNames();
        while (systemPropertyNames.hasMoreElements()) {
            final String propName = (String)systemPropertyNames.nextElement();
            final String propValue = systemProperties.getProperty(propName);
            props.put(propName, propValue);
        }
        final ObjectMapper jackson = new ObjectMapper();
        try
        {
            return jackson.writerWithDefaultPrettyPrinter().writeValueAsString(props);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "(system properties could not be retrieved due to an error: " + e.getMessage() + ")";
        }
    }

}
