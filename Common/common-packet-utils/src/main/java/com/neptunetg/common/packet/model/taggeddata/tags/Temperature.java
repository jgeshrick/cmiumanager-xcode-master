/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag 63.
 * CMIU temperature at end of last transmission attempt
 Signed 8-bit value in degrees Celsius

 */
public class Temperature extends IntegerData
{

    public Temperature(TagDataType dataType, long data)
    {
        super(TagId.Temperature, dataType, data);
    }

    @JsonProperty("degrees_c")
    public int getDegreesC()
    {
        return (byte)getValue(); //sign-extend the uint8 value as it is actually signed in this tag
    }

    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Temperature [");
        ret.append("Degrees C =").append(getDegreesC());
        ret.append("]");

        return ret.toString();
    }
}
