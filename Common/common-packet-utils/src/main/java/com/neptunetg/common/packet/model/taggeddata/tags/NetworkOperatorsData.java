/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.CharArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag 3
 *
 * A char array data tag class specifically for
 * the Network Operators Data. This Data can be parsed.
 *
 * {@code
 * " +COPS: [list of supported (<stat>,long alphanumeric <oper>,short alphanumeric <oper>,numeric <oper>,< AcT>)]
 * [,,(list of supported <mode>s),(list of supported <format>)]
 * where:
 * <stat> - operator availability
 * 0 - unknown
 * 1 - available
 * 2 - current
 * 3 - forbidden
 * <AcT> access technology selected:
 * 0 - GSM
 * 1 - GSM Compact
 * 2 - UTRAN
 * Note: once the command done with network scan, this command may require some seconds before the output is given. 46:46"
 * }
 */

public class NetworkOperatorsData extends CharArrayData
{
    private final String data;

    public NetworkOperatorsData(TagDataType dataType, byte[] data)
    {
        super(TagId.NetworkOperators, dataType, data);
        this.data = new String(data);
    }

    /**
     * returns the network operators response string
     */
    @JsonProperty("network_operators_value")
    public String getNetworkOperatorResponseString()
    {
        return data;
    }


    /**
     * returns a summary of the tag as a string
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Network Operators [");
        ret.append("Network Operators Response String=").append(super.toString());
        ret.append("]");

        return ret.toString();
    }
}
