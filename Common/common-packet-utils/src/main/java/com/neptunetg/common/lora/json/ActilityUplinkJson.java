/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.lora.parser.LoraPacketParser;
import com.neptunetg.common.lora.pdu.L900Packet;
import com.neptunetg.common.lora.pdu.PacketId;
import com.neptunetg.common.util.HexUtils;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * A class to represent the part of a Actility message we want
 * Only mapping values that are used
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ActilityUplinkJson
{
    private final static LoraPacketParser packetParser = new LoraPacketParser();

    /**
     * 64-bit Extended Unique Identifier or Node ID
     */
    @JsonProperty("DevEUI")
    private String eui;

    /**
     * Customer payload data.  11 or 53 bytes.
     */
    @JsonProperty("payload_hex")
    private String pdu;

    /**
     * PDU as byte sequence
     */
    private byte[] pduBytes;

    /**
     * Actility LRR ID that processed the message, similar to a Senet gateway
     */
    @JsonProperty("Lrrid")
    private String gw;

    /**
     * Relative Signal Strength Indicator at LRR (RF gateway)
     */
    @JsonProperty("LrrRSSI")
    private float rssi;

    /**
     * Signal to Noise Ratio
     */
    @JsonProperty("LrrSNR")
    private float snr;

    //Todo - add data rate if it becomes available in the JSON

    //Todo - add transmit power if it becomes available in the JSON

    //Todo - add the transmit frequency if it becomes available in the JSON

    /**
     * The uplink counter for this packet
     */
    @JsonProperty("FCntUp")
    private int seqNo;

    /**
     * The LoRa port the packet was received on
     */
    @JsonProperty("FPort")
    private int port;

    /**
     * The time the LRR received the packet
     */
    @JsonProperty("Time")
    private String txTime;

    /**
     * Customer ID, only required for token authentication
     */
    @JsonProperty("CustomerID")
    private String customerId;

    public String getEui()
    {
        return eui;
    }

    public void setEui(String eui)
    {
        this.eui = eui;
    }

    public String getPdu()
    {
        return pdu;
    }

    public void setPdu(String pdu)
    {
        this.pdu = pdu;
        this.pduBytes = HexUtils.parseHexToByteArray(pdu);
    }

    public L900Packet parsePdu()
    {
        return packetParser.parsePacket(this.pduBytes);
    }

    @JsonIgnore
    public PacketId getPacketIdFromPdu()
    {
        return PacketId.fromFirstDataByte(this.pduBytes);
    }

    @JsonIgnore
    public byte[] getPduBytes()
    {
        return pduBytes.clone();
    }

    public String getGw()
    {
        return gw;
    }

    public void setGw(String gw)
    {
        this.gw = gw;
    }

    public int getRssi()
    {
        return (int) rssi;
    }

    public void setRssi(float rssi)
    {
        this.rssi = rssi;
    }

    public float getSnr()
    {
        return snr;
    }

    public void setSnr(float snr)
    {
        this.snr = snr;
    }

    public int getSeqNo()
    {
        return seqNo;
    }

    public void setSeqNo(int seqNo)
    {
        this.seqNo = seqNo;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public String getTxTime()
    {
        return txTime;
    }

    public Instant getTxTimeInstant()
    {
        try
        {
            return ZonedDateTime.parse(txTime, DateTimeFormatter.ISO_DATE_TIME).toInstant();
        }
        catch (DateTimeParseException e)
        {
            return null;
        }
    }

    public void setTxTime(String txTime)
    {
        this.txTime = txTime;
    }

    public String getCustomerId()
    {
        return customerId;
    }

    public void setCustomerId(String customerId)
    {
        this.customerId = customerId;
    }
}
