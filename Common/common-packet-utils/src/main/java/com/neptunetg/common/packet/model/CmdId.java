/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by WJD1 on 02/07/2015.
 * A class to represent the different command enums that can be used.
 * Used by tag 32
 */
public final class CmdId
{
    private static final Map<Integer, CmdId> LOOKUP_TABLE = new HashMap<>();

    public static final CmdId UpdateImage = new CmdId(0 , "Update Image", "Instructs the CMIU to perform an image update");
    public static final CmdId RebootCmiu = new CmdId(1 , "Reboot CMIU", "Instructs the CMIU to reboot");
    public static final CmdId MapSwipeEmulation = new CmdId(2 , "Mag Swipe Emulation", "The CMIU act as if a mag swipe has just occured");

    public static final CmdId EraseDatalog = new CmdId(6 , "Erase Datalog", "The CMIU should erase the water usage data log");

    public static final CmdId SetRecordingReportingInterval = new CmdId(10, "Set the Recording and Reporting Intervals", "Use this command to set the recording and reporting intervals the CMIU uses");
    public static final CmdId PublishRequestedPacket = new CmdId(11, "Publish Requested Packet", "The CMIU should publish the indicated packet");

    public static final CmdId ModemFota = new CmdId(32, "Perform modem firmware update", "The CMIU should update the Modems firmware");

    private final int id;

    private final String cmdName;

    private final String description;


    /**
     * @param id Represents a command ID (i.e. a byte value which signifies the
     * meaning of the command tag.
     */
    private CmdId(int id, String cmdName, String description)
    {
        this.id = id;
        this.cmdName = cmdName;
        this.description = description;
        LOOKUP_TABLE.put(id, this);
    }

    @JsonProperty("id")
    public int getId()
    {
        return id;
    }

    @JsonProperty("cmd_name")
    public String getTagName()
    {
        return cmdName;
    }

    @JsonIgnore
    public String getDescription()
    {
        return description;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }

        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        CmdId cmdId = (CmdId) o;

        return id == cmdId.id;
    }

    @Override
    public int hashCode()
    {
        return id;
    }

    public static CmdId fromId(int id)
    {
        return LOOKUP_TABLE.get(id);
    }
}
