/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.CharArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag 46
 * Holds a URL to an image file on a server
 */
public class ImageData extends CharArrayData
{
    private final String data;

    public ImageData(TagDataType dataType, byte[] data)
    {
        super(TagId.Image, dataType, data);
        this.data = new String(data);
    }

    public ImageData(String fileName)
    {
        this(TagDataType.ExtendedCharArray, fileName.getBytes());
    }

    @JsonProperty("image_filename")
    public String getFileName()
    {
        return data;
    }


    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Image [");
        ret.append("Filename String=").append(getFileName());
        ret.append("]");

        return ret.toString();
    }
}
