/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.pdu;

import java.time.Duration;

/**
 * Created by WJD1 on 07/07/2016.
 * A class to represent a time offset command packet
 */
public class TimeOffsetCommandPacket extends CommandPacket
{
    public TimeOffsetCommandPacket(Duration timeOffset, int updateIdentifier)
    {
        super(CommandPacketTypeEnum.TimeOffset);

        setNumber(13, timeOffset.toMillis()/1000, 64);
        setNumber(64 + 13, updateIdentifier, 8);
    }

    public TimeOffsetCommandPacket(byte[] packetBytes)
    {
        super(packetBytes);
    }

    public Duration getTimeOffset()
    {
        return Duration.ofMillis(getNumber(13, 64)*1000);
    }

    public int getUpdateIdentifier()
    {
        return (int) getNumber(64 + 13, 8);
    }


    @Override
    public String toString()
    {
        final StringBuilder ret = new StringBuilder("[PDU: COMMAND (TIME OFFSET): packetId=");
        ret.append(getPacketId()).append(" commandId=").append(getCommandId())
                .append(" offset=").append(getTimeOffset())
                .append(" updateId=").append(getUpdateIdentifier())
                .append(" length=").append(lengthInBytes()).append(" bytes]");
        return ret.toString();
    }
}
