/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.packets;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.packet.model.taggeddata.tags.SecureBlockArrayData;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Represents a tagged packet
 * Created by ABH1 on 05/02/2015.
 */
public class TaggedPacket implements Iterable<TaggedData>
{
    private final TaggedDataPacketType packetId;

    private final TagSequence tagList;

    /**
     * @param packetId The ID of the packet
     * @param tagList List of tags for this packet.  EOF will be added if not present
     */
    public TaggedPacket(TaggedDataPacketType packetId, List<TaggedData> tagList)
    {
        this.packetId = packetId;
        final List<TaggedData> listWithEof = new ArrayList<>(tagList);
        if (!TagId.Eof.equals(tagList.get(tagList.size() - 1).getTagId()))
        {
            listWithEof.add(new TaggedData(TagId.Eof));
        }
        this.tagList = new TagSequence(listWithEof);
    }


    /**
     * Build a packet that contains tagList, then a secure block, then EOF
     * @param packetId The ID of the packet
     * @param tagList List of tags for this packet.  No EOF.
     * @param secureTagList List of secure tags
     */
    public TaggedPacket(TaggedDataPacketType packetId, List<TaggedData> tagList, List<TaggedData> secureTagList)
    {
        this.packetId = packetId;

        final List<TaggedData> newTags = new ArrayList<>(tagList);

        if (secureTagList != null && !secureTagList.isEmpty())
        {
            final SecureBlockArrayData secureBlockArrayData = SecureBlockArrayData.build(secureTagList);
            newTags.add(secureBlockArrayData);
        }

        newTags.add(new TaggedData(TagId.Eof));

        this.tagList = new TagSequence(newTags);
    }

    /**
     * @param packetId The ID of the packet
     * @param tagSequence List of tags for this packet.  Used without modification (no EOF added)
     */
    public TaggedPacket(TaggedDataPacketType packetId, TagSequence tagSequence)
    {
        this.packetId = packetId;
        this.tagList = tagSequence;
    }

    @JsonIgnore
    public int getPacketId()
    {
        return packetId.getId() & 0xff;
    }

    @JsonProperty("meta")
    public TaggedDataPacketType getPacketType()
    {
        return packetId;
    }

    public Iterable<TaggedData> getTags()
    {
        return tagList;
    }

    /**
     * This returns the number of tags in the packet
     * @return  tag count
     */
    public int tagCount()
    {
        return tagList.tagCount();
    }

    /**
     * Provides an iterator to allow the tags in the packet to be iterated
     * @return iterator of the tag list
     */
    @Override
    public Iterator<TaggedData> iterator()
    {
        return tagList.iterator();
    }

    /**
     * Serialise the taggedPacket into raw byte array.
     * @return tagged packet as raw byte
     */
    public byte[] toByteSequence()
    {
        return tagList.toByteSequence((byte) this.getPacketId());
    }

    /**
     * Convert entire packet to a JSON string.  It consists of a structure {"tags": [tag1, tag2...], "meta": {"packet_type_id": 0, "packet_type_name" : "XXX"}}
     * @return JSON string
     */
    public String toJson()
    {
        try
        {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        }
        catch (JsonProcessingException e)
        {
            //not expected
            throw new RuntimeException("Error converting tagged packet to JSON", e);
        }
    }

    /**
     * Convert entire packet to a pretty JSON string.  It consists of a structure {"tags": [tag1, tag2...], "meta": {"packet_type_id": 0, "packet_type_name" : "XXX"}}
     * with indentation whitespace and carriage returns
     * @return JSON string
     */
    public String toJsonPretty()
    {
        try
        {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        }
        catch (JsonProcessingException e)
        {
            //not expected
            throw new RuntimeException("Error converting tagged packet to JSON", e);
        }

    }

    /**
     * Find the first tag of the given type and return its JSON representation
     * @param taggedDataType Required type
     * @param <T> Required type
     * @return JSON representation of tag, or null if tag not found
     */
    public <T extends TaggedData> String tagToJson(Class<T> taggedDataType)
    {
        return tagList.tagToJson(taggedDataType);
    }

    /**
     * Find the tag with the given ID and return its JSON representation
     *
     * @param requiredTagId  ID of tag to convert to JSON
     * @return JSON representation of tag, or null if tag not found
     */
    public String tagToJson(TagId requiredTagId)
    {
        return tagList.tagToJson(requiredTagId);
    }

    /**
     * Find the first tag of the given type and return its JSON representation with carriage returns and indents
     * @param taggedDataType Required type
     * @param <T> Required type
     * @return JSON representation of tag, or null if tag not found
     */
    public <T extends TaggedData> String tagToJsonPretty(Class<T> taggedDataType)
    {
        return tagList.tagToJson(taggedDataType);
    }

    /**
     * Find the tag with the given ID and return its JSON representation
     *
     * @param requiredTagId  ID of tag to convert to JSON
     * @return JSON representation of tag, or null if tag not found
     */
    public String tagToJsonPretty(TagId requiredTagId)
    {
        return tagList.tagToJsonPretty(requiredTagId);
    }

    /**
     * Extract and return the first tagged data type found
     * @param taggedDataType the tagged data type to be extracted from the tagged packet
     * @param <T> Tagged data and its subclass
     * @return The extracted tagged data, of type T
     */
    public <T extends TaggedData> T findTag(Class<T> taggedDataType)
    {
        return tagList.findTag(taggedDataType);
    }

    /**
     * Extract and return the first tag with the given tag ID and type
     * @param id The tag ID
     * @param taggedDataType the tagged data type to be extracted from the tagged packet
     * @param <T> Tagged data and its subclass
     * @return The extracted tagged data, of type T
     */
    public <T extends TaggedData> T findTag(TagId id, Class<T> taggedDataType)
    {
        return tagList.findTag(id, taggedDataType);
    }

}
