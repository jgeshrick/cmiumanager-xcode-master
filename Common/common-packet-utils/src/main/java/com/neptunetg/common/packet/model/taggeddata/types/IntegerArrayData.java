/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.types;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Represents an array of UInt8, UInt16 or UInt32 elements
 *
 * Tag data types 5-12 and 15-16
 */
public class IntegerArrayData extends TaggedDataWithType
{
    private final ByteBuffer data;
    private final int arrayLength;

    /**
     * Create an instance of a tag+data
     */
    public IntegerArrayData(TagId tag, TagDataType dataType, byte[] data, int arrayLength)
    {
        super(tag, dataType);
        this.data = ByteBuffer.wrap(data.clone());
        this.data.order(ByteOrder.LITTLE_ENDIAN).asReadOnlyBuffer();
        this.arrayLength = arrayLength;
    }

    @JsonProperty("meta")
    @Override
    public TaggedDataMetadata getMetadata()
    {
        return new TaggedDataMetadata(getTagId(), getDataType(), this.arrayLength);
    }

    @JsonIgnore
    public ByteBuffer getDataBuffer()
    {
        return data;
    }

    /**
     * Allows different values to be retrieved from a byte array where several
     * bytes indicate one number. Such as a 4 byte int.
     */
    public long getItem(int index)
    {
        switch (getDataType().getUnitSize())
        {
            case 1:
                return data.get(index) & 0xffL;
            case 2:
                return data.getShort(index * 2) & 0xffffL;
            case 4:
                return data.getInt(index * 4) & 0xffffffffffffL;
            case 8:
                return data.getLong(index * 8);
            default:
                throw new IllegalArgumentException("Unknown unit size " +
                                                getDataType().getUnitSize());
        }
    }


    @JsonIgnore
    protected final int getArrayLength()
    {
        return this.arrayLength;
    }

    protected final void setShort(int offset, int value) {
        if (value >= Short.MIN_VALUE && value <= Short.MAX_VALUE - Short.MIN_VALUE) { //allow signed and unsigned
            data.putShort(offset, (short)value);
        } else {
            throw new IllegalArgumentException("Cannot set short at offset " + offset + " to " + value + "; out of range.");
        }
    }


    protected final void setByte(int offset, int value) {
        if (value >= Byte.MIN_VALUE && value <= Byte.MAX_VALUE - Byte.MIN_VALUE) { //allow signed and unsigned
            data.put(offset, (byte) value);
        } else {
            throw new IllegalArgumentException("Cannot set byte at offset " + offset + " to " + value + "; out of range.");
        }
    }

    protected final void setInt(int offset, int value) {
        data.putInt(offset, value);
    }


    protected final void setLong(int offset, long value) {
        data.putLong(offset, value);
    }


    /**
     * Gets the data as an array of longs.  These are the unsigned array values for 8, 16 and 32 bit value
     * arrays, but as Java does not support UInt64 they are actually signed in case of 64 bit values.
     * @return array of longs
     */
    @JsonProperty("array")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public long[] getArrayEntries()
    {
        if (getDataType().isStruct())
        {
            return null;
        }
        else
        {
            final long[] ret = new long[arrayLength];
            for (int i = 0; i < arrayLength; i++)
            {
                ret[i] = getItem(i);
            }
            return ret;
        }
    }

    /**
     * Getter that returns a string summary of the tag
     * @return Summary text of tag
     */
    @Override
    public String getTagSummary()
    {
        return getTagId().getTagName() + " (" + (arrayLength) + " item " + getDataType().name() + ")";
    }

    /**
     * Convert to string
     * @return summary of tag
     */
    @Override
    public String toString()
    {
        return getTagSummary();
    }

    @Override
    public byte[] serialize()
    {
        byte[] tagIdAndType = super.serialize();
        final int arrayLengthLength = getDataType().getSizeBytesToFollow();

        final ByteBuffer ret = ByteBuffer.allocate(tagIdAndType.length + data.capacity() + arrayLengthLength)   //add array size
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(tagIdAndType);          //tag number, type

        switch (arrayLengthLength) {
            case 1:
                ret.put((byte) this.arrayLength); //array size
                break;
            case 2:
                ret.putShort((short) this.arrayLength); //array size
                break;
            default:
                throw new UnsupportedOperationException("Can't serialize tag with " + getDataType().getSizeBytesToFollow() + " bytes of length data");
        }
        ret.put(data);                  //array content
        return ret.array();
    }
}
