/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.parser;

import com.neptunetg.common.packet.model.CmdId;
import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.tags.*;
import com.neptunetg.common.packet.model.taggeddata.types.CharArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.ImageVersionInfoData;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerData;
import com.neptunetg.common.packet.model.taggeddata.types.R900FskReadingArray;
import com.neptunetg.common.packet.model.taggeddata.types.R900OokReadingArray;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedDataWithType;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

/**
 * Parses a byte array into a tag sequence
 */
public class TagSequenceParser
{

    /**
     * Parses raw tagged data into tagged data object
     */
    private static TaggedDataWithType parseTagData(TagId tagId, ByteBuffer byteReader)
    {
        final TagDataType dataType = TagDataType.forByte(byteReader.get());
        final int sizeBytes = dataType.getSizeBytesToFollow();

        if (sizeBytes == 0)
        {
            if (TagDataType.ImageVersionInfo.equals(dataType))
            {
                if (byteReader.remaining() < 10)
                {
                    throw new TagSequenceParseException("Insufficient data for ImageVersionInfo tag " + tagId, byteReader);
                }
                byte[] data = new byte[10];
                byteReader.get(data);
                return new ImageVersionInfoData(tagId, data);
            }
            //otherwise it's an int type
            final long value;
            switch (dataType)
            {
                case Byte:
                    if (byteReader.remaining() < 1)
                    {
                        throw new TagSequenceParseException("No data for Byte tag " + tagId, byteReader);
                    }
                    value = byteReader.get() & 0xff;
                    break;
                case UInt16:
                    if (byteReader.remaining() < 2)
                    {
                        throw new TagSequenceParseException("Insufficient data for UInt16 tag " + tagId, byteReader);
                    }
                    value = byteReader.getShort() & 0xffff;
                    break;
                case UInt32:
                    if (byteReader.remaining() < 4)
                    {
                        throw new TagSequenceParseException("Insufficient data for UInt32 tag " + tagId, byteReader);
                    }
                    value = byteReader.getInt() & 0xffffffff;
                    break;
                case UInt64:
                    if (byteReader.remaining() < 8)
                    {
                        throw new TagSequenceParseException("Insufficient data for UInt64 tag " + tagId, byteReader);
                    }
                    value = byteReader.getLong();
                    break;
                default:
                    throw new TagSequenceParseException("Unrecognized tag type " + dataType + " for tag " + tagId, byteReader);
            }

            //special wrapped int tags
            if (TagId.Command.equals(tagId))
            {
                return new CommandData(CmdId.fromId((int)value));
            } else if (TagId.BatteryVoltage.equals(tagId))
            {
                return new BatteryVoltage(dataType, value);
            } else if (TagId.Temperature.equals(tagId))
            {
                return new Temperature(dataType, value);
            } else
            {
                return new IntegerData(tagId, dataType, value);
            }

        } else
        {
            int itemSize;

            if (byteReader.remaining() < sizeBytes)
            {
                throw new TagSequenceParseException("Truncated at array size value - expected " + sizeBytes +
                        " bytes for array size for tag " + tagId, byteReader);
            }

            if (sizeBytes == 1)
            {
                itemSize = byteReader.get() & 0xff;
            }
            else if (sizeBytes == 2)
            {
                itemSize = byteReader.getShort() & 0xffff;
            }
            else
            {
                throw new IllegalArgumentException("Unexpected array length data: " + sizeBytes + " bytes long");
            }

            byte[] data = new byte[itemSize * dataType.getUnitSize()];

            if (byteReader.remaining() < data.length)
            {
                throw new TagSequenceParseException("Truncated in data of tag " + tagId + " - expected " +
                        data.length + " bytes but only " + byteReader.remaining() + " available", byteReader);
            }
            byteReader.get(data);

            if (TagId.CmiuPacketHeader.equals(tagId))
            {
                return new CmiuPacketHeaderData(dataType, data);
            }
            else if (TagId.DetailedCmiuConfiguration.equals(tagId))
            {
                return new DetailedCmiuConfigurationData(dataType, data);
            }
            else if (TagId.NetworkOperators.equals(tagId))
            {
                return new NetworkOperatorsData(dataType, data);
            }
            else if (TagId.NetworkPerformance.equals(tagId))
            {
                return new NetworkPerformanceData(dataType, data);
            }
            else if (TagId.RegistrationStatus.equals(tagId))
            {
                return new RegistrationStatusData(dataType, data);
            }
            else if (TagId.PinPukPukTwoRequestData.equals(tagId))
            {
                return new PinPukPukTwoRequestStatusData(dataType, data);
            }
            else if (TagId.CmiuHwDiagnostics.equals(tagId))
            {
                return new CmiuDiagnosticsData(dataType, data);
            }
            else if (TagId.ReportedDeviceConfiguration.equals(tagId))
            {
                return new ReportedDeviceConfigurationData(dataType, data);
            }
            else if (TagId.ConnectionTimingLog.equals(tagId))
            {
                return new ConnectionTimingLogData(dataType, data);
            }
            else if (TagId.CmiuStatus.equals(tagId))
            {
                return new CmiuStatusData(dataType, data);
            }
            else if (TagId.CmiuConfiguration.equals(tagId))
            {
                return new CmiuBasicConfigurationData(dataType, data);
            }
            else if (TagId.IntervalRecording.equals(tagId))
            {
                return new IntervalRecordingConfigurationData(dataType, data);
            }
            else if (TagId.CmiuInformation.equals(tagId))
            {
                return new CmiuInformationData(dataType, data);
            }
            else if (TagId.ImageMetaData.equals(tagId))
            {
                return new ImageMetaData(dataType, data);
            }
            else if (TagId.MQTTPacketHeader.equals(tagId))
            {
                return new MqttPacketHeaderData(dataType, data);
            }
            else if (TagId.RecordingandReportingInterval.equals(tagId))
            {
                return new RecordingAndReportingIntervalData(dataType, data);
            }
            else if (TagId.R900IntervalData.equals(tagId))
            {
                return new R900IntervalData(dataType, data);
            }
            else if (TagId.ModemSerialNumber.equals(tagId))
            {
                return new ModemSerialNumberData(dataType, data);
            }
            else if (TagId.SimImsi.equals(tagId))
            {
                return new SimImsiData(dataType, data);
            }
            else if (TagId.SimCardId.equals(tagId))
            {
                return new SimCardIdData(dataType, data);
            }
            else if (dataType == TagDataType.CharArray ||
                    dataType == TagDataType.ExtendedCharArray)
            {
                return new CharArrayData(tagId, dataType, data);
            }
            else if (dataType == TagDataType.ExtendedSecureBlockArray || dataType == TagDataType.SecureBlockArray)
            {
                return new SecureBlockArrayData(dataType, data, itemSize);
            }
            else if (dataType == TagDataType.R900OokReadingArray)
            {
                return new R900OokReadingArray(tagId, data, itemSize);
            }
            else if (dataType == TagDataType.R900FskReadingArray)
            {
                return new R900FskReadingArray(tagId, data, itemSize);
            }
            else
            {
                return new IntegerArrayData(tagId, dataType, data, itemSize);
            }
        }
    }

    public TagSequence parseTagSequence(byte[] rawData)
    {
        final ByteBuffer byteReader = ByteBuffer.wrap(rawData).asReadOnlyBuffer();
        byteReader.order(ByteOrder.LITTLE_ENDIAN);
        return parseTagSequence(byteReader);
    }

    public TagSequence parseTagSequence(ByteBuffer byteReader)
    {
        if (!ByteOrder.LITTLE_ENDIAN.equals(byteReader.order()))
        {
            throw new IllegalArgumentException("Only little-endian byte buffers can be parsed!");
        }
        final List<TaggedData> tagList = new ArrayList<>();

        int tagIdCalc = 0;

        while (byteReader.hasRemaining())
        {
            final byte tagIdValue = byteReader.get();

            TagId tagId = TagId.forByte(tagIdValue);
            if (tagId.equals(TagId.TagId256Extension))
            {
                tagIdCalc += 256;
            } else
            {
                tagIdCalc += tagIdValue;
                tagId = TagId.forByte(tagIdCalc);
                tagIdCalc = 0;
                if (tagId.hasType())
                {
                    if (!byteReader.hasRemaining())
                    {
                        throw new TagSequenceParseException("Unexpected end of tag sequence: Tag type missing for tag " + tagId, byteReader);
                    }
                    try
                    {
                        TaggedDataWithType dataItem = parseTagData(tagId, byteReader);
                        tagList.add(dataItem);
                    } catch (TagSequenceParseException e)
                    {
                        throw e;
                    } catch (Exception e)
                    {
                        throw new TagSequenceParseException("Unexpected exception occurred while parsing tag sequence", byteReader, e);
                    }

                } else //some other tag without data
                {
                    tagList.add(new TaggedData(tagId));
                }
            }
        }
        return new TagSequence(tagList);
    }
}
