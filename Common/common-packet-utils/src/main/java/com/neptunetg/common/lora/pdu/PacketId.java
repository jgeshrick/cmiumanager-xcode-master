/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.pdu;

import java.util.HashMap;
import java.util.Map;

/**
 * Packet IDs as defined in ETI 05-05
 */
public final class PacketId
{
    private static final Map<Integer, PacketId> LOOKUP_TABLE = new HashMap<>();

    public static final PacketId BASIC_CONFIG_PACKET = new PacketId(0, "Basic Config", "The 11 byte version of the configuration packet");
    public static final PacketId BASIC_READ_PACKET = new PacketId(1, "Basic Read", "The 11 byte version of the reading packet");
    public static final PacketId ALARM_PACKET = new PacketId(2, "Alarm Packet", "This packet indicates an alarm condition");
    public static final PacketId COMMAND_RESPONSE_PACKET = new PacketId(3, "Command Response", "This packet is sent in response to a command");
    public static final PacketId FULL_READ_PACKET = new PacketId(4, "Full Read", "The 53 byte version of the reading packet");
    public static final PacketId DETAILED_CONFIG_PACKET = new PacketId(5, "Detailed Config", "The 53 byte version of the configuration packet");
    public static final PacketId TIME_AND_DATE = new PacketId(6, "Time and Date", "A packet containing the time and date in the L900");
    public static final PacketId APP_COMMAND_PACKET = new PacketId(20, "App Command Packet", "A command packet with variable payload");

    private final int id;

    private final String packetName;

    private final String description;

    /**
     * @param id Represents a packet ID
     * (a 5 bit value which signifies the meaning of the data in the packet)
     */
    private PacketId(int id, String packetName, String description)
    {
        this.id = id;
        this.packetName = packetName;
        this.description = description;
        LOOKUP_TABLE.put(Integer.valueOf(id), this);
    }

    public int getId()
    {
        return id;
    }

    public String getPacketName()
    {
        return packetName;
    }

    public String getDescription()
    {
        return description;
    }

    public static PacketId forByte(int byteValue)
    {
        Integer key = Integer.valueOf(byteValue & 0xff);

        if (LOOKUP_TABLE.containsKey(key))
        {
            return LOOKUP_TABLE.get(key);
        }

        return new PacketId(byteValue, "Unknown packet 0x" + Integer.toHexString(byteValue), "Unknown packet");
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }

        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        PacketId packetId = (PacketId) o;

        return id == packetId.id;

    }

    @Override
    public int hashCode()
    {
        return id;
    }

    @Override
    public String toString()
    {
        return String.format("%d 0x%02x %s", this.id, this.id, this.packetName);
    }

    /**
     * Get the packet ID from the first byte of data.  The high 5 bits contain the
     * packet ID.
     * @param data The data array
     * @return Packet ID
     */
    public static PacketId fromFirstDataByte(byte[] data)
    {
        if (data == null || data.length == 0)
        {
            throw new IllegalArgumentException("null data block passed to PacketId.fromFirstDataByte");
        }
        return forByte((data[0] >> 3) & 0x1f);
    }
}
