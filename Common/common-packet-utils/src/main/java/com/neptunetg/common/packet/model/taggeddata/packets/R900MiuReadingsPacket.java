/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.packets;

import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.taggeddata.tags.MqttPacketHeaderData;
import com.neptunetg.common.packet.model.taggeddata.types.R900FskReadingArray;
import com.neptunetg.common.packet.model.taggeddata.types.R900OokReadingArray;

import java.time.Instant;
import java.util.Arrays;

/**
 * Packet type 7
 */
public class R900MiuReadingsPacket extends TaggedPacket
{
    private int packetCount;
    private Instant oldestPacketBuiltDate;
    private Instant newestPacketBuiltDate;

    public R900MiuReadingsPacket(MqttPacketHeaderData header, R900OokReadingArray ookReadings, R900FskReadingArray fskReadings)
    {
        super(TaggedDataPacketType.R900MiuReadings, Arrays.asList(header), Arrays.asList(ookReadings, fskReadings));

        UnixTimestamp ookDate = ookReadings.getOldestPacketBuiltDate();
        UnixTimestamp fskDate = fskReadings.getOldestPacketBuiltDate();
        this.oldestPacketBuiltDate = min(ookDate, fskDate);

        ookDate = ookReadings.getNewestPacketBuiltDate();
        fskDate = fskReadings.getNewestPacketBuiltDate();
        this.newestPacketBuiltDate = max(ookDate, fskDate);

        this.packetCount = ookReadings.getReadingCount() + fskReadings.getReadingCount();
    }

    private static Instant max(UnixTimestamp d1, UnixTimestamp d2)
    {
        if (d1 == null)
        {
            return d2 == null ? null : d2.asInstant();
        }
        else if (d2 == null)
        {
            return d1.asInstant();
        }
        else if (d1.isBefore(d2))
        {
            return d2.asInstant();
        }
        else
        {
            return d1.asInstant();
        }
    }

    private static Instant min(UnixTimestamp d1, UnixTimestamp d2)
    {
        if (d1 == null)
        {
            return d2 == null ? null : d2.asInstant();
        }
        else if (d2 == null)
        {
            return d1.asInstant();
        }
        else if (d1.isBefore(d2))
        {
            return d1.asInstant();
        }
        else
        {
            return d2.asInstant();
        }
    }


    public Instant getOldestPacketBuiltDate()
    {
        return oldestPacketBuiltDate;
    }

    public Instant getNewestPacketBuiltDate()
    {
        return newestPacketBuiltDate;
    }

    public int getPacketCount()
    {
        return packetCount;
    }
}
