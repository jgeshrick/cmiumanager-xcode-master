/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.common.packet.model;

/**
 * Created by WJD1 on 20/06/2016.
 * To represent the different types of packet instigator
 */
public enum PacketInstigatorId
{
    CVS(1),
    FFTS(2);

    private final int id;

    PacketInstigatorId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return this.id;
    }
}
