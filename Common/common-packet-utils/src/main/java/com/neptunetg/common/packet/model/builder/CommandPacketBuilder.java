/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.builder;

import com.neptunetg.common.packet.model.CmdId;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.*;
import com.neptunetg.common.packet.model.taggeddata.types.CharArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;

import java.util.List;

/**
 * Builder for constructing command packet
 */
public class CommandPacketBuilder
{
    private long executionTimeSecond;

    //image address, mqtt broker address, fallback host address
    private String fileName;

    //for image update
    private List<TaggedData> imageUpdateTaggedData;

    //for recording and reporting interval
    private int reportingStartMins;
    private int reportingIntervalHours;
    private int reportingRetries;
    private int reportingTransmitWindows;
    private int reportingQuietStart;
    private int reportingQuietEnd;
    private int recordingStartTime;
    private int recordingIntervalMins;
    private int numberOfDevices;
    private int eventMask;

    //for publish requested packet
    private TaggedDataPacketType packetType;

    //for modem FOTA update command packet
    private String fotaURL;
    private int fotaPort;
    private String fotaUsername;
    private String fotaPassword;
    private String fotaFilename;
    private String fotaNewImageVersionInfo;

    public CommandPacketBuilder fotaDetails(String fotaURL, int fotaPort, String fotaUsername, String fotaPassword,
                                            String fotaFilename, String fotaNewImageVersionInfo)
    {
        this.fotaURL = fotaURL;
        this.fotaPort = fotaPort;
        this.fotaUsername = fotaUsername;
        this.fotaPassword = fotaPassword;
        this.fotaFilename = fotaFilename;
        this.fotaNewImageVersionInfo = fotaNewImageVersionInfo;
        return this;
    }

    public CommandPacketBuilder executionTime(long timeToExecuteCommandSecond)
    {
        executionTimeSecond = timeToExecuteCommandSecond;
        return this;
    }

    public CommandPacketBuilder reportingInterval(int startMins, int intervalHours, int retries, int transmitWindows, int quietStart, int quietEnd)
    {
        reportingStartMins = startMins;
        reportingIntervalHours = intervalHours;
        reportingRetries = retries;
        reportingTransmitWindows = transmitWindows;
        reportingQuietStart = quietStart;
        reportingQuietEnd = quietEnd;
        return this;
    }

    public CommandPacketBuilder recordingInterval(int startTimeMins, int intervalMins)
    {
        recordingStartTime = startTimeMins;
        recordingIntervalMins = intervalMins;
        return this;
    }

    public CommandPacketBuilder numberOfDevices(int numberOfDevices)
    {
        this.numberOfDevices = numberOfDevices;
        return this;
    }

    public CommandPacketBuilder eventMask(int eventMask)
    {
        this.eventMask = eventMask;
        return this;
    }

    public CommandPacketBuilder packetType(TaggedDataPacketType packetType)
    {
        this.packetType = packetType;
        return this;
    }

    public TaggedPacket buildImageUpdateCommandPacket(int sequenceNumber, ImageUpdateCommandBuilder... imageBuilders)
    {
        //build MqttPacketHeader
        final MqttPacketHeaderData mqttPacketHeaderData = buildMqttPacketHeader(sequenceNumber);

        final PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.CommandConfiguration)
                .appendTagData(mqttPacketHeaderData)
                .appendSecureTagData(new CommandData(CmdId.UpdateImage))
                .appendSecureTagData(new TimeAndDateData(executionTimeSecond));

        //build imageBuilders to get list of taggedPacket per builder, and append to secure tag data
        for (ImageUpdateCommandBuilder builder : imageBuilders)
        {
            for (TaggedData tag : builder.build())
            {
                packetBuilder.appendSecureTagData(tag);
            }
        }

        return packetBuilder.buildTaggedPacket();
    }

    public TaggedPacket buildRebootCmiuCommandPacket(int sequenceNumber)
    {
        return buildCommandPacket(sequenceNumber, CmdId.RebootCmiu);
    }

    public TaggedPacket buildMagSwipeEmulationCommandPacket(int sequenceNumber)
    {
        return buildCommandPacket(sequenceNumber, CmdId.MapSwipeEmulation);
    }

    public TaggedPacket buildEraseDatalogCommandPacket(int sequenceNumber)
    {
        return buildCommandPacket(sequenceNumber, CmdId.EraseDatalog);
    }

    public TaggedPacket buildSetRecordingReportingIntervalsCommandPacket(int sequenceNumber)
    {
        //build MqttPacketHeader
        MqttPacketHeaderData mqttPacketHeaderData = buildMqttPacketHeader(sequenceNumber);

        PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.CommandConfiguration)
                .appendTagData(mqttPacketHeaderData)
                .appendSecureTagData(new CommandData(CmdId.SetRecordingReportingInterval))
                .appendSecureTagData(new RecordingAndReportingIntervalData(
                        reportingStartMins,
                        reportingIntervalHours,
                        reportingRetries,
                        reportingTransmitWindows,
                        reportingQuietStart,
                        reportingQuietEnd,
                        recordingStartTime,
                        recordingIntervalMins,
                        numberOfDevices,
                        eventMask));

        return packetBuilder.buildTaggedPacket();
    }

    public TaggedPacket buildPublishRequestedPacketCommandPacket(int sequenceNumber)
    {
        //build MqttPacketHeader
        MqttPacketHeaderData mqttPacketHeaderData = buildMqttPacketHeader(sequenceNumber);

        PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.CommandConfiguration)
                .appendTagData(mqttPacketHeaderData)
                .appendSecureTagData(new CommandData(CmdId.PublishRequestedPacket))
                .appendSecureTagData(new IntegerData(TagId.PacketType, TagDataType.Byte, packetType.getId()));
        //Todo in future this command will include tag 67 which defines some extra TBD meta-data

        return packetBuilder.buildTaggedPacket();
    }

    public TaggedPacket buildUpdateModemFirmwareCommandPacket(int sequenceNumber)
    {
        //build MqttPacketHeader
        MqttPacketHeaderData mqttPacketHeaderData = buildMqttPacketHeader(sequenceNumber);

        PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.CommandConfiguration)
                .appendTagData(mqttPacketHeaderData)
                .appendSecureTagData(new CommandData(CmdId.ModemFota))
                .appendSecureTagData(new CharArrayData(TagId.ModuleFotaFtpUrl, TagDataType.CharArray, fotaURL.getBytes()))
                .appendSecureTagData(new IntegerData(TagId.ModuleFotaFtpPort, TagDataType.UInt16, fotaPort))
                .appendSecureTagData(new CharArrayData(TagId.ModuleFotaFtpUsername, TagDataType.CharArray, fotaUsername.getBytes()))
                .appendSecureTagData(new CharArrayData(TagId.ModuleFotaFtpPassword, TagDataType.CharArray, fotaPassword.getBytes()))
                .appendSecureTagData(new CharArrayData(TagId.Image, TagDataType.CharArray, fotaFilename.getBytes()))
                .appendSecureTagData(new CharArrayData(TagId.NewImageVersionInformation, TagDataType.CharArray, fotaNewImageVersionInfo.getBytes()));

        return packetBuilder.buildTaggedPacket();
    }

    private static MqttPacketHeaderData buildMqttPacketHeader(int sequenceNumber)
    {
        //build MqttPacketHeader
        return new MqttPacketHeaderData.Builder()
                .sequenceNumber(sequenceNumber)
                .keyInfo(0)
                .encryptionMethod(0)
                .token(0)
                .build();
    }

    /**
     * Build command packet with no variable data tag
     *
     * @param sequenceNumber sequence number for the packet
     * @param commandId command id
     * @return tagged packet
     */
    private TaggedPacket buildCommandPacket(int sequenceNumber, CmdId commandId)
    {
        //build MqttPacketHeader
        MqttPacketHeaderData mqttPacketHeaderData = buildMqttPacketHeader(sequenceNumber);

        PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.CommandConfiguration)
                .appendTagData(mqttPacketHeaderData)
                .appendSecureTagData(new CommandData(commandId));

        return packetBuilder.buildTaggedPacket();
    }
}
