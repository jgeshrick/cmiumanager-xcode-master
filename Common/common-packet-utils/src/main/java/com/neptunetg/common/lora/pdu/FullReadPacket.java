/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.pdu;

/**
 * Created by WJD1 on 09/08/2016.
 * A class to represent a 53 byte read packet for L900
 * As defined in ETI 05 - 05
 */
public class FullReadPacket extends L900Packet
{
    private final static int PACKET_SIZE_BYTES = 53;
    private final static int READINGS_IN_PACKET = 12;

    public FullReadPacket(ReadingWithFlags[] readings, int timeSinceRead)
    {
        super(PacketId.FULL_READ_PACKET, PACKET_SIZE_BYTES);

        if(readings.length != READINGS_IN_PACKET)
        {
            throw new IllegalArgumentException("Readings array must contain 12 readings");
        }

        int bitPosition = 8;

        for(ReadingWithFlags reading : readings)
        {
            setNumber(bitPosition, r900ReadingIntFromReadingWithFlags(reading), 32);

            bitPosition += 32;
        }

        setNumber(bitPosition, timeSinceRead, 16);
    }

    public FullReadPacket(byte[] packetBytes)
    {
        super(packetBytes);
    }

    public ReadingWithFlags getReading(int readingNumber)
    {
        if(readingNumber < 0 || readingNumber >= READINGS_IN_PACKET)
        {
            throw new IllegalArgumentException("Cannot request a reading outside of 0 - 11");
        }

        return readingWithFlagsFromR900ReadingInt((int)getNumber(8 + (readingNumber * 32), 32));
    }

    public int getTimeSinceLastRead()
    {
        return (int)getNumber(8 + (32*READINGS_IN_PACKET), 16);
    }

    private int r900ReadingIntFromReadingWithFlags(ReadingWithFlags readingWithFlags)
    {
        int reading = 0;

        reading |= (readingWithFlags.getReading() & 0xFF) << 24;
        reading |= ((readingWithFlags.getReading() >> 8) & 0xFF) << 16;
        reading |= ((readingWithFlags.getReading() >> 16) & 0xFF) << 8;
        reading |= ((readingWithFlags.getReading() >> 24) & 0x07) << 5;
        reading |= readingWithFlags.getRevFlowFlag() << 2;
        reading |= readingWithFlags.getLeakFlag();

        return reading;
    }

    private ReadingWithFlags readingWithFlagsFromR900ReadingInt(int reading)
    {
        ReadingWithFlags readingWithFlags = new ReadingWithFlags();
        readingWithFlags.setReading(((reading >> 24) & 0xFF) +
                (((reading >> 16) & 0xFF) << 8) +
                (((reading >> 8 ) & 0xFF) << 16) +
                (((reading >> 5 ) & 0x07) << 24));
        readingWithFlags.setRevFlowFlag((byte)((reading >> 2) & 0x03));
        readingWithFlags.setLeakFlag((byte)(reading & 0x03));

        return readingWithFlags;
    }

    @Override
    public String toString()
    {
        final StringBuilder ret = new StringBuilder("[PDU: FULL/READ: packetId=");
        ret.append(getPacketId())
                .append(" read1=").append(getReading(0))
                .append(" read2=").append(getReading(1))
                .append(" read3=").append(getReading(2))
                .append(" read4=").append(getReading(3))
                .append(" read5=").append(getReading(4))
                .append(" read6=").append(getReading(5))
                .append(" read7=").append(getReading(6))
                .append(" read8=").append(getReading(7))
                .append(" read9=").append(getReading(8))
                .append(" read10=").append(getReading(9))
                .append(" read11=").append(getReading(10))
                .append(" read12=").append(getReading(11))
                .append(" TimeSinceLastRead=").append(getTimeSinceLastRead())
                .append(" length=").append(lengthInBytes()).append(" bytes]");
        return ret.toString();
    }
}
