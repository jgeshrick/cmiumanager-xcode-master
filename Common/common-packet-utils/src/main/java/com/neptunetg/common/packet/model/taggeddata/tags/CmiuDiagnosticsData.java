/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag ID 23
 */
public class CmiuDiagnosticsData extends IntegerArrayData
{

    public static int SIZE_IN_BYTES = 5;

    public CmiuDiagnosticsData(int diagnosticsResult, int processorResetCounter)
    {
        super(TagId.CmiuHwDiagnostics, TagDataType.ByteRecord, new byte[SIZE_IN_BYTES], SIZE_IN_BYTES);

        setInt(0, diagnosticsResult);
        setByte(4, processorResetCounter);
    }

    public CmiuDiagnosticsData(TagDataType dataType, byte[] data)
    {
        super(TagId.CmiuHwDiagnostics, dataType, data, data.length);
    }

    @JsonProperty("cmiu_hw_diag_status_flags")
    public int getDiagnosticFlags()
    {
        return getDataBuffer().getInt(0);
    }

    @JsonProperty("cmiu_hw_diag_reset_counter")
    public int getProcessorResetCount()
    {
        return getDataBuffer().get(4);
    }

    /**
     * formats the CMIU Diagnsotics data into a string
     * @return String with CMIU Diagnostics
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": CMIU Diagnostics [");
        ret.append("HW Self Diagnostic Flags=").append(getDiagnosticFlags());
        ret.append(", Processor Reset Count=").append(getProcessorResetCount());
        ret.append("]");

        return ret.toString();
    }

}
