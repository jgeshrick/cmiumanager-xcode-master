/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.pdu;

/**
 * Created by WJD1 on 07/07/2016.
 * A class to represent a deactivate command packet
 */
public class DeactivateCommandPacket extends CommandPacket
{
    public DeactivateCommandPacket(int sequenceNumber)
    {
        super(CommandPacketTypeEnum.Deactivate);

        setNumber(13, sequenceNumber, 8);
    }

    public DeactivateCommandPacket(byte[] packetBytes)
    {
        super(packetBytes);
    }

    public int getSequenceNumber()
    {
        return (int)getNumber(13, 8);
    }

    @Override
    public String toString()
    {
        final StringBuilder ret = new StringBuilder("[PDU: COMMAND (TIME OFFSET): packetId=");
        ret.append(getPacketId()).append(" commandId=").append(getCommandId())
                .append(" sequenceNumber=").append(getSequenceNumber())
                .append(" length=").append(lengthInBytes()).append(" bytes]");
        return ret.toString();
    }
}
