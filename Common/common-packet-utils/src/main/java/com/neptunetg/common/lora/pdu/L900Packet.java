/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.pdu;

/**
 * Class to represent a generic L900 packet (PDU) - without any JSON wrapper
 */
public class L900Packet
{
    private final byte[] packetBytes;

    public L900Packet(byte[] packetBytes)
    {
        if (packetBytes == null)
        {
            this.packetBytes = new byte[0];
        }
        else
        {
            this.packetBytes = packetBytes.clone();
        }

    }

    public L900Packet(PacketId packetId, int packetBytesLength)
    {
        this.packetBytes = new byte[packetBytesLength];

        setNumber(0, packetId.getId(), 5);

    }

    public String getPacketName()
    {
        return getPacketId().getPacketName();
    }

    public PacketId getPacketId()
    {
        return PacketId.fromFirstDataByte(this.packetBytes);
    }

    public byte[] getBytes()
    {
        return packetBytes;
    }

    public int lengthInBytes()
    {
        return packetBytes.length;
    }


    /**
     * This function is used to set a number of bits in the packetBytes array at
     * any bit position. It will set the number of bits indicated by bitsToSet
     * @param packetBitPosition The bit position in the byte array to start setting bits
     * @param number The value to set
     * @param bitsToSet The number of bits to use to set the number
     */
    protected void setNumber(int packetBitPosition, long number, int bitsToSet)
    {
        bitsToSet--;

        for(int i=0; i <= bitsToSet; i++)
        {
            int byteIndex  = (i + packetBitPosition) / 8;
            int byteOffset = 7 - ((i + packetBitPosition) % 8);

            packetBytes[byteIndex] &= ~(0x01 << byteOffset);
            packetBytes[byteIndex] |= (((number >> (bitsToSet - i)) & 0x01) << byteOffset);
        }
    }

    /**
     * This function is used to get a number of bits in the packetBytes array and
     * return then as a long
     * @param packetBitPosition The bit position in the array to get the number from
     * @param bitsToGet The number of bits to get for the number
     * @return The number
     */
    public long getNumber(int packetBitPosition, int bitsToGet)
    {
        long number = 0;
        bitsToGet--;

        for(int i=0; i <= bitsToGet; i++)
        {
            int byteIndex  = (i + packetBitPosition) / 8;
            int byteOffset = 7 - ((i + packetBitPosition) % 8);

            number |= ((packetBytes[byteIndex] >> byteOffset) & 0x01L) << (bitsToGet - i);
        }

        return number;
    }

    @Override
    public String toString()
    {
        final StringBuilder ret = new StringBuilder("[PDU: packetId=");
        ret.append(getPacketId()).append(" length=").append(lengthInBytes()).append(" bytes]");
        return ret.toString();
    }
}
