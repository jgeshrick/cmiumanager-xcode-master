/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.util;

import java.nio.ByteBuffer;
import java.util.Formatter;

/**
 * Utilities for converting between hex strings and binary
 */
public class HexUtils
{
    private HexUtils() {} //not instantiatable

    /**
     * Convert a hex string to a byte array.
     * All characters other than 0-9, A-F and a-f are skipped
     * @param dataHex Hex string
     * @return Byte array of the hex data
     */
    public static byte[] parseHexToByteArray(String dataHex)
    {
        if (dataHex == null)
        {
            return new byte[0];
        }
        final int bufSize = 4096;
        byte[] buf = new byte[bufSize];
        boolean highNibble = true;
        int ptr = 0;
        int cp;
        for (cp = 0; cp < dataHex.length() && ptr < bufSize; cp++)
        {
            int val = Character.digit(dataHex.charAt(cp), 16);
            if (val >= 0)
            {
                if (highNibble)
                {
                    buf[ptr] = (byte)(val << 4);
                }
                else
                {
                    buf[ptr++] += (byte) val;
                }
                highNibble = !highNibble;
            }
        }
        if (ptr == bufSize) //buffer exceeded - count total hex chars and try again
        {
            //count the remaining characters in ptr
            boolean highNibbleTemp = highNibble;
            int tempPtr = ptr;
            for (int tempCp = cp; tempCp < dataHex.length(); tempCp++)
            {
                int val = Character.digit(dataHex.charAt(tempCp), 16);
                if (val >= 0)
                {
                    if (!highNibbleTemp)
                    {
                        tempPtr++;
                    }
                    highNibbleTemp = !highNibbleTemp;
                }
            }
            if (!highNibbleTemp)
            {
                throw new IllegalArgumentException("Odd number of hex characters in input " + dataHex);
            }
            if (tempPtr > bufSize)
            {
                byte[] oldBuf = buf;
                buf = new byte[tempPtr];
                System.arraycopy(oldBuf, 0, buf, 0, bufSize);
                oldBuf = null;
                for (; cp < dataHex.length(); cp++) //continue parsing the characters that didn't fit in
                {
                    int val = Character.digit(dataHex.charAt(cp), 16);
                    if (val >= 0)
                    {
                        if (highNibble)
                        {
                            buf[ptr] = (byte)(val << 4);
                        }
                        else
                        {
                            buf[ptr++] += (byte) val;
                        }
                        highNibble = !highNibble;
                    }
                }

            }
        }
        else if (!highNibble)
        {
            throw new IllegalArgumentException("Odd number of hex characters in input " + dataHex);
        }
        else
        {
            byte[] oldBuf = buf;
            buf = new byte[ptr];
            System.arraycopy(oldBuf, 0, buf, 0, ptr);
        }
        return buf;
    }

    /**
     * Converts data in a byte buffer to a string representation
     * @param data Data to convert.  The contents and the position are not changed.
     * @return String representation of data
     */
    public static String byteBufferToHex(ByteBuffer data)
    {
        if (data == null) {
            return "";
        }
        try (final Formatter f = new Formatter())
        {
            final int len = data.limit();
            for (int i = 0; i < len; i++)
            {
                f.format("%02x", data.get(i));
            }
            return f.toString();
        }
    }


    /**
     * Converts data in a byte buffer to a string representation
     * @param data Data to convert.  The contents and the position are not changed.
     * @param offset Offset in buffer to first byte to convert
     * @param length Number of bytes to convert
     * @return String representation of data starting at offset
     */
    public static String byteBufferRangeToHex(ByteBuffer data, int offset, int length)
    {
        if (data == null) {
            return "";
        }
        try (final Formatter f = new Formatter())
        {
            for (int i = 0; i < length; i++)
            {
                f.format("%02x", data.get(offset + i));
            }
            return f.toString();
        }
    }


    /**
     * Converts data in a byte array to a string representation
     * @param data Data to convert.  The contents are not changed.
     * @return String representation of data
     */
    public static String byteArrayToHex(byte[] data)
    {
        if (data == null) {
            return "";
        }
        try (final Formatter f = new Formatter())
        {
            final int len = data.length;
            for (int i = 0; i < len; i++)
            {
                f.format("%02x", data[i]);
            }
            return f.toString();
        }
    }


}
