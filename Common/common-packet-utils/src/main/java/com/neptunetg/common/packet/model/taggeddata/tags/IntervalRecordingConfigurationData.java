/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag 34
 */
public class IntervalRecordingConfigurationData extends IntegerArrayData
{
    public static int SIZE_IN_BYTES = 14;
    public IntervalRecordingConfigurationData(int deviceNumber, int intervalFormat, int deviceRecordingInterval,
                                              int reportingIntervalHours, int startTimeOfInterval,
                                              UnixTimestamp timeOfMostRecentReading)
    {
        super(TagId.IntervalRecording, TagDataType.ByteRecord, new byte[SIZE_IN_BYTES], SIZE_IN_BYTES);

        setByte(0, deviceNumber);
        setByte(1, intervalFormat);
        setByte(2, deviceRecordingInterval);
        setByte(3, reportingIntervalHours);
        setShort(4, startTimeOfInterval);
        setLong(6, timeOfMostRecentReading.getEpochSecond());
    }

    public IntervalRecordingConfigurationData(TagDataType dataType, byte[] data)
    {
        super(TagId.IntervalRecording, dataType, data, data.length);
    }

    @JsonProperty("device_id")
    public int getDeviceNumber()
    {
        return getDataBuffer().get(0);
    }

    @JsonProperty("cmiu_recording_interval_format")
    public int getIntervalFormat()
    {
        return getDataBuffer().get(1);
    }

    @JsonProperty("cmiu_recording_interval_mins")
    public int getDeviceRecordingInterval()
    {
        return getDataBuffer().get(2);
    }

    @JsonProperty("cmiu_reporting_device_log_interval_hours")
    public int getReportingIntervalHours()
    {
        return getDataBuffer().get(3);
    }

    @JsonProperty("cmiu_recording_interval_start_time")
    public int getStartTimeOfRecordingInterval()
    {
        return getDataBuffer().getShort(4);
    }

    @JsonProperty("interval_last_read_datetime")
    public UnixTimestamp getTimeOfMostRecentReading()
    {
        return new UnixTimestamp(getDataBuffer().getLong(6));
    }

    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Interval Recording Data [");
        ret.append("Device Number =").append(getDeviceNumber());
        ret.append(", Interval Format =").append(getIntervalFormat());
        ret.append(", Device Read Interval =").append(getDeviceRecordingInterval());
        ret.append(", Reporting Interval (hours) =").append(getReportingIntervalHours());
        ret.append(", Start Time of Interval =").append(getStartTimeOfRecordingInterval());
        ret.append(", Time of Most Recent Reading =").append(getTimeOfMostRecentReading());
        ret.append("]");

        return ret.toString();
    }
}
