/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.CharArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;


/**
 * Tag 22
 */
public class PinPukPukTwoRequestStatusData extends CharArrayData
{
    private final String data;

    public PinPukPukTwoRequestStatusData(TagDataType dataType, byte[] data)
    {
        super(TagId.PinPukPukTwoRequestData, dataType, data);
        this.data = new String(data);
    }

    @JsonProperty("pin_puk_puk2_status_value")
    public String getPinPukPukTwoRequestDataString()
    {
        return data;
    }


    /**
     * requests the PIN/PUK/PUK2 device status
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": PIN/PUK/PUK2 Device Status Request [");
        ret.append("Request=").append(super.toString());
        ret.append("]");

        return ret.toString();
    }
}
