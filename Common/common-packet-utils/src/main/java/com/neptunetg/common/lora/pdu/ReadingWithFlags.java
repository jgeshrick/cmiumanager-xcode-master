/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.pdu;

/**
 * Created by WJD1 on 09/08/2016.
 * Class to represent a 32 bit reading which includes flags
 * As defined in ETI 05 - 05
 */
public class ReadingWithFlags
{
    private int reading;
    private byte revFlowFlag;
    private byte leakFlag;

    public int getReading()
    {
        return reading;
    }

    public void setReading(int reading)
    {
        this.reading = reading;
    }

    public byte getRevFlowFlag()
    {
        return revFlowFlag;
    }

    public void setRevFlowFlag(byte revFlowFlag)
    {
        this.revFlowFlag = revFlowFlag;
    }

    public byte getLeakFlag()
    {
        return leakFlag;
    }

    public void setLeakFlag(byte leakFlag)
    {
        this.leakFlag = leakFlag;
    }
}
