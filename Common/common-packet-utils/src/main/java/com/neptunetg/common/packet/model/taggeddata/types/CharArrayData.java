/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.types;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.charset.*;


/**
 * Tag types 8 and 12
 * A class for char array tags, such as a
 * username tag.
 */
public class CharArrayData extends TaggedDataWithType
{
    private final byte[] data;
    private String stringRepresentation = null;
    private String warning = null;

    /**
     * Char array tag type
     * @param tag The ID of the tag
     * @param dataType The data type - char array or extended char array
     * @param data The byte data of the char array (in ASCII7 encoding)
     */
    public CharArrayData(TagId tag, TagDataType dataType, byte[] data)
    {
        super(tag, dataType);
        this.data = data.clone();
    }

    /**
     * Get a summary of the tag type definition
     * @return Metadata of tag
     */
    @JsonProperty("meta")
    @Override
    public TaggedDataMetadata getMetadata()
    {
        return new TaggedDataMetadata(getTagId(), getDataType(), data.length);
    }

    /**
     * Char array tag type
     * @param tag The ID of the tag
     * @param dataType The data type - char array or extended char array
     * @param value The string value of the character array
     * */
    public CharArrayData(TagId tag, TagDataType dataType, String value)
    {
        super(tag, dataType);

        CharsetEncoder asciiEncoder = StandardCharsets.US_ASCII.newEncoder();
        asciiEncoder.onMalformedInput(CodingErrorAction.REPORT);
        asciiEncoder.onUnmappableCharacter(CodingErrorAction.REPORT);

        ByteBuffer bb = ByteBuffer.allocate(value.length());
        CoderResult result = asciiEncoder.encode(CharBuffer.wrap(value), bb, true);
        if (result.isError()) {
            throw new IllegalArgumentException("Non-ASCII characters in string \"" + value + "\"!  Not allowed in char array tag.");
        }
        this.data = bb.array();
        this.stringRepresentation = value;
    }

    /**
     * Getter that returns the char array
     * @return Copy of the char array
     */
    @JsonIgnore
    public byte[] getData()
    {
        return data.clone();
    }


    private void ensureStringRepresentation()
    {
        if (this.stringRepresentation == null)
        {
            final CharsetDecoder asciiDecoder = StandardCharsets.US_ASCII.newDecoder();
            asciiDecoder.onMalformedInput(CodingErrorAction.REPORT);
            asciiDecoder.onUnmappableCharacter(CodingErrorAction.REPORT);

            final ByteBuffer dataIn = ByteBuffer.wrap(data);
            final CharBuffer cb = CharBuffer.allocate(data.length);
            CoderResult result = asciiDecoder.decode(dataIn, cb, true);
            if (result.isError())
            {

                final CharsetDecoder latinDecoder = StandardCharsets.ISO_8859_1.newDecoder();
                latinDecoder.onMalformedInput(CodingErrorAction.REPORT);
                latinDecoder.onUnmappableCharacter(CodingErrorAction.REPORT);
                dataIn.rewind();
                cb.rewind();
                result = latinDecoder.decode(dataIn, cb, true);
                this.warning = "Not ASCII7";
            }
            else
            {
                this.warning = null;
            }
            cb.rewind();
            this.stringRepresentation = cb.toString();
        }
    }

    /**
     * Convert byte array to string (ASCII characters only)
     * @return ASCII string
     */
    @JsonProperty("string_value")
    public String getAsString()
    {
        ensureStringRepresentation();
        return this.stringRepresentation;
    }


    /**
     * Convert byte array to string (ASCII characters only)
     * @return ASCII string
     */
    @JsonProperty("warning")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getWarning()
    {
        ensureStringRepresentation();
        return this.warning;

    }

    /**
     * Getter that returns a string summary of the tag
     * @return Summary text
     */
    @Override
    @JsonIgnore
    public String getTagSummary()
    {
        return getTagId().getTagName() + " (" + data.length + " item " + getDataType().name() + ")";
    }

    /**
     * Convert to string
     * @return The ASCII7 string in the char array
     */
    @Override
    public String toString()
    {
        final String w = getWarning();
        if (w != null)
        {
            return getAsString() + " (WARNING: " + w + ")";
        }
        else
        {
            return getAsString();
        }
    }

    /**
     * Convert to byte array
     * @return byte array of this tag
     */
    @Override
    public byte[] serialize()
    {
        if(this.getDataType() == TagDataType.CharArray)
        {
            if(data.length > 255)
            {
                throw new IndexOutOfBoundsException("Char array too long for char array tag");
            }

            byte[] tagIdAndType = super.serialize();
            return ByteBuffer.allocate(tagIdAndType.length + data.length + 1)   //add array size
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .put(tagIdAndType)       //tag number, type
                    .put((byte) data.length) //array size
                    .put(data)               //array content
                    .array();
        }
        else
        {
            byte[] tagIdAndType = super.serialize();
            return ByteBuffer.allocate(tagIdAndType.length + data.length + 2)   //add array size
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .put(tagIdAndType)          //tag number, type
                    .putShort((short) data.length) //array size
                    .put(data)                  //array content
                    .array();
        }
    }
}
