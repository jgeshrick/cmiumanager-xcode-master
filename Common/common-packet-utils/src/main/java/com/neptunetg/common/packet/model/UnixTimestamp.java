/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.DateTimeException;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Represents a UNIX timestamp as used in the packets - epoch seconds
 */
public class UnixTimestamp
{
    private final long epochSeconds;

    /**
     * Represents a timestamp as seconds since 1/1/1970
     * @param epochSeconds Seconds since epoch start
     */
    public UnixTimestamp(long epochSeconds)
    {
        this.epochSeconds = epochSeconds;
    }

    @JsonProperty("timestamp")
    public long getEpochSecond()
    {
        return epochSeconds;
    }

    public long toEpochMilli()
    {
        return epochSeconds * 1000L;
    }


    /**
     * Converts to java.util.Date
     * @return Date representing this timestamp
     */
    public Date asDate()
    {
        return new Date(this.toEpochMilli());
    }

    /**
     * Converts to java.util.Calendar
     * @return Calendar representing this timestamp
     */
    public Calendar asCalendar()
    {
        Calendar ret = new GregorianCalendar();
        ret.setTime(asDate());
        return ret;
    }

    /**
     * Converts to JSR310 Instant
     * @return Instant representing this timestamp
     */
    public Instant asInstant()
    {
        return Instant.ofEpochSecond(epochSeconds);
    }

    /**
     * Returns unix timestamp as string
     * @return ISO-8601 formatted date/time of instant
     */
    @Override
    public String toString()
    {
        try
        {
            return asInstant().toString();
        }
        catch (Exception e) //e.g. java.time.DateTimeException: Instant exceeds minimum or maximum instant
        {
            return epochSeconds + " epoch seconds";
        }
    }

    /**
     * Get the ISO-8601 representation of the date
     * @return representation
     */
    @JsonProperty("iso8601")
    public String getIso8601DateString()
    {
        try
        {
            return Instant.ofEpochSecond(epochSeconds).toString();
        }
        catch (DateTimeException e)
        {
            return epochSeconds + " epoch seconds";
        }
    }

    /**
     * Check if a UnixTimestamp is before another one
     * @param other Other UnixTimestamp
     * @return True if before
     */
    public boolean isBefore(UnixTimestamp other)
    {
        if (other == null)
        {
            return true;
        }
        else
        {
            return this.epochSeconds < other.epochSeconds;
        }
    }


    /**
     * Check if a UnixTimestamp is after another one
     * @param other Other UnixTimestamp
     * @return True if after
     */
    public boolean isAfter(UnixTimestamp other)
    {
        if (other == null)
        {
            return true;
        }
        else
        {
            return this.epochSeconds > other.epochSeconds;
        }
    }

    /**
     * Utility function to get current time in seconds from 1/1/1970.
     * @return representation of current time
     */
    public static UnixTimestamp getCurrentUnixTimestamp()
    {
        return new UnixTimestamp(Instant.now().getEpochSecond());
    }


}
