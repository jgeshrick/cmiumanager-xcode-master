/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag 62.
 * Battery Voltage at end of last transmission attempt
     The ADC binary value is sent. (ADCVAL)
     For the host calculation of actual battery voltage:
     Battery voltage= (ADCVAL / 4096 ) * 2.5 * 3 )
 */
public class BatteryVoltage extends IntegerData
{

    public BatteryVoltage(TagDataType dataType, long data)
    {
        super(TagId.BatteryVoltage, dataType, data);
    }

    @JsonProperty("millivolts")
    public double getMillivolts()
    {
        return getValue() * (2.5 * 3.0 * 1000.0 / 4096.0);
    }

    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Battery voltage [");
        ret.append("Raw ADV value =").append(getValue());
        ret.append(", equivalent in millivolts =").append(getMillivolts());
        ret.append("]");

        return ret.toString();
    }
}
