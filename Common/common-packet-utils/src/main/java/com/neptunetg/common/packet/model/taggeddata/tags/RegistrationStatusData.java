/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.CharArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;


/**
 * Tag 5
 */
public class RegistrationStatusData extends CharArrayData
{
    private final String data;

    public RegistrationStatusData(TagDataType dataType, byte[] data)
    {
        super(TagId.RegistrationStatus, dataType, data);
        this.data = new String(data);
    }

    @JsonProperty("registration_status_value")
    public String getRegistrationStatusString()
    {
        return data;
    }


    /**
     * returns the registration status as a string
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Registration Status [");
        ret.append("Registration Status String=").append(getRegistrationStatusString());
        ret.append("]");

        return ret.toString();
    }
}
