/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;

/**
 * Metadata about a TaggedData item. For use in JSON serialization.
 */
public class TaggedDataMetadata
{
    private final TagId id;

    private final TagDataType dataType;

    private final Integer arrayLength;

    public TaggedDataMetadata(TagId id, TagDataType dataType, Integer arrayLength)
    {
        this.id = id;
        this.dataType = dataType;
        this.arrayLength = arrayLength;
    }

    @JsonProperty("tag_id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public TagId getTagId()
    {
        return id;
    }

    @JsonProperty("data_type")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public TagDataType getDataType()
    {
        return dataType;
    }


    @JsonProperty("array_length")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Integer getArrayLength()
    {
        return arrayLength;
    }
}
