/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by WJD1 on 02/07/2015.
 * A class to represent the different error enums that can be used.
 * Used in error tag 51, to indicate an error. At the moment only errors
 * for commands are specified.
 */
public final class ErrorId
{
    private static final Map<Integer, ErrorId> LOOKUP_TABLE = new HashMap<>();

    public static final ErrorId NoError = new ErrorId(0 , "No error", "No error, can be used to indicate success");
    public static final ErrorId InvalidCommand = new ErrorId(1 , "Invalid Command", "The command issued has an invalid ID");

    private final int id;

    private final String errorName;

    private final String description;

    /**
     * @param id Represents a error ID (i.e. a byte value which signifies the
     * meaning of the error tag.
     */
    private ErrorId(int id, String errorName, String description)
    {
        this.id = id;
        this.errorName = errorName;
        this.description = description;
        LOOKUP_TABLE.put(id, this);
    }

    @JsonProperty("id")
    public int getId()
    {
        return id;
    }

    @JsonProperty("error_code")
    public String getTagName()
    {
        return errorName;
    }

    @JsonIgnore
    public String getDescription()
    {
        return description;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }

        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        ErrorId errorId = (ErrorId) o;

        return id == errorId.id;
    }

    @Override
    public int hashCode()
    {
        return id;
    }
}
