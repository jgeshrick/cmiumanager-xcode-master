/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.types;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Tag types 1-4
 * Created by ABH1 on 05/02/2015.
 * A class for integer type tags, such as the
 * the mag swipe count.
 */
public class IntegerData extends TaggedDataWithType
{
    private final long integerValue;

    /**
     * Constructs a tag occurrence with an integer data type e.g. UInt32
     * tag = the tag ID
     * dataType = type of data (UInt8...UInt64)
     * integerValue = the value held by the tag
     */
    public IntegerData(TagId tag, TagDataType dataType, long integerValue)
    {
        super(tag, dataType);
        this.integerValue = integerValue;
    }

    @JsonProperty("value")
    public long getValue()
    {
        return integerValue;
    }

    /**
     * Getter that returns a string summary of the tag
     */
    @Override
    public String getTagSummary()
    {
        return getTagId().getTagName() + " (" +
                getDataType().name() + ")";
    }

    @Override
    public String toString()
    {
        return getTagSummary() + ": Integer [" + getTagId().getTagName() + "=" + getValue() + "]";
    }

    @Override
    public byte[] serialize()
    {
        byte[] tagIdAndType = super.serialize();
        if(this.getDataType() == TagDataType.Byte)
        {
            return ByteBuffer.allocate(tagIdAndType.length + Byte.BYTES)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .put(tagIdAndType)
                    .put((byte) this.integerValue)
                    .array();
        }
        if(this.getDataType() == TagDataType.UInt16)
        {
            return ByteBuffer.allocate(tagIdAndType.length + Short.BYTES)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .put(tagIdAndType)
                    .putShort((short) this.integerValue)
                    .array();
        }
        if(this.getDataType() == TagDataType.UInt32)
        {
            return ByteBuffer.allocate(tagIdAndType.length + Integer.BYTES)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .put(tagIdAndType)
                    .putInt((int) this.integerValue)
                    .array();
        }
        else
        {
            return ByteBuffer.allocate(tagIdAndType.length + Long.BYTES)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .put(tagIdAndType)
                    .putLong((long) this.integerValue)
                    .array();
        }
    }
}
