/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Created by WJD1 on 10/06/2015.
 * This tags holds status information about the CMIU
 * tag ID 44
 */
public class CmiuInformationData extends IntegerArrayData
{
    public static int SIZE_IN_BYTES = 31;

    public CmiuInformationData(UnixTimestamp manufactureDate, UnixTimestamp dateOfInstallation,
                               UnixTimestamp dateOfLastMagSwipe, int magSwipeCounter,
                               int batteryCapacityMah, int timeErrorOnLastNetworkAccess)
    {
        super(TagId.CmiuInformation, TagDataType.ByteRecord, new byte[SIZE_IN_BYTES], SIZE_IN_BYTES);

        setLong(0, manufactureDate.getEpochSecond());
        setLong(8, dateOfInstallation.getEpochSecond());
        setLong(16, dateOfLastMagSwipe.getEpochSecond());
        setByte(24, magSwipeCounter);
        setShort(25, (short)batteryCapacityMah);
        setInt(27, timeErrorOnLastNetworkAccess);
    }

    public CmiuInformationData(TagDataType dataType, byte[] data)
    {
        super(TagId.CmiuInformation, dataType, data, data.length);
    }

    @JsonProperty("cmiu_mfg_date")
    public UnixTimestamp getManufactureDate()
    {
        return new UnixTimestamp(getDataBuffer().getLong(0));
    }

    @JsonProperty("cmiu_install_date")
    public UnixTimestamp getDateOfInstallation()
    {
        return new UnixTimestamp(getDataBuffer().getLong(8));
    }

    @JsonProperty("cmiu_last_mag_swipe_date")
    public UnixTimestamp getDateOfLastMagSwipe()
    {
        return new UnixTimestamp(getDataBuffer().getLong(16));
    }

    @JsonProperty("cmiu_mag_swipe_counter")
    public int getMagSwipeCounter()
    {
        return getDataBuffer().get(24);
    }

    @JsonProperty("cmiu_battery_capacity")
    public int getBatteryCapacityMah()
    {
        //return battery capacity in mAh
        return getDataBuffer().getShort(25) & 0xFFFF;
    }

    @JsonProperty("cmiu_time_error")
    public int getTimeErrorOnLastNetworkAccess()
    {
        return getDataBuffer().getInt(27);
    }

    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Cmiu Information [");
        ret.append(" Manufacture Date").append(getManufactureDate());
        ret.append(", Date of Installation =").append(getDateOfInstallation());
        ret.append(", Date of Last Mag Swipe =").append(getDateOfLastMagSwipe());
        ret.append(", Mag Swipe Counter =").append(getMagSwipeCounter());
        ret.append(", Estimated Battery Capacity Remaining =").append(getBatteryCapacityMah());
        ret.append(", Time Error On Last Network Access =").append(getTimeErrorOnLastNetworkAccess());
        ret.append("]");

        return ret.toString();
    }
}
