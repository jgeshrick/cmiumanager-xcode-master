/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.types;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;

/**
 * Represents data tag that has a type
 */
public class TaggedDataWithType extends TaggedData {


    private final TagDataType dataType;

    /**
     * This represents a tagged data item which has a type
     * @param tag = content type such as Byte Array
     */
    public TaggedDataWithType(TagId tag, TagDataType dataType)
    {
        super(tag);
        this.dataType = dataType;
    }

    /**
     * Getter that returns a string summary of the tag
     */
    @Override
    public String getTagSummary()
    {
        return getTagId().getTagName() + " (" + dataType.name() + ")";
    }

    @JsonIgnore
    public TagDataType getDataType()
    {
        return this.dataType;
    }

    @JsonProperty("meta")
    @Override
    public TaggedDataMetadata getMetadata()
    {
        return new TaggedDataMetadata(getTagId(), dataType, null);
    }


    @Override
    public String toString()
    {
        return getTagSummary();
    }

    /**
     * Serialise into byte array
     * @return serialised data
     */
    @Override
    public byte[] serialize()
    {
        return new byte[]{
                (byte)this.getTagId().getId(),  //tag type
                this.getDataType().getId()      //data type
        };
    }
}
