/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.types;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Tag types as defined in ETI-48-00
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TagDataType
{

    None(0x00, "None", 0, 0, false),

    Byte(0x01, "Byte", 0, 1, false),
    UInt16(0x02, "16 bit little-endian", 0, 2, false),
    UInt32(0x03, "32 bit little-endian", 0, 4, false),
    UInt64(0x04, "64 bit little-endian", 0, 8, false),

    ByteRecord(0x05, "Byte record (followed by Byte = # bytes, followed by struct data)", 1, 1, true),
    UInt16Array(0x06, "Uint16 array (followed by Byte = # elements, followed by Short elements)", 1, 2, false),
    UInt32Array(0x07, "Uint32 array (followed by Byte = # elements, followed by Int32 elements)", 1, 4, false),
    CharArray(0x08, "Char array(followed by Byte = # elements, followed by comma delimited(null?). Char is ASCII 8 bit value.", 1, 1, false),

    ExtendedByteRecord(0x09, "Extended Byte record (followed by Short = # elements (Uint16), followed by elements)", 2, 1, true),
    ExtendedUInt16Array(0x0A, "Extended Uint16 array (followed by Short = # elements(Uint16), followed by Short elements)", 2, 2, false),
    ExtendedUInt32Array(0x0B, "Extended Uint32 array (followed by Short = # elements(Uint16), followed by Int elements)", 2, 4, false),
    ExtendedCharArray(0x0C, "Extended Char array(followed by Short = # elements(Uint16), followed by comma delimited(null?) Char is ASCII 8 bit value.", 2, 1, false),
    ExtendedSecureBlockArray(0x0D, "Secure block array (Type followed by Uint16 value for # elements, followed secure block elements. )Secure blocks are a group of 16 Uint8 elements.", 2, 16, false),

    ImageVersionInfo(0x0E, "Image version info", 0, 10, false),
    ByteArray(0x0f, "Byte array (followed by Byte = # elements, followed by elements)", 1, 1, false),
    ExtendedByteArray(0x10, "Extended Byte array (followed by Short = # elements (Uint16), followed by elements)", 2, 1, false),

    SecureBlockArray(0x11, "Secure block array (Type follow by Uint8 value for # elements, followed by secure block elements. ) Secure blocks are a group of 16 Uint8 elements.", 1, 16, false),

    R900OokReadingArray(0x12, "R900 OOK reading array (Type followed by UInt8 value for # elements, followed by R900 OOK packets).  Each R900 OOK element is a block of 25 UInt8 defined in ETI 05-00", 1, 25, false),
    R900FskReadingArray(0x13, "R900 FSK reading array (Type followed by UInt8 value for # elements, followed by R900 FSK packets).  Each R900 FSK element is a block of 25 UInt8 defined in ETI 05-00", 1, 25, false),
    ;

    private final byte id;

    private final String description;

    private final int sizeBytesToFollow;

    private final int unitSize;

    private final boolean struct;

    /**
     * Represents a data type of a tag, e.g. byte array
     * Corresponds to a special byte value in the packet
     */
    private TagDataType(int id, String description, int sizeBytesToFollow, int unitSize, boolean struct)
    {
        if ((id & -256) != 0)
        {
            throw new IllegalArgumentException("ID out of range (0-255): " + id);
        }

        this.id = (byte)id;
        this.description = description;
        this.sizeBytesToFollow = sizeBytesToFollow;
        this.unitSize = unitSize;
        this.struct = struct;
    }

    @JsonProperty("id")
    public byte getId()
    {
        return id;
    }

    @JsonProperty("name")
    public String getName()
    {
        return name();
    }

    @JsonIgnore
    public String getDescription()
    {
        return description;
    }

    @JsonIgnore
    public int getSizeBytesToFollow()
    {
        return sizeBytesToFollow;
    }

    @JsonIgnore
    public int getUnitSize()
    {
        return unitSize;
    }

    /**
     * Indicates that this type is a "struct".  This means that the underlying array data will
     * not be serialized to JSON.  Instead, getters should be provided for all the properties
     * represented by the array of data.
     */
    @JsonIgnore
    public boolean isStruct()
    {
        return struct;
    }


    /**
     * Get the tag data type represented by this byte value
     * @param byteValue The byte value
     * @return The corresponding tag data type
     */
    public static TagDataType forByte(int byteValue)
    {

        for (TagDataType t : values())
        {
            if (t.getId() == byteValue)
            {
                return t;
            }
        }

        throw new IllegalArgumentException("Unknown tag data type " + byteValue);
    }
}
