/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.builder;

/**
 * Summarizes RSSI values from a set of readings from an R900 MIU
 */
public class R900MiuRssiSummary
{

    private final int rssiMin;

    private final int rssiMax;

    private final int rssiSum;

    private final int packetCount;

    R900MiuRssiSummary(int rssiMin, int rssiMax, int rssiSum, int packetCount)
    {
        this.rssiMin = rssiMin;
        this.rssiMax = rssiMax;
        this.rssiSum = rssiSum;
        this.packetCount = packetCount;
    }

    public int getRssiMin()
    {
        return rssiMin;
    }

    public int getRssiMax()
    {
        return rssiMax;
    }

    public int getRssiSum()
    {
        return rssiSum;
    }

    public int getPacketCount()
    {
        return packetCount;
    }

    public double getRssiAverage()
    {
        return ((double)rssiSum) / Math.max(packetCount, 1);
    }
}
