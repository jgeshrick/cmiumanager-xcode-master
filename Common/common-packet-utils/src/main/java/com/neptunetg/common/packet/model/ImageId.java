/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * A class to represent the different command enums that can be used,
 * as well as the tag data type
 */
public final class ImageId
{
    private static final Map<Integer, ImageId> LOOKUP_TABLE = new HashMap<>();

    public static final ImageId FirmwareImage = new ImageId(0 , "Firmware Image");
    public static final ImageId ConfigImage = new ImageId(1 , "Config Image");
    public static final ImageId TelitModuleImage = new ImageId(2 , "Telit Model Image");

    public static final ImageId BleConfigImage = new ImageId(3 , "BLE Config Image");
    public static final ImageId ArbConfigImage = new ImageId(4 , "ARB Config Image");
    public static final ImageId EncryptionImage = new ImageId(5 , "Ecryption Image");

    public static final ImageId BootloaderImage = new ImageId(6 , "Bootloader Image");

    private final int id;

    private final String cmdName;

    /**
     * @param id Represents a image ID (i.e. a byte value which signifies the
     * use of the image.
     */
    private ImageId(int id, String cmdName)
    {
        this.id = id;
        this.cmdName = cmdName;
        LOOKUP_TABLE.put(id, this);
    }

    @JsonProperty("id")
    public int getId()
    {
        return id;
    }

    @JsonProperty("image_name")
    public String getTagName()
    {
        return cmdName;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }

        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        ImageId imageId = (ImageId) o;

        return id == imageId.id;
    }

    @Override
    public int hashCode()
    {
        return id;
    }
}
