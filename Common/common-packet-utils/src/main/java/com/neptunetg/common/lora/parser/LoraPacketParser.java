/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.parser;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.lora.json.ActilityForwardPacketJson;
import com.neptunetg.common.lora.json.ActilityUplinkJson;
import com.neptunetg.common.lora.json.SenetForwardPacketJson;
import com.neptunetg.common.lora.pdu.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class LoraPacketParser
{

    private final ObjectMapper jackson = new ObjectMapper();

    /**
     * Parse a PDU
     * @param packetBytes Raw packet bytes
     * @return Packet object.  May be L900Packet or one of its subclasses
     */
    public L900Packet parsePacket(byte[] packetBytes)
    {
        final PacketId packetId = PacketId.fromFirstDataByte(packetBytes);

        if (PacketId.APP_COMMAND_PACKET.equals(packetId) )
        {
            final CommandPacket commandPacket = new CommandPacket(packetBytes);

            if (CommandPacketTypeEnum.class.isInstance(commandPacket.getCommandId()))
            {
                switch (commandPacket.getCommandId())
                {
                    case Reset:
                        return new ResetCommandPacket(packetBytes);

                    case TimeOffset:
                        return new TimeOffsetCommandPacket(packetBytes);

                    case Deactivate:
                        return new DeactivateCommandPacket(packetBytes);
                }
            }

            return commandPacket;
        }
        else if (PacketId.TIME_AND_DATE.equals(packetId))
        {
            final TimeAndDatePacket timeAndDatePacket = new TimeAndDatePacket(packetBytes);
            return timeAndDatePacket;
        }
        else if (PacketId.BASIC_CONFIG_PACKET.equals(packetId))
        {
            final BasicConfigPacket basicConfigPacket = new BasicConfigPacket(packetBytes);
            return basicConfigPacket;
        }
        else if (PacketId.BASIC_READ_PACKET.equals(packetId))
        {
            final BasicReadPacket basicReadPacket = new BasicReadPacket(packetBytes);
            return basicReadPacket;
        }
        else if (PacketId.FULL_READ_PACKET.equals(packetId))
        {
            final FullReadPacket fullReadPacket = new FullReadPacket(packetBytes);
            return fullReadPacket;
        }

        // some other packet
        return new L900Packet(packetBytes);
    }

    /**
     * Parse a PDU as a time and date packet
     * @param packetBytes Bytes of the time and date packet
     * @return TimeAndDatePacket object
     */
    public TimeAndDatePacket parseTimeAndDatePacket(byte[] packetBytes)
    {
        final PacketId packetId = PacketId.fromFirstDataByte(packetBytes);
        if (PacketId.TIME_AND_DATE.equals(packetId))
        {
            final TimeAndDatePacket timeAndDatePacket = new TimeAndDatePacket(packetBytes);
            return timeAndDatePacket;
        }
        else
        {
            throw new IllegalArgumentException("Attempted to parse an L900 PDU of type " + packetId + " as a time and date packet");
        }
    }

    /**
     * Parse raw data as Senet JSON
     * @param rawBytesOfJson Raw bytes received - assumed to be UTF-8 encoded string
     * @return Senet JSON
     * @throws IOException if parsing fails
     */
    public SenetForwardPacketJson parseSenetForwardPacketJson(byte[] rawBytesOfJson) throws IOException
    {
        return parseSenetForwardPacketJson(new String(rawBytesOfJson, StandardCharsets.UTF_8));
    }


    /**
     * Parse raw data as Senet JSON
     * @param jsonString String containing JSON data
     * @return Senet JSON
     * @throws IOException if parsing fails
     */
    public SenetForwardPacketJson parseSenetForwardPacketJson(String jsonString) throws IOException
    {
        return jackson.readValue(jsonString, SenetForwardPacketJson.class);
    }


    /**
     * Parse raw data as Actility JSON
     * @param jsonString String containing JSON data
     * @return Actility JSON
     * @throws IOException if parsing fails
     */
    public ActilityUplinkJson parseActilityForwardPacketJson(String jsonString) throws IOException
    {
        ActilityForwardPacketJson packetJson = jackson.readValue(jsonString, ActilityForwardPacketJson.class);

        return packetJson.getDevEuiUplink();
    }

}
