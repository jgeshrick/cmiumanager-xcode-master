/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A class to represent a packet from Actility
 * Only mapping values that are used
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ActilityForwardPacketJson
{
    @JsonProperty("DevEUI_uplink")
    private ActilityUplinkJson devEuiUplink;

    public ActilityUplinkJson getDevEuiUplink()
    {
        return devEuiUplink;
    }

    public void setDevEuiUplink(ActilityUplinkJson devEuiUplink)
    {
        this.devEuiUplink = devEuiUplink;
    }
}
