/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.pdu;

/**
 * Created by WJD1 on 07/07/2016.
 * A class to represent a generic command packet
 */
public class CommandPacket extends L900Packet
{
    private static final int COMMAND_PACKET_SIZE_BYTES = 11;

    public CommandPacket(byte[] packetBytes)
    {
        super(packetBytes);
    }

    public CommandPacket(CommandPacketTypeEnum commandType)
    {
        super(PacketId.APP_COMMAND_PACKET, COMMAND_PACKET_SIZE_BYTES);

        setNumber(5, commandType.numericValue(), 8);
    }

    public CommandPacketTypeEnum getCommandId()
    {
        return CommandPacketTypeEnum.of((byte) getNumber(5, 8));
    }


    @Override
    public String toString()
    {
        final StringBuilder ret = new StringBuilder("[PDU: COMMAND: packetId=");
        ret.append(getPacketId()).append(" commandId=").append(getCommandId()).append(" length=").append(lengthInBytes()).append(" bytes]");
        return ret.toString();
    }
}
