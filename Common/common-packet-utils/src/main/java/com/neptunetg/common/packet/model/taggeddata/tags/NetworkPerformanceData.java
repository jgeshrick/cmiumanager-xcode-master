/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.packet.model.taggeddata.types.CharArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;

/**
 * Tag 4
 *
 * A char array data tag class specifically for
 * the Network Performance Data. This Data can be parsed.
 *
 * The network performance data tag contains a string
 * formatted as below, which could be parsed:
 *
 * {@code

 * (LTE network)
AT#RFSTS
#RFSTS: <PLMN>,<EARFCN>,<RSRP>,<RSSI>,<RSRQ>,<TAC>,[<TXPWR>],<DRX>,<MM>,<RRC>,<CID>,<IMSI>,[<NetNameAsc>],<SD>,<ABND>[CR,LF] [CR,LF] Parameter

E.g. from a real CMIU: #RFSTS:"311 480",5230,-84,-57,-10,2C02,255,,128,19,0,0ADD502,"311480148913416","Verizon Wireless",2,13,208


GSM Example
Description

PLMN
"262 25"
Country code and operator code(MCC, MNC)

EARFCN
6400
E-UTRA Assigned Radio Channel

RSRP
-99
Reference Signal Received Power

RSSI
-76
Received Signal Strength Indication

RSRQ
-7
Reference Signal Received Quality

TAC
40A5
Tracking Area Code

TXPWR
0
Tx Power (In traffic only)

DRX
64
Discontinuous reception cycle Length (cycle
length : display using ms)

MM
19
Mobility Management

RRC
0
Radio Resource Control

CID
0000007
Cell ID

IMSI
“262011242110776”
International Mobile Station ID

" Telekom.de "
Operation Name, Quoted string type or “” if
network name is unknown

SD
3
Service Domain
(0: No Service, 1: CS only, 2: PS only, 3:
CS+PS)

ABND
20
Active Band (1..63) 3GPP TS 36.101

Note 1 : nSAT : Number of active set. Maximum is 6
Note 2 : If nSAT value is 1, it means that active set number 1. Module does not display
after parameters of nSAT.
Note 3 : TXPWR of GSM network means 1 tx burst
Note 4 : MM - Mobility Management States are:
0: NULL
3: LOCATION_UPDATE_INITIATED
5: WAIT_FOR_OUTGOING_MM_CONNECTION
6: CONNECTION_ACTIVE
7: IMSI_DETACH_INITIATED
8: PROCESS_CM_SERVICE_PROMPT
9: WAIT_FOR_NETWORK_COMMAND
10: LOCATION_UPDATE_REJECTED
13: WAIT_FOR_RR_CONNECTION_LU
14: WAIT_FOR_RR_CONNECTION_MM
15: WAIT_FOR_RR_CONNECTION_IMSI_DETACH
17: REESTABLISHMENT_INITIATED
18: WAIT_FOR_RR_ACTIVE
19: IDLE
20: WAIT_FOR_ADDITIONAL_OUTGOING_MM_CONNECTION
21: WAIT_FOR_RR_CONNECTION_REESTABLISHMENT
22: WAIT_FOR_REESTABLISH_DECISION
23: LOCATION_UPDATING_PENDING
25: CONNECTION_RELEASE_NOT_ALLOWED
Note 5: RR/RRC - Radio Resource Control States are:
0: INACTIVE
1: GOING_ACTIVE
2: GOING_INACTIVE
3: CELL_SELECTION
4: PLMN_LIST_SEARCH
5: IDLE
6: CELL_RESELECTION
7: CONNECTION_PENDING
8: CELL_REESTABLISH
9: DATA_TRANSFER
10: NO_CHANNELS
11: CONNECTION_RELEASE
12: EARLY_CAMPED_WAIT_FOR_SI
13: W2G_INTERRAT_HANDOVER_PROGRESS
14: W2G_INTERRAT_RESELECTION_PROGRESS
15: W2G_INTERRAT_CC_ORDER_PROGRESS
16: G2W_INTERRAT_RESELECTION_PROGRESS
17: WAIT_FOR_EARLY_PSCAN
18: GRR
19: G2W_INTERRAT_HANDOVER_PROGRESS
21: W2G_SERVICE_REDIRECTION_IN_PROGRESS
22: RESET
29: FEMTO
30: X2G_RESEL
31: X2G_RESEL_ABORTED
32: X2G_REDIR
33: G2X_REDIR
34: X2G_CGI
35: X2G_CCO_FAILED
36: X2G_CCO_ABORTED
37: X2G_CCO_FAILED_ABORTED
38: RR_STATE_MAX
 * }
 */

public class NetworkPerformanceData extends CharArrayData
{
    private final int RSRQ_CSV_INDEX = 4;
    private final int MAX_INTERESTING_INDEX = 4;

    private final String data;
    private final int rsrq;

    public NetworkPerformanceData(TagDataType dataType, byte[] data)
    {
        super(TagId.NetworkPerformance, dataType, data);
        this.data = new String(data);


        final int length = this.data.length();
        int offset = 0;
        if (this.data.startsWith("#RFSTS:"))
        {
            offset += "#RFSTS:".length();
        }

        boolean inQuotes = false;
        int currentValueStart = offset;
        int currentValueIndex = 0;
        int newRsrqValue = 9999;

        //parse the string and
        while (offset < length && currentValueIndex <= MAX_INTERESTING_INDEX)
        {
            final int codepoint = this.data.codePointAt(offset);

            switch (codepoint)
            {
                case '"':
                    inQuotes = !inQuotes;
                    break;
                case ',':
                    if (!inQuotes)
                    {
                        final String currentValue = this.data.substring(currentValueStart, offset);
                        if (RSRQ_CSV_INDEX == currentValueIndex)
                        {
                            newRsrqValue = parseRsrq(currentValue);
                        }
                        currentValueIndex++;
                        currentValueStart = offset + 1;
                    }
                    break;
            }
            // do something with the codepoint

            offset += Character.charCount(codepoint);
        }

        this.rsrq = newRsrqValue;
    }

    private static int parseRsrq(String currentValue)
    {
        try
        {
            return Integer.parseInt(currentValue);
        }
        catch (Exception e) //something weird happening in the response
        {
            return 9999;
        }

    }

    @JsonProperty("network_performance_value")
    public String getNetworkPerformanceString()
    {
        return data;
    }

    /**
     * Get the RSRQ value in dB
     * @return RSRQ value in dB
     */
    public int getRsrq()
    {
        return rsrq;
    }

    /**
     * returns the network performance as a string
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder(getTagSummary());
        ret.append(": Network Performance [");
        ret.append("Network Performance String=").append(super.toString());
        ret.append("]");

        return ret.toString();
    }
}
