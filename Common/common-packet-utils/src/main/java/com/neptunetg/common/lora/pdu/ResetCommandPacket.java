/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.lora.pdu;

/**
 * Created by WJD1 on 07/07/2016.
 * A class to represent a reset command packet
 */
public class ResetCommandPacket extends CommandPacket
{
    public ResetCommandPacket()
    {
        super(CommandPacketTypeEnum.Reset);
    }

    public ResetCommandPacket(byte[] packetBytes)
    {
        super(packetBytes);
    }

    @Override
    public String toString()
    {
        final StringBuilder ret = new StringBuilder("[PDU: COMMAND (TIME OFFSET): packetId=");
        ret.append(getPacketId()).append(" commandId=").append(getCommandId())
                .append(" length=").append(lengthInBytes()).append(" bytes]");
        return ret.toString();
    }
}
