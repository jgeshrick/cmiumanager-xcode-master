/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.util;

import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.*;

/**
 * Tests for the HexUtils
 */
public class HexUtilsTest
{
    @Test
    public void testParseHexToByteArray()
    {
        byte[] expected = {1, 3, 4, -1};

        String hexString = "01 0304 ff\r\n";

        assertArrayEquals(expected, HexUtils.parseHexToByteArray(hexString));

        StringBuilder hexBuilder = new StringBuilder();
        for (int i = 0; i < 4088/8; i++)
        {
            hexBuilder.append("ff02ff04ff06FF08"); //8 bytes
        }
        byte[] bytes = HexUtils.parseHexToByteArray(hexBuilder.toString());
        assertEquals(4088, bytes.length);
        assertEquals(-1, bytes[0]);
        assertEquals(2, bytes[1]);

        hexBuilder.append("f");
        try
        {
            HexUtils.parseHexToByteArray(hexBuilder.toString());
            fail("Shouldn't be able to parse odd number of hex characters");
        }
        catch (IllegalArgumentException e)
        {
            //expected
        }

        hexBuilder.append("f02ff04ff06FF08"); //8 bytes
        bytes = HexUtils.parseHexToByteArray(hexBuilder.toString());
        assertEquals(4096, bytes.length);
        assertEquals(-1, bytes[4094]);
        assertEquals(8, bytes[4095]);

        hexBuilder.append("f");
        try
        {
            HexUtils.parseHexToByteArray(hexBuilder.toString());
            fail("Shouldn't be able to parse odd number of hex characters");
        }
        catch (IllegalArgumentException e)
        {
            //expected
        }

        hexBuilder.append("c");
        bytes = HexUtils.parseHexToByteArray(hexBuilder.toString());
        assertEquals(4097, bytes.length);
        assertEquals((byte)0xfc, bytes[4096]);


        hexBuilder.append("1");
        try
        {
            HexUtils.parseHexToByteArray(hexBuilder.toString());
            fail("Shouldn't be able to parse odd number of hex characters");
        }
        catch (IllegalArgumentException e)
        {
            //expected
        }

        assertArrayEquals(new byte[]{}, HexUtils.parseHexToByteArray(null));
    }


    @Test
    public void testByteArrayToHex() {
        byte[] data = {(byte)0xaf, 0x54, 0x12, (byte)0x81};
        assertEquals("af541281", HexUtils.byteArrayToHex(data));
    }


    @Test
    public void testByteBufferToHex() {
        byte[] data = {(byte)0xaf, 0x54, 0x12, (byte)0x81};
        ByteBuffer bb = ByteBuffer.wrap(data);
        assertEquals("af541281", HexUtils.byteBufferToHex(bb));
    }

}
