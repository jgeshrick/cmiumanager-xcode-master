/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model;

import org.junit.Before;
import org.junit.Test;

import java.time.Instant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UnixTimestampTest
{
    private UnixTimestamp unixTimestamp;
    private long currentUnixTimeSeconds;

    @Before
    public void setUp()
    {
        Instant currentInstant = Instant.now();
        currentUnixTimeSeconds = currentInstant.getEpochSecond();
        unixTimestamp = new UnixTimestamp(currentInstant.getEpochSecond());
    }

    @Test
    public void testGetTimestamp() throws Exception
    {
        assertEquals(currentUnixTimeSeconds, unixTimestamp.getEpochSecond());
    }

    @Test
    public void testGetCurrentUnixTimestamp() throws Exception
    {
        Instant now = Instant.now();
        UnixTimestamp unixTimestampNow = UnixTimestamp.getCurrentUnixTimestamp();

        //test getCurrentUnixTimeStamp created current is within expected time 1s in seconds
        assertTrue(Math.abs(now.getEpochSecond() - unixTimestampNow.getEpochSecond()) < 1.0);
    }
}