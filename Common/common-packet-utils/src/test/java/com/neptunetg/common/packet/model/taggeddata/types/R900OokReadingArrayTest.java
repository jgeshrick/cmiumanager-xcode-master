/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.types;

import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class R900OokReadingArrayTest
{
    @Test
    public void testCreateOokReadingArray()
    {
        byte[] packets = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
                51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75};

        R900OokReadingArray ar = new R900OokReadingArray(TagId.R900ReadingsOok, packets, 2);

        assertEquals(2, ar.getReadingCount());

        String[] readingsHex = ar.getReadingsOokHex();

        assertEquals("0102030405060708090A0B0C0D0E0F10111213141516171819".toLowerCase(), readingsHex[0]);
        assertEquals("333435363738393A3B3C3D3E3F404142434445464748494A4B".toLowerCase(), readingsHex[1]);


        packets = new byte[]{-95,
                0,
                40,
                19,
                0,
                87,
                36,
                55,
                36,
                63,

                -82, // date LSB = 0xAE
                -84,  //         = 0xAC
                107, //          = 0x6B
                66, // date MSB  = 0x42.  Date 0x426Bacae = 1114352814 = unix time 1429885614 = 2015-04-24T14:26:54+00:00 in ISO 8601

                -82,
                -84,
                107,
                66,
                0,
                -1,
                -112,
                1,
                0,
                0,
                0};

        ar = new R900OokReadingArray(TagId.R900ReadingsOok, packets, 1);

        assertEquals(1, ar.getReadingCount());

        UnixTimestamp timestamp = ar.getReadTimestamp(0);
        assertEquals(1429885614L, timestamp.getEpochSecond());


    }

}
