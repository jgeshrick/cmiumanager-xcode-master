/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.builder;

import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.CmiuPacketHeaderData;
import com.neptunetg.common.packet.model.taggeddata.tags.DetailedCmiuConfigurationData;
import com.neptunetg.common.packet.model.taggeddata.tags.SecureBlockArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Iterator;

import static org.junit.Assert.*;

public class PacketCommandPacketBuilderTest
{

    @Test
    public void testBuildTaggedPacket() throws Exception
    {
        final byte[] rawCmiuPacketHeaderData = {
                123, 0,                 //utility ID 123
                0, 0, 0, 0,             //CMIU ID - to complete later
                54,                     //sequence number
                0, 0, 0,                //key info, encryption method, token
                3,                      //network flags
                31,                     //RSSI = -51 dBm or greater
                4,                      //BER = 1.6% to 3.2%
                0, 0, 0, 0, 0, 0, 0, 0, //current time and date, to fill in later
        };

        final byte[] rawConfigDataPacketData = {
                10,                         //Made up CMIU type
                0x01, 0x02,                 //CMIU HW revision 1.2
                0x02, 0x03,                 //CMIU Firmware revision 2.3
                0x31, 0x12, 0x15, 0x00,     //CMIU Firmware revision date Dec 31, 2015
                0x56, 0x34, 0x12, 0x00,     //CMIU Firmware build # 123456
                0x03, 0x04,                 //CMIU Bootloader revision 3.4
                0x31, 0x12, 0x15, 0x00,     //CMIU Bootloader revision date Dec 31, 2015
                0x56, 0x34, 0x12, 0x00,     //CMIU Bootloader build # 123456
                0x04, 0x05,                 //CMIU Config revision 4.5
                0x31, 0x12, 0x15, 0x00,     //CMIU Config revision date Dec 31, 2015
                0x56, 0x34, 0x12, 0x00,     //CMIU Config build # 123456
                0, 0, 0, 0, 0, 0, 0, 0,     //CMIU manufacture date substituted below
                60, 0,                      //CMIU Time for initial call in defined in minutes after Midnight UTC, i.e. 0100h
                60, 0,                      //CMIU call in interval
                120, 0,                     //Duration of randomized call in minutes.
                0,                          //window number of retries
                (byte)240, 0,               //Reporting window quiet time start in minutes, 0400h
                360%256, 360-256,           //Reporting window quiet time end in minutes, 0600h
                0,                          //CMIU number of attached devices
                0, 0, 0, 0, 0, 0, 0, 0,     //date of CMIU installation substituted below
                0, 0, 0, 0, 0, 0, 0, 0,     //date of last CMIU mag swipe time substituted below
                2,                          //CMIU total mag swipes
                0, 0,                       //CMIU battery time left substituted below
                1,                          //CMIU time error on last network time access
        };

        final ByteBuffer valueSub = ByteBuffer.wrap(rawConfigDataPacketData).order(ByteOrder.LITTLE_ENDIAN);

        OffsetDateTime lastMagSwipe = OffsetDateTime.of(2015, 6, 22, 15, 31, 0, 0, ZoneOffset.UTC);

        valueSub.putLong(33, OffsetDateTime.of(2015, 4, 20, 10, 31, 0, 0, ZoneOffset.UTC).toEpochSecond()); //manufactured 4/20/2015 10:31am UTC
        valueSub.putLong(53, OffsetDateTime.of(2015, 5, 21, 20, 31, 0, 0, ZoneOffset.UTC).toEpochSecond()); //installed 5/21/2015 8:31pm UTC
        valueSub.putLong(61, lastMagSwipe.toEpochSecond()); //last mag swipe 6/22/2015 3:31pm UTC

        final byte[] secureData = {1,2,3,4,5,6,7,8,9, 10, 11, 12, 13, 14, 15, 16};

        PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.BasicConfigurationStatus)
                .appendTagData(new CmiuPacketHeaderData(TagDataType.ByteRecord, rawCmiuPacketHeaderData))
                .appendTagData(new DetailedCmiuConfigurationData(TagDataType.ByteRecord, rawConfigDataPacketData))
                .appendTagData(new SecureBlockArrayData(TagDataType.ExtendedSecureBlockArray, secureData, 1));

        TaggedPacket tp = packetBuilder.buildTaggedPacket();

        assertEquals(4, tp.tagCount()); //3 tags including EOF
        assertEquals(TaggedDataPacketType.BasicConfigurationStatus.getId(), tp.getPacketId());

        final Iterator<TaggedData> dataIterator = tp.iterator();
        assertTrue(dataIterator.next() instanceof CmiuPacketHeaderData);
        DetailedCmiuConfigurationData configData = (DetailedCmiuConfigurationData) dataIterator.next();
        assertArrayEquals(secureData, ((SecureBlockArrayData) dataIterator.next()).getEncryptedDataCopy());

        assertEquals("2.3.151231.123456", configData.getFirmwareRevision().toString());

        assertEquals(lastMagSwipe.toInstant(), configData.getDateOfLastMagSwipe().asInstant());

    }
}