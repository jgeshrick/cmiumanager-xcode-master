/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.packet.model;

import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Unit test for packet type
 */
public class TaggedDataPacketTypeTest
{
    @Test
    public void testGetPacketType() throws Exception
    {
        TaggedDataPacketType packetType = TaggedDataPacketType.BasicConfigurationStatus;
        assertEquals(packetType, TaggedDataPacketType.getPacketType(packetType.getId()));
    }

    @Test
    public void testGetPacketTypeUnknownId() throws Exception
    {
        TaggedDataPacketType packetType = TaggedDataPacketType.getPacketType((byte) 0xff);
        assertFalse(packetType.isKnown());
    }

}