/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.builder;

import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.MqttPacketHeaderData;
import com.neptunetg.common.packet.model.taggeddata.tags.SecureBlockArrayData;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.R900FskReadingArray;
import com.neptunetg.common.packet.model.taggeddata.types.R900OokReadingArray;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.TagSequenceParser;
import com.neptunetg.common.util.HexUtils;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Iterator;

import static org.junit.Assert.*;


public class R900MiuReadingsPacketBuilderTest
{
    private static final byte[] p1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25};
    private static final byte[] p2 = {51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75};
    private static final byte[] p3 = {101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125};


    @Test
    public void testBuildR900Packet()
    {
        R900MiuReadingsPacketBuilder builder = new R900MiuReadingsPacketBuilder(43);
        builder.addFskPacket(ByteBuffer.wrap(p1));
        builder.addFskPacket(ByteBuffer.wrap(p2));
        builder.addOokPacket(ByteBuffer.wrap(p3));

        TaggedPacket packet = builder.build();

        assertEquals(TaggedDataPacketType.R900MiuReadings, packet.getPacketType());
        Iterator<TaggedData> it = packet.iterator();

        assertTrue(it.hasNext());
        MqttPacketHeaderData header = (MqttPacketHeaderData)it.next();
        assertEquals(43, header.getSequenceNumber());

        assertTrue(it.hasNext());
        SecureBlockArrayData secBlock = (SecureBlockArrayData)it.next();

        assertTrue(it.hasNext());
        assertEquals(TagId.Eof, it.next().getTagId());

        assertFalse(it.hasNext());

        TagSequenceParser parser = new TagSequenceParser();
        TagSequence secureTags = parser.parseTagSequence(secBlock.getEncryptedData());

        System.out.println(secureTags.toJsonPretty());

        it = secureTags.iterator();
        assertTrue(it.hasNext());
        R900OokReadingArray ook = (R900OokReadingArray)it.next();
        assertEquals(1, ook.getReadingCount());
        assertEquals(R900MiuReadingsPacketBuilder.getMiuIdFromOokPacketInBigEndianByteBuffer(ByteBuffer.wrap(p3).order(ByteOrder.BIG_ENDIAN)), ook.getMiuId(0));

        String[] readingsHex = ook.getReadingsOokHex();
        assertEquals(1, readingsHex.length);
        assertEquals(HexUtils.byteArrayToHex(p3), readingsHex[0]);

        assertTrue(it.hasNext());
        R900FskReadingArray fsk = (R900FskReadingArray)it.next();
        assertEquals(2, fsk.getReadingCount());
        assertEquals(R900MiuReadingsPacketBuilder.getMiuIdFromFskPacketInBigEndianByteBuffer(ByteBuffer.wrap(p1).order(ByteOrder.BIG_ENDIAN)), fsk.getMiuId(0));
        assertEquals(R900MiuReadingsPacketBuilder.getMiuIdFromFskPacketInBigEndianByteBuffer(ByteBuffer.wrap(p2).order(ByteOrder.BIG_ENDIAN)), fsk.getMiuId(1));

        readingsHex = fsk.getReadingsFskHex();
        assertEquals(2, readingsHex.length);
        assertEquals(HexUtils.byteArrayToHex(p1), readingsHex[0]);
        assertEquals(HexUtils.byteArrayToHex(p2), readingsHex[1]);

        while (it.hasNext())
        {
            assertEquals(TagId.NullTag, it.next().getTagId());
        }

    }

}
