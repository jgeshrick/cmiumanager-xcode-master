/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.parser;

import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.tags.BatteryVoltage;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.tags.Temperature;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerData;
import com.neptunetg.common.packet.model.taggeddata.types.R900FskReadingArray;
import com.neptunetg.common.packet.model.taggeddata.types.R900OokReadingArray;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.util.HexUtils;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * A set of unit tests for the parser.
 * In each test a byte array is produced
 * that represents a packet containing tagged
 * data. The byte array is then parsed and
 * the result compared to the original input
 * byte array to check that it was parsed
 * correctly.
 */

public class TagSequenceParserTest
{

    @Test
    public void testParseIntegerData()
    {
        final byte[] rawTagData = {
                25,                        //Last BLE login
                4,                         //uint64
                1, 0, 0, 0,                //1
                0, 0, 0, 0                 //CMIU flags, substituted below
        };

        final TagSequenceParser tagSequenceParser = new TagSequenceParser();
        final TagSequence ts = tagSequenceParser.parseTagSequence(rawTagData);

        assertEquals(1, ts.tagCount());
        final IntegerData d = (IntegerData) ts.iterator().next();
        assertEquals(1L, d.getValue());

    }


    @Test
    public void testParseR900Data()
    {
        final byte[] rawTagSequenceData = HexUtils.parseHexToByteArray("3c1201a1000345205373a52a3f38a86b4290c16b4200ff94120000003d130000");

        final TagSequenceParser tagSequenceParser = new TagSequenceParser();
        final TagSequence ts = tagSequenceParser.parseTagSequence(rawTagSequenceData);

        assertEquals(3, ts.tagCount());
        Iterator<TaggedData> it = ts.iterator();

        final R900OokReadingArray ook = (R900OokReadingArray) it.next();
        assertEquals(TagId.R900ReadingsOok, ook.getTagId());

        final R900FskReadingArray fsk = (R900FskReadingArray) it.next();
        assertEquals(TagId.R900ReadingsFsk, fsk.getTagId());

        final TaggedData f = it.next();
        assertEquals(TagId.NullTag, f.getTagId());
    }


    @Test
    public void testParseTruncatedData()
    {
        final byte[][] rawTagSequenceDataList = {
                HexUtils.parseHexToByteArray("3c1201a1000345205373a52a3f38a86b4290c16b4200ff94120000003d130000"),
                {
                        1,
                        5,
                        22,
                        8,
                        1,
                        0,
                        1,
                        0,
                        0,
                        0,
                        3,
                        4,
                        5,
                        6,
                        7,
                        2,
                        0,
                        51,
                        116,
                        9,
                        85,
                        0,
                        0,
                        0,
                        0,
                        2,
                        5,
                        51,
                        7,
                        123,
                        0,
                        -22,
                        0,
                        89,
                        1,
                        28,
                        9,
                        -105,
                        84,
                        0,
                        0,
                        0,
                        0,
                        51,
                        116,
                        9,
                        85,
                        0,
                        0,
                        0,
                        0,
                        60,
                        15,
                        0,
                        -56,
                        1,
                        55,
                        2,
                        0,
                        -108,
                        37,
                        9,
                        85,
                        0,
                        0,
                        0,
                        0,
                        -93,
                        37,
                        9,
                        85,
                        0,
                        0,
                        0,
                        0,
                        1,
                        -90,
                        2,
                        1,
                        -1
                }
        };
        final TagSequenceParser tagSequenceParser = new TagSequenceParser();

        for (int testPacket = 0; testPacket < rawTagSequenceDataList.length; testPacket++)
        {
            byte[] rawTagSequenceData = rawTagSequenceDataList[testPacket];
            TagSequence lastValidTagSequence = null;
            for (int len = 0; len < rawTagSequenceData.length - 1; len++)
            {
                final ByteBuffer truncatedData = ByteBuffer.wrap(rawTagSequenceData, 0, len).order(ByteOrder.LITTLE_ENDIAN);
                try
                {
                    final TagSequence validTagSequence = tagSequenceParser.parseTagSequence(truncatedData);
                    //if we get here, it was not truncated
                    if (lastValidTagSequence != null)
                    {
                        //check same
                        final Iterator<TaggedData> previous = lastValidTagSequence.iterator();
                        final Iterator<TaggedData> latest = validTagSequence.iterator();
                        while (previous.hasNext())
                        {
                            final TaggedData was = previous.next();
                            final TaggedData now = latest.next();
                            assertEquals(was.toJsonPretty(), now.toJsonPretty());
                        }
                    }
                    lastValidTagSequence = validTagSequence;
                } catch (TagSequenceParseException e)
                {
                    System.out.println("Packet truncated to length " + len + ": " + e.getMessage());
                    assertNull(e.getCause());
                } catch (Exception e)
                {
                    e.printStackTrace();
                    fail("Unexpected exception " + e);
                }

            }
        }
    }


    @Test
    public void testParseTemperatureAndBatteryVoltageData()
    {
        final byte[] rawTagSequenceData = {
                62, 2, //battery voltage, UInt16
                0x22, 0x02, //ADC reading 546 = 0x222
                63, 1, //CMIU temperature, UInt8
                -1 //0xff = -1 degrees C
        };

        final TagSequenceParser tagSequenceParser = new TagSequenceParser();
        final TagSequence ts = tagSequenceParser.parseTagSequence(rawTagSequenceData);

        assertEquals(2, ts.tagCount());
        Iterator<TaggedData> it = ts.iterator();

        final BatteryVoltage bv = (BatteryVoltage) it.next();
        assertEquals(TagId.BatteryVoltage, bv.getTagId());
        assertEquals(0x222, bv.getValue());
        assertEquals(999.755859375, bv.getMillivolts(), 0.0001);

        final Temperature temp = (Temperature) it.next();
        assertEquals(TagId.Temperature, temp.getTagId());
        assertEquals(255, temp.getValue());
        assertEquals(-1, temp.getDegreesC());

    }

}
