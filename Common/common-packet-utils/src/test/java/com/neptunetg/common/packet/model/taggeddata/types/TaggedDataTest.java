/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.types;

import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TaggedDataTest
{

    @Test
    public void testSerialise() throws Exception
    {
        TaggedData td = new TaggedData(TagId.Eof);
        byte[] serialisedData = td.serialize();

        assertEquals(1, serialisedData.length);
        assertEquals((byte)TagId.Eof.getId(), serialisedData[0]);
    }
}