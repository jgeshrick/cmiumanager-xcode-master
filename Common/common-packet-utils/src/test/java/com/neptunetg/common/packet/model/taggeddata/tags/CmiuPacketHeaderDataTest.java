/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.neptunetg.common.packet.model.UnixTimestamp;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class CmiuPacketHeaderDataTest
{
    /*
    RSSI
        0 - (-113) dBm or less
    1 - (-111) dBm
    2..30 - (-109)dBm..(-53)dBm / 2 dBm per step
    31 - (-51)dBm or greater
    99 - not known or not detectable

     * bit error rate
     * @return
    0 - less than 0.2%
    1 - 0.2% to 0.4%
    2 - 0.4% to 0.8%
    3 - 0.8% to 1.6%
    4 - 1.6% to 3.2%
    5 - 3.2% to 6.4%
    6 - 6.4% to 12.8%
    7 - more than 12.8%
    99 - not known or not detectable
     */
    @Test
    public void testRssiAndBer()
    {
        CmiuPacketHeaderData packetHeaderData = makeTag(0, 0);

        assertEquals(Integer.valueOf(-113), packetHeaderData.getRssiDbm());
        assertEquals(Double.valueOf(0.0), packetHeaderData.getBerPercentageMin());
        assertEquals(Double.valueOf(0.2), packetHeaderData.getBerPercentageMax());

        packetHeaderData = makeTag(1, 1);

        assertEquals(Integer.valueOf(-111), packetHeaderData.getRssiDbm());
        assertEquals(Double.valueOf(0.2), packetHeaderData.getBerPercentageMin());
        assertEquals(Double.valueOf(0.4), packetHeaderData.getBerPercentageMax());

        packetHeaderData = makeTag(31, 6);

        assertEquals(Integer.valueOf(-51), packetHeaderData.getRssiDbm());
        assertEquals(Double.valueOf(6.4), packetHeaderData.getBerPercentageMin());
        assertEquals(Double.valueOf(12.8), packetHeaderData.getBerPercentageMax());

        packetHeaderData = makeTag(32, 7);

        assertNull(packetHeaderData.getRssiDbm());
        assertEquals(Double.valueOf(12.8), packetHeaderData.getBerPercentageMin());
        assertEquals(Double.valueOf(25.6), packetHeaderData.getBerPercentageMax());

        packetHeaderData = makeTag(99, 8);

        assertNull(packetHeaderData.getRssiDbm());
        assertNull(packetHeaderData.getBerPercentageMin());
        assertNull(packetHeaderData.getBerPercentageMax());

        packetHeaderData = makeTag(99, 99);

        assertNull(packetHeaderData.getRssiDbm());
        assertNull(packetHeaderData.getBerPercentageMin());
        assertNull(packetHeaderData.getBerPercentageMax());

    }

    private static CmiuPacketHeaderData makeTag(int rssi, int ber)
    {
        return new CmiuPacketHeaderData(1 /*cmiu id*/,
                2 /*seq no*/,
                3 /*keyInfo */,
                4 /*encryptionMethod */,
                5 /* token */,
                6 /*networkFlags */,
                rssi /* rssi */,
                ber,
                new UnixTimestamp(4));
    }

}