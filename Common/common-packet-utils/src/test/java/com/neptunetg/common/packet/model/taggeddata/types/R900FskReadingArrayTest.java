/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.types;

import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class R900FskReadingArrayTest
{
    @Test
    public void testCreateFskReadingArray()
    {
        final byte[] packets = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
                51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75};

        R900FskReadingArray ar = new R900FskReadingArray(TagId.R900ReadingsFsk, packets, 2);

        assertEquals(2, ar.getReadingCount());

        String[] readingsHex = ar.getReadingsFskHex();
        assertEquals("0102030405060708090A0B0C0D0E0F10111213141516171819".toLowerCase(), readingsHex[0]);
        assertEquals("333435363738393A3B3C3D3E3F404142434445464748494A4B".toLowerCase(), readingsHex[1]);

    }

}
