/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;
import org.junit.Test;

import static org.junit.Assert.*;

public class MqttPacketHeaderDataTest
{
    @Test
    public void testDeserialiseData()
    {
        final byte[] rawTagData = {
                0x01,   //sequence number
                0x02,   //key info
                0x03,   //encryption info
                0x10,   //token
        };

        MqttPacketHeaderData mqttPacketHeaderData = new MqttPacketHeaderData(TagDataType.ByteRecord, rawTagData);

        assertEquals(rawTagData[0], mqttPacketHeaderData.getSequenceNumber());
        assertEquals(rawTagData[1], mqttPacketHeaderData.getKeyInfo());
        assertEquals(rawTagData[2], mqttPacketHeaderData.getEncryptionMethod());
        assertEquals(rawTagData[3], mqttPacketHeaderData.getToken());

        assertEquals("packet header for messages sent to CMIU (4 item ByteRecord): MQTT Packet Header [Sequence number=1, Key info=2, Encryption method=3, Token=16]",
                mqttPacketHeaderData.toString());
    }

    @Test
    public void testPacketBuilder()
    {
        final byte[] rawTagData = {
                101,   //sequence number
                102,   //key info
                103,   //encryption info
                (byte) 200,   //token
        };

        MqttPacketHeaderData mqttPacketHeaderData =
                new MqttPacketHeaderData.Builder()
                        .sequenceNumber(rawTagData[0])
                        .keyInfo(rawTagData[1])
                        .encryptionMethod(rawTagData[2])
                        .token(rawTagData[3])
                        .build();

        assertEquals(rawTagData[0], mqttPacketHeaderData.getSequenceNumber());
        assertEquals(rawTagData[1], mqttPacketHeaderData.getKeyInfo());
        assertEquals(rawTagData[2], mqttPacketHeaderData.getEncryptionMethod());
        assertEquals(rawTagData[3], (byte) (mqttPacketHeaderData.getToken() & 0xff));      //convert getToken to unsigned byte by masking higher byte

        assertArrayEquals(rawTagData, mqttPacketHeaderData.getDataBuffer().array());
    }

    @Test
    public void testSerialize()
    {
        final byte[] rawTagData = {
                101,   //sequence number
                102,   //key info
                103,   //encryption info
                (byte) 200,   //token
        };

        MqttPacketHeaderData mqttPacketHeaderData =
                new MqttPacketHeaderData.Builder()
                        .sequenceNumber(rawTagData[0])
                        .keyInfo(rawTagData[1])
                        .encryptionMethod(rawTagData[2])
                        .token(rawTagData[3])
                        .build();

        byte[] deserialisedRawData = mqttPacketHeaderData.getDataBuffer().array();
        assertEquals(35, mqttPacketHeaderData.getTagId().getId());
        assertEquals(5, mqttPacketHeaderData.getDataType().getId());
        assertEquals(4, mqttPacketHeaderData.getDataBuffer().capacity());

        assertEquals(rawTagData[0], deserialisedRawData[0]);    // seq number
        assertEquals(rawTagData[1], deserialisedRawData[1]);    // key info
        assertEquals(rawTagData[2], deserialisedRawData[2]);    // encryption method
        assertEquals(rawTagData[3], deserialisedRawData[3]);    // token
    }

    @Test
    public void testBuilderThrowExceptionIfFieldsNotDefined()
    {
        MqttPacketHeaderData.Builder mqttPacketHeaderDataBuilder =
                new MqttPacketHeaderData.Builder();

        buildMqttPacketExpectException(mqttPacketHeaderDataBuilder);

        mqttPacketHeaderDataBuilder = mqttPacketHeaderDataBuilder.sequenceNumber(0);
        buildMqttPacketExpectException(mqttPacketHeaderDataBuilder);

        mqttPacketHeaderDataBuilder = mqttPacketHeaderDataBuilder.keyInfo(0);
        buildMqttPacketExpectException(mqttPacketHeaderDataBuilder);

        mqttPacketHeaderDataBuilder = mqttPacketHeaderDataBuilder.encryptionMethod(0);
        buildMqttPacketExpectException(mqttPacketHeaderDataBuilder);

        mqttPacketHeaderDataBuilder = mqttPacketHeaderDataBuilder.token(0);
        MqttPacketHeaderData mqttPacketHeaderData = mqttPacketHeaderDataBuilder.build();

        assertNotNull(mqttPacketHeaderData);
    }

    /**
     * Short hand to build a HostPacketHeader but expect exception to be thrown.
     * @param mqttPacketHeaderDataBuilder the builder which has not been completely defined
     */
    private void buildMqttPacketExpectException(MqttPacketHeaderData.Builder mqttPacketHeaderDataBuilder)
    {
        try
        {
            mqttPacketHeaderDataBuilder.build();
            fail("Expected IllegalArgumentException");
        }
        catch (IllegalArgumentException e)
        {
        }
    }


}