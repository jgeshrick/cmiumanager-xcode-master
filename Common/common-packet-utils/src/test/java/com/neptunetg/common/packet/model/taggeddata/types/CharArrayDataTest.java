/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.types;

import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CharArrayDataTest
{

    @Test
    public void testSerialise() throws Exception
    {
        byte[] data = new byte[]{1,2,3,4};
        final CharArrayData charArrayData = new CharArrayData(TagId.CmiuPacketHeader, TagDataType.CharArray, data);

        byte[] serializedData = charArrayData.serialize();

        assertEquals(7, serializedData.length); //data length + tag number, type, array size
        assertEquals((byte)TagId.CmiuPacketHeader.getId(), serializedData[0]);
        assertEquals(TagDataType.CharArray.getId(), serializedData[1]);
        assertEquals(data.length, serializedData[2]);
        assertEquals(data[0], serializedData[3]);
        assertEquals(data[1], serializedData[4]);
        assertEquals(data[2], serializedData[5]);
        assertEquals(data[3], serializedData[6]);

    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void testStringTooLong() throws Exception
    {
        byte[] data = new byte[256];
        for (int i = 0; i<256; i++)
        {
            data[i] = 1;
        }

        final CharArrayData charArrayData = new CharArrayData(TagId.MsisdnRequest, TagDataType.CharArray, data);

        byte[] serializedData = charArrayData.serialize();
    }


    @Test
    public void testGetAsString() throws Exception
    {
        final CharArrayData charArrayData = new CharArrayData(TagId.MsisdnRequest, TagDataType.CharArray, "Testy");
        assertEquals("Testy", charArrayData.getAsString());
    }


    @Test
    public void testNonAscii7() throws Exception
    {
        final byte[] data = new byte[128];
        for (int i = 0; i < 128; i++)
        {
            data[i] = (byte)(i + 128);
        }
        final CharArrayData charArrayData = new CharArrayData(TagId.MsisdnRequest, TagDataType.CharArray, data);
        assertEquals(new String(data, "ISO-8859-1"), charArrayData.getAsString());
        assertNotNull(charArrayData.getWarning());

    }
}