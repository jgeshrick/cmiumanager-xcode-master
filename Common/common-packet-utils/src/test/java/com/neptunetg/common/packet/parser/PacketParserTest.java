/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.packet.parser;

import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.substructures.R900Reading;
import com.neptunetg.common.packet.model.taggeddata.tags.*;
import com.neptunetg.common.packet.model.taggeddata.types.*;
import com.neptunetg.common.util.HexUtils;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by ABH1 on 06/02/2015.
 * <p/>
 * A set of unit tests for the parser.
 * In each test a byte array is produced
 * that represents a packet containing tagged
 * data. The byte array is then parsed and
 * the result compared to the original input
 * byte array to check that it was parsed
 * correctly.
 */

public class PacketParserTest
{
    @Test
    public void testParseCmiuHeaderData()
    {

        final byte[] rawPacketData = {(byte) 255, //made-up packet ID
                1,                      //CMIU packet header tag ID
                5,                      //byte array tag type
                19,                     //size of following data in bytes
        /*+4*/  0, 0, 0, 0,             //CMIU ID - to complete later
        /*+8*/ 54,                     //sequence number
        /*+8*/ 0, 0, 0,                //key info, encryption method, token
        /*+12*/ 3,                      //network flags
        /*+13*/ 31,                     //RSSI = -51 dBm or greater
        /*+14*/ 4,                      //BER = 1.6% to 3.2%
        /*+15*/ 0, 0, 0, 0, 0, 0, 0, 0, //current time and date, to fill in later
                (byte) 255               //EOF - only the header tag is present
        };

        ByteBuffer packetData = ByteBuffer.wrap(rawPacketData).order(ByteOrder.LITTLE_ENDIAN);
        packetData.putInt(4, 5000002); //CMIUID = 5,000,002

        long unixTimestamp = 1423248037L;
        packetData.putLong(15, unixTimestamp);

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertEquals(255, tp.getPacketId());

        assertEquals(2, tp.tagCount());

        Iterator<TaggedData> tagIterator = tp.iterator();

        TaggedData tag = tagIterator.next();

        assertTrue(tag instanceof CmiuPacketHeaderData);

        System.out.println("First tag: " + tag.toString());

        CmiuPacketHeaderData packetHeader = (CmiuPacketHeaderData) tag;

        assertEquals(5000002, packetHeader.getCmiuId());
        assertEquals(54, packetHeader.getSequenceNumber());
        assertEquals(0, packetHeader.getKeyInfo());
        assertEquals(0, packetHeader.getEncryptionMethod());
        assertEquals(0, packetHeader.getToken());
        assertEquals(3, packetHeader.getNetworkFlags());
        assertEquals(31, packetHeader.getRssi());
        assertEquals(4, packetHeader.getBer());
        assertEquals(unixTimestamp, packetHeader.getCurrentTimeAndDate().getEpochSecond());

        tag = tagIterator.next();

        System.out.println("Second tag: " + tag.toString());

        assertEquals(TagId.Eof, tag.getTagId());

    }

    /**
     * Test parser can parse packet with secure data. Now ignore content of secure data.
     */
    @Test
    public void testParseBasicConfigPacketDefWithSecureData()
    {

        final byte[] rawPacketData = {(byte) 255, //made-up packet ID
                1,                      //CMIU packet header tag ID
                5,                      //byte array tag type
                19,                     //size of following data in bytes
                0, 0, 0, 0,             //CMIU ID - to complete later
                54,                     //sequence number
                0, 0, 0,                //key info, encryption method, token
                3,                      //network flags
                31,                     //RSSI = -51 dBm or greater
                4,                      //BER = 1.6% to 3.2%
                0, 0, 0, 0, 0, 0, 0, 0, //current time and date, to fill in later
                (byte) 254, 0x0d, 1, 0, //begin secure data, 1 16 byte block
                1, 2, 3, 4, 5, 6, 7, 8,
                9, 10, 11, 12, 13, 14, 15, 16,
                (byte) 255               //EOF
        };

        ByteBuffer packetData = ByteBuffer.wrap(rawPacketData).order(ByteOrder.LITTLE_ENDIAN);
        packetData.putInt(4, 5000002); //CMIUID = 5,000,002

        long unixTimestamp = 1423248037L;
        packetData.putLong(15, unixTimestamp);

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertEquals(255, tp.getPacketId());

        assertEquals(3, tp.tagCount()); //header, secure data, EOF

        Iterator<TaggedData> tagIterator = tp.iterator();

        TaggedData tag = tagIterator.next();

        assertTrue(tag instanceof CmiuPacketHeaderData);

        System.out.println("First tag: " + tag.toString());

        CmiuPacketHeaderData packetHeader = (CmiuPacketHeaderData) tag;

        assertEquals(5000002, packetHeader.getCmiuId());
        assertEquals(54, packetHeader.getSequenceNumber());
        assertEquals(0, packetHeader.getKeyInfo());
        assertEquals(0, packetHeader.getEncryptionMethod());
        assertEquals(0, packetHeader.getToken());
        assertEquals(3, packetHeader.getNetworkFlags());
        assertEquals(31, packetHeader.getRssi());
        assertEquals(4, packetHeader.getBer());

        //secure data tag
        tag = tagIterator.next();
        assertEquals(TagId.SecureDataBlockArray, tag.getTagId());

        SecureBlockArrayData secureData = (SecureBlockArrayData) tag;

        assertEquals(16, secureData.getEncryptedDataLengthInBytes());

        assertArrayEquals(new byte[]{
                1, 2, 3, 4, 5, 6, 7, 8,
                9, 10, 11, 12, 13, 14, 15, 16,
        }, secureData.getEncryptedDataCopy());


        //eof tag
        tag = tagIterator.next();
        assertEquals(TagId.Eof, tag.getTagId());

    }

    @Test
    public void testParseDetailedCmiuConfigurationData() throws Exception
    {

        final byte[] rawPacketData = {(byte) 0, //+0 packet id: detailed configuration/status
                2,                          //+1 CMIU configuration data tag ID
                5,                          //+2 byte array tag type
                (byte) DetailedCmiuConfigurationData.SIZE_IN_BYTES,//+3 size of following data in bytes
                10,                         //+4+0 Made up CMIU type
                0x01, 0x02,                 //+4+1 CMIU HW revision 1.2
                0x02, 0x03,                 //+4+3 CMIU Firmware revision 2.3
                0x31, 0x12, 0x15, 0x00,     //+4+5 CMIU Firmware revision date Dec 31, 2015
                0x56, 0x34, 0x12, 0x00,     //+4+9 CMIU Firmware build # 123456
                0x03, 0x04,                 //+4+13 CMIU Bootloader revision 3.4
                0x31, 0x12, 0x15, 0x00,     //+4+15 CMIU Bootloader revision date Dec 31, 2015
                0x56, 0x34, 0x12, 0x00,     //+4+19 CMIU Bootloader build # 123456
                0x04, 0x05,                 //+4+23 CMIU Config revision 4.5
                0x31, 0x12, 0x15, 0x00,     //+4+25 CMIU Config revision date Dec 31, 2015
                0x56, 0x34, 0x12, 0x00,     //+4+29 CMIU Config build # 123456
                0, 0, 0, 0, 0, 0, 0, 0,     //+4+33 CMIU manufacture date substituted below
                60, 0,                      //+4+41 CMIU initial call in time - 0100h;
                60, 0,                      //+4+43 CMIU call in interval - 1h
                120, 0,                     //+4+45 CMIU call in window end - 0200h
                0,                          //+4+47 window number of retries
                (byte)180, 0,               //+4+48 Reporting window quiet time start in minutes - 0300h
                (byte)240, 0,               //+4+50 Reporting window quiet time end in minutes - 0400h
                0,                          //+4+52 CMIU number of attached devices
                0, 0, 0, 0, 0, 0, 0, 0,     //+4+53 date of CMIU installation substituted below
                0, 0, 0, 0, 0, 0, 0, 0,     //+4+61 date of last CMIU mag swipe time substituted below
                2,                          //+4+69 CMIU total mag swipes
                12, 0,                      //+4+70 CMIU battery time left - 12
                1,                          //+4+72 CMIU time error on last network time access
                (byte) 255                  //+77   EOF - only the header tag is present
        };

        assertEquals(1 + 3 + DetailedCmiuConfigurationData.SIZE_IN_BYTES + 1, rawPacketData.length);

        ByteBuffer packetData = ByteBuffer.wrap(rawPacketData).order(ByteOrder.LITTLE_ENDIAN);

        long unixTimestampManufacture = 1423248038L;
        packetData.putLong(+4 + 33, unixTimestampManufacture); //Set CMIU manufacture data
        long unixTimestampInstall = 1423248050L;
        packetData.putLong(+4 + 53, unixTimestampInstall); //Set CMIU install time
        long unixTimestampMagSwipe = 1423254631L;
        packetData.putLong(+4 + 61, unixTimestampMagSwipe); //Set CMIU last mag swipe time

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertEquals((byte) 0, tp.getPacketId());
        assertEquals(2, tp.tagCount());

        Iterator<TaggedData> tagIterator = tp.iterator();

        TaggedData tag = tagIterator.next();

        assertTrue(tag instanceof DetailedCmiuConfigurationData);

        System.out.println("First tag: " + tag.toString());

        final DetailedCmiuConfigurationData packetConfigurationData = (DetailedCmiuConfigurationData) tag;

        assertEquals(10, packetConfigurationData.getCmiuType());
        assertEquals(2 * 256 + 1, packetConfigurationData.getDeviceHwRevision());

        final DetailedCmiuConfigurationData.ImageRevisionInfo fwRevision = packetConfigurationData.getFirmwareRevision();
        assertEquals(2, fwRevision.getMajorRelease());
        assertEquals(3, fwRevision.getMinorRelease());
        assertEquals(151231, fwRevision.getRevDate());
        assertEquals(123456, fwRevision.getBuildRev());

        final DetailedCmiuConfigurationData.ImageRevisionInfo blRevision = packetConfigurationData.getBootloaderVersion();
        assertEquals(3, blRevision.getMajorRelease());
        assertEquals(4, blRevision.getMinorRelease());
        assertEquals(151231, blRevision.getRevDate());
        assertEquals(123456, blRevision.getBuildRev());

        final DetailedCmiuConfigurationData.ImageRevisionInfo cfgRevision = packetConfigurationData.getConfigVersion();
        assertEquals(4, cfgRevision.getMajorRelease());
        assertEquals(5, cfgRevision.getMinorRelease());
        assertEquals(151231, cfgRevision.getRevDate());
        assertEquals(123456, cfgRevision.getBuildRev());

        assertEquals(unixTimestampManufacture, packetConfigurationData.getManufactureDate().getEpochSecond());
        assertEquals(60, packetConfigurationData.getReportingStartTime());
        assertEquals(60, packetConfigurationData.getReportingInterval());
        assertEquals(120, packetConfigurationData.getReportingWindow());
        assertEquals(0, packetConfigurationData.getReportingWindowNumberOfRetries());
        assertEquals(180, packetConfigurationData.getMinsToReportingWindowQuietTime());
        assertEquals(240, packetConfigurationData.getMinsToReportingWindowQuietTimeEnd());
        assertEquals(0, packetConfigurationData.getNumberOfAttachedDevices());
        assertEquals(unixTimestampInstall, packetConfigurationData.getDateOfInstallation().getEpochSecond());
        assertEquals(unixTimestampMagSwipe, packetConfigurationData.getDateOfLastMagSwipe().getEpochSecond());
        assertEquals(2, packetConfigurationData.getMagSwipeCounter());
        assertEquals(12, packetConfigurationData.getEstimateBatteryCapacityRemaining());
        assertEquals(1, packetConfigurationData.getTimeErrorOnLastNetworkTimeAccess());

        tag = tagIterator.next();

        System.out.println("Second tag: " + tag.toString());

        assertEquals(TagId.Eof, tag.getTagId());

    }

    @Test
    public void testR900IntervalData()
    {
        final byte[] rawPacketData = {(byte) 1, //Interval data packet type
        28, //R900 Interval Data Tag Type
        0x0B, //Extended Uint32 array
        5, 0, //Followed by 5 Uint32's
        0x00, 0x00, 0x28, (byte) 0xFA,
        0x01, 0x00, 0x29,0x04,
        0x02, 0x00, 0x30, (byte) 0xE8,
        0x09, 0x00, 0x39, (byte) 0x8A,
        0x08, 0x00, 0x3A, 0x3E,
        (byte) 255
        };

        System.out.println(HexUtils.byteArrayToHex(rawPacketData));

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertEquals(1, tp.getPacketId());
        assertEquals(2, tp.tagCount());

        Iterator<TaggedData> tagIterator = tp.iterator();

        TaggedData tag = tagIterator.next();

        assertTrue(tag instanceof R900IntervalData);

        System.out.println("First tag: " + tag.toString());

        R900IntervalData intervalData = (R900IntervalData) tag;

        List<R900Reading> readings = intervalData.getReadings();

        assertEquals(5, readings.size());

        assertEquals(10490, readings.get(0).getReading());
        assertEquals(0, readings.get(0).getLeakDetection());
        assertEquals(0, readings.get(0).getPeakBackflow());

        assertEquals(10500, readings.get(1).getReading());
        assertEquals(1, readings.get(1).getLeakDetection());
        assertEquals(0, readings.get(1).getPeakBackflow());

        assertEquals(12520, readings.get(2).getReading());
        assertEquals(2, readings.get(2).getLeakDetection());
        assertEquals(0, readings.get(2).getPeakBackflow());

        assertEquals(14730, readings.get(3).getReading());
        assertEquals(1, readings.get(3).getLeakDetection());
        assertEquals(2, readings.get(3).getPeakBackflow());

        assertEquals(14910, readings.get(4).getReading());
        assertEquals(0, readings.get(4).getLeakDetection());
        assertEquals(2, readings.get(4).getPeakBackflow());

        String json = ParserUtil.parsePacketToJsonPretty(HexUtils.byteArrayToHex(rawPacketData));

        System.out.println(json);
    }

    @Test
    public void testCmiuDiagnosticsData()
    {

        final byte[] rawPacketData = {(byte) 255, //made-up packet ID
                23,                         //CMIU diagnostics tag ID
                5,                         //byte array tag type
                5,                         //size of following data in bytes
                0, 0, 0, 0,                //CMIU flags, substituted below
                7,                         //Made up reset count
                (byte) 255                  //EOF - only the header tag is present
        };
        ByteBuffer packetData = ByteBuffer.wrap(rawPacketData).order(ByteOrder.LITTLE_ENDIAN);
        packetData.putInt(4, 6735); //CMIUID = 5,000,002

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertEquals(255, tp.getPacketId());
        assertEquals(2, tp.tagCount());

        Iterator<TaggedData> tagIterator = tp.iterator();

        TaggedData tag = tagIterator.next();

        assertTrue(tag instanceof CmiuDiagnosticsData);

        System.out.println("First tag: " + tag.toString());

        CmiuDiagnosticsData packetCmiuDiagnosticsData = (CmiuDiagnosticsData) tag;

        assertEquals(6735, packetCmiuDiagnosticsData.getDiagnosticFlags());
        assertEquals(7, packetCmiuDiagnosticsData.getProcessorResetCount());

        tag = tagIterator.next();

        System.out.println("Second tag: " + tag.toString());

        assertEquals(TagId.Eof, tag.getTagId());
    }

    /**
     * Test for parsing of tag 27 - new format
     */
    @Test
    public void testReportedDeviceConfigurationDataNewFormat()
    {

        final byte[] rawPacketData = {(byte) 255, //made-up packet ID
                27,                        //Device configuration tag ID
                5,                         //byte array tag type
                17,                        //size of following data in bytes
                1,                         //+0 UInt8 made up device number
                0, 0, 0, 0, 0, 0, 0, 0,    //+1 UInt64 Attached Device ID, substituted below
                0, 0,                      //+9 UInt16 Device Type, substituted below
                0, 0, 0, 0,                //+11 UInt32 Current device data
                0, 0,                      //+15 Device Flags, substituted below
                (byte) 255                  //EOF - only the header tag is present
        };
        ByteBuffer packetData = ByteBuffer.wrap(rawPacketData).order(ByteOrder.LITTLE_ENDIAN);

        final long attachedDeviceId = 8292734234L;
        final short deviceType = 141;
        final int currentDeviceData = 14213425;
        final short deviceFlags = 4252;
        packetData.putLong(4 + 1, attachedDeviceId);
        packetData.putShort(4 + 9, deviceType);
        packetData.putInt(4 + 11, currentDeviceData);
        packetData.putShort(4 + 15, deviceFlags);

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertEquals(255, tp.getPacketId());
        assertEquals(2, tp.tagCount());

        Iterator<TaggedData> tagIterator = tp.iterator();

        TaggedData tag = tagIterator.next();

        assertTrue(tag instanceof ReportedDeviceConfigurationData);

        System.out.println("First tag: " + tag.toString());

        ReportedDeviceConfigurationData DeviceConfigurationData = (ReportedDeviceConfigurationData) tag;

        assertEquals(attachedDeviceId, DeviceConfigurationData.getAttachedDeviceId());
        assertEquals(deviceType, DeviceConfigurationData.getDeviceType());
        assertEquals(Integer.valueOf(currentDeviceData), DeviceConfigurationData.getCurrentDeviceData());
        assertEquals(deviceFlags, DeviceConfigurationData.getDeviceFlags());

        tag = tagIterator.next();

        System.out.println("Second tag: " + tag.toString());

        assertEquals(TagId.Eof, tag.getTagId());
    }


    /**
     * Test for parsing of tag 27 - old 13 byte format
     */
    @Test
    public void testReportedDeviceConfigurationDataOldFormat()
    {

        final byte[] rawPacketData = {(byte) 255, //made-up packet ID
                27,                        //Device configuration tag ID
                5,                         //byte array tag type
                13,                        //size of following data in bytes
                1,                         //+0 UInt8 made up device number
                0, 0, 0, 0, 0, 0, 0, 0,    //+1 UInt64 Attached Device ID, substituted below
                0, 0,                      //+9 UInt16 Device Type, substituted below
                0, 0,                      //+11 Device Flags, substituted below
                (byte) 255                  //EOF - only the header tag is present
        };
        ByteBuffer packetData = ByteBuffer.wrap(rawPacketData).order(ByteOrder.LITTLE_ENDIAN);

        final long attachedDeviceId = 8292734234L;
        final short deviceType = 141;
        final short deviceFlags = 4252;
        packetData.putLong(4 + 1, attachedDeviceId);
        packetData.putShort(4 + 9, deviceType);
        packetData.putShort(4 + 11, deviceFlags);

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertEquals(255, tp.getPacketId());
        assertEquals(2, tp.tagCount());

        Iterator<TaggedData> tagIterator = tp.iterator();

        TaggedData tag = tagIterator.next();

        assertTrue(tag instanceof ReportedDeviceConfigurationData);

        System.out.println("First tag: " + tag.toString());

        ReportedDeviceConfigurationData DeviceConfigurationData = (ReportedDeviceConfigurationData) tag;

        assertEquals(attachedDeviceId, DeviceConfigurationData.getAttachedDeviceId());
        assertEquals(deviceType, DeviceConfigurationData.getDeviceType());
        assertNull(DeviceConfigurationData.getCurrentDeviceData());
        assertEquals(deviceFlags, DeviceConfigurationData.getDeviceFlags());

        tag = tagIterator.next();

        System.out.println("Second tag: " + tag.toString());

        assertEquals(TagId.Eof, tag.getTagId());
    }


    @Test
    public void testConnectionTimingLogData()
    {

        final byte[] rawPacketData = {(byte) 255, //made-up packet ID
                31,                        //Connection timing log tag ID
                5,                         //byte array tag type
                18,                        //size of following data in bytes
                0, 0, 0, 0, 0, 0, 0, 0,    //Date and Time of the connection, substituted below
                0, 0,                      //Time from power on to register on Verizon network, substituted below
                0, 0,                      //Time from register on Verizon to activate context, substituted below
                0, 0,                      //Time from context activation to server connection, substituted below
                0, 0,                      //Time to transfer a data message to the server, substituted below
                0, 0,                      //Time to disconnect and shutdown, substituted below
                (byte) 255                  //EOF - only the header tag is present
        };
        ByteBuffer packetData = ByteBuffer.wrap(rawPacketData).order(ByteOrder.LITTLE_ENDIAN);

        long unixTimestamp = 1423248037L;
        packetData.putLong(4, unixTimestamp);

        packetData.putShort(12, (short) 1000);
        packetData.putShort(14, (short) 1200);
        packetData.putShort(16, (short) 1400);
        packetData.putShort(18, (short) 1600);
        packetData.putShort(20, (short) 1800);

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertEquals(255, tp.getPacketId());
        assertEquals(2, tp.tagCount());

        Iterator<TaggedData> tagIterator = tp.iterator();

        TaggedData tag = tagIterator.next();

        assertTrue(tag instanceof ConnectionTimingLogData);

        System.out.println("First tag: " + tag.toString());

        ConnectionTimingLogData ConnectionTimingLog = (ConnectionTimingLogData) tag;

        assertEquals(unixTimestamp, ConnectionTimingLog.getDateOfConnection().getEpochSecond());
        assertEquals(1000, ConnectionTimingLog.getTimeFromPowerOnToNetworkRegistration());
        assertEquals(1200, ConnectionTimingLog.getTimeFromNetworkRegistrationToContextActivation());
        assertEquals(1400, ConnectionTimingLog.getTimeFromContextActivationToServerConnection());
        assertEquals(1600, ConnectionTimingLog.getTimeToTransferMessageToServer());
        assertEquals(1800, ConnectionTimingLog.getTimeToDisconnectAndShutdown());

        tag = tagIterator.next();

        System.out.println("Second tag: " + tag.toString());

        assertEquals(TagId.Eof, tag.getTagId());

        

    }

    @Test
    public void testNetworkOperatorsData()
    {

        final byte[] rawPacketData = {(byte) 255, //made-up packet ID
                3,                         //Network Operators tag type ID
                8,                         //char array tag type
                0,                         //char array length, substituted below
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,       //Space for a string to be substituted below
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0,
                (byte) 255                  //EOF - only the header tag is present
        };
        ByteBuffer packetData = ByteBuffer.wrap(rawPacketData).order(ByteOrder.LITTLE_ENDIAN);

        byte[] testString = "This is a test string".getBytes();
        packetData.put(3, (byte) testString.length);
        for (int i = 0; i < testString.length; i++)
        {
            packetData.put(4 + i, testString[i]);
        }

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertEquals(255, tp.getPacketId());
        assertEquals(2, tp.tagCount());

        Iterator<TaggedData> tagIterator = tp.iterator();

        TaggedData tag = tagIterator.next();

        assertTrue(tag instanceof NetworkOperatorsData);

        System.out.println("First tag: " + tag.toString());

        NetworkOperatorsData NetworkOperators = (NetworkOperatorsData) tag;

        assertEquals("This is a test string", NetworkOperators.getNetworkOperatorResponseString());

        tag = tagIterator.next();

        System.out.println("Second tag: " + tag.toString());

        assertEquals(TagId.Eof, tag.getTagId());

        

    }

    @Test
    public void testNetworkPerformanceData()
    {

        final byte[] rawPacketData = {(byte) 255, //made-up packet ID
                4,                         //Network performance tag type ID
                8,                         //char array tag type
                0,                         //char array length, substituted below
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,       //Space for a string to be substituted below
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0,
                (byte) 255                  //EOF - only the header tag is present
        };
        ByteBuffer packetData = ByteBuffer.wrap(rawPacketData).order(ByteOrder.LITTLE_ENDIAN);

        byte[] testString = "This is a test string".getBytes();
        packetData.put(3, (byte) testString.length);
        for (int i = 0; i < testString.length; i++)
        {
            packetData.put(4 + i, testString[i]);
        }

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertEquals(255, tp.getPacketId());
        assertEquals(2, tp.tagCount());

        Iterator<TaggedData> tagIterator = tp.iterator();

        TaggedData tag = tagIterator.next();

        assertTrue(tag instanceof NetworkPerformanceData);

        System.out.println("First tag: " + tag.toString());

        NetworkPerformanceData NetworkPerformance = (NetworkPerformanceData) tag;

        assertEquals("This is a test string", NetworkPerformance.getNetworkPerformanceString());

        tag = tagIterator.next();

        System.out.println("Second tag: " + tag.toString());

        assertEquals(TagId.Eof, tag.getTagId());

        

    }

    @Test
    public void testRegistrationStatusData()
    {

        final byte[] rawPacketData = {(byte) 255, //made-up packet ID
                5,                         //Registration Status tag type ID
                8,                         //char array tag type
                0,                         //char array length, substituted below
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,       //Space for a string to be substituted below
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0,
                (byte) 255                  //EOF - only the header tag is present
        };
        ByteBuffer packetData = ByteBuffer.wrap(rawPacketData).order(ByteOrder.LITTLE_ENDIAN);

        byte[] testString = "This is a test string".getBytes();
        packetData.put(3, (byte) testString.length);
        for (int i = 0; i < testString.length; i++)
        {
            packetData.put(4 + i, testString[i]);
        }

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertEquals(255, tp.getPacketId());
        assertEquals(2, tp.tagCount());

        Iterator<TaggedData> tagIterator = tp.iterator();

        TaggedData tag = tagIterator.next();

        assertTrue(tag instanceof RegistrationStatusData);

        System.out.println("First tag: " + tag.toString());

        RegistrationStatusData RegistrationStatus = (RegistrationStatusData) tag;

        assertEquals("This is a test string", RegistrationStatus.getRegistrationStatusString());

        tag = tagIterator.next();

        System.out.println("Second tag: " + tag.toString());

        assertEquals(TagId.Eof, tag.getTagId());

        

    }

    @Test
    public void testPinPukPukTwoRequestStatusData()
    {

        final byte[] rawPacketData = {(byte) 255, //made-up packet ID
                22,                         //Registration Status tag type ID
                8,                         //char array tag type
                0,                         //char array length, substituted below
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,       //Space for a string to be substituted below
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0,
                (byte) 255                  //EOF - only the header tag is present
        };
        ByteBuffer packetData = ByteBuffer.wrap(rawPacketData).order(ByteOrder.LITTLE_ENDIAN);

        byte[] testString = "This is a test string".getBytes();
        packetData.put(3, (byte) testString.length);
        for (int i = 0; i < testString.length; i++)
        {
            packetData.put(4 + i, testString[i]);
        }

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertEquals(255, tp.getPacketId());
        assertEquals(2, tp.tagCount());

        Iterator<TaggedData> tagIterator = tp.iterator();

        TaggedData tag = tagIterator.next();

        assertTrue(tag instanceof PinPukPukTwoRequestStatusData);

        System.out.println("First tag: " + tag.toString());

        PinPukPukTwoRequestStatusData PinPukPukTwoRequestStatus = (PinPukPukTwoRequestStatusData) tag;

        assertEquals("This is a test string", PinPukPukTwoRequestStatus.getPinPukPukTwoRequestDataString());

        tag = tagIterator.next();

        System.out.println("Second tag: " + tag.toString());

        assertEquals(TagId.Eof, tag.getTagId());

        

    }

    @Test
    public void testParsePacketWithTagUnknownID()
    {
         final byte[] rawPacketData = {(byte) 255, //made-up packet ID
                (byte) 189,                        //Unknown tag type
                8,                                 //char array tag type
                0,                                 //char array length, substituted below
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,      //Space for a string to be substituted below
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0,
                (byte) 189, 3, 1, 2, 3, 4,         //Uint32 type tag 67305985 in decimal
                (byte) 189, 6, 2, 2, 1, 4, 3,      //Uint16 array type tag, 2 16-bit values, 258 and 772
                (byte) 189, 14,                    //Revision tag
                9,
                8,
                7, 6, 5, 4,
                3, 2, 1, 0,
                (byte) 255                         //EOF - only the header tag is present
        };
        ByteBuffer packetData = ByteBuffer.wrap(rawPacketData).order(ByteOrder.LITTLE_ENDIAN);

        byte[] testString = "This is a test string".getBytes();
        packetData.put(3, (byte) testString.length);
        for (int i = 0; i < testString.length; i++)
        {
            packetData.put(4 + i, testString[i]);
        }

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertEquals(255, tp.getPacketId());
        assertEquals(5, tp.tagCount());

        Iterator<TaggedData> tagIterator = tp.iterator();

        TaggedData tag = tagIterator.next();
        assertTrue(tag instanceof CharArrayData);
        System.out.println("First tag: " + tag.toString());
        CharArrayData testChartag = (CharArrayData) tag;
        assertEquals("This is a test string", testChartag.getAsString());

        tag = tagIterator.next();
        assertTrue(tag instanceof IntegerData);
        System.out.println("Second tag: " + tag.toString());
        IntegerData integerData = (IntegerData) tag;
        assertEquals(67305985, integerData.getValue());

        tag = tagIterator.next();
        assertTrue(tag instanceof IntegerArrayData);
        System.out.println("Third tag: " + tag.toString());
        IntegerArrayData integerArrayData = (IntegerArrayData) tag;
        assertEquals(258, integerArrayData.getArrayEntries()[0]);
        assertEquals(772, integerArrayData.getArrayEntries()[1]);

        tag = tagIterator.next();
        assertTrue(tag instanceof ImageVersionInfoData);
        System.out.println("Fourth tag: " + tag.toString());
        ImageVersionInfoData imageVersionInfoData = (ImageVersionInfoData) tag;
        assertEquals(9, imageVersionInfoData.getMajorRelease());
        assertEquals(8, imageVersionInfoData.getMinorRelease());
        assertEquals(7060504, imageVersionInfoData.getRevDate());
        assertEquals(3020100, imageVersionInfoData.getBuildRev());

        tag = tagIterator.next();

        System.out.println("Second tag: " + tag.toString());

        assertEquals(TagId.Eof, tag.getTagId());

    }

    //@Ignore("Used for parser testing with data obtained from Cmiu simulator")
    @Test
    public void parseCmiuSimulatorPacket()
    {
        final byte[] rawPacketData = {
                0x00,
                0x01, 0x05, 0x16, 0x08, 0x01, 0x00, 0x08, 0x00,
                0x00, 0x00, 0x03, 0x04, 0x05, 0x06, 0x07, 0x02,
                0x00, (byte) 0x89, 0x69, 0x09, 0x55, 0x00, 0x00, 0x00,
                0x00, 0x02, 0x05, 0x33, 0x07, 0x7b, 0x00, (byte) 0xea, 0x00,
                0x59,
                0x01,
                (byte) 0xc0,
                0x06,
                (byte) 0x92,
                0x54,
                0x00,
                0x00,
                0x00,
                0x00,
                (byte) 0x89,
                0x69,
                0x09,
                0x55,
                0x00,
                0x00,
                0x00,
                0x00,
                0x3c,
                0x0f,
                0x00,
                (byte) 0xc8,
                0x01,
                0x37,
                0x02,
                0x00,
                0x74,
                (byte) 0xd9,
                0x08,
                0x55,
                0x00,
                0x00,
                0x00,
                0x00,
                0x1d,
                (byte) 0xda,
                0x08,
                0x55,
                0x00,
                0x00,
                0x00,
                0x00,
                0x01,
                (byte) 0xa6,
                0x02,
                0x00,
                (byte) 0xff};

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        assertNotNull(tp);
        assertEquals(3, tp.tagCount()); //cmiu header, config, eof

    }

    //@Ignore("Used for parser testing with data obtained from Cmiu simulator")
    @Test
    public void parseCmiuSimulatorPacket2()
    {
        final byte[] rawPacketData = {
                0,
                1,
                5,
                22,
                8,
                1,
                0,
                1,
                0,
                0,
                0,
                3,
                4,
                5,
                6,
                7,
                2,
                0,
                51,
                116,
                9,
                85,
                0,
                0,
                0,
                0,
                2,
                5,
                51,
                7,
                123,
                0,
                -22,
                0,
                89,
                1,
                28,
                9,
                -105,
                84,
                0,
                0,
                0,
                0,
                51,
                116,
                9,
                85,
                0,
                0,
                0,
                0,
                60,
                15,
                0,
                -56,
                1,
                55,
                2,
                0,
                -108,
                37,
                9,
                85,
                0,
                0,
                0,
                0,
                -93,
                37,
                9,
                85,
                0,
                0,
                0,
                0,
                1,
                -90,
                2,
                1,
                -1
        };

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);
        assertNotNull(tp);
        assertEquals(3, tp.tagCount()); //cmiu header, config, eof

    }

    @Test
    public void testFindTagInPacketParsesCmiuHeader() throws Exception
    {
        final byte[] rawPacketData = {(byte) 255, //made-up packet ID
                1,                      //CMIU packet header tag ID
                5,                      //byte array tag type
                19,                     //size of following data in bytes
                0, 0, 0, 0,             //CMIU ID - to complete later
                54,                     //sequence number
                0, 0, 0,                //key info, encryption method, token
                3,                      //network flags
                31,                     //RSSI = -51 dBm or greater
                4,                      //BER = 1.6% to 3.2%
                0, 0, 0, 0, 0, 0, 0, 0, //current time and date, to fill in later
                (byte) 255               //EOF - only the header tag is present
        };

        ByteBuffer packetData = ByteBuffer.wrap(rawPacketData).order(ByteOrder.LITTLE_ENDIAN);
        packetData.putInt(4, 5000002); //CMIUID = 5,000,002

        long unixTimestamp = 1423248037L;
        packetData.putLong(15, unixTimestamp);

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        CmiuPacketHeaderData cmiuPacketHeaderData  = tp.findTag(CmiuPacketHeaderData.class);
        assertNotNull(cmiuPacketHeaderData);
        assertEquals(5000002, cmiuPacketHeaderData.getCmiuId());
    }

    @Test
    public void testFindTagInPacketCorrectlyParsesTags()
    {
        final byte[] rawPacketData = {
                0,
                1,
                5,
                20,
                0,
                1,
                0,
                0,
                0,
                3,
                4,
                5,
                6,
                7,
                2,
                0,
                51,
                116,
                9,
                85,
                0,
                0,
                0,
                0,
                2,
                5,
                51,
                7,
                123,
                0,
                -22,
                0,
                89,
                1,
                28,
                9,
                -105,
                84,
                0,
                0,
                0,
                0,
                51,
                116,
                9,
                85,
                0,
                0,
                0,
                0,
                60,
                15,
                0,
                -56,
                1,
                55,
                2,
                0,
                -108,
                37,
                9,
                85,
                0,
                0,
                0,
                0,
                -93,
                37,
                9,
                85,
                0,
                0,
                0,
                0,
                1,
                -90,
                2,
                1,
                -1
        };

        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        //cmiu packet header data
        CmiuPacketHeaderData cmiuPacketHeaderData  = tp.findTag(CmiuPacketHeaderData.class);
        assertNotNull(cmiuPacketHeaderData);
        assertEquals(256, cmiuPacketHeaderData.getCmiuId());
        assertEquals(1, cmiuPacketHeaderData.getTagId().getId());

        DetailedCmiuConfigurationData cmiuConfigurationData  = tp.findTag(DetailedCmiuConfigurationData.class);
        assertNotNull(cmiuConfigurationData);
        assertEquals(7, cmiuConfigurationData.getCmiuType());
        assertEquals(2, cmiuConfigurationData.getTagId().getId());

        TaggedData taggedData  = tp.findTag(TaggedData.class);
        assertNotNull(taggedData);
        assertEquals(255, taggedData.getTagId().getId());

    }

    @Test
    public void testSerialisePacket()
    {
        final byte[] rawPacketData = {
                0,
                1,
                5,
                22,
                8,
                1,
                0,
                1,
                0,
                0,
                0,
                3,
                4,
                5,
                6,
                7,
                2,
                0,
                51,
                116,
                9,
                85,
                0,
                0,
                0,
                0,
                2,
                5,
                51,
                7,
                123,
                0,
                -22,
                0,
                89,
                1,
                28,
                9,
                -105,
                84,
                0,
                0,
                0,
                0,
                51,
                116,
                9,
                85,
                0,
                0,
                0,
                0,
                60,
                15,
                0,
                -56,
                1,
                55,
                2,
                0,
                -108,
                37,
                9,
                85,
                0,
                0,
                0,
                0,
                -93,
                37,
                9,
                85,
                0,
                0,
                0,
                0,
                1,
                -90,
                2,
                1,
                -1
        };

        //deserialise
        TaggedPacket tp = PacketParser.parseTaggedPacket(rawPacketData);

        //serialize
        byte[] deserialisedByte = tp.toByteSequence();

        assertArrayEquals(rawPacketData, deserialisedByte);
    }

    /**
     * A packet received in production in April 2016
     *
     * There was an error when converting to JSON due to a bad date.
     */
    @Test
    public void testProdPacketApril2016() {
        final byte[] packetBytes = HexUtils.parseHexToByteArray("00010513768DD71700000000003EBCC5EE561400000000FE0D190003080D4E6F7420617661696C61626C6504080D4E6F7420617661696C61626C6505080D4E6F7420617661696C61626C6506081062726F6B65722E70726F642E6D64636508080D31302E3233302E35392E3138370F080D4E6F7420617661696C61626C6510080D4E6F7420617661696C61626C6511080D4E6F7420617661696C61626C6512080D4E6F7420617661696C61626C6513080D4E6F7420617661696C61626C6514080D4E6F7420617661696C61626C6515080D4E6F7420617661696C61626C6517050500000000011A0504000100001B05110100000000030000000F000000000700001E08154E6F204572726F722054657374204D6573736167651F0512B4EE561400000000FC308601AA00002E808022050E01000F0100002C4DC43C3DFDFFFF270E00120016032300000504280E00120016032300000380290E001200160323000005042C051F108A371A177040044093E55500000000938050101410022C009F070000000030050E00000100000000000000000F01003E02BF3F3F01000000000000000000000000000000FF");
        final TaggedPacket packet = PacketParser.parseTaggedPacket(packetBytes);
        final byte[] secureDataBytes = packet.findTag(SecureBlockArrayData.class).getEncryptedDataCopy();
        final TagSequenceParser secDataParser = new TagSequenceParser();
        final TagSequence secureTags = secDataParser.parseTagSequence(secureDataBytes);
        for (TaggedData td : secureTags) {
            td.toJson();
        }
    }

}
