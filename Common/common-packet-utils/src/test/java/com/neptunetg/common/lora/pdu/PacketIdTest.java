/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.lora.pdu;

import org.junit.Test;

import static org.junit.Assert.*;
/**
 * Tests for PacketId class
 */
public class PacketIdTest
{
    @Test
    public void testFromFirstDataByte()
    {
        byte[] data = {(byte)0xf0, (byte)0xab};

        L900Packet packet = new L900Packet(data);

        assertEquals(packet.getPacketId(), PacketId.fromFirstDataByte(data));

        data = new byte[]{(byte)0xff, (byte)0xab};

        packet = new L900Packet(data);

        assertEquals(packet.getPacketId(), PacketId.fromFirstDataByte(data));


        try
        {
            data = new byte[0];
            PacketId.fromFirstDataByte(data);
            fail("Expected IllegalArgumentException");
        }
        catch (IllegalArgumentException e)
        {
            //expected
        }

        try
        {
            data = null;
            PacketId.fromFirstDataByte(data);
            fail("Expected IllegalArgumentException");
        }
        catch (IllegalArgumentException e)
        {
            //expected
        }

    }

}
