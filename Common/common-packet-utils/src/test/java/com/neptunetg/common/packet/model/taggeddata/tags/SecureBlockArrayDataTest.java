/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SecureBlockArrayDataTest
{

    @Test
    public void testSerialise() throws Exception
    {
        byte[] data = new byte[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
        SecureBlockArrayData sbad =
                new SecureBlockArrayData(TagDataType.ExtendedSecureBlockArray, data, 1);

        byte[] serializedData = sbad.serialize();

        assertEquals(20, serializedData.length); //data length + tag number, type, array size
        assertEquals((byte)TagId.SecureDataBlockArray.getId(), serializedData[0]);
        assertEquals(TagDataType.ExtendedSecureBlockArray.getId(), serializedData[1]);
        assertEquals(1, serializedData[2]); //1 block
        assertEquals(0, serializedData[3]); //1 block
        assertEquals(data[0], serializedData[4]);
        assertEquals(data[1], serializedData[5]);
        assertEquals(data[2], serializedData[6]);
        assertEquals(data[3], serializedData[7]);
        assertEquals(data[15], serializedData[19]);
    }
}