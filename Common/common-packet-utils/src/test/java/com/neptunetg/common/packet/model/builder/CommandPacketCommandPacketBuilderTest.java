/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.builder;

import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.*;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.PacketParser;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Command packet builder unit test
 */
public class CommandPacketCommandPacketBuilderTest
{
    @Test
    public void testBuildImageUpdateCommandPacket() throws Exception
    {
        ImageUpdateCommandBuilder imageBuilder = new ImageUpdateCommandBuilder(ImageMetaData.ImageType.FIRMWARE_IMAGE)
                .withFilename("testFileName")
                .withMetaData(0x1234, 0x5678)
                .withRevision(3, 4, 5, 6);

        TaggedPacket packet = new CommandPacketBuilder()
                .buildImageUpdateCommandPacket(123, imageBuilder);


        assertNotNull(packet);
        assertEquals(3, packet.tagCount()); //header tag, secure tag, EOF

        //deserialise
        byte[] commandPacketRaw = packet.toByteSequence();

        //parse it back
        TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(commandPacketRaw.clone());

        assertNotNull(parsedTaggedPacket.findTag(MqttPacketHeaderData.class));

        Map<Integer, TaggedData> secureDataMap = PacketParser.parseSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));

        assertEquals(5, secureDataMap.size());
        assertNotNull(secureDataMap.get(32));   //Command or configuration tag
        assertNotNull(secureDataMap.get(50));   //Current Time & Date tag
        assertNotNull(secureDataMap.get(39));   //Firmware tag
        assertNotNull(secureDataMap.get(46));   //Image
        assertNotNull(secureDataMap.get(47));   //Image Meta data
    }

    @Test
    public void testBuildBootloaderImageUpdateCommandPacket() throws Exception
    {
        ImageUpdateCommandBuilder imageBuilder = new ImageUpdateCommandBuilder(ImageMetaData.ImageType.BOOTLOADER_IMAGE)
                .withFilename("testFileName")
                .withMetaData(0x1234, 0x5678)
                .withRevision(3, 4, 5, 6);

        TaggedPacket packet = new CommandPacketBuilder()
                .buildImageUpdateCommandPacket(123, imageBuilder);


        assertNotNull(packet);
        assertEquals(3, packet.tagCount()); //header tag, secure tag, EOF

        //deserialise
        byte[] commandPacketRaw = packet.toByteSequence();

        //parse it back
        TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(commandPacketRaw.clone());

        assertNotNull(parsedTaggedPacket.findTag(MqttPacketHeaderData.class));

        Map<Integer, TaggedData> secureDataMap = PacketParser.parseSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));

        assertNotNull(secureDataMap.get(32));   //Command or configuration tag
        assertNotNull(secureDataMap.get(50));   //Current Time & Date tag
        assertNotNull(secureDataMap.get(46));   //Image
        assertNotNull(secureDataMap.get(47));   //Image Meta data
        assertNotNull(secureDataMap.get(40));   //Bootloader tag
    }

    @Test
    public void testBuildConfigImageUpdateCommandPacket() throws Exception
    {
        ImageUpdateCommandBuilder imageBuilder = new ImageUpdateCommandBuilder(ImageMetaData.ImageType.CONFIG_IMAGE)
                .withFilename("testFileName")
                .withMetaData(0x1234, 0x5678)
                .withRevision(3, 4, 5, 6);

        TaggedPacket packet = new CommandPacketBuilder()
                .buildImageUpdateCommandPacket(123, imageBuilder);


        assertNotNull(packet);
        assertEquals(3, packet.tagCount()); //header tag, secure tag, EOF

        //deserialise
        byte[] commandPacketRaw = packet.toByteSequence();

        //parse it back
        TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(commandPacketRaw.clone());

        assertNotNull(parsedTaggedPacket.findTag(MqttPacketHeaderData.class));

        Map<Integer, TaggedData> secureDataMap = PacketParser.parseSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));

        assertNotNull(secureDataMap.get(32));   //Command or configuration tag
        assertNotNull(secureDataMap.get(50));   //Current Time & Date tag
        assertNotNull(secureDataMap.get(46));   //Image
        assertNotNull(secureDataMap.get(47));   //Image Meta data
        assertNotNull(secureDataMap.get(41));   //Bootloader tag
    }

    @Test
    public void testBuildArbImageUpdateCommandPacket() throws Exception
    {
        ImageUpdateCommandBuilder imageBuilder = new ImageUpdateCommandBuilder(ImageMetaData.ImageType.ARB_CONFIG_IMAGE)
                .withFilename("testFileName")
                .withMetaData(0x1234, 0x5678)
                .withRevision(3, 4, 5, 6);

        TaggedPacket packet = new CommandPacketBuilder()
                .buildImageUpdateCommandPacket(123, imageBuilder);


        assertNotNull(packet);
        assertEquals(3, packet.tagCount()); //header tag, secure tag, EOF

        //deserialise
        byte[] commandPacketRaw = packet.toByteSequence();

        //parse it back
        TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(commandPacketRaw.clone());

        assertNotNull(parsedTaggedPacket.findTag(MqttPacketHeaderData.class));

        Map<Integer, TaggedData> secureDataMap = PacketParser.parseSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));

        assertNotNull(secureDataMap.get(32));   //Command or configuration tag
        assertNotNull(secureDataMap.get(50));   //Current Time & Date tag
        assertNotNull(secureDataMap.get(46));   //Image
        assertNotNull(secureDataMap.get(47));   //Image Meta data
        assertNotNull(secureDataMap.get(42));   //Bootloader tag
    }

    @Test
    public void testBuildBleImageUpdateCommandPacket() throws Exception
    {
        ImageUpdateCommandBuilder imageBuilder = new ImageUpdateCommandBuilder(ImageMetaData.ImageType.BLE_CONFIG_IMAGE)
                .withFilename("testFileName")
                .withMetaData(0x1234, 0x5678)
                .withRevision(3, 4, 5, 6);

        TaggedPacket packet = new CommandPacketBuilder()
                .buildImageUpdateCommandPacket(123, imageBuilder);


        assertNotNull(packet);
        assertEquals(3, packet.tagCount()); //header tag, secure tag, EOF

        //deserialise
        byte[] commandPacketRaw = packet.toByteSequence();

        //parse it back
        TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(commandPacketRaw.clone());

        assertNotNull(parsedTaggedPacket.findTag(MqttPacketHeaderData.class));

        Map<Integer, TaggedData> secureDataMap = PacketParser.parseSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));

        assertNotNull(secureDataMap.get(32));   //Command or configuration tag
        assertNotNull(secureDataMap.get(50));   //Current Time & Date tag
        assertNotNull(secureDataMap.get(46));   //Image
        assertNotNull(secureDataMap.get(47));   //Image Meta data
        assertNotNull(secureDataMap.get(32));   //Bootloader tag
    }

    @Test
    public void testBuildMutipleImageUpdateCommandPacket() throws Exception
    {
        ImageUpdateCommandBuilder firmwareImageBuilder = new ImageUpdateCommandBuilder(ImageMetaData.ImageType.FIRMWARE_IMAGE)
                .withFilename("firmware image")
                .withMetaData(1, 1)
                .withRevision(1, 1, 1, 1);

        ImageUpdateCommandBuilder bootloaderImageBuilder = new ImageUpdateCommandBuilder(ImageMetaData.ImageType.BOOTLOADER_IMAGE)
                .withFilename("bootloader image")
                .withMetaData(2, 2)
                .withRevision(2, 2, 2, 2);

        ImageUpdateCommandBuilder configImageBuilder = new ImageUpdateCommandBuilder(ImageMetaData.ImageType.CONFIG_IMAGE)
                .withFilename("config image")
                .withMetaData(3, 3)
                .withRevision(3, 3, 3, 3);
        ImageUpdateCommandBuilder arbImageBuilder = new ImageUpdateCommandBuilder(ImageMetaData.ImageType.ARB_CONFIG_IMAGE)
                .withFilename("arb image")
                .withMetaData(4, 4)
                .withRevision(4, 4, 4, 4);
        ImageUpdateCommandBuilder bleImageBuilder = new ImageUpdateCommandBuilder(ImageMetaData.ImageType.BLE_CONFIG_IMAGE)
                .withFilename("ble image")
                .withMetaData(5, 5)
                .withRevision(5, 5, 5, 5);

        TaggedPacket packet = new CommandPacketBuilder()
                .buildImageUpdateCommandPacket(123, firmwareImageBuilder, bootloaderImageBuilder, configImageBuilder, arbImageBuilder, bleImageBuilder);

        assertNotNull(packet);
        assertEquals(3, packet.tagCount()); //header tag, secure tag, EOF

        //deserialise
        byte[] commandPacketRaw = packet.toByteSequence();

        //parse it back
        TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(commandPacketRaw.clone());

        assertNotNull(parsedTaggedPacket.findTag(MqttPacketHeaderData.class));

        List<TaggedData> secureDataList = PacketParser.parseNonUniqueSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));

        assertEquals(17, secureDataList.size());

        assertEquals(TagId.Command, secureDataList.get(0).getTagId());
        assertEquals(TagId.TimeAndDate, secureDataList.get(1).getTagId());

        assertEquals(TagId.Image, secureDataList.get(2).getTagId());
        assertEquals(TagId.ImageMetaData, secureDataList.get(3).getTagId());
        assertEquals(TagId.FirmwareRevision, secureDataList.get(4).getTagId());

        assertEquals(TagId.Image, secureDataList.get(5).getTagId());
        assertEquals(TagId.ImageMetaData, secureDataList.get(6).getTagId());
        assertEquals(TagId.BootloaderVersion, secureDataList.get(7).getTagId());

        assertEquals(TagId.Image, secureDataList.get(8).getTagId());
        assertEquals(TagId.ImageMetaData, secureDataList.get(9).getTagId());
        assertEquals(TagId.ConfigRevision, secureDataList.get(10).getTagId());

        assertEquals(TagId.Image, secureDataList.get(11).getTagId());
        assertEquals(TagId.ImageMetaData, secureDataList.get(12).getTagId());
        assertEquals(TagId.ArbConfigRevision, secureDataList.get(13).getTagId());

        assertEquals(TagId.Image, secureDataList.get(14).getTagId());
        assertEquals(TagId.ImageMetaData, secureDataList.get(15).getTagId());
        assertEquals(TagId.BleConfigRevision, secureDataList.get(16).getTagId());

    }

    @Test
    public void testBuildRebootCmiuCommandPacket() throws Exception
    {
        TaggedPacket packet = new CommandPacketBuilder()
                .buildRebootCmiuCommandPacket(123);

        assertNotNull(packet);
        assertEquals(3, packet.tagCount()); //header tag, secure tag, EOF

        //deserialise
        byte[] commandPacketRaw = packet.toByteSequence();

        //parse it back
        TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(commandPacketRaw.clone());

        assertNotNull(parsedTaggedPacket.findTag(MqttPacketHeaderData.class));

        Map<Integer, TaggedData> secureDataMap = PacketParser.parseSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));

        assertEquals(1, secureDataMap.size());
        assertNotNull(secureDataMap.get(32));   //Command or configuration tag
    }

    @Test
    public void testBuildMagWipeEmulationCommandPacket() throws Exception
    {
        TaggedPacket packet = new CommandPacketBuilder()
                .buildMagSwipeEmulationCommandPacket(123);

        assertNotNull(packet);
        assertEquals(3, packet.tagCount()); //header tag, secure tag, EOF

        //deserialise
        byte[] commandPacketRaw = packet.toByteSequence();

        //parse it back
        TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(commandPacketRaw.clone());

        assertNotNull(parsedTaggedPacket.findTag(MqttPacketHeaderData.class));

        Map<Integer, TaggedData> secureDataMap = PacketParser.parseSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));

        assertEquals(1, secureDataMap.size());
        assertNotNull(secureDataMap.get(32));   //Command or configuration tag
    }

    @Test
    public void testBuildEraseDatalogCommandPacket() throws Exception
    {
        TaggedPacket packet = new CommandPacketBuilder()
                .buildEraseDatalogCommandPacket(123);

        assertNotNull(packet);
        assertEquals(3, packet.tagCount()); //header tag, secure tag, EOF

        //deserialise
        byte[] commandPacketRaw = packet.toByteSequence();

        //parse it back
        TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(commandPacketRaw.clone());

        assertNotNull(parsedTaggedPacket.findTag(MqttPacketHeaderData.class));

        Map<Integer, TaggedData> secureDataMap = PacketParser.parseSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));

        assertEquals(1, secureDataMap.size());
        assertNotNull(secureDataMap.get(32));   //Command or configuration tag

    }

    @Test
    public void testBuildSetRecordingReportingIntervalsCommandPacket() throws Exception
    {
        TaggedPacket packet = new CommandPacketBuilder()
                .reportingInterval(1, 2, 3, 4, 5, 6)
                .recordingInterval(7,8)
                .buildSetRecordingReportingIntervalsCommandPacket(123);

        assertNotNull(packet);
        assertEquals(3, packet.tagCount()); //header tag, secure tag, EOF

        //deserialise
        byte[] commandPacketRaw = packet.toByteSequence();

        //parse it back
        TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(commandPacketRaw.clone());

        assertNotNull(parsedTaggedPacket.findTag(MqttPacketHeaderData.class));

        Map<Integer, TaggedData> secureDataMap = PacketParser.parseSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));

        assertEquals(2, secureDataMap.size());
        assertNotNull(secureDataMap.get(32));   //Command or configuration tag
        assertNotNull(secureDataMap.get(48));
    }

    @Test
    public void testBuildPublishRequestPacketCommandPacket() throws Exception
    {
        TaggedPacket packet = new CommandPacketBuilder()
                .packetType(TaggedDataPacketType.BasicConfigurationStatus)
                .buildPublishRequestedPacketCommandPacket(123);

        assertNotNull(packet);
        assertEquals(3, packet.tagCount());

        //deserialise
        byte[] commandPacketRaw = packet.toByteSequence();

        //parse it back
        TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(commandPacketRaw.clone());

        assertNotNull(parsedTaggedPacket.findTag(MqttPacketHeaderData.class));

        Map<Integer, TaggedData> secureDataMap = PacketParser.parseSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));

        assertEquals(2, secureDataMap.size());
        assertNotNull(secureDataMap.get(32));   //Command or configuration tag
        assertNotNull(secureDataMap.get(65));   //Packet Type Tag
    }
}