/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.parser;

import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.CurrentTimeAndDateData;
import com.neptunetg.common.packet.model.taggeddata.tags.NetworkOperatorsData;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.util.HexUtils;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Tests for the ParserUtil
 */
public class ParserUtilTest
{

    @Test
    public void testConvertUtf8HexToString()
    {
        final String testString = "{\"Some JSON with \u4523 \u4215 unicode characters\": 4}";
        final String hexString = HexUtils.byteArrayToHex(testString.getBytes(StandardCharsets.UTF_8));
        final String convertedString = ParserUtil.convertUtf8HexToString(hexString);
        assertEquals(testString, convertedString);
    }

    @Test
    public void testParseHexToJson()
    {
        List<TaggedData> tagList = new ArrayList<>(1);
        tagList.add(new CurrentTimeAndDateData(1434731663));
        TaggedPacket tp = new TaggedPacket(TaggedDataPacketType.TimeOfDay, tagList);

        byte[] rawData = tp.toByteSequence();
        String rawDataHexString = HexUtils.byteArrayToHex(rawData);

        //packet, unformatted
        String packetJson = ParserUtil.parsePacketToJson(rawDataHexString);
        assertTrue(packetJson.indexOf("1434731663") > -1);
        String tagJson = ParserUtil.parseTagInPacketToJson(rawDataHexString, TagId.CurrentTimeAndDate.getId());
        assertTrue(tagJson.indexOf("1434731663") > -1);
        assertNull(ParserUtil.parseTagInPacketToJson(rawDataHexString, 65));

        //packet, formatted
        packetJson = ParserUtil.parsePacketToJsonPretty(rawDataHexString);
        assertTrue(packetJson.indexOf("1434731663") > -1);
        tagJson = ParserUtil.parseTagInPacketToJsonPretty(rawDataHexString, TagId.CurrentTimeAndDate.getId());
        assertTrue(tagJson.indexOf("1434731663") > -1);
        assertNull(ParserUtil.parseTagInPacketToJsonPretty(rawDataHexString, 65));

        TagSequence ts = new TagSequence(tagList);
        rawData = ts.toByteSequence();
        rawDataHexString = HexUtils.byteArrayToHex(rawData);

        //tag sequence, unformatted
        String tagSequenceJson = ParserUtil.parseTagSequenceToJson(rawDataHexString);
        assertTrue(tagSequenceJson.indexOf("1434731663") > -1);
        tagJson = ParserUtil.parseTagToJson(rawDataHexString, TagId.CurrentTimeAndDate.getId());
        assertTrue(tagJson.indexOf("1434731663") > -1);
        assertNull(ParserUtil.parseTagToJson(rawDataHexString, 65));

        //tag sequence, formatted
        tagSequenceJson = ParserUtil.parseTagSequenceToJsonPretty(rawDataHexString);
        assertTrue(tagSequenceJson.indexOf("1434731663") > -1);
        tagJson = ParserUtil.parseTagToJsonPretty(rawDataHexString, TagId.CurrentTimeAndDate.getId());
        assertTrue(tagJson.indexOf("1434731663") > -1);
        assertNull(ParserUtil.parseTagToJsonPretty(rawDataHexString, 65));

    }

    @Test
    public void testNonAscii7()
    {
        //the following data block has a text entry that contains non-ASCII characters
        final String secureDataHex = "02054901000000061409150046020000ffffffffffffffffffff310caeb3c6dc8f772b774093e5550000000000001e008e060000000000014093e555000000004093e55500000000017c070003080819e662b863751f7704086c2352465354533a2233313120343830222c353233302c2d36312c2d33332c2d31312c414130392c3235352c2c3132382c31392c302c323938343130332c22333131343830393930343934363738222c22566572697a6f6e20576972656c657373222c322c31332c3134380d0a05080d2b43475245473a20302c310d0a08080c31302e3132382e322e3232370f0806302e30300d0a10080b4c453931302d5356470d0a11080e2343474d493a2054656c69740d0a1208122343474d523a2031372e30312e3537310d0a130818234347534e3a203335333233383036303136323835350d0a1408182343494d493a203331313438303939303439343637380d0a15081d23434349443a2038393134383030303030313838373732343337340d0a17050500000000001808074e6f2055736572190400aa2d55000000001e08084e6f204572726f721f0512d4fb0a5600000000c0307201aa001a220e29270e00060015091400000246280e00060015091100000138290e000600150914000002460000000000000000";
        final TagSequence tags = new TagSequenceParser().parseTagSequence(HexUtils.parseHexToByteArray(secureDataHex));
        final NetworkOperatorsData nod = tags.findTag(NetworkOperatorsData.class);
        final String warning = nod.getWarning();
        assertNotNull(warning);
        final String packetJson = ParserUtil.parseTagSequenceToJsonPretty(secureDataHex);
        assertTrue(packetJson.contains(warning));
    }

}
