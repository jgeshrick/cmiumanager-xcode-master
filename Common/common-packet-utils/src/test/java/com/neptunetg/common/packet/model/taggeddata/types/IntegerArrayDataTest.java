/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.types;

import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IntegerArrayDataTest
{

    @Test
    public void testSerialise() throws Exception
    {
        byte[] data = new byte[]{1,2,3,4};
        IntegerArrayData integerArrayData =
                new IntegerArrayData(TagId.CmiuPacketHeader, TagDataType.ByteRecord, data, data.length);

        byte[] serializedData = integerArrayData.serialize();

        assertEquals(7, serializedData.length); //data length + tag number, type, array size
        assertEquals((byte)TagId.CmiuPacketHeader.getId(), serializedData[0]);
        assertEquals(TagDataType.ByteRecord.getId(), serializedData[1]);
        assertEquals(data.length, serializedData[2]);
        assertEquals(data[0], serializedData[3]);
        assertEquals(data[1], serializedData[4]);
        assertEquals(data[2], serializedData[5]);
        assertEquals(data[3], serializedData[6]);
    }
}