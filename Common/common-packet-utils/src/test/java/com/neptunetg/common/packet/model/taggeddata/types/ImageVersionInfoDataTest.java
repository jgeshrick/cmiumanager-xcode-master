/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.types;

import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ImageVersionInfoDataTest
{

    @Test
    public void testCreateImageVersionInfoDataFromValues() throws Exception
    {
        ImageVersionInfoData iri = new ImageVersionInfoData(TagId.ConfigRevision, 1, 2, 20150416, 1234556);
        assertEquals(1, iri.getMajorRelease());
        assertEquals(2, iri.getMinorRelease());
        assertEquals(20150416, iri.getRevDate());
        assertEquals(1234556, iri.getBuildRev());
        assertEquals("1.2.20150416.1234556", iri.getVersion());

        assertArrayEquals(new byte[]{41, 14, 0x01, 0x02, 0x20, 0x15, 0x04, 0x16, 0x01, 0x23, 0x45, 0x56},
                iri.serialize());

    }


    @Test
    public void testCreateImageVersionInfoDataFromBinaryData() throws Exception
    {
        ImageVersionInfoData iri = new ImageVersionInfoData(TagId.ConfigRevision, new byte[]{0x01, 0x02, 0x20, 0x15, 0x04, 0x16, 0x01, 0x23, 0x45, 0x56});
        assertEquals(1, iri.getMajorRelease());
        assertEquals(2, iri.getMinorRelease());
        assertEquals(20150416, iri.getRevDate());
        assertEquals(1234556, iri.getBuildRev());
        assertEquals("1.2.20150416.1234556", iri.getVersion());

        assertArrayEquals(new byte[]{41, 14, 0x01, 0x02, 0x20, 0x15, 0x04, 0x16, 0x01, 0x23, 0x45, 0x56},
                iri.serialize());

    }
}