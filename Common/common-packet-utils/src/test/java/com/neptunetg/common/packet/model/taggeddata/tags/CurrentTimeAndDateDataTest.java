/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.Instant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CurrentTimeAndDateDataTest
{
    @Test
    public void testCurrentTimeAndDate() throws Exception
    {
        long testData = 0x1234567890abcdefL;

        //construct object
        CurrentTimeAndDateData currentTimeAndDateData = new CurrentTimeAndDateData(testData);

        assertEquals(testData, currentTimeAndDateData.getValue());
        assertEquals(TagId.CurrentTimeAndDate, currentTimeAndDateData.getTagId());

        //serialize back and get raw tag data
        byte[] rawData = currentTimeAndDateData.getRawBytes();
        assertEquals(36, rawData[0]);   //tag number
        assertEquals(4, rawData[1]);    //tag type: UINT64


        byte[] testDataByte = ByteBuffer.allocate(Long.BYTES).order(ByteOrder.LITTLE_ENDIAN)
                .putLong(testData).array();

        assertEquals(testDataByte[0], rawData[2]);   //uint64 value
        assertEquals(testDataByte[1], rawData[3]);   //uint64 value
        assertEquals(testDataByte[2], rawData[4]);   //uint64 value
        assertEquals(testDataByte[3], rawData[5]);   //uint64 value
        assertEquals(testDataByte[4], rawData[6]);   //uint64 value
        assertEquals(testDataByte[5], rawData[7]);   //uint64 value
        assertEquals(testDataByte[6], rawData[8]);   //uint64 value
        assertEquals(testDataByte[7], rawData[9]);   //uint64 value

    }

    @Test
    public void testBuilder() throws Exception
    {
        CurrentTimeAndDateData currentTimeAndDateData = CurrentTimeAndDateData.Builder.build();
        Instant now = Instant.now();

        //time difference should be less than 0.1 seconds
        assertTrue(Math.abs(now.getEpochSecond() - currentTimeAndDateData.getValue()) < 0.1);
    }
}