/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.common.packet.model.taggeddata.tags;

import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

/**
 * Test for NetworkPerformanceData class
 */
public class NetworkPerformanceDataTest
{

    private static final String[] EXAMPLE_RFSTS =
    {
            "#RFSTS:\"311 480\",5230,-84,-57,-10,2C02,255,,128,19,0,0ADD502,\"311480148913416\",\"Verizon Wireless\",2,13,208",
            "#RFSTS:\"311 480\",2100,-91,-65,-7,2C02,255,,128,19,0,541B516,\"311480148913416\",\"Verizon Wireless\",2,4,217",
            "#RFSTS:\"311 480\",2100,-91,-65,-7,2C02,255,,128,19,0,541B516,\"311480148913416\",\"Verizon Wireless\",2,4,233"
    };

    private static final String CRAZY_RFSTS = "#RFSTS:\"311 480,,,,lots of commas,,, \u252a \ufff0 \",14215,-84,-57,-9999,2C02,255,,128,19,0,0ADD502,\"311480148913416\",\"Verizon Wireless\",2,13,208";

    @Test
    public void testGetRsrq()
    {
        NetworkPerformanceData npd = new NetworkPerformanceData(TagDataType.CharArray, EXAMPLE_RFSTS[0].getBytes());
        assertEquals(-10, npd.getRsrq());
        npd = new NetworkPerformanceData(TagDataType.CharArray, EXAMPLE_RFSTS[1].getBytes());
        assertEquals(-7, npd.getRsrq());
        npd = new NetworkPerformanceData(TagDataType.CharArray, EXAMPLE_RFSTS[2].getBytes());
        assertEquals(-7, npd.getRsrq());

        npd = new NetworkPerformanceData(TagDataType.CharArray, CRAZY_RFSTS.getBytes(StandardCharsets.UTF_8));
        assertEquals(-9999, npd.getRsrq());

    }


}
