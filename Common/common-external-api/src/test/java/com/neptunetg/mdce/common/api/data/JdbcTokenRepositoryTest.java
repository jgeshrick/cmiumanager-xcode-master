/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.api.data;

import com.neptunetg.mdce.common.api.domain.RestClientId;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.mockito.AdditionalMatchers;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.xml.bind.DatatypeConverter;
import java.util.Arrays;

import static org.mockito.Mockito.*;

public class JdbcTokenRepositoryTest
{
    private final JdbcTemplate jdbcTemplate = mock(JdbcTemplate.class);

    @Test
    public void testCaching() throws Exception
    {

        final JdbcTokenRepository repo = new JdbcTokenRepository(jdbcTemplate);
        final TokenInfo tokenForPartnerString123141 = repo.insertToken("123141", RestClientId.forUtility(425), "1.2.3.4", 50);
        final Matcher<Object[]> binds123141 = new BindByteArrayMatcher(tokenForPartnerString123141.getTokenString());

        repo.getToken(tokenForPartnerString123141.getTokenString());
        verify(jdbcTemplate, never()).queryForObject(anyString(), argThat(binds123141), eq(repo)); //should have hit cache
        verify(jdbcTemplate, never()).queryForObject(anyString(), any(), eq(repo)); //should have missed cache

        repo.getToken("fafafa");

        verify(jdbcTemplate, times(1)).queryForObject(anyString(), any(), eq(repo)); //should have missed cache

        //insert 999 more entries in cache
        for (int i = 0; i < 999; i++)
        {
            repo.insertToken("123141" + Integer.toHexString(0xF0000000+i), RestClientId.forUtility(425), "1.2.3.4", 50);
        }

        repo.getToken(tokenForPartnerString123141.getTokenString());
        verify(jdbcTemplate, never()).queryForObject(anyString(), argThat(binds123141), eq(repo)); //should have hit cache

        Thread.sleep(10L);

        //add one more entry to cache
        for (int i = 1000; i < 1001; i++)
        {
            repo.insertToken("123141" + Integer.toHexString(0xF0000000+i), RestClientId.forUtility(425), "1.2.3.4", 50);
        }

        Thread.sleep(10L);

        repo.getToken(tokenForPartnerString123141.getTokenString());
        verify(jdbcTemplate, never()).queryForObject(anyString(), argThat(binds123141), eq(repo)); //should have hit cache

        Thread.sleep(10L);

        //insert 1000 more entries in cache
        for (int i = 0; i < 1000; i++)
        {
            repo.insertToken("123141" + Integer.toHexString(0xF0000000+i), RestClientId.forUtility(425), "1.2.3.4", 50);
        }

        repo.getToken(tokenForPartnerString123141.getTokenString());
        verify(jdbcTemplate, times(1)).queryForObject(anyString(), argThat(binds123141), eq(repo)); //should have missed cache as LRU should have been removed

    }

    private static class BindByteArrayMatcher extends BaseMatcher<Object[]>
    {
        private final String tokenString;

        public BindByteArrayMatcher(String tokenString)
        {
            this.tokenString = tokenString;
        }

        @Override
        public void describeTo(Description description)
        {
            description.appendText("one bind param, byte array of 0x" + tokenString);
        }

        @Override
        public boolean matches(Object item)
        {
            if (item instanceof Object[])
            {
                Object[] theArray = (Object[]) item;

                return theArray.length == 1 && Arrays.equals(DatatypeConverter.parseHexBinary(tokenString), (byte[])theArray[0]);
            }
            return false;
        }
    }

}

