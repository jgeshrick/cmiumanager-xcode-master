/*
 *  Neptune Technology Group
 *  Copyright 2015, 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.api.domain.auth;

import com.neptunetg.mdce.common.api.data.TokenInfo;
import com.neptunetg.mdce.common.api.data.TokenRepository;
import com.neptunetg.mdce.common.api.domain.RestClientId;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import org.junit.Test;

import java.lang.Exception;import java.lang.String;import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class TokenManagerServiceImplTest
{
    private static final String PARTNER_KEY_141 =   "e96aa29de811a75c22ea2db049ee7f4f";  //non-prod partner key for utility 141
    private static final String PARTNER_KEY_11245 = "61bd45bc68288bef44902ab9aff223e0";  //non-prod partner key for utility 11245

    @Test(expected = NotAuthorizedException.class)
    public void testGenerateTokenThrowsExceptionWithInvalidPartnerKey() throws Exception
    {
        final TokenRepository tokenRepo = mock(TokenRepository.class);
        TokenManagerServiceImpl service = new TokenManagerServiceImpl(tokenRepo, "1,2");

        service.generateToken("12345", 0, "0.0.0.0"); //invalid partner key length
    }

    @Test(expected = NotAuthorizedException.class)
    public void testGenerateTokenForAllSitesWithInvalidPartnerKey() throws Exception
    {

        final TokenRepository tokenRepo = mock(TokenRepository.class);
        TokenManagerServiceImpl service = new TokenManagerServiceImpl(tokenRepo, "1,2");

        final String partnerKey = PARTNER_KEY_141; //partner key for utility 141

        service.generateToken(partnerKey, 0, "0.0.0.0");

    }


    @Test
    public void testGeneratePartnerKeyForUtility() throws Exception
    {

        final String partnerKey = PARTNER_KEY_11245;

        assertEquals("Unexpected length key " + partnerKey, 32, partnerKey.length());

        assertTrue("Partner key " + partnerKey + " did not match regex", partnerKey.matches("^[0-9a-f]+$"));

    }

    @Test(expected = NotAuthorizedException.class)
    public void testDisallowingNegativeSiteId() throws Exception
    {

        final TokenRepository tokenRepo = mock(TokenRepository.class);
        TokenManagerServiceImpl service = new TokenManagerServiceImpl(tokenRepo, "1,2");

        final String partnerKey = PARTNER_KEY_11245;

        final Instant now = Instant.now();

        final TokenInfo tokenFromDb = new TokenInfo(new byte[]{0x12, 0x34, 0x56, 0x78, 0x12, 0x34, 0x56, 0x78},
                RestClientId.forUtility(11245), now,
                partnerKey, now.plus(1, ChronoUnit.HOURS), "0.0.0.0");
        when(tokenRepo.insertToken(eq(partnerKey), eq(RestClientId.forUtility(11245)), eq("0.0.0.0"), anyInt())).thenReturn(tokenFromDb);

        service.generateToken(partnerKey, -3, "0.0.0.0");

        fail("Exception should have been thrown");

    }

    @Test
    public void testGenerateTokenForUtility() throws Exception
    {
        final TokenRepository tokenRepo = mock(TokenRepository.class);
        TokenManagerServiceImpl service = new TokenManagerServiceImpl(tokenRepo, "1,2");

        final String partnerKey = PARTNER_KEY_11245;

        final Instant now = Instant.now();

        final TokenInfo tokenFromDb = new TokenInfo(new byte[]{0x12, 0x34, 0x56, 0x78, 0x12, 0x34, 0x56, 0x78},
                RestClientId.forUtility(11245), now,
                partnerKey, now.plus(1, ChronoUnit.HOURS), "0.0.0.0");
        when(tokenRepo.insertToken(eq(partnerKey), eq(RestClientId.forUtility(11245)),
                eq("0.0.0.0"), anyInt())).thenReturn(tokenFromDb);


        final TokenInfo tokenReturned = service.generateToken(partnerKey, 11245, "0.0.0.0");

        verify(tokenRepo).insertToken(eq(partnerKey), eq(RestClientId.forUtility(11245)), eq("0.0.0.0"), anyInt());

        assertSame(tokenFromDb, tokenReturned);

        assertFalse(tokenReturned.isExpired());

    }

}