/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserve
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.api.rest.exception;

/**
 * Global REST exception for HTTP 403 Forbidden error
 */
public class ForbiddenException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public ForbiddenException()
    {
    }

    public ForbiddenException(String message)
    {
        super(message);
    }

    public ForbiddenException(Throwable cause)
    {
        super(cause);
    }

    public ForbiddenException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ForbiddenException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
