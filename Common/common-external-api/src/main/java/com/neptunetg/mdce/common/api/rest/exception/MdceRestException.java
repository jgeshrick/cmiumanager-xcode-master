/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserve
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.api.rest.exception;

/**
 * Global REST exception for unexpected exceptions
 */
public class MdceRestException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public MdceRestException()
    {
    }

    public MdceRestException(String message)
    {
        super(message);
    }

    public MdceRestException(Throwable cause)
    {
        super(cause);
    }

    public MdceRestException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MdceRestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
