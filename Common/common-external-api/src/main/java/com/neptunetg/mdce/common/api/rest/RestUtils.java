/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.api.rest;


import com.neptunetg.mdce.common.api.domain.RestClientId;

import javax.servlet.http.HttpServletRequest;
import java.lang.Integer;import java.lang.String;import java.util.Optional;

/**
 * Utilities for REST controllers to use
 */
public final class RestUtils
{
    private static final String CLIENT_ID_ATTRIBUTE = "validatedClientId";

    private RestUtils() //never instantiated
    {
    }

    /**
     * Get the originating IP of a request.  If proxied, this method attempts
     * to find the true originator using HTTP headers.
     *
     * @param request Request
     * @return originating IP
     */
    public static String getOriginatingIp(HttpServletRequest request)
    {
        final String xForwardedFor = request.getHeader("X-Forwarded-For");
        if (xForwardedFor != null)
        {
            final int firstComma = xForwardedFor.indexOf(',');
            if (firstComma < 0)
            {
                return xForwardedFor;
            }
            else
            {
                return xForwardedFor.substring(0, firstComma);
            }
        }
        else
        {
            return request.getRemoteAddr();
        }
    }

    /**
     * Save site id to request attribute.
     * This is used to pass site id from RequestInterceptor to Rest controller.
     * @param request
     * @param restClientId client ID
     */
    public static void setClientIdAttribute(HttpServletRequest request, RestClientId restClientId)
    {
        request.setAttribute(CLIENT_ID_ATTRIBUTE, restClientId);
    }

    /**
     * Get current site id from HTTP request.
     * This is used by Rest Controller to retrieve the site id set by RequestInterceptor.
     * @param request HTTP request
     * @return client ID
     */
    public static RestClientId getClientIdAttribute(final HttpServletRequest request)
    {
        return (RestClientId)request.getAttribute(CLIENT_ID_ATTRIBUTE);
    }

}
