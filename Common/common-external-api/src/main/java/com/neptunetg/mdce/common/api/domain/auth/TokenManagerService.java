/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015, 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.api.domain.auth;

import com.neptunetg.mdce.common.api.data.TokenInfo;
import com.neptunetg.mdce.common.api.domain.RestClientId;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;

/**
 * Service for managing tokens
 */
public interface TokenManagerService
{

    /**
     * Generate a temporary token from partner string for a configurable period of time
     * @param partnerString 32 character partner string
     * @param siteId checked against partner string
     * @param requestIp originating IP address
     * @return generated token
     */
    TokenInfo generateToken(String partnerString, int siteId, String requestIp) throws NotAuthorizedException, BadRequestException;

    /**
     * Check if the token is issued, and it has not expired.
     * @param token Token to check
     * @param requestIp originating IP.  Must match the IP to which the token was issued
     * @return The site ID of the token
     * @throws NotAuthorizedException if the token is not valid
     */
    RestClientId checkToken(String token, String requestIp) throws NotAuthorizedException;

}
