/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015, 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.api.domain.auth;

import com.neptunetg.mdce.common.api.data.TokenInfo;
import com.neptunetg.mdce.common.api.data.TokenRepository;
import com.neptunetg.mdce.common.api.domain.RestClientId;
import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Exception;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

/**
 * Service for managing tokens
 */
@Service
public class TokenManagerServiceImpl implements TokenManagerService
{
    private static final Logger log = LoggerFactory.getLogger(TokenManagerServiceImpl.class);

    private static final int TOKEN_VALIDITY_PERIOD_MINS = 60;

    private static final String ALGORITHM = "AES/ECB/NoPadding";

    private final long ksPwSeed;
    private final long kePwSeed;

    private final TokenRepository tokenRepo;

    @Autowired
    public TokenManagerServiceImpl(
            TokenRepository tokenRepo,
            @Value("#{envProperties['ks.pw.seeds']}") String ksPwSeeds
            )
    {
        this.tokenRepo = tokenRepo;
        final String[] ksPwSeedValues = ksPwSeeds.split(",");
        this.ksPwSeed = Long.parseLong(ksPwSeedValues[0]);
        this.kePwSeed = Long.parseLong(ksPwSeedValues[1]);
    }


    private static int calculateCrc32ForFirst12Bytes(ByteBuffer buf)
    {
        final byte[] unencryptedBytesWithoutCrc = new byte[12];
        buf.get(unencryptedBytesWithoutCrc);

        final Checksum checksum = new CRC32();
        checksum.update(unencryptedBytesWithoutCrc, 0, 12);

        return (int) checksum.getValue();
    }

    private static char[] getKeystorePassword(long seed)
    {
        final char[] ret = new char[40];
        long val = seed;
        for (int i = 0; i < 40; i++)
        {
            val = val / 9L + val * 2L;
            ret[i] = (char) ((val & 0x3fL) + 50L); //append a pseudorandom character
        }
        return ret;
    }


    private ByteBuffer decrypt(String partnerKey)
    {
        try
        {
            final Cipher aesCipher = getPartnerKeyCipher(Cipher.DECRYPT_MODE);
            final byte[] partnerKeyData = DatatypeConverter.parseHexBinary(partnerKey);
            return ByteBuffer.wrap(aesCipher.doFinal(partnerKeyData)).order(ByteOrder.BIG_ENDIAN);
        }
        catch (BadPaddingException | IllegalBlockSizeException e)
        { //none of this is expected
            log.error("Decryption error: " + e.getClass().getName() + " thrown with message: " + e.getMessage(), e);
            throw new MdceRestException("An exception occurred. See log for details");
        }
    }

    private Cipher getPartnerKeyCipher(int mode) throws MdceRestException
    {
        try
        {
            final KeyStore ks = KeyStore.getInstance("JCEKS");

            // get user password and file input stream
            final char[] storePass = getKeystorePassword(ksPwSeed);

            try (final InputStream fis = getClass().getResourceAsStream("/api/api_partner_key.jce"))
            {
                ks.load(fis, storePass);
            }

            final Key secretKey = ks.getKey("pkek", getKeystorePassword(kePwSeed));
            final Cipher aesCipher = Cipher.getInstance(ALGORITHM);
            aesCipher.init(mode, secretKey);
            return aesCipher;
        }
        catch (IOException | KeyStoreException | CertificateException | UnrecoverableKeyException | NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException e)
        { //none of this is expected
            log.error("Encryption error: " + e.getClass().getName() + " thrown with message: " + e.getMessage(), e);
            throw new MdceRestException("An exception occurred. See log for details");
        }
    }

    private static RestClientId getRestClientIdFromDecryptedPartnerString(ByteBuffer decryptedBytes)
    {
        return new RestClientId(decryptedBytes.getInt(4) /*site ID*/, decryptedBytes.getInt(0) /*partner ID */);
    }

    private static boolean checkCrc(ByteBuffer decryptedBytes)
    {
        final int calculatedCrc = calculateCrc32ForFirst12Bytes(decryptedBytes);
        final int embeddedCrc = decryptedBytes.getInt(12);

        return calculatedCrc == embeddedCrc;
    }


    /**
     * Generate a temporary token from client ID for a configurable period of time
     *
     * @return generated token
     */
    @Override
    public TokenInfo generateToken(String partnerString, int siteId, String requestIp) throws NotAuthorizedException
    {
        if (siteId < 0)
        {
            throw new NotAuthorizedException("Site ID must be non-negative: " + siteId);
        }

        RestClientId restClientId = checkPartnerString(partnerString, siteId);

        //TODO: also cache token in memory
        return tokenRepo.insertToken(partnerString, restClientId, requestIp, TOKEN_VALIDITY_PERIOD_MINS);
    }


    private RestClientId checkPartnerString(String partnerString, int siteId) throws NotAuthorizedException
    {
        if (partnerString.length() != 32)
        {
            throw new NotAuthorizedException("partner string length not 32 characters");
        }

        final ByteBuffer decryptedBytes = decrypt(partnerString);

        if (!checkCrc(decryptedBytes))
        {
            throw new NotAuthorizedException();
        }

        final RestClientId clientIdFromPartnerString = getRestClientIdFromDecryptedPartnerString(decryptedBytes);

        if (clientIdFromPartnerString.getSiteId() != siteId)
        {
            throw new NotAuthorizedException();
        }
        return clientIdFromPartnerString;
    }

    private TokenInfo getTokenOrThrowException(String token) throws NotAuthorizedException
    {
        try
        {
            return tokenRepo.getToken(token);
        }
        catch (EmptyResultDataAccessException e)
        {
            throw new NotAuthorizedException("Token not found");
        }
        catch (Exception e)
        {
            throw new NotAuthorizedException("Exception thrown retrieving Token" + e.getClass().getName() + " " + e.getMessage(), e);
        }

    }


    /**
     * Check if the token is issued, and it has not expired. This also extend the token time.
     *
     * @param token     Token to check
     * @param requestIp originating IP.  Must match the IP to which the token was issued
     * @return REST client ID of valid token
     * @throws NotAuthorizedException if the token is not valid
     */
    @Override
    public RestClientId checkToken(String token, String requestIp) throws NotAuthorizedException
    {

        final TokenInfo tokenInfo = getTokenOrThrowException(token);

        if (tokenInfo == null)
        {
            throw new NotAuthorizedException("Token " + token + " not recognized");
        }

        if (!requestIp.equals(tokenInfo.getRequestIp()))
        {
            throw new NotAuthorizedException("IP mismatch - token created for " + tokenInfo.getRequestIp() + " but request originates from " + requestIp);
        }

        if (tokenInfo.isExpired())
        {
            throw new NotAuthorizedException("Token expired at " + tokenInfo.getExpiry());
        }

        return tokenInfo.getClientId();
    }

}
