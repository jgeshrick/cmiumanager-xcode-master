/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserve
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.api.rest.exception;

/**
 * Global REST exception handler for HTTP 401 Not Authorized error
 */
public class NotAuthorizedException extends Exception
{
    private static final long serialVersionUID = 1L;

    public NotAuthorizedException()
    {
    }

    public NotAuthorizedException(String message)
    {
        super(message);
    }

    public NotAuthorizedException(Throwable cause)
    {
        super(cause);
    }

    public NotAuthorizedException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public NotAuthorizedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
