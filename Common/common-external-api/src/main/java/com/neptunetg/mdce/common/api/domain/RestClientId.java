/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */
package com.neptunetg.mdce.common.api.domain;

import org.springframework.web.client.RestClientException;

/**
 * Value object that encapsulates the site ID and partner ID of a REST client
 */
public final class RestClientId
{
    private final int siteId;
    private final int partnerId;

    public static final RestClientId MSOC = new RestClientId(0, 0);
    public static final RestClientId BPC = new RestClientId(0, 1);
    public static final RestClientId FPC = new RestClientId(0, 2);

    public RestClientId(int siteId, int partnerId)
    {
        this.siteId = siteId;
        this.partnerId = partnerId;
    }

    public static RestClientId forUtility(int siteId)
    {
        return new RestClientId(siteId, 0);
    }

    public boolean isMsoc()
    {
        return MSOC.equals(this);
    }

    public boolean isBpc()
    {
        return BPC.equals(this);
    }

    public boolean isFpc()
    {
        return FPC.equals(this);
    }

    public int getSiteId()
    {
        return this.siteId;
    }

    public Integer getSiteIdWrapperValue()
    {
        return Integer.valueOf(this.siteId);
    }

    public int getPartnerId()
    {
        return this.partnerId;
    }

    public Integer getPartnerIdWrapperValue()
    {
        return Integer.valueOf(this.partnerId);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RestClientId that = (RestClientId) o;

        if (siteId != that.siteId) return false;
        return partnerId == that.partnerId;

    }

    @Override
    public int hashCode()
    {
        int result = siteId;
        result = 31 * result + partnerId;
        return result;
    }

    @Override
    public String toString()
    {
        return "RestClientId{" +
                "siteId=" + siteId +
                ", partnerId=" + partnerId +
                '}';
    }

    /**
     * To be valid, site ID and partner ID must be non-negative
     * @return true if non-negatice
     */
    public boolean isValid()
    {
        if (siteId < 0 || partnerId < 0)
        {
            return false;
        }
        return true;
    }
}
