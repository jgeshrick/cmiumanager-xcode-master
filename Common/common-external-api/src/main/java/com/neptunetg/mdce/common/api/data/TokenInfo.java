/*
 *  Neptune Technology Group
 *  Copyright 2015, 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.api.data;

import com.neptunetg.mdce.common.api.domain.RestClientId;

import javax.xml.bind.DatatypeConverter;
import java.time.Instant;

/**
 * Information about a valid token
 */
public class TokenInfo
{
    private final String tokenString;

    private final RestClientId clientId;

    private final Instant created;

    private final String partnerKey;

    private final Instant expiry;

    private final String requestIp;

    public TokenInfo(byte[] tokenBytes, RestClientId clientId, Instant created, String partnerKey, Instant expiry, String requestIp)
    {
        this.tokenString = DatatypeConverter.printHexBinary(tokenBytes);
        this.clientId = clientId;
        this.created = created;
        this.partnerKey = partnerKey;
        this.expiry = expiry;
        this.requestIp = requestIp;
    }

    public String getTokenString()
    {
        return tokenString;
    }

    public RestClientId getClientId()
    {
        return this.clientId;
    }

    public Instant getCreated()
    {
        return created;
    }

    public boolean isExpired()
    {
        return Instant.now().isAfter(this.expiry);
    }

    public String getPartnerKey()
    {
        return partnerKey;
    }

    public Instant getExpiry()
    {
        return expiry;
    }

    public String getRequestIp()
    {
        return requestIp;
    }


}
