/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.api.data;

import com.neptunetg.mdce.common.api.domain.RestClientId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.xml.bind.DatatypeConverter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Interacts with DB for token management
 */
@Repository
public class JdbcTokenRepository implements TokenRepository, RowMapper<TokenInfo>
{
    private final JdbcTemplate db;

    private static final Logger log = LoggerFactory.getLogger(JdbcTokenRepository.class);

    private static final int MAX_CACHE_SIZE = 1000; //max number of tokens to remember
    private final Map<String, TokenInfo> tokenCache = new HashMap<>(); //simple LRU cache
    private final Map<String, Instant> tokenCacheAccessTime = new HashMap<>(); //last access time for cache

    @Autowired
    public JdbcTokenRepository(JdbcTemplate db)
    {
        this.db = db;
    }

    @Override
    public TokenInfo insertToken(String partnerKeyHex, RestClientId clientId, String originatingIp, int validityMins)
    {
        final Instant insertTime = Instant.now();
        final Instant expiryTime = insertTime.plus(validityMins, ChronoUnit.MINUTES);

        final Date insertTimeDate = Date.from(insertTime);
        final Date expiryTimeDate = Date.from(expiryTime);

        final String insertSql = "INSERT INTO mdce.mdce_api_tokens " +
                "(`token`, `created`, `partner_key`, `originating_ip`, `expiry`, `site_id`, partner_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";

        final Random randomToken = new Random();
        final byte[] tokenBytes = new byte[8];
        final byte[] partnerKey = DatatypeConverter.parseHexBinary(partnerKeyHex);
        while (true)
        {
            try
            {
                randomToken.nextBytes(tokenBytes);
                db.update(insertSql,
                        tokenBytes, insertTimeDate,
                        partnerKey,
                        originatingIp, expiryTimeDate,
                        clientId.getSiteIdWrapperValue(),
                        clientId.getPartnerIdWrapperValue());
                break;
            }
            catch (DuplicateKeyException e)
            {
                //whoops, duplicate token, try again
            }

        }
        final TokenInfo ret = new TokenInfo(tokenBytes, clientId, insertTime, partnerKeyHex, expiryTime, originatingIp);

        cacheToken(ret);

        return ret;
    }


    @Override
    public TokenInfo getToken(String token)
    {
        TokenInfo ret = getTokenFromCache(token);
        if (ret == null)
        {
            final String selectSql = "select token, created, partner_key, originating_ip, expiry, site_id, partner_id from mdce.mdce_api_tokens " +
                    "where token=?";
            ret = db.queryForObject(selectSql, new Object[]{DatatypeConverter.parseHexBinary(token)}, this);
            cacheToken(ret);
        }
        return ret;
    }


    @Override
    public TokenInfo mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        final byte[] tokenBytes = rs.getBytes(1);
        final int siteId = rs.getInt(6);
        final int partnerId = rs.getInt(7);
        final RestClientId clientId = new RestClientId(siteId, partnerId);
        final Instant created = rs.getTimestamp(2).toInstant();
        final String partnerKey = DatatypeConverter.printHexBinary(rs.getBytes(3));
        final String originatingIp = rs.getString(4);
        final Instant expiry = rs.getTimestamp(5).toInstant();
        return new TokenInfo(tokenBytes, clientId, created, partnerKey, expiry, originatingIp);
    }

    private void cacheToken(TokenInfo token)
    {
        synchronized (tokenCache)
        {

            if (token != null)
            {
                final String tokenString = token.getTokenString();
                tokenCache.put(tokenString, token);
                tokenCacheAccessTime.put(tokenString, Instant.now());
                int tokensToDelete = tokenCache.size() - MAX_CACHE_SIZE;
                if (tokensToDelete > 0)
                {
                    final List<String> leastRecentlyUsedKeysToDelete =
                            tokenCacheAccessTime.entrySet().stream()
                            .sorted(Map.Entry.comparingByValue())  //oldest first
                            .map(Map.Entry::getKey)
                            .limit(tokensToDelete)
                            .collect(Collectors.toList());

                    for (String ts : leastRecentlyUsedKeysToDelete)
                    {
                        tokenCache.remove(ts);
                        tokenCacheAccessTime.remove(ts);
                    }
                }
            }
        }
    }


    private TokenInfo getTokenFromCache(String tokenString)
    {
        synchronized (tokenCache)
        {
            final TokenInfo ret = tokenCache.get(tokenString);
            if (ret != null)
            {
                tokenCacheAccessTime.put(tokenString, Instant.now());
            }
            return ret;
        }
    }
}
