/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.api.data;

import com.neptunetg.mdce.common.api.domain.RestClientId;

/**
 * Interactions with DB for tokens
 */
public interface TokenRepository
{
    TokenInfo insertToken(String partnerKey, RestClientId restClientId, String originatingIp, int validityMins);

    TokenInfo getToken(String token);
}
