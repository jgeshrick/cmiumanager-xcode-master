/*
 *  Neptune Technology Group
 *  Copyright 2015, 2016 as unpublished work.
 *  All rights reserve
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.api.rest.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * Controller advice to specifically handle exceptions in REST api.
 */
@ControllerAdvice
public class RestExceptionHandlingControllerAdvice
{
    private static final Logger log = LoggerFactory.getLogger(RestExceptionHandlingControllerAdvice.class);

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<String> catchAllErrorHandler(final Exception caughtException, final HttpServletRequest requestContext)
    {
        Throwable rootCause = getRootCause(caughtException);

        HttpStatus httpStatus = determineResponseCode(caughtException, rootCause);

        String responseMessage = composeResponseMessage(httpStatus, rootCause);

        final String pathInfo = (requestContext == null) ? "" : "; Request URI: " + requestContext.getRequestURI();

        if (httpStatus.is5xxServerError())
        {
            logExceptionReportingRootCause(responseMessage + pathInfo, rootCause);
        }
        else
        {
            log.warn(responseMessage + pathInfo);
        }

        return returnResponse(responseMessage, httpStatus);
    }

    private String composeResponseMessage(HttpStatus httpStatus, Throwable rootCause)
    {
        if (httpStatus == HttpStatus.FORBIDDEN)
        {
            return "{\"ResultCode\":403, " + "\"ErrorString\":\"Forbidden. The utility is authenticated, but is not allowed to do the specific operation. ";
        }

        String message = rootCause.getMessage();
        if (message == null) {
            message = rootCause.getStackTrace()[0].toString();
        }

        return "{\"ResultCode\":" + httpStatus.value() + ", \"ErrorString\":\"" + httpStatus.getReasonPhrase() + ". " + rootCause.getClass().getSimpleName() + " - " + message + "\"}";
    }

    private HttpStatus determineResponseCode(Throwable caughtException, Throwable rootCause)
    {
        // if a spring servlet exception has been thrown then interpret it as a bad request (eg missing parameter)
        if (isServletException(caughtException, rootCause)
                || isBadRequest(caughtException, rootCause))
        {
            return HttpStatus.BAD_REQUEST;
        }
        if (isNotAuthorized(caughtException, rootCause))
        {
            return HttpStatus.UNAUTHORIZED;
        }
        if (isForbidden(caughtException, rootCause))
        {
            return HttpStatus.FORBIDDEN;
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;

    }

    private void logExceptionReportingRootCause(String responseMessage, Throwable rootCause)
    {
        log.error(responseMessage, rootCause);
    }

    private Throwable getRootCause(Throwable throwable)
    {
        Throwable rootCause = throwable;
        while (rootCause.getCause() != null)
        {
            rootCause = rootCause.getCause();
        }
        return rootCause;
    }

    private ResponseEntity<String> returnResponse(String responseMessage, HttpStatus status)
    {
        HttpHeaders responseHeaders = getJsonContentType();
        return new ResponseEntity<>(responseMessage, responseHeaders, status);
    }

    private HttpHeaders getJsonContentType()
    {
        HttpHeaders responseHeaders = new HttpHeaders();
//        responseHeaders.setLocation(location);
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
        return responseHeaders;
    }

    private boolean isForbidden(Throwable caughtException, Throwable rootCause)
    {
        return isForbidden(caughtException) || isForbidden(rootCause);
    }

    private boolean isForbidden(Throwable throwable)
    {
        return throwable instanceof ForbiddenException;
    }

    private boolean isNotAuthorized(Throwable caughtException, Throwable rootCause)
    {
        return isNotAuthorized(caughtException) || isNotAuthorized(rootCause);
    }

    private boolean isNotAuthorized(Throwable throwable)
    {
        return throwable instanceof NotAuthorizedException;
    }

    private boolean isBadRequest(Throwable caughtException, Throwable rootCause)
    {
        return isBadRequest(caughtException) || isBadRequest(rootCause);
    }

    private boolean isBadRequest(Throwable throwable)
    {
        return throwable instanceof BadRequestException;
    }

    private boolean isServletException(Throwable caughtException, Throwable rootCause)
    {
        return isServletException(caughtException) || isServletException(rootCause);
    }

    private boolean isServletException(Throwable throwable)
    {
        return throwable instanceof ServletException;
    }
}
