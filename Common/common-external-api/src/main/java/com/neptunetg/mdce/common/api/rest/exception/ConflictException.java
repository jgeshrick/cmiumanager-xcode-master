/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.mdce.common.api.rest.exception;

/**
 * Global REST exception handler for HTTP 409 conflict exception
 */
public class ConflictException extends Exception
{
    private static final long serialVersionUID = 1L;

    public ConflictException()
    {
    }

    public ConflictException(String message)
    {
        super(message);
    }

    public ConflictException(Throwable cause)
    {
        super(cause);
    }

    public ConflictException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ConflictException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
