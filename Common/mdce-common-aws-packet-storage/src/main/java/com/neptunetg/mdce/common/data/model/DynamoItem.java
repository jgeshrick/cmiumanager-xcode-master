/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data.model;

import com.amazonaws.services.dynamodbv2.datamodeling.S3Link;

import java.nio.ByteBuffer;

/**
 * Created by sml1 on 30/07/2015.
 */
public interface DynamoItem
{
    ByteBuffer getPacketData();

    void setPacketData(ByteBuffer packetData);
    S3Link getPacketDataExternal();
    void setPacketDataExternal(S3Link packetDataExternal);
    void setPacketData(byte[] packetData);


    /**
     * Getter for insert date
     */
    long getInsertDate();

    void setInsertDate(long insertDate);

}
