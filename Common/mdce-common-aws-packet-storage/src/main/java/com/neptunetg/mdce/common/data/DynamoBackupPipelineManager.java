/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015, 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.data;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.services.datapipeline.DataPipelineClient;
import com.amazonaws.services.datapipeline.model.ActivatePipelineRequest;
import com.amazonaws.services.datapipeline.model.CreatePipelineRequest;
import com.amazonaws.services.datapipeline.model.CreatePipelineResult;
import com.amazonaws.services.datapipeline.model.DeletePipelineRequest;
import com.amazonaws.services.datapipeline.model.DescribePipelinesRequest;
import com.amazonaws.services.datapipeline.model.DescribePipelinesResult;
import com.amazonaws.services.datapipeline.model.Field;
import com.amazonaws.services.datapipeline.model.ListPipelinesResult;
import com.amazonaws.services.datapipeline.model.ParameterAttribute;
import com.amazonaws.services.datapipeline.model.ParameterObject;
import com.amazonaws.services.datapipeline.model.ParameterValue;
import com.amazonaws.services.datapipeline.model.PipelineDescription;
import com.amazonaws.services.datapipeline.model.PipelineIdName;
import com.amazonaws.services.datapipeline.model.PipelineNotFoundException;
import com.amazonaws.services.datapipeline.model.PipelineObject;
import com.amazonaws.services.datapipeline.model.PutPipelineDefinitionRequest;
import com.amazonaws.services.datapipeline.model.PutPipelineDefinitionResult;
import com.amazonaws.services.datapipeline.model.Tag;
import com.amazonaws.services.datapipeline.model.ValidatePipelineDefinitionRequest;
import com.amazonaws.services.datapipeline.model.ValidatePipelineDefinitionResult;
import com.amazonaws.services.datapipeline.model.ValidationError;
import com.amazonaws.services.datapipeline.model.ValidationWarning;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.YearMonth;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Manages backup pipelines for Dynamo tables
 */
public class DynamoBackupPipelineManager
{

    private static final Logger log = LoggerFactory.getLogger(DynamoBackupPipelineManager.class);

    private final DynamoPacketRepositoryConfig config;

    private final Region dynamoEndpointRegion;

    private final DataPipelineClient dataPipelineClient;

    private final AmazonS3Client s3Client;

    private static final String failureSubject = "Data backup failed";

    public DynamoBackupPipelineManager(
            AWSCredentialsProvider credentialsProvider,
            DynamoPacketRepositoryConfig config
    )
    {
        this.config = config;

        this.dynamoEndpointRegion = config.getRegion();

        this.dataPipelineClient = new DataPipelineClient(credentialsProvider);
        this.dataPipelineClient.setRegion(dynamoEndpointRegion);

        this.s3Client = new AmazonS3Client(credentialsProvider);
        this.s3Client.setRegion(dynamoEndpointRegion);
    }

    /**
     * Check for old backups of the table.  The backup makes a new folder every day.  The old
     * folders should be deleted to prevent accumulation in S3.  The most recently modified folder
     * containing a _SUCCESS file and the folder with the lexicographically maximum folder
     * name containing a _SUCCESS file will be retained, the other folders containing
     * a _SUCCESS file will be removed.
     * @return true if any old folders were deleted
     */
    public boolean deleteSupersededBackups(String tableName, YearMonth date)
    {
        final String bucketName = config.getBackupS3Bucket();
        final String backupPath = config.getBackupS3FolderPath(tableName, date);
        try
        {
            ObjectListing currentFolders = this.s3Client.listObjects( //recursive listing
                    bucketName,  // e.g. neptune-mdce-int-old-data
                    backupPath); // e.g. /2015-10/dynamodb/env-miu-packets-received-2015-10

            S3ObjectSummary mostRecentlyModifiedSuccessFile = null;
            S3ObjectSummary lexicographicMaxSuccessFile = null;

            if (log.isDebugEnabled())
            {
                log.debug("Looking for superseded backup files for " + tableName + ", date " + date);
            }

            final Map<String, S3ObjectSummary> successFiles = new HashMap<>();
            final List<String> allKeys = new ArrayList<>();
            do
            {
                for (S3ObjectSummary objectSummary : currentFolders.getObjectSummaries())
                {
                    allKeys.add(objectSummary.getKey());
                    if (isSuccessFile(objectSummary))
                    {
                        if (log.isDebugEnabled())
                        {
                            log.debug("Found backup " + objectSummary.getKey() + ".");
                        }
                        successFiles.put(objectSummary.getKey(), objectSummary);
                        if (mostRecentlyModifiedSuccessFile == null || objectSummary.getLastModified().toInstant().isAfter(mostRecentlyModifiedSuccessFile.getLastModified().toInstant()))
                        {
                            mostRecentlyModifiedSuccessFile = objectSummary;
                        }
                        if (lexicographicMaxSuccessFile == null || objectSummary.getKey().compareTo(lexicographicMaxSuccessFile.getKey()) > 0)
                        {
                            lexicographicMaxSuccessFile = objectSummary;
                        }
                    }
                }

                currentFolders = this.s3Client.listNextBatchOfObjects(currentFolders);
            } while (currentFolders.isTruncated());

            if (mostRecentlyModifiedSuccessFile != null)
            {
                log.debug("Most recently modified backup is " + mostRecentlyModifiedSuccessFile.getKey() + "; it will not be deleted");
                successFiles.remove(mostRecentlyModifiedSuccessFile.getKey());
            }

            if (lexicographicMaxSuccessFile != null)
            {
                log.debug("Backup with latest timestamp in name is " + lexicographicMaxSuccessFile.getKey() + "; it will not be deleted");
                successFiles.remove(lexicographicMaxSuccessFile.getKey());
            }

            boolean ret = false;
            for (S3ObjectSummary successFileInFolder : successFiles.values())
            {
                final String[] filesInFolder = filterToFolderContainingSuccessFile(allKeys, successFileInFolder);
                final DeleteObjectsRequest delReq = new DeleteObjectsRequest(bucketName)
                        .withKeys(filesInFolder);
                log.info("Deleting historic backup folder containing " + successFileInFolder.getKey() + " - " + filesInFolder.length + " S3 objects to delete");
                try
                {
                    this.s3Client.deleteObjects(delReq);
                    ret = true;
                }
                catch (Exception e)
                {
                    log.error("Failed to delete historic backup " + successFileInFolder.getKey());
                }

            }
            return ret;
        }
        catch (Exception e)
        {
            log.error("Error while checking for historic backups in " + bucketName + ", path " + backupPath, e);
            return false;
        }
    }

    private String[] filterToFolderContainingSuccessFile(Collection<String> allKeys, S3ObjectSummary successFileInFolder)
    {
        if (isSuccessFile(successFileInFolder))
        {
            final String successFileKey = successFileInFolder.getKey();
            final String keyPrefix = successFileKey.substring(0, successFileKey.length() - "/_SUCCESS".length());
            return allKeys.stream()
                    .filter(k -> k.startsWith(keyPrefix))
                    .sorted(DynamoBackupPipelineManager::compareS3KeysDepthFirst)
                    .toArray(size -> new String[size]);
        }
        else
        {
            throw new IllegalArgumentException("Not a success file! " + successFileInFolder.getKey());
        }
    }

    /**
     * Comparator for S3 keys that facilitates deleting the deepest objects first, so that
     * folders are empty when we ask to delete them
     * @param k1 First key
     * @param k2 Second key
     * @return -1 if k1 is a subfile of k2
     */
    private static int compareS3KeysDepthFirst(String k1, String k2)
    {
        //return -1 to delete k1 first, return 1 to delete k2 first
        if (k2.startsWith(k1))
        {
            return 1; //remove k2 first, it is deeper
        }
        else if (k1.startsWith(k2))
        {
            return -1; //remove k1 first, it is deeper
        }
        else
        { //no containment relationship so don't care which goes first
            return k1.compareTo(k2);
        }
    }

    private boolean isSuccessFile(S3ObjectSummary objectSummary)
    {
        return objectSummary.getKey().endsWith("/_SUCCESS");
    }


    public boolean deleteOldBackupPipelines()
    {
        log.debug("Checking for completed backup data pipelines");
        boolean ret = false;
        for (PipelineIdName idAndName : listPipelines())
        {

            if (config.isBackupPipelineForMyEnvironment(idAndName.getName()))
            {

                final DescribePipelinesRequest describePipelinesRequest = new DescribePipelinesRequest()
                        .withPipelineIds(idAndName.getId());

                log.debug("Checking whether data backup pipeline " + idAndName.getName() + " (" + idAndName.getId() + ") has finished");
                try
                {
                    final DescribePipelinesResult describePipelinesResult = this.dataPipelineClient.describePipelines(describePipelinesRequest);
                    if (!describePipelinesResult.getPipelineDescriptionList().isEmpty())
                    {
                        final PipelineDescription pipelineDescription = describePipelinesResult.getPipelineDescriptionList().get(0);

                        final List<Field> pipelineFields = pipelineDescription.getFields();
                        if (checkDeletePipeline(pipelineFields))
                        {
                            log.info("Backup pipeline " + pipelineDescription.getName() + " has finished.  Deleting");

                            deleteBackupPipeline(idAndName);
                            ret = true;
                        }
                    }
                }
                catch (PipelineNotFoundException e)
                {
                    log.warn("Pipeline with ID " + idAndName.getId() + " cannot be described.", e);
                }
            }
        }
        return ret;
    }

    /**
     * Delete the pipeline if backups are disabled for the current environment or the pipeline is finished (has expired)
     * @param pipelineFields List of pipeline fields
     * @return True if pipeline is to be deleted.
     */
    private boolean checkDeletePipeline(Collection<Field> pipelineFields)
    {
        if (!config.isBackupEnabled())
        {
            return false;
        }
        else
        {
            for (Field f : pipelineFields)
            {
                //see http://docs.aws.amazon.com/datapipeline/latest/DeveloperGuide/dp-pipeline-status.html

                if ("@pipelineState".equals(f.getKey()) && "FINISHED".equals(f.getStringValue()))
                {
                    return true;
                }
            }
        }
        return false;
    }

    private void deleteBackupPipeline(PipelineIdName idAndName)
    {

        try
        {
            final DeletePipelineRequest deletePipelineRequest = new DeletePipelineRequest()
                    .withPipelineId(idAndName.getId());

            log.info("Deleting data backup pipeline " + idAndName.getName() + " (ID = " + idAndName.getId() + ")");

            this.dataPipelineClient.deletePipeline(deletePipelineRequest);
        } catch (Exception e)
        {
            log.error("Failed to delete data pipeline " + idAndName.getName() + " (ID = " + idAndName.getId() + ")", e);
        }

    }

    public List<PipelineIdName> listPipelines()
    {
        final ListPipelinesResult listPipelinesResult = this.dataPipelineClient.listPipelines();
        return listPipelinesResult.getPipelineIdList();
    }

    public boolean ensureBackupPipelineForTable(String tableName, YearMonth date, Collection<PipelineIdName> currentPipelines) throws Exception
    {
        final String pipelineName = config.getBackupPipelineNameForTable(tableName);

        final Optional<PipelineIdName> existingPipeline = currentPipelines.stream().filter(idn -> pipelineName.equals(idn.getName())).findFirst();

        log.debug("Checking data backup pipeline " + pipelineName + " is present");
        if (existingPipeline.isPresent())
        {
            log.debug("Data backup pipeline " + pipelineName + " is present");
            return false; //TODO: we could check that the data pipeline seems okay if present
        }
        else
        {
            log.info("Data backup pipeline to be created: " + pipelineName);

            createBackupPipeline(pipelineName, tableName, date);

            return true;

        }


    }

    private void createBackupPipeline(String pipelineName, String tableName, YearMonth date) throws Exception
    {
        log.info("Creating data backup pipeline " + pipelineName + " for table " + tableName + ", month = " + date);

        final CreatePipelineRequest createPipelineRequest = new CreatePipelineRequest()
                .withName(pipelineName)
                .withUniqueId(pipelineName)
                .withTags(new Tag().withKey("Env").withValue(config.getEnvName()));

        final CreatePipelineResult createResult = this.dataPipelineClient.createPipeline(createPipelineRequest);

        final String pipelineId = createResult.getPipelineId();

        ZonedDateTime startTime = config.getArchiveStartTimeForTableDate(date);
        while (startTime.toInstant().isBefore(Instant.now()))
        {
            startTime = startTime.plus(1L, ChronoUnit.DAYS);
        }
        final ZonedDateTime endTime = config.getArchiveEndTimeForTableDate(date);

        if (endTime.isBefore(startTime))
        {
            log.error("Cannot create data backup pipeline " + pipelineName + " because start time " + startTime + " is after end time " + endTime);
        }
        else
        {

            final List<ParameterObject> parameterObjects = new ArrayList<>();
            final List<ParameterValue> parameterValues = new ArrayList<>();
            final List<PipelineObject> pipelineObjects = new ArrayList<>();

            preparePipelineDefinition(tableName, this.dynamoEndpointRegion.getName(),
                    config.getBackupEmrSubnetId(), config.getBackupEmrKeyPairName(),
                    config.getBackupS3FolderUrl(tableName, date),
                    endTime, config.getBackupSnsArn(),
                    parameterObjects, parameterValues, pipelineObjects);

            final ValidatePipelineDefinitionRequest validateRequest = createValidatePipelineDefinitionRequest(pipelineId, parameterObjects, parameterValues, pipelineObjects);

            final ValidatePipelineDefinitionResult validateResult = this.dataPipelineClient.validatePipelineDefinition(validateRequest);

            logWarningsAndErrors(pipelineName, validateResult.getErrored(), validateResult.getValidationWarnings(), validateResult.getValidationErrors());

            if (Boolean.TRUE.equals(validateResult.getErrored()))
            {
                log.error("Cannot create backup pipeline " + pipelineName + " due to validation errors!  Deleting the empty pipeline from AWS.");

                deleteBackupPipeline(new PipelineIdName().withName(pipelineName).withId(pipelineId));
            }
            else
            {

                final PutPipelineDefinitionRequest pipelineDef = createPutPipelineDefinitionRequest(pipelineId, parameterObjects, parameterValues, pipelineObjects);

                final PutPipelineDefinitionResult pipelineResult = this.dataPipelineClient.putPipelineDefinition(pipelineDef);

                logWarningsAndErrors(pipelineName, pipelineResult.getErrored(), pipelineResult.getValidationWarnings(), pipelineResult.getValidationErrors());


                if (Boolean.TRUE.equals(pipelineResult.getErrored()))
                {
                    log.error("Error occurred while setting pipeline definition for " + pipelineName);
                }
                else
                {

                    final Date startTimeDate = Date.from(startTime.toInstant());

                    final ActivatePipelineRequest activatePipelineRequest = new ActivatePipelineRequest()
                            .withPipelineId(pipelineId)
                            .withStartTimestamp(startTimeDate);

                    this.dataPipelineClient.activatePipeline(activatePipelineRequest);

                    log.info("Activated data backup pipeline " + pipelineName);

                }
            }
        }
    }

    private void logWarningsAndErrors(String pipelineName, Boolean errored, List<ValidationWarning> validationWarnings, List<ValidationError> validationErrors)
    {

        if (validationWarnings != null)
        {
            for (ValidationWarning warning : validationWarnings)
            {
                for (String warningText : warning.getWarnings())
                {
                    log.warn("Validation warning when creating backup pipeline " + pipelineName + ": " + warningText);
                }
            }
        }
        if (validationErrors != null)
        {
            for (ValidationError error : validationErrors)
            {
                for (String errorText : error.getErrors())
                {
                    log.error("Validation error when creating backup pipeline " + pipelineName + ": " + errorText);
                }
            }
        }
        if (Boolean.TRUE.equals(errored))
        {
            log.error("Error in pipeline definition for " + pipelineName);
        }
    }


    private PutPipelineDefinitionRequest createPutPipelineDefinitionRequest(String pipelineId,
                                                                            List<ParameterObject> parameterObjects,
                                                                            List<ParameterValue> parameterValues,
                                                                            List<PipelineObject> pipelineObjects)
    {

        final PutPipelineDefinitionRequest pipelineDef = new PutPipelineDefinitionRequest()
                .withPipelineId(pipelineId)
                .withParameterObjects(parameterObjects)
                .withParameterValues(parameterValues)
                .withPipelineObjects(pipelineObjects);

        return pipelineDef;
    }



    private ValidatePipelineDefinitionRequest createValidatePipelineDefinitionRequest(String pipelineId,
                                                                                      List<ParameterObject> parameterObjects,
                                                                                      List<ParameterValue> parameterValues,
                                                                                      List<PipelineObject> pipelineObjects)
    {
        final ValidatePipelineDefinitionRequest ret = new ValidatePipelineDefinitionRequest()
                .withPipelineId(pipelineId)
                .withParameterObjects(parameterObjects)
                .withParameterValues(parameterValues)
                .withPipelineObjects(pipelineObjects);

        return ret;
    }

    /**
     * Prepare the backup datapipeline definition
     * @param tableName Name of table to back up
     * @param region Region of table to back up
     * @param emrClusterSubnetId Subnet where EMR cluster should be created
     * @param emrClusterKeyPairName Key pair to use for EMR instances
     * @param backupS3FolderUrl S3 location for backup
     * @param endTime Date/time to run the final backup (after the table goes cold)
     * @param snsTopicArn ARN of SNS topic to notify if backup fails
     * @param parameterObjectsToPopulate mutable List that acts as OUT parameter to receive the parameter keys for the pipeline
     * @param parameterValuesToPopulate mutable List that acts as OUT parameter to receive the parameter values for the pipeline
     * @param pipelineObjectsToPopulate mutable List that acts as OUT parameter to receive the pipeline objects for the pipeline
     */
    private static void preparePipelineDefinition(String tableName, String region,
                                                  String emrClusterSubnetId, String emrClusterKeyPairName,
                                                  String backupS3FolderUrl, ZonedDateTime endTime, String snsTopicArn,
                                           List<ParameterObject> parameterObjectsToPopulate, List<ParameterValue> parameterValuesToPopulate, List<PipelineObject> pipelineObjectsToPopulate)
    {

        final String endTimeUtcIso8601 = DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(endTime.withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime());
        final boolean snsOnFail = StringUtils.hasText(snsTopicArn);

        ParameterAttribute type;
        ParameterAttribute desc;
        ParameterAttribute watermark;
        ParameterAttribute defaultValue;

        type = new ParameterAttribute().withKey("type").withStringValue("String");
        desc = new ParameterAttribute().withKey("description").withStringValue("Source DynamoDB table name");
        final ParameterObject paramMyDDBTableName = new ParameterObject().withId("myDDBTableName").withAttributes(type, desc);
        final ParameterValue valueMyDDBTableName = new ParameterValue().withId("myDDBTableName").withStringValue(tableName);

        type = new ParameterAttribute().withKey("type").withStringValue("AWS::S3::ObjectKey");
        desc = new ParameterAttribute().withKey("description").withStringValue("Output S3 folder");
        final ParameterObject paramMyOutputS3Loc = new ParameterObject().withId("myOutputS3Loc").withAttributes(type, desc);
        final ParameterValue valueMyOutputS3Loc = new ParameterValue().withId("myOutputS3Loc").withStringValue(backupS3FolderUrl); //e.g. s3://neptune-mdce-int-old-data/2015-10/dynamodb/env-miu-packets-received-2015-10

        type = new ParameterAttribute().withKey("type").withStringValue("Double");
        desc = new ParameterAttribute().withKey("description").withStringValue("DynamoDB read throughput ratio");
        watermark = new ParameterAttribute().withKey("watermark").withStringValue("Enter value between 0.1-1.0");
        defaultValue = new ParameterAttribute().withKey("default").withStringValue("0.2");
        final ParameterObject paramMyDDBReadThroughputRatio = new ParameterObject().withId("myDDBReadThroughputRatio").withAttributes(type, desc, watermark, defaultValue);
        final ParameterValue valueMyDDBReadThroughputRatio = new ParameterValue().withId("myDDBReadThroughputRatio").withStringValue("0.3"); //e.g. s3://neptune-mdce-int-old-data/2015-10/dynamodb/env-miu-packets-received-2015-10

        type = new ParameterAttribute().withKey("type").withStringValue("String");
        desc = new ParameterAttribute().withKey("description").withStringValue("Region of the DynamoDB table");
        watermark = new ParameterAttribute().withKey("watermark").withStringValue("us-east-1");
        defaultValue = new ParameterAttribute().withKey("default").withStringValue("us-east-1");
        final ParameterObject paramMyDDBRegion = new ParameterObject().withId("myDDBRegion").withAttributes(type, desc, watermark, defaultValue);
        final ParameterValue valueMyDDBRegion = new ParameterValue().withId("myDDBRegion").withStringValue(region);

        final PipelineObject ddbSourceTablePipelineObject = new PipelineObject()
                .withId("DDBSourceTable").withName("DDBSourceTable")
                .withFields(
                        new Field().withKey("readThroughputPercent").withStringValue("#{myDDBReadThroughputRatio}"),
                        new Field().withKey("type").withStringValue("DynamoDBDataNode"),
                        new Field().withKey("tableName").withStringValue("#{myDDBTableName}")
                );

        // See http://docs.aws.amazon.com/datapipeline/latest/DeveloperGuide/dp-object-emrcluster.html
        final PipelineObject emrClusterForBackupPipelineObject = new PipelineObject()
                .withId("EmrClusterForBackup").withName("EmrClusterForBackup")
                .withFields(
                        new Field().withKey("amiVersion").withStringValue("3.8.0"),
                        new Field().withKey("coreInstanceCount").withStringValue("1"),
                        new Field().withKey("coreInstanceType").withStringValue("m3.xlarge"),
                        new Field().withKey("masterInstanceType").withStringValue("m3.xlarge"),
                        new Field().withKey("keyPair").withStringValue(emrClusterKeyPairName), //errors were observed on startup of EC2 instances if no key pair was specified
                        new Field().withKey("region").withStringValue(region),
                        new Field().withKey("subnetId").withStringValue(emrClusterSubnetId), //without specifying subnet, it launches the cluster in the default VPC if it exists, and fails otherwise
                        new Field().withKey("terminateAfter").withStringValue("3 hours"),
                        new Field().withKey("type").withStringValue("EmrCluster")
                );


        final PipelineObject snsAlarmForBackupPipelineObject;

        final List<Field> fieldsForTableBackupActivity = new ArrayList<>();
        fieldsForTableBackupActivity.add(new Field().withKey("output").withRefValue("S3BackupLocation"));
        fieldsForTableBackupActivity.add(new Field().withKey("input").withRefValue("DDBSourceTable"));
        fieldsForTableBackupActivity.add(new Field().withKey("maximumRetries").withStringValue("2"));
        fieldsForTableBackupActivity.add(new Field().withKey("step").withStringValue("s3://dynamodb-emr-#{myDDBRegion}/emr-ddb-storage-handler/2.1.0/emr-ddb-2.1.0.jar,org.apache.hadoop.dynamodb.tools.DynamoDbExport,#{output.directoryPath},#{input.tableName},#{input.readThroughputPercent}"));
        fieldsForTableBackupActivity.add(new Field().withKey("runsOn").withRefValue("EmrClusterForBackup"));
        fieldsForTableBackupActivity.add(new Field().withKey("type").withStringValue("EmrActivity"));
        fieldsForTableBackupActivity.add(new Field().withKey("resizeClusterBeforeRunning").withStringValue("true"));

        if (snsOnFail)
        {
            final String failureMessage = "DynamoDB data backup to S3 FAILED for table " + tableName;
            snsAlarmForBackupPipelineObject = new PipelineObject()
                    .withId("SnsAlarmForBackup").withName("SnsAlarmForBackup")
                    .withFields(
                            new Field().withKey("type").withStringValue("SnsAlarm"),
                            new Field().withKey("topicArn").withStringValue(snsTopicArn),
                            new Field().withKey("subject").withStringValue(failureSubject),
                            new Field().withKey("message").withStringValue(failureMessage)
                    );
            fieldsForTableBackupActivity.add(new Field().withKey("onFail").withRefValue("SnsAlarmForBackup"));
        }
        else
        {
            snsAlarmForBackupPipelineObject = null;
        }


        final PipelineObject tableBackupActivityPipelineObject = new PipelineObject()
                .withId("TableBackupActivity").withName("TableBackupActivity")
                .withFields(fieldsForTableBackupActivity);

        //note - if myOutputS3Loc already exists, backup will fail
        final PipelineObject s3BackupLocationPipelineObject = new PipelineObject()
                .withId("S3BackupLocation").withName("S3BackupLocation")
                .withFields(new Field().withKey("directoryPath").withStringValue("#{myOutputS3Loc}/#{format(@scheduledStartTime, 'YYYY-MM-dd-HH-mm-ss')}"),
                        new Field().withKey("type").withStringValue("S3DataNode")
                );

        final PipelineObject defaultSchedulePipelineObject = new PipelineObject()
                .withId("DefaultSchedule").withName("Every 1 day")
                .withFields(
                        new Field().withKey("period").withStringValue("1 days"),
                        new Field().withKey("type").withStringValue("Schedule"),
                        new Field().withKey("endDateTime").withStringValue(endTimeUtcIso8601),
                        new Field().withKey("startAt").withStringValue("FIRST_ACTIVATION_DATE_TIME")
                );

        final PipelineObject defaultPipelineObject = new PipelineObject()
                .withId("Default").withName("Default")
                .withFields(
                        new Field().withKey("failureAndRerunMode").withStringValue("CASCADE"),
                        new Field().withKey("schedule").withRefValue("DefaultSchedule"),
                        new Field().withKey("resourceRole").withStringValue("DataPipelineDefaultResourceRole"),
                        new Field().withKey("role").withStringValue("DataPipelineDefaultRole"),
                        new Field().withKey("pipelineLogUri").withStringValue(backupS3FolderUrl + "/data-pipeline-log"),
                        new Field().withKey("scheduleType").withStringValue("cron")
                );

        parameterObjectsToPopulate.addAll(Arrays.asList(paramMyDDBTableName, paramMyOutputS3Loc, paramMyDDBReadThroughputRatio, paramMyDDBRegion));
        parameterValuesToPopulate.addAll(Arrays.asList(valueMyDDBTableName, valueMyOutputS3Loc, valueMyDDBReadThroughputRatio, valueMyDDBRegion));
        pipelineObjectsToPopulate.addAll(Arrays.asList(
                ddbSourceTablePipelineObject, emrClusterForBackupPipelineObject, tableBackupActivityPipelineObject,
                s3BackupLocationPipelineObject, defaultSchedulePipelineObject, defaultPipelineObject));
        if (snsOnFail)
        {
            pipelineObjectsToPopulate.add(snsAlarmForBackupPipelineObject);
        }
    }


}
