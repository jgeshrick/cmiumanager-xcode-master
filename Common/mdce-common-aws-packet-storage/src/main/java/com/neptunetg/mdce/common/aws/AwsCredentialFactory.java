/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;

/**
 * To generate global credential for all Amazon web services.
 */
public class AwsCredentialFactory
{
    final AWSCredentialsProvider credentialsProvider;

    public AwsCredentialFactory(boolean useLocalDynamoAndDummyCredentials, String localCredentialsClasspathLocation)
    {
        if (useLocalDynamoAndDummyCredentials)
        {
            this.credentialsProvider = new ClasspathPropertiesFileCredentialsProvider(localCredentialsClasspathLocation);
        }
        else
        {
            //credentials will be retrieved from IAM role - see http://docs.aws.amazon.com/AWSSdkDocsJava/latest/DeveloperGuide/java-dg-roles.html
            //use EC2 IAM credential
            this.credentialsProvider = new InstanceProfileCredentialsProvider();
        }
    }

    public AWSCredentialsProvider getCredentialProvider()
    {
        return this.credentialsProvider;
    }

}
