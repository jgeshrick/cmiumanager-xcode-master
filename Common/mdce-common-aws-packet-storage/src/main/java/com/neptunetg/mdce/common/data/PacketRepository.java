/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data;

import com.amazonaws.services.dynamodbv2.document.Table;
import com.neptunetg.mdce.common.data.model.GatewayPacketReceivedDynamoItem;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import com.neptunetg.mdce.common.data.model.MiuPacketSentDynamoItem;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

/**
 * Interface for interacting with a packet repository
 */
public interface PacketRepository
{


    /**
     * Insert a new data entry row into table: miuPacketReceivedTableName, given a CmiuPacketHeader information.
     * @param miuPacket miuPacket data model
     */
    void insertMiuPacketReceived(MiuPacketReceivedDynamoItem miuPacket) throws MdceDataException, InterruptedException;

    /**
     * Insert multiple rows into table: miuPacketReceivedTableName, given a CmiuPacketHeader information.
     * @param miuPackets multiple miuPacket data model - they will be inserted in 25 item batches
     */
    void insertMiuPacketsReceived(Iterable<MiuPacketReceivedDynamoItem> miuPackets) throws MdceDataException, InterruptedException;

    void insertMiuPacketSent(MiuPacketSentDynamoItem miuPacket) throws InterruptedException;

    /**
     * Insert a new data entry row into table: gatewayPacketReceivedTableName, given a GatewayPacketReceivedDynamoItem information.
     * @param gatewayPacket gatewayPacketReceived data model
     */
    void insertR900GatewayPacketReceived(GatewayPacketReceivedDynamoItem gatewayPacket) throws InterruptedException;

    /**
     * Get the table list for an MIU packet query over a date range
     * @param minInsertTimestamp Minimum insertion
     * @param maxInsertTimestamp Maximum insertion
     * @return List of tables
     */
    List<Table> getRelevantTablesForMiuPacketQuery(Instant minInsertTimestamp, Instant maxInsertTimestamp);

    /**
     * Get the table list for a Gateway packet query over a date range
     * @param minInsertTimestamp Minimum insertion
     * @param maxInsertTimestamp Maximum insertion
     * @return List of tables
     */
    List<Table> getRelevantTablesForGatewayPacketQuery(Instant minInsertTimestamp, Instant maxInsertTimestamp);

    /**
     * Return all packets received from this MIU id and inserted in the date range
     * @param miuId MIU id matching of the packet to return
     * @param minInsertTimestamp the minimum insert date.
     * @param maxInsertTimestamp the maximum insert date.
     * @param tablesToQuery List of tables to query
     * @return list of miu packet
     */
    Stream<MiuPacketReceivedDynamoItem> streamMiuPacketsInsertedInDateRange(int miuId,
                                                                            Instant minInsertTimestamp, Instant maxInsertTimestamp,
                                                                            Collection<Table> tablesToQuery);

    /**
     * Return all packets received from this MIU id and inserted in the date range
     * @param miuId MIU id matching of the packet to return
     * @param minInsertTimestamp the minimum insert date.
     * @param maxInsertTimestamp the maximum insert date.
     * @param tablesToQuery List of tables to query
     * @return stream iterator of packets
     */
    Iterable<MiuPacketReceivedDynamoItem> iterateMiuPacketsInsertedInDateRange(int miuId,
                                                                            Instant minInsertTimestamp, Instant maxInsertTimestamp,
                                                                            Collection<Table> tablesToQuery);

    /**
     * Return all packets received from gateway and inserted in the date range
     * @param gatewayId The gateway ID
     * @param minInsertTimestamp the minimum insert date.
     * @param maxInsertTimestamp the maximum insert date.
     * @param tablesToQuery List of tables to query
     * @return stream of packets
     */
    Stream<GatewayPacketReceivedDynamoItem> streamGatewayPacketsInsertedInDateRange(String gatewayId,
                                                                                    Instant minInsertTimestamp, Instant maxInsertTimestamp,
                                                                                    Collection<Table> tablesToQuery);

    /**
     * Return all packets received from gateway and inserted in the date range
     * @param gatewayId The gateway ID
     * @param minInsertTimestamp the minimum insert date.
     * @param maxInsertTimestamp the maximum insert date.
     * @param tablesToQuery List of tables to query
     * @return stream iterator of packets
     */
    Iterable<GatewayPacketReceivedDynamoItem> iterateGatewayPacketsInsertedInDateRange(String gatewayId,
                                                                                    Instant minInsertTimestamp, Instant maxInsertTimestamp,
                                                                                       Collection<Table> tablesToQuery);


}
