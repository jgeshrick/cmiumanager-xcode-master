/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data;

import com.amazonaws.services.dynamodbv2.document.Table;

import java.time.YearMonth;
import java.util.List;

/**
 * Interface for maintaining packet repository
 */
public interface DynamoPacketRepositoryMaintenanceService
{

    /**
     * This method should be prodded by the PacketRepoManagementService every hour.
     * It creates tomorrow's tables a few hours before they will be used, and sets yesterday's tables
     * to cold a few hours after they've stopped being used.
     */
    void performHourlyMaintenance() throws InterruptedException, Exception;


    /**
     * Ensure that tables exist
     *
     * @param d Date for which tables should be ensured to exist
     */
    boolean ensureHotTables(YearMonth d) throws InterruptedException;

    /**
     * Get a list of all tables associated to the account and prefixed with the environment variable
     *
     * @return table list
     */
    List<Table> listTables();

    /**
     * Wipe the database containing table for the supplied date and recreate the tables.
     * This is for test purpose.
     * @deprecated To be removed - test only
     */
    @Deprecated
    void wipe();

}
