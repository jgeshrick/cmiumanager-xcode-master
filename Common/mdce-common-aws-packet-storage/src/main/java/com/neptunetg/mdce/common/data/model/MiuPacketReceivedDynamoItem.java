/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.S3Link;

import java.nio.ByteBuffer;

/**
    POJO for DynamoDb table MIU_PACKETS_RECEIVED.  Mapped by DynamoDBMapper to Dynamo DB item.
 */
@DynamoDBTable(tableName = "env-miu-packets-received-yyyymmdd")  //default name
public class MiuPacketReceivedDynamoItem implements DynamoItem, Comparable<MiuPacketReceivedDynamoItem>
{
    @DynamoDBHashKey(attributeName="miuId")
    private int miuId;

    @DynamoDBRangeKey(attributeName="insertDate")
    private long insertDate;    //unix time in millisecond

    private String packetType;

    private long dateBuilt;
    private long dateReceived;
    private String sourceDeviceType;
    private String targetDeviceType;
    private ByteBuffer packetData;
    private S3Link packetDataExternal;

    public int getMiuId()
    {
        return miuId;
    }

    public void setMiuId(int miuId)
    {
        this.miuId = miuId;
    }

    @Override
    public long getInsertDate()
    {
        return insertDate;
    }

    @Override
    public void setInsertDate(long insertDate)
    {
        this.insertDate = insertDate;
    }

    public String getPacketType()
    {
        return packetType;
    }

    public void setPacketType(String packetType)
    {
        this.packetType = packetType;
    }

    public long getDateBuilt()
    {
        return dateBuilt;
    }

    public void setDateBuilt(long dateBuilt)
    {
        this.dateBuilt = dateBuilt;
    }

    public long getDateReceived()
    {
        return dateReceived;
    }

    public void setDateReceived(long dateReceived)
    {
        this.dateReceived = dateReceived;
    }

    public String getSourceDeviceType()
    {
        return sourceDeviceType;
    }

    public void setSourceDeviceType(String sourceDeviceType)
    {
        this.sourceDeviceType = sourceDeviceType;
    }

    public String getTargetDeviceType()
    {
        return targetDeviceType;
    }

    public void setTargetDeviceType(String targetDeviceType)
    {
        this.targetDeviceType = targetDeviceType;
    }

    public ByteBuffer getPacketData()
    {
        return packetData;
    }

    public void setPacketData(ByteBuffer packetData)
    {
        this.packetData = packetData;
    }

    public void setPacketData(byte[] packetData)
    {
        this.packetData = ByteBuffer.wrap(packetData);
    }

    public S3Link getPacketDataExternal()
    {
        return packetDataExternal;
    }

    public void setPacketDataExternal(S3Link packetDataExternal)
    {
        this.packetDataExternal = packetDataExternal;
    }

    /**
     * Sort by MIU ID, then insert date, then data
     * @param other Other MiuPacketReceivedDynamoItem
     * @return -1 if MIU ID less, or insert date less, or a data byte is less, or the data length is less
     */
    @Override
    public int compareTo(MiuPacketReceivedDynamoItem other)
    {
        if (other == null)
        {
            return -1;
        }

        if (this.miuId < other.miuId)
        {
            return -1;
        }
        else if (this.miuId > other.miuId)
        {
            return 1;
        }
        else if (this.insertDate < other.insertDate)
        {
            return -1;
        }
        else if (this.insertDate > other.insertDate)
        {
            return 1;
        }
        else
        {
            int cmp = packetType.compareTo(other.packetType);
            if (cmp != 0)
            {
                return cmp;
            }
            for (int i = 0; i < packetData.capacity() && i < other.packetData.capacity(); i++)
            {
                cmp = ((this.packetData.get(i) & 0xff) - (other.packetData.get(i) & 0xff));
                if (cmp < 0)
                {
                    return -1;
                }
                else if (cmp > 0)
                {
                    return 1;
                }
                i++;
            }
            if (packetData.capacity() < other.packetData.capacity())
            {
                return -1;
            }
            else if (packetData.capacity() > other.packetData.capacity())
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

    }
}
