/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.S3Link;

import java.nio.ByteBuffer;

/**
 POJO for DynamoDb table GATEWAY_PACKETS_SENT.  Mapped by DynamoDBMapper to Dynamo DB item.
 */
@DynamoDBTable(tableName = "env-gateway-packets-sent-yyyymmdd")  //default name
public class GatewayPacketSentDynamoItem implements DynamoItem
{
    @DynamoDBHashKey(attributeName="collectorId")
    private String collectorId;

    @DynamoDBRangeKey(attributeName="insertDate")
    private long insertDate;

    private long dateSent;

    private String packetType;

    private ByteBuffer packetData;
    private S3Link packetDataExternal;

    private String sourceDeviceType;
    private String targetDeviceType;

    public String getCollectorId()
    {
        return collectorId;
    }

    public void setCollectorId(String collectorId)
    {
        this.collectorId = collectorId;
    }

    @Override
    public long getInsertDate()
    {
        return insertDate;
    }

    @Override
    public void setInsertDate(long insertDate)
    {
        this.insertDate = insertDate;
    }

    public long getDateSent()
    {
        return dateSent;
    }

    public void setDateSent(long dateSent)
    {
        this.dateSent = dateSent;
    }

    public String getPacketType()
    {
        return packetType;
    }

    public void setPacketType(String packetType)
    {
        this.packetType = packetType;
    }

    public ByteBuffer getPacketData()
    {
        return packetData;
    }

    public void setPacketData(ByteBuffer packetData)
    {
        this.packetData = packetData;
    }

    @Override
    public S3Link getPacketDataExternal()
    {
        return packetDataExternal;
    }


    @Override
    public void setPacketDataExternal(S3Link packetDataExternal)
    {
        this.packetDataExternal = packetDataExternal;
    }

    @Override
    public void setPacketData(byte[] packetData)
    {
        this.packetData = ByteBuffer.wrap(packetData);
    }

    public String getSourceDeviceType()
    {
        return sourceDeviceType;
    }

    public void setSourceDeviceType(String sourceDeviceType)
    {
        this.sourceDeviceType = sourceDeviceType;
    }

    public String getTargetDeviceType()
    {
        return targetDeviceType;
    }

    public void setTargetDeviceType(String targetDeviceType)
    {
        this.targetDeviceType = targetDeviceType;
    }
}
