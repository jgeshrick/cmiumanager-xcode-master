/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.aws;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A very basic Amazon S3 interface to get/store packetData byte arrays to S3.
 * Packets are stored to packetBucketName, under the subfolder: date/tablename/filename.
 */
@Repository
public class S3Manager
{
    private static final String FOLDER_SEPARATOR = "/";
    private static final Logger log = LoggerFactory.getLogger(S3Manager.class);

    private final AmazonS3Client s3Client;

    ///For storing data packets from CMIU that could not fit into DynamoDb item size limit
    private final String packetBucket;

    //For storing archived dynamoDb tables
    private String tableBucket;

    @Autowired
    public S3Manager(AmazonS3Client s3Client,
                     @Value("#{envProperties['aws.s3.bucket.packet']}") String packetBucketName,
                     @Value("#{envProperties['aws.s3.bucket.table']}") String tableBucketName)
    {
        this.s3Client = s3Client;

        this.packetBucket = packetBucketName;
        this.tableBucket = tableBucketName;
    }

    /**
     * Get a byte array representing the packet data from the packet bucket.
     * @param key key identifying the full file path to the s3 object
     * @return retrieved s3 object
     * @throws IOException if something goes wrong
     */
    public byte[] getPacket(String key) throws IOException
    {
        S3Object object = this.s3Client.getObject(this.packetBucket, key);
        return IOUtils.toByteArray(object.getObjectContent());
    }

    /**
     * Upload byte array of data to S3.
     * @param filePath file path to the s3 object
     * @param cmiuId cmiuId of the data, this is used to form part of the file name
     * @param unixTimeSeconds unix time in seconds, this is used to form the second part of the file name
     * @param packetData the data to be stored in S3
     * @return the s3 key of the object.
     */
    public String putPacket(String filePath, long cmiuId, long unixTimeSeconds, final byte[] packetData)
    {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(packetData.length);
        metadata.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION);

        final String objectKey = filePath + "_" + unixTimeSeconds;

        Map<String, String> userMetadata=new HashMap<String, String>()
        {
            {
                put("key", objectKey);
                put("insertDate", String.format("%d", unixTimeSeconds));
                put("cmiuId", String.format("%06d", cmiuId));
                put("createBy", "mdce-integration");    //TODO change creator
            }
        };

        metadata.setUserMetadata(userMetadata);

        PutObjectResult result = this.s3Client.putObject(new PutObjectRequest(
                this.packetBucket,
                objectKey,
                new ByteArrayInputStream(packetData),
                metadata));

        return objectKey;
    }

    /**
     * Delete an S3 item
     * @param key Key to item
     */
    public void deletePacket(String key)
    {
        this.s3Client.deleteObject(this.packetBucket, key);
    }

    /**
     * Verify whether both packetBucket and tableBucket has been created in the S3 account.
     * @return true only if both buckets are present.
     */
    public boolean areBucketsPresent()
    {
        return this.s3Client.doesBucketExist(this.packetBucket) &&
                this.s3Client.doesBucketExist(this.tableBucket);
    }

    /**
     * Check if the data object is present by querying the object meta data.
     * @param key key identifying the full file path to the s3 object
     * @return true if the object is present in S3.
     */
    public boolean isPacketPresent(String key)
    {
        try
        {
            ObjectMetadata objectMetaData = this.s3Client.getObjectMetadata(this.packetBucket, key);
            return objectMetaData != null;
        }
        catch (AmazonS3Exception s3e)
        {
            return false;
        }
    }

    public List<String> getBucketList()
    {
        return this.s3Client.listBuckets().stream().map(Bucket::getName).collect(Collectors.toList());
    }

    /**
     * Get a list of s3 object in the folder.
     * @param bucketName
     * @param folder
     * @return
     */
    public List<S3ObjectSummary> getItemsInFolder(String bucketName, String folder)
    {
        ObjectListing listing = this.s3Client.listObjects( bucketName, folder );
        List<S3ObjectSummary> summaries = listing.getObjectSummaries();

        while (listing.isTruncated())
        {
            listing = this.s3Client.listNextBatchOfObjects (listing);
            summaries.addAll (listing.getObjectSummaries());
        }

        return summaries;
    }

    /**
     * Get a file identified by the s3 key and return as an input stream.
     * @param bucketName s3 bucketname
     * @param key s3 key to the file
     * @return inputstream if file is found, null if not found.]
     */
    public InputStream getObjectAsStream(String bucketName, String key)
    {
        try
        {
            S3Object object = this.s3Client.getObject(bucketName, key);
            return object.getObjectContent();
        }
        catch(AmazonS3Exception ex)
        {
            log.error("Error retriving S3 object: {}.", key, ex);
            return null;
        }
    }
    /**
     * Check whether the account has the bucket
     * @param bucketName the name of the bucket to verify
     * @return true if the bucket exist
     */
    public boolean hasBucket(String bucketName)
    {
        return this.getBucketList().stream().filter(e -> e.equals(bucketName)).findFirst().isPresent();
    }




}
