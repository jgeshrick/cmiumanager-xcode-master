/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.data;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.neptunetg.mdce.common.config.DynamoDynamicConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Reads configuration settings from mdce-env.properties and provides them
 * (and other hard-coded config) to the DynamoPacketRepository
 */
@Service
public class DynamoPacketRepositoryConfig
{
    private static final Logger log = LoggerFactory.getLogger(DynamoPacketRepositoryConfig.class);

    private static final Pattern ENDPOINT_TO_REGION_REGEX = Pattern.compile("https://dynamodb\\.([a-zA-Z0-9\\-]+)\\.amazonaws\\.com/?");

    private static final String MIU_PACKETS_RECEIVED_TABLE_NAME_MIDDLE = "miu-packets-received";
    private static final String MIU_PACKETS_SENT_TABLE_NAME_MIDDLE = "miu-packets-sent";
    private static final String GATEWAY_PACKETS_RECEIVED_TABLE_NAME_MIDDLE = "gateway-packets-received";
    //for future expansion: private static final String GATEWAY_PACKETS_SENT_TABLE_NAME_MIDDLE = "gateway-packets-sent";

    private static final String S3_PROTOCOL = "s3://";
    private static final String S3_OLD_DATA_BUCKET_NAME_END = "-old-data";
    private static final String S3_LARGE_PACKETS_BUCKET_NAME_END = "-large-packets";
    private static final String S3_BUCKET_PREFIX = "neptune-mdce-";

    private static final String S3_FOLDER_SEPARATOR = "/";

    private static final ZoneId TIME_ZONE_FOR_TABLE_ROTATION = ZoneId.of("America/Chicago"); //we tick over our DB at midnight CT

    private static final String PROPERTY_ENV_NAME = "env.name";
    private static final String PROPERTY_ENDPOINT_NAME = "aws.dynamo.endpoint";
    private static final String PROPERTY_MIU_PACKETS_RECEIVED_CONFIG = "aws.dynamo.iops.miu.received";
    private static final String PROPERTY_MIU_PACKETS_SENT_CONFIG = "aws.dynamo.iops.miu.sent";
    private static final String PROPERTY_GATEWAY_PACKETS_RECEIVED_CONFIG = "aws.dynamo.iops.gw.received";

    private static final String PROPERTY_BACKUP_ENABLED = "aws.dynamo.backup.enabled";
    private static final String PROPERTY_BACKUP_SNS_ARN = "aws.dynamo.backup.sns.arn";
    private static final String PROPERTY_BACKUP_EMR_SUBNET_ID = "aws.dynamo.backup.emr.subnet.id";
    private static final String PROPERTY_BACKUP_KEY_PAIR_NAME = "aws.dynamo.backup.emr.keypair";

    /**
     * Ensure the Java standard forkJoin pool has this many threads at least.  Normally it is sized as (#cores - 1)
     * which is good for CPU-bound tasks, but Dyanmo queries are IO bound.
     */
    private static final int DYNAMO_QUERY_MIN_THREAD_POOL_SIZE = 30;

    private static final String JAVA_DEFAULT_FORK_JOIN_THREAD_POOL_SIZE_SYSTEM_PROPERTY_NAME = "java.util.concurrent.ForkJoinPool.common.parallelism";
    private static final DateTimeFormatter YEAR_MONTH_FORMATTER = DynamoPacketTableDefinition.TABLE_NAME_DATE_FORMATTER;

    private static final Duration TIME_BEFORE_END_OF_MONTH_TO_CREATE_NEXT_MONTHS_TABLES = Duration.of(150L, ChronoUnit.MINUTES); //create next month's tables after 9:30pm on the last day of the month
    private static final Duration TIME_AFTER_START_OF_MONTH_TO_MAKE_LAST_MONTHS_TABLES_COLD = Duration.of(1L, ChronoUnit.DAYS); //Make last month's tables cold after 1 day
    private static final String BACKUP_PIPELINE_SUFFIX = "-backup-pipeline";

    private final String envName;
    private final Region region;

    private final DynamoPacketTableDefinition miuPacketsReceivedTableDefinition;
    private final DynamoPacketTableDefinition miuPacketsSentTableDefinition;
    private final DynamoPacketTableDefinition gatewayPacketsReceivedTableDefinition;

    private final DynamoDynamicConfigService dynamicConfig;

    private final boolean backupEnabled;
    private final String backupSnsArn;
    private final String backupEmrSubnetId;
    private final String backupEmrKeyPairName;


    @Autowired
    public DynamoPacketRepositoryConfig(@Qualifier("envProperties") Properties envProperties,
                                        DynamoDynamicConfigService dynamicConfig)
    {
        this.dynamicConfig = dynamicConfig;

        this.envName = envProperties.getProperty(PROPERTY_ENV_NAME);

        if (!StringUtils.hasText(envName))
        {
            throw new IllegalArgumentException("Environment name cannot be empty!");
        }

        if ("true".equalsIgnoreCase(envProperties.getProperty(PROPERTY_BACKUP_ENABLED)))
        {
            this.backupEnabled = true;
            this.backupSnsArn = envProperties.getProperty(PROPERTY_BACKUP_SNS_ARN);
            this.backupEmrSubnetId = envProperties.getProperty(PROPERTY_BACKUP_EMR_SUBNET_ID);
            this.backupEmrKeyPairName = envProperties.getProperty(PROPERTY_BACKUP_KEY_PAIR_NAME);

            if (this.backupSnsArn == null || this.backupEmrSubnetId == null || this.backupEmrKeyPairName == null)
            {
                throw new Error("DynamoDB table backup is enabled but not fully configured.  " +
                        "Check the aws.dynamo.backup.* properties in mdce-env.properties");
            }

            log.info("Table backups are enabled - errors to SNS @ " + this.backupSnsArn);
        }
        else
        {
            this.backupEnabled = false;
            this.backupSnsArn = null;
            this.backupEmrSubnetId = null;
            this.backupEmrKeyPairName = null;

            log.info("Table backups are disabled");
        }

        final String endpoint = envProperties.getProperty(PROPERTY_ENDPOINT_NAME);
        this.region = getRegionFromEndpoint(endpoint);

        ensureForkJoinThreadPoolSize();

        this.miuPacketsReceivedTableDefinition = initTable(envName,
                MIU_PACKETS_RECEIVED_TABLE_NAME_MIDDLE, envProperties, PROPERTY_MIU_PACKETS_RECEIVED_CONFIG);
        this.miuPacketsSentTableDefinition = initTable(envName,
                MIU_PACKETS_SENT_TABLE_NAME_MIDDLE, envProperties, PROPERTY_MIU_PACKETS_SENT_CONFIG);
        this.gatewayPacketsReceivedTableDefinition = initTable(envName,
                GATEWAY_PACKETS_RECEIVED_TABLE_NAME_MIDDLE, envProperties, PROPERTY_GATEWAY_PACKETS_RECEIVED_CONFIG);

    }

    private static DynamoPacketTableDefinition initTable(String envName, String tableNameMiddle, Properties envProperties, String propertyNameBase)
    {
        final int hotWriteIops = Integer.parseInt(envProperties.getProperty(propertyNameBase + ".hot.write"));
        final int hotReadIops = Integer.parseInt(envProperties.getProperty(propertyNameBase + ".hot.read"));
        final int coldWriteIops = Integer.parseInt(envProperties.getProperty(propertyNameBase + ".cold.write"));
        final int coldReadIops = Integer.parseInt(envProperties.getProperty(propertyNameBase + ".cold.read"));

        return new DynamoPacketTableDefinition(envName, tableNameMiddle, hotWriteIops, hotReadIops, coldWriteIops, coldReadIops);
    }

    private static void ensureForkJoinThreadPoolSize()
    {
        final String currentCommonThreadPoolSizeValue = System.getProperty(JAVA_DEFAULT_FORK_JOIN_THREAD_POOL_SIZE_SYSTEM_PROPERTY_NAME);
        int currentCommonThreadPoolSize = 0;

        if (currentCommonThreadPoolSizeValue != null)
        {
            try
            {
                currentCommonThreadPoolSize = Integer.parseInt(currentCommonThreadPoolSizeValue);
            } catch (Exception e)
            {
                log.warn("Unexpected error parsing Java system property " + JAVA_DEFAULT_FORK_JOIN_THREAD_POOL_SIZE_SYSTEM_PROPERTY_NAME + ".", e);
            }
        }

        if (currentCommonThreadPoolSize < DYNAMO_QUERY_MIN_THREAD_POOL_SIZE)
        {
            log.info("Upping " + JAVA_DEFAULT_FORK_JOIN_THREAD_POOL_SIZE_SYSTEM_PROPERTY_NAME + " from " + currentCommonThreadPoolSize + " to " + DYNAMO_QUERY_MIN_THREAD_POOL_SIZE);
            System.setProperty(JAVA_DEFAULT_FORK_JOIN_THREAD_POOL_SIZE_SYSTEM_PROPERTY_NAME, Integer.toString(DYNAMO_QUERY_MIN_THREAD_POOL_SIZE));
        }
    }


    public DynamoPacketTableDefinition getTableDefinition(Table t)
    {
        final String tableName = t.getTableName();

        if (tableName.indexOf(MIU_PACKETS_RECEIVED_TABLE_NAME_MIDDLE) > 0)
        {
            return this.miuPacketsReceivedTableDefinition;
        }
        else if (tableName.indexOf(MIU_PACKETS_SENT_TABLE_NAME_MIDDLE) > 0)
        {
            return this.miuPacketsSentTableDefinition;
        }
        else if (tableName.indexOf(GATEWAY_PACKETS_RECEIVED_TABLE_NAME_MIDDLE) > 0)
        {
            return this.gatewayPacketsReceivedTableDefinition;
        }
        else
        {
            return null;
        }

    }

    public DynamoPacketTableDefinition getMiuPacketsReceivedTableDefinition()
    {
        return miuPacketsReceivedTableDefinition;
    }

    public DynamoPacketTableDefinition getMiuPacketsSentTableDefinition()
    {
        return miuPacketsSentTableDefinition;
    }

    public DynamoPacketTableDefinition getGatewayPacketsReceivedTableDefinition()
    {
        return gatewayPacketsReceivedTableDefinition;
    }

    public String getLargePacketsS3BucketName()
    {
        return S3_BUCKET_PREFIX + this.envName + S3_LARGE_PACKETS_BUCKET_NAME_END;
    }

    public String getBackupS3Bucket()
    {
        return S3_BUCKET_PREFIX + this.envName + S3_OLD_DATA_BUCKET_NAME_END;
    }

    /**
     *
     * @param tableName test-2015-11
     * @param date 2015-11
     * @return 2015-11/dynamodb/test-2015-11
     */
    public String getBackupS3FolderPath(String tableName, YearMonth date)
    {
        return YEAR_MONTH_FORMATTER.format(date) + "/dynamodb/" + tableName;
    }

    public String getBackupS3FolderUrl(String tableName, YearMonth date)
    {
        return S3_PROTOCOL + getBackupS3Bucket() + "/" + getBackupS3FolderPath(tableName, date);
    }

    public String generateS3PacketDataKey(YearMonth folderDate, String id, String tableName, long timestampMillis)
    {
        return folderDate.format(YEAR_MONTH_FORMATTER) + S3_FOLDER_SEPARATOR +
                tableName + S3_FOLDER_SEPARATOR + id + "_" + timestampMillis + ".packetdata";
    }

    /**
     * Gets the table date for a point in time
     * @param point point in time.  If null, throw IllegalArgumentException
     * @return Table date for that point in time
     */
    public YearMonth getTableDate(Instant point)
    {
        if (point == null)
        {
            throw new IllegalArgumentException("Date for Dynamo table not specified!");
        }
        return YearMonth.from(point.atZone(TIME_ZONE_FOR_TABLE_ROTATION));
    }

    public ZonedDateTime getArchiveStartTimeForTableDate(YearMonth tableDate)
    {
        return tableDate.atDay(1).atTime(12, 00).atZone(TIME_ZONE_FOR_TABLE_ROTATION);
    }


    public ZonedDateTime getArchiveEndTimeForTableDate(YearMonth tableDate)
    {
        return tableDate.atDay(1).atTime(13, 00).atZone(TIME_ZONE_FOR_TABLE_ROTATION).plus(1L, ChronoUnit.MONTHS).plus(2L, ChronoUnit.DAYS);
    }


    /**
     * Are we close enough to midnight to create next month's tables?  This means after 9:30pm on the last day of the month.
     * @param time Current time
     * @return true if close to end of month
     */
    public boolean isItTimeToCreateTablesForNextMonth(Instant time)
    {
        final LocalDateTime now = LocalDateTime.ofInstant(time, TIME_ZONE_FOR_TABLE_ROTATION);
        final LocalDateTime startOfNextMonth = now
                .plus(1L, ChronoUnit.MONTHS)
                .withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
        final Duration timeTillNextMonth = Duration.between(now, startOfNextMonth);

        return (timeTillNextMonth.compareTo(TIME_BEFORE_END_OF_MONTH_TO_CREATE_NEXT_MONTHS_TABLES) <= 0);
    }

    /**
     * If after the 1st of the month, this date is last month, otherwise it is the month
     * before that
     * @param time Current time
     * @return last month or the one before
     */
    public YearMonth getMaxColdTableDate(Instant time)
    {
        final LocalDateTime now = LocalDateTime.ofInstant(time, TIME_ZONE_FOR_TABLE_ROTATION);
        final LocalDateTime startOfThisMonth = now
                .withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
        final Duration timeSinceStartOfMonthMonth = Duration.between(startOfThisMonth, now);

        if (timeSinceStartOfMonthMonth.compareTo(TIME_AFTER_START_OF_MONTH_TO_MAKE_LAST_MONTHS_TABLES_COLD) >= 0)
        {
            // we are far enough into the current month that last month's tables can be made cold
            return YearMonth.from(now).minus(1L, ChronoUnit.MONTHS);
        }
        else
        {
            return YearMonth.from(now).minus(2L, ChronoUnit.MONTHS);
        }
    }

    public boolean isDynamicConfigAvailable()
    {
        return this.dynamicConfig.isDynamicConfigAvailable();
    }


    /**
     * Get the newest table date for which tables can be deleted.  Check isDynamicConfigAvailable() before calling
     * @param time Current time
     * @return last month or the one before
     */
    public YearMonth getMaxDeleteTableDate(Instant time)
    {
        if (dynamicConfig.isDynamicConfigAvailable())
        {
            final LocalDateTime now = LocalDateTime.ofInstant(time, TIME_ZONE_FOR_TABLE_ROTATION);

            long ageOfTablesToDeleteMonths = dynamicConfig.getOldTableDeletionMinAgeMonths();
            if (ageOfTablesToDeleteMonths < 3L)
            {
                ageOfTablesToDeleteMonths = 3L; //safety backstop to avoid deleting tables that are too new!
            }
            final LocalDateTime threeMonthsAgo = now.minusMonths(ageOfTablesToDeleteMonths);

            final YearMonth minNonDeleteTableDate = YearMonth.of(threeMonthsAgo.getYear(), threeMonthsAgo.getMonth());

            final YearMonth maxDeleteTableDate = minNonDeleteTableDate.minus(1L, ChronoUnit.MONTHS);

            return maxDeleteTableDate;
        }
        else
        {
            throw new UnsupportedOperationException("Dynamic configuration not available!");
        }
    }

    /**
     * Checks whether this is a packet table for this environment
     * @param table Table definition
     * @return True if it is a packet table
     */
    public boolean isPacketTableForMyEnvironment(Table table)
    {
        return this.miuPacketsReceivedTableDefinition.matchesTable(table)
                || this.miuPacketsSentTableDefinition.matchesTable(table)
                || this.gatewayPacketsReceivedTableDefinition.matchesTable(table);
    }


    /**
     * Checks whether this is a packet table for this environment
     * @param tableName Table name
     * @return True if it is a packet table
     */
    public boolean isPacketTableForMyEnvironment(String tableName)
    {
        return this.miuPacketsReceivedTableDefinition.matchesTableName(tableName)
                || this.miuPacketsSentTableDefinition.matchesTableName(tableName)
                || this.gatewayPacketsReceivedTableDefinition.matchesTableName(tableName);
    }
    /**
     * Get the backup pipeline name for the table
     * @param tableName Name of table
     * @return {tableName}-backup-pipeline
     */
    public String getBackupPipelineNameForTable(String tableName)
    {
        return tableName + BACKUP_PIPELINE_SUFFIX;
    }

    /**
     * Checks whether this is a backup pipeline for this environment
     * @param pipelineName Name of pipeline
     * @return True if name is {tableName}-backup-pipeline
     */
    public boolean isBackupPipelineForMyEnvironment(String pipelineName)
    {
        if (!pipelineName.endsWith(BACKUP_PIPELINE_SUFFIX))
        {
            return false;
        }
        final String tableName = pipelineName.substring(0, pipelineName.length() - BACKUP_PIPELINE_SUFFIX.length());
        return isPacketTableForMyEnvironment(tableName);
    }

    public String getEnvName()
    {
        return envName;
    }

    private static Region getRegionFromEndpoint(String endpoint)
    {
        final Matcher matcher = ENDPOINT_TO_REGION_REGEX.matcher(endpoint);
        if (matcher.matches())
        {
            final String regionName = matcher.group(1);
            log.info("Extracted AWS region name " + regionName + " from DynamoDB endpoint " + endpoint);
            return Region.getRegion(Regions.fromName(matcher.group(1)));
        }
        else
        {
            return Region.getRegion(Regions.US_WEST_2);
        }
    }

    public Region getRegion()
    {
        return this.region;
    }

    public boolean isBackupEnabled()
    {
        return this.backupEnabled;
    }

    public String getBackupSnsArn()
    {
        return backupSnsArn;
    }

    public String getBackupEmrSubnetId()
    {
        return backupEmrSubnetId;
    }

    public String getBackupEmrKeyPairName()
    {
        return backupEmrKeyPairName;
    }
}
