/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.http.IdleConnectionReaper;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedList;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodbv2.datamodeling.S3Link;
import com.amazonaws.services.dynamodbv2.document.BatchWriteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.neptunetg.mdce.common.data.model.DynamoItem;
import com.neptunetg.mdce.common.data.model.GatewayPacketReceivedDynamoItem;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import com.neptunetg.mdce.common.data.model.MiuPacketSentDynamoItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Repository;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Packet repository that stores packets in DynamoDB
 */
@Repository
public class DynamoPacketRepository implements PacketRepository, ApplicationListener<ContextClosedEvent>
{
    private static final Logger log = LoggerFactory.getLogger(DynamoPacketRepository.class);

    private final DynamoPacketRepositoryConfig config;
    private final DynamoPacketRepositoryMaintenanceService maintenenceService;

    private static final List<Object> EMPTY_LIST = Collections.emptyList();

    private final AmazonDynamoDBClient dynamo;
    private final DynamoDBMapper dynamoDBMapper;
    private final DynamoDB dynamoDBDocumentApi;

    // There is a 400kB max limit for dynamo item size, however that includes attribute names and overheads,
    // so we put an arbitrary smaller limit here to limit the size of the values only
    private static final int DYNAMO_DB_ITEM_MAX_SIZE_BYTES = 300 * 1024;


    /**
     * Constructor of the repository
     *
     * @param dynamoDBClient      Dynamo client
     * @param credentialsProvider aws credential for access s3.
     * @param config              Configuration for MDCE Dynamo
     */
    @Autowired
    public DynamoPacketRepository(
            AmazonDynamoDBClient dynamoDBClient,
            AWSCredentialsProvider credentialsProvider,
            DynamoPacketRepositoryConfig config,
            DynamoPacketRepositoryMaintenanceService maintenenceService)
    {
        super();
        this.config = config;

        this.dynamo = dynamoDBClient;
        this.dynamoDBMapper = new DynamoDBMapper(this.dynamo, credentialsProvider);
        this.dynamoDBDocumentApi = new DynamoDB(dynamoDBClient);

        this.maintenenceService = maintenenceService;

    }


    /******************************  Insert packets **********************************************/

    /**
     * Insert multiple rows into current hot table: miuPacketReceivedTableName, given a CmiuPacketHeader information.
     *
     * @param miuPackets multiple miuPacket data model - they will be inserted in 25 item batches.
     *                   The objects may be modified to bump timestamps to avoid PK clashes
     */
    @Override
    public void insertMiuPacketsReceived(Iterable<MiuPacketReceivedDynamoItem> miuPackets) throws MdceDataException, InterruptedException
    {
        assert (miuPackets != null);

        final DynamoPacketTableDefinition tableDef = config.getMiuPacketsReceivedTableDefinition();
        final List<MiuPacketReceivedDynamoItem> batch = new ArrayList<>(25);
        YearMonth tableDate = null;
        String tableName = null;

        final Set<String> pkDedup = new HashSet<>(); //deduplicate the PKs in the batch
        for (MiuPacketReceivedDynamoItem miuPacket : miuPackets)
        {

            String pk = miuPacket.getMiuId() + ":" + miuPacket.getInsertDate();
            while (!pkDedup.add(pk))
            {
                miuPacket.setInsertDate(miuPacket.getInsertDate() + 1L); //bump the date if clash
                pk = miuPacket.getMiuId() + ":" + miuPacket.getInsertDate();
            }

            if (miuPacket.getPacketData().capacity() > DYNAMO_DB_ITEM_MAX_SIZE_BYTES)
            {
                //externalise to S3
                miuPacket.setPacketDataExternal(
                        this.dynamoDBMapper.createS3Link(config.getLargePacketsS3BucketName(),
                                generateS3Key(tableDate, miuPacket.getMiuId(), tableName, miuPacket.getInsertDate())));

                storeS3Object(miuPacket.getPacketDataExternal(), miuPacket.getPacketData().array());

                //remove packet data from data model as the data has already been externalised to s3.
                miuPacket.setPacketData((ByteBuffer) null);
            }
        }

        for (MiuPacketReceivedDynamoItem miuPacket : miuPackets)
        {
            final YearMonth miuPacketDate = config.getTableDate(Instant.ofEpochMilli(miuPacket.getInsertDate()));
            if (!miuPacketDate.equals(tableDate))
            {
                if (batch.size() > 0)
                {
                    this.insertSingleMiuPacketBatch(batch, tableName); //insert partial batch using old tableDate
                    batch.clear();
                }
                tableDate = miuPacketDate;
                tableName = tableDef.getTableName(tableDate);
                maintenenceService.ensureHotTables(tableDate);
            }

            batch.add(miuPacket);
            if (log.isTraceEnabled())
            {
                log.trace("Adding to dynamoDb miuPacketsReceived table cmiu:insertDate {}:{}", miuPacket.getMiuId(), miuPacket.getInsertDate());
            }

            if (batch.size() >= 25)
            {
                //save to dynamo
                this.insertSingleMiuPacketBatch(batch, tableName);
                batch.clear();
            }
        }

        if (!batch.isEmpty())
        {
            this.insertSingleMiuPacketBatch(batch, tableName);
        }
    }

    /**
     * Insert a new data entry row into table: miuPacketReceivedTableName, given a CmiuPacketHeader information.
     *
     * @param miuPacket              miuPacket data model
     */
    @Override
    public void insertMiuPacketReceived(MiuPacketReceivedDynamoItem miuPacket) throws MdceDataException, InterruptedException
    {
        assert (miuPacket != null);

        this.saveRecord(miuPacket, config.getMiuPacketsReceivedTableDefinition(), String.valueOf(miuPacket.getMiuId()));
    }

    @Override
    public void insertMiuPacketSent(MiuPacketSentDynamoItem miuPacket) throws InterruptedException
    {
        this.saveRecord(miuPacket, config.getMiuPacketsSentTableDefinition(), String.valueOf(miuPacket.getMiuId()));
    }

    /**
     * Insert a new data entry row into table: gatewayPacketReceivedTableName, given a GatewayPacketReceivedDynamoItem information.
     *
     * @param gatewayPacket gatewayPacketReceived data model
     */
    @Override
    public void insertR900GatewayPacketReceived(GatewayPacketReceivedDynamoItem gatewayPacket) throws InterruptedException
    {
        assert (gatewayPacket != null);

        this.saveRecord(gatewayPacket, config.getGatewayPacketsReceivedTableDefinition(), gatewayPacket.getCollectorId());
    }

    /**
     * Persist the POJO data model to dynamoDBDocumentApi, externalising data to S3 if the size exceeds the limit.
     *
     * @param packet    packet data model representing a gateway configuration record
     * @param tableDefinition table to insert the record into
     */
    private <T extends DynamoItem> void saveRecord(T packet, DynamoPacketTableDefinition tableDefinition,
                                                   String collectorOrMiuId) throws InterruptedException
    {

        final Instant insertTimestamp = Instant.ofEpochMilli(packet.getInsertDate());

        final YearMonth tableDate = config.getTableDate(insertTimestamp);
        maintenenceService.ensureHotTables(tableDate);

        //insert item to database
        final String gatewayPacketReceivedTableName = tableDefinition.getTableName(tableDate);

        if (packet.getPacketData().capacity() > DYNAMO_DB_ITEM_MAX_SIZE_BYTES)
        {
            //externalise to S3
            packet.setPacketDataExternal(
                    this.dynamoDBMapper.createS3Link(config.getLargePacketsS3BucketName(),
                            generateS3Key(tableDate, collectorOrMiuId, gatewayPacketReceivedTableName, System.currentTimeMillis())));

            storeS3Object(packet.getPacketDataExternal(), packet.getPacketData().array());

            //remove packet data from data model as the data has already been externalised to s3.
            packet.setPacketData((ByteBuffer) null);
        }

        packet.setInsertDate(insertTimestamp.toEpochMilli());
        //use high level api
        this.dynamoDBMapper.save(packet,
                new DynamoDBMapperConfig(new DynamoDBMapperConfig.TableNameOverride(gatewayPacketReceivedTableName)));

        //TODO: if PK clash, nudge the insert date and try again

        /*  alternatively, use low level api. However there is not observed performance gain and manipulating model is
            more verbose than attributeValues map.
            low level api example code :
            PutItemRequest putItemRequest = new PutItemRequest(miuPacketReceivedTableName, miuPacket.toDynamoDbItem());
            PutItemResult putItemResult = this.dynamo.putItem(new PutItemRequest(miuPacketReceivedTableName, miuPacket.toDynamoDbItem()));
         */
    }


    /**
     * Persist the POJO data model to dynamoDBDocumentApi.  Data should already have been externalised to S3 if the size exceeds the limit.
     *
     * @param miuPackets       packet data model representing up to 25 records
     * @param tableName        name of table, which must already exist
     * @return null if all inserts succeeded, otherwise list of failed MIU IDs
     */
    private List<Integer> insertSingleMiuPacketBatch(List<MiuPacketReceivedDynamoItem> miuPackets, String tableName)
    {
        List<Integer> ret = null;
        //use high level api
        final List<DynamoDBMapper.FailedBatch> failures = this.dynamoDBMapper.batchWrite(miuPackets, EMPTY_LIST,
                new DynamoDBMapperConfig(new DynamoDBMapperConfig.TableNameOverride(tableName)));

        if (!failures.isEmpty())
        {
            boolean anyFailuresOnRetry = false;

            //try to insert the failed items
            for (DynamoDBMapper.FailedBatch failedBatch : failures)
            {
                log.warn("Batch insert into " + tableName + " failed", failedBatch.getException());
                final Map<String, List<WriteRequest>> unprocessedItems = failedBatch.getUnprocessedItems(); //map from table name to list of write requests

                final List<WriteRequest> failedInserts = unprocessedItems.get(tableName);

                if (failedInserts != null && !failedInserts.isEmpty())
                {
                    //insert individually
                    for (WriteRequest wr : failedInserts)
                    {
                        if (!retryIndividualInsert(tableName, wr))
                        {
                            log.error("Insert of packet into DynamoDB failed." + printPacket(wr.getPutRequest().getItem()));
                            if (ret == null)
                            {
                                ret = new ArrayList<Integer>();
                            }
                            ret.add(getMiuId(wr.getPutRequest().getItem()));
                        }
                    }
                }

            }

        }
        return ret;
    }

    private Integer getMiuId(Map<String, AttributeValue> item)
    {
        return Integer.valueOf(item.get("miuId").getN());
    }

    private static String printPacket(Map<String, AttributeValue> item)
    {
        final StringBuilder ret = new StringBuilder();
        for (Map.Entry<String, AttributeValue> me : item.entrySet())
        {
            ret.append("{").append(me.getKey()).append("=").append(me.getValue().toString()).append("}");
        }
        return ret.toString();
    }

    private boolean retryIndividualInsert(String tableName, WriteRequest wr)
    {
        final Map<String, List<WriteRequest>> req = new HashMap<>();
        req.put(tableName, Arrays.asList(wr));
        try
        {
            for (int attempt = 0; attempt < 3; attempt++)
            {
                final BatchWriteItemOutcome result = dynamoDBDocumentApi.batchWriteItemUnprocessed(req);
                final Map<String, List<WriteRequest>> failures = result.getUnprocessedItems();
                if (failures == null || failures.isEmpty())
                {
                    return true; //success
                }
                //bump the insert date - last gasp attempt to get it in
                final Map<String, AttributeValue> item = wr.getPutRequest().getItem();
                final AttributeValue av = item.get("insertDate");
                long insertDate = Long.parseLong(av.getN());
                insertDate += 10L;
                av.setN(Long.toString(insertDate));
                item.put("insertDate", av); //don't know if this is necessary
            }

        }
        catch (Exception e)
        {
            log.error("Exception when retrying insert", e);
            return false;
        }
        log.error("Insert failed after multiple attempts");
        return false;

    }


    /**
     * Generate the s3 key given miuId and table name.
     *
     * @param folderDate date of the folder, to be used as the root folder for this record
     * @param miuId      miu id
     * @param tableName  table name
     *
     * @return s3 key in the format: {rootFolder}/{tableName}/{miuId}_currentUnixTimeInSeconds
     */
    private String generateS3Key(YearMonth folderDate, int miuId, String tableName, long timestampMillis)
    {
        return this.config.generateS3PacketDataKey(folderDate, String.format("%09d", miuId), tableName, timestampMillis);

    }

    /**
     * Generate the s3 key given collectorId and table name.
     *
     * @param folderDate  date of the folder, to be used as the root folder for this record
     * @param collectorId collector id
     * @param tableName   table name
     * @return s3 key in the format: {rootFolder}/{tableName}/{collectorId}_currentUnixTimeInSeconds
     */
    private String generateS3Key(YearMonth folderDate, String collectorId, String tableName, long timestampMillis)
    {
        return this.config.generateS3PacketDataKey(folderDate, collectorId, tableName, timestampMillis);
    }


    /**
     * Store an array of bytes to destinated S3 bucket with server side encryption.
     * This function differs to the default s3Link which does not perform encryption.
     *
     * @param s3link     the s3 link with bucket name and key defined
     * @param packetData data to be saved to s3
     * @return result of the put operation.
     */
    private static PutObjectResult storeS3Object(S3Link s3link, final byte[] packetData)
    {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(packetData.length);
        metadata.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION);

        PutObjectResult result = s3link.getAmazonS3Client().putObject(new PutObjectRequest(
                s3link.getBucketName(),
                s3link.getKey(),
                new ByteArrayInputStream(packetData),
                metadata));

        return result;
    }


    /* *********************************** query for packets *****************************/


    /**
     * Given the packet list, if data are externalised into S3, retrieve it.
     *
     * @param packet dynamoItem packet where the data will be retrieved and stored into the fields
     */
    private static <T extends DynamoItem> T retrievePacketDataFromS3(T packet)
    {
        if (packet != null && packet.getPacketDataExternal() != null && !packet.getPacketDataExternal().getKey().isEmpty())
        {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            //data stored in S3, retrieve.
            packet.getPacketDataExternal().downloadTo(byteArrayOutputStream);
            packet.setPacketData(ByteBuffer.wrap(byteArrayOutputStream.toByteArray()));
        }
        return packet;
    }

    @Override
    public List<Table> getRelevantTablesForMiuPacketQuery(Instant minInsertTimestamp, Instant maxInsertTimestamp)
    {
        return getRelevantTablesForPacketQuery(minInsertTimestamp, maxInsertTimestamp, config.getMiuPacketsReceivedTableDefinition());
    }

    @Override
    public List<Table> getRelevantTablesForGatewayPacketQuery(Instant minInsertTimestamp, Instant maxInsertTimestamp)
    {
        return getRelevantTablesForPacketQuery(minInsertTimestamp, maxInsertTimestamp, config.getGatewayPacketsReceivedTableDefinition());
    }

    private List<Table> getRelevantTablesForPacketQuery(Instant minInsertTimestamp, Instant maxInsertTimestamp,
                                                        DynamoPacketTableDefinition tableDefinition)
    {
        final YearMonth minInsertDate = config.getTableDate(minInsertTimestamp);
        final YearMonth maxInsertDate = config.getTableDate(maxInsertTimestamp);

        //TODO: could check for maximum date range here

        final List<Table> actuallyExistingTableList = maintenenceService.listTables();
        final Map<String, Table> tableByName = new HashMap<>(actuallyExistingTableList.size());
        actuallyExistingTableList.forEach(t -> tableByName.put(t.getTableName(), t));

        final List<Table> ret = new ArrayList<>();

        for (YearMonth d = minInsertDate; !d.isAfter(maxInsertDate); d = d.plus(1L, ChronoUnit.MONTHS)) {
            final String tableName = tableDefinition.getTableName(d);
            final Table table = tableByName.get(tableName);
            if (table != null) {
                ret.add(table);
            }
        }
        return ret;
    }


    /**
     * Return all packets matching MIU id and later than or equal the insert date.
     *
     * @param miuId        MIU ID to return
     * @param minInsertTimestamp the minimum insert date
     * @param maxInsertTimestamp the maximum insert date to retrieve
     * @param tablesToQuery tables containing the data
     * @return stream of miu packet
     */
    @Override
    public Stream<MiuPacketReceivedDynamoItem> streamMiuPacketsInsertedInDateRange(int miuId,
                                                                                   Instant minInsertTimestamp, Instant maxInsertTimestamp,
                                                                                   Collection<Table> tablesToQuery)
    {
        if (maxInsertTimestamp.isBefore(minInsertTimestamp))
        {
            return Stream.of();
        }

        final MiuPacketReceivedDynamoItem hashKeyValues = new MiuPacketReceivedDynamoItem();
        hashKeyValues.setMiuId(miuId);

        final Condition betweenDateCondition = dateTimestampRangeCondition(minInsertTimestamp, maxInsertTimestamp);

        final DynamoDBQueryExpression<MiuPacketReceivedDynamoItem> queryExpression = new DynamoDBQueryExpression<MiuPacketReceivedDynamoItem>()
                .withHashKeyValues(hashKeyValues)
                .withRangeKeyCondition("insertDate", betweenDateCondition)
                .withConsistentRead(false);

        return this.streamAllRecords(MiuPacketReceivedDynamoItem.class,
                tablesToQuery,
                queryExpression);

    }

    @Override
    public Iterable<MiuPacketReceivedDynamoItem> iterateMiuPacketsInsertedInDateRange(int miuId, Instant minInsertTimestamp, Instant maxInsertTimestamp, Collection<Table> tablesToQuery)
    {
        return streamMiuPacketsInsertedInDateRange(miuId, minInsertTimestamp, maxInsertTimestamp, tablesToQuery)::iterator;
    }

    @Override
    public Stream<GatewayPacketReceivedDynamoItem> streamGatewayPacketsInsertedInDateRange(String gatewayId,
                                                                                           Instant minInsertTimestamp, Instant maxInsertTimestamp,
                                                                                           Collection<Table> tables)
    {
        if (maxInsertTimestamp.isBefore(minInsertTimestamp))
        {
            return Stream.of();
        }

        GatewayPacketReceivedDynamoItem hashKeyValues = new GatewayPacketReceivedDynamoItem();
        hashKeyValues.setCollectorId(gatewayId);

        final Condition betweenDateCondition = dateTimestampRangeCondition(minInsertTimestamp, maxInsertTimestamp);

        DynamoDBQueryExpression<GatewayPacketReceivedDynamoItem> queryExpression = new DynamoDBQueryExpression<GatewayPacketReceivedDynamoItem>()
                .withHashKeyValues(hashKeyValues)
                .withRangeKeyCondition("insertDate", betweenDateCondition)
                .withConsistentRead(false);

        return this.streamAllRecords(GatewayPacketReceivedDynamoItem.class,
                tables,
                queryExpression);
    }

    @Override
    public Iterable<GatewayPacketReceivedDynamoItem> iterateGatewayPacketsInsertedInDateRange(String gatewayId, Instant minInsertTimestamp, Instant maxInsertTimestamp,
                                                                                              Collection<Table> tables)
    {
        return streamGatewayPacketsInsertedInDateRange(gatewayId,
                minInsertTimestamp, maxInsertTimestamp,
                tables)::iterator;
    }

    /**
     * Search all tables for the environment within the date range and retrieve all records from this tables.
     *
     * @param dynamoItemClass          The dynamo item class
     * @param searchTables             Tables to search
     * @param queryExpression          all query expression passed to dynamoDb search
     * @param <T>                      child class derived from DynamoItem
     * @return return a list of retrieved records
     */
    private <T extends DynamoItem> Stream<T> streamAllRecords(Class<T> dynamoItemClass,
                                                              Collection<Table> searchTables,
                                                              DynamoDBQueryExpression<T> queryExpression)
    {

        if (searchTables == null || searchTables.isEmpty())
        {
            return Stream.of();
        }
        else if (searchTables.size() == 1)
        {
            final Table t = searchTables.iterator().next();
            final PaginatedQueryList<T> paginatedQueryList = this.dynamoDBMapper.query(
                    dynamoItemClass,
                    queryExpression,
                    dbMapperConfigForTableQuery(t));
            return lazyPaginatedListStream(paginatedQueryList).map(DynamoPacketRepository::retrievePacketDataFromS3);
        }
        else
        {
            return searchTables.parallelStream() //uses the forkJoinThreadPool to parallelize this query if possible,
                                            // so all Dynamo tables are queried at once.  This effectively sums the IOPS available.
                    .flatMap(t -> {

                        final PaginatedQueryList<T> paginatedQueryList = this.dynamoDBMapper.query(
                                dynamoItemClass,
                                queryExpression,
                                dbMapperConfigForTableQuery(t));
                        return lazyPaginatedListStream(paginatedQueryList);
                    })
                    .map(DynamoPacketRepository::retrievePacketDataFromS3);
        }
    }


    /**
     * This is a workaround for the issue that PaginatedList.stream() eagerly-loads all of the results
     * and then does not return any of them.
     * See https://forums.aws.amazon.com/thread.jspa?messageID=680745 for more information.
     * To see if the issue is resolved, set up and run com.neptunetg.mdce.common.aws.DynamoDbMapperTest
     * @param paginatedList Paginated list returned from scan() or query()
     * @param <T> Type of result in the list
     * @return Stream of the results.  Results are lazy-loaded
     */
    private static <T> Stream<T> lazyPaginatedListStream(PaginatedList<T> paginatedList)
    {
        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(paginatedList.iterator(), Spliterator.ORDERED),
                false);

    }

    private static final Condition dateTimestampRangeCondition(Instant minTimestamp, Instant maxTimestamp)
    {
        return new Condition()
                .withComparisonOperator(ComparisonOperator.BETWEEN.toString())
                .withAttributeValueList(new AttributeValue().withN(Long.toString(minTimestamp.toEpochMilli())),
                        new AttributeValue().withN(Long.toString(maxTimestamp.toEpochMilli())));
    }

    private static DynamoDBMapperConfig dbMapperConfigForTableQuery(Table t)
    {
        DynamoDBMapperConfig.Builder builder = new DynamoDBMapperConfig.Builder();
        builder.setTableNameOverride(new DynamoDBMapperConfig.TableNameOverride(t.getTableName()));
        builder.setPaginationLoadingStrategy(DynamoDBMapperConfig.PaginationLoadingStrategy.ITERATION_ONLY);
        return builder.build();
    }



    @Override
    public void onApplicationEvent(ContextClosedEvent contextClosedEvent)
    {
        IdleConnectionReaper.shutdown(); //see https://forums.aws.amazon.com/thread.jspa?threadID=92422
    }
}
