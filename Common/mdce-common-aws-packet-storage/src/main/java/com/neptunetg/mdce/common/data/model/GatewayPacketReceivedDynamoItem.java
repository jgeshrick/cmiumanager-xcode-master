/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.S3Link;

import java.nio.ByteBuffer;

/**
 POJO for DynamoDb table GATEWAY_PACKETS_RECEIVED.  Mapped by DynamoDBMapper to Dynamo DB item.
 */
@DynamoDBTable(tableName = "env-gateway-packets-received-yyyymmdd")  //default name
public class GatewayPacketReceivedDynamoItem implements DynamoItem, Comparable<GatewayPacketReceivedDynamoItem>
{
    @DynamoDBHashKey(attributeName="collectorId")
    private String collectorId;

    @DynamoDBRangeKey(attributeName="insertDate")
    private long insertDate;

    private long dateReceived;

    private String packetType;

    private long dateBuilt;

    private ByteBuffer packetData;
    private S3Link packetDataExternal;

    private String sourceDeviceType;
    private String targetDeviceType;

    /**
     * Getter for collectorid
     * @return collector id
     */
    public String getCollectorId()
    {
        return collectorId;
    }

    /**
     * Setter for collectorid
     */
    public void setCollectorId(String collectorId)
    {
        this.collectorId = collectorId;
    }

    /**
     * Getter for insert date
     */
    @Override
    public long getInsertDate()
    {
        return insertDate;
    }

    /**
     * Setter for insertDate
     */
    @Override
    public void setInsertDate(long insertDate)
    {
        this.insertDate = insertDate;
    }

    /**
     * Getter for dateReceived
     */
    public long getDateReceived()
    {
        return dateReceived;
    }

    /**
     * Setter for dateReceived
     */
    public void setDateReceived(long dateReceived)
    {
        this.dateReceived = dateReceived;
    }

    /**
     * Getter for packetType
     */
    public String getPacketType()
    {
        return packetType;
    }

    /**
     * Setter for packetType
     */
    public void setPacketType(String packetType)
    {
        this.packetType = packetType;
    }

    /**
     * Getter for dateBuilt
     */
    public long getDateBuilt()
    {
        return dateBuilt;
    }

    /**
     * Setter for dateBuilt
     */
    public void setDateBuilt(long dateBuilt)
    {
        this.dateBuilt = dateBuilt;
    }

    /**
     * Getter for packetDate
     */
    public ByteBuffer getPacketData()
    {
        return packetData;
    }

    /**
     * Setter for packetDate
     */
    public void setPacketData(ByteBuffer packetData)
    {
        this.packetData = packetData;
    }

    /**
     * Getter for packetDateExternal
     */
    public S3Link getPacketDataExternal()
    {
        return packetDataExternal;
    }

    /**
        Setter for packetDateExternal
     */
    public void setPacketDataExternal(S3Link packetDataExternal)
    {
        this.packetDataExternal = packetDataExternal;
    }

    @Override
    public void setPacketData(byte[] packetData)
    {
        this.packetData = ByteBuffer.wrap(packetData);
    }

    /**
     * Getter for sourceDeviceType
     */
    public String getSourceDeviceType()
    {
        return sourceDeviceType;
    }

    /**
     * Setter for sourceDeviceType
     */
    public void setSourceDeviceType(String sourceDeviceType)
    {
        this.sourceDeviceType = sourceDeviceType;
    }

    /**
     * Getter for targetDeviceType
     */
    public String getTargetDeviceType()
    {
        return targetDeviceType;
    }

    /**
     * Setter for targetDeviceType
     */
    public void setTargetDeviceType(String targetDeviceType)
    {
        this.targetDeviceType = targetDeviceType;
    }



    /**
     * Sort by GW ID, then insert date, then data
     * @param other Other GatewayPacketReceivedDynamoItem
     * @return -1 if MIU ID less, or insert date less, or a data byte is less, or the data length is less
     */
    @Override
    public int compareTo(GatewayPacketReceivedDynamoItem other)
    {
        if (other == null)
        {
            return -1;
        }

        int cmp = this.collectorId.compareTo(other.collectorId);
        if (cmp != 0)
        {
            return cmp;
        }
        else if (this.insertDate < other.insertDate)
        {
            return -1;
        }
        else if (this.insertDate > other.insertDate)
        {
            return 1;
        }
        else
        {
            cmp = packetType.compareTo(other.packetType);
            if (cmp != 0)
            {
                return cmp;
            }
            for (int i = 0; i < packetData.capacity() && i < other.packetData.capacity(); i++)
            {
                cmp = ((this.packetData.get(i) & 0xff) - (other.packetData.get(i) & 0xff));
                if (cmp < 0)
                {
                    return -1;
                }
                else if (cmp > 0)
                {
                    return 1;
                }
                i++;
            }
            if (packetData.capacity() < other.packetData.capacity())
            {
                return -1;
            }
            else if (packetData.capacity() > other.packetData.capacity())
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

    }
}
