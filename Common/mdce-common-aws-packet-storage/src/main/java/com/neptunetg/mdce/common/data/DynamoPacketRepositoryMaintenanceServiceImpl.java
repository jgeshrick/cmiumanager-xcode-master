/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.datapipeline.model.PipelineIdName;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableCollection;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.GlobalSecondaryIndexDescription;
import com.amazonaws.services.dynamodbv2.model.ListTablesResult;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughputDescription;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.util.Tables;
import com.neptunetg.mdce.common.data.model.GatewayPacketReceivedDynamoItem;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import com.neptunetg.mdce.common.data.model.MiuPacketSentDynamoItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.YearMonth;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * This class is responsible for managing the Dynamo tables
 */
@Service
public class DynamoPacketRepositoryMaintenanceServiceImpl implements DynamoPacketRepositoryMaintenanceService
{
    private static final Logger log = LoggerFactory.getLogger(DynamoPacketRepositoryMaintenanceServiceImpl.class);

    private final DynamoPacketRepositoryConfig config;

    private final AmazonDynamoDBClient dynamo;
    private final DynamoDBMapper dynamoDBMapper;
    private final DynamoDB dynamoDBDocumentApi;
    private final DynamoBackupPipelineManager dataPipelineManager;

    @Autowired
    public DynamoPacketRepositoryMaintenanceServiceImpl(
            AmazonDynamoDBClient dynamoDBClient,
            AWSCredentialsProvider credentialsProvider,
            DynamoPacketRepositoryConfig config
    )
    {
        this.config = config;

        this.dynamo = dynamoDBClient;
        this.dynamoDBMapper = new DynamoDBMapper(this.dynamo, credentialsProvider);
        this.dynamoDBDocumentApi = new DynamoDB(dynamoDBClient);
        this.dataPipelineManager = new DynamoBackupPipelineManager(credentialsProvider, config);
    }

    /************************************** table maintenance *********************************/

    @Override
    public void performHourlyMaintenance() throws InterruptedException, Exception
    {
        if (!this.config.isDynamicConfigAvailable())
        {
            throw new UnsupportedOperationException("No dynamic configuration available");
        }

        log.info("Performing DynamoDB maintenance check - looking for old data backup pipelines");

        this.dataPipelineManager.deleteOldBackupPipelines();

        final Instant now = Instant.now();
        final YearMonth currentMonth = config.getTableDate(now);

        log.info("Performing DynamoDB maintenance check - ensuring hot tables for this month");

        if (ensureHotTables(currentMonth)) //normally the tables have been created in advance, so this shouldn't be needed...
        {
            log.warn("Created DynamoDB tables for current month " + currentMonth + ".  These should have been created previously by scheduled maintenance");
        }

        if (config.isItTimeToCreateTablesForNextMonth(now))
        {
            final YearMonth nextMonth = currentMonth.plus(1L, ChronoUnit.MONTHS);
            if (ensureHotTables(nextMonth))
            {
                log.info("Created DynamoDB tables for next month " + nextMonth + ".");
            }
        }

        final YearMonth maxColdTableDate = config.getMaxColdTableDate(now);
        final YearMonth maxDeleteTableDate = config.getMaxDeleteTableDate(now);

        log.info("Performing DynamoDB maintenance check - checking for old tables to make cold or delete");

        makeTablesColdOrDelete(maxColdTableDate, maxDeleteTableDate);

        if (config.isBackupEnabled())
        {
            log.info("Performing DynamoDB maintenance check - checking for backup data pipelines");
            ensureBackupPipelinesForHotTables(currentMonth);

            log.info("Performing DynamoDB maintenance check - deleting historic backups");
            deleteSupersededBackups(currentMonth);

        }

    }

    /**
     * Update provision throughput of all old tables to cold.
     * @param maxColdTableDate e.g. if this is 2015-07 then all tables up to and including
     *                env-miu-packets-received-2015-07 will go cold
     * @param maxDeleteTableDate e.g. if this is 2015-03 then all tables up to and including
     *                env-miu-packets-received-2015-03 will be deleted
     */
    private void makeTablesColdOrDelete(YearMonth maxColdTableDate, YearMonth maxDeleteTableDate)
    {
        //set tables which have different date then activeTableDate to cold table
        for (Table t : this.listTables())
        {
            final DynamoPacketTableDefinition tableDefinition = config.getTableDefinition(t);
            if (tableDefinition != null)
            {
                try
                {
                    final YearMonth tableDate = tableDefinition.getTableDate(t);
                    if (!tableDate.isAfter(maxDeleteTableDate))
                    {
                        this.deleteTable(t);
                    }
                    else if (!tableDate.isAfter(maxColdTableDate))
                    {
                        this.changeTableIops(t, tableDefinition.getColdIops());
                    }
                }
                catch (DateTimeParseException e)
                {
                    log.warn("Bad table name: " + t.getTableName() + "; making it cold", e);
                    this.changeTableIops(t, tableDefinition.getColdIops());
                }
            }
            else
            {
                log.warn("Bad table name: " + t.getTableName() + "; IOPS not updated and not considered for deletion");
            }
        }

    }


    /**
     * Ensure that tables exist
     *
     * @param d Date for which tables should be ensured to exist
     */
    @Override
    public boolean ensureHotTables(YearMonth d) throws InterruptedException
    {
        if (!checkTablesPresent(d))
        {
            createHotTablesForDate(d);
            return true;
        }
        return false;
    }


    /**
     * Internal method to ensure that tables exist
     *
     * @param date Date for which pipelines should be ensured to exist
     * @return true if changes were made in AWS
     */
    private boolean ensureBackupPipelinesForHotTables(YearMonth date) throws Exception
    {
        boolean ret = false;
        final List<PipelineIdName> pipelines = this.dataPipelineManager.listPipelines();
        ret |= this.dataPipelineManager.ensureBackupPipelineForTable(config.getMiuPacketsReceivedTableDefinition().getTableName(date), date, pipelines);
        ret |= this.dataPipelineManager.ensureBackupPipelineForTable(config.getMiuPacketsSentTableDefinition().getTableName(date), date, pipelines);
        ret |= this.dataPipelineManager.ensureBackupPipelineForTable(config.getGatewayPacketsReceivedTableDefinition().getTableName(date), date, pipelines);
        return ret;
    }


    /**
     * Internal method to delete old backups for this month
     *
     * @param date Date for which old backups should be found and deleted
     * @return true if changes were made in AWS
     */
    private boolean deleteSupersededBackups(YearMonth date)
    {
        boolean ret = false;
        ret |= this.dataPipelineManager.deleteSupersededBackups(config.getMiuPacketsReceivedTableDefinition().getTableName(date), date);
        ret |= this.dataPipelineManager.deleteSupersededBackups(config.getMiuPacketsSentTableDefinition().getTableName(date), date);
        ret |= this.dataPipelineManager.deleteSupersededBackups(config.getGatewayPacketsReceivedTableDefinition().getTableName(date), date);
        return ret;
    }

    /**
     * Shorthand for creating table from data model, customizing the request with customized table name, provisioned throughput and projection
     *
     * @param tableModel       The class representing the table to create
     * @param definition       Table config
     * @param date             date used to form part of the customised table name
     * @return table info
     */
    private TableDescription createHotTable(Class<?> tableModel, DynamoPacketTableDefinition definition, YearMonth date)
    {
        final String tableName = definition.getTableName(date);
        final CreateTableRequest createTableRequest = this.dynamoDBMapper.generateCreateTableRequest(tableModel)
                .withTableName(tableName)
                .withProvisionedThroughput(definition.getHotIops());

        return this.dynamoDBDocumentApi.createTable(createTableRequest).getDescription();
    }


    /**
     * Check if a given throughput description has been decreased today
     *
     * @param throughputDescription throughdescription obtained from the ProvisionedThroughput object
     * @return true if throughput has been decreased today
     */
    private static boolean hasThroughputBeenDecreasedToday(ProvisionedThroughputDescription throughputDescription)
    {
        final Date lastDecreaseTime = throughputDescription.getLastDecreaseDateTime();
        //@note if throughput has not been decreased before, getLastDecreaseDateTime will be NULL.
        if (lastDecreaseTime != null)
        {
            return lastDecreaseTime.toInstant().isAfter(Instant.now().minus(1L, ChronoUnit.DAYS));
        }
        else
        {
            return false;
        }
    }

    /**
     * Change the IOPS of table and GSI to new throughput
     */
    private void changeTableIops(Table table, ProvisionedThroughput newThroughput)
    {
        //Retrieves the table description from DynamoDB.
        table.describe();

        try
        {
            ProvisionedThroughputDescription oldThroughput = table.getDescription().getProvisionedThroughput();

            if (!hasThroughputBeenDecreasedToday(oldThroughput))
            {
                //IOPS has NOT been decreased today (GMT), continue
                if (!oldThroughput.getWriteCapacityUnits().equals(newThroughput.getWriteCapacityUnits()) ||
                        !oldThroughput.getReadCapacityUnits().equals(newThroughput.getReadCapacityUnits()))
                {
                    log.debug("Changing IOPS for table {}", table.getTableName());
                    table.updateTable(newThroughput);
                }
            }
        }
        catch (AmazonServiceException e)
        {
            log.error("Error while changing table IOPS", e);
        }

        try
        {
            List<GlobalSecondaryIndexDescription> gsi = table.getDescription().getGlobalSecondaryIndexes();

            if (gsi != null)
            {
                gsi.stream()
                        .forEach(indexDesc -> {

                            if (hasThroughputBeenDecreasedToday(indexDesc.getProvisionedThroughput()))
                            {
                                //skip updating if it has been updated today already
                                return;
                            }

                            if (!indexDesc.getProvisionedThroughput().getReadCapacityUnits().equals(newThroughput.getReadCapacityUnits()) ||
                                    !indexDesc.getProvisionedThroughput().getWriteCapacityUnits().equals(newThroughput.getWriteCapacityUnits()))
                            {
                                log.debug("Changing IOPS for table {}, index {}", table.getTableName(), indexDesc.getIndexName());
                                table.getIndex(indexDesc.getIndexName()).updateGSI(newThroughput);
                            }

                        });
            }

        }
        catch (AmazonServiceException e)
        {
            log.error("Error in changeTableIops: {}", e);
        }

        try
        {
            table.waitForActive();
        }
        catch (Exception e)
        {
            log.error("Error while waiting for table to be active", e);
        }
    }

    /**
     * Create the required tables. This assumes the tables have not been created.
     *
     * @param date Date for tables
     */
    private void createHotTablesForDate(YearMonth date) throws InterruptedException
    {
        log.info("Creating tables for date " + date);
        final TableDescription[] newTable = new TableDescription[3];
        newTable[0] = this.createHotTable(MiuPacketReceivedDynamoItem.class, config.getMiuPacketsReceivedTableDefinition(), date);
        newTable[1] = this.createHotTable(MiuPacketSentDynamoItem.class, config.getMiuPacketsSentTableDefinition(), date);
        newTable[2] = this.createHotTable(GatewayPacketReceivedDynamoItem.class, config.getGatewayPacketsReceivedTableDefinition(), date);

        for (TableDescription td : newTable)
        {
            Tables.awaitTableToBecomeActive(this.dynamo, td.getTableName(), 50000, 1000);
        }
    }

    /**
     * Check all 3 tables related to the environment name and date are present.
     *
     * @param date suffix to table
     * @return true if all 3 tables are present.
     */
    private boolean checkTablesPresent(YearMonth date)
    {
        return Tables.doesTableExist(this.dynamo, config.getMiuPacketsReceivedTableDefinition().getTableName(date)) &&
                Tables.doesTableExist(this.dynamo, config.getMiuPacketsSentTableDefinition().getTableName(date)) &&
                Tables.doesTableExist(this.dynamo, config.getGatewayPacketsReceivedTableDefinition().getTableName(date));
    }



    /**
     * Get a list of all tables associate to the account and prefix with the environment variable
     *
     * @return table list
     */
    @Override
    public List<Table> listTables()
    {
        TableCollection<ListTablesResult> tables = this.dynamoDBDocumentApi.listTables();
        Iterator<Table> iterator = tables.iterator();

        List<Table> tableList = new ArrayList<>();

        while (iterator.hasNext())
        {
            final Table table = iterator.next();
            if (config.isPacketTableForMyEnvironment(table))
            {
                //this is the table we want
                tableList.add(table);
            }
        }

        return tableList;
    }


    /**
     * Wipe the database containing table for the supplied date and recreate the tables.
     * This is for test purpose.
     */
    @Deprecated
    @Override
    public void wipe()
    {
        //delete all tables associate to this environment. All tables prefix with the env will be deleted.
        List<Table> tables = listTables();
        tables.forEach(this::deleteTable);

        listTables();
    }

    private void deleteTable(Table table)
    {
        table.delete();
        try
        {
            table.waitForDelete();
        }
        catch (InterruptedException e)
        {
            log.error("Interrupted while waiting for table to be deleted", e);
        }
    }


}
