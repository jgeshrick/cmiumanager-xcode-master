/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.utility;

import java.time.Instant;

/**
    Utility function for creating unix time stamps in seconds
 */
public class UnixTimeUtility
{
    /**
     * Utility function to get current time in seconds from 1/1/1970.
     * @return current time in seconds from 1/1/1970.
     */
    public static long getUnixTimeSeconds()
    {
        return Instant.now().getEpochSecond();
    }
}
