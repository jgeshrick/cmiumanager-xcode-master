/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data;

import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParsePosition;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Represents configuration data for an MDCE Dyanamo table - name formulation and provisioned IOPS
 */
public class DynamoPacketTableDefinition
{
    private static final Logger log = LoggerFactory.getLogger(DynamoPacketTableDefinition.class);

    public static final DateTimeFormatter TABLE_NAME_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM");

    private final String tableNameBase;

    private final ProvisionedThroughput hotIops; //IOPS for hot table (today's)
    private final ProvisionedThroughput coldIops; //IOPS for cold table (before today)


    public DynamoPacketTableDefinition(String environmentName, String tableNameMiddle,
                                       int hotWriteIops, int hotReadIops, int coldWriteIops, int coldReadIops)
    {

        this.tableNameBase = environmentName + "-" + tableNameMiddle + "-";

        this.hotIops = new ProvisionedThroughput().withReadCapacityUnits(Long.valueOf(hotReadIops)).withWriteCapacityUnits(Long.valueOf(hotWriteIops));
        this.coldIops = new ProvisionedThroughput().withReadCapacityUnits(Long.valueOf(coldReadIops)).withWriteCapacityUnits(Long.valueOf(coldWriteIops));

        log.info("Provisioned IOPS for table " + this.tableNameBase + "<date>: " +
                "hot = " + hotWriteIops + " write / " + hotReadIops + " read, " +
                "cold = " + coldWriteIops + " write / " + coldReadIops + " read");

    }

    public ProvisionedThroughput getHotIops()
    {
        return hotIops;
    }

    public ProvisionedThroughput getColdIops()
    {
        return coldIops;
    }

    public String getTableName(YearMonth date)
    {
        return tableNameBase + TABLE_NAME_DATE_FORMATTER.format(date);
    }

    public YearMonth getTableDate(Table table) throws DateTimeParseException
    {
        if (!matchesTable(table))
        {
            throw new DateTimeParseException("Table name " + table.getTableName() + " does not start with " + this.tableNameBase + " and so cannot be parsed",
                    table.getTableName(), 0);
        }

        return YearMonth.from(TABLE_NAME_DATE_FORMATTER.parse(table.getTableName(), new ParsePosition(this.tableNameBase.length())));
    }

    /**
     * Check whether table matches this definition
     * @param table Table
     * @return true if its name starts with the right prefix
     */
    public boolean matchesTable(Table table)
    {
        final String tableName = table.getTableName();
        return matchesTableName(tableName);
    }


    /**
     * Check whether table matches this definition
     * @param tableName Table name to match
     * @return true if the name starts with the right prefix
     */
    public boolean matchesTableName(String tableName)
    {
        return tableName.startsWith(this.tableNameBase);
    }
}
