/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3Client;

/**
 * Creates Amazon S3 client.
 */
public class S3ClientFactory
{
    private final AmazonS3Client s3Client;

    /**
     * Factory for creating S3 client
     * @param credentialsProvider    AWS credential provider
     * @param region region for connection to s3 server
     */
    public S3ClientFactory(AWSCredentialsProvider credentialsProvider,
                           String region)
    {
        this.s3Client = new AmazonS3Client(credentialsProvider);
    }

    /**
     * Get the unique S3Client held by this object
     * @return S3Client
     */
    public AmazonS3Client getClient()
    {
        return this.s3Client;
    }
}
