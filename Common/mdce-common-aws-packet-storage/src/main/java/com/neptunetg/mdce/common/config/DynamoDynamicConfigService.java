/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.config;

/**
 * An interface that allows dynamic config to be provided to the Dynamo repository
 * from other code
 */
public interface DynamoDynamicConfigService
{
    /**
     * If false, calling any other method in this interface will cause an exception
     * @return True if dynamic config is available
     */
    boolean isDynamicConfigAvailable();

    /**
     * Get the minimum age in months before old tables are deleted
     * @return
     */
    long getOldTableDeletionMinAgeMonths();
}
