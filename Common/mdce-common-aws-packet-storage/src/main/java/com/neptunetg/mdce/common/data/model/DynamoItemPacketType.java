/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.data.model;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Holder for PacketType as defined in
 * ETI 48-02 (Managed Data Collection Engine External Interfaces).docx
 *
 * Corresponds to a string representation such as R900:51
 */
public class DynamoItemPacketType
{

    /**
     * Regex for parsing representation such as R900:51.  Picks out R900 and 51 as the groups.
     */
    private static final Pattern TEXT_REPRESENTATION_PATTERN = Pattern.compile("^([A-Za-z0-9]+):([0-9]+)$");

    /**
     * System part of packet type for CMIU (tagged) packets
     */
    public static final String SYSTEM_CMIU = "CMIU";

    /**
     * System part of packet type for R900 MIU 25-byte packets
     */
    public static final String SYSTEM_R900 = "R900";

    /**
     * System part of packet type for R900 Gateway packets
     */
    public static final String SYSTEM_GATEWAY = "GW";

    /**
     * System part of packet type for L900
     */
    public static final String SYSTEM_L900 = "L900";

    /**
     * Lookup for known packet types
     */
    private static final Map<String, DynamoItemPacketType> KNOWN_PACKET_TYPES = new HashMap<>();

    public static final DynamoItemPacketType CMIU_DETAILED_CONFIGURATION_STATUS = new DynamoItemPacketType(0x00, SYSTEM_CMIU, true);
    public static final DynamoItemPacketType CMIU_INTERVAL_DATA = new DynamoItemPacketType(0x01, SYSTEM_CMIU, true);
    public static final DynamoItemPacketType CMIU_BASIC_CONFIGURATION_STATUS = new DynamoItemPacketType(0x02, SYSTEM_CMIU, true);
    public static final DynamoItemPacketType CMIU_EVENT = new DynamoItemPacketType(0x03, SYSTEM_CMIU, true);
    public static final DynamoItemPacketType MDCE_TIME_OF_DAY = new DynamoItemPacketType(0x04, SYSTEM_CMIU, true); //er, why are we storing this?
    public static final DynamoItemPacketType CMIU_COMMAND_CONFIGURATION = new DynamoItemPacketType(0x05, SYSTEM_CMIU, true);
    public static final DynamoItemPacketType CMIU_COMMAND_CONFIGURATION_RESPONSE = new DynamoItemPacketType(0x06, SYSTEM_CMIU, true);
    public static final DynamoItemPacketType R900_MIU_READINGS = new DynamoItemPacketType(0x07, SYSTEM_R900, true);

    public static final DynamoItemPacketType GW_OOK_GW_CONFIGURATION = new DynamoItemPacketType(0x01, SYSTEM_GATEWAY, true);
    public static final DynamoItemPacketType GW_FSK_GW_CONFIGURATION = new DynamoItemPacketType(0x02, SYSTEM_GATEWAY, true);
    public static final DynamoItemPacketType GW_FSK_MIU_CONFIGURATION = new DynamoItemPacketType(0x03, SYSTEM_GATEWAY, true);
    public static final DynamoItemPacketType GW_FSK_MIU_RATE = new DynamoItemPacketType(0x04, SYSTEM_GATEWAY, true);
    public static final DynamoItemPacketType GW_FSK_UNKNOWN = new DynamoItemPacketType(0x05, SYSTEM_GATEWAY, true);

    public static final DynamoItemPacketType L900_11_BYTE_CONFIG_PACKET_SENET = new DynamoItemPacketType(0x00, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_11_BYTE_READ_PACKET_SENET = new DynamoItemPacketType(0x01, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_11_BYTE_AlARM_PACKET_SENET = new DynamoItemPacketType(0x02, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_11_BYTE_APP_COMMAND_RESPONSE = new DynamoItemPacketType(0x03, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_53_BYTE_READ_PACKET_SENET = new DynamoItemPacketType(0x04, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_53_BYTE_DETAILED_CONFIG_PACKET_SENET = new DynamoItemPacketType(0x05, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_11_BYTE_TIME_AND_DATE_PACKET_SENET = new DynamoItemPacketType(0x06, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_APP_COMMAND_PACKET_SENET = new DynamoItemPacketType(0x14, SYSTEM_L900, true);

    public static final DynamoItemPacketType L900_11_BYTE_CONFIG_PACKET_1M2M = new DynamoItemPacketType(0x32, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_11_BYTE_READ_PACKET_1M2M = new DynamoItemPacketType(0x33, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_11_BYTE_AlARM_PACKET_1M2M = new DynamoItemPacketType(0x34, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_11_BYTE_APP_COMMAND_RES1M2M = new DynamoItemPacketType(0x35, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_53_BYTE_READ_PACKET_1M2M = new DynamoItemPacketType(0x36, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_53_BYTE_DETAILED_CONFIG_PACKET_1M2M = new DynamoItemPacketType(0x37, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_11_BYTE_TIME_AND_DATE_PACKET_1M2M = new DynamoItemPacketType(0x38, SYSTEM_L900, true);
    public static final DynamoItemPacketType L900_APP_COMMAND_PACKET_1M2M = new DynamoItemPacketType(0x46, SYSTEM_L900, true);

    /**
     * Numeric part of packet type
     */
    private final int id;

    /**
     * String part of packet type
     */
    private final String system;

    /**
     * Internal constructor
     * @param packetTypeId Numeric part
     * @param systemName String part
     * @param known Boolean if standard
     */
    private DynamoItemPacketType(int packetTypeId, String systemName, boolean known)
    {
        if (systemName == null)
        {
            throw new IllegalArgumentException("System name null");
        }
        this.system = systemName;
        this.id = packetTypeId;
        if (known)
        {
            KNOWN_PACKET_TYPES.put(toString(), this);
        }

    }

    /**
     * Get the numeric part of the packet type
     * @return Numeric oart
     */
    public int getId()
    {
        return id;
    }

    /**
     * Return the string describing the packet in the format {System:ID}
     * @return System:ID
     */
    @Override
    public String toString()
    {
        return this.getSystemName() + ":" + id;
    }

    /**
     * Get the string part of the packet type
     * @return System
     */
    public String getSystemName()
    {
        return this.system;
    }

    /**
     * Get PacketType enum from packet id
     * @param packetTypeId the packet id
     * @return the packet type enum
     */
    public static DynamoItemPacketType fromSystemAndId(int packetTypeId, String systemName)
    {
        for (DynamoItemPacketType p : KNOWN_PACKET_TYPES.values())
        {
            if (p.getId() == packetTypeId && systemName.equals(p.system))
            {
                return p;
            }
        }

        return new DynamoItemPacketType(packetTypeId, systemName, false);
    }

    /**
     * Given a string {System:ID}, return the packet type.
     * @param packetName The full string in the format {System:ID}
     * @return the packet type enum
     */
    public static DynamoItemPacketType fromString(String packetName)
    {
        DynamoItemPacketType ret = KNOWN_PACKET_TYPES.get(packetName);
        if (ret == null)
        {
            final Matcher m = TEXT_REPRESENTATION_PATTERN.matcher(packetName);
            if (!m.matches())
            {
                throw new IllegalArgumentException("Invalid format: " + packetName);
            }
            String system = m.group(1);
            int packetTypeNumber = Integer.parseInt(m.group(2));
            ret = new DynamoItemPacketType(packetTypeNumber, system, false);
        }
        return ret;

    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DynamoItemPacketType that = (DynamoItemPacketType) o;

        return id == that.id && system.equals(that.system);
    }

    @Override
    public int hashCode()
    {
        int result = id;
        result = 31 * result + system.hashCode();
        return result;
    }
}
