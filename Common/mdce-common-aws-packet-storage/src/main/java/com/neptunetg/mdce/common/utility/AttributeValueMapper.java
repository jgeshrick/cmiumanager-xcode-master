/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.utility;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Utility to generate DynamoDb AttributeValue from primitive types
 */
public class AttributeValueMapper
{
    /**
     * Shorthand for creating an attribute value from long.
     * @param number value to be created from
     * @return attribute value for adding to dynamo db
     */
    public static AttributeValue asNumber(long number)
    {
        return new AttributeValue().withN(Long.toString(number));
    }

    /**
     * Shorthand for creating an attribute value from int.
     * @param number value to be created from
     * @return attribute value for adding to dynamo db
     */
    public static AttributeValue asNumber(int number)
    {
        return new AttributeValue().withN(Integer.toString(number));
    }

    /**
     * Shorthand for creating an attribute value from byte array.
     * @param data byte array value to be created from
     * @return attribute value for adding to dynamo db
     */
    public static AttributeValue asBinary(byte[] data)
    {
        ByteBuffer packetData = ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN);
        return new AttributeValue().withB(packetData);
    }

    /**
     * Shorthand for creating an attribute value from long.
     * @param data string to be created from
     * @return attribute value for adding to dynamo db
     */
    public static AttributeValue asString(String data)
    {
        return new AttributeValue().withS(data);
    }
}
