/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a database exception in the MDCE
 */
public class MdceDataException extends Exception
{

    private final List<DynamoDBMapper.FailedBatch> failures;

    /**
     * Construct with a method and no inner exception
     * @param message Message for exception
     */
    public MdceDataException(String message)
    {
        super(message);
        this.failures = null;
    }

    /**
     * Construct with a method and an inner exception
     * @param message Message for exception
     * @param cause Inner exception
     */
    public MdceDataException(String message, Throwable cause)
    {
        super(message, cause);
        this.failures = null;
    }

    /**
     * Construct with a list of failures
     * @param message Message for exception
     * @param failures Failed items
     */
    public MdceDataException(String message, List<DynamoDBMapper.FailedBatch> failures)
    {
        super(message);
        this.failures = new ArrayList<>();
        this.failures.addAll(failures);
    }

}
