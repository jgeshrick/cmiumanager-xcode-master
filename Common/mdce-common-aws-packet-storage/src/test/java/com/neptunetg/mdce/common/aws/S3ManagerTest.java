/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.aws;


import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3Client;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * Live test with Amazon S3. This requires a valid Amazon S3 credential and the required buckets to be created.
 */
@Ignore("Require access to Amazon s3, use for local debug purpose.")
public class S3ManagerTest
{
    private final String bucketNameLargePackets = "test-large-packets";
    private final String bucketNameOldPacketTables = "test-old-packet-tables";
    private S3Manager s3Manager;

    @Before
    public void setUp() throws IOException
    {
        AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider("/dev/aws-local-credential.properties");
        AmazonS3Client s3Client = new S3ClientFactory(credentialsProvider, "us-west-2").getClient();

        s3Manager = new S3Manager(s3Client, bucketNameLargePackets, bucketNameOldPacketTables);
    }

    private static String generateFilePath(int miuId)
    {
        return "unit-test/" + DateTime.now().toString() + "/" + String.format("%06d", miuId);
    }

    @Test
    public void testGetPacket() throws Exception
    {
        final byte[] data = new byte[]{1,2,3,4,5,6,7,8,9,10};

        String key = s3Manager.putPacket(generateFilePath(123), 123L, 1, data);

        assertTrue(s3Manager.isPacketPresent(key));

        //delete packet
        s3Manager.deletePacket(key);
        assertFalse(s3Manager.isPacketPresent(key));
    }

    @Test
    public void testPutPacket() throws Exception
    {
        final byte[] data = new byte[]{1,2,3,4,5,6,7,8,9,10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        String packetDateTimeFolder = DateTime.now().toString();

        String key = s3Manager.putPacket(generateFilePath(123), 123L, 1, data);
        byte[] retrievedPacketData = s3Manager.getPacket(key);

        assertArrayEquals(data, retrievedPacketData);

        //delete packet
        s3Manager.deletePacket(key);
        assertFalse(s3Manager.isPacketPresent(key));
    }

    /**
     * Test buckets {} are present in S3
     */
    @Test
    public void testAreBucketPresent()
    {
        assertTrue(s3Manager.areBucketsPresent());
    }

    @Test
    public void testGetObjectAsStream()
    {
        InputStream s3Stream = s3Manager.getObjectAsStream("neptune-image-distribution", "CmiuApplication/CmiuApplication_v0.0.0.0.bin");
        assertTrue(s3Stream != null);

        s3Stream = s3Manager.getObjectAsStream("neptune-image-distribution", "CmiuApplication/INVALID KEY");
        assertTrue(s3Stream == null);

    }
}