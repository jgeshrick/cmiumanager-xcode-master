/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.aws;


import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.junit.Assert.*;

/**
 * Test whether Amazon have fixed https://forums.aws.amazon.com/thread.jspa?messageID=680745
 *
 * Requires a table with some data in running local DynamoDB-local.
 */
@Ignore("Requires data setup and DynamoDB-local")
public class DynamoDbMapperTest
{
    private static final String tableWithSomeData = "local-dev-miu-packets-received-2015-10"; //Update this to the name of a table with some CMIU packets in it

    private AmazonDynamoDBClient dynamo;
    private DynamoDBMapper dynamoDBMapper;
    private DynamoDB dynamoDBDocumentApi;
    private int foundCount;

    @Before
    public void setUp() throws IOException
    {
        AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider("/dev/dummy_aws_credentials.properties");

        dynamo = new AmazonDynamoDBClient(credentialsProvider);
        dynamo.setEndpoint("http://localhost:8000");
        dynamoDBMapper = new DynamoDBMapper(dynamo);
        dynamoDBDocumentApi = new DynamoDB(dynamo);
    }

    @Test
    public void testStream() throws Exception
    {
        Table testTable = null;
        for (Table t : dynamoDBDocumentApi.listTables())
        {
            if (t.getTableName().equals(tableWithSomeData))
            {
                testTable = t;
                break;
            }

        }
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();


        final Condition betweenDateCondition = dateTimestampRangeCondition(Instant.now().minus(10L, ChronoUnit.DAYS), Instant.now());

        scanExpression.addFilterCondition("insertDate", betweenDateCondition);

        scanExpression.addFilterCondition("sourceDeviceType", new Condition()
                .withComparisonOperator(ComparisonOperator.EQ.toString())
                .withAttributeValueList(new AttributeValue().withS("CMIU")));

        PaginatedScanList<MiuPacketReceivedDynamoItem> psl;
        Stream<MiuPacketReceivedDynamoItem> psls;
        Iterator<MiuPacketReceivedDynamoItem> psli;

        psl = this.dynamoDBMapper.scan(MiuPacketReceivedDynamoItem.class, scanExpression,
                dbMapperConfig(testTable));


        //test using spliterator of iterator
        psls = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(psl.iterator(), Spliterator.ORDERED),
                false);

        List<String> foundIds1 = new ArrayList<>();

        foundCount = 0;
        psls.forEach(p -> {
            foundCount++;
            foundIds1.add(p.getMiuId() + ":" + p.getInsertDate());
        });
        assertNotEquals("Using spliteratorUnknownSize - this worked before!!  Is there really data in table " + tableWithSomeData + " (prerequisite of this test)",
                0, foundCount);

        int foundCountWithSpliteratorUnknownSize = foundCount;

        psl = this.dynamoDBMapper.scan(MiuPacketReceivedDynamoItem.class, scanExpression,
                dbMapperConfig(testTable));

        psli = psl.iterator();
        assertTrue("Iterator empty without calling size()", psli.hasNext());


        psl = this.dynamoDBMapper.scan(MiuPacketReceivedDynamoItem.class, scanExpression,
                dbMapperConfig(testTable));

        psli = psl.iterator();
        long s = psl.size();
        assertTrue("Iterator empty after calling size()  (indicates AWS have not fixed the problem https://forums.aws.amazon.com/thread.jspa?messageID=680745)", psli.hasNext());

        psls = psl.stream();

        List<String> foundIds2 = new ArrayList<>();
        foundCount = 0;
        psls.forEach(p -> {
            foundCount++;
            foundIds2.add(p.getMiuId() + ":" + p.getInsertDate());
        });

        Iterator<String> idi2 = foundIds2.iterator();
        String nextId2 = idi2.hasNext() ? idi2.next() : null;
        int matchCount = 0;
        int nonMatchCount = 0;
        for (String id1 : foundIds1)
        {
            if (id1.equals(nextId2))
            {
                if (nonMatchCount > 0)
                {
                    System.out.println(nonMatchCount + " not matched");
                }
                nonMatchCount = 0;
                matchCount++;
                nextId2 = idi2.hasNext() ? idi2.next() : null;
            }
            else
            {
                if (nonMatchCount == 0)
                {
                    System.out.println("First " + matchCount + " matched");
                }
                else if (matchCount > 0)
                {
                    System.out.println("In the middle, another " + matchCount + " matched");
                }
                matchCount = 0;
                nonMatchCount++;
            }
        }
        if (nonMatchCount > 0)
        {
            System.out.println(nonMatchCount + " not matched");
        }

        assertEquals("Using PaginatedScanList.stream() (indicates AWS have not fixed the problem https://forums.aws.amazon.com/thread.jspa?messageID=680745)", foundCountWithSpliteratorUnknownSize, foundCount);

    }

    private static DynamoDBMapperConfig dbMapperConfig(Table t)
    {
        DynamoDBMapperConfig.Builder builder = new DynamoDBMapperConfig.Builder();
        builder.setTableNameOverride(new DynamoDBMapperConfig.TableNameOverride(t.getTableName()));
        //builder.setPaginationLoadingStrategy(DynamoDBMapperConfig.PaginationLoadingStrategy.ITERATION_ONLY);
        return builder.build();
    }


    private static final Condition dateTimestampRangeCondition(Instant minTimestamp, Instant maxTimestamp)
    {
        return new Condition()
                .withComparisonOperator(ComparisonOperator.BETWEEN.toString())
                .withAttributeValueList(new AttributeValue().withN(Long.toString(minTimestamp.toEpochMilli())),
                        new AttributeValue().withN(Long.toString(maxTimestamp.toEpochMilli())));
    }

}