/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test PacketType enum.
 */
public class DynamoItemPacketTypeTest
{
    @Test
    public void testGetId() throws Exception
    {
        DynamoItemPacketType packetType = DynamoItemPacketType.CMIU_DETAILED_CONFIGURATION_STATUS;
        assertEquals(0, packetType.getId());
        assertEquals("CMIU:0", packetType.toString());
        assertEquals("CMIU", packetType.getSystemName());

        packetType = DynamoItemPacketType.GW_OOK_GW_CONFIGURATION;
        assertEquals(1, packetType.getId());
        assertEquals("GW:1", packetType.toString());
        assertEquals("GW", packetType.getSystemName());

    }

    @Test
    public void testConstructPacketType()
    {
        DynamoItemPacketType packetType = DynamoItemPacketType.fromSystemAndId(0, DynamoItemPacketType.SYSTEM_CMIU);
        assertEquals(0, packetType.getId());
        assertEquals("CMIU:0", packetType.toString());

        DynamoItemPacketType packetType2 = DynamoItemPacketType.fromString("CMIU:2");
        assertEquals(2, packetType2.getId());
        assertEquals("CMIU", packetType2.getSystemName());
    }

    @Test (expected = IllegalArgumentException.class)
    public void testConstructPacketTypeInvalidArgument()
    {
        DynamoItemPacketType.fromString("MIU :1");
    }
}