/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.neptunetg.mdce.common.config.DynamoDynamicConfigService;
import org.junit.Ignore;
import org.junit.Test;

import java.time.YearMonth;
import java.util.Properties;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for DynamoBackupPipelineManager
 */
public class DynamoBackupPipelineManagerTest
{

    /**
     * Requires a bunch of folders in neptune-mdce-dev1-old-data/2015-11/dynamodb/test-2015-11.
     * Run to check that only the newest is left and the rest are deleted.
     * Note: no assertions in this test.  You need to check S3 in AWS management console.
     * @throws Exception
     */
    @Ignore("Requires a bunch of folders in neptune-mdce-dev1-old-data/2015-11/dynamodb/test-2015-11.")
    @Test
    public void testDeleteSupersededBackups() throws Exception
    {
        AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider("/dev/dummy_aws_credentials.properties");

        final Properties dynamoProps = new Properties();
        dynamoProps.load(getClass().getResourceAsStream("/dynamo-junit-env.properties"));

        dynamoProps.setProperty("env.name", "dev1");

        DynamoDynamicConfigService dynamicConfig = mock(DynamoDynamicConfigService.class);
        when(dynamicConfig.getOldTableDeletionMinAgeMonths()).thenReturn(3L);

        DynamoPacketRepositoryConfig config = new DynamoPacketRepositoryConfig(dynamoProps, dynamicConfig);

        DynamoBackupPipelineManager pipelineManager = new DynamoBackupPipelineManager(credentialsProvider, config);

        pipelineManager.deleteSupersededBackups("test-2015-11", YearMonth.of(2015, 11));
    }




}
