/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.BatchWriteItemRequest;
import com.amazonaws.services.dynamodbv2.model.BatchWriteItemResult;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import com.neptunetg.mdce.common.config.DynamoDynamicConfigService;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.time.Instant;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test the DynamoPacketRepo
 */
public class DynamoPacketRepositoryTest
{
    private static final ZoneId TIME_ZONE_FOR_TABLE_ROTATION = ZoneId.of("America/Chicago"); //we tick over our DB at midnight CT

    /**
     * Checks that all MIU packets received get inserted into the right table by their insert date,
     * even when the dates span two days
     * @throws Exception if an error occurs
     */
    @Test
    public void testInsertMiuPacketsReceived() throws Exception
    {
        AWSCredentialsProvider credentialsProvider = mock(AWSCredentialsProvider.class);

        AmazonDynamoDBClient dynamo = mock(AmazonDynamoDBClient.class);

        DynamoPacketRepositoryMaintenanceService maintenenceService = mock(DynamoPacketRepositoryMaintenanceService.class);

        AWSCredentials awsCredentials = mock(AWSCredentials.class);
        when(awsCredentials.getAWSAccessKeyId()).thenReturn("access");
        when(awsCredentials.getAWSSecretKey()).thenReturn("secret");
        when(credentialsProvider.getCredentials()).thenReturn(awsCredentials);

        Properties envProperties = new Properties();
        envProperties.load(DynamoPacketRepositoryTest.class.getResourceAsStream("/dynamo-junit-env.properties"));

        DynamoPacketRepositoryConfig config = new DynamoPacketRepositoryConfig(envProperties,  mock(DynamoDynamicConfigService.class));

        DynamoPacketRepository repo = new DynamoPacketRepository(dynamo, credentialsProvider, config, maintenenceService);

        final Instant insertTime = ZonedDateTime.of(2016, 1, 1, 0, 0, 0, 0, TIME_ZONE_FOR_TABLE_ROTATION).minus(1L, ChronoUnit.MILLIS).toInstant();
        assertEquals(YearMonth.of(2015, 12), config.getTableDate(insertTime)); //check we've got the right timezone
        assertEquals(YearMonth.of(2016, 1), config.getTableDate(insertTime.plus(1L, ChronoUnit.MILLIS)));

        final List<MiuPacketReceivedDynamoItem> miuPackets = new ArrayList<>();

        final Set<String> miuIdsExpected = new HashSet<>();
        for (int i = 0; i < 56; i++)
        {
            int miuId = i;
            final MiuPacketReceivedDynamoItem di = new MiuPacketReceivedDynamoItem();
            di.setInsertDate(insertTime.plus(i % 5, ChronoUnit.MILLIS).toEpochMilli()); //some inserted just before midnight, some just after
            di.setMiuId(miuId);
            di.setDateBuilt(insertTime.toEpochMilli());
            di.setPacketData(new byte[]{1, 2, 3, 4});
            miuPackets.add(di);
            miuIdsExpected.add(Integer.toString(miuId));
        }

        ArgumentCaptor<BatchWriteItemRequest> batchWriteRequest = ArgumentCaptor.forClass(BatchWriteItemRequest.class);
        final BatchWriteItemResult batchWriteResultAllGood = new BatchWriteItemResult();
        batchWriteResultAllGood.setUnprocessedItems(new HashMap<>());
        when(dynamo.batchWriteItem(batchWriteRequest.capture())).thenReturn(batchWriteResultAllGood);

        repo.insertMiuPacketsReceived(miuPackets);

        final List<BatchWriteItemRequest> requests = batchWriteRequest.getAllValues();
        for (BatchWriteItemRequest request : requests)
        {
            final Map<String, List<WriteRequest>> batchItems = request.getRequestItems();

            for (Map.Entry<String, List<WriteRequest>> me : batchItems.entrySet())
            {
                final String tableName = me.getKey();
                final List<WriteRequest> writes = me.getValue();
                for (WriteRequest write : writes)
                {
                    String miuIdString = write.getPutRequest().getItem().get("miuId").getN();
                    String insertDateString = write.getPutRequest().getItem().get("insertDate").getN();

                    assertTrue(miuIdsExpected.remove(miuIdString));
                    final Instant insertDate = Instant.ofEpochMilli(Long.parseLong(insertDateString));
                    final YearMonth tableDate = config.getTableDate(insertDate);

                    assertEquals("Inserted into wrong table", "junit-miu-packets-received-" + tableDate.format(DynamoPacketTableDefinition.TABLE_NAME_DATE_FORMATTER), tableName);

                }
            }

        }

        assertTrue("Some MIUs didn't get their packet inserted", miuIdsExpected.isEmpty());

    }


}
