/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.neptunetg.mdce.common.config.DynamoDynamicConfigService;
import com.neptunetg.mdce.common.data.DynamoPacketRepositoryConfig;
import com.neptunetg.mdce.common.data.DynamoPacketRepositoryMaintenanceServiceImpl;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.YearMonth;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Handy test for creating and deleting data pipelines.  Must run against real AWS and really creates pipelines.
 */
@Ignore("Runs against AWS cloud API and creates data pipelines")
public class DynamoPacketRepositoryMaintenanceServiceImplTest
{
    private static final Logger log = LoggerFactory.getLogger(DynamoPacketRepositoryMaintenanceServiceImplTest.class);

    private static Pattern ARN_REGION_CAPTURE_PATTERN = Pattern.compile("arn:aws:dynamodb:([A-Za-z0-9\\-]+):[0-9]+:table/.+");

    private AmazonDynamoDBClient dynamo;

    /************************************** table maintenance *********************************/

    @Test
    public void testPerformHourlyMaintenance() throws Exception
    {
        AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider("/dev/dummy_aws_credentials.properties");

        dynamo = new AmazonDynamoDBClient(credentialsProvider);
        dynamo.setEndpoint("https://dynamodb.us-west-2.amazonaws.com/");

        final Properties dynamoProps = new Properties();
        dynamoProps.load(getClass().getResourceAsStream("/dynamo-junit-env.properties"));

        dynamoProps.setProperty("endpoint", "https://dynamodb.us-west-2.amazonaws.com/");

        DynamoDynamicConfigService dynamicConfig = mock(DynamoDynamicConfigService.class);
        when(dynamicConfig.getOldTableDeletionMinAgeMonths()).thenReturn(3L);

        DynamoPacketRepositoryConfig config = new DynamoPacketRepositoryConfig(dynamoProps, dynamicConfig);

        DynamoPacketRepositoryMaintenanceServiceImpl maintenanceService = new DynamoPacketRepositoryMaintenanceServiceImpl(
                dynamo, credentialsProvider,config
                );

        maintenanceService.ensureHotTables(YearMonth.of(2015, 3));

        maintenanceService.performHourlyMaintenance(); //should make the old table cold

        maintenanceService.wipe();

    }


    @Test
    public void testTableArnToRegion()
    {
        String tableArn = "arn:aws:dynamodb:us-west-2:934427565871:table/junit-miu-packets-sent-2015-03";
        Matcher m = ARN_REGION_CAPTURE_PATTERN.matcher(tableArn);

        assertTrue(m.matches());
        assertEquals("us-west-2", m.group(1));
    }
}
