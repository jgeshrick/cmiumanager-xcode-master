/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.data;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.neptunetg.mdce.common.config.DynamoDynamicConfigService;
import org.junit.Test;

import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Properties;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
/**
 * Test DynamoPacketRepositoryConfig
 */
public class DynamoPacketRepositoryConfigTest
{

    private static final ZoneId TIME_ZONE_FOR_TABLE_ROTATION = ZoneId.of("America/Chicago"); //we tick over our DB at midnight CT

    @Test
    public void testIsItTimeToCreateTablesForNextMonth() throws Exception
    {

        final Properties envProperties = new Properties();
        envProperties.load(DynamoPacketRepositoryTest.class.getResourceAsStream("/dynamo-junit-env.properties"));

        final DynamoPacketRepositoryConfig config = new DynamoPacketRepositoryConfig(envProperties, mock(DynamoDynamicConfigService.class));

        //9:30pm on the last day of the month is the time to create the new table

        final ZonedDateTime oct31At929pm = ZonedDateTime.of(2015, 10, 31, 21, 29, 0, 0, TIME_ZONE_FOR_TABLE_ROTATION);

        assertFalse(config.isItTimeToCreateTablesForNextMonth(oct31At929pm.toInstant()));

        final ZonedDateTime oct31At930pm = ZonedDateTime.of(2015, 10, 31, 21, 30, 0, 0, TIME_ZONE_FOR_TABLE_ROTATION);

        assertTrue(config.isItTimeToCreateTablesForNextMonth(oct31At930pm.toInstant()));

        final ZonedDateTime oct31At1159pm = ZonedDateTime.of(2015, 10, 31, 23, 59, 0, 0, TIME_ZONE_FOR_TABLE_ROTATION);

        assertTrue(config.isItTimeToCreateTablesForNextMonth(oct31At1159pm.toInstant()));

    }


    @Test
    public void testGetMaxColdTableDate() throws Exception
    {

        final Properties envProperties = new Properties();
        envProperties.load(DynamoPacketRepositoryTest.class.getResourceAsStream("/dynamo-junit-env.properties"));

        DynamoDynamicConfigService dynamicConfig = mock(DynamoDynamicConfigService.class);
        when(dynamicConfig.getOldTableDeletionMinAgeMonths()).thenReturn(3L);

        final DynamoPacketRepositoryConfig config = new DynamoPacketRepositoryConfig(envProperties, dynamicConfig);

        final YearMonth september = YearMonth.of(2015, 9);

        final YearMonth october = YearMonth.of(2015, 10);

        //From 2nd of month make last month's tables code

        final ZonedDateTime nov1At1159pm = ZonedDateTime.of(2015, 11, 1, 23, 59, 0, 0, TIME_ZONE_FOR_TABLE_ROTATION);

        assertEquals(september, config.getMaxColdTableDate(nov1At1159pm.toInstant()));

        final ZonedDateTime nov2AtMidnight = ZonedDateTime.of(2015, 11, 2, 0, 0, 0, 0, TIME_ZONE_FOR_TABLE_ROTATION);

        assertEquals(october, config.getMaxColdTableDate(nov2AtMidnight.toInstant()));

    }


    @Test
    public void testGetMaxDeleteTableDate() throws Exception
    {

        final Properties envProperties = new Properties();
        envProperties.load(DynamoPacketRepositoryTest.class.getResourceAsStream("/dynamo-junit-env.properties"));

        DynamoDynamicConfigService dynamicConfig = mock(DynamoDynamicConfigService.class);
        when(dynamicConfig.isDynamicConfigAvailable()).thenReturn(Boolean.TRUE);
        when(dynamicConfig.getOldTableDeletionMinAgeMonths()).thenReturn(3L);

        final DynamoPacketRepositoryConfig config = new DynamoPacketRepositoryConfig(envProperties,  dynamicConfig);

        final YearMonth september = YearMonth.of(2015, 9);

        final YearMonth october = YearMonth.of(2015, 10);

        //From 2nd of month make last month's tables code

        final ZonedDateTime threeMonthsAfterOct31 = ZonedDateTime.of(2015, 10, 31, 23, 59, 0, 0, TIME_ZONE_FOR_TABLE_ROTATION).plusMonths(3L);

        assertEquals(september, config.getMaxDeleteTableDate(threeMonthsAfterOct31.toInstant()));

        final ZonedDateTime threeMonthsAfterNov1 = ZonedDateTime.of(2015, 11, 1, 0, 0, 0, 0, TIME_ZONE_FOR_TABLE_ROTATION).plusMonths(3L);

        assertEquals(october, config.getMaxDeleteTableDate(threeMonthsAfterNov1.toInstant()));

    }


    @Test
    public void testGetArchiveEndTimeForTableDate() throws Exception
    {
        final Properties envProperties = new Properties();
        envProperties.load(DynamoPacketRepositoryTest.class.getResourceAsStream("/dynamo-junit-env.properties"));

        DynamoDynamicConfigService dynamicConfig = mock(DynamoDynamicConfigService.class);
        when(dynamicConfig.getOldTableDeletionMinAgeMonths()).thenReturn(3L);

        final DynamoPacketRepositoryConfig config = new DynamoPacketRepositoryConfig(envProperties,  dynamicConfig);

        YearMonth nov2015 = YearMonth.of(2015, 11);
        ZonedDateTime archiveEnd = config.getArchiveEndTimeForTableDate(nov2015);
        assertTrue(archiveEnd.isAfter(YearMonth.of(2015, 12).atDay(1).atStartOfDay(archiveEnd.getZone())));
    }

    @Test
    public void testGetRegion() throws Exception
    {
        final Properties envProperties = new Properties();
        envProperties.load(DynamoPacketRepositoryTest.class.getResourceAsStream("/dynamo-junit-env.properties"));
        envProperties.setProperty("aws.dynamo.endpoint", "https://dynamodb.us-west-2.amazonaws.com/");

        final DynamoPacketRepositoryConfig config = new DynamoPacketRepositoryConfig(envProperties,  mock(DynamoDynamicConfigService.class));

        assertSame(Region.getRegion(Regions.US_WEST_2), config.getRegion());
    }

}
