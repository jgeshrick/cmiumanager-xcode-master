/*
 *  **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 *
 */

package com.neptunetg.mdce.common.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3Client;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by SML1 on 15/04/2015.
 */
@Ignore("Requires S3 access")
public class S3ClientFactoryTest
{

    @Test
    public void testGetClient() throws Exception
    {
        AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider("/dev/aws-local-credential.properties");
        AmazonS3Client s3Client = new S3ClientFactory(credentialsProvider, "us-west-2").getClient();
        assertNotNull(s3Client);
    }

    @Test
    public void testGetClientWithIamCredential()
    {
        AWSCredentialsProvider credentialsProvider = new InstanceProfileCredentialsProvider();
        AmazonS3Client s3Client = new S3ClientFactory(credentialsProvider, "us-west-2").getClient();
        assertNotNull(s3Client);
    }
}