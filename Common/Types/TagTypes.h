/******************************************************************************
*******************************************************************************
**
**         Filename: TagTypes.h
**    
**           Author: Duncan Willis
**          Created: 2015.03.05
**
**     Last Edit By: Anthony Hayward
**        Last Edit: 2015.06.19
**    
**      Description: Packet structure definitions for the Tagged Packet Builder Parser
**
** Revision History:
** 
**
**
**      Code Review:
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
/**
 * @addtogroup Tpbp
 * @{
 */


/******************************************************************************
 * @file TagTypes.h
 *
 * @brief Packet structure definitions for the Tagged Packet Builder Parser
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 ******************************************************************************/

#ifndef _TAG_TYPES_H_
#define _TAG_TYPES_H_

#include "typedefs.h"
/*===========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                        */
/*===========================================================================*/


/**
* As per ETI 48-01 "Data packets types" section.
* The E_PACKET_TYPE_ID enum indicates the packet
* type.
*/
typedef enum E_PACKET_TYPE_ID
{
    /** A detailed config packet */
    E_PACKET_TYPE_DETAILED_CONFIG               = 0,
    /** Regular meter reading data packet */
    E_PACKET_TYPE_INTERVAL_DATA                 = 1,
    /** A basic config packet */
    E_PACKET_TYPE_BASIC_CONFIG                  = 2,
    /** This packet indicates an event has occured */
    E_PACKET_TYPE_EVENT                         = 3,
    /** This packet is used to send the time */
    E_PACKET_TYPE_TIME                          = 4,
    /** A command packet */
    E_PACKET_TYPE_COMMAND                       = 5,    
    /** A response packet */
    E_PACKET_TYPE_RESPONSE                      = 6,

    E_PACKET_TYPE_COUNT
} E_PACKET_TYPE_ID;


/**
* Enum of the valid tag number that identifies the tag (must be <= 255) as per ETI 48-00.
*/
typedef enum E_TAG_NUMBER
{
    /** A null tag, used for padding */
    E_TAG_NUMBER_NULL                           = 0,
    /** Indicates the header data for each packet the CMIU sends */
    E_TAG_NUMBER_CMIU_PACKET_HEADER             = 1,
    /** Indicates CMIU configuration data */
    E_TAG_NUMBER_CMIU_CONFIGURATION             = 2,
    /** Indicates a response string from the cellular network operator */
    E_TAG_NUMBER_NETWORK_OPERATORS              = 3,
    /** Indicates a response string containing the cellular network performance information */
    E_TAG_NUMBER_NETWORK_PERFORMANCE            = 4,
    /** Indicates a response string containing the cellular network registration status */
    E_TAG_NUMBER_REGISTRATION_STATUS            = 5,
    /** Indicates a response string containing the hosts IP address */
    E_TAG_NUMBER_HOST_ADDRESS                   = 6,
    /** Indicates a response string containing the IP address of the fallback host */
    E_TAG_NUMBER_FALLBACK_HOST_ADDRESS          = 7,
    /** Indicates a response string containing the CMIUs IP address */
    E_TAG_NUMBER_ASSIGNED_CMIU_ADDRESS          = 8,
    /** Indicates a response string containing the FTP DNS address */
    E_TAG_NUMBER_FTP_DNS_ADDRESS                = 9,
    /** Indicates a response uint16_t containing the FTP port number */
    E_TAG_NUMBER_FTP_PORT_NUMBER                = 10,
    /** Indicates a response string containing the FTP user name */
    E_TAG_NUMBER_FTP_USERNAME                   = 11,
    /** Indicates a response string containing the FTP password */
    E_TAG_NUMBER_FTP_PASSWORD                   = 12,
    /** Tags 13 and 14 are currently undefined */
    /** Indicates a response string containing the modems hardware revision  */
    E_TAG_NUMBER_MODEM_HARDWARE_REVISION        = 15,
    /** Indicates a response string containing the modems ID code */
    E_TAG_NUMBER_MODEM_MODEL_IDENTIFICATION_CODE= 16,
    /** Indicates a response string containing manufacturers ID code */
    E_TAG_NUMBER_MODEM_MANUFACTURER_ID_CODE     = 17,
    /** Indicates a response string containing the modems software revision */
    E_TAG_NUMBER_MODEM_SOFTWARE_REVISION        = 18,
    /** Indicates a response string containing modems IMEI number */
    E_TAG_NUMBER_IMEI                           = 19,
    /** Indicates a response string containing SIMs IMSI number */
    E_TAG_NUMBER_SIM_IMSI                       = 20,
    /** Indicates a response string containing SIM cards ID number */
    E_TAG_NUMBER_SIM_CARD_ID                    = 21,
    /** Indicates a response string containing PIN/PUK/PUK2 status of the device */
    E_TAG_NUMBER_PINPUKPUK2_REQUEST_STATUS      = 22,
    /** Indicates the CMIUs HW diagnostics information */
    E_TAG_NUMBER_CMIU_HW_DIAGNOSTICS            = 23,
    /** Indicates a response string containing the ID of the last BLE user */
    E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_ID     = 24,
    /** Indicates the date and time the last BLE user logged in */
    E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_DATE   = 25,
    /** Indicates CMIU flags */
    E_TAG_NUMBER_CMIU_FLAGS                     = 26,
    /** Indicates the the current device configuration settings */
    E_TAG_NUMBER_DEVICE_CONFIGURATION           = 27,
    /** Indicates R900 regular interval readings data */
    E_TAG_NUMBER_R900_INTERVAL_DATA             = 28,
    /** Indicates an event */
    E_TAG_NUMBER_EVENT                          = 29,
    /** Indicates a response string containing any error log messages */
    E_TAG_NUMBER_ERROR_LOG                      = 30,
    /** Indicates the connection timing log data */
    E_TAG_NUMBER_CONNECTION_TIMING_LOG          = 31,
    /** Indicates a command or configuration setting */
    E_TAG_NUMBER_COMMAND                        = 32,
    /**   */
    E_TAG_NUMBER_CMIU_BASIC_CONFIGURATION       = 33, //TODO - remove duplicate data in #2
    /**   */
    E_TAG_NUMBER_INTERVAL_RECORDING             = 34, //TODO - remove this as it is a duplicate of 27
    /** Indicates the packet header for UART transfers */
    E_TAG_NUMBER_UART_PACKET_HEADER             = 35,
    /** Indicates the current time and date */
    E_TAG_NUMBER_CURRENT_TIME                   = 36,
    /** Indicates a MSISDN request (phone number request) */
    E_TAG_NUMBER_MSISDN_REQUEST                 = 37,    
   
    /** Indicates a HW Rev*/
    E_TAG_NUMBER_HARDWARE_REVISION              = 38,    
    /** Indicates a F/W Rev */
    E_TAG_NUMBER_FIRMWARE_REVISION              = 39,   
    /** Indicates a Bootloader Rev */
    E_TAG_NUMBER_BOOTLOADER_REVISION            = 40,
    /** CMIU Config Image revision - operating characteristics*/
    E_TAG_NUMBER_CONFIG_REVISION                = 41,
    /** Indicates a ARB Rev */
    E_TAG_NUMBER_ARB_REVISION                   = 42,
    /** Indicates a Config Rev */
    E_TAG_NUMBER_BLE_CONFIG_REVISION            = 43,
    /** Indicates a CMIU Device Info */
    E_TAG_NUMBER_CMIU_INFORMATION               = 44,
     
    /** Firmware Image name*/
    E_TAG_NUMBER_IMAGE                          = 46,
    /** Firmware Metadata */
    E_TAG_NUMBER_IMAGE_METADATA                 = 47,
    
    /** Recording and Reporting Interval */
    E_TAG_NUMBER_RECORDING_REPORTING_INTERVAL   = 48,

    /** Image revision for the encryption details */
    E_TAG_NUMBER_ENCRYPTION_CONFIG_REVISION     = 49,
    
    /** An 8-byte Unix timestamp */
    E_TAG_NUMBER_TIMESTAMP                      = 50,
    
    /** Error Code */
    E_TAG_NUMBER_ERROR_CODE                     = 51,

	/** Debug Data */
	E_TAG_NUMBER_DEBUG_DATA						= 52,

	/** Command Start Delay */
	E_TAG_NUMBER_COMMAND_STARTDELAY				= 53,

	/** Command Duration */
	E_TAG_NUMBER_COMMAND_DURATION				= 54,

	/** Command Read Connected Device */
	E_TAG_NUMBER_COMMAND_READCONNECTEDEVICE		= 55,

	/** Command Power Mode */
	E_TAG_NUMBER_COMMAND_POWERMODE				= 56,

	/** Command Read Memory */
	E_TAG_NUMBER_COMMAND_MEMORY					 = 57,

	/** Command LTE RF Test Mode */
	E_TAG_NUMBER_COMMAND_RFTESTMODE				= 58,
    
	/** Command Toggle LTE Modem */
	E_TAG_NUMBER_COMMAND_TOGGLELTEMODEM			= 59,
    
	/** R900 - OOK*/
	E_TAG_NUMBER_R900_READINGS_OOK		        = 60,

	/** R900 - FSK*/
	E_TAG_NUMBER_R900_READINGS_FSK		        = 61,
    
	/** Battery V */
	E_TAG_NUMBER_BATTERY_VOLTAGE			    = 62,
    
	/** CMIU Temperature */
	E_TAG_NUMBER_CMIU_TEMPERATURE			    = 63,
    
	/** Memory Image*/
	E_TAG_NUMBER_MEMORY_IMAGE			        = 64,
    
    /** Packet Type */
    E_TAG_NUMBER_PACKET_TYPE                    = 65,
    
    /** Destination */
    E_TAG_NUMBER_DESTINATION                    = 66,

    /** Meta data for packet request command */
    E_TAG_NUMBER_META_DATA_PACKET_REQUEST       = 67,

    
    /** CMIU ID */
    E_TAG_NUMBER_CMIU_ID                        = 68,

    /** Cellular APN */
    E_TAG_NUMBER_CMIU_APN                       = 69,

    /** Command BTLE RF Test Mode */
    E_TAG_NUMBER_COMMAND_BTLE_RFTESTMODE        = 70,

    /** New image version info for Modem FOTA update */
    E_TAG_NUMBER_MODEM_FOTA_VERSION             = 71,

    /** Packet Instigator */
    E_TAG_NUMBER_PACKET_INSTIGATOR              = 72,


    /** Tag used to allow extended tag numbers greater than 255 */
    E_TAG_NUMBER_EXTENSION_256                  = 250,
    /** Indicates a block of encrypted data */
    E_TAG_NUMBER_SECURE_DATA                    = 254,
    /** Indicates the end of a packet */
    E_TAG_NUMBER_EOF                            = 255,
    E_TAG_NUMBER_COUNT
} E_TAG_NUMBER;

     

/**
* Enum of the possible types of data in a tag  (must be <= 255) 
*/
typedef enum E_TAG_TYPE
{
    /** No tag type, such as the EOF tag */
    E_TAG_TYPE_NONE                 = 0x00,
    /** A Uint8 value follows */
    E_TAG_TYPE_BYTE                 = 0x01,
    /** A Uint16 value follows */
    E_TAG_TYPE_U16                  = 0x02,
    /** A Uint32 value follows */
    E_TAG_TYPE_U32                  = 0x03,
    /** A Uint64 value follows */
    E_TAG_TYPE_U64                  = 0x04,
    /** A struct based on an array of Uint8 values follows */
    E_TAG_TYPE_BYTE_RECORD          = 0x05,
    /** An array of Uint16 values follows */
    E_TAG_TYPE_U16_ARRAY            = 0x06,
    /** An array of Uint32 values follows */
    E_TAG_TYPE_U32_ARRAY            = 0x07,
    /** An array of chars follows */
    E_TAG_TYPE_CHAR_ARRAY           = 0x08,
    /** A struct based on an extended array of Uint8 values follows */
    E_TAG_TYPE_EXTENDED_BYTE_RECORD = 0x09,
    /** An extended array of Uint16 values follows */
    E_TAG_TYPE_EXTENDED_U16_ARRAY   = 0x0A,
    /** An extended array of Uint32 values follows */
    E_TAG_TYPE_EXTENDED_U32_ARRAY   = 0x0B,
    /** An extended array of chars follows */
    E_TAG_TYPE_EXTENDED_CHAR_ARRAY  = 0x0C,
    /** An extended array of secure encrypted data follows */
    E_TAG_TYPE_EXTENDED_SECURE_BLOCK_ARRAY = 0x0D,
    /** 10 bytes of image version info follows -
    major (1 byte), minor (1 byte), date (4 bytes), rev (4 bytes) as big-endian BCD */
    E_TAG_TYPE_IMAGE_VERSION_INFO   = 0x0E,
    /** An array of Uint8 values follows */
    E_TAG_TYPE_U8_ARRAY             = 0x0F,
    /** An extended array of Uint8 values follows */
    E_TAG_TYPE_EXTENDED_U8_ARRAY    = 0x10,
    /** An array of 16-byte secure encrypted data blocks follows */
    E_TAG_TYPE_SECURE_BLOCK_ARRAY   = 0x11,

    /* R900 Readings */ 
    E_TAG_TYPE_R900_OOK_READING_ARRAY       = 0x12,
    E_TAG_TYPE_R900_FSK_READING_ARRAY       = 0x13,

    E_TAG_TYPE_U64_ARRAY            = 0x14, // Added by DEW. Awaiting guidance from DH




    E_TAG_TYPE_COUNT
} E_TAG_TYPE;


/**
Command IDs used in tag 32
*/
typedef enum E_COMMAND
{
    /** 0 Update image/s */
    E_COMMAND_UPDATE_IMAGE       = 0x00,
    /** 1	Reboot CMIU */
    E_COMMAND_REBOOT             = 0x01,
    /** 2	Mag swipe Emulation */
    E_COMMAND_MAGSWIPE           = 0x02,
    /** 3	Enter Sleep state */
    E_COMMAND_SLEEP              = 0x03,
    /** 4	Enter normal operating mode from sleep state */
    E_COMMAND_WAKE               = 0x04,  // no longer in spec
    /** 5	Enter operation mode */
    E_COMMAND_ENTER_OPERATION    = 0x05,
    /** 6	Erase indicated memory image */
    E_COMMAND_ERASE_MEMORY_IMAGE  = 0x06,
    /** 7	Set MQTT Broker Address */
    E_COMMAND_SET_BROKER_ADDRESS = 0x07,    // no longer in spec
    /** 8	Set Fallback MQTT Broker Address */
    E_COMMAND_SET_FALLBACK_BROKER_ADDRESS              = 0x08,  // no longer in spec
    /** 9	Set Current Time & Date */
    E_COMMAND_SET_CLOCK          = 0x09,
    /** 10	Set the Recording and Reporting Intervals */
    E_COMMAND_SET_RECORDING_AND_REPORTING_INTERVALS		= 0x0A,
    /** 11	Publish Requested Packet */
    E_COMMAND_PUBLISH_REQUESTED_PACKET                  = 0x0B,
    /** 12	Publish Basic Config Packet */
    E_COMMAND_PUBLISH_BASIC_CONFIG_PACKET				= 0x0C,  // no longer in spec
    /** 13	Publish Interval Data Packet */
    E_COMMAND_PUBLISH_INTERVAL_PACKET					= 0x0D,  // no longer in spec
	/** 14	Read Connected Devices */
	E_COMMAND_READ_CONNECTED_DEVICES					= 0x0E,
	/** 15	Read Device Memory */
	E_COMMAND_READ_MEMORY								= 0x0F,
	/** 16	Sleep for X seconds */
	E_COMMAND_SLEEP_SECONDS								= 0x10,
	/** 17	Power Mode */
	E_COMMAND_SELECT_POWER_MODE							= 0x11,
	/** 18	AT Passthrough */
	E_COMMAND_AT_PASSTHROUGH							= 0x12,
	/** 19	VSense */
	E_COMMAND_VSENSE_MEASUREMENT						= 0x13,
	/** 20	Carrier Assert */
	E_COMMAND_LTE_CARRIER_ASSERT						= 0x14,
	/** 21	LTE RX Test Mode */
	E_COMMAND_LTE_RX_TEST_MODE							= 0x15,
	/** 22	Get CAN+ Data */
	E_COMMAND_GET_CAN_DATA								= 0x16,
	/** 23	Set Micro EM */
	E_COMMAND_SET_MICRO_EM								= 0x17,
	/** 24	BTLE Advertising */
	E_COMMAND_BTLE_ADVERTISING							= 0x18,
	/** 25	BTLE Carrier Assert */
	E_COMMAND_BTLE_CARRIER_ASSERT						= 0x19,
	/** 26	BTLE RX Test Mode */
	E_COMMAND_BTLE_RX_TEST_MODE							= 0x1A,
	/** 27	Assert 32kHz on GPIO */
	E_COMMAND_32KHZ_GPIO								= 0x1B,
	/** 28	Register on LTE Network */
    E_COMMAND_REGISTER_ONLTE_NETWORK                    = 0x1C,
    /** 29	Toggle LTE Modem */
    E_COMMAND_TOGGLE_LTE_MODEM                          = 0x1D,
    /** 30	Get Debug Log */
    E_COMMAND_GET_DEBUG_LOG                             = 0x1E,
    /** 31	Request Current CMIU LTE Signal Quality */
    E_COMMAND_GET_CMIU_SIGNAL_QUALITY                   = 0x1F,
    /** 32	Modem FOTA command */
    E_COMMAND_MODEM_FOTA                                = 0x20,
    /** 33	Request CMIU APN */
    E_COMMAND_REQUEST_APN                               = 0x21,
    /** 34	Update CMIU APN */
    E_COMMAND_UPDATE_APN                                = 0x22,
    /** 35 Run Connectivity Test */
    E_COMMAND_RUN_CONNECTIVITY_TEST                     = 0x23,

    E_COMMAND_COUNT
} E_COMMAND;

/**
Interval format type used in tag 34
*/
typedef enum E_INTERVAL_FORMAT
{
    /** 0 - R900 interval data format. */
    E_R900_INTERVAL_DATA_FORMAT = 0x00,
    /** 1 - Next format (not yet defined). */
    E_UNDEFINED_INTERVAL_FORMAT
} E_INTERVAL_FORMAT;


/**
Image type IDs used in tag 47
*/
typedef enum E_IMAGE_TYPE
{
    /** 0 - Firmware Image */
    E_IMAGE_TYPE_FIRMWARE           =0x00,
    /** 1 - Config Image */
    E_IMAGE_TYPE_CONFIG             =0x01,
    /** 2 - Telit Image */
    E_IMAGE_TYPE_TELIT_MODULE       =0x02,
    /** 3 - BLE config Image */
    E_IMAGE_TYPE_BLE_CONFIG         =0x03,
    /** 4 - ARB config Image */
    E_IMAGE_TYPE_ARB_CONFIG         =0x04,
    /** 5 - Encryption Image */
    E_IMAGE_TYPE_ENCRYPTION         =0x05
} E_IMAGE_TYPE;


/**
Error type IDs used in tag 51
*/
typedef enum E_ERROR_TAG
{
    /** 0 - No Error */
    E_ERROR_NONE                    =0x00,
    /** 1 - Invalid Command*/
    E_ERROR_INVALID_CMD             =0x01
} E_ERROR_TAG;

/**
Power Mode values used in tag 56
*/
typedef enum E_POWER_MODE_BITMASK
{
    /** 1 - Modem */
    E_POWER_MODE_MODEM                  =0x01,
    /** 2 - BTLE Module */
    E_POWER_MODE_BTLE                   =0x02,
    /** 4 - Flash */
    E_POWER_MODE_FLASH                  =0x04,
    /** 8 - Register Interface */
    E_POWER_MODE_REGISTER               =0x08,    
} E_POWER_MODE_BITMASK;

/**
Packet Instigator used in tag 72
*/
typedef enum E_PACKET_INSTIGATOR
{
    /** 0 - Regularly Scheduled Packet */
    E_PACKET_INSTIGATOR_NORMAL = 0x00,
    /** 1 - Connection Vaildation Station (CVS) */
    E_PACKET_INSTIGATOR_CVS = 0x01,
    /** 2 - Final Functional Test Station (FFTS) */
    E_PACKET_INSTIGATOR_FFTS = 0x02
} E_PACKET_INSTIGATOR;

typedef enum E_PACKET_DESTINATION
{
    /** 0 - LTE  */
    E_PACKET_DESTINATION_LTE            =0x00,
    /** 1 - BTLE  */
    E_PACKET_DESTINATION_BTLE           =0x01,
    /** 2 - UART */
    E_PACKET_DESTINATION_UART           =0x02,
} E_PACKET_DESTINATION;

// Size in bytes of the tags, must be packed size
#define S_CMIU_PACKET_HEADER_PACKED_SIZE			(19)
#define S_CMIU_CONFIGURATION_PACKED_SIZE			(73)
#define S_CMIU_DIAGNOSTICS_PACKED_SIZE				(5)
#define S_CONNECTION_TIMING_LOG_PACKED_SIZE			(18)
#define S_CMIU_STATUS_PACKED_SIZE					(1+1+2)             // Tag number 26
#define S_REPORTED_DEVICE_CONFIG_PACKED_SIZE        (1+8+2+4+2)         // tag number 27
#define S_CMIU_BASIC_CONFIGURATION_PACKED_SIZE		(2+1+1+1+2+2+1+1)   // Tag number 33
#define S_INTERVAL_RECORDING_CONFIG_PACKED_SIZE		(1+1+1+1+2+8)       // tag number 34
#define S_MQTT_BLE_UART_PACKED_SIZE					(1+1+1+1)           // tag number 35
#define S_IMAGE_VERSION_INFO_PACKED_SIZE			(1+1+4+4)           // tag type 14
#define S_REVISION_PACKED_SIZE						(10)                // Tag number 38-43 & 49 
#define S_CMIU_INFORMATION_PACKED_SIZE				(8+8+8+1+2+4)       // Tag number 44
#define S_IMAGE_METADATA_PACKED_SIZE				(1+4+4)             // Tag number 47
#define S_READING_RECORDING_PACKED_SIZE				(2+1+1+1+2+2+2+1+1+1)   // Tag number 48 
#define S_COMMAND_READCONNECTEDDEVICE_PACKED_SIZE   (1+8+8)				// Tag number 55 
#define S_COMMAND_MEMORY_PACKED_SIZE                (1+4+2)             // Tag number 57
#define S_COMMAND_RFTESTMODE_PACKED_SIZE			(1+2+1)		        // Tag number 58 // LTE
#define S_COMMAND_RFTESTMODE_BTLE_PACKED_SIZE    	(1+1+1+1)		        // Tag number 70 // BTLE


/**
* Definition of the tag data bytes
*/
typedef struct S_TPBP_TAG
{
    /** The meaning of the tag */
    E_TAG_NUMBER    tagNumber;
    /** If packet is data, then use this */
    E_TAG_TYPE      tagType; 
    /** The number of bytes following this tag */
    uint32_t        dataSize;
} 
S_TPBP_TAG;



/**
* Struct for passing CMIU Packet Header payload to packer (tag num 1)
*/
typedef struct S_CMIU_PACKET_HEADER
{
    uint32_t  cmiuId; 
    uint8_t    sequenceNumber;
    uint8_t    keyInfo;
    uint8_t    encryptionMethod;
    uint8_t    tokenAesCrc;
    uint8_t    networkFlags;
    uint16_t  cellularRssiAndBer;	
    uint64_t  timeAndDate;
} S_CMIU_PACKET_HEADER;


/**
* Struct for passing CMIU Configuration payload to packer (tag num 2)
*/
typedef struct S_CMIU_CONFIGURATION
{	
    uint8_t     cmiuType;	
    uint16_t    hwRevision;
    
    uint16_t    fwRelease;
    uint32_t    fwRevDate;
    uint32_t    fwBuildNum;
    
    uint16_t    blRelease;
    uint32_t    blRevDate;
    uint32_t    blBuildNum;
    
    uint16_t    configRelease;
    uint32_t    configRevDate;
    uint32_t    configBuildNum;
    
    uint64_t    manufactureDate;
    uint16_t    initialCallInTime;	
    uint16_t    callInInterval;
    uint16_t    callInWindow;
    uint8_t     reportingwindowNRetries;
    
    uint16_t    quietTimeStartMins;
    uint16_t    quietTimeEndMins;
    
    uint8_t     numAttachedDevices;
    uint64_t    installationDate;
    uint64_t    dateLastMagSwipe;
    uint8_t     magSwipes;
    uint16_t    batteryRemaining;
    int8_t      timeErrorLastNetworkTimeAccess;
} S_CMIU_CONFIGURATION;


/**
* Struct for passing CMIU Diagnostics payload to packer, (tag number 23)
*/
typedef struct S_CMIU_DIAGNOSTICS
{	
    uint32_t    diagnosticsResult;	
    uint8_t     processorResetCounter;
} S_CMIU_DIAGNOSTICS;

/**
* Struct for Status flags (tag number 26)
*/
typedef struct S_CMIU_STATUS
{
    uint8_t     cmiuType;
    uint8_t     numberOfDevices;
    uint16_t    cmiuFlags;
} S_CMIU_STATUS;


/**
* Struct for Reported Device Config (tag number 27)
*/
typedef struct S_REPORTED_DEVICE_CONFIG
{
    uint8_t     deviceNumber;
    uint64_t    attachedDeviceId;
    uint16_t    deviceType;
    uint32_t    currentDeviceData;
    uint16_t    deviceFlags;
} S_REPORTED_DEVICE_CONFIG;


/**
* Struct for passing connection timing log payload to packer (tag num 31)
*/
typedef struct S_CONNECTION_TIMING_LOG
{
    uint64_t dateOfConnection;
    uint16_t timeFromPowerOnToNetworkRegistration;
    uint16_t timeFromNetworkRegistrationToContextActivation;
    uint16_t timeFromContextActivationToServerConnection;
    uint16_t timeToTransferMessageToServer;
    uint16_t timeToDisconnectAndShutdown;
} S_CONNECTION_TIMING_LOG;


/**
* Struct for passing CMIU Basic Configuration payload to packer (tag num 33)
*/
typedef struct S_CMIU_BASIC_CONFIGURATION
{	
    uint16_t    initialCallInTime;	
    uint8_t     callInInterval;
    uint8_t     callInWindow;
    uint8_t     reportingwindowNRetries;
    uint16_t    quietTimeStartMins;
    uint16_t    quietTimeEndMins;    
    uint8_t     numAttachedDevices;
    uint8_t     eventMask;
} S_CMIU_BASIC_CONFIGURATION;


/**
* Struct for INTERVAL_RECORDING_CONFIG (tag number 34)
*/
typedef struct S_INTERVAL_RECORDING_CONFIG
{
    /** The connected device associated with the packet. Will be '1' until
     * we support multiple devices. */
    uint8_t     deviceNumber;
    /** We only have one format, 0 (normal R900 packets). */
    uint8_t     intervalFormat;
    /** How often the reading is stored in the datalog. */
    uint8_t     recordingIntervalMin;
    /** How often the datalog records are sent to the server. */
    uint8_t     reportingIntervalHrs;
    /** Offset from midnight in UTC. Not clearly defined yet. */
    uint16_t    intervalStartTime;
    /** Timestamp of most recent reading in this packet. Normally this
     * is also the most recent reading. */
    uint64_t    intervalLastRecordDateTime;
} S_INTERVAL_RECORDING_CONFIG;


/**
* Struct for MQTT_BLE_UART (tag number 35)
*/
typedef struct S_MQTT_BLE_UART
{
    uint8_t     sequenceNumber;
    uint8_t     keyInfo;
    uint8_t     encryptionMethod;
    uint8_t     token;
} S_MQTT_BLE_UART;
 

/**
* Struct for passing CMIU Info (tag num 44)
*/
typedef struct S_CMIU_INFORMATION
{
    uint64_t    manufactureDate;
    uint64_t    installationDate;
    uint64_t    dateLastMagSwipe;
    uint8_t     magSwipes;
    uint16_t    batteryRemaining;
    int32_t     timeErrorLastNetworkTimeAccess;
} S_CMIU_INFORMATION;


/**
* Struct for IMAGE_METADATA (tag number 47)
*/
typedef struct S_IMAGE_METADATA
{
    E_IMAGE_TYPE imageType;
    uint32_t    startAddress;    
    uint32_t    imageSize;
} S_IMAGE_METADATA;


/**
* Struct for passing Recording Reporting Interval payload to packer (tag num 48)
*/
typedef struct S_RECORDING_REPORTING_INTERVAL
{	
    uint16_t    reportingStartMins;	
    uint8_t     reportingIntervalHours;
    uint8_t     reportingRetries;
    uint8_t     reportingTransmitWindowsMins;
    uint16_t    reportingQuietStartMins;
    uint16_t    reportingQuietEndMins;
    uint16_t    recordingStartMins;
    uint8_t     recordingIntervalMins;
    uint8_t     recordingNumberAttachedDevices;
    uint8_t     recordingEventMaskForDevice;
} S_RECORDING_REPORTING_INTERVAL;

/**
* Struct for passing device reading payload to packer (tag num 55)
*/
typedef struct S_COMMAND_READCONNECTEDEVICE
{
	uint8_t     pulse;
	uint64_t    delayBetweenReadsMsec;
	uint64_t    numberOfRepeats;
} S_COMMAND_READCONNECTEDEVICE;

/**
* Struct for passing memory payload to packer (tag num 57)
*/
typedef struct S_COMMAND_MEMORY
{
    uint8_t     memoryDesignator;
    uint32_t    startAddress;
    uint16_t    numberOfBytes;
} S_COMMAND_MEMORY;

/**
* Struct for passing LTE rf test mode payload to packer (tag num 58)
*/
typedef struct S_COMMAND_RFTESTMODE
{
	uint8_t    rfBand;
	uint16_t   rfChannel;
	uint8_t    rfPowerLevel;
} S_COMMAND_RFTESTMODE;


/**
* Struct for E_TAG_TYPE_IMAGE_VERSION_INFO (tag *type* 14), tag numbers 38-43, 49 
*/
typedef struct S_IMAGE_VERSION_INFO
{
    /** Major version as binary coded decimal - e.g. version fifteen = 0x15 */
    uint8_t     versionMajorBcd;

    /** Minor version as binary coded decimal - e.g. version sixteen = 0x16 */
    uint8_t     versionMinorBcd;    

    /** Date as binary coded decimal - e.g. 0x150619 for June 19, 2015 */
    uint32_t    versionYearMonthDayBcd;

    /** Build number as binary coded decimal - e.g. 0x1234 for one thousand two hundred thirty four*/
    uint32_t    versionBuildBcd;

} S_IMAGE_VERSION_INFO;


/**
* Struct for passing BTLE rf test mode payload to packer (tag num 70)
*/
typedef struct S_COMMAND_BTLE_RFTESTMODE
{
    uint8_t    rfCommand;
    uint8_t    rfFrequency;
    uint8_t    rfPacketLength;
    uint8_t    rfPacketType;
} S_COMMAND_BTLE_RFTESTMODE;


/*===========================================================================*/
/*  M A C R O S                                                              */
/*===========================================================================*/


#define PACK_U64(p,n)   \
                        do {\
                        Tpbp_Pack((p), (uint32_t)(n>> 0), 4);\
                        Tpbp_Pack((p), (uint32_t)(n>>32), 4);\
                        } while(0)

#define PACK_I8(p,n)  (Tpbp_Pack((p), (uint32_t)(n), 1))
#define PACK_I16(p,n) (Tpbp_Pack((p), (uint32_t)(n), 2))
#define PACK_I32(p,n) (Tpbp_Pack((p), (uint32_t)(n), 4))

#define PACK_U8(p,n)  (Tpbp_Pack((p), (uint32_t)(n), 1))
#define PACK_U16(p,n) (Tpbp_Pack((p), (uint32_t)(n), 2))
#define PACK_U32(p,n) (Tpbp_Pack((p), (uint32_t)(n), 4))

#define UNPACK_U8(p)  ((uint8_t)Tpbp_Unpack((p), 1))
#define UNPACK_U16(p) ((uint16_t)Tpbp_Unpack((p), 2))
#define UNPACK_U32(p) ((uint32_t)Tpbp_Unpack((p),  4))
#define UNPACK_U64(p) (Tpbp_Unpack64((p)))

#define UNPACK_I8(p)  ((int8_t)Tpbp_Unpack((p), 1))
#define UNPACK_I16(p) ((int16_t)Tpbp_Unpack((p), 2))
#define UNPACK_I32(p) ((int32_t)Tpbp_Unpack((p),  4))
#define UNPACK_I64(p) ((int64_t)Tpbp_Unpack64((p)))

/** 
* Macro pair to wrap a pack block. Does initial tag and checks for space
*/
#define PACK_BEGIN(tagNum, tagType, size)\
    bool isOk = Tpbp_PackerHelper( pPacker, (tagNum), (tagType), (size));\
    if(isOk == true){

#define PACK_END\
    } return isOk;

/** 
* Macro pair to wrap an unpack block. Checks for enough data available.
* Asserts if incorrect number of bytes written
*/
#define UNPACK_BEGIN(size)\
    bool isOk = false; uint32_t sz = (size);\
    uint32_t available = Tpbp_ParserGetBytesRemaining(pParser);\
    if (available >= (size)){isOk = true;

#define UNPACK_END\
    ASSERT((available - Tpbp_ParserGetBytesRemaining(pParser)) == sz);\
    } return isOk;


/**
* Packer macro for the CMIU Packet Header type
*/
#define PACK_CMIU_PACKET_HEADER(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_CMIU_PACKET_HEADER,\
            E_TAG_TYPE_BYTE_RECORD,\
            S_CMIU_PACKET_HEADER_PACKED_SIZE)\
    PACK_U32((p),   (pkt)->cmiuId);\
    PACK_U8((p),    (pkt)->sequenceNumber );\
    PACK_U8((p),    (pkt)->keyInfo);\
    PACK_U8((p),    (pkt)->encryptionMethod);\
    PACK_U8((p),    (pkt)->tokenAesCrc);\
    PACK_U8((p),    (pkt)->networkFlags);\
    PACK_U16((p),   (pkt)->cellularRssiAndBer);\
    PACK_U64((p),   (pkt)->timeAndDate);\
PACK_END;
/**
* Unpacker macro for the CMIU Packet Header TAG type
*/
#define UNPACK_CMIU_PACKET_HEADER(p, pkt)\
UNPACK_BEGIN(S_CMIU_PACKET_HEADER_PACKED_SIZE)\
    (pkt)->cmiuId             = UNPACK_U32(p);\
    (pkt)->sequenceNumber     = UNPACK_U8(p);\
    (pkt)->keyInfo            = UNPACK_U8(p);\
    (pkt)->encryptionMethod   = UNPACK_U8(p);\
    (pkt)->tokenAesCrc        = UNPACK_U8(p);\
    (pkt)->networkFlags       = UNPACK_U8(p);\
    (pkt)->cellularRssiAndBer = UNPACK_U16(p);\
    (pkt)->timeAndDate        = UNPACK_U64(p);\
UNPACK_END;


/**
* Packer macro for the CMIU Configuration TAG type (tag number 2)
*/
#define PACK_CMIU_CONFIGURATION(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_CMIU_CONFIGURATION,\
            E_TAG_TYPE_BYTE_RECORD,\
            S_CMIU_CONFIGURATION_PACKED_SIZE)\
\
    PACK_U8 ((p),   (pkt)->cmiuType);\
    PACK_U16((p),   (pkt)->hwRevision);\
    PACK_U16((p),   (pkt)->fwRelease);\
    PACK_U32((p),   (pkt)->fwRevDate);\
    PACK_U32((p),   (pkt)->fwBuildNum);\
    PACK_U16((p),   (pkt)->blRelease);\
    PACK_U32((p),   (pkt)->blRevDate);\
    PACK_U32((p),   (pkt)->blBuildNum);\
    PACK_U16((p),   (pkt)->configRelease);\
    PACK_U32((p),   (pkt)->configRevDate);\
    PACK_U32((p),   (pkt)->configBuildNum);\
    PACK_U64((p),   (pkt)->manufactureDate);\
    PACK_U16((p),   (pkt)->initialCallInTime);\
    PACK_U16((p),   (pkt)->callInInterval );\
    PACK_U16((p),   (pkt)->callInWindow);\
    PACK_U8((p),    (pkt)->reportingwindowNRetries);\
    PACK_U16((p),   (pkt)->quietTimeStartMins);\
    PACK_U16((p),   (pkt)->quietTimeEndMins);\
    PACK_U8((p),    (pkt)->numAttachedDevices);\
    PACK_U64((p),   (pkt)->installationDate);\
    PACK_U64((p),   (pkt)->dateLastMagSwipe);\
    PACK_U8((p),    (pkt)->magSwipes);\
    PACK_U16((p),   (pkt)->batteryRemaining);\
    PACK_I8((p),    (pkt)->timeErrorLastNetworkTimeAccess);\
PACK_END;
/**
* Unpacker macro for the CMIU Configuration type (tag number 2)
*/
#define UNPACK_CMIU_CONFIGURATION(p, pkt)\
UNPACK_BEGIN(S_CMIU_CONFIGURATION_PACKED_SIZE)\
    (pkt)->cmiuType                             = UNPACK_U8(p);\
    (pkt)->hwRevision                           = UNPACK_U16(p);\
    (pkt)->fwRelease                            = UNPACK_U16(p);\
    (pkt)->fwRevDate                            = UNPACK_U32(p);\
    (pkt)->fwBuildNum                           = UNPACK_U32(p);\
    (pkt)->blRelease                            = UNPACK_U16(p);\
    (pkt)->blRevDate                            = UNPACK_U32(p);\
    (pkt)->blBuildNum                           = UNPACK_U32(p);\
    (pkt)->configRelease                        = UNPACK_U16(p);\
    (pkt)->configRevDate                        = UNPACK_U32(p);\
    (pkt)->configBuildNum                       = UNPACK_U32(p);\
    (pkt)->manufactureDate                      = UNPACK_U64(p);\
    (pkt)->initialCallInTime                    = UNPACK_U16(p);\
    (pkt)->callInInterval                       = UNPACK_U16(p);\
    (pkt)->callInWindow                         = UNPACK_U16(p);\
    (pkt)->reportingwindowNRetries              = UNPACK_U8(p);\
    (pkt)->quietTimeStartMins                   = UNPACK_U16(p);\
    (pkt)->quietTimeEndMins                     = UNPACK_U16(p);\
    (pkt)->numAttachedDevices                   = UNPACK_U8(p);\
    (pkt)->installationDate                     = UNPACK_U64(p);\
    (pkt)->dateLastMagSwipe                     = UNPACK_U64(p);\
    (pkt)->magSwipes                            = UNPACK_U8(p);\
    (pkt)->batteryRemaining                     = UNPACK_U16(p);\
    (pkt)->timeErrorLastNetworkTimeAccess       = UNPACK_U8(p);\
UNPACK_END


/**
* Packer macro for the CMIU Diagnostic TAG (tag number 23)
*/
#define PACK_CMIU_DIAGNOSTICS(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_CMIU_HW_DIAGNOSTICS,\
            E_TAG_TYPE_BYTE_RECORD,\
            S_CMIU_DIAGNOSTICS_PACKED_SIZE)\
    PACK_U32((p), (pkt)->diagnosticsResult);\
    PACK_U8((p),  (pkt)->processorResetCounter);\
PACK_END;
/**
* Unpacker macro for the CMIU Diagnostic TAG (tag number 23)
*/
#define UNPACK_CMIU_DIAGNOSTICS(p, pkt)\
UNPACK_BEGIN(S_CMIU_DIAGNOSTICS_PACKED_SIZE)\
\
    (pkt)->diagnosticsResult                = UNPACK_U32(p);\
    (pkt)->processorResetCounter            = UNPACK_U8(p);\
UNPACK_END;


/**
* Packer macro for the CMIU STATUS FLAGS TAG (tag number 26)
*/
#define PACK_CMIU_STATUS(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_CMIU_FLAGS,\
    E_TAG_TYPE_BYTE_RECORD,\
    S_CMIU_STATUS_PACKED_SIZE)\
\
    PACK_U8((p),                (pkt)->cmiuType);\
    PACK_U8((p),                (pkt)->numberOfDevices);\
    PACK_U16((p),               (pkt)->cmiuFlags);\
PACK_END;
/**
* Unpacker macro for the CMIU STATUS FLAGS TAG (tag number 26)
*/
#define UNPACK_CMIU_STATUS(p, pkt)\
UNPACK_BEGIN (S_CMIU_STATUS_PACKED_SIZE)\
\
    (pkt)->cmiuType             = UNPACK_U8(p);\
    (pkt)->numberOfDevices      = UNPACK_U8(p);\
    (pkt)->cmiuFlags            = UNPACK_U16(p);\
UNPACK_END;


/**
* Packer macro for the REPORTED DEVICE CONFIG TAG (tag number 27)
*/
#define PACK_DEVICE_CONFIGURATION(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_DEVICE_CONFIGURATION,\
    E_TAG_TYPE_BYTE_RECORD,\
    S_REPORTED_DEVICE_CONFIG_PACKED_SIZE)\
\
    PACK_U8((p),                (pkt)->deviceNumber);\
    PACK_U64((p),               (pkt)->attachedDeviceId);\
    PACK_U16((p),               (pkt)->deviceType);\
    PACK_U32((p),               (pkt)->currentDeviceData);\
    PACK_U16((p),               (pkt)->deviceFlags);\
PACK_END;
/**
* Unpacker macro for the REPORTED DEVICE CONFIG TAG (tag number 27)
*/
#define UNPACK_DEVICE_CONFIGURATION(p, pkt)\
UNPACK_BEGIN (S_REPORTED_DEVICE_CONFIG_PACKED_SIZE)\
\
    (pkt)->deviceNumber         = UNPACK_U8(p);\
    (pkt)->attachedDeviceId     = UNPACK_U64(p);\
    (pkt)->deviceType           = UNPACK_U16(p);\
    (pkt)->currentDeviceData    = UNPACK_U32(p);\
    (pkt)->deviceFlags          = UNPACK_U16(p);\
UNPACK_END;


/**
* Packer macro for the Connection Timing Log TAG (tag number 31)
*/
#define PACK_CONNECTION_TIMING_LOG(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_CONNECTION_TIMING_LOG,\
            E_TAG_TYPE_BYTE_RECORD,\
            S_CONNECTION_TIMING_LOG_PACKED_SIZE)\
    PACK_U64((p),                                   (pkt)->dateOfConnection);\
    PACK_U16((p),                                   (pkt)->timeFromPowerOnToNetworkRegistration);\
    PACK_U16((p),                                   (pkt)->timeFromNetworkRegistrationToContextActivation);\
    PACK_U16((p),                                   (pkt)->timeFromContextActivationToServerConnection);\
    PACK_U16((p),                                   (pkt)->timeToTransferMessageToServer);\
    PACK_U16((p),                                   (pkt)->timeToDisconnectAndShutdown);\
PACK_END;
/**
* Unpacker macro for the Connection Timing Log TAG (tag number 31)
*/
#define UNPACK_CONNECTION_TIMING_LOG(p, pkt)\
UNPACK_BEGIN(S_CONNECTION_TIMING_LOG_PACKED_SIZE)\
    (pkt)->dateOfConnection                                 = UNPACK_U64(p);\
    (pkt)->timeFromPowerOnToNetworkRegistration             = UNPACK_U16(p);\
    (pkt)->timeFromNetworkRegistrationToContextActivation   = UNPACK_U16(p);\
    (pkt)->timeFromContextActivationToServerConnection      = UNPACK_U16(p);\
    (pkt)->timeToTransferMessageToServer                    = UNPACK_U16(p);\
    (pkt)->timeToDisconnectAndShutdown                      = UNPACK_U16(p);\
UNPACK_END;


/**
* Packer macro for the CMIU Basic Configuration TAG type (tag number 33)
*/
#define PACK_CMIU_BASIC_CONFIGURATION(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_CMIU_BASIC_CONFIGURATION,\
            E_TAG_TYPE_BYTE_RECORD,\
            S_CMIU_BASIC_CONFIGURATION_PACKED_SIZE)\
\
    PACK_U16((p),                               (pkt)->initialCallInTime);\
    PACK_U8((p),                                (pkt)->callInInterval );\
    PACK_U8((p),                                (pkt)->callInWindow);\
    PACK_U8((p),                                (pkt)->reportingwindowNRetries);\
    PACK_U16((p),                               (pkt)->quietTimeStartMins);\
    PACK_U16((p),                               (pkt)->quietTimeEndMins);\
    PACK_U8((p),                                (pkt)->numAttachedDevices);\
    PACK_U8((p),                                (pkt)->eventMask);\
PACK_END;
/**
* Unpacker macro for the CMIU Basic Configuration type (tag number 33)
*/
#define UNPACK_CMIU_BASIC_CONFIGURATION(p, pkt)\
UNPACK_BEGIN(S_CMIU_BASIC_CONFIGURATION_PACKED_SIZE)\
\
    (pkt)->initialCallInTime                    = UNPACK_U16(p);\
    (pkt)->callInInterval                       = UNPACK_U8(p);\
    (pkt)->callInWindow                         = UNPACK_U8(p);\
    (pkt)->reportingwindowNRetries              = UNPACK_U8(p);\
    (pkt)->quietTimeStartMins                   = UNPACK_U16(p);\
    (pkt)->quietTimeEndMins                     = UNPACK_U16(p);\
    (pkt)->numAttachedDevices                   = UNPACK_U8(p);\
    (pkt)->eventMask                            = UNPACK_U8(p);\
UNPACK_END;


/**
* Packer macro for the INTERVAL_RECORDING_CONFIG TAG (tag number 34)
*/
#define PACK_INTERVAL_RECORDING_CONFIG(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_INTERVAL_RECORDING,\
    E_TAG_TYPE_BYTE_RECORD,\
    S_INTERVAL_RECORDING_CONFIG_PACKED_SIZE)\
\
    PACK_U8((p),                                (pkt)->deviceNumber);\
    PACK_U8((p),                                (pkt)->intervalFormat);\
    PACK_U8((p),                                (pkt)->recordingIntervalMin);\
    PACK_U8((p),                                (pkt)->reportingIntervalHrs);\
    PACK_U16((p),                               (pkt)->intervalStartTime);\
    PACK_U64((p),                               (pkt)->intervalLastRecordDateTime);\
PACK_END;
/**
* Unpacker macro for the INTERVAL_RECORDING_CONFIG TAG (tag number 34)
*/
#define UNPACK_INTERVAL_RECORDING_CONFIG(p, pkt)\
UNPACK_BEGIN (S_INTERVAL_RECORDING_CONFIG_PACKED_SIZE)\
\
    (pkt)->deviceNumber                         = UNPACK_U8(p);\
    (pkt)->intervalFormat                       = UNPACK_U8(p);\
    (pkt)->recordingIntervalMin                 = UNPACK_U8(p);\
    (pkt)->reportingIntervalHrs                 = UNPACK_U8(p);\
    (pkt)->intervalStartTime                    = UNPACK_U16(p);\
    (pkt)->intervalLastRecordDateTime           = UNPACK_U64(p);\
UNPACK_END;


/**
* Packer macro for the MQTT_BLE_UART TAG type (tag number 35)
*/
#define PACK_MQTT_BLE_UART(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_UART_PACKET_HEADER,\
            E_TAG_TYPE_BYTE_RECORD,\
            S_MQTT_BLE_UART_PACKED_SIZE)\
\
    PACK_U8((p),                                (pkt)->sequenceNumber);\
    PACK_U8((p),                                (pkt)->keyInfo );\
    PACK_U8((p),                                (pkt)->encryptionMethod);\
    PACK_U8((p),                                (pkt)->token);\
PACK_END;
/**
* Unpacker macro for the MQTT_BLE_UART TAG type (tag number 35)
*/
#define UNPACK_MQTT_BLE_UART(p, pkt)\
UNPACK_BEGIN(S_MQTT_BLE_UART_PACKED_SIZE)\
\
    (pkt)->sequenceNumber                       = UNPACK_U8(p);\
    (pkt)->keyInfo                              = UNPACK_U8(p);\
    (pkt)->encryptionMethod                     = UNPACK_U8(p);\
    (pkt)->token                                = UNPACK_U8(p);\
UNPACK_END;


/**
* Packer macro for the CMIU INFORMATION TAG type (tag number 44)
*/
#define PACK_CMIU_INFORMATION(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_CMIU_INFORMATION,\
            E_TAG_TYPE_BYTE_RECORD,\
            S_CMIU_INFORMATION_PACKED_SIZE)\
\
    PACK_U64((p),                               (pkt)->manufactureDate);\
    PACK_U64((p),                               (pkt)->installationDate);\
    PACK_U64((p),                               (pkt)->dateLastMagSwipe);\
    PACK_U8((p),                                (pkt)->magSwipes);\
    PACK_U16((p),                               (pkt)->batteryRemaining);\
    PACK_I32((p),                               (pkt)->timeErrorLastNetworkTimeAccess);\
PACK_END;
/**
* Unpacker macro for the CMIU INFORMATION type (tag number 44)
*/
#define UNPACK_CMIU_INFORMATION(p, pkt)\
UNPACK_BEGIN(S_CMIU_INFORMATION_PACKED_SIZE)\
\
    (pkt)->manufactureDate                      = UNPACK_U64(p);\
    (pkt)->installationDate                     = UNPACK_U64(p);\
    (pkt)->dateLastMagSwipe                     = UNPACK_U64(p);\
    (pkt)->magSwipes                            = UNPACK_U8(p);\
    (pkt)->batteryRemaining                     = UNPACK_U16(p);\
    (pkt)->timeErrorLastNetworkTimeAccess       = UNPACK_I32(p);\
UNPACK_END
 
/**
* Packer macro for the IMAGE METADATA (tag number 47)
*/
#define PACK_IMAGE_METADATA(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_IMAGE_METADATA,\
    E_TAG_TYPE_BYTE_RECORD,\
    S_IMAGE_METADATA_PACKED_SIZE)\
\
    PACK_U8((p),                                (pkt)->imageType);\
    PACK_U32((p),                               (pkt)->startAddress);\
    PACK_U32((p),                               (pkt)->imageSize);\
PACK_END;
/**
* Unpacker macro for the IMAGE METADATA (tag number 47)
*/
#define UNPACK_IMAGE_METADATA(p, pkt)\
UNPACK_BEGIN (S_IMAGE_METADATA_PACKED_SIZE)\
    (pkt)->imageType                            = (E_IMAGE_TYPE)UNPACK_U8(p);\
    (pkt)->startAddress                         = UNPACK_U32(p);\
    (pkt)->imageSize                            = UNPACK_U32(p);\
UNPACK_END;



/**
* Packer macro for the  RECORDING_REPORTING_INTERVAL (tag number 48)
*/
#define PACK_RECORDING_REPORTING_INTERVAL(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_RECORDING_REPORTING_INTERVAL,\
    E_TAG_TYPE_BYTE_RECORD,\
    S_READING_RECORDING_PACKED_SIZE)\
\
    PACK_U16((p),                                   (pkt)->reportingStartMins);\
    PACK_U8((p),                                    (pkt)->reportingIntervalHours);\
    PACK_U8((p),                                    (pkt)->reportingRetries);\
    PACK_U8((p),                                    (pkt)->reportingTransmitWindowsMins);\
    PACK_U16((p),                                   (pkt)->reportingQuietStartMins);\
    PACK_U16((p),                                   (pkt)->reportingQuietEndMins);\
    PACK_U16((p),                                   (pkt)->recordingStartMins);\
    PACK_U8((p),                                    (pkt)->recordingIntervalMins);\
    PACK_U8((p),                                    (pkt)->recordingNumberAttachedDevices);\
    PACK_U8((p),                                    (pkt)->recordingEventMaskForDevice);\
PACK_END;
/**
* Unpacker macro for the RECORDING_REPORTING_INTERVAL (tag number 48)
*/
#define UNPACK_RECORDING_REPORTING_INTERVAL(p, pkt)\
UNPACK_BEGIN (S_READING_RECORDING_PACKED_SIZE)\
    (pkt)->reportingStartMins                      = UNPACK_U16(p);\
    (pkt)->reportingIntervalHours                  = UNPACK_U8(p);\
    (pkt)->reportingRetries                        = UNPACK_U8(p);\
    (pkt)->reportingTransmitWindowsMins            = UNPACK_U8(p);\
    (pkt)->reportingQuietStartMins                 = UNPACK_U16(p);\
    (pkt)->reportingQuietEndMins                   = UNPACK_U16(p);\
    (pkt)->recordingStartMins                      = UNPACK_U16(p);\
    (pkt)->recordingIntervalMins                   = UNPACK_U8(p);\
    (pkt)->recordingNumberAttachedDevices          = UNPACK_U8(p);\
    (pkt)->recordingEventMaskForDevice             = UNPACK_U8(p);\
UNPACK_END;

/**
* Packer macro for the  COMMAND_READCONNECTEDEVICE (tag number 55)
*/
#define PACK_COMMAND_READCONNECTEDDEVICE(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_COMMAND_READCONNECTEDEVICE,\
    E_TAG_TYPE_BYTE_RECORD,\
    S_COMMAND_READCONNECTEDDEVICE_PACKED_SIZE)\
\
    PACK_U8((p),                                    (pkt)->pulse);\
    PACK_U64((p),                                   (pkt)->delayBetweenReadsMsec);\
    PACK_U64((p),                                   (pkt)->numberOfRepeats);\
PACK_END;
/**
* Unpacker macro for the COMMAND_READCONNECTEDEVICE (tag number 55)
*/
#define UNPACK_COMMAND_READCONNECTEDDEVICE(p, pkt)\
UNPACK_BEGIN (S_COMMAND_READCONNECTEDDEVICE_PACKED_SIZE)\
    (pkt)->pulse									= UNPACK_U8(p);\
    (pkt)->delayBetweenReadsMsec                    = UNPACK_U64(p);\
    (pkt)->numberOfRepeats				            = UNPACK_U64(p);\
UNPACK_END;

/**
* Packer macro for the COMMAND_MEMORY (tag number 57)
*/
#define PACK_COMMAND_MEMORY(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_COMMAND_MEMORY,\
    E_TAG_TYPE_BYTE_RECORD,\
    S_COMMAND_MEMORY_PACKED_SIZE)\
\
    PACK_U8((p),                                    (pkt)->memoryDesignator);\
    PACK_U32((p),                                   (pkt)->startAddress);\
    PACK_U16((p),                                   (pkt)->numberOfBytes);\
PACK_END;
/**
* Unpacker macro for the COMMAND_MEMORY (tag number 57)
*/
#define UNPACK_COMMAND_MEMORY(p, pkt)\
UNPACK_BEGIN (S_COMMAND_MEMORY_PACKED_SIZE)\
    (pkt)->memoryDesignator                     = UNPACK_U8(p);\
    (pkt)->startAddress							= UNPACK_U32(p);\
    (pkt)->numberOfBytes                        = UNPACK_U16(p);\
UNPACK_END;

/**
* Packer macro for the COMMAND_RFTESTMODE (tag number 58) LTE
*/
#define PACK_COMMAND_RFTESTMODE(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_COMMAND_RFTESTMODE,\
    E_TAG_TYPE_BYTE_RECORD,\
    S_COMMAND_RFTESTMODE_PACKED_SIZE)\
\
    PACK_U8((p),                                   (pkt)->rfBand);\
    PACK_U16((p),                                  (pkt)->rfChannel);\
	PACK_U8((p),                                   (pkt)->rfPowerLevel);\
PACK_END;
/**
* Unpacker macro for the COMMAND_RFTESTMODE (tag number 58) LTE
*/
#define UNPACK_COMMAND_RFTESTMODE(p, pkt)\
UNPACK_BEGIN (S_COMMAND_RFTESTMODE_PACKED_SIZE)\
    (pkt)->rfBand   							= UNPACK_U8(p);\
    (pkt)->rfChannel							= UNPACK_U16(p);\
	(pkt)->rfPowerLevel							= UNPACK_U8(p);\
UNPACK_END;

/**
* Packer macro for the COMMAND_BTLE_RFTESTMODE (tag number 70) BTLE
*/
#define PACK_COMMAND_BTLE_RFTESTMODE(p, pkt)\
PACK_BEGIN (E_TAG_NUMBER_COMMAND_BTLE_RFTESTMODE,\
    E_TAG_TYPE_BYTE_RECORD,\
    S_COMMAND_RFTESTMODE_BTLE_PACKED_SIZE)\
\
    PACK_U8((p),                                   (pkt)->rfCommand);\
    PACK_U8((p),                                   (pkt)->rfFrequency);\
    PACK_U8((p),                                   (pkt)->rfPacketLength);\
	PACK_U8((p),                                   (pkt)->rfPacketType);\
PACK_END;

/**
* Unpacker macro for the COMMAND_BTLE_RFTESTMODE (tag number 70) BTLE
*/
#define UNPACK_COMMAND_BTLE_RFTESTMODE(p, pkt)\
UNPACK_BEGIN (S_COMMAND_RFTESTMODE_BTLE_PACKED_SIZE)\
    (pkt)->rfCommand  						    = UNPACK_U8(p);\
    (pkt)->rfFrequency  						= UNPACK_U8(p);\
    (pkt)->rfPacketLength						= UNPACK_U8(p);\
	(pkt)->rfPacketType							= UNPACK_U8(p);\
UNPACK_END;

#endif

/**
 * @}
 */
