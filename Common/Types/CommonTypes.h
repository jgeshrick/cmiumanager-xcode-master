/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @addtogroup  TPBP
 * @{
 */


/**
 * @file
 *
 * @brief  Common type definitions 
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 */


#ifndef __COMMON_TYPES_H__
#define __COMMON_TYPES_H__


#include <assert.h>
#include <stdint.h>
 
//
// Compiler specific typedefs
//
#if TARGET_OS_IPHONE
    #include <stdbool.h>

    //Target CPU is 1ms tick. Used for integration test
    #define TICK_CLOCKS_PER_SEC 1000

    // No exported DLL for iOS
    #define DLL_EXPORT

    #define ASSERT(expr) assert(expr)

#elif __arm__
    // For embedded hardware typedefs and macros i.e. Keil / EFM32 target hardware
    
    #include <stdbool.h>
	    
    //Target CPU is 1ms tick. Used for integration test
    #define TICK_CLOCKS_PER_SEC 1000   

    // No exported DLL for embedded systems
    #define DLL_EXPORT

    // Assert macro for a specific embedded target  
    // Typically this may be used to output a string over a debug port
    // and terminate the embedded app gracefully.
    // See http://www.keil.com/support/man/docs/armlib/armlib_chr1358938924063.htm
     //                                  //  This line removed to allow build on Keil assert(0);  
    #define ASSERT(expr) do {                       \
                                if(!(expr))         \
                                {                   \
                                }                   \
                            } while(0)

    static uint32_t strnlen(const char *s, uint32_t maxlen) //seems this is not defined in ARM land
    {
        uint32_t cp;

        for (cp = 0u; cp < maxlen && s[cp] != (char)0; cp++)
        {
        }

        return cp;
    }

#elif _WIN32 
    //
    // MSVC2010
    // C99 (bool type) not supported on MSVC 2010
    typedef unsigned int bool;  
    #define true  1
    #define false 0
    
    // See clock.h :  on win32 PC is 1ms tick. Used for integration test
    #define TICK_CLOCKS_PER_SEC 1000   

    // Allows optional export of functions via Windows DLL
    #define DLL_EXPORT __declspec(dllexport)

    #define ASSERT(expr) do {                       \
                                if(!(expr))         \
                                {                   \
                                    assert(0);      \
                                }                   \
                            } while(0)
#else
    #error "Must define target platform"
#endif



// During testing we want to access static functions
#ifdef UNITY
    #define STATIC 
#else
    #define STATIC static
#endif


#define OK_STATUS   0
#define ECODE_BAD  -1

typedef union UU16
{
   uint16_t U16;
   int16_t S16;
   uint8_t U8[2];
   int8_t S8[2];
} UU16;

typedef union UU32
{
   uint32_t U32;
   int32_t S32;
   UU16 UU16[2];
   uint16_t U16[2];
   int16_t S16[2];
   uint8_t U8[4];
   int8_t S8[4];
} UU32;



typedef union UU64
{
   uint64_t U64;
   uint32_t U32[2];
   UU32 UU32[2];
   uint16_t U16[4];
   uint8_t U8[8];
} UU64;

#endif // __COMMON_TYPES_H__

/**
 * @}
*/
