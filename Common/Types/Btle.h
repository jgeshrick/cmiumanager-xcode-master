/* ****************************************************************************
******************************************************************************
**
**         Filename: Btle.h
**    
**          Author: Duncan Willis
**          Created: 6 May 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
 

/*******************************************************************************
 *
 * BTLE Common Definitions.
 *
 * This header has no corresponding class.  It is used across multiple
 * platforms, and must accomodate multiple languages : C, C++ and C#.
 *
 *                                                    VVVVVVVVVVVVVVVVV
 *                                                    VVVVVVVVVVVVVVVVV
 * This leads to some apparantly obscure statements.  EDIT WITH CAUTION, and not
 *                                                    ^^^^^^^^^^^^^^^^^
 *                                                    ^^^^^^^^^^^^^^^^^
 * without a good knowledge of all three languages.
 *
 ******************************************************************************/
 
#ifndef BTLE_COMMON_H
#define BTLE_COMMON_H

/*************************************************************************
*  Include Statements
*************************************************************************/

/*************************************************************************
*   Global Constants
*************************************************************************/
#define LITTLE_END(sixteenBits) ((uint8_t)(sixteenBits & 0xFF))
#define BIG_END(sixteenBits) ((uint8_t)(sixteenBits >> 8))
#define SMALLEST_BYTE(thirtyTwoBits)                                           \
          ((uint8_t)(thirtyTwoBits & 0xFF))
#define SMALL_INTERMEDIATE_BYTE(thirtyTwoBits)                                 \
          ((uint8_t)((thirtyTwoBits >> 8) & 0xFF))
#define LARGE_INTERMEDIATE_BYTE(thirtyTwoBits)                                 \
          ((uint8_t)((thirtyTwoBits >> 16) & 0xFF))
#define BIGGEST_BYTE(thirtyTwoBits)                                            \
          ((uint8_t)((thirtyTwoBits >> 24) & 0xFF))
#ifdef BIG_ENDIAN
  #define FIRST_END BIG_END
  #define SECOND_END LITTLE_END
  #define COMBINED_ENDS(f, s) (((uint16_t)f << 8) + s)
  #define FIRST_BYTE BIGGEST_BYTE
  #define SECOND_BYTE LARGE_INTERMEDIATE_BYTE
  #define THIRD_BYTE SMALL_INTERMEDIATE_BYTE
  #define FOURTH_BYTE SMALLEST_BYTE
  #define COMBINED_BYTES(b0, b1, b2, b3)                                       \
       (((uint32_t)b0 << 24) + ((uint32_t)b1 << 16) + ((uint32_t)b2 << 8) + b3)
#else /* LITTLE_ENDIAN, default */
  #define FIRST_END LITTLE_END
  #define SECOND_END BIG_END
  #define COMBINED_ENDS(f, s) (((uint16_t)s << 8) + f)
  #define FIRST_BYTE SMALLEST_BYTE
  #define SECOND_BYTE SMALL_INTERMEDIATE_BYTE
  #define THIRD_BYTE LARGE_INTERMEDIATE_BYTE
  #define FOURTH_BYTE BIGGEST_BYTE
  #define COMBINED_BYTES(b0, b1, b2, b3)                                       \
       (((uint32_t)b3 << 24) + ((uint32_t)b2 << 16) + ((uint32_t)b1 << 8) + b0)
#endif /* BIG_ENDIAN */

/*************************************************************************
*   Global typedefs and enums
*************************************************************************/
typedef enum BLE_CmiuBleCommand_Tag
{
    CMIU_BLE_EG_COMMAND_LOOPTEST = 0x01,
    CMIU_BLE_EG_COMMAND_DELETE_BONDS = 0x02,
}BLE_CmiuBleCommand_t;

typedef enum BLE_CmiuBleResponse_Tag
{
    CMIU_BLE_EG_RESPONSE_LOOPTEST = (CMIU_BLE_EG_COMMAND_LOOPTEST | 0x80),
    CMIU_BLE_EG_RESPONSE_DELETE_BONDS,
}BLE_CmiuBleResponse_t;

typedef enum BLE_CmiuServiceUuid_Tag
{
    BLE_UUID_CMIU_UART_SERVICE = 0x0101,
}BLE_CmiuServiceUuid_t;

typedef enum BLE_CmiuCharacteristicUuid_Tag
{
    BLE_UUID_CMIU_COMMAND_RX_CHAR = 0x0102,
    BLE_UUID_CMIU_RESPONSE_TX_CHAR = 0x0103,
}BLE_CmiuCharacteristicUuid_t;

typedef enum BLE_CmiuBleVendorUuid_Tag
{
    CMIU_BLE_VENDOR_UUID_0 = 0x32,
    CMIU_BLE_VENDOR_UUID_1 = 0x15,
    CMIU_BLE_VENDOR_UUID_2 = 0xBB,
    CMIU_BLE_VENDOR_UUID_3 = 0x45,
    CMIU_BLE_VENDOR_UUID_4 = 0x5F,
    CMIU_BLE_VENDOR_UUID_5 = 0x78,
    CMIU_BLE_VENDOR_UUID_6 = 0x23,
    CMIU_BLE_VENDOR_UUID_7 = 0x15,
    CMIU_BLE_VENDOR_UUID_8 = 0xDE,
    CMIU_BLE_VENDOR_UUID_9 = 0xEF,
    CMIU_BLE_VENDOR_UUID_A = 0x12,
    CMIU_BLE_VENDOR_UUID_B = 0x12,
    CMIU_BLE_VENDOR_UUID_C = 0x00,
    CMIU_BLE_VENDOR_UUID_D = 0x00,
    CMIU_BLE_VENDOR_UUID_E = 0x00,
    CMIU_BLE_VENDOR_UUID_F = 0x00,
}BLE_CmiuBleVendorUuid_t;

/*************************************************************************
*   External Class Prototypes
*************************************************************************/
// None

/*************************************************************************
*   Global (Singleton) Object (if any)
*************************************************************************/
// None

#endif /* BTLE_COMMON_H */
