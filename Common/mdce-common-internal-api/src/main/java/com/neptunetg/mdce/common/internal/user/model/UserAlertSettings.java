/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.user.model;

/**
 * Created by WJD1 on 14/09/2015.
 * Class to hold the alert settings for an individual user
 */
public class UserAlertSettings
{
    private int userId;
    private Boolean systemWarning;
    private Boolean systemError;
    private Boolean cmiuWarning;
    private Boolean cmiuError;
    private String requester;   //who request this change

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public Boolean getSystemWarning()
    {
        return systemWarning;
    }

    public void setSystemWarning(Boolean systemWarning)
    {
        this.systemWarning = systemWarning;
    }

    public Boolean getSystemError()
    {
        return systemError;
    }

    public void setSystemError(Boolean systemError)
    {
        this.systemError = systemError;
    }

    public Boolean getCmiuWarning()
    {
        return cmiuWarning;
    }

    public void setCmiuWarning(Boolean cmiuWarning)
    {
        this.cmiuWarning = cmiuWarning;
    }

    public Boolean getCmiuError()
    {
        return cmiuError;
    }

    public void setCmiuError(Boolean cmiuError)
    {
        this.cmiuError = cmiuError;
    }

    public String getRequester()
    {
        return requester;
    }

    public void setRequester(String requester)
    {
        this.requester = requester;
    }
}
