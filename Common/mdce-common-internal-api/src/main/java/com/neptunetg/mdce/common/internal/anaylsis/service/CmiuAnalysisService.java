/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.internal.anaylsis.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.anaylsis.model.CmiuTimingAnalysisDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 *
 */
@RestController
@RequestMapping("/service")
public interface CmiuAnalysisService
{
    String URL_CMIU_TIMING_LIST = "/internal/analysis/list";

    String PARAM_CONNECTION_THRESHOLD = "connection-threshold";
    String PARAM_CMIU_MODE = "cmiu-mode";
    String PARAM_CMIU_MNO = "cmiu-mno";
    String PARAM_CMIU_STATE = "cmiu-state";
    String PARAM_CMIU_SITE_ID = "cmiu-site-id";

    /**
     * Get list of miu information about connection time.
     *
     * @param connectionThreshold max amount of connect time.
     * @param cmiuMode cmiu mode
     * @param mno network provider
     * @param state lifecycle state
     * @param siteId site id
     * @return list of mius based on conditions
     * @throws InternalApiException
     */
    @RequestMapping(value = URL_CMIU_TIMING_LIST, method = RequestMethod.GET)
    List<CmiuTimingAnalysisDetails> getCmiuTimingInfo(@RequestParam(value = PARAM_CONNECTION_THRESHOLD, required = false) Long connectionThreshold,
                                                      @RequestParam(value = PARAM_CMIU_MODE, required = false) String cmiuMode,
                                                      @RequestParam(value = PARAM_CMIU_MNO, required = false) String mno,
                                                      @RequestParam(value = PARAM_CMIU_STATE, required = false) String state,
                                                      @RequestParam(value = PARAM_CMIU_SITE_ID,  required = false) String siteId) throws InternalApiException;
}
