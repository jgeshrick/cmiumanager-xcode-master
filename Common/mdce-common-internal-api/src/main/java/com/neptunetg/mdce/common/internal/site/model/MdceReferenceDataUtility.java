/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.common.internal.site.model;

/**
 * MdceReferenceDataUtility
 *
 *  "site_id": 32,
 *   "info": {
 *     ...
 *   }
 */
public class MdceReferenceDataUtility
{

    private int siteId;

    private int infoId;

    private MdceReferenceDataInfo info;

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public int getInfoId()
    {
        return infoId;
    }

    public void setInfoId(int infoId)
    {
        this.infoId = infoId;
    }

    public void setInfo(MdceReferenceDataInfo info)
    {
        this.info = info;
    }

    public MdceReferenceDataInfo getInfo()
    {
        return info;
    }

}
