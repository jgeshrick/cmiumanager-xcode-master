/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.sim.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.sim.model.SimDisplayDetails;
import com.neptunetg.mdce.common.internal.sim.model.SimDisplayDetailsPagedResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

/**
 * Service for providing access to miu accounts
 */
public interface SimService
{
    /**
     * URL of the service
     */
    String URL_SIM_LIST = "/internal/sim/list";

    String URL_SIM_DETAILS = "/internal/sim/details";

    String URL_GET_SIM_ICCIDS = "/internal/ref/get/iccids";

    String URL_GET_SIM_MNO_LIST = "/internal/sim/simMnoList";

    String URL_SIM_LIFECYCLESTATE = "/internal/sim/lifecyclestate";

    String URL_DEVICE_DETAILS = "/internal/sim/devicedetails";

    String REQUEST_PARAM_ICCIID = "iccid";

    String SIM_LIFECYCLE_STATE = "lifeCycleState";

    String ICCID = "iccid";

    String MNO = "mno";

    String MSISDN = "msisdn";

    /**
     * Get list of SIMs
     */
    @RequestMapping(value = URL_SIM_LIST, method = RequestMethod.GET)
    SimDisplayDetailsPagedResult getSimList(Map<String, String> options) throws InternalApiException;

    /**
     * Updates or inserts a SIM.
     * TODO: Created temporarily to allow integration testing. Remove when no longer required.
     * @param options Additional SIM details. Currently only supports "msisdn".
     * @param iccid The unique SIM ICCID.
     * @param mno The mobile network operator.
     * @throws InternalApiException
     */
    @RequestMapping(value = SimService.URL_DEVICE_DETAILS, method = RequestMethod.POST)
    void updateOrInsertSimDetails(Map<String, String> options, String iccid, String mno) throws InternalApiException;

    /**
     * Updates the lifecycle state of a SIM.
     * @param options Contains the iccid to identify the SIM and lifecycleState with the string value of the new state.
     * @throws InternalApiException
     */
    @RequestMapping(value = SimService.URL_SIM_LIFECYCLESTATE, method = RequestMethod.POST)
    int updateSimLifecycleState(Map<String, String> options) throws InternalApiException;

    /**
     * Get details of specific SIM
     * @param options
     * @return
     * @throws InternalApiException
     */
    @RequestMapping(value = SimService.URL_SIM_DETAILS, method = RequestMethod.GET)
    SimDisplayDetails getSimDetails(Map<String,String> options, String iccid) throws InternalApiException;


    @RequestMapping(value = URL_GET_SIM_ICCIDS, method = RequestMethod.GET)
    List<String> getSimIccids(Map<String, String> options) throws InternalApiException;

    /**
     * Gets a list of all supported SIM cellular networks.
     * @return A list of MNOs.
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value = URL_GET_SIM_MNO_LIST, method = RequestMethod.GET)
    List<String> getSimMnoList() throws InternalApiException;
}
