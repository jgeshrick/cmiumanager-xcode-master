/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.miu.model;

/**
 * Cmiu configuration data set, which is aggregrating from table: cmiu_config_set, reporting plans and data collection plans
 */
public class CmiuConfigSet
{
    private long id;

    private String name;    //name of the config set

    private String cmiuModeName;

    private int reportingStartMins;
    private int reportingIntervalMins;
    private int reportingTransmitWindowMins;
    private int reportingQuietStartMins;
    private int reportingQuietEndMins;
    private int reportingNumberOfRetries;
    private int reportingSyncWarningMins;
    private int reportingSyncErrorMins;

    private int recordingStartTimeMins;
    private int recordingIntervalMins;

    private long miuId;
    private int siteId;
    private Boolean billable;
    private boolean officialMode;

    public CmiuConfigSet(){}

    public CmiuConfigSet(String name)
    {
        this.name = name;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getReportingStartMins()
    {
        return reportingStartMins;
    }

    public void setReportingStartMins(int reportingStartMins)
    {
        this.reportingStartMins = reportingStartMins;
    }

    public int getReportingIntervalMins()
    {
        return reportingIntervalMins;
    }

    public void setReportingIntervalMins(int reportingIntervalMins)
    {
        this.reportingIntervalMins = reportingIntervalMins;
    }

    public int getReportingTransmitWindowMins()
    {
        return reportingTransmitWindowMins;
    }

    public void setReportingTransmitWindowMins(int reportingTransmitWindowMins)
    {
        this.reportingTransmitWindowMins = reportingTransmitWindowMins;
    }

    public int getReportingQuietStartMins()
    {
        return reportingQuietStartMins;
    }

    public void setReportingQuietStartMins(int reportingQuietStartMins)
    {
        this.reportingQuietStartMins = reportingQuietStartMins;
    }

    public int getReportingQuietEndMins()
    {
        return reportingQuietEndMins;
    }

    public void setReportingQuietEndMins(int reportingQuietEndMins)
    {
        this.reportingQuietEndMins = reportingQuietEndMins;
    }

    public int getRecordingStartTimeMins()
    {
        return recordingStartTimeMins;
    }

    public void setRecordingStartTimeMins(int recordingStartTimeMins)
    {
        this.recordingStartTimeMins = recordingStartTimeMins;
    }

    public int getRecordingIntervalMins()
    {
        return recordingIntervalMins;
    }

    public void setRecordingIntervalMins(int recordingIntervalMins)
    {
        this.recordingIntervalMins = recordingIntervalMins;
    }

    public int getReportingNumberOfRetries()
    {
        return reportingNumberOfRetries;
    }

    public void setReportingNumberOfRetries(int reportingNumberOfRetries)
    {
        this.reportingNumberOfRetries = reportingNumberOfRetries;
    }

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public Boolean isBillable()
    {
        return billable;
    }

    public String isBillableAsString()
    {
        if (billable == null) {
            return null;
        }
        return billable ? "Y" : "N";
    }

    public void setBillable(Boolean billable)
    {
        this.billable = billable;
    }

    /**
     * Setter for billiable.
     * Note: this method name is deliberately kept separate from setBillable(Boolean) else it will confuse Jackson parser
     * (it doesn't seems to like two setter with same name in this case)
     * @param billable
     */
    public void setBillableFromString(String billable)
    {
        this.billable = billable.equals("Y");
    }

    public int getReportingSyncWarningMins()
    {
        return reportingSyncWarningMins;
    }

    public void setReportingSyncWarningMins(int reportingSyncWarningMins)
    {
        this.reportingSyncWarningMins = reportingSyncWarningMins;
    }

    public int getReportingSyncErrorMins()
    {
        return reportingSyncErrorMins;
    }

    public void setReportingSyncErrorMins(int reportingSyncErrorMins)
    {
        this.reportingSyncErrorMins = reportingSyncErrorMins;
    }

    public String getCmiuModeName()
    {
        return cmiuModeName;
    }

    public void setCmiuModeName(String cmiuModeName)
    {
        this.cmiuModeName = cmiuModeName;
    }

    public boolean getOfficialMode()
    {
        return officialMode;
    }

    public void setOfficialMode(boolean officialMode)
    {
        this.officialMode = officialMode;
    }

    /**
     * Check if the current config set has the same values as the supplied config set
     * @param compare config set to compare
     * @return true if they have the same values
     */
    public boolean isEqual(final CmiuConfigSet compare )
    {
        return this.reportingStartMins == compare.getReportingStartMins() &&
                this.reportingIntervalMins == compare.getReportingIntervalMins() &&
                this.reportingTransmitWindowMins == compare.getReportingTransmitWindowMins() &&
                this.reportingQuietStartMins == compare.reportingQuietStartMins &&
                this.reportingQuietEndMins == compare.reportingQuietEndMins &&
                this.reportingNumberOfRetries == compare.getReportingNumberOfRetries() &&
                this.reportingSyncErrorMins == compare.reportingSyncErrorMins &&
                this.reportingSyncWarningMins == compare.reportingSyncWarningMins &&
                this.recordingStartTimeMins == compare.getRecordingStartTimeMins() &&
                this.recordingIntervalMins == compare.getRecordingIntervalMins();
    }

    @Override
    public String toString()
    {
        return "Name: " + this.getName() + " [" + this.getId() +
                "] , Site id: " + this.getSiteId() +
                ", CMIU mode: " + this.getCmiuModeName() +
                ", Recording: [" + this.getRecordingStartTimeMins() + ", " + this.getRecordingIntervalMins() + "]" +
                ", Reporting: [" + this.getReportingStartMins() + ", " + this.getReportingIntervalMins() + "]";

    }
}
