/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.common.internal.site.model;

/**
 * Class to store a sites name and ID
 */
public class SiteDetails
{
    private int siteId;
    private String siteName;
    private String regionalManager;
    private String state;
    // date yyyy-MM-dd HH-mm-ss
    private String lastHeardDate;

    public SiteDetails(int siteId, String siteName, String regionalManager, String state)
    {
        this.siteId = siteId;
        this.siteName = siteName;
    }

    public SiteDetails()
    {
        //Default constructor
    }

    public SiteDetails(int siteId, String siteName, String lastHeardDate)
    {
        this.siteId = siteId;
        this.siteName = siteName;
        this.lastHeardDate = lastHeardDate;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public String getSiteName()
    {
        return siteName;
    }

    public void setSiteName(String siteName)
    {
        this.siteName = siteName;
    }

    public String getRegionalManager()
    {
        return regionalManager;
    }

    public void setRegionalManager(String regionalManager)
    {
        this.regionalManager = regionalManager;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getLastHeardDate()
    {
        return lastHeardDate;
    }

    public void setLastHeardDate(String lastHeardDate)
    {
        this.lastHeardDate = lastHeardDate;
    }
}
