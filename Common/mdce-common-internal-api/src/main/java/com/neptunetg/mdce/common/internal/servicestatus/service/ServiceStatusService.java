/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.common.internal.servicestatus.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.servicestatus.model.ServiceStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * A service for finding out the status of back-end servers
 */
public interface ServiceStatusService
{
    public static final String URL_SERVICE_STATUS = "/internal/service-status";

    @RequestMapping(URL_SERVICE_STATUS)
    @ResponseBody
    public ServiceStatus getServiceStatus() throws InternalApiException;


}
