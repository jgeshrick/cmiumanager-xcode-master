/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.internal.miu.model.imageupdate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImageDescription
{
    private String fileName;
    private ImageType imageType;
    private int startAddress;
    private int sizeBytes;
    private int revisionMajor;
    private int revisionMinor;
    private int revisionDate;
    private int revisionBuildNum;

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public ImageType getImageType()
    {
        return imageType;
    }

    public void setImageType(ImageType imageType)
    {
        this.imageType = imageType;
    }

    public int getStartAddress()
    {
        return startAddress;
    }

    public void setStartAddress(int startAddress)
    {
        this.startAddress = startAddress;
    }

    public int getSizeBytes()
    {
        return sizeBytes;
    }

    public void setSizeBytes(int sizeBytes)
    {
        this.sizeBytes = sizeBytes;
    }

    public int getRevisionMajor()
    {
        return revisionMajor;
    }

    public void setRevisionMajor(int revisionMajor)
    {
        this.revisionMajor = revisionMajor;
    }

    public int getRevisionMinor()
    {
        return revisionMinor;
    }

    public void setRevisionMinor(int revisionMinor)
    {
        this.revisionMinor = revisionMinor;
    }

    public int getRevisionDate()
    {
        return revisionDate;
    }

    public void setRevisionDate(int revisionDate)
    {
        this.revisionDate = revisionDate;
    }

    public int getRevisionBuildNum()
    {
        return revisionBuildNum;
    }

    public void setRevisionBuildNum(int revisionBuildNum)
    {
        this.revisionBuildNum = revisionBuildNum;
    }

    public void setRevision(int revisionMajor, int revisionMinor, int revisionDate, int revisionBuildNum)
    {
        this.revisionMajor = revisionMajor;
        this.revisionMinor = revisionMinor;
        this.revisionDate = revisionDate;
        this.revisionBuildNum = revisionBuildNum;
    }

    /**
     * Set image revision from string.
     * @param revision verison string with format w.x.y.z
     */
    public void setRevision(String revision)
    {
        final Pattern pattern = Pattern.compile("(\\d+).(\\d+).(\\d+).(\\d+)");
        Matcher matcher = pattern.matcher(revision);

        String result = "";
        if (matcher.find() && matcher.groupCount()==4)
        {
            this.revisionMajor = Integer.parseInt(matcher.group(1));
            this.revisionMinor = Integer.parseInt(matcher.group(2));
            this.revisionDate = Integer.parseInt(matcher.group(3));
            this.revisionBuildNum = Integer.parseInt(matcher.group(4));
        }

    }

    public String getVersion()
    {
        return String.format("%d.%d.%d.%d", this.revisionMajor, this.revisionMinor, this.revisionDate, this.revisionBuildNum);
    }


    public enum ImageType
    {
        FirmwareImage(0, "Application image", "CmiuApplication"),
        ConfigImage(1, "Configuration image", "CmiuConfiguration"),
        TelitModuleImage(2, "Telit Module image", "CmiuTelitModule"),
        BleConfigImage(3, "BLE Configuration", "CmiuBleConfiguration"),
        ArbConfigImage(4, "ARB configuration", "CmiuArbConfiguration"),
        EncryptionImage(5, "Encryption key table", "CmiuEncryption"),
        BootloaderImage(6, "Bootloader image", "CmiuBootloader");

        private final int value;
        private final String displayName;
        private final String s3folderName;

        private ImageType(int value, String name, String s3folderName)
        {
            this.value=value;
            this.displayName = name;
            this.s3folderName = s3folderName;
        }

        public static ImageType fromValue(int value)
        {
            for (ImageType type : ImageType.values())
            {
                if (type.getValue() == value)
                {
                    return type;
                }
            }

            throw new IllegalArgumentException("Value not recognised");
        }

        public static ImageType fromS3FolderName(String value)
        {
            for (ImageType type : ImageType.values())
            {
                if (type.getS3folderName().equals(value))
                {
                    return type;
                }
            }

            throw new IllegalArgumentException("S3 subfolder not recognised and not mapped to imageType");
        }

        /**
         * Return a list of all available image type defined.
         *
         * @return list of all image type
         */
        public static List<ImageType> getList()
        {
            List<ImageType> imageTypeList = new ArrayList<>();
            Collections.addAll(imageTypeList, ImageType.values());
            return imageTypeList;
        }

        public int getValue()
        {
            return value;
        }

        public String getS3folderName()
        {
            return s3folderName;
        }

        public String getDisplayName()
        {
            return displayName;
        }
    }
}
