/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */
package com.neptunetg.mdce.common.internal.anaylsis.model;

/**
 * CMIU Timing Model.
 */
public class CmiuTimingAnalysisDetails
{
    private Long miuId;
    private Long totalConnectionTimeMilliSeconds;
    private Integer registerTimeMilliSeconds;
    private Integer registerActivateContextTimeMilliSeconds;
    private Integer registerConnectionTimeMilliSeconds;
    private Integer registerTransferPacketTimeMilliSeconds;
    private Integer disconnectTimeMilliSeconds;
    private String cmiuMode;
    private String mno;
    private String state;
    private Integer siteId;

    public CmiuTimingAnalysisDetails(){}

    public CmiuTimingAnalysisDetails(Long miuId, Long totalConnectionTimeMilliSeconds, Integer registerTimeMilliSeconds,
                                     Integer registerActivateContextTime, Integer registerConnectionTime,
                                     Integer registerTransferPacketTimeMilliSeconds, Integer disconnectTimeMilliSeconds, String cmiuMode,
                                     String mno, String state, Integer siteId) {
        this.miuId = miuId;
        this.totalConnectionTimeMilliSeconds = totalConnectionTimeMilliSeconds;
        this.registerTimeMilliSeconds = registerTimeMilliSeconds;
        this.registerActivateContextTimeMilliSeconds = registerActivateContextTime;
        this.registerConnectionTimeMilliSeconds = registerConnectionTime;
        this.registerTransferPacketTimeMilliSeconds = registerTransferPacketTimeMilliSeconds;
        this.disconnectTimeMilliSeconds = disconnectTimeMilliSeconds;
        this.cmiuMode = cmiuMode;
        this.mno = mno;
        this.state = state;
        this.siteId = siteId;
    }

    public long getMiuId() {
        return miuId;
    }

    public void setMiuId(long miuId) {
        this.miuId = miuId;
    }

    public Long getTotalConnectionTimeMilliSeconds(long total_connection_time) {
        return totalConnectionTimeMilliSeconds;
    }

    public void setTotalConnectionTimeMilliSeconds(Long totalConnectionTimeMilliSeconds) {
        this.totalConnectionTimeMilliSeconds = totalConnectionTimeMilliSeconds;
    }

    public int getRegisterTimeMilliSeconds() {
        return registerTimeMilliSeconds;
    }

    public void setRegisterTimeMilliSeconds(int registerTimeMilliSeconds) {
        this.registerTimeMilliSeconds = registerTimeMilliSeconds;
    }

    public int getRegisterActivateContextTimeMilliSeconds() {
        return registerActivateContextTimeMilliSeconds;
    }

    public void setRegisterActivateContextTimeMilliSeconds(int registerActivateContextTimeMilliSeconds) {
        this.registerActivateContextTimeMilliSeconds = registerActivateContextTimeMilliSeconds;
    }

    public int getRegisterConnectionTime() {
        return registerConnectionTimeMilliSeconds;
    }

    public void setRegisterConnectionTime(int registerConnectionTime) {
        this.registerConnectionTimeMilliSeconds = registerConnectionTime;
    }

    public Integer getRegisterTransferPacketTimeMilliSeconds() {
        return registerTransferPacketTimeMilliSeconds;
    }

    public void setRegisterTransferPacketTimeMilliSeconds(Integer registerTransferPacketTimeMilliSeconds) {
        this.registerTransferPacketTimeMilliSeconds = registerTransferPacketTimeMilliSeconds;
    }

    public int getDisconnectTimeMilliSeconds() {
        return disconnectTimeMilliSeconds;
    }

    public void setDisconnectTimeMilliSeconds(int disconnectTimeMilliSeconds) {
        this.disconnectTimeMilliSeconds = disconnectTimeMilliSeconds;
    }

    public String getCmiuMode() {
        return cmiuMode;
    }

    public void setCmiuMode(String cmiuMode) {
        this.cmiuMode = cmiuMode;
    }

    public String getMno() {
        return mno;
    }

    public void setMno(String mno) {
        this.mno = mno;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }
}

