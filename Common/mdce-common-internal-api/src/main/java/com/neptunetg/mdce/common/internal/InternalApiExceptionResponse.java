/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.common.internal;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;

/**
 * JSON-mapped object to contain details of an error that occurred on this server while processing the request
 */
public class InternalApiExceptionResponse implements Serializable
{

    private String message;

    private String stackTrace;

    /**
    Create a JSON object for the given exception
     */
    public InternalApiExceptionResponse(InternalApiException e)
    {
        this.message = e.getMessage();
        final StringWriter w = new StringWriter();
        e.printStackTrace(new PrintWriter(w));
        this.stackTrace = w.toString();
    }

    /**
     * Default constructor - for Jackson
     */
    public InternalApiExceptionResponse()
    {
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getStackTrace()
    {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace)
    {
        this.stackTrace = stackTrace;
    }
}
