/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.common.internal.server;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.InternalApiExceptionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Detects InternalApiException and returns it in a useful JSON form to the caller
 */
@ControllerAdvice
public class InternalApiErrorHandler
{

    private static final Logger logger = LoggerFactory.getLogger(InternalApiErrorHandler.class);

    /**
     * Return 500 response code + useful exception information
     * @param exception Exception that occurred
     * @return JSON-mappable exception object
     */
    @ExceptionHandler(InternalApiException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public InternalApiExceptionResponse handleError(InternalApiException exception)
    {
        logger.error("Error occurred processing internal API request.  Returning error to client as JSON.", exception);
        return new InternalApiExceptionResponse(exception);
    }

}
