/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.miu.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.MiuDisplayDetailsPagedResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * Service for providing access to miu accounts
 */
public interface MiuService
{
    /**
     * URL of the service
     */
    String URL_MIU_LIST = "/internal/miu/list";
    String URL_MIU_CLAIM = "/internal/miu/claim";
    String URL_MIU_LIFECYCLE_STATES = "/internal/miu/lifecycleStates";
    String URL_CMIU_MODES = "/internal/miu/cmiumodes";

    String REQUEST_PARAM_MIU_ID = "miuId";
    String REQUEST_PARAM_NEW_SITE_ID = "newSiteId";
    String REQUEST_PARAM_USER = "user";

    /**
     * add a miu
     */
    @RequestMapping(value = URL_MIU_LIST, method = RequestMethod.GET)
    MiuDisplayDetailsPagedResult getMiuList(Map<String, String> options) throws InternalApiException;

    @RequestMapping(value = URL_MIU_CLAIM, method = RequestMethod.POST)
    void claimMiu(long miuId, int newSiteId, String requester) throws InternalApiException;

    @RequestMapping(value = URL_MIU_LIFECYCLE_STATES, method = RequestMethod.GET)
    List<String> getSetableLifecycleStates(@RequestParam(value = "miuId") long miuId,
                                           @RequestParam(value = "miuType") String miuType) throws InternalApiException;

    @RequestMapping(value = URL_MIU_LIFECYCLE_STATES, method = RequestMethod.POST)
    void setLifecycleState(@RequestParam(value = "miuId") long miuId,
                           @RequestParam(value = "lifecycleState") String lifecycleState) throws InternalApiException;

    @RequestMapping(value = URL_CMIU_MODES, method = RequestMethod.GET)
    List<String> getAllCmiuModes() throws InternalApiException;
}
