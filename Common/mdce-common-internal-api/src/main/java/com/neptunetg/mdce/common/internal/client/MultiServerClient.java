/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLog;
import com.neptunetg.mdce.common.internal.alert.service.AlertRestService;
import com.neptunetg.mdce.common.internal.anaylsis.service.CmiuAnalysisService;
import com.neptunetg.mdce.common.internal.anaylsis.model.CmiuTimingAnalysisDetails;
import com.neptunetg.mdce.common.internal.audit.model.AuditLogPagedResult;
import com.neptunetg.mdce.common.internal.audit.service.AuditLogService;
import com.neptunetg.mdce.common.internal.dashboard.model.StatusCounts;
import com.neptunetg.mdce.common.internal.dashboard.service.MsrService;
import com.neptunetg.mdce.common.internal.email.model.EmailMessage;
import com.neptunetg.mdce.common.internal.email.service.EmailService;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigChange;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigHistory;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigMgmt;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;
import com.neptunetg.mdce.common.internal.miu.model.CmiuDetails;
import com.neptunetg.mdce.common.internal.miu.model.MiuDisplayDetailsPagedResult;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CmiuDescriptionViewModel;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandSummary;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import com.neptunetg.mdce.common.internal.miu.service.MiuCommandControlService;
import com.neptunetg.mdce.common.internal.miu.service.CmiuConfigMgmtService;
import com.neptunetg.mdce.common.internal.miu.service.CmiuConfigSetService;
import com.neptunetg.mdce.common.internal.miu.service.CmiuDetailsService;
import com.neptunetg.mdce.common.internal.miu.service.CmiuImageUpdateService;
import com.neptunetg.mdce.common.internal.miu.service.MiuService;
import com.neptunetg.mdce.common.internal.settings.service.MdceSettingsService;
import com.neptunetg.mdce.common.internal.sim.model.SimDisplayDetails;
import com.neptunetg.mdce.common.internal.sim.model.SimDisplayDetailsPagedResult;
import com.neptunetg.mdce.common.internal.sim.service.SimService;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataDistributer;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataUtility;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.common.internal.site.model.SiteForUser;
import com.neptunetg.mdce.common.internal.site.service.RefService;
import com.neptunetg.mdce.common.internal.site.service.SiteService;
import com.neptunetg.mdce.common.internal.user.model.LoginCredentials;
import com.neptunetg.mdce.common.internal.user.model.LoginResult;
import com.neptunetg.mdce.common.internal.user.model.ModifiedUserDetails;
import com.neptunetg.mdce.common.internal.user.model.NewUserDetails;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequest;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequestClaim;
import com.neptunetg.mdce.common.internal.user.model.UserAlertSettings;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import com.neptunetg.mdce.common.internal.user.service.UserService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * A client that contacts multiple servers in parallel with timeouts.  Method calls block until
 * all servers have responded or timed out
 */
public class MultiServerClient implements MsrService,
        CmiuDetailsService,
        CmiuConfigMgmtService,
        CmiuConfigSetService,
        MiuCommandControlService,
        CmiuImageUpdateService,
        MiuService,
        SiteService,
        UserService,
        AlertRestService,
        AuditLogService,
        RefService,
        MdceSettingsService,
        SimService,
        EmailService,
        CmiuAnalysisService
{

    private final List<SingleServerClient> serverClients;

    private final ExecutorService threadPool;

    private final long timeoutSeconds;

    /**
     * Create an instance of a multi-server client
     *
     * @param servers        The servers to contact
     * @param threadPool     A thread pool to use to contact the servers
     * @param timeoutSeconds Give up waiting for back-end servers after this long
     * @throws InternalApiException If something goes wrong
     */
    public <R> MultiServerClient(List<InternalApiClientConfig> servers, ExecutorService threadPool,
                                 long timeoutSeconds) throws InternalApiException
    {
        this.serverClients = new ArrayList<>();
        for (InternalApiClientConfig cc : servers)
        {
            this.serverClients.add(new SingleServerClient(cc.getName(), cc.getUrl()));
        }

        this.threadPool = threadPool;
        this.timeoutSeconds = timeoutSeconds;
    }

    /**
     * Get the MSR for a group of endpoints
     *
     * @param groupId Group ID identifying the endpoints
     * @return MSR stats
     * @throws InternalApiException In case we don't manage to the MSR from all the servers
     */
    @Override
    public StatusCounts getMsrByGroup(final String groupId) throws InternalApiException
    {
        final StatusCounts ret = new StatusCounts();
        final List<Future<StatusCounts>> counts = new ArrayList<>();
        for (SingleServerClient client : serverClients)
        {
            counts.add(this.threadPool.submit(() -> client.getMsrByGroup(groupId)));
        }

        long timeout = System.currentTimeMillis() + timeoutSeconds * 1000L;

        try
        {
            for (Future<StatusCounts> count : counts)
            {
                long timeLeft = timeout - System.currentTimeMillis();
                if (timeLeft > 0L)
                {
                    ret.add(count.get(timeLeft, TimeUnit.MILLISECONDS));
                }
                else
                {
                    throw new TimeoutException("Ran out of time before invocation");
                }
            }
            return ret;
        }
        catch (ExecutionException e)
        {
            throw new InternalApiException("Failed to execute", e);
        }
        catch (InterruptedException e)
        {
            throw new InternalApiException("Interrupted", e);
        }
        catch (TimeoutException e)
        {
            throw new InternalApiException("Request timed out (max " + timeoutSeconds + " seconds)", e);
        }
    }


    /**
     * getCmiuByAccount
     *
     * @param accountId ID uniquely identifying group of endpoints
     * @return Status counts
     * @throws InternalApiException If something goes wrong in the request
     */
    @Override
    public List<CmiuDetails> getCmiuByAccount(@RequestParam(PARAM_CMIU_ACCOUNT_ID) String accountId) throws InternalApiException
    {
        List<List<CmiuDetails>> result = this.executeOnMultiServer(singleServerClient ->
        {
            try
            {
                return singleServerClient.getCmiuByAccount(accountId);
            }
            catch (InternalApiException e)
            {
                throw new RuntimeException(e);
            }
        });

        return result.stream().flatMap(Collection::stream).collect(Collectors.toList());

    }

    /**
     * Get the CMIU config
     *
     * @param cmiuId the cmiu id
     * @return Status counts
     * @throws InternalApiException If something goes wrong in the request
     */
    @Override
    public CmiuConfigMgmt getCmiuConfigMgmt(@RequestParam(PARAM_CMIU_ID) long cmiuId) throws InternalApiException
    {
        List<CmiuConfigMgmt> result = this.executeOnMultiServer(singleServerClient ->
        {
            try
            {
                return singleServerClient.getCmiuConfigMgmt(cmiuId);
            }
            catch (InternalApiException e)
            {
                throw new RuntimeException(e);
            }
        });

        return result.get(0);   //todo
    }

    @Override
    public List<CmiuTimingAnalysisDetails> getCmiuTimingInfo(@RequestParam(value = PARAM_CONNECTION_THRESHOLD, required = false) Long connectionThreshold,
                                                             @RequestParam(value = PARAM_CMIU_MODE, required = false) String cmiuMode,
                                                             @RequestParam(value = PARAM_CMIU_MNO, required = false) String mno,
                                                             @RequestParam(value = PARAM_CMIU_STATE, required = false) String state,
                                                             @RequestParam(value = PARAM_CMIU_SITE_ID, required = false) String siteId) throws InternalApiException {

        final List<CmiuTimingAnalysisDetails> result = new ArrayList<>();

        this.executeOnMultiServer(client ->
        {
            try
            {
                return client.getCmiuTimingInfo(connectionThreshold,cmiuMode,mno,state,siteId);
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        });

        return result;
    }

    /**
     * Add a new CMIU config
     *
     * @param newCmiuConfigChangeList
     * @return
     * @throws InternalApiException
     */
    @Override
    public CmiuConfigMgmt addCmiuConfig(String userName, List<CmiuConfigChange> newCmiuConfigChangeList) throws InternalApiException
    {
        final List<CmiuConfigMgmt> result = this.executeOnMultiServer(client ->
        {
            try
            {
                return client.addCmiuConfig(userName, newCmiuConfigChangeList);
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        });

        return result.get(0);   //TODO
    }

    private SingleServerClient getPrimaryServer()
    {
        return serverClients.get(0);
    }


    @Override
    public void sendCommand(CommandRequest commandRequest) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        primaryServer.sendCommand(commandRequest);
    }

    @Override
    public int flushCommand(@RequestParam(MiuCommandControlService.REQUEST_PARAM_CMIUID) long cmiuId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.flushCommand(cmiuId);
    }

    @Override
    public void rejectCommand(@RequestParam(MiuCommandControlService.REQUEST_PARAM_COMMANDID) long commandId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        primaryServer.rejectCommand(commandId);
    }

    @Override
    public CmiuDescriptionViewModel getCmiuDescription(@RequestParam(MiuCommandControlService.REQUEST_PARAM_CMIUID) long cmiuId) throws InternalApiException
    {
        final List<CmiuDescriptionViewModel> result = this.executeOnMultiServer(client ->
        {
            try
            {
                return client.getCmiuDescription(cmiuId);
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        });
        return result.get(0);   //TODO

    }

    @Override
    public List<CommandSummary> getCommandsInProgress() throws InternalApiException
    {
        final List<CommandSummary> result = new ArrayList<>();

        this.executeOnMultiServer(client ->
        {
            try
            {
                return client.getCommandsInProgress();
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }).forEach(c -> result.addAll(c));

        return result;
    }

    @Override
    public List<CommandSummary> getCommandsReceived() throws InternalApiException
    {
        final List<CommandSummary> result = new ArrayList<>();

        this.executeOnMultiServer(client ->
        {
            try
            {
                return client.getCommandsReceived();
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }).forEach(c -> result.addAll(c));

        return result;
    }

    @Override
    public List<CommandSummary> getCommandsCompleted(@RequestParam(REQUEST_PARAM_DAYS_AGO) int daysAgo) throws InternalApiException
    {
        final List<CommandSummary> result = new ArrayList<>();

        this.executeOnMultiServer(client ->
        {
            try
            {
                return client.getCommandsCompleted(daysAgo);
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }).forEach(c -> result.addAll(c));
        return result;
    }

    @Override
    public List<CommandSummary> getCommandsRejected(@RequestParam(REQUEST_PARAM_DAYS_AGO) int daysAgo) throws InternalApiException
    {
        final List<CommandSummary> result = new ArrayList<>();

        this.executeOnMultiServer(client ->
        {
            try
            {
                return client.getCommandsRejected(daysAgo);
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }).forEach(c -> result.addAll(c));
        return result;
    }

    /**
     * Get a list of cmiu config mgmt entries representing CMIU config commands that are being schedule for sending to CMIU
     *
     * @return
     */
    @Override
    public List<CmiuConfigMgmt> getCmiuConfigChangeInProcessList() throws InternalApiException
    {
        List<List<CmiuConfigMgmt>> result = this.executeOnMultiServer(client ->
        {
            try
            {
                return client.getCmiuConfigChangeInProcessList();
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        });

        return result.stream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    /**
     * Get a list of cmiu config mgmt entries representing CMIU config commands that has been changed and acknowledged (reported) by the CMIU
     * for the last X days.
     * This filters the Cmiu config mgmt table to find reported config which has been changed in the last X days and matches planned config.
     *
     * @param changedDaysAgo the number of days ago the changes has been made to the reported config
     * @return filtered list of cmiu mgmt table
     * @throws InternalApiException
     */
    @Override
    public List<CmiuConfigMgmt> getCmiuConfigChangeCompletedList(int changedDaysAgo) throws InternalApiException
    {
        List<List<CmiuConfigMgmt>> result = this.executeOnMultiServer(client ->
        {
            try
            {
                return client.getCmiuConfigChangeCompletedList(changedDaysAgo);
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        });

        return result.stream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    @Override
    public List<CmiuConfigHistory> getCmiuConfigHistory(@RequestParam(PARAM_CMIU_ID) long cmiuId) throws InternalApiException
    {
        List<List<CmiuConfigHistory>> result = this.executeOnMultiServer(client ->
        {
            try
            {
                return client.getCmiuConfigHistory(cmiuId);
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        });

        return result.stream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    /**
     * Run the task on multiple SingleServerClients.
     *
     * @param task the task definition to run on each SingleServerClients
     * @param <R>  The return type of executing the function on SingleServerClient
     * @return List of results aggregrated from running in the list of SingleServerClients
     * @throws InternalApiException
     */
    private <R> List<R> executeOnMultiServer(Function<SingleServerClient, ? extends R> task) throws InternalApiException
    {
        final List<Future<R>> taskList = new ArrayList<>();
        for (SingleServerClient client : this.serverClients)
        {
            taskList.add(this.threadPool.submit(() -> task.apply(client)));
        }

        long timeout = System.currentTimeMillis() + timeoutSeconds * 1000L;

        try
        {
            final List<R> resultList = new ArrayList<>();

            for (Future<R> count : taskList)
            {
                long timeLeft = timeout - System.currentTimeMillis();
                if (timeLeft > 0L)
                {
                    resultList.add(count.get(timeLeft, TimeUnit.MILLISECONDS));
                }
                else
                {
                    throw new TimeoutException("Ran out of time before invocation");
                }
            }
            return resultList;
        }
        catch (ExecutionException e)
        {
            throw new InternalApiException("Failed to execute", e);
        }
        catch (InterruptedException e)
        {
            throw new InternalApiException("Interrupted", e);
        }
        catch (TimeoutException e)
        {
            throw new InternalApiException("Request timed out (max " + timeoutSeconds + " seconds)", e);
        }
    }

    /**
     * Get a list of config sets
     *
     * @return
     * @throws InternalApiException
     */
    @Override
    public List<CmiuConfigSet> getCmiuConfigSetList() throws InternalApiException
    {
        List<List<CmiuConfigSet>> result = this.executeOnMultiServer(singleServerClient ->
        {
            try
            {
                return singleServerClient.getCmiuConfigSetList();
            }
            catch (InternalApiException e)
            {
                throw new RuntimeException(e);
            }
        });

        return result.stream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    /**
     * Get a config set from id
     *
     * @param configSetId the configset id
     * @return
     * @throws InternalApiException
     */
    @Override
    public CmiuConfigSet getCmiuConfigSet(@RequestParam(value = PARAM_CONFIG_SET_ID) Long configSetId) throws InternalApiException
    {
        List<CmiuConfigSet> result = this.executeOnMultiServer(singleServerClient ->
        {
            try
            {
                return singleServerClient.getCmiuConfigSet(configSetId);
            }
            catch (InternalApiException e)
            {
                throw new RuntimeException(e);
            }
        });

        return result.get(0);   //TODO
    }

    /**
     * Add a new CMIU config set to database
     *
     * @param cmiuConfigSet new config set definition
     * @return
     * @throws InternalApiException
     */
    @Override
    public void addCmiuConfigSet(String userName, CmiuConfigSet cmiuConfigSet) throws InternalApiException, JsonProcessingException
    {
        this.executeOnMultiServer(singleServerClient ->
        {
            try
            {
                singleServerClient.addCmiuConfigSet(userName, cmiuConfigSet);
                return null;
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        });
    }


    /**
     * Edit an existing CMIU config set
     *
     * @param cmiuConfigSet new config set definition
     * @return
     * @throws InternalApiException
     */
    @Override
    public void editCmiuConfigSet(String userName, CmiuConfigSet cmiuConfigSet) throws InternalApiException, JsonProcessingException
    {
        this.executeOnMultiServer(singleServerClient ->
        {
            try
            {
                singleServerClient.editCmiuConfigSet(userName, cmiuConfigSet);
                return null;
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        });
    }


    /**
     * Get list of available images of a specific types
     *
     * @param imageType The type of image, such as a firmware image
     * @return
     * @throws InternalApiException
     */
    @Override
    public List<ImageDescription> getAvailableImageList(ImageDescription.ImageType imageType) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getAvailableImageList(imageType);
    }

    /**
     * Get list of available images
     *
     * @return
     * @throws InternalApiException
     */
    @Override
    public List<ImageDescription> getAvailableImageList() throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getAvailableImageList();
    }

    /**
     * Get a list of build artifacts
     *
     * @param imageType the type of image to retrieve, if null, all imageTypes are returned
     * @return
     * @throws InternalApiException
     */
    @Override
    public Map<ImageDescription.ImageType, List<String>> getBuildArtifactList(ImageDescription.ImageType imageType) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getBuildArtifactList(imageType);
    }

    /**
     * Register a build artifact
     *
     * @param imageType
     * @param filePath
     * @throws InternalApiException
     */
    @Override
    public void registerBuildArtifact(ImageDescription.ImageType imageType, String filePath) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        primaryServer.registerBuildArtifact(imageType, filePath);
    }

    /**
     * Add a user
     *
     * @param newUserDetails The details of the new user
     * @throws InternalApiException
     */
    @Override
    public boolean addUser(@RequestBody NewUserDetails newUserDetails) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.addUser(newUserDetails);
    }

    @Override
    public void removeUser(@RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        primaryServer.removeUser(modifiedUserDetails);
    }

    /**
     * Get a list of all users details
     *
     * @return
     * @throws InternalApiException
     */
    @Override
    public List<UserDetails> listUsers() throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.listUsers();
    }

    @Override
    public void resetPassword(@RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        primaryServer.resetPassword(modifiedUserDetails);
    }

    @Override
    public List<SiteDetails> getSitesForUser(@RequestBody int userId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSitesForUser(userId);
    }

    @Override
    public List<SiteDetails> getSitesNotForUser(@RequestBody int userId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSitesNotForUser(userId);
    }

    @Override
    public boolean addSiteToUser(@RequestBody SiteForUser siteForUser) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.addSiteToUser(siteForUser);
    }

    @Override
    public boolean removeSiteFromUser(@RequestBody SiteForUser siteForUser) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.removeSiteFromUser(siteForUser);
    }

    @Override
    public LoginResult verifyUser(@RequestBody LoginCredentials loginCredentials) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.verifyUser(loginCredentials);
    }

    @Override
    public List<AlertDetails> getNewAlertList(@RequestBody String alertSource) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getNewAlertList(alertSource);
    }

    @Override
    public List<AlertDetails> getHandledAlertList(@RequestBody String alertSource) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getHandledAlertList(alertSource);
    }

    @Override
    public List<AlertDetails> getStaleAlertList(@RequestBody String alertSource) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getStaleAlertList(alertSource);
    }

    @Override
    public List<AlertDetails> getClearedAlertList(@RequestBody String alertSource) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getClearedAlertList(alertSource);
    }

    @Override
    public AlertDetails getAlert(@RequestBody Long alertId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getAlert(alertId);
    }

    @Override
    public boolean addAlert(@RequestBody AlertDetails alertDetails) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.addAlert(alertDetails);
    }

    @Override
    public boolean setAlertStateToNew(@RequestBody Long alertId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.setAlertStateToNew(alertId);
    }

    @Override
    public boolean setAlertStateToHandled(@RequestBody Long alertId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.setAlertStateToHandled(alertId);
    }

    @Override
    public boolean setAlertStateToStale(@RequestBody Long alertId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.setAlertStateToStale(alertId);
    }

    @Override
    public boolean setAlertStateToCleared(@RequestBody Long alertId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.setAlertStateToCleared(alertId);
    }

    @Override
    public boolean setAlertLevelToWarning(@RequestBody Long alertId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.setAlertLevelToWarning(alertId);
    }

    @Override
    public boolean setAlertLevelToError(@RequestBody Long alertId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.setAlertLevelToError(alertId);
    }

    @Override
    public boolean setAlertTicketId(@RequestBody AlertDetails alertDetails) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.setAlertTicketId(alertDetails);
    }

    @Override
    public boolean addAlertLogMessage(@RequestBody AlertDetails alertDetails) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.addAlertLogMessage(alertDetails);
    }

    @Override
    public List<AlertLog> getAlertLog(@RequestBody Long alertId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getAlertLog(alertId);
    }

    @Override
    public UserDetails getUserDetails(@PathVariable("username") String userName) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getUserDetails(userName);
    }

    @Override
    public List<UserDetails> findUsers(@RequestBody Map<String, Object> options) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.findUsers(options);
    }

    @Override
    public boolean setUserDetails(@PathVariable("username") String userName,
                                  @RequestBody ModifiedUserDetails modifiedUserDetails
    ) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.setUserDetails(userName, modifiedUserDetails);
    }

    @Override
    public UserAlertSettings getUserAlertSettings(@PathVariable("userid") int userId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getUserAlertSettings(userId);
    }

    @Override
    public boolean setUserAlertSettings(@RequestBody UserAlertSettings userAlertSettings) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.setUserAlertSettings(userAlertSettings);
    }

    @Override
    public boolean addPasswordResetRequest(@RequestBody PasswordResetRequest passwordResetRequest) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.addPasswordResetRequest(passwordResetRequest);
    }

    @Override
    public boolean claimPasswordResetRequest(@PathVariable("token") String token,
                                             @RequestBody PasswordResetRequestClaim passwordResetRequestClaim) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.claimPasswordResetRequest(token, passwordResetRequestClaim);
    }

    @Override
    public int expirePasswordResetRequestsByUser(@RequestParam(USER_ID_PARAM) long userId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.expirePasswordResetRequestsByUser(userId);
    }

    @Override
    public PasswordResetRequest getPasswordResetRequestByToken(String token) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getPasswordResetRequestByToken(token);
    }

    @Override
    public AuditLogPagedResult getAuditLog(@RequestParam(value = REQUEST_PARAM_AUDIT_FILTER_MIU, required = false) Long miuId,
                                           @RequestParam(value = REQUEST_PARAM_AUDIT_FILTER_USERNAME, required = false) String userName,
                                           @RequestParam(value = REQUEST_PARAM_AUDIT_LOG_DAYS_AGO, required = false) Integer daysAgo,
                                           @RequestParam(value = REQUEST_PARAM_AUDIT_LOG_PAGE, required = false) Integer page) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getAuditLog(miuId, userName, daysAgo, page);
    }

    @Override
    public void logAccessDenied(@RequestParam(value = REQUEST_PARAM_AUDIT_USERNAME) String userName,
                                @RequestParam(value = REQUEST_PARAM_AUDIT_DESCRIPTION) String description) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        primaryServer.logAccessDenied(userName, description);
    }

    @Override
    public SimDisplayDetailsPagedResult getSimList(Map<String, String> options) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSimList(options);
    }

    @Override
    public void updateOrInsertSimDetails(Map<String, String> options, String iccid, String mno) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        primaryServer.updateOrInsertSimDetails(options, iccid, mno);
    }

    @Override
    public int updateSimLifecycleState(Map<String, String> options) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.updateSimLifecycleState(options);
    }

    @Override
    public SimDisplayDetails getSimDetails(Map<String, String> options, String iccid) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSimDetails(options, iccid);
    }


    @Override
    public List<String> getSimLifeCycleStates(Map<String, String> options) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSimLifeCycleStates(options);
    }

    @Override
    public List<String> getSimNetworks(Map<String, String> options) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSimNetworks(options);
    }

    @Override
    public List<String> getSimIccids(Map<String, String> options) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSimIccids(options);
    }

    @Override
    public List<String> getSimMnoList() throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSimMnoList();
    }

    @Override
    public MiuDisplayDetailsPagedResult getMiuList(Map<String, String> options) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getMiuList(options);
    }

    @Override
    public void claimMiu(long miuId, int newSiteId, String requester) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        primaryServer.claimMiu(miuId, newSiteId, requester);
    }

    @Override
    public List<String> getSetableLifecycleStates(long miuId, String miuType) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSetableLifecycleStates(miuId, miuType);
    }

    @Override
    public void setLifecycleState(long miuId, String lifecycleState) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        primaryServer.setLifecycleState(miuId, lifecycleState);
    }

    @Override
    public List<String> getAllCmiuModes() throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getAllCmiuModes();
    }

    /**
     * Finds sites that have not contacted mdce between a configurable amount of time.
     *
     * @param siteConnectionCheckHours max amount of hours an site should be talking
     *        to mdce anything after that is an issue.
     * @param siteConnectionIgnoreAfterHours max amount of hours a site can be inactive
     *        before we declare inactive and ignore.
     * @return list of sites
     */
    @Override
    public List<SiteDetails> getSitesNotRequestingDataWithinXHours(int siteConnectionCheckHours, int siteConnectionIgnoreAfterHours) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return  primaryServer.getSitesNotRequestingDataWithinXHours(siteConnectionCheckHours, siteConnectionIgnoreAfterHours);
    }

    @Override
    public List<SiteDetails> getSiteList() throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSiteList();
    }

    @Override
    public SiteDetails getSiteDetails(int siteId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSiteDetails(siteId);
    }

    /**
     * Gets a list of the names of the regional managers
     */
    @Override
    public List<String> getRegionalManagers() throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getRegionalManagers();
    }

    @Override
    public List<String> getStates() throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getStates();
    }

    @Override
    public List<String> getMiuTypes() throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getMiuTypes();
    }

    @Override
    public List<SiteDetails> getSitesForRegionalManager(String regionalManager) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSitesForRegionalManager(regionalManager);
    }

    @Override
    public List<SiteDetails> getSitesForState(String state) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSitesForState(state);
    }

    @Override
    public MdceReferenceDataUtility getSiteReferenceData(int siteId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getSiteReferenceData(siteId);
    }

    @Override
    public MdceReferenceDataDistributer getDistributerReferenceData(String distributerId) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getDistributerReferenceData(distributerId);
    }


    @Override
    public String getTextSettingValue(String key) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getTextSettingValue(key);
    }

    @Override
    public Long getIntSettingValue(String key, Long defaultValue) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getIntSettingValue(key, defaultValue);
    }

    @Override
    public void setTextSettingValue(String key, String newValue, String requester) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        primaryServer.setTextSettingValue(key, newValue, requester);
    }

    @Override
    public void setIntSettingValue(String key, Long newValue, String requester) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        primaryServer.setIntSettingValue(key, newValue, requester);

    }

    @Override
    public void sendEmail(EmailMessage emailMessage) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        primaryServer.sendEmail(emailMessage);
    }

    @Override
    public EmailMessage getDebugModeEmail() throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getDebugModeEmail();
    }

    /**
     * Find user details by email. To check if email in use.
     *
     * @param emailAddress user email.
     * @return user details or null.
     * @throws InternalApiException If something goes wrong in the request.
     */
    @Override
    public List<UserDetails> getUserDetailsByEmail(@RequestParam("email") String emailAddress) throws InternalApiException
    {
        final SingleServerClient primaryServer = getPrimaryServer();
        return primaryServer.getUserDetailsByEmail(emailAddress);
    }
}
