/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *
 *  All rights reserved
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.internal.miu.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.CmiuDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Interface for internal API service to get CMIU stats
 */
public interface CmiuDetailsService
{
    /**
     * URL of the service
     */
    String URL_CMIU_BY_ACCOUNT = "/internal/miu-details-by-group";

    /**
     * Parameter name for group ID
     */
    String PARAM_CMIU_ACCOUNT_ID = "accountId";

    /**
     * Get the MSR for a group
     * @param accountId ID uniquely identifying group of endpoints
     * @return Status counts
     * @throws InternalApiException If something goes wrong in the request
     */
    @RequestMapping(URL_CMIU_BY_ACCOUNT)
    @ResponseBody
    List<CmiuDetails> getCmiuByAccount(@RequestParam(PARAM_CMIU_ACCOUNT_ID) String accountId) throws InternalApiException;


}
