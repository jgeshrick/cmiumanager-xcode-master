/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.lora.model;

/**
 * Created by WJD1 on 05/12/2016.
 * Model LoRa Details
 */
public class LoraDisplayDetails
{
    private String miuId;

    private String eui;

    private String network;

    private String lastUpdateTime;

    public LoraDisplayDetails(String miuId, String eui, String networ, String lastUpdateTime)
    {
        this.miuId = miuId;
        this.eui = eui;
        this.network = network;
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getMiuId()
    {
        return miuId;
    }

    public void setMiuId(String miuId)
    {
        this.miuId = miuId;
    }

    public String getEui()
    {
        return eui;
    }

    public void setEui(String eui)
    {
        this.eui = eui;
    }

    public String getNetwork()
    {
        return network;
    }

    public void setNetwork(String network)
    {
        this.network = network;
    }

    public String getLastUpdateTime()
    {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime)
    {
        this.lastUpdateTime = lastUpdateTime;
    }
}
