/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.miu.model;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;

/**
 * Miu data usage POJO
 */
public class MiuDataUsage
{
    private long miuId;
    private LocalDate dataUsageDate;
    private int byteUsedToday;
    private int byteUsedMonthToDate;
    private Instant dataUsageDatetime;

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public LocalDate getDataUsageDate()
    {
        return dataUsageDate;
    }

    public void setDataUsageDate(LocalDate dataUsageDate)
    {
        this.dataUsageDate = dataUsageDate;
    }

    public int getByteUsedToday()
    {
        return byteUsedToday;
    }

    public void setByteUsedToday(int byteUsedToday)
    {
        this.byteUsedToday = byteUsedToday;
    }

    public int getByteUsedMonthToDate()
    {
        return byteUsedMonthToDate;
    }

    public void setByteUsedMonthToDate(int byteUsedMonthToDate)
    {
        this.byteUsedMonthToDate = byteUsedMonthToDate;
    }

    public Instant getDataUsageDatetime()
    {
        return dataUsageDatetime;
    }

    public void setDataUsageDatetime(Instant dataUsageDatetime)
    {
        this.dataUsageDatetime = dataUsageDatetime;
    }
}
