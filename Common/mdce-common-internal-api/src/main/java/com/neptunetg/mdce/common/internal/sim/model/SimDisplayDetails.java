/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *
 *  All rights reserved
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.internal.sim.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.neptunetg.mdce.common.internal.audit.model.AuditLogPagedResult;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Model SIM details.
 */
public class SimDisplayDetails
{
    // yyyy-MM-dd'T'HH:mm:ss.SSSZ, e.g. 2000-10-31 01:30:00.000-05:00.
    private static final DateTimeFormatter DATE_FORMAT =
            DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("US/Central"));

    private long simId;

    private String iccid;

    private String state;

    private Instant creationDate;

    private String network;

    private String miuId;

    private AuditLogPagedResult auditLog;

    public SimDisplayDetails() { }

    public long getSimId() { return simId; }

    public void setSimId(long simId) { this.simId = simId; }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Instant getCreationDate() { return creationDate; }

    public void setCreationDate(Instant creationDate) { this.creationDate = creationDate; }

    public String getState() { return state; }

    public void setState(String state) { this.state = state; }

    public String getNetwork() { return network; }

    public void setNetwork(String network) { this.network = network; }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getMiuId() { return miuId; }

    public void setMiuId(String miuId) { this.miuId = miuId; }

    public String getIccid() { return iccid; }

    public void setIccid(String iccid) { this.iccid = iccid; }

    public AuditLogPagedResult getAuditLog() { return auditLog; }

    public void setAuditLog(AuditLogPagedResult auditLog) { this.auditLog = auditLog; }

}
