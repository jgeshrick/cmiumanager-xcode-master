/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.site.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataDistributer;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataUtility;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Service for providing access to reference data from SLX
 */
@RestController
@RequestMapping("/service")
public interface RefService
{
    /**
     * URL of the service
     */
    String URL_GET_REGIONAL_MANAGERS = "/internal/ref/get/regionalManagers";
    String URL_GET_STATES = "/internal/ref/get/states";
    String URL_GET_MIU_TYPES = "/internal/ref/get/miuTypes";
    String URL_GET_SITES_FOR_REGIONAL_MANAGER = "/internal/ref/get/sitesForRegionalManager";
    String URL_GET_SITES_FOR_STATE = "/internal/ref/get/sitesForState";
    String URL_GET_SITE_REFERENCE_DATA = "/internal/ref/site";
    String URL_GET_DISTRIBUTER_REFERENCE_DATA = "/internal/ref/distributer";

    String URL_GET_SIM_NETWORKS = "/internal/ref/get/networks";
    String URL_GET_SIM_LIFE_CYCLE_STATES = "/internal/ref/get/lifeCycleStates";

    String REGIONAL_MANAGER = "regionalManager";
    String STATE = "state";
    String SITE_ID = "siteId";
    String DISTRIBUTER_ID = "distributerId";

    String SIM_LIFE_CYCLE_STATE = "lifeCycleState";
    String SIM_ICCID = "iccid";
    String SIM_NETWORK = "network";

    @RequestMapping(value= URL_GET_REGIONAL_MANAGERS, method= RequestMethod.GET)
    List<String> getRegionalManagers() throws InternalApiException;

    @RequestMapping(value= URL_GET_STATES, method= RequestMethod.GET)
    List<String> getStates() throws InternalApiException;

    @RequestMapping(value= URL_GET_MIU_TYPES, method= RequestMethod.GET)
    List<String> getMiuTypes() throws InternalApiException;

    @RequestMapping(value= URL_GET_SITES_FOR_REGIONAL_MANAGER, method= RequestMethod.GET)
    List<SiteDetails> getSitesForRegionalManager(@RequestParam(value = REGIONAL_MANAGER) String regionalManager) throws InternalApiException;

    @RequestMapping(value= URL_GET_SITES_FOR_STATE, method= RequestMethod.GET)
    List<SiteDetails> getSitesForState(@RequestParam(value = STATE) String state) throws InternalApiException;

    @RequestMapping(value= URL_GET_SITE_REFERENCE_DATA, method= RequestMethod.GET)
    MdceReferenceDataUtility getSiteReferenceData(@RequestParam(value = SITE_ID) int siteId) throws InternalApiException;

    @RequestMapping(value= URL_GET_DISTRIBUTER_REFERENCE_DATA, method= RequestMethod.GET)
    MdceReferenceDataDistributer getDistributerReferenceData(@RequestParam(value = DISTRIBUTER_ID) String distributerId) throws InternalApiException;

    @RequestMapping(value= URL_GET_SIM_NETWORKS, method= RequestMethod.GET)
    List<String> getSimNetworks(Map<String, String> options) throws InternalApiException;

    @RequestMapping(value= URL_GET_SIM_LIFE_CYCLE_STATES, method= RequestMethod.GET)
    List<String> getSimLifeCycleStates(Map<String, String> options) throws InternalApiException;

}
