/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.settings.service;


import com.neptunetg.mdce.common.internal.InternalApiException;
import org.springframework.web.bind.annotation.*;

import java.rmi.RemoteException;

/**
 * Provides methods for registering and unregistering MNO callbacks
 */
public interface MnoCallbackService
{
    String MNO = "mno";

    String URL_REGISTER_VERIZON_CALLBACK = "/internal/callback/{" + MNO + "}/register";
    String URL_UNREGISTER_VERIZON_CALLBACK = "/internal/callback/{" + MNO + "}/unregister";

    /**
     * Register callback with a url provided
     * @throws RemoteException
     */
    @RequestMapping(value = URL_REGISTER_VERIZON_CALLBACK, method = RequestMethod.GET)
    @ResponseBody
    String registerCallback(@PathVariable(value = MNO) String mno) throws RemoteException, InternalApiException;

    /**
     *
     * unRegister callback with a url provided
     * @throws RemoteException
     */
    @RequestMapping(value = URL_UNREGISTER_VERIZON_CALLBACK, method = RequestMethod.GET)
    @ResponseBody
    String unregisterCallback(@PathVariable(value = MNO) String mno) throws RemoteException, InternalApiException;
}

