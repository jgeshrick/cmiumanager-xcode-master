/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.internal.settings.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * A service for reading and writing MDCE application settings
 */
public interface MdceSettingsService
{
    String URL_MDCE_SETTINGS = "/internal/mdce-settings";
    String URL_MDCE_SETTINGS_TEXT_SETTING = URL_MDCE_SETTINGS + "/text-setting";
    String URL_MDCE_SETTINGS_INT_SETTING = URL_MDCE_SETTINGS + "/int-setting";

    String OLD_TABLE_MIN_AGE_MONTHS_TO_DELETE_KEY = "aws.dynamo.deletetable.min.age.months";
    long OLD_TABLE_MIN_AGE_MONTHS_TO_DELETE_DEFAULT_VALUE = 3L;
    long OLD_TABLE_MIN_AGE_MONTHS_TO_DELETE_MIN_VALUE = 3L;

    String MAX_LOGIN_ATTEMPTS_KEY = "mdce.maxLoginAttempts";
    long MAX_LOGIN_ATTEMPTS_DEFAULT_VALUE = 3L;
    long MAX_LOGIN_ATTEMPTS_MIN_VALUE = 1L;

    String LOCKED_ACCOUNT_TIMEOUT_MINUTES_KEY = "mdce.lockedAccountTimeoutMinutes";
    long LOCKED_ACCOUNT_TIMEOUT_MINUTES_DEFAULT_VALUE = 5L;
    long LOCKED_ACCOUNT_TIMEOUT_MINUTES_MIN_VALUE = 1L;

    String KEY = "key";
    String DEFAULT_VALUE = "defaultValue";
    String REQUESTER = "requester";

    @RequestMapping(value = URL_MDCE_SETTINGS_TEXT_SETTING, method = RequestMethod.GET)
    @ResponseBody
    String getTextSettingValue(@RequestParam(KEY) String key) throws InternalApiException;

    @RequestMapping(value = URL_MDCE_SETTINGS_INT_SETTING, method = RequestMethod.GET)
    @ResponseBody
    Long getIntSettingValue(@RequestParam(KEY) String key,
                            @RequestParam(name = DEFAULT_VALUE, required = false) Long defaultValue) throws InternalApiException;

    @RequestMapping(value = URL_MDCE_SETTINGS_TEXT_SETTING, method = RequestMethod.POST)
    void setTextSettingValue(@RequestParam(KEY) String key,
                             @RequestBody String newValue,
                             @RequestParam(REQUESTER) String requester) throws InternalApiException;

    @RequestMapping(value = URL_MDCE_SETTINGS_INT_SETTING, method = RequestMethod.POST)
    void setIntSettingValue(@RequestParam(KEY) String key,
                            @RequestBody Long newValue,
                            @RequestParam(REQUESTER) String requester) throws InternalApiException;

}
