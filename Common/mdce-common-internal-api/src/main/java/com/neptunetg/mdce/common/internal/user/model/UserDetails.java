/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.user.model;

import java.time.Instant;

/**
 * Created by WJD1 on 31/07/2015.
 * Class to hold the details for a new user
 */
public class UserDetails
{
    private long userId;
    private String userName;
    private String email;
    private byte[] hash;
    private byte[] salt;
    private int saltIterations;
    private MdceUserRole userLevel;
    private UserAlertSettings userAlertSettings;
    private boolean accountLockoutEnabled;
    private long failedLoginAttempts;
    private Instant disabledUntil;
    private boolean allowConcurrentSessions;

    public long getUserId()
    {
        return userId;
    }

    public void setUserId(long userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public byte[] getHash()
    {
        return hash;
    }

    public void setHash(byte[] hash)
    {
        this.hash = hash;
    }

    public byte[] getSalt()
    {
        return salt;
    }

    public void setSalt(byte[] salt)
    {
        this.salt = salt;
    }

    public int getSaltIterations()
    {
        return saltIterations;
    }

    public void setSaltIterations(int saltInterations)
    {
        this.saltIterations = saltInterations;
    }

    public MdceUserRole getUserLevel()
    {
        return userLevel;
    }

    public void setUserLevel(MdceUserRole userLevel)
    {
        this.userLevel = userLevel;
    }

    public UserAlertSettings getUserAlertSettings()
    {
        return userAlertSettings;
    }

    public void setUserAlertSettings(UserAlertSettings userAlertSettings)
    {
        this.userAlertSettings = userAlertSettings;
    }

    /**
     * Generate a description of the user detail
     * @return
     */
    @Override
    public String toString()
    {
        return String.format("%s [%d], %s, %s",
                userName, userId, email, userLevel.getRoleName());
    }

    public boolean isAccountLockoutEnabled()
    {
        return accountLockoutEnabled;
    }

    public void setAccountLockoutEnabled(boolean accountLockoutEnabled)
    {
        this.accountLockoutEnabled = accountLockoutEnabled;
    }

    public long getFailedLoginAttempts()
    {
        return failedLoginAttempts;
    }

    public void setFailedLoginAttempts(long failedLoginAttempts)
    {
        this.failedLoginAttempts = failedLoginAttempts;
    }

    public Instant getDisabledUntil()
    {
        return disabledUntil;
    }

    public void setDisabledUntil(Instant disabledUntil)
    {
        this.disabledUntil = disabledUntil;
    }

    /**
     * Gets a value indicating whether the user account is currently disabled.
     * @return True if the disabledUntil time is not null and is not in the past; otherwise, false.
     */
    public boolean isCurrentlyDisabled()
    {
        if (disabledUntil != null && disabledUntil.isAfter(Instant.now()))
        {
            return true;
        }

        return false;
    }

    public boolean getAllowConcurrentSessions()
    {
        return allowConcurrentSessions;
    }

    public void setAllowConcurrentSessions(boolean allowConcurrentSessions)
    {
        this.allowConcurrentSessions = allowConcurrentSessions;
    }
}
