/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *
 *  All rights reserved
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.internal.miu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.neptunetg.mdce.common.internal.audit.model.AuditLogPagedResult;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Model CMIU details.
 */
public class MiuDisplayDetails
{
    // yyyy-MM-dd'T'HH:mm:ss.SSSZ, e.g. 2000-10-31 01:30:00.000-05:00.
    private static final DateTimeFormatter DATE_FORMAT =
            DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("US/Central"));

    private int miuId;
    private int siteId;

    private String siteDescription;

    private String lastHeardTime;
    private String creationDate;

    private String meterActive;
    private String imei;
    private String iccid;
    private String msisdn;
    private String modemVendor;
    private String modemMno;
    private String networkProvider;

    private String type;

    private String cmiuFirmwareRevision;
    private String cmiuConfigRevision;
    private String cmiuArbRevision;
    private String cmiuBleRevision;
    private String cmiuEncryptionRevision;
    private String telitFirmwareRevision;

    private String lifecycleState;

    private MiuDataUsage miuDataUsage;

    private String cmiuConfigSetName;
    private String cmiuModeName;
    private int reportingIntervalMins;
    private int recordingIntervalMins;
    private int readingIntervalMins;

    private String reportedCmiuConfigSetName;
    private String reportedCmiuModeName;
    private int reportedReportingIntervalMins;
    private int reportedRecordingIntervalMins;
    private int reportedReadingIntervalMins;

    private String eui;
    private String antennaType;


    private AuditLogPagedResult auditLog;

    // TODO http://redsea:8080/browse/MSPD-829 miu type (R900/CMIU), slx data, reporting/recording interval


    public MiuDisplayDetails()
    {
    }

    public MiuDisplayDetails(int id)
    {
        this.miuId = id;
    }

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(int miuId)
    {
        this.miuId = miuId;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getSiteDescription()
    {
        return siteDescription;
    }

    public void setSiteDescription(String siteDescription)
    {
        this.siteDescription = siteDescription;
    }

    @JsonIgnore
    public String getSiteDisplay()
    {
        if (StringUtils.hasText(this.siteDescription))
        {
            final String siteIdAsString = Integer.toString(this.siteId);
            if (this.siteDescription.indexOf(siteIdAsString) < 0)
            {
                return this.siteDescription.trim() + " (Site ID " + siteIdAsString + ")";
            }
            else
            {
                return this.siteDescription.trim();
            }
        }
        else
        {
            return "Site ID " + siteId;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getLastHeardTime()
    {
        return lastHeardTime;
    }

    public void setLastHeardTime(String lastHeardTime)
    {
        this.lastHeardTime = lastHeardTime;
    }

    @JsonIgnore
    public void setLastHeardTimeToInstant(Instant lastHeardTime)
    {
        if (lastHeardTime == null)
        {
            this.lastHeardTime = null;
        }
        else
        {
            this.lastHeardTime = DATE_FORMAT.format(lastHeardTime);
        }
    }

    @JsonIgnore
    public Instant getLastHeardTimeAsInstant()
    {
        if (this.lastHeardTime == null)
        {
            return null;
        }
        else
        {
            return Instant.from(DATE_FORMAT.parse(this.lastHeardTime));
        }
    }


    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getMeterActive()
    {
        return meterActive;
    }

    public void setMeterActive(String meterActive)
    {
        this.meterActive = meterActive;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(String creationDate)
    {
        this.creationDate = creationDate;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getImei()
    {
        return imei;
    }

    public void setImei(String imei)
    {
        this.imei = imei;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getIccid()
    {
        return iccid;
    }

    public void setIccid(String iccid)
    {
        this.iccid = iccid;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getMsisdn()
    {
        return msisdn;
    }

    public void setMsisdn(String msisdn)
    {
        this.msisdn = msisdn;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public AuditLogPagedResult getAuditLog()
    {
        return auditLog;
    }

    public void setAuditLog(AuditLogPagedResult auditLog)
    {
        this.auditLog = auditLog;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getCmiuFirmwareRevision()
    {
        return cmiuFirmwareRevision;
    }

    public void setCmiuFirmwareRevision(String cmiuFirmwareRevision)
    {
        this.cmiuFirmwareRevision = cmiuFirmwareRevision;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getCmiuConfigRevision()
    {
        return cmiuConfigRevision;
    }

    public void setCmiuConfigRevision(String cmiuConfigRevision)
    {
        this.cmiuConfigRevision = cmiuConfigRevision;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getCmiuArbRevision()
    {
        return cmiuArbRevision;
    }

    public void setCmiuArbRevision(String cmiuArbRevision)
    {
        this.cmiuArbRevision = cmiuArbRevision;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getCmiuBleRevision()
    {
        return cmiuBleRevision;
    }

    public void setCmiuBleRevision(String cmiuBleRevision)
    {
        this.cmiuBleRevision = cmiuBleRevision;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getCmiuEncryptionRevision()
    {
        return cmiuEncryptionRevision;
    }

    public void setCmiuEncryptionRevision(String cmiuEncryptionRevision)
    {
        this.cmiuEncryptionRevision = cmiuEncryptionRevision;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getTelitFirmwareRevision()
    {
        return telitFirmwareRevision;
    }

    public void setTelitFirmwareRevision(String telitFirmwareRevision)
    {
        this.telitFirmwareRevision = telitFirmwareRevision;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getLifecycleState()
    {
        return lifecycleState;
    }

    public void setLifecycleState(String lifecycleState)
    {
        this.lifecycleState = lifecycleState;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public MiuDataUsage getMiuDataUsage()
    {
        return miuDataUsage;
    }

    public void setMiuDataUsage(MiuDataUsage miuDataUsage)
    {
        this.miuDataUsage = miuDataUsage;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getModemVendor()
    {
        return modemVendor;
    }

    public void setModemVendor(String modemVendor)
    {
        this.modemVendor = modemVendor;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getModemMno()
    {
        return modemMno;
    }

    public void setModemMno(String modemMno)
    {
        this.modemMno = modemMno;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getNetworkProvider()
    {
        return networkProvider;
    }

    public void setNetworkProvider(String networkProvider)
    {
        this.networkProvider = networkProvider;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getCmiuConfigSetName()
    {
        return cmiuConfigSetName;
    }

    public void setCmiuConfigSetName(String cmiuConfigSetName)
    {
        this.cmiuConfigSetName = cmiuConfigSetName;
    }

    public int getReportingIntervalMins()
    {
        return reportingIntervalMins;
    }

    public void setReportingIntervalMins(int reportingIntervalMins)
    {
        this.reportingIntervalMins = reportingIntervalMins;
    }

    public int getRecordingIntervalMins()
    {
        return recordingIntervalMins;
    }

    public void setRecordingIntervalMins(int recordingIntervalMins)
    {
        this.recordingIntervalMins = recordingIntervalMins;
    }

    public int getReadingIntervalMins()
    {
        return readingIntervalMins;
    }

    public void setReadingIntervalMins(int readingIntervalMins)
    {
        this.readingIntervalMins = readingIntervalMins;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getCmiuModeName()
    {
        return cmiuModeName;
    }

    public void setCmiuModeName(String cmiuModeName)
    {
        this.cmiuModeName = cmiuModeName;
    }

    public String getReportedCmiuConfigSetName()
    {
        return reportedCmiuConfigSetName;
    }

    public void setReportedCmiuConfigSetName(String reportedCmiuConfigSetName)
    {
        this.reportedCmiuConfigSetName = reportedCmiuConfigSetName;
    }

    public String getReportedCmiuModeName()
    {
        return reportedCmiuModeName;
    }

    public void setReportedCmiuModeName(String reportedCmiuModeName)
    {
        this.reportedCmiuModeName = reportedCmiuModeName;
    }

    public int getReportedReportingIntervalMins()
    {
        return reportedReportingIntervalMins;
    }

    public void setReportedReportingIntervalMins(int reportedReportingIntervalMins)
    {
        this.reportedReportingIntervalMins = reportedReportingIntervalMins;
    }

    public int getReportedRecordingIntervalMins()
    {
        return reportedRecordingIntervalMins;
    }

    public void setReportedRecordingIntervalMins(int reportedRecordingIntervalMins)
    {
        this.reportedRecordingIntervalMins = reportedRecordingIntervalMins;
    }

    public int getReportedReadingIntervalMins()
    {
        return reportedReadingIntervalMins;
    }

    public void setReportedReadingIntervalMins(int reportedReadingIntervalMins)
    {
        this.reportedReadingIntervalMins = reportedReadingIntervalMins;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getEui()
    {
        return eui;
    }

    public void setEui(String eui)
    {
        this.eui = eui;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getAntennaType()
    {
        return antennaType;
    }

    public void setAntennaType(String antennaType)
    {
        this.antennaType = antennaType;
    }
}
