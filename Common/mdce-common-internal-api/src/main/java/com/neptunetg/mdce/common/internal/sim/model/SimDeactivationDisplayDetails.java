/* ***************************************************************************
        *
        *    Neptune Technology Group
        *    Copyright 2017 as unpublished work.
        *    All rights reserved
        *
        *    The information contained herein is confidential
        *    property of Neptune Technology Group. The use, copying, transfer
        *    or disclosure of such information is prohibited except by express
        *    written agreement with Neptune Technology Group.
        *
        *****************************************************************************/


        package com.neptunetg.mdce.common.internal.sim.model;

/**
 * Created by dcloud on 3/16/2017.
 */
public class SimDeactivationDisplayDetails
{
    private long miuId;

    private String iccid;

    private String imei;

    private String lastTimeHeard;

    private String lastCommunicationTime;

    private String currentLifecycleState;

    private String carrier;

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public String getIccid()
    {
        return iccid;
    }

    public void setIccid(String iccid)
    {
        this.iccid = iccid;
    }

    public String getImei()
    {
        return imei;
    }

    public void setImei(String imei)
    {
        this.imei = imei;
    }

    public String getLastTimeHeard()
    {
        return lastTimeHeard;
    }

    public void setLastTimeHeard(String lastTimeHeard)
    {
        this.lastTimeHeard = lastTimeHeard;
    }

    public String getLastCommunicationTime()
    {
        return lastCommunicationTime;
    }

    public void setLastCommunicationTime(String lastCommunicationTime)
    {
        this.lastCommunicationTime = lastCommunicationTime;
    }

    public String getCurrentLifecycleState()
    {
        return currentLifecycleState;
    }

    public void setCurrentLifecycleState(String currentLifecycleState)
    {
        this.currentLifecycleState = currentLifecycleState;
    }

    public String getCarrier()
    {
        return carrier;
    }

    public void setCarrier(String carrier)
    {
        this.carrier = carrier;
    }
}
