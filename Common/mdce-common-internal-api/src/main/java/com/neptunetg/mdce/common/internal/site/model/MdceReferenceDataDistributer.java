/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.common.internal.site.model;


/**
 * MdceReferenceDataDistributer
 *
 *  "distributers": [{
 *    "customer_number": "7278500",
 *       "info": {
 *          ...
 *       }
 *    }]
 */
public class MdceReferenceDataDistributer
{

    private String customerNumber;

    private int infoId;
    private MdceReferenceDataInfo info;


    public int getInfoId()
    {
        return infoId;
    }

    public void setInfoId(int infoId)
    {
        this.infoId = infoId;
    }

    public String getCustomerNumber()
    {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber)
    {
        this.customerNumber = customerNumber;
    }

    public MdceReferenceDataInfo getInfo()
    {
        return info;
    }

    public void setInfo(MdceReferenceDataInfo info)
    {
        this.info = info;
    }
}
