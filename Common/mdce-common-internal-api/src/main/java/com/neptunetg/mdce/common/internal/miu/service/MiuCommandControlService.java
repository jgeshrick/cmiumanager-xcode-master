/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.common.internal.miu.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CmiuDescriptionViewModel;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandSummary;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Interface for internal API service for MIU command and control service excluding CMIU image updates.
 */
public interface MiuCommandControlService
{
    /**
     * URL of the service
     */
    String URL_CMIU_COMMAND = "/internal/command";
    String URL_CMIU_METADATA = "/internal/metadata";
    String URL_CMIU_COMMAND_FLUSH = "/internal/command/flush";
    String URL_CMIU_COMMAND_REJECT = "/internal/command/reject";

    String URL_COMMAND_IN_PROGRESS = "/internal/command/in-progress";
    String URL_COMMAND_RECEIVED = "/internal/command/received";
    String URL_COMMAND_COMPLETED = "/internal/command/completed";
    String URL_COMMAND_REJECTED = "/interal/command/rejected";

    String REQUEST_PARAM_CMIUID = "cmiuId";
    String REQUEST_PARAM_COMMANDID = "commandId";
    String REQUEST_PARAM_DAYS_AGO = "days-ago";

    /**
     * Send a command to the backend for processing and publishing to MQTT broker.
     * @param commandRequest containing the details of the command to be send to the backend
     */
    @RequestMapping(name=URL_CMIU_COMMAND, method=RequestMethod.POST)
    void sendCommand(CommandRequest commandRequest) throws InternalApiException;

    /**
     * Mark CMIU command as rejected.
     * @param commandId the command id
     */
    @RequestMapping(value=URL_CMIU_COMMAND_REJECT, method=RequestMethod.POST)
    void rejectCommand(@RequestParam(REQUEST_PARAM_COMMANDID) long commandId) throws InternalApiException;

    /**

     * Remove all commands that queue for a CMIU. This causes the RDS to change all commands that are in CREATED or QUEUED
     * state to RECALLED.
     * @param cmiuId the cmiu id
     * @return number of command affected
     */
    @RequestMapping(value=URL_CMIU_COMMAND_FLUSH, method=RequestMethod.POST)
    int flushCommand(@RequestParam(REQUEST_PARAM_CMIUID) long cmiuId) throws InternalApiException;

    /**
     * Get meta data for the cmiu.
     * @param cmiuId the cmiu.
     * @return meta data containing site id, cellular information, reporting and recording infos. Further information TBD.
     * @throws InternalApiException
     */
    @RequestMapping(value=URL_CMIU_METADATA, method=RequestMethod.GET)
    CmiuDescriptionViewModel getCmiuDescription(@RequestParam(REQUEST_PARAM_CMIUID) long cmiuId) throws InternalApiException;

    /**
     * Get a list of commands that are queued or created but not ACK/NACKed
     * @return list of command summary
     * @throws InternalApiException
     */
    @RequestMapping(value=URL_COMMAND_IN_PROGRESS, method=RequestMethod.GET)
    List<CommandSummary> getCommandsInProgress() throws InternalApiException;

    /**
     * Get a list of commands that have been ACKed but no detailed config packet
     * has been received to confirm an image update has been successful
     * @return list of command summary
     * @throws InternalApiException
     */
    @RequestMapping(value=URL_COMMAND_RECEIVED, method=RequestMethod.GET)
    List<CommandSummary> getCommandsReceived() throws InternalApiException;


    /**
     * Get a list of commands that has been completed ACK by the CMIU
     * @param daysAgo filter to show commands history to specified days ago
     * @return list of command summary
     * @throws InternalApiException
     */
    @RequestMapping(value=URL_COMMAND_COMPLETED, method=RequestMethod.GET)
    List<CommandSummary> getCommandsCompleted(@RequestParam(REQUEST_PARAM_DAYS_AGO) int daysAgo) throws InternalApiException;

    /**
     * Get a list of commands that has been NACKed by the CMIu
     * @return list of command summary
     * @throws InternalApiException
     */
    @RequestMapping(value=URL_COMMAND_REJECTED, method=RequestMethod.GET)
    List<CommandSummary> getCommandsRejected(@RequestParam(REQUEST_PARAM_DAYS_AGO) int daysAgo) throws InternalApiException;

}
