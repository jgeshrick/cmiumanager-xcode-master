/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.miu.model.imageupdate;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Contains command description and details for publishing a new command request.
 * Used to build a single command,  populated by mdce-web and consumed by mdce-integration services.
 */
public class CommandRequest
{
    //Type of command if CMIU
    private CommandTypeCmiu commandCmiu;

    //Type of command if L900
    private CommandTypeL900 commandL900;

    //Who send this commandCmiu
    private String user;

    //list of MIU to update, mandatory
    private List<Long> miuList;

    //time in seconds from unix epoch when the cmiu should perform the commandCmiu
    private long executionTimeEpochSecond;

    //Optional fields here
    private List<ImageDescription> imageDescription;  //for image update, contain a list of images and meta data

    //For modem firmware update (FOTA)
    private String fotaFileImageDescription;
    private String fotaFilename;

    //for Time and Date commandCmiu
    private long publishIntervalDataStartTime;
    private long publishIntervalDataEndTime;

    //for recording and reporting interval
    private int reportingStartMins;
    private int reportingIntervalHours;
    private int reportingRetries;
    private int reportingTransmitWindows;
    private int reportingQuietStart;
    private int reportingQuietEnd;
    private int recordingStartTime;
    private int recordingInterval;

    //for publish requested packet
    private PacketType packetType;

    //for L900 time offset
    private long timeOffset;


    public CommandTypeCmiu getCommandCmiu()
    {
        return commandCmiu;
    }

    public void setCommandCmiu(CommandTypeCmiu commandCmiu)
    {
        this.commandCmiu = commandCmiu;
    }

    public CommandTypeL900 getCommandL900()
    {
        return commandL900;
    }

    public void setCommandL900(CommandTypeL900 commandL900)
    {
        this.commandL900 = commandL900;
    }

    public List<Long> getMiuList()
    {
        return miuList;
    }

    public void setMiuList(List<Long> miuList)
    {
        this.miuList = miuList;
    }

    public long getExecutionTimeEpochSecond()
    {
        return executionTimeEpochSecond;
    }

    public void setExecutionTimeEpochSecond(long executionTimeEpochSecond)
    {
        this.executionTimeEpochSecond = executionTimeEpochSecond;
    }

    public List<ImageDescription> getImageDescription()
    {
        return imageDescription;
    }

    public void setImageDescription(List<ImageDescription> imageDescription)
    {
        this.imageDescription = imageDescription;
    }

    public String getFotaFilename()
    {
        return fotaFilename;
    }

    public void setFotaFilename(String fotaFilename)
    {
        this.fotaFilename = fotaFilename;
    }

    public String getFotaFileImageDescription()
    {
        return fotaFileImageDescription;
    }

    public void setFotaFileImageDescription(String fotaFileImageDescription)
    {
        this.fotaFileImageDescription = fotaFileImageDescription;
    }

    public long getPublishIntervalDataStartTime()
    {
        return publishIntervalDataStartTime;
    }

    public void setPublishIntervalDataStartTime(long publishIntervalDataStartTime)
    {
        this.publishIntervalDataStartTime = publishIntervalDataStartTime;
    }

    public long getPublishIntervalDataEndTime()
    {
        return publishIntervalDataEndTime;
    }

    public void setPublishIntervalDataEndTime(long publishIntervalDataEndTime)
    {
        this.publishIntervalDataEndTime = publishIntervalDataEndTime;
    }

    public int getReportingStartMins()
    {
        return reportingStartMins;
    }

    public void setReportingStartMins(int reportingStartMins)
    {
        this.reportingStartMins = reportingStartMins;
    }

    public int getReportingIntervalHours()
    {
        return reportingIntervalHours;
    }

    public void setReportingIntervalHours(int reportingIntervalHours)
    {
        this.reportingIntervalHours = reportingIntervalHours;
    }

    public int getReportingRetries()
    {
        return reportingRetries;
    }

    public void setReportingRetries(int reportingRetries)
    {
        this.reportingRetries = reportingRetries;
    }

    public int getReportingTransmitWindows()
    {
        return reportingTransmitWindows;
    }

    public void setReportingTransmitWindows(int reportingTransmitWindows)
    {
        this.reportingTransmitWindows = reportingTransmitWindows;
    }

    public int getReportingQuietStart()
    {
        return reportingQuietStart;
    }

    public void setReportingQuietStart(int reportingQuietStart)
    {
        this.reportingQuietStart = reportingQuietStart;
    }

    public int getReportingQuietEnd()
    {
        return reportingQuietEnd;
    }

    public void setReportingQuietEnd(int reportingQuietEnd)
    {
        this.reportingQuietEnd = reportingQuietEnd;
    }

    public int getRecordingStartTime()
    {
        return recordingStartTime;
    }

    public void setRecordingStartTime(int recordingStartTime)
    {
        this.recordingStartTime = recordingStartTime;
    }

    public int getRecordingInterval()
    {
        return recordingInterval;
    }

    public void setRecordingInterval(int recordingInterval)
    {
        this.recordingInterval = recordingInterval;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public PacketType getPacketType()
    {
        return packetType;
    }

    public void setPacketType(PacketType packetType)
    {
        this.packetType = packetType;
    }

    public long getTimeOffset()
    {
        return timeOffset;
    }

    public void setTimeOffset(long timeOffset)
    {
        this.timeOffset = timeOffset;
    }

    public enum MiuType
    {
        CMIU(1, "CMIU"),
        L900(2, "L900"),
        R900(50, "R900");

        private static final Map<Integer, MiuType> LOOKUP_TABLE = new HashMap<>();

        static
        {
            LOOKUP_TABLE.put(null, null);

            for(MiuType miuType : EnumSet.allOf(MiuType.class))
            {
                LOOKUP_TABLE.put(miuType.getId(), miuType);
            }

        }

        private final int id;
        private final String title;

        MiuType(int id, String title)
        {
            this.id = id;
            this.title = title;
        }

        int getId() { return id; }

        String getTitle() { return title; }

        public static MiuType fromId(Integer id) { return LOOKUP_TABLE.get(id); }

    }

    public interface CommandType
    {
       String getCommandDescription();

       static CommandType fromCommandNumber(MiuType miuType, int commandNumber)
       {
           switch( miuType )
           {
               case L900:
                   return CommandTypeL900.fromCommandNumber(commandNumber);

               case CMIU:
                   return CommandTypeCmiu.fromCommandNumber(commandNumber);
           }

           return new CommandTypeUnknown();
       }

        static CommandType fromCommandNumber(Integer miuTypeId, Integer commandNumber)
        {
            MiuType miuType = MiuType.fromId(miuTypeId);

            if( miuType != null && commandNumber != null ) {

                return fromCommandNumber(miuType, commandNumber);

            } else
            {
                return new CommandTypeUnknown();
            }
        }

        class CommandTypeUnknown implements CommandType
        {
            public String getCommandDescription()  { return "Unknown command"; }
        }

    }



    //Enum for all commands
    public enum CommandTypeCmiu implements CommandType
    {
        COMMAND_UPDATE_IMAGE(0, "Image update"),
        COMMAND_REBOOT_CMIU(1, "Reboot CMIU"),
        COMMAND_MAG_SWIPE_EMULATION(2, "Emulate Mag swipe"),
        COMMAND_ERASE_DATALOG(6, "Erase Datalog"),
        COMMAND_SET_RECORDING_REPORTING_INTERVALS(10, "Set Recording/Reporting Intervals"),
        COMMAND_PUBLISH_REQUESTED_PACKET(11, "Publish Requested Packet"),
        COMMAND_MODEM_FOTA(32, "Update modem firmware with FOTA"),

        //TODO: there are other new commands in Cmiu data and Packet definitions

        COMMAND_UNKNOWN(0xff, "Unknown command");

        private final int commandNumber;
        private final String commandDescription;

        CommandTypeCmiu(int commandNumber, String commandDescription)
        {
            this.commandNumber = commandNumber;
            this.commandDescription = commandDescription;
        }

        public int getCommandNumber()
        {
            return this.commandNumber;
        }

        public String getCommandDescription()
        {
            return commandDescription;
        }

        /**
         * Get enum from number
         * @param commandNumber
         * @return
         */
        public static CommandTypeCmiu fromCommandNumber(int commandNumber)
        {
            for(CommandTypeCmiu commandTypeCmiu : CommandTypeCmiu.values())
            {
                if (commandTypeCmiu.getCommandNumber() == commandNumber)
                {
                    return commandTypeCmiu;
                }
            }

            return CommandTypeCmiu.COMMAND_UNKNOWN; //unknown commandCmiu
        }
    }

    public enum CommandTypeL900 implements CommandType
    {
        COMMAND_RESET(0, "Reset"),
        COMMAND_TIME_OFFSET(1, "Set time offset"),
        COMMAND_DEACTIVATE(2, "Deactivate"),
        COMMAND_UNKNOWN(0xff, "Unknown command");

        private final int commandNumber;
        private final String commandDescription;

        private CommandTypeL900(int commandNumber, String commandDescription)
        {
            this.commandNumber = commandNumber;
            this.commandDescription = commandDescription;
        }

        public int getCommandNumber()
        {
            return this.commandNumber;
        }

        public String getCommandDescription()
        {
            return commandDescription;
        }

        /**
         * Get enum from number
         * @param commandNumber
         * @return
         */
        public static CommandTypeL900 fromCommandNumber(int commandNumber)
        {
            for(CommandTypeL900 commandTypeL900 : CommandTypeL900.values())
            {
                if (commandTypeL900.getCommandNumber() == commandNumber)
                {
                    return commandTypeL900;
                }
            }

            return CommandTypeL900.COMMAND_UNKNOWN; //unknown commandL900
        }
    }

    //Enum for packets types
    public enum PacketType
    {
        DETAILED_CONFIG_PACKET(0, "Detailed Config"),
        INTERVAL_DATA_PACKET(1, "Interval Data"),
        BASIC_CONFIG_PACKET(2, "Basic Config"),
        DEBUG_STRING_PACKET(8, "Debug String"),
        DEBUG_LOG_PACKET(9, "Debug Log");

        private final int packetNumber;
        private final String packetDescription;

        private PacketType(int packetNumber, String packetDescription)
        {
            this.packetNumber = packetNumber;
            this.packetDescription = packetDescription;
        }

        public int getPacketNumber()
        {
            return this.packetNumber;
        }

        public String getPacketDescription()
        {
            return this.packetDescription;
        }

        /**
         * Get enum from number
         * @param packetNumber
         * @return
         */
        public static PacketType fromPacketNumber(int packetNumber)
        {
            for(PacketType packetType: PacketType.values())
            {
                if (packetType.getPacketNumber() == packetNumber)
                {
                    return packetType;
                }
            }

            return null;
        }
    }

}
