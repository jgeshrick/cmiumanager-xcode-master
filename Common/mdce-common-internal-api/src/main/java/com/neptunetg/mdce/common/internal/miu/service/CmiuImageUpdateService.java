/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.miu.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * Interface for internal API service for CMIU image udpate
 */
public interface CmiuImageUpdateService
{
    /**
     * URL of the service
     */
    String URL_CMIU_IMAGE = "/internal/miu-config/image";
    String URL_CMIU_BUILD_ARTIFACT = "/internal/miu-config/build-artifact";

    String REQUEST_PARAM_CMIUID = "cmiuId";
    String REQUEST_PARAM_IMAGE_TYPE = "imageType";
    String REQUEST_PARAM_NAME = "name";

    /**
     * Get a list of all available image matching the imageType
     * @return
     * @throws InternalApiException
     */
    @RequestMapping(value = URL_CMIU_IMAGE, params = REQUEST_PARAM_IMAGE_TYPE)
    List<ImageDescription> getAvailableImageList(@RequestParam(REQUEST_PARAM_IMAGE_TYPE) ImageDescription.ImageType imageType) throws InternalApiException;

    /**
     * Get a list of all available image from the RDS
     * @return
     * @throws InternalApiException
     */
    @RequestMapping(URL_CMIU_IMAGE)
    List<ImageDescription> getAvailableImageList() throws InternalApiException;

    /**
     * Get a list of available build artifact (from S3)for purpose of selection/importing into the RDS
     * @param imageType    the type of image to retrieve, if null, all imageTypes are returned
     * @return a list of available artifacts grouped by imageType
     */
    @RequestMapping(value = URL_CMIU_BUILD_ARTIFACT)
    Map<ImageDescription.ImageType, List<String>> getBuildArtifactList(@RequestParam(REQUEST_PARAM_IMAGE_TYPE) ImageDescription.ImageType imageType) throws InternalApiException;

    /**
     * Register a build architact to be used in future image
     */
    @RequestMapping(value = URL_CMIU_BUILD_ARTIFACT, method=RequestMethod.POST)
    void registerBuildArtifact(@RequestParam(REQUEST_PARAM_IMAGE_TYPE) ImageDescription.ImageType imageType, @RequestParam(REQUEST_PARAM_NAME)String filePath) throws InternalApiException;

}
