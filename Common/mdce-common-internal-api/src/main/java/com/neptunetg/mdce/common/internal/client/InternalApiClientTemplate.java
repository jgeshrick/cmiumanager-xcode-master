/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.common.internal.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.InternalApiExceptionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

/**
 * Makes calls to internal API and deals with error responses
 */
public class InternalApiClientTemplate
{

    private final RestTemplate restTemplate = new RestTemplate();
    private final ObjectMapper jsonMapper = new ObjectMapper();

    private static final Logger logger = LoggerFactory.getLogger(InternalApiClientTemplate.class);

    /**
     * Make a call to a server using the internal API and parse the response
     * @param serverId Server name for debugging
     * @param restUrl URL to request
     * @param returnTypeClass Desired type for response
     * @param <T> Desired type for response
     * @return The object
     * @throws InternalApiException if something went wrong
     */
    public <T> T get(String serverId, URI restUrl, Class<T> returnTypeClass) throws InternalApiException
    {
        try
        {
            return restTemplate.getForObject(restUrl, returnTypeClass);
        }
        catch (HttpStatusCodeException e)
        {
            if (e.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR))
            {
                InternalApiExceptionResponse response;
                try
                {
                    //see if we can parse the response body as a InternalApiExceptionResponse
                    response = jsonMapper.readValue(e.getResponseBodyAsByteArray(), InternalApiExceptionResponse.class);
                }
                catch (Exception e2)
                {
                    final String message ="HTTP response code " + e.getStatusCode().value() + " occurred in internal API request from server " + serverId + "" +
                            " using URL " + restUrl;
                    logger.error(message, e);
                    logger.warn("Could not deserialize response either: " + e.getResponseBodyAsString(), e2);
                    throw new InternalApiException(message, e);
                }
                final String detailedMessage = "Remote error processing internal API request on server " + serverId + " using URL " +
                        restUrl + "; message was: " + response.getMessage() + ";\nRemote stack trace:\n" + response.getStackTrace();
                logger.error(detailedMessage, e);
                throw new InternalApiException(response.getMessage(), e);
            }
            else
            {
                final String message ="HTTP response code " + e.getStatusCode().value() + " occurred in internal API request from server " + serverId + "" +
                        " using URL " + restUrl;
                logger.error(message, e);
                throw new InternalApiException(message, e);
            }
        }
        catch (Exception e)
        {
            final String message ="Error occurred in internal API request from server " + serverId + "" +
                    " using URL " + restUrl;
            logger.error(message, e);
            throw new InternalApiException(message, e);

        }
    }

    /**
     * Post data to REST server and parse return response.
     * @param serverId Server name for debugging
     * @param restUrl URL to request
     * @param postObject      the object to post
     * @param returnTypeClass Desired type for response
     * @param <T> Desired type for response
     * @return The object
     * @throws InternalApiException if something went wrong
     */
    public <T> T post(String serverId, URI restUrl, Object postObject, Class<T> returnTypeClass) throws InternalApiException
    {
        try
        {
            return restTemplate.postForObject(restUrl, postObject, returnTypeClass);
        }
        catch (HttpStatusCodeException e)
        {
            if (e.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR))
            {
                InternalApiExceptionResponse response;
                try
                {
                    //see if we can parse the response body as a InternalApiExceptionResponse
                    response = jsonMapper.readValue(e.getResponseBodyAsByteArray(), InternalApiExceptionResponse.class);
                }
                catch (Exception e2)
                {
                    final String message = "HTTP response code " + e.getStatusCode().value() + " occurred in internal API request from server " + serverId + "" +
                            " using URL " + restUrl;
                    logger.error(message, e);
                    logger.warn("Could not deserialize response either: " + e.getResponseBodyAsString(), e2);
                    throw new InternalApiException(message, e);
                }
                final String detailedMessage = "Remote error processing internal API request on server " + serverId + " using URL " +
                        restUrl + "; message was: " + response.getMessage() + ";\nRemote stack trace:\n" + response.getStackTrace();
                logger.error(detailedMessage, e);
                throw new InternalApiException(response.getMessage(), e);
            }
            else
            {
                final String message = "HTTP response code " + e.getStatusCode().value() + " occurred in internal API request from server " + serverId + "" +
                        " using URL " + restUrl;
                logger.error(message, e);
                throw new InternalApiException(message, e);
            }
        }
    }



}
