/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.email.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.email.model.EmailMessage;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Provides methods for sending emails and, when operating in debug mode, retrieving outbound emails.
 */
public interface EmailService
{
    String URL_EMAIL_SERVICE_SEND_EMAIL = "/internal/email/send-email";
    String URL_EMAIL_SERVICE_GET_DEBUG_MODE_EMAIL = "/internal/email/debug-mode-email";

    /**
     * Sends an email using supplied content.
     * @param emailMessage The email message to be sent.
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value = URL_EMAIL_SERVICE_SEND_EMAIL, method = RequestMethod.POST)
    void sendEmail(EmailMessage emailMessage) throws InternalApiException;

    /**
     * When in integration test mode, gets the most recent email message that would have been sent by MDCE and then
     * clears the message.
     * @return The most recently generated email message.
     * @throws InternalApiException In case of internal errors.
     */
    @RequestMapping(value = URL_EMAIL_SERVICE_GET_DEBUG_MODE_EMAIL, method = RequestMethod.GET)
    EmailMessage getDebugModeEmail() throws InternalApiException;
}
