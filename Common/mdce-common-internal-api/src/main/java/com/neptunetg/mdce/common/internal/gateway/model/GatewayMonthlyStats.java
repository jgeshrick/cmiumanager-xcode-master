/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.gateway.model;

/**
 * Created by WJD1 on 04/02/2016.
 * Used to store gateway monthly stats row from SQL
 */
public class GatewayMonthlyStats
{
    private String gatewayId;
    private String year;
    private String month;
    private int numExpected;
    private int numHeard;
    private float avgMiusHeard;
    private int minMiusHeard;
    private int maxMiusHeard;
    private float avgOokPacketsHeard;
    private int minOokPacketsHeard;
    private int maxOokPacketsHeard;
    private float avgFskPacketsHeard;
    private int minFskPacketsHeard;
    private int maxFskPacketsHeard;
    private float avgConfigPacketsHeard;
    private int minConfigPacketsHeard;
    private int maxConfigPacketsHeard;

    public String getGatewayId()
    {
        return gatewayId;
    }

    public void setGatewayId(String gatewayId)
    {
        this.gatewayId = gatewayId;
    }

    public String getYear()
    {
        return year;
    }

    public void setYear(String year)
    {
        this.year = year;
    }

    public String getMonth()
    {
        return month;
    }

    public void setMonth(String month)
    {
        this.month = month;
    }

    public int getNumExpected()
    {
        return numExpected;
    }

    public void setNumExpected(int numExpected)
    {
        this.numExpected = numExpected;
    }

    public int getNumHeard()
    {
        return numHeard;
    }

    public void setNumHeard(int numHeard)
    {
        this.numHeard = numHeard;
    }

    public float getAvgMiusHeard()
    {
        return avgMiusHeard;
    }

    public void setAvgMiusHeard(float avgMiusHeard)
    {
        this.avgMiusHeard = avgMiusHeard;
    }

    public int getMinMiusHeard()
    {
        return minMiusHeard;
    }

    public void setMinMiusHeard(int minMiusHeard)
    {
        this.minMiusHeard = minMiusHeard;
    }

    public int getMaxMiusHeard()
    {
        return maxMiusHeard;
    }

    public void setMaxMiusHeard(int maxMiusHeard)
    {
        this.maxMiusHeard = maxMiusHeard;
    }

    public float getAvgOokPacketsHeard()
    {
        return avgOokPacketsHeard;
    }

    public void setAvgOokPacketsHeard(float avgOokPacketsHeard)
    {
        this.avgOokPacketsHeard = avgOokPacketsHeard;
    }

    public int getMinOokPacketsHeard()
    {
        return minOokPacketsHeard;
    }

    public void setMinOokPacketsHeard(int minOokPacketsHeard)
    {
        this.minOokPacketsHeard = minOokPacketsHeard;
    }

    public int getMaxOokPacketsHeard()
    {
        return maxOokPacketsHeard;
    }

    public void setMaxOokPacketsHeard(int maxOokPacketsHeard)
    {
        this.maxOokPacketsHeard = maxOokPacketsHeard;
    }

    public float getAvgFskPacketsHeard()
    {
        return avgFskPacketsHeard;
    }

    public void setAvgFskPacketsHeard(float avgFskPacketsHeard)
    {
        this.avgFskPacketsHeard = avgFskPacketsHeard;
    }

    public int getMinFskPacketsHeard()
    {
        return minFskPacketsHeard;
    }

    public void setMinFskPacketsHeard(int minFskPacketsHeard)
    {
        this.minFskPacketsHeard = minFskPacketsHeard;
    }

    public int getMaxFskPacketsHeard()
    {
        return maxFskPacketsHeard;
    }

    public void setMaxFskPacketsHeard(int maxFskPacketsHeard)
    {
        this.maxFskPacketsHeard = maxFskPacketsHeard;
    }

    public float getAvgConfigPacketsHeard()
    {
        return avgConfigPacketsHeard;
    }

    public void setAvgConfigPacketsHeard(float avgConfigPacketsHeard)
    {
        this.avgConfigPacketsHeard = avgConfigPacketsHeard;
    }

    public int getMinConfigPacketsHeard()
    {
        return minConfigPacketsHeard;
    }

    public void setMinConfigPacketsHeard(int minConfigPacketsHeard)
    {
        this.minConfigPacketsHeard = minConfigPacketsHeard;
    }

    public int getMaxConfigPacketsHeard()
    {
        return maxConfigPacketsHeard;
    }

    public void setMaxConfigPacketsHeard(int maxConfigPacketsHeard)
    {
        this.maxConfigPacketsHeard = maxConfigPacketsHeard;
    }
}
