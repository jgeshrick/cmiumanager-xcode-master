/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.common.internal.site.model;

import java.util.ArrayList;
import java.util.List;

/**
 * MdceReferenceDataInfo
 *
 * "info": {
 *  "account_id": "A6UJ9A000RJV",
 *  "account_name": "Hidden Valley Ranch Assoc.",
 *  "address1": "P.O. Box 4490",
 *  "address2": "NULL",
 *  "address3": "NULL",
 *  "city": "Pagosa Springs",
 *  "state": "CO",
 *  "postal_code": "81157",
 *  "country": "Usa",
 *  "main_phone": "NULL",
 *  "fax": "NULL",
 *  "owner": "37062",
 *  "account_manager": "Mike Lavin",
 *  "regional_manager": "Jeff Olsen",
 *  "type": "Private - Non-Water",
 *   "sub_type": "NULL",
 *   "status": "Active",
 *   "primary_contact": {
 *    "name": "NULL, NULL",
 *    "work_phone": "NULL",
 *    "title": "NULL"
 *   },
 *  "parent_customer_number": "7277400"
 * }
 */
public class MdceReferenceDataInfo
{

    private String accountId;

    private String systemId;

    private String customerNumber;

    private String accountName;

    private String address1;

    private String address2;

    private String address3;

    private String city;

    private String state;

    private String postalCode;

    private String country;

    private String mainPhone;

    private String fax;

    private String salesTerritoryOwner;

    private String accountManager;

    private String regionalManager;

    private String type;

    private String subType;

    private String status;

    private int primaryContactId;
    private MdceReferenceDataInfoContact primaryContact;

    private List<Integer> customerNumberIds = new ArrayList<>();
    private List<String> customerNumbers = new ArrayList<>();

    private List<Integer> secondaryContactIds = new ArrayList<>();

    private String parentCustomerNumber;
    private List<MdceReferenceDataInfoContact> activeSecondaryContacts;

    public List<MdceReferenceDataInfoContact> getActiveSecondaryContacts()
    {
        return activeSecondaryContacts;
    }

    public void setActiveSecondaryContacts(List<MdceReferenceDataInfoContact> activeSecondaryContacts)
    {
        this.activeSecondaryContacts = activeSecondaryContacts;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getSystemId()
    {
        return systemId;
    }

    public void setSystemId(String systemId)
    {
        this.systemId = systemId;
    }

    public String getCustomerNumber()
    {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber)
    {
        this.customerNumber = customerNumber;
    }

    public String getAccountName()
    {
        return accountName;
    }

    public void setAccountName(String accountName)
    {
        this.accountName = accountName;
    }

    public String getAddress1()
    {
        return address1;
    }

    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    public String getAddress2()
    {
        return address2;
    }

    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    public String getAddress3()
    {
        return address3;
    }

    public void setAddress3(String address3)
    {
        this.address3 = address3;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getMainPhone()
    {
        return mainPhone;
    }

    public void setMainPhone(String mainPhone)
    {
        this.mainPhone = mainPhone;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax = fax;
    }

    public String getSalesTerritoryOwner()
    {
        return salesTerritoryOwner;
    }

    public void setSalesTerritoryOwner(String salesTerritoryOwner)
    {
        this.salesTerritoryOwner = salesTerritoryOwner;
    }

    public String getAccountManager()
    {
        return accountManager;
    }

    public void setAccountManager(String accountManager)
    {
        this.accountManager = accountManager;
    }

    public String getRegionalManager()
    {
        return regionalManager;
    }

    public void setRegionalManager(String regionalManager)
    {
        this.regionalManager = regionalManager;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getSubType()
    {
        return subType;
    }

    public void setSubType(String subType)
    {
        this.subType = subType;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public MdceReferenceDataInfoContact getPrimaryContact()
    {
        return primaryContact;
    }

    public void setPrimaryContact(MdceReferenceDataInfoContact primaryContact)
    {
        this.primaryContact = primaryContact;
    }

    public int getPrimaryContactId()
    {
        return primaryContactId;
    }

    public void setPrimaryContactId(int primaryContactId)
    {
        this.primaryContactId = primaryContactId;
    }

    public String getParentCustomerNumber()
    {
        return parentCustomerNumber;
    }

    public void setParentCustomerNumber(String parentCustomerNumber)
    {
        this.parentCustomerNumber = parentCustomerNumber;
    }

    public List<Integer> getCustomerNumberIds()
    {
        return customerNumberIds;
    }

    public List<String> getCustomerNumbers()
    {
        return customerNumbers;
    }

    public void setCustomerNumbers(List<String> customerNumbers)
    {
        this.customerNumbers = customerNumbers;
    }
}