/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal;

import java.util.List;

public abstract class PagedResult<T>
{

    Integer totalResults;
    Integer numResults;
    Integer pageNum;

    List<T> results;

    public PagedResult()
    {
    }

    public PagedResult(Integer totalResults, Integer numResults, Integer page, List<T> results)
    {
        this.totalResults = totalResults;
        this.numResults = numResults;
        this.pageNum = page;
        this.results = results;
    }

    public Integer getTotalResults()
    {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults)
    {
        this.totalResults = totalResults;
    }

    public Integer getNumResults()
    {
        return numResults;
    }

    public void setNumResults(Integer numResults)
    {
        this.numResults = numResults;
    }

    public Integer getPageNum()
    {
        return pageNum;
    }

    public void setPageNum(Integer pageNum)
    {
        this.pageNum = pageNum;
    }

    public List<T> getResults()
    {
        return results;
    }

    public void setResults(List<T> results)
    {
        this.results = results;
    }
}
