/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.common.internal.client;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Information about a back end
 */
public class InternalApiClientConfig implements Serializable
{

    private final String name;

    private final URL url;

    /**
     * Create an object representing a back end
     * @param name Unique name of back end service
     * @param url URL of back end service
     * @throws MalformedURLException if URL is bad
     */
    public InternalApiClientConfig(String name, String url) throws MalformedURLException
    {
        this.name = name;

        this.url = new URL(url);
    }

    /**
     * Get the unique name of the service
     * @return The name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Get the URL of the service
     * @return The URL
     */
    public URL getUrl()
    {
        return url;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InternalApiClientConfig that = (InternalApiClientConfig) o;

        return !(name != null ? !name.equals(that.name) : that.name != null);

    }

    @Override
    public int hashCode()
    {
        return name != null ? name.hashCode() : 0;
    }
}
