/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.alert.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLog;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Service for providing access to alerts
 */
@RestController
@RequestMapping("/service")
public interface AlertRestService
{
    String URL_ALERT_LIST_NEW = "/internal/alert/list/new";
    String URL_ALERT_LIST_HANDLED = "/internal/alert/list/handled";
    String URL_ALERT_LIST_STALE = "/internal/alert/list/stale";
    String URL_ALERT_LIST_CLEARED = "/internal/alert/list/cleared";
    String URL_ALERT_SINGLE = "/internal/alert/single";

    String URL_ALERT_ADD = "/internal/alert/add";

    String URL_ALERT_SET_NEW = "/internal/alert/set/new";
    String URL_ALERT_SET_HANDLED = "/internal/alert/set/handled";
    String URL_ALERT_SET_STALE = "/internal/alert/set/stale";
    String URL_ALERT_SET_CLEARED = "/internal/alert/set/cleared";
    String URL_ALERT_SET_WARNING = "/internal/alert/set/warning";
    String URL_ALERT_SET_ERROR = "/internal/alert/set/error";
    String URL_ALERT_SET_TICKET_ID = "/internal/alert/set/ticketid";

    String URL_ALERT_ADD_MESSAGE = "/internal/alert/add/message";

    String URL_ALERT_GET_LOG = "/internal/alert/get/log";


    @RequestMapping(value=URL_ALERT_LIST_NEW, method=RequestMethod.POST)
    List<AlertDetails> getNewAlertList(@RequestBody String alertSource) throws InternalApiException;

    @RequestMapping(value=URL_ALERT_LIST_HANDLED, method=RequestMethod.POST)
    List<AlertDetails> getHandledAlertList(@RequestBody String alertSource) throws InternalApiException;

    @RequestMapping(value=URL_ALERT_LIST_STALE, method=RequestMethod.POST)
    List<AlertDetails> getStaleAlertList(@RequestBody String alertSource) throws InternalApiException;

    @RequestMapping(value=URL_ALERT_LIST_CLEARED, method=RequestMethod.POST)
    List<AlertDetails> getClearedAlertList(@RequestBody String alertSource) throws InternalApiException;

    @RequestMapping(value=URL_ALERT_SINGLE, method=RequestMethod.POST)
    AlertDetails getAlert(@RequestBody Long alertId) throws InternalApiException;

    @RequestMapping(value=URL_ALERT_ADD, method=RequestMethod.POST)
    boolean addAlert(@RequestBody AlertDetails alertDetails) throws InternalApiException;


    @RequestMapping(value=URL_ALERT_SET_NEW, method=RequestMethod.POST)
    boolean setAlertStateToNew(Long alertId) throws InternalApiException;

    @RequestMapping(value=URL_ALERT_SET_HANDLED, method=RequestMethod.POST)
    boolean setAlertStateToHandled(Long alertId) throws InternalApiException;

    @RequestMapping(value=URL_ALERT_SET_STALE, method=RequestMethod.POST)
    boolean setAlertStateToStale(Long alertId) throws InternalApiException;

    @RequestMapping(value=URL_ALERT_SET_CLEARED, method=RequestMethod.POST)
    boolean setAlertStateToCleared(Long alertId) throws InternalApiException;

    @RequestMapping(value=URL_ALERT_SET_WARNING, method=RequestMethod.POST)
    boolean setAlertLevelToWarning(Long alertId) throws InternalApiException;

    @RequestMapping(value=URL_ALERT_SET_ERROR, method=RequestMethod.POST)
    boolean setAlertLevelToError(Long alertId) throws InternalApiException;

    @RequestMapping(value=URL_ALERT_SET_TICKET_ID, method=RequestMethod.POST)
    boolean setAlertTicketId(@RequestBody AlertDetails alertDetails) throws InternalApiException;


    @RequestMapping(value=URL_ALERT_ADD_MESSAGE, method=RequestMethod.POST)
    boolean addAlertLogMessage(@RequestBody AlertDetails alertDetails) throws InternalApiException;


    @RequestMapping(value=URL_ALERT_GET_LOG, method=RequestMethod.POST)
    List<AlertLog> getAlertLog(@RequestBody Long alertId) throws InternalApiException;
}
