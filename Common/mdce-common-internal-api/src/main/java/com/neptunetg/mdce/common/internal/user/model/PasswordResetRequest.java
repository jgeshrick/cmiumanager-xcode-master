/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.user.model;

import java.security.SecureRandom;
import java.time.Instant;

/**
 * Created by MC5 on 06/10/2016.
 * A request to reset a user's password.
 */
public class PasswordResetRequest
{
    private static final String TOKEN_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static final int TOKEN_LENGTH = 32;

    private static SecureRandom secureRandom = new SecureRandom();

    private long passwordResetRequestId;
    /**
     * The unique, single-use token that allows the password reset to be claimed.
     */
    private String token;
    private Integer userId;
    /**
     * The username that was matched to the supplied email address at the time of entry.
     */
    private String matchedUserName;
    /**
     * The email address exactly as entered by the requestor.
     */
    private String enteredEmailAddress;
    private Instant createdTimestamp;
    private Instant validUntilTimestamp;
    private String requestorIpAddress;
    private boolean claimed;
    private Instant claimedTimestamp;
    private String claimantIpAddress;

    /**
     * Generates a new 32-character, secure-random alphanumeric string for use as a unique password reset token.
     * @return A new password reset token.
     */
    public static String generateToken()
    {
        StringBuilder sb = new StringBuilder(TOKEN_LENGTH);

        for (int i = 0; i < TOKEN_LENGTH; i++)
        {
            sb.append(TOKEN_CHARS.charAt(secureRandom.nextInt(TOKEN_CHARS.length())));
        }

        return sb.toString();
    }

    public long getPasswordResetRequestId()
    {
        return passwordResetRequestId;
    }

    public void setPasswordResetRequestId(long passwordResetRequestId)
    {
        this.passwordResetRequestId = passwordResetRequestId;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public Integer getUserId()
    {
        return userId;
    }

    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }

    public String getMatchedUserName()
    {
        return matchedUserName;
    }

    public void setMatchedUserName(String matchedUserName)
    {
        this.matchedUserName = matchedUserName;
    }

    public String getEnteredEmailAddress()
    {
        return enteredEmailAddress;
    }

    public void setEnteredEmailAddress(String enteredEmailAddress)
    {
        this.enteredEmailAddress = enteredEmailAddress;
    }

    public Instant getCreatedTimestamp()
    {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(Instant createdTimestamp)
    {
        this.createdTimestamp = createdTimestamp;
    }

    public Instant getValidUntilTimestamp()
    {
        return validUntilTimestamp;
    }

    public void setValidUntilTimestamp(Instant validUntilTimestamp)
    {
        this.validUntilTimestamp = validUntilTimestamp;
    }

    public String getRequestorIpAddress()
    {
        return requestorIpAddress;
    }

    public void setRequestorIpAddress(String requestorIpAddress)
    {
        this.requestorIpAddress = requestorIpAddress;
    }

    public boolean isClaimed()
    {
        return claimed;
    }

    public void setClaimed(boolean claimed)
    {
        this.claimed = claimed;
    }

    public Instant getClaimedTimestamp()
    {
        return claimedTimestamp;
    }

    public void setClaimedTimestamp(Instant claimedTimestamp)
    {
        this.claimedTimestamp = claimedTimestamp;
    }

    public String getClaimantIpAddress()
    {
        return claimantIpAddress;
    }

    public void setClaimantIpAddress(String claimantIpAddress)
    {
        this.claimantIpAddress = claimantIpAddress;
    }
}
