/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.common.internal.servicestatus.service;

import java.io.IOException;
import java.util.Properties;

/**
 * Provides access to the version of the library
 */
public class InternalApiLibraryVersionInfo
{
    /**
     * The build of the applications
     */
    public static final String LIBRARY_VERSION = initLibraryVersion();

    /**
     * Initialize the libary version
     * @return application version string read from Maven-filtered file
     */
    private static String initLibraryVersion() {
        Properties versionDetails = new Properties();
        try {
            versionDetails.load(InternalApiLibraryVersionInfo.class.getResourceAsStream("/internalApiLibraryVersion.properties"));
        } catch (IOException e) {
            throw new RuntimeException("Couldn't read library version!", e);
        }
        return versionDetails.getProperty("internalApiLibraryVersion");
    }
}
