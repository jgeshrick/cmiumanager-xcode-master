/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.user.model;

/**
 * Created by WJD1 on 12/08/2015.
 * Class to store details for modifying a users password
 */
public class ModifiedUserDetails
{
    private int userId;
    private String userName;
    private String previousUserName;
    private String password;
    private String email;
    private MdceUserRole userRole;
    private Boolean isAccountLockoutEnabled;
    private Boolean isCurrentlyDisabled;
    private Boolean allowConcurrentSessions;

    private String requester;   //who request this change

    public ModifiedUserDetails()
    {
        //Default constructor
    }

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPreviousUserName()
    {
        return previousUserName;
    }

    public void setPreviousUserName(String previousUserName)
    {
        this.previousUserName = previousUserName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getRequester()
    {
        return requester;
    }

    public void setRequester(String requester)
    {
        this.requester = requester;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public MdceUserRole getUserRole()
    {
        return userRole;
    }

    public void setUserRole(MdceUserRole userRole)
    {
        this.userRole = userRole;
    }

    public Boolean getAccountLockoutEnabled()
    {
        return isAccountLockoutEnabled;
    }

    public void setAccountLockoutEnabled(Boolean accountLockoutEnabled)
    {
        isAccountLockoutEnabled = accountLockoutEnabled;
    }

    public Boolean getCurrentlyDisabled()
    {
        return isCurrentlyDisabled;
    }

    public void setCurrentlyDisabled(Boolean currentlyDisabled)
    {
        isCurrentlyDisabled = currentlyDisabled;
    }

    public Boolean getAllowConcurrentSessions()
    {
        return allowConcurrentSessions;
    }

    public void setAllowConcurrentSessions(Boolean allowConcurrentSessions)
    {
        this.allowConcurrentSessions = allowConcurrentSessions;
    }
}
