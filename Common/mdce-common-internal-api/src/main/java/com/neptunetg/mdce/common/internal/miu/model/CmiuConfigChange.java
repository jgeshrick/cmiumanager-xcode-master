/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserve
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.internal.miu.model;

/**
 * Backing form for CMIU config commands page.
 */
public class CmiuConfigChange
{
    private int cmiuId;
    private long configSetId;
    private String changeNote;

    public CmiuConfigChange()
    {

    }

    public CmiuConfigChange(int cmiuId)
    {
        this.cmiuId = cmiuId;
    }

    public CmiuConfigChange(int cmiuId, long configSetId)
    {
        this.cmiuId = cmiuId;
        this.configSetId = configSetId;
    }

    public int getCmiuId()
    {
        return cmiuId;
    }

    public void setCmiuId(int cmiuId)
    {
        this.cmiuId = cmiuId;
    }

    public long getConfigSetId()
    {
        return configSetId;
    }

    public void setConfigSetId(long configSetId)
    {
        this.configSetId = configSetId;
    }

    public String getChangeNote()
    {
        return changeNote;
    }

    public void setChangeNote(String changeNote)
    {
        this.changeNote = changeNote;
    }
}
