/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.sim.model;

/**
 * Represents the lifecycle state of a SIM. See documentation under
 * http://gontg/ntgprojects/10065-1/Project%20Documents/Documents/10065-1_DO01_0014%20CMIU%20State%20Monitoring
 */
public enum SimLifecycleState
{
    /**
     * The SIM ICCID number has appeared in the network operators billing account.
     */
    UNASSIGNED("Unassigned"),
    /**
     * The ICCID is linked with a CMIU ID.
     */
    ASSIGNED("Assigned"),
    /**
     * VZW intermediate state once the contract is cancelled.
     */
    AGING("Aging"),
    /**
     * The SIM contract is terminated.
     */
    TERMINATED("Terminated"),
    /**
     * SIM not assigned to a CMIU, and either a timeout has been passed, or data usage has been detected.
     */
    ROGUE("Rogue");

    private final String sqlString;

    SimLifecycleState(String sqlEnumString)
    {
        this.sqlString = sqlEnumString;
    }

    public static SimLifecycleState fromString(String sqlEnumString)
    {
        for(SimLifecycleState state : SimLifecycleState.values())
        {
            if(state.getStringValue().equalsIgnoreCase(sqlEnumString))
            {
                return state;
            }
        }

        return null;
    }

    public String getStringValue()
    {
        return this.sqlString;
    }
}
