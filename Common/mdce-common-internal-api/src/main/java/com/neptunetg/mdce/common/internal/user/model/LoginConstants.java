/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.user.model;

public abstract class LoginConstants
{
    // add a ROLE prefix for Spring Security 4, see http://stackoverflow.com/questions/19525380/difference-between-role-and-grantedauthority-in-spring-security
    public static final String DISTRIBUTOR_ROLE = "ROLE_DISTRIBUTOR";
    public static final String SITE_OPERATOR_ROLE = "ROLE_SITE_OPERATOR";
    public static final String MSOC_VIEWER_ROLE = "ROLE_MSOC_VIEWER";
    public static final String MSOC_OPERATOR_ROLE = "ROLE_MSOC_OPERATOR";
    public static final String MSOC_ADMIN_ROLE = "ROLE_MSOC_ADMIN";
    public static final String MSOC_MASTER_OF_CELLULAR = "ROLE_MSOC_MASTER_OF_CELLULAR";

    public static final String IS_DISTRIBUTOR = "hasRole('" + DISTRIBUTOR_ROLE + "')";

    public static final String IS_SITE_OPERATOR = "hasRole('" + SITE_OPERATOR_ROLE + "')";

    public static final String IS_MSOC_VIEWER = "hasRole('" + MSOC_VIEWER_ROLE + "')";
    public static final String IS_MSOC_VIEWER_WHITELISTED = "hasRole('" + MSOC_VIEWER_ROLE +
                                                            "') and @whitelistedModeUtility.whitelistedMode()";

    public static final String IS_MSOC_OPERATOR = "hasRole('" + MSOC_OPERATOR_ROLE + "')";
    public static final String IS_MSOC_OPERATOR_WHITELISTED = "hasRole('" + MSOC_OPERATOR_ROLE +
                                                            "') and @whitelistedModeUtility.whitelistedMode()";

    public static final String IS_MSOC_ADMIN = "hasRole('" + MSOC_ADMIN_ROLE + "')";
    public static final String IS_MSOC_ADMIN_WHITELISTED = "hasRole('" + MSOC_ADMIN_ROLE +
                                                           "') and @whitelistedModeUtility.whitelistedMode()";

    public static final String IS_MSOC_MASTER_OF_CELLULAR = "hasRole('" + MSOC_MASTER_OF_CELLULAR + "')";
    public static final String IS_MSOC_MASTER_OF_CELLULAR_WHITELISTED = "hasRole('" + MSOC_MASTER_OF_CELLULAR +
                                                                        "') and @whitelistedModeUtility.whitelistedMode()";
}
