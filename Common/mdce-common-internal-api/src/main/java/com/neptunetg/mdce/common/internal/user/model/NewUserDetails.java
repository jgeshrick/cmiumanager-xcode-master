/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.user.model;

/**
 * Created by WJD1 on 12/08/2015.
 * Class to store data for a new user
 */
public class NewUserDetails
{
    private String userName;
    private String email;
    private String password;
    private MdceUserRole userLevel;
    private String requester;   //who request this change

    public NewUserDetails(String userName, String email, String password, MdceUserRole userLevel, String requester)
    {
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.userLevel = userLevel;
        this.requester = requester;
    }

    public NewUserDetails()
    {
        //Default constructor
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public MdceUserRole getUserLevel()
    {
        return userLevel;
    }

    public void setUserLevel(MdceUserRole userLevel)
    {
        this.userLevel = userLevel;
    }

    public String getRequester()
    {
        return requester;
    }

    public void setRequester(String requester)
    {
        this.requester = requester;
    }
}
