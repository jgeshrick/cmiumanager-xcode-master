/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.audit.model;

import java.time.Instant;

/**
 * POJO to contain audit log entries
 */
public class AuditLog
{
    private Instant dateTime;
    private String userName;
    private Long miuId;
    private String auditType;
    private String oldValue;
    private String newValue;

    public static String getCsvHeader()
    {
        return "Date, User, MIU, Audit Type, Old value, New value";
    }

    public Instant getDateTime()
    {
        return dateTime;
    }

    public void setDateTime(Instant dateTime)
    {
        this.dateTime = dateTime;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public Long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(Long miuId)
    {
        this.miuId = (miuId == null || miuId == 0)? null:miuId;
    }

    public String getAuditType()
    {
        return auditType;
    }

    public void setAuditType(String auditType)
    {
        this.auditType = auditType;
    }

    public String getOldValue()
    {
        return oldValue;
    }

    public void setOldValue(String oldValue)
    {
        this.oldValue = oldValue;
    }

    public String getNewValue()
    {
        return newValue;
    }

    public void setNewValue(String newValue)
    {
        this.newValue = newValue;
    }

    public String toCsv()
    {
        return String.format("%s, %s, %d, %s, %s, %s",
                dateTime.toString(), userName, miuId, auditType, oldValue, newValue);
    }

}
