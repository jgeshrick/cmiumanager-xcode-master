/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.internal.miu.model.imageupdate;

import java.time.Instant;

/**
 * Provide a summary of command for displaying to web
 */
public class CommandSummary
{
    private long commandId;
    private long miuId;
    private String user;
    private String commandType;
    private String details;
    private Instant commandEnteredTime;
    private Instant commandFulfilledTime;
    private String networkProvider;

    public long getCommandId()
    {
        return commandId;
    }

    public void setCommandId(long commandId)
    {
        this.commandId = commandId;
    }

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getCommandType()
    {
        return commandType;
    }

    public void setCommandType(String commandType)
    {
        this.commandType = commandType;
    }

    public String getDetails()
    {
        return details;
    }

    public void setDetails(String details)
    {
        this.details = details;
    }

    public Instant getCommandEnteredTime() { return commandEnteredTime; }

    public void setCommandEnteredTime(Instant commandEnteredTime) { this.commandEnteredTime = commandEnteredTime; }

    public Instant getCommandFulfilledTime() { return commandFulfilledTime; }

    public void setCommandFulfilledTime(Instant commandFulfilledTime) { this.commandFulfilledTime = commandFulfilledTime; }

    public String getNetworkProvider()
    {
        return networkProvider;
    }

    public void setNetworkProvider(String networkProvider)
    {
        this.networkProvider = networkProvider;
    }
}
