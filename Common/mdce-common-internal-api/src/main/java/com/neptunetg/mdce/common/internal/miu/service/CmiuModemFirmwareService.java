/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.miu.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.CmiuModemFirmwareDetails;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * Interface for internal API service for CMIU modem firmware updates
 */
public interface CmiuModemFirmwareService
{
    /**
     * URL of the service
     */
    String URL_CMIU_MODEM_FIRMWARE_LIST = "/internal/cmiu-modem-firmware/list";

    /**
     * Get a list of all available CMIU modem firmware images
     * @return
     * @throws com.neptunetg.mdce.common.internal.InternalApiException
     */
    @RequestMapping(value = URL_CMIU_MODEM_FIRMWARE_LIST)
    List<CmiuModemFirmwareDetails> getAvailableCmiuModemFirmwareImages() throws InternalApiException;
}
