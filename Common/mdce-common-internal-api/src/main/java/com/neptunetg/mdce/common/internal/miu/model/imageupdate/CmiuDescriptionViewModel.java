/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.internal.miu.model.imageupdate;

/**
 * Cmiu and meta data
 */
public class CmiuDescriptionViewModel
{
    private Long cmiuId;
    private String metaData;

    public CmiuDescriptionViewModel()
    {}

    public CmiuDescriptionViewModel(long cmiuId, String description)
    {
        this.cmiuId = cmiuId;
        this.metaData = description;
    }

    public Long getCmiuId()
    {
        return cmiuId;
    }

    public void setCmiuId(long cmiuId)
    {
        this.cmiuId = cmiuId;
    }

    public String getMetaData()
    {
        return metaData;
    }

    public void setMetaData(String metaData)
    {
        this.metaData = metaData;
    }
}
