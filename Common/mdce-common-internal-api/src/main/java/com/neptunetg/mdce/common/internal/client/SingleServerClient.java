/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLog;
import com.neptunetg.mdce.common.internal.alert.service.AlertRestService;
import com.neptunetg.mdce.common.internal.anaylsis.service.CmiuAnalysisService;
import com.neptunetg.mdce.common.internal.anaylsis.model.CmiuTimingAnalysisDetails;
import com.neptunetg.mdce.common.internal.audit.model.AuditLogPagedResult;
import com.neptunetg.mdce.common.internal.audit.service.AuditLogService;
import com.neptunetg.mdce.common.internal.dashboard.model.StatusCounts;
import com.neptunetg.mdce.common.internal.dashboard.service.MsrService;
import com.neptunetg.mdce.common.internal.diagnostics.service.CnsDumpService;
import com.neptunetg.mdce.common.internal.diagnostics.service.PacketDumpService;
import com.neptunetg.mdce.common.internal.email.model.EmailMessage;
import com.neptunetg.mdce.common.internal.email.service.EmailService;
import com.neptunetg.mdce.common.internal.gateway.model.GatewayDisplayDetailsPagedResult;
import com.neptunetg.mdce.common.internal.gateway.service.GatewayService;
import com.neptunetg.mdce.common.internal.lora.model.LoraDisplayDetails;
import com.neptunetg.mdce.common.internal.lora.service.LoraService;
import com.neptunetg.mdce.common.internal.miu.model.*;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CmiuDescriptionViewModel;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandSummary;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import com.neptunetg.mdce.common.internal.miu.service.*;
import com.neptunetg.mdce.common.internal.servicestatus.model.ServiceStatus;
import com.neptunetg.mdce.common.internal.servicestatus.service.ServiceStatusService;
import com.neptunetg.mdce.common.internal.settings.service.MdceSettingsService;
import com.neptunetg.mdce.common.internal.settings.service.MnoCallbackService;
import com.neptunetg.mdce.common.internal.sim.model.SimDeactivationDetails;
import com.neptunetg.mdce.common.internal.sim.model.SimDeactivationDisplayDetails;
import com.neptunetg.mdce.common.internal.sim.model.SimDisplayDetails;
import com.neptunetg.mdce.common.internal.sim.model.SimDisplayDetailsPagedResult;
import com.neptunetg.mdce.common.internal.sim.service.SimDeactivationService;
import com.neptunetg.mdce.common.internal.sim.service.SimService;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataDistributer;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataUtility;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.common.internal.site.model.SiteForUser;
import com.neptunetg.mdce.common.internal.site.service.RefService;
import com.neptunetg.mdce.common.internal.site.service.SiteService;
import com.neptunetg.mdce.common.internal.user.model.LoginCredentials;
import com.neptunetg.mdce.common.internal.user.model.LoginResult;
import com.neptunetg.mdce.common.internal.user.model.ModifiedUserDetails;
import com.neptunetg.mdce.common.internal.user.model.NewUserDetails;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequest;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequestClaim;
import com.neptunetg.mdce.common.internal.user.model.UserAlertSettings;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import com.neptunetg.mdce.common.internal.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Carries out synchronous requests to a single server
 */
public class SingleServerClient
        implements MsrService, ServiceStatusService, CmiuDetailsService,
        CmiuConfigMgmtService, CmiuConfigSetService, CmiuImageUpdateService,
        MiuCommandControlService, UserService, AlertRestService,
        AuditLogService, MiuService, GatewayService, SiteService, RefService,
        MdceSettingsService, CmiuModemFirmwareService, SimService,
        EmailService, LoraService, PacketDumpService, CnsDumpService,
        MnoCallbackService,SimDeactivationService,CmiuAnalysisService

{
    private static final Logger logger = LoggerFactory.getLogger(SingleServerClient.class);

    private final String serverId;
    private final URL baseUrl;
    private final InternalApiClientTemplate template = new InternalApiClientTemplate();

    public SingleServerClient(String serverId, URL baseUrl) throws InternalApiException
    {
        try
        {
            this.serverId = serverId;
            final String path = baseUrl.getPath();

            if (!path.endsWith("/"))
            {
                this.baseUrl = new URL(baseUrl, path + "/");
            }
            else
            {
                this.baseUrl = baseUrl;
            }

        }
        catch (MalformedURLException e)
        {
            throw new InternalApiException("Bad URL " + baseUrl, e);
        }
    }

    /**
     * Get a request URL for a resource
     *
     * @param relative            The path to the resource e.g. /internal/msr-by-group
     * @param paramNamesAndValues List of parameter name/value pairs
     * @return URL of resource as a URI object
     * @throws InternalApiException If the URI could not be formed
     */
    public URI requestUrl(String relative, String... paramNamesAndValues) throws InternalApiException
    {
        if (relative == null || (paramNamesAndValues.length & 1) != 0)
        {
            throw new IllegalArgumentException("Bad parameters: " + relative + ", query params of length " + paramNamesAndValues.length + " (must be even)");
        }

        try
        {
            final StringBuilder specialization = new StringBuilder(relative);

            if (relative.startsWith("/"))
            {
                specialization.delete(0, 1);
            }

            for (int paramIndex = 0; paramIndex < paramNamesAndValues.length; paramIndex += 2)
            {
                String paramName = paramNamesAndValues[paramIndex];
                String paramValue = urlEncode(paramNamesAndValues[paramIndex + 1]);

                specialization.append(paramIndex == 0 ? '?' : '&');
                specialization.append(paramName).append('=').append(paramValue);
            }

            return new URL(baseUrl, specialization.toString()).toURI();
        }
        catch (MalformedURLException | URISyntaxException | UnsupportedEncodingException e)
        {
            throw new InternalApiException("Could not add path " + relative + " to URL " + baseUrl, e);
        }
    }

    /**
     * Utility method to encode that is null-safe, unlike URLEncoder.encode()
     * @param paramValue Value to encode, which may be null
     * @return Encoded value.  "" if paramValue is null.
     * @throws UnsupportedEncodingException won't happen
     */
    private static String urlEncode(String paramValue) throws UnsupportedEncodingException
    {
        if (paramValue == null)
        {
            return "";
        }
        else
        {
            return URLEncoder.encode(paramValue, StandardCharsets.UTF_8.name());
        }
    }


    /**
     * Compose URI from options map
     * @param url
     * @param options
     */
    private URI restUrlFromOptionsMap(String url, Map<String, String> options) throws InternalApiException
    {
        List<String> params = new ArrayList<>();

        for (Map.Entry<String, String> entry : options.entrySet())
        {
            params.add(entry.getKey());
            params.add(entry.getValue());
        }

        String[] nameAndParams = params.toArray(new String[params.size()]);

        return requestUrl(url, nameAndParams);
    }

    /**
     * Get the MSR for a group of endpoints
     *
     * @param groupId Group ID identifying the endpoints
     * @return MSR stats
     * @throws InternalApiException In case we don't manage to the MSR from the server
     */
    @Override
    public StatusCounts getMsrByGroup(String groupId) throws InternalApiException
    {
        final URI restUrl = requestUrl(MsrService.URL_MSR_BY_GROUP,
                MsrService.PARAM_MSR_GROUP_ID, groupId);

        return template.get(serverId, restUrl, StatusCounts.class);


    }

    /**
     * Get the status of a service
     *
     * @return Information about the service status
     * @throws InternalApiException Only thrown in case of a bad URL
     */
    @Override
    public ServiceStatus getServiceStatus() throws InternalApiException
    {
        final URI restUrl = requestUrl(ServiceStatusService.URL_SERVICE_STATUS);

        try
        {
            return template.get(serverId, restUrl, ServiceStatus.class);
        }
        catch (Exception e)
        {
            logger.error("Error checking status on " + serverId, e);
            final ServiceStatus ret = new ServiceStatus();
            ret.setEnvironmentName("Not available");
            ret.setErrorMessage(e.getMessage());
            ret.setLibraryVersion("Not available");
            ret.setError(true);
            return ret;
        }

    }

    @Override
    public List<CmiuTimingAnalysisDetails> getCmiuTimingInfo(@RequestParam(value = "connection-threshold", required = false) Long connectionThreshold,
                                                             @RequestParam(value = "cmiu-mode", required = false) String cmiuMode,
                                                             @RequestParam(value = "cmiu-mno", required = false) String mno,
                                                             @RequestParam(value = "cmiu-state", required = false) String state,
                                                             @RequestParam(value = "cmiu-site-id", required = false) String siteId) throws InternalApiException {

        final URI restUrl = requestUrl(CmiuAnalysisService.URL_CMIU_TIMING_LIST,
                                        CmiuAnalysisService.PARAM_CONNECTION_THRESHOLD, String.valueOf(connectionThreshold),
                                        CmiuAnalysisService.PARAM_CMIU_MODE, cmiuMode,
                                        CmiuAnalysisService.PARAM_CMIU_MNO, mno,
                                        CmiuAnalysisService.PARAM_CMIU_STATE, state,
                                        CmiuAnalysisService.PARAM_CMIU_SITE_ID, siteId);
        return Arrays.asList(this.template.get(serverId, restUrl, CmiuTimingAnalysisDetails[].class));
    }

    /**
     * Get the MSR for a group
     *
     * @param accountId ID uniquely identifying group of endpoints
     * @return Status counts
     * @throws InternalApiException If something goes wrong in the request
     */
    @Override
    public List<CmiuDetails> getCmiuByAccount(@RequestParam(PARAM_CMIU_ACCOUNT_ID) String accountId) throws InternalApiException
    {
        final URI restUrl = requestUrl(CmiuDetailsService.URL_CMIU_BY_ACCOUNT, CmiuDetailsService.PARAM_CMIU_ACCOUNT_ID, accountId);
        return Arrays.asList(template.get(serverId, restUrl, CmiuDetails[].class));
    }

    /**
     * Get the CMIU config
     *
     * @param cmiuId the cmiu id
     * @return Status counts
     * @throws InternalApiException If something goes wrong in the request
     */
    @Override
    public CmiuConfigMgmt getCmiuConfigMgmt(@RequestParam(PARAM_CMIU_ID) long cmiuId) throws InternalApiException
    {
        final URI restUrl = requestUrl(CmiuConfigMgmtService.URL_CMIU_CONFIG, CmiuConfigMgmtService.PARAM_CMIU_ID, String.valueOf(cmiuId));
        return template.get(serverId, restUrl, CmiuConfigMgmt.class);
    }

    /**
     * Add a new CMIU config
     *
     * @param newCmiuConfigChangeList list of config set
     * @return
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(value = URL_CMIU_CONFIG, method = RequestMethod.POST)
    public CmiuConfigMgmt addCmiuConfig(String userName, List<CmiuConfigChange> newCmiuConfigChangeList)
            throws InternalApiException, JsonProcessingException
    {
        ObjectMapper mapper = new ObjectMapper();

        final URI restUrl = requestUrl(CmiuConfigMgmtService.URL_CMIU_CONFIG,
                CmiuConfigMgmtService.PARAM_USER, userName);
        CmiuConfigMgmt rtn = template.post(serverId, restUrl, newCmiuConfigChangeList, CmiuConfigMgmt.class);

        return rtn;
    }

    /**
     * Get a list of cmiu config mgmt entries representing CMIU config commands that are being schedule for sending to CMIU
     *
     * @return
     */
    @Override
    public List<CmiuConfigMgmt> getCmiuConfigChangeInProcessList() throws InternalApiException
    {
        final URI restUrl = requestUrl(CmiuConfigMgmtService.URL_CMIU_CONFIG_CHANGE_IN_PROCESS);
        return Arrays.asList(template.get(serverId, restUrl, CmiuConfigMgmt[].class));
    }

    /**
     * Get a list of cmiu config mgmt entries representing CMIU config commands that has been changed and acknowledged (reported) by the CMIU
     * for the last X days.
     * This filters the Cmiu config mgmt table to find reported config which has been changed in the last X days and matches planned config.
     *
     * @param changedDaysAgo the number of days ago the changes has been made to the reported config
     * @return filtered list of cmiu mgmt table
     * @throws InternalApiException
     */
    @Override
    public List<CmiuConfigMgmt> getCmiuConfigChangeCompletedList(int changedDaysAgo) throws InternalApiException
    {
        final URI restUrl = requestUrl(CmiuConfigMgmtService.URL_CMIU_CONFIG_CHANGE_COMPLETED,
                CmiuConfigMgmtService.PARAM_CONFIG_SET_LAST_CHANGED_DAYS_AGO, String.valueOf(changedDaysAgo));
        return Arrays.asList(template.get(serverId, restUrl, CmiuConfigMgmt[].class));
    }

    @Override
    public List<CmiuConfigHistory> getCmiuConfigHistory(@RequestParam(PARAM_CMIU_ID) long cmiuId) throws InternalApiException
    {
        final URI restUrl = requestUrl(CmiuConfigMgmtService.URL_CMIU_CONFIG_HISTORY,
                CmiuConfigMgmtService.PARAM_CMIU_ID, String.valueOf(cmiuId));
        return Arrays.asList(template.get(serverId, restUrl, CmiuConfigHistory[].class));
    }

    /**
     * Get a list of config sets
     *
     * @return
     * @throws InternalApiException
     */
    @Override
    public List<CmiuConfigSet> getCmiuConfigSetList() throws InternalApiException
    {
        URI restUrl = requestUrl(CmiuConfigSetService.URL_CMIU_CONFIG_SET_LIST);
        CmiuConfigSet[] response = template.get(serverId, restUrl, CmiuConfigSet[].class);
        return Arrays.asList(response);
    }



    /**
     * Get a config set from id
     *
     * @param configSetId the configset id
     * @return
     * @throws InternalApiException
     */
    @Override
    public CmiuConfigSet getCmiuConfigSet(@RequestParam(value = PARAM_CONFIG_SET_ID) Long configSetId) throws InternalApiException
    {
        URI restUrl = requestUrl(CmiuConfigSetService.URL_CMIU_CONFIG_SET, CmiuConfigSetService.PARAM_CONFIG_SET_ID, configSetId.toString());
        return template.get(serverId, restUrl, CmiuConfigSet.class);
    }

    /**
     * Add a new CMIU config set to database
     *
     * @param cmiuConfigSet new config set definition
     * @return
     * @throws InternalApiException
     */
    @Override
    public void addCmiuConfigSet(@RequestParam(value = CmiuConfigSetService.PARAM_USER) String userName, @RequestParam(PARAM_CONFIG_SET) CmiuConfigSet cmiuConfigSet) throws InternalApiException, JsonProcessingException
    {
        final URI restUrl = requestUrl(CmiuConfigSetService.URL_CMIU_CONFIG_SET, CmiuConfigSetService.PARAM_USER, userName);
        template.post(serverId, restUrl, cmiuConfigSet, CmiuConfigSet.class);
    }

    /**
     * Edit an existing CMIU config set
     *
     * @param cmiuConfigSet new config set definition
     * @return
     * @throws InternalApiException
     */
    @Override
    public void editCmiuConfigSet(@RequestParam(value = CmiuConfigSetService.PARAM_USER) String userName, @RequestBody CmiuConfigSet cmiuConfigSet) throws InternalApiException, JsonProcessingException
    {
        final URI restUrl = requestUrl(CmiuConfigSetService.URL_CMIU_CONFIG_SET_EDIT, CmiuConfigSetService.PARAM_USER, userName);
        template.post(serverId, restUrl, cmiuConfigSet, CmiuConfigSet.class);
    }



    /**
     * Get a list of config sets
     *
     * @param imageType
     * @return
     * @throws InternalApiException
     */
    @Override
    public List<ImageDescription> getAvailableImageList(@RequestParam(REQUEST_PARAM_IMAGE_TYPE) ImageDescription.ImageType imageType) throws InternalApiException
    {
        final URI restUrl = requestUrl(CmiuImageUpdateService.URL_CMIU_IMAGE, REQUEST_PARAM_IMAGE_TYPE, imageType.toString());
        ImageDescription[] response = template.get(serverId, restUrl, ImageDescription[].class);
        return Arrays.asList(response);
    }

    @Override
    public List<ImageDescription> getAvailableImageList() throws InternalApiException
    {
        final URI restUrl = requestUrl(CmiuImageUpdateService.URL_CMIU_IMAGE);
        ImageDescription[] response = template.get(serverId, restUrl, ImageDescription[].class);
        return Arrays.asList(response);
    }

    /**
     * Get a list of available build artifact (from S3)for purpose of selection/importing into the RDS
     *
     * @param imageType the type of image to retrieve, if null, all imageTypes are returned
     * @return a list of available artifacts grouped by imageType
     */
    @Override
    public Map<ImageDescription.ImageType, List<String>> getBuildArtifactList(ImageDescription.ImageType imageType) throws InternalApiException
    {
        final URI restUrl = (imageType == null) ?
                requestUrl(CmiuImageUpdateService.URL_CMIU_BUILD_ARTIFACT) :
                requestUrl(CmiuImageUpdateService.URL_CMIU_BUILD_ARTIFACT, CmiuImageUpdateService.REQUEST_PARAM_IMAGE_TYPE, imageType.name());

        Map<ImageDescription.ImageType, List<String>> result = template.get(serverId, restUrl, Map.class);

        return result;
    }

    /**
     * Register a build architact to be used in future image
     *
     * @param imageType
     * @param filePath
     */
    @Override
    public void registerBuildArtifact(ImageDescription.ImageType imageType, String filePath) throws InternalApiException
    {
        final URI restUrl = requestUrl(CmiuImageUpdateService.URL_CMIU_BUILD_ARTIFACT,
                REQUEST_PARAM_IMAGE_TYPE, imageType.name(),
                REQUEST_PARAM_NAME, filePath);

        template.post(serverId, restUrl, "", Map.class);
    }

    /**
     * Send a command to the backend for processing and publishing to MQTT broker.
     *
     * @param commandRequest containing the details of the command to be send to the backend
     */
    @Override
    public void sendCommand(CommandRequest commandRequest) throws InternalApiException
    {
        final URI restUrl = requestUrl(MiuCommandControlService.URL_CMIU_COMMAND);
        template.post(serverId, restUrl, commandRequest, Object.class);
    }

    /**
     * Remove all commands that queue for a CMIU. This causes the RDS to change all commands that are in CREATED or QUEUED
     * state to RECALLED.
     *
     * @param cmiuId the cmiu id
     * @return number of command affected
     */
    @Override
    public int flushCommand(@RequestParam(MiuCommandControlService.REQUEST_PARAM_CMIUID) long cmiuId) throws InternalApiException
    {
        final URI restUrl = requestUrl(MiuCommandControlService.URL_CMIU_COMMAND_FLUSH, MiuCommandControlService.REQUEST_PARAM_CMIUID, String.valueOf(cmiuId));
        return template.post(serverId, restUrl, "", Integer.class);
    }

    /**
     * Mark command for CMIU as rejected.
     *
     *
     * @param commandId the command id
     *
     */
    @Override
    public void rejectCommand(@RequestParam(MiuCommandControlService.REQUEST_PARAM_COMMANDID) long commandId) throws InternalApiException
    {
        final URI restUrl = requestUrl(MiuCommandControlService.URL_CMIU_COMMAND_REJECT, MiuCommandControlService.REQUEST_PARAM_COMMANDID, String.valueOf(commandId));
        template.post(serverId, restUrl, "", Object.class);
    }


    @Override
    public CmiuDescriptionViewModel getCmiuDescription(@RequestParam(MiuCommandControlService.REQUEST_PARAM_CMIUID) long cmiuId) throws InternalApiException
    {
        final URI restUrl = requestUrl(MiuCommandControlService.URL_CMIU_METADATA, MiuCommandControlService.REQUEST_PARAM_CMIUID, String.valueOf(cmiuId));
        return template.get(serverId, restUrl, CmiuDescriptionViewModel.class);
    }

    @Override
    public List<CommandSummary> getCommandsInProgress() throws InternalApiException
    {
        final URI restUrl = requestUrl(MiuCommandControlService.URL_COMMAND_IN_PROGRESS);
        CommandSummary[] response = template.get(serverId, restUrl, CommandSummary[].class);
        return Arrays.asList(response);
    }

    @Override
    public List<CommandSummary> getCommandsReceived() throws InternalApiException
    {
        final URI restUrl = requestUrl(MiuCommandControlService.URL_COMMAND_RECEIVED);
        CommandSummary[] response = template.get(serverId, restUrl, CommandSummary[].class);
        return Arrays.asList(response);
    }

    @Override
    public List<CommandSummary> getCommandsCompleted(@RequestParam(REQUEST_PARAM_DAYS_AGO) int daysAgo) throws InternalApiException
    {
        final URI restUrl = requestUrl(MiuCommandControlService.URL_COMMAND_COMPLETED, REQUEST_PARAM_DAYS_AGO, String.valueOf(daysAgo));
        CommandSummary[] response = template.get(serverId, restUrl, CommandSummary[].class);
        return Arrays.asList(response);
    }

    @Override
    public List<CommandSummary> getCommandsRejected(@RequestParam(REQUEST_PARAM_DAYS_AGO) int daysAgo) throws InternalApiException
    {
        final URI restUrl = requestUrl(MiuCommandControlService.URL_COMMAND_REJECTED, REQUEST_PARAM_DAYS_AGO, String.valueOf(daysAgo));
        CommandSummary[] response = template.get(serverId, restUrl, CommandSummary[].class);
        return Arrays.asList(response);
    }

    @Override
    public boolean addUser(@RequestBody NewUserDetails newUserDetails) throws InternalApiException
    {
        URI restUrl = requestUrl(UserService.URL_USER_ADD);
        return template.post(serverId, restUrl, newUserDetails, boolean.class);
    }

    @Override
    public void removeUser(@RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException
    {
        URI restUrl = requestUrl(UserService.URL_USER_REMOVE);
        template.post(serverId, restUrl, modifiedUserDetails, Object.class);
    }

    @Override
    public List<UserDetails> listUsers() throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_LIST);
        UserDetails[] userDetails = template.get(serverId, restUrl, UserDetails[].class);
        return Arrays.asList(userDetails);
    }

    @Override
    public void resetPassword(@RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException
    {
        URI restUrl = requestUrl(UserService.URL_USER_PASSWORD);
        template.post(serverId, restUrl, modifiedUserDetails, Object.class);
    }

    @Override
    public List<SiteDetails> getSitesForUser(@RequestBody int userId) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_SITE_LIST);
        SiteDetails[] siteDetails = template.post(serverId, restUrl, userId, SiteDetails[].class);
        return Arrays.asList(siteDetails);
    }

    @Override
    public List<SiteDetails> getSitesNotForUser(@RequestBody int userId) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_NOT_SITE_LIST);
        SiteDetails[] siteDetails = template.post(serverId, restUrl, userId, SiteDetails[].class);
        return Arrays.asList(siteDetails);
    }

    @Override
    public boolean addSiteToUser(@RequestBody SiteForUser siteForUser) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_ADD_SITE);
        return template.post(serverId, restUrl, siteForUser, Boolean.class);
    }

    @Override
    public boolean removeSiteFromUser(@RequestBody SiteForUser siteForUser) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_REMOVE_SITE);
        return template.post(serverId, restUrl, siteForUser, Boolean.class);
    }

    @Override
    public LoginResult verifyUser(@RequestBody LoginCredentials loginCredentials) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_VERIFY);

        return template.post(serverId, restUrl, loginCredentials, LoginResult.class);
    }

    @Override
    public List<AlertDetails> getNewAlertList(@RequestBody String alertSource) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_LIST_NEW);
        AlertDetails[] alertDetails = template.post(serverId, restUrl, alertSource, AlertDetails[].class);
        return Arrays.asList(alertDetails);
    }

    @Override
    public List<AlertDetails> getHandledAlertList(@RequestBody String alertSource) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_LIST_HANDLED);
        AlertDetails[] alertDetails = template.post(serverId, restUrl, alertSource, AlertDetails[].class);
        return Arrays.asList(alertDetails);
    }

    @Override
    public List<AlertDetails> getStaleAlertList(@RequestBody String alertSource) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_LIST_STALE);
        AlertDetails[] alertDetails = template.post(serverId, restUrl, alertSource, AlertDetails[].class);
        return Arrays.asList(alertDetails);
    }

    @Override
    public List<AlertDetails> getClearedAlertList(@RequestBody String alertSource) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_LIST_CLEARED);
        AlertDetails[] alertDetails = template.post(serverId, restUrl, alertSource, AlertDetails[].class);
        return Arrays.asList(alertDetails);
    }

    @Override
    public AlertDetails getAlert(@RequestBody Long alertId) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_SINGLE);
        AlertDetails alertDetails = template.post(serverId, restUrl, alertId, AlertDetails.class);
        return alertDetails;
    }

    @Override
    public boolean addAlert(@RequestBody AlertDetails alertDetails) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_ADD);
        return template.post(serverId, restUrl, alertDetails, boolean.class);
    }

    @Override
    public boolean setAlertStateToNew(@RequestBody Long alertId) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_SET_NEW);
        return template.post(serverId, restUrl, alertId, boolean.class);
    }

    @Override
    public boolean setAlertStateToHandled(@RequestBody Long alertId) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_SET_HANDLED);
        return template.post(serverId, restUrl, alertId, boolean.class);
    }

    @Override
    public boolean setAlertStateToStale(@RequestBody Long alertId) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_LIST_STALE);
        return template.post(serverId, restUrl, alertId, boolean.class);
    }

    @Override
    public boolean setAlertStateToCleared(@RequestBody Long alertId) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_SET_CLEARED);
        return template.post(serverId, restUrl, alertId, boolean.class);
    }

    @Override
    public boolean setAlertLevelToWarning(@RequestBody Long alertId) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_SET_WARNING);
        return template.post(serverId, restUrl, alertId, boolean.class);
    }

    @Override
    public boolean setAlertLevelToError(@RequestBody Long alertId) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_SET_ERROR);
        return template.post(serverId, restUrl, alertId, boolean.class);
    }

    @Override
    public boolean setAlertTicketId(@RequestBody AlertDetails alertDetails) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_SET_TICKET_ID);
        return template.post(serverId, restUrl, alertDetails, boolean.class);
    }

    @Override
    public boolean addAlertLogMessage(@RequestBody AlertDetails alertDetails) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_ADD_MESSAGE);
        return template.post(serverId, restUrl, alertDetails, boolean.class);
    }

    @Override
    public List<AlertLog> getAlertLog(@RequestBody Long alertId) throws InternalApiException
    {
        final URI restUrl = requestUrl(AlertRestService.URL_ALERT_GET_LOG);
        AlertLog[] alertLogs = template.post(serverId, restUrl, alertId, AlertLog[].class);
        return Arrays.asList(alertLogs);
    }

    @Override
    public UserDetails getUserDetails(@PathVariable("username") String userName) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_DETAIL + "/" + userName);
        return template.get(serverId, restUrl, UserDetails.class);
    }

    @Override
    public List<UserDetails> findUsers(@RequestBody Map<String, Object> options) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_FIND);
        UserDetails[] userDetailsResults = template.post(serverId, restUrl, options, UserDetails[].class);
        return Arrays.asList(userDetailsResults);
    }

    @Override
    public boolean setUserDetails(@PathVariable("username") String userName,
                                      @RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_DETAIL + "/" + userName);
        return template.post(serverId, restUrl, modifiedUserDetails, boolean.class);
    }

    @Override
    public UserAlertSettings getUserAlertSettings(@PathVariable("userid") int userId) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_ALERT_SETTINGS + "/" + userId);
        return template.get(serverId, restUrl, UserAlertSettings.class);
    }

    @Override
    public boolean setUserAlertSettings(@RequestBody UserAlertSettings userAlertSettings) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_ALERT_SETTINGS);
        return template.post(serverId, restUrl, userAlertSettings, boolean.class);
    }

    @Override
    public boolean addPasswordResetRequest(@RequestBody PasswordResetRequest passwordResetRequest) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_PASSWORD_RESET_REQUEST);
        return template.post(serverId, restUrl, passwordResetRequest, boolean.class);
    }

    @Override
    public boolean claimPasswordResetRequest(@PathVariable("token") String token, @RequestBody PasswordResetRequestClaim passwordResetRequestClaim) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_PASSWORD_RESET_REQUEST + "/" + token + "/claim");
        return template.post(serverId, restUrl, passwordResetRequestClaim, boolean.class);
    }

    @Override
    public int expirePasswordResetRequestsByUser(@RequestParam(USER_ID_PARAM) long userId) throws InternalApiException
    {
        final URI restUrl = requestUrl(
                UserService.URL_USER_EXPIRE_PASSWORD_RESET_REQUESTS,
                UserService.USER_ID_PARAM, String.valueOf(userId));

        return template.post(serverId, restUrl, "", int.class);
    }

    @Override
    public PasswordResetRequest getPasswordResetRequestByToken(String token) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_PASSWORD_RESET_REQUEST + "/" + token);
        return template.get(serverId, restUrl, PasswordResetRequest.class);
    }

    /**
     * Find user details by email. To check if email in use.
     * @param email user email.
     * @return user details or null.
     * @throws InternalApiException If something goes wrong in the request.
     */
    @Override
    public List<UserDetails> getUserDetailsByEmail(@RequestParam("email") String email) throws InternalApiException
    {
        final URI restUrl = requestUrl(UserService.URL_USER_FIND_EMAIL, "email", email);

        UserDetails[] userDetails = template.get(serverId, restUrl, UserDetails[].class);

        if (null == userDetails)
        {
            return new ArrayList<>();
        }

        return Arrays.asList(userDetails);
    }

    @Override
    public AuditLogPagedResult getAuditLog(@RequestParam(value = REQUEST_PARAM_AUDIT_FILTER_MIU, required = false) Long miuId,
                                      @RequestParam(value = REQUEST_PARAM_AUDIT_FILTER_USERNAME, required = false) String userName,
                                      @RequestParam(value = REQUEST_PARAM_AUDIT_LOG_DAYS_AGO, required = false) Integer daysAgo,
                                      @RequestParam(value = REQUEST_PARAM_AUDIT_LOG_PAGE, required = false) Integer page) throws InternalApiException
    {
        List<String> params = new ArrayList<>();
        if (miuId != null)
        {
            params.add(AuditLogService.REQUEST_PARAM_AUDIT_FILTER_MIU);
            params.add(String.valueOf(miuId));
        }

        if (userName != null)
        {
            params.add(AuditLogService.REQUEST_PARAM_AUDIT_FILTER_USERNAME);
            params.add(userName);
        }

        if (daysAgo != null)
        {
            params.add(AuditLogService.REQUEST_PARAM_AUDIT_LOG_DAYS_AGO);
            params.add(String.valueOf(daysAgo));
        }

        if (page != null)
        {
            params.add(AuditLogService.REQUEST_PARAM_AUDIT_LOG_PAGE);
            params.add(String.valueOf(page));
        }

        String[] nameAndParams = params.toArray(new String[params.size()]);

        final URI restUrl = requestUrl(AuditLogService.URL_AUDIT_LOG, nameAndParams);

        return template.get(serverId, restUrl, AuditLogPagedResult.class);

    }

    @Override
    public void logAccessDenied(String userName, String description) throws InternalApiException
    {
        List<String> params = new ArrayList<>();
        if (StringUtils.hasText(userName))
        {
            params.add("userName");
            params.add(userName);
        }

        if (StringUtils.hasText(description))
        {
            params.add("description");
            params.add(description);
        }

        String[] userNameAndDescription = params.toArray(new String[params.size()]);
        final URI restUrl = requestUrl(AuditLogService.URL_AUDIT_LOG_ACCESS_DENIED, userNameAndDescription);

        template.get(serverId, restUrl, Void.class);
    }

    @Override
    public SimDisplayDetailsPagedResult getSimList(Map<String, String> options) throws InternalApiException
    {
        final URI restUrl = restUrlFromOptionsMap(SimService.URL_SIM_LIST, options);

        return template.get(serverId, restUrl, SimDisplayDetailsPagedResult.class);
    }

    @Override
    public void updateOrInsertSimDetails(Map<String, String> options, String iccid, String mno) throws InternalApiException
    {
        final URI restUrl = requestUrl(SimService.URL_DEVICE_DETAILS, SimService.ICCID, iccid, SimService.MNO, mno);

        template.post(serverId, restUrl, options, Object.class);
    }

    @Override
    public int updateSimLifecycleState(Map<String, String> options) throws InternalApiException
    {
        final URI restUrl = requestUrl(SimService.URL_SIM_LIFECYCLESTATE);

        return template.post(serverId, restUrl, options, int.class);
    }

    @Override
    public SimDisplayDetails getSimDetails(Map<String,String> options, String iccid) throws InternalApiException
    {
        options.putIfAbsent("iccid", iccid);

        final URI restUrl = restUrlFromOptionsMap(SimService.URL_SIM_DETAILS, options);

        return template.get(serverId, restUrl, SimDisplayDetails.class);
    }

    @Override
    public List<String> getSimLifeCycleStates(Map<String, String> options) throws InternalApiException
    {
        final URI restUrl = restUrlFromOptionsMap(RefService.URL_GET_SIM_LIFE_CYCLE_STATES, options);

        return Arrays.asList(template.get(serverId, restUrl, String[].class));
    }

    @Override
    public List<String> getSimNetworks(Map<String, String> options) throws InternalApiException
    {
        final URI restUrl = restUrlFromOptionsMap(RefService.URL_GET_SIM_NETWORKS, options);

        return Arrays.asList(template.get(serverId, restUrl, String[].class));
    }

    @Override
    public List<String> getSimIccids(Map<String, String> options) throws InternalApiException
    {
        final URI restUrl = restUrlFromOptionsMap(SimService.URL_GET_SIM_ICCIDS, options);

        return Arrays.asList(template.get(serverId, restUrl, String[].class));
    }

    @Override
    public List<String> getSimMnoList() throws InternalApiException
    {
        final URI restUrl = requestUrl(SimService.URL_GET_SIM_MNO_LIST);

        return Arrays.asList(template.get(serverId, restUrl, String[].class));
    }

    @Override
    public MiuDisplayDetailsPagedResult getMiuList(Map<String, String> options) throws InternalApiException
    {
        List<String> params = new ArrayList<>();

        for (Map.Entry<String, String> entry : options.entrySet())
        {
            params.add(entry.getKey());
            params.add(entry.getValue());
        }

        String[] nameAndParams = params.toArray(new String[params.size()]);
        final URI restUrl = requestUrl(MiuService.URL_MIU_LIST, nameAndParams);

        //return template.post(serverId, restUrl, options, MiuDisplayDetailsPagedResult.class);
        return template.get(serverId, restUrl, MiuDisplayDetailsPagedResult.class);
    }

    @Override
    public void claimMiu(long miuId, int newSiteId, String requester) throws InternalApiException
    {
        final URI restUrl = requestUrl(MiuService.URL_MIU_CLAIM,
                REQUEST_PARAM_MIU_ID, String.valueOf(miuId),
                REQUEST_PARAM_NEW_SITE_ID, String.valueOf(newSiteId),
                REQUEST_PARAM_USER, requester);

        final Object[] objects = {miuId, newSiteId};
        Arrays.asList(template.post(serverId, restUrl, objects, Object.class));
    }

    @Override
    public List<String> getSetableLifecycleStates(long miuId, String miuType) throws InternalApiException
    {
        String[] params = new String[4];
        params[0] = "miuId";
        params[1] = Long.toString(miuId);
        params[2] = "miuType";
        params[3] = miuType;
        final URI restUrl = requestUrl(MiuService.URL_MIU_LIFECYCLE_STATES, params);
        return Arrays.asList(template.get(serverId, restUrl, String[].class));
    }

    @Override
    public void setLifecycleState(long miuId, String lifecycleState) throws InternalApiException
    {
        final URI restUrl = requestUrl(MiuService.URL_MIU_LIFECYCLE_STATES,
                "miuId", Long.toString(miuId),
                "lifecycleState", lifecycleState);
        template.post(serverId, restUrl, null, String.class);
    }

    @Override
    public List<String> getAllCmiuModes() throws InternalApiException
    {
        final URI restUrl = requestUrl(MiuService.URL_CMIU_MODES);
        return Arrays.asList(template.get(serverId, restUrl, String[].class));
    }

    @Override
    public GatewayDisplayDetailsPagedResult getGatewayList(Map<String, String> options) throws InternalApiException
    {
        List<String> params = new ArrayList<>();

        for (Map.Entry<String, String> entry : options.entrySet())
        {
            params.add(entry.getKey());
            params.add(entry.getValue());
        }

        String[] nameAndParams = params.toArray(new String[params.size()]);
        final URI restUrl = requestUrl(GatewayService.URL_GATEWAY_LIST, nameAndParams);

        return template.get(serverId, restUrl, GatewayDisplayDetailsPagedResult.class);
    }

    @Override
    public List<SiteDetails> getSiteList() throws InternalApiException
    {
        final URI restUrl = requestUrl(SiteService.URL_SITE_LIST);
        return Arrays.asList(template.get(serverId, restUrl, SiteDetails[].class));
    }

    @Override
    public SiteDetails getSiteDetails(int siteId) throws InternalApiException
    {
        final URI restUrl = requestUrl(SiteService.URL_SITE_DETAILS, SiteService.SITE_ID, Integer.toString(siteId));
        return template.get(serverId, restUrl, SiteDetails.class);
    }

    /**
     * Finds sites that have not contacted mdce between a configurable amount of time.
     *
     * @param siteConnectionCheckHours max amount of hours an site should be talking
     *        to mdce anything after that is an issue.
     * @param siteConnectionIgnoreAfterHours max amount of hours a site can be inactive
     *        before we declare inactive and ignore.
     * @return list of sites
     */
    @Override
    public List<SiteDetails> getSitesNotRequestingDataWithinXHours(int siteConnectionCheckHours, int siteConnectionIgnoreAfterHours) throws InternalApiException
    {
        final URI restUrl = requestUrl(SiteService.URL_SITE_REQUEST_DATA_ISSUE_CHECK);
        return Arrays.asList(template.get(serverId, restUrl, SiteDetails[].class));
    }

    @Override
    public List<String> getRegionalManagers() throws InternalApiException
    {
        final URI restUrl = requestUrl(RefService.URL_GET_REGIONAL_MANAGERS);
        return Arrays.asList(template.get(serverId, restUrl, String[].class));
    }

    @Override
    public List<String> getStates() throws InternalApiException
    {
        final URI restUrl = requestUrl(RefService.URL_GET_STATES);
        return Arrays.asList(template.get(serverId, restUrl, String[].class));
    }

    @Override
    public List<String> getMiuTypes() throws InternalApiException
    {
        final URI restUrl = requestUrl(RefService.URL_GET_MIU_TYPES);
        return Arrays.asList(template.get(serverId, restUrl, String[].class));
    }

    @Override
    public List<SiteDetails> getSitesForRegionalManager(String regionalManager) throws InternalApiException
    {
        final URI restUrl = requestUrl(RefService.URL_GET_SITES_FOR_REGIONAL_MANAGER, RefService.REGIONAL_MANAGER, regionalManager);
        return Arrays.asList(template.get(serverId, restUrl, SiteDetails[].class));
    }

    @Override
    public List<SiteDetails> getSitesForState(String state) throws InternalApiException
    {
        final URI restUrl = requestUrl(RefService.URL_GET_SITES_FOR_STATE, RefService.STATE, state);
        return Arrays.asList(template.get(serverId, restUrl, SiteDetails[].class));
    }

    @Override
    public MdceReferenceDataUtility getSiteReferenceData(int siteId) throws InternalApiException
    {
        final URI restUrl = requestUrl(RefService.URL_GET_SITE_REFERENCE_DATA, RefService.SITE_ID, Integer.toString(siteId));
        return template.get(serverId, restUrl, MdceReferenceDataUtility.class);
    }

    @Override
    public MdceReferenceDataDistributer getDistributerReferenceData(String distributerId) throws InternalApiException
    {
        final URI restUrl = requestUrl(RefService.URL_GET_DISTRIBUTER_REFERENCE_DATA, RefService.DISTRIBUTER_ID, distributerId);
        return template.get(serverId, restUrl, MdceReferenceDataDistributer.class);
    }

    @Override
    public String getTextSettingValue(String key) throws InternalApiException
    {
        final URI restUrl = requestUrl(MdceSettingsService.URL_MDCE_SETTINGS_TEXT_SETTING,
                MdceSettingsService.KEY, key);
        return template.get(serverId, restUrl, String.class);
    }

    @Override
    public Long getIntSettingValue(String key, Long defaultValue) throws InternalApiException
    {
        String defaultValueParameter = defaultValue == null ? null : defaultValue.toString();
        final URI restUrl = requestUrl(MdceSettingsService.URL_MDCE_SETTINGS_INT_SETTING,
                MdceSettingsService.KEY, key, MdceSettingsService.DEFAULT_VALUE, defaultValueParameter);

        return template.get(serverId, restUrl, Long.class);
    }

    @Override
    public void setTextSettingValue(String key, String newValue, String requester) throws InternalApiException
    {
        final URI restUrl = requestUrl(MdceSettingsService.URL_MDCE_SETTINGS_TEXT_SETTING,
                MdceSettingsService.KEY, key, MdceSettingsService.REQUESTER, requester);
        template.post(serverId, restUrl, newValue, Void.class);
    }

    @Override
    public void setIntSettingValue(String key, Long newValue, String requester) throws InternalApiException
    {
        final URI restUrl = requestUrl(MdceSettingsService.URL_MDCE_SETTINGS_INT_SETTING,
                MdceSettingsService.KEY, key, MdceSettingsService.REQUESTER, requester);
        template.post(serverId, restUrl, newValue, Void.class);
    }

    @Override
    public List<CmiuModemFirmwareDetails> getAvailableCmiuModemFirmwareImages() throws InternalApiException
    {
        final URI restUrl = requestUrl(CmiuModemFirmwareService.URL_CMIU_MODEM_FIRMWARE_LIST);
        return Arrays.asList(template.get(serverId, restUrl, CmiuModemFirmwareDetails[].class));
    }

    @Override
    public void sendEmail(EmailMessage emailMessage) throws InternalApiException
    {
        final URI restUrl = requestUrl(EmailService.URL_EMAIL_SERVICE_SEND_EMAIL);
        template.post(serverId, restUrl, emailMessage, Void.class);
    }

    @Override
    public EmailMessage getDebugModeEmail() throws InternalApiException
    {
        final URI restUrl = requestUrl(EmailService.URL_EMAIL_SERVICE_GET_DEBUG_MODE_EMAIL);
        return template.get(serverId, restUrl, EmailMessage.class);
    }

    @Override
    public LoraDisplayDetails getLoraDetails(int miuId) throws InternalApiException
    {
        final URI restUrl = requestUrl(LoraService.URL_LORA_DETAILS, Integer.toString(miuId));
        return template.get(serverId, restUrl, LoraDisplayDetails.class);
    }

    @Override
    public List<String> getLoraNetworkList() throws InternalApiException
    {
        final URI restUrl = requestUrl(LoraService.URL_LORA_NETWORK_LIST);
        return Arrays.asList(template.get(serverId, restUrl, String[].class));
    }

    @Override
    public InputStream getPacketDump(String l900IdParam, String cmiuIdParam, String iccid, String imei,
                              Integer siteId, Integer daysOfRecord) throws IOException, InternalApiException
    {
        final URI restUrl = requestUrl(PacketDumpService.URL_PACKET_DUMP,
                PacketDumpService.L900_ID, l900IdParam,
                PacketDumpService.CMIU_ID, cmiuIdParam,
                PacketDumpService.ICCID, iccid,
                PacketDumpService.IMEI, imei,
                PacketDumpService.CMIU_SITE_ID, siteId !=null ? siteId.toString() : "",
                PacketDumpService.DAYS, daysOfRecord.toString());

        URL url = new URL(restUrl.toString());

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

        if(200 > conn.getResponseCode() || 300 <= conn.getResponseCode())
        {
            throw new InternalApiException("Response code not in 200 range when getting packet dump");
        }

        return conn.getInputStream();
    }

    @Override
    public InputStream getCellularConnectionHistoryWithDateRange( String mno, Integer miuIdParam, String iccidParam,
                                                           String imeiParam, Integer siteIdParam, String fromDate,
                                                           String toDate) throws InternalApiException, IOException
    {
        final URI restUrl = requestUrl(CnsDumpService.URL_GET_CELLULAR_CONNECTION_HISTORY_WITH_DATE_RANGE.replace("{mno}", mno),
                CnsDumpService.MIU_ID, miuIdParam != null ? miuIdParam.toString() : "",
                CnsDumpService.ICCID, iccidParam,
                CnsDumpService.IMEI, imeiParam,
                CnsDumpService.SITE_ID, siteIdParam != null ? siteIdParam.toString() : "",
                CnsDumpService.FROM, fromDate,
                CnsDumpService.TO, toDate);

        URL url = new URL(restUrl.toString());

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

        if(200 > conn.getResponseCode() || 300 <= conn.getResponseCode())
        {
            throw new InternalApiException("Response code not in 200 range when getting cellular connection history dump");
        }

        return conn.getInputStream();
    }

    @Override
    public InputStream downloadCellularUsageHistoryByDateRange(String mno, Integer miuIdParam, String iccidParam,
                                                        String imeiParam, Integer siteIdParam, String fromDate,
                                                        String toDate) throws InternalApiException, IOException
    {
        final URI restUrl = requestUrl(CnsDumpService.URL_DOWNLOAD_CELLULAR_CONNECTION_HISTORY_BY_DATE_RANGE.replace("{mno}", mno),
                CnsDumpService.MIU_ID, miuIdParam != null ? miuIdParam.toString() : "",
                CnsDumpService.ICCID, iccidParam,
                CnsDumpService.IMEI, imeiParam,
                CnsDumpService.SITE_ID, siteIdParam != null ? siteIdParam.toString() : "",
                CnsDumpService.FROM, fromDate,
                CnsDumpService.TO, toDate);

        URL url = new URL(restUrl.toString());

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

        if(200 > conn.getResponseCode() || 300 <= conn.getResponseCode())
        {
            throw new InternalApiException("Response code not in 200 range when getting cellular usage dump");
        }

        return conn.getInputStream();
    }

    @Override
    public String registerCallback(String mno) throws RemoteException, InternalApiException
    {
        URI restUrl = requestUrl(MnoCallbackService.URL_REGISTER_VERIZON_CALLBACK.replace("{mno}", mno));
        return template.get(serverId, restUrl, String.class);
    }

    @Override
    public String unregisterCallback(String mno) throws RemoteException, InternalApiException
    {
        URI restUrl = requestUrl(MnoCallbackService.URL_UNREGISTER_VERIZON_CALLBACK.replace("{mno}", mno));
        return template.get(serverId, restUrl, String.class);
    }

    @Override
    public SimDeactivationDisplayDetails getDeactivationDetails(String miuID) throws InternalApiException
    {
        final URI restUrl = requestUrl(SimDeactivationService.URL_SIM_DETAILS_BY_MIUID, SimDeactivationService.REQUEST_PARAM_MIU_ID_FOR_DEACTIVATION, miuID);
        return template.get(serverId, restUrl, SimDeactivationDisplayDetails.class);
    }

    @Override
    public SimDeactivationDetails deactivateSims(String miuID) throws InternalApiException
    {
        final URI restUrl = requestUrl(SimDeactivationService.URL_SIM_DETAILS_BY_MIUID, SimDeactivationService.REQUEST_PARAM_MIU_ID_FOR_DEACTIVATION, miuID);
        return template.post(serverId, restUrl, String.class, SimDeactivationDetails.class);
    }
}

