/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.site.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Service for providing access to site information
 */
public interface SiteService
{
    /**
     * URL of the service
     */
    String URL_SITE_LIST = "/internal/site/list";
    String URL_SITE_DETAILS = "/internal/site";
    String URL_SITE_REQUEST_DATA_ISSUE_CHECK = "/internal/site/list/communication";

    String SITE_ID = "siteId";
    String SITE_CONNECTION_IGNORE_AFTER_HOURS = "siteConnectionIgnoreAfterHours";
    String SITE_CONNCETION_CHECK_HOURS = "siteConnectionCheckHours";

    /**
     * Get list of all sites
     */
    @RequestMapping(value = URL_SITE_LIST, method = RequestMethod.GET)
    List<SiteDetails> getSiteList() throws InternalApiException;

    /**
     * Get list of all sites
     */
    @RequestMapping(value = URL_SITE_DETAILS, method = RequestMethod.GET)
    SiteDetails getSiteDetails(@RequestParam(SITE_ID) int siteId) throws InternalApiException;

    /**
     * Finds sites that have not contacted mdce between a configurable amount of time.
     *
     * @param siteConnectionCheckHours max amount of hours an site should be talking
     *        to mdce anything after that is an issue.
     * @param siteConnectionIgnoreAfterHours max amount of hours a site can be inactive
     *        before we declare inactive and ignore.
     * @return list of sites
     */
    @RequestMapping(value = URL_SITE_REQUEST_DATA_ISSUE_CHECK, method = RequestMethod.GET)
    List<SiteDetails> getSitesNotRequestingDataWithinXHours(
            @RequestParam(SITE_CONNCETION_CHECK_HOURS) int siteConnectionCheckHours,
            @RequestParam(SITE_CONNECTION_IGNORE_AFTER_HOURS) int siteConnectionIgnoreAfterHours) throws InternalApiException;
}
