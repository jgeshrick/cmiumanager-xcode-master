/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.user.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.common.internal.site.model.SiteForUser;
import com.neptunetg.mdce.common.internal.user.model.LoginCredentials;
import com.neptunetg.mdce.common.internal.user.model.LoginResult;
import com.neptunetg.mdce.common.internal.user.model.ModifiedUserDetails;
import com.neptunetg.mdce.common.internal.user.model.NewUserDetails;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequest;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequestClaim;
import com.neptunetg.mdce.common.internal.user.model.UserAlertSettings;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Service for providing access to user accounts
 */
@RestController
@RequestMapping("/service")
public interface UserService
{
    /**
     * URL of the service
     */
    String URL_USER_ADD = "/internal/user/add";
    String URL_USER_LIST = "/internal/user/list";
    String URL_USER_VERIFY = "/internal/user/verify";
    String URL_USER_REMOVE = "/internal/user/remove";
    String URL_USER_PASSWORD = "/internal/user/newPassword";
    String URL_USER_SITE_LIST = "/internal/user/siteList";
    String URL_USER_NOT_SITE_LIST = "/internal/user/notSiteList";
    String URL_USER_ADD_SITE = "/internal/user/addSite";
    String URL_USER_REMOVE_SITE = "/internal/user/removeSite";
    String URL_USER_DETAIL = "/internal/user/detail";
    String URL_USER_FIND = "/internal/user/find";
    String URL_USER_FIND_EMAIL = "/internal/user/find/email";
    String URL_USER_ALERT_SETTINGS = "/internal/user/alertDetails";
    String URL_USER_PASSWORD_RESET_REQUEST = "/internal/user/passwordResetRequest";
    String URL_USER_EXPIRE_PASSWORD_RESET_REQUESTS = "/internal/user/expirePasswordResetRequests";
    String USER_ID_PARAM = "userId";

    /**
     * add a user
     * @param newUserDetails details of the new user
     */
    @RequestMapping(value=URL_USER_ADD, method=RequestMethod.POST)
    boolean addUser(@RequestBody NewUserDetails newUserDetails) throws InternalApiException;


    /**
     * Remove user from the system
     * @param modifiedUserDetails
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value=URL_USER_REMOVE, method=RequestMethod.POST)
    void removeUser(@RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException;


    /**
     * Get a list of all users
     * @return list of all users
     */
    @RequestMapping(value=URL_USER_LIST, method=RequestMethod.GET)
    List<UserDetails> listUsers() throws InternalApiException;


    /**
     * Set a new password for a user
     * @param modifiedUserDetails
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value=URL_USER_PASSWORD, method=RequestMethod.POST)
    void resetPassword(@RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException;


    /**
     * Get a list of all sites a user has access to
     * @param userId
     * @return
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value=URL_USER_SITE_LIST, method=RequestMethod.POST)
    List<SiteDetails> getSitesForUser(@RequestBody int userId) throws InternalApiException;


    /**
     * Get a list of all sites a user doesn't have access to
     * @param userId
     * @return
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value=URL_USER_NOT_SITE_LIST, method=RequestMethod.POST)
    List<SiteDetails> getSitesNotForUser(@RequestBody int userId) throws InternalApiException;


    @RequestMapping(value=URL_USER_ADD_SITE, method=RequestMethod.POST)
    boolean addSiteToUser(@RequestBody SiteForUser siteForUser) throws InternalApiException;

    @RequestMapping(value=URL_USER_REMOVE_SITE, method=RequestMethod.POST)
    boolean removeSiteFromUser(@RequestBody SiteForUser siteForUser) throws InternalApiException;

    /**
     * Verify a user
     * @param loginCredentials contains the username and authentication credential
     * @return an instance of LoginResult that indicates whether the user can be logged in or not.
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value=URL_USER_VERIFY, method=RequestMethod.POST)
    LoginResult verifyUser(@RequestBody LoginCredentials loginCredentials) throws InternalApiException;

    /**
     * Get details of a user
     * @param userName user name
     * @return UserDetails containing description of the required user
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value=URL_USER_DETAIL + "/{username}", method=RequestMethod.GET)
    UserDetails getUserDetails(@PathVariable("username") String userName) throws InternalApiException;

    /**
     * Find users by the supplied criteria.
     * @param options search parameters
     * @return UserDetails for any users matching the search criteria.
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value=URL_USER_FIND, method=RequestMethod.POST)
    List<UserDetails> findUsers(@RequestBody Map<String, Object> options) throws InternalApiException;

    /**
     * Update the details of a user
     * @param userName the existing user name of the user being updated
     * @param modifiedUserDetails user details to update
     * @return true if the operation was successful, false otherwise
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value=URL_USER_DETAIL + "/{username}", method=RequestMethod.POST)
    boolean setUserDetails(@PathVariable("username") String userName,
                               @RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException;

    /**
     * Get the alert settings for a user
     * @param userId
     * @return
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value=URL_USER_ALERT_SETTINGS + "/{userid}", method=RequestMethod.GET)
    UserAlertSettings getUserAlertSettings(@PathVariable("userid") int userId) throws InternalApiException;

    /**
     * Set the alert settings for a user
     * @param userAlertSettings
     * @return
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value=URL_USER_ALERT_SETTINGS, method=RequestMethod.POST)
    boolean setUserAlertSettings(@RequestBody UserAlertSettings userAlertSettings) throws InternalApiException;

    /**
     * Create a new password reset request.
     * @param passwordResetRequest The details of the request.
     * @return true if the operation succeeded, otherwise false.
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value = URL_USER_PASSWORD_RESET_REQUEST, method = RequestMethod.POST)
    boolean addPasswordResetRequest(@RequestBody PasswordResetRequest passwordResetRequest) throws InternalApiException;

    /**
     * Sets an existing password reset request as claimed, preventing it from being used again.
     * @param token The token belonging to the reset request.
     * @param passwordResetRequestClaim Includes information about the claim.
     * @return true if the operation succeeded, otherwise false.
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value = URL_USER_PASSWORD_RESET_REQUEST + "/{token}/claim", method = RequestMethod.POST)
    boolean claimPasswordResetRequest(@PathVariable("token") String token,
                                      @RequestBody PasswordResetRequestClaim passwordResetRequestClaim
            ) throws InternalApiException;

    /**
     * Sets an existing password reset request as claimed, preventing it from being used again.
     * @param userId The userId of the user whose password reset tokens should be expired.
     * @return The number of password reset requests that were expired by the operation.
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value = URL_USER_EXPIRE_PASSWORD_RESET_REQUESTS, method = RequestMethod.POST)
    int expirePasswordResetRequestsByUser(@RequestParam(USER_ID_PARAM) long userId) throws InternalApiException;

    /**
     * Gets a password reset request by token.
     * @param token The token belonging to the reset request.
     * @return the password reset request, if found; otherwise, null.
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value = URL_USER_PASSWORD_RESET_REQUEST + "/{token}", method = RequestMethod.GET)
    PasswordResetRequest getPasswordResetRequestByToken(@PathVariable("token") String token) throws InternalApiException;

    /**
     * Find user details by email. To check if email in use.
     * @param email user email.
     * @return user details list or null.
     * @throws InternalApiException If something goes wrong in the request.
     */
    @RequestMapping(value = URL_USER_FIND_EMAIL, method = RequestMethod.GET)
    List<UserDetails> getUserDetailsByEmail(@RequestParam(name = "email") String email) throws InternalApiException;

}
