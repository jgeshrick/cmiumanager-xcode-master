/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.gateway.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.gateway.model.GatewayDisplayDetailsPagedResult;
import com.neptunetg.mdce.common.internal.miu.model.MiuDisplayDetailsPagedResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * Service for providing access to gateway detail
 */
public interface GatewayService
{
    /**
     * URL of the service
     */
    String URL_GATEWAY_LIST = "/internal/gateway/list";
    String REQUEST_PARAM_GATEWAY_ID = "gatewayId";

    @RequestMapping(value = URL_GATEWAY_LIST, method = RequestMethod.GET)
    GatewayDisplayDetailsPagedResult getGatewayList(Map<String, String> options) throws InternalApiException;

}
