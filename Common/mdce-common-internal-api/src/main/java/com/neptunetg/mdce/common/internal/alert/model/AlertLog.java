/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.common.internal.alert.model;


import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Class to store message logs for a single alert
 */
public class AlertLog
{

    private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
    private static final String UTC = "UTC";

    private Instant logDate;
    private String logMessage;

    public String getFormattedLogDate()
    {
        return formatter.format(logDate.atZone(ZoneId.of(UTC)));
    }

    public Instant getLogDate()
    {
        return logDate;
    }

    public void setLogDate(Instant logDate)
    {
        this.logDate = logDate;
    }

    public String getLogMessage()
    {
        return logMessage;
    }

    public void setLogMessage(String logMessage)
    {
        this.logMessage = logMessage;
    }
}
