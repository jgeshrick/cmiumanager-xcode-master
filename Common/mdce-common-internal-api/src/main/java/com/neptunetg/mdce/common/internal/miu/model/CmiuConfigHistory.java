/*
 *  Neptune Technology Group
 *  Copyright 2015, 2016 as unpublished work.
 *  All rights reserve
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.internal.miu.model;

import java.time.Instant;

/**
 * Representation of Cmiu config history entry.
 */
public class CmiuConfigHistory
{
    private int miuId;
    private String miuType;
    private Integer miuTypeId;
    private Instant dateTime;  //date of config change?
    private boolean receivedFromCmiu;
    private String changeNote;
    private int siteId; //optional
    private Instant lastHeardTime;
    private String iccid;
    private String msisdn;
    private String networkProvider;
    private String eui;
    private boolean meterActive;

    private CmiuConfigSet cmiuConfigSet;

    public int getMiuId()
    {
        return miuId;
    }

    public void setMiuId(int miuId)
    {
        this.miuId = miuId;
    }

    public Instant getDateTime()
    {
        return dateTime;
    }

    public void setDateTime(Instant dateTime)
    {
        this.dateTime = dateTime;
    }

    public boolean isReceivedFromCmiu()
    {
        return receivedFromCmiu;
    }

    public void setReceivedFromCmiu(boolean receivedFromCmiu)
    {
        this.receivedFromCmiu = receivedFromCmiu;
    }

    public String getChangeNote()
    {
        return changeNote;
    }

    public void setChangeNote(String changeNote)
    {
        this.changeNote = changeNote;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public CmiuConfigSet getCmiuConfigSet()
    {
        return cmiuConfigSet;
    }

    public void setCmiuConfigSet(CmiuConfigSet cmiuConfigSet)
    {
        this.cmiuConfigSet = cmiuConfigSet;
    }

    public Instant getLastHeardTime()
    {
        return lastHeardTime;
    }

    public void setLastHeardTime(Instant lastHeardTime)
    {
        this.lastHeardTime = lastHeardTime;
    }

    public String getIccid()
    {
        return iccid;
    }

    public void setIccid(String iccid)
    {
        this.iccid = iccid;
    }

    public String getMsisdn()
    {
        return msisdn;
    }

    public void setMsisdn(String msisdn)
    {
        this.msisdn = msisdn;
    }

    public String getNetworkProvider()
    {
        return networkProvider;
    }

    public void setNetworkProvider(String simMno)
    {
        this.networkProvider = simMno;
    }

    public String getMiuType()
    {
        return miuType;
    }

    public void setMiuType(String miuType)
    {
        this.miuType = miuType;
    }

    public Integer getMiuTypeId()
    {
        return miuTypeId;
    }

    public void setMiuTypeId(Integer miuTypeId)
    {
        this.miuTypeId = miuTypeId;
    }

    public String getEui()
    {
        return eui;
    }

    public void setEui(String eui)
    {
        this.eui = eui;
    }

    public boolean isMeterActive()
    {
        return meterActive;
    }

    public void setMeterActive(boolean meterActive)
    {
        this.meterActive = meterActive;
    }
}
