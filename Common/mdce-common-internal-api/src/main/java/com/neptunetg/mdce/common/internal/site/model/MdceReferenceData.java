/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.common.internal.site.model;

import java.util.ArrayList;
import java.util.List;

/**
 * MdceReferenceData
 *
 * {
 *   "distributers": [
 *     ...
 *   ],
 *   "utilities": [
 *     ...
 *   ]}
 * }
 */
public class MdceReferenceData
{

    private List<MdceReferenceDataDistributer> distributers = new ArrayList<>();

    private List<MdceReferenceDataUtility> utilities = new ArrayList<>();


    public List<MdceReferenceDataUtility> getUtilities()
    {
        return utilities;
    }

    public void setUtilities(List<MdceReferenceDataUtility> utilities)
    {
        this.utilities = utilities;
    }

    public List<MdceReferenceDataDistributer> getDistributers()
    {
        return distributers;
    }

    public void setDistributers(List<MdceReferenceDataDistributer> distributers)
    {
        this.distributers = distributers;
    }
}

