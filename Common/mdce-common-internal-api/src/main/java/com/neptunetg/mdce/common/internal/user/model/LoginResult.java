/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.user.model;

/**
 * Created by MC5 on 04/10/2016.
 * Represents the state of a user login request.
 */
public enum LoginResult
{
    /**
     * The supplied credentials are correct.
     */
    OK,
    /**
     * The supplied credentials are incorrect or there was an error during verification.
     */
    LOGIN_FAILED,
    /**
     * The account is currently disabled.
     */
    ACCOUNT_DISABLED
}
