/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 */

package com.neptunetg.mdce.common.internal.gateway.model;

/**
 * Created by WJD1 on 04/02/2016.
 * Used to store gateway heard packets stats from SQL
 */
public class GatewayHeardPackets
{
    private String gatewayId;
    private String heardTime;
    private int miusHeard;
    private int ookPacketsHeard;
    private int fskPacketsHeard;
    private int configPacketsHeard;

    public int getMiusHeard()
    {
        return miusHeard;
    }

    public void setMiusHeard(int miusHeard)
    {
        this.miusHeard = miusHeard;
    }

    public int getOokPacketsHeard()
    {
        return ookPacketsHeard;
    }

    public void setOokPacketsHeard(int ookPacketsHeard)
    {
        this.ookPacketsHeard = ookPacketsHeard;
    }

    public int getFskPacketsHeard()
    {
        return fskPacketsHeard;
    }

    public void setFskPacketsHeard(int fskPacketsHeard)
    {
        this.fskPacketsHeard = fskPacketsHeard;
    }

    public int getConfigPacketsHeard()
    {
        return configPacketsHeard;
    }

    public void setConfigPacketsHeard(int configPacketsHeard)
    {
        this.configPacketsHeard = configPacketsHeard;
    }

    public String getGatewayId()
    {
        return gatewayId;
    }

    public void setGatewayId(String gatewayId)
    {
        this.gatewayId = gatewayId;
    }

    public String getHeardTime()
    {
        return heardTime;
    }

    public void setHeardTime(String heardTime)
    {
        this.heardTime = heardTime;
    }

}
