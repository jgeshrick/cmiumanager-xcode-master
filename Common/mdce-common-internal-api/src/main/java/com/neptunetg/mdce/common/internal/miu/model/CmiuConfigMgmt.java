/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.internal.miu.model;

import java.time.Instant;

/**
    Cmiu config management object. Stores configuration for a CMIU.
 */
public class CmiuConfigMgmt
{
    private long id;
    private long cmiuId;

    private CmiuConfigSet currentConfig;

    private Instant currentConfigApplyDate;

    private CmiuConfigSet defaultConfig;

    private CmiuConfigSet plannedConfig;
    private Instant plannedConfigApplyDate;
    private Instant plannedConfigRevertDate;

    private CmiuConfigSet reportedConfig;
    private Instant reportedConfigDate;

    public CmiuConfigMgmt() {}

    public CmiuConfigMgmt(long cmiuId )
    {
        this.setCmiuId(cmiuId);
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public CmiuConfigSet getCurrentConfig()
    {
        return currentConfig;
    }

    public void setCurrentConfig(CmiuConfigSet currentConfig)
    {
        this.currentConfig = currentConfig;
    }

    public CmiuConfigSet getDefaultConfig()
    {
        return defaultConfig;
    }

    public void setDefaultConfig(CmiuConfigSet defaultConfig)
    {
        this.defaultConfig = defaultConfig;
    }

    public CmiuConfigSet getPlannedConfig()
    {
        return plannedConfig;
    }

    public void setPlannedConfig(CmiuConfigSet plannedConfig)
    {
        this.plannedConfig = plannedConfig;
    }

    public Instant getPlannedConfigApplyDate()
    {
        return plannedConfigApplyDate;
    }

    public void setPlannedConfigApplyDate(Instant plannedConfigApplyDate)
    {
        this.plannedConfigApplyDate = plannedConfigApplyDate;
    }

    public Instant getPlannedConfigRevertDate()
    {
        return plannedConfigRevertDate;
    }

    public void setPlannedConfigRevertDate(Instant plannedConfigRevertDate)
    {
        this.plannedConfigRevertDate = plannedConfigRevertDate;
    }

    public CmiuConfigSet getReportedConfig()
    {
        return reportedConfig;
    }

    public void setReportedConfig(CmiuConfigSet reportedConfig)
    {
        this.reportedConfig = reportedConfig;
    }

    public Instant getReportedConfigDate()
    {
        return reportedConfigDate;
    }

    public void setReportedConfigDate(Instant reportedConfigDate)
    {
        this.reportedConfigDate = reportedConfigDate;
    }

    public long getCmiuId()
    {
        return cmiuId;
    }

    public void setCmiuId(long cmiuId)
    {
        this.cmiuId = cmiuId;
    }

    public Instant getCurrentConfigApplyDate()
    {
        return currentConfigApplyDate;
    }

    public void setCurrentConfigApplyDate(Instant currentConfigApplyDate)
    {
        this.currentConfigApplyDate = currentConfigApplyDate;
    }
}
