/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.miu.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigChange;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigHistory;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigMgmt;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Interface for internal API service for changing CMIU configuration.
 */
public interface CmiuConfigMgmtService
{
    /**
     * URL of the service
     */
    String URL_CMIU_CONFIG = "/internal/miu-config";
    String URL_CMIU_CONFIG_CHANGE_IN_PROCESS = URL_CMIU_CONFIG + "/in-process";
    String URL_CMIU_CONFIG_CHANGE_COMPLETED = URL_CMIU_CONFIG + "/process-completed";
    String URL_CMIU_CONFIG_HISTORY = URL_CMIU_CONFIG + "/history";

    /**
     * Parameter name for CMIU ID
     */
    String PARAM_CMIU_ID = "cmiuId";
    String PARAM_CMIU_CONFIG_MGMT_LIST = "cmiuConfigSetList";
    String PARAM_CONFIG_SET_LAST_CHANGED_DAYS_AGO = "lastChangedDaysAgo";
    String PARAM_USER = "user";

    /**
     * Get the CMIU config managment details for a cmiu. This includes the current config, planned config, reported config etc.
     * @param cmiuId the cmiu id
     * @return Status counts
     * @throws InternalApiException If something goes wrong in the request
     */
    @RequestMapping(URL_CMIU_CONFIG)
    @ResponseBody
    CmiuConfigMgmt getCmiuConfigMgmt(@RequestParam(PARAM_CMIU_ID) long cmiuId) throws InternalApiException;

    /**
     * Mass commit a list of new CMIU configuration changes for different CMIUs.
     * @param cmiuConfigChangeList list of config set for each cmiu id
     * @return
     * @throws InternalApiException
     */
    @RequestMapping(value = URL_CMIU_CONFIG, method = RequestMethod.POST)
    @ResponseBody
    CmiuConfigMgmt addCmiuConfig(@RequestParam(value = PARAM_USER) String userName, @RequestParam(PARAM_CMIU_CONFIG_MGMT_LIST) List<CmiuConfigChange> cmiuConfigChangeList) throws InternalApiException, JsonProcessingException;

    /**
     * Get a list of cmiu config mgmt entries representing CMIU config commands that are being schedule for sending to CMIU.
     * This is done by comparing the configSet value of current and reported config set regardless of the config set id. If they are different,
     * then a config change has been made and is awaiting confirmation from the CMIU that it has been implemented in the hardware.
     * @return list of CMIU which are awaiting confirmation of changes to their config.
     */
    @RequestMapping(value = URL_CMIU_CONFIG_CHANGE_IN_PROCESS)
    @ResponseBody
    List<CmiuConfigMgmt> getCmiuConfigChangeInProcessList() throws InternalApiException;

    /**
     * Get a list of cmiu config mgmt entries representing CMIU config commands that has been changed and acknowledged (reported) by the CMIU
     * for the last X days.
     * This filters the Cmiu config mgmt table to find reported config which has been changed in the last X days and matches planned config.
     * @param changedDaysAgo    the number of days ago the changes has been made to the reported config
     * @return filtered list of cmiu mgmt table
     * @throws InternalApiException
     */
    @RequestMapping(value = URL_CMIU_CONFIG_CHANGE_COMPLETED)
    @ResponseBody
    List<CmiuConfigMgmt> getCmiuConfigChangeCompletedList(@RequestParam(PARAM_CONFIG_SET_LAST_CHANGED_DAYS_AGO) int changedDaysAgo) throws InternalApiException;


    @RequestMapping(value = URL_CMIU_CONFIG_HISTORY)
    @ResponseBody
    List<CmiuConfigHistory> getCmiuConfigHistory(@RequestParam(PARAM_CMIU_ID) long cmiuId) throws InternalApiException;
}
