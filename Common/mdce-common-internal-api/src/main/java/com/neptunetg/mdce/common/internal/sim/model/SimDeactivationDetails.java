/* ***************************************************************************
        *
        *    Neptune Technology Group
        *    Copyright 2017 as unpublished work.
        *    All rights reserved
        *
        *    The information contained herein is confidential
        *    property of Neptune Technology Group. The use, copying, transfer
        *    or disclosure of such information is prohibited except by express
        *    written agreement with Neptune Technology Group.
        *
        *****************************************************************************/


package com.neptunetg.mdce.common.internal.sim.model;


/**
 * Created by dcloud on 3/16/2017.
 */
public class SimDeactivationDetails
{

    private String result;

    private String oldState;

    private String newState;

    private long miuId;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getOldState() {
        return oldState;
    }

    public void setOldState(String oldState) {
        this.oldState = oldState;
    }

    public String getNewState() {
        return newState;
    }

    public void setNewState(String newState) {
        this.newState = newState;
    }

    public long getMiuId() {
        return miuId;
    }

    public void setMiuId(long miuId) {
        this.miuId = miuId;
    }
}

