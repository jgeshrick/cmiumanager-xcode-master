/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.common.internal.user.model;

/**
 * Created by WJD1 on 12/08/2015.
 * Class to store details for modifying a users password
 */
public class NewPasswordDetails
{
    int userId;
    String password;

    public NewPasswordDetails(int userId, String email, String password, String userLevel)
    {
        this.userId = userId;
        this.password = password;
    }

    public NewPasswordDetails()
    {
        //Default constructor
    }

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

}
