/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.alert.model;

/**
 * Created by sml1 on 29/09/2015.
 */
public enum AlertState
{
    NEW,
    HANDLED,
    STALE,
    CLEARED;

    public static AlertState getAlertState(String value)
    {
        for (AlertState alertState : values())
        {
            if (alertState.name().equals(value)
                    || alertState.getState().equals(value))
            {
                return alertState;
            }
        }
        return null;
    }

    public String getState()
    {
        return name().toLowerCase();
    }

}
