/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.audit.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.audit.model.AuditLogPagedResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Backend service for requesting audit log and filters for mdce-web
 */
public interface AuditLogService
{
    /**
     * URL of the service
     */
    String URL_AUDIT_LOG = "/internal/audit-log";
    String URL_AUDIT_LOG_ACCESS_DENIED = "/internal/audit-log/access-denied";

    String REQUEST_PARAM_AUDIT_FILTER_MIU = "miu";
    String REQUEST_PARAM_AUDIT_FILTER_USERNAME = "user";
    String REQUEST_PARAM_AUDIT_LOG_DAYS_AGO = "days-ago";
    String REQUEST_PARAM_AUDIT_LOG_PAGE = "page";
    String REQUEST_PARAM_AUDIT_USERNAME = "userName";
    String REQUEST_PARAM_AUDIT_DESCRIPTION = "description";

    @RequestMapping(value= URL_AUDIT_LOG, method= RequestMethod.GET)
    AuditLogPagedResult getAuditLog(@RequestParam(value = REQUEST_PARAM_AUDIT_FILTER_MIU, required = false) Long miuId,
                               @RequestParam(value = REQUEST_PARAM_AUDIT_FILTER_USERNAME,required = false) String userName,
                               @RequestParam(value = REQUEST_PARAM_AUDIT_LOG_DAYS_AGO, required = false) Integer daysAgo,
                               @RequestParam(value = REQUEST_PARAM_AUDIT_LOG_PAGE, required = false) Integer page
    )
            throws InternalApiException;


    @RequestMapping(value = URL_AUDIT_LOG_ACCESS_DENIED, method = RequestMethod.GET)
    void logAccessDenied(@RequestParam(value = REQUEST_PARAM_AUDIT_USERNAME) String userName,
                         @RequestParam(value = REQUEST_PARAM_AUDIT_DESCRIPTION) String description
    )
            throws InternalApiException;

}
