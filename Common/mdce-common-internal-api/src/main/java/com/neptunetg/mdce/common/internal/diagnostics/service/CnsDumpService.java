/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.diagnostics.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * Provides methods for getting CNS dumps
 */
public interface CnsDumpService
{
    String MNO = "mno";
    String MIU_ID = "miu_id";
    String ICCID = "iccid";
    String IMEI = "imei";
    String SITE_ID = "site_id";
    String FROM = "from";
    String TO = "to";

    String URL_GET_CELLULAR_CONNECTION_HISTORY_WITH_DATE_RANGE = "/internal/diagnostics/getcnsdump/{"+ MNO + "}";
    String URL_DOWNLOAD_CELLULAR_CONNECTION_HISTORY_BY_DATE_RANGE = "/internal/diagnostics/downloadcnsdump/{" + MNO + "}";

    @RequestMapping(value = URL_GET_CELLULAR_CONNECTION_HISTORY_WITH_DATE_RANGE, method = RequestMethod.GET)
    public InputStream getCellularConnectionHistoryWithDateRange(@PathVariable(MNO) String mno,
                                                          @RequestParam(value = MIU_ID, required = false, defaultValue = "") Integer miuIdParam,
                                                          @RequestParam(value = ICCID, required = false, defaultValue = "") String iccidParam,
                                                          @RequestParam(value = IMEI, required = false, defaultValue = "") String imeiParam,
                                                          @RequestParam(value = SITE_ID, required = false, defaultValue = "") Integer siteIdParam,
                                                          @RequestParam(value = FROM) String fromDate,
                                                          @RequestParam(value = TO) String toDate) throws InternalApiException, IOException;


    @RequestMapping(value = URL_DOWNLOAD_CELLULAR_CONNECTION_HISTORY_BY_DATE_RANGE, method = RequestMethod.GET)
    public InputStream downloadCellularUsageHistoryByDateRange(@PathVariable(MNO) String mno,
                                                        @RequestParam(value = MIU_ID, required = false, defaultValue = "") Integer miuIdParam,
                                                        @RequestParam(value = ICCID, required = false, defaultValue = "") String iccidParam,
                                                        @RequestParam(value = IMEI, required = false, defaultValue = "") String imeiParam,
                                                        @RequestParam(value = SITE_ID, required = false, defaultValue = "") Integer siteIdParam,
                                                        @RequestParam(value = FROM) String fromDate,
                                                        @RequestParam(value = TO) String toDate) throws InternalApiException, IOException;
}
