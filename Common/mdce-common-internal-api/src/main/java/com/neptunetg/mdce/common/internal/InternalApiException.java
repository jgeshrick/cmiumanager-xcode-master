/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.common.internal;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * An exception in the internal API invocation
 */
@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="MDCE API exception")
public class InternalApiException extends Exception
{
    /**
     * Exception with message only
     * @param message The message
     */
    public InternalApiException(String message)
    {
        super(message);
    }

    /**
     * Exception with message and cause
     * @param message Message
     * @param cause Cause
     */
    public InternalApiException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
