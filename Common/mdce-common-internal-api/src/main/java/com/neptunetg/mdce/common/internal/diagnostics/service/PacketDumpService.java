/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.diagnostics.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;

/**
 * Provides methods for getting a CSV packet dump
 */
public interface PacketDumpService
{
    String URL_PACKET_DUMP = "/internal/diagnostics/packetdump";

    String L900_ID = "l900_id";
    String CMIU_ID = "cmiu_id";
    String ICCID = "iccid";
    String IMEI = "imei";
    String CMIU_SITE_ID = "cmiu_site_id";
    String DAYS = "days";

    @ResponseBody
    @RequestMapping(value= URL_PACKET_DUMP, method= RequestMethod.GET)
    InputStream getPacketDump(
            @RequestParam(value = L900_ID, required = false, defaultValue = "") String l900IdParam,
            @RequestParam(value = CMIU_ID, required = false, defaultValue = "") String cmiuIdParam,
            @RequestParam(value = ICCID, required = false, defaultValue = "") String iccid,
            @RequestParam(value = IMEI, required = false, defaultValue = "") String imei,
            @RequestParam(value = CMIU_SITE_ID, required = false, defaultValue = "") Integer siteId,
            @RequestParam(value = DAYS, required = false) Integer daysOfRecord
    ) throws IOException, InternalApiException;
}
