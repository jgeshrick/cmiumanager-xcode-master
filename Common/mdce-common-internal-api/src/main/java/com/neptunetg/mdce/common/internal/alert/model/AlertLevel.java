/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.alert.model;

/**
 * Created by sml1 on 29/09/2015.
 */
public enum AlertLevel
{
    WARNING,
    ERROR;

    public static AlertLevel getAlertLevel(String value)
    {
        for (AlertLevel alertLevel : values())
        {
            if (alertLevel.name().equals(value)
                    || alertLevel.getLevel().equals(value))
            {
                return alertLevel;
            }
        }
        return null;
    }

    public String getLevel()
    {
        return name().toLowerCase();
    }

}
