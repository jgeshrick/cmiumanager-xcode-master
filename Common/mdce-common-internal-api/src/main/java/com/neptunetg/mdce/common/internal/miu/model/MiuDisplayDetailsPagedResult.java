/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.miu.model;

import com.neptunetg.mdce.common.internal.PagedResult;

import java.util.List;

public class MiuDisplayDetailsPagedResult extends PagedResult<MiuDisplayDetails>
{
    public MiuDisplayDetailsPagedResult(Integer totalResults, Integer numResults, Integer page, List<MiuDisplayDetails> results)
    {
        super(totalResults, numResults, page, results);
    }

    public MiuDisplayDetailsPagedResult()
    {

    }
}
