/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.alert.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.time.ZonedDateTime;

/**
 * Class to store alert details
 */
public class AlertDetails
{
    private final static String TRUNCATED = " ...";
    private final static int MAX_MESSAGE_LENGTH = 1000 - TRUNCATED.length();

    private long alertId;
    private String alertSource;
    private ZonedDateTime alertCreationDate;
    private ZonedDateTime latestAlertDate;

    //Can't be null
    private AlertState alertState = null;
    private AlertLevel alertLevel = null;
    private String recentMessage = "";
    private String alertTicketId = "";

    public long getAlertId()
    {
        return alertId;
    }

    public void setAlertId(long alertId)
    {
        this.alertId = alertId;
    }

    public String getAlertSource()
    {
        return alertSource;
    }

    public void setAlertSource(String alertSource)
    {
        this.alertSource = alertSource;
    }

    public ZonedDateTime getAlertCreationDate()
    {
        return alertCreationDate;
    }

    public void setAlertCreationDate(ZonedDateTime alertCreationDate)
    {
        this.alertCreationDate = alertCreationDate;
    }

    public ZonedDateTime getLatestAlertDate()
    {
        return latestAlertDate;
    }

    public void setLatestAlertDate(ZonedDateTime latestAlertDate)
    {
        this.latestAlertDate = latestAlertDate;
    }

    public AlertState getAlertState()
    {
        return alertState;
    }

    @JsonSetter
    public void setAlertState(AlertState alertState)
    {
        this.alertState = alertState;
    }

    @JsonIgnore
    public void setAlertState(String alertState)
    {
        //alertState is NOT NULL in the db so could throw exception here
        this.alertState = AlertState.getAlertState(alertState.toUpperCase());
    }

    public AlertLevel getAlertLevel()
    {
        return alertLevel;
    }

    @JsonIgnore
    public void setAlertLevel(String alertLevel)
    {
        //level is NOT NULL in the db so could throw exception here
        this.alertLevel = AlertLevel.getAlertLevel(alertLevel);
    }

    @JsonSetter
    public void setAlertLevel(AlertLevel level)
    {
        this.alertLevel = level;
    }

    public String getRecentMessage()
    {
        return recentMessage;
    }

    public void setRecentMessage(String recentMessage)
    {
        int length = recentMessage.length();

        if (length > 0 && length > MAX_MESSAGE_LENGTH)
        {
            this.recentMessage = recentMessage.substring(0, length-1) + TRUNCATED;
        }
        else
        {
            this.recentMessage = recentMessage;
        }
    }

    public String getAlertTicketId()
    {
        return alertTicketId;
    }

    public void setAlertTicketId(String alertTicketId)
    {
        this.alertTicketId = alertTicketId;
    }

}
