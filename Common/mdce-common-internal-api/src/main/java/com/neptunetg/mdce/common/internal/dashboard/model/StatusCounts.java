/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.common.internal.dashboard.model;

import java.io.Serializable;

/**
 * Counts for a pie chart - failed, success and pending
 */
public class StatusCounts implements Serializable
{
    private int failCount;

    private int successCount;

    private int pendingCount;

    public int getFailCount()
    {
        return failCount;
    }

    public void setFailCount(int failCount)
    {
        this.failCount = failCount;
    }

    public int getSuccessCount()
    {
        return successCount;
    }

    public void setSuccessCount(int successCount)
    {
        this.successCount = successCount;
    }

    public int getPendingCount()
    {
        return pendingCount;
    }

    public void setPendingCount(int pendingCount)
    {
        this.pendingCount = pendingCount;
    }

    /**
     * Add another set of status counts
     * @param toAdd Counts to add to this one
     */
    public void add(StatusCounts toAdd)
    {
        failCount += toAdd.failCount;
        successCount += toAdd.successCount;
        pendingCount += toAdd.pendingCount;

    }
}
