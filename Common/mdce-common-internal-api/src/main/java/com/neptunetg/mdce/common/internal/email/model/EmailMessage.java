/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.email.model;

/**
 * Allows construction of an outbound email message.
 */
public class EmailMessage
{
    /**
     * Email addresses to send email to
     */
    private String[] recipientAddresses;
    /**
     * Email subject
     */
    private String subject;
    /**
     * The contents of the email
     */
    private String contents;

    public EmailMessage()
    {
    }

    public EmailMessage(String[] recipientAddresses, String subject, String contents)
    {
        this.recipientAddresses = recipientAddresses;
        this.subject = subject;
        this.contents = contents;
    }

    public String[] getRecipientAddresses()
    {
        return recipientAddresses;
    }

    public void setRecipientAddresses(String[] recipientAddresses)
    {
        this.recipientAddresses = recipientAddresses;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public String getContents()
    {
        return contents;
    }

    public void setContents(String contents)
    {
        this.contents = contents;
    }
}
