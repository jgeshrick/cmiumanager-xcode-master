/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *
 *  All rights reserved
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.internal.miu.model;

/**
 * Model CMIU details.
 * @todo more fields to be added
 */
public class CmiuDetails
{
    private long miuId;
    private String lastHeardTime;
    private long reading;
    private long simId;
    private int freqBand;
    private boolean roaming;
    private String carrierInfo;
    private float registering;
    private float initializing;
    private String towerId;
    private String network;
    private String localCode;
    private int bsic;
    private int bcc;
    private int rxLevel;
    private int rxDBm;
    private String meterActive;

    public CmiuDetails()
    {}

    public CmiuDetails(long id)
    {
        this.miuId = id;
        this.carrierInfo="VZW";
    }

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public String getLastHeardTime()
    {
        return lastHeardTime;
    }

    public void setLastHeardTime(String lastHeardTime)
    {
        this.lastHeardTime = lastHeardTime;
    }

    public long getReading()
    {
        return reading;
    }

    public void setReading(long reading)
    {
        this.reading = reading;
    }

    public long getSimId()
    {
        return simId;
    }

    public void setSimId(long simId)
    {
        this.simId = simId;
    }

    public int getFreqBand()
    {
        return freqBand;
    }

    public void setFreqBand(int freqBand)
    {
        this.freqBand = freqBand;
    }

    public boolean isRoaming()
    {
        return roaming;
    }

    public void setRoaming(boolean roaming)
    {
        this.roaming = roaming;
    }

    public String getCarrierInfo()
    {
        return carrierInfo;
    }

    public void setCarrierInfo(String carrierInfo)
    {
        this.carrierInfo = carrierInfo;
    }

    public float getRegistering()
    {
        return registering;
    }

    public void setRegistering(float registering)
    {
        this.registering = registering;
    }

    public float getInitializing()
    {
        return initializing;
    }

    public void setInitializing(float initializing)
    {
        this.initializing = initializing;
    }

    public String getTowerId()
    {
        return towerId;
    }

    public void setTowerId(String towerId)
    {
        this.towerId = towerId;
    }

    public String getNetwork()
    {
        return network;
    }

    public void setNetwork(String network)
    {
        this.network = network;
    }

    public String getLocalCode()
    {
        return localCode;
    }

    public void setLocalCode(String localCode)
    {
        this.localCode = localCode;
    }

    public int getBsic()
    {
        return bsic;
    }

    public void setBsic(int bsic)
    {
        this.bsic = bsic;
    }

    public int getBcc()
    {
        return bcc;
    }

    public void setBcc(int bcc)
    {
        this.bcc = bcc;
    }

    public int getRxLevel()
    {
        return rxLevel;
    }

    public void setRxLevel(int rxLevel)
    {
        this.rxLevel = rxLevel;
    }

    public int getRxDBm()
    {
        return rxDBm;
    }

    public void setRxDBm(int rxDBm)
    {
        this.rxDBm = rxDBm;
    }

    public String getMeterActive()
    {
        return meterActive;
    }

    public void setMeterActive(String meterActive)
    {
        this.meterActive = meterActive;
    }
}
