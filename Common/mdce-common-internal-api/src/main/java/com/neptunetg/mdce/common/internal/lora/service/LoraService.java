/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.lora.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.lora.model.LoraDisplayDetails;
import com.neptunetg.mdce.common.internal.sim.service.SimService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.w3c.dom.ls.LSException;

import java.util.List;

/**
 * Created by WJD1 on 05/12/2016.
 * An interface for the LoRa service which provides access to LoRa details
 * for MIUs
 */
public interface LoraService
{
    String URL_LORA_NETWORK_LIST = "/internal/lora/network/list";

    String URL_LORA_DETAILS = "/internal/lora/details";

    /**
     * Get the details for a LoRa device
     */
    @RequestMapping(value = LoraService.URL_LORA_DETAILS, method = RequestMethod.GET)
    LoraDisplayDetails getLoraDetails(int miuId) throws InternalApiException;

    /**
     * Get a list of the LoRa network operators
     */
    @RequestMapping(value = LoraService.URL_LORA_NETWORK_LIST, method = RequestMethod.GET)
    List<String> getLoraNetworkList() throws InternalApiException;
}
