/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.sim.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.sim.model.SimDeactivationDetails;
import com.neptunetg.mdce.common.internal.sim.model.SimDeactivationDisplayDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Service for providing access to miu accounts
 */
public interface SimDeactivationService
{
    /**
     * URL of the service
     */
    String URL_SIM_DETAILS_BY_MIUID = "/internal/sim/deactivation";

    String REQUEST_PARAM_MIU_ID_FOR_DEACTIVATION = "miuId";

    /**
     * add a miu
     */
    @RequestMapping(value = URL_SIM_DETAILS_BY_MIUID, method = RequestMethod.GET)
    SimDeactivationDisplayDetails getDeactivationDetails(@RequestParam(REQUEST_PARAM_MIU_ID_FOR_DEACTIVATION) String miuID) throws InternalApiException;

    @RequestMapping(value = URL_SIM_DETAILS_BY_MIUID, method = RequestMethod.POST)
    SimDeactivationDetails deactivateSims(@RequestParam(REQUEST_PARAM_MIU_ID_FOR_DEACTIVATION) String miuID) throws InternalApiException;
}
