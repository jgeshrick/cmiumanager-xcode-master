/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.audit;

public enum AuditEventType
{
    AUDIT_EVENT_TYPE_UNKNOWN(0, "Unknown"),
    AUDIT_EVENT_TYPE_MIU(1, "MIU changed"),

    AUDIT_EVENT_TYPE_MIU_CONFIG_CHANGED(2, "MIU config changed"),
    AUDIT_EVENT_TYPE_MIU_CELLULAR_CONFIG(3, "MIU cellular changed"),
    AUDIT_EVENT_TYPE_MIU_SEND_COMMAND(4, "Send command"),
    AUDIT_EVENT_TYPE_MIU_SET_OWNERSHIP(5, "Set MIU ownership"),

    AUDIT_EVENT_TYPE_USER_CREATED(6, "User created"),
    AUDIT_EVENT_TYPE_USER_EDIT(7, "User detail changed"),
    AUDIT_EVENT_TYPE_USER_REMOVED(8, "User removed"),

    AUDIT_EVENT_TYPE_NEW_MIU(9, "MIU created"),

    AUDIT_EVENT_TYPE_CONFIG_NEW(10, "Config set created"),
    AUDIT_EVENT_TYPE_CONFIG_CHANGE(11, "Config set change"),
    AUDIT_EVENT_TYPE_MIU_REPORT(12, "Miu report"),

    AUDIT_EVENT_TYPE_MIU_SET_OWNERSHIP_REFUSED(13, "Set MIU ownership - REFUSED"),

    AUDIT_EVENT_TYPE_MDCE_SETTING_CHANGE(14, "Change MDCE setting"),

    //FPC related audit events
    AUDIT_EVENT_TYPE_FPC_REGISTER_DEVICE(15, "FPC register device"),
    AUDIT_EVENT_TYPE_FPC_ACTIVATE_DEVICE(16, "FPC activate device"),

    AUDIT_EVENT_TYPE_VERIFY_USER_FAILED(17, "User verification failed"),

    AUDIT_EVENT_TYPE_ACCESS_DENIED(18, "Access denied"),

    ;

    private final int id;
    private final String name;

    AuditEventType(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    /**
     * Get the enum from id
     *
     * @param id
     * @return
     */
    public static AuditEventType fromId(int id)
    {
        for (AuditEventType auditEventType : AuditEventType.values())
        {
            if (auditEventType.getId() == id)
            {
                return auditEventType;
            }
        }

        return AUDIT_EVENT_TYPE_UNKNOWN;

    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }
}
