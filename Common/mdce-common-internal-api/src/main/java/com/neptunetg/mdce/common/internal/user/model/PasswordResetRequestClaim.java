/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.user.model;

/**
 * Created by MC5 on 06/10/2016.
 * Used to set the 'claimed' flag on a password reset request.
 */
public class PasswordResetRequestClaim
{
    private long passwordResetRequestId;
    private String claimantIpAddress;

    public long getPasswordResetRequestId()
    {
        return passwordResetRequestId;
    }

    public void setPasswordResetRequestId(long passwordResetRequestId)
    {
        this.passwordResetRequestId = passwordResetRequestId;
    }

    public String getClaimantIpAddress()
    {
        return claimantIpAddress;
    }

    public void setClaimantIpAddress(String claimantIpAddress)
    {
        this.claimantIpAddress = claimantIpAddress;
    }
}
