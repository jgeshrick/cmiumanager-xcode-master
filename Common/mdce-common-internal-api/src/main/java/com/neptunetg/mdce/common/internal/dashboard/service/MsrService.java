/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.common.internal.dashboard.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.dashboard.model.StatusCounts;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Interface for internal API service to get MSR stats
 */
public interface MsrService
{
    /**
     * URL of the service
     */
    String URL_MSR_BY_GROUP = "/internal/msr-by-group";

    /**
     * Parameter name for group ID
     */
    String PARAM_MSR_GROUP_ID = "groupId";

    /**
     * Get the MSR for a group
     * @param groupId ID uniquely identifying group of endpoints
     * @return Status counts
     * @throws InternalApiException If something goes wrong in the request
     */
    @RequestMapping(URL_MSR_BY_GROUP)
    @ResponseBody
    StatusCounts getMsrByGroup(@RequestParam(PARAM_MSR_GROUP_ID) String groupId) throws InternalApiException;

}
