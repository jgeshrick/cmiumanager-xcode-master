/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *
 *  All rights reserved
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.internal.gateway.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.neptunetg.mdce.common.internal.audit.model.AuditLogPagedResult;
import com.neptunetg.mdce.common.internal.miu.model.MiuDataUsage;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Model Gateway details.
 */
public class GatewayDisplayDetails
{
    // yyyy-MM-dd'T'HH:mm:ss.SSSZ, e.g. 2000-10-31 01:30:00.000-05:00.
    private static final DateTimeFormatter DATE_FORMAT =
            DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("US/Central"));

    private String gatewayId;
    private int siteId;

    private String siteDescription;

    private String lastHeardTime;
    private String creationDate;

    private boolean gateway_active;

    private GatewayHeardPackets gatewayHeardPackets;

    public GatewayMonthlyStats getGatewayMonthlyStats()
    {
        return gatewayMonthlyStats;
    }

    public void setGatewayMonthlyStats(GatewayMonthlyStats gatewayMonthlyStats)
    {
        this.gatewayMonthlyStats = gatewayMonthlyStats;
    }

    public GatewayHeardPackets getGatewayHeardPackets()
    {
        return gatewayHeardPackets;
    }

    public void setGatewayHeardPackets(GatewayHeardPackets gatewayHeardPackets)
    {
        this.gatewayHeardPackets = gatewayHeardPackets;
    }

    private GatewayMonthlyStats gatewayMonthlyStats;

    public boolean isGateway_active()
    {
        return gateway_active;
    }

    public void setGateway_active(boolean gateway_active)
    {
        this.gateway_active = gateway_active;
    }

    public static DateTimeFormatter getDateFormat()
    {
        return DATE_FORMAT;
    }

    public String getGatewayId()
    {
        return gatewayId;
    }

    public void setGatewayId(String gatewayId)
    {
        this.gatewayId = gatewayId;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public String getSiteDescription()
    {
        return siteDescription;
    }

    public void setSiteDescription(String siteDescription)
    {
        this.siteDescription = siteDescription;
    }

    public String getLastHeardTime()
    {
        return lastHeardTime;
    }

    public void setLastHeardTime(String lastHeardTime)
    {
        this.lastHeardTime = lastHeardTime;
    }

    public String getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(String creationDate)
    {
        this.creationDate = creationDate;
    }
}
