/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.miu.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Interface for internal API service to get CMIU stats
 */
public interface CmiuConfigSetService
{
    /**
     * URL of the service
     */
    String URL_CMIU_CONFIG_SET = "/internal/miu-config/config-set";
    String URL_CMIU_CONFIG_SET_LIST = "/internal/miu-config/config-set/list";
    String URL_CMIU_CONFIG_SET_EDIT = "/internal/miu-config/config-set/edit";

    /**
     * Parameter name for CMIU ID
     */
    String PARAM_CONFIG_SET_ID = "configSetId";
    String PARAM_CONFIG_SET = "cmiuConfigSet";
    String PARAM_USER = "user";

    /**
     * Get a list of config sets
     * @return
     * @throws InternalApiException
     */
    @RequestMapping(URL_CMIU_CONFIG_SET_LIST)
    @ResponseBody
    List<CmiuConfigSet> getCmiuConfigSetList() throws InternalApiException;

    /**
     * Get a config set from id
     * @param configSetId the configset id
     * @return
     * @throws InternalApiException
     */
    @RequestMapping(URL_CMIU_CONFIG_SET)
    @ResponseBody
    CmiuConfigSet getCmiuConfigSet(@RequestParam(value = PARAM_CONFIG_SET_ID) Long configSetId) throws InternalApiException;


    /**
     * Add a new CMIU config set to database
     * @param cmiuConfigSet new config set definition
     * @return
     * @throws InternalApiException
     */
    @RequestMapping(value = URL_CMIU_CONFIG_SET, method = RequestMethod.POST)
    @ResponseBody
    void addCmiuConfigSet(@RequestParam(value = PARAM_USER) String userName, @RequestBody CmiuConfigSet cmiuConfigSet) throws InternalApiException, JsonProcessingException;

    /**
     * Edit an existing CMIU config set
     * @param cmiuConfigSet new config set definition
     * @return
     * @throws InternalApiException
     */
    @RequestMapping(value = URL_CMIU_CONFIG_SET_EDIT, method = RequestMethod.POST)
    @ResponseBody
    void editCmiuConfigSet(@RequestParam(value = PARAM_USER) String userName, @RequestBody CmiuConfigSet cmiuConfigSet) throws InternalApiException, JsonProcessingException;
}
