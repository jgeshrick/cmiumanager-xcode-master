/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.user.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Enum representing all the MDCE roles for user authentication
 */
public enum MdceUserRole
{
    /**
     * User is not authorized
     */
    NotAuthorized("", "Not Authorized", 0),

    /**
     * distributor, lowest logged in access level.
     */
    Distributor(LoginConstants.DISTRIBUTOR_ROLE, "Distributor", 1),

    /**
     * Normal site operator who can only read commands, and status dashboard, no write permission
     */
    SiteOperator(LoginConstants.SITE_OPERATOR_ROLE, "Site operator", 2),

    MsocViewer(LoginConstants.MSOC_VIEWER_ROLE, "Msoc viewer", 3),

    /**
     * Send commands, + access to all sites
     */
    MsocOperator(LoginConstants.MSOC_OPERATOR_ROLE, "Msoc operator", 4),

    /**
     * Send commands, users management + access to all sites
     */
    MsocAdmin(LoginConstants.MSOC_ADMIN_ROLE,"Msoc administrator", 5),

    /**
     * Send commands, users management + access to all sites + activate / deactivate cellular devices
     */
    MsocMasterOfCellular(LoginConstants.MSOC_MASTER_OF_CELLULAR,"Msoc master of cellular", 6);

    private final String roleName;
    private final String roleDescription;
    private final int authLevel;

    private MdceUserRole(String name, String description, int value)
    {
        this.roleName = name;
        this.roleDescription = description;
        this.authLevel = value;
    }

    public static MdceUserRole fromAuthLevel(int roleValue)
    {
        MdceUserRole mdceUserRole = NotAuthorized;  //default

        for (MdceUserRole role : MdceUserRole.values())
        {
            if (role.getAuthLevel() == roleValue)
            {
                mdceUserRole = role;
            }
        }

        return mdceUserRole;
    }

    /**
     * Generate a MdceUserRole enum from access level obtained from the database.
     * @param accessLevel access level enum retrieved from database
     * @return MdceUserRole enum object
     */
    public static MdceUserRole fromSqlAccessLevel(String accessLevel)
    {
        try
        {
            return MdceUserRole.valueOf(accessLevel);
        }
        catch (Exception ex)
        {
            return MdceUserRole.NotAuthorized;
        }
    }

    /**
     * Get a subset of UserRole can be used to associate to a new user in add user operation.
     * @return
     */
    public static List<MdceUserRole> getCreatableRoles()
    {
        List<MdceUserRole> creatableRoles = new ArrayList<>();
        for (MdceUserRole role : MdceUserRole.values())
        {
            if (role.getAuthLevel() > 0)
            {
                creatableRoles.add(role);
            }
        }

        return creatableRoles;

    }

    public String getRoleName()
    {
        return roleName;
    }

    public String getRoleDescription()
    {
        // add a ROLE prefix for Spring Security 4, see http://stackoverflow.com/questions/19525380/difference-between-role-and-grantedauthority-in-spring-security
        return this.roleDescription;
    }

    public int getAuthLevel()
    {
        return authLevel;
    }



}
