/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.common.internal.dashboard.model;

import com.neptunetg.mdce.common.internal.user.model.MdceUserRole;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MdceUserRoleTest
{
    @Test
    public void testFromSqlAccessLevel() throws Exception
    {
        assertEquals(MdceUserRole.NotAuthorized, MdceUserRole.fromSqlAccessLevel(MdceUserRole.NotAuthorized.toString()));
        assertEquals(MdceUserRole.Distributor, MdceUserRole.fromSqlAccessLevel(MdceUserRole.Distributor.toString()));
        assertEquals(MdceUserRole.SiteOperator, MdceUserRole.fromSqlAccessLevel(MdceUserRole.SiteOperator.toString()));
        assertEquals(MdceUserRole.MsocOperator, MdceUserRole.fromSqlAccessLevel(MdceUserRole.MsocOperator.toString()));
        assertEquals(MdceUserRole.MsocAdmin, MdceUserRole.fromSqlAccessLevel(MdceUserRole.MsocAdmin.toString()));

        assertEquals(MdceUserRole.NotAuthorized, MdceUserRole.fromSqlAccessLevel(null));
        assertEquals(MdceUserRole.NotAuthorized, MdceUserRole.fromSqlAccessLevel("any other strings"));
    }
}