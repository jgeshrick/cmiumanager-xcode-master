/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.common.internal.client;

import org.junit.Test;

import java.net.URI;
import java.net.URL;

import static org.junit.Assert.assertEquals;

/**
 * Test for SingleServerClient
 */
public class SingleServerClientTest
{
    @Test
    public void testRequestUrl() throws Exception
    {
        SingleServerClient sscNoSlash = new SingleServerClient("junit", new URL("http://notreal/basepath"));
        SingleServerClient sscSlash = new SingleServerClient("junit", new URL("http://notreal/basepath"));

        final URI desiredUrl = new URI("http://notreal/basepath/internal/somethingservice?param1=+&param2=f");

        assertEquals(desiredUrl,
                sscNoSlash.requestUrl("/internal/somethingservice", "param1", " ", "param2", "f"));

        assertEquals(desiredUrl,
                sscSlash.requestUrl("/internal/somethingservice", "param1", " ", "param2", "f"));

        assertEquals(desiredUrl,
                sscNoSlash.requestUrl("internal/somethingservice", "param1", " ", "param2", "f"));

        assertEquals(desiredUrl,
                sscSlash.requestUrl("internal/somethingservice", "param1", " ", "param2", "f"));

    }
}
