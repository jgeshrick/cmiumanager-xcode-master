/******************************************************************************
*******************************************************************************
**
**         Filename: MqttManagerDebug.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/**
 * @addtogroup  MqttManager
 * @{
 */

/*!***************************************************************************
 * @file MqttManagerDebug.h
 *
 * @brief Debug helpers for MqttManager
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/

#ifndef MQTT_MANAGER_DBG_H_
#define MQTT_MANAGER_DBG_H_


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include "MqttManager.h"


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/


void MqttDebugSubscribe(const MQTT_MANAGER_SUBSCRIPTION* pSub);

void MqttDebugPublishSend(const MQTT_MANAGER_PUB* pPub);
void MqttDebugPublishReceive(const MQTT_MANAGER_PUB* pPub);
void MqttDebugPublish(const char* direction, const MQTT_MANAGER_PUB* pPub);
const char* MqttDebugGetPktString(int32_t pktType);
void MqttDebugSessionResults(const MQTT_MANAGER_SESSION_RESULTS* pSr);
void MqttDebugStateChange(E_MQTTM_STATE oldState, E_MQTTM_STATE newState);
void MqtDebugAtTimeout(const MQTT_MANAGER* pM);

#endif

/**
 * @}
 */
