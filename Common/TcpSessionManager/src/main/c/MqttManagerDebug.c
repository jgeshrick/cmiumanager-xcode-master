/******************************************************************************
*******************************************************************************
**
**         Filename: MqttManagerDebug.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup MqttManager 
 * @{
 */


/*!***************************************************************************
 * @file  MqttManagerDebug.c
 *
 * @brief Debug helpers for MqttManager
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/

 
/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <stdio.h>
#include <string.h>
#include "DebugTrace.h"
#include "MqttManagerDebug.h"


//!< Temp buffer length for debug printing msg text
#define MQTT_MAX_DEBUG_DATA_STRING 80


/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/


/******************************************************************************
**    
**           @param[in] pSub The Subscribe msg to print
**              @return None
**  
**  @brief
**         Description: Helper to Debug Print a MQTT_MANAGER_SUBSCRIPTION object
**
******************************************************************************/
void MqttDebugSubscribe(const MQTT_MANAGER_SUBSCRIPTION* pSub)
{
    DebugTrace("SUBSCRIBE : \"%s\" QoS %d\n", 
        pSub->topicFilter, (int32_t) pSub->qos);
}

/******************************************************************************
**    
**           @param[in] pPub The Publish msg to print
**              @return None
**  
**  @brief
**         Description: Helper to debug a PUBLISH packet - SEND
**
******************************************************************************/
void MqttDebugPublishSend(const MQTT_MANAGER_PUB* pPub)
{
      MqttDebugPublish("SEND", pPub);
}

/******************************************************************************
**    
**           @param[in] pPub The Publish msg to print
**              @return None
**  
**  @brief
**         Description: Helper to debug a PUBLISH packet - Receive
**
******************************************************************************/
void MqttDebugPublishReceive(const MQTT_MANAGER_PUB* pPub)
{
      MqttDebugPublish("RECV", pPub);
}

/******************************************************************************
**    
**           @param[in] direction A string typically SEND or RECV
**           @param[in] pPub The Publish msg to print
**              @return None
**  
**  @brief
**         Description: Helper to debug print a PUBLISH packet
**
******************************************************************************/
void MqttDebugPublish(const char* direction, const MQTT_MANAGER_PUB* pPub)
{
    char buf[MQTT_MAX_DEBUG_DATA_STRING] = {0};
    size_t numToCopy = pPub->dataLength;
    
    ASSERT(pPub != NULL);
    ASSERT(pPub->topic != NULL);
    ASSERT(pPub->pData != NULL);

    // Make a null terminated copy of the data to make it printable.
    // Truncate to available space allowing for null term.
    if (numToCopy >= sizeof(buf))
    {
        numToCopy = sizeof(buf) - 1;
    }
 
    if (pPub->pData != NULL)
    {
        memcpy(buf, pPub->pData, numToCopy);
    }

    if (NULL != pPub)
    {
        DebugTrace("%s PUBLISH   : \"%s\" : QoS=%u, Ret=%u, Dup=%u, ID=%u, %d bytes : [%s]\n",
            direction,
            pPub->topic,
            pPub->qos,
            pPub->retained,
            pPub->dup,
            pPub->packetId,
            pPub->dataLength,
            buf);
    }
}

/******************************************************************************
**    
**           @param[in] pktType The packet type
**              @return A string
**  
**  @brief
**         Description: Helper to get text string of type of packet
**
******************************************************************************/
const char* MqttDebugGetPktString(int32_t pktType)
{
    static const char* constMsgStrings[] = 
    {
        "UNKNOWN", "CONNECT", "CONNACK", 
        "PUBLISH", "PUBACK", "PUBREC", 
        "PUBREL", "PUBCOMP", "SUBSCRIBE", 
        "SUBACK", "UNSUBSCRIBE", "UNSUBACK",
        "PINGREQ", "PINGRESP", "DISCONNECT"
    };
 
    if (pktType < 0)
    {
        return "";
    }
    else if (pktType > DISCONNECT)
    {
        return constMsgStrings[0];
    }
    else
    {
        return  constMsgStrings[pktType];
    }
}

/******************************************************************************
**    
**          @param[out] pSr The results to debug print
**              @return None
**  
**  @brief
**         Description: Helper to print results
**
******************************************************************************/
void MqttDebugSessionResults(const MQTT_MANAGER_SESSION_RESULTS* pSr)
{
    DebugTrace("Status: CONNECT     SENT = %u  RECV = %u\n", pSr->sentCounts[CONNECT],    pSr->receivedCounts[CONNECT]    );
    DebugTrace("Status: CONNACK     SENT = %u  RECV = %u\n", pSr->sentCounts[CONNACK],    pSr->receivedCounts[CONNACK]    );
    DebugTrace("Status: PUBLISH     SENT = %u  RECV = %u\n", pSr->sentCounts[PUBLISH],    pSr->receivedCounts[PUBLISH]    );
    DebugTrace("Status: PUBACK      SENT = %u  RECV = %u\n", pSr->sentCounts[PUBACK],     pSr->receivedCounts[PUBACK]     );
    DebugTrace("Status: SUBSCRIBE   SENT = %u  RECV = %u\n", pSr->sentCounts[SUBSCRIBE],  pSr->receivedCounts[SUBSCRIBE]);
    DebugTrace("Status: SUBACK      SENT = %u  RECV = %u\n", pSr->sentCounts[SUBACK],     pSr->receivedCounts[SUBACK]     );    
    DebugTrace("Status: UNSUBSCRIBE SENT = %u  RECV = %u\n", pSr->sentCounts[UNSUBSCRIBE],pSr->receivedCounts[UNSUBSCRIBE]);
    DebugTrace("Status: UNSUBACK    SENT = %u  RECV = %u\n", pSr->sentCounts[UNSUBACK],   pSr->receivedCounts[UNSUBACK]   );
    DebugTrace("Status: PINGREQ     SENT = %u  RECV = %u\n", pSr->sentCounts[PINGREQ],    pSr->receivedCounts[PINGREQ]    );    
    DebugTrace("Status: PINGRESP    SENT = %u  RECV = %u\n", pSr->sentCounts[PINGRESP],   pSr->receivedCounts[PINGRESP]   );
    DebugTrace("Status: DISCONNECT  SENT = %u  RECV = %u\n", pSr->sentCounts[DISCONNECT], pSr->receivedCounts[DISCONNECT] );
    DebugTrace("Status: Bytes SENT = %u;  RECV = %u\n", pSr->sentBytes, pSr->receivedBytes); 
    DebugTrace("\nStatus: Session executed in %ums\n", pSr->sessionExecutionTimeMs);
}

/******************************************************************************
**    
**           @param[in] oldState The from-state
**           @param[in] newState The to-state
**              @return None
**  
**  @brief
**         Description: Helper to print a state change
**
******************************************************************************/
void MqttDebugStateChange(E_MQTTM_STATE oldState, E_MQTTM_STATE newState)
{
    static const char* constStates[] = 
    {
        "E_MQTT_UNINITIALISED_ERROR",
        "E_MQTT_BEGIN",
        "E_MQTT_CONNECT",
        "E_MQTT_WAIT_PKT",
        "E_MQTT_TIMED_OUT",
        "E_MQTT_DO_SUBSCRIBE",
        "E_MQTT_DO_PUBLISH",
        "E_MQTT_DISCONNECT",
        "E_MQTT_DONE_ERROR",
        "E_MQTT_DONE_OK" 
    };
    
    // Trap if more states were added to the enumeration
    ASSERT(E_MQTT_COUNT == 10);

    DebugTrace("STATE %s->%s\n", 
        constStates[(uint32_t)oldState],
        constStates[(uint32_t)newState]);
}


/******************************************************************************
**    
**      @param[in] pM The MQTT Manager object
**              @return None
**  
**  @brief
**         Description: Debug if timed out (CanClose returned false)
**
******************************************************************************/
void MqtDebugAtTimeout(const MQTT_MANAGER* pM)
{
    const MQTT_MANAGER_PUB* pPub = NULL;  
    const MQTT_MANAGER_SUBSCRIPTION* pSub = NULL;
    bool noPubsToSend;
    bool noSubsToSend;
    ASSERT(NULL != pM);

    if (pM->currentSendPub < pM->pSessionParams->pubToSendCount)
    {
        pPub = pM->pSessionParams->pubs[pM->currentSendPub];
    }
 
    if (pM->currentSubscription < pM->pSessionParams->subscriptionCount)
    {
        pSub = pM->pSessionParams->subscriptions[pM->currentSubscription];
    }

    noPubsToSend = (NULL == pPub);
 
    // If session present, then we don't need to send any subs, as the server is already aware. 
    noSubsToSend = (pM->sessionPresent) ? true : (NULL == pSub);

    DebugTrace("Timeout Analysis: NoPubsToSend = %s; NoSubsToSend = %s (sessionPresent = %u)", 
        noPubsToSend ? "TRUE" : "FALSE",
        noSubsToSend ? "TRUE" : "FALSE",
        (uint32_t)pM->sessionPresent);

    DebugTrace("Timeout Analysis: Pubs Rx'd = %u, (expected %u).  PubAcks Rx'd = %u, (expected %u)\n",
        pM->pSessionResults->receivedCounts[PUBLISH] ,
        pM->pSessionParams->expectedPubsToReceive,
        pM->pSessionResults->receivedCounts[PUBACK ],
        pM->pubAcksExpected);
}
/**
 * @}
 */
