/******************************************************************************
*******************************************************************************
**
**         Filename: MqttManager.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup MqttManager
 * @{
 */


/*!***************************************************************************
 * @file MqttManager.h
 *
 * @brief   
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/


#ifndef MQTT_MANAGER_H_
#define MQTT_MANAGER_H_


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include "FsmTypes.h"
#include "MQTTPacket.h"
#include "TransportManager.h"


/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/


// Version of MQTT protocol
#define MQTT_VERSION_3_1_0          3  
#define MQTT_VERSION_3_1_1          4   //Use this. Paho default; but not working on Azure test server yet

#define MQTT_MAX_SUBSCRIPTIONS      4
#define MQTT_MAX_PUBS_TO_SEND       4
#define MQTT_WORKING_BUFFER_SIZE    1024    // Formally, 2048. 1024 matches the ringbuffer size.
#define MQTT_MAX_TOPIC_STRING       200         ///@todo - what to be?
#define MQTT_MAX_PACKET_ID          0xFFFFul

// Time to wait for receiving PUBLISH messages before disconnecting the session
// - Note - for demo Windows socket, this never happens as the recv() is blocking.
// - however, when implemented on a proper non-blocking interface then this is used.
#define DEFAULT_MQTT_SESSION_TIMEOUT_MS     8000


/*===========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                        */
/*===========================================================================*/


typedef enum MQTT_ERROR
{
    MQTT_OK                             = 0,
    MQTT_ERROR_FSM_UNINITIALISED        = 1,    //!< Attempt to run FSM without first initialising it 
    MQTT_ERROR_SOCKET_SEND              = 2,    //!< Failed to send data over connection
    MQTT_ERROR_SERIALISATION            = 4,    //!< Failed to serialise a packet to send (buffer too small?)
    MQTT_ERROR_SOCKET_TIMEOUT           = 5,    //!< Socket RX timed out
    MQTT_ERROR_PUBLISH_PUBACK_MISMATCH  = 6     //!< Failed to receive all pubacks for number of pubs
}
MQTT_ERROR;


/**
** @brief Quality of service enumeration
**/
typedef enum E_MQTT_QOS
{
    E_MQTT_QOS_0                    = 0,
    E_MQTT_QOS_1                    = 1,
    E_MQTT_QOS_2                    = 2
} E_MQTT_QOS;


/**
** @brief Internal state machine states
**/
typedef enum E_MQTTM_STATE
{
    E_MQTT_UNINITIALISED_ERROR = 0,
    E_MQTT_BEGIN,
    E_MQTT_CONNECT,
    E_MQTT_WAIT_PKT,
    E_MQTT_TIMED_OUT,
    E_MQTT_DO_SUBSCRIBE,
    E_MQTT_DO_PUBLISH,
    E_MQTT_DISCONNECT,
    E_MQTT_DONE_ERROR,
    E_MQTT_DONE_OK,
    
    E_MQTT_COUNT
} E_MQTTM_STATE;


/**
** @brief Container for SUBSCRIPTIONS messages (outgoing)
**/
typedef struct MQTT_MANAGER_SUBSCRIPTION 
{
    char*                                       topicFilter; 
    E_MQTT_QOS                                  qos;  
}
MQTT_MANAGER_SUBSCRIPTION;




/**
** @brief Container for PUBLISH messages (incoming or outgoing)
**/
typedef struct MQTT_MANAGER_PUB
{
    char*                                       topic; 
    uint8_t                                     retained;
    uint8_t                                     dup;
    uint16_t                                    packetId;
    E_MQTT_QOS                                  qos;
    const uint8_t*                              pData;
    uint32_t                                    dataLength;
    uint32_t                                    timestamp; 
    uint32_t                                    (*onPopulate)(struct MQTT_MANAGER_PUB* pPubMsg);
}
MQTT_MANAGER_PUB;

/**
** @brief Callback on incoming PUBLISH messages
**/
typedef void (*pfOnPublishMessageReceivedCallback)(const struct MQTT_MANAGER_PUB* pPubMsg);

/**
** @brief Callback for what to do after receiving a PUBACK
**/
typedef void (*pfnOnPubackCallback)(void *pMm);


/**
** @brief The object that runs the MQTT protocol (Wrapper around Paho)
**/
typedef struct MQTT_MANAGER 
{
    E_MQTTM_STATE                               state;
    struct TRANSPORT_MANAGER*                   pTm;    
    const struct  MQTT_MANAGER_SESSION_PARAMS*  pSessionParams;
    struct MQTT_MANAGER_SESSION_RESULTS*        pSessionResults;

    // Needed for Paho code compatibilty
    MQTTPacket_connectData                      mqttConnectData;
    MQTTTransport                               mqttTransport; 

    // Internal working buffer 
    uint8_t                                     buffer[MQTT_WORKING_BUFFER_SIZE];
    uint32_t                                    bufferSize;
    
    E_MQTT_QOS                                  grantedQoS;
    uint16_t                                    nextPacketId;
    uint8_t                                     sessionPresent;
    uint32_t                                    currentSubscription;
    uint32_t                                    currentSendPub;
    uint32_t                                    pubAcksExpected;
    
    uint32_t                                    timeElapsedMs;
    pfOnPublishMessageReceivedCallback          pfOnPublishRxCallback;
    pfnOnPubackCallback                         pfnOnPuback;

    MQTT_ERROR                                  errorState;
}
MQTT_MANAGER;


/**
** @brief Init params for the Mqtt Manager
**/
typedef struct MQTT_MANAGER_INIT_PARAMS 
{
     pfOnPublishMessageReceivedCallback         pfOnPublishRxCallback;
}
MQTT_MANAGER_INIT_PARAMS;


/**
** @brief Container for defining the MQTT session
**/
typedef struct MQTT_MANAGER_SESSION_PARAMS 
{
    char*                                       clientID;
    unsigned short                              keepAliveInterval;
    unsigned char                               cleansession; 
    unsigned char                               willFlag; 
    MQTTPacket_willOptions                      will;
    char*                                       username;
    char*                                       password;
    uint32_t                                    mqttVersion;
    uint32_t                                    expectedPubsToReceive;

    const MQTT_MANAGER_SUBSCRIPTION*            subscriptions[MQTT_MAX_SUBSCRIPTIONS];
    uint32_t                                    subscriptionCount;

    const  MQTT_MANAGER_PUB*                    pubs[MQTT_MAX_PUBS_TO_SEND];
    uint32_t                                    pubToSendCount;

    uint32_t                                    timeoutForWaitMs;
}
MQTT_MANAGER_SESSION_PARAMS;


/*
* Represents the state of a send Publish, whether it has been sent, and if it has 
* been Acked via a PUBACK
*/
typedef enum 
{
    PUB_STATE_NOT_SENT      = 0,    //!< Initialised state
    PUB_STATE_SENT,                 //!< Sent (or pending in TX ringbuffer)
    PUB_STATE_ACKED,                //!< Corresponding PUBACK received

    PUB_STATE_COUNT
} PUB_STATE;


typedef struct MQTT_MANAGER_PUB_RESULTS
{
    PUB_STATE pubState;
    int packetId;
} MQTT_MANAGER_PUB_RESULT;


/**
** @brief Container populated at end of session with statistics / results
**/
typedef struct MQTT_MANAGER_SESSION_RESULTS 
{
    uint32_t                                    sentCounts[DISCONNECT + 1]; // 1-based enum
    uint32_t                                    receivedCounts[DISCONNECT + 1]; // 1-based enum
  
    
    MQTT_MANAGER_PUB_RESULT                     pubResults[MQTT_MAX_PUBS_TO_SEND];

    uint32_t                                    sentBytes;
    uint32_t                                    receivedBytes;
    uint32_t                                    sessionExecutionTimeMs;
}
MQTT_MANAGER_SESSION_RESULTS;


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/

DLL_EXPORT void
    MqttManagerInitSessionParams(MQTT_MANAGER_SESSION_PARAMS* pSessionParams);

DLL_EXPORT 
    int32_t MqttManagerAddSubscription(MQTT_MANAGER_SESSION_PARAMS* pSessionParams,
    const  MQTT_MANAGER_SUBSCRIPTION* pSub);

DLL_EXPORT int32_t 
    MqttManagerAddPublish(MQTT_MANAGER_SESSION_PARAMS* pSessionParams,
    const  MQTT_MANAGER_PUB* pPub);

DLL_EXPORT void    
    MqttManagerInitSessionResults(MQTT_MANAGER_SESSION_RESULTS* pSessionResults);

DLL_EXPORT int32_t  
    MqttManagerFsmInit(MQTT_MANAGER* pM, TRANSPORT_MANAGER* pTm,     
   const MQTT_MANAGER_SESSION_PARAMS* pSessionParams,
   MQTT_MANAGER_SESSION_RESULTS* pSessionResults);

DLL_EXPORT MQTT_MANAGER_SESSION_RESULTS* 
    MqttManagerGetSessionResults(MQTT_MANAGER* pM);

DLL_EXPORT void    
    MqttManagerSetPublishCallback(MQTT_MANAGER* pM, 
    pfOnPublishMessageReceivedCallback cb);

DLL_EXPORT
    E_FSM MqttManagerRun(MQTT_MANAGER* pM, uint32_t);

DLL_EXPORT
    MQTT_ERROR MqttManagerGetErrorState(const MQTT_MANAGER* pM);

DLL_EXPORT void 
    MqttManagerInit(MQTT_MANAGER* pM, 
    const MQTT_MANAGER_INIT_PARAMS* pInitParams,         
    const MQTT_MANAGER_SESSION_PARAMS* pSessionParams,
    MQTT_MANAGER_SESSION_RESULTS* pSessionResults);

DLL_EXPORT int32_t    
    MqttManagerBegin(MQTT_MANAGER* pM, 
    TRANSPORT_MANAGER* pTm);

DLL_EXPORT uint32_t    
    MqttManagerCalculatePublishLength(E_MQTT_QOS qos, const char* topicName, uint32_t payloadLength);

DLL_EXPORT bool    
    MqttManagerWasPublishDelivered(const MQTT_MANAGER_SESSION_RESULTS* pSessionResults , uint32_t pubIndex);

#endif // MQTT_MANAGER_H_

/**
 * @}
 */
