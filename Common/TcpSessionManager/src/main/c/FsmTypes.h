/******************************************************************************
*******************************************************************************
**
**         Filename: FsmTypes.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/**
 * @addtogroup FSM
 * @{
 */

/*!***************************************************************************
 * @file  FsmTypes.h
 *
 * @brief Definitions for run-to completion state machine.
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/

#ifndef FSM_H_
#define FSM_H_


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <stdint.h>


/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/


typedef enum E_FSM
{
    E_RUNNING,
    E_RUNNING_RECONNECT_NEEDED,
    E_DONE_OK,
    E_DONE_OK_RECONNECT_NEEDED,
    E_ERROR
 
} E_FSM;


typedef void    (*FsmInitFunction)(void* );
typedef E_FSM   (*FsmRunFunction)(void*, uint32_t);

#endif

/**
 * @}
 */
