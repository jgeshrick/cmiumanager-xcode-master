/******************************************************************************
*******************************************************************************
**
**         Filename: FileDownloader.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: Downloads a file using HTTP
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup FileDownloader
 * @{
 */


/*!***************************************************************************
 * @file FileDownloader.h
 *
 * @brief  Uses HTTP Get to download a file
 * @author Duncan Willis
 * @date 2015.06.17
 * @version 1.0
 *****************************************************************************/


#ifndef FILE_DOWNLOADER_H_
#define FILE_DOWNLOADER_H_


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include "FsmTypes.h"
#include "TransportManager.h"
#include "HttpClient.h"

/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/
 


/*===========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                        */
/*===========================================================================*/

typedef enum E_FILE_DOWNLOADER_ERROR
{
    FILE_DOWNLOADER_OK                      = 0,
    FILE_DOWNLOADER_UNINITIALISED           = 1,    //!< Attempt to run FSM without first initialising it
    FILE_DOWNLOADER_CONNECTION_FAILED       = 2,
    FILE_DOWNLOADER_FILE_NOT_FOUND          = 3,
    FILE_DOWNLOADER_FILE_OVERFLOW           = 4, //!< More bytes received than actual resource
    FILE_DOWNLOADER_RETRIES_FAILED          = 5, //!< More bytes received than actual resource
    FILE_DOWNLOADER_USER_CANCELLED          = 6, //!< User cancelled the download
    FILE_DOWNLOADER_DISCONNECTED            = 7, //!< System cancelled the download following disconnect
     
    FILE_DOWNLOADER_HTTP_SERVER_ERROR       = 100 //!< Use this as a base to add HTTP_CLIENT errors
}
E_FILE_DOWNLOADER_ERROR;



/**
** @brief Internal state machine states
**/
typedef enum E_FD_STATE
{
    E_FD_UNINITIALIZED_ERROR = 0,
    E_FD_INIT, //initialized but not yet started
    E_FD_RUN,

    E_FD_DONE_OK,
    E_FD_DONE_ERROR,
    E_FD_COUNT
} E_FD_STATE;

#ifndef _WIN32
#pragma push
#pragma diag_suppress 231 // Keil "#231-D: declaration is not visible outside of function"
typedef void (*pfnOnReceivedByte)(struct FILE_DOWNLOADER*, uint8_t, uint32_t index);
#pragma pop
#else
typedef void (*pfnOnReceivedByte)(struct FILE_DOWNLOADER*, uint8_t, uint32_t index);
#endif


/**
** @brief The object that runs the FileDownloader protocol on top of HTTP
**/
typedef struct FILE_DOWNLOADER 
{
    TRANSPORT_MANAGER* pTm;

    HTTP_CLIENT                                 httpClient;
    HTTP_CLIENT_SESSION_PARAMS                  httpSessionParams;
    HTTP_CLIENT_SESSION_RESULTS                 httpSessionResults;

    uint8_t*                                    buffer;
    uint32_t                                    bufferSize;

    void (*OnReceivedByte)(struct FILE_DOWNLOADER*, uint8_t, uint32_t index);
    uint32_t                                    byteCount;
    E_FD_STATE                                  state;

    const char*                                 fileName;
    uint32_t                                    timeElapsedMs;

    E_FILE_DOWNLOADER_ERROR                     errorState;

    uint32_t                                    requestSize;       
    uint32_t                                    requestTimeoutMs;
    uint32_t                                    totalContentLength;
    uint32_t                                    requestRetryCount;
    uint32_t                                    maxRequestRetries;
    uint32_t                                    lastServerResponseCode;
    uint32_t                                    numRequests;
    uint32_t                                    numFailedRequests;
}
FILE_DOWNLOADER;


/**
** @brief Init params for the downloader
**/
typedef struct FILE_DOWNLOADER_INIT_PARAMS 
{
    uint8_t*                                    buffer;
    uint32_t                                    bufferSize;

    const char*                                 fileName;
    uint32_t                                    blockSize;       
    uint32_t                                    maxRequestRetries;

    uint32_t                                    requestTimeoutMs;

    pfnOnReceivedByte                           OnReceivedByte;
}
FILE_DOWNLOADER_INIT_PARAMS;



/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/
DLL_EXPORT void FileDownloaderInit(FILE_DOWNLOADER* pDownloader, 
    const FILE_DOWNLOADER_INIT_PARAMS* pInitParams);
 
DLL_EXPORT void    
    FileDownloaderBegin(FILE_DOWNLOADER* pDownloader, 
    TRANSPORT_MANAGER* pTm);

DLL_EXPORT E_FSM 
    FileDownloaderRun (FILE_DOWNLOADER* pDownloader, uint32_t tickIntervalMs);
 
DLL_EXPORT void FileDownloaderCancel(FILE_DOWNLOADER* pDownloader, E_FILE_DOWNLOADER_ERROR eCancelCode);
DLL_EXPORT void FileDownloaderStopOnDisconnect(FILE_DOWNLOADER* pDownloader);

DLL_EXPORT uint32_t    
    FileDownloaderGetProgressPercent(const FILE_DOWNLOADER* pDownloader, 
    uint32_t* pBytesReceived, uint32_t* pTotalBytes);

void FileDownloaderDebugPrintResult(const FILE_DOWNLOADER* pDownloader);
    
#endif // FILE_DOWNLOADER_H_

/**
 * @}
 */
