/******************************************************************************
*******************************************************************************
**
**         Filename: HttpClient.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup HttpClient
 * @{
 */


/*!***************************************************************************
 * @file HttpClient.h
 *
 * @brief   
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/


#ifndef HTTP_CLIENT_H_
#define HTTP_CLIENT_H_


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include "FsmTypes.h"
#include "TransportManager.h"
#include "HttpUtilities.h"

/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/



#define HTTP_WORKING_BUFFER_SIZE    512  ///@todo buf too small


// Time to wait for receiving before disconnecting the session
// - Note - for demo Windows socket, this never happens as the recv() is blocking.
// - however, when implemented on a proper non-blocking interface then this is used.
#define DEFAULT_HTTP_SESSION_TIMEOUT_MS     500




/*===========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                        */
/*===========================================================================*/


/**
** @brief Internal state machine states
**/
typedef enum E_HR_STATE
{
    E_HTTP_UNINITIALISED_ERROR = 0,

    E_HTTP_BEGIN,   
    E_HTTP_READ_BYTES,
    E_HTTP_CONNECT,
    E_HTTP_WAIT_HEADER,
    E_HTTP_WAIT_CONTENT,
    E_HTTP_SEND_CONTENT,
    E_HTTP_TIMED_OUT,
    E_HTTP_FINISH_OK,
    E_HTTP_SEND_FAIL,
    E_HTTP_STOP,
    
    E_HTTP_COUNT
} E_HR_STATE;

/**
** @brief Internal state machine states
**/

#ifndef _WIN32
#pragma push
#pragma diag_suppress 231 // Keil "#231-D: declaration is not visible outside of function"
typedef void (*pfnOnReceivedContentByte)(struct HTTP_CLIENT* p, uint8_t);
#pragma pop
#else
typedef void (*pfnOnReceivedContentByte)(struct HTTP_CLIENT* p, uint8_t);
#endif


/**
** @brief The object that runs the HTTP protocol (Wrapper around Paho)
**/
typedef struct HTTP_CLIENT 
{
    void*                                       pCtx;
    E_HR_STATE                                  state;
    struct TRANSPORT_MANAGER*                   pTm;    
    const struct  HTTP_CLIENT_SESSION_PARAMS*   pSessionParams;
    struct HTTP_CLIENT_SESSION_RESULTS*         pSessionResults;

    HTTP_LINE_READER                            lineReader;

    // working buffer (allocated externally and supplied at init time)
    uint8_t*                                    buffer;
    uint32_t                                    bufferSize;
    uint32_t                                    bufferPos;

    uint32_t                                    timeElapsedMs;

    HTTP_ERROR                                  errorState;

    void                                        (*OnReceivedContentByte)(struct HTTP_CLIENT*, uint8_t);

    //if server sends "Connection: close", this is set to false and the connection needs to be reopened
    bool                                        isConnectionStillAlive;  

}
HTTP_CLIENT;


/**
** @brief Init params for the Mqtt Manager
**/
typedef struct HTTP_CLIENT_INIT_PARAMS 
{
    uint8_t*                                    buffer;
    uint32_t                                    bufferSize;
    void*                                       pCtx;
    pfnOnReceivedContentByte                    OnReceivedContentByte; 
}
HTTP_CLIENT_INIT_PARAMS;


/**
** @brief Container for defining the HTTP session
**/
typedef struct HTTP_CLIENT_SESSION_PARAMS 
{
    const char*                 fileName;
    const char*                 hostName;
    uint32_t                    firstRangeByte;
    uint32_t                    lastRangeByte;
    uint32_t                    timeoutForWaitMs;
}
HTTP_CLIENT_SESSION_PARAMS;


/**
** @brief Container populated at end of session with statistics / results
**/
typedef struct HTTP_CLIENT_SESSION_RESULTS 
{
    uint32_t                    responseCode;
    
    bool                        hasReceivedHeader;
    uint32_t                    contentBytesReceived;
    bool                        hasContentLength;    
    bool                        hasRange;
    bool                        hasBlankLine;
    uint32_t                    contentLength;              // Of this particular GET (May be partial GET)
    uint32_t                    rangeFirstByte;             // The first byte if partial GET
    uint32_t                    rangeLastByte;              // The last byte if partial GET
    uint32_t                    rangeTotalContentLength;    // The entire resource length byte if partial GET
    uint32_t                    sentBytes;
    uint32_t                    receivedBytes;
    uint32_t                    sessionExecutionTimeMs;
}
HTTP_CLIENT_SESSION_RESULTS;


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/


DLL_EXPORT void
    HttpClientInit(HTTP_CLIENT* pClient, 
    const HTTP_CLIENT_INIT_PARAMS* pInitParams,
    const HTTP_CLIENT_SESSION_PARAMS* pSessionParams,
    HTTP_CLIENT_SESSION_RESULTS* pSessionResults);

DLL_EXPORT void
    HttpClientInitSessionParams(HTTP_CLIENT_SESSION_PARAMS* pSessionParams);

DLL_EXPORT void    
    HttpClientInitSessionResults(HTTP_CLIENT_SESSION_RESULTS* pSessionResults);

DLL_EXPORT void  
    HttpClientBegin(struct HTTP_CLIENT* pClient, TRANSPORT_MANAGER* pTm);

DLL_EXPORT
    E_FSM HttpClientRun(HTTP_CLIENT* pClient, uint32_t);


  bool HttpClientReceiveHeaderHelper(HTTP_CLIENT* pClient, uint8_t b);
#endif // HTTP_CLIENT_H_

/**
 * @}
 */
