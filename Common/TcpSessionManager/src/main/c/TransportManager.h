/******************************************************************************
*******************************************************************************
**
**         Filename: TransportManager.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 /**
 * @addtogroup  
 * @{
 */

/*!***************************************************************************
 * @file TransportManager.h
 *
 * @brief A simple object to represent the data transport once established. (send/recv function pointers)  
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/

#ifndef TRANSPORT_MANAGER_H_
#define TRANSPORT_MANAGER_H_


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include "TransportInterface.h"


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/

 
/**
** @brief Interface used by manager clients (e.g. MqttManager)
**/
void                TransportManagerInit(TRANSPORT_MANAGER* pT, const TRANSPORT_INTERFACE* pInterface, void* pCtx);
int                 TransportManagerCommand(TRANSPORT_MANAGER* pT, E_CONNECTION_COMMAND cmd, void* cmdParams);
void                TransportManagerTick(TRANSPORT_MANAGER* pT, uint32_t tickIntervalMs);
E_CONNECTION_STATE  TransportManagerGetState(const TRANSPORT_MANAGER* pT, E_QUERY query, void* pQueryResult);

// Data fns
int                 TransportManagerReceive(TRANSPORT_MANAGER* pT, unsigned char* buf, int bufferSize);
int                 TransportManagerSend(TRANSPORT_MANAGER* pT, unsigned char* buf, int count);


void TransportManagerInitConnectionParams(CONNECTION_PARAMS* pConnectionParams,
    const char* pHostName,
    uint16_t port);

bool                TransportManagerIsValid(const TRANSPORT_MANAGER* pT);

#endif //TRANSPORT_MANAGER
 /** 
 * @}
 */
