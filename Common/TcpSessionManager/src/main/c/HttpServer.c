/******************************************************************************
*******************************************************************************
**
**         Filename: HttpServer.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: Trivial HTTP Server 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup Http 
 * @{
 */


/*!***************************************************************************
 * @file HttpServer.c
 *
 * @brief Trivial HTTP server
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/

 
 /*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "CommonTypes.h"
#include "DebugTrace.h"
#include "HttpServer.h"


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/

/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/



 
static uint32_t fakeDataSize = 4321;


/******************************************************************************
**    
**      @param[in, out] pServer The HTTP SERVER object
**           @param[in] pInitParams The init data
**              @return None
**  
**  @brief
**         Description: Init an HTTP_SERVER object
**
******************************************************************************/
DLL_EXPORT void
    HttpServerInit(HTTP_SERVER* pServer,
    const HTTP_SERVER_INIT_PARAMS* pInitParams)

{
    pServer->pRbRx              = pInitParams->pRbRx;
    pServer->pRbTx              = pInitParams->pRbTx;

    pServer->errorState         = HTTP_OK;

    pServer->hasGetField        = false;
    pServer->hasRange           = false;
    pServer->hasBlankLine       = false;
    pServer->percentServed      = 0;
    pServer->basePath[0]        = '\0';

    LineReaderInit(&pServer->lineReader,
        pServer->lineBuffer, 
        HTTP_MAX_LINE_LEN);
    
    pServer->pDataProvider = pInitParams->dataProvider;
}


/******************************************************************************
**    
**      @param[in] pServer The HTTP server object
**      @param[in] tickIntervalMs Time in ms between polls of this function
**              @return None
**  
**  @brief
**         Description: Poll loop to read the data stream for HTTP Requests
**         Continue reading bytes while available (do not block)
**         Drop out when no bytes available from stream
**         Handle Responses when a request is found
******************************************************************************/
void  HttpServerProcess(HTTP_SERVER* pServer, uint32_t tickIntervalMs)
{
    bool isComplete = false;
    uint32_t numRx = 0;
    uint8_t b;

    ASSERT(NULL != pServer);

    numRx = RingBufferUsed(pServer->pRbRx);

    while(numRx)
    {
        numRx--;

        b = RingBufferReadOne(pServer->pRbRx);

        isComplete = HttpServerReceiveByteHelper(pServer, b);

        if (isComplete)
        {
            HttpServerSendResponse(pServer);
        }  
    }
}


/******************************************************************************
**    
**      @param[in] pServer The HTTP server object
**              @return None
**  
**  @brief
**         Description: Send the response to a GET
**         Continue reading bytes while available (do not block)
**         Drop out when no bytes available from stream
**         Handle Responses when a request is found
******************************************************************************/
void  HttpServerSendResponse(HTTP_SERVER* pServer)
{
    uint8_t  headerBuffer[HTTP_SERVER_HEADER_BUFFER_SIZE];
    uint8_t  contentBuffer[HTTP_SERVER_CONTENT_BUFFER_SIZE];
    uint32_t headerLength   = 0;       
    uint32_t responseCode   = HTTP_RESPONSE_NOT_FOUND;
    uint32_t contentLength  = 0;
    uint32_t firstRangeByte = 0;
    uint32_t lastRangeByte  = 0;
    uint32_t totalDataSize  = 0;
    uint32_t fifoFree       = 0;
    HTTP_ERROR ecode        = HTTP_ERROR_UNINITIALISED;
    char                    fullFilename[ HTTP_MAX_URL_LEN ];

    fullFilename[0] = '\0';

    // Make a full path and name for fopen(), also skip in test case
    if ((strlen(pServer->resourceStr) + strlen(pServer->basePath) >= HTTP_MAX_URL_LEN) 
        && (pServer->pDataProvider != NULL))
    {
        ecode = HTTP_ERROR_FILE_PATH_LENGTH;
    }
    else
    {
        if (pServer->pDataProvider != NULL)
        {
            strcpy(fullFilename, pServer->basePath);
            strcat(fullFilename, pServer->resourceStr);
        }

        ecode = HttpServeReadDataFromFile(contentBuffer,
            HTTP_SERVER_CONTENT_BUFFER_SIZE, 
            pServer->pDataProvider,
            fullFilename,
            pServer->rangeFirstByte,
            pServer->rangeLastByte,
            &contentLength); 
        
        totalDataSize = HttpServerGetDataSize(pServer->pDataProvider);
    }
  
    switch (ecode)
    {
    case HTTP_OK:
        responseCode = pServer->hasRange ? HTTP_RESPONSE_PARTIAL_CONTENT : HTTP_RESPONSE_OK;
        firstRangeByte = pServer->rangeFirstByte;
        lastRangeByte = pServer->rangeLastByte;
        if (totalDataSize > 0) 
        {
            pServer->percentServed = (100 * (lastRangeByte + 1)) / totalDataSize;
        }
        else
        {
            pServer->percentServed = 0; // No data
        }
        break;

    case HTTP_ERROR_BUFFER_OVERFLOW:
    case HTTP_ERROR_BUFFER_TOO_SMALL:
        responseCode = HTTP_RESPONSE_INTERNAL_ERROR;
        break;

    case HTTP_ERROR_BAD_URL:
    case HTTP_ERROR_FILE_OPEN:
        responseCode = HTTP_RESPONSE_NOT_FOUND;
        break;

    case HTTP_ERROR_FILE_PATH_LENGTH:
    default:
        responseCode = HTTP_RESPONSE_BAD_REQUEST;
        break;
    }

    headerLength = HttpMakeGetResponseHelper(headerBuffer,
        HTTP_SERVER_HEADER_BUFFER_SIZE,
        responseCode,
        contentLength,
        firstRangeByte,
        lastRangeByte,
        totalDataSize);

    fifoFree =  RingBufferFree(pServer->pRbTx);

    if (headerLength + contentLength <= fifoFree)
    {
        if (headerLength > 0)
        {
            RingBufferWrite(pServer->pRbTx, headerBuffer, headerLength);

            if (contentLength > 0)
            {
                RingBufferWrite(pServer->pRbTx, contentBuffer, contentLength);
            }
        }
    }
    else
    {
        ///@todo handle cannot fit error
    }
}


/******************************************************************************
**    
**      @param[in] pServer The HTTP server object
**              @return Percentage of resource now served, following latest request.
**  
**  @brief
**         Description: For progress bar type uses
******************************************************************************/
uint32_t  HttpServerGetPercentServed(const HTTP_SERVER* pServer)
{
    return pServer->percentServed;
}


/******************************************************************************
**    
**      @param[in] pCtx The data provider object
**              @return Total size of the resource in bytes
**  
**  @brief
**         Description: Get the size of the resource to be served (eg file size)
******************************************************************************/
uint32_t HttpServerGetDataSize(const DATA_PROVIDER* pCtx)
{
    DATA_PROVIDER* pDataProvider = (DATA_PROVIDER*)pCtx;
    if (NULL == pDataProvider)
    {
        return fakeDataSize;
    }
    else
    {
        return pDataProvider->sizeBytes;
    }
}


/******************************************************************************
**    
**      @param[out] buffer A buffer to fill with requested data
**      @param[in] bufferSize Size of the buffer
**      @param[in] pCtx The data provider object, NULL for fake data
**      @param[in] fileName A full DOS filename, if used
**      @param[in] firstRangeByte The index of the first byte (0= first)
**      @param[in] lastRangeByte The index of the last byte (N-1 is last if N is filesize)
**      @param[out] pNumBytesRead The number of bytes copied into buffer
**              @return Internal HTTP_ERROR code
**  
**  @brief
**         Description: Read Data from a file on a PC (if appropriate)
**         If context is NULL (no file system), then fill data with test text
** The range MUST have been prechecked
******************************************************************************/
HTTP_ERROR HttpServeReadDataFromFile(uint8_t buffer[], 
    uint32_t bufferSize,
    const DATA_PROVIDER* pCtx,
    const char* fileName,
    uint32_t firstRangeByte, 
    uint32_t lastRangeByte,
    uint32_t* pNumBytesRead)
{
    DATA_PROVIDER*      pDataProvider = (DATA_PROVIDER*)pCtx;
    HTTP_ERROR          ecode = HTTP_ERROR_UNINITIALISED;
    uint32_t            numBytesRead = 0;
    size_t              readCount = 0;
    uint32_t            numBytesToRead = 1 + lastRangeByte - firstRangeByte;

    ASSERT(lastRangeByte >= firstRangeByte);
    ASSERT(NULL != pNumBytesRead);
    ASSERT(NULL != buffer);
    ASSERT(NULL != fileName);


    // Requested range will not fit, or no range...
    if ((numBytesToRead > bufferSize) || (0 == numBytesToRead))
    {
        *pNumBytesRead = 0;
        return HTTP_ERROR_BUFFER_TOO_SMALL;
    }

    // For test, dataProvider ctx may be null. Simple test facilty using fake data
    // fill with dummy data  
    if (NULL == pDataProvider)
    {
        // demo when there is no file system
        memset(buffer, 0, bufferSize);
        strncpy((char*)buffer, "TEST DATA++TEST DATA++", bufferSize);
        *pNumBytesRead = numBytesToRead;

        return HTTP_OK;
    }

    // Bad name?
    if (strlen(fileName) == 0)
    {
        return HTTP_ERROR_BAD_URL;
    }

    if (NULL == pDataProvider->pFile)
    {
        pDataProvider->pFile = fopen(fileName, "rb");
        if (NULL != pDataProvider->pFile)
        {
            // File opened ok - determine size if not already known
            if (0 == pDataProvider->sizeBytes)
            {
                (void)fseek(pDataProvider->pFile, 0, SEEK_END);   // non-portable
                pDataProvider->sizeBytes = ftell(pDataProvider->pFile);
            }

            // Check data content > 0
            if (pDataProvider->sizeBytes > 0)
            {
                // Check range? (includes both == 0, which is entire data)
                if ((firstRangeByte <= lastRangeByte)
                    && (lastRangeByte <= pDataProvider->sizeBytes - 1))
                {
                    // Range OK; Now go to requested part of file
                    ecode = (HTTP_ERROR)fseek(pDataProvider->pFile, firstRangeByte, SEEK_SET);
                    if (0 == ecode)
                    {
                        // Whole file requested (both 0)?
                        if ((0 == lastRangeByte) && (0 == firstRangeByte))
                        {
                            numBytesToRead = pDataProvider->sizeBytes;
                        }

                        // Read from file
                        readCount = fread(buffer, 1, numBytesToRead, pDataProvider->pFile);
                    }
                }
            }

            if (readCount == numBytesToRead)
            {
                // OK -  buffer filled as requested
                numBytesRead = readCount;
                ecode = HTTP_OK;
            }
            else
            {
                // Error - attempt to read over end of file
                numBytesRead = 0;
                ecode = HTTP_ERROR_INCONSISTENT_RANGE;
            }
            
            fclose (pDataProvider->pFile);
        }
        else
        {
            // File did not open - not found etc.
            numBytesRead = 0;
            ecode = HTTP_ERROR_FILE_OPEN;
        }
    }


    pDataProvider->pFile = NULL;

    *pNumBytesRead = numBytesRead;

    return ecode;
}


/******************************************************************************
**    
**      @param[in, out] pServer The HTTP Server object
**      @param[in] b A byte to process from the SERVER (GET request)
**              @return True if the request is finished (valid GET found, end on blank line)
**  
**  @brief
**         Description: Parse stream, get header info. 
**
******************************************************************************/
bool HttpServerReceiveByteHelper(HTTP_SERVER* pServer, uint8_t b)
{    
    const uint8_t* pLine;
    const char *pResource;
    char *pWhitespace;
    bool isComplete = false;

    // read GET request header, line by line
    pLine = LineReaderReadByte(&pServer->lineReader, b);

    if (NULL != pLine)
        {
            DebugTrace("Server RX   \"%s\"", pLine);

            pResource = LineReaderParseString(pLine, "GET");
        
            if (NULL != pResource)
            {
                pServer->hasGetField = true;

                if (strlen(pResource) >= HTTP_MAX_URL_LEN - 1)
                {
                    DebugTrace("ERROR Resource string truncated\n", pLine);
                }

                // Copy the URL, but remove the HTTP\1.1 that follows it                 
                strncpy(pServer->resourceStr, pResource, HTTP_MAX_URL_LEN);
                pWhitespace = strchr(pServer->resourceStr, ' ');
                if (NULL != pWhitespace)
                {
                    *pWhitespace = '\0';
                }
            }
 
            if (LineReaderParseContentRange(pLine, 
               HTTP_PARTIAL_RANGE_STR,
               &(pServer->rangeFirstByte),
               &(pServer->rangeLastByte),
               NULL))
                {
                    pServer->hasRange = true; 
                };

            // Have everything we need from header? 
            // A blank line finishes the request field
            if( LineReaderIsBlankLine(pLine))
            {
                pServer->hasBlankLine = true;
                
                if (pServer->hasGetField)
                {
                    isComplete = true;
                }
            }
        } 

    return isComplete;
}



/******************************************************************************
**    
**      @param[in] pServer The HTTP Server object
**      @param[in] basePath A string to prepend to the file name (to make the full path) 
**              @return True if set OK, false if too long, not set.
**  
**  @brief
**         Description: Used to make URL relative to Docs folder
**
******************************************************************************/
bool  HttpServerSetBasePath(HTTP_SERVER* pServer, const char* basePath)
{
    ASSERT(NULL != basePath);

    if (strlen(basePath) <= HTTP_MAX_URL_LEN)
    {
        strcpy(pServer->basePath, basePath);
        return true;
    }

    return false;
}


/**
 * @}
*/
