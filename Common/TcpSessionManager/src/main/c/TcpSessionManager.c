/******************************************************************************
*******************************************************************************
**
**         Filename: TcpSessionManager.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/**
 * @addtogroup TcpSessionManager
 * @{
 */

/*!***************************************************************************
 * @file TcpSessionManager.c
 *
 * @brief  Handles the connect-run-disconnect MQTT session  
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/

 
/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "FsmTypes.h"
#include "CommonTypes.h"
#include "DebugTrace.h"
#include "TransportManager.h"
#include "TcpSessionManager.h"


/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/
 
static   void TcpSessionManagerOnBeginHelper(const TCP_SESSION_MANAGER* pSm);
static E_FSM TcpSessionManagerOnRunHelper(const TCP_SESSION_MANAGER* pSm,
    uint32_t tickIntervalMs);

static E_SM_STATE TcpSessionManagerOnDoneHelper(TCP_SESSION_MANAGER* pSm);



/******************************************************************************
**    
**      @param[in, out] pSm The Session Manager object
**      @param[in, out] pTm The Transport Manager object on which data is communicated
**      @param[in, out] pMm The Mqtt Manager object which controls the MQTT Protocol
**              @return None
**  
**  @brief
**         Description: This object mananges the establish - run - disconnect session
**
******************************************************************************/
DLL_EXPORT void 
    TcpSessionManagerInit(TCP_SESSION_MANAGER* pSm, 
    TRANSPORT_MANAGER* pTm, 
    CONNECTION_PARAMS* pConnectParams)
{
    pSm->state = E_SM_RESET;

    pSm->pTm            = pTm;
    pSm->pConnectParams = pConnectParams;
    pSm->beginFlag      = false; 
    pSm->pManager       = NULL;
}


/******************************************************************************
**    
**      @param[in, out] pSm The Session Manager object
**           @param[in] pParams The Connection Params to send to the Transport
**           @param[in] pManager The object to use the connection once made
**           @param[in] pfnInit The manager's init function
**           @param[in] pfnRun The manager's run function
**
**              @return None
**  
**  @brief
**         Description: Begin a Session
**
******************************************************************************/
DLL_EXPORT void 
    TcpSessionManagerBegin(TCP_SESSION_MANAGER* pSm, 
    void*                           pManager,
    pfnManagerBeginFunction         pfnBegin,
    pfnManagerRunFunction           pfnRun,
    pfnManagerCompletionFunction    pfnOnComplete)
{
    ASSERT(NULL != pSm);

    pSm->beginFlag          = true;
    pSm->pManager           = pManager;
    pSm->pfnManagerBegin    = pfnBegin;
    pSm->pfnManagerRun      = pfnRun;
    pSm->pfnOnComplete      = pfnOnComplete;
}




/******************************************************************************
**    
**      @param[in, out] pSm The Session Manager object
**           @param[in] tickIntervalMs The time between ticks in ms
**              @return None
**  
**  @brief
**         Description: Tick-process the session 
**
******************************************************************************/
DLL_EXPORT void 
    TcpSessionManagerTick(TCP_SESSION_MANAGER* pSm, uint32_t tickIntervalMs)
{
    E_FSM r;
    E_SM_STATE nextState = pSm->state;
    E_CONNECTION_STATE cs;

    ASSERT(NULL != pSm);

    TransportManagerTick(pSm->pTm, tickIntervalMs);
   
    nextState = pSm->state;

    switch (pSm->state)
    {
    case E_SM_RESET:
        if (pSm->beginFlag)
        {
            nextState = E_SM_BEGIN_OPEN_CONNECTION;
            pSm->beginFlag = false;
        }
        break;
        // Issue command to begin connection
    case E_SM_BEGIN_OPEN_CONNECTION: 
        {
            TransportManagerCommand(pSm->pTm, 
                CON_CMD_OPEN_CONNECTION, 
            (void*)(pSm->pConnectParams));

            nextState = E_SM_OPEN_CONNECTION_WAIT;
        }
        break;

    case E_SM_OPEN_CONNECTION_WAIT:
        {
            // Poll the transport state until connected
            cs = TransportManagerGetState(pSm->pTm, 
                QRY_CONNECTION_STATE,
                NULL);

            r = (CON_STATE_CONNECTION_OPEN_SOCKET_CLOSED == cs) ? E_DONE_OK : E_RUNNING;

            switch(r)
            {
            case E_RUNNING:
                // DebugTrace( "Waiting for connection\n" );
                // Do nothing; Wait while connection is being made
                break; 

            case E_DONE_OK:
                DebugTrace( "Open connection (Device Driver) SUCCESS\n" );
                nextState = E_SM_ON_CONNECTION_OPENED;
                break;

            case E_ERROR: // e.g timeout
                nextState = E_SM_RESET;
                break;
            }
        }
        break;

    case E_SM_ON_CONNECTION_OPENED:
        {
            nextState = E_SM_BEGIN_OPEN_SOCKET;
        }
        break;

    case E_SM_BEGIN_OPEN_SOCKET: 
        {
            TransportManagerCommand(pSm->pTm, 
                CON_CMD_OPEN_SOCKET, 
            (void*)(pSm->pConnectParams));

            nextState = E_SM_OPEN_SOCKET_WAIT;
        }
        break;

    case E_SM_OPEN_SOCKET_WAIT:
        {
            // Poll the transport state until socket open
            cs = TransportManagerGetState(pSm->pTm, 
                QRY_CONNECTION_STATE,
                NULL);

            r = (CON_STATE_SOCKET_OPEN == cs) ? E_DONE_OK : E_RUNNING;
            if (CON_STATE_CONNECTION_OPEN_SOCKET_CLOSED == cs) r = E_ERROR;

            switch(r)
            {
            case E_RUNNING:
                // Do nothing; Wait while socket is being opened
                break; 

            case E_DONE_OK:
                DebugTrace( "Open socket SUCCESS\n" );
                nextState = E_SM_ON_SOCKET_OPENED;
                break;

            case E_ERROR: // e.g timeout
                nextState = E_SM_RESET;
                break;
            }
        }
        break;
        // Init anything (i.e the MQTT manager) that needs to happen on 
        // opening a connection
    case E_SM_ON_SOCKET_OPENED:
        {
            TcpSessionManagerOnBeginHelper(pSm);
            nextState = E_SM_CONNECTED;
        }
        break;

        // Remain in this state while connection is up and a   
        // transaction is in progress
    case E_SM_CONNECTED:
        {
            r = TcpSessionManagerOnRunHelper(pSm, tickIntervalMs);
            if (E_RUNNING == r) 
            {
                nextState = E_SM_CONNECTED;
            }
            else if (E_RUNNING_RECONNECT_NEEDED == r)
            {
                DebugTrace( "Reconnect needed\n" );
                nextState = E_SM_BEGIN_CLOSE_SOCKET; //disconnect to reconnect
                pSm->beginFlag = true; //reconnect
            }
            else
            {
                nextState = TcpSessionManagerOnDoneHelper(pSm);
                pSm->beginFlag = false; //we're done so don't begin again after disconnect
            }
        }
        break;

    case E_SM_BEGIN_CLOSE_SOCKET:
        {
            TransportManagerCommand(pSm->pTm, CON_CMD_CLOSE_SOCKET, NULL); 
            nextState = E_SM_CLOSE_SOCKET_WAIT;
        }
        break;


    // Poll the transport state until socket closed
    case E_SM_CLOSE_SOCKET_WAIT:
        {
            cs = TransportManagerGetState(pSm->pTm, QRY_CONNECTION_STATE, NULL);
            r = (CON_STATE_CONNECTION_OPEN_SOCKET_CLOSED == cs) ? E_DONE_OK : E_RUNNING;

            switch(r)
            {
            case E_RUNNING:
                nextState = E_SM_CLOSE_SOCKET_WAIT; 
                break; 

            case E_DONE_OK:
                DebugTrace( "Close socket SUCCESS\n" );
                nextState = E_SM_ON_SOCKET_CLOSED;
                break;

            case E_ERROR: // e.g. timeout
                nextState = E_SM_RESET;
                break;
            }
        }
        break;

        case E_SM_ON_SOCKET_CLOSED:
        {
            if (pSm->beginFlag == true)
            {
                //reopen the socket
                nextState = E_SM_BEGIN_OPEN_SOCKET;
            }
            else
            {
                nextState = E_SM_BEGIN_CLOSE_CONNECTION;
            }
        }
        break;

    case E_SM_BEGIN_CLOSE_CONNECTION:
        {
            TransportManagerCommand(pSm->pTm, CON_CMD_CLOSE_CONNECTION, NULL); 
            nextState = E_SM_CLOSE_CONNECTION_WAIT;
        }
        break;

        // Poll the transport state until disconnected
    case E_SM_CLOSE_CONNECTION_WAIT:
        {
            cs = TransportManagerGetState(pSm->pTm, QRY_CONNECTION_STATE, NULL);
            r = (CON_STATE_CONNECTION_CLOSED == cs) ? E_DONE_OK : E_RUNNING;

            switch(r)
            {
            case E_RUNNING:
                nextState = E_SM_CLOSE_CONNECTION_WAIT; 
                break; 

            case E_DONE_OK:
                DebugTrace( "Close connection SUCCESS\n" );
                nextState = E_SM_ON_CONNECTION_CLOSED;
                break;

            case E_ERROR: // e.g timeout
                nextState = E_SM_RESET;
                break;
            }
        }
        break;

        // Do any tidyup on close
    case E_SM_ON_CONNECTION_CLOSED:
        nextState = E_SM_RESET;
        break;

    default:
        ASSERT(0);
        break;
    }

    if (pSm->state != nextState)
    {
        pSm->state = nextState;
    }
}


/******************************************************************************
**    
**      @param[in, out] pSm The Session Manager object
**              @return true if a session is in progress
**  
**  @brief
**         Description: Is session in progress?
**
******************************************************************************/
bool TcpSessionManagerIsBusy(TCP_SESSION_MANAGER* pSm)
{
    ASSERT(NULL != pSm);

    return (E_SM_RESET == pSm->state) ? false : true;
}


/******************************************************************************
**    
**      @param[in, out] pSm The Session Manager object
**              @return None
**  
**  @brief
**         Helper to call the Begin of the manager object (on connection
**          or if another session is run)
******************************************************************************/
static void TcpSessionManagerOnBeginHelper(const TCP_SESSION_MANAGER* pSm)
{
    if (pSm->pManager != NULL)
    {
        if (pSm->pfnManagerBegin != NULL)
        {
            pSm->pfnManagerBegin((void*)pSm->pManager, pSm->pTm);
        }
    }
    else
    {
        ASSERT(0);
    }
}


/******************************************************************************
**    
**      @param[in, out] pSm The Session Manager object
**              @return The runner FSM state
**  
**  @brief
**         Helper to call the Run of the manager object 
******************************************************************************/
static E_FSM TcpSessionManagerOnRunHelper(const TCP_SESSION_MANAGER* pSm,
    uint32_t tickIntervalMs)
{
    E_FSM fsmReturn = E_ERROR;
    
    if (pSm->pManager != NULL)
    {
        if (pSm->pfnManagerRun != NULL)
        {
            fsmReturn = pSm->pfnManagerRun((void*)pSm->pManager, tickIntervalMs);
        }
        else
        {
            fsmReturn = E_ERROR;
        }
    }
    else
    {
        ASSERT(0);
    }

    return fsmReturn;
}


/******************************************************************************
**    
**      @param[in, out] pSm The Session Manager object
**              @return The next state - either disconnect or keep open
**  
**  @brief
**         Helper on completion of the Run FSM (ok or failed) 
**         Call an optional  user-given callback with a ref to the just completed 
**         manager. This gives a facility to check the manager's end state, and also 
**         to start another request
******************************************************************************/
static E_SM_STATE TcpSessionManagerOnDoneHelper(TCP_SESSION_MANAGER* pSm)
{    
    void*       pManagerForCallback = pSm->pManager;
    E_SM_STATE  nextState           = E_SM_BEGIN_CLOSE_SOCKET;
 
    // null manager in TSM, so it's not run again
    pSm->pManager = NULL;
   
    if (NULL != pSm->pfnOnComplete)
    {
        pSm->pfnOnComplete(( TCP_SESSION_MANAGER*)pSm, pManagerForCallback);
        pSm->pfnOnComplete = NULL;

        // Callback *might* have requested another run.
        // In this case the pManger would have been set again, hence keep connection open 
        if (NULL != pSm->pManager) 
        {
            nextState = E_SM_ON_SOCKET_OPENED;
        }
    }

    return nextState;
}


/**
 * @}
 */
