/******************************************************************************
*******************************************************************************
**
**         Filename: TransportManager.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
 
/**
 * @addtogroup TransportManager
 * @{
 */

/*!***************************************************************************
 * @file TransportManager.c
 *
 * @brief  This wraps the actual interface (e.g. a socket ot a modem) into a 
 *          platform independent form
 * @author Duncan Willis
 * @date 2015.03.18
 * @version 1.0
 *****************************************************************************/

 
/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "FsmTypes.h"
#include "CommonTypes.h"
#include "TransportManager.h"


/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/


/******************************************************************************
**    
**      @param[in, out] pT The TransportManager object
**      @param[in, out] pInterface The interface to use 
**      @param[in, out] pCtx The Object that owns the interface (eg winsock or modem) 
**              @return None
**  
**  @brief
**         Description: Init a transport Manager
**
******************************************************************************/
void TransportManagerInit(TRANSPORT_MANAGER* pT, const TRANSPORT_INTERFACE* pInterface, void* pCtx)
{
    int r = 0;
    void* pInitParams = NULL; // @note. For possible future use
    
    ASSERT(NULL != pT);
    ASSERT(NULL != pCtx);

    pT->pCtx = pCtx;
    pT->pInterface = pInterface;
    ASSERT(NULL != pInterface);

    // Call the driver's initialise function, if it exists 
    if (pT->pInterface->pfnInit)
    {
        r = pT->pInterface->pfnInit(pCtx, pInitParams);
    }
    (void)r;
}

/******************************************************************************
**    
**      @param[in, out] pT The TransportManager object
**           @param[in] cmd The time command to post
**           @param[in] cmdParams (optional) Paameters associated with command if any
**              @return Error code
**  
**  @brief
**         Description: Begin a command
**
******************************************************************************/
int TransportManagerCommand(TRANSPORT_MANAGER* pT, E_CONNECTION_COMMAND cmd, void* cmdParams)
{
    int r = -1;
    if (NULL != pT)
    {
        if (NULL != pT->pInterface->pfnCmd)
        {
            r = pT->pInterface->pfnCmd(pT->pCtx, cmd, cmdParams);    
        }
    }

    return r;
}

/******************************************************************************
**    
**      @param[in, out] pT The TransportManager object
**           @param[in] tickIntervalMs The time between ticks in ms
**              @return None
**  
**  @brief
**         Description: Tick-process the interface
**
******************************************************************************/
void TransportManagerTick(TRANSPORT_MANAGER* pT, uint32_t tickIntervalMs)
{
    if (NULL != pT)
    {
        if (NULL != pT->pInterface->pfnTick)
        {
           pT->pInterface->pfnTick(pT->pCtx, tickIntervalMs);    
        }
    }
}

/******************************************************************************
**    
**      @param[in, out] pT The TransportManager object
**           @param[in] query The parameter to query
**          @param[out] pQueryResult Optional query result if applicable (may be NULL)
**              @return E_CONNECTION_STATE enum
**  
**  @brief
**         Description: Issue a query e.g. Is (socket)Open? 
**
******************************************************************************/
E_CONNECTION_STATE TransportManagerGetState(const TRANSPORT_MANAGER* pT, 
    E_QUERY query, 
    void* pQueryResult)
{
    E_CONNECTION_STATE s = CON_STATE_UNINITIALIZED;
   
    if (NULL != pT)
    {
        if (NULL != pT->pInterface->pfnGetState)
        {
            s = pT->pInterface->pfnGetState(pT->pCtx, query, pQueryResult);
        }
    }
    return s;
}

/******************************************************************************
**    
**      @param[in, out] pT The TransportManager object
**          @param[out] buf The received data d
**           @param[in] bufferSize The size of the buffer
**              @return 0 if there's  no data, else will return the num of bytes 
**                      copied into the buffer buf (up to bufferSize).
**  
**  @brief
**         Description: Receive data - Not blocking
**
******************************************************************************/
int TransportManagerReceive(TRANSPORT_MANAGER* pT, unsigned char* buf, int bufferSize)
{ 
    int r = -1;

    if (NULL != pT)
    {
          if (NULL != pT->pCtx)
        {
            if (NULL != pT->pInterface->pfnReceive)
            {
                r = pT->pInterface->pfnReceive( pT->pCtx, buf, bufferSize);
            }
        }
    }

    return r;
} 

/******************************************************************************
**    
**      @param[in, out] pT The TransportManager object
**           @param[in] buf The data to send
**           @param[in] count The number of bytes to send
**              @return -1 if error, else num bytes sent.
**  
**  @brief
**         Description: Send data 
**
******************************************************************************/
int TransportManagerSend(TRANSPORT_MANAGER* pT, unsigned char* buf, int count)
{ 
    int r = -1;

    if (NULL != pT)
    {
        if (NULL != pT->pCtx)
        {
            if (NULL != pT->pInterface->pfnSend)
            {
                r = pT->pInterface->pfnSend(pT->pCtx, buf, count);     
            }
        }
    }

    return r;
} 

/******************************************************************************
**    
**      @param[in, out] pConnectionParams The object to init
**           @param[in] pHostName The server name or IP address - NOTE must 
**                                be statically allocated!
**           @param[in] port The port number (typ 1883)
**              @return None
**  
**  @brief
**         Description: Init the structure that gives the server address
**
******************************************************************************/
void TransportManagerInitConnectionParams(CONNECTION_PARAMS* pConnectionParams,
    const char* pHostName,
    uint16_t port)
{
    ASSERT(NULL != pConnectionParams);
    ASSERT(NULL != pHostName);

    pConnectionParams->host = pHostName;
    pConnectionParams->port = port;
    pConnectionParams->modemType = 0;
}



/******************************************************************************
**    
**      @param[in] pT The object to check
**              @return False if any internal pointers are null indicating uinitialised state
**  
**  @brief
**         Description: Helper to check for unitialised PT
**
******************************************************************************/
bool TransportManagerIsValid(const TRANSPORT_MANAGER* pT)
{
    if (NULL == pT)             return false;
    if (NULL == pT->pCtx)       return false;    
    if (NULL == pT->pInterface) return false;
    
    return true;
}


/**
 * @}
 */
