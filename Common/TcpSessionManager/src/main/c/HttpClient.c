/******************************************************************************
*******************************************************************************
**
**         Filename: HttpClient.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: Trivial HTTP Client 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup Http 
 * @{
 */


/****************************************************************************
 * @file HttpClient.c
 *
 * @brief  Handles the HTTP GET transaction
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/

 
/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <string.h>
#include <stdlib.h>
#include "CommonTypes.h"
#include "DebugTrace.h"
#include "TransportManager.h"
#include "HttpClient.h"



 // Suppress warnings pertaining to Keil only.
#ifndef _WIN32
#pragma push 
#pragma diag_suppress 513 // Keil args of function pointer do not match - cannot be assigned
#endif

/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/


static HTTP_ERROR HttpSendRequestHelper(HTTP_CLIENT* pClient);
static E_HR_STATE HttpClientPacketReadHeaderHelper(HTTP_CLIENT* pClient );
static void HttpClientBindToTransport(HTTP_CLIENT* pClient, TRANSPORT_MANAGER* pTm);
static void HttpClientCleanupHelper(HTTP_CLIENT* pClient);
static E_HR_STATE HttpClientSendContent (HTTP_CLIENT* pClient);
static void HttpSetError(HTTP_CLIENT* pClient, HTTP_ERROR httpError);
static void HttpDebugSessionResults (const HTTP_CLIENT_SESSION_RESULTS* pSr);
bool HttpClientReceiveHeaderHelper(HTTP_CLIENT* pClient, uint8_t b);
static E_HR_STATE HttpClientWaitForContent(HTTP_CLIENT* pClient);


/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/



/******************************************************************************
**    
**      @param[in, out] pClient The HTTP_MANGER object
**           @param[in] pInitParams The init data
**           @param[in] pSessionParams The request info struct to use
**           @param[in] pSessionResults The result struct to be populated at end.
**              @return None
**  
**  @brief
**         Description: Init an HTTP_CLIENT object
**
******************************************************************************/
DLL_EXPORT void
    HttpClientInit(HTTP_CLIENT* pClient, 
    const HTTP_CLIENT_INIT_PARAMS* pInitParams,
    const HTTP_CLIENT_SESSION_PARAMS* pSessionParams,
    HTTP_CLIENT_SESSION_RESULTS* pSessionResults)
{     
    pClient->errorState             = HTTP_ERROR_UNINITIALISED;
    pClient->state                  = E_HTTP_BEGIN;
    pClient->pTm                    = NULL;
   
    pClient->pSessionParams         = pSessionParams;
    pClient->pSessionResults        = pSessionResults;
    
    pClient->buffer                 = pInitParams->buffer;
    pClient->bufferSize             = pInitParams->bufferSize;

    pClient->OnReceivedContentByte  = pInitParams->OnReceivedContentByte;
    
    pClient->pCtx                   = pInitParams->pCtx;

    LineReaderInit(&pClient->lineReader, pClient->buffer, pClient->bufferSize);
}


/******************************************************************************
**    
**      @param[in, out] pSessionParams The HTTP_CLIENT_SESSION_RESULTS object
**              @return None
**  
**  @brief
**         Description: Init an HTTP_CLIENT_SESSION_PARAMS object to defaults
**
******************************************************************************/
DLL_EXPORT void 
    HttpClientInitSessionParams(HTTP_CLIENT_SESSION_PARAMS* pSessionParams)
{ 
    ASSERT(NULL != pSessionParams);
    memset(pSessionParams, 0, sizeof(HTTP_CLIENT_SESSION_PARAMS));

    pSessionParams->timeoutForWaitMs  = DEFAULT_HTTP_SESSION_TIMEOUT_MS;
}


/******************************************************************************
**    
**      @param[in, out] pSessionResults The HTTP_CLIENT_SESSION_RESULTS object
**              @return None
**  
**  @brief
**         Description: Init a HTTP_CLIENT_SESSION_RESULTS object
**
******************************************************************************/
DLL_EXPORT void    
    HttpClientInitSessionResults(HTTP_CLIENT_SESSION_RESULTS* pSessionResults)
{
    ASSERT(NULL != pSessionResults);   
    memset(pSessionResults, 0, sizeof(HTTP_CLIENT_SESSION_RESULTS));
    
    pSessionResults->hasBlankLine                       = false;
    pSessionResults->hasContentLength                   = false;
    pSessionResults->contentBytesReceived               = 0;     
    pSessionResults->hasReceivedHeader                  = false;
}


/******************************************************************************
**    
**      @param[in, out] pClient The HTTP Manager object
**              @return Get the session results
**  
**  @brief
**         Description: Get the session results
**
******************************************************************************/
DLL_EXPORT HTTP_CLIENT_SESSION_RESULTS*  
    HttpClientGetSessionResults(HTTP_CLIENT* pClient)
{
    return pClient->pSessionResults;
}


/******************************************************************************
**    
**      @param[in, out] pClient The HTTP Manager object
**      @param[in, out] pTm The Transport
**              @return None
**  
**  @brief
**         Description: Prepare the HTTP to do a GET (init state machine)
**
******************************************************************************/
DLL_EXPORT void    
    HttpClientBegin(struct HTTP_CLIENT* pClient, 
    TRANSPORT_MANAGER* pTm)
{
    ASSERT(NULL != pTm);

    HttpClientBindToTransport(pClient, pTm);

    pClient->timeElapsedMs                       = 0;
    pClient->state                               = E_HTTP_BEGIN; 
    pClient->errorState                          = HTTP_OK;
    pClient->bufferPos                           = 0;

    pClient->isConnectionStillAlive              = true; //if we spot a "Connection: close" header, this will change to false

    HttpClientInitSessionResults(pClient->pSessionResults);

    LineReaderInit(&pClient->lineReader, pClient->buffer, pClient->bufferSize);
}


/******************************************************************************
**    
**      @param[in, out] pClient The HTTP Manager object
**           @param[in] tickIntervalMs The time between ticks
**              @return E_ERROR = Finished - in error state, E_RUNNING = continue 
**                                calling E_DONE_OK Finished and OK
**  
**  @brief
**         Description: Run a HTTP Manager Session  
**
******************************************************************************/
DLL_EXPORT E_FSM 
    HttpClientRun (HTTP_CLIENT* pClient, uint32_t tickIntervalMs)
{    
    E_FSM r = E_RUNNING;
    E_HR_STATE nextState = E_HTTP_UNINITIALISED_ERROR;
    ASSERT(NULL != pClient);

    pClient->timeElapsedMs += tickIntervalMs;

    if (HTTP_OK != pClient->errorState)
    {
        return E_ERROR;  
    }

    switch (pClient->state)
    {
    case E_HTTP_BEGIN:  
        {
            if (HTTP_OK == HttpSendRequestHelper(pClient))
            {
                nextState = E_HTTP_WAIT_HEADER;
            }
            else
            {
                nextState = E_HTTP_SEND_FAIL;
            }
        }
        break;

    case E_HTTP_WAIT_HEADER:   
        {
            nextState = HttpClientPacketReadHeaderHelper(pClient);        
        }
        break;

    case E_HTTP_WAIT_CONTENT:
        {
            nextState = HttpClientWaitForContent(pClient);
        }
        break;

    case E_HTTP_SEND_CONTENT:
        {
            nextState = HttpClientSendContent(pClient);
            //nextState = E_HTTP_FINISH_OK;
        }
        break;

    case E_HTTP_FINISH_OK:
        {
            if (pClient->isConnectionStillAlive == false)
            {
                r = E_DONE_OK_RECONNECT_NEEDED;
                #ifdef VERBOSE_DEBUG
                DebugTrace("E_HTTP_FINISH; E_DONE_OK_RECONNECT_NEEDED\n");  
                #endif 
            }
            else
            {
                r = E_DONE_OK;
                #ifdef VERBOSE_DEBUG
                DebugTrace("E_HTTP_FINISH; E_DONE_OK\n");  
                #endif 
            }

            HttpClientCleanupHelper(pClient);
            nextState = E_HTTP_STOP;
        }
        break;

    case E_HTTP_SEND_FAIL:
        {
            HttpClientCleanupHelper(pClient);
            nextState = E_HTTP_STOP;
            r = E_ERROR;  
        }
        break;

    case E_HTTP_TIMED_OUT:
        {
            DebugTrace("E_HTTP_TIMEOUT - timeout waiting for data\n");      
            HttpSetError(pClient, HTTP_ERROR_TIMEOUT);
            HttpClientCleanupHelper(pClient);
            nextState = E_HTTP_STOP;
            r = E_ERROR;   
        }
        break;

    case E_HTTP_STOP: // FSM has finished to completion, should not be ticked again
        {
            ASSERT(0);
        }
        break;

    case E_HTTP_UNINITIALISED_ERROR:
    default:
        {
            ASSERT(0);
        }
        break;
    } // switch state

    if (nextState != pClient->state)
    {
        pClient->state = nextState;
    }

    return r;
}


/******************************************************************************
**    
**      @param[in, out] pClient The HTTP Manager object
**              @return None
**  
**  @brief
**         Description: Helper called at end (ok or otherwise) of session
**
******************************************************************************/
static void  HttpClientCleanupHelper (HTTP_CLIENT* pClient)
{ 
    HTTP_CLIENT_SESSION_RESULTS* pSr = pClient->pSessionResults;
    ASSERT(NULL != pClient);
    HttpClientBindToTransport(pClient, NULL);

    pClient->pSessionResults->sessionExecutionTimeMs = pClient->timeElapsedMs;       

    // Check contentLength supplied is consistent with range eg. 0-1 is 2 bytes...
    if (pSr->hasRange)
    {
        if ((1 + pSr->rangeLastByte - pSr->rangeFirstByte) != pSr->contentLength)
        {   
            HttpSetError(pClient, HTTP_ERROR_INCONSISTENT_RANGE);
            DebugTrace("\nHttpClient ERROR Inconsistent range rx'd\n"); 
        }

        if ((pSr->rangeFirstByte != pClient->pSessionParams->firstRangeByte) ||
            (pSr->rangeLastByte  != pClient->pSessionParams->lastRangeByte))
        {            
            DebugTrace("\nHttpClient ERROR mismatched range rx'd\n"); 
            HttpSetError(pClient, HTTP_ERROR_MISMATCHED_RANGE);
        }
    }

    if (HTTP_OK == pClient->errorState)
    {
        HttpDebugSessionResults(pClient->pSessionResults);
    }
    else
    {
        DebugTrace("HTTP session ERROR=%d\n", pClient->errorState);      
    }
}


/******************************************************************************
**    
**      @param[in, out] pClient The HTTP Manager object
**              @return None
**  
**  @brief
**         Description: Helper called when content is OK, sends the content 
**            byte by byte to the callback (flash prog)
**
******************************************************************************/
static E_HR_STATE  HttpClientSendContent (HTTP_CLIENT* pClient)
{
    uint32_t    i;
    uint8_t     contentByte;
    uint32_t    contentBytesInBuffer; 
    uint8_t     numByteRx;

    HTTP_CLIENT_SESSION_RESULTS* pSr = pClient->pSessionResults;
    ASSERT(NULL != pClient);

    // We already counted the header, now add in the content (which is ok)
    pSr->receivedBytes += pSr->contentLength;

    // Check. 
    contentBytesInBuffer = (uint32_t)TransportManagerCommand(pClient->pTm, CON_CMD_GET_RX_COUNT, NULL);
    ASSERT(contentBytesInBuffer == pSr->contentLength);
    if (contentBytesInBuffer != pSr->contentLength)
    {
        DebugTrace ("### bytes in buffer: %u \n", contentBytesInBuffer);
        return E_HTTP_WAIT_CONTENT;
    }

    #ifdef VERBOSE_DEBUG
    DebugTrace("Sent to Flash: contentLength = %u bytes\n", pSr->contentLength);
    #endif 
    
    // Send the complete contents to the user if callback registered (but must flush FIFO regardless)
    for (i = 0; i < pSr->contentLength; i++)
    {
        numByteRx = TransportManagerReceive(pClient->pTm, &contentByte, 1);
        ASSERT(1 == numByteRx); 
        
        if (NULL != pClient->OnReceivedContentByte)
        {
            pClient->OnReceivedContentByte((struct HTTP_CLIENT* )pClient, contentByte);
        }
    }
    
    return E_HTTP_FINISH_OK;
}


/******************************************************************************
**    
**      @param[in, out] pClient The HTTP Manager object
**      @param[in, out] pTm The Transport Manager object
**              @return None
**  
**  @brief
**         Description: Helper to bind HTTP Manager to an established connection
**
******************************************************************************/
static void HttpClientBindToTransport(HTTP_CLIENT* pClient, TRANSPORT_MANAGER* pTm)
{
    ASSERT(NULL != pClient);

    pClient->pTm = pTm;
}


/******************************************************************************
**    
**      @param[in, out] pClient The HTTP Manager object
**              @return Error code
**  
**  @brief
**         Description: Helper to http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
** Partial Get : http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.35
******************************************************************************/
static HTTP_ERROR HttpSendRequestHelper(HTTP_CLIENT* pClient)
{
    HTTP_ERROR eCode = HTTP_OK;
    int32_t bytesSent = 0;
    int32_t bytesToSend = 0;

    ASSERT(NULL != pClient);

    if (HTTP_OK != pClient->errorState)
    {
        return pClient->errorState;
    }
    
    eCode = HttpMakeGetRequestHelper(pClient->buffer,
        pClient->bufferSize,
        pClient->pSessionParams->fileName,
        pClient->pSessionParams->hostName,
        pClient->pSessionParams->firstRangeByte,
        pClient->pSessionParams->lastRangeByte);
    
    if (HTTP_OK != eCode)
    {        
        HttpSetError(pClient, eCode);
    }
    else
    {
        bytesToSend = strlen((const char*)pClient->buffer);

        bytesSent =  TransportManagerSend(pClient->pTm, pClient->buffer, bytesToSend);

        if (bytesSent > 0)
        {
            pClient->pSessionResults->sentBytes += bytesSent;

            // Fuller debug info on simu hardware
            #ifdef BOARD_PCA10028 
             DebugTrace("\nHTTP REQ of \"%s\" Range %u-%u (%d bytes sent with %u bytes payload)\n", 
                 pClient->pSessionParams->fileName,             
                 pClient->pSessionParams->firstRangeByte,
                 pClient->pSessionParams->lastRangeByte,
                 bytesSent,
                 bytesToSend);    
            #else
             DebugTrace("\nHTTP REQ Range %u-%u\n", 
                            pClient->pSessionParams->firstRangeByte,
                            pClient->pSessionParams->lastRangeByte);
            #endif
            
            #ifdef VERBOSE_DEBUG // print the actual GET text, optionally.
            DebugTrace((const char*)pClient->buffer);
            DebugTrace("\r\n");
            #endif
        }
        else
        {
           // DebugTrace("\r\nERROR : SENT REQ FAILED in buffer write, %u sent from %u bytes payload\n", bytesSent, bytesToSend);
            HttpSetError(pClient, HTTP_ERROR_SOCKET_SEND);
        }
    }
    
    return pClient->errorState;
}


/******************************************************************************
**    
**      @param[in, out] pClient The HTTP Manager object
**           @param[in] httpError The Error Code to set
**              @return None
**  
**  @brief
**         Description: Helper to set error code
**
******************************************************************************/
static void HttpSetError(HTTP_CLIENT* pClient, HTTP_ERROR httpError)
{
    ASSERT(NULL != pClient);

    if (HTTP_OK == pClient->errorState)
    {
        DebugTrace("HTTP ERROR CODE SET = %d\n", httpError);
        pClient->errorState = httpError;
    }
}


/******************************************************************************
**    
**      @param[in, out] pClient The HTTP Manager object
**              @return The next state
**  
**  @brief
**         Description: Helper to read header from the data stream
**         Continue reading bytes while available (do not block)
**         Continue until req is complete, or timeout 
******************************************************************************/
static E_HR_STATE HttpClientPacketReadHeaderHelper(HTTP_CLIENT* pClient)
{
    E_HR_STATE httpState = E_HTTP_READ_BYTES;
    int32_t numRx = 0;
    uint8_t b;
    
    HTTP_CLIENT_SESSION_RESULTS* pSr = pClient->pSessionResults;
    ASSERT(NULL != pClient);

    do
    {
        // Initially, attempt to read one byte to get the http header
        // Once header is received, then do not consume content, but wait for it to build 
        // up in rx buffer. Once fully there, we can go and prog into flash etc.
        // Otherwise, we continue waiting, subject to the timeout.
   
        // Read a header byte
        numRx = TransportManagerReceive(pClient->pTm, &b, 1);
  
        if (numRx == 1)
        {
            pSr->hasReceivedHeader = HttpClientReceiveHeaderHelper(pClient, b);

            if (pSr->hasReceivedHeader)
            {
                httpState = E_HTTP_WAIT_CONTENT;
            }
        }
        else if (0 == numRx)
        {
            if (pClient->timeElapsedMs >= pClient->pSessionParams->timeoutForWaitMs)
            {
                DebugTrace("\nHttpClient TIMEOUT in %ums\n", pClient->timeElapsedMs);
                httpState = E_HTTP_TIMED_OUT;
            }
            else
            {
                // Continue waiting for more header
                httpState = E_HTTP_WAIT_HEADER;
            }
        }
        else // -ve return, socket was closed
        {
            httpState = E_HTTP_TIMED_OUT;
        }
    }
    while (E_HTTP_READ_BYTES == httpState);

    return httpState;
}


/******************************************************************************
**    
**      @param[in, out] pClient The HTTP Manager object
**      @param[in] b A byte to process from the RESPONSE
**              @return True if the Header is completely received and ok
**  
**  @brief
**         Description: Parse stream, get header info. Once done, read payload.
**
******************************************************************************/
bool HttpClientReceiveHeaderHelper(HTTP_CLIENT* pClient, uint8_t b)
{
    const uint8_t* pLine;

    HTTP_CLIENT_SESSION_RESULTS* pSr = pClient->pSessionResults;
    HTTP_LINE_READER* pLr = ( HTTP_LINE_READER*)(&pClient->lineReader);

    pSr->receivedBytes++;

    pLine = LineReaderReadByte(pLr, b);

    if (NULL != pLine)
    {
        #ifdef VERBOSE_DEBUG
        DebugTrace("RX: %s", pLine);
        #endif

        LineReaderParseNumeric(pLine, HTTP_1_1, &(pSr->responseCode));

        if (LineReaderParseContentLength(pLine, &(pSr->contentLength)))
        {               
            pSr->hasContentLength = true;
        }
        // Content-range (if used)
        else if (LineReaderParseContentRange(pLine,
            HTTP_CONTENT_RANGE_STR,
            &(pSr->rangeFirstByte),
            &(pSr->rangeLastByte),
            &(pSr->rangeTotalContentLength)))
        {                
            pSr->hasRange = true;
        }
        else if (LineReaderParseConnectionClose(pLine)) //server has chosen to close connection after this request
        {
            pClient->isConnectionStillAlive = false;
        }
        // Have everything we need from header? A blank line immediately precedes the content
        else if( LineReaderIsBlankLine(pLine))
        {
            pSr->hasBlankLine = true;

            if (pSr->hasContentLength)
            {
                // Header now complete.
                pSr->hasReceivedHeader = true; 
            }
        } 
    }

    return pSr->hasReceivedHeader;
}


/******************************************************************************
**    
**      @param[in, out] pClient The HTTP Manager object 
**              @return The next state - still waiting / ok / timeout
**  
**  @brief Called from FSM. Waits for content to accumulate in FIFO. If it arrives
**   then we can send it, otherwise, we timeout.
******************************************************************************/
static E_HR_STATE HttpClientWaitForContent(HTTP_CLIENT* pClient)
{    
    uint32_t    i;
    uint8_t     flushByte;
    E_HR_STATE  httpState = E_HTTP_WAIT_CONTENT;
    HTTP_CLIENT_SESSION_RESULTS* pSr = pClient->pSessionResults;
    
    // Set this to simulate a missing byte
    bool testDropAByte = false;

    // get number of bytes waiting in FIFO
    pSr->contentBytesReceived = TransportManagerCommand(pClient->pTm, CON_CMD_GET_RX_COUNT, NULL);


    // Test feature. Optionally drop a byte once there are some to drop.
    if (testDropAByte)
    {
        if ( pSr->contentBytesReceived > 1)
        {
             pSr->contentBytesReceived -= 1;
        }
    }

    // All there?
    if (pSr->contentBytesReceived >= pSr->contentLength)
    {
        httpState = E_HTTP_SEND_CONTENT;
    }

    if (pClient->timeElapsedMs >= pClient->pSessionParams->timeoutForWaitMs)
    {
        DebugTrace("\nHttpClient TIMEOUT waiting for content in %ums, Expected=%u, Got=%u\n", 
            pClient->timeElapsedMs,
            pSr->contentLength,
            pSr->contentBytesReceived);

        // Flush FIFO of failed content
        for (i = 0; i < pSr->contentBytesReceived; i++)
        {
            (void)TransportManagerReceive(pClient->pTm, &flushByte, 1);   
        }

        httpState = E_HTTP_TIMED_OUT;
    }

    return httpState;
}


/******************************************************************************
**    
**      @param[in] pClient The HTTP Manager object
**              @return None
**  
**  @brief
**         Description: Debug print results of HTTP GET
**
******************************************************************************/
static void  HttpDebugSessionResults (const HTTP_CLIENT_SESSION_RESULTS* pSr)
{    
    const char* sSuccess = "GET ERROR";
    ASSERT(NULL != pSr); 

        if ((HTTP_RESPONSE_OK              == pSr->responseCode) || 
            (HTTP_RESPONSE_PARTIAL_CONTENT == pSr->responseCode))
        {
            sSuccess= "(GET SUCCESS)";
        }
        
        sSuccess = sSuccess; // fix compiler warning
        
        #ifdef VERBOSE_DEBUG
        DebugTrace("\nHTTP Request results:\n");
        DebugTrace("Response Code    = %u %s\n", pSr->responseCode, sSuccess);

        
        DebugTrace("Content Length   = %u\n", pSr->contentLength);
        DebugTrace("Content Range    = %u, %u, Total = %u\n", pSr->rangeFirstByte, pSr->rangeLastByte, pSr->rangeTotalContentLength);
        DebugTrace("Bytes         TX = %u  RX = %u\n", pSr->sentBytes, pSr->receivedBytes); 
        DebugTrace("Status:     Time = %ums\n\n", pSr->sessionExecutionTimeMs);     
        #endif
}

#ifndef _WIN32
#pragma pop
#endif

/**
* @}
*/
