/******************************************************************************
*******************************************************************************
**
**         Filename: HttpClient.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup HttpUtilities
 * @{
 */


/*!***************************************************************************
 * @file HttpUtilities.c
 *
 * @brief  HTTP related definitions and helpers
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/

 
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "CommonTypes.h"
#include "HttpUtilities.h"



/******************************************************************************
**    
**      @param[in, out] pM The HTTP_LINE_READER object
**      @param[in] buffer The buffer to use
**      @param[in] bufferSize The size of buffer in bytes
**              @return None
**  
**  @brief
**         Description: 
******************************************************************************/
void LineReaderInit(HTTP_LINE_READER* pLr, uint8_t* buffer, uint32_t bufferSize)
{
    pLr->bufferSize     = bufferSize;
    pLr->pLineBuffer    = buffer;
    pLr->pos            = 0;
}


/******************************************************************************
**    
**      @param[in] pM The HTTP_LINE_READER object
**      @param[in] b The byte to read
**              @return The line if found, a null term is added, or null if no line yet
**  
**  @brief
**         Description: Read a line that is CRLF terminated
**http://www.w3.org/Protocols/rfc2616/rfc2616-sec2.html#sec2.1
******************************************************************************/
const uint8_t* LineReaderReadByte(HTTP_LINE_READER* pLr, uint8_t b)
{
    uint8_t* pReturn = NULL;
    
    ASSERT(pLr != NULL);
    ASSERT(pLr->pLineBuffer != NULL);
    
    // Write byte into line, allow for later addition of \0 at end
    if (pLr->pos < pLr->bufferSize - 1)
    {
        pLr->pLineBuffer[pLr->pos] = b;
        pLr->pos++;
    }
    else
    {
        pLr->pos = 0;
    }

    ASSERT(pLr->pos <  pLr->bufferSize);

    if ('\n' == b)
    {
        if (pLr->pos >= 2)
        {
            if (pLr->pLineBuffer[pLr->pos - 2] == '\r')
            {
                pLr->pLineBuffer[pLr->pos - 0] = '\0';
                pReturn = &(pLr->pLineBuffer[0]);
                pLr->pos = 0;
            }
        }
    }

    return pReturn;
}


/******************************************************************************
**    
**      @param[in, out] pM The HTTP_LINE_READER object
**      @param[in] buffer The buffer to use
**      @param[in] bufferSize The size of buffer in bytes
**              @return true if line is empty (just CRLF)
**  
**  @brief
**         Description: 
******************************************************************************/
bool LineReaderIsBlankLine(const uint8_t* pLine)
{    
    ASSERT(NULL != pLine);

    return (    (pLine[0] == '\r')
            &&  (pLine[1] == '\n')
            &&  (pLine[2] == '\0'));
}


/******************************************************************************
**     
**      @param[in] pLine The line to scan
**      @param[in] pToken The tag to look for (must include all chars (except whitespace) before number
**      @param[out] pNumber The value (can be NULL)
**              @return pNumberPosition if the token was found, pointer to the token (and if supplied, the 
**                      pNumber was filled). Null if not found
**  
**  @brief
**         Description: 
******************************************************************************/
const char* LineReaderParseNumeric(const uint8_t* pLine, const char* pToken, uint32_t* pNumber)
{
    uint32_t n = 0;
    const char* p; 
    const char* pNumberPosition = NULL; 

    ASSERT(NULL != pLine);
     
    p = strstr((const char*)pLine, pToken);

    if (p != NULL)
    {
        pNumberPosition = &(p[strlen(pToken)]);

        n = (uint32_t)atol(pNumberPosition);
        
        if (NULL != pNumber)
        {
            *pNumber = n;
        }
    }

    return pNumberPosition;
}


/******************************************************************************
**     
**      @param[in] pLine The line to scan
**      @param[in] pToken The tag to look for (must include all chars (except whitespace) before number
**      @param[out] pNumber The value (can be NULL)
**              @return pNumberPosition if the token was found, pointer to the token (and if supplied, the 
**                      pNumber was filled). Null if not found
**  
**  @brief
**         Description: 
******************************************************************************/
const char* LineReaderParseString(const uint8_t* pLine, const char* pToken)
{
    const char* p; 
    const char* pStrPosition = NULL; 

    ASSERT(NULL != pLine);
     
    p = strstr((const char*)pLine, pToken);

    if (p != NULL)
    {
        pStrPosition = &(p[strlen(pToken)]);

        // Strip leading whitespace from result
        while (' ' == *pStrPosition)
        {
            pStrPosition++;
        }

        // Got to end
        if ('\0' == *pStrPosition)
        {
            pStrPosition = NULL;
        }
    }

    return pStrPosition;
}


/******************************************************************************
**     
**      @param[in] pLine The line to scan
**      @param[out] pContentLength The value (can be NULL)
**              @return true if the content length was found (and if supplied, the 
**                      pContentLength was filled)
**  
**  @brief
**         Description: 
******************************************************************************/
bool LineReaderParseContentLength(const uint8_t* pLine, uint32_t* pContentLength)
{
    return (NULL != LineReaderParseNumeric(pLine, HTTP_CONTENT_LENGTH_STR, pContentLength));
}


/******************************************************************************
**     
**      @param[in] pLine The line to scan
**      @param[out] pContentLength The value (can be NULL)
**              @return true if the content length was found (and if supplied, the 
**                      pContentLength was filled)
**  
**  @brief
**         Description: 
******************************************************************************/
bool LineReaderParseContentRange(const uint8_t* pLine,
      const char* pToken,
      uint32_t* pFirstByte,
      uint32_t* pLastyte,
      uint32_t* pTotalContentLength)
{
    bool isFound = false;
    const char* p1;
    const char* pHyphen;
    const char* pSlash;

    p1 = LineReaderParseNumeric(pLine, pToken, pFirstByte);

    if (NULL != p1)
    {
        pHyphen = strchr(p1, '-');
        pSlash  = strchr(p1, '/');
         
        if (NULL != pHyphen)
        {
            pHyphen++;
            pSlash++;
            *pLastyte = (uint32_t)atol(pHyphen);

            // Optional output (and this field will not be present in a GET)
            if (NULL != pTotalContentLength)
            {
                if(NULL != pSlash)
                {
                    *pTotalContentLength    = (uint32_t)atol(pSlash);
                }
                else
                {
                    *pTotalContentLength    = 0;
                }
            }

             isFound = true;
        }
    }

    return  isFound;
}


/******************************************************************************
**     
**      @param[in] pLine The line to scan
**              @return true if the line says Connection: close
**  
**  @brief
**         Description: Returns true if this line indicates the connection
**                      will be closed after this request.
**                      See http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.10
******************************************************************************/
bool LineReaderParseConnectionClose(const uint8_t* pLine)
{
    const char* const pHeaderLine = (const char*) pLine;
    bool isFound = false;
    const char* p;

    p = strstr(pHeaderLine, HTTP_CONNECTION_STR); //"Connection:"
    
    if (p != NULL)
    {
        p = strstr(pHeaderLine, HTTP_CLOSE_STR); //"close"
        if (p != NULL)
        {
            isFound = true;
        }
    }
    return  isFound;
}


/******************************************************************************
**    
**      @param[in, out] pClient The HTTP Manager object
**           @param[out] buffer The output string
**           @param[in] bufferSize The size of the output buffer
**           @param[in] url The text string URL
**           @param[in] hostName The text string hostName
**           @param[in] firstRangeByte Index first byte of partial GET resource (0 is first)
**           @param[in] lastRangeByte Index last byte of partial GET resource (N-1 is last)
**              @return HTTP error code for bad URL
**  
**  @brief
**         Description: Helper to http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
** Partial Get : http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.35
** @note Set firstRangeByte and lastRangeByte to zero to get entire resource
**
******************************************************************************/
HTTP_ERROR HttpMakeGetRequestHelper(uint8_t* buffer, uint32_t bufferSize,
    const char* url,
    const char* hostName, 
    uint32_t firstRangeByte, 
    uint32_t lastRangeByte )
{
    int32_t urlLength = 0;
    char  sTemp[50];
    bool requestRange;
    
    ASSERT(NULL != buffer);
    ASSERT(NULL != url);
    ASSERT(NULL != hostName);

    urlLength = strlen(url);

    if (urlLength > HTTP_MAX_URL_LEN)
    { 
        return HTTP_ERROR_BAD_URL;
    } 
        
    // Request a range from the resource?
    // Both zero - request entire resource. Otherwise, get only the requested bytes
    // Also, last must not be less than first
    requestRange = (0 != firstRangeByte) || (0 != lastRangeByte);
    if (firstRangeByte > lastRangeByte)
    {
        requestRange = false;
    }

    sprintf((char*)buffer, "GET %s %s\r\nHost: %s\r\n", url, HTTP_1_1, hostName);

    if (requestRange)
    {
        sprintf(sTemp,  "%s%u-%u\r\n", 
            HTTP_PARTIAL_RANGE_STR,
            firstRangeByte,
            lastRangeByte);

        strncat((char*)buffer, sTemp, bufferSize);
    } 

    // Not needed strncat((char*)buffer, "Connection: Keep-Alive\r\n", bufferSize);   
    strncat((char*)buffer, "Content-Length: 0\r\n", bufferSize); // Might help in persisting the TCP conenction on Tomcat,  although no evidence to support.
    // Not needed - for reference : strncat((char*)buffer, "Cache-Control: max-age=0\r\n", bufferSize);     
    strncat((char*)buffer, HTTP_BLANK_LINE, bufferSize);

    return HTTP_OK;
}


/******************************************************************************
**    
**           @param[out] buffer The output string
**           @param[in] bufferSize The size of the output buffer
**           @param[in] url The text string URL
**           @param[in] hostName The text string hostName
**           @param[in] firstRangeByte Index first byte of partial GET resource (0 is first)
**           @param[in] lastRangeByte Index last byte of partial GET resource (N-1 is last)
**              @return Number of bytes to send, 0 if error.
**  
**  @brief 
**         Make the header for a get response
** Partial Get : http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.35
** @note Set firstRangeByte and lastRangeByte to zero to get entire resource
**
******************************************************************************/
uint32_t HttpMakeGetResponseHelper(uint8_t* buffer, uint32_t bufferSize,   
    uint32_t responseCode,
    uint32_t contentLength,
    uint32_t firstRangeByte, 
    uint32_t lastRangeByte, 
    uint32_t totalDataSize)
{
    uint32_t headerLength = 0;
    char  sTemp[50];
    bool partialRequest;
    const char *strResponse = "";
    ASSERT(NULL != buffer);
        
    // Request a range from the resource?
    // Both zero - request entire resource. Otherwise, get only the requested bytes
    // Also, last must not be less than first
    partialRequest = (0 != firstRangeByte) || (0 != lastRangeByte);
    if (firstRangeByte > lastRangeByte)
    {
        partialRequest = false;
    }

    if      (HTTP_RESPONSE_PARTIAL_CONTENT  == responseCode)    strResponse = HTTP_PARTIAL_CONTENT_STR;
    else if (HTTP_RESPONSE_OK               == responseCode)    strResponse = "OK";
    else                                                        strResponse = "";

    sprintf((char*)buffer, "%s %u %s\r\n", HTTP_1_1, responseCode, strResponse);

    if (partialRequest)
    {
        sprintf(sTemp,  "%s %u-%u/%u\r\n", 
            HTTP_CONTENT_RANGE_STR,
            firstRangeByte,
            lastRangeByte,
            totalDataSize);

        strncat((char*)buffer, sTemp, bufferSize);
    } 

    sprintf((char*)sTemp, "%s %u\r\n", HTTP_CONTENT_LENGTH_STR, contentLength);
    strncat((char*)buffer, sTemp, bufferSize - strlen((const char*)buffer) -2 -1);     
    strcat((char*)buffer, HTTP_BLANK_LINE);

    // If clipped to the buffer size - will be an error if so.
    headerLength = strlen((const char*)buffer);
    return (headerLength >= (bufferSize - 1)) ? 0 : headerLength;
}

/**
* @}
*/
