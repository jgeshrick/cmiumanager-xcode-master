/******************************************************************************
*******************************************************************************
**
**         Filename: HttpServer.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup HttpServer
 * @{
 */


/*!***************************************************************************
 * @file HttpServer.h
 *
 * @brief Trivial HTTP Server
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/


#ifndef HTTP_SERVER_H_
#define HTTP_SERVER_H_


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include "FsmTypes.h"
#include "RingBuffer.h"
#include "HttpUtilities.h"


/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/


// Max size for   temp buffer - largest header +  data block
// Warning - on stack - but should be PC/iOS side only
#define HTTP_SERVER_HEADER_BUFFER_SIZE      (HTTP_MAX_URL_LEN + 256)
#define HTTP_SERVER_CONTENT_BUFFER_SIZE     1024



/*===========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                        */
/*===========================================================================*/


typedef struct DATA_PROVIDER
{
    FILE*       pFile;
    uint32_t    sizeBytes;
} DATA_PROVIDER;




/**
** @brief The object that runs the HTTP protocol (Wrapper around Paho)
**/
typedef struct HTTP_SERVER
{
    RingBufferT*                                pRbRx;
    RingBufferT*                                pRbTx;
    DATA_PROVIDER*                              pDataProvider;
    HTTP_LINE_READER                            lineReader;

    // working buffer (allocated externally and supplied at init time)
    uint8_t                                     lineBuffer[ HTTP_MAX_LINE_LEN ];

    HTTP_ERROR                                  errorState;
    
    char                                        basePath[HTTP_MAX_URL_LEN + 1];     // root of the HTTP requests
    char                                        resourceStr[HTTP_MAX_URL_LEN + 1];
    bool                                        hasGetField;
    bool                                        hasRange;
    bool                                        hasBlankLine;
    uint32_t                                    rangeFirstByte;             // The first byte if partial GET
    uint32_t                                    rangeLastByte;              // The last byte if partial GET
    uint32_t                                    percentServed;              // Percentage of total file that's been served
}
HTTP_SERVER;


/**
** @brief Init params for the HTTP Server
**/
typedef struct HTTP_SERVER_INIT_PARAMS 
{
    RingBufferT*                                pRbRx;
    RingBufferT*                                pRbTx;
    DATA_PROVIDER*                             dataProvider;
}
HTTP_SERVER_INIT_PARAMS;




/**
** @brief   
**/
#if 0 //not used - basis of possible enhancement
typedef struct HTTP_SERVER_GET 
{
    uint32_t                    responseCode;
    uint32_t                    contentLength;              // Of this particular GET (May be partial GET)
    uint32_t                    rangeFirstByte;             // The first byte if partial GET
    uint32_t                    rangeLastByte;              // The last byte if partial GET
    uint32_t                    rangeTotalContentLength;    // The entire resource length byte if partial GET
    uint32_t                    sentBytes;
    uint32_t                    receivedBytes;
    uint32_t                    sessionExecutionTimeMs;
}
HTTP_SERVER_GET;
#endif


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/


DLL_EXPORT void
            HttpServerInit(HTTP_SERVER* pServer, const HTTP_SERVER_INIT_PARAMS* pInitParams);


void        HttpServerProcess(HTTP_SERVER* pServer, uint32_t tickIntervalMs);
void        HttpServerSendResponse(HTTP_SERVER* pServer);
uint32_t    HttpServerGetPercentServed(const HTTP_SERVER* pServer);
bool        HttpServerReceiveByteHelper(HTTP_SERVER* pServer, uint8_t b);
bool        HttpServerSetBasePath(HTTP_SERVER* pServer, const char* basePath);



uint32_t    HttpServerGetDataSize(const DATA_PROVIDER* pCtx);

HTTP_ERROR  HttpServeReadDataFromFile(uint8_t buffer[],
    uint32_t bufferSize,
    const DATA_PROVIDER* pCtx,
    const char* fileName,
    uint32_t firstRangeByte,
    uint32_t lastRangeByte,
    uint32_t* pNumBytesRead);




#endif // HTTP_SERVER_H_

/**
 * @}
 */
