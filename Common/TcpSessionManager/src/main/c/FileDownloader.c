/******************************************************************************
*******************************************************************************
**
**         Filename: FileDownloader.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: File Downloader using HTTP Client 
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup FileDownloader 
 * @{
 */


/*!***************************************************************************
 * @file FileDownloader.c
 *
 * @brief  Handles file get via the HTTP GET transaction types
 * @author Duncan Willis
 * @date 2015.06.17
 * @version 1.0
 *****************************************************************************/


#include <string.h>
#include <stdint.h>
#include "CommonTypes.h"
#include "DebugTrace.h"
#include "TransportManager.h"
#include "FileDownloader.h" 


 // Suppress warnings pertaining to Keil only.
#ifndef _WIN32
#pragma push
#pragma diag_suppress 144 // Keil args of function pointer do not match
#pragma diag_suppress 513 // Keil args of function pointer do not match - cannot be assigned
#endif
 
 
/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/
STATIC void         HandleReceivedContentByte(struct HTTP_CLIENT* pClient, uint8_t b);
STATIC E_FD_STATE   OnRequestCompleted(FILE_DOWNLOADER* pDownloader);
STATIC E_FD_STATE   OnRequestFailed(FILE_DOWNLOADER* pDownloader);
STATIC void         FileDownloaderCalculateNextBlock(FILE_DOWNLOADER* pDownloader);
STATIC void         FileDownloaderSetError(FILE_DOWNLOADER* pDownloader, 
    E_FILE_DOWNLOADER_ERROR errorCode);


/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/


/******************************************************************************
**    
**      @param[in, out] pDownloader The Downloader object
**           @param[in] pInitParams The init data 
**              @return None
**  
**  @brief
**         Description: Init a FILE_DOWNLOADER object
**
******************************************************************************/
void FileDownloaderInit(FILE_DOWNLOADER* pDownloader, 
    const FILE_DOWNLOADER_INIT_PARAMS* pInitParams)
{
    memset(pDownloader, 0, sizeof(FILE_DOWNLOADER));

    pDownloader->buffer                         = pInitParams->buffer;
    pDownloader->bufferSize                     = pInitParams->bufferSize;
    ASSERT(NULL != pDownloader->buffer);
    ASSERT(pDownloader->bufferSize > 0);

    pDownloader->fileName                       = pInitParams->fileName;
    pDownloader->requestTimeoutMs               = pInitParams->requestTimeoutMs;
    pDownloader->requestSize                    = pInitParams->blockSize;
    pDownloader->maxRequestRetries              = pInitParams->maxRequestRetries;
    pDownloader->OnReceivedByte                 = pInitParams->OnReceivedByte;

    pDownloader->errorState                     = FILE_DOWNLOADER_OK;
    pDownloader->state                          = E_FD_INIT;
}


/******************************************************************************
**    
**      @param[in, out] FileDownloader The HTTP Manager object
**      @param[in, out] pTm The Transport
**           @param[in] pSessionParams The Session Params to use
**          @param[out] pSessionResults The Session Results to populate when done
**              @return None
**  
**  @brief
**         Description: Prepare the session state machine (begin session)
**
******************************************************************************/
void FileDownloaderBegin(FILE_DOWNLOADER* pDownloader, TRANSPORT_MANAGER* pTm)
{
    HTTP_CLIENT_INIT_PARAMS httpClientInitParams = 
    {
        pDownloader->buffer, 
        pDownloader->bufferSize, 
        pDownloader,
        &HandleReceivedContentByte
    };

    if (pDownloader->state == E_FD_INIT) //this might be being called on a reconnect
    {

        ASSERT(NULL != pTm);
        ASSERT(NULL != pDownloader);  
        ASSERT(NULL != pDownloader->buffer);
        ASSERT(pDownloader->bufferSize > 0);

        pDownloader->pTm                                = pTm;
        pDownloader->totalContentLength                 = 0;  
        pDownloader->byteCount                          = 0;
        pDownloader->timeElapsedMs                      = 0;
        pDownloader->errorState                         = FILE_DOWNLOADER_OK;
        pDownloader->requestRetryCount                  = 0;
        pDownloader->lastServerResponseCode             = 0;
        pDownloader->numRequests                        = 0;
        pDownloader->numFailedRequests                  = 0;

        // Set up session args for first (and subsequent) http gets 
        HttpClientInitSessionParams(&(pDownloader->httpSessionParams));
        pDownloader->httpSessionParams.fileName         = pDownloader->fileName;
        pDownloader->httpSessionParams.hostName         = "";
        pDownloader->httpSessionParams.timeoutForWaitMs = pDownloader->requestTimeoutMs;
    }


    // Clear down HTTP Session results.
    HttpClientInitSessionResults(&(pDownloader->httpSessionResults));
 
    HttpClientInit(&(pDownloader->httpClient), &httpClientInitParams, 
        &(pDownloader->httpSessionParams), 
        &(pDownloader->httpSessionResults));  

    FileDownloaderCalculateNextBlock(pDownloader);
    
    HttpClientBegin(&(pDownloader->httpClient), pTm);

    pDownloader->state = E_FD_RUN;
}


/******************************************************************************
**    
**      @param[in, out] FileDownloader The HTTP Manager object
**           @param[in] tickIntervalMs The time between ticks
**              @return E_ERROR = Finished - in error state, E_RUNNING = continue 
**                                calling E_DONE_OK Finished and OK
**  
**  @brief
**         Description: Run a File Download Session  
**
******************************************************************************/
E_FSM FileDownloaderRun (FILE_DOWNLOADER* pDownloader, uint32_t tickIntervalMs)
{    
    E_FSM r = E_RUNNING;
    E_FD_STATE nextState = E_FD_UNINITIALIZED_ERROR;

    ASSERT(NULL != pDownloader);

    pDownloader->timeElapsedMs += tickIntervalMs;

    // Abandon transfer if demanded.
    if ((FILE_DOWNLOADER_USER_CANCELLED == pDownloader->errorState) ||
        (FILE_DOWNLOADER_DISCONNECTED   == pDownloader->errorState))
    {
        pDownloader->state = E_FD_DONE_ERROR;
    }

    switch (pDownloader->state)
    {
        // Keep doing HTTP Gets
    case E_FD_RUN:  
        {
            switch (HttpClientRun(&(pDownloader->httpClient), tickIntervalMs))
            {
            case E_RUNNING:
                nextState = E_FD_RUN;
                break; 

            case E_DONE_OK_RECONNECT_NEEDED:
                nextState = E_FD_RUN;
                r = E_RUNNING_RECONNECT_NEEDED;
                break; 

            case E_DONE_OK:
                nextState = OnRequestCompleted(pDownloader);
                break;

            case E_ERROR:
            default:
                nextState = OnRequestFailed(pDownloader);
                break;
            }
        }
        break;

    case E_FD_DONE_OK:
        {
            nextState = E_FD_DONE_OK;
            r = E_DONE_OK;
        }
        break;

    case E_FD_DONE_ERROR:
        {
            nextState = E_FD_DONE_ERROR;
            r = E_ERROR;
        }
        break;

    case E_HTTP_UNINITIALISED_ERROR:
    default:
        {
            ASSERT(0);
        }
        break;
    } // switch state


    if (nextState != pDownloader->state)
    {
        pDownloader->state = nextState;
    }

    return r;
}


/******************************************************************************
**    
**      @param[in, out] pClient The HTTP  Manager object
**      @param[in] b The byte received
**              @return None
**  
**  @brief
**         Description: Handle a content byte - callback from client.
** In turn, this will call up to the user's callabck (for programming)
******************************************************************************/
STATIC void HandleReceivedContentByte(struct HTTP_CLIENT* pClient, uint8_t b)
{
    struct FILE_DOWNLOADER* pDownloader = (FILE_DOWNLOADER*)(pClient->pCtx);

    ASSERT(NULL != pDownloader);

    if (NULL != pDownloader->OnReceivedByte)
    {
        pDownloader->OnReceivedByte(pDownloader, b, pDownloader->byteCount);
    }

    pDownloader->byteCount++;
}


/******************************************************************************
**    
**      @param[in, out] FileDownloader The   Manager object
**              @return the next internal FSM state
**  
**  @brief
**         Description: Handle the event on completion of HTTP req
**  (NOTE - this does NOT mean that the HTTP response was ok, only that a 
**  response was actually got - we could have a 404 for example)
******************************************************************************/
STATIC E_FD_STATE  OnRequestCompleted(FILE_DOWNLOADER* pDownloader)
{
    E_FD_STATE                              nextState = E_FD_UNINITIALIZED_ERROR;
    struct HTTP_CLIENT_SESSION_RESULTS*     pSessionResults = pDownloader->httpClient.pSessionResults;

    ASSERT(NULL != pDownloader); 
    
    pDownloader->numRequests++;

    // Reset the request retry count (will retry a failed http request)
    pDownloader->requestRetryCount = 0;

    // Retain the code in case of error - for debug/diagnostics
    pDownloader->lastServerResponseCode = pSessionResults->responseCode;

    switch(pSessionResults->responseCode)
    {
    case HTTP_RESPONSE_OK:  // 200 OK. Entire file was delivered in one request.
        pDownloader->totalContentLength = pSessionResults->contentLength;
        nextState = E_FD_DONE_OK;
        break;

    case HTTP_RESPONSE_PARTIAL_CONTENT:
        {
            pDownloader->totalContentLength = pSessionResults->rangeTotalContentLength;

            if (pDownloader->byteCount == pDownloader->totalContentLength)
            {
                // Correct total number of bytes now received
                nextState = E_FD_DONE_OK;
            }
            else if (pDownloader->byteCount > pDownloader->totalContentLength)
            {
                // Error - more bytes got than in the resource
                FileDownloaderSetError(pDownloader, FILE_DOWNLOADER_FILE_OVERFLOW);
                nextState = E_FD_DONE_ERROR;
            }
            else
            {
                // More data to get
                FileDownloaderCalculateNextBlock(pDownloader);

                HttpClientBegin(&(pDownloader->httpClient), pDownloader->pTm);
                nextState = E_FD_RUN;
            }
        }
        break;

    case HTTP_RESPONSE_NOT_FOUND:   // 404 error
        {
            FileDownloaderSetError(pDownloader, FILE_DOWNLOADER_FILE_NOT_FOUND);
            nextState = E_FD_DONE_ERROR;
        }
        break;

    default:                        // Bad request and all other fail codes
        {
            FileDownloaderSetError(pDownloader, FILE_DOWNLOADER_HTTP_SERVER_ERROR);
            nextState = E_FD_DONE_ERROR;
        }
        break;
    }

    return nextState;
}


/******************************************************************************
**    
**      @param[in, out] FileDownloader The   Manager object
**              @return the next internal FSM state
**  
**  @brief
**         Description: Handle the failure of HTTP - e.g no response (timeout)
**  (NOTE - this is for situations where no or non-http compliant responses were
** received, NOT 404's). we retry the current request, up to a limit count.
******************************************************************************/
STATIC E_FD_STATE  OnRequestFailed(FILE_DOWNLOADER* pDownloader)
{
    E_FD_STATE nextState = E_FD_UNINITIALIZED_ERROR;

    pDownloader->numFailedRequests++;

    // The request failed, so there can be no server response code.
    pDownloader->lastServerResponseCode = 0;

    if (pDownloader->requestRetryCount < pDownloader->maxRequestRetries)
    {
        pDownloader->requestRetryCount++; 

        // run the same request again
        HttpClientBegin(&(pDownloader->httpClient), pDownloader->pTm);
        nextState = E_FD_RUN;
    }
    else
    {
        FileDownloaderSetError(pDownloader, FILE_DOWNLOADER_RETRIES_FAILED);
        nextState = E_FD_DONE_ERROR;
    }

    return nextState;
}


/******************************************************************************
**    
**      @param[in, out] FileDownloader The  Manager object
**              @return None
**  
**  @brief
**         Description: Move the range indices another block onwards
**
******************************************************************************/
STATIC void  FileDownloaderCalculateNextBlock(FILE_DOWNLOADER* pDownloader)
{ 
    ASSERT(NULL != pDownloader); 

    pDownloader->httpSessionParams.firstRangeByte   = pDownloader->byteCount;
    pDownloader->httpSessionParams.lastRangeByte    = pDownloader->httpSessionParams.firstRangeByte 
                                                        + pDownloader->requestSize - 1;
    
    // Do not request beyond the end of the resource
    if (pDownloader->httpSessionParams.lastRangeByte > pDownloader->totalContentLength - 1)
    {
        pDownloader->httpSessionParams.lastRangeByte = pDownloader->totalContentLength - 1;
    }    
}


/******************************************************************************
**    
**      @param[in] pDownloader The File Downloader object
**      @param[out] pBytesReceived Number of bytes got so far (may be NULL)
**      @param[out] pTotalBytes Number of bytes in file (may be NULL)
**              @return Percent complete
**  
**  @brief
**         Description: Get progress in percent. Also (optionally) gets the total 
**          size and num of received bytes thus far.
******************************************************************************/
uint32_t FileDownloaderGetProgressPercent(const FILE_DOWNLOADER* pDownloader, 
    uint32_t* pBytesReceived, uint32_t* pTotalBytes)
{
    uint32_t percentComplete = 0;

    ASSERT(NULL != pDownloader); 

    if (pDownloader->totalContentLength > 0)
    {
        percentComplete = (100 * pDownloader->byteCount) / pDownloader->totalContentLength; 
    }

    if (NULL != pBytesReceived)
    {
        *pBytesReceived = pDownloader->byteCount;
    }

    if (NULL != pTotalBytes)
    {
        *pTotalBytes = pDownloader->totalContentLength;
    }

    return percentComplete;
}


/******************************************************************************
**    
**      @param[in] pDownloader The File Downloader object
**      @param[in] errorCode The error code to set
**              @return None
**  
**  @brief
**         Sets the objects error state
******************************************************************************/
STATIC void FileDownloaderSetError(FILE_DOWNLOADER* pDownloader, E_FILE_DOWNLOADER_ERROR errorCode)
{
    ASSERT(NULL != pDownloader); 

      if (errorCode != pDownloader->errorState)
      {
          pDownloader->errorState = errorCode;
      }
}


/******************************************************************************
**    
**      @param[in] pDownloader The File Downloader object
**  
**  @brief
**         Aborts the download session
******************************************************************************/
void FileDownloaderCancel(FILE_DOWNLOADER* pDownloader, E_FILE_DOWNLOADER_ERROR eCancelCode)
{
   FileDownloaderSetError(pDownloader, eCancelCode);
} 


/******************************************************************************
**    
**      @param[in] pDownloader The File Downloader object
**  
**  @brief
**         Debug output at end of session
******************************************************************************/
void FileDownloaderDebugPrintResult(const FILE_DOWNLOADER* pDownloader)
{
    ASSERT(NULL != pDownloader);  
    
    if (FILE_DOWNLOADER_OK == pDownloader->errorState)
    {            
        DebugTrace("\n\n\nFILE DOWNLOAD SUCCESS\nURL=%s, Bytes = %u in %us = %u bytes/s\n",
            pDownloader->fileName,
            pDownloader->byteCount,
            pDownloader->timeElapsedMs / 1000ul,
            (1000 * pDownloader->byteCount) / pDownloader->timeElapsedMs); 

    }
    else
    {
        DebugTrace("\n\n\nFILE DOWNLOAD ERROR = %u\n(last server response = %u %s)\n", 
            pDownloader->errorState, 
            pDownloader->lastServerResponseCode,
            (0 == pDownloader->lastServerResponseCode) ? "NO RESP." : "");
    }
    
    DebugTrace("FILE DOWNLOAD STATS:\nrequestSize = %u, totalContentLength =  %u\n",   
        pDownloader->requestSize,   
        pDownloader->totalContentLength);
    
    DebugTrace("Num Reqs = %u, numFailedRequests = %u \n",   
        pDownloader->numRequests,   
        pDownloader->numFailedRequests); 
}

#ifndef _WIN32
#pragma pop
#endif

/**
* @}
*/
