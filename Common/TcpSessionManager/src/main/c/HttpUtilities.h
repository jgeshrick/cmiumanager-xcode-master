/******************************************************************************
*******************************************************************************
**
**         Filename: HttpUtilities.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: Helper functions for HTTP client and server
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup Http
 * @{
 */


/*!***************************************************************************
 * @file HttpUtilities.h
 *
 * @brief   
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/


#ifndef HTTP_UTIL_H_
#define HTTP_UTIL_H_

#include "CommonTypes.h"

/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/



/**
** @brief The Max length of a URL String
**/ 
#define HTTP_MAX_URL_LEN                256
#define HTTP_MAX_LINE_LEN               (HTTP_MAX_URL_LEN + 128)



/**
** @brief HTTP Header strings
**/
#define HTTP_1_1                        "HTTP/1.1"
#define HTTP_PARTIAL_CONTENT_STR        "Partial Content" 
#define HTTP_PARTIAL_RANGE_STR          "Range: bytes=" 
#define HTTP_CONTENT_RANGE_STR          "Content-Range: bytes"
#define HTTP_CONTENT_LENGTH_STR         "Content-Length:"
#define HTTP_CONNECTION_STR             "Connection:"
#define HTTP_CLOSE_STR                  "close"
#define HTTP_BLANK_LINE                 "\r\n"

#define HTTP_BLANK_LINE                 "\r\n"
#define HTTP_RESPONSE_OK                200
#define HTTP_RESPONSE_PARTIAL_CONTENT   206
#define HTTP_RESPONSE_BAD_REQUEST       400
#define HTTP_RESPONSE_NOT_FOUND         404
#define HTTP_RESPONSE_INTERNAL_ERROR    500

 
/**
** @brief The object that runs the HTTP reading lines 
**/
typedef struct HTTP_LINE_READER 
{
    uint8_t*            pLineBuffer;
    uint32_t            bufferSize;
    uint32_t            pos;
}
HTTP_LINE_READER;


/**
** @brief Error codes at end of HTTP transaction.
**/
typedef enum HTTP_ERROR
{
    HTTP_OK                         = 0,
    HTTP_ERROR_UNINITIALISED        = 1,    //!< Attempt to run FSM without first initialising it 
    HTTP_ERROR_SOCKET_SEND          = 2,    //!< Failed to send data over connection
    HTTP_ERROR_SERIALISATION        = 4,    //!< Failed to serialise a packet to send (buffer too small?)
    HTTP_ERROR_BAD_URL              = 5,    //!< Too long or invalid
    HTTP_ERROR_BUFFER_OVERFLOW      = 6,
    HTTP_ERROR_BUFFER_TOO_SMALL     = 7,
    HTTP_ERROR_FILE_OPEN            = 8,
    HTTP_ERROR_INCONSISTENT_RANGE   = 9,    //!< Client rx'd inconsistent range info, or server asked to supply bad range of data
    HTTP_ERROR_MISMATCHED_RANGE     = 10,   //!< Did not receive the range we requested
    HTTP_ERROR_TIMEOUT              = 11,   //!< Timeout in HTTP Client FSM
    HTTP_ERROR_FILE_PATH_LENGTH     = 12    //!< Unable to form full file path and name 
}
HTTP_ERROR;



/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/

void LineReaderInit(HTTP_LINE_READER* pLr, uint8_t* buffer, uint32_t bufferSize);
const uint8_t* LineReaderReadByte(HTTP_LINE_READER* pLr, uint8_t b);
bool LineReaderIsBlankLine(const uint8_t* pLine);
const char* LineReaderParseNumeric(const uint8_t* pLine, const char* pToken, uint32_t* pNumber);
const char* LineReaderParseString(const uint8_t* pLine, const char* pToken);
bool LineReaderParseContentLength(const uint8_t* pLine, uint32_t* pContentLength);
bool LineReaderParseContentRange(const uint8_t* pLine,
      const char* pToken,
      uint32_t* pFirstByte,
      uint32_t* pLastyte,
      uint32_t* pTotalContentLength);
bool LineReaderParseConnectionClose(const uint8_t* pLine);

HTTP_ERROR HttpMakeGetRequestHelper(uint8_t* buffer, uint32_t bufferSize,
    const char* url,
    const char* hostName, 
    uint32_t firstRangeByte, 
    uint32_t lastRangeByte);

uint32_t HttpMakeGetResponseHelper(uint8_t* buffer, uint32_t bufferSize,   
    uint32_t responseCode,
    uint32_t contentLength,
    uint32_t firstRangeByte, 
    uint32_t lastRangeByte, 
    uint32_t totalDataSize);

#endif  

/**
 * @}
 */
