/******************************************************************************
*******************************************************************************
**
**         Filename: TransportInterface.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 /**
 * @addtogroup  
 * @{
 */

/*!***************************************************************************
 * @file TransportInterface.h
 *
 * @brief The interface to a modem / comms object 
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/

#ifndef TRANSPORT_INTERFACE_H_
#define TRANSPORT_INTERFACE_H_


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include "CommonTypes.h" 


/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/


/**
** @brief Command types that can be used in pfnCommandFunction
**/
typedef enum E_CONNECTION_COMMAND
{
    CON_CMD_NULL,
    CON_CMD_OPEN_CONNECTION,
    CON_CMD_OPEN_SOCKET,
    CON_CMD_CLOSE_SOCKET,
    CON_CMD_CLOSE_CONNECTION,
    CON_CMD_GET_RX_COUNT,
    CON_CMD_GET_INFO,
    CON_CMD_ASSERT_LTE,
    CON_CMD_UPDATE_MODEM_FIRMWARE,
    CON_CMD_GET_APN,
    CON_CMD_SET_APN,
    CON_CMD_SHUTDOWN_MODEM,
    CON_CMD_GET_SIGNAL_QUALITY
}
E_CONNECTION_COMMAND;


/**
** @brief Parameters required to connect to target
**/
typedef struct CONNECTION_PARAMS
{
    const char*   host;           // = "iot.eclipse.org";
    int     port;           // = 1883;
    int     modemType;      /// @todo for test only 0 = winsock, 1 = modem
}
CONNECTION_PARAMS;


/**
** @brief Parameters required for the assert LTE command
**/
typedef struct ASSERT_LTE_CARRIER_PARAMS
{
	uint8_t    rfBand;
	uint16_t   rfChannel;
	uint8_t    rfPowerLevel;
} ASSERT_LTE_CARRIER_PARAMS;

/**
** @brief Parameters required for the get CAN data command
**/
typedef struct GET_CAN_DATA_PARAMS
{
	bool    leaveModemOn;
} GET_CAN_DATA_PARAMS;

/**
** @brief Parameters required for the Update Modem command
**/
typedef struct UPDATE_MODEM_FIRMWARE_PARAMS
{
	uint8_t        *ftpServer;
	uint16_t       ftpPort;
    uint8_t        *username;
    uint8_t        *password;
	uint8_t        *fotaFilename;
    uint8_t        *newFirmwareVersion;
} UPDATE_MODEM_FIRMWARE_PARAMS;

/**
** @brief Parameters required for the Update APN command
**/
typedef struct UPDATE_APN_PARAMS
{
	uint8_t        *apnString;
} UPDATE_APN_PARAMS;

/**
** @brief Structure for returning a string result
**/
typedef struct QUERY_STRING_RESULT
{
    char*   buffer;     // points to a buffer to store the result
    uint16_t      buffSize;   // gives the size of the buffer
}
QUERY_STRING_RESULT;


/**
** @brief Structure for returning a 16 bit numeric result
**/
typedef struct QUERY_NUMBER_RESULT
{
    UU64      numberResult;   // returns the numeric value
}
QUERY_NUMBER_RESULT;

/**
** @brief States that can be reported by pfnGetStateFunction calls
**/
typedef enum E_CONNECTION_STATE
{
    CON_STATE_UNINITIALIZED,
    CON_STATE_OPENING_CONNECTION,
    CON_STATE_CONNECTION_OPEN_SOCKET_CLOSED,
    CON_STATE_OPENING_SOCKET,
    CON_STATE_SOCKET_OPEN,
    CON_STATE_CONNECTION_CLOSED,
    CON_STATE_ERROR
}
E_CONNECTION_STATE;



/**
** @brief States that can be reported by pfnGetStateFunction calls
**/
typedef enum E_QUERY
{
    QRY_CONNECTION_STATE,
    QRY_SOCKET_STATE,
    QRY_GET_RSSI,
    QRY_GET_REGISTRATION_INFO_STR,
    QRY_GET_CURRENT_OPERATOR_STR,
    QRY_GET_MODEM_HARWARE_REV_STR,
    QRY_GET_MODEM_MODEL_ID_STR,
    QRY_GET_MODEM_MANUFACTURER_ID_STR,
    QRY_GET_MODEM_SOFTWARE_REV_STR,
    QRY_GET_MODEM_SERIAL_NUMBER_STR,
    QRY_GET_IMSI_STR,
    QRY_GET_SIM_ID_STR,
    QRY_GET_PIN_STATUS_STR,
    QRY_GET_AVAILABLE_OPERATORS_STR,
    QRY_GET_NETWORK_PERFORMANCE_STR,
    QRY_GET_CURRENT_IP_ADDRESS_STR,
    QRY_GET_CURRENT_APN_STR,
    QRY_GET_MSISDN_STR,
    QRY_GET_TIME_REGISTRATION,
    QRY_GET_TIME_CONTEXT_ACTIVATION,
    QRY_GET_TIME_SERVER_CONNECT,
    QRY_GET_TIME_DATA_TRANSFER,
    QRY_GET_TIME_DISCONNECT,
    QRY_GET_CONNECTION_TIME,

}
E_QUERY;


/**
** Function pointer type definitions for transport manager abstraction
**/
typedef int32_t             (*pfnInitFunction)(void* pCtx, const void* initParams );
typedef int32_t             (*pfnCommandFunction)(void* pCtx, E_CONNECTION_COMMAND cmd, void* cmdParams );
typedef void                (*pfnTickFunction)(void* pCtx, uint32_t tickIntervalMs);
typedef E_CONNECTION_STATE  (*pfnGetStateFunction)(void* pCtx, E_QUERY query, void* pQueryResult);
typedef int32_t             (*pfnReceiveFunction)(void* pCtx, uint8_t* rxBuffer, uint32_t bufferSize);
typedef int32_t             (*pfnSendFunction)(void* pCtx, const uint8_t* txData, uint32_t txCount);


/**
** @brief Abstraction object used by the MqttManager to communicate with the TCP stack
**/
typedef struct TRANSPORT_INTERFACE 
{ 
	// Function pointers to underlying implementation of this API
    pfnInitFunction         pfnInit;
    pfnCommandFunction      pfnCmd;
    pfnTickFunction         pfnTick;
    pfnGetStateFunction     pfnGetState;
    pfnReceiveFunction      pfnReceive; 
    pfnSendFunction         pfnSend;
} 
TRANSPORT_INTERFACE;


/**
** @brief Abstraction object used by the MqttManager to communicate with the TCP stack
**/
typedef struct TRANSPORT_MANAGER 
{ 
    // Reference pointer to be used as required; this is passed into
    // the function pointer calls by TransportManager at runtime
    void*                    pCtx;         

    const TRANSPORT_INTERFACE*   pInterface;
} 
TRANSPORT_MANAGER;




#endif //TRANSPORT_INTERFACE_H_
