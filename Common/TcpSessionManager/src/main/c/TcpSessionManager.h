/******************************************************************************
*******************************************************************************
**
**         Filename: TcpSessionManager.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup TcpSessionManager 
 * @{
 */


/*!***************************************************************************
 * @file TcpSessionManager.h
 *
 * @brief Handles the connect-run-disconnect MQTT session  
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/

#ifndef TCP_SESSION_MANAGER_H_
#define TCP_SESSION_MANAGER_H_


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/

//Include types for the state machine
#include "FsmTypes.h"
#include "CommonTypes.h"
#include "DebugTrace.h"
#include "TransportManager.h"

/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/


typedef enum E_SM_STATE
{
    E_SM_RESET,
    E_SM_BEGIN_OPEN_CONNECTION, 
    E_SM_OPEN_CONNECTION_WAIT,
    E_SM_ON_CONNECTION_OPENED,
    E_SM_BEGIN_OPEN_SOCKET,
    E_SM_OPEN_SOCKET_WAIT,
    E_SM_ON_SOCKET_OPENED,
    E_SM_CONNECTED,
    E_SM_BEGIN_CLOSE_SOCKET,
    E_SM_CLOSE_SOCKET_WAIT,
    E_SM_ON_SOCKET_CLOSED,
    E_SM_BEGIN_CLOSE_CONNECTION,
    E_SM_CLOSE_CONNECTION_WAIT,
    E_SM_ON_CONNECTION_CLOSED

} E_SM_STATE;


typedef void  (*pfnManagerBeginFunction)(void* pCtx, struct TRANSPORT_MANAGER* pCp);
typedef E_FSM (*pfnManagerRunFunction)(void* pCtx,  uint32_t tickIntervalMs);
typedef void  (*pfnManagerCompletionFunction)(void *pSm, void* pCtx);               // Note void* pSm used to avoid Keil error 167    

typedef struct TCP_SESSION_MANAGER
{
    E_SM_STATE state;
    int beginFlag;
    
    const struct MQTT_MANAGER_SESSION_PARAMS*   pSessionParams;
    struct MQTT_MANAGER_SESSION_RESULTS*        pSessionResults;
    struct TRANSPORT_MANAGER*                   pTm;
    const CONNECTION_PARAMS*                    pConnectParams;
    void*                                       pManager;
    pfnManagerBeginFunction                     pfnManagerBegin;
    pfnManagerRunFunction                       pfnManagerRun;
    pfnManagerCompletionFunction                pfnOnComplete;
}
TCP_SESSION_MANAGER;


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/

    DLL_EXPORT void 
    TcpSessionManagerInit(TCP_SESSION_MANAGER* pSm,
    TRANSPORT_MANAGER* pTm,
    CONNECTION_PARAMS* pConnectParams);



DLL_EXPORT void 
    TcpSessionManagerBegin(TCP_SESSION_MANAGER* pSm,
    void* pManager,
    pfnManagerBeginFunction pfnBegin,
    pfnManagerRunFunction pfnRun,
    pfnManagerCompletionFunction    pfnOnComplete);

DLL_EXPORT void 
    TcpSessionManagerTick(TCP_SESSION_MANAGER* pSm, uint32_t tickIntervalMs);

DLL_EXPORT bool 
    TcpSessionManagerIsBusy(TCP_SESSION_MANAGER* pSm);


#endif // TCP_SESSION_MANAGER_H_


/**
 * @}
 */
