/******************************************************************************
*******************************************************************************
**
**         Filename: MqttManager.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**          The MQTT manager handles the MQTT transaction
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup MqttManager
 * @{
 */


/*!***************************************************************************
 * @file MqttManager.c
 *
 * @brief  Handles the MQTT transaction
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/

 
 /*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "CommonTypes.h"
#include "DebugTrace.h"
#include "MQTTPacket.h"
#include "TransportManager.h"
#include "MqttManagerDebug.h"
#include "MqttManager.h"


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/

static void             MqttManagerCleanupHelper (MQTT_MANAGER* pM);
static E_MQTTM_STATE    MqttManagerPacketWaitHelper(MQTT_MANAGER* pM );
static E_MQTTM_STATE    MqttManagerPacketReceiveHelper(MQTT_MANAGER* pM, int32_t pktRxType);
static int32_t          MqttPackConnectHelper(MQTT_MANAGER* pM);
static void             MqttSendSubscribeHelper(MQTT_MANAGER* pM, const MQTT_MANAGER_SUBSCRIPTION* pSub);
static int32_t          MqttPackPublishHelper(MQTT_MANAGER* pM, const MQTT_MANAGER_PUB* pPub, int32_t bufferWritePosition);
static void             MqttMarkSentPublish(MQTT_MANAGER* pM,uint32_t currentSendPub, uint16_t packetId);
static void             MqttSendPubackHelper(MQTT_MANAGER* pM, uint16_t msgId);
static void             MqttSendDisconnectHelper(MQTT_MANAGER* pM);

static const MQTT_MANAGER_SUBSCRIPTION* 
                        GetCurrentSubscription(const MQTT_MANAGER* pM);

static const MQTT_MANAGER_PUB* 
                        GetCurrentMessageToPublish(const MQTT_MANAGER* pM);

static int32_t          MqttReceivePublishHelper(MQTT_MANAGER* pM);
static E_MQTTM_STATE    MqttReceivePubackHelper(MQTT_MANAGER* pM);
static bool             MqttSessionCanClose(const MQTT_MANAGER* pM);
static void             MqttManagerBindToTransport(MQTT_MANAGER* pM, TRANSPORT_MANAGER* pTm);
static int              t_recv_nb(void* vp, unsigned char* buf, int count);
static void             MqttSendPktHelper(MQTT_MANAGER* pM, int32_t bytesToSend, uint8_t pktTypes[], uint8_t numberPackets);
static uint16_t         MqttGetNextPacketId(MQTT_MANAGER* pM);
static void             MqttSetError(MQTT_MANAGER* pM, MQTT_ERROR mqttError);


/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/


/******************************************************************************
**    
**      @param[in, out] pM The MQTT_MANGER object
**           @param[in] pInitParams The init data
**           @param[in] pSessionParams The Session Params to use
**           @param[out] pSessionResults The Session Results to populate when done
**        
**              @return None
**  
**  @brief
**         Description: Init an MQTT_MANAGER object
**
******************************************************************************/
DLL_EXPORT void 
    MqttManagerInit(MQTT_MANAGER* pM, 
    const MQTT_MANAGER_INIT_PARAMS* pInitParams,         
    const MQTT_MANAGER_SESSION_PARAMS* pSessionParams,
    MQTT_MANAGER_SESSION_RESULTS* pSessionResults)
{    
    MQTTPacket_connectData  connectData = MQTTPacket_connectData_initializer;
    
    
    pM->pSessionParams                      = pSessionParams;
    pM->pSessionResults                     = pSessionResults;

    pM->errorState      = MQTT_ERROR_FSM_UNINITIALISED;
    pM->state           = E_MQTT_BEGIN;
    pM->pTm             = NULL;
    pM->grantedQoS      = E_MQTT_QOS_0;
    pM->sessionPresent  = 0;    
    pM->nextPacketId    = 1;

    // Paho workaround way of initialising our copy of this...
    pM->mqttConnectData = connectData;

    pM->pfOnPublishRxCallback   = pInitParams->pfOnPublishRxCallback;
    pM->bufferSize              = MQTT_WORKING_BUFFER_SIZE;
}


/******************************************************************************
**    
**      @param[in, out] pSessionParams The MQTT_MANAGER_SESSION_RESULTS object
**              @return None
**  
**  @brief
**         Description: Init an MQTT_MANAGER_SESSION_PARAMS object to defaults
**
******************************************************************************/
DLL_EXPORT void 
    MqttManagerInitSessionParams(MQTT_MANAGER_SESSION_PARAMS* pSessionParams)
{ 
    memset(pSessionParams, 0, sizeof(MQTT_MANAGER_SESSION_PARAMS));

    ///@ todo find sensible defaults
    pSessionParams->mqttVersion       = MQTT_VERSION_3_1_1;  /// Note : 3.1.1 = 4 and 3.1.0 = 3
    pSessionParams->clientID          = "DEFAULT_ClientID";
	pSessionParams->keepAliveInterval = 20;
	pSessionParams->cleansession      = 0;
	pSessionParams->username          = "DEFAULT_user";
	pSessionParams->password          = "DEFAULT_password";   
    
    pSessionParams->subscriptionCount = 0;
    pSessionParams->pubToSendCount = 0;

    pSessionParams->timeoutForWaitMs  = DEFAULT_MQTT_SESSION_TIMEOUT_MS;
}

/******************************************************************************
**    
**      @param[in, out] pSessionParams The MQTT_MANAGER_SESSION object
**           @param[in] pSub The Subscribe msg to add
**              @return 0 if OK, -1 if list full
**  
**  @brief
**         Description: Add a Subscribe to the list
**
******************************************************************************/
DLL_EXPORT int32_t 
    MqttManagerAddSubscription(MQTT_MANAGER_SESSION_PARAMS* pSessionParams,
    const  MQTT_MANAGER_SUBSCRIPTION* pSub)
{  
    int32_t r = -1;
    ASSERT(pSub != NULL);    
    ASSERT(pSub->topicFilter != NULL);

    if (pSessionParams->subscriptionCount < MQTT_MAX_SUBSCRIPTIONS)
    {
        pSessionParams->subscriptions[pSessionParams->subscriptionCount] = pSub;
        pSessionParams->subscriptionCount++;
        r = 0;
    }
    return r;
}

/******************************************************************************
**    
**      @param[in, out] pSessionParams The MQTT_MANAGER_SESSION object
**           @param[in] pPub The Publish msg to add
**              @return Index of Pub added. 0-n if OK, or -1 if list full
**  
**  @brief
**         Description: Add a Publish to the list
** The first call will return 0, the next 1 etc, or -1 if list was full and 
** no more pubs were added
******************************************************************************/
DLL_EXPORT int32_t 
    MqttManagerAddPublish(MQTT_MANAGER_SESSION_PARAMS* pSessionParams,
    const  MQTT_MANAGER_PUB* pPub)
{  
    int32_t r = -1;
    ASSERT(pPub != NULL);
    ASSERT(pPub->topic != NULL);

    if (pSessionParams->pubToSendCount < MQTT_MAX_PUBS_TO_SEND)
    {
        pSessionParams->pubs[pSessionParams->pubToSendCount] = pPub;
        
        r = (int32_t)pSessionParams->pubToSendCount;
        pSessionParams->pubToSendCount++;
    }

    return r;
}

/******************************************************************************
**    
**      @param[in, out] pSessionResults The MQTT_MANAGER_SESSION_RESULTS object
**              @return None
**  
**  @brief
**         Description: Init a MQTT_MANAGER_SESSION_RESULTS object
**
******************************************************************************/
DLL_EXPORT void    
    MqttManagerInitSessionResults(MQTT_MANAGER_SESSION_RESULTS* pSessionResults)
{
    ASSERT(NULL != pSessionResults);   
    memset(pSessionResults, 0, sizeof(MQTT_MANAGER_SESSION_RESULTS));
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**              @return Get the session results
**  
**  @brief
**         Description: Get the session results
**
******************************************************************************/
DLL_EXPORT MQTT_MANAGER_SESSION_RESULTS*  
    MqttManagerGetSessionResults(MQTT_MANAGER* pM)
{
    return pM->pSessionResults;
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**      @param[in, out] pTm The Transport
**           @param[in] pSessionParams The Session Params to use
**          @param[out] pSessionResults The Session Results to populate when done
**              @return 0 if OK, -1 if error
**  
**  @brief
**         Description: Prepare the session state machine (begin session)
**
******************************************************************************/
DLL_EXPORT int32_t    
    MqttManagerFsmInit(MQTT_MANAGER* pM, 
    TRANSPORT_MANAGER* pTm,     
   const MQTT_MANAGER_SESSION_PARAMS* pSessionParams,
   MQTT_MANAGER_SESSION_RESULTS* pSessionResults)
{
    ASSERT(NULL != pTm);
    ASSERT(NULL != pSessionResults);


    // No comms link / results buffer. Cannot run protocol.
    if ((NULL == pTm) || (NULL == pSessionResults))
    {
        return -1;
    }    
    
    MqttManagerBindToTransport(pM, pTm);

    pM->pSessionParams                      = pSessionParams;
    pM->pSessionResults                     = pSessionResults;

    pM->mqttConnectData.clientID.cstring    = pM->pSessionParams->clientID;
    pM->mqttConnectData.MQTTVersion         = pM->pSessionParams->mqttVersion;
    pM->mqttConnectData.keepAliveInterval   = pM->pSessionParams->keepAliveInterval;
    pM->mqttConnectData.cleansession        = pM->pSessionParams->cleansession;
    pM->mqttConnectData.username.cstring    = pM->pSessionParams->username;
    pM->mqttConnectData.password.cstring    = pM->pSessionParams->password;

    // workaround for MQTT API 
    pM->mqttTransport.sck                   = (void*)pTm;
    pM->mqttTransport.getfn                 = t_recv_nb;
    pM->mqttTransport.state                 = 0;    
    
    pM->grantedQoS                          = E_MQTT_QOS_0;
    pM->currentSubscription                 = 0;
    pM->currentSendPub                      = 0;
    pM->pubAcksExpected                     = 0;
 
    MqttManagerInitSessionResults(pM->pSessionResults);

    pM->timeElapsedMs                       = 0;
    pM->state                               = E_MQTT_BEGIN; 

    pM->errorState                          = MQTT_OK;
    return 0;
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**      @param[in, out] cb The callback to register
**              @return None
**  
**  @brief
**         Description: Set a callback to be called when a PUBLISH msgs is received    
**
******************************************************************************/
void MqttManagerSetPublishCallback(MQTT_MANAGER* pM, pfOnPublishMessageReceivedCallback cb)
{
    ASSERT(NULL != pM);
    pM->pfOnPublishRxCallback = cb;
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**           @param[in] tickIntervalMs The time between ticks
**              @return E_ERROR = Finished - in error state, E_RUNNING = continue 
**                                calling E_DONE_OK Finished and OK
**  
**  @brief
**         Description: Run an MQTT Manager Session  
**
******************************************************************************/
DLL_EXPORT E_FSM 
    MqttManagerRun (MQTT_MANAGER* pM, uint32_t tickIntervalMs)
{    
    const MQTT_MANAGER_SUBSCRIPTION* pSub;
    const MQTT_MANAGER_PUB* pPub;
    uint32_t size;
    uint32_t numWritten= 0;
    uint8_t mqttPackets[5];
    uint8_t mqttPacketCount;

    E_FSM r = E_RUNNING;
    E_MQTTM_STATE nextState = E_MQTT_UNINITIALISED_ERROR;

    ASSERT(NULL != pM);

    pM->timeElapsedMs += tickIntervalMs;

    if (MQTT_OK != pM->errorState)
    {
        if ((E_MQTT_DONE_OK == pM->state) ||
            (E_MQTT_DONE_ERROR == pM->state))
        {
            pM->state = E_MQTT_DONE_ERROR;
        }
        else
        {
            DebugTrace("MQTT ErrorState set to %u; Terminating FSM", pM->errorState);
            pM->state = E_MQTT_DISCONNECT;
        }
    }

    switch (pM->state)
    {
    case E_MQTT_BEGIN:  
        {
            pM->sessionPresent  = 0; // Must be cleared initially; Will be read from the CONNACK when it arrives.

            // Make list of initial packets to sent (first is CONNECT, then Pubs)
            size = MqttPackConnectHelper(pM);
            mqttPackets[0]  = CONNECT;
            mqttPacketCount = 1;

            /** Add the first packet (hopefully it can fit with the connect) */
            pPub = GetCurrentMessageToPublish(pM);
            if ((pPub != NULL) && (pPub->dataLength < (pM->bufferSize - size)))
            {
                numWritten = MqttPackPublishHelper(pM, pPub, size);
                if (numWritten > 0)
                {
                    size += numWritten;
                    mqttPackets[mqttPacketCount] = PUBLISH;
                    mqttPacketCount++;
                }
            }

            MqttSendPktHelper(pM, size, mqttPackets, mqttPacketCount);

            if(pPub != NULL)
            {
                nextState = E_MQTT_DO_PUBLISH;
            }
            else
            {
                nextState = E_MQTT_WAIT_PKT;
            }
        }
        break;

    case E_MQTT_WAIT_PKT:      
         nextState = MqttManagerPacketWaitHelper(pM);
        break;
        
        // Send all subscriptions. When done, do the publishing.
    case E_MQTT_DO_SUBSCRIBE:
        pSub = GetCurrentSubscription(pM);
        if (NULL != pSub)
        {
            MqttSendSubscribeHelper(pM, pSub);
            nextState = E_MQTT_WAIT_PKT; // wait for SUBACK
        }
        else
        {
            nextState = E_MQTT_DO_PUBLISH;
        }
        break;

    // Send all PUBLISH messages. When done, if we have received 
    // the expected number of PUBLISH msgs, then disconnect.
    // Else wait a while to allow any other incoming msgs to arrive before closing.
    case E_MQTT_DO_PUBLISH:
        mqttPacketCount = 0;
        size = 0;

        pPub = GetCurrentMessageToPublish(pM);
        if ((pPub != NULL) && (pPub->dataLength < (pM->bufferSize - size)))
        {          
            numWritten = MqttPackPublishHelper(pM, pPub, size);
            if (numWritten > 0)
            {
                size += numWritten;
                mqttPackets[mqttPacketCount] = PUBLISH;
                mqttPacketCount++;
            }
        }

        if (mqttPacketCount > 0)
        {
            MqttSendPktHelper(pM, size, mqttPackets, mqttPacketCount);
            nextState = E_MQTT_DO_PUBLISH;
        }
        else
        {
            nextState = E_MQTT_WAIT_PKT;
        }

        break;
 
    case E_MQTT_DISCONNECT:
        if(pM->pSessionResults->receivedCounts[PUBACK] != pM->pubAcksExpected)
        {                 
            MqttSetError(pM, MQTT_ERROR_PUBLISH_PUBACK_MISMATCH);
        }
        MqttManagerCleanupHelper(pM);
        nextState = E_MQTT_DONE_OK;
        break;

    case E_MQTT_TIMED_OUT:
        DebugTrace("E_MQTT_TIMEOUT - Socket timeout\n");        
        MqttSetError(pM, MQTT_ERROR_SOCKET_TIMEOUT);
        MqttManagerCleanupHelper(pM);
        nextState = E_MQTT_DONE_ERROR;
        break;

    case E_MQTT_DONE_ERROR:        
        MqttManagerBindToTransport(pM, NULL);
        nextState = E_MQTT_DONE_ERROR;
        r = E_ERROR;    
        break;

    case E_MQTT_DONE_OK:        
        MqttManagerBindToTransport(pM, NULL);
        nextState = E_MQTT_DONE_OK;
        r = E_DONE_OK;
        break;

    case E_MQTT_UNINITIALISED_ERROR:
    default:
        ASSERT(0);
        break;
    } // switch state

    if (nextState != pM->state)
    {
        MqttDebugStateChange(pM->state, nextState);
        pM->state = nextState;
    }

    return r;
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**              @return None
**  
**  @brief
**         Description: Helper called at end (ok or otherwise) of session
**
******************************************************************************/
static void  MqttManagerCleanupHelper (MQTT_MANAGER* pM)
{    
    ASSERT(NULL != pM);

    MqttSendDisconnectHelper(pM);  
    pM->pSessionResults->sessionExecutionTimeMs = pM->timeElapsedMs;  

    DebugTrace("MQTT ErrorState = %u %s", pM->errorState, pM->errorState ? "" : "(SUCCESS)");
    MqttDebugSessionResults(pM->pSessionResults);      
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**              @return The next state
**  
**  @brief
**         Description: Helper to wait for an incoming packet
** @note. This is using 2 timeouts. Whichever comes **earliest** will cause 
** the connection to close. Mostly in the case of sockets on a PC simulator,
** this will be the TCP timeout. See SocketWin.c
******************************************************************************/
static E_MQTTM_STATE MqttManagerPacketWaitHelper(MQTT_MANAGER* pM )
{
    E_MQTTM_STATE s = E_MQTT_UNINITIALISED_ERROR;
    int32_t pktRxType = 0;
        
    ASSERT(NULL != pM);

    if (MqttSessionCanClose(pM))
    {
        DebugTrace("SUCCESS Expected number of Pub Msgs (=%u) received, and nothing left to send\n",
            pM->pSessionResults->receivedCounts[PUBLISH]);

        s = E_MQTT_DISCONNECT;
    }
    else if (NULL != GetCurrentMessageToPublish(pM))
    {
        // Restart sending Pub(s) if more have been added to queue 
        // during session (i.e from within a PubRx callback)
        s = E_MQTT_DO_PUBLISH;
    }
    else
    {
        pktRxType = MQTTPacket_readnb(pM->buffer, pM->bufferSize, &(pM->mqttTransport)); 

        switch (pktRxType)
        {
        case 0: // No packet yet... (only with non-blocking comms)
            {
                if (pM->timeElapsedMs >= pM->pSessionParams->timeoutForWaitMs)
                {
                    // see note in function header
                    DebugTrace("Normal TIMEOUT waiting for msgs in %ums\n", 
                        pM->timeElapsedMs);

                    MqtDebugAtTimeout(pM);

                    s = E_MQTT_DISCONNECT;
                }
                else
                {
                    s = E_MQTT_WAIT_PKT; 
                }
            } 
            break;
            
        case -1: // socket timed out - see note in function header                        
            MqtDebugAtTimeout(pM);
            s = E_MQTT_TIMED_OUT; 
            break;

        default: // normal packets
            s = MqttManagerPacketReceiveHelper(pM, pktRxType);
            break;
        }
    }

    return s;
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**           @param[in] pRxType Type of the message - See msgTypes enum in MQTTPacket.h
**              @return The next state
**  
**  @brief
**         Description: Helper to handle an incoming  MQTT Packet
**
******************************************************************************/
static E_MQTTM_STATE MqttManagerPacketReceiveHelper(MQTT_MANAGER* pM, int32_t pktRxType)
{
    E_MQTTM_STATE s                 = E_MQTT_DONE_ERROR;
    int32_t       rc                = 0;
    uint8_t       connack_rc;
    uint16_t      submsgid;
    int32_t       subcount;
    int32_t       grantedQos;
    int32_t       bytesReceived     = 0;
    const char*   pktStr            = "";
 
    ASSERT(NULL != pM);

    if ((pktRxType >= CONNECT) && (pktRxType <= DISCONNECT))
    {    
        bytesReceived = pM->mqttTransport.len;
        pM->pSessionResults->receivedBytes += bytesReceived;        
        pM->pSessionResults->receivedCounts[pktRxType]++;

        pktStr = MqttDebugGetPktString(pktRxType);
        DebugTrace("RECVD %s (%d bytes)\n", pktStr, bytesReceived);
    }
    else
    {
        ASSERT(0);
    }

    switch(pktRxType)
    {
    case CONNACK:
        rc = MQTTDeserialize_connack(&(pM->sessionPresent), &connack_rc, pM->buffer, pM->bufferSize); //!= 1 || connack_rc != 0) 
        if ((1 != rc) || (connack_rc != 0))
        {
            DebugTrace("ERROR Unable to connect, return code %d\n", connack_rc);
            s = E_MQTT_DONE_ERROR;
        }
        else
        {
            DebugTrace("CONNACK Rx'd; SessionPresent = %u\n", pM->sessionPresent);
            // If session present, then we don't need to send any subs, as the server is already aware.
            s = (pM->sessionPresent) ? E_MQTT_DO_PUBLISH : E_MQTT_DO_SUBSCRIBE;
        }

        break;   

    case SUBACK:
        rc = MQTTDeserialize_suback(&submsgid, 1, &subcount, &grantedQos, pM->buffer, pM->bufferSize);

        pM->currentSubscription++;

        if ((uint32_t)grantedQos <= (uint32_t)E_MQTT_QOS_2)
        {
            pM->grantedQoS = (E_MQTT_QOS)grantedQos;
            DebugTrace("Granted QoS = %u\n", (uint32_t)pM->grantedQoS); 
        }

        s = (GetCurrentSubscription(pM) == NULL) ? 
                E_MQTT_DO_PUBLISH : E_MQTT_DO_SUBSCRIBE;
        break;

    case PUBLISH: /*RECEIVE*/
        MqttReceivePublishHelper(pM);
        s = E_MQTT_WAIT_PKT;
        break;

    case PUBACK:
        s = MqttReceivePubackHelper(pM);
        break;
      

        // Continue processing incoming packets, ignore unhandled types.
    default:
        s = E_MQTT_WAIT_PKT;
        break;
    }

    return s;
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**              @return Current buffer write position
**  
**  @brief
**         Description: Helper to send a CONNECT packet
**
******************************************************************************/
static int32_t MqttPackConnectHelper(MQTT_MANAGER* pM)
{
    int32_t bytesSize = 0;
    
    ASSERT(NULL != pM);

	DebugTrace("CONNECT called");
    bytesSize = MQTTSerialize_connect(pM->buffer, 
        pM->bufferSize, 
        &(pM->mqttConnectData));

	DebugTrace("Bytes = %u, BufSize = %u", bytesSize, pM->bufferSize);

    return bytesSize;
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**           @param[in] pSub The Sub to send
**              @return None
**  
**  @brief
**         Description: Helper to send a SUBSCRIBE packet
**
******************************************************************************/
static void MqttSendSubscribeHelper(MQTT_MANAGER* pM, const MQTT_MANAGER_SUBSCRIPTION* pSub)
{
    int32_t bytesToSend = 0;
    MQTTString topic = MQTTString_initializer;
    uint16_t msgid;
    uint8_t packets[] = {SUBSCRIBE};
    
    ASSERT(NULL != pM);
    ASSERT(NULL != pSub);

	topic.cstring = pSub->topicFilter; 
    
    msgid = MqttGetNextPacketId(pM);
 
	MqttDebugSubscribe(pSub);

	bytesToSend = MQTTSerialize_subscribe(pM->buffer, 
        pM->bufferSize,
        0, 
        msgid,
        1, 
        &topic, 
        (int32_t*)&(pSub->qos));
    
    MqttSendPktHelper(pM, bytesToSend, packets, 1);
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**           @param[in] pPub The Pub to send
**           @param[in] bufferWritePosition The position to start writing to the buffer
**              @return Number bytes written in buffer, 0 if error (won't fit)
**  
**  @brief
**         Description: Helper to send a PUBLISH packet
**
******************************************************************************/
static int32_t MqttPackPublishHelper(MQTT_MANAGER* pM, 
    const MQTT_MANAGER_PUB* pPub, 
    int32_t bufferWritePosition)
{
    int32_t  bytesSize = 0;
    MQTTString topicString = MQTTString_initializer;
    MQTT_MANAGER_PUB pubCopy = *pPub; // Make a mutable copy for filling in packetId etc.

    ASSERT(NULL != pM);
    ASSERT(NULL != pPub);
    ASSERT(NULL != pPub->topic);

    pM->currentSendPub++;

    // QoS 1 or 2 expects a unique ID (QoS0 has no ID field in pkt).  
    // Use 0 to populate results struct when QoS = 0.
    pubCopy.packetId = (pubCopy.qos >= E_MQTT_QOS_1) ? MqttGetNextPacketId(pM) : 0; 

    // Optional callback.
    // If used, then set the Pubs data pointer to point to your local populated area, and return
    // the number of bytes pointed to.
    if (NULL != pubCopy.onPopulate)
    {
        pubCopy.dataLength = pubCopy.onPopulate(&pubCopy);
    }

    MqttDebugPublishSend(&pubCopy);

    // Send, if we have anything
    if ((NULL != pubCopy.pData) && (pubCopy.dataLength > 0))
    {
        // >= QoS 1 expects a PUBACK
        if (E_MQTT_QOS_1 <= pubCopy.qos)
        {
            pM->pubAcksExpected++;
        }    

        topicString.cstring = (char*)pubCopy.topic;

        bytesSize = MQTTSerialize_publish((pM->buffer) + bufferWritePosition,
            (pM->bufferSize) - bufferWritePosition,
            0, 
            pubCopy.qos, 
            0, 
            pubCopy.packetId,
            topicString, 
            (unsigned char*)pubCopy.pData,
            pubCopy.dataLength);	

        // Successfully serialised?
        if (bytesSize > 0)
        {
            MqttMarkSentPublish(pM, pM->currentSendPub, pubCopy.packetId);
        }
        else
        {
            // >= QoS 1 expects a PUBACK, but we're not adding this, so
            // we need to undo what we did earlier.
            if (E_MQTT_QOS_1 <= pubCopy.qos)
            {
                pM->pubAcksExpected--;
            }
        }

    }

    // +ve if OK, 0 on error
    return (bytesSize > 0) ? bytesSize : 0;
}


/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**           @param[in] currentSendPub. The number (starting at 1) of the current pub being sent
**           @param[in] packetId. The pktId expected from the eventual Puback
**              @return None
**  
**  @brief
**         Description: Helper to store the packetId on send, 
** so we can match a PUBACK to it. This is used so we know if the sent Pub gets an Ack.
******************************************************************************/
static void MqttMarkSentPublish(MQTT_MANAGER* pM,uint32_t currentSendPub, uint16_t packetId)
{
    ASSERT(pM->currentSendPub >= 1);
    ASSERT(pM->currentSendPub <= MQTT_MAX_PUBS_TO_SEND);

    pM->pSessionResults->pubResults[currentSendPub - 1].packetId = packetId;
    pM->pSessionResults->pubResults[currentSendPub - 1].pubState = PUB_STATE_SENT;
}


/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**           @param[in] msgId The ID of the received PUB that we are acking
**              @return None
**  
**  @brief
**         Description: Helper to send a PUBACK packet
**
******************************************************************************/
static void MqttSendPubackHelper(MQTT_MANAGER* pM, uint16_t msgId)
{
    int32_t bytesToSend = 0;
    uint8_t packets[] = {PUBACK};
        
    ASSERT(NULL != pM);

    bytesToSend = MQTTSerialize_ack(pM->buffer, pM->bufferSize, PUBACK, 0, msgId);
   
    MqttSendPktHelper(pM, bytesToSend, packets, 1);
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**              @return None
**  
**  @brief
**         Description: Helper to send a DISCONNECT packet
**
******************************************************************************/
static void MqttSendDisconnectHelper(MQTT_MANAGER* pM)
{
    int32_t bytesToSend = 0;
    uint8_t packets[] = {DISCONNECT};
        
    ASSERT(NULL != pM);

    bytesToSend = MQTTSerialize_disconnect(pM->buffer, pM->bufferSize);
   
    MqttSendPktHelper(pM, bytesToSend, packets, 1);
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**              @return The current Sub Msg (null if none)
**  
**  @brief
**         Description: Helper to get the current sub to send
**
******************************************************************************/
static const MQTT_MANAGER_SUBSCRIPTION* GetCurrentSubscription(const MQTT_MANAGER* pM)
{
    const MQTT_MANAGER_SUBSCRIPTION* pSub = NULL;
        
    ASSERT(NULL != pM);

    if (pM->currentSubscription < pM->pSessionParams->subscriptionCount)
    {
        pSub = pM->pSessionParams->subscriptions[pM->currentSubscription];
    }

    return pSub;
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**              @return The current Pub Msg (null if none)
**  
**  @brief
**         Description: Helper to get the current pub to send
**
******************************************************************************/
static const MQTT_MANAGER_PUB* GetCurrentMessageToPublish(const MQTT_MANAGER* pM)
{
    const MQTT_MANAGER_PUB* pPub = NULL;
        
    ASSERT(NULL != pM);

    if (pM->currentSendPub < pM->pSessionParams->pubToSendCount)
    {
        pPub = pM->pSessionParams->pubs[pM->currentSendPub];
    }

    return pPub;
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**              @return 1 if OK
**  
**  @brief
**         Description: Helper to receive a PUBLISH packet
**
******************************************************************************/
static int32_t MqttReceivePublishHelper(MQTT_MANAGER* pM)
{
    int32_t numToCopy;
    int32_t rc;
    MQTTString receivedTopic;
    MQTT_MANAGER_PUB msg;
    char topicBuf[MQTT_MAX_TOPIC_STRING];

    ASSERT(NULL != pM);

    rc = MQTTDeserialize_publish((unsigned char*)&msg.dup, 
        (int*)&msg.qos, 
        (unsigned char*)&msg.retained, 
        (unsigned short*)&msg.packetId, 
        &receivedTopic,
        (unsigned char**)&msg.pData, 
        (int*)&msg.dataLength, 
        pM->buffer, 
        pM->bufferSize);

    if (1 == rc)
    {
        // Ack the received Publish msg if QoS is 1
        if (E_MQTT_QOS_1 == msg.qos)
        {
             MqttSendPubackHelper(pM, msg.packetId);
        }

        // Need to make a copy of the topic string 
        // so it can be referenced in the callback
        numToCopy = sizeof(topicBuf);
        if (receivedTopic.lenstring.len < numToCopy)
        {
            numToCopy = receivedTopic.lenstring.len;
        }
        memset(topicBuf, 0, sizeof(topicBuf));
        memcpy(topicBuf, receivedTopic.lenstring.data, numToCopy);
        msg.topic = topicBuf;

        MqttDebugPublishReceive(&msg);

        if (NULL != pM->pfOnPublishRxCallback)
        {
            pM->pfOnPublishRxCallback(&msg);
        }
    }

    return rc;
}


/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**              @return 1 if OK
**  
**  @brief
**         Description: Helper to receive a PUBACK packet
**
******************************************************************************/
static E_MQTTM_STATE MqttReceivePubackHelper(MQTT_MANAGER* pM)
{
    E_MQTTM_STATE s = E_MQTT_WAIT_PKT;
    int32_t rc;
    unsigned char  packettype = 0; 
    unsigned char  dup = 0;
    unsigned short packetId = 0;
    int i;
    ASSERT(NULL != pM); 

    rc = MQTTDeserialize_ack(&packettype, &dup, &packetId, pM->buffer, pM->bufferSize);
     
    if (1 != rc) 
    {
        DebugTrace("PUBACK ERROR  %d\n", rc);
    }
    else
    {
        DebugTrace("PUBACK Rx'd; packetId = %u\n", packetId);

        // Search the results for the sent pub having a matching ID. Mark as ack'd.
        for (i = 0; i < pM->pSessionParams->pubToSendCount; i++)
        {
            if (pM->pSessionResults->pubResults[i].packetId == packetId)
            {
                pM->pSessionResults->pubResults[i].pubState = PUB_STATE_ACKED;

                if (NULL != pM->pfnOnPuback)
                {
                    pM->pfnOnPuback(pM);
                    //The state should be updated by the On Puback function
                    s = pM->state;
                }
                break;
            }
        }
    }

    return s;
}


/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**              @return true if no more pubs or subs to send, and the expected number 
**                      of PUBLISH msgs has been received. 
**  
**  @brief
**         Description: Helper to determine if session is finished.
**
******************************************************************************/
static bool MqttSessionCanClose(const MQTT_MANAGER* pM)
{
    bool canClose = false;
    bool noPubsToSend;
    bool noSubsToSend;
    ASSERT(NULL != pM);

    noPubsToSend = (NULL == GetCurrentMessageToPublish(pM));
 
    // If session present, then we don't need to send any subs, as the server is already aware. 
    noSubsToSend = (pM->sessionPresent) ? true : (NULL == GetCurrentSubscription(pM));

    canClose = 
         (noSubsToSend &&
          noPubsToSend &&
         (pM->pSessionResults->receivedCounts[PUBLISH] >= pM->pSessionParams->expectedPubsToReceive) &&
         (pM->pSessionResults->receivedCounts[PUBACK ] >= pM->pubAcksExpected));
     
    return canClose;
}


/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**      @param[in, out] pTm The Transport Manager object
**              @return None
**  
**  @brief
**         Description: Helper to bind MQTT Manager to an established connection
**
******************************************************************************/
static void MqttManagerBindToTransport(MQTT_MANAGER* pM, TRANSPORT_MANAGER* pTm)
{
    ASSERT(NULL != pM);

    pM->pTm = pTm;
}


/******************************************************************************
**    
**               @param vp Unused (required by Paho code)
**          @param[out] buf Data received 
**           @param[in] count Max size of buffer
**              @return 0 if no data, -ve if error, else number of bytes copied into buf.
**  
**  @brief
**         Description: Wrapper function  for MQTT API. Refer to Paho demo code
**
******************************************************************************/
static int t_recv_nb(void* vp, unsigned char* buf, int count)
{
    TRANSPORT_MANAGER* pTm = (TRANSPORT_MANAGER*)vp;
    return TransportManagerReceive(pTm, buf, count);
}


/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**           @param[in] bytesToSend The number of bytes
**           @param[in] pktType The packet type e.g. CONNECT etc
**              @return None
**  
**  @brief
**         Description: Helper to send a packet
**
******************************************************************************/
static void MqttSendPktHelper(MQTT_MANAGER* pM, int32_t bytesToSend, uint8_t pktTypes[], uint8_t numberPackets)
{
    int32_t bytesSent = 0;
    uint8_t i = 0;
    const char* s = "";

    ASSERT(NULL != pM);

    if (pktTypes[0] <= DISCONNECT)
    {   
        if (bytesToSend > 0)
        {
            bytesSent =  TransportManagerSend(pM->pTm, pM->buffer, bytesToSend);

            if (bytesSent > 0)
            {
                pM->pSessionResults->sentBytes += bytesSent;
                for(i=0; i<numberPackets; i++)
                {
                    pM->pSessionResults->sentCounts[pktTypes[i]]++;
                    s = MqttDebugGetPktString(pktTypes[i]);
                    DebugTrace("SENT %s \n", s);

                }
                DebugTrace("TOTAL %d bytes \n", bytesSent);
            }
            else
            {
                MqttSetError(pM, MQTT_ERROR_SOCKET_SEND);
            }
        }
        else
        {
            // Nothing to send or serialisation function failed.
            MqttSetError(pM, MQTT_ERROR_SERIALISATION);
        }
    }
    else
    {
        ASSERT(0);
    }
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**              @return The ID, incremented each time this function is called 1 to 65535
**  
**  @brief
**         Description: Helper to get a packet ID 
**
******************************************************************************/
static uint16_t MqttGetNextPacketId(MQTT_MANAGER* pM) 
{
    pM->nextPacketId++;

    if (pM->nextPacketId >= MQTT_MAX_PACKET_ID)
    {
        pM->nextPacketId = 1;
    }

    return pM->nextPacketId;
}

/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**           @param[in] mqttError The Error Code
**              @return None
**  
**  @brief
**         Description: Helper to set error code
**
******************************************************************************/
static void MqttSetError(MQTT_MANAGER* pM, MQTT_ERROR mqttError)
{
    ASSERT(NULL != pM);

    if (MQTT_OK == pM->errorState)
    {
        DebugTrace("MQTT ERROR CODE SET = %d\n", mqttError);
        pM->errorState = mqttError;
    }
}


/******************************************************************************
**    
**      @param[in] pM The MQTT Manager object
**      @return The error state on completion of the session
**  @brief
**         Description: Helper to get state of the session after completion
**
******************************************************************************/
DLL_EXPORT
    MQTT_ERROR MqttManagerGetErrorState(const MQTT_MANAGER* pM)
{
    ASSERT(NULL != pM);
    return pM->errorState;
}


/******************************************************************************
**    
**      @param[in, out] pM The MQTT Manager object
**      @param[in, out] pTm The Transport
**              @return 0 if OK, -1 if error
**  
**  @brief
**         Description: Prepare the session state machine (begin session)
**
******************************************************************************/
DLL_EXPORT int32_t    
    MqttManagerBegin(MQTT_MANAGER* pM, 
    TRANSPORT_MANAGER* pTm )
{
    ASSERT(NULL != pTm);
    ASSERT(NULL != pM->pSessionParams);
    ASSERT(NULL != pM->pSessionResults);

    // No comms link / results buffer. Cannot run protocol.
    if (NULL == pTm)
    {
        return -1;
    }    
    
    MqttManagerBindToTransport(pM, pTm);

    pM->mqttConnectData.clientID.cstring    = pM->pSessionParams->clientID;
    pM->mqttConnectData.MQTTVersion         = pM->pSessionParams->mqttVersion;
    pM->mqttConnectData.keepAliveInterval   = pM->pSessionParams->keepAliveInterval;
    pM->mqttConnectData.cleansession        = pM->pSessionParams->cleansession;
    pM->mqttConnectData.username.cstring    = pM->pSessionParams->username;
    pM->mqttConnectData.password.cstring    = pM->pSessionParams->password;

    // workaround for MQTT API 
    pM->mqttTransport.sck                   = (void*)pTm;
    pM->mqttTransport.getfn                 = t_recv_nb;
    pM->mqttTransport.state                 = 0;    
    
    pM->sessionPresent                      = 0;
    pM->grantedQoS                          = E_MQTT_QOS_0;
    pM->currentSubscription                 = 0;
    pM->currentSendPub                      = 0;
    pM->pubAcksExpected                     = 0;
 
    MqttManagerInitSessionResults(pM->pSessionResults);

    pM->timeElapsedMs                       = 0;
    pM->state                               = E_MQTT_BEGIN; 

    pM->errorState                          = MQTT_OK;
    return 0;
}



/******************************************************************************
**    
**      @param[in] qos. Packet QoS
**      @param[in] topicName. Null term string
**      @param[in] payloadLength. Num bytes in payload
**      @return The number of bytes needed to serialise. 
**  
**  @brief
**         Description: Helper to calc the num of bytes a Pub would be serialised to
**
******************************************************************************/
DLL_EXPORT uint32_t  MqttManagerCalculatePublishLength(
    E_MQTT_QOS qos, 
    const char* topicName, 
    uint32_t payloadLength)
{
    uint32_t numBytes;
    uint32_t contentLen;
    
    ASSERT(NULL != topicName);
    ASSERT(qos <= E_MQTT_QOS_2);
    ASSERT(payloadLength <= 32767ul); // trap truly egregious faults

    // Calc num of bytes for topic name and payload first.
    contentLen  = 2 + strlen(topicName); // 2 for topic string length (like a Pascal string)
    contentLen += payloadLength;

    // Then add the num of bytes for header (variable length encoding of length field)
    numBytes = MQTTPacket_len(contentLen);

    // QoS > 0 have a packetid as well. 
    if (qos > E_MQTT_QOS_0)
    {
		numBytes += 2; 
    }

    return numBytes;
}


/******************************************************************************
**    
**      @param[in] pSessionResults.  The results at end of session
**      @param[in] pubIndex The index 0 based of the pub to be queried 
**      @return True if a corresponding PUBACK (having same packetId) was received
**  
**  @brief
**        Note: Will return false if the Pub was sent with QoS=0, since they are
** not acknowledged. 
******************************************************************************/
bool MqttManagerWasPublishDelivered(const MQTT_MANAGER_SESSION_RESULTS* pSessionResults,
    uint32_t pubIndex)
{
    if (pubIndex >= MQTT_MAX_PUBS_TO_SEND) return false;

    return (bool)(PUB_STATE_ACKED == pSessionResults->pubResults[pubIndex].pubState);
}

/**
 * @}
 */
