﻿

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;


//
// Note. To enable debugging DLL, in the projects Debug settings, Configuration Debug, Enable unmanaged code debugging. 
//

namespace TsmTestHarness
{
    partial class TsmTestHarness
    {
        int oldIsBusy = 0;
        bool hasFinishedSession = false;
        Byte[] uplinkDataBuffer = new Byte[8192];
        Byte[] rxData = new Byte[8192];
        Byte[] debugDataBuffer = new byte[8192];


        //
        // Main init and polling
        //

        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int MqttTick(int tickIntervalMs);

        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int MqttBeginSessionForCSharp(int n);

        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int HttpBeginSession(int dummy);

        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int FileDownloaderBeginSession(string server, int blockSize, string filename);

        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int FileDownloaderCancelSession(int dummy);
    
        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt32 FileDownloaderGetProgress(int dummy);

        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern int MqttIsBusy(); 


        // Set Pointer to the message  rx buffers
        // Call this to tell the DLL where to write the incoming PUBs (up to 4)
        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern int MqttSetReceiveBuffer(UInt32 index, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] Byte[] rxData, int rxSize);

        // Set Pointer to the message to send
        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern int MqttSetTransmitBuffer([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] Byte[] txData, int txCount);
            
        // Set Topic string (as byte array)
        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern int TsmSetTopicString([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] Byte[] topicStringBytes, int n);


        // Get num of PUBS received
        // This returns the number of PUBs received at the end of a session
        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern UInt32 MqttGetReceivedPublishCount();


        // Get num of bytes received for a given received PUB
        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern int DllGetReceiveCount(UInt32 index);




        // HTTP Server applet
        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern void HttpServerTestAppletInit();
        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern void HttpServerTestAppletTick(int tickIntervalMs);
        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern void HttpServerTestGenerateRequest(
    [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] Byte[] url,
    [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] Byte[] hostName,
    UInt32 firstRangeByte,
    UInt32 lastRangeByte);

        // TRACE debug data
        //
        [DllImport("TsmLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public unsafe static extern UInt32 DebugTraceGetChars([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] Byte[] txData,
            int sizeofTx);


        public bool IsBusy { get; private set; }

        /// <summary>
        /// Data exchange to/from the DLL
        /// </summary>
        void TickedDataTransfer()
        {
            UInt32 uplinkByteCount = 0;  // count of data to send  
            unsafe
            {
                //
                // trace stream
                //
                uplinkByteCount = DebugTraceGetChars(uplinkDataBuffer, uplinkDataBuffer.GetLength(0));
                if (uplinkByteCount > 0)
                {
                    traceReceiver.OnData(uplinkDataBuffer, (UInt32)uplinkByteCount);
                }

                // Update the BUSY indicator
                int isBusy = MqttIsBusy();
                if (isBusy != oldIsBusy)
                {
                    if (0 == isBusy)
                    {
                        hasFinishedSession = true;
                    }
                    oldIsBusy = isBusy;
                    labelIsBusy.BackColor = (isBusy > 0) ? Color.Coral : Color.LightGray;
                    labelIsBusy.Text = (isBusy > 0) ? "BUSY" : "READY";
                    this.IsBusy = isBusy != 0;
                }


                UInt32 percent = FileDownloaderGetProgress(0);
                labelFilePercentComplete.Text = percent.ToString() + "%";

                if (hasFinishedSession)
                {
                    hasFinishedSession = false;

                    UInt32 pubsRecvd = MqttGetReceivedPublishCount();

                   // LogTrace("pubsRecvd....:" + pubsRecvd.ToString());

                int rxC = 0;// DllGetReceiveCount(0);
                    if (rxC > 0)
                    {
                        LogTrace("Rx....:" + rxC.ToString());
                //        var str = System.Text.Encoding.ASCII.GetString(receiverArray0);
                 //       LogTrace(str);
                    }
                }
            }
        }
    }


}