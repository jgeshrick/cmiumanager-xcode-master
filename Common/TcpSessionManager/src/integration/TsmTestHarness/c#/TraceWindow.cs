﻿


using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;

namespace TsmTestHarness
{
    /// <summary>
    /// Move clutter away from main app's file
    /// </summary>
    public partial class TsmTestHarness : Form
    {
        /// <summary>
        /// Enables colored lines to be used in log windows
        /// </summary>
        public class MyListBoxItem
        {
            public MyListBoxItem(Color c, string m)
            {
                ItemColor = c;
                Message = m;
            }
            public Color ItemColor { get; set; }
            public string Message { get; set; }
        }


        /// <summary>
        /// Add string s to the TRACE Log box
        /// </summary>
        /// <param name="s"></param>
        public void LogTrace(string s)
        {
            if (s.Length > 0)
            {
                DateTime dt = DateTime.Now;
                String sLine = dt.ToLongTimeString() + "."+ dt.Millisecond.ToString("000") +" " + s;

                LogToListbox(listBoxTrace, sLine);

                Console.WriteLine(sLine);
            }
        }

        /// <summary>
        /// Add string s to the given Log box, highlight errors etc
        /// </summary>
        /// <param name="s"></param>
        private void LogToListbox(ListBox lb, string s)
        {
            Color colour = Color.Cornsilk;

            if (lb == this.listBoxTrace)
            {
                bool suppress = ShowErrorsCheckBox.Checked;
                if (s.ToUpper().Contains("TIMEOUT"))
                {
                    colour = Color.OrangeRed;
                    suppress = false;
                }
                if (s.ToUpper().Contains("SUCCESS")) colour = Color.Lime;
                if (s.ToUpper().Contains("ERROR"))
                {
                    colour = Color.OrangeRed;
                    suppress = false;
                }
                if (!suppress)
                {

                    // Default RX colour
                    if (s.ToUpper().Contains("RX"))
                    {
                        colour = Color.LightSteelBlue;
                    }
                    if (s.ToUpper().Contains("RECV ")) colour = Color.LightSteelBlue;

                    // Default TX colour
                    if (s.ToUpper().Contains("TX ")) colour = Color.Khaki;
                    if (s.ToUpper().Contains("SEND ")) colour = Color.Khaki;
                    if (s.ToUpper().Contains("GET /")) colour = Color.Aqua;

                    if (s.ToUpper().Contains("RECV PUBLISH")) colour = Color.LightBlue; //.Lavender; //.LightSteelBlue;
                    if (s.ToUpper().Contains("SUBSCRIBE")) colour = Color.Plum;
                    //   if (s.ToUpper().Contains("RX: PUBLISH")) colour = Color.SteelBlue;  
                    if (s.ToUpper().Contains("RX: CONNACK")) colour = Color.PaleGreen;
                    if (s.ToUpper().Contains("DISCONNECT")) colour = Color.LightCoral;

                    if (s.ToUpper().Contains("STATE")) colour = Color.LightGray;
                    if (s.ToUpper().Contains("STATUS:")) colour = Color.LightGray;


                    lb.Items.Add(new MyListBoxItem(colour, s));
                }
            }

            else
            {
                lb.Items.Add(s);
            }

            lb.SelectedIndex = lb.Items.Count - 1;
            lb.SelectedIndex = -1;
        }


        /// <summary>
        /// Puts items in a listbox with specified background colour
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxCustomDrawItem(ListBox lb, object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();

            if (e.Index >= 0 && e.Index < lb.Items.Count)
            {
                MyListBoxItem item = lb.Items[e.Index] as MyListBoxItem;

                e.Graphics.FillRectangle(new SolidBrush(item.ItemColor), e.Bounds);
                if (item != null)
                {
                    e.Graphics.DrawString(
                        item.Message,
                        e.Font,
                        new SolidBrush(Color.Black),
                        new PointF(e.Bounds.X, e.Bounds.Y));
                }
            }
        }


        /// <summary>
        /// Puts items in trace listbox with specified background colour
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxTrace_DrawItem(object sender, DrawItemEventArgs e)
        {
            listBoxCustomDrawItem(listBoxTrace, sender, e);
        }


        /// <summary>
        /// Handler that's called when a trace line is received from DLL
        /// </summary>
        /// <param name="source"></param>
        /// <param name="s"></param>
        void traceReceiver_onLineAvailable(object source, string s)
        {
            if (s.Length == 0) return;
            // Kill empty lines on Trace only
            if (s.Length <= 2)
            {
                if ((s[0] == '\r') || (s[0] == '\n') || (s[0] == '"'))
                {
                    return;
                }
            }
            LogTrace(s);
        }



	}
}