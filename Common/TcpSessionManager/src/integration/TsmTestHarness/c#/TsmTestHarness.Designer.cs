﻿namespace TsmTestHarness
{
    partial class TsmTestHarness
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listBoxTrace = new System.Windows.Forms.ListBox();
            this.timerTick = new System.Windows.Forms.Timer(this.components);
            this.buttonGo = new System.Windows.Forms.Button();
            this.buttonGoModem = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.labelIsBusy = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.labelFilePercentComplete = new System.Windows.Forms.Label();
            this.buttonCancelDownload = new System.Windows.Forms.Button();
            this.ServerLabel = new System.Windows.Forms.Label();
            this.ServerTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BlockSizeTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.RepeatTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.FileTextBox = new System.Windows.Forms.TextBox();
            this.ShowErrorsCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // listBoxTrace
            // 
            this.listBoxTrace.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxTrace.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.listBoxTrace.Font = new System.Drawing.Font("Consolas", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxTrace.FormattingEnabled = true;
            this.listBoxTrace.HorizontalScrollbar = true;
            this.listBoxTrace.Location = new System.Drawing.Point(26, 145);
            this.listBoxTrace.Name = "listBoxTrace";
            this.listBoxTrace.ScrollAlwaysVisible = true;
            this.listBoxTrace.Size = new System.Drawing.Size(653, 420);
            this.listBoxTrace.TabIndex = 57;
            this.listBoxTrace.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.listBoxTrace_DrawItem);
            // 
            // timerTick
            // 
            this.timerTick.Interval = 10;
            this.timerTick.Tick += new System.EventHandler(this.timerTick_Tick);
            // 
            // buttonGo
            // 
            this.buttonGo.Location = new System.Drawing.Point(26, 84);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(113, 23);
            this.buttonGo.TabIndex = 58;
            this.buttonGo.Text = "WINSOCK Go!";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // buttonGoModem
            // 
            this.buttonGoModem.Location = new System.Drawing.Point(173, 84);
            this.buttonGoModem.Name = "buttonGoModem";
            this.buttonGoModem.Size = new System.Drawing.Size(151, 23);
            this.buttonGoModem.TabIndex = 59;
            this.buttonGoModem.Text = "MODEM Go!";
            this.buttonGoModem.UseVisualStyleBackColor = true;
            this.buttonGoModem.Click += new System.EventHandler(this.buttonGoModem_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(621, 110);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(58, 23);
            this.buttonClear.TabIndex = 60;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // labelIsBusy
            // 
            this.labelIsBusy.AutoSize = true;
            this.labelIsBusy.Location = new System.Drawing.Point(23, 120);
            this.labelIsBusy.Name = "labelIsBusy";
            this.labelIsBusy.Size = new System.Drawing.Size(38, 13);
            this.labelIsBusy.TabIndex = 61;
            this.labelIsBusy.Text = "Ready";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(349, 84);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 62;
            this.button1.Text = "HTTP get";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_Http_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(430, 84);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 23);
            this.button2.TabIndex = 63;
            this.button2.Text = "FileDownload";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.buttonFileDownload_Click);
            // 
            // labelFilePercentComplete
            // 
            this.labelFilePercentComplete.AutoSize = true;
            this.labelFilePercentComplete.Location = new System.Drawing.Point(476, 120);
            this.labelFilePercentComplete.Name = "labelFilePercentComplete";
            this.labelFilePercentComplete.Size = new System.Drawing.Size(35, 13);
            this.labelFilePercentComplete.TabIndex = 64;
            this.labelFilePercentComplete.Text = "label1";
            // 
            // buttonCancelDownload
            // 
            this.buttonCancelDownload.Location = new System.Drawing.Point(526, 84);
            this.buttonCancelDownload.Name = "buttonCancelDownload";
            this.buttonCancelDownload.Size = new System.Drawing.Size(98, 23);
            this.buttonCancelDownload.TabIndex = 65;
            this.buttonCancelDownload.Text = "Cancel D/L";
            this.buttonCancelDownload.UseVisualStyleBackColor = true;
            this.buttonCancelDownload.Click += new System.EventHandler(this.buttonCancelDownload_Click);
            // 
            // ServerLabel
            // 
            this.ServerLabel.AutoSize = true;
            this.ServerLabel.Location = new System.Drawing.Point(26, 13);
            this.ServerLabel.Name = "ServerLabel";
            this.ServerLabel.Size = new System.Drawing.Size(38, 13);
            this.ServerLabel.TabIndex = 66;
            this.ServerLabel.Text = "Server";
            // 
            // ServerTextBox
            // 
            this.ServerTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ServerTextBox.Location = new System.Drawing.Point(70, 10);
            this.ServerTextBox.MaxLength = 128;
            this.ServerTextBox.Name = "ServerTextBox";
            this.ServerTextBox.Size = new System.Drawing.Size(268, 20);
            this.ServerTextBox.TabIndex = 67;
            this.ServerTextBox.Text = "52.27.80.45";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(356, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 68;
            this.label1.Text = "Block size";
            // 
            // BlockSizeTextBox
            // 
            this.BlockSizeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BlockSizeTextBox.Location = new System.Drawing.Point(417, 11);
            this.BlockSizeTextBox.MaxLength = 6;
            this.BlockSizeTextBox.Name = "BlockSizeTextBox";
            this.BlockSizeTextBox.Size = new System.Drawing.Size(63, 20);
            this.BlockSizeTextBox.TabIndex = 69;
            this.BlockSizeTextBox.Text = "256";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(486, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 70;
            this.label2.Text = "Repeat";
            // 
            // RepeatTextBox
            // 
            this.RepeatTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RepeatTextBox.Location = new System.Drawing.Point(534, 11);
            this.RepeatTextBox.MaxLength = 4;
            this.RepeatTextBox.Name = "RepeatTextBox";
            this.RepeatTextBox.Size = new System.Drawing.Size(44, 20);
            this.RepeatTextBox.TabIndex = 71;
            this.RepeatTextBox.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 72;
            this.label3.Text = "File";
            // 
            // FileTextBox
            // 
            this.FileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileTextBox.Location = new System.Drawing.Point(70, 37);
            this.FileTextBox.MaxLength = 256;
            this.FileTextBox.Name = "FileTextBox";
            this.FileTextBox.Size = new System.Drawing.Size(609, 20);
            this.FileTextBox.TabIndex = 73;
            this.FileTextBox.Text = "/image-distribution/CmiuApplication/CmiuApplication_v0.4.150805.161.bin";
            // 
            // ShowErrorsCheckBox
            // 
            this.ShowErrorsCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ShowErrorsCheckBox.AutoSize = true;
            this.ShowErrorsCheckBox.Location = new System.Drawing.Point(584, 12);
            this.ShowErrorsCheckBox.Name = "ShowErrorsCheckBox";
            this.ShowErrorsCheckBox.Size = new System.Drawing.Size(95, 17);
            this.ShowErrorsCheckBox.TabIndex = 74;
            this.ShowErrorsCheckBox.Text = "Log errors only";
            this.ShowErrorsCheckBox.UseVisualStyleBackColor = true;
            // 
            // TsmTestHarness
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(719, 595);
            this.Controls.Add(this.ShowErrorsCheckBox);
            this.Controls.Add(this.FileTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RepeatTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BlockSizeTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ServerTextBox);
            this.Controls.Add(this.ServerLabel);
            this.Controls.Add(this.buttonCancelDownload);
            this.Controls.Add(this.labelFilePercentComplete);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelIsBusy);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonGoModem);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.listBoxTrace);
            this.MaximumSize = new System.Drawing.Size(1600, 1200);
            this.Name = "TsmTestHarness";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "TSM test harness";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxTrace;
        private System.Windows.Forms.Timer timerTick;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.Button buttonGoModem;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Label labelIsBusy;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label labelFilePercentComplete;
        private System.Windows.Forms.Button buttonCancelDownload;
        private System.Windows.Forms.Label ServerLabel;
        private System.Windows.Forms.TextBox ServerTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox BlockSizeTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox RepeatTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox FileTextBox;
        private System.Windows.Forms.CheckBox ShowErrorsCheckBox;
    }
}

