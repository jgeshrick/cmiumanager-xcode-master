﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data; 
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;



namespace TsmTestHarness
{
    public delegate void OnLineEventHandler(object source, String s);

    /// <summary>
    /// This helper class assembles incoming strings from DLL (which may be fragmented)
    /// If a whole line (is cr or null terminated) is received, an event fires with the
    /// assembled string.
    /// The event handler can then choose to print it into a text box / file etc
    /// </summary>
    public class LineReceiver
    {
        public event OnLineEventHandler onLineAvailable;
        const UInt32 BUF_SIZE = 1024;
        char[] lineBuffer;
        UInt32 index;

        public LineReceiver()
        {
            index = 0;
            lineBuffer = new char[BUF_SIZE];
        }

        /// <summary>
        /// This Function is called when raw data has been received via the 
        /// Trace ring buffer
        /// If a null term is found (which may occur anywhere because the strings 
        /// may be fragmented, an event fires.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="count"></param>
        public void OnData(byte[] data, UInt32 count)
        {
            char c;
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < count; i++)
            {
                c = (char)data[i];

                lineBuffer[index++] = c;

                // Null term means a whole line has been received. Or CR
                if ((index >= BUF_SIZE) || (c == '\0') || (c == '\r'))
                {
                    for (int j = 0; j < index - 1; j++)
                    {
                        // replace non-printing chars with nothing (when binary data passes)
                        if (lineBuffer[j] >= ' ' && lineBuffer[j] <= '~')
                        {
                            sb.Append(lineBuffer[j]);
                        }
                        else
                        {
                            // sb.Append('.');
                        }
                    }
                    index = 0;

                    // Fire event
                    if (onLineAvailable != null)
                    {
                        onLineAvailable(this, sb.ToString());
                    }

                    sb.Remove(0, sb.Length); //Clear() not available in this version of .NET 
                }
            }
        }
    }
}