﻿/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @defgroup TsmIntegrationTest
 *
 * @brief Integration Test for the @ref TCP Session Manager
 *
 */

/**
 * @addtogroup Integration Test
 * @{
 */

/**
 * @addtogroup Test 
 * @{
 */

/**
 * @file
 *
 * @brief Windows test app for the @ref TCP SessionManager (Tsm)
 * @author Duncan Willis
 * @date 2015.03.05  
 * @version 1.0
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.Drawing;


namespace TsmTestHarness
{
    public partial class TsmTestHarness : Form
    {
        public TsmTestHarness()
        {
            InitializeComponent();
        }


        byte[] receiverArray0 = new byte[4096];
        byte[] receiverArray1 = new byte[4096];
        byte[] receiverArray2 = new byte[4096];
        byte[] receiverArray3 = new byte[4096];
        byte[] TxArray = new byte[4096];
        static byte[] topicStringBytes = new byte[256];

        const string WindowTitle = "MQTT DLL Test  Harness in C";

        private volatile int fileDownloadsRemaining = 0;


       byte[,] f = new byte[4, 4096];

        
        /// <summary>
        /// Version of this application
        /// </summary>
        const string Version = "0.01"; //  

        DateTime dtPrev = DateTime.Now;
        LineReceiver traceReceiver;

         
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load_1(object sender, EventArgs e)
        {
            // Init the system under test
            // (if this fails - check the DLL was generated and is visible)
            timerTick.Enabled = false;

            // Set window's title bar
            this.Text = WindowTitle + " V" + Version;

            timerTick.Interval = 10;
            timerTick.Enabled = true;

            // Install event handler, fired when a whole line (\n terminated) has been received in the 
            // trace buffer.
            traceReceiver = new LineReceiver();
            traceReceiver.onLineAvailable +=new OnLineEventHandler(traceReceiver_onLineAvailable);
                     
            // Set up the TX message buffer
            TxArray = Encoding.ASCII.GetBytes("Greetings from Windows C#"); 
            MqttSetTransmitBuffer(TxArray, TxArray.GetLength(0));

            // Set the app to put its data received here.
            MqttSetReceiveBuffer(0, receiverArray0, 4096);
            MqttSetReceiveBuffer(1, receiverArray1, 4096);
            MqttSetReceiveBuffer(2, receiverArray2, 4096);
            MqttSetReceiveBuffer(3, receiverArray3, 4096);

            // Set up topic
             topicStringBytes = Encoding.ASCII.GetBytes("WinTopicString");
          

            // HTTP Trivial server test
             HttpServerTestAppletInit();
        }


        /// <summary>
        /// Timer event handler, poll the DLL, handle data transfer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private unsafe void timerTick_Tick(object sender, EventArgs e)
        {
            DateTime dtNow = DateTime.Now;

            Int32 intervalMs;
            TimeSpan ts = dtNow-dtPrev;

            intervalMs = ts.Milliseconds;

            int r = MqttTick(intervalMs);


            HttpServerTestAppletTick(intervalMs);
            TickedDataTransfer();

            dtPrev = dtNow;

            if (!IsBusy && fileDownloadsRemaining > 0)
            {
                fileDownloadsRemaining--;

                if (fileDownloadsRemaining > 0)
                {
                    LogToListbox(listBoxTrace, "Triggering file download - " + fileDownloadsRemaining + " more to go after this one");
                }

                string server = ServerTextBox.Text;

                int blockSize = Int32.Parse(BlockSizeTextBox.Text);

                string file = FileTextBox.Text;

                FileDownloaderBeginSession(server, blockSize, file);
            }

        }

        /// <summary>
        /// Initiate the session - with Winsock
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonGo_Click(object sender, EventArgs e)
        {
           MqttBeginSessionForCSharp(0); // 0 = winsock
        }


        /// <summary>
        /// Initiate Modem session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonGoModem_Click(object sender, EventArgs e)
        {
            MqttBeginSessionForCSharp(1); // 1 = stub modem
        }



        private void buttonClear_Click(object sender, EventArgs e)
        {
            listBoxTrace.Items.Clear();
        }

        private void button_Http_Click(object sender, EventArgs e)
        {
           byte[] urlArray = new byte[256];
            urlArray = Encoding.ASCII.GetBytes("MyUrl");
           byte[] urlHost = new byte[256];
            urlHost = Encoding.ASCII.GetBytes("MyHost");

            HttpBeginSession(0);

            HttpServerTestGenerateRequest(urlArray, urlHost, 12, 15);
        }

        private void buttonFileDownload_Click(object sender, EventArgs e)
        {

            fileDownloadsRemaining = Int32.Parse(RepeatTextBox.Text);

        }

        private void buttonCancelDownload_Click(object sender, EventArgs e)
        {
            FileDownloaderCancelSession(0);
        }

    }
}

