/******************************************************************************
*******************************************************************************
**
**         Filename: ModemStub.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/**
 * @addtogroup ModemStub
 * @{
 */

/*!***************************************************************************
 * @file ModemStub.h
 *
 * @brief  Handles the connect on a modem
 * @author Duncan Willis
 * @date 2015.03.19
 * @version 1.0
 *****************************************************************************/


#ifndef _MODEM_H_
#define _MODEM_H_


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include "CommonTypes.h"
#include "RingBuffer.h"
#include "TransportInterface.h"


/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/


/**
** Internal operating state
**/
typedef enum E_MODEM_STATE
{
    E_MS_RESET,
    E_MS_BEGIN_OPEN_CONNECTION, 
    E_MS_OPEN_CONNECTION_WAIT,
    E_MS_BEGIN_OPEN_SOCKET,
    E_MS_OPEN_SOCKET_WAIT,
    E_MS_BEGIN_CLOSE_SOCKET,
    E_MS_CLOSE_SOCKET_WAIT,
    E_MS_BEGIN_CLOSE_CONNECTION,
    E_MS_CLOSE_CONNECTION_WAIT
}
E_MODEM_STATE;

/**
** The driver object
**/
typedef struct MODEM_STUB
{
    // This contains the function set for the device
    const TRANSPORT_INTERFACE* pInterface;

    // Below are implementational details, add / subtract as necessary
    E_MODEM_STATE           state;
    E_CONNECTION_COMMAND    cmd;
    void*                   pCmdParams; 
    int                     sock;


    RingBufferT* pRxRingBuffer;   // Buffer for data FROM cloud
    RingBufferT* pTxRingBuffer;   // Buffer for data TO cloud

} MODEM_STUB;


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/

 
/**
** Public functions - common interface
**/
int32_t ModemStubInit(void* pModem, const void* initParams);

#endif
