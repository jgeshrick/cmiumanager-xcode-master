/******************************************************************************
*******************************************************************************
**
**         Filename: Mqtt.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup Mqtt
 * @{
 */


/*!***************************************************************************
 * @file Mqtt.c
 *
 * @brief Sample app for use by C~ test harness and as interface for Python
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/

 
/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include "CommonTypes.h"
#include "Ringbuffer.h"
#include "MqttManager.h"
#include "HttpClient.h"
#include "TransportManager.h"
#include "TcpSessionManager.h"
#include "SocketWin.h"
#include "ModemStub.h"
#include "DebugTrace.h"
#include "FileDownloader.h"
#include "Mqtt.h"


/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/


/**
** @brief The object that manages the overall session, state machine example
            -- This is the "demo app" to illustrate the use of the other classes
**/
static TCP_SESSION_MANAGER              sm;


/**
** The params to use on begin of MQTT session 
**/
static MQTT_MANAGER_SESSION_PARAMS      sessionParams;
    

static MQTT_MANAGER_SUBSCRIPTION        subscriptionSubstopic;
static MQTT_MANAGER_SUBSCRIPTION        subscriptionFishes;
static MQTT_MANAGER_SUBSCRIPTION        subscriptionTime;

static MQTT_MANAGER_PUB                 pubMREAD = {0};
static MQTT_MANAGER_PUB                 pubTest1 = {0};


/**
** The results of the MQTT session 
**/
static MQTT_MANAGER_SESSION_RESULTS     sessionResults;
    

/**
** @brief The object that runs the MQTT protocol (Wrapper around Paho)
**/
static MQTT_MANAGER                     mm;


/**
** @brief The object that runs the HTTP Firmware request
**/
static uint8_t                          buf[HTTP_WORKING_BUFFER_SIZE];
HTTP_CLIENT_INIT_PARAMS                 hmInitParams;

static HTTP_CLIENT_SESSION_PARAMS       sp;
static HTTP_CLIENT_SESSION_RESULTS      sr;
static HTTP_CLIENT                      httpClient;

/**
** @brief The object that manages the interface to the modem or socket - device INDEPENDENT interface
**/
static TRANSPORT_MANAGER                tmSocketWin;        // Example of Win Sock interface
static TRANSPORT_MANAGER                tmModem;            // Example of Telit modem


static char                             fileDownloadServer[128]; //holds the DNS name / server IP of the download server
static char                             fileDownloadFilename[256]; //holds the filename to download from the server

/**
** @brief The device specific driver
**/
static SOCKET_WIN                           socketWin; 


/**
** @brief The device specific driver - replace with your own object
**/
static MODEM_STUB                           modemStub; 


/**
** The target address etc to connect to
**/
CONNECTION_PARAMS                           connectParams;



// Currently a placeholder and unused
MQTT_MANAGER_INIT_PARAMS                    mqttInitParams;


static void onPublishMessageReceivedCallback(const MQTT_MANAGER_PUB* pPubMsg);

#define PUB_BUFFER_MAX_COUNT                4

static uint32_t                             pubBufferCount = 0;


// Object to get files for f/w upgrades
static FILE_DOWNLOADER                      fileDownloader;


// Points to areas of memory owned by the host (C# or Python) into which
// the publish message data is copied
unsigned char* rxDataBuf[PUB_BUFFER_MAX_COUNT];

extern bool debugLogEnable; //defined in DebugTrace.c non-CMIU for debug purposes

static void OnMqttDone(TCP_SESSION_MANAGER* pSm, MQTT_MANAGER* pM);
static void OnHttpDone(TCP_SESSION_MANAGER* pSm, HTTP_CLIENT* pM);


/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/


/******************************************************************************
**    
**      @param[in] enable If true, enable debug log, otherwise disable
**      @return None
**      @brief
**         Description: Enable/disable the debug log - Use with Python.
**         Debug log must be disabled if >1 thread as not thread-safe
**
******************************************************************************/
DLL_EXPORT void 
    EnableDebugLog(bool enable)
{
    DebugTrace("EnableDebugLog(%d) - before", enable);

    debugLogEnable = enable;

    DebugTrace("EnableDebugLog(%d) - after", enable);
}


/******************************************************************************
**    
**      @param[in, out] pTm The Transport manager that will have the SocketWin bound to it.
**              @return None
**  @brief
**         Description: Init a socketWin interface and associate a Transport 
**                      Manager with it. - Use with Python
**
******************************************************************************/
DLL_EXPORT void 
    MqttInitForPython(TRANSPORT_MANAGER* pTm, SOCKET_WIN* socketWinPy)
{
    DebugTrace("Mqtt Init called");

    SocketWinInit(socketWinPy, NULL);
    TransportManagerInit(pTm, socketWinPy->pInterface, (void*)socketWinPy);
}


/******************************************************************************
**    
**           @param[in] useModem - if 0, use win sock, else use stub modem
**              @return 0
**  @brief
**         Description: Start a session, for use with the C# test harness only
**
******************************************************************************/
DLL_EXPORT int32_t 
    MqttBeginSessionForCSharp(int useModem)
{    
      DebugTrace("Mqtt MqttBeginSessionForCSharp called");

    // Defaults for session params and results
    // Set up topic and pointers to **local** buffering for messages
    // This might well be over-written when using DLL interface (will use external arrays)
    MqttManagerInitSessionParams(&sessionParams); 
    MqttManagerInitSessionResults(&sessionResults);


    //!!! WARNING - Must be 3 to be compatible with Azure server
    ////sessionParams.mqttVersion = MQTT_VERSION_3_1_0;
    sessionParams.expectedPubsToReceive = 1;

    subscriptionSubstopic.topicFilter = "substopic";
    subscriptionSubstopic.qos = E_MQTT_QOS_0;
    subscriptionFishes.topicFilter = "fishes";
    subscriptionTime.topicFilter = "time";
    MqttManagerAddSubscription(&sessionParams, &subscriptionSubstopic);
    MqttManagerAddSubscription(&sessionParams, &subscriptionFishes);
    MqttManagerAddSubscription(&sessionParams, &subscriptionTime);
  
    pubMREAD.pData = "Meter reading 987654321";
    pubMREAD.dataLength = strlen((const char*)pubMREAD.pData); 
    pubMREAD.topic = "MREAD";
    pubMREAD.qos = E_MQTT_QOS_1;
    pubTest1.pData = "Warning - more sharks";
    pubTest1.dataLength = strlen((const char*)pubTest1.pData); 
    pubTest1.topic = "fishes";
    pubTest1.qos = E_MQTT_QOS_1;
    MqttManagerAddPublish(&sessionParams, &pubMREAD);
    MqttManagerAddPublish(&sessionParams, &pubTest1);
    
    pubBufferCount = 0;

    // Init the MQTT protocol manager    
    mqttInitParams.pfOnPublishRxCallback = onPublishMessageReceivedCallback;
    MqttManagerInit(&mm, &mqttInitParams, &sessionParams, &sessionResults);   
 
    
    // Init a socketWin interface and associate a Transport Manager with it.
     TransportManagerInitConnectionParams(&connectParams, 
        MQTT_DEMO_TEST_SERVER, 
        MQTT_DEMO_TEST_PORT); 

    SocketWinInit(&socketWin, NULL);
    TransportManagerInit(&tmSocketWin, socketWin.pInterface, (void*)&socketWin);

    // Init session manager, using the given connection and protocol
    TcpSessionManagerInit(&sm, 
        &tmSocketWin,
        &connectParams);

    // Run the MQTT Session
    TcpSessionManagerBegin(&sm,
        (void*)&mm,
        &MqttManagerBegin,
        &MqttManagerRun,
        &OnMqttDone); 

    return 0;
}


/******************************************************************************
**    
**           @param[in] pSm The TcpSessionManager
**           @param[in] pM The MQTT_MANAGER  
**          @param[out]  
**              @return None
**  @brief
**         Description: Callback when the MQTT Session completes
**
******************************************************************************/
static void OnMqttDone(TCP_SESSION_MANAGER* pSm, MQTT_MANAGER* pM)
{
    DebugTrace("SUCCESS OnMqttDone called\n");
}


/******************************************************************************
**    
**           @param[in] sessionManager The TCP Session manager
**           @param[in] fileDownloaderPy The FileDownloader object 
**              @return zero 
**  @brief
**         Description: Used to start tcp session manager for file downloader
**                      from Python
**
******************************************************************************/
DLL_EXPORT int32_t 
    MqttBeginSessionForPython(TCP_SESSION_MANAGER* sessionManager, 
                                 void* mqttManagerPy)
{
    TcpSessionManagerBegin(sessionManager,
                            mqttManagerPy,
                            (pfnManagerBeginFunction)&MqttManagerBegin,
                            (pfnManagerRunFunction)&MqttManagerRun,
                            NULL); 

    pubBufferCount = 0;

    return 0;
}


/******************************************************************************
**    
**           @param[in] dummy Needed for DLL (not used)
**          
**              @return 0
**  @brief
**         Description: Start a session,  to do a single HTTP Get
******************************************************************************/
DLL_EXPORT int32_t 
    HttpBeginSession(int dummy)
{
    (void)dummy;

    // Init a socketWin interface and associate a Transport Manager with it.
    TransportManagerInitConnectionParams(&connectParams, 
    HTTP_AWS_SERVER,
        HTTP_PORT); 

    SocketWinInit(&socketWin, NULL);
    TransportManagerInit(&tmSocketWin, socketWin.pInterface, (void*)&socketWin);
       
    // Set up session args for the HTTP 
    sp.fileName         = "/firmware/test.hex";
    sp.hostName         = "";
    sp.timeoutForWaitMs = 2000;
    sp.firstRangeByte   = 0;
    sp.lastRangeByte    = 23;

    hmInitParams.buffer = buf;
    hmInitParams.bufferSize = sizeof(buf);
    hmInitParams.OnReceivedContentByte = NULL;
    hmInitParams.pCtx = NULL;
    HttpClientInit(&httpClient, &hmInitParams, &sp, &sr);  

    // Init to run the session, using the given connection and protocol
    TcpSessionManagerInit(&sm,
        &tmSocketWin,
        &connectParams);

    TcpSessionManagerBegin(&sm,
        (void*)&httpClient,
        &HttpClientBegin,
        &HttpClientRun,
        &OnHttpDone); 

    return 0;
}


/******************************************************************************
**    
**           @param[in] pSm The TcpSessionManager
**           @param[in] pM The HTTP client
**          @param[out]  
**              @return None
**  @brief
**         Description: Callback when the HTTP Get completes
**
******************************************************************************/
static void OnHttpDone(TCP_SESSION_MANAGER* pSm, HTTP_CLIENT* pM)
{
    DebugTrace("OnHttpDone called\n");


    if(pM->errorState != HTTP_OK)
    {
        // DebugTrace("HTTP Get ERROR %d", pM->errorState);
    }

#if 0 // demo below
    sp.url              = "/mdce-web/resources/this_request/will_fail.png";

    // Start another
    TcpSessionManagerBegin(&sm,
        (void*)&httpClient,
        &HttpManagerBegin,
        &HttpManagerRun,
        NULL); 

#endif
}


void HandleReceivedFileByte(struct FILE_DOWNLOADER* pDl,
    uint8_t b, 
    uint32_t byteIndex)
{
  //  if((byteIndex % 5000) == 0) DebugTrace("By %u\n", byteIndex);
}


/******************************************************************************
**    
**           @param[in] dummy - Needed for DLL (not used)
**          
**              @return 0
**  @brief
**         Description: Start a session,  to do file download
**
**   Tested on "52.27.80.45" and fdInitParams.fileName = "/image-distribution/CmiuConfiguration/CmiuConfiguration_v0.3.150715.28.bin";
**
******************************************************************************/
DLL_EXPORT int32_t 
    FileDownloaderBeginSession(const char* const server, 
        const int blockSize, 
        const char* const file)
{
    static uint8_t                                  buf[HTTP_MAX_LINE_LEN];

    char textBuf[4096];
    FILE_DOWNLOADER_INIT_PARAMS fdInitParams;

    strcpy(fileDownloadServer, server); //copy the string contents to C memory as they are owned by C# interop and will not survive after this method completes
    strcpy(fileDownloadFilename, file);

    sprintf(textBuf, "Start file download, server=%s, blockSize=%d, file=%s", fileDownloadServer, blockSize, fileDownloadFilename);
    DebugTrace(textBuf);

    // Init a socketWin interface and associate a Transport Manager with it.
    TransportManagerInitConnectionParams(&connectParams, 
        fileDownloadServer,
        HTTP_PORT); 

    SocketWinInit(&socketWin, NULL);

    TransportManagerInit(&tmSocketWin, socketWin.pInterface, (void*)&socketWin);

    // Init to run the session, using the given connection and protocol
    TcpSessionManagerInit(&sm, 
        &tmSocketWin,
        &connectParams);

    fdInitParams.buffer             = buf;
    fdInitParams.bufferSize         = sizeof(buf);
    fdInitParams.fileName           = fileDownloadFilename;
    fdInitParams.blockSize          = blockSize; //default 256
    fdInitParams.maxRequestRetries  = 1;
    fdInitParams.requestTimeoutMs   = 2000;
    fdInitParams.OnReceivedByte     = &HandleReceivedFileByte;
    FileDownloaderInit(&fileDownloader, &fdInitParams);

    TcpSessionManagerBegin(&sm,
        (void*)&fileDownloader,
        &FileDownloaderBegin,
        &FileDownloaderRun,
        NULL); 

    return 0;
}


/******************************************************************************
**    
**           @param[in] sessionManager The TCP Session manager
**           @param[in] fileDownloaderPy The FileDownloader object 
**              @return zero 
**  @brief
**         Description: Used to start tcp session manager for file downloader
**                      from Python
**
******************************************************************************/
DLL_EXPORT int32_t 
    FileDownloadBeginForPython(TCP_SESSION_MANAGER* sessionManager, 
                                 void* fileDownloaderPy)
{
    TcpSessionManagerBegin(sessionManager,
                            fileDownloaderPy,
                            (pfnManagerBeginFunction)&FileDownloaderBegin,
                            (pfnManagerRunFunction)&FileDownloaderRun,
                            NULL); 
    return 0;
}


/******************************************************************************
**    
**           @param[in] dummy Not used
**              @return Percent of transfer complete
**  @brief
**         Description: Gets the progress info
**
******************************************************************************/
DLL_EXPORT uint32_t FileDownloaderGetProgress(int dummy)
{
    return FileDownloaderGetProgressPercent(&fileDownloader, NULL, NULL);
}


/******************************************************************************
**    
**           @param[in] dummy - Needed for DLL (not used)
**          
**              @return 0
**  @brief
**         Description: Cancel a session.
******************************************************************************/
DLL_EXPORT int32_t   FileDownloaderCancelSession(int dummy)
{
    FileDownloaderCancel(&fileDownloader,FILE_DOWNLOADER_USER_CANCELLED);
    return 0;
}


/******************************************************************************
**    
**           @param[in] tickIntervalMs Time between ticks in ms
**              @return None
**  @brief
**         Description: Main Tick for MQTT -  the overall Session (i.e our demo)
**
******************************************************************************/
DLL_EXPORT void 
    MqttTick(uint32_t tickIntervalMs)
{
    TcpSessionManagerTick(&sm, tickIntervalMs);
}


/******************************************************************************
**    
**           @param[in] index The 0 based area (one per PUB msg)
**           @param[in] rxData Pointer to an area allocated by the host
**           @param[in] rxSize The size of the allocated block
**              @return 0 if OK, -1 for out of range index
**  @brief
**         Description: Sets memory area to use as incoming copy-buffer on 
**                      receipt of a PUBLISH message 
**
******************************************************************************/
DLL_EXPORT int 
    MqttSetReceiveBuffer(unsigned int index, unsigned char* rxData, unsigned int rxSize)
{
    if (rxSize  >= sizeof(MQTT_MANAGER_PUB_AND_PAYLOAD)) 
    {
        if (index < PUB_BUFFER_MAX_COUNT)
        {
            rxDataBuf[ index ] = rxData;
            return 0;
        }
    }
    else
    {
        ASSERT(0);
    }

    return -1; // failed
}


/******************************************************************************
**    
**          @param[out] txData Pointer to an area to be sent (owned by the host)
**           @param[in] txCount The number to be sent
**              @return 0
**  @brief
**         Description: Sets memory area to use as tx buffer  
**                      - see TsmSetTopicString()
**
******************************************************************************/
DLL_EXPORT int 
    MqttSetTransmitBuffer(unsigned char* txData, unsigned int txCount)
{
    pubMREAD.pData = txData;
    pubMREAD.dataLength = txCount;

    return 0;
}


/******************************************************************************
**    
**          @param[out] pPubMsg Msg received
**              @return None
**  @brief
**         Description: Callback that is called on receipt of a PUBLISH message  (C# demo)
**
******************************************************************************/
static void onPublishMessageReceivedCallback(const MQTT_MANAGER_PUB* pPubMsg)
{ 
    MQTT_MANAGER_PUB_AND_PAYLOAD* pExportedPub;
    size_t numToCopy = pPubMsg->dataLength;
    DebugTrace("CB Called" );

    if (pubBufferCount < PUB_BUFFER_MAX_COUNT)
    {
        if (numToCopy > PUB_BUFFER_SIZE)
        {
            numToCopy = PUB_BUFFER_SIZE;
            DebugTrace("ERROR Insufficient buffer size to copy into; truncated" );
        }

        pExportedPub = (MQTT_MANAGER_PUB_AND_PAYLOAD*)(rxDataBuf[ pubBufferCount ]);

        if (NULL != pExportedPub)
        {
        // Make a copy of the pub msg (which is just pointers to payload)
        pExportedPub->pub = *pPubMsg;

        // Adjust the stored pub data pointers to point to local copies
        pExportedPub->pub.pData = &(pExportedPub->payloadBytes[0]);
        pExportedPub->pub.topic = &(pExportedPub->topicStringBytes[0]);

        // Make a full copy of the payload
        memcpy(pExportedPub->payloadBytes, 
            pPubMsg->pData, 
            numToCopy);         
        pExportedPub->payloadByteCount = numToCopy;

        // Make a full copy of the topic string
        strncpy(pExportedPub->topicStringBytes, pPubMsg->topic,  
            MQTT_MAX_TOPIC_STRING);  
        }
        else
        {
             DebugTrace("ERROR No memory allocated" );
        }
        
        DebugTrace("Received PUBLISH Copied OK" );
        pubBufferCount++;
    }
    else
    {
        DebugTrace("ERROR Insufficient buffers for PUBLISH messages" );
    }
}


/******************************************************************************
**    
**               @param None
**              @return The number that were received in the last session  
**  @brief
**         Description: Get the number of received publish msgs
**
******************************************************************************/
DLL_EXPORT uint32_t 
    MqttGetReceivedPublishCount(void)
{
    return pubBufferCount;
}


/******************************************************************************
**    
**           @param[in] index  0 based index of the msg received
**              @return Pointer to a received PUB message, NULL if index out of range (0 based)
**  @brief
**         Description: Get the copy of the received publish msg  
**
******************************************************************************/
DLL_EXPORT MQTT_MANAGER_PUB_AND_PAYLOAD* 
    MqttGetReceivedPublishMsg(uint32_t index)
{
    MQTT_MANAGER_PUB_AND_PAYLOAD* pPub = NULL;

    if (index < pubBufferCount)
    {        
        pPub = (MQTT_MANAGER_PUB_AND_PAYLOAD*)(rxDataBuf[ index ]);
    }

    return pPub;
}


/******************************************************************************
**    
**               @param None
**              @return 1 if busy, 0 if ready
**  @brief
**         Description: Is session in progress?
**
******************************************************************************/
DLL_EXPORT int 
    MqttIsBusy(void)
{
    return (int)TcpSessionManagerIsBusy(&sm);
}


/// Deprecated (example)
DLL_EXPORT void 
    TsmSetTopicString(unsigned char* topicStringBytes)
{
    pubMREAD.topic = (char*) topicStringBytes;
}


/**
 * @}
 */
