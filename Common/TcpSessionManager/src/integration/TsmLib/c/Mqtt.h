/******************************************************************************
*******************************************************************************
**
**         Filename: Mqtt.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**       Sample app for use by C# test harness and as interface for Python
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/**
 * @addtogroup Mqtt
 * @{
 */

/*!***************************************************************************
 * @file Mqtt.h
 *
 * @brief Sample app for use by C# test harness and as interface for Python
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/


#ifndef __MQTT_DEMO_H__
#define __MQTT_DEMO_H__


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/

#include <stdint.h>
#include "CommonTypes.h"
#include "MqttManager.h"
#include "TransportManager.h"
#include "TcpSessionManager.h"
#include "SocketWin.h"

// Some popular MQTT servers for testing

#define MQTT_PORT               1883
#define MQTT_LOCAL_TEST_SERVER  "mqttTestUbuntu.cloudapp.net" 
#define MQTT_ECLIPSE_SERVER     "iot.eclipse.org"
#define MQTT_AWS_SERVER         "52.25.21.32" //  "52.27.100.200" //"52.11.212.174"  

#define MQTT_DEMO_TEST_SERVER   MQTT_AWS_SERVER
#define MQTT_DEMO_TEST_PORT     MQTT_PORT

// Demo for http firmware server
#define HTTP_AWS_SERVER         "52.27.80.45" //<-- aw-mdce-dev1-brk01.  Its private IP: "10.120.101.58" // was"52.26.146.127" // "Troy:10.120.0.174"//   
#define HTTP_PORT               80


#define PUB_BUFFER_SIZE         1024


/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/


typedef struct MQTT_MANAGER_PUB_AND_PAYLOAD
{
    MQTT_MANAGER_PUB    pub;
    char                topicStringBytes[MQTT_MAX_TOPIC_STRING];
    uint8_t             payloadBytes[PUB_BUFFER_SIZE];
    uint32_t            payloadByteCount;

} MQTT_MANAGER_PUB_AND_PAYLOAD;



/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/

DLL_EXPORT void 
    EnableDebugLog(bool enable);

DLL_EXPORT void 
    MqttInitForPython(TRANSPORT_MANAGER* pTm, SOCKET_WIN* socketWinPy);

DLL_EXPORT void 
    MqttInitForCSharp(void);

DLL_EXPORT void        
    MqttTick(uint32_t tickMs);

DLL_EXPORT int32_t 
    MqttBeginSessionForPython(TCP_SESSION_MANAGER* sessionManager, 
                                 void* mqttManagerPy);
DLL_EXPORT uint32_t    
    MqttGetReceivedPublishCount(void);

DLL_EXPORT MQTT_MANAGER_PUB_AND_PAYLOAD* 
    MqttGetReceivedPublishMsg(uint32_t index);

DLL_EXPORT int 
    TsmSetTransmitBuffer(unsigned char* txData, unsigned int txCount);

DLL_EXPORT int 
    TsmSetReceiveBuffer(unsigned int index, unsigned char* rxData, unsigned int rxSize);

DLL_EXPORT int 
    TsmGetReceiveCount();

DLL_EXPORT int32_t 
    FileDownloaderBeginSession(const char* const server, 
        const int blockSize, 
        const char* const file);

DLL_EXPORT void 
    TsmSetTopicString(unsigned char* topicStringBytes);

DLL_EXPORT int 
    TsmIsBusy(void);

DLL_EXPORT int32_t 
    FileDownloadBeginForPython(TCP_SESSION_MANAGER* pSm, 
                                 void* fileDownloaderPy);


#endif //__MQTT_DEMO_H__


/**
 * @}
 */
