/******************************************************************************
*******************************************************************************
**
**         Filename: SocketWin.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/**
 * @addtogroup SocketWin
 * @{
 */

/*!***************************************************************************
 * @file SocketWin.h
 *
 * @brief  Handles the connect on winsock
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/


#ifndef _SOCKET_WIN_H_
#define _SOCKET_WIN_H_


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include "CommonTypes.h"
#include "TransportInterface.h"
#include "RingBuffer.h"


/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/

#define RING_BUFFER_SIZE_RX 4096  // depends on tick rate / data rate etc.
#define RING_BUFFER_SIZE_TX 4096

/**
** Internal operating state
**/
typedef enum E_SOCKET_WIN_STATE
{
    E_SWS_RESET,
    E_SWS_BEGIN_OPEN_CONNECTION, 
    E_SWS_OPEN_CONNECTION_WAIT,
    E_SWS_BEGIN_OPEN_SOCKET,
    E_SWS_OPEN_SOCKET_WAIT,
    E_SWS_BEGIN_CLOSE_SOCKET,
    E_SWS_CLOSE_SOCKET_WAIT,
    E_SWS_BEGIN_CLOSE_CONNECTION,
    E_SWS_CLOSE_CONNECTION_WAIT
}
E_SOCKET_WIN_STATE;



/**
** The driver object
**/
typedef struct SOCKET_WIN
{
    const TRANSPORT_INTERFACE* pInterface;

    E_SOCKET_WIN_STATE      state;
    E_CONNECTION_COMMAND    cmd;
    void*                   pCmdParams; 
    int                     sock;
    bool                    wsaStartupCalled;

    uint8_t rxData[RING_BUFFER_SIZE_RX];
    uint8_t txData[RING_BUFFER_SIZE_TX];
    RingBufferT rxRingBuffer;   // Buffer for data FROM cloud
    RingBufferT txRingBuffer;   // Buffer for data TO cloud
} SOCKET_WIN;


 
/**
** Public functions - common interface
**/
 
int SocketWinInit   (void* pCtx, const void* pInitParams);

#endif

/**
 * @}
 */