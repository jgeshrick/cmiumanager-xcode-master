/******************************************************************************
*******************************************************************************
**
**         Filename: HttpServerTestApplet.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup HttpServerTestApplet
 * @{
 */


/*!***************************************************************************
 * @file HttpServerTestApplet.h
 *
 * @brief Test host for Trivial HTTP Server
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/


#ifndef HTTP_SERVER_TEST_APPLET_H_
#define HTTP_SERVER_TEST_APPLET_H_


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include "FsmTypes.h" 
#include "HttpUtilities.h"

/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/


// Max size for header temp buffer
#define HTTP_SERVER_MAX_HEADER_SIZE 256


/*===========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                        */
/*===========================================================================*/


DLL_EXPORT 
void HttpServerTestAppletInit(void);

DLL_EXPORT 
void HttpServerTestAppletTick(uint32_t tickIntervalMs);

DLL_EXPORT
void HttpServerReceiveData(const uint8_t* buffer, uint32_t numBytes);

DLL_EXPORT 
    void HttpServerTestGenerateRequest(
    const char* url,
    const char* hostName, 
    uint32_t firstRangeByte, 
    uint32_t lastRangeByte);

DLL_EXPORT uint32_t HttpServerGetResponseBytes(unsigned char* data, uint32_t arraySize);

#endif


/**
 * @}
 */
