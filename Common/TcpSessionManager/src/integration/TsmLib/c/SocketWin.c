/******************************************************************************
*******************************************************************************
**
**         Filename: SocketWin.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup  
 * @{
 */


/*!***************************************************************************
 * @file  SocketWin.c
 *
 * @brief  Handles the connect on a  win sock
 * @author Duncan Willis
 * @date 2015.03.19
 * @version 1.0
 *****************************************************************************/

 
/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <winsock2.h>
#include <Iphlpapi.h>
#include <ws2tcpip.h>
#include "Mqtt.h"

#include "CommonTypes.h"
#include "DebugTrace.h"
#include "TransportManager.h"
#include "SocketWin.h"



#if !defined(SOCKET_ERROR)
/** error in socket operation */
#define SOCKET_ERROR -1
#endif
#define SHUT_OPTIONS        SD_SEND
#define MAXHOSTNAMELEN      256
#define EAGAIN              WSAEWOULDBLOCK
#define EINTR               WSAEINTR
#define EINVAL              WSAEINVAL
#define EINPROGRESS         WSAEINPROGRESS
#define EWOULDBLOCK         WSAEWOULDBLOCK
#define ENOTCONN            WSAENOTCONN
#define ECONNRESET          WSAECONNRESET

/* From Paho demo: "default on Windows is 64 - increase to make Linux and Windows the same" */
//#define FD_SETSIZE 1024 // DEW Removed this - not sure why it was needed and provokes a warning.

#define ioctl ioctlsocket
#define socklen_t int


// To link winsock into dll
#pragma comment(lib, "Ws2_32.lib")

/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/


static int32_t SocketWinCommand(void* pCtx, E_CONNECTION_COMMAND cmd, void* cmdParams);
static void SocketWinTick(void* pCtx,  uint32_t tickIntervalMs);
static E_CONNECTION_STATE  SocketWinGetState(void* pCtx, E_QUERY query, void* pQueryResult);

static int32_t SocketWinSend(void* pCtx, const uint8_t* buf, uint32_t numBytes);
static int32_t SocketWinRecv(void* pCtx, uint8_t* buf, uint32_t bufferSize);
static int32_t SocketWinOpenConnection(void* pCtx);
static int32_t SocketWinOpenSocket(void* pCtx);
static int32_t SocketWinCloseSocket(void* pCtx);
static int32_t SocketWinCloseConnection(void* pCtx);


static const TRANSPORT_INTERFACE sockWinIninterface =
{
    SocketWinInit,
    SocketWinCommand,
    SocketWinTick,
    SocketWinGetState,
    SocketWinRecv,
    SocketWinSend,
};



/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/

//S_DATA_CONN_STATE_INFO  dataConnInfo = {DATA_CONN_STATE_DEFINE_CONTEXT, 0, 0};



/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/


/******************************************************************************
**    
**      @param[in, out] pSockWin The object to init
**           @param[in] initParams Optional pointer to init params
**              @return OK_STATUS
**  
**  @brief
**         Description: Init the object - this should not block. Do any lengthy tasks
**                      as a command.
**
******************************************************************************/
int SocketWinInit(SOCKET_WIN* pSockWin, const void* initParams)
{
    int e = OK_STATUS;

    pSockWin->pInterface        = &sockWinIninterface;
    pSockWin->cmd               = CON_CMD_NULL;
    pSockWin->sock              = 0;
    pSockWin->wsaStartupCalled  = false;

    RingBufferInit(&pSockWin->rxRingBuffer, pSockWin->rxData, RING_BUFFER_SIZE_RX);
    RingBufferInit(&pSockWin->txRingBuffer, pSockWin->txData, RING_BUFFER_SIZE_TX);

    return e;
}


/******************************************************************************
**    
**      @param[in, out] pCtx The modem driver
**           @param[in] cmd The command enumeration
**           @param[in] CmdParamms Optional pointer to static data (may be null)
**              @return The status (OK_STATUS here)
**  
**  @brief
**         Description: The command to send (non blocking)
**
******************************************************************************/
static int SocketWinCommand(void* pCtx, E_CONNECTION_COMMAND cmd, void* cmdParams)
{
       SOCKET_WIN* pSockWin = (SOCKET_WIN*)pCtx;
       uint32_t socketBytesAvailable = 0;

       pSockWin->cmd = cmd;
       pSockWin->pCmdParams = cmdParams;
      

       // Query the amount of data not yet processed (FIFO plus socket data not yet read into FIFO)
       // Note that the Winsock Rx function is not polled during the wait for content, 
       // therefore, the FIFO won't get written to, and so the content will never get seen.
       if (CON_CMD_GET_RX_COUNT == cmd)
       {
            // Get amount queued in socket
            ioctlsocket(pSockWin->sock, FIONREAD, &socketBytesAvailable);  

            return (int)(socketBytesAvailable + RingBufferUsed(&pSockWin->rxRingBuffer));
       }

       return OK_STATUS;
}


/******************************************************************************
**    
**      @param[in, out] pCtx The SocketWin object
**           @param[in] tickIntervalMs The time between ticks in ms
**              @return None
**  
**  @brief
**         Description: Tick-process the SocketWin (demo state machine)
**
******************************************************************************/
static void SocketWinTick(void* pCtx,  uint32_t tickIntervalMs)
{
       SOCKET_WIN* pSockWin = (SOCKET_WIN*)pCtx;
       
       switch (pSockWin->state)
       {
       case E_SWS_RESET:
           switch (pSockWin->cmd)
           {
           case CON_CMD_NULL:
               break;

           case CON_CMD_OPEN_CONNECTION:
               pSockWin->cmd = CON_CMD_NULL;
               pSockWin->state = E_SWS_BEGIN_OPEN_CONNECTION;
               break;

           case CON_CMD_OPEN_SOCKET:
               pSockWin->cmd = CON_CMD_NULL;
               pSockWin->state = E_SWS_BEGIN_OPEN_SOCKET;
               break;

           case CON_CMD_CLOSE_SOCKET:              
               pSockWin->cmd = CON_CMD_NULL;
               pSockWin->state = E_SWS_BEGIN_CLOSE_SOCKET;
               break;

           case CON_CMD_CLOSE_CONNECTION:              
               pSockWin->cmd = CON_CMD_NULL;
               pSockWin->state = E_SWS_BEGIN_CLOSE_CONNECTION;
               break;

           default:
               pSockWin->cmd = CON_CMD_NULL;
               break;
           }
           break;

       case E_SWS_BEGIN_OPEN_CONNECTION:
            SocketWinOpenConnection(pCtx);
            pSockWin->state = E_SWS_OPEN_CONNECTION_WAIT;
            break;       

       case E_SWS_OPEN_CONNECTION_WAIT:
            // wait for conn if needed
            pSockWin->state = E_SWS_RESET;
            break;
             
       case E_SWS_BEGIN_OPEN_SOCKET:
            SocketWinOpenSocket(pCtx);
            pSockWin->state = E_SWS_OPEN_SOCKET_WAIT;
            break;       
       
       case E_SWS_OPEN_SOCKET_WAIT:
            // wait for socket if needed
            pSockWin->state = E_SWS_RESET;
            break;
             
       case E_SWS_BEGIN_CLOSE_SOCKET:
           SocketWinCloseSocket(pCtx);
           pSockWin->state = E_SWS_CLOSE_SOCKET_WAIT;
           break;       
       
       case E_SWS_CLOSE_SOCKET_WAIT:
            // wait for disconn if needed
            pSockWin->state = E_SWS_RESET;
            break;
            
       case E_SWS_BEGIN_CLOSE_CONNECTION:
           SocketWinCloseConnection(pCtx);
           pSockWin->state = E_SWS_CLOSE_CONNECTION_WAIT;
           break;       
       
       case E_SWS_CLOSE_CONNECTION_WAIT:
            // wait for disconn if needed
            pSockWin->state = E_SWS_RESET;
            break;

       default:
           ASSERT(0);
           break;
       }
}

/******************************************************************************
**    
**      @param[in, out] pCtx The TransportManager object
**           @param[in] query The parameter to query
**          @param[out] pQueryResult Optional query result if applicable (may be NULL)
**              @return E_CONNECTION_STATE enum
**  
**  @brief
**         Description: Issue a query e.g. Is (socket)Open? 
**
******************************************************************************/
static E_CONNECTION_STATE  SocketWinGetState(void* pCtx, E_QUERY query, void* pQueryResult)
{
    E_CONNECTION_STATE rt;
    SOCKET_WIN* pSockWin = (SOCKET_WIN*)pCtx;

    switch (pSockWin->state)
    {
    case E_SWS_RESET:
        {
            if (pSockWin->sock > 0)
            {
                rt = CON_STATE_SOCKET_OPEN;
            }
            else if (pSockWin->wsaStartupCalled)
            {
                rt = CON_STATE_CONNECTION_OPEN_SOCKET_CLOSED;
            }
            else 
            {
                rt = CON_STATE_CONNECTION_CLOSED;
            }
        }
        break;
    case E_SWS_BEGIN_OPEN_CONNECTION:
    case E_SWS_OPEN_CONNECTION_WAIT:
        rt = CON_STATE_OPENING_CONNECTION;
        break;
    case E_SWS_BEGIN_OPEN_SOCKET:
    case E_SWS_OPEN_SOCKET_WAIT:
        rt = CON_STATE_OPENING_SOCKET;
        break;
    case E_SWS_BEGIN_CLOSE_SOCKET:
    case E_SWS_CLOSE_SOCKET_WAIT:
        rt = CON_STATE_CONNECTION_OPEN_SOCKET_CLOSED;
        break;
    case E_SWS_BEGIN_CLOSE_CONNECTION:
    case E_SWS_CLOSE_CONNECTION_WAIT:
        rt = CON_STATE_CONNECTION_CLOSED;
        break;
    default:
        ASSERT(0);
    }
    return rt;
}

/******************************************************************************
**    
**      @param[in, out] pCtx The SocketWin object
**           @param[in] buf The data to send
**           @param[in] numBytes The number of bytes to send
**              @return -1 if error, else num bytes sent.
**  
**  @brief
**         Description: Send data 
**
******************************************************************************/
static int32_t SocketWinSend(void* pCtx, const uint8_t* buf, uint32_t numBytes)
{
	SOCKET_WIN* pSockWin = (SOCKET_WIN*)pCtx;
    int rc = 0;

	rc = send(pSockWin->sock, (const char*) buf, numBytes, 0);  //  write(sock, buf, buflen); linux
	return rc;
}

/******************************************************************************
**    
**      @param[in, out] pCtx The SocketWin object
**          @param[out] buf The received data d
**           @param[in] bufferSize The size of the buffer
**              @return -1 if error, else num bytes sent.
**                      0 if there's  no data, else will return the num of bytes copied 
**                      into the buffer buf (up to bufferSize).
**  
**  @brief
**         Description: Receive data - Not blocking
**
******************************************************************************/
static int32_t SocketWinRecv(void* pCtx, uint8_t* rxBuffer, uint32_t bufferSize)
{
    SOCKET_WIN* pSockWin = (SOCKET_WIN*)pCtx;
    char dataByteFromModem;
    int bytesReceived;
    int bytesPeeked = 0;
    int32_t iResult = 0;
    int32_t numBytesCopied = 0;
    uint32_t rxCount = 0;
    uint32_t bytesAvailable = 0;
    uint32_t i;

    uint32_t bufferFreeCount = RingBufferFree(&pSockWin->rxRingBuffer);

    //
    // Step 1. If data in ringbuffer, just return it.
    //
    rxCount = RingBufferUsed(&pSockWin->rxRingBuffer);

    if (rxCount > 0)
    {
        // Limit transfer to size of supplied buffer
        if (rxCount > bufferSize) 
        {
            rxCount = bufferSize;
        }

        RingBufferRead(&pSockWin->rxRingBuffer, rxBuffer, rxCount);
        return rxCount;
    }

    //
    // Step 2. Otherwise (i.e. FIFO was empty), go read from socket, and put into FIFO
    //
    while ((uint32_t)iResult < bufferSize)
    {
        /* this call will return after the timeout set on initialization if no bytes;
        in your system you will use whatever you use to get whichever outstanding
        bytes your socket equivalent has ready to be extracted right now, if any,
        or return immediately */
          
        // Wait for one byte. If so, then there's likely more - find out how many.
        bytesReceived = recv(pSockWin->sock, &dataByteFromModem, 1, 0);
        ioctlsocket(pSockWin->sock, FIONREAD, &bytesAvailable);  

        if (bytesReceived == WSAETIMEDOUT || bytesReceived < 0)
        {
            iResult = bytesReceived;
            break;
        }
        else if (bytesReceived == 0)
        {
            break;
        }
        else
        {
            iResult += bytesReceived;
            RingBufferWriteOne(&pSockWin->rxRingBuffer, dataByteFromModem); // insert byte into ringbuffer

            // May as well get the other bytes from the socket into the FIFO
            if (bytesAvailable < bufferFreeCount)
            {
                for (i = 0; i < bytesAvailable; i++)
                {
                    recv(pSockWin->sock, &dataByteFromModem, 1, 0);
                    RingBufferWriteOne(&pSockWin->rxRingBuffer, dataByteFromModem);
                }
            }
        }
    }


    if (iResult > 0 )
    {
        rxCount = RingBufferUsed(&pSockWin->rxRingBuffer);
        numBytesCopied = rxCount;
        // Limit transfer to size of supplied buffer
        if (numBytesCopied > (int)bufferSize) 
        {
            numBytesCopied = bufferSize;
        }

        // Copy data if there is any.
        if (numBytesCopied > 0)
        {
            RingBufferRead(&pSockWin->rxRingBuffer, rxBuffer, numBytesCopied);
        }

        //DebugTrace("Bytes received: %d\n", numBytesCopied);
    }
    else if ( iResult == 0 )
    {
        DebugTrace("Connection closed\n");
    }    
    else if ( iResult == WSAETIMEDOUT )
    {
        DebugTrace("WSAETIMEDOUT : Rx Timeout\n");
    }
    else
    {
        DebugTrace("ERROR: recv failed: ret = %d, WSALastError=%d %s\n",iResult, WSAGetLastError(), (WSAETIMEDOUT==WSAGetLastError()) ? "WSAETIMEDOUT":"");  
    }

    if (iResult <= 0) 
    {
        /* check error conditions from your system here, and return -1 */
        if ( iResult != 0 )
        {
            DebugTrace("SocketWinRecv ERROR: %d\n", iResult);  
        }
        return -1;
    }

    return (iResult > 0) ? numBytesCopied : iResult;
}



/******************************************************************************
**    
**      @param[in, out] pCtx The SocketWin object
**              @return return code from WSAStartup - 0 if okay
**  
**  @brief
**         Description: Helper to init a network connection
**
**  @todo 
**          Basically moved from the sample without changes, should accommodate 
**          same usage for 'sock' for clarity, removing indirections.
**
******************************************************************************/
static int32_t SocketWinOpenConnection(void* pCtx)
{  
    SOCKET_WIN* pSockWin = (SOCKET_WIN*)pCtx;
    unsigned short wVersionRequested;
    WSADATA wsaData;
    int err = 0;

     CONNECTION_PARAMS* pCp = (CONNECTION_PARAMS*)(pSockWin->pCmdParams);          
	 ASSERT(NULL !=  pCp);
	 ASSERT(NULL !=  pCp->host);

    // Need to start the sockets interface in Win32
    wVersionRequested = MAKEWORD(2, 2);
    err = WSAStartup(wVersionRequested, &wsaData);
    DebugTrace("WSAStartup returned %d\n", err); 
    if (err == 0)
    {
        pSockWin->wsaStartupCalled = true;
    }
    else
    {
        pSockWin->wsaStartupCalled = false;
    }
    return err;
}


/******************************************************************************
**    
**      @param[in, out] pCtx The SocketWin object
**              @return socket number or 0 if couldn't open
**  
**  @brief
**         Description: Helper to open a socket
**
**  @todo 
**          Basically moved from the sample without changes, should accomodate 
**          same usage for 'sock' for clarity, removing indirections.
**
******************************************************************************/
static int32_t SocketWinOpenSocket(void* pCtx)
{  
    SOCKET_WIN* pSockWin = (SOCKET_WIN*)pCtx;
    int mysock = INVALID_SOCKET;
	int type = SOCK_STREAM;
	struct sockaddr_in address;
#if defined(AF_INET6)
	struct sockaddr_in6 address6;
#endif
	int rc = -1;
#if defined(WIN32)
	short family;
#else
	sa_family_t family = AF_INET;
#endif
	struct addrinfo *result = NULL;

	struct addrinfo hints = {0, AF_UNSPEC, SOCK_STREAM, IPPROTO_TCP, 0, NULL, NULL, NULL};
	static struct timeval tv;
    struct sockaddr* pSockAddress;
    int sockAddressSize;
    unsigned int  rxTimeoutMs = 20000;

     CONNECTION_PARAMS* pCp = (CONNECTION_PARAMS*)(pSockWin->pCmdParams);          
	 ASSERT(NULL !=  pCp);
	 ASSERT(NULL !=  pCp->host);

     
    DebugTrace("SocketWinOpenSocket: %s:%u\n", pCp->host, pCp->port); 

	if ((rc = getaddrinfo(pCp->host, NULL, &hints, &result)) == 0)
	{
		struct addrinfo* res = result;

		/* prefer ip4 addresses */
		while (res)
		{
			if (res->ai_family == AF_INET)
			{
				result = res;
				break;
			}
			res = res->ai_next;
		}

#if defined(AF_INET6)
		if (result->ai_family == AF_INET6)
		{
			address6.sin6_port = htons(pCp->port);
			address6.sin6_family = family = AF_INET6;
			address6.sin6_addr = ((struct sockaddr_in6*)(result->ai_addr))->sin6_addr;
            pSockAddress = (struct sockaddr*)(&address6);
            sockAddressSize = sizeof(address6);
		}
		else
#endif
		if (result->ai_family == AF_INET)
		{
			address.sin_port = htons(pCp->port);
			address.sin_family = family = AF_INET;
			address.sin_addr = ((struct sockaddr_in*)(result->ai_addr))->sin_addr;
            pSockAddress = (struct sockaddr*)(&address);
            sockAddressSize = sizeof(address);
		}
		else
			rc = -1;

		freeaddrinfo(result);
	}

	if (rc == 0)
	{
		mysock = socket(family, type, 0);
		
        if (mysock != -1)
		{
            #if defined(NOSIGPIPE)
			            int opt = 1;

			            if (setsockopt(*sock, SOL_SOCKET, SO_NOSIGPIPE, (void*)&opt, sizeof(opt)) != 0)
				            Log(TRACE_MIN, -1, "Could not set SO_NOSIGPIPE for socket %d", *sock);
            #endif

            // Blocking attempt to connect, return 0 if connected ok
             rc = connect(mysock, pSockAddress, sockAddressSize);
		}
	}

	if (mysock == INVALID_SOCKET)
    {        
        DebugTrace("INVALID_SOCKET ERROR\n"); 
		return rc;
    }

    if (0 == rc)
    {
	    setsockopt(mysock, SOL_SOCKET, SO_RCVTIMEO, (char*)&rxTimeoutMs, sizeof(unsigned int));
        pSockWin->sock = mysock;
    }
    else
    {
        DebugTrace("ERROR: Socket can't connect\n"); 
        pSockWin->sock = 0;
    }

    return pSockWin->sock;
}


/******************************************************************************
**    
**      @param[in, out] pCtx The SocketWin object
**              @return error code from closesocket
**  
**  @brief
**         Description: Helper to Close a socket
**
**  @todo 
**          Basically moved from the sample without changes, should accomodate 
**          same usage for 'sock' for clarity, removing indirections.
**
******************************************************************************/
static int SocketWinCloseSocket(void* pCtx)
{
    int rc;
    SOCKET_WIN* pSockWin = (SOCKET_WIN*)pCtx;

    rc = shutdown(pSockWin->sock, SHUT_OPTIONS);
    rc = recv(pSockWin->sock, NULL, (size_t)0, 0);
    rc = closesocket(pSockWin->sock);

    pSockWin->sock = 0;
    return rc;
}

/******************************************************************************
**    
**      @param[in, out] pCtx The SocketWin object
**              @return error code from WSACleanup
**  
**  @brief
**         Description: Close WinSock
**
**
******************************************************************************/
static int SocketWinCloseConnection(void* pCtx)
{
    int rc;

    rc = WSACleanup();
    return rc;
}

/**
 * @}
 */
