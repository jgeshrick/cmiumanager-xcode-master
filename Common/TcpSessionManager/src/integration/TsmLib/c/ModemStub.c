/******************************************************************************
*******************************************************************************
**
**         Filename: ModemStub.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
 
/**
 * @addtogroup ModemStub
 * @{
 */

/*!***************************************************************************
 * @file  modemStub.c
 *
 * @brief  Handles the connect on a modem (example stub code)
 * @author Duncan Willis
 * @date 2015.03.19
 * @version 1.0
 *****************************************************************************/
 
 
/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <stdio.h>
#include <string.h>

#include "TransportInterface.h"
#include "DebugTrace.h"
#include "ModemStub.h"


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/


// The interface.
static int32_t ModemCommand(void* pCtx, E_CONNECTION_COMMAND cmd, void* cmdParams);
static void ModemTick(void* pCtx,  uint32_t tickIntervalMs);
static E_CONNECTION_STATE  ModemGetState(void* pCtx, E_QUERY query, void* pQueryResult);
static int32_t ModemSend(void* pCtx, const uint8_t* txData, uint32_t txCount);
static int32_t ModemRecv(void* pCtx, uint8_t* rxBuffer, uint32_t bufferSize);


static int ModemOpenConnectionHelper(void* pCtx);
static int ModemOpenSocketHelper(void* pCtx);
static int ModemCloseSocket(void* pCtx);
static int ModemCloseConnection(void* pCtx);


static const TRANSPORT_INTERFACE modemInterface =
{
    ModemStubInit,
    ModemCommand,
    ModemTick,
    ModemGetState,
    ModemRecv,
    ModemSend,
};


/**
* Ringbuffers
* These need to be seen both by the UART ISR, and by the modem object, which will 
* keep a reference to them
*/
#define RING_BUFFER_SIZE_RX 4096  // depends on tick rate / data rate etc.
#define RING_BUFFER_SIZE_TX 4096

static uint8_t rxData[RING_BUFFER_SIZE_RX];
static uint8_t txData[RING_BUFFER_SIZE_TX];
static RingBufferT rxRingBuffer;   // Buffer for data FROM cloud
static RingBufferT txRingBuffer;   // Buffer for data TO cloud


/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/


/******************************************************************************
**
**     @param[in, out] pCtx The modem driver 
**          @param[in] initParams Optional pointer to init params
**             @return None
**  @brief
**         Description: Init the object - this should not block. Do any lengthy tasks
**                      as a command.
**
******************************************************************************/
int32_t ModemStubInit(void* pCtx, const void* initParams)
{
    int32_t e = OK_STATUS;
    MODEM_STUB* pModem = (MODEM_STUB*)pCtx;

    pModem->pInterface = &modemInterface;
    pModem->cmd = CON_CMD_NULL;

    /// @todo Put non-blocking inits here (eg clearing buffers, setting up 
    /// the object.
    

    // Note: This reference means that the Modem must be a singleton 
    // - the limitation being that the UART ISR needs access to the 
    // ringbuffers, so they must be outside the modem classs.
    pModem->pRxRingBuffer = &rxRingBuffer;
    pModem->pTxRingBuffer = &txRingBuffer;
    RingBufferInit(pModem->pRxRingBuffer, rxData, RING_BUFFER_SIZE_RX);
    RingBufferInit(pModem->pTxRingBuffer, txData, RING_BUFFER_SIZE_TX);

    /// @note ! Prefill rx buffer with some dummy data as an example
    RingBufferWrite(pModem->pRxRingBuffer, (const uint8_t*)"abc", 4);
    
    return e;
}

/******************************************************************************
**    
**     @param[in, out] pCtx The modem driver
**          @param[in] cmd The command enumeration
**          @param[in] pCmdParamms Optional pointer to static data (may be null)
**             @return OK_STATUS if command accepted, else error code.
**  @brief
**         Description: Run a command (eg CONNECT)
**
******************************************************************************/
static int32_t ModemCommand(void* pCtx, E_CONNECTION_COMMAND cmd, void* pCmdParams)
{
    int32_t r = OK_STATUS;

    MODEM_STUB* pModem = (MODEM_STUB*)pCtx;

    /// @todo - check that the cmd is allowable, or not already busy for example
    pModem->cmd = cmd;
    pModem->pCmdParams = pCmdParams;

    return r;
}

/******************************************************************************
**    
**     @param[in, out] pCtx The modem driver
**          @param[in] tickIntervalMs The tick time in ms
**             @return None
**  @brief
**         Description: Run the state machine - must not block.  
**
******************************************************************************/
static void ModemTick(void* pCtx,  uint32_t tickIntervalMs)
{
       MODEM_STUB* pModem = (MODEM_STUB*)pCtx;
       
       switch (pModem->state)
       {
       case E_MS_RESET:
           switch (pModem->cmd)
           {
           case CON_CMD_NULL:
               break;

           case CON_CMD_OPEN_CONNECTION:
               pModem->cmd = CON_CMD_NULL;
               pModem->state = E_MS_BEGIN_OPEN_CONNECTION;

           case CON_CMD_OPEN_SOCKET:
               pModem->cmd = CON_CMD_NULL;
               pModem->state = E_MS_BEGIN_OPEN_SOCKET;
               break;

           case CON_CMD_CLOSE_SOCKET:              
               pModem->cmd = CON_CMD_NULL;
               pModem->state = E_MS_BEGIN_CLOSE_SOCKET;
               break;

           case CON_CMD_CLOSE_CONNECTION:            
               pModem->cmd = CON_CMD_NULL;
               pModem->state = E_MS_BEGIN_CLOSE_CONNECTION;
               break;

           default:
               pModem->cmd = CON_CMD_NULL;
               break;
           }
           break;

       case E_MS_BEGIN_OPEN_CONNECTION:
            ModemOpenConnectionHelper(pCtx);
            pModem->state = E_MS_OPEN_CONNECTION_WAIT;
            break;       

       case E_MS_OPEN_CONNECTION_WAIT:
            // wait for conn if needed
            pModem->state = E_MS_RESET;
            break;

       case E_MS_BEGIN_OPEN_SOCKET:
            ModemOpenSocketHelper(pCtx);
            pModem->state = E_MS_OPEN_SOCKET_WAIT;
            break;       
       
       case E_MS_OPEN_SOCKET_WAIT:
            // wait for socket if needed
            pModem->state = E_MS_RESET;
            break;
             
       case E_MS_BEGIN_CLOSE_SOCKET:
            ModemCloseSocket(pCtx);
            pModem->state = E_MS_CLOSE_SOCKET_WAIT;
            break;       
       
       case E_MS_CLOSE_SOCKET_WAIT:
            // wait for socket to close if needed
            pModem->state = E_MS_RESET;
            break;
            
       case E_MS_BEGIN_CLOSE_CONNECTION:
            ModemCloseConnection(pCtx);
            pModem->state = E_MS_CLOSE_CONNECTION_WAIT;
            break;       
       
       case E_MS_CLOSE_CONNECTION_WAIT:
            // wait for connection to close if needed
            pModem->state = E_MS_RESET;
            break;

       default:
           ASSERT(0);
           break;
       }
}

/******************************************************************************
**    
**     @param[in, out] pCtx The modem driver
**          @param[in] query An enummeration to get a query type
**         @param[out] pQueryResult Optional structure to be filled in 
**                                  with the query results
**             @return None
**  @brief
**         Description: Run the state machine - must not block
**
******************************************************************************/
static E_CONNECTION_STATE  ModemGetState(void* pCtx, E_QUERY query, void* pQueryResult)
{
    MODEM_STUB* pModem = (MODEM_STUB*)pCtx;

    switch (query)
    {
    case QRY_CONNECTION_STATE:

        break;

    case QRY_GET_RSSI:
        ///@ todo - example. ((RSSI_RESULT*)pQueryResult)->value = -99;
        // Should not block here. The value should have been previously obtained by
        // issuing a command, and waiting for its completion.
        break;

    default :
        ASSERT(0);
        break;
    }

    (void)pModem;
    
    // This stub code always returns OPEN
    return CON_STATE_SOCKET_OPEN;
}

/******************************************************************************
**    
**     @param[in, out] pCtx The modem driver
**          @param[in] txData The data to send
**          @param[in] txCount The number of bytes to send.      
**             @return OK or error code
**  @brief
**         Description: Send data (typically non blocking - add to ringbuffer)  
**
******************************************************************************/
static int32_t ModemSend(void* pCtx, const uint8_t* txData, uint32_t txCount)
{
	MODEM_STUB* pModem = (MODEM_STUB*)pCtx;
    int32_t rc = 0;

    /// Copy the data to a ringbuffer so the UART can send it via an ISR
    if (txCount > 0)
    {
        RingBufferWrite(pModem->pTxRingBuffer, txData, txCount);
    }

    if (RingBufferHasOverflowed(pModem->pTxRingBuffer))
    {
        rc = -1;
    }

	return rc;
}



/******************************************************************************
**    
**      @param[in, out] pCtx The modem driver
**          @param[out] rxBuffer The buffer to copy into
**           @param[in] bufferSize The number of bytes to send.     
**              @return Return -1 for error,
**                      0 for no data (non block)
**                      N for N bytes copied into the supplied buffer
**  @brief
**         Description: Non-blocking receive function. 
**
******************************************************************************/
static int32_t ModemRecv(void* pCtx, uint8_t* rxBuffer, uint32_t bufferSize)
{
    MODEM_STUB* pModem = (MODEM_STUB*)pCtx;
    uint32_t rxCount = 0;

    // Data in ring buf to be read?
    rxCount = RingBufferUsed(pModem->pRxRingBuffer);

    // Limit transfer to size of supplied buffer
    if (rxCount > bufferSize) 
    {
        rxCount = bufferSize;
    }

    // Copy data if there is any.
    if (rxCount > 0)
    {
        RingBufferRead(pModem->pRxRingBuffer, rxBuffer, rxCount);
    }

    if ( rxCount > 0 )
    {
        DebugTrace("MODEM Bytes received: %d\n", rxCount);
    }
 

    return (int32_t)rxCount;
}


// Implementation stub function
static int32_t ModemOpenConnectionHelper(void* pCtx)
{
    MODEM_STUB* pModem = (MODEM_STUB*)pCtx; 
    CONNECTION_PARAMS* pCp = (CONNECTION_PARAMS*)(pModem->pCmdParams);
    ASSERT(NULL != pCp);

    DebugTrace("MODEM OPEN DATA CONNECTION");  

    // Stub - In this function, start the action to begin connection.
    // The ticked state machine then needs to send AT commands etc, and poll until connection made. 
    return 0;
}


// Implementation stub function
static int32_t ModemOpenSocketHelper(void* pCtx)
{
    MODEM_STUB* pModem = (MODEM_STUB*)pCtx; 
    CONNECTION_PARAMS* pCp = (CONNECTION_PARAMS*)(pModem->pCmdParams);
    ASSERT(NULL != pCp);

    DebugTrace("MODEM OPEN SOCKET %s %d\n", pCp->host, pCp->port);  

    // Stub - In this function, start the action to begin connection.
    // The ticked state machine then needs to send AT commands etc, and poll until connection made. 
    return 0;
}


// Implementation stub function
static int32_t ModemCloseSocket(void* pCtx)
{
    int32_t rc = OK_STATUS;
    DebugTrace("MODEM CLOSE SOCKET");  

    // Stub - In this function, start the action to begin socket close.
    // The ticked state machine then needs to send AT commands etc, and poll until socket closed.
    return rc;
}


// Implementation stub function
static int32_t ModemCloseConnection(void* pCtx)
{
    int32_t rc = OK_STATUS;
    DebugTrace("MODEM CLOSE CONNECTION");  

    // Stub - In this function, start the action to begin disconnection from the modem.
    // The ticked state machine then needs to send AT commands etc, and poll until connection closed.
    return rc;
}


#if 0 // pseudo code for uart ISRs
void RxISR(void)
{
    // on rx char....
     RingBufferWrite(&rxRingBuffer, (const uint8_t*)&uartRxChar, 1);
}

void TxISR(void)
{
    // on uart has space in tx register...
     RingBufferRead(&txRingBuffer,  &uartTxChar, 1);
}
#endif
