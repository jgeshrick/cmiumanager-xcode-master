/******************************************************************************
*******************************************************************************
**
**         Filename: HttpServerTestApplet.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

 
/**
 * @addtogroup HttpServerTestApplet  
 * @{
 */


/*!***************************************************************************
 * @file  HttpServerTestApplet.c
 *
 * @brief Sample app for use by C# test harness and as interface for Python
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/

 
/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include "CommonTypes.h"
#include "Ringbuffer.h"
#include "HttpServer.h"
#include "DebugTrace.h"
#include "HttpServerTestApplet.h" 


/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/
 


static HTTP_SERVER httpServer;
static DATA_PROVIDER dp = { NULL, 0 };

#define RX_RB_SIZE  4096
#define TX_RB_SIZE  4096
static RingBufferT  rbRx;
static RingBufferT  rbTx;
static uint8_t      txBuf[TX_RB_SIZE];
static uint8_t      rxBuf[RX_RB_SIZE];

static bool         isInitialised = false;


/******************************************************************************
**    
**      @param None 
**              @return None
**  
**  @brief
**         Description:  Inits the server with the data objects
******************************************************************************/
DLL_EXPORT
void HttpServerTestAppletInit(void)
{
 
    HTTP_SERVER_INIT_PARAMS ip;
    RingBufferInit(&rbTx, txBuf, sizeof(txBuf));
    RingBufferInit(&rbRx, rxBuf, sizeof(rxBuf));
    
    ip.pRbRx = &rbRx;
    ip.pRbTx = &rbTx;


    // Set to NULL for no file system. 
    // Otherwise, the requested file must be present on this machine (absolute path eg c:\work\myfile.hex)
    /*  ip.dataProvider = NULL; */ // use fake data
    ip.dataProvider = &dp;  // use file system
    ip.dataProvider = NULL;// &dp;  // use file system


    HttpServerInit(&httpServer, &ip); 

    isInitialised = true;
}


/******************************************************************************
**    
**      @param[in] tickIntervalMs The interval between polls in ms (unused)
**              @return None
**  
**  @brief
**         Description: Poll loop to read the data stream for HTTP Requests
**         Continue reading bytes while available (do not block)
**         Drop out when no bytes available from stream
**         Handle Responses when a request is found
******************************************************************************/
DLL_EXPORT 
void HttpServerTestAppletTick(uint32_t tickIntervalMs)
{
    if (isInitialised)
    {
        HttpServerProcess(&httpServer, tickIntervalMs);
    }
}


/******************************************************************************
**
**      @param[in] buffer - Data (request) from BTLE or UART etc via Uart controller
**      @param[in] numBytes - Number of bytes in buffer
**              @return None
**
**  @brief
**         Description: Use this to put incoming requests into the ringbuf
**        for processing.
******************************************************************************/
DLL_EXPORT
void HttpServerReceiveData(const uint8_t* buffer, uint32_t numBytes)
{
    RingBufferWrite(httpServer.pRbRx, buffer, numBytes);
}


/******************************************************************************
**
**  @param data. A buffer to receive the response data into, to be sent over BTLE 
**  @param arraySize The size of the   buffer
**
**  @eturn The number of chars copied into the buffer (0 if none available)
**
**  @brief This should be polled by the main app, and then sent out over btle via 
** uart controller
******************************************************************************/
DLL_EXPORT uint32_t HttpServerGetResponseBytes(unsigned char* data, uint32_t arraySize)
{
    uint32_t txCount = 0;
    RingBufferT* pRb = httpServer.pRbTx;
     
        // Data in ring buf to send as response?
        txCount = RingBufferUsed(pRb);

        if (txCount > arraySize)
        {
            txCount = arraySize;
        }

        if (txCount > 0)
        {
            RingBufferRead(pRb, data, txCount);
        }
  
    return txCount;
}


/******************************************************************************
**
**      @param[in]
**              @return None
**
**  @brief
**         Description: Test wrapper for create client request
******************************************************************************/
DLL_EXPORT
void HttpServerTestGenerateRequest(
const char* url,
const char* hostName,
uint32_t firstRangeByte,
uint32_t lastRangeByte)
{
    static uint8_t buffer[4096];
    uint32_t bytesToSend = 0;

    (void)HttpMakeGetRequestHelper(buffer, sizeof(buffer),
        url,
        hostName,
        firstRangeByte,
        lastRangeByte);

    bytesToSend = strlen((const char*)buffer);

    RingBufferWrite(httpServer.pRbRx, buffer, bytesToSend);
}


/**
 * @}
 */
