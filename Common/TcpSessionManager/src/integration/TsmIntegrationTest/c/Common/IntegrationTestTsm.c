/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @defgroup TsmIntegrationTest
 *
 * @brief Integration Test for the @ref TCP Session Manager
 *
 */

/**
 * @addtogroup Integration Test
 * @{
 */

/**
 * @addtogroup Test 
 * @{
 */

/**
 * @file
 *
 * @brief Integration Test runner for the @ref TCP SessionManager (Tsm)
 * @author Duncan Willis
 * @date 2015.03.05  
 * @version 1.0
 */


#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "unity.h"
#include "TeamCity.h"
#include "DebugTrace.h"
#include "MqttManager.h"
#include "TransportManager.h"
#include "TcpSessionManager.h"
#include "IntegrationTestTsm.h"


 // Suppress warnings pertaining to Keil only.
#ifndef _WIN32
#pragma push
#pragma diag_suppress 167 // Keil args of function pointer do not match
#endif 

// These h/w specificfunctions must be supplied in the IntegrationTestWindows.c
// or IntegrationTest<platform>.c file.
extern uint32_t ClockTickGetMs(void); // Get a 32 bit number incrementing at TICK_CLOCKS_PER_SEC 
extern void TestPutc(int c);          // putc a char on debug output


#define LOOPBACK_TOPIC          "loopbk_topic"
#define LOOPBACK_TEST_STRING    "loopback Test String"
#define TICK_INTERVAL_MS        10 

static uint32_t                 testsRunCount = 0;
static bool                     testCallbackOk = false;
static bool doSendPubFromCallback = false;

static void callbackOnCompletedSession(TCP_SESSION_MANAGER* pSm, MQTT_MANAGER* pM);

static uint32_t sessionCallbacksExpected = 0; // == number of test sessions
static uint32_t sessionCallbacksCalled = 0;

/**
** @brief The object that manages the overall session, state machine example
            -- This is the "demo app" to illustrate the use of the other classes
**/
static TCP_SESSION_MANAGER      sm;


/**
** @brief The object that runs the MQTT protocol (Wrapper around Paho)
**/
static MQTT_MANAGER             mm;


/**
** @brief The object that manages the interface to the modem or socket - device INDEPENDENT interface
**/
TRANSPORT_MANAGER               transportManager;


/**
** @brief The MQTT init vars (for future use)
**/
MQTT_MANAGER_INIT_PARAMS        mqttInitParams = {0};


/**
** @brief The IP of the server, anything specific for connection.
**/
CONNECTION_PARAMS               connectionParams;


static void mainSetup (const char* ip, uint16_t port);
static void onPublishMessageReceivedCallback(const MQTT_MANAGER_PUB* pPubMsg);

static void TestHelper (MQTT_MANAGER_SESSION_PARAMS* pSp,  
                        MQTT_MANAGER_SESSION_RESULTS* pSr);

static void ProcessTest (void);
static void DelayBlocking(uint32_t delayMs);
void setUp(void)    {;}
void tearDown(void) {;}


/**
** Runs the unit tests for module TCP Session Manager
**/
int32_t IntegrationTestsRunner(const char* ipAddr, uint16_t port)
{
    int testFailures = 0;

    Unity.TestFile = "IntegrationTestTsm.c";
    UnityBegin();

    printf("Testing module %s\n", Unity.TestFile);
    
    // One time init
    mainSetup(ipAddr,   port);

    // General tests
    RUN_TEST(Tsm_TestBasicLoopback,                     __LINE__);
    RUN_TEST(Tsm_TestEmptyTransaction,                  __LINE__);
    RUN_TEST(Tsm_TestNoSendButExpectAPub,               __LINE__);
    RUN_TEST(Tsm_TestPublishNoSubscribe,                __LINE__);
    RUN_TEST(Tsm_TestPublishNoSubscribeTwice,           __LINE__);
    RUN_TEST(Tsm_TestSubscribeNoPublish,                __LINE__);
    RUN_TEST(Tsm_TestPubAndPuback,                      __LINE__);
    RUN_TEST(Tsm_TestPubAndPubackMultiple,              __LINE__);
    RUN_TEST(Tsm_TestPubAndPubackMultipleWithCallback,  __LINE__);    
    RUN_TEST(Tsm_TestPubAndPubackHandling,  __LINE__);

    // Test sending a pub from within rx pub callback 
    RUN_TEST(Tsm_TestPublishNoSubscribeAndPublishAgain, __LINE__);

    // Final test
    RUN_TEST(Tsm_TestNumberOfSessionsCallbacks,         __LINE__);
   
    testFailures = UnityEnd();
    
    return testFailures;
}


//
// Set up for MQTT  Integration tests
// This needs to be done only once at init time
//
void mainSetup (const char* ipAddr, uint16_t port)
{
    // Set address of the MQTT server in Transport
    TransportManagerInitConnectionParams(&connectionParams, 
        ipAddr, 
        port);  

    // Init TSM to given transport
    TcpSessionManagerInit(&sm, 
        &transportManager,
        &connectionParams);
}


// Test 1. Basic session. Subscribe to a loopback topic. Publish to it and then 
// check we received it back 
void Tsm_TestBasicLoopback(void)
{
    int32_t testResultCode = -1;
    MQTT_MANAGER_SESSION_PARAMS     sessionParams;
    MQTT_MANAGER_SESSION_RESULTS    sessionResults;
    
    MQTT_MANAGER_SUBSCRIPTION       subscriptionLoopback = {0};
    MQTT_MANAGER_PUB                pubLoopback = {0};

    MqttManagerInitSessionParams(&sessionParams);
    MqttManagerInitSessionResults(&sessionResults);
      
    // Add a sub:
    subscriptionLoopback.topicFilter = LOOPBACK_TOPIC;
    MqttManagerAddSubscription(&sessionParams, &subscriptionLoopback);

    // Add a Pub:
    pubLoopback.topic               = subscriptionLoopback.topicFilter;
    pubLoopback.pData               = LOOPBACK_TEST_STRING;
    pubLoopback.dataLength          = strlen(LOOPBACK_TEST_STRING) + 1;
    pubLoopback.qos                 = E_MQTT_QOS_1;

    MqttManagerAddPublish(&sessionParams, &pubLoopback);

    // Expect to get none back at first
    sessionParams.expectedPubsToReceive = 0;

    //Run once with cleansession to clean session information
    sessionParams.cleansession = 1;
    TestHelper( &sessionParams, &sessionResults);

    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [CONNECT    ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [DISCONNECT ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [SUBSCRIBE  ]);    
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [PUBLISH    ]);  
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.receivedCounts   [SUBACK     ]);
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.receivedCounts   [PUBLISH    ]);

    //Run second time with cleansession cleared so that subscribe is remembered
    sessionParams.cleansession = 0;
    TestHelper( &sessionParams, &sessionResults);

    // Run third time to send and receive loopback message    
    // Expect to get one back (the loopback)
    sessionParams.expectedPubsToReceive = 1;
    sessionParams.cleansession = 0;
    TestHelper( &sessionParams, &sessionResults);
    
    testResultCode = testCallbackOk ? 0 : -1;
    DebugTrace("TEST RESULT = %d (%s)\n", testResultCode, (testResultCode == 0) ? "LOOPBACK Called OK":"FAIL");
    TEST_ASSERT_TRUE(testResultCode == 0);

    
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [CONNECT    ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [DISCONNECT ]);
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.sentCounts       [SUBSCRIBE  ]);    
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [PUBLISH    ]);  
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.receivedCounts   [SUBACK     ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.receivedCounts   [PUBLISH    ]);
}


// Test 2. No sub, no pub, Check state machine handles this case.
void Tsm_TestEmptyTransaction(void)
{
    MQTT_MANAGER_SESSION_PARAMS     sessionParams;
    MQTT_MANAGER_SESSION_RESULTS    sessionResults;

    MqttManagerInitSessionParams(&sessionParams);
    MqttManagerInitSessionResults(&sessionResults);

    sessionParams.expectedPubsToReceive = 0;

    TestHelper( &sessionParams, &sessionResults);
    
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts[CONNECT    ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts[DISCONNECT ]);
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.sentCounts[SUBSCRIBE  ]);    
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.sentCounts[PUBLISH    ]);
     
    // Note: We may or may not have received the CONNACK before we closed the connection
    // Therefore we cannot test its count.
}


// Test 3. No sub, no pub, but expect one PUB (which will not arrive)
void Tsm_TestNoSendButExpectAPub(void)
{      
    MQTT_MANAGER_SESSION_PARAMS     sessionParams;
    MQTT_MANAGER_SESSION_RESULTS    sessionResults;

    MqttManagerInitSessionParams(&sessionParams);
    MqttManagerInitSessionResults(&sessionResults);

    sessionParams.expectedPubsToReceive = 1;

    TestHelper( &sessionParams, &sessionResults);
    
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts[CONNECT    ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts[DISCONNECT ]);
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.sentCounts[SUBSCRIBE  ]);    
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.sentCounts[PUBLISH    ]);
    
    // Becasue we must have waited for a PUB, we must have seen the CONNACK arrive
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.receivedCounts[CONNACK     ]);
}


// Test4. No sub, one pub 
void    Tsm_TestPublishNoSubscribe(void)
{
    MQTT_MANAGER_SESSION_PARAMS     sessionParams;
    MQTT_MANAGER_SESSION_RESULTS    sessionResults;
    MQTT_MANAGER_PUB                pubLoopback = {0};

    MqttManagerInitSessionParams(&sessionParams);
    MqttManagerInitSessionResults(&sessionResults);
    sessionParams.cleansession = 1;

    // Add a Pub:
    pubLoopback.topic = LOOPBACK_TOPIC;
    pubLoopback.pData = LOOPBACK_TEST_STRING;
    pubLoopback.dataLength = strlen(LOOPBACK_TEST_STRING) + 1;

    MqttManagerAddPublish(&sessionParams, &pubLoopback);

    // Expect to get none back
    sessionParams.expectedPubsToReceive = 0;

    TestHelper( &sessionParams, &sessionResults);
    
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [CONNECT    ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [DISCONNECT ]);
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.sentCounts       [SUBSCRIBE  ]);    
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [PUBLISH    ]);  
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.receivedCounts   [SUBACK     ]);
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.receivedCounts   [PUBLISH    ]);
}


// Test5. No sub, one pub - Run twice without intermediate initialisation
void    Tsm_TestPublishNoSubscribeTwice(void)
{
    uint32_t numSessions;
    const uint32_t numSessionsToRun = 2;

    MQTT_MANAGER_SESSION_PARAMS     sessionParams;
    MQTT_MANAGER_SESSION_RESULTS    sessionResults;
    MQTT_MANAGER_PUB                pubLoopback = {0};  

    MqttManagerInitSessionParams(&sessionParams);
    sessionParams.cleansession = 1;

    // Add a Pub:
    pubLoopback.topic               = LOOPBACK_TOPIC;
    pubLoopback.pData               = LOOPBACK_TEST_STRING;
    pubLoopback.dataLength          = strlen(LOOPBACK_TEST_STRING) + 1;

    MqttManagerAddPublish(&sessionParams, &pubLoopback);

    // Expect to get none back
    sessionParams.expectedPubsToReceive = 0;

    for (numSessions = 0; numSessions < numSessionsToRun; numSessions++)
    {
        // need to clear down results first
        MqttManagerInitSessionResults(&sessionResults);
        TestHelper( &sessionParams, &sessionResults);

        TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [CONNECT    ]);
        TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [DISCONNECT ]);
        TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.sentCounts       [SUBSCRIBE  ]);    
        TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [PUBLISH    ]);  
        TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.receivedCounts   [SUBACK     ]);
        TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.receivedCounts   [PUBLISH    ]);
    }
}


// Sub but no pub
void    Tsm_TestSubscribeNoPublish(void)
{
    MQTT_MANAGER_SESSION_PARAMS     sessionParams;
    MQTT_MANAGER_SESSION_RESULTS    sessionResults;
    MQTT_MANAGER_SUBSCRIPTION sub = {0};

    MqttManagerInitSessionParams(&sessionParams);
    MqttManagerInitSessionResults(&sessionResults);

    //Set clean session to 1 so that broker will reply with no session present
    //This tells TSM to perform a subscription
    sessionParams.cleansession = 1;
      
    // Add a sub:
    sub.topicFilter = LOOPBACK_TOPIC;
    MqttManagerAddSubscription(&sessionParams, &sub);

    // Espect to get 0 back
    sessionParams.expectedPubsToReceive = 0;

    TestHelper(&sessionParams, &sessionResults);

    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [CONNECT    ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [DISCONNECT ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [SUBSCRIBE  ]);    
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.sentCounts       [PUBLISH    ]);  
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.receivedCounts   [SUBACK     ]);
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.receivedCounts   [PUBLISH    ]);
}



 
// Send one pub - expect PUBACK
void    Tsm_TestPubAndPuback(void)
{
    MQTT_MANAGER_SESSION_PARAMS     sessionParams;
    MQTT_MANAGER_SESSION_RESULTS    sessionResults;
    MQTT_MANAGER_PUB                pubLoopback = {0};

    MqttManagerInitSessionParams(&sessionParams);
    MqttManagerInitSessionResults(&sessionResults);

    // Add a Pub:
    pubLoopback.topic = "DummyTopic";
    pubLoopback.pData = "DummyData";
    pubLoopback.dataLength = strlen((const char*)(pubLoopback.pData)) + 1;
    pubLoopback.qos = E_MQTT_QOS_1;

    MqttManagerAddPublish(&sessionParams, &pubLoopback);

    // Expect to *receive* no PUBS
    sessionParams.expectedPubsToReceive = 0;

    TestHelper( &sessionParams, &sessionResults);
    
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [CONNECT    ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [DISCONNECT ]);
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.sentCounts       [SUBSCRIBE  ]);    
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [PUBLISH    ]);  
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.receivedCounts   [SUBACK     ]);
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.receivedCounts   [PUBLISH    ]);

    // Expect a PUBACK back.
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.receivedCounts   [PUBACK     ]);
}


 
// Send n pubs - expect n PUBACKs back
void    Tsm_TestPubAndPubackMultiple(void)
{  
    #define NUM_TEST_PUBS  4
    int i;
    MQTT_MANAGER_SESSION_PARAMS     sessionParams;
    MQTT_MANAGER_SESSION_RESULTS    sessionResults;
    MQTT_MANAGER_PUB                pubs[NUM_TEST_PUBS];

    MqttManagerInitSessionParams(&sessionParams);
    MqttManagerInitSessionResults(&sessionResults);

    for (i = 0; i < NUM_TEST_PUBS; i++)
    {
        pubs[i].topic = "Topic n";
        pubs[i].pData = "Data  n";
        pubs[i].dataLength = strlen((const char*)(pubs[i].pData)) + 1;
        pubs[i].qos = E_MQTT_QOS_1;
        pubs[i].onPopulate = NULL;
        MqttManagerAddPublish(&sessionParams, &pubs[i]);
    }

    // Expect to *receive* no PUBS
    sessionParams.expectedPubsToReceive = 0;

    TestHelper( &sessionParams, &sessionResults);
    
    TEST_ASSERT_EQUAL_UINT32(1,             sessionResults.sentCounts       [CONNECT    ]);
    TEST_ASSERT_EQUAL_UINT32(1,             sessionResults.sentCounts       [DISCONNECT ]);
    TEST_ASSERT_EQUAL_UINT32(0,             sessionResults.sentCounts       [SUBSCRIBE  ]);    
    TEST_ASSERT_EQUAL_UINT32(NUM_TEST_PUBS, sessionResults.sentCounts       [PUBLISH    ]);  
    TEST_ASSERT_EQUAL_UINT32(0,             sessionResults.receivedCounts   [SUBACK     ]);
    TEST_ASSERT_EQUAL_UINT32(0,             sessionResults.receivedCounts   [PUBLISH    ]);

    // Expect same no. of PUBACKs back.
    TEST_ASSERT_EQUAL_UINT32(NUM_TEST_PUBS, sessionResults.receivedCounts   [PUBACK     ]);
}


uint32_t publishCallback1(MQTT_MANAGER_PUB* pPub)
{
    static const char* workingBuffer = "Callback Scratch data 1";
    pPub->pData = (const uint8_t*)workingBuffer;

    return (uint32_t)strlen(workingBuffer);
}


// Send n pubs - expect n PUBACKs back, populate with data using callback
void    Tsm_TestPubAndPubackMultipleWithCallback(void)
{  
    #define NUM_TEST_PUBS  4
    int i;
    MQTT_MANAGER_SESSION_PARAMS     sessionParams;
    MQTT_MANAGER_SESSION_RESULTS    sessionResults;
    MQTT_MANAGER_PUB                pubs[NUM_TEST_PUBS];

    MqttManagerInitSessionParams(&sessionParams);
    MqttManagerInitSessionResults(&sessionResults);

    for (i = 0; i < NUM_TEST_PUBS; i++)
    {
        pubs[i].topic = "Topic n";
        pubs[i].pData = "This data should be ignored";
        pubs[i].dataLength = strlen((const char*)(pubs[i].pData)) + 1;
        pubs[i].qos = E_MQTT_QOS_1;
        pubs[i].onPopulate = publishCallback1;
        MqttManagerAddPublish(&sessionParams, &pubs[i]);
    }

    // Expect to *receive* no PUBS
    sessionParams.expectedPubsToReceive = 0;

    TestHelper( &sessionParams, &sessionResults);
    
    TEST_ASSERT_EQUAL_UINT32(1,             sessionResults.sentCounts       [CONNECT    ]);
    TEST_ASSERT_EQUAL_UINT32(1,             sessionResults.sentCounts       [DISCONNECT ]);
    TEST_ASSERT_EQUAL_UINT32(0,             sessionResults.sentCounts       [SUBSCRIBE  ]);    
    TEST_ASSERT_EQUAL_UINT32(NUM_TEST_PUBS, sessionResults.sentCounts       [PUBLISH    ]);  
    TEST_ASSERT_EQUAL_UINT32(0,             sessionResults.receivedCounts   [SUBACK     ]);
    TEST_ASSERT_EQUAL_UINT32(0,             sessionResults.receivedCounts   [PUBLISH    ]);

    // Expect same no. of PUBACKs back.
    TEST_ASSERT_EQUAL_UINT32(NUM_TEST_PUBS, sessionResults.receivedCounts   [PUBACK     ]);
}


// Send n pubs - test fo correct PubAck handling
void    Tsm_TestPubAndPubackHandling(void)
{  
    #define NUM_TEST_PUBS  3
    int i;
    MQTT_MANAGER_SESSION_PARAMS     sessionParams;
    MQTT_MANAGER_SESSION_RESULTS    sessionResults;
    MQTT_MANAGER_PUB                pubs[NUM_TEST_PUBS];

    MqttManagerInitSessionParams(&sessionParams);
    MqttManagerInitSessionResults(&sessionResults);

    for (i = 0; i < NUM_TEST_PUBS; i++)
    {
        pubs[i].topic = "Topic n";
        pubs[i].pData = "A Payload";
        pubs[i].dataLength = strlen((const char*)(pubs[i].pData)) + 1;
        pubs[i].qos = E_MQTT_QOS_1;
        pubs[i].onPopulate = NULL;
        MqttManagerAddPublish(&sessionParams, &pubs[i]);
    }
    
    // Force Pub no.2 to NOT require a PUBACK
    pubs[1].qos = E_MQTT_QOS_0;

    // Force Pub no.3 to NOT send anything.
    pubs[2].dataLength = 0;

    // Expect to *receive* no PUBS
    sessionParams.expectedPubsToReceive = 0;

    TestHelper( &sessionParams, &sessionResults);
    
    TEST_ASSERT_EQUAL_UINT32(1,                 sessionResults.sentCounts       [CONNECT    ]);
    TEST_ASSERT_EQUAL_UINT32(1,                 sessionResults.sentCounts       [DISCONNECT ]);
    TEST_ASSERT_EQUAL_UINT32(0,                 sessionResults.sentCounts       [SUBSCRIBE  ]);
    TEST_ASSERT_EQUAL_UINT32(NUM_TEST_PUBS - 1, sessionResults.sentCounts       [PUBLISH    ]);   // Pub [2] won't be sent.
    TEST_ASSERT_EQUAL_UINT32(0,                 sessionResults.receivedCounts   [SUBACK     ]);
    TEST_ASSERT_EQUAL_UINT32(0,                 sessionResults.receivedCounts   [PUBLISH    ]);

    // Expect 1 PUBACK  back, just for the one set to QoS 1, and which was actually sent 
    TEST_ASSERT_EQUAL_UINT32(1,                 sessionResults.receivedCounts   [PUBACK     ]);  
    
    TEST_ASSERT_TRUE(PUB_STATE_ACKED    ==      sessionResults.pubResults[0].pubState); // OK, QoS = 1, ack got.
    TEST_ASSERT_TRUE(PUB_STATE_SENT     ==      sessionResults.pubResults[1].pubState); // No ack as QoS == 0
    TEST_ASSERT_TRUE(PUB_STATE_NOT_SENT ==      sessionResults.pubResults[2].pubState); // Added to q, but never sent because len == 0
    TEST_ASSERT_TRUE(PUB_STATE_NOT_SENT ==      sessionResults.pubResults[3].pubState); 
    
    // Test accessor function
    TEST_ASSERT_TRUE ( MqttManagerWasPublishDelivered(&sessionResults, 0));
    TEST_ASSERT_FALSE( MqttManagerWasPublishDelivered(&sessionResults, 1));
    TEST_ASSERT_FALSE( MqttManagerWasPublishDelivered(&sessionResults, 2));
    TEST_ASSERT_FALSE( MqttManagerWasPublishDelivered(&sessionResults, 3)); 
}



// No sub, one pub , and do another Pub in the rx callback.
void    Tsm_TestPublishNoSubscribeAndPublishAgain(void)
{
    int32_t testResultCode = -1;
    MQTT_MANAGER_SESSION_PARAMS     sessionParams;
    MQTT_MANAGER_SESSION_RESULTS    sessionResults;
    
    MQTT_MANAGER_SUBSCRIPTION       subscriptionLoopback = {0};
    MQTT_MANAGER_PUB                pubLoopback = {0};

    MqttManagerInitSessionParams(&sessionParams);
    MqttManagerInitSessionResults(&sessionResults);
      
    // Add a sub:
    subscriptionLoopback.topicFilter = LOOPBACK_TOPIC;
    MqttManagerAddSubscription(&sessionParams, &subscriptionLoopback);

    // Add a Pub:
    pubLoopback.topic               = subscriptionLoopback.topicFilter;
    pubLoopback.pData               = LOOPBACK_TEST_STRING;
    pubLoopback.dataLength          = strlen(LOOPBACK_TEST_STRING) + 1; // Include null term so compare works
    pubLoopback.qos                 = E_MQTT_QOS_1;

    MqttManagerAddPublish(&sessionParams, &pubLoopback);

    // Expect to get none back at first
    sessionParams.expectedPubsToReceive = 0;

    // Run once with cleansession to clean session information
    sessionParams.cleansession = 1;
    TestHelper( &sessionParams, &sessionResults);

    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [CONNECT    ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [DISCONNECT ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [SUBSCRIBE  ]);    
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [PUBLISH    ]);  
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.receivedCounts   [SUBACK     ]);
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.receivedCounts   [PUBLISH    ]);

    //Run second time with cleansession cleared so that subscribe is remembered
    sessionParams.cleansession = 0;
    TestHelper( &sessionParams, &sessionResults);

    // Run third time for Pub being added from within the Rx Pub callback 
    sessionParams.expectedPubsToReceive = 1;

    doSendPubFromCallback = true;
    TestHelper( &sessionParams, &sessionResults);    
    doSendPubFromCallback = false;

    testResultCode = testCallbackOk ? 0 : -1;
    DebugTrace("TEST RESULT = %d (%s)\n", testResultCode, (testResultCode == 0) ? "LOOPBACK Called OK":"FAIL");
    TEST_ASSERT_TRUE(testResultCode == 0);
    
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [CONNECT    ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.sentCounts       [DISCONNECT ]);
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.sentCounts       [SUBSCRIBE  ]);    
    TEST_ASSERT_EQUAL_UINT32(2,     sessionResults.sentCounts       [PUBLISH    ]);  
    TEST_ASSERT_EQUAL_UINT32(0,     sessionResults.receivedCounts   [SUBACK     ]);
    TEST_ASSERT_EQUAL_UINT32(1,     sessionResults.receivedCounts   [PUBLISH    ]);
    TEST_ASSERT_EQUAL_UINT32(2,     sessionResults.receivedCounts   [PUBACK     ]);
}



// Run this last - tests number of callbacks received in entire test
void    Tsm_TestNumberOfSessionsCallbacks(void)
{
    TEST_ASSERT_EQUAL_UINT32(sessionCallbacksExpected,     sessionCallbacksCalled);
}
    
    
/******************************************************************************
**    
**           @param[in] pSm The TcpSessionManager
**           @param[in] pM The MQTT client
**          @param[out]  
**              @return None
**  @brief
**         Description: Callback when the MQTT Get completes
**
******************************************************************************/
static void callbackOnCompletedSession(TCP_SESSION_MANAGER* pSm, MQTT_MANAGER* pM)
{   
    sessionCallbacksCalled++;
    DebugTrace("callbackOnCompletedSession called: Count = %u\n", sessionCallbacksCalled);
}


//
// Does the test with the given objects (previously set up)
// This undertakes running of a session with the 
// given test params, and storage for results.
static void TestHelper (MQTT_MANAGER_SESSION_PARAMS* pSessionParams,  
                        MQTT_MANAGER_SESSION_RESULTS* pSessionResults)
{ 
    // Init the MQTT Manager 
    mqttInitParams.pfOnPublishRxCallback = onPublishMessageReceivedCallback;
    MqttManagerInit(&mm, &mqttInitParams, pSessionParams, pSessionResults);   

    // Prepare the TSM to run the MqttManager
    TcpSessionManagerBegin(&sm,     // The TSM
        (void*)&mm,                 // The Mqtt manager
        &MqttManagerBegin,          // MQTT Manager Begin function
        &MqttManagerRun,            // MQTT Manager Run Function
        callbackOnCompletedSession); 

    ProcessTest();
    testsRunCount++;

    // Allow time for trace buffer to flush
    DelayBlocking(250);
}


//
// Called by MQTT Manager on receive of PUBLISH message.
// Here, we check we received  what we published.
static void onPublishMessageReceivedCallback(const MQTT_MANAGER_PUB* pPubMsg)
{
    int c1;
    int c2;
    
   static MQTT_MANAGER_PUB  pubChained = {0};  
    
    MQTT_MANAGER_SESSION_PARAMS*     pSessionParams = ( MQTT_MANAGER_SESSION_PARAMS*) mm.pSessionParams;

    c1 = strcmp(LOOPBACK_TOPIC,         pPubMsg->topic);
    c2 = strcmp(LOOPBACK_TEST_STRING,   (const char*)(pPubMsg->pData));

    if ((0 == c1) && (0 == c2))
    {
        testCallbackOk = true;
    }
    else
    {
        testCallbackOk = false;
    }     
 
    if (doSendPubFromCallback)
    {
        // Add a Pub:
        pubChained.topic        = LOOPBACK_TOPIC;
        pubChained.pData        = "CHAINED";
        pubChained.dataLength   = strlen("CHAINED");    
        pubChained.qos          = E_MQTT_QOS_1;

        MqttManagerAddPublish(pSessionParams, &pubChained);
    }

   TEST_ASSERT_TRUE(testCallbackOk);
}


//
// Start and Tick a TCPSession. Blocks until complete.
//
static void ProcessTest (void)
{
    int32_t isBusy = 1;

    // Clear the status for callback test
    testCallbackOk = false;
     
    while (isBusy)
    {
        // Blocking delay and trace handling
        DelayBlocking(TICK_INTERVAL_MS);

        // Tick the process
        TcpSessionManagerTick(&sm, TICK_INTERVAL_MS);

        // Break out when session done.
        isBusy = TcpSessionManagerIsBusy(&sm);
    }

    sessionCallbacksExpected++;
}


// Delay a fixed amaount - blocking
// Might as well do trace buffer handling here too in "background".
// The function is platform independent by using two externally linked functions.
// ClockTickGetMs() and TestPutc()
static void DelayBlocking(uint32_t delayMs)
{
    uint32_t tEnd;
    uint32_t tNow;
    uint32_t clocksDivider = 1000 / delayMs;
    int32_t i;
    int32_t nCh = 0;
    static uint8_t traceBuf[256];

    tNow =  ClockTickGetMs();
    tEnd = tNow + (TICK_CLOCKS_PER_SEC / clocksDivider);

    do 
    {
        // Handle trace passthrough
        nCh = DebugTraceGetChars(traceBuf, sizeof(traceBuf));
        for (i = 0; i < nCh; i++)
        {
            TestPutc(traceBuf[i]);
        }

       tNow =  ClockTickGetMs();
    }
    while (tNow < tEnd); 
}

#ifndef _WIN32
#pragma pop
#endif

/**
 * @}
 * @}
 */
