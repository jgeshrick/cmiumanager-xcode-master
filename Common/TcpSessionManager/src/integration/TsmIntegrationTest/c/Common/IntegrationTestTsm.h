/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/


/**
 * @addtogroup IntegrationTest
 * @{
 */

/**
 * @addtogroup TsmIntegrationTest
 * @{
 */

/**
 * @file
 *
 * @brief Integration Test for the @ref TCP Session Manager
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 */


#ifndef _TSM_INT_TEST_H__
#define _TSM_INT_TEST_H__

// Some popular MQTT servers for testing
#define MQTT_PORT                       1883
#define MQTT_LOCAL_TEST_SERVER          "mqttTestUbuntu.cloudapp.net" 
#define MQTT_ECLIPSE_SERVER             "iot.eclipse.org"

#define MQTT_AWS_SERVER		            "auto-test.mdce-nonprod.neptunensight.com"

#define DEFAULT_INTEGRATION_TEST_SERVER MQTT_AWS_SERVER
#define DEFAULT_INTEGRATION_TEST_PORT   MQTT_PORT


int32_t IntegrationTestsRunner(const char* ipAddr, uint16_t port);
void    Tsm_TestBasicLoopback(void);
void    Tsm_TestEmptyTransaction(void);
void    Tsm_TestNoSendButExpectAPub(void);
void    Tsm_TestPublishNoSubscribe(void);
void    Tsm_TestPublishNoSubscribeTwice(void);
void    Tsm_TestSubscribeNoPublish(void);
void    Tsm_TestPubAndPuback(void);
void    Tsm_TestPubAndPubackMultiple(void);
void    Tsm_TestPubAndPubackMultipleWithCallback(void);
void    Tsm_TestPubAndPubackHandling(void);
void    Tsm_TestPublishNoSubscribeAndPublishAgain(void);
void    Tsm_TestNumberOfSessionsCallbacks(void);

#endif

/**
 * @}
 * @}
 */
