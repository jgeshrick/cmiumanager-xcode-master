/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @defgroup TsmIntegrationTest
 *
 * @brief Integration Test for the @ref TCP Session Manager
 *
 */

/**
 * @addtogroup Integration Test
 * @{
 */

/**
 * @addtogroup Test 
 * @{
 */

/**
 * @file
 *
 * @brief Integration Test runner for the @ref TCP SessionManager (Tsm)
 * @author Duncan Willis
 * @date 2015.03.05  
 * @version 1.0
 */
 
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "unity.h"
#include "TeamCity.h"
#include "IntegrationTestTsm.h"
#include "TransportManager.h"
#include "SocketWin.h"
#include "DebugTrace.h"


extern void setUp(void);
extern void tearDown(void);
extern TRANSPORT_MANAGER transportManager;

/**
** @brief The platform specific driver
**/
static SOCKET_WIN socketWin; 


/** This function is used by unity */
void resetTest(void)
{
    tearDown();
    setUp();
}


/**
** Platform dependent:
** This function must return a 32bit number incrementing at TICK_CLOCKS_PER_SEC (is 1ms for Win on PC)
**/
uint32_t ClockTickGetMs(void)
{
    return (uint32_t)clock();
}


/**
** Platform dependent:
** This function must output a char from a serial port or other test output
**/
void TestPutc(int c)
{
    putchar(c);
}


/**
** Runs the integration tests for module TCP Session Manager - MQTT session on the given IP broker
** 
** Command line args Usage :  IntegrationTestTsm [IP addr] [Port]
**  Example:  IntegrationTestTsm 52.12.12.34 1883
** If args are not supplied the default value MQTT_AWS_SERVER:1883 is used
**/
int main(int argc, char*argv[])
{
    int testFailures = 0;
    char ipAddressString[128] = {0};
    
    uint16_t mqttPort = DEFAULT_INTEGRATION_TEST_PORT;
    strncpy (ipAddressString, DEFAULT_INTEGRATION_TEST_SERVER, sizeof(ipAddressString) - 1);

    // Note by convention argv[0] is the prog name
    if ((argc > 1) && (argv[1] != NULL))
    {
         strncpy (ipAddressString, argv[1], sizeof(ipAddressString) - 1);

         if ((argc > 2) && (argv[2] != NULL))
         {
             mqttPort = (uint16_t)atol(argv[2]);
         }
    }

    DebugTrace("Integration Test target = %s:%u\n", ipAddressString, mqttPort);

    // Platform dependent - on Windows use Winsock...
    SocketWinInit(&socketWin, NULL);

    // Abstract the platform dependent comms to a platform independent TransportManager
    TransportManagerInit(&transportManager, socketWin.pInterface, (void*)&socketWin);

    testFailures = IntegrationTestsRunner(ipAddressString, mqttPort);

    return testFailures;
}


/**
 * @}
 * @}
 */
