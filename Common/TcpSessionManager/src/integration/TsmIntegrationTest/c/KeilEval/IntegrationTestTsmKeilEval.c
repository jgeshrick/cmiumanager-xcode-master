/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @defgroup TsmIntegrationTest
 *
 * @brief Integration Test for the @ref TCP Session Manager
 *
 */

/**
 * @addtogroup Integration Test
 * @{
 */

/**
 * @addtogroup Test 
 * @{
 */

/**
 * @file
 *
 * @brief Integration Test runner for the @ref TCP SessionManager (Tsm)
 * @author Duncan Willis
 * @date 2015.03.05  
 * @version 1.0
 */
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include "unity.h"
#include "TeamCity.h"
#include "em_device.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "bsp.h"
#include "bsp_trace.h"
#include "retargetserial.h"
#include "IntegrationTestTsm.h"
#include "TransportManager.h"
#include "ModemStub.h"



/* Counts 1ms timeTicks */
volatile uint32_t msTicks = 0;

/* Local prototypes */
void Delay(uint32_t dlyTicks);
static void PlatformInit(void);

/**************************************************************************//**
 * @brief SysTick_Handler
 * Interrupt Service Routine for system tick counter
 *****************************************************************************/
void SysTick_Handler(void)
{
    msTicks++;       /* increment counter necessary in Delay()*/
}

/**************************************************************************//**
 * @brief Delays number of msTick Systicks (typically 1 ms)
 * @param dlyTicks Number of ticks to delay
 *****************************************************************************/
void Delay(uint32_t dlyTicks)
{
    uint32_t curTicks;

    curTicks = msTicks;
    while ((msTicks - curTicks) < dlyTicks) ;
}


extern void setUp(void);
extern void tearDown(void);
extern TRANSPORT_MANAGER        transportManager;        //  

/**
** @brief The device specific driver
**/
static MODEM_STUB               modemStub; 


/** This function is used by unity */
void resetTest(void)
{
    tearDown();
    setUp();
}


/**
** Platform dependent:
** This function must return a 32bit number incrementing at TICK_CLOCKS_PER_SEC (is 1ms for Win on PC)
**/
uint32_t ClockTickGetMs(void)
{
    return msTicks;
}


/**
** Platform dependent:
** This function must output a char from a serial port or other test output
**/
void TestPutc(int c)
{
    putchar(c);
}


/**
** Runs the unit tests for module TCP Session Manager
**/
int main(void)
{    
    int testFailures = 0; 

    PlatformInit();
    
    // Platform dependent - on Keil use stub code...
    ModemStubInit(&modemStub, NULL);
    
    // Abstract the platform dependent comms to a platform independent TransportManager
    TransportManagerInit(&transportManager, modemStub.pInterface, (void*)&modemStub);

    testFailures = IntegrationTestsRunner(DEFAULT_INTEGRATION_TEST_SERVER, DEFAULT_INTEGRATION_TEST_PORT);
    
    printf("Test");
    
    return testFailures;
}


/// @brief Set up the dev board hardware
static void PlatformInit(void)
{       
    SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000);

    /* Initialize DK board register access */
    BSP_Init(BSP_INIT_DEFAULT);

    /* Initialize USART and map LF to CRLF */
    RETARGET_SerialInit();
    RETARGET_SerialCrLf(1);
}
