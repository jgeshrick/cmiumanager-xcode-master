/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup TsmUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test for the @ref TCP SessionManager
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 */
 
#include "unity.h"
#include "stdint.h"
#include "CommonTypes.h"

#include <stdio.h>
#include <string.h>
#include "MqttManager.h"
#include "HttpClient.h"
#include "HttpServer.h"
#include "TransportManager.h"
#include "TcpSessionManager.h"

#include "TestTsm.h"

static uint8_t s_TestNo = 0u;

bool  ProcessRxHelper(HTTP_SERVER* pServer, const uint8_t* inputString);
bool ProcessClientResponseHelper(HTTP_CLIENT* pClient, const uint8_t* inputString);


// Server related data
#define RX_RB_SIZE  4096
#define TX_RB_SIZE  4096
static RingBufferT rbRx;
static RingBufferT rbTx;
static uint8_t txBuf[TX_RB_SIZE];
static uint8_t rxBuf[RX_RB_SIZE];
static const uint8_t* myDataProvider(void* pCtx, uint32_t index, uint32_t numBytes);
static HTTP_SERVER_INIT_PARAMS httpServerInitParams;
static HTTP_SERVER  httpServer;
/**
 * Reset the unit and print the test number before each test.
 */
void setUp(void)
{
    ++s_TestNo;
    printf("Test %3d.\n",s_TestNo);

    RingBufferInit(&rbTx, txBuf, sizeof(txBuf));
    RingBufferInit(&rbTx, rxBuf, sizeof(rxBuf));
    
    httpServerInitParams.pRbRx = &rbRx;
    httpServerInitParams.pRbTx = &rbTx; 
}


/**
 * Reset the state after each test.
 */
void tearDown(void)
{
}


TCP_SESSION_MANAGER tsm;
TRANSPORT_MANAGER tm;
CONNECTION_PARAMS cp;


extern bool HttpServerReceiveByteHelper(HTTP_SERVER* pServer, uint8_t b);
extern int MQTTSerialize_publishLength(int qos, MQTTString topicName, int payloadlen);

/// Test how the PUBLISH packing works - 
/// how much overhead addded and handling of overflow
void Tsm_TestMqttSerializePublish(void)
{
    static uint8_t buffer[8192 + 100];
    int32_t  bytesSize = 0;
    int32_t  bytesSizeCalculated = 0;
    int32_t  expectedBytesSize;
    uint16_t pktId = 0x1234;
    unsigned char testData[4] = {'D','A', 'T', 'A'};
    static unsigned char bigData[8192] = {0};
    MQTTString topicString = MQTTString_initializer;

    topicString.cstring = (char*)"TEST_TOPIC";

    // Test Qos0
    bytesSize = MQTTSerialize_publish(buffer,
        sizeof(buffer),
        0, 
        E_MQTT_QOS_0, 
        0, 
        pktId, 
        topicString, 
        testData,
        sizeof(testData));
    
    expectedBytesSize  = 1; // header
    expectedBytesSize += 1; // msg length, encoded as variable length (would be 2 bytes if >= 128)
    expectedBytesSize += 2 /*len of string that follows*/  + strlen("TEST_TOPIC");
    expectedBytesSize += 0; // Only if QoS0
    expectedBytesSize += sizeof(testData);  

    
    bytesSizeCalculated = MqttManagerCalculatePublishLength(E_MQTT_QOS_0, "TEST_TOPIC", sizeof(testData));

    TEST_ASSERT_EQUAL_INT32(expectedBytesSize,   bytesSize); 
    TEST_ASSERT_EQUAL_INT32(expectedBytesSize,   bytesSizeCalculated);  
    
    // test QoS 1,     
    bytesSize = MQTTSerialize_publish(buffer,
        sizeof(buffer),
        0, 
        E_MQTT_QOS_1, 
        0, 
        pktId, 
        topicString, 
        testData,
        sizeof(testData));

    expectedBytesSize  = 1; // header
    expectedBytesSize += 1; // msg length, encoded as variable length (would be 2 bytes if >= 128)
    expectedBytesSize += 2 /*len of string that follows*/  + strlen("TEST_TOPIC");
    expectedBytesSize += 2; // Only if QoS != 0
    expectedBytesSize += sizeof(testData);  


    TEST_ASSERT_EQUAL_INT32(expectedBytesSize,   bytesSize);
    
    bytesSizeCalculated = MqttManagerCalculatePublishLength(E_MQTT_QOS_1, "TEST_TOPIC", sizeof(testData));
    TEST_ASSERT_EQUAL_INT32(expectedBytesSize,   bytesSizeCalculated);


    // test QoS 1, big data     
    bytesSize = MQTTSerialize_publish(buffer,
        sizeof(buffer),
        0, 
        E_MQTT_QOS_1, 
        0, 
        pktId, 
        topicString, 
        bigData,
        sizeof(bigData));

    expectedBytesSize  = 1; // header
    expectedBytesSize += 2; // msg length, encoded as variable length - big data
    expectedBytesSize += 2 /*len of string that follows*/  + strlen("TEST_TOPIC");
    expectedBytesSize += 2; // Only if QoS != 0
    expectedBytesSize += sizeof(bigData);  

    TEST_ASSERT_EQUAL_INT32(expectedBytesSize,   bytesSize); 
    
    bytesSizeCalculated = MqttManagerCalculatePublishLength(E_MQTT_QOS_1, "TEST_TOPIC", sizeof(bigData));
    TEST_ASSERT_EQUAL_INT32(expectedBytesSize,   bytesSizeCalculated);

    // test QoS 1, overflow    
    bytesSize = MQTTSerialize_publish(buffer,
        8192 + 10, // so msg of 8192 won't fit after header is added
        0, 
        E_MQTT_QOS_1, 
        0, 
        pktId, 
        topicString, 
        bigData,
        sizeof(bigData));

    expectedBytesSize  = 1; // header
    expectedBytesSize += 2; // msg length, encoded as variable length - big data, need 2 bytes to encode len
    expectedBytesSize += 2 /*len of string that follows*/  + strlen("TEST_TOPIC");
    expectedBytesSize += 2; // Only if QoS != 0
    expectedBytesSize += sizeof(bigData);  

    TEST_ASSERT_EQUAL_INT32(MQTTPACKET_BUFFER_TOO_SHORT,   bytesSize);
}



void Tsm_MqttTestInit(void)
{
    MQTT_MANAGER_SESSION_PARAMS sp;
    MQTT_MANAGER_SESSION_RESULTS sr;
    MQTT_MANAGER mm;
    MQTT_MANAGER_INIT_PARAMS mqttInitParams;

    MqttManagerInit(&mm, &mqttInitParams, &sp, &sr);

    // fill with rubbish
    mm.bufferSize  = 3;
    mm.state = E_MQTT_TIMED_OUT;
    mm.errorState  = MQTT_ERROR_SERIALISATION;
    mm.pTm = (void*)4;
    MqttManagerInit(&mm, &mqttInitParams, &sp, &sr);
    
    TEST_ASSERT_TRUE(MQTT_WORKING_BUFFER_SIZE == mm.bufferSize);
    TEST_ASSERT_TRUE(E_MQTT_BEGIN == mm.state);
    TEST_ASSERT_TRUE(MQTT_ERROR_FSM_UNINITIALISED == mm.errorState);
    TEST_ASSERT_EQUAL_PTR(NULL, mm.pTm);
}


void Tsm_MqttTestInitSessionParams(void)
{   
    MQTT_MANAGER_SESSION_PARAMS sp;

    // fill with rubbish
    sp.pubToSendCount = 999;
    sp.subscriptionCount = 999;
    sp.expectedPubsToReceive = 999;
    sp.mqttVersion  = 0;

    MqttManagerInitSessionParams(&sp);

    TEST_ASSERT_EQUAL_UINT32(4,                                 sp.mqttVersion);    // 4 == MQTT_VERSION_3_1_1
    TEST_ASSERT_EQUAL_UINT32(0,                                 sp.subscriptionCount);
    TEST_ASSERT_EQUAL_UINT32(0,                                 sp.pubToSendCount);
    TEST_ASSERT_EQUAL_UINT32(DEFAULT_MQTT_SESSION_TIMEOUT_MS,   sp.timeoutForWaitMs);
}


void Tsm_TestInit(void)
{    
    // fill with rubbish
    CONNECTION_PARAMS connectionParams = {"Host", 1234, 0};
    tsm.beginFlag = true;
    tsm.pConnectParams = (CONNECTION_PARAMS*)1024;
    tsm.state = E_SM_BEGIN_OPEN_CONNECTION;
    TcpSessionManagerInit(&tsm, &tm, &connectionParams); 
    
    TEST_ASSERT_EQUAL_PTR(&tm,   tsm.pTm);
    TEST_ASSERT_EQUAL_PTR(NULL,  tsm.pManager);
    TEST_ASSERT_EQUAL_PTR(&connectionParams,  tsm.pConnectParams);
    TEST_ASSERT_TRUE(   false == tsm.beginFlag); 
    TEST_ASSERT_TRUE(E_SM_RESET == tsm.state);
}


void Tsm_TestBegin(void)
{
    CONNECTION_PARAMS connectionParams = {"Host", 1234, 0};
    MQTT_MANAGER_SESSION_PARAMS sp;
    MQTT_MANAGER_SESSION_RESULTS sr;
        MQTT_MANAGER mm;
    MQTT_MANAGER_INIT_PARAMS mqttInitParams;

    MqttManagerInit(&mm, &mqttInitParams, &sp, &sr);

    // fill with rubbish
    tsm.beginFlag = true;
    tsm.pConnectParams = (CONNECTION_PARAMS*)1024;

    TcpSessionManagerInit(&tsm, &tm, &connectionParams);  
    TcpSessionManagerBegin(&tsm, &mm, &MqttManagerBegin, &MqttManagerRun, NULL); 
    
    TEST_ASSERT_TRUE(tsm.beginFlag);    
    TEST_ASSERT_EQUAL_PTR(&connectionParams,  tsm.pConnectParams);
}

 

void Tsm_TestTick(void)
{    
    CONNECTION_PARAMS connectionParams = {"Host", 1234, 0};
    MQTT_MANAGER_SESSION_PARAMS sp;
    MQTT_MANAGER_SESSION_RESULTS sr;
    MQTT_MANAGER mm;
    MQTT_MANAGER_INIT_PARAMS mqttInitParams;
    uint32_t tickIntervalMs = 10;

    MqttManagerInit(&mm, &mqttInitParams, &sp, &sr);

    // No transport manager - should have the effect of stubbing out
    TcpSessionManagerInit(&tsm, NULL, &connectionParams);
    
    // Run a few ticks - should remain in reset state ...
    TcpSessionManagerTick(&tsm, tickIntervalMs);
    TcpSessionManagerTick(&tsm, tickIntervalMs);
    TcpSessionManagerTick(&tsm, tickIntervalMs);
    
    TEST_ASSERT_TRUE(E_SM_RESET == tsm.state);

    // ...until the begin is called
    TcpSessionManagerBegin(&tsm, &mm, &MqttManagerBegin, &MqttManagerRun, NULL);    
    TcpSessionManagerTick(&tsm, tickIntervalMs);
    
    TEST_ASSERT_TRUE(E_SM_BEGIN_OPEN_CONNECTION == tsm.state);

    
    TcpSessionManagerTick(&tsm, tickIntervalMs);
    TEST_ASSERT_TRUE(E_SM_OPEN_CONNECTION_WAIT == tsm.state);
}


void Tsm_TestIsBusy(void)
{    
    CONNECTION_PARAMS connectionParams = {"Host", 1234, 0};
    MQTT_MANAGER_SESSION_PARAMS sp;
    MQTT_MANAGER_SESSION_RESULTS sr;
    MQTT_MANAGER mm;
    MQTT_MANAGER_INIT_PARAMS mqttInitParams;
    uint32_t tickIntervalMs = 10;
    bool isBusy;

    MqttManagerInit(&mm, &mqttInitParams, &sp, &sr);


    // No transport manager - should have the effect of stubbing out
    TcpSessionManagerInit(&tsm, NULL, &connectionParams);

    isBusy =  TcpSessionManagerIsBusy(&tsm);
    TEST_ASSERT_FALSE(isBusy);

    // Run a few ticks - should remain in NOT BUSY state ...
    TcpSessionManagerTick(&tsm, tickIntervalMs);
    TcpSessionManagerTick(&tsm, tickIntervalMs);
    TcpSessionManagerTick(&tsm, tickIntervalMs);
    isBusy =  TcpSessionManagerIsBusy(&tsm);
    TEST_ASSERT_FALSE(isBusy);

    // Stays not busy...until first tick after begin is called
    TcpSessionManagerBegin(&tsm, &mm, &MqttManagerBegin, &MqttManagerRun, NULL);    
    TcpSessionManagerTick(&tsm, tickIntervalMs);
    isBusy =  TcpSessionManagerIsBusy(&tsm);
    TEST_ASSERT_TRUE(isBusy);
}


void Tsm_TestAddSubscribe(void)
{    
    int i = 0;
    int32_t r = 0; 
    MQTT_MANAGER_SESSION_PARAMS sp;
    MQTT_MANAGER_SUBSCRIPTION sub;

    MqttManagerInitSessionParams(&sp);

    sub.topicFilter = "abc";
    sub.qos = E_MQTT_QOS_0;

    // Up to MQTT_MAX_SUBSCRIPTIONS subs allowed...
    for (i = 0; i < MQTT_MAX_SUBSCRIPTIONS; i++)
    {
        r = MqttManagerAddSubscription(&sp, &sub);
        TEST_ASSERT_TRUE(0 == r);
    }

    // Now full - should fail
    r = MqttManagerAddSubscription(&sp, &sub);
    TEST_ASSERT_TRUE(-1 == r);

    // Re-init to ensure is now empty again
    MqttManagerInitSessionParams(&sp);
    for (i = 0; i < MQTT_MAX_SUBSCRIPTIONS; i++)
    {
        r = MqttManagerAddSubscription(&sp, &sub);
        TEST_ASSERT_TRUE(0 == r);
    }

    r = MqttManagerAddSubscription(&sp, &sub);
    TEST_ASSERT_TRUE(-1 == r);
}


void Tsm_TestAddPublish(void)
{
    int32_t r = 0; 
    int32_t i = 0; 
    MQTT_MANAGER_SESSION_PARAMS sp;
    MQTT_MANAGER_PUB pub;
    pub.topic = "abc";

    MqttManagerInitSessionParams(&sp);

    // Up to MQTT_MAX_PUBS_TO_SEND subs allowed...
    for (i = 0; i < MQTT_MAX_PUBS_TO_SEND; i++)
    {
        r = MqttManagerAddPublish(&sp, &pub);
        TEST_ASSERT_EQUAL_INT32(i, r);
    }

    // Now full - should fail
    r = MqttManagerAddPublish(&sp, &pub);
    TEST_ASSERT_TRUE(-1 == r);

    // Re-init to ensure is now empty again
    MqttManagerInitSessionParams(&sp);
    // Up to MQTT_MAX_PUBS_TO_SEND subs allowed...
    for (i = 0; i < MQTT_MAX_PUBS_TO_SEND; i++)
    {
        r = MqttManagerAddPublish(&sp, &pub);
        TEST_ASSERT_EQUAL_INT32(i, r);
    }

    // Now full - should fail
    r = MqttManagerAddPublish(&sp, &pub);
    TEST_ASSERT_TRUE(-1 == r);
}


// Trap if more states were added - will affect debug print
void Tsm_TestStateCount(void)
{    
    TEST_ASSERT_TRUE(E_MQTT_COUNT == 10);
}


// Must be same as MQTT bytes 
void Tsm_TestQoSEnum(void)
{    
    TEST_ASSERT_TRUE(E_MQTT_QOS_0 == 0);
    TEST_ASSERT_TRUE(E_MQTT_QOS_1 == 1);
    TEST_ASSERT_TRUE(E_MQTT_QOS_2 == 2);
}

 

void Tsm_TestLineReaderInit(void)
{  
    HTTP_LINE_READER lr;
    uint8_t buffer[2];

    lr.pLineBuffer  = NULL;
    lr.bufferSize   = 1234;
    lr.pos          = 9999;

    LineReaderInit(&lr, buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL_PTR(lr.pLineBuffer, buffer);
    TEST_ASSERT_EQUAL_UINT32(2, lr.bufferSize);
    TEST_ASSERT_EQUAL_UINT32(0, lr.pos);
}

void Tsm_TestLineReaderReadByte(void)
{  
    HTTP_LINE_READER lr;
    uint8_t buffer[128] = {'9'};
    const uint8_t* p;  
    memset(buffer, '~', sizeof(buffer));

    LineReaderInit(&lr, buffer, sizeof(buffer));
    
    p = LineReaderReadByte(&lr, 'a');    
    TEST_ASSERT_EQUAL_PTR(NULL, p);

    p = LineReaderReadByte(&lr, 'b');
    TEST_ASSERT_EQUAL_PTR(NULL, p);

    p = LineReaderReadByte(&lr, '\r');
    TEST_ASSERT_EQUAL_PTR(NULL, p);

    // Line found
    p = LineReaderReadByte(&lr, '\n');
    TEST_ASSERT_EQUAL_PTR(buffer, p);

    
    TEST_ASSERT_EQUAL_UINT8('a',  buffer[0]);
    TEST_ASSERT_EQUAL_UINT8('b',  buffer[1]);
    TEST_ASSERT_EQUAL_UINT8('\r', buffer[2]);
    TEST_ASSERT_EQUAL_UINT8('\n', buffer[3]);
    TEST_ASSERT_EQUAL_UINT8('\0', buffer[4]);

    // Should restart
    p = LineReaderReadByte(&lr, 'x');
    TEST_ASSERT_EQUAL_PTR(NULL,  p);    
    TEST_ASSERT_EQUAL_UINT8('x', buffer[0]);

    // Should return blank lines (CRLF only)
    LineReaderInit(&lr, buffer, sizeof(buffer));
    
    p = LineReaderReadByte(&lr, '\r');
    TEST_ASSERT_EQUAL_PTR(NULL, p);

    // Empty Line must be found
    p = LineReaderReadByte(&lr, '\n');
    TEST_ASSERT_EQUAL_PTR(buffer, p);        
    TEST_ASSERT_EQUAL_UINT8('\r', buffer[0]);
    TEST_ASSERT_EQUAL_UINT8('\n', buffer[1]);    
    TEST_ASSERT_EQUAL_UINT8('\0', buffer[2]);
}


// Parsing of lines
void Tsm_TestLineReaderIsBlankLine(void)
{  
    // Only case it's true
    TEST_ASSERT_TRUE(LineReaderIsBlankLine("\r\n"));

    TEST_ASSERT_FALSE(LineReaderIsBlankLine("\n\r"));
    TEST_ASSERT_FALSE(LineReaderIsBlankLine("\r\n "));
    TEST_ASSERT_FALSE(LineReaderIsBlankLine(" \r\n"));
    TEST_ASSERT_FALSE(LineReaderIsBlankLine("\r \n"));
}


void Tsm_TestLineReaderParseNumeric(void)
{    
    const char *p;
    const char* pInput1 = "xfind_me 1234";
    const char* pInput2 = "xfind_me 12_34";
    
    const char* pInput3 = "xfind_me 2147483647";
    const char* pInput4 = "xfind_me 0";
    uint32_t nNumber = 0;

    // Empty input; should not crash
    p = LineReaderParseNumeric("", "find_me", NULL);
    TEST_ASSERT_TRUE(p == NULL);

    // 8th char in (1st char after match token = ' ')
    p = LineReaderParseNumeric((const uint8_t*)pInput1, "find_me", &nNumber);
    TEST_ASSERT_TRUE(p == &(pInput1[8]));    
    TEST_ASSERT_EQUAL_UINT32(1234, nNumber);     
    
    // Can accept NULL where number is found
    p = LineReaderParseNumeric((const uint8_t*)pInput1, "find_me", NULL);
    TEST_ASSERT_TRUE(p == &(pInput1[8]));   

    // Same, but split number
    p = LineReaderParseNumeric((const uint8_t*)pInput2, "find_me", &nNumber);
    TEST_ASSERT_TRUE(p == &(pInput2[8]));    
    TEST_ASSERT_EQUAL_UINT32(12, nNumber); 

    // Same, but split number, last pair
    p = LineReaderParseNumeric((const uint8_t*)pInput2, "find_me 12_", &nNumber);
    TEST_ASSERT_TRUE(p == &(pInput2[8 + 1 + 2+1]));    
    TEST_ASSERT_EQUAL_UINT32(34, nNumber);   
    
    // Largest parsable +ve number
    p = LineReaderParseNumeric((const uint8_t*)pInput3, "find_me", &nNumber);    
    TEST_ASSERT_EQUAL_UINT32(2147483647, nNumber);
    
    // 0
    p = LineReaderParseNumeric((const uint8_t*)pInput4, "find_me", &nNumber);  
    TEST_ASSERT_TRUE(p != NULL);
    TEST_ASSERT_EQUAL_UINT32(0, nNumber);     
    
    // Won't find - number must be unchanged
    nNumber = 654;
    p = LineReaderParseNumeric((const uint8_t*)pInput4, "_ind_me", &nNumber);  
    TEST_ASSERT_TRUE(p == NULL);
    TEST_ASSERT_EQUAL_UINT32(654, nNumber);     
}

void Tsm_TestLineReaderParseString(void)
{    
    const char *p;
    const char* pInput1 = "xfind_me astring";
    const char* pInput2 = "xfind_me   astring";
    const char* pInput3 = "xfind_me   astring notseen";
    const char* pInput4 = "xfind_me  ";
    
    // Empty input; should not crash
    p = LineReaderParseString("", "find_me");
    TEST_ASSERT_TRUE(p == NULL);

    // 9th char in  
    p = LineReaderParseString((const uint8_t*)pInput1, "find_me" );
    TEST_ASSERT_TRUE(p == &(pInput1[9]));    

    // 11th char in  
    p = LineReaderParseString((const uint8_t*)pInput2, "find_me" );
    TEST_ASSERT_TRUE(p == &(pInput2[11]));    
     
    // Same as above, last token did not affect result
    p = LineReaderParseString((const uint8_t*)pInput3, "find_me" );
    TEST_ASSERT_TRUE(p == &(pInput3[11]));    
     
    // Whitespace was skipped, and no token found?
    p = LineReaderParseString((const uint8_t*)pInput4, "find_me" );
    TEST_ASSERT_TRUE(p == NULL);    
 
}


void Tsm_TestLineReaderParseContentLength(void)
{
    bool b;
    const char* pInput1 = "Content-Length: 1234";
    const char* pInput2 = "Content-Length:  1234";
    const char* pInput3 = "Content-Length:  1234-";
     
    uint32_t nNumber = 0;
    
    // Empty input; should not crash
    b = LineReaderParseContentLength("", NULL);
    TEST_ASSERT_FALSE(b);

    // Normal case
    b = LineReaderParseContentLength((const uint8_t*)pInput1, &nNumber);
    TEST_ASSERT_TRUE(b);    
    TEST_ASSERT_EQUAL_UINT32(1234, nNumber); 

    // Normal case, leading whitespace
    b = LineReaderParseContentLength((const uint8_t*)pInput2, &nNumber);
    TEST_ASSERT_TRUE(b);    
    TEST_ASSERT_EQUAL_UINT32(1234, nNumber); 

    // Normal case, leading whitespace, - after
    b = LineReaderParseContentLength((const uint8_t*)pInput3, &nNumber);
    TEST_ASSERT_TRUE(b);    
    TEST_ASSERT_EQUAL_UINT32(1234, nNumber);
}


void Tsm_TestLineReaderParseContentRange(void)
{
     bool b;
    const char* pInput0 = "Not content ";
    const char* pInput1 = "Content-Range: bytes 0-1/1234";
    const char* pInput2 = "Content-Range: bytes 1-0/1234";
    const char* pInput3 = "Content-Range: bytes 0-1/0";
    const char* pInput4 = "Content-Range: bytes 2147483647-2147483647/2147483647";     
    const char* pInput5 = "Content-Range: bytes 0 - 1 / 0";

    uint32_t nN1 = 99;
    uint32_t nN2 = 99;
    uint32_t nN3 = 99;
    
    // Empty input; should not crash
    b = LineReaderParseContentRange("", "Content-Range: bytes", NULL, NULL, NULL);
    TEST_ASSERT_FALSE(b);

    // No content to parse, numbers must be unaltererd
    b = LineReaderParseContentRange((const uint8_t*)pInput0,"Content-Range: bytes",  &nN1, &nN2, &nN3);
    TEST_ASSERT_TRUE(b == false);    
    TEST_ASSERT_EQUAL_UINT32(99,     nN1); 
    TEST_ASSERT_EQUAL_UINT32(99,     nN2); 
    TEST_ASSERT_EQUAL_UINT32(99,     nN3); 

    // Normal case
    b = LineReaderParseContentRange((const uint8_t*)pInput1, "Content-Range: bytes", &nN1, &nN2, &nN3);
    TEST_ASSERT_TRUE(b);    
    TEST_ASSERT_EQUAL_UINT32(0,     nN1); 
    TEST_ASSERT_EQUAL_UINT32(1,     nN2); 
    TEST_ASSERT_EQUAL_UINT32(1234,  nN3); 
    
    // Reversed case
    b = LineReaderParseContentRange((const uint8_t*)pInput2, "Content-Range: bytes", &nN1, &nN2, &nN3);
    TEST_ASSERT_TRUE(b);    
    TEST_ASSERT_EQUAL_UINT32(1,     nN1); 
    TEST_ASSERT_EQUAL_UINT32(0,     nN2); 
    TEST_ASSERT_EQUAL_UINT32(1234,  nN3); 
    
    //  Zero
    b = LineReaderParseContentRange((const uint8_t*)pInput3, "Content-Range: bytes", &nN1, &nN2, &nN3);
    TEST_ASSERT_TRUE(b);    
    TEST_ASSERT_EQUAL_UINT32(0,     nN1); 
    TEST_ASSERT_EQUAL_UINT32(1,     nN2); 
    TEST_ASSERT_EQUAL_UINT32(0,     nN3); 
    
    //  Large numbers
    b = LineReaderParseContentRange((const uint8_t*)pInput4, "Content-Range: bytes", &nN1, &nN2, &nN3);
    TEST_ASSERT_TRUE(b);    
    TEST_ASSERT_EQUAL_UINT32(2147483647,     nN1); 
    TEST_ASSERT_EQUAL_UINT32(2147483647,     nN2); 
    TEST_ASSERT_EQUAL_UINT32(2147483647,     nN3); 
     
    //  Inserted spaces
    b = LineReaderParseContentRange((const uint8_t*)pInput5, "Content-Range: bytes", &nN1, &nN2, &nN3);
    TEST_ASSERT_TRUE(b);    
    TEST_ASSERT_EQUAL_UINT32(0,     nN1); 
    TEST_ASSERT_EQUAL_UINT32(1,     nN2); 
    TEST_ASSERT_EQUAL_UINT32(0,     nN3); 
}




void Tsm_TestHttpClientInitSessionParams(void)
{
    HTTP_CLIENT_SESSION_PARAMS sp;

    sp.fileName = "abc";
    sp.hostName = "x";
    sp.firstRangeByte = 11;
    sp.lastRangeByte = 22;
    sp.timeoutForWaitMs = 0;

    HttpClientInitSessionParams(&sp);  

    TEST_ASSERT_EQUAL_UINT32(0,     sp.firstRangeByte);
    TEST_ASSERT_EQUAL_UINT32(0,     sp.lastRangeByte); 
    
    TEST_ASSERT_EQUAL_PTR(NULL,     sp.fileName ); 
    TEST_ASSERT_EQUAL_PTR(NULL,     sp.hostName ); 

    TEST_ASSERT_EQUAL_UINT32(DEFAULT_HTTP_SESSION_TIMEOUT_MS,     sp.timeoutForWaitMs);
}


void Tsm_TestHttpClientInitSessionResults(void)
{
    HTTP_CLIENT_SESSION_RESULTS sr;

    sr.contentLength = 99;
    sr.rangeFirstByte = 99;
    sr.rangeLastByte = 99;
    sr.rangeTotalContentLength = 999;
    sr.receivedBytes = 999;
    sr.responseCode = 2;
    sr.sentBytes = 999;
    sr.sessionExecutionTimeMs = 9;

    HttpClientInitSessionResults(&sr);  

    TEST_ASSERT_EQUAL_UINT32(0,     sr.contentLength);
    TEST_ASSERT_EQUAL_UINT32(0,     sr.rangeFirstByte); 
    TEST_ASSERT_EQUAL_UINT32(0,     sr.rangeLastByte); 
    TEST_ASSERT_EQUAL_UINT32(0,     sr.rangeTotalContentLength); 
    TEST_ASSERT_EQUAL_UINT32(0,     sr.receivedBytes); 
    TEST_ASSERT_EQUAL_UINT32(0,     sr.responseCode); 
    TEST_ASSERT_EQUAL_UINT32(0,     sr.sentBytes); 
    TEST_ASSERT_EQUAL_UINT32(0,     sr.sessionExecutionTimeMs);
}



// Init, and Begin
void Tsm_TestHttpClientBegin(void)
{
    HTTP_CLIENT hm;
    TRANSPORT_MANAGER tm;
    HTTP_CLIENT_INIT_PARAMS ip;
    HTTP_CLIENT_SESSION_PARAMS sp;
    HTTP_CLIENT_SESSION_RESULTS sr;

    uint8_t buf[128];

    ip.buffer = buf;
    ip.bufferSize = sizeof(buf);

    sr.responseCode                     = 999;

    hm.errorState = HTTP_ERROR_BAD_URL;
    HttpClientInit(&hm, &ip, &sp, &sr);        
    TEST_ASSERT_TRUE(HTTP_ERROR_UNINITIALISED ==  hm.errorState);
    TEST_ASSERT_TRUE(E_HTTP_BEGIN       ==  hm.state);

    hm.timeElapsedMs                    = 999;
    hm.state                            = E_HTTP_COUNT;
    hm.errorState                       = HTTP_ERROR_BAD_URL;

    HttpClientBegin(&hm, &tm);
      
    TEST_ASSERT_EQUAL_UINT32(0,         hm.timeElapsedMs);
    TEST_ASSERT_TRUE(E_HTTP_BEGIN   ==  hm.state);
    TEST_ASSERT_TRUE(HTTP_OK        ==  hm.errorState);    
    TEST_ASSERT_EQUAL_UINT32(0,         sr.responseCode);      
}


// Test the make reqest of the Client, and the GET parser of the Server
// Make a GET and feed it into the server for parsing
void Tsm_TestHttpServerGet(void)
{
    uint8_t buffer[2048];
    uint32_t firstRangeByte = 12;
    uint32_t lastRangeByte = 34;
    bool isComplete; 
    HTTP_ERROR eCode = HTTP_OK;

    HttpServerInit(&httpServer, &httpServerInitParams);

    eCode = HttpMakeGetRequestHelper(buffer,
        sizeof(buffer),
        "myurl",
        "myhostName",
        firstRangeByte,
        lastRangeByte);

    TEST_ASSERT_TRUE(HTTP_OK        ==  eCode); 

    isComplete = ProcessRxHelper(&httpServer, buffer);

    
    TEST_ASSERT_TRUE( isComplete ); 
    TEST_ASSERT_TRUE( httpServer.hasBlankLine);    
    TEST_ASSERT_TRUE( httpServer.hasGetField);    
    TEST_ASSERT_TRUE( httpServer.hasRange);    

    TEST_ASSERT_EQUAL_UINT32(firstRangeByte, httpServer.rangeFirstByte);
    TEST_ASSERT_EQUAL_UINT32(lastRangeByte,  httpServer.rangeLastByte); 

    // Repeat, but with 0 for range values
    eCode = HttpMakeGetRequestHelper(buffer,
        sizeof(buffer),
        "myurl2",
        "myhostName2",
        0,
        0);
        
    TEST_ASSERT_TRUE(HTTP_OK ==  eCode);        
     
    // Reset the server
    HttpServerInit(&httpServer, &httpServerInitParams);

    ProcessRxHelper(&httpServer, buffer);
    // Should be no range  in GET request
    TEST_ASSERT_FALSE(httpServer.hasRange);   
}



// Test the make reqest of the Client, for a long URL 
void Tsm_TestHttpServerGetLongUrl(void)
{
    uint8_t buffer[2048];
    char longUrl[HTTP_MAX_URL_LEN + 1 + 1]; // URL Limit is 256
    uint32_t firstRangeByte = 12;
    uint32_t lastRangeByte = 34;
    bool isComplete; 
    HTTP_ERROR eCode = HTTP_OK;
     
    memset(longUrl, 'Z', sizeof(longUrl));

    // make URL string HTTP_MAX_URL_LEN+1 = 256+1 chars long
    longUrl[HTTP_MAX_URL_LEN + 1] = '\0';
    TEST_ASSERT_TRUE(HTTP_MAX_URL_LEN + 1 == strlen(longUrl));

    HttpServerInit(&httpServer, &httpServerInitParams);

    eCode = HttpMakeGetRequestHelper(buffer,
        sizeof(buffer),
        longUrl,
        "myhostName",
        firstRangeByte,
        lastRangeByte);

    TEST_ASSERT_TRUE(HTTP_ERROR_BAD_URL        ==  eCode); 
   
    
    // Now make URL string equal to limit 
    longUrl[HTTP_MAX_URL_LEN] = '\0';
    TEST_ASSERT_TRUE(HTTP_MAX_URL_LEN == strlen(longUrl));

    HttpServerInit(&httpServer, &httpServerInitParams);

    eCode = HttpMakeGetRequestHelper(buffer,
        sizeof(buffer),
        longUrl,
        "myhostName",
        firstRangeByte,
        lastRangeByte);

    TEST_ASSERT_TRUE(HTTP_OK        ==  eCode); 

    
    // Finally, set up a server to serve that URL and ensure it's identical
    HttpServerInit(&httpServer, &httpServerInitParams);

    isComplete = ProcessRxHelper(&httpServer, buffer);
    TEST_ASSERT_TRUE(isComplete);
    TEST_ASSERT_EQUAL_STRING(longUrl, httpServer.resourceStr);
}



// Test the make response header of the Server
void Tsm_TestResponseHeader(void)
{
    HTTP_CLIENT httpClient;
    TRANSPORT_MANAGER tm;
    HTTP_CLIENT_INIT_PARAMS ip;
    HTTP_CLIENT_SESSION_PARAMS sp;
    HTTP_CLIENT_SESSION_RESULTS sr;
    uint32_t headerLength = 0;
    uint8_t buf[128];
    char buffer[2048];
    uint32_t firstRangeByte = 12;
    uint32_t lastRangeByte = 34;
    uint32_t totalContentLength = 999;
    uint32_t contentLength = lastRangeByte - firstRangeByte;
    uint32_t responseCode = 200;

    headerLength = HttpMakeGetResponseHelper((uint8_t*)buffer, sizeof(buffer),   
        responseCode,
        contentLength,
        firstRangeByte, 
        lastRangeByte, 
        totalContentLength);
    
    TEST_ASSERT_TRUE(headerLength > 0);
     

    // Set up a client to parse the Response header
    ip.buffer = buf;
    ip.bufferSize = sizeof(buf);
    HttpClientInit(&httpClient, &ip, &sp, &sr); 
    
    HttpClientBegin(&httpClient, &tm);
    ProcessClientResponseHelper(&httpClient, (const uint8_t*)buffer);

    // Should read out what we put in!
    TEST_ASSERT_EQUAL_UINT32(contentLength,         sr.contentLength);
    TEST_ASSERT_EQUAL_UINT32(firstRangeByte,        sr.rangeFirstByte); 
    TEST_ASSERT_EQUAL_UINT32(lastRangeByte,         sr.rangeLastByte); 
    TEST_ASSERT_EQUAL_UINT32(totalContentLength,    sr.rangeTotalContentLength); 
    TEST_ASSERT_EQUAL_UINT32(responseCode,          sr.responseCode);

    /* Note - we are only testing the population of the header here. Do 
    data transfer separately */
}


// Test Percent served - 100% for dummy data
void Tsm_TestHttpServerGetPercentServed(void)
{
    uint32_t firstRangeByte = 12;
    uint32_t lastRangeByte = 34;
     
    // Note   fakeDataSize = 4321 set in HttpServer.
    uint32_t fakeDataSize = 4321;

    HTTP_ERROR eCode = HTTP_OK;

    HttpServerInit(&httpServer, &httpServerInitParams);

    TEST_ASSERT_EQUAL_UINT32(0, httpServer.percentServed);

    httpServer.pDataProvider = NULL;// fake data
    httpServer.hasRange = true;
    httpServer.rangeFirstByte = 1;
    httpServer.rangeLastByte = 10;

    HttpServerSendResponse(&httpServer);
    TEST_ASSERT_EQUAL_UINT32((100 * (httpServer.rangeLastByte+1)) / fakeDataSize, httpServer.percentServed);
    
    // half way
    httpServer.rangeFirstByte = 2000;
    httpServer.rangeLastByte = 2160;

    HttpServerSendResponse(&httpServer);
    TEST_ASSERT_EQUAL_UINT32(50, httpServer.percentServed);
    
    // Last chunk
    httpServer.rangeFirstByte = 4300;
    httpServer.rangeLastByte = 4320;

    HttpServerSendResponse(&httpServer);
    TEST_ASSERT_EQUAL_UINT32(100, httpServer.percentServed);

}

// Process the whole GET query string until NULL term found.
// In this case, the isComplete should be true
bool ProcessRxHelper(HTTP_SERVER* pServer, const uint8_t* inputString)
{    
    bool isComplete;
    uint32_t index = 0;
    uint8_t c = inputString[0];

    do
    {        
        isComplete = HttpServerReceiveByteHelper(pServer, c);

        c = inputString[++index]; 
    } 
    while ('\0' != c);

    return isComplete;
}


// Process the whole GET RESPONSE string (do not send any NULL term).
// In this case, the isComplete should be true
bool ProcessClientResponseHelper(HTTP_CLIENT* pClient, const uint8_t* inputString)
{    
    bool isHeaderComplete;
    uint32_t index = 0;
    uint8_t c = inputString[0];

    do
    {        
        isHeaderComplete = HttpClientReceiveHeaderHelper(pClient, c);

        c = inputString[++index]; 
    } 
    while ('\0' != c);

    return isHeaderComplete;
}

static const uint8_t* myDataProvider(void* pCtx, uint32_t index, uint32_t numBytes) 
{
    static const uint8_t* pSomeData = (const uint8_t*)"Hello cruel world";
    return pSomeData;
}

/**
* @}
 * @}
 */
