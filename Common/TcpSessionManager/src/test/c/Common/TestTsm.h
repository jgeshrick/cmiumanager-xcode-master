/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/


/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup TsmUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test for the @ref TCP Session Manager
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 */


#ifndef _TSM_UNITTEST_H__
#define _TSM_UNITTEST_H__

void Tsm_TestMqttSerializePublish(void);

void Tsm_MqttTestInit(void);
void Tsm_MqttTestInitSessionParams(void);
void Tsm_TestInit(void); 
void Tsm_TestBegin(void);
void Tsm_TestTick(void);
void Tsm_TestIsBusy(void);
void Tsm_TestAddSubscribe(void);
void Tsm_TestAddPublish(void);
void Tsm_TestStateCount(void);
void Tsm_TestQoSEnum(void);
void Tsm_TestLineReaderInit(void);
void Tsm_TestLineReaderReadByte(void);
void Tsm_TestLineReaderIsBlankLine(void);
void Tsm_TestLineReaderParseNumeric(void);
void Tsm_TestLineReaderParseString(void);
void Tsm_TestLineReaderParseContentLength(void);
void Tsm_TestLineReaderParseContentRange(void);
void Tsm_TestHttpClientInitSessionParams(void);
void Tsm_TestHttpClientInitSessionResults(void);
void Tsm_TestHttpClientBegin(void);
void Tsm_TestHttpServerGet(void);
void Tsm_TestHttpServerGetLongUrl(void);
void Tsm_TestResponseHeader(void);
void Tsm_TestHttpServerGetPercentServed(void);

#endif /* _TSM_UNITTEST_H__ */
