/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @defgroup TsmUnitTest 
 *
 * @brief Unit Test for the @ref TCP Session Manager
 *
 */

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup TsmUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test runner for the @ref TCP SessionManager
 * @author Duncan Willis
 * @date 2015.03.05  
 * @version 1.0
 */
 
#ifndef TEST_TSM_RUNNER_H
#define TEST_TSM_RUNNER_H

/**
** Runs the unit tests for module TCP Session Manager
**/
int TestTsm(void);

#endif

/**
 * @}
 * @}
 */
