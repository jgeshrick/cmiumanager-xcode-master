/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @defgroup RingBufferUnitTest 
 *
 * @brief Unit Test for the @ref RingBuffer  
 *
 */

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup RingBufferUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test runner for the @ref RingBuffer
 * @author Duncan Willis
 * @date 2015.03.24  
 * @version 1.0
 */
 

#include "unity.h"
#include "TeamCity.h"
#include "TestRingBuffer.h"


extern void setUp(void);
extern void tearDown(void);



/** This function is used by unity */
void resetTest(void)
{
    tearDown();
    setUp();
}


/**
** Runs the unit tests for module RingBuffer
**/
int TestRingBuffer(void)
{
    int testFailures = 0;
 
    Unity.TestFile = "TsmUnitTest.c";
    UnityBegin();

    printf("Testing module %s\n", Unity.TestFile);
     
    // Ring Buffer tests
    RUN_TEST( RingBuffer_TestStructSize,         __LINE__);   
    RUN_TEST( RingBuffer_TestInit,               __LINE__);  
    RUN_TEST( RingBuffer_TestSize,               __LINE__); 
    RUN_TEST( RingBuffer_TestFree,               __LINE__);  
    RUN_TEST( RingBuffer_TestFlush,              __LINE__);  
    RUN_TEST( RingBuffer_TestEmpty,              __LINE__);  
    RUN_TEST( RingBuffer_TestRW_single,          __LINE__);
    RUN_TEST( RingBuffer_TestOverflow,           __LINE__);   
    RUN_TEST( RingBuffer_TestRW_array,           __LINE__);

    testFailures = UnityEnd();

    return testFailures;
}

/**
 * @}
 * @}
 */
