/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @defgroup TsmUnitTest 
 *
 * @brief Unit Test for the @ref TCP Session Manager
 *
 */

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup TsmUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test runner for the @ref TCP SessionManager
 * @author Duncan Willis
 * @date 2015.03.05  
 * @version 1.0
 */
 

#include "unity.h"
#include "TeamCity.h"
#include "TestTsm.h"

extern void setUp(void);
extern void tearDown(void);


/** This function is used by unity */
void resetTest(void)
{
    tearDown();
    setUp();
}


/**
** Runs the unit tests for module TCP Session Manager
**/
int TestTsm(void)
{
    int testFailures = 0;
 
    Unity.TestFile = "TsmUnitTest.c";
    UnityBegin();

    printf("Testing module %s\n", Unity.TestFile);

    // General tests
    RUN_TEST(Tsm_TestMqttSerializePublish,      __LINE__);
    RUN_TEST(Tsm_MqttTestInit,                  __LINE__);
    RUN_TEST(Tsm_MqttTestInitSessionParams,     __LINE__);
    RUN_TEST(Tsm_TestInit,                      __LINE__);
    RUN_TEST(Tsm_TestBegin,                     __LINE__);
    RUN_TEST(Tsm_TestTick,                      __LINE__);
    RUN_TEST(Tsm_TestIsBusy,                    __LINE__);
    RUN_TEST(Tsm_TestAddSubscribe,              __LINE__);
    RUN_TEST(Tsm_TestAddPublish,                __LINE__);
    RUN_TEST(Tsm_TestStateCount,                __LINE__);
    RUN_TEST(Tsm_TestQoSEnum,                   __LINE__);
    RUN_TEST(Tsm_TestLineReaderInit,                __LINE__);
    RUN_TEST(Tsm_TestLineReaderReadByte,            __LINE__);    
    RUN_TEST(Tsm_TestLineReaderIsBlankLine,         __LINE__);
    RUN_TEST(Tsm_TestLineReaderParseNumeric,        __LINE__);
    RUN_TEST(Tsm_TestLineReaderParseString,         __LINE__);
    RUN_TEST(Tsm_TestLineReaderParseContentLength,  __LINE__);
    RUN_TEST(Tsm_TestLineReaderParseContentRange,   __LINE__);
    RUN_TEST(Tsm_TestHttpClientInitSessionParams,   __LINE__);
    RUN_TEST(Tsm_TestHttpClientInitSessionResults,  __LINE__);
    RUN_TEST(Tsm_TestHttpClientBegin,               __LINE__);
    RUN_TEST(Tsm_TestHttpServerGet,                 __LINE__);
    RUN_TEST(Tsm_TestHttpServerGetLongUrl,          __LINE__);
    RUN_TEST(Tsm_TestResponseHeader,                __LINE__);
    RUN_TEST(Tsm_TestHttpServerGetPercentServed,    __LINE__);

    testFailures = UnityEnd();

    return testFailures;
}

/**
 * @}
 * @}
 */
