/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @defgroup TpbpUnitTest 
 *
 * @brief Unit Test for the @ref Tagged Packet Builder Parser  
 *
 */

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup TpbpUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Windows-specific application wrapper
 * @author Rupert Menzies
 * @date 2015.04.09
 * @version 1.0
 */

#include <stdio.h>
#include <stdbool.h>
#include "em_device.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "bsp.h"
#include "bsp_trace.h"
#include "retargetserial.h"
#include "TestUartControllerRunner.h"

/* Counts 1ms timeTicks */
volatile uint32_t msTicks;

/* Local prototypes */
void Delay(uint32_t dlyTicks);

/**************************************************************************//**
 * @brief SysTick_Handler
 * Interrupt Service Routine for system tick counter
 *****************************************************************************/
void SysTick_Handler(void)
{
    msTicks++;       /* increment counter necessary in Delay()*/
}

/**************************************************************************//**
 * @brief Delays number of msTick Systicks (typically 1 ms)
 * @param dlyTicks Number of ticks to delay
 *****************************************************************************/
void Delay(uint32_t dlyTicks)
{
    uint32_t curTicks;

    curTicks = msTicks;
    while ((msTicks - curTicks) < dlyTicks) ;
}

int main(void)
{
    int forever = 1;
    
    SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000);

    /* Initialize DK board register access */
    BSP_Init(BSP_INIT_DEFAULT);

    /* Initialize USART and map LF to CRLF */
    RETARGET_SerialInit();
    RETARGET_SerialCrLf(1);
    
    int ret = TestUartController();
    
    while (forever)
    {
        BSP_LedsSet(0xff00);
        Delay(200);
        BSP_LedsSet(0x00ff);
        Delay(200);
    }

    return ret;
}

/**
 * @}
 * @}
 */
