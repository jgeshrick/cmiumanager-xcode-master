/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

/**
 * @defgroup UartControllerUnitTest 
 *
 * @brief Unit Test for the @ref Uart controller
 *
 */
/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup UartControllerUnitTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test for the @ref UartController
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 */

#include "unity.h"
#include "TeamCity.h"
#include "TestUartController.h"

extern void setUp(void);
extern void tearDown(void);




/** This function is used by unity */
void resetTest(void)
{
    tearDown();
    setUp();
}


/**
** Runs the unit tests for module TCP Session Manager
**/
int TestUartController(void)
{
    int testFailures = 0;
 
    Unity.TestFile = "UartControllerUnitTest.c";
    UnityBegin();

    printf("Testing module %s\n", Unity.TestFile);
     

    // General tests
    RUN_TEST(UartController_TestInit,                   __LINE__);
    RUN_TEST(UartController_TestEnum,                   __LINE__);
    RUN_TEST(UartController_TestCrcSeedInitialisation,  __LINE__);
    RUN_TEST(UartController_TestSendHelper,             __LINE__);
    RUN_TEST(UartController_SimpleRx,                   __LINE__);
    RUN_TEST(UartController_CrcError,                   __LINE__);
    RUN_TEST(UartController_ErrorInCrc,                 __LINE__);
    RUN_TEST(UartController_MultiRx,                    __LINE__);
    RUN_TEST(UartController_MultiRxInsertedByte,        __LINE__);
  
    testFailures = UnityEnd();

    return testFailures;
}

/**
 * @}
 * @}
 */
