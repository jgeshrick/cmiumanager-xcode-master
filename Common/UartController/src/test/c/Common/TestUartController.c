/*****************************************************************************
******************************************************************************
**
**         Filename: TestUartController.c
**    
**          Author: Duncan Willis
**          Created: 17 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file TestUartController.c
*
* @brief UART Controller unit tests
* 
******************************************************************************/
 
/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup UartControllerUnitTest
 * @{
 */

 
#include "stdint.h"
#include "CommonTypes.h"
#include "unity.h"
#include <stdio.h>
#include "UartController.h"
#include "Crc32Fast.h"
#include "TestUartController.h"


static uint8_t s_TestNo = 0u;


UART_CONTROLLER uc;
UARC_MSG_READER mr;
UART_CONTROLLER_INIT_PARAMS gInitParams;

RingBufferT rbRx;
RingBufferT rbTx;

// Max len of a debug string
#define RINGBUF_LEN 1024

// for ring buffer 
static uint8_t     rbTxData[RINGBUF_LEN];
static uint8_t     rbRxData[RINGBUF_LEN];

 

static uint8_t payloadBuffer[4096];

/**
 * Reset the unit and print the test number before each test.
 */
void setUp(void)
{
    ++s_TestNo;
    printf("Test %3d.\n",s_TestNo);


    RingBufferInit(&rbRx, rbRxData, sizeof(rbRxData));
    RingBufferInit(&rbTx, rbTxData, sizeof(rbTxData));
    
    gInitParams.pRbReceive       = &rbRx;
    gInitParams.pRbTransmit      = &rbTx;    
    gInitParams.pMessageReader   = &mr;
    gInitParams.pfnRxHandler     = NULL;
   

    // Init reader to use the assembly buffer
    UartMessageReaderInit (&mr, payloadBuffer, sizeof(payloadBuffer));

    // Init controller itself
    UartControllerInit(&uc, &gInitParams);
}


/**
 * Reset the state after each test.
 */
void tearDown(void)
{
}


/**
 * Test the object initialisation
 */
void UartController_TestInit(void)
{   
    UART_CONTROLLER_INIT_PARAMS initParams;

    initParams.pRbReceive   = &rbRx;
    initParams.pRbTransmit  = &rbTx;
    UartControllerInit(&uc, &initParams);

    TEST_ASSERT_EQUAL_PTR(&rbRx,    uc.pRbReceive);
    TEST_ASSERT_EQUAL_PTR(&rbTx,    uc.pRbTransmit); 

    TEST_ASSERT_EQUAL_UINT32(UARC_MSS_READY_TO_SEND,    uc.sendState); 
}


/**
 * Test enums 
 */
void UartController_TestEnum(void)
{   
    TEST_ASSERT_EQUAL_UINT32(0,    UARC_MSS_READY_TO_SEND); 
    TEST_ASSERT_EQUAL_UINT32(1,    UARC_MSS_BUSY); 
    TEST_ASSERT_EQUAL_UINT32(2,    UARC_MSS_TIMEOUT); 
}


/**
 * Test CRC seed OK
 */
void UartController_TestCrcSeedInitialisation(void)
{   
    mr.crc32Seed = 0;
    UartMessageReaderInit(&mr, payloadBuffer, sizeof(payloadBuffer));
	TEST_ASSERT_EQUAL_UINT32(4294967295, mr.crc32Seed);
}



/**
* Test Data gets put in buffer OK
*/
void UartController_TestSendHelper(void)
{
    uint32_t i      = 0;
    uint32_t crc    = 0;
    uint32_t n      = 0;
    uint32_t type   = 0x55;
    uint8_t  data[10] = {1,2,3,4,5,6,7,8,9,10};
    uint32_t payloadBytesToSend = 10;
    uint32_t numSent = 0;
    UartControllerInit(&uc, &gInitParams);

    numSent = UartControllerSend(&uc,
        (E_UARC_MSG_TYPE)type,
        data, 
        payloadBytesToSend); 
    
    TEST_ASSERT_EQUAL_UINT32(1+1+2+1+payloadBytesToSend + 4,
        numSent);

    TEST_ASSERT_EQUAL_UINT32(MRS_HEADER_1,      rbTxData[0]);
    TEST_ASSERT_EQUAL_UINT32(MRS_HEADER_2,      rbTxData[1]);
    TEST_ASSERT_EQUAL_UINT32(10,                rbTxData[2]);
    TEST_ASSERT_EQUAL_UINT32(0,                 rbTxData[3]);
    TEST_ASSERT_EQUAL_UINT32(type,              rbTxData[4]);

    i = 5;
    for (n = 0; n < 10; n++)
    {
        TEST_ASSERT_EQUAL_UINT32(1 + n,         rbTxData[i++]);
    }

    // recreate the CRC
    crc = CRC_SEED;
    crc = FastCrc32Byte(crc, &rbTxData[2], 1);
    crc = FastCrc32Byte(crc, &rbTxData[3], 1);
    crc = FastCrc32Byte(crc, &rbTxData[4], 1);
    crc = FastCrc32Byte(crc, data, payloadBytesToSend);

    // test it's put in ringbuffer as expected
    TEST_ASSERT_EQUAL_UINT32( ((uint8_t)((crc) >>  0)),                rbTxData[i++]);
    TEST_ASSERT_EQUAL_UINT32( ((uint8_t)((crc) >>  8)),                rbTxData[i++]);
    TEST_ASSERT_EQUAL_UINT32( ((uint8_t)((crc) >> 16)),                rbTxData[i++]);
    TEST_ASSERT_EQUAL_UINT32( ((uint8_t)((crc) >> 24)),                rbTxData[i++]);
}

extern   void UartControllerPollReceiver(UART_CONTROLLER* pUc);

uint32_t callbackCount = 0;
uint32_t callbackOk = 0;

static void TestUartControllerOnReceiveMessage(const UARC_MSG*  pRxMsg, const void *context)
{
    uint32_t i;

    callbackCount++;

    callbackOk = 0;
    do
    {
        if (pRxMsg->length != 10)
            break;

        for (i = 0; i < pRxMsg->length; i++)
        {
        if (pRxMsg->pPayload[ i ]  != i + 1)
            break;
        }

        callbackOk = 1;
    }   
    while (0);
}


/**
* Test a simple write and readback 
*/
void UartController_SimpleRx(void)
{
//    uint32_t i      = 0;
    uint32_t msgNumBytes    = 2 + 2 + 1 + 10 + 4;
 //   uint32_t crc    = 0;
//    uint32_t n      = 0;
    uint32_t type   = 0x55;
    uint8_t  data[10] = {1,2,3,4,5,6,7,8,9,10};
    uint32_t payloadBytesToSend = 10;
    uint32_t rxCount;    
    UART_CONTROLLER_INIT_PARAMS initParams = gInitParams;

    // Use one RB for both Send and receive, in order to do loopback test.
    RingBufferT* pRbTest = &rbTx;
     
    initParams.pRbReceive   = pRbTest;
    initParams.pRbTransmit  = pRbTest;
    initParams.pfnRxHandler = TestUartControllerOnReceiveMessage;
    UartControllerInit(&uc, &initParams);

    UartControllerSend(&uc,
        (E_UARC_MSG_TYPE)type,
        data, 
        payloadBytesToSend); 
    
    rxCount = RingBufferUsed(pRbTest);
    TEST_ASSERT_EQUAL_UINT32(msgNumBytes, rxCount);

    // Check we can get one message
    callbackCount = 0;
    UartControllerPollReceiver(&uc);
    TEST_ASSERT_EQUAL_UINT32(1, callbackCount);
    TEST_ASSERT_EQUAL_UINT32(1, callbackOk);

    // Now the Ringbuff should be empty
    rxCount = RingBufferUsed(pRbTest);
    TEST_ASSERT_EQUAL_UINT32(0, rxCount);
}


/**
* Test CRC detection
*/
void UartController_CrcError(void)
{
    uint32_t msgNumBytes = 2 + 2 + 1 + 10 + 4;

    uint32_t type = 0x55;
    uint8_t  data[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    uint32_t payloadBytesToSend = 10; 
    uint32_t rxCount;    
    UART_CONTROLLER_INIT_PARAMS initParams = gInitParams;

    // Use one RB for both Send and receive, in order to do loopback test.
    RingBufferT* pRbTest = &rbTx;
    initParams.pRbReceive   = pRbTest;
    initParams.pRbTransmit  = pRbTest;
    initParams.pfnRxHandler = TestUartControllerOnReceiveMessage;
    UartControllerInit(&uc, &initParams);

    UartControllerSend(&uc,
        (E_UARC_MSG_TYPE)type,
        data,
        payloadBytesToSend);

    rxCount = RingBufferUsed(pRbTest);
    TEST_ASSERT_EQUAL_UINT32(msgNumBytes, rxCount);

    // Cause a CRC error in payload. Note direct access to ringbuf data means we need to be
    // initied it at start of test - ensure that here
    TEST_ASSERT_EQUAL_UINT8(MRS_HEADER_1, pRbTest->pucBuf[0]);
    // Cause a CRC error (1st byte of payload)
    pRbTest->pucBuf[5] ^= 0x01;

    // Check that message detected CRC error    
    TEST_ASSERT_EQUAL_UINT32(0, uc.pMessageReader->crcErrorCount);
    callbackCount = 0;
    UartControllerPollReceiver(&uc);
    TEST_ASSERT_EQUAL_UINT32(0, callbackCount);
    TEST_ASSERT_EQUAL_UINT32(1, callbackOk);
    TEST_ASSERT_EQUAL_UINT32(1, uc.pMessageReader->crcErrorCount);
}


/**
* Test CRC detection in CRC itself
*/
void UartController_ErrorInCrc(void)
{
    uint32_t msgNumBytes = 2 + 2 + 1 + 10 + 4;
    uint32_t type = 0x55;
    uint8_t  data[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    uint32_t payloadBytesToSend = 10;
    UART_CONTROLLER_INIT_PARAMS initParams = gInitParams;
    uint32_t rxCount;

    // Use one RB for both Send and receive, in order to do loopback test.
    RingBufferT* pRbTest = &rbTx;

    initParams.pRbReceive = pRbTest;
    initParams.pRbTransmit = pRbTest;
    initParams.pfnRxHandler = TestUartControllerOnReceiveMessage;
    UartControllerInit(&uc, &initParams);

    UartControllerSend(&uc,
        (E_UARC_MSG_TYPE)type,
        data,
        payloadBytesToSend);

    rxCount = RingBufferUsed(pRbTest);
    TEST_ASSERT_EQUAL_UINT32(msgNumBytes, rxCount);

    // Cause a CRC error in CRC. Note direct access to ringbuf data means we need to be
    // initied it at start of test - ensure that here
    TEST_ASSERT_EQUAL_UINT8(MRS_HEADER_1, pRbTest->pucBuf[0]);

    pRbTest->pucBuf[msgNumBytes - 1] ^= 0x01;

    // Check that message detected CRC error    
    TEST_ASSERT_EQUAL_UINT32(0, uc.pMessageReader->crcErrorCount);
    UartControllerPollReceiver(&uc);
    TEST_ASSERT_EQUAL_UINT32(0, callbackCount);
    TEST_ASSERT_EQUAL_UINT32(1, uc.pMessageReader->crcErrorCount);
}


/**
* Test several writes and readback 
*/
void UartController_MultiRx(void)
{
    uint32_t i          = 0;
    uint32_t numMsgs    = 12;
    uint32_t msgNumBytes    = 2 + 2 + 1 + 10 + 4;

    uint32_t type   = 0x55;
    uint8_t  data[10] = {1,2,3,4,5,6,7,8,9,10};
    uint32_t payloadBytesToSend = 10;
    UART_CONTROLLER_INIT_PARAMS initParams = gInitParams;
    uint32_t rxCount;

    // Use one RB for both Send and receive, in order to do loopback test.
    RingBufferT* pRbTest = &rbTx;

    initParams.pRbReceive = pRbTest;
    initParams.pRbTransmit = pRbTest;
    initParams.pfnRxHandler = TestUartControllerOnReceiveMessage;
    UartControllerInit(&uc, &initParams);

    for (i = 0; i < numMsgs;  i++)
    {
        UartControllerSend(&uc,
            (E_UARC_MSG_TYPE)type,
            data, 
            payloadBytesToSend); 
    }

    rxCount = RingBufferUsed(pRbTest);
    TEST_ASSERT_EQUAL_UINT32(numMsgs * msgNumBytes, rxCount);


    // Check we can get all messages
    callbackCount = 0;
    UartControllerPollReceiver(&uc);
    TEST_ASSERT_EQUAL_UINT32(numMsgs, callbackCount);
    
    TEST_ASSERT_EQUAL_UINT32(1, callbackOk);

    // Now the Ringbuff should be empty
    rxCount = RingBufferUsed(pRbTest);
    TEST_ASSERT_EQUAL_UINT32(0, rxCount);
}



/**
* Test two packets separated by a spurious byte
*/
void UartController_MultiRxInsertedByte(void)
{
    uint32_t numMsgs    = 2;
    uint32_t msgNumBytes    = 2 + 2 + 1 + 10 + 4;
    uint32_t type   = 0x55;
    uint8_t  data[10] = {1,2,3,4,5,6,7,8,9,10};
    uint32_t payloadBytesToSend = 10;
    UART_CONTROLLER_INIT_PARAMS initParams = gInitParams;
    uint32_t rxCount;

    // Use one RB for both Send and receive, in order to do loopback test.
    RingBufferT* pRbTest = &rbTx;

    initParams.pRbReceive = pRbTest;
    initParams.pRbTransmit = pRbTest;
    initParams.pfnRxHandler = TestUartControllerOnReceiveMessage;
    UartControllerInit(&uc, &initParams);

    // Pkt 1
    UartControllerSend(&uc,
        (E_UARC_MSG_TYPE)type,
        data, 
        payloadBytesToSend); 

    // Insert one byte of junk
    RingBufferWriteOne(pRbTest, 'E');

    // Pkt 2
    UartControllerSend(&uc,
        (E_UARC_MSG_TYPE)type,
        data, 
        payloadBytesToSend); 


    rxCount = RingBufferUsed(pRbTest);

    // Ensure junk byte is present
    TEST_ASSERT_EQUAL_UINT32(2 * msgNumBytes + 1, rxCount);


    // Check we can get both messages
    callbackCount = 0;
    UartControllerPollReceiver(&uc);
    TEST_ASSERT_EQUAL_UINT32(numMsgs, callbackCount);

    // Now the Ringbuff should be empty
    rxCount = RingBufferUsed(pRbTest);
    TEST_ASSERT_EQUAL_UINT32(0, rxCount);

    // Ensure junk byte was seen by parser
    TEST_ASSERT_EQUAL_UINT32(1, uc.pMessageReader->unsyncCount);
}
 

/**
 * @}
 * @}
 */
