/******************************************************************************
*******************************************************************************
**
**         Filename: TestUartControllerRunner.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
** Revision History:
** 
**
**
**      Code Review:
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/


/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup  UART ControllerUniTest
 * @{
 */

/**
 * @file
 *
 * @brief Unit Test runner for the @ref UART Controller
 * @author Duncan Willis
 * @date 2015.03.05  
 * @version 1.0
 */
 
#ifndef TEST_UART_CONTROLLER_RUNNER_H
#define TEST_UART_CONTROLLER_RUNNER_H

/**
** Runs the unit tests
**/

int TestUartController(void);


#endif

/**
 * @}
 * @}
 */
