/*****************************************************************************
******************************************************************************
**
**         Filename: TestUartController.h
**    
**          Author: Duncan Willis
**          Created: 17 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file TestUartController.h
*
* @brief UART Controller unit tests
* 
******************************************************************************/

/**
 * @addtogroup UnitTest
 * @{
 */

/**
 * @addtogroup UartControllerUnitTest
 * @{
 */


#ifndef _UARC_UNITTEST_H__
#define _UARC_UNITTEST_H__

void UartController_TestInit(void);
void UartController_TestEnum(void);
void UartController_TestCrcSeedInitialisation(void);
void UartController_TestSendHelper(void);
void UartController_SimpleRx(void);
void UartController_CrcError(void);
void UartController_ErrorInCrc(void);
void UartController_MultiRx(void);
void UartController_MultiRxInsertedByte(void);


#endif /* _UARC_UNITTEST_H__ */
