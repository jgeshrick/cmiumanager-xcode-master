/*****************************************************************************
******************************************************************************
**
**         Filename: UartControllerMessageWriter.c
**    
**          Author: Duncan Willis
**          Created: 30 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file UartControllerMessageReader.c
*
* @brief Part of UART Controller handles writing of framed message
* 
******************************************************************************/ 


/**
 * @addtogroup UARTController
 * @{
 */


#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "CommonTypes.h"
#include "Crc32Fast.h"
#include "Ringbuffer.h"
#include "UartControllerMessageWriter.h"
 

static void WritePacketHelper(UARC_MSG_WRITER* pMw, const UARC_TX_MSG* pTxMsg);

static void WriteByteMemoryHelper(UARC_MSG_WRITER* pMw, uint8_t b);

static void WriteByteRingBufferHelper(UARC_MSG_WRITER* pMw, uint8_t b);



/*****************************************************************************
 * @brief Writes a packet with the given msg into the given buffer 
 *    
 * 
 * @param [in] pMw 
 *  The Msg Writer (Does not need to be pre-initialised)
 * 
 * @param [in] pTxMsg 
 *  The Msg to write
 *
 * @param [in] pBuffer 
 *  The buffer to write into
 *
 * @param [in] bufferMaxSize 
 *  The buffer size.
 *  
 * @return total number bytes written, 0 if buffer was too small
 *
 *****************************************************************************/
 uint32_t UartControllerMessageWriterWriteBuffer(UARC_MSG_WRITER* pMw,
     const UARC_TX_MSG* pTxMsg,
     uint8_t*    pBuffer,
     uint32_t    bufferMaxSize)
 {
     uint32_t totalSize;
     
     ASSERT(NULL != pMw);
     ASSERT(NULL != pTxMsg);
     ASSERT(NULL != pBuffer);

     totalSize = UartControllerMessageWriterCalculateSize(pTxMsg);
     
     pMw->pRingbuffer   = NULL; // Not used in this use-case
     pMw->pBuffer = pBuffer;
     pMw->bufferMaxSize = bufferMaxSize;
     pMw->byteIndex = 0;
     pMw->pByteWriter = WriteByteMemoryHelper;

     if (totalSize <= bufferMaxSize)
     {
         WritePacketHelper(pMw, pTxMsg);

         ASSERT(pMw->byteIndex == totalSize);
     }

     return pMw->byteIndex;
 }


/*****************************************************************************
 * @brief Writes a packet with the given msg into the given ring buffer 
 *    
 * 
 * @param [in] pMw 
 *  The Msg Writer (Does not need to be pre-initialised)
 * 
 * @param [in] pTxMsg 
 *  The Msg to write
 *
 * @param [in] pRb 
 *  The ring buffer to write into
 *
 *  
 * @return total number bytes written.
 * @note that the rb overwrites silently so it's the caller's responsibility 
 * to check with a call to GetFree
 *****************************************************************************/
 uint32_t UartControllerMessageWriterWriteRingBuffer(UARC_MSG_WRITER* pMw,
     const UARC_TX_MSG* pTxMsg,
     RingBufferT* pRb)
 {
     ASSERT(NULL != pMw);
     ASSERT(NULL != pTxMsg);
     ASSERT(NULL != pRb);
     
     memset(pMw, 0, sizeof(UARC_MSG_WRITER));

     pMw->byteIndex     = 0;          
     pMw->pByteWriter   = WriteByteRingBufferHelper;
     pMw->pRingbuffer   = pRb;
     pMw->pBuffer       = NULL; // Not used in this use-case
     pMw->bufferMaxSize = 0;

     WritePacketHelper(pMw, pTxMsg);

     return pMw->byteIndex;
 }


/*****************************************************************************
 * @brief
 *   Calculate the number of bytes on the wire to send the given msg
 * 
 * @param [in] pTxMsg 
 *  The Msg to get number of bytes in total after wrapping
 *  
 * @return size in bytes
 *
 *****************************************************************************/
 uint32_t UartControllerMessageWriterCalculateSize(const UARC_TX_MSG* pTxMsg)
 {
    uint32_t sizeInBytes;
    ASSERT(NULL != pTxMsg);

    sizeInBytes = 2 + 2 + 1 + pTxMsg->payloadLength + 4;

    return sizeInBytes;
 }


/*****************************************************************************
 * @brief
 *   Write a msg into stream or memory
 * 
 * @param [in] pMw 
 *  The Msg Writer
 *
 * @param [in] pTxMsg 
 *  The Msg to send
 *
 * @return None
 * Helper to write the message into a byte stream or memory via a function 
 * pointer interface
 *****************************************************************************/
static void WritePacketHelper(UARC_MSG_WRITER* pMw, const UARC_TX_MSG* pTxMsg)
{
    uint32_t    i;
    uint32_t    payloadBytesToSend;
    uint8_t     lenLow;
    uint8_t     lenHigh;
    uint8_t     typeByte;
    uint32_t    crc = CRC_SEED;
    
    ASSERT(NULL != pTxMsg);
    ASSERT(NULL != pMw);
 
    payloadBytesToSend = pTxMsg->payloadLength;
    lenLow  = LOBYTE(payloadBytesToSend);
    lenHigh = HIBYTE(payloadBytesToSend);
    typeByte = (uint8_t)pTxMsg->type;

    pMw->pByteWriter(pMw, MRS_HEADER_1);
    pMw->pByteWriter(pMw, MRS_HEADER_2);
    
    pMw->pByteWriter(pMw, lenLow);
    crc = FastCrc32Byte(crc, &lenLow, 1);  

    pMw->pByteWriter(pMw, lenHigh);
    crc = FastCrc32Byte(crc, &lenHigh, 1);

    pMw->pByteWriter(pMw, typeByte);
    crc = FastCrc32Byte(crc, &typeByte, 1);
    
    for (i = 0 ; i < payloadBytesToSend; i++)
    {
        pMw->pByteWriter(pMw, pTxMsg->pPayload[i]);   
    } 
    
    crc = FastCrc32Byte(crc, pTxMsg->pPayload, payloadBytesToSend);
    pMw->pByteWriter(pMw, ((uint8_t)((crc) >>  0)));
    pMw->pByteWriter(pMw, ((uint8_t)((crc) >>  8)));
    pMw->pByteWriter(pMw, ((uint8_t)((crc) >> 16)));
    pMw->pByteWriter(pMw, ((uint8_t)((crc) >> 24)));
}


/*****************************************************************************
 * @brief
 *  helper to write a msg into memory array
 * 
 * @param [in] pMw 
 *  The Msg Writer
 *
 * @param [in] b 
 *  The byte to send
 *
 * @return None
 * Helper to write the byte into memory  
 *****************************************************************************/
static void WriteByteMemoryHelper(UARC_MSG_WRITER* pMw,
     uint8_t b)
 {
     if (pMw->byteIndex < pMw->bufferMaxSize)
     {
         pMw->pBuffer[pMw->byteIndex] = b;
         pMw->byteIndex++;
     }
 }


/*****************************************************************************
 * @brief
 * Helper to write a msg into byte stream
 * 
 * @param [in] pMw 
 *  The Msg Writer
 *
 * @param [in] b 
 *  The byte to send
 *
 * @return None
 * Helper to write the byte into memory  
 *****************************************************************************/
static void WriteByteRingBufferHelper(UARC_MSG_WRITER* pMw,
     uint8_t b)
 {
     RingBufferWriteOne(pMw->pRingbuffer, b);
     pMw->byteIndex++;
 }


/** 
 * @}
 */
