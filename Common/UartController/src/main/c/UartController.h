/*****************************************************************************
******************************************************************************
**
**         Filename: UartController.h
**    
**          Author: Duncan Willis
**          Created: 17 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file UartController.h
*
* @brief UART Controller - wrapper for messages sent over UART 
*  (or BTLE virtual UART)
* 
******************************************************************************/

#ifndef __UART_CONTROLLER_H__
#define __UART_CONTROLLER_H__

#include <stdint.h>
#include "commonTypes.h"
#include "RingBuffer.h"
#include "UartControllerMessageReader.h"
#include "UartControllerMessageWriter.h"    
#include "UartControllerMessageTypes.h"
typedef void (*pfnReceiveHandlerT)(const struct  UARC_MSG* pMsg, const void *context);
 



typedef struct UART_CONTROLLER
{
    UARC_MSG_READER*        pMessageReader;

    RingBufferT*            pRbReceive;
    RingBufferT*            pRbTransmit;

    pfnReceiveHandlerT      pfnRxHandler;

    uint32_t                sendTimerMs;
    E_UARC_MSG_SEND_STATE   sendState;

    uint32_t                sendInactivityTimerMs;
    uint32_t                sendInactivityThresholdMs;
    uint32_t                keepaliveSentCount;

    uint32_t                receiveInactivityTimerMs;
    
    const void*             context;
} UART_CONTROLLER;


typedef struct UART_CONTROLLER_INIT_PARAMS
{
    RingBufferT*            pRbReceive;
    RingBufferT*            pRbTransmit;
         
    UARC_MSG_READER*        pMessageReader;
    uint32_t                sendInactivityThresholdMs;
    pfnReceiveHandlerT      pfnRxHandler;
    const void*             context;
} 
UART_CONTROLLER_INIT_PARAMS;


void                    UartControllerInit(UART_CONTROLLER* pUc, UART_CONTROLLER_INIT_PARAMS* pInitParams);
void                    UartControllerTick(UART_CONTROLLER* pUc, uint32_t tickIntervalMs);
uint32_t                UartControllerStartSendTimer(UART_CONTROLLER* pUc, uint32_t timeoutMs);
E_UARC_MSG_SEND_STATE   UartControllerGetSendState(const UART_CONTROLLER* pUc);
void                    UartControllerReadByte(UART_CONTROLLER* pUc, uint8_t b);

uint32_t                UartControllerSend(UART_CONTROLLER* pUc,
                                E_UARC_MSG_TYPE type,
                                const uint8_t* data, 
                                uint32_t dataBytesToSend);

uint32_t                UartControllerSendMessage(UART_CONTROLLER* pUc, const UARC_TX_MSG txMsg);
void                    UartControllerSetHeartbeatInterval(UART_CONTROLLER* pUc, const uint32_t heartbeatIntervalMs);        
void                    UartControllerResetSendTimer(UART_CONTROLLER* pUc);                                
void                    UartControllerPollReceiver(UART_CONTROLLER* pUc);

                                
uint32_t                UartControllerGetTimeSinceLastReceive(const UART_CONTROLLER* pUc);
uint32_t                UartControllerGetTimeSinceLastSend(const UART_CONTROLLER* pUc);
void                    UartControllerFormatMessage(const struct UARC_MSG* pRxMsg, char bufStr[], uint32_t bufSize);

#endif

/** 
 * @}
*/
 
