/*****************************************************************************
******************************************************************************
**
**         Filename: UartControllerMessageTypes.h
**    
**          Author: Duncan Willis
**          Created: 17 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file UartControllerMessageTypes.h
*
* @brief UART Controller constants and types
* 
******************************************************************************/

/**
 * @addtogroup UARTController
 * @{
 */



#ifndef __UARC_MSG_TYPES_H__
#define __UARC_MSG_TYPES_H__

#include <stdint.h>
#include "commonTypes.h"


#ifndef LOBYTE
#define LOBYTE(x)           ((uint8_t)((x) >>  0))
#define HIBYTE(x)           ((uint8_t)((x) >>  8))
#endif // LOBYTE

#define MRS_HEADER_1        ((uint8_t)0x31)
#define MRS_HEADER_2        ((uint8_t)0x32)

typedef enum E_UARC_MSG_TYPE
{
    UARC_MT_NULL        = 0x00,
    UARC_MT_CMD         = 0x01,
    UARC_MT_PING        = 0x02,
    UARC_MT_HTTP_CMD    = 0x04,


    UARC_MT_ERROR       = 0x80,
    UARC_MT_RESP        = 0x81,
    UARC_MT_GNIP        = 0x82,
    UARC_MT_ASYNC       = 0x83,
    UARC_MT_HTTP_RSP    = 0x84, 
    UARC_MT_HEARTBEAT   = 0x85      //!< Periodic async message send to Master (iPad) to indicate we're alive
}
E_UARC_MSG_TYPE;



typedef enum E_UARC_MSG_SEND_STATE
{
    UARC_MSS_READY_TO_SEND  = 0,
    UARC_MSS_BUSY           = 1,
    UARC_MSS_TIMEOUT        = 2

}
E_UARC_MSG_SEND_STATE;







typedef struct UARC_MSG
{
    uint32_t    length;
    uint32_t    type;
    uint8_t*    pPayload;
    uint32_t    pPayloadMaxSize;
}
UARC_MSG;


typedef struct UARC_TX_MSG
{  
    uint32_t    type;
    uint32_t    payloadLength;

    const uint8_t*    pPayload;
}
UARC_TX_MSG;


#endif

/** 
 * @}
 */
