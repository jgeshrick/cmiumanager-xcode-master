/*****************************************************************************
******************************************************************************
**
**         Filename: UartController.c
**    
**          Author: Duncan Willis
**          Created: 17 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file UartController.c
*
* @brief UART Controller - wrapper for messages sent over UART 
*  (or BTLE virtual UART)
* 
******************************************************************************/

/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "CommonTypes.h"
#include "Crc32Fast.h"
#include "RingBuffer.h"
#include "DebugTrace.h"
#include "UartController.h"
#include "UartControllerMessageReader.h"

#ifdef _WIN32
#define snprintf _snprintf
#endif

#define MSG_IS_RESPONSE(m) (((uint8_t)((m)->type) == (uint8_t)UARC_MT_RESP) ||\
                            ((uint8_t)((m)->type) == (uint8_t)UARC_MT_GNIP))


static const char* GetTypeString(const E_UARC_MSG_TYPE type);




/*****************************************************************************
 * @brief
 *   Init the Uart Controller  object
 * 
 * @param [in] pUc 
 *  The reader controller to init
 * 
 * @param pInitParams 
 *  Pointer to initialiser structure (refs to ringbuffers) 
 *
 * @param bytesToSend 
 *  The initialising info
 *
 * @return 
 *   None
 *
 *****************************************************************************/
void UartControllerInit(UART_CONTROLLER* pUc, UART_CONTROLLER_INIT_PARAMS* pInitParams)
{
    ASSERT(NULL != pUc); 
    ASSERT(NULL != pInitParams); 

    memset(pUc, 0, sizeof(UART_CONTROLLER));
    
    pUc->pRbReceive                 = pInitParams->pRbReceive;
    pUc->pRbTransmit                = pInitParams->pRbTransmit;
    pUc->pfnRxHandler               = pInitParams->pfnRxHandler;
    pUc->context                    = pInitParams->context;
    pUc->sendInactivityThresholdMs  = pInitParams->sendInactivityThresholdMs;

    pUc->pMessageReader             = pInitParams->pMessageReader;
    pUc->sendTimerMs                = 0;
    pUc->sendInactivityTimerMs      = 0;
    pUc->receiveInactivityTimerMs   = 0;
    pUc->sendState                  = UARC_MSS_READY_TO_SEND;
    pUc->keepaliveSentCount         = 0;
}


/*****************************************************************************
 * @brief
 *   Run a tick operation
 * 
 * @param [in] pUc 
 *  The reader controller to use
 * 
 * @param tickIntervalMs 
 *  The time between ticks in ms
 *
 * @return None
 *
 *****************************************************************************/
void UartControllerTick(UART_CONTROLLER* pUc, uint32_t tickIntervalMs)
{
    ASSERT(NULL != pUc); 

    // Assume we're inactive; reset elsewhere as applicable if found to be active.
    pUc->sendInactivityTimerMs      += tickIntervalMs;
    pUc->receiveInactivityTimerMs   += tickIntervalMs;
    
    UartControllerPollReceiver(pUc);
    
    //
    // Waiting for a response?
    // Handle send timer, this waits for a response to a send operation.
    //
    if (pUc->sendTimerMs > 0)
    {
        // reset send inactivity indicator; 
        // We cannot be inactive if we're waiting for a response
        pUc->sendInactivityTimerMs = 0;

        // Decrement send timer and indicate Timeout when expired
        if (pUc->sendTimerMs > tickIntervalMs)
        {
            pUc->sendTimerMs -= tickIntervalMs;      
        }
        else
        {
            pUc->sendState = UARC_MSS_TIMEOUT;
            pUc->sendTimerMs = 0;
        }
    }


    // Prevent use in non-polling DLL. Master does not send heartbeats.
#ifndef _WIN32
    // Have we not sent anything recently? If not, send a heartbeat 
    // Value of 0 disables inactivity operation
    if (pUc->sendInactivityThresholdMs != 0)
    {
        // Send a msg, (this will therefore reset the inactivity timer)
        if (pUc->sendInactivityTimerMs >= pUc->sendInactivityThresholdMs)
        {
            UartControllerSend(pUc, UARC_MT_HEARTBEAT, (const uint8_t*)"HX", 2);
            pUc->keepaliveSentCount++;
        }
    }
#endif
}


/*****************************************************************************
* @brief
*   Set a timer to wait for a response
*
* @param [in] pUc
*  The controller to use
*
* @param timeoutMs
*  The time wait to get response
*
* @return  0 if already in progress (busy), 1 if OK
*
*****************************************************************************/
uint32_t UartControllerStartSendTimer(UART_CONTROLLER* pUc, uint32_t timeoutMs)
{
    uint32_t isOk = 0;

    ASSERT(NULL != pUc);

    // Note; if we timed out last time, we restart normally,
    // So the only state we don't restart in is if we are in process of sending (busy).
    if (UARC_MSS_BUSY != pUc->sendState)
    {
        pUc->sendTimerMs = timeoutMs;
        pUc->sendState = UARC_MSS_BUSY;

        isOk = 1;
    }

    return isOk;
}


/*****************************************************************************
* @brief
*   Query the state of the send operation
*
* @param [in] pUc
*  The controller to use
* @return  Enumeration
*
*****************************************************************************/
E_UARC_MSG_SEND_STATE UartControllerGetSendState(const UART_CONTROLLER* pUc)
{
    ASSERT(NULL != pUc);

    return pUc->sendState;
}


/*****************************************************************************
* @brief
*   Set an optional heartbeat. This is be used at the remote end to indicate
* that the connection is alive. Heartbeat messages are sent periodically
* when no other messages are being sent.
*
* @param [in] heartbeatIntervalMs
*  The interval of messages in ms. 0 to turn off.
* @return  None
*
*****************************************************************************/
void UartControllerSetHeartbeatInterval(UART_CONTROLLER* pUc, 
    const uint32_t heartbeatIntervalMs)
{
    ASSERT(NULL != pUc);

    pUc->sendInactivityTimerMs = heartbeatIntervalMs;
}


/*****************************************************************************
* @brief
*   Reset send inactivity timer. Call this at the point of send.
* Note - needed only for Windows apps / DLL where messages are sent outside 
* of this module (buffers owned by host)
*
* @param [in] pUC The Uart Controller
* @return  None
*
*****************************************************************************/
void UartControllerResetSendTimer(UART_CONTROLLER* pUc)
{
    ASSERT(NULL != pUc);

    pUc->sendInactivityTimerMs = 0;
}


/*****************************************************************************
 * @brief
 *   Process the byte b with the reader. If this completes a message, call
 *   the handler
 * @param [in] pUc 
 *  The  controller to use
 * @param [in] b 
 *  The  byte to process
 *
 * @return None
 *
 *****************************************************************************/
void UartControllerReadByte(UART_CONTROLLER* pUc, uint8_t b)
{
    const UARC_MSG*  pRxMsg;

    ASSERT(NULL != pUc);

    // Reset inactivity timer if there's incoming data
    pUc->receiveInactivityTimerMs = 0;
    
    pRxMsg = UartMessageReaderReadByte(pUc->pMessageReader, b);

    if (NULL != pRxMsg)
    {
        if (MSG_IS_RESPONSE(pRxMsg))
        {
            pUc->sendTimerMs    = 0;
            pUc->sendState      = UARC_MSS_READY_TO_SEND;
        }

        if (NULL != pUc->pfnRxHandler)
        {
            pUc->pfnRxHandler(pRxMsg, pUc->context);
        }
    }
}


/*****************************************************************************
 * @brief
 *   Poll the rx ringbuffer and process any chars in it. If a msg is found,  
 *   then call a handler function
 *
 * @param [in] pUc 
 *  The  controller to use
 *
 * @return None
 *
 *****************************************************************************/
void UartControllerPollReceiver(UART_CONTROLLER* pUc)
{
    uint8_t rxChar;
    uint32_t rxCount = 0;
    
    ASSERT(NULL != pUc); 
    ASSERT(NULL != pUc->pRbReceive); 

    rxCount = RingBufferUsed(pUc->pRbReceive);

    if (rxCount > 0)
    {
        while (rxCount > 0)
        {
            rxChar = RingBufferReadOne(pUc->pRbReceive);

            UartControllerReadByte(pUc, rxChar);

            rxCount--;
        }
    }
}


/*****************************************************************************
 * @brief
 *   Send payload data via ringbuffer
 * 
 * @param [in] pUc 
 *  The reader controller to use
 * 
 * @param data 
 *  Pointer to data to send
 *
 * @param dataBytesToSend 
 *  The number of bytes (of data array) to send
 *
 * @return 
 * Number of bytes written to ringbuffer
 *
 * This function is NOT for use with DLL, since the DLL does not poll the Tx FIFO
 * 
 *****************************************************************************/
  uint32_t UartControllerSend(UART_CONTROLLER* pUc,
    E_UARC_MSG_TYPE type,
    const uint8_t* data, 
    uint32_t dataBytesToSend)
{
    UARC_TX_MSG         txMsg = {0};

    txMsg.type          = type;
    txMsg.pPayload      = data;
    txMsg.payloadLength = dataBytesToSend;

    return UartControllerSendMessage(pUc, txMsg);
}


/*****************************************************************************
 * @brief
 *   Send payload data via ringbuffer
 * 
 * @param [in] pUc 
 *  The reader controller to use
 * 
 * @param txMsg 
 *  Pointer to Tx Message to send
 * 
 * @return 
 * Number of bytes written to ringbuffer
 *
 *****************************************************************************/
uint32_t UartControllerSendMessage(UART_CONTROLLER* pUc, const UARC_TX_MSG txMsg)
{
    UARC_MSG_WRITER     mw = {0};   
    uint32_t            numBytesWritten = 0;

     UartControllerResetSendTimer(pUc);
    
    // Cannot send if insufficient room in ringbuffer
    if (txMsg.payloadLength <= RingBufferFree(pUc->pRbTransmit))
    {
        numBytesWritten = UartControllerMessageWriterWriteRingBuffer(&mw, 
            &txMsg, 
            pUc->pRbTransmit);
    }

    return numBytesWritten;
}


/*****************************************************************************
* @brief Get the time, in ms, since data was last received
*   
* @param [in] pUc
*  The controller to use
*
* @return The time, in ms of inactivity
*
*****************************************************************************/
uint32_t UartControllerGetTimeSinceLastReceive(const UART_CONTROLLER* pUc)
{
    return pUc->receiveInactivityTimerMs;
}


/*****************************************************************************
* @brief Get the time, in ms,  since data was last sent
*   
* @param [in] pUc
*  The controller to use
*
* @return The time, in ms of inactivity
*
*****************************************************************************/
uint32_t UartControllerGetTimeSinceLastSend(const UART_CONTROLLER* pUc)
{
    return pUc->sendInactivityTimerMs;
}



/*****************************************************************************
 * @brief
 *   Debug foramt a message into a string for debug 
 *
 * @param [in] pRxMsg 
 *  The  message to format 
 *
 * @param [out] bufStr 
 *  The output text
 *
 * @param [in] bufSize
 *  The output buffer size
 *
 * @return None
 *
// *****************************************************************************/
void UartControllerFormatMessage(const struct UARC_MSG* pRxMsg, 
    char bufStr[] ,
    uint32_t bufSize)
{
    char payloadBuf[128];
    
    uint32_t numToCopy = pRxMsg->length;
    
    // Allow for adding a null term
    if (numToCopy > sizeof(payloadBuf) - 1)
    {
        numToCopy = sizeof(payloadBuf) - 1;
        strncat(bufStr, "(truncated) ", bufSize);
    } 
    
    memcpy(payloadBuf, pRxMsg->pPayload, numToCopy);
    payloadBuf[numToCopy] = '\0';    
    
    snprintf(bufStr, bufSize, "Type = %s (0x%02X), Payload len = %u, Payload = %s", 
            GetTypeString((E_UARC_MSG_TYPE)pRxMsg->type),
            (uint32_t)pRxMsg->type, 
            pRxMsg->length,
            payloadBuf );
    

    
    //strncat(bufStr, payloadBuf, bufSize - (strlen(bufStr) + 1));
}


/*****************************************************************************
 * @brief
 *   Helper to get a text string for the msg type
 *
 * @param [in] type 
 *  The message type enumeration
 *
 * @return The string representation
 *
// *****************************************************************************/
static const char* GetTypeString(const E_UARC_MSG_TYPE type)
{
    switch (type)
    {
        case  UARC_MT_NULL:
            return "UARC_MT_NULL";
        
        case UARC_MT_CMD:
            return "UARC_MT_CMD";
        
        case UARC_MT_PING:
            return "UARC_MT_PING";
        
        case UARC_MT_HTTP_CMD:
            return "UARC_MT_HTTP_CMD";

        case UARC_MT_ERROR:
            return "UARC_MT_ERROR";
        
        case UARC_MT_RESP:
            return "UARC_MT_RESP";
        
        case UARC_MT_GNIP:
            return "UARC_MT_GNIP";
        
        case UARC_MT_ASYNC:
            return "UARC_MT_ASYNC";
                
        case UARC_MT_HTTP_RSP:
            return "UARC_MT_HTTP_RSP";
            
        case UARC_MT_HEARTBEAT:    
            return "UARC_MT_HEARTBEAT";
        
        default:
            return "Unknown Message Type";
    }
    
}



/**
 * @}
 */
