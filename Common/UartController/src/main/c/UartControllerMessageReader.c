/*****************************************************************************
******************************************************************************
**
**         Filename: UartControllerMessageReader.c
**    
**          Author: Duncan Willis
**          Created: 17 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file UartControllerMessageReader.c
*
* @brief Part of UART Controller handles parsing of framed message
* 
******************************************************************************/ 


/**
 * @addtogroup UARTController
 * @{
 */


#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "CommonTypes.h"
#include "Crc32Fast.h"
#include "UartControllerMessageReader.h"
 



/*****************************************************************************
 * @brief
 *   Initialise a message
 * 
 * @param [in] pMsg 
 *  The message to initialise - associate with memory area.
 * 
 * @param [in] pBuffer 
 *  Payload area * 
 * @param [in] bufferSize 
 *  Payload area, size in bytes
 *
 * @return None. Note the message points to an area of externally allocated memory.
 *****************************************************************************/
void UartMessageInit(UARC_MSG* pMsg, uint8_t* pBuffer,  uint32_t bufferSize)
{
    ASSERT(NULL != pBuffer);
    ASSERT(NULL != pMsg);

    memset(pMsg, 0, sizeof (UARC_MSG));   
    pMsg->pPayloadMaxSize = bufferSize;
    pMsg->pPayload = pBuffer;
}


/*****************************************************************************
 * @brief
 *   Initialise a reader
 * 
 * @param [in] pMsgReader 
 *  The reader object to initialise
 * 
 * @param pBuffer 
 *  Working memory buffer that the reader assembles a msg into
 * @param bufferSize 
 *  Size of working memory
 *
 * @return None. 
 * @ Note The reader accepts incoming bytes, and outputs a message structure.
 * This message is assembled in the memory supplied into this function.
 *****************************************************************************/
void UartMessageReaderInit (UARC_MSG_READER* pMsgReader,  
    uint8_t* pBuffer,  
    uint32_t bufferSize) 
{
    ASSERT(NULL != pMsgReader);
    ASSERT(NULL != pBuffer);

    memset(pMsgReader, 0, sizeof (UARC_MSG_READER));

    UartMessageInit(&(pMsgReader->rxMsg), pBuffer, bufferSize);
    
    pMsgReader->crc32Seed = CRC_SEED;

    UartMessageReaderReset(pMsgReader);
}


/*****************************************************************************
 * @brief
 *    Reset the state of the reader
 * 
 * @param [in] pMsgReader 
 *  The reader object to reset
 *
 * @return None
 *****************************************************************************/
void UartMessageReaderReset (UARC_MSG_READER* pMsgReader)
{
    pMsgReader->state           = E_MRS_HDR1;
    pMsgReader->crcErrorCount = 0;
    pMsgReader->byteCount = 0;
    pMsgReader->unsyncCount = 0;
    pMsgReader->oversizeCount   = 0;
}


/*****************************************************************************
 * @brief
 *   Handle an incoming byte
 * 
 * @param [in] pMsgReader 
 *  The reader object to use
 * 
 * @param [in] bIn 
 *  The byte to read
 *
 * @return Pointer to a msg, if received and CRC OK, else NULL
 *
 *****************************************************************************/
const UARC_MSG* UartMessageReaderReadByte(UARC_MSG_READER* pMsgReader, uint8_t bIn)
{
    const UARC_MSG* pRxMsgReturn = NULL;
    E_MSG_READER_STATE nextState = E_MRS_UNINITIALISED; 
    
    ASSERT(pMsgReader != NULL);

    pMsgReader->byteCount++;

    switch (pMsgReader->state)
    {
    case E_MRS_HDR1:
        if (MRS_HEADER_1 == bIn )
        {
            nextState = E_MRS_HDR2;
        }
        else
        {
            pMsgReader->unsyncCount++;
            nextState = E_MRS_HDR1;
        }
        break;

    case E_MRS_HDR2:
        pMsgReader->crc32 = pMsgReader->crc32Seed; 
        pMsgReader->byteIndex = 0;
        if (MRS_HEADER_2 == bIn )
        {
            nextState = E_MRS_LEN1;
        }
        else
        {            
            pMsgReader->unsyncCount++;
            nextState = E_MRS_HDR1;
        }
        break;           

    case E_MRS_LEN1:
        pMsgReader->rxMsg.length = (((uint32_t)bIn) << 0);
        pMsgReader->crc32 = FastCrc32Byte(pMsgReader->crc32, &bIn, 1);
        nextState = E_MRS_LEN2;
        break;

    case E_MRS_LEN2:
        pMsgReader->rxMsg.length  |= (((uint32_t)bIn) << 8);
        if (pMsgReader->rxMsg.length > pMsgReader->rxMsg.pPayloadMaxSize)
        { 
            pMsgReader->oversizeCount++;
            nextState = E_MRS_HDR1; 
        }
        else
        {
            pMsgReader->crc32 = FastCrc32Byte(pMsgReader->crc32, &bIn, 1);
            nextState = E_MRS_TYPE;
        }
        break;

    case E_MRS_TYPE:
        pMsgReader->rxMsg.type = (uint32_t)bIn;
        pMsgReader->crc32 = FastCrc32Byte(pMsgReader->crc32, &bIn, 1);
        nextState =  (pMsgReader->rxMsg.length > 0) ? 
                                E_MRS_PAYLOAD : E_MRS_CRC1;
        break;       

    case E_MRS_PAYLOAD:
        pMsgReader->rxMsg.pPayload[pMsgReader->byteIndex] = bIn;
        pMsgReader->byteIndex++;
        pMsgReader->crc32 = FastCrc32Byte(pMsgReader->crc32, &bIn, 1);

        if ( pMsgReader->byteIndex >= pMsgReader->rxMsg.length ) 
        {
            nextState = E_MRS_CRC1;
        }
        else
        {
            nextState = E_MRS_PAYLOAD;
        }
        break;

    case E_MRS_CRC1:
        pMsgReader->crc32Received = (((uint32_t)bIn) << 0);
        nextState = E_MRS_CRC2;
        break;

    case E_MRS_CRC2:
        pMsgReader->crc32Received |= (((uint32_t)bIn) << 8);
        nextState = E_MRS_CRC3;
        break;

    case E_MRS_CRC3:
        pMsgReader->crc32Received |= (((uint32_t)bIn) << 16);            
        nextState = E_MRS_CRC4;
        break;            

    case E_MRS_CRC4:

        pMsgReader->crc32Received |= (((uint32_t)bIn) << 24);

        if (pMsgReader->crc32Received == pMsgReader->crc32)
        {
            pRxMsgReturn = &(pMsgReader->rxMsg);
        }
        else
        {
            pMsgReader->crcErrorCount++;
        }

        nextState = E_MRS_HDR1;
        break;           

    default:
        ASSERT(0);
        nextState = E_MRS_HDR1;
        break;
    }

    pMsgReader->state = nextState;

    return pRxMsgReturn;
}

/** 
 * @}
 */
