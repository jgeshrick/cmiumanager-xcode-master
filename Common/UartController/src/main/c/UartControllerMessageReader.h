/*****************************************************************************
******************************************************************************
**
**         Filename: UartControllerMessageReader.h
**    
**          Author: Duncan Willis
**          Created: 17 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file UartControllerMessageReader.h
*
* @brief Part of UART Controller handles parsing of framed message
* 
******************************************************************************/ 


/**
 * @addtogroup UARTController
 * @{
 */

#ifndef __UARC_MSG_READER_H__
#define __UARC_MSG_READER_H__

#include "UartControllerMessageTypes.h"


typedef enum 
{
     E_MRS_UNINITIALISED = 0,
     E_MRS_HDR1,
     E_MRS_HDR2,     
     E_MRS_LEN1,
     E_MRS_LEN2,
     E_MRS_TYPE,
     E_MRS_PAYLOAD,
     E_MRS_CRC1,
     E_MRS_CRC2, 
     E_MRS_CRC3,         
     E_MRS_CRC4            
}
E_MSG_READER_STATE;


typedef struct UARC_MSG_READER
{
    uint32_t    byteIndex;
    uint32_t    type;
    uint32_t    crc32Received;
    uint32_t    crc32;
    uint32_t    crc32Seed;
    E_MSG_READER_STATE state;
    UARC_MSG    rxMsg;
    uint32_t    byteCount;          // Number of bytes handled since init.
    uint32_t    unsyncCount;        // Number of bytes handled while not decoding a packet (not necessarily an error)
    uint32_t    crcErrorCount;      // Num Packets with bad CRC
    uint32_t    oversizeCount;      // Num Packets having length > available rx buffer.
}
UARC_MSG_READER;
 


void UartMessageInit(UARC_MSG* pMsg, uint8_t* pBuffer,  uint32_t bufferSize);

void UartMessageReaderInit (UARC_MSG_READER* pMsgReader,  
    uint8_t* pBuffer,  
    uint32_t bufferSize) ;

void UartMessageReaderReset(UARC_MSG_READER* pMsgReader);

const UARC_MSG* UartMessageReaderReadByte  (UARC_MSG_READER* pMsgReader, uint8_t bIn);



#endif

/** 
 * @}
 */
