/*****************************************************************************
******************************************************************************
**
**         Filename: UartControllerMessageWriter.h
**    
**          Author: Duncan Willis
**          Created: 30 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file UartControllerMessageReader.h
*
* @brief Part of UART Controller handles writing of framed message
* 
******************************************************************************/ 


/**
 * @addtogroup UARTController
 * @{
 */

#ifndef __UARC_MSG_WRITER_H__
#define __UARC_MSG_WRITER_H__

#include "RingBuffer.h"
#include "UartControllerMessageTypes.h"


typedef struct UARC_MSG_WRITER
{
    uint32_t            byteIndex;
    uint8_t*            pBuffer;
    uint32_t            bufferMaxSize;
    RingBufferT*        pRingbuffer;
    void (*pByteWriter) (struct UARC_MSG_WRITER* pMw, uint8_t b);
}
UARC_MSG_WRITER;


typedef void (*pfnWriteByteT)(  UARC_MSG_WRITER* pMw, uint8_t b);


uint32_t UartControllerMessageWriterWriteBuffer(UARC_MSG_WRITER* pMw,
    const UARC_TX_MSG* pTxMsg,
    uint8_t*    pBuffer,
    uint32_t    bufferMaxSize);


uint32_t UartControllerMessageWriterWriteRingBuffer(UARC_MSG_WRITER* pMw,
    const UARC_TX_MSG* pTxMsg,
    RingBufferT* pRb);


uint32_t UartControllerMessageWriterCalculateSize(const UARC_TX_MSG* pTxMsg);

void UartControllerMessageWriterWrite(const UARC_TX_MSG* pTxMsg,
    pfnWriteByteT pfnW);


#endif

/** 
 * @}
 */
