/*****************************************************************************
******************************************************************************
**
**         Filename: IntegrationTestUartController.h
**    
**          Author: Duncan Willis
**          Created: 17 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file IntegrationTestUartController.h
*
* @brief Integration Tests for the UartController
* 
******************************************************************************/
 
#ifndef _UARC_INT_TEST_H__
#define _UARC_INT_TEST_H__

int32_t IntegrationTestsRunner(void);
void    UartController_TestMakePacket(void);
void    UartController_TestBasicLoopback(void);

#endif

/**
 * @}
 * @}
 */
