/*****************************************************************************
******************************************************************************
**
**         Filename: IntegrationTestUartController.c
**    
**          Author: Duncan Willis
**          Created: 17 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file IntegrationTestUartController.c
*
* @brief Integration Tests for the UartController
* 
******************************************************************************/

/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "unity.h"
#include "TeamCity.h"
#include "Ringbuffer.h"
#include "UartController.h"
#include "IntegrationTestUartController.h"


// These h/w specificfunctions must be supplied in the IntegrationTestWindows.c
// or IntegrationTest<platform>.c file.
extern uint32_t ClockTickGetMs(void); // Get a 32 bit number incrementing at TICK_CLOCKS_PER_SEC 
extern void TestPutc(int c);          // putc a char on debug output


static  uint32_t testsRunCount = 0;

static void ProcessTest (void);
static void DelayBlocking(uint32_t delayMs);

void tearDown(void) {;}
void mainSetup (void);


// The object to test
UART_CONTROLLER uc;
UART_CONTROLLER_INIT_PARAMS initParams;

RingBufferT rbRx;
RingBufferT rbTx;


// Max len of a debug string
#define RINGBUF_LEN 1024


/**
** Private Variables (File Local Variables)
**/
static uint8_t     rb1Data[RINGBUF_LEN];
static uint8_t     rb2Data[RINGBUF_LEN];




/**
** Setup for all Integration tests, done once at starting
**/
void mainSetup (void)
{
    // Init the testing objects
    RingBufferInit(&rbRx, rb1Data, sizeof(rb1Data));
    RingBufferInit(&rbTx, rb2Data, sizeof(rb2Data));
}


/**
** Setup for each test
**/
void setUp(void)   
{
    // Clear the ringbuffers
    RingBufferInit(&rbRx, rb1Data, sizeof(rb1Data));
    RingBufferInit(&rbTx, rb2Data, sizeof(rb2Data));

    initParams.pRbReceive  = &rbRx;
    initParams.pRbTransmit = &rbTx;
}


/**
** Runs the unit tests for module TCP Session Manager
**/
int32_t IntegrationTestsRunner(void)
{
    int testFailures = 0;

    Unity.TestFile = "IntegrationTestUartController.c";
    UnityBegin();

    printf("Testing module %s\n", Unity.TestFile);
    
    mainSetup ();

    // General tests
    RUN_TEST(UartController_TestMakePacket,                 __LINE__);
    RUN_TEST(UartController_TestBasicLoopback,              __LINE__);
  
    testFailures = UnityEnd();

    return testFailures;
}




// Test Data gets put in buffer OK
void UartController_TestSendHelper(void)
{
    uint32_t type = 0x55;
    uint8_t  data[10] = {1,2,3,4,5,6,7,8,9,10};
    uint32_t dataBytesToSend = 10;

    UartControllerInit(&uc, &initParams);

    UartControllerSend(&uc,
        type,
        data, 
        dataBytesToSend); 
    
    TEST_ASSERT_EQUAL_UINT8(type,               data[2]);
    TEST_ASSERT_EQUAL_UINT8(dataBytesToSend,    data[3]);
}


// Test 1. Basic 
void UartController_TestMakePacket(void)
{
    
    TEST_ASSERT_EQUAL_UINT32(1,    1);
}


// Test  
void UartController_TestBasicLoopback(void)
{
    
    TEST_ASSERT_EQUAL_UINT32(1,    1);
}


