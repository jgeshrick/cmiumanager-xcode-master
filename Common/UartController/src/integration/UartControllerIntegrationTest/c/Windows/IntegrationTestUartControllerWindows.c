/******************************************************************************
*******************************************************************************
**
**         Filename: IntegrationTestUartControllerWindows.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
** Revision History:
** 
**
**
**      Code Review:
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/**
 * @defgroup UartControllerIntegrationTest
 *
 * @brief Integration Test for the @ref UartController
 *
 */

/**
 * @addtogroup Integration TestUartController
 * @{
 */

/**
 * @addtogroup Test 
 * @{
 */

/**
 * @file
 *
 * @brief Integration Test runner for the @ref UartController
 * @author Duncan Willis
 * @date 2015.03.05  
 * @version 1.0
 */
 
#include <time.h>
#include "unity.h"
#include "TeamCity.h"
#include "IntegrationTestUartController.h"


extern void setUp(void);
extern void tearDown(void);

/** This function is used by unity */
void resetTest(void)
{
    tearDown();
    setUp();
}


/**
** Platform dependent:
** This function must return a 32bit number incrementing at TICK_CLOCKS_PER_SEC (is 1ms for Win on PC)
**/
uint32_t ClockTickGetMs(void)
{
    return (uint32_t)clock();
}


/**
** Platform dependent:
** This function must output a char from a serial port or other test output
**/
void TestPutc(int c)
{
    putchar(c);
}


/**
** Runs the unit tests for module TCP Session Manager
**/
int main(void)
{
    int testFailures = 0;

    testFailures = IntegrationTestsRunner();

    return testFailures;
}


/**
 * @}
 * @}
 */