/*****************************************************************************
******************************************************************************
**
**         Filename: UartControllerDll.c
**    
**          Author: Duncan Willis
**          Created: 17 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*****************************************************************************
* @file UartControllerDll.c
*
* @brief UART Controller - DLL wrapper to make usable in C#
*  
* 
******************************************************************************/

/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "CommonTypes.h"
#include "RingBuffer.h"
#include "UartController.h"


 
// Ringbuffers
// These need to be seen both by the UART ISR, and by the modem object, which will 
// keep a reference to them
#define RING_BUFFER_SIZE_RX                     4096  // depends on tick rate / data rate etc.
#define RING_BUFFER_SIZE_TX                     4096
static uint8_t                                  rxData[RING_BUFFER_SIZE_RX];
static uint8_t                                  txData[RING_BUFFER_SIZE_TX];
static RingBufferT                              rxRingBuffer;    
static RingBufferT                              txRingBuffer;   // Buffer for data out
 

// Assembly buffer for reader
// Max size of an rx payload 
#define RX_PAYLOAD_BUFFER_SIZE                  2048
static uint8_t                                  payloadBuffer[RX_PAYLOAD_BUFFER_SIZE];


static UART_CONTROLLER                          uc;
static UARC_MSG_READER                          mr;


static uint8_t* rxDataBuffer                    = NULL;
static uint32_t rxDataBufferSize                = 0;
static int32_t rxBytesReceived                  = -1;
static uint32_t typeReceived                    = 0;
static bool isInitialised                       = false;

static void OnReceiveMessage(const struct  UARC_MSG* pRxMsg, const void *context);



/*****************************************************************************
 * @brief
 *   Init the Uart Controller DLL object
 * 
 * @param None
 *
 * @return 
 *   None
 *
 *****************************************************************************/
DLL_EXPORT void UartControllerDllInit(void)
{
    UART_CONTROLLER_INIT_PARAMS initParams = {0};

    RingBufferInit(&rxRingBuffer, rxData, sizeof(rxData));
    RingBufferInit(&txRingBuffer, txData, sizeof(txData));

    initParams.pRbReceive       = &rxRingBuffer;
    initParams.pRbTransmit      = &txRingBuffer;
    initParams.pMessageReader   = &mr;
    initParams.sendInactivityThresholdMs = 2000;
    initParams.pfnRxHandler     = &OnReceiveMessage;
    
    rxBytesReceived             = -1;
    
    // Init reader to use the assembly message
    UartMessageReaderInit (&mr, payloadBuffer, sizeof(payloadBuffer));
    
    UartControllerInit(&uc, &initParams);
    isInitialised = true;
}


/*****************************************************************************
* @brief
*   Set a timer to wait for a response
*
* @param timeoutMs
*  The time wait to get response
*
* @return  0 if already in progress (busy), 1 if OK
*
*****************************************************************************/
DLL_EXPORT uint32_t UartControllerDllStartSendTimer(uint32_t timeoutMs)
{
    UartControllerStartSendTimer(&uc, timeoutMs);
    return 0;
}


/*****************************************************************************
* @brief
*   Get the state of the send operation
 
* @return  see underlying enumeration:-
*    UARC_MSS_READY_TO_SEND  = 0,
*    UARC_MSS_BUSY           = 1,
*    UARC_MSS_TIMEOUT        = 2
*****************************************************************************/
DLL_EXPORT uint32_t UartControllerDllGetSendState(uint32_t dummy)
{
    return (uint32_t)UartControllerGetSendState(&uc);
}


/*****************************************************************************
 * @brief
 *   Make a packet into the supplied mem 
 * 
 * @param [in] type 
 *  The type number of the msg
 * 
 * @param payload 
 *  Pointer to data to send
 *
 * @param payloadBytesToSend 
 *  The number of bytes (of payload) to send
 *
 *
 * @param pBuffer 
 *  The destination memory to pack into
 *
 * @param bufferMaxSize 
 *  The size of the supplied destination memory
 *
 * @return Number of bytes written or 0 if insufficient mem supplied.
 *
 *****************************************************************************/
DLL_EXPORT uint32_t UartControllerDllMakePacket(uint32_t type,
    const uint8_t* payload, 
    uint32_t payloadBytesToSend,
    uint8_t* pBuffer,
    uint32_t bufferMaxSize
    )
{ 
    UARC_MSG_WRITER     mw;
    UARC_TX_MSG         txMsg;
    uint32_t            numBytesWritten;

    txMsg.type          = type;
    txMsg.pPayload      = payload;
    txMsg.payloadLength = payloadBytesToSend;
  
    numBytesWritten = UartControllerMessageWriterWriteBuffer(&mw, 
        &txMsg,
        pBuffer,
        bufferMaxSize);

    return numBytesWritten;
}


/*****************************************************************************
 * @brief
 *   Send payload data, do not require ack 
 * 
 * @param [in] type 
 *  The type number of the msg
 * 
 * @param data 
 *  Pointer to data to send
 *
 * @param dataBytesToSend 
 *  The number of bytes (of data array) to send
 *
 * @return None
 *
 *****************************************************************************/
DLL_EXPORT void UartControllerDllSend(uint32_t type,
    const uint8_t* payload, 
    uint32_t payloadBytesToSend)
{ 
    UartControllerSend(&uc,type, payload, payloadBytesToSend);
}


/*****************************************************************************
 * @brief
 *   Handles a char that arrived from BTLE, byte is parsed
 *
 * @param [in] b 
 *  The received byte
 *
 * @return None
 *
 *****************************************************************************/
DLL_EXPORT void UartControllerDllOnRxByte(uint8_t b)
{
    UartControllerReadByte(&uc, b);
}


/*****************************************************************************
 * @brief
 *   Handles a message once received and checked. will be copied into a data
 * buffer supplied by the DLL's host
 *
 * @param [in] pRxMsg 
 *  The received message
 *
 * @return None
 *
// *****************************************************************************/
static void OnReceiveMessage(const struct  UARC_MSG*  pRxMsg, const void *context)
{
    uint32_t numToCopy = pRxMsg->length;

    if (NULL != rxDataBuffer)
    {
        if (-1 == rxBytesReceived)
        {
            if (numToCopy > rxDataBufferSize)
            {
                numToCopy = rxDataBufferSize;
            }

            memcpy(rxDataBuffer, pRxMsg->pPayload, numToCopy);
            typeReceived = pRxMsg->type;
            rxBytesReceived = numToCopy;
        }
    }
}


/*****************************************************************************
 * @brief
 *   Run a tick operation
 * 
 * @param [in] pUc 
 *  The reader controller to use
 * 
 * @param tickIntervalMs 
 *  The time between ticks in ms
 *
 * @return None
 *
 *****************************************************************************/
DLL_EXPORT void UartControllerDllTick(uint32_t tickIntervalMs)
{
    if (isInitialised)
    {
        UartControllerTick(&uc, tickIntervalMs);
    }
}


/*****************************************************************************
* @brief
*   Get the number of Keepalive that were sent. In case of DLL usage, the
*  messages cannot be sent internally from UrtController (as uses ringbuffer that
* is incompatible with DLL)
*
* @param [in] dummy
*  Dummy var for DLL
*
* @return The number of times the inactivity timer fired
*
*****************************************************************************/
DLL_EXPORT uint32_t UartControllerDllGetKeepaliveSentCount(uint32_t dummy)
{
    return uc.keepaliveSentCount;
}


/*****************************************************************************
* @brief
*   Get the time in ms since the last message was received
*
* @param [in] dummy
*  Dummy var for DLL
*
* @return Inactivity time in ms
*
*****************************************************************************/
DLL_EXPORT uint32_t UartControllerDllGetTimeSinceLastReceive(uint32_t dummy)
{
    return UartControllerGetTimeSinceLastReceive(&uc);
}


/*****************************************************************************
* @brief
*   Get the elapsed time in ms since the last message was sent
*
* @param [in] dummy
*  Dummy var for DLL
*
* @return Inactivity time in ms
*
*****************************************************************************/
DLL_EXPORT uint32_t UartControllerDllGetTimeSinceLastSend(uint32_t dummy)
{
    return UartControllerGetTimeSinceLastSend(&uc);
}



/*****************************************************************************
 * @brief
 *   Get status
 * 
 * @param [in] pUc 
 *  The reader controller to query
 *
 * @return 
 *
 *****************************************************************************/
DLL_EXPORT uint32_t UartControllerDllIsBusy(void)
{
    uint32_t n = 0;
    
    return n;
}


/*****************************************************************************
 * @brief
 *   Set Rx Buffer 
 * 
 * @param data 
 *  Pointer to  a buffer owned by the DLL's host
 *
 * @param dataBytes 
 *  The size of the buffer
 * @return None
 *
 *****************************************************************************/
DLL_EXPORT void UartControllerDllSetRxBuffer(uint8_t* data, 
    uint32_t dataBytes)
{ 
    rxDataBuffer = data;
    rxDataBufferSize = dataBytes;

    // Test sig
    memcpy(rxDataBuffer, "111111111111111", 10);
}


/******************************************************************************
**  @brief Get the number of payload bytes available from last message
**
**  @param doReset if 0, do not reset the state (new message will be ignored)
**  @return The number of chars copied into the rx buffer (0 if an empty message, 
**    or -1 if no message available)
**
**  @note This should be polled by the main app to see if a message has been received
* The number is cleared on calling this function. if doReset != 0
******************************************************************************/
DLL_EXPORT int32_t
UartControllerDllGetRxMessageByteCount(uint32_t doReset)
{
    int32_t retVal = rxBytesReceived;

    if (doReset != 0)
    {
        rxBytesReceived = -1;
    }
    return retVal;
}


/******************************************************************************
**  @brief Get the type of the last message
**
**  @return The type
**
**  @note Call this after receiving a decoded message 
******************************************************************************/
DLL_EXPORT uint32_t
UartControllerDllGetRxMessageType(uint32_t dummy)
{
    return typeReceived;
}





#if 0 // Test only

DLL_EXPORT uint32_t UartControllerDllGetBuffer(uint32_t x)
{
    return (uint32_t)rxDataBuffer;
}

/******************************************************************************
**  @brief This should be polled by the main app, and trace data then emitted  
** over BTLE   
**
**  @param dataToSend. A buffer to receive the packetised data (to be sent over BTLE)
**  @param dataArraySize The size of the buffer
**
**  @eturn The number of chars copied into the buffer (0 if none available)
**

******************************************************************************/
DLL_EXPORT uint32_t 
UartControllerDllGetCharsToSend(unsigned char* dataToSend, uint32_t dataArraySize)
{
    uint32_t txCount = 0;

    // Data in ring buf to send over BTLE?
    txCount = RingBufferUsed(&txRingBuffer);

    if (txCount > dataArraySize)
    {
        txCount = dataArraySize;
    }

    if (txCount > 0)
    {
        RingBufferRead(&txRingBuffer, dataToSend, txCount);
    }

    return txCount;
}

/*****************************************************************************
* @brief
*    Get a byte
*
* @param [in]
*  The
*
* @return
*
*****************************************************************************/
DLL_EXPORT uint32_t UartControllerGetPayloadByte(uint32_t index)
{
    if (index >= 256) return 0;

    if (NULL == rxDataBuffer) return 0;
    return rxDataBuffer[index];
}
#endif
/**
 * @}
 */
