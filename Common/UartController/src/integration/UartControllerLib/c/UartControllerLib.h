/*****************************************************************************
******************************************************************************
**
**         Filename: UartControllerDll.h
**    
**          Author: Duncan Willis
**          Created: 27 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file UartControllerDll.h
*
* @brief UART Controller - DLL wrapper to make it usable in C#
*  
* 
******************************************************************************/


#ifndef _UART_CONTROLLER_DLL_H
#define _UART_CONTROLLER_DLL_H


DLL_EXPORT void UartControllerDllInit(void);

DLL_EXPORT uint32_t UartControllerDllStartSendTimer(uint32_t timeoutMs);

DLL_EXPORT uint32_t UartControllerDllGetSendState(uint32_t dummy);

DLL_EXPORT void UartControllerDllOnRxByte(uint8_t b);

DLL_EXPORT void UartControllerDllTick(uint32_t tickIntervalMs);
 
DLL_EXPORT void UartControllerDllSend(uint32_t type,
    const uint8_t* payload, 
    uint32_t payloadBytesToSend);

DLL_EXPORT void UartControllerDllSetRxBuffer(uint8_t* data, uint32_t dataBytes);

DLL_EXPORT uint32_t 
    UartControllerDllGetRxMessageByteCount(uint32_t doReset);

DLL_EXPORT uint32_t
    UartControllerDllGetRxMessageType(uint32_t dummy);
#endif
