@echo off
cls
echo "Commencing Trinity Build"
"C:\Program Files (x86)\National Instruments\LabVIEW 2015\LabVIEW.exe" "..\..\BuildTools\LabView\2011\Build Targets via Command Line.vi" -- "PcTools\Trinity\trinity.lvproj" "TeamCity" "My Computer"


echo "PcTools\Trinity\ and Sub-Directories"
@echo on
dir /s *.*
@echo off


echo "Build complete"
exit

