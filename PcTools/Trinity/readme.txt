To run Trinity.exe you will need to download the LabVIEW runtime.  This is available online at http://www.ni.com/download/labview-run-time-engine-2011-sp1/2897/en/.  See also "LabVIEW Runtime Download.lnk" in the same folder as this readme.

example.csv is provided to illustrate the necessary format that should be used for batch additions of units via trinity.