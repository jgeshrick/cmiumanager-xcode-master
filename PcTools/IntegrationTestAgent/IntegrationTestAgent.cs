﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Threading;

using System.IO.Ports;

namespace Runner
{
    class Program
    {
        private static SerialPort EfmOutput;

        static int Main(string[] args)
        {
            //Set return code, and get path to JLink commander
            int ret = -1;
            String programmerPath = System.AppDomain.CurrentDomain.BaseDirectory + "Resources\\JLink\\JLink.exe";

            //Attempt to open serial port
            EfmOutput = new SerialPort("COM1", 115200);
            try
            {
                EfmOutput.Open();
            }
            catch
            {
                Console.WriteLine("Could not open COM port");
                ret = 2;
            }

            //Open the serial port and assign a handler
            if (EfmOutput.IsOpen)
            {
                EfmOutput.DataReceived += new SerialDataReceivedEventHandler(EfmOutput_DataReceived);
            }

            //Program the EFM32
            ret = ProgramEfm32(programmerPath, args[0]);
            if (ret == 0)
            {
                //Reset the EFM32 to start the unit tests
                ret = ResetEfm32(programmerPath);
            }

            //If programming or reseting the EFM32 returned an error; exit
            if (ret != 0)
            {
                printErrorCodes();
                Environment.Exit(ret);
            }

            //Delay for fifteen seconds while the unit tests run and print to the console
            for (int i = 0; i < 15; i++)
            {
                Thread.Sleep(1000);
            }

            //Print error codes and exit
            printErrorCodes();
            return ret;
        }

        ///////////////////////////////////////////////////////////////////////////////
        //  Function:   ResetEfm32
        //   Purpose:   Using J-link Command to reset the attached uC
        // Arguments:   programmerPath - Path to the programmer  
        //   Returns:   void
        ///////////////////////////////////////////////////////////////////////////////
        private static int ResetEfm32(string programmerPath)
        {
            string result = "";
            string error = "";

            Process programmerProcess = SetupProcess(programmerPath);
            programmerProcess.OutputDataReceived += (sender, args) => result = result + args.Data + "\n";
            programmerProcess.ErrorDataReceived += (sender, args) => error = error + args.Data + "\n";
            programmerProcess.Start();
            programmerProcess.BeginOutputReadLine();
            programmerProcess.BeginErrorReadLine();

            Thread.Sleep(200);
            programmerProcess.StandardInput.WriteLine("r");
            Thread.Sleep(900);
            programmerProcess.StandardInput.WriteLine("g");
            Thread.Sleep(500);
            programmerProcess.StandardInput.WriteLine("exit");
            Thread.Sleep(100);

            try
            {
                programmerProcess.WaitForExit(1000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                programmerProcess.Kill();
            }

            Console.WriteLine(result);

            if (result.Contains("ERROR"))
            {
                return 1;
            }

            return 0;
        }

        ///////////////////////////////////////////////////////////////////////////////
        //  Function:   ProgramEfm32
        //   Purpose:   Using J-link Command to program the EFM32GG990F1024 uC
        // Arguments:   binFilePath - path to the bin file to program
        //              programmerPath - Path to the programmer
        //   Returns:   void
        ///////////////////////////////////////////////////////////////////////////////
        private static int ProgramEfm32(string programmerPath, string binFilePath)
        {
            string result = "";
            string error = "";

            Process programmerProcess = SetupProcess(programmerPath);
            programmerProcess.StartInfo.Arguments = "-SelectEmuBySN 440020189";
            programmerProcess.OutputDataReceived += (sender, args) => result = result + args.Data + "\n";
            programmerProcess.ErrorDataReceived += (sender, args) => error = error + args.Data + "\n";
            programmerProcess.Start();
            programmerProcess.BeginOutputReadLine();
            programmerProcess.BeginErrorReadLine();

            Thread.Sleep(1000);
            programmerProcess.StandardInput.WriteLine("exec device = EFM32GG990F1024");
            Thread.Sleep(1000);
            programmerProcess.StandardInput.WriteLine("loadbin {0} , 0x00", binFilePath);
            Thread.Sleep(45000);
            programmerProcess.StandardInput.WriteLine("exit");
            Thread.Sleep(100);

            try
            {
                programmerProcess.WaitForExit(1000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                programmerProcess.Kill();
            }

            Console.WriteLine(result);

            if (result.Contains("ERROR"))
            {
                return 1;
            }

            return 0;
        }

        ///////////////////////////////////////////////////////////////////////////////
        //  Function:   SetupProcess
        //   Purpose:   Starts a process with the relevant options
        // Arguments:   processPath - path to the application to start
        //   Returns:   process
        ///////////////////////////////////////////////////////////////////////////////
        private static Process SetupProcess(string processPath)
        {
            Process l_process = new Process();
            l_process.StartInfo.UseShellExecute = false;
            l_process.StartInfo.RedirectStandardOutput = true;
            l_process.StartInfo.RedirectStandardError = true;
            l_process.StartInfo.RedirectStandardInput = true;
            l_process.StartInfo.FileName = processPath;

            return l_process;
        }

        ///////////////////////////////////////////////////////////////////////////////
        //  Function:   EfmOutput_DataReceived
        //   Purpose:   Copy serial output from EFM dev board to console
        // Arguments:   sender - information about the serial device that called the 
        //              function
        //              e - serial event type
        //   Returns:   process
        ///////////////////////////////////////////////////////////////////////////////
        static void EfmOutput_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            String line = EfmOutput.ReadExisting();
            Console.Write(line);
        }

        ///////////////////////////////////////////////////////////////////////////////
        //  Function:   printErrorCodes
        //   Purpose:   print possible error codes to the console
        ///////////////////////////////////////////////////////////////////////////////
        static void printErrorCodes()
        {
            Console.Write("\n\n");
            Console.WriteLine("Error Codes");
            Console.WriteLine("0 - No error");
            Console.WriteLine("1 - Error programming or reseting dev board");
            Console.WriteLine("2 - Error accessing COM port");
        }
    }
}
