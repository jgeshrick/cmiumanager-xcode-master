/******************************************************************************
*******************************************************************************
**
**         Filename: config.h
**    
**           Author: Lawrence Wimble <lwimble@redstone-labs.com>
**          Created: 5/24/2010
**
**     Last Edit By: L. Wimble
**        Last Edit: Created
**    
**         Comments: This file contains the master config for the bootloader
**                   project.
**
** Revision History:
**         xx/xx/xx: 
**
**    Copyright 2010 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/

/*=========================================================================*/
/* D E F I N I T I O N S
/*=========================================================================*/

#ifndef __CONFIG_H
#define __CONFIG_H

#define UI_FLASH_SIZE   0x10000

/* Image Index */
#define IMG_APP1       1
#define IMG_CFG        3
//#define IMG_KEYTABLE   4

/* Version and CRC Constants */
#define CRC_SIZE        0x0004UL
#define VERSION_SIZE    0x0004UL

/* BootLoader SPACE */
#define BASIC_BL_ADDR 0x0000
#define BASIC_BL_SECTORS 8
#define BASIC_BL_ADDR2 0xFC00
#define BASIC_BL_SECTORS2 1

/* NFC AES Key TABLE */
#define KEYTABLE_ADDR 0x2000
#define KEYTABLE_SECTORS 1UL
#define KEYTABLE_RECORDS ((KEYTABLE_SECTORS * FLASH_SECTOR_SIZE) / 256)

/* Application 1 SPACE */
#define APP1_ADDR 0x2400UL
#define APP1_SECTORS 51UL
#define APP1_RECORDS  ((APP1_SECTORS * FLASH_SECTOR_SIZE) / 256UL)
#define APP1_VERSION  ((APP1_ADDR + (APP1_SECTORS * FLASH_SECTOR_SIZE)) - (CRC_SIZE + VERSION_SIZE))


/* Meter Configuration storage */
#define CONFIG_ADDR 0xF000   
#define CONFIG_SECTORS 1
#define CONFIG_RECORDS  ((CONFIG_SECTORS * FLASH_SECTOR_SIZE) / 256UL)

/* ARB Configuration storage */
#define ARBCONFIG_ADDR 0xF800 
#define ARBCONFIG_LEN  31  
#define ARBCONFIG_CS_LOC (ARBCONFIG_ADDR + ARBCONFIG_LEN)
#define ARBCONFIG_SECTORS 1
#define ARBCONFIG_RECORDS  ((ARBCONFIG_SECTORS * FLASH_SECTOR_SIZE) / 256UL)

#endif
