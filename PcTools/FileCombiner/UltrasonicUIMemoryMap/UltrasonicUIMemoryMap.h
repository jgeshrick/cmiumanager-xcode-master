// UltrasonicUIMemoryMap.h

#pragma once

#include "config.h"

using namespace System;

namespace UltrasonicUIMemoryMap 
{
	public ref struct Flash
	{
		literal Int32 TotalSize = UI_FLASH_SIZE;
	};

	public ref struct Bootloader
	{
		// Bootloader memory is in two blocks with the CRC at the end of the second.
		literal Int32 BaseAddress = BASIC_BL_ADDR;
		literal Int32 Length = BASIC_BL_SECTORS * 1024;

		literal Int32 ExtAddress = BASIC_BL_ADDR2;
		literal Int32 ExtLength = BASIC_BL_SECTORS2 * 1024;

		// NB: There is a 'lock' byte at the end of the image so we place the CRC 1 byte earlier to leave room for it.
		literal Int32 CrcAddress = ExtAddress + ExtLength - (4 + 1);
	};
	
	public ref struct  UserKeyTable
	{
		literal Int32 BaseAddress = KEYTABLE_ADDR;
		literal Int32 Length = KEYTABLE_SECTORS * 1024;
		literal Int32 CrcAddress = BaseAddress + Length - 4;
	};

	public ref struct  UserInterfaceApp
	{
		literal Int32 BaseAddress = APP1_ADDR;
		literal Int32 Length = APP1_SECTORS * 1024;
		literal Int32 CrcAddress = BaseAddress + Length - 4;
	};

	public ref struct Config
	{
		literal Int32 BaseAddress = CONFIG_ADDR;
		literal Int32 Length = CONFIG_SECTORS * 1024;
		literal Int32 CrcAddress = BaseAddress + Length - 4;
	};
}
