/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2013 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/
#ifndef __METER_MEMORY_MAP_H__
#define __METER_MEMORY_MAP_H__
/**
 * @defgroup MeterMemoryMap MCM Memory Map
 *
 * @brief Defines a set of macros to be used by both the applications and
 * scatter files on the metrology processor.
 *
 * @warning Syntax in this file must be compatible with both the C preprocessor
 * and scatter file.
 *
 * Hardware resources used: None
 */

/**
 * @addtogroup MeterMemoryMap
 * @{
 */

/**
 * @file
 *
 * @brief Flash layout
 * @author Peter Bonham
 * @date 2013.01.08 (8th Jan 2013)
 * @copyright Sagentia Limited, 2013
 * @version 1.0
 */
 
/** @{
 * @name Application Sizes
 * Define the sizes of the three applications.
 */
#define BOOTLOADER_SIZE         0x3000
#define RESTRICTED_SIZE         0x2400
#define RAW_CAL_DATA_SIZE       0x1400
#define METERING_SIZE           0x5800 /* Not including calibration tables. */
#define METER_INSTANCE_CAL_SIZE 0x0400
#define METER_CLASS_CAL_SIZE    0x3C00

/** @{
 * @name Flash sizes
 * Define total and page flash size
 */
#define MCU_TOTAL_FLASH_SIZE_NORMAL   0x10000
#define MCU_TOTAL_FLASH_SIZE_EXTENDED 0x20000

#define MCU_TOTAL_FLASH_SIZE          (MCU_TOTAL_FLASH_SIZE_EXTENDED)
#define MCU_FLASH_PAGE_SIZE           0x0400

/** @} */

/** @{
 * @name Application Base and End Addresses
 * Defines the base locations of the three applications and calibration tables.
 *
 * @note End addresses are the address of the first flash location @em after
 * the application.
 *
 * @note The meter instance and class information in the lower half
 * of memory (below 0x10000) are for the default 5/8" meter. These can
 * be overwritten by one of the sets of instance/class information from
 * the upper half of memory when another class of meter is required.
 * This update is performed by the restricted application.
 * It is intended that this mechanism will be superseded allowing a Sim3L14x
 * device (64kB flash rather than 128kB and 16kB RAM rather than 32kB) so 
 * make it easy to remove all of the 'extended' flash definitions and use 
 * linker definitions for 16kB RAM to ensure that this is possible.
 */
#define BOOTLOADER_BASE          0x00000000
#define BOOTLOADER_END           ((BOOTLOADER_BASE) + (BOOTLOADER_SIZE))

#define RESTRICTED_BASE          (BOOTLOADER_END)
#define RESTRICTED_END           (RESTRICTED_BASE + RESTRICTED_SIZE)

#define RAW_CAL_DATA_BASE        (RESTRICTED_END)
#define RAW_CAL_DATA_END         (RAW_CAL_DATA_BASE + RAW_CAL_DATA_SIZE)

#define METERING_BASE            (RAW_CAL_DATA_END)
#define METERING_END             ((METERING_BASE) + (METERING_SIZE))

#define METER_INSTANCE_CAL_BASE  (METERING_END)
#define METER_INSTANCE_CAL_END   ((METER_INSTANCE_CAL_BASE) + (METER_INSTANCE_CAL_SIZE))
#define METER_UNIT_CAL_ADDRESS   (METER_INSTANCE_CAL_BASE)

#define METER_CLASS_CAL_BASE     (METER_INSTANCE_CAL_END)
#define METER_CLASS_CAL_END      ((METER_CLASS_CAL_BASE) + (METER_CLASS_CAL_SIZE))
#define METER_FAMILY_CAL_ADDRESS (METER_CLASS_CAL_BASE)

#define MCU_FLASH_END_NORMAL     (METER_CLASS_CAL_END)

/* Test that the applications all fit into memory */
#if MCU_TOTAL_FLASH_SIZE_NORMAL < MCU_FLASH_END_NORMAL
    #error MCU normal mode flash size exceeded
#endif

/* Now define the 'extended' areas of flash */
#define MCU_FLASH_EXTENDED_BASE     0x00010000
#if MCU_FLASH_EXTENDED_BASE < MCU_FLASH_END_NORMAL
    #error Normal and extended flash regions overlap
#endif

/* Location for 3/4" meter data (but don't enforce this in the option naming) */
#define METER_INSTANCE_OPT1_BASE    (MCU_FLASH_EXTENDED_BASE)
#define METER_INSTANCE_OPT1_END     ((METER_INSTANCE_OPT1_BASE) + (METER_INSTANCE_CAL_SIZE))
#define METER_CLASS_OPT1_BASE       (METER_INSTANCE_OPT1_END)
#define METER_CLASS_OPT1_END        ((METER_CLASS_OPT1_BASE) + (METER_CLASS_CAL_SIZE))

/* Location for 1" meter data */
#define METER_INSTANCE_OPT2_BASE    (METER_CLASS_OPT1_END)
#define METER_INSTANCE_OPT2_END     ((METER_INSTANCE_OPT2_BASE) + (METER_INSTANCE_CAL_SIZE))
#define METER_CLASS_OPT2_BASE       (METER_INSTANCE_OPT2_END)
#define METER_CLASS_OPT2_END        ((METER_CLASS_OPT2_BASE) + (METER_CLASS_CAL_SIZE))

/* Location for 1.5" meter data */
#define METER_INSTANCE_OPT3_BASE    (METER_CLASS_OPT2_END)
#define METER_INSTANCE_OPT3_END     ((METER_INSTANCE_OPT3_BASE) + (METER_INSTANCE_CAL_SIZE))
#define METER_CLASS_OPT3_BASE       (METER_INSTANCE_OPT3_END)
#define METER_CLASS_OPT3_END        ((METER_CLASS_OPT3_BASE) + (METER_CLASS_CAL_SIZE))

/* Location for 2" meter data */
#define METER_INSTANCE_OPT4_BASE    (METER_CLASS_OPT3_END)
#define METER_INSTANCE_OPT4_END     ((METER_INSTANCE_OPT4_BASE) + (METER_INSTANCE_CAL_SIZE))
#define METER_CLASS_OPT4_BASE       (METER_INSTANCE_OPT4_END)
#define METER_CLASS_OPT4_END        ((METER_CLASS_OPT4_BASE) + (METER_CLASS_CAL_SIZE))

#define MCU_FLASH_END_EXTENDED      (METER_CLASS_OPT4_END)

/* Test that the options all fit into memory */
#if MCU_TOTAL_FLASH_SIZE_EXTENDED < MCU_FLASH_END_EXTENDED
    #error MCU extended mode flash size exceeded
#endif


/** @{
 * @name Application and correction table CRC locations
 */
/** Application and correction table CRC size
  * NB: The CRCs are calculated as 16-bit numbers but must be 4-byte aligned
  */
#define APPLICATION_CRC_SIZE 0x04 
#define BOOTLOADER_CRC         	((BOOTLOADER_END) - (APPLICATION_CRC_SIZE))
#define METERING_CRC           	((METERING_END) - (APPLICATION_CRC_SIZE))
#define METER_CLASS_CAL_CRC    	((METER_CLASS_CAL_END) - (APPLICATION_CRC_SIZE))
#define METER_INSTANCE_CAL_CRC 	((METER_INSTANCE_CAL_END) - (APPLICATION_CRC_SIZE))
#define RESTRICTED_CRC         	((RESTRICTED_END) - (APPLICATION_CRC_SIZE))
#define	METER_INSTANCE_OPT1_CRC	((METER_INSTANCE_OPT1_END) - (APPLICATION_CRC_SIZE))
#define	METER_CLASS_OPT1_CRC		((METER_CLASS_OPT1_END) - (APPLICATION_CRC_SIZE))
#define	METER_INSTANCE_OPT2_CRC	((METER_INSTANCE_OPT2_END) - (APPLICATION_CRC_SIZE))
#define	METER_CLASS_OPT2_CRC		((METER_CLASS_OPT2_END) - (APPLICATION_CRC_SIZE))
#define	METER_INSTANCE_OPT3_CRC	((METER_INSTANCE_OPT3_END) - (APPLICATION_CRC_SIZE))
#define	METER_CLASS_OPT3_CRC		((METER_CLASS_OPT3_END) - (APPLICATION_CRC_SIZE))
#define	METER_INSTANCE_OPT4_CRC	((METER_INSTANCE_OPT4_END) - (APPLICATION_CRC_SIZE))
#define	METER_CLASS_OPT4_CRC		((METER_CLASS_OPT4_END) - (APPLICATION_CRC_SIZE))

/** The following gives the offset into the space reserved for the CRC 
 * (APPLICATION_CRC_SIZE defined above) at which the CRC is stored.
 */
#define APPLICATION_CRC_OFFSET  0x02
/** @} */


/** @{
 * @name Application version number
 */
#define APP_VERSION_SIZE 0x04 /** >= size of Version_t - must be 4 byte aligned */
#define BOOTLOADER_VERSION_ADDRESS ((BOOTLOADER_CRC) - (APP_VERSION_SIZE))
#define METERING_VERSION_ADDRESS ((METERING_CRC) - (APP_VERSION_SIZE))
#define RESTRICTED_VERSION_ADDRESS ((RESTRICTED_CRC) - (APP_VERSION_SIZE))


/** @} */

/** @{
 * @name Bootloader Flash Regions */
#ifdef BOOTLOADER_APPLICATION
    #define MCU_FLASH_BASE  BOOTLOADER_BASE
    #define MCU_FLASH_SIZE  BOOTLOADER_SIZE

    #define METER_CRC_ADDRESS    BOOTLOADER_CRC
    #define APP_VERSION_ADDRESS  BOOTLOADER_VERSION_ADDRESS
/** @} */

/** @{
 * @name Metering Application Flash Regions
 * @note Calibration tables and correction tables are compiled into the Metering
 * Application hex file. The binary file conversion should only include the
 * Metering Application for bootloading.  */
#else
    #ifdef METERING_APPLICATION
        #define MCU_FLASH_BASE  METERING_BASE
        #define MCU_FLASH_SIZE  (METERING_SIZE)

        #define METER_CRC_ADDRESS    METERING_CRC
        #define APP_VERSION_ADDRESS  METERING_VERSION_ADDRESS
/** @} */

/** @{
 * @name Restricted Application Flash Regions */
    #else
        #ifdef RESTRICTED_APPLICATION
            #define MCU_FLASH_BASE  RESTRICTED_BASE
            #define MCU_FLASH_SIZE  RESTRICTED_SIZE

            #define METER_CRC_ADDRESS    RESTRICTED_CRC
            #define APP_VERSION_ADDRESS  RESTRICTED_VERSION_ADDRESS
        #endif

/** @} */
    #endif
#endif

#endif /*  __METER_MEMORY_MAP_H__ */

