// UltrasonicMemoryMap.h

// This project is to allow access to the content of MeterMemoryMap.h from C# 

#pragma once

#include "MeterMemoryMap.h"

using namespace System;

namespace UltrasonicMemoryMap 
{
	public ref struct MemorySize
	{
		literal Int32 Bootloader = BOOTLOADER_SIZE;
		literal Int32 Restricted = RESTRICTED_SIZE;
		literal Int32 RawCalData = RAW_CAL_DATA_SIZE;
		literal Int32 Metering = METERING_SIZE;
		literal Int32 MeterInstanceCal = METER_INSTANCE_CAL_SIZE;
		literal Int32 MeterClassCal = METER_CLASS_CAL_SIZE;

		literal Int32 Crc = APPLICATION_CRC_SIZE - APPLICATION_CRC_OFFSET;
		literal Int32 Version = APP_VERSION_SIZE;
	};

	public ref struct FlashSize
	{
		literal Int32 McuTotalNormal = MCU_TOTAL_FLASH_SIZE_NORMAL;
		literal Int32 McuTotalExtended = MCU_TOTAL_FLASH_SIZE_EXTENDED;
		literal Int32 McuTotal = MCU_TOTAL_FLASH_SIZE;
		literal Int32 McuPage = MCU_FLASH_PAGE_SIZE;
	};

	public ref struct MeterMap
	{
		literal Int32 BootloaderBase = BOOTLOADER_BASE;
		literal Int32 BootloaderEnd = BOOTLOADER_END;
		literal Int32 RestrictedBase = RESTRICTED_BASE;
		literal Int32 RestrictedEnd = RESTRICTED_END;
		literal Int32 RawCalDataBase = RAW_CAL_DATA_BASE;
		literal Int32 RawCalDataEnd = RAW_CAL_DATA_END;
		literal Int32 MeteringBase = METERING_BASE;
		literal Int32 MeteringEnd = METERING_END;

		// Location at which the Metrology Application acceses the instance data.
		// Initially this contains the default (5/8" meter) data but may be 
		// overwritten by one of the other meter types via the Restricted Application
		literal Int32 MeterInstanceCalBase = METER_INSTANCE_CAL_BASE;
		literal Int32 MeterInstanceCalEnd = METER_INSTANCE_CAL_END;
		literal Int32 MeterUnitCalAddress = METER_UNIT_CAL_ADDRESS;

		// Location at which the Metrology Application acceses the class data.
		// Initially this contains the default (5/8" meter) class data but may be 
		// overwritten by one of the other meter types via the Restricted Application
		literal Int32 MeterClassCalBase = METER_CLASS_CAL_BASE;
		literal Int32 MeterClassCalEnd = METER_CLASS_CAL_END;
		literal Int32 MeterFamilyCalAddress = METER_FAMILY_CAL_ADDRESS;

		// Instance/class data for 3/4" meter (upper memory)
		literal Int32 MeterInstance_0_75_Base = METER_INSTANCE_OPT1_BASE;
		literal Int32 MeterInstance_0_75_End = METER_INSTANCE_OPT1_END;
		literal Int32 MeterClass_0_75_Base = METER_CLASS_OPT1_BASE;
		literal Int32 MeterClass_0_75_End = METER_CLASS_OPT1_END;

		// Instance/class data for 1" meter (upper memory)
		literal Int32 MeterInstance_1_00_Base = METER_INSTANCE_OPT2_BASE;
		literal Int32 MeterInstance_1_00_End = METER_INSTANCE_OPT2_END;
		literal Int32 MeterClass_1_00_Base = METER_CLASS_OPT2_BASE;
		literal Int32 MeterClass_1_00_End = METER_CLASS_OPT2_END;

		// Instance/class data for 1.5" meter (upper memory)
		literal Int32 MeterInstance_1_50_Base = METER_INSTANCE_OPT3_BASE;
		literal Int32 MeterInstance_1_50_End = METER_INSTANCE_OPT3_END;
		literal Int32 MeterClass_1_50_Base = METER_CLASS_OPT3_BASE;
		literal Int32 MeterClass_1_50_End = METER_CLASS_OPT3_END;

		// Instance/class data for 2" meter (upper memory)
		literal Int32 MeterInstance_2_00_Base = METER_INSTANCE_OPT4_BASE;
		literal Int32 MeterInstance_2_00_End = METER_INSTANCE_OPT4_END;
		literal Int32 MeterClass_2_00_Base = METER_CLASS_OPT4_BASE;
		literal Int32 MeterClass_2_00_End = METER_CLASS_OPT4_END;
	};

	public ref struct CrcAddress
	{
		literal Int32 Bootloader = BOOTLOADER_CRC + APPLICATION_CRC_OFFSET;
		literal Int32 Metering = METERING_CRC + APPLICATION_CRC_OFFSET;
		literal Int32 MeterClassCal = METER_CLASS_CAL_CRC + APPLICATION_CRC_OFFSET;
		literal Int32 MeterInstanceCal = METER_INSTANCE_CAL_CRC + APPLICATION_CRC_OFFSET;
		literal Int32 Restricted = RESTRICTED_CRC + APPLICATION_CRC_OFFSET;

		literal Int32 MeterInstance_0_75 = METER_INSTANCE_OPT1_CRC + APPLICATION_CRC_OFFSET;
		literal Int32 MeterClass_0_75 = METER_CLASS_OPT1_CRC + APPLICATION_CRC_OFFSET;

		literal Int32 MeterInstance_1_00 = METER_INSTANCE_OPT2_CRC + APPLICATION_CRC_OFFSET;
		literal Int32 MeterClass_1_00 = METER_CLASS_OPT2_CRC + APPLICATION_CRC_OFFSET;

		literal Int32 MeterInstance_1_50 = METER_INSTANCE_OPT3_CRC + APPLICATION_CRC_OFFSET;
		literal Int32 MeterClass_1_50 = METER_CLASS_OPT3_CRC + APPLICATION_CRC_OFFSET;

		literal Int32 MeterInstance_2_00 = METER_INSTANCE_OPT4_CRC + APPLICATION_CRC_OFFSET;
		literal Int32 MeterClass_2_00 = METER_CLASS_OPT4_CRC + APPLICATION_CRC_OFFSET;

		literal Int32 Offset = APPLICATION_CRC_OFFSET;
	};

	public ref struct VersionAddress
	{		
		literal Int32 Bootloader = BOOTLOADER_VERSION_ADDRESS;
		literal Int32 Metering = METERING_VERSION_ADDRESS;
		literal Int32 Restricted = RESTRICTED_VERSION_ADDRESS;
	};
}
