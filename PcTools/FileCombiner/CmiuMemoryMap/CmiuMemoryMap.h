// CmiuMemoryMap.h

#pragma once

#include "MemoryMap.h"

using namespace System;

namespace CmiuMemoryMap 
{
	public ref struct Flash
	{
		literal Int32 TotalSize = UI_FLASH_SIZE;
	};

	public ref struct Bootloader
	{
		literal Int32 BaseAddress = BOOTLOADER_START_ADDRESS;
		literal Int32 Length = BOOTLOADER_SIZE;
		literal Int32 CrcAddress = BOOTLOADER_CRC;
	};
	
	public ref struct Config
	{
		literal Int32 BaseAddress = CONFIGURATION_START_ADDRESS;
		literal Int32 Length = CONFIGURATION_SIZE;
		literal Int32 CrcAddress = CONFIGURATION_CRC;
	};
	
	public ref struct  UserKeyTable
	{
		literal Int32 BaseAddress = KEY_TABLE_START_ADDRESS;
		literal Int32 Length = KEY_TABLE_SIZE;
		literal Int32 CrcAddress = KEY_TABLE_CRC;;
	};

	public ref struct  BleConfig
	{
		literal Int32 BaseAddress = BLE_CONFIG_START_ADDRESS;
		literal Int32 Length = BLE_CONFIG_SIZE;
		literal Int32 CrcAddress = BLE_CONFIG_CRC;
	};
	
	public ref struct  Application
	{
		literal Int32 BaseAddress = APPLICATION_START_ADDRESS;
		literal Int32 Length = APPLICATION_SIZE;
		literal Int32 CrcAddress = APPLICATION_CRC;
	};

	
}
