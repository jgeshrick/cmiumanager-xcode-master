﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileCombiner
{
    class IV3 : INeptuneMeter
    {
        private MemoryImage IV3MemoryImage = new MemoryImage(Constants.IV3_NUM_BYTES + 1, 0xFF);

        private string appFileLocation = "";
        private string blFileLocation = "";
        private string outputFileLocation = "";

        /// <summary>
        /// Constructor. Stores program arguments
        /// </summary>
        public IV3()
        {
            String[] arguments = Environment.GetCommandLineArgs();

            Console.WriteLine("You have selected the R900iV3 product");
            
            // Check to make sure there are enough parameters
            if (arguments.Length < 5)
            {
                throw new Exception("You have not passed enough parameters");
            }

            try
            {
                // Address range is shorter than the image size
                IV3MemoryImage.SetAddressRange(0, Constants.IV3_NUM_BYTES);

                appFileLocation = arguments[Constants.INDEX_APP_FILE];
                blFileLocation = arguments[Constants.INDEX_BL_FILE];
                outputFileLocation = arguments[Constants.INDEX_OUTPUT];
            }
            catch (Exception Ex)
            {
                throw new Exception("Failed to read parameters: " + Ex.Message);
            }
        }

        /// <summary>
        /// Read suppiled input files and merge into single image. Add CRCs.
        /// </summary>
        public void ReadFiles()
        {
            try
            {
                // The two input files are mandatory
                IV3MemoryImage.ProcessHexFile(appFileLocation, "IV3 Application", true);
                IV3MemoryImage.ProcessHexFile(blFileLocation, "Bootloader Application", true);

                if (IV3MemoryImage.RangeInUse(Constants.IV3_APP1_START, Constants.IV3_APP1_END))
                {
                    // App1 range in use
                    IV3MemoryImage.Crc32(Constants.IV3_APP1_START, Constants.IV3_APP_NUM_BYTES - Constants.CRC_LENGTH_32, (Constants.IV3_APP1_END - Constants.CRC_LENGTH_32) + 1);
                }
                else
                {
                    // Must be App2
                    IV3MemoryImage.Crc32(Constants.IV3_APP2_START, Constants.IV3_APP_NUM_BYTES - Constants.CRC_LENGTH_32, (Constants.IV3_APP2_END - Constants.CRC_LENGTH_32) + 1);
                }

                IV3MemoryImage.Crc32(Constants.IV3_CONFIG_START, Constants.IV3_CONFIG_NUM_BYTES - Constants.CRC_LENGTH_32, (Constants.IV3_CONFIG_END - Constants.CRC_LENGTH_32) + 1);
                
                // Next CRC is over multiple regions
                IV3MemoryImage.Crc32(Constants.IV3_BL_SEC1_START, Constants.IV3_BL_SEC1_NUM_BYTES, (Constants.IV3_BL_SEC2_END - Constants.CRC_LENGTH_32) + 1, true);
                IV3MemoryImage.Crc32AddRegion(Constants.IV3_BL_SEC2_START, Constants.IV3_BL_SEC2_NUM_BYTES - Constants.CRC_LENGTH_32, true);
            }

            catch (Exception Ex)
            {
                throw new Exception("Failed to combine files: " + Ex.Message);
            }
        }

        /// <summary>
        /// Create o/p file.
        /// </summary>
        public void WriteFiles()
        {
            IV3MemoryImage.WriteHexFile(outputFileLocation, 0, IV3MemoryImage.MemoryImageLength);
        }

        /// <summary>
        /// Help specific to IV3
        /// </summary>
        public static void DisplayProgramHelp()
        {
            // Display information
            Console.WriteLine("The paramters to use this program for the IV3 meter are:");
            Console.WriteLine("   [0]: Product (IV3)");
            Console.WriteLine("   [1]: Path to metering application file for this executable");
            Console.WriteLine("   [2]: Path to bootloader file for this executable");
            Console.WriteLine("   [3]: Path and name of output file");
            Console.WriteLine();
        }
    }
}
