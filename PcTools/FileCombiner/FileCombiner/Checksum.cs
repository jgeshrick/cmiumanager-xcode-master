﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileCombiner
{
    class Checksum
    {
        public static byte CalculateChecksum(IntelHex line)
        {
            int CalculatedChecksum = 0;

            // Byte count
            CalculatedChecksum += line.ByteCount;
            // Address
            byte[] addressbytes = BitConverter.GetBytes(line.Address);
            CalculatedChecksum += addressbytes[0];
            CalculatedChecksum += addressbytes[1];
            // Record Type
            CalculatedChecksum += line.RecordType;
            // Data            
            foreach (byte data_byte in line.Data)
            {
                CalculatedChecksum += data_byte;
            }

            CalculatedChecksum &= 0xFF;

            CalculatedChecksum = (0x100 - CalculatedChecksum);

            return (byte)CalculatedChecksum;
        }

        //public static bool VerifyChecksum(List<IntelHex> file, byte checksum)
        //{
        //    foreach (IntelHex line in file)
        //    {
        //        byte calculated_checksum = CalculateChecksum(line);
        //        if (calculated_checksum != checksum)
        //        {
        //            return false;
        //        }
        //    }
            
        //    return true;
        //}
    }
}
