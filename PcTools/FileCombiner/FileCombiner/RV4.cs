﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FileCombiner
{
    class RV4 : INeptuneMeter
    {
        private MemoryImage RV4MemoryImage = new MemoryImage(Constants.RV4_NUM_BYTES + 1, 0xFF);

        private string appFileLocation = "";
        private string blFileLocation = "";
        private string keyFileLocation = "";
        private string outputFileLocation = "";

        /// <summary>
        /// Constructor. Stores program arguments
        /// </summary>
        public RV4()
        {
            String[] arguments = Environment.GetCommandLineArgs();

            Console.WriteLine("You have selected the R900v4 product");
            
            // Check to make sure there are enough parameters
            if (arguments.Length < 6)
            {
                throw new Exception("You have not passed enough parameters");
            }

            try
            {
                // Address range is shorter than the image size
                RV4MemoryImage.SetAddressRange(0, Constants.RV4_NUM_BYTES);

                appFileLocation = arguments[Constants.INDEX_APP_FILE];
                blFileLocation = arguments[Constants.INDEX_BL_FILE];
                keyFileLocation = arguments[Constants.INDEX_KEYTABLE];
                outputFileLocation = arguments[Constants.INDEX_OUTPUT];
            }
            catch (Exception Ex)
            {
                throw new Exception("Failed to read parameters: " + Ex.Message);
            }
        }
                
        /// <summary>
        /// Read suppiled input files and merge into single image. Add CRCs.
        /// </summary>
        public void ReadFiles()
        {
            try
            {
                // The three input files are mandatory
                RV4MemoryImage.ProcessHexFile(appFileLocation, "RV4 Application", true);
                RV4MemoryImage.ProcessHexFile(blFileLocation, "Bootloader Application", true);
                // Key file is allowed to overwrite used memory locations
                RV4MemoryImage.ProcessHexFile(keyFileLocation, "RV4 Key File", true, true);

                RV4MemoryImage.Crc32(Constants.RV4_APP_START, Constants.RV4_APP_NUM_BYTES - Constants.CRC_LENGTH_32, (Constants.RV4_APP_END - Constants.CRC_LENGTH_32) + 1);
                RV4MemoryImage.Crc32(Constants.RV4_CONFIG_START, Constants.RV4_CONFIG_NUM_BYTES - Constants.CRC_LENGTH_32, (Constants.RV4_CONFIG_END - Constants.CRC_LENGTH_32) + 1);
                RV4MemoryImage.Crc32(Constants.RV4_KEYTABLE_START, Constants.RV4_KEYTABLE_NUM_BYTES - Constants.CRC_LENGTH_32, (Constants.RV4_KEYTABLE_END - Constants.CRC_LENGTH_32) + 1);

                // Next CRC is over multiple regions
                RV4MemoryImage.Crc32(Constants.RV4_BL_SEC1_START, Constants.RV4_BL_SEC1_NUM_BYTES, (Constants.RV4_BL_SEC2_END - Constants.CRC_LENGTH_32) + 1, true);
                RV4MemoryImage.Crc32AddRegion(Constants.RV4_BL_SEC2_START, Constants.RV4_BL_SEC2_NUM_BYTES - Constants.CRC_LENGTH_32, true);

            }

            catch (Exception Ex)
            {
                throw new Exception("Failed to combine files: " + Ex.Message);
            }
        }

        /// <summary>
        /// Create o/p file.
        /// </summary>
        public void WriteFiles()
        {
            RV4MemoryImage.WriteHexFile(outputFileLocation, 0, RV4MemoryImage.MemoryImageLength);
        }

        /// <summary>
        /// Help specific to RV4
        /// </summary>
        public static void DisplayProgramHelp()
        {
            // Display information
            Console.WriteLine("The paramters to use this program for the RV4 meter are:");
            Console.WriteLine("   [0]: Product (RV4)");
            Console.WriteLine("   [1]: Path to metering application file for this executable");
            Console.WriteLine("   [2]: Path to bootloader file for this executable");
            Console.WriteLine("   [3]: Path and name of output file");
            Console.WriteLine("   [4]: Path to key table file for this executable");
            Console.WriteLine();
        }
    }
}
