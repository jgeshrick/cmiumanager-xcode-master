﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileCombiner
{
    class CombinedFile
    {

        public List<string> File = new List<string>();

        public CombinedFile(Constants.Products product, string appfile, string blfile)
        {
            File.Clear();

            if (product == Constants.Products.EV2)
            {
                EV2 ev2File = new EV2(appfile, blfile);
                File.AddRange(ev2File.CombinedFile);
            }

            else if (product == Constants.Products.IV3)
            {
                IV3 iv3File = new IV3(appfile, blfile);
                File.AddRange(iv3File.CombinedFile);

            }
            else
            {
                Error.Error_Found = true;
                Error.Error_Message = "You have selected an invalid product.";
            }
 
        }

        public CombinedFile(Constants.Products product, string meteringAppPath, string restrictedAppFile, string bootloaderFile, string instanceCalPath, string classCalPath)
        {
            File.Clear();

            if (product == Constants.Products.USMeter)
            {
                USMeter usMeterFile = new USMeter(meteringAppPath, restrictedAppFile, bootloaderFile, instanceCalPath, classCalPath);
                File.AddRange(usMeterFile.CombinedFile);
            }
            else
            {
                Error.Error_Found = true;
                Error.Error_Message = "You have selected an invalid product.";
            }

        }

    }
}
