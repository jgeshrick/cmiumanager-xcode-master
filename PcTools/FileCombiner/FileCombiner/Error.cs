﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileCombiner
{
    class Error
    {
        public static bool Error_Found = false;
        public static string Error_Message;

        public static void ClearError()
        {
            Error_Found = false;
            Error_Message = "";
        }
    }
}
