﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace FileCombiner
{
    class FileIO
    {

        public static List<string> ReadFile(string filePath)
        {
            List<string> file = new List<string>();

            try
            {
                file.AddRange(File.ReadAllLines(filePath));
            }
            catch (Exception ex)
            {
                Error.Error_Found = true;
                Error.Error_Message = "Error when opening file. Details: " + ex.Message;
            }

            return file;
        }

        public static void WriteFile(List<string> file, string filepath)
        {
            try
            {
                if (File.Exists(filepath))
                    File.Delete(filepath);
                
                File.WriteAllLines(filepath, file.ToArray());
            }
            catch (Exception ex)
            {
                Error.Error_Found = true;
                Error.Error_Message = "Error when writing file. Details: " + ex.Message;
            }
        }
    }
}
