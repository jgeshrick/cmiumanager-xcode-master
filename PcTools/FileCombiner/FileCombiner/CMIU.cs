﻿using System;
using System.Collections.Generic;
using System.IO;
using CmiuMap = CmiuMemoryMap;

namespace FileCombiner
{
    class CMIU : INeptuneMeter
    {
        private string uiAppPath;
        private string bootloaderPath;
        private string outputFileNameFull;
        private string keyFileLocation;

        private string outputFileNameBase;

        private bool gotBootloader;
        private bool gotApplication;
        private bool gotOutputName;
        private bool gotKeyTable;

        private MemoryImage CmiuMemoryImage = new MemoryImage(CmiuMap.Flash.TotalSize, 0xFF);

        /// <summary>
        /// Constructor - generates combined file from the given inputs
        /// </summary>
        public CMIU()
        {
            String[] arguments = Environment.GetCommandLineArgs();

            Console.WriteLine("You have selected the CMIU product");

            // Check to make sure there are enough parameters
            if (arguments.Length < 6)
            {
                throw new Exception("You have not passed enough parameters");
            }

            try
            {
                /** parse the argument list */
                uiAppPath = arguments[Constants.INDEX_APP_FILE];
                bootloaderPath = arguments[Constants.INDEX_BL_FILE];
                outputFileNameFull = arguments[Constants.INDEX_OUTPUT];
                keyFileLocation = arguments[Constants.INDEX_KEYTABLE];

                /** What images do we actually have to work with? */
                gotBootloader = (bootloaderPath != "");
                gotApplication = (uiAppPath != "");
                gotOutputName = (outputFileNameFull != "");
                gotKeyTable = (keyFileLocation != "");
            }
            catch (Exception Ex)
            {
                throw new Exception("Failed to read parameters: " + Ex.Message);
            }
        }

        /// <summary>
        /// Read suppiled input files and merge into single image. Add CRCs.
        /// </summary>
        public void ReadFiles()
        {
            try
            {
                // Read in app and bootloader hex files
                CmiuMemoryImage.ProcessHexFile(bootloaderPath, "Bootloader", false);
                // The Application is not allowed to overwrite anything used by the bootloader. 
                CmiuMemoryImage.ProcessHexFile(uiAppPath, "User Interface Application", false, false);
                // Key file is allowed to overwrite used memory locations
                CmiuMemoryImage.ProcessHexFile(keyFileLocation, "CMIU Key Table", false, true);

                // Add CRCs

                // Firstly, the booloader (2 parts)
                CmiuMemoryImage.Crc32(CmiuMap.Bootloader.BaseAddress, CmiuMap.Bootloader.Length - Constants.CRC_LENGTH_32, CmiuMap.Bootloader.CrcAddress);

                // Next, the Config memory region
                CmiuMemoryImage.Crc32(CmiuMap.Config.BaseAddress, CmiuMap.Config.Length - Constants.CRC_LENGTH_32, CmiuMap.Config.CrcAddress);

                // Next the Key Table
                CmiuMemoryImage.Crc32(CmiuMap.UserKeyTable.BaseAddress, CmiuMap.UserKeyTable.Length - Constants.CRC_LENGTH_32, CmiuMap.UserKeyTable.CrcAddress);

                // Next the BLE Configuration Memory Location
                CmiuMemoryImage.Crc32(CmiuMap.BleConfig.BaseAddress, CmiuMap.BleConfig.Length - Constants.CRC_LENGTH_32, CmiuMap.BleConfig.CrcAddress);

                // Finally, the UI application
                CmiuMemoryImage.Crc32(CmiuMap.Application.BaseAddress, CmiuMap.Application.Length - Constants.CRC_LENGTH_32, CmiuMap.Application.CrcAddress);

           }
            catch (Exception Ex)
            {
                throw new Exception("Failed to combine files: " + Ex.Message);
            }
        }


        /// <summary>
        /// Write out files to disk
        /// </summary>
        public void WriteFiles()
        {
            try
            {
                /** If an output name wasn't passed, let's decide on one. */
                if ((!gotOutputName))
                {
                    if (gotApplication)
                    {
                        outputFileNameFull = uiAppPath;
                    }
                    else if (gotBootloader)
                    {
                        outputFileNameFull = bootloaderPath;
                    }
                    else if (gotKeyTable)
                    {
                        outputFileNameFull = keyFileLocation;
                    }
                    else
                    {
                        outputFileNameFull = "./EmptyFile.hex";
                    }

                }

                /** We'll use this later. No need to run the same call multiple times. */
                outputFileNameBase = Path.GetDirectoryName(outputFileNameFull) + "/";// +Path.GetFileNameWithoutExtension(outputFileNameFull);

                /** Output the full combined file. */
                CmiuMemoryImage.WriteHexFile(outputFileNameBase + Path.GetFileNameWithoutExtension(outputFileNameFull) + ".hex", 0, CmiuMap.Flash.TotalSize);
                CmiuMemoryImage.WriteBinaryFile(outputFileNameBase + Path.GetFileNameWithoutExtension(outputFileNameFull) + ".bin", 0, CmiuMap.Flash.TotalSize);

                /** Output the bootloader image. */
                CmiuMemoryImage.WriteHexFile(outputFileNameBase + "CmiuBootloader.hex", CmiuMap.Bootloader.BaseAddress, CmiuMap.Bootloader.Length);
                CmiuMemoryImage.WriteBinaryFile(outputFileNameBase + "CmiuBootloader.bin", CmiuMap.Bootloader.BaseAddress, CmiuMap.Bootloader.Length);

                /** Output the application image. */
                CmiuMemoryImage.WriteHexFile(outputFileNameBase+"CmiuApplication.hex", CmiuMap.Application.BaseAddress, CmiuMap.Application.Length);
                CmiuMemoryImage.WriteBinaryFile(outputFileNameBase + "CmiuApplication.bin", CmiuMap.Application.BaseAddress, CmiuMap.Application.Length);

                /** Output the configuration image. */
                CmiuMemoryImage.WriteHexFile(outputFileNameBase+"CmiuConfiguration.hex", CmiuMap.Config.BaseAddress, CmiuMap.Config.Length);
                CmiuMemoryImage.WriteBinaryFile(outputFileNameBase + "CmiuConfiguration.bin", CmiuMap.Config.BaseAddress, CmiuMap.Config.Length);

                /** Output the key table image. */
                CmiuMemoryImage.WriteHexFile(outputFileNameBase+"CmiuEncryption.hex", CmiuMap.UserKeyTable.BaseAddress, CmiuMap.UserKeyTable.Length);
                CmiuMemoryImage.WriteBinaryFile(outputFileNameBase + "CmiuEncryption.bin", CmiuMap.UserKeyTable.BaseAddress, CmiuMap.UserKeyTable.Length);

                /** Output the ble configuration image. */
                CmiuMemoryImage.WriteHexFile(outputFileNameBase+"CmiuBleConfiguration.hex", CmiuMap.BleConfig.BaseAddress, CmiuMap.BleConfig.Length);
                CmiuMemoryImage.WriteBinaryFile(outputFileNameBase + "CmiuBleConfiguration.bin", CmiuMap.BleConfig.BaseAddress, CmiuMap.BleConfig.Length);
            }
            catch (Exception Ex)
            {
                throw new Exception("Failed to write files: " + Ex.Message);
            }
        }

        /// <summary>
        /// Help specific to CMIU
        /// </summary>
        public static void DisplayProgramHelp()
        {
            // Display information
            Console.WriteLine("The paramters to use this program for CMIU are:");
            Console.WriteLine("   [0]: Product (CMIU)");
            Console.WriteLine("   [1]: Path to user interface application hex file");
            Console.WriteLine("   [2]: Path to bootloader hex file");
            Console.WriteLine("   [3]: Path to output hex file");
            Console.WriteLine("   [4]: Path to key table hex file");
            Console.WriteLine();
        }
    }
}
