﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using IntelHexFileHelper;
using CheckSum = IntelHexFileHelper.Checksum;
using IntelHexConstants = IntelHexFileHelper.Constants;

namespace FileCombiner
{
    class MemoryImage
    {
        private Int32 ImageLength;
        private Int32 StartAddress = 0;
        private Int32 EndAddress = 0;

        // The memory image and indicator of used memory
        private List<byte> TargetMemory = new List<byte>();
        private List<bool> OverlapTracking = new List<bool>();

        private List<byte> Crc16MultiRegion = null;
        private Int32 Crc16Location;
    
        private List<byte> Crc32MultiRegion = null;
        private Int32 Crc32Location;

        public MemoryImage(Int32 imageLength, byte nullDataByte)
        {
            if (imageLength < 1)
            {
                throw new Exception("Memory image length must be >= 1");
            }

            ImageLength = imageLength;

            // Default end address is based on 0 start address and imageLength
            EndAddress = imageLength - 1;


            // Initialise memory tracking
            TargetMemory.Capacity = imageLength;
            OverlapTracking.Capacity = imageLength;
            for (var byteCount = 0; byteCount < imageLength; byteCount++)
            {
                TargetMemory.Add(nullDataByte);
                OverlapTracking.Add(false);
            }
        }

        ~MemoryImage()
        {
            TargetMemory = null;
            OverlapTracking = null;
            Crc16MultiRegion = null;
            Crc32MultiRegion = null;
        }

        /// <summary>
        /// Return length in memory
        /// </summary>
        public Int32 MemoryImageLength
        {
            get { return ImageLength; }
        }

        /// <summary>
        /// Sets the valid address range (default 0 .. Image Length - 1) 
        /// </summary>
        /// <param name="startAddress">Address on target (not index into TargetMemory)</param>
        /// <param name="endAddress">Address on target (not index into TargetMemory)</param>
        public void SetAddressRange(Int32 startAddress, Int32 endAddress)
        {
            if (startAddress >= endAddress)
            {
                throw new Exception("Start address must be < end address");
            }
            else if (endAddress >= (startAddress + ImageLength))
            {
                throw new Exception("End address must be < startAddress + ImageLength");
            }
            else
            {
                StartAddress = startAddress;
                EndAddress = endAddress;
            }
        }

        /// <summary>
        /// Checks whether any data in the range is marked as in-use
        /// </summary>
        /// <param name="startAddress">Address on target (not index into TargetMemory)</param>
        /// <param name="endAddress">Address on target (not index into TargetMemory)</param>
        /// <returns></returns>
        public bool RangeInUse(Int32 startAddress, Int32 endAddress)
        {
            bool rangeInUse = false;

            // Bounds checking
            if (startAddress < StartAddress)
            {
                startAddress = StartAddress;
            }

            if (endAddress > EndAddress)
            {
                endAddress = EndAddress;
            }

            for (Int32 address = startAddress; (address <= endAddress) & !rangeInUse; address++)
            {
                if (OverlapTracking[address - StartAddress])
                {
                    rangeInUse = true;
                }
            }

            return rangeInUse;
        }


        /// <summary>
        /// Reads an Intel Hex file and parses into local representation of target device memory
        /// </summary>
        /// <param name="path">Path and name of file (may be null/empty)</param>
        /// <param name="name">File name for use in error messages</param>
        /// <param name="fileRequired">If true then a file must exist</param>
        /// <param name="allowOverwrite">If true then a file may overwrite memory written by a previous file</param>
        public void ProcessHexFile(string path, string name, bool fileRequired = false, bool allowOverwrite = false)
        {
            if (string.IsNullOrEmpty(path))
            {
                if (fileRequired)
                {
                    throw new Exception("Required file " + name + " specified with empty parameter");
                }

                return;
            }

            // Read in hex file and remove an end of file entry if present
            var stringFile = new List<string>();
            stringFile.AddRange(FileIO.ReadFile(path));
            if (Error.Error_Found)
            {
                throw new Exception("Failed to read " + name + " hex file: " + Error.Error_Message);
            }

            // parse hex file to IntelHex format
            var intelHexFile = new List<IntelHex>();
            intelHexFile.AddRange(IntelHex.ParseToIntelHex(stringFile));
            if (Error.Error_Found)
            {
                throw new Exception("Failed to parse " + name + " hex file: " + Error.Error_Message);
            }

            // clear table to free memory
            stringFile = null;

            // Verify line checksums and sanity-check addressing of hex file
            if (!VerifyFile(intelHexFile))
            {
                throw new Exception(name + " hex file supplied contained an invalid checksum. File cannot be trusted.");
            }

            // Load data into target memory.
            LoadTargetMemory(intelHexFile, allowOverwrite);

            if (Error.Error_Found)
            {
                throw new Exception("Failed to populate " + name + " binary file: " + Error.Error_Message);
            }
        }

        /// <summary>
        /// Verifies the checksum of each of a list of IntelHex objects.
        /// Verifies that data records have addresses that are in range based on the meter
        /// type in use.
        /// NB. The order of the IntelHex objects is important as addresses are calculated based on the most recent
        /// Extended Segment Address or Extended Linear Address record.
        /// </summary>
        /// <param name="file">list of IntelHex objects where each represents a line from an Intel Hex format file</param>
        /// <returns>bool true if checksums are valid, false if not</returns>
        private bool VerifyFile(List<IntelHex> file)
        {
            Int32 baseAddress = 0;
            bool recordsValid = true;

            foreach (IntelHex line in file)
            {
                // Verify the checksum of each line
                if (Checksum.CalculateChecksum(line) != line.LineChecksum)
                {
                    recordsValid = false;
                }
                else
                {
                    // Verify record types are correct
                    switch (line.RecordType)
                    {
                        case IntelHexConstants.RECORD_TYPE_DATA:
                            {
                                // Get the address of the first and last data byte in the line
                                Int32 lineStartAddress = baseAddress + line.Address;
                                Int32 lineEndAddress = lineStartAddress + line.ByteCount - 1;

                                if ((lineStartAddress < StartAddress) ||  (lineEndAddress > EndAddress))
                                {
                                    recordsValid = false;
                                }
                            }
                            break;

                        case IntelHexConstants.RECORD_TYPE_EXTENDED_SEGMENT_ADDRESS:
                        case IntelHexConstants.RECORD_TYPE_EXTENDED_LINEAR_ADDRESS:
                            baseAddress = GetBaseAddress(line);
                            if (baseAddress < 0)
                            {
                                recordsValid = false;
                            }
                            break;

                        case IntelHexConstants.RECORD_TYPE_EOF: //End of File
                        case IntelHexConstants.RECORD_TYPE_START_SEGMENT_ADDRESS:
                        case IntelHexConstants.RECORD_TYPE_START_LINEAR_ADDRESS:
                            // Ignore these record types
                            break;

                        default:
                            recordsValid = false;
                            break;
                    }
                }

                if (!recordsValid)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Takes an IntelHex object representing an Extended Segment Address or
        /// Extended Linear Address and extracts the embedded memory address.
        /// </summary>
        /// <param name="line">IntelHex object</param>
        /// <returns>Base memory address to be used for records that follow (or -1 if error)</returns>
        private static Int32 GetBaseAddress(IntelHex line)
        {
            Int32 baseAddress = -1; // Indicates error

            if (line.ByteCount == 2)
            {
                byte[] dataCopy = line.Data.ToArray();
                Array.Reverse(dataCopy);
                UInt16 baseVal = BitConverter.ToUInt16(dataCopy, 0);

                if (line.RecordType == IntelHexConstants.RECORD_TYPE_EXTENDED_SEGMENT_ADDRESS)
                {
                    baseAddress = (Int32)baseVal * 16;
                }
                else if (line.RecordType == IntelHexConstants.RECORD_TYPE_EXTENDED_LINEAR_ADDRESS)
                {
                    if (baseVal < 0x8000u)
                    {
                        baseAddress = (Int32)baseVal << 16;
                    }
                }
            }

            return baseAddress;
        }
        /// <summary>
        /// This function takes a list of bytes passed by reference (representing the memory on the target device)
        /// and adds the data from the file passed.
        /// It then checks to make sure the address has not been written to before, to make sure there are no
        /// overwrites
        /// </summary>
        /// <param name="targetMemory">list of data to add to</param>
        /// <param name="overlapList">overlap list to check against</param>
        /// <param name="file">data to add</param>
        private void LoadTargetMemory(List<IntelHex> file, bool allowOverwrite)
        {
            Int32 baseAddress = 0;

            foreach (IntelHex line in file)
            {
                if ((line.RecordType == IntelHexConstants.RECORD_TYPE_EXTENDED_SEGMENT_ADDRESS) ||
                    (line.RecordType == IntelHexConstants.RECORD_TYPE_EXTENDED_LINEAR_ADDRESS))
                {
                    // Note that we've already verified that the addresses are in range
                    baseAddress = GetBaseAddress(line);
                }
                else if (line.RecordType == IntelHexConstants.RECORD_TYPE_DATA)
                {
                    Int32 address = baseAddress + line.Address;
                    foreach (byte data_byte in line.Data)
                    {
                        if (!allowOverwrite)
                        {
                            if (OverlapTracking[address - StartAddress])
                            {
                                Error.Error_Found = true;
                                Error.Error_Message = "Overlap of data detected. Attempted to combine data at the same address.";
                                return;
                            }
                        }

                        TargetMemory[address - StartAddress] = data_byte;
                        OverlapTracking[address - StartAddress] = true;
                        address++;
                    }
                }
                else
                {
                    // Other record types? Ignore.
                }
            }
        }

        /// <summary>
        /// Perform CRC calculaion
        /// </summary>
        /// <param name="startAddress">Address on target (not index into TargetMemory)</param>
        /// <param name="length"></param>
        /// <param name="crcLocation"></param>
        /// <param name="multiRegion">True if this is the first part og a multi-region CRC calculation</param>
        public void CrcCcitt16(Int32 startAddress, Int32 length, Int32 crcLocation, bool multiRegion = false)
        {
            CheckCrcAddress(startAddress, length, crcLocation);

            if (multiRegion)
            {
                Crc16MultiRegion = new List<byte>();
                Crc16MultiRegion.AddRange(TargetMemory.GetRange(startAddress - StartAddress, length));
                Crc16Location = crcLocation;
            }
            else
            {
                UInt16 crc = CRC.CalculateCrcCcitt16(TargetMemory.GetRange(startAddress - StartAddress, length));
                CRC.InsertCrcCcitt16(ref TargetMemory, crcLocation - StartAddress, crc);
            }
        }

        /// <summary>
        /// Adds a region to a multi-region CRC calculation
        /// </summary>
        /// <param name="startAddress">Address on target (not index into TargetMemory)</param>
        /// <param name="length"></param>
        /// <param name="completed">True iff this is the last region to be added - causes CRC to be inserted</param>
        public void CrcCcitt16AddRegion(Int32 startAddress, Int32 length, bool completed = false)
        {
            CheckCrcAddress(startAddress, length, Crc16Location);
            Crc16MultiRegion.AddRange(TargetMemory.GetRange(startAddress - StartAddress, length));
            if (completed)
            {
                UInt16 crc = CRC.CalculateCrcCcitt16(Crc16MultiRegion);
                CRC.InsertCrcCcitt16(ref TargetMemory, Crc16Location - StartAddress, crc); 
                
                Crc16MultiRegion = null;
            }
        }

        /// <summary>
        /// Perform CRC calculaion
        /// </summary>
        /// <param name="startAddress">Address on target (not index into TargetMemory)</param>
        /// <param name="length"></param>
        /// <param name="crcLocation"></param>
        /// <param name="multiRegion">True if this is the first part og a multi-region CRC calculation</param>
        public void Crc32(Int32 startAddress, Int32 length, Int32 crcLocation, bool multiRegion = false)
        {
            CheckCrcAddress(startAddress, length, crcLocation);
            if (multiRegion)
            {
                Crc32MultiRegion = new List<byte>();
                Crc32MultiRegion.AddRange(TargetMemory.GetRange(startAddress - StartAddress, length));
                Crc32Location = crcLocation;
            }
            else
            {
                UInt32 crc = CRC.CalculateCRC32(TargetMemory.GetRange(startAddress - StartAddress, length));
                CRC.InsertCRC32(ref TargetMemory, crcLocation - StartAddress, crc);
            }
        }

        /// <summary>
        /// Adds a region to a multi-region CRC calculation
        /// </summary>
        /// <param name="startAddress">Address on target (not index into TargetMemory)</param>
        /// <param name="length"></param>
        /// <param name="completed">True iff this is the last region to be added - causes CRC to be inserted</param>
        public void Crc32AddRegion(Int32 startAddress, Int32 length, bool completed = false)
        {
            CheckCrcAddress(startAddress, length, Crc32Location);

            Crc32MultiRegion.AddRange(TargetMemory.GetRange(startAddress - StartAddress, length));
            if (completed)
            {
                UInt32 crc = CRC.CalculateCRC32(Crc32MultiRegion);
                CRC.InsertCRC32(ref TargetMemory, Crc32Location - StartAddress, crc); 
                
                Crc32MultiRegion = null;
            }
        }

        /// <summary>
        /// Validates address given for CRC
        /// </summary>
        /// <param name="startAddress">Address on target (not index into TargetMemory)</param>
        /// <param name="length"></param>
        /// <param name="crcLocation"></param>
        private void CheckCrcAddress(Int32 startAddress, Int32 length, Int32 crcLocation)
        {
            // Validate i/p parameters
            if (startAddress < StartAddress)
            {
                throw new Exception("Start address for CRC calculation outside memory address range.");
            }

            if ((startAddress + length) > EndAddress)
            {
                throw new Exception("End address for CRC calculation outside memory address range.");
            }

            if ((crcLocation < StartAddress) || (crcLocation > (EndAddress - 1)))
            {
                throw new Exception("CRC location outside memory address range.");
            }

            if ((crcLocation >= startAddress) && (crcLocation <= (startAddress + length - 1)))
            {
                throw new Exception("CRC location inside range being CRCd.");
            }
        }

        /// <summary>
        /// Write out a portion of the combined data list as a stand-alone, addressless binary file
        /// </summary>
        /// <param name="path">path to which to write the file (overwrites existing file)</param>
        /// <param name="start">Address on target (not index into TargetMemory)</param>
        /// <param name="length">length of block to write out</param>
        public void WriteBinaryFile(string path, Int32 start, int length)
        {
            FileStream binfile = File.Open(path, FileMode.OpenOrCreate, FileAccess.Write);
            foreach (var entry in TargetMemory.GetRange(start - StartAddress, length))
            {
                binfile.WriteByte(entry);
            }
            binfile.Close();

            if (Error.Error_Found)
            {
                throw new Exception(Error.Error_Message);
            }
            else
            {
                Console.WriteLine("Wrote output file: " + path);
            }
        }


        /// <summary>
        /// Write out a portion of the combined data list as a stand-alone hex file, maintaining block address
        /// </summary>
        /// <param name="path">Path to which to write the file (overwrites existing file)</param>
        /// <param name="start">Address on target (not index into TargetMemory)</param>
        /// <param name="length">length of block to write out</param>
        public void WriteHexFile(string path, Int32 start, Int32 length)
        {
            // Create final combined hex file
            var combinedFile = IntelHex.CreateFile(start, length, ref TargetMemory, StartAddress);
            FileIO.WriteFile(combinedFile, path);

            if (Error.Error_Found)
            {
                throw new Exception(Error.Error_Message);
            }
            else
            {
                Console.WriteLine("Wrote output file: " + path);
            }
        }
    }
}
