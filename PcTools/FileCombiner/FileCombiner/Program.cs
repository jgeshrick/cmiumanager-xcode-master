﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Reflection;



//*****************************************************************************
// FileCombiner v2.7
// This program takes parameters:
//      Product (IV3,EV2,RV4,USMeter,CMIU)
//      App file location
//      Bootloader file location
//      [Output file name]
//      [Key table file for RV4,CMIU]
//      [Configuration files for CMIU]
//      [Class and instance files for USMeter]
//      Name of output file and location
//
// This program combines the bootloader and the application and recalculates
// the crc for each image:
//      application
//      configuration
//      bootloader
//
//  Revision History:
//      2012.6.15 - Updated the memory locations after the bootloader
//                  was trimmed down to allow for more space in the app
//      2015.06.25 -Added support for the CMIU
//
//*****************************************************************************
namespace FileCombiner
{

    class Program
    {
        /// <summary>
        /// Exit codes
        /// </summary>
        private enum ExitCode
        {
            Ok = 0,
            Error = 1,
        }

        /// <summary>
        /// Store meter type string
        /// </summary>
        private static string ProductType;

        /// <summary>
        /// Main function - application entry point from command line
        /// </summary>
        /// <param name="args"></param>
        static int Main(string[] args)
        {
            /** Found here: https://www.youtube.com/watch?v=x-KK7bmo1AM forces dlls to be embedded with the exe. */
            AppDomain.CurrentDomain.AssemblyResolve += (Object sender, ResolveEventArgs argz) =>
            {
                String thisExe = Assembly.GetExecutingAssembly().GetName().Name;
                AssemblyName embeddedAssembly = new System.Reflection.AssemblyName(argz.Name);
                String resourceName = thisExe + "." + embeddedAssembly.Name + ".dll";

                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
                {
                    Byte[] assemblyData = new Byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }
            };

            /** Actually run the FileCombiner program. */
            return FileCombiner(args);
        }



        /// <summary>
        /// FileCombiner Logic - application logic accessed from main
        /// </summary>
        /// <param name="args"></param>
        static int FileCombiner(string[] args)
        {
            try
            {
                INeptuneMeter meter;

                DisplayProgramCallInfo(args);

                ProductType = args[0];
                // Check the product parameter
                switch (ProductType)
                {
                    case Constants.CMIU:
                        meter = new CMIU();
                        break;

                    case Constants.USMeter:
                        meter = new USMeter();
                        break;

                    case Constants.USMeterUI:
                        meter = new USMeterUI();
                        break;

                    case Constants.EV2:
                        meter = new EV2();
                        break;

                    case Constants.IV3:
                        meter = new IV3();
                        break;

                    case Constants.RV4:
                        meter = new RV4();
                        break;

                    default:
                        throw new Exception("You have not passed a valid product");
                }

                // At this point all parameters have been read and saved
                // Create combined files
                meter.ReadFiles();

                // Write combined files
                meter.WriteFiles();

                // No errors, done
                Console.WriteLine("Files successfully combined");
                return (int)ExitCode.Ok;
            }

            catch (Exception Ex)
            {
                Console.WriteLine("Error: \"" + Ex.Message + "\"");

                DisplayProgramHelp();

                return (int)ExitCode.Error;
            }
        }

        /// <summary>
        /// Display program arguments
        /// </summary>
        /// <param name="args"></param>
        static void DisplayProgramCallInfo(string[] args)
        {
            Console.WriteLine("FileCombiner v" + Constants.VERSION);
            Console.WriteLine("Arguments: ");
            foreach (string arg in args)
            {
                Console.WriteLine("    " + arg);
            }
            Console.WriteLine("");
        }

        /// <summary>
        /// Help for display on error
        /// </summary>
        static void DisplayProgramHelp()
        {

            // Write header for information
            switch (ProductType)
            {
                case Constants.USMeter:
                    USMeter.DisplayProgramHelp();
                    break;

                case Constants.USMeterUI:
                    USMeterUI.DisplayProgramHelp();
                    break;

                case Constants.EV2:
                    EV2.DisplayProgramHelp();
                    break;

                case Constants.IV3:
                    IV3.DisplayProgramHelp();
                    break;

                case Constants.RV4:
                    RV4.DisplayProgramHelp();
                    break;

                default:
                    break;
            }

            Console.WriteLine();
            Console.WriteLine("Exit codes:");
            foreach (var value in Enum.GetValues(typeof(ExitCode)))
            {
                Console.WriteLine("   {0,3}: {1}", (int)value, (ExitCode)value);
            }
            Console.WriteLine();
            Console.WriteLine("======================================================================");
            Console.WriteLine("");
        }
    }
}
