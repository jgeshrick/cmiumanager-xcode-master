﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FileCombiner
{
    interface INeptuneMeter
    {
        void ReadFiles();
        void WriteFiles();
    }
}
