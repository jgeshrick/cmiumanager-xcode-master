﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileCombiner
{
    class EV2 : INeptuneMeter
    {
        private MemoryImage EV2MemoryImage = new MemoryImage(Constants.EV2_NUM_BYTES + 1, 0xFF);

        private string appFileLocation = "";
        private string blFileLocation = "";
        private string outputFileLocation = "";
        
        /// <summary>
        /// Constructor. Stores program arguments
        /// </summary>
        public EV2()
        {
            String[] arguments = Environment.GetCommandLineArgs();
            
            Console.WriteLine("You have selected the EcoderV2 product"); 
            
            // Check to make sure there are enough parameters
            if (arguments.Length < 5)
            {
                throw new Exception("You have not passed enough parameters");
            }

            try
            {
                // Address range is shorter than the image size
                EV2MemoryImage.SetAddressRange(0, Constants.EV2_NUM_BYTES);

                appFileLocation = arguments[Constants.INDEX_APP_FILE];
                blFileLocation = arguments[Constants.INDEX_BL_FILE];
                outputFileLocation = arguments[Constants.INDEX_OUTPUT];
            }
            catch (Exception Ex)
            {
                throw new Exception("Failed to read parameters: " + Ex.Message);
            }
        }

        /// <summary>
        /// Read suppiled input files and merge into single image. Add CRCs.
        /// </summary>
        public void ReadFiles()
        {
            try
            {
                // The two input files are mandatory
                EV2MemoryImage.ProcessHexFile(appFileLocation, "EV2 Application", true);
                EV2MemoryImage.ProcessHexFile(blFileLocation, "Bootloader Application", true);

                EV2MemoryImage.Crc32(Constants.EV2_APP_START, Constants.EV2_APP_NUM_BYTES - Constants.CRC_LENGTH_32, (Constants.EV2_APP_END - Constants.CRC_LENGTH_32) + 1);
                EV2MemoryImage.Crc32(Constants.EV2_CONFIG_START, Constants.EV2_CONFIG_NUM_BYTES - Constants.CRC_LENGTH_32, (Constants.EV2_CONFIG_END - Constants.CRC_LENGTH_32) + 1);

                // Next CRC is over multiple regions
                EV2MemoryImage.Crc32(Constants.EV2_BL_SEC1_START, Constants.EV2_BL_SEC1_NUM_BYTES, (Constants.EV2_BL_SEC2_END - Constants.CRC_LENGTH_32) + 1, true);
                EV2MemoryImage.Crc32AddRegion(Constants.EV2_BL_SEC2_START, Constants.EV2_BL_SEC2_NUM_BYTES - Constants.CRC_LENGTH_32, true);
            }
             
            catch(Exception Ex)
            {
                throw new Exception("Failed to combine files: " + Ex.Message);
            }

        }

        /// <summary>
        /// Create o/p file.
        /// </summary>
        public void WriteFiles()
        {
            EV2MemoryImage.WriteHexFile(outputFileLocation, 0, EV2MemoryImage.MemoryImageLength);
        }

        /// <summary>
        /// Help specific to EV2
        /// </summary>
        public static void DisplayProgramHelp()
        {
            // Display information
            Console.WriteLine("The paramters to use this program for the EV2 meter are:");
            Console.WriteLine("   [0]: Product (EV2)");
            Console.WriteLine("   [1]: Path to metering application file for this executable");
            Console.WriteLine("   [2]: Path to bootloader file for this executable");
            Console.WriteLine("   [3]: Path and name of output file");
            Console.WriteLine();
        }
    }
}
