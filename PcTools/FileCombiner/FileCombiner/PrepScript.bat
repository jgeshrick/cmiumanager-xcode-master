:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::        Filename: PrepScript.bat
::          Author: Brian Arnberg
::            Date: 2015.07.17
::
::         Purpose: This script pulls necessary build files into the current
::          directory.
::
::          Notes:  This script should be run by TeamCity prior to running
::              the actual build. It should be run by a developer if he/she
::              recompiles any of the builds that are move below.
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

@SET INTEL_HEX_DLL=..\..\..\Common\IntelHexFileHelper\IntelHexFileHelper\bin\Release\IntelHexFileHelper.dll

copy %INTEL_HEX_DLL% .
