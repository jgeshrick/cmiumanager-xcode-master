﻿using System;
using System.Collections.Generic;
using System.IO;
using UMM = UltrasonicMemoryMap; 

namespace FileCombiner
{
    class USMeter : INeptuneMeter
    {        
        private string meteringAppPath = "";
        private string restrictedAppPath = "";
        private string bootloaderPath = "";
        private string instanceCalPath = "";
        private string classCalPath = "";

        private string Instance_0_75_CalPath = "";
        private string Class_0_75_CalPath = "";
        private string Instance_1_00_CalPath = "";
        private string Class_1_00_CalPath = "";
        private string Instance_1_50_CalPath = "";
        private string Class_1_50_CalPath = "";
        private string Instance_2_00_CalPath = "";
        private string Class_2_00_CalPath = "";

        private string outputFileLocation;

        // Which parameters have we been passed?
        private bool gotMeteringApp;
        private bool gotRestrictedApp;
        private bool gotBootloader;
        private bool gotClassCal;
        private bool gotInstanceCal;

        private MemoryImage USMemoryImage = new MemoryImage(UMM.FlashSize.McuTotal, 0xa5);

        /// <summary>
        /// Constructor - generates combined file from the given inputs
        /// </summary>
        public USMeter()
        {
            String[] arguments = Environment.GetCommandLineArgs();

            Console.WriteLine("You have selected the USMeter product");

            // Check to make sure there are enough parameters
            if (arguments.Length < 16)
            {
                throw new Exception("You have not passed enough parameters");
            }

            try
            {
                
                meteringAppPath = arguments[2];
                restrictedAppPath = arguments[3];
                bootloaderPath = arguments[4];
                instanceCalPath = arguments[5];
                classCalPath = arguments[6];
                Instance_0_75_CalPath = arguments[7];
                Class_0_75_CalPath = arguments[8];
                Instance_1_00_CalPath = arguments[9];
                Class_1_00_CalPath = arguments[10];
                Instance_1_50_CalPath = arguments[11];
                Class_1_50_CalPath = arguments[12];
                Instance_2_00_CalPath = arguments[13];
                Class_2_00_CalPath = arguments[14];
                outputFileLocation = arguments[15];

                gotMeteringApp = meteringAppPath != "";
                gotRestrictedApp = restrictedAppPath != "";
                gotBootloader = bootloaderPath != "";
                gotInstanceCal = instanceCalPath != "";
                gotClassCal = classCalPath != "";
            }
            catch(Exception Ex)
            {
                throw new Exception("Failed to read parameters: " + Ex.Message);
            }
        }

        /// <summary>
        /// Read suppiled input files and merge into single image. Add CRCs.
        /// </summary>
        public void ReadFiles()
        {
            try
            {
                // Read in app and bootloader hex files then the various instance and class calibration files
                USMemoryImage.ProcessHexFile(meteringAppPath, "Metering Application");
                USMemoryImage.ProcessHexFile(restrictedAppPath, "Restricted Application");
                USMemoryImage.ProcessHexFile(bootloaderPath, "Bootloader");
                USMemoryImage.ProcessHexFile(instanceCalPath, "Instance Calibration");
                USMemoryImage.ProcessHexFile(classCalPath, "Class Calibration");
                USMemoryImage.ProcessHexFile(Instance_0_75_CalPath, "Instance Calibration Option 1");
                USMemoryImage.ProcessHexFile(Class_0_75_CalPath, "Class Calibration Option 1");
                USMemoryImage.ProcessHexFile(Instance_1_00_CalPath, "Instance Calibration Option 2");
                USMemoryImage.ProcessHexFile(Class_1_00_CalPath, "Class Calibration Option 2");
                USMemoryImage.ProcessHexFile(Instance_1_50_CalPath, "Instance Calibration Option 3");
                USMemoryImage.ProcessHexFile(Class_1_50_CalPath, "Class Calibration Option 3");
                USMemoryImage.ProcessHexFile(Instance_2_00_CalPath, "Instance Calibration Option 4");
                USMemoryImage.ProcessHexFile(Class_2_00_CalPath, "Class Calibration Option 4");


                // Calculate CRC's - we need to truncate the last couple of bytes of each block (where the 16-bit CRC will be inserted) for the checking maths to work here.

                // Firstly, the 3 applications
                USMemoryImage.CrcCcitt16(UMM.MeterMap.MeteringBase, UMM.MemorySize.Metering - UMM.MemorySize.Crc, UMM.CrcAddress.Metering);
                USMemoryImage.CrcCcitt16(UMM.MeterMap.RestrictedBase, UMM.MemorySize.Restricted - UMM.MemorySize.Crc, UMM.CrcAddress.Restricted);
                USMemoryImage.CrcCcitt16(UMM.MeterMap.BootloaderBase, UMM.MemorySize.Bootloader - UMM.MemorySize.Crc, UMM.CrcAddress.Bootloader);

                //// Default instance and class data
                USMemoryImage.CrcCcitt16(UMM.MeterMap.MeterInstanceCalBase, UMM.MemorySize.MeterInstanceCal - UMM.MemorySize.Crc, UMM.CrcAddress.MeterInstanceCal);
                USMemoryImage.CrcCcitt16(UMM.MeterMap.MeterClassCalBase, UMM.MemorySize.MeterClassCal - UMM.MemorySize.Crc, UMM.CrcAddress.MeterClassCal);

                //// Finally, the 4 additional sets of instance and class data
                USMemoryImage.CrcCcitt16(UMM.MeterMap.MeterInstance_0_75_Base, UMM.MemorySize.MeterInstanceCal - UMM.MemorySize.Crc, UMM.CrcAddress.MeterInstance_0_75);
                USMemoryImage.CrcCcitt16(UMM.MeterMap.MeterClass_0_75_Base, UMM.MemorySize.MeterClassCal - UMM.MemorySize.Crc, UMM.CrcAddress.MeterClass_0_75);

                USMemoryImage.CrcCcitt16(UMM.MeterMap.MeterInstance_1_00_Base, UMM.MemorySize.MeterInstanceCal - UMM.MemorySize.Crc, UMM.CrcAddress.MeterInstance_1_00);
                USMemoryImage.CrcCcitt16(UMM.MeterMap.MeterClass_1_00_Base, UMM.MemorySize.MeterClassCal - UMM.MemorySize.Crc, UMM.CrcAddress.MeterClass_1_00);
                
                USMemoryImage.CrcCcitt16(UMM.MeterMap.MeterInstance_1_50_Base, UMM.MemorySize.MeterInstanceCal - UMM.MemorySize.Crc, UMM.CrcAddress.MeterInstance_1_50);
                USMemoryImage.CrcCcitt16(UMM.MeterMap.MeterClass_1_50_Base, UMM.MemorySize.MeterClassCal - UMM.MemorySize.Crc, UMM.CrcAddress.MeterClass_1_50);

                USMemoryImage.CrcCcitt16(UMM.MeterMap.MeterInstance_2_00_Base, UMM.MemorySize.MeterInstanceCal - UMM.MemorySize.Crc, UMM.CrcAddress.MeterInstance_2_00);
                USMemoryImage.CrcCcitt16(UMM.MeterMap.MeterClass_2_00_Base, UMM.MemorySize.MeterClassCal - UMM.MemorySize.Crc, UMM.CrcAddress.MeterClass_2_00);
                
            }
            catch(Exception Ex)
            {
                throw new Exception("Failed to combine files: " + Ex.Message);
            }
        }


        /// <summary>
        /// Write out files to disk
        /// </summary>
        /// <param name="outputFileLocation">location for the main hex file. Other files are generated with similar names to the hex files originally passed in.</param>
        public void WriteFiles()
        {
            try
            {
                // main files - only write out if we had all the components
                if (gotMeteringApp && gotRestrictedApp && gotBootloader && outputFileLocation != "")
                {
                    USMemoryImage.WriteHexFile(outputFileLocation,
                        0,
                        UMM.FlashSize.McuTotal);
                    USMemoryImage.WriteBinaryFile(Path.GetDirectoryName(outputFileLocation) + "/" + Path.GetFileNameWithoutExtension(outputFileLocation) + ".bin",
                        0,
                        UMM.FlashSize.McuTotal);
                }

                // bootloader, if we were given it AND it's the only one
                if (gotBootloader && (!gotMeteringApp) && (!gotRestrictedApp))
                {
                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(bootloaderPath) + "/" + Path.GetFileNameWithoutExtension(bootloaderPath) + ".bin",
                        UMM.MeterMap.BootloaderBase,
                        UMM.MemorySize.Bootloader);
                    USMemoryImage.WriteHexFile(
                        Path.GetDirectoryName(bootloaderPath) + "/" + Path.GetFileNameWithoutExtension(bootloaderPath) + ".hex",
                        UMM.MeterMap.BootloaderBase,
                        UMM.MemorySize.Bootloader);
                }

                // restricted applicaiton, if we were given it AND it's the only one
                if (gotRestrictedApp && (!gotBootloader) && (!gotMeteringApp))
                {
                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(restrictedAppPath) + "/" + Path.GetFileNameWithoutExtension(restrictedAppPath) + ".bin",
                        UMM.MeterMap.RestrictedBase,
                        UMM.MemorySize.Restricted);
                    USMemoryImage.WriteHexFile(
                        Path.GetDirectoryName(restrictedAppPath) + "/" + Path.GetFileNameWithoutExtension(restrictedAppPath) + ".hex",
                        UMM.MeterMap.RestrictedBase,
                        UMM.MemorySize.Restricted);
                }

                // metering application and its calibration blocks, if we were given it AND it's the only one
                if (gotMeteringApp && (!gotRestrictedApp) && (!gotBootloader))
                {
                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + ".bin",
                        UMM.MeterMap.MeteringBase,
                        UMM.MeterMap.MeterClass_2_00_End - UMM.MeterMap.MeteringBase);
                    USMemoryImage.WriteHexFile(Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + ".hex",
                        UMM.MeterMap.MeteringBase,
                        UMM.MeterMap.MeterClass_2_00_End - UMM.MeterMap.MeteringBase);

                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_WithoutCalibration.bin",
                        UMM.MeterMap.MeteringBase, 
                        UMM.MemorySize.Metering);
                    USMemoryImage.WriteHexFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_WithoutCalibration.hex",
                        UMM.MeterMap.MeteringBase,
                        UMM.MemorySize.Metering);

#if GENERATE_CLASS_AND_INSTANCE_FILES

                    // Default class and instance data only (no upper memory class and instance info)
                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_ClassCalibration.bin",
                        UMM.MeterMap.MeterClassCalBase,
                        UMM.MemorySize.MeterClassCal);
                    USMemoryImage.WriteHexFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_ClassCalibration.hex",
                        UMM.MeterMap.MeterClassCalBase,
                        UMM.MemorySize.MeterClassCal); 
                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_InstanceCalibration.bin",
                        UMM.MeterMap.MeterInstanceCalBase,
                        UMM.MemorySize.MeterInstanceCal);
                    USMemoryImage.WriteHexFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_InstanceCalibration.hex",
                        UMM.MeterMap.MeterInstanceCalBase,
                        UMM.MemorySize.MeterInstanceCal);

                    // The four other class and instance variations

                    // 0.75"
                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Class_0_75.bin",
                        UMM.MeterMap.MeterClass_0_75_Base,
                        UMM.MemorySize.MeterClassCal);
                    USMemoryImage.WriteHexFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Class_0_75.hex",
                        UMM.MeterMap.MeterClass_0_75_Base,
                        UMM.MemorySize.MeterClassCal);
                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Instance_0_75_.bin",
                        UMM.MeterMap.MeterInstance_0_75_Base,
                        UMM.MemorySize.MeterInstanceCal);
                    USMemoryImage.WriteHexFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Instance_0_75_.hex",
                        UMM.MeterMap.MeterInstance_0_75_Base,
                        UMM.MemorySize.MeterInstanceCal);

                    // 1"
                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Class_1_00.bin",
                        UMM.MeterMap.MeterClass_1_00_Base,
                        UMM.MemorySize.MeterClassCal);
                    USMemoryImage.WriteHexFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Class_1_00.hex",
                        UMM.MeterMap.MeterClass_1_00_Base,
                        UMM.MemorySize.MeterClassCal);
                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Instance_1_00_.bin",
                        UMM.MeterMap.MeterInstance_1_00_Base,
                        UMM.MemorySize.MeterInstanceCal);
                    USMemoryImage.WriteHexFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Instance_1_00_.hex",
                        UMM.MeterMap.MeterInstance_1_00_Base,
                        UMM.MemorySize.MeterInstanceCal);

                    // 1.5" 
                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Class_1_50.bin",
                        UMM.MeterMap.MeterClass_1_50_Base,
                        UMM.MemorySize.MeterClassCal);
                    USMemoryImage.WriteHexFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Class_1_50.hex",
                        UMM.MeterMap.MeterClass_1_50_Base,
                        UMM.MemorySize.MeterClassCal);
                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Instance_1_50_.bin",
                        UMM.MeterMap.MeterInstance_1_50_Base,
                        UMM.MemorySize.MeterInstanceCal);
                    USMemoryImage.WriteHexFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Instance_1_50_.hex",
                        UMM.MeterMap.MeterInstance_1_50_Base,
                        UMM.MemorySize.MeterInstanceCal);

                    // 2"
                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Class_2_00.bin",
                        UMM.MeterMap.MeterClass_2_00_Base,
                        UMM.MemorySize.MeterClassCal);
                    USMemoryImage.WriteHexFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Class_2_00.hex",
                        UMM.MeterMap.MeterClass_2_00_Base,
                        UMM.MemorySize.MeterClassCal);
                    USMemoryImage.WriteBinaryFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Instance_2_00_.bin",
                        UMM.MeterMap.MeterInstance_2_00_Base,
                        UMM.MemorySize.MeterInstanceCal);
                    USMemoryImage.WriteHexFile(
                        Path.GetDirectoryName(meteringAppPath) + "/" + Path.GetFileNameWithoutExtension(meteringAppPath) + "_Instance_2_00_.hex",
                        UMM.MeterMap.MeterInstance_2_00_Base,
                        UMM.MemorySize.MeterInstanceCal);
#endif  // GENERATE_CLASS_AND_INSTANCE_FILES
                }

            }
            catch(Exception Ex)
            {
                throw new Exception("Failed to write files: " + Ex.Message);
            }
        }

        /// <summary>
        /// Help specific to USMeter
        /// </summary>
        public static void DisplayProgramHelp()
        {
            // Display information
            Console.WriteLine("The paramters to use this program for USMeter are:");
            Console.WriteLine("   [0]: Product (USMeter)");
            Console.WriteLine("   [1]: Path to metering application file for this executable");
            Console.WriteLine("   [2]: Path to restricted application file for this executable");
            Console.WriteLine("   [3]: Path to bootloader file from this executable");
            Console.WriteLine("   [4]: Path to instance calibration file from this executable");
            Console.WriteLine("   [5]: Path to class calibration file from this executable");
            Console.WriteLine("   [6]: Path to option 1 instance calibration");
            Console.WriteLine("   [7]: Path to option 1 class calibration file");
            Console.WriteLine("   [8]: Path to option 2 instance calibration");
            Console.WriteLine("   [9]: Path to option 2 class calibration file");
            Console.WriteLine("   [10]: Path to option 3 instance calibration");
            Console.WriteLine("   [11]: Path to option 3 class calibration file");
            Console.WriteLine("   [12]: Path to option 4 instance calibration");
            Console.WriteLine("   [13]: Path to option 4 class calibration file");
            Console.WriteLine("   [14]: Path to output hex file from this executable");
            Console.WriteLine();
            Console.WriteLine("Note that input hex files will be overwritten to add CRCs.");
            Console.WriteLine("Binaries will be generated alongside their equivalent hex files.");
            Console.WriteLine();
        }
    }
}
