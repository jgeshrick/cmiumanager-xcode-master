﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileCombiner
{
    class Constants
    {
        // Product enumeration
        public enum Products
        {
            IV3,
            EV2,
            RV4,
            USMeter,
            CMIU,
            error
        }

        // Products
        public const string EV2 = "EV2";
        public const string IV3 = "IV3";
        public const string RV4 = "RV4";
        public const string USMeter = "USMeter";        // Metrology + Restricted App. + Met. Bootloader
        public const string USMeterUI = "USMeterUI";    // User Interface App. + UI Bootloader
        public const string CMIU = "CMIU";
        
        // Program info
        public const string VERSION = "2.6";

        // Indexes of arguments
        public const int INDEX_PRODUCT = 1;
        public const int INDEX_APP_FILE = 2;
        public const int INDEX_BL_FILE = 3;
        public const int INDEX_OUTPUT = 4;          // Not applicable to USMeter
        public const int INDEX_KEYTABLE = 5;        // Only applicable to RV4, USMeterUI, CMIU

        // RV4, EV2 and IV3 CRC is 4 bytes
        public const int CRC_LENGTH_32 = 4;

        // 16-bit CRC is 2 bytes (USMeter)
        public const int CRC_LENGTH_16 = 2;

        // ECoder V2 Locations
        public const int EV2_NUM_BYTES = 0x7FFE;
        public const int EV2_APP_START = 0x1000;
        public const int EV2_APP_END = 0x77FF;
        public const int EV2_APP_NUM_BYTES = 26624;
        public const int EV2_CONFIG_START = 0x0800;
        public const int EV2_CONFIG_END = 0x0BFF;
        public const int EV2_CONFIG_NUM_BYTES = 1024;
        public const int EV2_BL_SEC1_START = 0x00BB;
        public const int EV2_BL_SEC1_END = 0x07FF;
        public const int EV2_BL_SEC1_NUM_BYTES = 1861;
        public const int EV2_BL_SEC2_START = 0x7C00;
        public const int EV2_BL_SEC2_END = 0x7FFE;
        public const int EV2_BL_SEC2_NUM_BYTES = 1023;

        // IV3 Locations
        public const int IV3_NUM_BYTES = 0xFBFE;
        public const int IV3_APP_NUM_BYTES = 24576;
        public const int IV3_APP1_START = 0x1000;
        public const int IV3_APP1_END = 0x6FFF;
        public const int IV3_APP2_START = 0x7000;
        public const int IV3_APP2_END = 0xCFFF;
        public const int IV3_CONFIG_START = 0x0C00;
        public const int IV3_CONFIG_END = 0x0FFF;
        public const int IV3_CONFIG_NUM_BYTES = 1024;
        public const int IV3_BL_SEC1_START = 0x00A0;
        public const int IV3_BL_SEC1_END = 0x0BFF;
        public const int IV3_BL_SEC1_NUM_BYTES = 2912;
        public const int IV3_BL_SEC2_START = 0xF800;
        public const int IV3_BL_SEC2_END = 0xFBFE;
        public const int IV3_BL_SEC2_NUM_BYTES = 1023;

        // R900v4 Locations
        public const int RV4_NUM_BYTES = 0xFFFE;
        public const int RV4_APP_START = 0x1800;
        public const int RV4_APP_END = 0xD3FF;
        public const int RV4_APP_NUM_BYTES = 48128;

        public const int RV4_CONFIG_START = 0x1400;
        public const int RV4_CONFIG_END = 0x17FF;
        public const int RV4_CONFIG_NUM_BYTES = 1024;

        public const int RV4_KEYTABLE_START = 0x1000;
        public const int RV4_KEYTABLE_END = 0x13FF;
        public const int RV4_KEYTABLE_NUM_BYTES = 1024;

        public const int RV4_BL_SEC1_START = 0x00BB;
        public const int RV4_BL_SEC1_END = 0x0FFF;
        public const int RV4_BL_SEC1_NUM_BYTES = 3909;
        public const int RV4_BL_SEC2_START = 0xFC00;
        public const int RV4_BL_SEC2_END = 0xFFFE;
        public const int RV4_BL_SEC2_NUM_BYTES = 1023;
    }
}
