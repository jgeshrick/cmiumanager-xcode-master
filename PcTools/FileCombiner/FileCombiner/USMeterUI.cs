﻿using System;
using System.Collections.Generic;
using System.IO;
using UltrasonicUIMemoryMap;
using UIM = UltrasonicUIMemoryMap;

namespace FileCombiner
{
    class USMeterUI : INeptuneMeter
    {
        private string uiAppPath;
        private string bootloaderPath;
        private string outputFileLocation;
        private string keyFileLocation;

        private MemoryImage UIMemoryImage = new MemoryImage(UIM.Flash.TotalSize, 0xFF);

        /// <summary>
        /// Constructor - generates combined file from the given inputs
        /// </summary>
        public USMeterUI()
        {
            String[] arguments = Environment.GetCommandLineArgs();

            Console.WriteLine("You have selected the USMeterUI product");

            // Check to make sure there are enough parameters
            if (arguments.Length < 5)
            {
                throw new Exception("You have not passed enough parameters");
            }

            try
            {

                uiAppPath = arguments[2];
                bootloaderPath = arguments[3];
                outputFileLocation = arguments[4];
                keyFileLocation = arguments[5];
            }
            catch (Exception Ex)
            {
                throw new Exception("Failed to read parameters: " + Ex.Message);
            }
        }

        /// <summary>
        /// Read suppiled input files and merge into single image. Add CRCs.
        /// </summary>
        public void ReadFiles()
        {
            try
            {
                // Read in app and bootloader hex files
                UIMemoryImage.ProcessHexFile(uiAppPath, "User Interface Application", true);
                UIMemoryImage.ProcessHexFile(bootloaderPath, "Bootloader", true);
                // Key file is allowed to overwrite used memory locations
                UIMemoryImage.ProcessHexFile(keyFileLocation, "Mach10 Key Table", true, true);

                // Add CRCs

                // Firstly, the booloader (2 parts)
                UIMemoryImage.Crc32(UIM.Bootloader.BaseAddress, UIM.Bootloader.Length, UIM.Bootloader.CrcAddress, true);
                // NB: There is a 'lock' byte at the end of the image so we place the CRC 1 byte earlier to leave room for it.
                UIMemoryImage.Crc32AddRegion(UIM.Bootloader.ExtAddress, UIM.Bootloader.ExtLength - (Constants.CRC_LENGTH_32 + 1), true);

                // Next the Key Table
                UIMemoryImage.Crc32(UIM.UserKeyTable.BaseAddress, UIM.UserKeyTable.Length - Constants.CRC_LENGTH_32, UIM.UserKeyTable.CrcAddress);

                // Next the UI application
                UIMemoryImage.Crc32(UIM.UserInterfaceApp.BaseAddress, UIM.UserInterfaceApp.Length - Constants.CRC_LENGTH_32, UIM.UserInterfaceApp.CrcAddress);

                // Finally, the Config memory region
                UIMemoryImage.Crc32(UIM.Config.BaseAddress, UIM.Config.Length - Constants.CRC_LENGTH_32, UIM.Config.CrcAddress);
           }
            catch (Exception Ex)
            {
                throw new Exception("Failed to combine files: " + Ex.Message);
            }
        }


        /// <summary>
        /// Write out files to disk
        /// </summary>
        public void WriteFiles()
        {
            try
            {
                UIMemoryImage.WriteHexFile(outputFileLocation, 0, UIM.Flash.TotalSize);
            }
            catch (Exception Ex)
            {
                throw new Exception("Failed to write files: " + Ex.Message);
            }
        }

        /// <summary>
        /// Help specific to USMeter
        /// </summary>
        public static void DisplayProgramHelp()
        {
            // Display information
            Console.WriteLine("The paramters to use this program for USMeterUI are:");
            Console.WriteLine("   [0]: Product (USMeterUI)");
            Console.WriteLine("   [1]: Path to user interface application hex file");
            Console.WriteLine("   [2]: Path to bootloader hex file");
            Console.WriteLine("   [3]: Path to output hex file");
            Console.WriteLine("   [4]: Path to key table hex file");
            Console.WriteLine();
        }
    }
}
