﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MdceInterfaceLibrary;

namespace MdceInterfaceTestHarness
{
    public partial class MdceInterfaceTestHarness : Form
    {
        private MdceRestClient itsRestClient;

        public MdceInterfaceTestHarness()
        {
            InitializeComponent();
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            const string SiteId = "1";
            const string PartnerId1 = "7266688fa739b2b3be92961975b095a7";
            const string ServerUrl = "http://dvtest.mdce-nonprod.neptunensight.com:8081/mdce";
            const string ServerUrlSubstring = "/api/v1/";

            itsRestClient = new MdceRestClient(PartnerId1, SiteId, ServerUrl, ServerUrlSubstring);
        }

        private void clickGetPackets(object sender, EventArgs e)
        {
            textBox1.Text = "Get Packets started";
            Form.ActiveForm.Refresh();

            MdceInterfaceLibrary.JsonModel.PacketInformation newPacketInfo = new MdceInterfaceLibrary.JsonModel.PacketInformation();
            var result = itsRestClient.GetPackets(ref newPacketInfo, cbGateway.Checked, tbPacketDate.Text.ToString());
            if (result)
            {
                textBox1.Text = MdceInterfaceLibrary.JsonModel.PacketInformation.ToJSON(newPacketInfo);
            }
            else
            {
                textBox1.Text = "Get Packets failed";
            }
        }
    }
}
