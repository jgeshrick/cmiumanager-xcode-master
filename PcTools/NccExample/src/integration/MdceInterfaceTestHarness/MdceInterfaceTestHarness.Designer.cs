﻿namespace MdceInterfaceTestHarness
{
    partial class MdceInterfaceTestHarness
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.cbGateway = new System.Windows.Forms.CheckBox();
            this.tbPacketDate = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(167, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Get Packets";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.clickGetPackets);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(13, 68);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(293, 181);
            this.textBox1.TabIndex = 1;
            // 
            // cbGateway
            // 
            this.cbGateway.AutoSize = true;
            this.cbGateway.Location = new System.Drawing.Point(201, 18);
            this.cbGateway.Name = "cbGateway";
            this.cbGateway.Size = new System.Drawing.Size(106, 17);
            this.cbGateway.TabIndex = 2;
            this.cbGateway.Text = "Include Gateway";
            this.cbGateway.UseVisualStyleBackColor = true;
            // 
            // tbPacketDate
            // 
            this.tbPacketDate.Location = new System.Drawing.Point(13, 43);
            this.tbPacketDate.Name = "tbPacketDate";
            this.tbPacketDate.Size = new System.Drawing.Size(293, 20);
            this.tbPacketDate.TabIndex = 3;
            this.tbPacketDate.Text = "2016-06-06T14:01:24-05:00";
            // 
            // MdceInterfaceTestHarness
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 261);
            this.Controls.Add(this.tbPacketDate);
            this.Controls.Add(this.cbGateway);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Name = "MdceInterfaceTestHarness";
            this.Text = "MdceInterfaceTestHarness";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.CheckBox cbGateway;
        private System.Windows.Forms.TextBox tbPacketDate;
    }
}

