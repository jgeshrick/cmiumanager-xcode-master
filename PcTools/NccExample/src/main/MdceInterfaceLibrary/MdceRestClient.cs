﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NeptuneCloudConnector.JsonModel;
using NeptuneCloudConnector.Exception;
//using MdceInterfaceLibrary.JsonModel.PacketInformation;
using MdceInterfaceLibrary.JsonModel;

namespace MdceInterfaceLibrary
{
    public class MdceRestClient : NeptuneCloudConnector.NeptuneCloudRestClient
    {

        private Token currentToken;
        private const short TokenExpireTimeJitterMin = 10;
        private const short FpcRestRetries = 3;
        private const string DateFormatIso8601 = "yyyy-MM-dd";

        #region MdceRestClient
        /// <summary>
        /// MdceRestClient
        /// </summary>
        public MdceRestClient(string partnerId, string siteId, string mdceIntegrationUrl, string mdceIntegrationSubUrl)
            : base(partnerId, siteId, mdceIntegrationUrl, mdceIntegrationSubUrl)
        {
        }
        #endregion MdceRestClient


        #region AuthenticationToken (IF03)
        /// <summary>
        /// Attempt to recycle the token if it has not expire
        /// </summary>
        /// <returns>Authenticated token or exception</returns>
        public bool GetAuthenticationToken(ref Token token)
        {
            bool returnCode = false;
            try
            {
                if ((this.currentToken == null) ||
                    !this.currentToken.Authenticated ||
                    (this.currentToken.Expires.HasValue && this.currentToken.Expires.Value.AddMinutes(TokenExpireTimeJitterMin) < DateTime.UtcNow))
                {
                    //either no token has been issued or it has expired.
                    this.currentToken = base.GetAuthenticationToken();
                }
                token = this.currentToken;
                returnCode = true;
            }
            catch
            {
                returnCode = false;
            }
            return returnCode;
        }
        #endregion //AuthenticationToken (IF03)

        #region Packets (IF15)
        /// <summary>
        /// Get Packets (IF15)
        /// This web service will be used by external systems to retrieve packets
        /// received from endpoints corresponding to the site id that the token represents.
        /// Note that a token that requested site id 0 will never return any packets using this.#
        /// As the GetPackets call is only for one site id at a time.
        /// http://dvtest.mdce-nonprod.neptunensight.com:8081/mdce/api/v1/token?partner=7266688fa739b2b3be92961975b095a7&site_id=1
        /// mdce/api/v1/packet?token=ABCDEF0123456789&min_insert_date=2015-01-01T14:01:24-05:00 
        /// </summary>
        /// <param name="serverInformation">ServerInformation object to populate</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public bool GetPackets(ref PacketInformation packetInformation, bool includeGateway, string packetDate)
        {
            bool returnCode = false;
            Token token = new Token();
            int loopCount = 0;

            while ((loopCount < FpcRestRetries) && (returnCode != true))
            {
                loopCount++;
                try
                {
                    returnCode = this.GetAuthenticationToken(ref token);
                }
                catch
                {
                    returnCode = false;
                }

                if (true == returnCode)
                {
                    try
                    {
                        if (includeGateway)
                        {
                            packetInformation = DoGet<PacketInformation>("packet", "token", token.AuthToken, "min_insert_date", packetDate, "include_gateway_packets", "true");
                        }
                        else
                        {
                            packetInformation = DoGet<PacketInformation>("packet", "token", token.AuthToken, "min_insert_date", packetDate);
                        }
                        returnCode = true;
                    }
                    catch
                    {
                        returnCode = false;
                    }
                }
            }
            return returnCode;
        }
        #endregion Packets (IF15)
    }
}
