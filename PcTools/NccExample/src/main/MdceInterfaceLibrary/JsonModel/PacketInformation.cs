﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using RestSharp;
using RestSharp.Deserializers;
using Newtonsoft.Json;
using System.Collections.Generic;


namespace MdceInterfaceLibrary.JsonModel
{
    /// <summary>
    /// Information regarding the endpoint packets
    /// Required for - Get Packets (IF15)
    /// </summary>
    public class PacketInformation
    {
        public DateTimeOffset server_time { get; set; }
        public List<Miu> mius { get; set; }
        public List<Gateway> gateways { get; set; }
        public DateTimeOffset server_last_check { get; set; }

        /// <summary>
        /// This uses RestSharp internal JsonDeserialiser to convert a json string to a business object
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public static String ToJSON(PacketInformation packetInformation)
        {
            var json = JsonConvert.SerializeObject(packetInformation);
            return json.ToString();
        }
    }

    public class Miu
    {
        public int miu_id { get; set; }
        public List<Packets> packets { get; set; }
    }

    public class Gateway
    {
        public string gw_id { get; set; }
        public List<Packets> packets { get; set; }
    }

    public class Packets
    {
        public DateTimeOffset built_date { get; set; }
        public DateTimeOffset insert_date { get; set; }
        public string device_type { get; set; }
        public int packet_type { get; set; }
        public string packetHex { get; set; }
    }
}
