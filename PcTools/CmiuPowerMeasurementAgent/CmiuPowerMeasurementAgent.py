import os
import serial
import time
import subprocess

uMonitorPort = os.environ["UMONITOR_COM_PORT"]
jlinkSerialNo = os.environ["JLINK_SN"]
jlinkPath = os.environ["JLINK_PATH"]
cmiuApplicationBin = os.environ["CMIU_APPLICATION_BIN"]

print "CMIU power measurement script"
print "uMonitor Port: " + uMonitorPort
print "JLink Serial No: " + jlinkSerialNo
print "JLink Path: " + jlinkPath
print "CMIU Application File: " + cmiuApplicationBin

uMon = serial.Serial(uMonitorPort, 115200, timeout=1)

# Prepare uMonitor and cycle power to CMIU
print "Setting up uMonitor..."
uMon.write(b'stop\n')
time.sleep(0.1)
uMon.write(b'set_v 0\n')
time.sleep(1)
uMon.write(b'set_v 3400\n')
time.sleep(10)

# Connect programmer
print "Connecting JLink..."
uMon.write(b'bank_a_on\n')

# Program the CMIU
print "Programming CMIU..."
process = subprocess.Popen(
    jlinkPath + "/JLink.exe -SelectEmuBySN " + jlinkSerialNo,
    stdin=subprocess.PIPE,
    stdout=subprocess.PIPE,
    stderr=subprocess.STDOUT,
    shell=True)

process.stdin.write('si 1\n')
time.sleep(0.5)
process.stdin.write('device EFM32LG332F256\n')
time.sleep(0.5)
process.stdin.write('4000\n')
time.sleep(0.5)
process.stdin.write('connect\n')
time.sleep(0.5)
process.stdin.write('loadbin ./' + cmiuApplicationBin + ' , 0x00\n')
time.sleep(30)
process.stdin.write('exit\n')

process.stdout.read()

process.kill()

print "Restarting CMIU..."
uMon.write(b'bank_a_off\n')
time.sleep(0.1)
uMon.write(b'set_v 0\n')
time.sleep(3)
uMon.write(b'set_v 3400\n')
time.sleep(10)
uMon.write(b'start\n')

print "Monitoring Startup Power Consumption..."

# Repeat 6 times to run for 1 1/2 minutes
for i in range(0, 6):
    uMon.write(b'get_info\n')
    time.sleep(1)
    while uMon.inWaiting() > 0:
        line = uMon.readline()
        if "Average current:" in line:
            averageStartupCurrent = line.split(':')[1].strip()
            print ("##teamcity[progressMessage 'Startup Current: " + averageStartupCurrent + "']")
        if "Average current since last update:" in line:
            print line

    time.sleep(14)


print "Waiting till startup finished..."
time.sleep(60)

uMon.write(b'stop\n')
time.sleep(0.1)
uMon.write(b'start\n')

print "Monitoring Average Power Consumption..."

# Repeat 240 times to run for 1 hour
for i in range(0, 240):
    uMon.write(b'get_info\n')
    time.sleep(1)
    while uMon.inWaiting() > 0:
        line = uMon.readline()
        if "Average current:" in line:
            averageCurrent = line.split(':')[1].strip()
            print ("##teamcity[progressMessage 'Startup Current: " + averageStartupCurrent + "     Average Current: " + averageCurrent + "']")
        if "Average current since last update:" in line:
            print line

    time.sleep(14)

print ("##teamcity[buildStatus status='SUCCESS' text='Startup Current: " + averageStartupCurrent + "     Average Current: " + averageCurrent + "']")

uMon.write(b'stop')
time.sleep(0.1)
uMon.write(b'set_v 0')
