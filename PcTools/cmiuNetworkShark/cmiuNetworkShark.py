# -*- coding: utf-8 -*-
"""
Created on Mon Jun 08 13:54:43 2015

@author: WJD1
"""

import pyshark
import sys
from collections import defaultdict
import os
     
class Pkt:            
    def __init__(self, time, tcp, ip, setBytesToZero):
        #Does not initially have Mqtt details
        self.hasMqtt = False        
        
        #sniff time
        self.time = float(time)
        
        #TCP info
        self.srcport = int(tcp.srcport)
        self.dstport = int(tcp.dstport)
        self.flags_ack = bool(int(tcp.flags_ack))
        self.flags_syn = bool(int(tcp.flags_syn))
        self.flags_fin = bool(int(tcp.flags_fin))
         
        #TCP timing info
        self.iRtt = 0 #default if none
        self.ackRtt = 0 #default if none

	#IP info
	if(setBytesToZero):
            self.sizeBytes = 0
	else:
	    self.sizeBytes = int(ip.len)
        
        if hasattr(tcp, 'analysis_initial_rtt'):
            self.iRtt = float(tcp.analysis_initial_rtt)
        
        if hasattr(tcp, 'analysis_ack_rtt'):
            self.ackRtt = float(tcp.analysis_ack_rtt)
        
    def addMqtt(self, mqtt):
        self.hasMqtt = True
        
        self.msgType = int(mqtt.msgtype)
        self.cmiuId = 0 #default if none
        self.clientId = "" #default if none
        
        if hasattr(mqtt, 'topic'):
            splitTopic = mqtt.topic.split('/')
            if len(splitTopic) == 3:
                if splitTopic[0] == "CMIU":
                    try:
                        self.cmiuId = int(splitTopic[2])
                    except:
                        pass
                
        if hasattr(mqtt, 'clientid'):
            self.clientId = mqtt.clientid
    
    
class MqttTran:
    def __init__(self):
        #Initialise to zeros
        self.cmiuId = 0
        self.startTime = 0
        self.endTime = 0
        self.port = 0
	self.bytesUsed = 0
        
        self.iRtt = 0
        self.rtts = list()
        self.timeCONNECT = 0
        self.timeCONNACK = 0
        self.timePUBLISH = 0
        self.timePUBACK = 0
        self.timeSUBSCRIBE = 0
        self.timeSUBACK = 0
        self.timeDISCONNECT = 0
                 
                 
def processPackets(pkts):
    mqttTrans = list()    
    for i, pkt in enumerate(pkts):
	if pkt.hasMqtt and pkt.msgType == 1 and "CMIU/" in pkt.clientId:
            #Get packets within time frame of publish and on same port
            #Then process those packets into a MqttTrans object
            startTime = pkt.time - 10
            startPkt = i
            endTime = pkt.time + 30
            endPkt = i
            while((startPkt > 0) and (pkts[startPkt].time > startTime)):
                startPkt -= 1
            while((endPkt < len(pkts)) and (pkts[endPkt].time < endTime)):
                endPkt += 1
            tempPkts = list()
            for j in range(startPkt, endPkt):
                tempPkts.append(pkts[j])
            filteredPkts = list()
            for tempPkt in tempPkts :      
                if (tempPkt.srcport == pkt.srcport) or (tempPkt.dstport == pkt.srcport):
                    filteredPkts.append(tempPkt)
            
            mqttTrans.append(processMqttTransPackets(filteredPkts))
        cliProgress(float(i)/float(len(pkts)))
                
    return mqttTrans
                
                
def processMqttTransPackets(pkts):
    mqttTran = MqttTran()
    
    tempPkt = next((pkt for pkt in pkts if pkt.flags_syn), None)
    if tempPkt:
        mqttTran.startTime = tempPkt.time
        mqttTran.port = tempPkt.srcport
    
    tempPkt = next((pkt for pkt in pkts if (pkt.hasMqtt and pkt.msgType == 1)), None)
    if tempPkt:
        mqttTran.timeCONNECT = tempPkt.time
    
    tempPkt = next((pkt for pkt in pkts if (pkt.hasMqtt and pkt.msgType == 2)), None)
    if tempPkt:
        mqttTran.timeCONNACK = tempPkt.time
    
    tempPkt = next((pkt for pkt in pkts if (pkt.hasMqtt and pkt.msgType == 3)), None)
    if tempPkt:
        mqttTran.timePUBLISH = tempPkt.time
        mqttTran.cmiuId = tempPkt.cmiuId
        mqttTran.iRtt = tempPkt.iRtt
    
    tempPkt = next((pkt for pkt in pkts if (pkt.hasMqtt and pkt.msgType == 4)), None)
    if tempPkt:
        mqttTran.timePUBACK = tempPkt.time
    
    tempPkt = next((pkt for pkt in pkts if (pkt.hasMqtt and pkt.msgType == 8)), None)
    if tempPkt:
        mqttTran.timeSUBSCRIBE = tempPkt.time
    
    tempPkt = next((pkt for pkt in pkts if (pkt.hasMqtt and pkt.msgType == 9)), None)
    if tempPkt:
        mqttTran.timeSUBACK = tempPkt.time
    
    tempPkt = next((pkt for pkt in pkts if (pkt.hasMqtt and pkt.msgType == 14)), None)
    if tempPkt:
        mqttTran.timeDISCONNECT = tempPkt.time
        
    tempPkt = next((pkt for pkt in pkts if pkt.flags_fin), None)
    if tempPkt:
        mqttTran.endTime = tempPkt.time

    #Get the number of bytes used at IP layer for entire transaction
    bytesUsed = 0
    for pkt in pkts:
        bytesUsed += pkt.sizeBytes
    mqttTran.bytesUsed = bytesUsed

    
    for pkt in pkts:
        if pkt.ackRtt != 0:
            if pkt.ackRtt > 0.001: #Would expect over 1ms if not host replying
                mqttTran.rtts.append(pkt.ackRtt)
            
    return mqttTran            
    

def cliProgress(progress):
        hashes = '#' * int(round(progress * 60))
        spaces = ' ' * (60 - len(hashes))
        #sys.stdout.write("\rPercent: [{0}] {1}%".format(hashes + spaces, int(round(progress * 100))))
        #sys.stdout.flush()
        
    
def runShark(inputFile):    
    cap = pyshark.FileCapture(input_file=inputFile, 
                              keep_packets=False,
			      display_filter="ip.src != 10.121.101.5 && ip.dst != 10.121.101.5",
                              decode_as={"tcp.port == 1883":"mqtt"},
                              tshark_path="C:/Program Files/Wireshark/tshark.exe")    
                    
    #Create a list of packets to save memory and speed up processing    
    pkts = list()      
    for i, pkt in enumerate(cap):
        packet = Pkt(float(pkt.sniff_timestamp), pkt.tcp, pkt.ip, False)
	appendedPkt = False
	for layer in pkt:
            if(layer.layer_name == "mqtt"):
                packetTemp = Pkt(float(pkt.sniff_timestamp), pkt.tcp, pkt.ip, appendedPkt)
                packetTemp.addMqtt(layer)
		pkts.append(packetTemp)
		appendedPkt = True
	if(appendedPkt == False):
            pkts.append(packet)                          
        sys.stdout.write("\r{0} packets read in".format(i))
        sys.stdout.flush()

    mqttTrans = processPackets(pkts)   

    cmius = defaultdict(list)
    for mqttTran in mqttTrans:
        cmius[mqttTran.cmiuId].append(mqttTran)                       
    try:            
        os.remove(inputFile + "_processed.csv")
    except:
        pass
    f = open(inputFile + "_processed.csv", "w")    
    f.write("CMIU ID, start time, end time, connecting port, bytes, CONNECT time, "  +
            "CONNACK time, PUBLISH time, PUBACK time, SUBSCRIBE time, SUBACK time, " +
            "DISCONNECT time, initial RTT, ACK RTT 1, ACK RTT 2, ACK RTT 3, "        +
            "ACK RTT 4, ACK RTT 5, ACK RTT 6, ACK RTT 7, ACK RTT 8"                  +
            "\n")          
                  
    print "\n\nCMIU ID   || Num"   
    for cmiuKey in cmius:
        print str(cmius[cmiuKey][0].cmiuId) + str(" ||  ") + str(len(cmius[cmiuKey]))
        
        for trans in cmius[cmiuKey]:
            row = str(trans.cmiuId)                + ","
            row += ('%.3f' % trans.startTime)      + ","
            row += ('%.3f' % trans.endTime)        + ","
            row += str(trans.port)                 + ","
	    row += str(trans.bytesUsed)            + ","
            row += ('%.3f' % trans.timeCONNECT)    + ","
            row += ('%.3f' % trans.timeCONNACK)    + ","
            row += ('%.3f' % trans.timePUBLISH)    + ","
            row += ('%.3f' % trans.timePUBACK)     + ","
            row += ('%.3f' % trans.timeSUBSCRIBE)  + ","
            row += ('%.3f' % trans.timeSUBACK)     + ","
            row += ('%.3f' % trans.timeDISCONNECT) + ","
            row += ('%.3f' % trans.iRtt)           + ","
            for rtt in trans.rtts:
                row += str(rtt) + ","
            row += "\n"
            
            f.write(row)
            
    f.close()    

if __name__ == "__main__":
    runShark(sys.argv[1])








                          
                          
