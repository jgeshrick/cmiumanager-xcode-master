﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using Ntg.BpcCore.DataConnectors;
using Ntg.BpcCore.Domain;
using Ntg.BpcCore.Entities.CustomerData;
using Ntg.BpcCore.IntegrationTests.Common;
using Ntg.BpcCore.JsonModel.CrmReference;
using Ntg.BpcCore.Settings;
using NUnit.Framework;

namespace Ntg.BpcCore.IntegrationTests.DataServices
{
    /// <summary>
    /// Integration tests for the <see cref="CustomerDataConnector"/> data service class.
    /// </summary>
    [TestFixture]
    [Explicit("Requires environment variables to be configured")]
    public class CustomerDataServiceTest
    {
        private MdceEnvironment mdceEnvironment;
        private IEnvironmentsConfig environments;
        private IBpcRestClients bpcRestClients;
        private IDataConnector dataService;
        private Mock<IEnvironmentSettingsRepository> environmentSettingsRepositoryMock;
        private Mock<ICustomerDataRepository> customerDataRepositoryMock;
        private List<Account> generatedAccountList;

        /// <summary>
        /// Holds the MDCE data resulting from the TestFixtureSetUp method. Implemented as a singleton to 
        /// avoid needing to re-read the same data back for each test case.
        /// </summary>
        private static CrmReference crmReferenceData;

        /// <summary>
        /// Loads the CRM data into the target system - including some generated reference accounts that 
        /// can be used to validate the ETL process - and then reads the resulting CRM data back from 
        /// MDCE into the static field, "crmReferenceData".
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixture_Execute_DataService_Once()
        {
            // Prepare environments configuration
            Init();

            // Use the AccountListReceived even to intercept the loaded accounts and inject some test data
            ((CustomerDataConnector) dataService).AccountListReceived += (sender, eventArgs) =>
            {
                // Inject the generated accounts
                generatedAccountList.ForEach(eventArgs.AccountList.Add);
            };

            // Run the ETL process
            dataService.Execute();

            // Get the resulting data set back from the MDCE target.
            var restClient = bpcRestClients.RestClients.First().Value;
            crmReferenceData = restClient.GetCrmReferenceData();
        }

        /// <summary>
        /// Clean up on completion.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixture_Cleanup()
        {
            crmReferenceData = null;
        }

        /// <summary>
        /// Prepares a one-to-one environment mapping between SLX and the target MDCE integration test environment.
        /// </summary>
        public void Init()
        {
            environmentSettingsRepositoryMock = new Mock<IEnvironmentSettingsRepository>();
            environmentSettingsRepositoryMock.Setup(m => m.GetBpcPartnerString(It.IsAny<string>())).Returns(BpcCoreTest.SITE_0_PARTNER_ID_1);
            environmentSettingsRepositoryMock.Setup(m => m.HasBpcPartnerString(It.IsAny<string>())).Returns(true);

            customerDataRepositoryMock = new Mock<ICustomerDataRepository>();

            mdceEnvironment = TestUtilities.GetMdceEnvironmentFromEnvironmentVariables();
            environments = new Environments {EnvironmentList = new List<MdceEnvironment> {mdceEnvironment}};
            bpcRestClients = new BpcRestClients(environments, environmentSettingsRepositoryMock.Object);
            dataService = new CustomerDataConnector(bpcRestClients, customerDataRepositoryMock.Object);
            generatedAccountList = ProcedurallyGenerateAccounts();
        }

        /// <summary>
        /// Tests that the three generated accounts have been saved.
        /// </summary>
        [Test]
        public void PostExecute_CrmReferenceData_Contains_Generated_Distributors()
        {
            // Find the generated distributors
            var distributor0 = crmReferenceData.Distributers.SingleOrDefault(d => d.CustomerNumber == "73570150");
            var distributor1 = crmReferenceData.Distributers.SingleOrDefault(d => d.CustomerNumber == "73570151");
            var distributor2 = crmReferenceData.Distributers.SingleOrDefault(d => d.CustomerNumber == "73570152");

            // Assert
            Assert.IsNotNull(distributor0);
            Assert.IsNotNull(distributor1);
            Assert.IsNotNull(distributor2);
        }

        /// <summary>
        /// Tests that the three generated accounts have been saved.
        /// </summary>
        [Test]
        public void PostExecute_CrmReferenceData_Contains_Generated_Utilities()
        {
            // Find the generated utilities
            var utility0 = crmReferenceData.Utilities.SingleOrDefault(u => u.SiteId == 30070);
            var utility1 = crmReferenceData.Utilities.SingleOrDefault(u => u.SiteId == 30071);
            var utility2 = crmReferenceData.Utilities.SingleOrDefault(u => u.SiteId == 30072);

            // Assert
            Assert.IsNotNull(utility0);
            Assert.IsNotNull(utility1);
            Assert.IsNotNull(utility2);
        }

        /// <summary>
        /// Tests that the first distributor has 0 secondary contacts.
        /// </summary>
        [Test]
        public void PostExecute_Distributor0_Has_0_Secondary_Contacts()
        {
            // Find distributor0
            var distributor0 = crmReferenceData.Distributers.Single(d => d.CustomerNumber == "73570150");
            List<Contact> secondaryContacts = new List<Contact>(distributor0.Info.ActiveSecondaryContacts);

            // Assert
            Assert.AreEqual(0, secondaryContacts.Count);
        }

        /// <summary>
        /// Tests that the second distributor has 1 secondary contact.
        /// </summary>
        [Test]
        public void PostExecute_Distributor1_Has_1_Secondary_Contact()
        {
            // Find distributor1
            var distributor1 = crmReferenceData.Distributers.Single(d => d.CustomerNumber == "73570151");
            var slxAccounts = generatedAccountList.Where(a => a.CustomerNumber == "73570151" && !a.IsPrimaryContact());
            List<Contact> secondaryContacts = new List<Contact>(distributor1.Info.ActiveSecondaryContacts);

            // Assert
            Assert.AreEqual(1, secondaryContacts.Count);
            Assert.IsTrue(DoContactsMatch(secondaryContacts.First(), slxAccounts.First()));
        }

        /// <summary>
        /// Tests that the third distributor has 2 secondary contacts.
        /// </summary>
        [Test]
        public void PostExecute_Distributor2_Has_2_Secondary_Contacts()
        {
            // Find distributor2
            var distributor2 = crmReferenceData.Distributers.Single(d => d.CustomerNumber == "73570152");
            var slxAccounts = generatedAccountList.Where(a => a.CustomerNumber == "73570152" && !a.IsPrimaryContact());
            List<Contact> secondaryContacts = new List<Contact>(distributor2.Info.ActiveSecondaryContacts);

            // Assert
            Assert.AreEqual(2, secondaryContacts.Count);
            foreach (var slxAccount in slxAccounts) // Check each secondary contact
            {
                Assert.IsTrue(secondaryContacts.Any(sc => DoContactsMatch(sc, slxAccount)));
            }
        }

        /// <summary>
        /// Tests that the first utility has 0 secondary contacts.
        /// </summary>
        [Test]
        public void PostExecute_Utility0_Has_0_Secondary_Contacts()
        {
            // Find utility0
            var utility0 = crmReferenceData.Utilities.Single(u => u.SiteId == 30070);
            List<Contact> secondaryContacts = new List<Contact>(utility0.Info.ActiveSecondaryContacts);

            // Assert
            Assert.AreEqual(0, secondaryContacts.Count);
        }

        /// <summary>
        /// Tests that the second utility has 1 matching secondary contact.
        /// </summary>
        [Test]
        public void PostExecute_Utility1_Has_1_Matching_Secondary_Contact()
        {
            // Find utility1
            var utility1 = crmReferenceData.Utilities.Single(u => u.SiteId == 30071);
            var slxAccounts = generatedAccountList.Where(a => a.SiteId == "30071" && !a.IsPrimaryContact());
            List<Contact> secondaryContacts = new List<Contact>(utility1.Info.ActiveSecondaryContacts);

            // Assert
            Assert.AreEqual(1, secondaryContacts.Count);
            Assert.IsTrue(DoContactsMatch(secondaryContacts.First(), slxAccounts.First()));
        }

        /// <summary>
        /// Tests that the third utility has 2 matching secondary contacts.
        /// </summary>
        [Test]
        public void PostExecute_Utility2_Has_2_Matching_Secondary_Contacts()
        {
            // Find utility2
            var utility2 = crmReferenceData.Utilities.Single(u => u.SiteId == 30072);
            var slxAccounts = generatedAccountList.Where(a => a.SiteId == "30072" && !a.IsPrimaryContact());
            List<Contact> secondaryContacts = new List<Contact>(utility2.Info.ActiveSecondaryContacts);

            // Assert
            Assert.AreEqual(2, secondaryContacts.Count);
            foreach (var slxAccount in slxAccounts) // Check each secondary contact
            {
                Assert.IsTrue(secondaryContacts.Any(sc => DoContactsMatch(sc, slxAccount)));
            }
        }

        /// <summary>
        /// Tests that a sample distributor read back from MDCE matches the source Account data.
        /// </summary>
        [Test]
        public void PostExecute_Distributor0_Has_Same_Properties_As_Source_Data()
        {
            // Arrange
            var slxAccount = generatedAccountList.First(a => a.CustomerNumber == "73570150" && a.IsPrimaryContact());
            var mdceDistributor = crmReferenceData.Distributers.SingleOrDefault(d => d.CustomerNumber == "73570150");

            // Assert
            Assert.IsNotNull(mdceDistributor);
            var distributorInfo = mdceDistributor.Info;

            Assert.AreEqual(slxAccount.AccountId ?? "", distributorInfo.AccountId);
            Assert.AreEqual(slxAccount.AccountName ?? "", distributorInfo.AccountName);
            Assert.AreEqual(slxAccount.Address1 ?? "", distributorInfo.Address1);
            Assert.AreEqual(slxAccount.Address2 ?? "", distributorInfo.Address2);
            Assert.AreEqual(slxAccount.Address3 ?? "", distributorInfo.Address3);
            Assert.AreEqual(slxAccount.City ?? "", distributorInfo.City);
            Assert.AreEqual(slxAccount.State ?? "", distributorInfo.State);
            Assert.AreEqual(slxAccount.PostalCode ?? "", distributorInfo.PostalCode);
            Assert.AreEqual(slxAccount.Country ?? "", distributorInfo.Country);
            Assert.AreEqual(slxAccount.MainPhone ?? "", distributorInfo.MainPhone);
            Assert.AreEqual(slxAccount.Fax ?? "", distributorInfo.Fax);
            Assert.AreEqual(slxAccount.SalesTerritoryOwner ?? "", distributorInfo.SalesTerritoryOwner);
            Assert.AreEqual(slxAccount.AccountManager ?? "", distributorInfo.AccountManager);
            Assert.AreEqual(slxAccount.RegionalManager ?? "", distributorInfo.RegionalManager);
            Assert.AreEqual(slxAccount.AccountType ?? "", distributorInfo.Type);
            Assert.AreEqual(slxAccount.AccountSubtype ?? "", distributorInfo.SubType);
            Assert.AreEqual(slxAccount.AccountStatus ?? "", distributorInfo.Status);
            Assert.AreEqual(slxAccount.ParentCustomerNumber ?? "", distributorInfo.ParentCustomerNumber);

            Assert.IsTrue(DoContactsMatch(distributorInfo.Contact, slxAccount));
        }

        /// <summary>
        /// Tests that a sample utility read back from MDCE matches the source Account data.
        /// </summary>
        [Test]
        public void PostExecute_Utility0_Has_Same_Properties_As_Source_Data()
        {
            // Arrange
            var slxAccount = generatedAccountList.First(a => a.SiteId == "30070" && a.IsPrimaryContact());
            var mdceUtility = crmReferenceData.Utilities.SingleOrDefault(d => d.SiteId == 30070);

            // Assert
            Assert.IsNotNull(mdceUtility);
            var utilityInfo = mdceUtility.Info;

            Assert.AreEqual(slxAccount.SystemId ?? "", utilityInfo.SystemId);
            Assert.AreEqual(slxAccount.AccountName ?? "", utilityInfo.AccountName);
            Assert.AreEqual("", utilityInfo.CustomerNumber ?? ""); // Not currently mapped
            Assert.AreEqual(slxAccount.Address1 ?? "", utilityInfo.Address1);
            Assert.AreEqual(slxAccount.Address2 ?? "", utilityInfo.Address2);
            Assert.AreEqual(slxAccount.Address3 ?? "", utilityInfo.Address3);
            Assert.AreEqual(slxAccount.City ?? "", utilityInfo.City);
            Assert.AreEqual(slxAccount.State ?? "", utilityInfo.State);
            Assert.AreEqual(slxAccount.PostalCode ?? "", utilityInfo.PostalCode);
            Assert.AreEqual(slxAccount.Country ?? "", utilityInfo.Country);
            Assert.AreEqual(slxAccount.MainPhone ?? "", utilityInfo.MainPhone);
            Assert.AreEqual(slxAccount.Fax ?? "", utilityInfo.Fax);
            Assert.AreEqual(slxAccount.SalesTerritoryOwner ?? "", utilityInfo.SalesTerritoryOwner);
            Assert.AreEqual(slxAccount.AccountManager ?? "", utilityInfo.AccountManager);
            Assert.AreEqual(slxAccount.RegionalManager ?? "", utilityInfo.RegionalManager);
            Assert.AreEqual(slxAccount.AccountType ?? "", utilityInfo.Type);
            Assert.AreEqual(slxAccount.AccountSubtype ?? "", utilityInfo.SubType);
            Assert.AreEqual(slxAccount.AccountStatus ?? "", utilityInfo.Status);
            Assert.AreEqual(slxAccount.ParentCustomerNumber ?? "", utilityInfo.ParentCustomerNumber);

            Assert.IsTrue(DoContactsMatch(utilityInfo.Contact, slxAccount));
        }

        /// <summary>
        /// Returns a boolean value indicating whether the supplied <see cref="Contact"/> contains the 
        /// same data as the supplied <see cref="Account"/>.
        /// </summary>
        /// <param name="contact"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        private static bool DoContactsMatch(Contact contact, Account account)
        {
            bool match = 
                (contact.Name ?? "").Trim().Equals(string.Format("{0} {1}", account.ContactFirstName, account.ContactLastName).Trim(), StringComparison.OrdinalIgnoreCase) &&
                (contact.Title ?? "").Trim().Equals((account.ContactTitle ?? "").Trim(), StringComparison.OrdinalIgnoreCase) &&
                (contact.WorkPhone ?? "").Trim().Equals((account.ContactWorkPhone ?? "").Trim());

            return match;
        }

        /// <summary>
        /// Generates test accounts to be used as reference data in the integration tests.
        /// </summary>
        /// <returns></returns>
        private static List<Account> ProcedurallyGenerateAccounts()
        {
            // TODO: May need to modify when we have more information about the CustomerNumbers:List<string> property on distributor/utility.
            List<Account> accounts = new List<Account>();
            
            
            accounts.AddRange(ProcedurallyGenerateDistributors());
            accounts.AddRange(ProcedurallyGenerateUtilities());

            return accounts;
        }

        /// <summary>
        /// Generates 3 distributors with 0, 1 and 2 secondary contacts respectively
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<Account> ProcedurallyGenerateDistributors()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int line = 0; line < i + 1; line++)
                {
                    var distributor = new Account
                    {
                        AccountId = string.Format("INTTESTDIST{0}", i),
                        CustomerNumber = string.Format("7357015{0}", i),
                        AccountName = string.Format("Integration Test Distributor {0}", i),
                        Address1 = string.Format("Distributor Address {0} Line 1", i),
                        Address2 = string.Format("Distributor Address {0} Line 2", i),
                        Address3 = string.Format("Distributor Address {0} Line 3", i),
                        City = string.Format("Distributor City {0}", i),
                        State = "FL",
                        PostalCode = string.Format("7357{0}", i),
                        Country = "USA",
                        MainPhone = string.Format("735781570{0}", i),
                        Fax = string.Format("735781571{0}", i),
                        SalesTerritoryOwner = string.Format("DISTSTO0000{0}", i),
                        AccountManagerId = string.Format("DISTAMI0000{0}", i),
                        AccountManager = string.Format("Account Manager {0}", i),
                        RegionalManagerId = string.Format("DISTRMI0000{0}", i),
                        RegionalManager = string.Format("Regional Manager {0}", i),
                        AccountType = "End User", // TODO: Test different scenarios
                        AccountSubtype = null, // TODO: Test different scenarios
                        AccountStatus = "Active", // TODO: Test different scenarios
                        ParentAccountId = null,
                        ParentCustomerNumber = null,
                        SiteId = string.Format("7357{0}", i), // TODO: Verify that this should be the same as PostalCode
                        SystemId = null // TODO: Test with non-null values
                    };

                    if (line == 0)
                    {
                        // Populate as primary contact
                        distributor.ContactId = string.Format("DISTPC00000{0}", i);
                        distributor.ContactFirstName = string.Format("Dist{0}FirstName{1}", i, line);
                        distributor.ContactLastName = string.Format("Dist{0}LastName{1}", i, line);
                        distributor.ContactWorkPhone = string.Format("73578150{0}{1}", i, line);
                        distributor.ContactTitle = string.Format("Dist{0}Title{1}", i, line);
                        distributor.ContactStatus = "Active"; // TODO: Test different scenarios
                        distributor.ContactIsPrimary = "T";
                    }
                    else
                    {
                        // Populate as secondary contact
                        distributor.ContactId = string.Format("DISTSC0000{0}{1}", i, line);
                        distributor.ContactFirstName = string.Format("Dist{0}FirstName{1}", i, line);
                        distributor.ContactLastName = string.Format("Dist{0}LastName{1}", i, line);
                        distributor.ContactWorkPhone = string.Format("73578150{0}{1}", i, line);
                        distributor.ContactTitle = string.Format("Dist{0}Title{1}", i, line);
                        distributor.ContactStatus = "Active"; // TODO: Test different scenarios
                        distributor.ContactIsPrimary = "F"; // TODO: Test with nulls
                    }

                    yield return distributor;
                }
            }
        }

        /// <summary>
        /// Generates 3 utilities with 0, 1 and 2 secondary contacts respectively
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<Account> ProcedurallyGenerateUtilities()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int line = 0; line < i + 1; line++)
                {
                    var utility = new Account
                    {
                        AccountId = string.Format("INTTESTUTIL{0}", i),
                        CustomerNumber = string.Format("3007015{0}", i),
                        AccountName = string.Format("Integration Test Utility {0}", i),
                        Address1 = string.Format("Utility Address {0} Line 1", i),
                        Address2 = string.Format("Utility Address {0} Line 2", i),
                        Address3 = string.Format("Utility Address {0} Line 3", i),
                        City = string.Format("Utility City {0}", i),
                        State = "FL",
                        PostalCode = string.Format("3007{0}", i),
                        Country = "USA",
                        MainPhone = string.Format("300781570{0}", i),
                        Fax = string.Format("300781571{0}", i),
                        SalesTerritoryOwner = string.Format("UTILSTO0000{0}", i),
                        AccountManagerId = string.Format("UTILAMI0000{0}", i),
                        AccountManager = string.Format("Account Manager {0}", i),
                        RegionalManagerId = string.Format("UTILRMI0000{0}", i),
                        RegionalManager = string.Format("Regional Manager {0}", i),
                        AccountType = "End User", // TODO: Test different scenarios
                        AccountSubtype = null, // TODO: Test different scenarios
                        AccountStatus = "Active", // TODO: Test different scenarios
                        ParentAccountId = string.Format("INTTESTDIST{0}", i), // Refers to one of the procedurally generated distributors
                        ParentCustomerNumber = string.Format("7357015{0}", i), // Refers to one of the procedurally generated distributors
                        SiteId = string.Format("3007{0}", i), // TODO: Verify that this should be the same as PostalCode
                        SystemId = null // TODO: Test with non-null values
                    };

                    if (line == 0)
                    {
                        // Populate as primary contact
                        utility.ContactId = string.Format("UTILPC00000{0}", i);
                        utility.ContactFirstName = string.Format("Util{0}FirstName{1}", i, line);
                        utility.ContactLastName = string.Format("Util{0}LastName{1}", i, line);
                        utility.ContactWorkPhone = string.Format("30078150{0}{1}", i, line);
                        utility.ContactTitle = string.Format("Util{0}Title{1}", i, line);
                        utility.ContactStatus = "Active"; // TODO: Test different scenarios
                        utility.ContactIsPrimary = "T";
                    }
                    else
                    {
                        // Populate as secondary contact
                        utility.ContactId = string.Format("UTILSC0000{0}{1}", i, line);
                        utility.ContactFirstName = string.Format("Util{0}FirstName{1}", i, line);
                        utility.ContactLastName = string.Format("Util{0}LastName{1}", i, line);
                        utility.ContactWorkPhone = string.Format("30078150{0}{1}", i, line);
                        utility.ContactTitle = string.Format("Util{0}Title{1}", i, line);
                        utility.ContactStatus = "Active"; // TODO: Test different scenarios
                        utility.ContactIsPrimary = "F"; // TODO: Test with nulls
                    }

                    yield return utility;
                }
            }
        }
    }
}
