﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Threading;
using Moq;
using Ntg.BpcCore.DataConnectors;
using Ntg.BpcCore.Scheduling;
using Ntg.BpcCore.Settings;
using NUnit.Framework;

namespace Ntg.BpcCore.IntegrationTests.DataServices
{
    /// <summary>
    /// Contains tests to verify that the service scheduler behaves as expected, using short execution intervals.
    /// </summary>
    [TestFixture]
    public class DataServiceSchedulerTest
    {
        private Mock<IEnvironmentsConfig> environmentsConfigMock;
        private Mock<IEnvironmentSettingsRepository> environmentSettingsRepoMock;
        private Mock<IDataConnector> dataServiceMock;
        private ScheduledActivity scheduledService;
        private Mock<IBpcRestClients> restClientsMock;
        private Mock<IMdceEnvironmentBpcRestClient> mdceEnvironmentBpcRestClientMock;
        private MockServiceSettingsRepository mockServiceSettingsRepository;

        [SetUp]
        public void Init()
        {
            environmentsConfigMock = new Mock<IEnvironmentsConfig>();
            dataServiceMock = new Mock<IDataConnector>();
            mockServiceSettingsRepository = new MockServiceSettingsRepository();
            environmentSettingsRepoMock = new Mock<IEnvironmentSettingsRepository>();
            mdceEnvironmentBpcRestClientMock = new Mock<IMdceEnvironmentBpcRestClient>();
            restClientsMock = new Mock<IBpcRestClients>();

            // Mocked connector settings
            mockServiceSettingsRepository.SetMockSettings(DataConnectorType.CustomerDataConnector, false, null, null, DateTime.UtcNow.AddSeconds(1));
            mockServiceSettingsRepository.SetMockSettings(DataConnectorType.CostRevenueDataConnector, false, null, null, DateTime.UtcNow.AddSeconds(1));
            mockServiceSettingsRepository.SetMockSettings(DataConnectorType.MiuLifecycleDataConnector, false, null, null, DateTime.UtcNow.AddSeconds(1));

            // Configuration for a CustomerDataService with 1 second execution interval
            var testEnvironment = new MdceEnvironment
            {
                Name = "TEST",
                IsEnabled = true,
                BaseUrl = "http://localhost:9999"
            };

            mdceEnvironmentBpcRestClientMock.SetupGet(c => c.Environment).Returns(testEnvironment);
            //mdceEnvironmentBpcRestClientMock.SetupGet(c => c.).Returns(testEnvironment);

            var scheduledServiceConfigs = new List<ScheduleConfiguration>
            {
                new ScheduleConfiguration
                {
                    ScheduleType = ScheduleType.Daily,
                    StartTimeAsString = "0:00:00",
                    RepeatIntervalAsString = "0:00:01",
                    RepeatDurationAsString = "24:00:00",
                    DataConnectorType = DataConnectorType.CustomerDataConnector
                }
            };
            environmentsConfigMock.SetupGet(c => c.ScheduleConfigurations).Returns(scheduledServiceConfigs);
            environmentsConfigMock.SetupGet(c => c.PrimaryMdceEnvironmentName).Returns("TEST");
            environmentsConfigMock.SetupGet(c => c.EnvironmentList).Returns(new List<MdceEnvironment> {testEnvironment});

            // Data service mock
            dataServiceMock.SetupGet(m => m.DataConnectorType).Returns(DataConnectorType.CustomerDataConnector);
            dataServiceMock.SetupGet(m => m.Name).Returns("Mocked Customer Data Service");

            // Empty set of MDCE endpoints
            restClientsMock.SetupGet(m => m.RestClients).Returns(new Dictionary<string, IMdceEnvironmentBpcRestClient>
            {
                {"TEST", new MdceEnvironmentBpcRestClient(testEnvironment, environmentSettingsRepoMock.Object)}
            });
        }

        /// <summary>
        /// Verify that a data service is executed within the expected timeframe when it is enabled.
        /// </summary>
        [Test]
        public void EnableScheduledServices_WhenCalled_StartsScheduledDataServices()
        {
            // Arrange
            IScheduler scheduler = new Scheduler(environmentsConfigMock.Object, restClientsMock.Object, mockServiceSettingsRepository);
            scheduledService = new ScheduledActivity(dataServiceMock.Object, mockServiceSettingsRepository);

            //  - Replace the default service with the mock
            scheduler.ScheduledServices.Clear();
            scheduler.ScheduledServices.Add(scheduledService);
            bool wasExecuteCalled = false;
            dataServiceMock.Setup(m => m.Execute()).Callback(() => { wasExecuteCalled = true; }).Returns(true);
            dataServiceMock.SetupGet(m => m.IsExecuting).Returns(wasExecuteCalled);

            // Act
            var startTime = DateTime.UtcNow;
            scheduler.EnableScheduledServices();
            TimeSpan elapsed = TimeSpan.Zero;
            while (!wasExecuteCalled && elapsed.TotalSeconds < 5) // Allow 5 seconds for the method to be called
            {
                elapsed = DateTime.UtcNow - startTime;
                Thread.Sleep(TimeSpan.FromMilliseconds(100));
            }

            // Assert
            Assert.IsTrue(wasExecuteCalled);
        }

        /// <summary>
        /// Verify that a data service is executed within the expected timeframe when its saved state has IsEnabled=true.
        /// </summary>
        [Test]
        public void Ctor_WhenSavedStateIsEnabled_StartsScheduledDataServices()
        {
            // Arrange
            IScheduler scheduler = new Scheduler(environmentsConfigMock.Object, restClientsMock.Object, mockServiceSettingsRepository);
            mockServiceSettingsRepository.SetMockSettings(DataConnectorType.CustomerDataConnector, true, null, null, DateTime.UtcNow);
            var startTime = DateTime.UtcNow;
            scheduledService = new ScheduledActivity(dataServiceMock.Object, mockServiceSettingsRepository);

            //  - Replace the default service with the mock
            scheduler.ScheduledServices.Clear();
            scheduler.ScheduledServices.Add(scheduledService);
            bool wasExecuteCalled = false;
            dataServiceMock.Setup(m => m.Execute()).Callback(() => { wasExecuteCalled = true; }).Returns(true);
            dataServiceMock.SetupGet(m => m.IsExecuting).Returns(wasExecuteCalled);

            // Act
            TimeSpan elapsed = TimeSpan.Zero;
            while (!wasExecuteCalled && elapsed.TotalSeconds < 5) // Allow 5 seconds for the method to be called
            {
                elapsed = DateTime.UtcNow - startTime;
                Thread.Sleep(TimeSpan.FromMilliseconds(100));
            }

            // Assert
            Assert.IsTrue(wasExecuteCalled);
        }

        /// <summary>
        /// Verify that a data service is not executed after DisableScheduledServices() is called.
        /// </summary>
        [Test]
        public void DisableScheduledServices_WhenCalled_StopsScheduledDataServices()
        {
            // Arrange
            IScheduler scheduler = new Scheduler(environmentsConfigMock.Object, restClientsMock.Object, mockServiceSettingsRepository);
            scheduledService = new ScheduledActivity(dataServiceMock.Object, mockServiceSettingsRepository);

            //  - Replace the default service with the mock
            scheduler.ScheduledServices.Clear();
            scheduler.ScheduledServices.Add(scheduledService);
            bool wasExecuteCalled = false;
            dataServiceMock.Setup(m => m.Execute()).Callback(() => { wasExecuteCalled = true; }).Returns(true);
            dataServiceMock.SetupGet(m => m.IsExecuting).Returns(wasExecuteCalled);

            // Act
            var startTime = DateTime.UtcNow;
            scheduler.DisableScheduledServices();
            TimeSpan elapsed = TimeSpan.Zero;
            while (!wasExecuteCalled && elapsed.TotalSeconds < 5) // Allow 5 seconds for the method to be called
            {
                elapsed = DateTime.UtcNow - startTime;
                Thread.Sleep(TimeSpan.FromMilliseconds(100));
            }

            // Assert
            Assert.IsFalse(wasExecuteCalled);
        }
    }
}