﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.IO;
using Ntg.BpcCore.Settings;

namespace Ntg.BpcCore.IntegrationTests.Common
{
    public class TestUtilities
    {
        public static void CreateEnvironmentsXml(string fileName)
        {
            if (!File.Exists(fileName))
            {
                Environments env = new Environments();
                var envList = env.EnvironmentList;
                envList.Add(new MdceEnvironment { Name = "env1", BaseUrl = "https://env1.neptune.com:1234", IsEnabled = true });
                envList.Add(new MdceEnvironment { Name = "env2", BaseUrl = "https://env2.neptune.com:1234", IsEnabled = true });
                envList.Add(new MdceEnvironment { Name = "env3", BaseUrl = "https://env3.neptune.com:1234", IsEnabled = false });

                env.CostAndRevenueServerDataServer = "sql-server1";
                env.CustomerDataServer = "sql-server2";

                env.WriteToXml(fileName);
            }
        }

        /// <summary>
        /// Creates an instance of <see cref="MdceEnvironment"/> from the current Windows environment variables.
        /// </summary>
        /// <returns></returns>
        public static MdceEnvironment GetMdceEnvironmentFromEnvironmentVariables()
        {
            string name = Environment.GetEnvironmentVariable("NCC_INTEGRATION_ENVIRONEMNT_NAME") ?? "Development1";
            string rootUrl = Environment.GetEnvironmentVariable("NCC_INTEGRATION_URL") ?? "https://dev1.mdce-nonprod.neptunensight.com:8444/";
            string baseUrl = string.Format("{0}/", rootUrl.TrimEnd('/'));
            var mdceEnvironment = new MdceEnvironment
            {
                Name = name,
                BaseUrl = baseUrl,
                IsEnabled = true
            };

            return mdceEnvironment;
        }
    }
}




