﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Linq;
using System.Threading.Tasks;
using Ntg.BpcCore.IntegrationTests.JsonModel;
using Ntg.BpcCore.JsonModel.CrmReference;
using NUnit.Framework;

namespace Ntg.BpcCore.IntegrationTests
{
    [TestFixture]
    [Explicit("Dependent on Auto Testing environment")]
    public class BpcCoreTest
    {
        private const int TEST_MIU_ID = 400000110;

        internal static readonly string SITE_0_ID = "0";
        internal static readonly string SITE_0_PARTNER_ID_0 = "c2a31f64e9430f1c8b85ccbbb776ad75";
        internal static readonly string SITE_0_PARTNER_ID_1 = "d7a300692860293924ec82f849f25691";
        internal static readonly string SITE_1_ID = "1";
        internal static readonly string SITE_1_PARTNER_ID = "7266688fa739b2b3be92961975b095a7";

        private static readonly string MDCE_INTEGRATION_TEST_URL = "https://auto-test.mdce-nonprod.neptunensight.com:8444";
      
        [Test()]
        public async Task GetConfigurationHistoryTest()
        {
            var pcei = new BpcRestClient(SITE_0_PARTNER_ID_0, SITE_0_ID, MDCE_INTEGRATION_TEST_URL);

            var configHistoryAll = await pcei.GetConfigurationHistoryAsync(null);

            Assert.NotNull(configHistoryAll);
        }

        [Test()]
        public void SetCrmReferenceDataTest()
        {
            // Arrange
            var pcei = new BpcRestClient(SITE_0_PARTNER_ID_1, SITE_0_ID, MDCE_INTEGRATION_TEST_URL);

            // Act
            var referenceData = CrmReference.FromJson(CrmReferenceTests.JSON_CRM_REFERENCE);
            var result = pcei.SetCrmReferenceData(referenceData);

            // Assert
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Verify that a change to a Distributer secondary contact is persisted by MDCE.
        /// </summary>
        [Test()]
        // TODO: Remove ignore
        [Ignore("GET doesn't return distributors yet")]
        public void SetCrmReferenceDataTest_Distributer_ActiveSecondaryContact_VerifyChangeCanBeReadBack()
        {
            // Arrange
            var pcei = new BpcRestClient(SITE_0_PARTNER_ID_1, SITE_0_ID, MDCE_INTEGRATION_TEST_URL);
            var referenceData = CrmReference.FromJson(CrmReferenceTests.JSON_CRM_REFERENCE);
            referenceData.Distributers[0].Info.ActiveSecondaryContacts[0].Name = "de Stribouteur-Veritable, M.";
            var result = pcei.SetCrmReferenceData(referenceData);
            Assert.IsTrue(result);

            // Act
            var updated = pcei.GetCrmReferenceData();

            // Clean-up
            referenceData = CrmReference.FromJson(CrmReferenceTests.JSON_CRM_REFERENCE);
            result = pcei.SetCrmReferenceData(referenceData);
            Assert.IsTrue(result);

            // Assert
            var distributer = updated.Distributers.Single(d => d.CustomerNumber == referenceData.Distributers[0].CustomerNumber);
            var contact = distributer.Info.ActiveSecondaryContacts.SingleOrDefault(c => c.Name == "de Stribouteur-Veritable, M.");
            Assert.IsNotNull(contact);
        }

        /// <summary>
        /// Verify that a change to a Utility secondary contact is persisted by MDCE.
        /// </summary>
        [Test()]
        public void SetCrmReferenceDataTest_Utility_ActiveSecondaryContact_VerifyChangeCanBeReadBack()
        {
            // Arrange
            var pcei = new BpcRestClient(SITE_0_PARTNER_ID_1, SITE_0_ID, MDCE_INTEGRATION_TEST_URL);
            var referenceData = CrmReference.FromJson(CrmReferenceTests.JSON_CRM_REFERENCE);
            referenceData.Utilities[0].Info.ActiveSecondaryContacts[0].Name = "Philipson-Lee, Alan";
            var result = pcei.SetCrmReferenceData(referenceData);
            Assert.IsTrue(result);

            // Act
            var updated = pcei.GetCrmReferenceData();

            // Clean-up
            referenceData = CrmReference.FromJson(CrmReferenceTests.JSON_CRM_REFERENCE);
            result = pcei.SetCrmReferenceData(referenceData);
            Assert.IsTrue(result);

            // Assert
            var utility = updated.Utilities.Single(d => d.SiteId == referenceData.Utilities[0].SiteId);
            var contact = utility.Info.ActiveSecondaryContacts.SingleOrDefault(c => c.Name == "Philipson-Lee, Alan");
            Assert.IsNotNull(contact);
        }
    }
}
