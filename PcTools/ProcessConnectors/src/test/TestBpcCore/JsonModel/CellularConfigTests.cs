﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using NUnit.Framework;
using BpcCore.JsonModel.CellularConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BpcCore.JsonModel.CellularConfig.Tests
{
    [TestFixture()]
    public class CellularConfigTests
    {
        /// <summary>
        /// This is to test the correctness of deserialising a json to POCO.
        /// </summary>
        [Test()]
        public void CellularConfigDeserialiseTest()
        {
            var cellularConfig = CellularConfig.fromJSON(JSON_CELLULAR_CONFIG);

            Assert.NotNull(cellularConfig);

            Assert.AreEqual(5000001, cellularConfig.MiuId);
            Assert.AreEqual("1.0.13421", cellularConfig.ModemConfig.FirmwareRevision);
            Assert.AreEqual("356938035643809", cellularConfig.ModemConfig.Imei);
            Assert.AreEqual("Telit", cellularConfig.ModemConfig.Vendor);
            Assert.AreEqual("8991101200003204510", cellularConfig.SimConfig.Iccid);
            Assert.AreEqual("310150123456789", cellularConfig.SimConfig.Imsi);
            Assert.AreEqual("7307406945", cellularConfig.SimConfig.Msidn);
        }


        #region TEST_JSON_STRING
        /// <summary>
        /// Json string extracted from ETI CRM Reference Data (IF42)
        /// </summary>
        public static string JSON_CELLULAR_CONFIG = @"
{
    ""miu_id"": 5000001,
    ""modem_config"": 
    {
        ""vendor"": ""Telit"",
        ""firmware_revision"": ""1.0.13421"",
        ""imei"": ""356938035643809""
    },
    ""sim_config"": 
    {
        ""iccid"": ""8991101200003204510"",
        ""imsi"": ""310150123456789"",
        ""msidn"": ""7307406945""
    }

}
";
        #endregion

    }

}