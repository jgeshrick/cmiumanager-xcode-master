﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using Ntg.BpcCore.JsonModel.CrmReference;
using NUnit.Framework;

namespace Ntg.BpcCore.IntegrationTests.JsonModel
{
    [TestFixture()]
    public class CrmReferenceTests
    {
        /// <summary>
        /// This is to test the correctness of deserializing a json to POCO.
        /// </summary>
        [Test()]
        public void CrmReferenceDeserializeTest()
        {
            var crmReference = CrmReference.FromJson(JSON_CRM_REFERENCE);

            Assert.NotNull(crmReference);

            Assert.AreEqual("08225300", crmReference.Distributers[0].CustomerNumber);
            Assert.AreEqual("8070000", crmReference.Distributers[0].Info.ParentCustomerNumber);

            Assert.AreEqual("Opelika, City Of", crmReference.Utilities[0].Info.AccountName);
            Assert.AreEqual("Project Manager", crmReference.Utilities[0].Info.ActiveSecondaryContacts[0].Title);
            Assert.AreEqual("Wood, Robin", crmReference.Utilities[0].Info.ActiveSecondaryContacts[1].Name);

            Assert.AreEqual("Griffin, City Of", crmReference.Utilities[1].Info.AccountName);
            Assert.AreEqual("Keller, Brant", crmReference.Utilities[1].Info.Contact.Name);
            Assert.AreEqual(30223, crmReference.Utilities[1].SiteId);
        }

        #region TEST_JSON_STRING
        /// <summary>
        /// Json string extracted from ETI CRM Reference Data (IF42)
        /// </summary>
        public static string JSON_CRM_REFERENCE = @"
{
	""distributers"": [
		{
			""customer_number"": ""08225300"",
			""info"": {
				""account_name"": ""Distributer X"",
                          ""parent_customer_number"": ""8070000"",
				""account_id"": ""A3QEYA100000"",
				""address1"": ""Distributer Land"",
				""city"": ""Distributer Town"",
				""country"": ""USA"",
                          ""main_phone"": ""(334) 123-4567"",
				""fax"": ""(334) 123-4568"",
				""sales_territory_owner"": ""24020"",
				""account_manager"": ""Smith, Kevin"",
				""regional_manager"": ""Elliott, Mitch"",
				""type"": ""Distributer"",
				""sub_type"": """",
				""status"": ""Active"",
				""primary_contact"": {
					""name"": ""Stributer, Dee"",
					""work_phone"": ""(334) 123-4569"",
					""title"": """"					
				},
				""active_secondary_contacts"": [
					{
						""name"": ""de Stribouteur, M."",
						""work_phone"": ""(334) 123-4570"",
						""title"": ""Project Manager""					
					}
				],
				""customer_numbers"": [
					""743""
				]
			}
		}
	],
	""utilities"": [
		{
			""site_id"": 36801
			""info"": {
                          ""customer_number"": ""8070000"",
				""system_id"": ""127"",
				""account_name"": ""Opelika, City Of"",
				""address1"": ""Water Works Board"",
				""address2"": ""PO Box #1029"",
				""city"": "" Opelika "",
				""postal_code"": ""36801"",
				""state"": ""AL"",
				""country"": ""USA"",
				""main_phone"": ""(334) 705-5500"",
				""fax"": ""(334) 754-3487"",
				""sales_territory_owner"": ""24020"",
				""account_manager"": ""Smith, Kevin"",
				""regional_manager"": ""Elliott, Mitch"",
				""type"": ""Municipal - Neg"",
				""sub_type"": ""Key Account"",
				""status"": ""Active"",
				""primary_contact"": {
					""name"": ""Hilyer, Dan"",
					""work_phone"": ""(334) 705-5505"",
					""title"": """"					
				},
				""active_secondary_contacts"": [
					{
						""name"": ""Lee, Alan"",
						""work_phone"": ""(334) 705-5500"",
						""title"": ""Project Manager""					
					},
					{
						""name"": ""Wood, Robin"",
						""work_phone"": ""(334) 749-7589"",
						""title"": """"					
					}
				],
				""customer_numbers"": [
					""742""
				],
				""parent_customer_number"": ""123456""
			}
		},
		{
			""site_id"": 30223
			""info"": {
				""system_id"": ""128"",
				""account_name"": ""Griffin, City Of"",
                          ""customer_number"": ""8070000"",
				""address1"": ""229 North Expressway"",
				""address2"": ""PO Box T"",
				""city"": ""Griffin "",
				""postal_code"": ""36801"",
				""state"": ""GA"",
				""country"": ""USA"",
                          ""main_phone"": ""(770) 229-6423"",
				""fax"": ""(770) 754-3487"",
				""sales_territory_owner"": ""24040"",
				""account_manager"": ""Cone, Dallas"",
				""regional_manager"": ""Elliott, Mitch"",
				""type"": ""End User"",
				""sub_type"": """",
				""status"": ""Active"",
				""primary_contact"": {
					""name"": ""Keller, Brant"",
					""work_phone"": ""(770) 229-6423"",
					""title"": """"					
				},
				""active_secondary_contacts"": [
				],
				""customer_numbers"": [
					""1234567""
				],
				""parent_customer_number"": ""08225300""
			}
		}		
	
	]
}
";
        #endregion

    }
}