﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using Ntg.BpcCore.JsonModel;
using Ntg.BpcCore.JsonModel.ConfigHistory;
using NUnit.Framework;

namespace Ntg.BpcCore.IntegrationTests.JsonModel
{
    [TestFixture()]
    public class ConfigHistoryTests
    {
        /// <summary>
        /// This is to test the correctness of deserialising a json to POCO.
        /// </summary>
        [Test()]
        public void ConfigHistoryDeserializeTest()
        {
            var configHistory = ConfigHistory.FromJson(JSON_CONFIG_HISTORY);

            Assert.NotNull(configHistory);

            Assert.AreEqual(2, configHistory.Mius.Count);
            Assert.AreEqual("Y", configHistory.Mius[0].Billable);
            Assert.AreEqual(5000001, configHistory.Mius[0].MiuId);
            Assert.AreEqual(55555, configHistory.Mius[0].SiteId);
            Assert.AreEqual("CMIU", configHistory.Mius[0].DeviceType);
            Assert.AreEqual(1, configHistory.Mius[0].CMIUType);

            Assert.AreEqual(3, configHistory.Mius[0].ConfigurationHistory.Count);
            Assert.AreEqual(1, configHistory.Mius[0].ConfigurationHistory[0].StartDate.Day);
            Assert.AreEqual(1, configHistory.Mius[0].ConfigurationHistory[0].StartDate.Month);
            Assert.AreEqual(2015, configHistory.Mius[0].ConfigurationHistory[0].StartDate.Year);
            Assert.AreEqual(8, configHistory.Mius[0].ConfigurationHistory[0].EndDate.Value.Day);
            Assert.AreEqual(1, configHistory.Mius[0].ConfigurationHistory[0].EndDate.Value.Month);
            Assert.AreEqual(2015, configHistory.Mius[0].ConfigurationHistory[0].EndDate.Value.Year);
            Assert.AreEqual(1, configHistory.Mius[0].ConfigurationHistory[0].Config.CMIUType);
            Assert.AreEqual("Y", configHistory.Mius[0].ConfigurationHistory[0].Config.Billing.Billable);
            Assert.AreEqual(1, configHistory.Mius[0].ConfigurationHistory[0].Config.Billing.DistributerId);
            Assert.AreEqual(5555, configHistory.Mius[0].ConfigurationHistory[0].Config.Billing.SiteId);
            Assert.AreEqual(420, configHistory.Mius[0].ConfigurationHistory[0].Config.QuietTime.QuietStartMins);
            Assert.AreEqual(480, configHistory.Mius[0].ConfigurationHistory[0].Config.QuietTime.QuietEndMins);
            Assert.AreEqual(900, configHistory.Mius[0].ConfigurationHistory[0].Config.Recording.IntervalSecs);
            Assert.AreEqual(0, configHistory.Mius[0].ConfigurationHistory[0].Config.Recording.StartTimeSecs);
            Assert.AreEqual(1440, configHistory.Mius[0].ConfigurationHistory[0].Config.Reporting.IntervalMins);
            Assert.AreEqual(3, configHistory.Mius[0].ConfigurationHistory[0].Config.Reporting.NumRetries);
            Assert.AreEqual(360, configHistory.Mius[0].ConfigurationHistory[0].Config.Reporting.StartTimeMins);
            Assert.AreEqual(300, configHistory.Mius[0].ConfigurationHistory[0].Config.Reporting.TransmitWindowLengthMins);


            Assert.AreEqual(1, configHistory.Mius[1].ConfigurationHistory.Count);

        }


        #region TEST_JSON_STRING
        /// <summary>
        /// Json string extracted from ETI 48-02
        /// </summary>
        public static string JSON_CONFIG_HISTORY = @"
{
   ""mius"": [
      {
           ""miu_id"": 5000001,
           ""site_id"": 55555,
           ""distributer_id"": 1,
           ""device_type"": ""CMIU"",
           ""CMIU_type"": 1,
           ""billable"": ""Y"",
           ""configuration_history"": [
              {
                 ""start_date"": ""2015-01-01"",
                 ""end_date"": ""2015-01-08"",
               ""config"": 
               {
                  ""CMIU_type"": 1,
                  ""billing"":
                  {
                     ""site_id"": 5555,
              ""distributer_id"": 1,
                     ""billable"": ""Y""
                  },
                  ""recording"":
                  {
                     ""start_time_secs"": 0,
                     ""interval_secs"": 900
                  },
                  ""reporting"": 
                  {
                     ""start_time_mins"": 360,
                     ""interval_mins"": 1440,
                     ""num_retries"": 3,
                     ""transmit_window_length_mins"": 300
                  },
                  ""quiet_time"": 
                  {
                     ""quiet_start_mins"": 420,
                     ""quiet_end_mins"": 480
                    }
               }
              },
              {
               ""start_date"": ""2015-01-09"",
               ""end_date"": ""2015-01-11"",
               ""config"": 
               {
                  ""CMIU_type"": 1,
                  ""billing"":
                  {
                     ""site_id"": 5555,
              ""distributer_id"": 1,
                     ""billable"": ""Y""
                  },
                  ""recording"":
                  {
                     ""start_time_secs"": 0,
                     ""interval_secs"": 900
                  },
                  ""reporting"": 
                  {
                     ""start_time_mins"": 360,
                     ""interval_mins"": 1440,
                     ""num_retries"": 3,
                     ""transmit_window_length_mins"": 300
                  },
                  ""quiet_time"": 
                  {
                     ""quiet_start_mins"": 420,
                     ""quiet_end_mins"": 480
                    }
               }
            },
            {
               ""start_date"": ""2015-01-12"",
               ""end_date"": """",
               ""config"": 
               {
                  ""CMIU_type"": 1,
                  ""billing"":
                  {
                     ""site_id"": 5555,
               ""distributer_id"": 1,
                      ""billable"": ""Y""
                  },
                  ""recording"":
                  {
                     ""start_time_secs"": 0,
                     ""interval_secs"": 900
                  },
                  ""reporting"": 
                  {
                     ""start_time_mins"": 360,
                     ""interval_mins"": 1440,
                     ""num_retries"": 3,
                     ""transmit_window_length_mins"": 300
                  },
                  ""quiet_time"": 
                  {
                     ""quiet_start_mins"": 420,
                     ""quiet_end_mins"": 480
                    }
               }
            }
         ]
      }
      {
         ""miu_id"": 5000002,
         ""site_id"": 5555,
   ""distributer_id"": 1,
          ""device_type"": ""CMIU"",
         ""billable"": ""Y""
         ""configuration_history"": [
            {
               ""start_date"": ""2015-01-01"",
               ""end_date"": """",
               ""config"": 
               {
                  ""CMIU_type"": 1,
                  ""billing"":
                  {
                     ""site_id"": 5555,
                 ""distributer_id"": 1,
                        ""billable"": ""Y""
                  },
                  ""recording"":
                  {
                     ""start_time_secs"": 0,
                     ""interval_secs"": 900
                  },
                  ""reporting"": 
                  {
                     ""start_time_mins"": 360,
                     ""interval_mins"": 1440,
                     ""num_retries"": 3,
                     ""transmit_window_length_mins"": 300
                  },
                  ""quiet_time"": 
                  {
                     ""quiet_start_mins"": 420,
                     ""quiet_end_mins"": 480
                    }
               }
            }
         ]
      }
   ]
}

";
        #endregion

    }

}