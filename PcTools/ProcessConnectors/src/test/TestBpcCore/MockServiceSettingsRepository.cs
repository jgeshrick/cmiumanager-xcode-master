﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using Ntg.BpcCore.Settings;

namespace Ntg.BpcCore.IntegrationTests
{
    /// <summary>
    /// An in-memory mock implementation of <see cref="IServiceSettingsRepository"/>. The 
    /// <see cref="DataConnectorType"/> supplied in calls to SaveSettings and LoadSettings must 
    /// match the value of the DataServiceType property.
    /// </summary>
    public class MockServiceSettingsRepository : IServiceSettingsRepository
    {
        private readonly Dictionary<DataConnectorType, EnvironmentSettings> mockSettings;

        public MockServiceSettingsRepository()
        {
            mockSettings = new Dictionary<DataConnectorType, EnvironmentSettings>();
        }

        public void SaveSettings(DataConnectorType dataConnectorType, bool isEnabled, DateTime? lastExecutedTime, bool? succeeded,
            DateTime? nextExecutionTime)
        {
            var environmentSettings = new EnvironmentSettings(isEnabled, lastExecutedTime, succeeded, nextExecutionTime);
            mockSettings[dataConnectorType] = environmentSettings;
        }

        public void LoadSettings(DataConnectorType dataConnectorType, out bool isEnabled, out DateTime? lastExecutedTime, out bool? succeeded, out DateTime? nextExecutionTime)
        {
            if (!mockSettings.ContainsKey(dataConnectorType))
            {
                isEnabled = false;
                lastExecutedTime = null;
                succeeded = null;
                nextExecutionTime = null;
            }
            else
            {
                var environmentSettings = mockSettings[dataConnectorType];
                isEnabled = environmentSettings.IsEnabled;
                lastExecutedTime = environmentSettings.LastExecutedTime;
                succeeded = environmentSettings.Succeeded;
                nextExecutionTime = environmentSettings.NextExecutionTime;
            }
        }

        public void SetMockSettings(DataConnectorType dataConnectorType, bool isEnabled, DateTime? lastExecutedTime, bool? succeeded, DateTime? nextExecutionTime)
        {
            var environmentSettings = new EnvironmentSettings(isEnabled, lastExecutedTime, succeeded, nextExecutionTime);
            mockSettings[dataConnectorType] = environmentSettings;
        }

        private struct EnvironmentSettings
        {
            public EnvironmentSettings(bool isEnabled, DateTime? lastExecutedTime, bool? succeeded, DateTime? nextExecutionTime)
            {
                IsEnabled = isEnabled;
                LastExecutedTime = lastExecutedTime;
                Succeeded = succeeded;
                NextExecutionTime = nextExecutionTime;
            }

            public bool IsEnabled;
            public DateTime? LastExecutedTime;
            public bool? Succeeded;
            public DateTime? NextExecutionTime;
        }
    }
}