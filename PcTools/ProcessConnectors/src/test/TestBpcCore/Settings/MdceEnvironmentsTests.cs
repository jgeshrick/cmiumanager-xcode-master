﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using Ntg.BpcCore.IntegrationTests.Common;
using Ntg.BpcCore.Settings;
using NUnit.Framework;

namespace Ntg.BpcCore.IntegrationTests.Settings
{
    [TestFixture()]
    public class MdceEnvironmentsTests
    {
        private readonly string testEnvironmentString = "testenvironments.xml";

        [Test()]
        public void GetSetEnvironmentsFromXmlTest()
        {
            Environments env = new Environments();
            var envList = env.EnvironmentList;
            envList.Add(new MdceEnvironment { Name = "env1", BaseUrl = "https://env1.neptune.com:1234", IsEnabled = true });
            envList.Add(new MdceEnvironment { Name = "env2", BaseUrl = "https://env2.neptune.com:1234", IsEnabled = true });
            envList.Add(new MdceEnvironment { Name = "env3", BaseUrl = "https://env3.neptune.com:1234", IsEnabled = false });

            env.CostAndRevenueServerDataServer = "sql-server1";
            env.CustomerDataServer = "sql-server2";

            env.ScheduleConfigurations.Add(new ScheduleConfiguration
            {
                DataConnectorType = DataConnectorType.CustomerDataConnector, 
                ScheduleType = ScheduleType.Daily
            });
            
            env.ScheduleConfigurations.Add(new ScheduleConfiguration
            {
                DataConnectorType = DataConnectorType.MiuLifecycleDataConnector, 
                ScheduleType = ScheduleType.Monthly
            });
            
            env.ScheduleConfigurations.Add(new ScheduleConfiguration
            {
                DataConnectorType = DataConnectorType.CostRevenueDataConnector, 
                ScheduleType = ScheduleType.Monthly,
                DaysOfMonthAsString = "1, 5, 15",
                StartTimeAsString = "00:00:00",
                RepeatDurationAsString = "08:00:00",
                RepeatIntervalAsString = "00:10:00"
            });

            env.WriteToXml(testEnvironmentString);

            var env2 = Environments.ReadFromXml(testEnvironmentString);

            Assert.AreEqual(env.EnvironmentList.Count, env2.EnvironmentList.Count);

            Assert.AreEqual(env.EnvironmentList[0].Name, env2.EnvironmentList[0].Name);
            Assert.AreEqual(env.EnvironmentList[0].BaseUrl, env2.EnvironmentList[0].BaseUrl);
            Assert.AreEqual(env.EnvironmentList[0].IsEnabled, env2.EnvironmentList[0].IsEnabled);

            Assert.AreEqual(env.EnvironmentList[1].Name, env2.EnvironmentList[1].Name);
            Assert.AreEqual(env.EnvironmentList[1].BaseUrl, env2.EnvironmentList[1].BaseUrl);
            Assert.AreEqual(env.EnvironmentList[1].IsEnabled, env2.EnvironmentList[1].IsEnabled);

            Assert.AreEqual(env.EnvironmentList[2].Name, env2.EnvironmentList[2].Name);
            Assert.AreEqual(env.EnvironmentList[2].BaseUrl, env2.EnvironmentList[2].BaseUrl);
            Assert.AreEqual(env.EnvironmentList[2].IsEnabled, env2.EnvironmentList[2].IsEnabled);

            Assert.AreEqual(env.ScheduleConfigurations[0].DataConnectorType, env2.ScheduleConfigurations[0].DataConnectorType);
            Assert.AreEqual(env.ScheduleConfigurations[0].ScheduleType, env2.ScheduleConfigurations[0].ScheduleType);
            Assert.AreEqual(env.ScheduleConfigurations[0].DaysOfMonth.Count, env2.ScheduleConfigurations[0].DaysOfMonth.Count);
            Assert.AreEqual(env.ScheduleConfigurations[0].StartTimeAsString, env2.ScheduleConfigurations[0].StartTimeAsString);
            Assert.AreEqual(env.ScheduleConfigurations[0].RepeatDurationAsString, env2.ScheduleConfigurations[0].RepeatDurationAsString);
            Assert.AreEqual(env.ScheduleConfigurations[0].RepeatIntervalAsString, env2.ScheduleConfigurations[0].RepeatIntervalAsString);
            for (int i = 0; i < env.ScheduleConfigurations[0].DaysOfMonth.Count; i++)
            {
                Assert.AreEqual(env.ScheduleConfigurations[0].DaysOfMonth[i], env2.ScheduleConfigurations[0].DaysOfMonth[i]);
            }
            
            Assert.AreEqual(env.ScheduleConfigurations[1].DataConnectorType, env2.ScheduleConfigurations[1].DataConnectorType);
            Assert.AreEqual(env.ScheduleConfigurations[1].ScheduleType, env2.ScheduleConfigurations[1].ScheduleType);

            Assert.AreEqual(env.ScheduleConfigurations[2].DataConnectorType, env2.ScheduleConfigurations[2].DataConnectorType);
            Assert.AreEqual(env.ScheduleConfigurations[2].ScheduleType, env2.ScheduleConfigurations[2].ScheduleType);

        }
    }
}
