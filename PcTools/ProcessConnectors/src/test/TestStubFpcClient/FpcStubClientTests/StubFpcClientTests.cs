﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Threading;
using NUnit.Framework;
using NeptuneCloudConnector.Exception;
using FpcClient;
using FpcClient.JsonModel;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.Common.StatusCodes;
using FpcClient.Common.LifecycleStates;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.ConfigurationStructures;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.Common.CmiuCanConfigurationStructures;
using FpcClient.Common.CmiuLifecycleStructures;

using System.Diagnostics;
using System.IO;
using FpcClient.Settings;

namespace FpcClient.StubTests
{
    [TestFixture()]
    public class StubFpcClientTests
    {
        private const string StubTestPartnerId = "eb750ee73b5cf9d04ec78a0b23616e9b";
        private const uint StubTestTestCmiuId = 405754506;
        private const string StubTestImei = "555555555555555";
        private const string StubTestIccid = "55555555555555555555";
        private const string StubTestVendor = "SsSsS";
        private const string StubTestApn = " SSSSsssssSSSSSSS";

        [Test]
        public void InitStubFpcClientTest()
        {
            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient("ValidPartnerId");
            Assert.AreEqual(StatusCode.OK, testCode);

            string assemblyFileLocation = System.Reflection.Assembly.GetExecutingAssembly().Location;
            FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(assemblyFileLocation);
            var mdceEnvironments = MdceEnvironments.ReadFromXml();

            Assert.AreEqual(fpcClient.AssemblyVersion, myFileVersionInfo.FileVersion);
            Assert.AreEqual(fpcClient.Environment, mdceEnvironments.Environment.Name.ToString());
            Assert.AreEqual(fpcClient.MinimumErrorCode, 100);
        }

        [Test]
        public void GetSetStubFpcClientTest()
        {
            CmiuCan config = new CmiuCan();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient("ValidPartnerId");
            Assert.AreEqual(StatusCode.OK, testCode);

            /// Check return value is OK
            config.cmiuId = StubTestTestCmiuId;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = StubTestVendor;
            config.modemCan.imei = StubTestImei;
            config.simCan.iccid = StubTestIccid;
            config.apn = StubTestApn;

            /// Check return value is OK
            var result = fpcClient.SetCmiuCan(config).statusCode;
            Assert.AreEqual(StatusCode.WarningStubNotImplemented, result);
            Assert.AreEqual(config.cmiuId, StubTestTestCmiuId);
            Assert.AreEqual(config.apn, StubTestApn);
            Assert.AreEqual(config.modemCan.imei, StubTestImei);
            Assert.AreEqual(config.modemCan.mno, CellularProviderCode.ATT);
            Assert.AreEqual(config.modemCan.vendor, StubTestVendor);
            Assert.AreEqual(config.simCan.iccid, StubTestIccid);

            result = fpcClient.GetCmiuCan(config.cmiuId, ref config);
            Assert.AreEqual(StatusCode.WarningStubNotImplemented, result);
            Assert.AreEqual(config.cmiuId, StubTestTestCmiuId);
            Assert.AreEqual(config.apn, "NTG.GW10.VZWENTP");
            Assert.AreEqual(config.modemCan.imei, "123456789012345");
            Assert.AreEqual(config.modemCan.mno, CellularProviderCode.VZW);
            Assert.AreEqual(config.modemCan.vendor, "Telit");
            Assert.AreEqual(config.simCan.iccid, "24680246802468024680");
        }


        [Test]
        public void ServerDetailStubFpcClientTest()
        {
            ServerDetails details = new ServerDetails();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient("ValidPartnerId");
            Assert.AreEqual(StatusCode.OK, testCode);

            string assemblyFileLocation = System.Reflection.Assembly.GetExecutingAssembly().Location;
            FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(assemblyFileLocation);
            var mdceEnvironments = MdceEnvironments.ReadFromXml();

            /// Check return value is OK
            var result = fpcClient.GetServerDetails(ref details);
            Assert.AreEqual(StatusCode.WarningStubNotImplemented, result);
            Assert.AreEqual(details.server, mdceEnvironments.Environment.Name);
            Assert.AreEqual(details.version, "1.2.3.4");
        }

        [Test]
        public void LifecycleStubFpcClientTest()
        {
            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient("ValidPartnerId");
            Assert.AreEqual(StatusCode.OK, testCode);

            CmiuLifecycleStruct life = new CmiuLifecycleStruct();

            fpcClient.GetCmiuLifecycle(StubTestTestCmiuId, ref life);

            Assert.AreEqual(life.miuId, StubTestTestCmiuId);
            Assert.AreEqual(life.state.id, "PrePrePot");
            Assert.AreEqual(life.state.timestamp.Date, DateTime.UtcNow.Date);
        }
    }
}
