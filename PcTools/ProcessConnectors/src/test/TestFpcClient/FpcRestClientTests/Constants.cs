﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestFPCClient.FpcRestClientTests
{
    class Constants
    {
        public const string SiteId = "0";
        public const string PartnerId1 = "eb750ee73b5cf9d04ec78a0b23616e9b";
        public const string FpcServerUrl = "https://auto-test.mdce-nonprod.neptunensight.com:8445/fpc";    //for testing with test server
        public const string FpcServerUrlSubstring = "/api/v1/";
    }
}
