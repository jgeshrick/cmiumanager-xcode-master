﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Threading;
using NeptuneCloudConnector.JsonModel;
using NeptuneCloudConnector.Exception;
using NUnit.Framework;
using FpcClient;
using FpcClient.JsonModel;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.JsonModel.ApnInformation;
using FpcClient.Common.StatusCodes;
using FpcClient.Common.LifecycleStates;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.ApnInformationStructures;
using TestFPCClient.FpcRestClientTests;

namespace FpcClient.Tests
{
    [TestFixture()]
    public class FpcRestClientApnInformationTests
    {
        private const string SiteId = Constants.SiteId;
        private const string PartnerId1 = Constants.PartnerId1;
        private const string FpcServerUrl = Constants.FpcServerUrl;   //for testing with test server
        private const string FpcServerUrlSubstring = Constants.FpcServerUrlSubstring;

        /// <summary>
        /// To test Token recycling by calling the RESTful function more than once with the same RestClient.
        /// </summary>
        [Test()]
        public void GetApnStringValidRestClient()
        {
            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newApnInfo = new ApnInformation();

            //Should get a 400 response as you cannot use an empty MNO
            var statusCode = pcei.GetApnInformation(ref newApnInfo);
            Assert.AreEqual(StatusCode.ErrorServerResponseBadRequest, statusCode);

            newApnInfo.Mno = CellularProviderCode.VZW.ToString(); ;
            statusCode = pcei.GetApnInformation(ref newApnInfo);
            Assert.AreEqual(StatusCode.OK, statusCode);
            Assert.AreEqual(newApnInfo.Mno, CellularProviderCode.VZW.ToString()); ;
            string oldApn = newApnInfo.Apn;

            newApnInfo.Mno = CellularProviderCode.ATT.ToString();
            statusCode = pcei.GetApnInformation(ref newApnInfo);
            Assert.AreEqual(StatusCode.OK, statusCode);
            Assert.AreEqual(newApnInfo.Mno, CellularProviderCode.ATT.ToString());
            Assert.AreNotEqual(newApnInfo.Apn, oldApn);
        }
    }
}
