﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using NUnit.Framework;
using FpcClient.JsonModel;
using NeptuneCloudConnector.Exception;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;
using System.Threading;
using FpcClient.Common.CmiuCanConfigurationStructures;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.Common.LifecycleStates;


namespace FpcClient.Tests
{
    [TestFixture()]
    public class FpcRestClientLifecycleTest
    {
        private const string SiteId = "0";
        //Partner key for partner number 2, site 0 (non-production environments)
        private const string PartnerId1 = "eb750ee73b5cf9d04ec78a0b23616e9b";

        private const string FpcServerUrl = "https://auto-test.mdce-nonprod.neptunensight.com:8445/fpc";    //for testing with test server
        //private const string FPC_LOCAL_SERVER_URL = "http://localhost:8081/fpc";    //for testing with local server
        private const string FpcServerUrlSubstring = "/api/v1/";

        private const uint TestVirginId = 477264264;
        private const string TestVendor = "Telit";
        private const string TestApn = "TestApn1";

        private const uint StartTestExistingId = 401230110;
        private const string StartTestImeiValid = "65432109876";//5432;
        private const string StartTestIccidValid = "6543210987654321";//0987;

        private uint TestExistingId = StartTestExistingId;
        private uint TestImeiValid = 5432;
        private uint TestIccidValid = 1986;

        /// <summary>
        /// Set-up CMIU for lifecycle test - needs to be fresh so lifecycle can be set to earlier than Post-Pot and earlier than dead
        /// Set lifecycle to each expected state and try some boundary checks around post-pot
        /// Ensure all results are as expected.
        /// </summary>
        [Test]
        public void SetGetCmiuLifecycleStatesTests()
        {
            uint testAdjustment = 0;
            TestExistingId = StartTestExistingId + testAdjustment;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestExistingId;
            newCellularConfig.Apn = TestApn;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.SimConfig = new SimConfig();
            
            var statusCode = pcei.GetCellularConfig(newCellularConfig.MiuId, ref newCellularConfig);

            for (; ((testAdjustment < 1000)&&(statusCode != StatusCode.ErrorServerResponseNotFound)); testAdjustment++)
            {
                TestExistingId = StartTestExistingId + testAdjustment;
                newCellularConfig.MiuId = TestExistingId;
                statusCode = pcei.GetCellularConfig(newCellularConfig.MiuId, ref newCellularConfig);
            }
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, statusCode);



            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = StartTestImeiValid+TestImeiValid.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();
            newCellularConfig.SimConfig.Iccid = StartTestIccidValid+TestIccidValid.ToString();
            statusCode = pcei.SetCellularConfig(newCellularConfig).statusCode;

            if (StatusCode.ErrorDuplicateImei == statusCode)
            {
                for (int i = 0; ((i < 1000) && (statusCode == StatusCode.ErrorDuplicateImei)); i++)
                {
                    TestImeiValid++;
                    newCellularConfig.ModemConfig.Imei = StartTestImeiValid + TestImeiValid.ToString();
                    statusCode = pcei.SetCellularConfig(newCellularConfig).statusCode;
                }
            }

            if (StatusCode.ErrorDuplicateIccid == statusCode)
            {
                for (int i = 0; ((i < 1000) && (statusCode == StatusCode.ErrorDuplicateIccid)); i++)
                {
                    TestIccidValid += 2;
                    newCellularConfig.SimConfig.Iccid = StartTestIccidValid + TestIccidValid.ToString();
                    statusCode = pcei.SetCellularConfig(newCellularConfig).statusCode;
                }
            }


            Assert.AreEqual(StatusCode.OK, statusCode);

            var checkLifecycleState = new CmiuLifecycleState();
            statusCode = pcei.GetCmiuLifecycleState(TestExistingId, ref checkLifecycleState);
            Assert.AreEqual(StatusCode.OK, statusCode);
            Assert.AreEqual(checkLifecycleState.MiuId, TestExistingId);
            Assert.AreEqual(checkLifecycleState.State.Id, LifecycleState.PreActivated.ToString());

            checkLifecycleState.MiuId = TestExistingId;
            checkLifecycleState.State.Id = "PrePrePot";
            checkLifecycleState.State.TimeStamp = DateTime.UtcNow;
            statusCode = pcei.SetCmiuLifecycleState(checkLifecycleState);
            Assert.AreEqual(StatusCode.OK, statusCode);
            Assert.AreEqual(checkLifecycleState.State.Id, LifecycleState.PrePrePot.ToString());

            checkLifecycleState.MiuId = TestExistingId;
            checkLifecycleState.State.Id = "PrePot";
            checkLifecycleState.State.TimeStamp = DateTime.UtcNow;
            statusCode = pcei.SetCmiuLifecycleState(checkLifecycleState);
            Assert.AreEqual(StatusCode.OK, statusCode);
            Assert.AreEqual(checkLifecycleState.State.Id, LifecycleState.PrePot.ToString());

            checkLifecycleState.MiuId = TestExistingId;
            checkLifecycleState.State.Id = "Claimed";
            checkLifecycleState.State.TimeStamp = DateTime.UtcNow;
            statusCode = pcei.SetCmiuLifecycleState(checkLifecycleState);
            Assert.AreEqual(StatusCode.ErrorInvalidLifecycleState, statusCode);

            checkLifecycleState.MiuId = TestExistingId;
            checkLifecycleState.State.Id = "PostPot";
            checkLifecycleState.State.TimeStamp = DateTime.UtcNow;
            statusCode = pcei.SetCmiuLifecycleState(checkLifecycleState);
            Assert.AreEqual(StatusCode.OK, statusCode);
            Assert.AreEqual(checkLifecycleState.State.Id, LifecycleState.PostPot.ToString());

            checkLifecycleState.MiuId = TestExistingId;
            checkLifecycleState.State.Id = "PrePot";
            checkLifecycleState.State.TimeStamp = DateTime.UtcNow;
            statusCode = pcei.SetCmiuLifecycleState(checkLifecycleState);
            Assert.AreEqual(StatusCode.ErrorInvalidLifecycleState, statusCode);

            checkLifecycleState.MiuId = TestExistingId;
            checkLifecycleState.State.Id = "Claimed";
            checkLifecycleState.State.TimeStamp = DateTime.UtcNow;
            statusCode = pcei.SetCmiuLifecycleState(checkLifecycleState);
            Assert.AreEqual(StatusCode.ErrorInvalidLifecycleState, statusCode);
        }
    }
}
