﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using NUnit.Framework;
using FpcClient.JsonModel;
using NeptuneCloudConnector.Exception;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;
using System.Threading;
using FpcClient.Common.CmiuCanConfigurationStructures;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.Common.LifecycleStates;


namespace FpcClient.Tests
{
    [TestFixture()]
    public class FpcRestClientServerDetailTest
    {
        private const string SiteId = "0";
        private const string PartnerId = "eb750ee73b5cf9d04ec78a0b23616e9b";
        private const string FpcServerUrl = "https://auto-test.mdce-nonprod.neptunensight.com:8445/fpc";    //for testing with test server
        //private const string FPC_LOCAL_SERVER_URL = "http://localhost:8081/fpc";    //for testing with local server
        private const string FpcServerUrlSubstring = "/api/v1/";

        /// <summary>
        /// Create FPC Client, connect to FPC Server, get server information.
        /// Check that the server information matches the expected values.
        /// </summary>
        [Test]
        public void GetFpcServerInformationTest()
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;
            var restClient = new FpcRestClient(PartnerId, SiteId, FpcServerUrl, FpcServerUrlSubstring);
            ServerInformation serverInformation1 = new ServerInformation();
            returnCode = restClient.GetFPCServerDetail(ref serverInformation1);
            Assert.NotNull(serverInformation1.Version);
            Assert.NotNull(serverInformation1.CurrentTimeUtc);
            Assert.NotNull(serverInformation1.Environment);

            ServerInformation serverInformation2 = new ServerInformation();
            returnCode = restClient.GetFPCServerDetail(ref  serverInformation2);

            Assert.NotNull(serverInformation2.Version);
            Assert.NotNull(serverInformation2.CurrentTimeUtc);
            Assert.NotNull(serverInformation2.Environment);

            Assert.AreEqual(serverInformation2.CurrentTimeUtc.DayOfYear, serverInformation2.CurrentTimeUtc.DayOfYear);
            Assert.AreEqual(serverInformation2.CurrentTimeUtc.Hour, serverInformation2.CurrentTimeUtc.Hour);
        }
    }
}
