﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using NUnit.Framework;
using FpcClient.JsonModel;
using NeptuneCloudConnector.Exception;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;
using System.Threading;
using FpcClient.Common.CmiuCanConfigurationStructures;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.Common.LifecycleStates;


namespace FpcClient.Tests
{
    [TestFixture()]
    public class FpcRestClientManufacturingInformationTest
    {
        private const string SiteId = "0";
        private const string PartnerId1 = "eb750ee73b5cf9d04ec78a0b23616e9b";
        private const string FpcServerUrl = "https://auto-test.mdce-nonprod.neptunensight.com:8445/fpc";    //for testing with test server
        //private const string FPC_LOCAL_SERVER_URL = "http://localhost:8081/fpc";    //for testing with local server
        private const string FpcServerUrlSubstring = "/api/v1/";

        private const uint TestVirginId = 464264264;
        private const uint TestExistingId = 400000110;

        /// <summary>
        /// Set manufacturing information, get manufacturing information, compare the two.
        /// Ensure both sets of data match.
        /// </summary>
        [Test]
        public void SetGetManufacturingInformationEmptyTest()
        {
            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //ensure cmiu exists
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestExistingId;
            var myCode = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, myCode.statusCode);

            //set new config
            var newManufacturingInformation = new ManufacturingInformation();
            newManufacturingInformation.MiuId = TestExistingId;

            newManufacturingInformation.Tests = new System.Collections.Generic.List<TestStation>();
            newManufacturingInformation.Rework = new Rework();
            newManufacturingInformation.DeviceDetails = new DeviceDetails();
            newManufacturingInformation.DeviceDetails.Images = new Images();
            newManufacturingInformation.DeviceDetails.Modem = new Modem();
            newManufacturingInformation.DeviceDetails.Bluetooth = new Bluetooth();
            newManufacturingInformation.DeviceDetails.LastTestTimestamp = DateTime.UtcNow;
            var newTest = new TestStation();
            newTest.StationInfo = new StationInfo();
            newTest.TestTimestamp = DateTimeOffset.Now;
            newTest.Log = new System.Collections.Generic.List<string>();
            newManufacturingInformation.Tests.Add(newTest);

            var result = pcei.SetManufacturingInformation(newManufacturingInformation);
            Assert.AreEqual(StatusCode.OK, result);

            ManufacturingInformation checkManufacturingInformation = new ManufacturingInformation();
            var statusCode = pcei.GetManufacturingInformation(TestExistingId, ref checkManufacturingInformation);
            Assert.AreEqual(checkManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Date, newManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Date);
            Assert.AreEqual(checkManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Hour, newManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Hour);
            Assert.AreEqual(checkManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Minute, newManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Minute);
            Assert.AreEqual(checkManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Second, newManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Second);
            Assert.AreEqual(checkManufacturingInformation.MiuId, newManufacturingInformation.MiuId);
        }

        /// <summary>
        /// Set manufacturing information, get manufacturing information, compare the two.
        /// Ensure both sets of data match.
        /// </summary>
        [Test]
        public void SetGetManufacturingInformationFullTest()
        {
            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);


            //ensure cmiu exists
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestExistingId;
            var myCode = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, myCode.statusCode);


            //set new config
            var newManufacturingInformation = new ManufacturingInformation();
            newManufacturingInformation.MiuId = TestExistingId;

            newManufacturingInformation.Tests = new System.Collections.Generic.List<TestStation>();
            newManufacturingInformation.Rework = new Rework();
            newManufacturingInformation.Rework.Count = 35;
            newManufacturingInformation.DeviceDetails = new DeviceDetails();
            newManufacturingInformation.DeviceDetails.Images = new Images();
            newManufacturingInformation.DeviceDetails.Images.CmiuApplication = "CmiuApplication";
            newManufacturingInformation.DeviceDetails.Images.CmiuBootloader = "CmiuBootloader";
            newManufacturingInformation.DeviceDetails.Images.CmiuConfiguration = "CmiuConfiguration";
            newManufacturingInformation.DeviceDetails.Images.ModemLibrary = "ModemLibrary";
            newManufacturingInformation.DeviceDetails.Modem = new Modem();
            newManufacturingInformation.DeviceDetails.Modem.FirmwareRevision = "FirmwareRevision";
            newManufacturingInformation.DeviceDetails.Bluetooth = new Bluetooth();
            newManufacturingInformation.DeviceDetails.Bluetooth.MacAddress = "MacAddress";
            newManufacturingInformation.DeviceDetails.LastTestTimestamp = DateTime.UtcNow;
            newManufacturingInformation.DeviceDetails.ActivationTimestamp = DateTime.UtcNow;
            newManufacturingInformation.DeviceDetails.CreationTimestamp = DateTime.UtcNow;
            newManufacturingInformation.DeviceDetails.FirstTestTimestamp = DateTime.UtcNow;
            newManufacturingInformation.DeviceDetails.RegistrationTimestamp = DateTime.UtcNow;
            var newTest = new TestStation();
            newTest.StationInfo = new StationInfo();
            newTest.TestTimestamp = DateTimeOffset.Now;
            newTest.Log = new System.Collections.Generic.List<string>();
            newManufacturingInformation.Tests.Add(newTest);

            var result = pcei.SetManufacturingInformation(newManufacturingInformation);
            Assert.AreEqual(StatusCode.OK, result);

            ManufacturingInformation checkManufacturingInformation = new ManufacturingInformation();
            var statusCode = pcei.GetManufacturingInformation(TestExistingId, ref checkManufacturingInformation);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Date, checkManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Date);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Hour, checkManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Hour);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Minute, checkManufacturingInformation.DeviceDetails.LastTestTimestamp.UtcDateTime.Minute);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.RegistrationTimestamp.UtcDateTime.Second, checkManufacturingInformation.DeviceDetails.RegistrationTimestamp.UtcDateTime.Second);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.ActivationTimestamp.UtcDateTime.Date, checkManufacturingInformation.DeviceDetails.ActivationTimestamp.UtcDateTime.Date);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.CreationTimestamp.UtcDateTime.Hour, checkManufacturingInformation.DeviceDetails.CreationTimestamp.UtcDateTime.Hour);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.CreationTimestamp.UtcDateTime.Minute, checkManufacturingInformation.DeviceDetails.CreationTimestamp.UtcDateTime.Minute);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.FirstTestTimestamp.UtcDateTime.Second, checkManufacturingInformation.DeviceDetails.FirstTestTimestamp.UtcDateTime.Second);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.Images.CmiuApplication, checkManufacturingInformation.DeviceDetails.Images.CmiuApplication);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.Images.CmiuBootloader, checkManufacturingInformation.DeviceDetails.Images.CmiuBootloader);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.Images.CmiuConfiguration, checkManufacturingInformation.DeviceDetails.Images.CmiuConfiguration);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.Images.ModemLibrary, checkManufacturingInformation.DeviceDetails.Images.ModemLibrary);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.Modem.FirmwareRevision, checkManufacturingInformation.DeviceDetails.Modem.FirmwareRevision);
            Assert.AreEqual(newManufacturingInformation.DeviceDetails.Bluetooth.MacAddress, checkManufacturingInformation.DeviceDetails.Bluetooth.MacAddress);
            Assert.AreEqual(newManufacturingInformation.Rework.Count, checkManufacturingInformation.Rework.Count);
            Assert.AreEqual(newManufacturingInformation.MiuId, newManufacturingInformation.MiuId);
        }

        /// <summary>
        /// Set Manufacturing Information (IF11)
        /// This web service will be used by the NeptuneCloudConnector to store manufacturing information in the Fpc database.
        /// </summary>
        [Test()]
        public void SetManufacturingInformationTest()
        {
            ManufacturingInformation manuInfo = new ManufacturingInformation();

            manuInfo.MiuId = TestExistingId;

            var restClient = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //ensure cmiu exists
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestExistingId;
            var myCode = restClient.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, myCode.statusCode);

            var response = restClient.SetManufacturingInformation(manuInfo);

            Assert.AreEqual(StatusCode.OK, response);
        }

        /// <summary>
        /// Attempt to Set manufacturing information and get manufacturing information with 
        /// CMIU that has not had CAN set
        /// </summary>
        [Test]
        public void SetGetManufacturingInformationVirginCmiuTest()
        {
            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newManufacturingInformation = new ManufacturingInformation();
            newManufacturingInformation.MiuId = TestVirginId;

            newManufacturingInformation.Tests = new System.Collections.Generic.List<TestStation>();
            newManufacturingInformation.Rework = new Rework();
            newManufacturingInformation.DeviceDetails = new DeviceDetails();
            newManufacturingInformation.DeviceDetails.Images = new Images();
            newManufacturingInformation.DeviceDetails.Modem = new Modem();
            newManufacturingInformation.DeviceDetails.Bluetooth = new Bluetooth();
            newManufacturingInformation.DeviceDetails.LastTestTimestamp = DateTime.UtcNow;
            var newTest = new TestStation();
            newTest.StationInfo = new StationInfo();
            newTest.TestTimestamp = DateTimeOffset.Now;
            newTest.Log = new System.Collections.Generic.List<string>();
            newManufacturingInformation.Tests.Add(newTest);

            var result = pcei.SetManufacturingInformation(newManufacturingInformation);
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, result);

            ManufacturingInformation checkManufacturingInformation = new ManufacturingInformation();
            var statusCode = pcei.GetManufacturingInformation(TestVirginId, ref checkManufacturingInformation);
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, result);
        }
    }
}
