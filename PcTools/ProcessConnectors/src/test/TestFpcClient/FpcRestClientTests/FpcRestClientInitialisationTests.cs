﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using NUnit.Framework;
using FpcClient.JsonModel;
using NeptuneCloudConnector.Exception;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;
using System.Threading;
using FpcClient.Common.CmiuCanConfigurationStructures;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.Common.LifecycleStates;
using NeptuneCloudConnector.JsonModel;


namespace FpcClient.Tests
{
    [TestFixture()]
    public class FpcRestClientInitialisationTest
    {
        private const string SiteId = "0";
        private const string PartnerId1 = "eb750ee73b5cf9d04ec78a0b23616e9b";
        private const string FpcServerUrl = "https://auto-test.mdce-nonprod.neptunensight.com:8445/fpc";    //for testing with test server
        private const string FpcServerUrlSubstring = "/api/v1/";

        /// <summary>
        /// To test Token recycling by calling the RESTful function more than once with the same RestClient.
        /// </summary>
        [Test()]
        public void GetAuthenticationTokenTest()
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;
            var restClient = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            ServerInformation serverInformation = new ServerInformation();
            returnCode = restClient.GetFPCServerDetail(ref serverInformation);
            Assert.AreEqual(StatusCode.OK, returnCode);

            Token token1 = new Token();
            returnCode = restClient.GetAuthenticationToken(ref token1);
            Assert.AreEqual(StatusCode.OK, returnCode);
            returnCode = restClient.GetFPCServerDetail(ref serverInformation);
            Assert.AreEqual(StatusCode.OK, returnCode);
            Assert.NotNull(serverInformation.Version);

            Token token2 = new Token();
            returnCode = restClient.GetAuthenticationToken(ref token2);
            Assert.AreEqual(StatusCode.OK, returnCode);
            returnCode = restClient.GetFPCServerDetail(ref serverInformation);
            Assert.AreEqual(StatusCode.OK, returnCode);
            Assert.NotNull(serverInformation.Version);

            Assert.AreEqual(token2.AuthToken, token1.AuthToken);
            Assert.AreEqual(token2.Expires, token1.Expires);
        }
    }
}
