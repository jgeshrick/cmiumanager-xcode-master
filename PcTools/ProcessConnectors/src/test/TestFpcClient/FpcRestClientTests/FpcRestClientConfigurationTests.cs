﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using NUnit.Framework;
using FpcClient.JsonModel;
using NeptuneCloudConnector.Exception;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;
using System.Threading;
using FpcClient.Common.CmiuCanConfigurationStructures;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.Common.LifecycleStates;


namespace FpcClient.Tests
{
    [TestFixture()]
    public class FpcRestClientConfigurationTest
    {

        private const string SiteId = "0";
        private const string PartnerId1 = "eb750ee73b5cf9d04ec78a0b23616e9b";
        private const string FpcServerUrl = "https://auto-test.mdce-nonprod.neptunensight.com:8445/fpc";    //for testing with test server
        //private const string FPC_LOCAL_SERVER_URL = "http://localhost:8081/fpc";    //for testing with local server
        private const string FpcServerUrlSubstring = "/api/v1/";

        private const uint TestCmiuId = 400566780;
        private const uint TestCmiuIdB = 402566780;


        private const uint TestCmiuIdValidMin = 400000000;
        private const uint TestCmiuIdValidMid1Vzw = 452411500;
        private const uint TestCmiuIdValidMid2Vzw = 452422500;
        private const uint TestCmiuIdValidMaxVzw = 499999998;
        private const uint TestCmiuIdValidMid1Att = 553115400;
        private const uint TestCmiuIdValidMid2Att = 553225400;
        private const uint TestCmiuIdValidMaxAtt = 599999998;
        private const uint TestCmiuIdTooLow = 399999998;
        private const uint TestCmiuIdTooHigh = 600000000;
        private const uint TestCmiuIdOdd = 424682467;
        private const uint TestCmiuIdVirgin = 497550320;


        /// <summary>
        /// IMEI should be 15 characters.
        /// </summary>
        private const string TestImeiValidShort1V = "12545676901";
        private const string TestImeiValidShort1A = "12645677901";
        private const string TestImeiValidShort2V = "36201226343";
        private const string TestImeiValidShort2A = "34643437343";
        private const string TestImeiValidShort3V = "78538376978";
        private const string TestImeiValidShort3A = "72618177978";
        private const string TestImeiValidShort4V = "12333676901";
        private const string TestImeiValidShort4A = "12444677901";
        private const string TestImeiValidShort5V = "36333226343";
        private const string TestImeiValidShort5A = "34444437343";
        private const string TestImeiValidShort6V = "78333376978";
        private const string TestImeiValidShort7A = "72444177978";
        private const string TestImeiLong = "1234567890123456";
        private const string TestImeiShort = "12345678901234";
        private const string TestImeiAlpha = "123456789O12345";


        /// <summary>
        /// ICCID should be 20 characters.
        /// </summary>
        private const string TestIccidValidShort1A = "2465024681246802";
        private const string TestIccidValidShort1V = "2466024682246802";
        private const string TestIccidValidShort2A = "1355913579135791";
        private const string TestIccidValidShort2V = "1356703579135791";
        private const string TestIccidValidShort3A = "1015103450101010";
        private const string TestIccidValidShort3V = "1016106780101010";
        private const string TestIccidValidShort4A = "2465554681246802";
        private const string TestIccidValidShort4V = "2466664682246802";
        private const string TestIccidValidShort5A = "1355553579135791";
        private const string TestIccidValidShort5V = "1356663579135791";
        private const string TestIccidValidShort6A = "1015553450101010";
        private const string TestIccidValidShort6V = "1016666780101010";
        private const string TestIccidLong = "123456789012345678901";
        private const string TestIccidShort = "1234567890123456789";
        private const string TestIccidAlpha = "1234567890I234567890";

        private const string TestVendor = "Telit";

        private const string TestApn1 = "TestApn1";
        private const string TestApn2 = "TestApn2";


        /// <summary>
        /// Check functionality of SetCellularConfig and GetCellularConfig
        /// Test setting a cellular config and verify by reading it back.
        /// </summary>
        [Test()]
        public void SetGetCellularConfigTest()
        {
            uint testAdjustment = 1500;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1A + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1A + testAdjustment.ToString(); ;

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuId + testAdjustment, ref newCellularConfigReadback);

            Assert.AreEqual(newCellularConfig.MiuId, newCellularConfigReadback.MiuId);
            Assert.AreEqual(newCellularConfig.ModemConfig.Vendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(newCellularConfig.ModemConfig.Imei, newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(newCellularConfig.ModemConfig.Mno, newCellularConfigReadback.ModemConfig.Mno);
            Assert.AreEqual(newCellularConfig.SimConfig.Iccid, newCellularConfigReadback.SimConfig.Iccid);
            Assert.AreEqual(newCellularConfig.Apn, newCellularConfigReadback.Apn);
        }



        /// <summary>
        /// Check functionality of SetCellularConfig and GetCellularConfig with invalid Partner Id
        /// </summary>
        [Test]
        public void SetGetCellularConfigBadPartnerIdTest()
        {
            uint testAdjustment = 1502;

            var pcei = new FpcRestClient("PARTNER_ID_1", SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn2;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1A + testAdjustment.ToString(); ;
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1A + testAdjustment.ToString(); ;

            // Expect 400 Bad Request due to invalid partner string
            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorServerResponseBadRequest, result.statusCode);

            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuId + testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(StatusCode.ErrorServerResponseBadRequest, statusCode);
        }


        /// <summary>
        /// Check functionality of SetCellularConfig and GetCellularConfig with invalid Site Id
        /// </summary>
        [Test]
        public void SetGetCellularConfigBadSiteIdTest()
        {
            uint testAdjustment = 1506;

            var pcei = new FpcRestClient(PartnerId1, "SITE_ID", FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1V + testAdjustment.ToString(); ;
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1V + testAdjustment.ToString(); ;

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuId + testAdjustment, ref newCellularConfigReadback);

            Assert.AreEqual(newCellularConfig.MiuId, newCellularConfigReadback.MiuId);
            Assert.AreEqual(newCellularConfig.ModemConfig.Vendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(newCellularConfig.ModemConfig.Imei, newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(newCellularConfig.ModemConfig.Mno, newCellularConfigReadback.ModemConfig.Mno);
            Assert.AreEqual(newCellularConfig.SimConfig.Iccid, newCellularConfigReadback.SimConfig.Iccid);
            Assert.AreEqual(newCellularConfig.Apn, newCellularConfigReadback.Apn);
        }


        /// <summary>
        /// Check functionality of SetCellularConfig and GetCellularConfig with invalid server url
        /// </summary>
        [Test]
        public void SetGetCellularConfigBadServerUrlTest()
        {
            uint testAdjustment = 1508;

            var pcei = new FpcRestClient(PartnerId1, SiteId, "FPC_SERVER_URL", FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn2;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1A + testAdjustment.ToString(); ;
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1A + testAdjustment.ToString(); ;

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidUri, result.statusCode);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuId + testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(StatusCode.ErrorInvalidUri, statusCode);
        }


        /// <summary>
        /// Check functionality of SetCellularConfig and GetCellularConfig with invalid server url substring
        /// </summary>
        [Test]
        public void SetGetCellularConfigBadServerUrlSubStringTest()
        {
            uint testAdjustment = 1510;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, "FPC_SERVER_URL_SUBSTRING");

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1A + testAdjustment.ToString(); ;
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1A + testAdjustment.ToString(); ;

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, result.statusCode);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuId + testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, result.statusCode);
        }

        /// <summary>
        /// Check functionality of SetCellularConfig and GetCellularConfig in detail
        /// Check that each set is correct
        /// </summary>
        [Test]
        public void SetGetCellularConfigDetailTest()
        {
            uint testAdjustment = 2512;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuIdValidMin + testAdjustment;
            newCellularConfig.Apn = TestApn2;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1V + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();
            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1V + testAdjustment.ToString();
            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuIdValidMin + testAdjustment, ref newCellularConfigReadback);

            Assert.AreEqual(newCellularConfig.MiuId, newCellularConfigReadback.MiuId);
            Assert.AreEqual(newCellularConfig.ModemConfig.Vendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(newCellularConfig.ModemConfig.Imei, newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(newCellularConfig.ModemConfig.Mno, newCellularConfigReadback.ModemConfig.Mno);
            Assert.AreEqual(newCellularConfig.SimConfig.Iccid, newCellularConfigReadback.SimConfig.Iccid);
            Assert.AreEqual(newCellularConfig.Apn, newCellularConfigReadback.Apn);

            newCellularConfig.MiuId = TestCmiuIdValidMid1Vzw + testAdjustment;
            newCellularConfig.Apn = TestApn1;
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort2V + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort2V + testAdjustment.ToString();
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            newCellularConfig.MiuId = TestCmiuIdValidMid1Att + testAdjustment;
            newCellularConfig.Apn = TestApn1;
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort2A + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort2A + testAdjustment.ToString();
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            newCellularConfig.MiuId = TestCmiuIdValidMaxVzw - testAdjustment;
            newCellularConfig.Apn = TestApn2;
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort3V + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.ATT.ToString();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort3V + testAdjustment.ToString();
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            newCellularConfig.MiuId = TestCmiuIdValidMaxAtt - testAdjustment;
            newCellularConfig.Apn = TestApn2;
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort3A + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.ATT.ToString();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort3A + testAdjustment.ToString();
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);


            //read back and verify setting has been successful
            statusCode = pcei.GetCellularConfig(TestCmiuIdValidMid1Att + testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(TestCmiuIdValidMid1Att + testAdjustment, newCellularConfigReadback.MiuId);
            Assert.AreEqual(TestVendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(TestImeiValidShort2A + testAdjustment.ToString(), newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(CellularProviderCode.VZW.ToString(), newCellularConfigReadback.ModemConfig.Mno);
            Assert.AreEqual(TestIccidValidShort2A + testAdjustment.ToString(), newCellularConfigReadback.SimConfig.Iccid);
            Assert.AreEqual(TestApn1, newCellularConfigReadback.Apn);

            statusCode = pcei.GetCellularConfig(TestCmiuIdValidMid1Vzw + testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(TestCmiuIdValidMid1Vzw + testAdjustment, newCellularConfigReadback.MiuId);
            Assert.AreEqual(TestVendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(TestImeiValidShort2V + testAdjustment.ToString(), newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(CellularProviderCode.VZW.ToString(), newCellularConfigReadback.ModemConfig.Mno);
            Assert.AreEqual(TestIccidValidShort2V + testAdjustment.ToString(), newCellularConfigReadback.SimConfig.Iccid);
            Assert.AreEqual(TestApn1, newCellularConfigReadback.Apn);

            statusCode = pcei.GetCellularConfig(TestCmiuIdValidMaxVzw - testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(TestCmiuIdValidMaxVzw - testAdjustment, newCellularConfigReadback.MiuId);
            Assert.AreEqual(TestVendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(TestImeiValidShort3V + testAdjustment.ToString(), newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(CellularProviderCode.ATT.ToString(), newCellularConfigReadback.ModemConfig.Mno);
            Assert.AreEqual(TestIccidValidShort3V + testAdjustment.ToString(), newCellularConfigReadback.SimConfig.Iccid);
            Assert.AreEqual(TestApn2, newCellularConfigReadback.Apn);

            statusCode = pcei.GetCellularConfig(TestCmiuIdValidMaxAtt - testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(TestCmiuIdValidMaxAtt - testAdjustment, newCellularConfigReadback.MiuId);
            Assert.AreEqual(TestVendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(TestImeiValidShort3A + testAdjustment.ToString(), newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(CellularProviderCode.ATT.ToString(), newCellularConfigReadback.ModemConfig.Mno);
            Assert.AreEqual(TestIccidValidShort3A + testAdjustment.ToString(), newCellularConfigReadback.SimConfig.Iccid);
            Assert.AreEqual(TestApn2, newCellularConfigReadback.Apn);

            statusCode = pcei.GetCellularConfig(TestCmiuIdValidMin + testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(TestCmiuIdValidMin + testAdjustment, newCellularConfigReadback.MiuId);
            Assert.AreEqual(TestVendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(TestImeiValidShort1V + testAdjustment.ToString(), newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(CellularProviderCode.VZW.ToString(), newCellularConfigReadback.ModemConfig.Mno);
            Assert.AreEqual(TestIccidValidShort1V + testAdjustment.ToString(), newCellularConfigReadback.SimConfig.Iccid);
            Assert.AreEqual(TestApn2, newCellularConfigReadback.Apn);
        }

        /// <summary>
        /// Check functionality of SetCellularConfig and GetCellularConfig with null CellularConfig
        /// </summary>
        [Test]
        public void SetGetCellularConfigNullCellularConfigTest()
        {
            uint testAdjustment = 1514;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuId + testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(StatusCode.OK, result.statusCode);
            Assert.AreEqual(newCellularConfig.MiuId, newCellularConfigReadback.MiuId);
        }


        /// <summary>
        /// Check functionality of SetCellularConfig and GetCellularConfig with null ModemConfig
        /// </summary>
        [Test]
        public void SetGetCellularConfigNullModemConfigTest()
        {
            uint testAdjustment = 1516;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1A + testAdjustment.ToString();

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuId + testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            Assert.AreEqual(newCellularConfig.MiuId, newCellularConfigReadback.MiuId);
            Assert.AreEqual(newCellularConfig.Apn, newCellularConfigReadback.Apn);
            Assert.AreEqual(newCellularConfig.SimConfig.Iccid, newCellularConfigReadback.SimConfig.Iccid);
        }

        /// <summary>
        /// Check functionality of SetCellularConfig and GetCellularConfig with null SimConfig
        /// </summary>
        [Test]
        public void SetGetCellularConfigNullSimConfigTest()
        {
            uint testAdjustment = 1518;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn2;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1V + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuId + testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            Assert.AreEqual(newCellularConfig.MiuId, newCellularConfigReadback.MiuId);
            Assert.AreEqual(newCellularConfig.Apn, newCellularConfigReadback.Apn);
            Assert.AreEqual(newCellularConfig.ModemConfig.Vendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(newCellularConfig.ModemConfig.Imei, newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(newCellularConfig.ModemConfig.Mno, newCellularConfigReadback.ModemConfig.Mno);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with invaid cellular provider.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCellularConfigFailInvalidProviderTest()
        {
            uint testAdjustment = 1520;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1V + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = "invalid";

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1V + testAdjustment.ToString();

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidProvider, result.statusCode);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with invaid textual IMEI.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCellularConfigFailTextImeiTest()
        {
            uint testAdjustment = 1522;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            ///Set config with invalid IMEI
            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = "TEST_IMEI_VALID_3";
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort3V + testAdjustment.ToString();

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, result.statusCode);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with invaid textual ICCID.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCellularConfigFailTextIccidTest()
        {
            uint testAdjustment = 1524;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            ///Set config with invalid IMEI
            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort3V + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.ATT.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = "TEST_ICCID_VALID_3";

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, result.statusCode);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with IMEI that is too long.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCellularConfigLongImeiTest()
        {
            uint testAdjustment = 1526;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            ///Set config with invalid IMEI
            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiLong;
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort3A + testAdjustment.ToString();

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, result.statusCode);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with ICCID that is too long.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCellularConfigLongIccidTest()
        {
            uint testAdjustment = 1528;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            ///Set config with invalid IMEI
            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort3V + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.ATT.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidLong;

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, result.statusCode);
        }


        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with IMEI that is too long.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCellularConfigShortImeiTest()
        {
            uint testAdjustment = 1530;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            ///Set config with invalid IMEI
            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiShort;
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort3V + testAdjustment.ToString();

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, result.statusCode);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with ICCID that is too long.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCellularConfigShortIccidTest()
        {
            uint testAdjustment = 1532;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            ///Set config with invalid IMEI
            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort3A + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.ATT.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidShort;

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, result.statusCode);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set three sets of CMIU CAn data.
        /// Check that each set is correct
        /// </summary>
        [Test]
        public void SetGetCellularConfigOutOfRangeCmiuIdTest()
        {
            uint testAdjustment = 1534;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1V + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1V + testAdjustment.ToString();

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuId + testAdjustment, ref newCellularConfigReadback);

            Assert.AreEqual(newCellularConfig.MiuId, newCellularConfigReadback.MiuId);
            Assert.AreEqual(newCellularConfig.ModemConfig.Vendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(newCellularConfig.ModemConfig.Imei, newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(newCellularConfig.ModemConfig.Mno, newCellularConfigReadback.ModemConfig.Mno);
            Assert.AreEqual(newCellularConfig.SimConfig.Iccid, newCellularConfigReadback.SimConfig.Iccid);
            Assert.AreEqual(newCellularConfig.Apn, newCellularConfigReadback.Apn);


            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            newCellularConfig.MiuId = TestCmiuIdValidMin + testAdjustment;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1A + testAdjustment.ToString();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1A + testAdjustment.ToString();
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            newCellularConfig.MiuId = TestCmiuIdTooLow;
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidCmiuId, result.statusCode);

            newCellularConfig.MiuId = TestCmiuIdValidMaxAtt - testAdjustment;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort2A + testAdjustment.ToString();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort2A + testAdjustment.ToString();
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            newCellularConfig.MiuId = TestCmiuIdValidMaxVzw - testAdjustment;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort2V + testAdjustment.ToString();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort2V + testAdjustment.ToString();
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);
            
            newCellularConfig.MiuId = TestCmiuIdTooHigh;
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidCmiuId, result.statusCode);

            newCellularConfig.MiuId = TestCmiuIdValidMid1Vzw;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort3V + testAdjustment.ToString();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort3V + testAdjustment.ToString();
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            newCellularConfig.MiuId = TestCmiuIdValidMid1Att + testAdjustment;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort3A + testAdjustment.ToString();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort3A + testAdjustment.ToString();
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            newCellularConfig.MiuId = TestCmiuIdOdd;
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidCmiuId, result.statusCode);
        }

        /// <summary>
        /// Check functionality of rangeTestCmiuCan
        /// </summary>
        [Test]
        public void RangeTestCellularConfigTest()
        {
            uint testAdjustment = 1648;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn2;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1V + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1V + testAdjustment.ToString();

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuId + testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            Assert.AreEqual(newCellularConfig.MiuId, newCellularConfigReadback.MiuId);
            Assert.AreEqual(newCellularConfig.ModemConfig.Vendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(newCellularConfig.ModemConfig.Imei, newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(newCellularConfig.ModemConfig.Mno, newCellularConfigReadback.ModemConfig.Mno);
            Assert.AreEqual(newCellularConfig.SimConfig.Iccid, newCellularConfigReadback.SimConfig.Iccid);
            Assert.AreEqual(newCellularConfig.Apn, newCellularConfigReadback.Apn);

            // Test fail null CellularConfig;
            //set new config
            var errorPartialSimConfig = new CellularConfig();
            result = pcei.SetCellularConfig(errorPartialSimConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidCmiuId, result.statusCode);

            // Test OK - as CMIUID is set but rest is null
            errorPartialSimConfig.MiuId = TestCmiuIdValidMid1Vzw + testAdjustment;
            result = pcei.SetCellularConfig(errorPartialSimConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            // Test OK - as CMIUID is set but rest is null
            errorPartialSimConfig.MiuId = TestCmiuIdValidMid1Att + testAdjustment;
            result = pcei.SetCellularConfig(errorPartialSimConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            // Test OK - SimConfig defined
            errorPartialSimConfig.SimConfig = new SimConfig();
            result = pcei.SetCellularConfig(errorPartialSimConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            // Test OK - Modem Config defined
            //set new config
            var errorPartialModemConfig = new CellularConfig();
            result = pcei.SetCellularConfig(errorPartialModemConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidCmiuId, result.statusCode);

            // Test OK - as CMIUID is set but rest is null
            errorPartialModemConfig.MiuId = TestCmiuIdValidMid1Att + testAdjustment;
            result = pcei.SetCellularConfig(errorPartialModemConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            // Test OK - as CMIUID is set but rest is null
            errorPartialModemConfig.MiuId = TestCmiuIdValidMid1Vzw + testAdjustment;
            result = pcei.SetCellularConfig(errorPartialModemConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            errorPartialModemConfig.ModemConfig = new ModemConfig();
            result = pcei.SetCellularConfig(errorPartialModemConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            // Test OK - All Config defined
            errorPartialModemConfig.SimConfig = new SimConfig();
            result = pcei.SetCellularConfig(errorPartialModemConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);


            // Test OK - Iccid
            var errorConfig = new CellularConfig();
            errorConfig.MiuId = TestCmiuIdValidMid2Att + testAdjustment;
            newCellularConfig.Apn = TestApn1;
            errorConfig.SimConfig = new SimConfig();
            errorConfig.ModemConfig = new ModemConfig();
            errorConfig.SimConfig.Iccid = TestIccidValidShort2A + testAdjustment.ToString();
            errorConfig.ModemConfig.Imei = TestImeiValidShort2A + testAdjustment.ToString();
            result = pcei.SetCellularConfig(errorConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            // Test OK - Iccid
            errorConfig = new CellularConfig();
            errorConfig.MiuId = TestCmiuIdValidMid2Vzw + testAdjustment;
            errorConfig.Apn = TestApn1;
            errorConfig.SimConfig = new SimConfig();
            errorConfig.ModemConfig = new ModemConfig();
            errorConfig.SimConfig.Iccid = TestIccidValidShort4V + testAdjustment.ToString();
            errorConfig.ModemConfig.Imei = TestImeiValidShort4V + testAdjustment.ToString();
            result = pcei.SetCellularConfig(errorConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            // Test Fail - Alpha Iccid
            errorConfig.SimConfig.Iccid = TestIccidAlpha;
            result = pcei.SetCellularConfig(errorConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, result.statusCode);

            // Test Fail - Long Iccid
            errorConfig.SimConfig.Iccid = TestIccidLong;
            result = pcei.SetCellularConfig(errorConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, result.statusCode);

            // Test Fail - Short Iccid
            errorConfig.SimConfig.Iccid = TestIccidShort;
            result = pcei.SetCellularConfig(errorConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, result.statusCode);

            // Test OK - Imei
            errorConfig.SimConfig.Iccid = TestIccidValidShort5A + testAdjustment.ToString();
            errorConfig.ModemConfig.Imei = TestImeiValidShort5A + testAdjustment.ToString();
            result = pcei.SetCellularConfig(errorConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            // Test Fail - Alpha Imei
            errorConfig.ModemConfig.Imei = TestImeiAlpha;
            result = pcei.SetCellularConfig(errorConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, result.statusCode);

            // Test Fail - Long Imei
            errorConfig.ModemConfig.Imei = TestImeiLong;
            result = pcei.SetCellularConfig(errorConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, result.statusCode);

            // Test Fail - Short Imei
            errorConfig.ModemConfig.Imei = TestImeiShort;
            result = pcei.SetCellularConfig(errorConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, result.statusCode);

            // Test OK - Provider
            errorConfig.SimConfig.Iccid = TestIccidValidShort5V + testAdjustment.ToString();
            errorConfig.ModemConfig.Imei = TestImeiValidShort5V + testAdjustment.ToString();
            result = pcei.SetCellularConfig(errorConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            // Test OK - Valid Provider
            errorConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();
            result = pcei.SetCellularConfig(errorConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            // Test OK - Valid Provider
            errorConfig.ModemConfig.Mno = CellularProviderCode.ATT.ToString();
            result = pcei.SetCellularConfig(errorConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);
        }


        /// <summary>
        /// Check functionality of SetCellularConfig and GetCellularConfig
        /// Test setting a cellular config and verify by reading it back.
        /// </summary>
        [Test()]
        public void SetGetCellularConfigVirginCmiuTest()
        {
            uint testAdjustment = 1500;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuIdVirgin, ref newCellularConfigReadback);
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, statusCode);
        }


        /// <summary>
        /// Check functionality of SetCellularConfig and GetCellularConfig
        /// Test setting an ICCID/CMIUID pair then reusing the ICCID on another CMIUID
        /// </summary>
        [Test()]
        public void SetGetCellularConfigDuplicateIccidTest()
        {
            uint testAdjustment = 3500;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1A + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1A + testAdjustment.ToString(); ;

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuId + testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            Assert.AreEqual(newCellularConfig.MiuId, newCellularConfigReadback.MiuId);
            Assert.AreEqual(newCellularConfig.ModemConfig.Vendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(newCellularConfig.ModemConfig.Imei, newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(newCellularConfig.ModemConfig.Mno, newCellularConfigReadback.ModemConfig.Mno);
            Assert.AreEqual(newCellularConfig.SimConfig.Iccid, newCellularConfigReadback.SimConfig.Iccid);
            Assert.AreEqual(newCellularConfig.Apn, newCellularConfigReadback.Apn);

            //leave with duplicate ICCID to CMIUID
            newCellularConfig.MiuId = TestCmiuIdB + testAdjustment;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort2A + testAdjustment.ToString();
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorDuplicateIccid, result.statusCode);
        }

        /// <summary>
        /// Check functionality of SetCellularConfig and GetCellularConfig
        /// Test setting an IMEI/CMIUID pair then reusing the IMEI on another CMIUID
        /// </summary>
        [Test()]
        public void SetGetCellularConfigDuplicateImeiTest()
        {
            uint testAdjustment = 3510;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1A + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1A + testAdjustment.ToString(); ;

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuId + testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            Assert.AreEqual(newCellularConfig.MiuId, newCellularConfigReadback.MiuId);
            Assert.AreEqual(newCellularConfig.ModemConfig.Vendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(newCellularConfig.ModemConfig.Imei, newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(newCellularConfig.ModemConfig.Mno, newCellularConfigReadback.ModemConfig.Mno);
            Assert.AreEqual(newCellularConfig.SimConfig.Iccid, newCellularConfigReadback.SimConfig.Iccid);
            Assert.AreEqual(newCellularConfig.Apn, newCellularConfigReadback.Apn);

            //leave with duplicate ICCID to CMIUID
            newCellularConfig.MiuId = TestCmiuIdB + testAdjustment;
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort2A + testAdjustment.ToString(); ;
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorDuplicateImei, result.statusCode);
        }

        /// <summary>
        /// Check functionality of SetCellularConfig and GetCellularConfig
        /// Test setting an IMEI/CMIUID/ICCID tri then reusing the IMEI and ICCID on another CMIUID
        /// </summary>
        [Test()]
        public void SetGetCellularConfigDuplicateIccidImeiTest()
        {
            uint testAdjustment = 3510;

            var pcei = new FpcRestClient(PartnerId1, SiteId, FpcServerUrl, FpcServerUrlSubstring);

            //set new config
            var newCellularConfig = new CellularConfig();
            newCellularConfig.MiuId = TestCmiuId + testAdjustment;
            newCellularConfig.Apn = TestApn1;

            newCellularConfig.ModemConfig = new ModemConfig();
            newCellularConfig.ModemConfig.Vendor = TestVendor;
            newCellularConfig.ModemConfig.Imei = TestImeiValidShort1A + testAdjustment.ToString();
            newCellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();

            newCellularConfig.SimConfig = new SimConfig();
            newCellularConfig.SimConfig.Iccid = TestIccidValidShort1A + testAdjustment.ToString(); ;

            var result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            //read back and verify setting has been successful
            var newCellularConfigReadback = new CellularConfig();
            var statusCode = pcei.GetCellularConfig(TestCmiuId + testAdjustment, ref newCellularConfigReadback);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            Assert.AreEqual(newCellularConfig.MiuId, newCellularConfigReadback.MiuId);
            Assert.AreEqual(newCellularConfig.ModemConfig.Vendor, newCellularConfigReadback.ModemConfig.Vendor);
            Assert.AreEqual(newCellularConfig.ModemConfig.Imei, newCellularConfigReadback.ModemConfig.Imei);
            Assert.AreEqual(newCellularConfig.ModemConfig.Mno, newCellularConfigReadback.ModemConfig.Mno);
            Assert.AreEqual(newCellularConfig.SimConfig.Iccid, newCellularConfigReadback.SimConfig.Iccid);
            Assert.AreEqual(newCellularConfig.Apn, newCellularConfigReadback.Apn);

            //leave with duplicate ICCID to CMIUID
            newCellularConfig.MiuId = TestCmiuIdB + testAdjustment;
            result = pcei.SetCellularConfig(newCellularConfig);
            Assert.AreEqual(StatusCode.ErrorDuplicateImei, result.statusCode);
        }
    }
}
