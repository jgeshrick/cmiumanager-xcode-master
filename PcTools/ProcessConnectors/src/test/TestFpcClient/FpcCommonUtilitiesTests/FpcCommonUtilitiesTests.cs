﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Threading;
using NUnit.Framework;
using FpcClient;
using FpcClient.Common;
using FpcClient.Common.StatusCodes;
using FpcClient.Common.LifecycleStates;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.Common.CmiuCanConfigurationStructures;

namespace FpcClient.Tests
{
    [TestFixture()]
    public class FpcCommonUtilitiesTest
    {
        private const uint TestCmiuId1 = 400003330;
        private const uint TestCmiuId2 = 400055500;
        private const string TestImeiValid = "123456789012345";
        private const string TestImeiAplha = "123456789O12345";
        private const string TestImeiLong = "1234567890123456";
        private const string TestImeiShort = "12345678901234";
        private const string TestIccidValid = "12345678901234567890";
        private const string TestIccidAlpha = "1234567890I234567890";
        private const string TestIccidLong = "123456789012345678901";
        private const string TestIccidShort = "1234567890123456789";


        /// <summary>
        /// Check functionality of ValidateLifecycleState
        /// </summary>
        [Test]
        public void ValidateLifecycleStateTest()
        {
            // Test all null
            CmiuLifecycleState cmiuLifecycleState = new CmiuLifecycleState();
            var statusCode = Utilities.ValidateLifecycleState(cmiuLifecycleState);
            Assert.AreEqual(StatusCode.ErrorInvalidCmiuId, statusCode);

            // Test null - with CMIU Id set
            cmiuLifecycleState.MiuId = TestCmiuId1;
            statusCode = Utilities.ValidateLifecycleState(cmiuLifecycleState);
            Assert.AreEqual(StatusCode.ErrorInvalidLifecycleState, statusCode);

            // Test null - with State set
            cmiuLifecycleState.State = new State();
            statusCode = Utilities.ValidateLifecycleState(cmiuLifecycleState);
            Assert.AreEqual(StatusCode.ErrorInvalidLifecycleState, statusCode);

            // Test null - with date/time set
            cmiuLifecycleState.State.TimeStamp = DateTime.UtcNow;
            statusCode = Utilities.ValidateLifecycleState(cmiuLifecycleState);
            Assert.AreEqual(StatusCode.ErrorInvalidLifecycleState, statusCode);

            // Test null - lifecycle set to invalid setting
            cmiuLifecycleState.State.Id = "999";
            statusCode = Utilities.ValidateLifecycleState(cmiuLifecycleState);
            Assert.AreEqual(StatusCode.ErrorInvalidLifecycleState, statusCode);

            // Test null - lifecycle set to invalid setting
            cmiuLifecycleState.State.Id = "NotReal";
            statusCode = Utilities.ValidateLifecycleState(cmiuLifecycleState);
            Assert.AreEqual(StatusCode.ErrorInvalidLifecycleState, statusCode);

            // Test null - lifecycle set to valid setting
            LifecycleState lifecycleState = LifecycleState.PostPot;
            cmiuLifecycleState.State.Id = lifecycleState.ToString();
            statusCode = Utilities.ValidateLifecycleState(cmiuLifecycleState);
            Assert.AreEqual(StatusCode.OK, statusCode);
        }

        /// <summary>
        /// Check functionality of ValidateCmiuId
        /// These values are 9-digits, even values only and between 400000000 and 599999999
        /// </summary>
        [Test]
        public void ValidateCmiuIdTest()
        {
            // Test 0
            uint cmiuid = 0;
            var testResult = Utilities.ValidateCmiuId(cmiuid);
            Assert.AreEqual(false, testResult);

            // Test odd fails
            cmiuid = 400000001;
            testResult = Utilities.ValidateCmiuId(cmiuid);
            Assert.AreEqual(false, testResult);

            // Test lowest OK
            cmiuid = 400000000;
            testResult = Utilities.ValidateCmiuId(cmiuid);
            Assert.AreEqual(true, testResult);

            // Test just under lowest fails
            cmiuid = 399999998;
            testResult = Utilities.ValidateCmiuId(cmiuid);
            Assert.AreEqual(false, testResult);

            // Test high OK
            cmiuid = 499999998;
            testResult = Utilities.ValidateCmiuId(cmiuid);
            Assert.AreEqual(true, testResult);

            // Test highest OK
            cmiuid = 599999998;
            testResult = Utilities.ValidateCmiuId(cmiuid);
            Assert.AreEqual(true, testResult);

            // Test just over highest fails
            cmiuid = 600000000;
            testResult = Utilities.ValidateCmiuId(cmiuid);
            Assert.AreEqual(false, testResult);
        }

        /// <summary>
        /// Check functionality of rangeTestCmiuCan
        /// </summary>
        [Test]
        public void RangeTestCmiuCanTest()
        {
            // Test fail null CellularConfig;
            CmiuCan cmiuCan = new CmiuCan();
            var testResult = Utilities.RangeTestCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidCmiuId, testResult);

            // Test OK - as CMIUID is set but rest is null
            cmiuCan.cmiuId = TestCmiuId2;
            testResult = Utilities.RangeTestCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.OK, testResult);

            // Test OK - Iccid
            cmiuCan.simCan.iccid = TestIccidValid;
            testResult = Utilities.RangeTestCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.OK, testResult);
            // Test Fail - Alpha Iccid
            cmiuCan.simCan.iccid = TestIccidAlpha;
            testResult = Utilities.RangeTestCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, testResult);
            // Test Fail - Long Iccid
            cmiuCan.simCan.iccid = TestIccidLong;
            testResult = Utilities.RangeTestCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, testResult);
            // Test Fail - Short Iccid
            cmiuCan.simCan.iccid = TestIccidShort;
            testResult = Utilities.RangeTestCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, testResult);

            // Test OK - Imei
            cmiuCan.simCan.iccid = TestIccidValid;
            cmiuCan.modemCan.imei = TestImeiValid;
            testResult = Utilities.RangeTestCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.OK, testResult);
            // Test Fail - Alpha Imei
            cmiuCan.modemCan.imei = TestImeiAplha;
            testResult = Utilities.RangeTestCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, testResult);
            // Test Fail - Long Imei
            cmiuCan.modemCan.imei = TestImeiLong;
            testResult = Utilities.RangeTestCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, testResult);
            // Test Fail - Short Imei
            cmiuCan.modemCan.imei = TestImeiShort;
            testResult = Utilities.RangeTestCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, testResult);

            // Test OK - Provider
            cmiuCan.modemCan.imei = TestImeiValid;
            testResult = Utilities.RangeTestCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.OK, testResult);
            // Test OK - Valid Provider
            cmiuCan.modemCan.mno = CellularProviderCode.VZW;
            testResult = Utilities.RangeTestCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.OK, testResult);
            // Test OK - Valid Provider
            cmiuCan.modemCan.mno = CellularProviderCode.ATT;
            testResult = Utilities.RangeTestCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.OK, testResult);
        }

        /// <summary>
        /// Check functionality of rangeTestCellularConfig
        /// </summary>
        [Test]
        public void RangeTestCellularConfigTest()
        {
            // Test fail null CellularConfig;
            CellularConfig cellularConfig = new CellularConfig();
            var testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidCmiuId, testResult);

            // Test OK - as CMIUID is set but rest is null
            cellularConfig.MiuId = TestCmiuId2;
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.OK, testResult);

            // Test OK - ModemConfig not null
            cellularConfig.ModemConfig = new ModemConfig();
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.OK, testResult);

            // Test OK - SimConfig not null
            cellularConfig.SimConfig = new SimConfig();
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.OK, testResult);

            // Test OK - Iccid
            cellularConfig.SimConfig.Iccid = TestIccidValid;
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.OK, testResult);
            // Test Fail - Alpha Iccid
            cellularConfig.SimConfig.Iccid = TestIccidAlpha;
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, testResult);
            // Test Fail - Long Iccid
            cellularConfig.SimConfig.Iccid = TestIccidLong;
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, testResult);
            // Test Fail - Short Iccid
            cellularConfig.SimConfig.Iccid = TestIccidShort;
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, testResult);

            // Test OK - Imei
            cellularConfig.SimConfig.Iccid = TestIccidValid;
            cellularConfig.ModemConfig.Imei = TestImeiValid;
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.OK, testResult);
            // Test Fail - Alpha Imei
            cellularConfig.ModemConfig.Imei = TestImeiAplha;
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, testResult);
            // Test Fail - Long Imei
            cellularConfig.ModemConfig.Imei = TestImeiLong;
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, testResult);
            // Test Fail - Short Imei
            cellularConfig.ModemConfig.Imei = TestImeiShort;
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, testResult);

            // Test OK - Provider
            cellularConfig.ModemConfig.Imei = TestImeiValid;
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.OK, testResult);
            // Test Fail - Invalid Provider
            cellularConfig.ModemConfig.Mno = "bob";
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.ErrorInvalidProvider, testResult);
            // Test OK - Valid Provider
            cellularConfig.ModemConfig.Mno = CellularProviderCode.VZW.ToString();
            testResult = Utilities.RangeTestCellularConfig(cellularConfig);
            Assert.AreEqual(StatusCode.OK, testResult);
        }

        /// <summary>
        /// Check functionality of parseCellularProviderCode
        /// </summary>
        [Test]
        public void ParseCellularProviderCodeTest()
        {
            // Test Fail close
            string providerString = "ATaT";
            CellularProviderCode testProvider = CellularProviderCode.VZW;
            var testResult = Utilities.ParseCellularProviderCode(providerString, ref testProvider);
            Assert.AreEqual(StatusCode.ErrorInvalidProvider, testResult);
            Assert.AreEqual(CellularProviderCode.VZW, testProvider);

            // Test Fail 
            providerString = "Provider";
            testResult = Utilities.ParseCellularProviderCode(providerString, ref testProvider);
            Assert.AreEqual(StatusCode.ErrorInvalidProvider, testResult);
            Assert.AreEqual(CellularProviderCode.VZW, testProvider);

            // Test Fail 
            providerString = "Vendor";
            testResult = Utilities.ParseCellularProviderCode(providerString, ref testProvider);
            Assert.AreEqual(StatusCode.ErrorInvalidProvider, testResult);
            Assert.AreEqual(CellularProviderCode.VZW, testProvider);

            // Test OK
            providerString = "ATT";
            testResult = Utilities.ParseCellularProviderCode(providerString, ref testProvider);
            Assert.AreEqual(StatusCode.OK, testResult);
            Assert.AreEqual(CellularProviderCode.ATT, testProvider);

            // Test Fail null
            testResult = Utilities.ParseCellularProviderCode(null, ref testProvider);
            Assert.AreEqual(StatusCode.ErrorInvalidProvider, testResult);

            // Test OK
            providerString = "VZW";
            testResult = Utilities.ParseCellularProviderCode(providerString, ref testProvider);
            Assert.AreEqual(StatusCode.OK, testResult);
            Assert.AreEqual(CellularProviderCode.VZW, testProvider);

            // Test change OK
            providerString = "ATT";
            testResult = Utilities.ParseCellularProviderCode(providerString, ref testProvider);
            Assert.AreEqual(StatusCode.OK, testResult);
            Assert.AreEqual(CellularProviderCode.ATT, testProvider);
            providerString = "VZW";
            testResult = Utilities.ParseCellularProviderCode(providerString, ref testProvider);
            Assert.AreEqual(StatusCode.OK, testResult);
            Assert.AreEqual(CellularProviderCode.VZW, testProvider);
        }

        /// <summary>
        /// Check functionality of startStopWatch and stopStopWatch
        /// </summary>
        [Test]
        public void StopWatchTest()
        {
            const long lowerShort = 1500;
            const long upperShort = 2500;

            Utilities.StartStopWatch();
            Thread.Sleep(2000);
            var test1 = Utilities.StopStopWatch();

            Assert.Less(lowerShort, test1);
            Assert.Greater(upperShort, test1);
        }


        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with invaid cellular provider.
        /// Check that the cellular provider is set to a valid default value.
        /// </summary>
        [Test]
        public void GetStatusCodeStringTest()
        {
            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            var testString = fpcClient.GetStatusCodeString(StatusCode.OK);
            Assert.AreEqual("OK", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.WarningUpdatedRecord);
            Assert.AreEqual("WarningUpdatedRecord", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorNotImplemented);
            Assert.AreEqual("ErrorNotImplemented", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorApiNotInitialised);
            Assert.AreEqual("ErrorApiNotInitialised", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorLogFileNotInitialised);
            Assert.AreEqual("ErrorLogFileNotInitialised", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorServerUnavailable);
            Assert.AreEqual("ErrorServerUnavailable", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorServerTimeout);
            Assert.AreEqual("ErrorServerTimeout", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorPartnerId);
            Assert.AreEqual("ErrorPartnerId", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorConfigFile);
            Assert.AreEqual("ErrorConfigFile", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorClientInit);
            Assert.AreEqual("ErrorClientInit", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorServiceInit);
            Assert.AreEqual("ErrorServiceInit", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorRegisterState);
            Assert.AreEqual("ErrorRegisterState", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorUnspecified);
            Assert.AreEqual("ErrorUnspecified", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorInvalidCmiuId);
            Assert.AreEqual("ErrorInvalidCmiuId", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorInvalidProvider);
            Assert.AreEqual("ErrorInvalidProvider", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorInvalidImei);
            Assert.AreEqual("ErrorInvalidImei", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorInvalidIccid);
            Assert.AreEqual("ErrorInvalidIccid", testString);

            testString = fpcClient.GetStatusCodeString(StatusCode.ErrorRestDoPost);
            Assert.AreEqual("ErrorRestDoPost", testString);

            testString = fpcClient.GetStatusCodeString((StatusCode)4);
            Assert.AreEqual("4", testString);

            testString = fpcClient.GetStatusCodeString((StatusCode)99);
            Assert.AreEqual("99", testString);
        }
    }
}
