﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using NUnit.Framework;
using FpcClient.Settings;
using FpcClient.JsonModel;
using System.Threading;
using FpcClient.Common.StatusCodes;
using System;
using System.Diagnostics;

namespace FpcClient.Tests
{
    [TestFixture()]
    public class FpcFactoryTest
    {
        //Partner key for partner number 2, site 0 (non-production environments)
        private const string ValidPartnerId = "eb750ee73b5cf9d04ec78a0b23616e9b";
        private const string InvalidPartnerId = "fb750ee73b5cf9d04ec78a0b23616e9b";

        /// <summary>
        /// Create FPC Client
        /// </summary>
        [Test()]
        public void CreateFpcClientTest()
        {
            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);
        }

        /// <summary>
        /// Create FPC Client, read the environmanet xml.
        /// Check that the environment settings match the expected values.
        /// </summary>
        [Test()]
        public void InitFpcClientTest()
        {
            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(ValidPartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            /// Check environments are loaded
            var env = MdceEnvironments.ReadFromXml().Environment;

            Assert.AreEqual(env.Name, fpcClient.Environment);
        }

        /// <summary>
        /// Create FPC Client, read the environmanet xml.
        /// Check that the environment settings match the expected values.
        /// </summary>
        [Test()]
        public void InitFpcClientInValidPartnerIdTest()
        {
            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(InvalidPartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            /// Check environments are loaded
            var env = MdceEnvironments.ReadFromXml().Environment;

            Assert.AreEqual(env.Name, fpcClient.Environment);
        }

        /// <summary>
        /// Create FPC Client, read the environmanet xml.
        /// Check that the environment settings match the expected values.
        /// </summary>
        [Test()]
        public void InitFpcClientNullPartnerIdTest()
        {
            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(null);
            Assert.AreEqual(StatusCode.OK, testCode);

            /// Check environments are loaded
            var env = MdceEnvironments.ReadFromXml().Environment;

            Assert.AreEqual(env.Name, fpcClient.Environment);
        }

        /// <summary>
        /// Create FPC Client
        /// Check that the environment is set correctly
        /// </summary>
        [Test()]
        public void InitFpcClientEnvironmentTest()
        {
            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Set partner Id 
            ///     * InitPartnerId(string partnerId)
            var testCode = fpcClient.InitFpcClient(ValidPartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            /// Check environments are loaded
            var env = MdceEnvironments.ReadFromXml().Environment;

            string assemblyFileLocation = System.Reflection.Assembly.GetExecutingAssembly().Location;
            FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(assemblyFileLocation);
            string assemblyVersion = myFileVersionInfo.FileVersion.ToString();

            Assert.AreEqual(env.Name, fpcClient.Environment);
            Assert.AreEqual(assemblyVersion, fpcClient.AssemblyVersion);
            Assert.AreEqual(100, fpcClient.MinimumErrorCode);
        }

        /// <summary>
        /// Create FPC Client
        /// Check that the environment is set correctly
        /// </summary>
        [Test()]
        public void InitFpcClientEnvironmentNullTest()
        {
            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            /// Check environments are loaded
            var env = MdceEnvironments.ReadFromXml().Environment;

            string assemblyFileLocation = System.Reflection.Assembly.GetExecutingAssembly().Location;
            FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(assemblyFileLocation);
            string assemblyVersion = myFileVersionInfo.FileVersion.ToString();

            try
            {
                string testString = fpcClient.Environment.ToString();
                Assert.Fail();
            }
            catch (System.NullReferenceException)
            {
                Assert.Pass();
            }
            catch
            {
                Assert.Fail();
            }
            Assert.AreEqual(assemblyVersion, fpcClient.AssemblyVersion);
            Assert.AreEqual(100, fpcClient.MinimumErrorCode);
        }
    }
}