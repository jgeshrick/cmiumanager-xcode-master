﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using NUnit.Framework;
using FpcClient.JsonModel.CellularConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FpcClient.JsonModel.CellularConfig.Tests
{
    [TestFixture()]
    public class CellularConfigTests
    {
        /// <summary>
        /// This is to test the correctness of deserialising a json to POCO.
        /// </summary>
        [Test()]
        public void CellularConfigDeserialiseTest()
        {
            var cellularConfig = CellularConfig.FromJSON(JsonCellularConfig);

            Assert.NotNull(cellularConfig);

            Assert.AreEqual(5000001, cellularConfig.MiuId);
            Assert.AreEqual("VZW", cellularConfig.ModemConfig.Mno);
            Assert.AreEqual("356938035643809", cellularConfig.ModemConfig.Imei);
            Assert.AreEqual("Telit", cellularConfig.ModemConfig.Vendor);
            Assert.AreEqual("8991101200003204510", cellularConfig.SimConfig.Iccid);
        }


        #region TEST_JSON_STRING
        /// <summary>
        /// Json string extracted from ETI CRM Reference Data (IF42)
        /// </summary>
        public static string JsonCellularConfig = @"
{
    ""miu_id"": 5000001,
    ""modem_config"": 
    {
        ""vendor"": ""Telit"",
        ""mno"": ""VZW"",
        ""imei"": ""356938035643809""
    },
    ""sim_config"": 
    {
        ""iccid"": ""8991101200003204510"",
    }

}
";
        #endregion

    }

}