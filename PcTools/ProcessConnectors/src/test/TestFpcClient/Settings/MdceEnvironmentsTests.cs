﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FpcClient.Settings;
using NUnit.Framework;
namespace FpcClient.Settings.Tests
{
    [TestFixture()]
    public class MdceEnvironmentsTests
    {
        [Test()]
        public void WriteAndReadSettingTest()
        {
            MdceEnvironments env = new MdceEnvironments();
            env.Environment = new MdceEnvironment()
            {
                Name = "https://auto-test.mdce-nonprod.neptunensight.com:8445/fpc/api/v1/",
            };
            env.WriteToXml();

            var env2 = MdceEnvironments.ReadFromXml();

            Assert.AreEqual(env.Environment.Name, env2.Environment.Name);
        }
    }
}
