﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using NUnit.Framework;
using FpcClient.Settings;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;
using System;

namespace FpcClient.Tests
{
    [TestFixture()]
    public class StubFpcFactoryTests
    {
        //Partner key for partner number 2, site 0 (non-production environments)
        private static readonly string PARTNER_ID_1 = "eb750ee73b5cf9d04ec78a0b23616e9b";
        private const string testPartnerIdFail = "PartnerIdFail";

        /// <summary>
        /// TODO: Test summary.
        /// </summary>
        [Test()]
        public void CreateFpcClientStubTest()
        {
            var fpcStubClient = FpcFactory.CreateFpcClientStub();

            var env = MdceEnvironments.ReadFromXml();

            //Verify content of the stub service
            Assert.AreEqual(env.Environment.BaseUrl, fpcStubClient.ServerUrl);
            Assert.AreEqual(env.Environment.FpcSiteId, fpcStubClient.SiteId);
            Assert.AreEqual(env.Environment.Name, fpcStubClient.Environment);
        }

        /// <summary>
        /// TODO: Test summary.
        /// </summary>
        [Test]
        public void GetFpcServerDetailStubTest()
        {
            var fpcStubClient = FpcFactory.CreateFpcClientStub();

            ///TODO: For now just check function call and return
            var result = fpcStubClient.GetServerDetail();

            var env = MdceEnvironments.ReadFromXml();

            //Verify content of the result
            Assert.NotNull(result);
            Assert.AreEqual(env.Environment.BaseUrl, result.server);
            Assert.AreEqual("1.2.3.4", result.version);
        }

        /// <summary>
        /// TODO: Test summary.
        /// </summary>
        [Test()]
        public void InitFpcClientStubTest()
        {
            var fpcStubClient = FpcFactory.CreateFpcClientStub();

            var returnStatus = fpcStubClient.InitFpcClient(testPartnerIdFail);
            Assert.AreEqual(StatusCode.ServiceInitError, returnStatus);

            returnStatus = fpcStubClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, returnStatus);

            var env = MdceEnvironments.ReadFromXml();

            ///Verify content of the stub service
            Assert.AreEqual(env.Environment.BaseUrl, fpcStubClient.ServerUrl);
            Assert.AreEqual(env.Environment.FpcSiteId, fpcStubClient.SiteId);
            Assert.AreEqual(env.Environment.Name, fpcStubClient.Environment);
        }

        /// <summary>
        /// TODO: Test summary.
        /// </summary>
        [Test]
        public void SetGetCmiuCanStubTest()
        {
            FpcLibrary.CmiuCan config = new FpcLibrary.CmiuCan();

            var fpcStubClient = FpcFactory.CreateFpcClientStub();

            config.cmiuId = 12;
            config.modemCan.cellularProvider = CellularProviderCode.ATnT;
            config.modemCan.firmwareRevision = "AB.CD.EFG";
            config.modemCan.imei = "1234";
            config.simCan.iccid  = "5678";

            ///TODO: For now just check function call and return
            var result = fpcStubClient.SetCmiuCan(config);
            Assert.True(result);

            config.cmiuId = 34;
            config.modemCan.cellularProvider = CellularProviderCode.VZW;
            config.modemCan.firmwareRevision = "hijk.lmno";
            config.modemCan.imei = "13579";
            config.simCan.iccid = "24680";
            ///TODO: For now just check function call and return
            result = fpcStubClient.SetCmiuCan(config);
            Assert.True(result);

            config.cmiuId = 56;
            config.modemCan.cellularProvider = CellularProviderCode.None;
            config.modemCan.firmwareRevision = "WWW.XXX.YYY";
            config.modemCan.imei = "1122334455";
            config.simCan.iccid = "6677889900";
            ///TODO: For now just check function call and return
            result = fpcStubClient.SetCmiuCan(config);
            Assert.True(result);

            var canBack = fpcStubClient.GetCmiuCan(12);
            //Verify content of the result
            Assert.AreEqual(12, canBack.cmiuId);
            Assert.AreEqual("AB.CD.EFG", canBack.modemCan.firmwareRevision);
            Assert.AreEqual("1234", canBack.modemCan.imei);
            Assert.AreEqual(CellularProviderCode.ATnT, canBack.modemCan.cellularProvider);
            Assert.AreEqual("5678", canBack.simCan.iccid);
        }

        /// <summary>
        /// TODO: Test summary.
        /// </summary>
        [Test]
        public void SetCmiuCanStubTest()
        {
            FpcLibrary.CmiuCan config = new FpcLibrary.CmiuCan();

            var fpcStubClient = FpcFactory.CreateFpcClientStub();

            config.cmiuId = 0;
            ///TODO: For now just check function call and return
            var result = fpcStubClient.SetCmiuCan(config);
            Assert.False(result);

            config.cmiuId = 1;
            ///TODO: For now just check function call and return
            result = fpcStubClient.SetCmiuCan(config);
            Assert.True(result);

            config.cmiuId = 2;
            ///TODO: For now just check function call and return
            result = fpcStubClient.SetCmiuCan(config);
            Assert.True(result);
        }

        /// <summary>
        /// TODO: Test summary.
        /// </summary>
        [Test]
        public void SetGetSimCanStubTest()
        {
            FpcLibrary.SimCan configSim = new FpcLibrary.SimCan();
            var fpcStubClient = FpcFactory.CreateFpcClientStub();

            configSim.iccid = "5678";
            ///TODO: For now just check function call and return
            var result = fpcStubClient.SetSimCan(configSim, 12);
            Assert.True(result);

            configSim.iccid = "901234";
            ///TODO: For now just check function call and return
            result = fpcStubClient.SetSimCan(configSim, 13);
            Assert.True(result);

            configSim.iccid = "8765";
            ///TODO: For now just check function call and return
            result = fpcStubClient.SetSimCan(configSim, 14);
            Assert.True(result);

            var simBack = fpcStubClient.GetSimCan(12);
            //Verify content of the result
            Assert.AreEqual("5678", simBack.iccid);

            simBack = fpcStubClient.GetSimCan(14);
            //Verify content of the result
            Assert.AreEqual("8765", simBack.iccid);

            simBack = fpcStubClient.GetSimCan(13);
            //Verify content of the result
            Assert.AreEqual("901234", simBack.iccid);
        }

        /// <summary>
        /// TODO: Test summary.
        /// </summary>
        [Test]
        public void SetSimCanStubTest()
        {
            FpcLibrary.SimCan config = new FpcLibrary.SimCan();

            var fpcStubClient = FpcFactory.CreateFpcClientStub();
            Assert.NotNull(fpcStubClient);

            ///TODO: For now just check function call and return
            var result = fpcStubClient.SetSimCan(config, 0);
            Assert.False(result);

            ///TODO: For now just check function call and return
            result = fpcStubClient.SetSimCan(config, 1);
            Assert.True(result);

            ///TODO: For now just check function call and return
            result = fpcStubClient.SetSimCan(config, 2);
            Assert.True(result);
        }

        /// <summary>
        /// TODO: Test summary.
        /// </summary>
        [Test]
        public void SetModemCanStubTest()
        {
            FpcLibrary.ModemCan config = new FpcLibrary.ModemCan();

            var fpcStubClient = FpcFactory.CreateFpcClientStub();
            Assert.NotNull(fpcStubClient);

            ///TODO: For now just check function call and return
            var result = fpcStubClient.SetModemCan(config, 0);
            Assert.False(result);

            ///TODO: For now just check function call and return
            result = fpcStubClient.SetModemCan(config, 1);
            Assert.True(result);

            ///TODO: For now just check function call and return
            result = fpcStubClient.SetModemCan(config, 2);
            Assert.True(result);
        }

        /// <summary>
        /// TODO: Test summary.
        /// </summary>
        [Test]
        public void GetModemCanStubTest()
        {
            var fpcStubClient = FpcFactory.CreateFpcClientStub();

            ///TODO: For now just check function call and return
            var result = fpcStubClient.GetModemCan(12345);
            Assert.AreEqual("G.H.IJ.KL", result.firmwareRevision);
            Assert.AreEqual("1290", result.imei);
            Assert.AreEqual(CellularProviderCode.None, result.cellularProvider);
            result = fpcStubClient.GetModemCan(22);
            //Verify content of the result
            Assert.AreEqual("G.H.IJ.KL", result.firmwareRevision);
            Assert.AreEqual("1290", result.imei);
            Assert.AreEqual(CellularProviderCode.VZW, result.cellularProvider);
            result = fpcStubClient.GetModemCan(20);
            //Verify content of the result
            Assert.AreEqual("G.H.IJ.KL", result.firmwareRevision);
            Assert.AreEqual("1290", result.imei);
            Assert.AreEqual(CellularProviderCode.ATnT, result.cellularProvider);
        }

        /// <summary>
        /// TODO: Test summary.
        /// </summary>
        [Test]
        [Ignore("Requires new data structure to be defined")]
        public void SetMfgInfoStubTest()
        {
            /*
            ManufacturingInformation manuInf = new ManufacturingInformation();

            var fpcStubService = FpcFactory.CreateFpcStubService(testPartnerId);
           
            ///TODO: For now just check function call and return
            var result = fpcStubService.SetMfgInfo(12345, manuInf);
            Assert.True(result);

            ///TODO: For now just check function call and return
             result = fpcStubService.SetMfgInfo(0, manuInf);
            Assert.False(result);
            */
        }

        /// <summary>
        /// Checks to see whether the CMIU specified is ready for network activity.
        /// @duration TODO: Document expected duration
        /// </summary>
        /// <param name="cmiuId">Id of cmiu</param>
        /// <returns></returns>
        public void IsCmiuActiveStubTest(UInt32 cmiuId)
        {
            var fpcStubClient = FpcFactory.CreateFpcClientStub();

            ///TODO: For now just check function call and return
            var result = fpcStubClient.IsCmiuActive(0);
            //Verify content of the result
            Assert.False(result);
            result = fpcStubClient.IsCmiuActive(1);
            //Verify content of the result
            Assert.True(result);
        }

        /// <summary>
        /// Provide LabVIEW with a means to register CMIU state with the Fpc server.
        /// @duration TODO: Document expected duration
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to register</param>
        /// <returns></returns>
        public void RegisterCmiuBeforePrepotTestStubTest(UInt32 cmiuId)
        {
            var fpcStubClient = FpcFactory.CreateFpcClientStub();

            ///TODO: For now just check function call and return
            var result = fpcStubClient.RegisterCmiuBeforePrepotTest(0);
            //Verify content of the result
            Assert.AreEqual(StatusCode.RegisterError, result);
            result = fpcStubClient.RegisterCmiuBeforePrepotTest(1);
            //Verify content of the result
            Assert.AreEqual(StatusCode.OK, result);
        }

        /// <summary>
        /// Provide LabVIEW with a means to register CMIU state with the Fpc server.
        /// @duration TODO: Document expected duration
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to register</param>
        public void RegisterCmiuAfterSuccessfulPrepotTestStubTest(UInt32 cmiuId)
        {
            var fpcStubClient = FpcFactory.CreateFpcClientStub();

            ///TODO: For now just check function call and return
            var result = fpcStubClient.RegisterCmiuAfterSuccessfulPrepotTest(0);
            //Verify content of the result
            Assert.AreEqual(StatusCode.RegisterError, result);
            result = fpcStubClient.RegisterCmiuAfterSuccessfulPrepotTest(1);
            //Verify content of the result
            Assert.AreEqual(StatusCode.OK, result);
        }

        /// <summary>
        /// Provide LabVIEW with a means to register CMIU state with the Fpc server.
        /// @duration TODO: Document expected duration
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to register</param>
        public void RegisterCmiuAfterSuccessfulPostpotTestStubTest(UInt32 cmiuId)
        {
            var fpcStubClient = FpcFactory.CreateFpcClientStub();

            ///TODO: For now just check function call and return
            var result = fpcStubClient.RegisterCmiuAfterSuccessfulPostpotTest(0);
            //Verify content of the result
            Assert.AreEqual(StatusCode.RegisterError, result);
            result = fpcStubClient.RegisterCmiuAfterSuccessfulPostpotTest(1);
            //Verify content of the result
            Assert.AreEqual(StatusCode.OK, result);
        }
    }
}