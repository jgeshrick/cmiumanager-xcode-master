﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using NUnit.Framework;
using FpcClient.JsonModel;
using NeptuneCloudConnector.Exception;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;
using System.Threading;
using FpcClient.Common.CmiuCanConfigurationStructures;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.Common.LifecycleStates;


namespace FpcClient.Tests
{
    [TestFixture()]
    public class FpcClientInitialisationTests
    {
        private const string ValidPartnerId = "eb750ee73b5cf9d04ec78a0b23616e9b";

        /// <summary>
        /// Note the invalid "O" character in this partner string (all chars should be hex).
        /// </summary>
        private const string FailPartnerId1 = "eb750ee73b5cf9dO4ec78a0b23616e9b";

        private const string FailPartnerId2 = "eb750ee73b5cf9d04ec78a0b23616e9c";

        private const uint TestCmiuIdValid = 405050506;
        private const string TestImeiValidShort = "65432109876";
        private const string TestIccidValidShort = "6543210987654321";
        private const string TestVendor = "Telit";

        /// <summary>
        /// Check functionality of InitFpcClient
        /// </summary>
        [Test]
        public void InitFpcClientTest()
        {
            uint testAdjustment = 1422;

            CmiuCan config = new CmiuCan();

            //   * Create client
            //     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            //   * Initialise client
            //     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(ValidPartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            // Check return value is OK
            config.cmiuId = TestCmiuIdValid + testAdjustment;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidShort + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidShort + testAdjustment.ToString();

            // Check return value is OK
            var result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);
        }

        /// <summary>
        /// Check functionality of InitPartnerId
        /// </summary>
        [Test]
        public void InitPartnerIdTest()
        {
            uint testAdjustment = 1424;

            CmiuCan config = new CmiuCan();

            //   * Create client
            //     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            //   * Initialise client
            //     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(ValidPartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            // Check return value is OK
            config.cmiuId = TestCmiuIdValid;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidShort + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidShort + testAdjustment.ToString();

            // Check return value is OK
            var result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);


            //   * Initialise client
            //     * InitFpcClient(string partnerId);
            testCode = fpcClient.InitFpcClient(FailPartnerId1);
            Assert.AreEqual(StatusCode.OK, testCode);

            // Check return value is OK
            config.cmiuId = TestCmiuIdValid;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidShort + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidShort + testAdjustment.ToString();

            // Expect 400 Bad Request because partner string contains a non-hex character.
            result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.ErrorServerResponseBadRequest, result.statusCode);


            //   * Initialise client
            //     * InitFpcClient(string partnerId);
            testCode = fpcClient.InitFpcClient(ValidPartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            // Check return value is OK
            config.cmiuId = TestCmiuIdValid;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidShort + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidShort + testAdjustment.ToString();

            // Check return value is OK
            result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);


            //   * Initialise client
            //     * InitFpcClient(string partnerId);
            testCode = fpcClient.InitFpcClient(FailPartnerId2);
            Assert.AreEqual(StatusCode.OK, testCode);

            // Check return value is OK
            config.cmiuId = TestCmiuIdValid;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidShort + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidShort + testAdjustment.ToString();

            // Check return value is OK
            result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.ErrorServerResponseUnauthorized, result.statusCode);

            //   * Initialise client
            //     * InitFpcClient(string partnerId);
            testCode = fpcClient.InitFpcClient(ValidPartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            // Check return value is OK
            config.cmiuId = TestCmiuIdValid;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidShort + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidShort + testAdjustment.ToString();

            // Check return value is OK
            result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);
        }
    }
}
