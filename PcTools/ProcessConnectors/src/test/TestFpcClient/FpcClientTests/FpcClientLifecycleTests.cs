﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using NUnit.Framework;
using FpcClient.JsonModel;
using NeptuneCloudConnector.Exception;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;
using System.Threading;
using FpcClient.Common.CmiuCanConfigurationStructures;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.Common.LifecycleStates;
using FpcClient.Common.CmiuLifecycleStructures;


namespace FpcClient.Tests
{
    [TestFixture()]
    public class FpcClientLifecycleTest
    {
        private const string VALID_PARTNER_ID = "eb750ee73b5cf9d04ec78a0b23616e9b";
        private const uint TEST_CMUID_VALID = 402828282;
        private const uint TEST_MIU_ID = 486670110;
        private const uint TEST_CMUID_NEWBORN = 495976998;

        private const string TEST_IMEI_VALID_SHORT1 = "65432159876";
        private const string TEST_IMEI_VALID_SHORT2 = "65432169876";

        private const string TEST_ICCID_VALID_SHORT1 = "6543215987654321";
        private const string TEST_ICCID_VALID_SHORT2 = "6543216987654321";

        private const string TEST_VENDOR = "Telit";

        private const uint TEST_CMUID_VIRGIN = 443322118;

        /// <summary>
        /// Check functionality of IsCmiuActivated
        /// </summary>
        [Test]
        [Ignore]
        public void IsCmiuActivated()
        {
        }

        /// <summary>
        /// Check functionality of RegisterCmiu and GetCmiuLifecycle with null fpcClient
        /// </summary>
        [Test]
        public void RegisterCmiuLifecycleStatesNullTest()
        {
            uint testAdjustment = 2000;
            uint testCmiuId = TEST_MIU_ID + testAdjustment;

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   DON'T Initialise client

            var statusCode = fpcClient.RegisterCmiuBeforePrepotTest(testCmiuId);
            Assert.AreEqual(statusCode, StatusCode.ErrorApiNotInitialised);
        }


        /// <summary>
        /// Check functionality of RegisterCmiu and GetCmiuLifecycle
        /// </summary>
        [Test]
        public void RegisterCmiuLifecycleStatesNoConfigTest()
        {
            uint testAdjustment = 2100;
            uint testCmiuId = TEST_CMUID_NEWBORN + testAdjustment;

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(VALID_PARTNER_ID);
            Assert.AreEqual(StatusCode.OK, testCode);

            //get new config
            var newCellularConfig = new CmiuCan();
            newCellularConfig.cmiuId = testCmiuId;
            newCellularConfig.modemCan = new ModemCan();
            newCellularConfig.simCan = new SimCan();

            var resultCode = fpcClient.GetCmiuCan(newCellularConfig.cmiuId, ref newCellularConfig);

            //repeat till find certified virgin
            for (int i = 0; ((i < 50) && (resultCode != StatusCode.ErrorServerResponseNotFound)); i++)
            {
                testAdjustment += 2;
                testCmiuId = TEST_CMUID_VIRGIN + testAdjustment;
                newCellularConfig.cmiuId = testCmiuId;
                resultCode = fpcClient.GetCmiuCan(newCellularConfig.cmiuId, ref newCellularConfig);
            }
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, resultCode);

            var statusCode = fpcClient.RegisterCmiuBeforePrepotTest(testCmiuId);
            Assert.AreEqual(statusCode, StatusCode.ErrorInvalidLifecycleState);
        }

        /// <summary>
        /// Check functionality of RegisterCmiu and GetCmiuLifecycle
        /// </summary>
        [Test]
        public void RegisterCmiuLifecycleStatesTest()
        {
            uint testAdjustment = 2200;
            uint testCmiuId = TEST_MIU_ID + testAdjustment;

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(VALID_PARTNER_ID);
            Assert.AreEqual(StatusCode.OK, testCode);


            //get new config
            var newCellularConfig = new CmiuCan();
            newCellularConfig.cmiuId = testCmiuId;
            newCellularConfig.modemCan = new ModemCan();
            newCellularConfig.simCan = new SimCan();

            var resultCode = fpcClient.GetCmiuCan(newCellularConfig.cmiuId, ref newCellularConfig);

            //repeat till find certified virgin
            for (int i = 0; ((i < 50) && (resultCode != StatusCode.ErrorServerResponseNotFound)); i++)
            {
                testAdjustment += 2;
                testCmiuId = TEST_CMUID_VIRGIN + testAdjustment;
                newCellularConfig.cmiuId = testCmiuId;
                resultCode = fpcClient.GetCmiuCan(newCellularConfig.cmiuId, ref newCellularConfig);
            }
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, resultCode);



            CmiuCan config = new CmiuCan();
            /// Check return value is OK
            config.cmiuId = testCmiuId;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TEST_VENDOR;
            config.modemCan.imei = TEST_IMEI_VALID_SHORT1 + testAdjustment.ToString();
            config.simCan.iccid = TEST_ICCID_VALID_SHORT1 + testAdjustment.ToString();

            /// Check return value is OK
            var result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            CmiuLifecycleStruct myLifecycleState = new CmiuLifecycleStruct();
            var myCode = fpcClient.GetCmiuLifecycle(testCmiuId, ref myLifecycleState);


            var statusCode = fpcClient.RegisterCmiuBeforePrepotTest(testCmiuId);
            Assert.AreEqual(statusCode, StatusCode.OK);
            CmiuLifecycleStruct checkLifecycleState = new CmiuLifecycleStruct();
            statusCode = fpcClient.GetCmiuLifecycle(testCmiuId, ref checkLifecycleState);
            Assert.AreEqual(statusCode, StatusCode.OK);
            Assert.AreEqual(checkLifecycleState.state.id, LifecycleState.PrePrePot.ToString());

            statusCode = fpcClient.RegisterCmiuAfterSuccessfulPrepotTest(testCmiuId);
            Assert.AreEqual(statusCode, StatusCode.OK);
            statusCode = fpcClient.GetCmiuLifecycle(testCmiuId, ref checkLifecycleState);
            Assert.AreEqual(statusCode, StatusCode.OK);
            Assert.AreEqual(checkLifecycleState.state.id, LifecycleState.PrePot.ToString());

            statusCode = fpcClient.RegisterCmiuAfterSuccessfulPostpotTest(testCmiuId);
            Assert.AreEqual(statusCode, StatusCode.OK);
            statusCode = fpcClient.GetCmiuLifecycle(testCmiuId, ref checkLifecycleState);
            Assert.AreEqual(statusCode, StatusCode.OK);
            Assert.AreEqual(checkLifecycleState.state.id, LifecycleState.PostPot.ToString());

            statusCode = fpcClient.RegisterCmiuAfterSuccessfulPrepotTest(testCmiuId);
            Assert.AreEqual(statusCode, StatusCode.ErrorInvalidLifecycleState);
        }


        /// <summary>
        /// Check functionality of RegisterCmiu and GetCmiuLifecycle with Newborn CMIU
        /// </summary>
        [Test]
        public void RegisterCmiuLifecycleStatesNewbornCmiuTest()
        {
            uint testAdjustment = 2300;
            uint testCmiuId = TEST_CMUID_NEWBORN + testAdjustment;


            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(VALID_PARTNER_ID);
            Assert.AreEqual(StatusCode.OK, testCode);


            //get new config
            var newCellularConfig = new CmiuCan();
            newCellularConfig.cmiuId = testCmiuId;
            newCellularConfig.modemCan = new ModemCan();
            newCellularConfig.simCan = new SimCan();

            var resultCode = fpcClient.GetCmiuCan(newCellularConfig.cmiuId, ref newCellularConfig);

            //repeat till find certified virgin
            for (int i=0; ((i < 50) && (resultCode != StatusCode.ErrorServerResponseNotFound)); i++)
            {
                testAdjustment += 2;
                testCmiuId = TEST_CMUID_VIRGIN + testAdjustment;
                newCellularConfig.cmiuId = testCmiuId;
                resultCode = fpcClient.GetCmiuCan(newCellularConfig.cmiuId, ref newCellularConfig);
            }
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, resultCode);


            var statusCode = fpcClient.RegisterCmiuBeforePrepotTest(testCmiuId);
            Assert.AreEqual(statusCode, StatusCode.ErrorInvalidLifecycleState);

            CmiuLifecycleStruct checkLifecycleState = new CmiuLifecycleStruct();
            statusCode = fpcClient.GetCmiuLifecycle(testCmiuId, ref checkLifecycleState);
            Assert.AreEqual(statusCode, StatusCode.ErrorServerResponseNotFound);
        }


        /// <summary>
        /// Check functionality of RegisterCmiu and GetCmiuLifecycle
        /// </summary>
        [Test]
        public void RegisterCmiuLifecycleStatesVirginCmiuTest()
        {
            uint testAdjustment = 2400;
            uint testCmiuId = TEST_CMUID_VIRGIN + testAdjustment;

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(VALID_PARTNER_ID);
            Assert.AreEqual(StatusCode.OK, testCode);

            //set new config
            var newCellularConfig = new CmiuCan();
            newCellularConfig.cmiuId = testCmiuId;
            newCellularConfig.modemCan = new ModemCan();
            newCellularConfig.simCan = new SimCan();

            var resultCode = fpcClient.GetCmiuCan(newCellularConfig.cmiuId, ref newCellularConfig);

            //repeat till find certified virgin
            for (int i = 0; ((i < 50) && (resultCode != StatusCode.ErrorServerResponseNotFound)); i++)
            {
                testAdjustment += 2;
                testCmiuId = TEST_CMUID_VIRGIN + testAdjustment;
                newCellularConfig.cmiuId = testCmiuId;
                resultCode = fpcClient.GetCmiuCan(newCellularConfig.cmiuId, ref newCellularConfig);
            }
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, resultCode);

            CmiuLifecycleStruct checkLifecycleState = new CmiuLifecycleStruct();
            var statusCode = fpcClient.GetCmiuLifecycle(testCmiuId, ref checkLifecycleState);
            Assert.AreEqual(statusCode, StatusCode.ErrorServerResponseNotFound);
        }
    }
}
