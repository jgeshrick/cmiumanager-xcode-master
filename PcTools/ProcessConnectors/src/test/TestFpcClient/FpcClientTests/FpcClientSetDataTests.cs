﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Threading;
using NUnit.Framework;
using NeptuneCloudConnector.Exception;
using FpcClient;
using FpcClient.JsonModel;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.Common.StatusCodes;
using FpcClient.Common.LifecycleStates;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.ConfigurationStructures;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.Common.CmiuCanConfigurationStructures;

namespace FpcClient.Tests
{
    [TestFixture()]
    public class FpcClientSetDataTest
    {
        private const string PartnerId = "eb750ee73b5cf9d04ec78a0b23616e9b";

        private const uint TestCmiuValidMin = 400000000;
        private const uint TestCmiuValidMid = 500400400;
        private const uint TestCmiuValidMax = 599999998;
        private const uint TestCmiuLow = 399999998;
        private const uint TestCmiuHigh = 600000000;
        private const uint TestCmiuOdd = 424682467;
        private const uint TestCmiuVirgin = 498360040;

        private const string TestApnValid1 = "TestApnValid1";
        private const string TestApnValid2 = "TestApnValid2";
        
        /// <summary>
        /// IMEI should be 15 characters.
        /// </summary>
        private const string TestImeiValidStart1 = "12345678901";
        private const string TestImeiValidStart2 = "34343434343";
        private const string TestImeiValidStart3 = "78978978978";
        private const string TestImeiValidStart4 = "78923478978";

        private const string TestImeiLong = "1234567890123456";
        private const string TestImeiShort = "12345678901234";
        private const string TestImeiAlpha = "123456789O12345";


        /// <summary>
        /// ICCID should be 20 characters.
        /// </summary>
        private const string TestIccidValidStart1 = "2468024680246802";
        private const string TestIccidValidStart2 = "1357913579135791";
        private const string TestIccidValidStart3 = "1010101010101010";
        private const string TestIccidValidStart4 = "1012341010101010";

        private const string TestIccidLong = "123456789012345678901";
        private const string TestIccidShort = "1234567890123456789";
        private const string TestIccidAlpha = "1234567890I234567890";

        private const string TestVendor = "Telit";

        /// <summary>
        /// Check functionality of SetMfgInfo
        /// </summary>
        [Test]
        public void SetMfgInfoNullFPCClientTest()
        {
            ManufacturingInformationStruct manufacturingInformation = new ManufacturingInformationStruct();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            var result = fpcClient.SetMfgInfo(manufacturingInformation);
            Assert.AreEqual(StatusCode.ErrorApiNotInitialised, result);

            manufacturingInformation.miuId = TestCmiuValidMid;
            result = fpcClient.SetMfgInfo(manufacturingInformation);
            Assert.AreEqual(StatusCode.ErrorApiNotInitialised, result);
        }

        /// <summary>
        /// Check functionality of SetMfgInfo
        /// </summary>
        [Test]
        public void SetMfgInfoTest()
        {
            ManufacturingInformationStruct manufacturingInformation = new ManufacturingInformationStruct();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            var result = fpcClient.SetMfgInfo(manufacturingInformation);
            Assert.AreEqual(StatusCode.ErrorInvalidCmiuId, result);

            manufacturingInformation.miuId = TestCmiuValidMid;
            result = fpcClient.SetMfgInfo(manufacturingInformation);
            Assert.AreEqual(StatusCode.OK, result);
        }

        /// <summary>
        /// Check functionality of SetCmiuCan
        /// </summary>
        [Test]
        public void SetGetCmiuCanNullFPCClientTest()
        {
            uint testAdjustment = 1000;

            CmiuCan config = new CmiuCan();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * DON'T Initialise client

            /// Check return value is OK
            config.cmiuId = TestCmiuValidMin;
            config.apn = TestApnValid1;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidStart1 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidStart1 + testAdjustment.ToString();

            /// Check return value is ERROR
            var result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.ErrorApiNotInitialised, result.statusCode);

            CmiuCan canBack = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan((int)TestCmiuValidMid + testAdjustment, ref canBack);
            Assert.AreEqual(StatusCode.ErrorApiNotInitialised, result.statusCode);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            config.cmiuId = TestCmiuValidMid + testAdjustment;
            config.apn = TestApnValid2;
            config.modemCan.mno = CellularProviderCode.VZW;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidStart2 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidStart2 + testAdjustment.ToString();
            /// Check return value is OK
            result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            statusCode = fpcClient.GetCmiuCan((int)TestCmiuValidMid + testAdjustment, ref canBack);
            //Verify content of the result
            Assert.AreEqual(StatusCode.OK, statusCode);
            Assert.AreEqual(TestCmiuValidMid + testAdjustment, canBack.cmiuId);
            Assert.AreEqual(TestVendor, canBack.modemCan.vendor);
            Assert.AreEqual(TestImeiValidStart2 + testAdjustment.ToString(), canBack.modemCan.imei);
            Assert.AreEqual(CellularProviderCode.VZW, canBack.modemCan.mno);
            Assert.AreEqual(TestIccidValidStart2 + testAdjustment.ToString(), canBack.simCan.iccid);
            Assert.AreEqual(TestApnValid2, canBack.apn);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set three sets of CMIU CAn data.
        /// Check that each set is correct
        /// </summary>
        [Test]
        public void SetGetCmiuCanTest()
        {
            uint testAdjustment = 1002;

            CmiuCan config = new CmiuCan();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            /// Check return value is OK
            config.cmiuId = TestCmiuValidMin + testAdjustment;
            config.apn = TestApnValid1;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidStart1 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidStart1 + testAdjustment.ToString();

            /// Check return value is OK
            var result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            config.cmiuId = TestCmiuValidMid + testAdjustment;
            config.apn = TestApnValid2;
            config.modemCan.mno = CellularProviderCode.VZW;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidStart2 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidStart2 + testAdjustment.ToString();
            /// Check return value is OK
            result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            config.cmiuId = TestCmiuValidMax - testAdjustment;
            config.apn = TestApnValid1;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidStart3 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidStart3 + testAdjustment.ToString();
            /// Check return value is OK
            result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            CmiuCan canBack = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan((int)TestCmiuValidMin + testAdjustment, ref canBack);
            //Verify content of the result
            Assert.AreEqual(StatusCode.OK, statusCode);
            Assert.AreEqual(TestCmiuValidMin + testAdjustment, canBack.cmiuId);
            Assert.AreEqual(TestVendor, canBack.modemCan.vendor);
            Assert.AreEqual(TestImeiValidStart1 + testAdjustment.ToString(), canBack.modemCan.imei);
            Assert.AreEqual(CellularProviderCode.ATT, canBack.modemCan.mno);
            Assert.AreEqual(TestIccidValidStart1 + testAdjustment.ToString(), canBack.simCan.iccid);
            Assert.AreEqual(TestApnValid1, canBack.apn);

            statusCode = fpcClient.GetCmiuCan((int)TestCmiuValidMid + testAdjustment, ref canBack);
            //Verify content of the result
            Assert.AreEqual(StatusCode.OK, statusCode);
            Assert.AreEqual(TestCmiuValidMid + testAdjustment, canBack.cmiuId);
            Assert.AreEqual(TestVendor, canBack.modemCan.vendor);
            Assert.AreEqual(TestImeiValidStart2 + testAdjustment.ToString(), canBack.modemCan.imei);
            Assert.AreEqual(CellularProviderCode.VZW, canBack.modemCan.mno);
            Assert.AreEqual(TestIccidValidStart2 + testAdjustment.ToString(), canBack.simCan.iccid);
            Assert.AreEqual(TestApnValid2, canBack.apn);

            statusCode = fpcClient.GetCmiuCan((int)TestCmiuValidMax - testAdjustment, ref canBack);
            //Verify content of the result
            Assert.AreEqual(StatusCode.OK, statusCode);
            Assert.AreEqual(TestCmiuValidMax - testAdjustment, canBack.cmiuId);
            Assert.AreEqual(TestVendor, canBack.modemCan.vendor);
            Assert.AreEqual(TestImeiValidStart3 + testAdjustment.ToString(), canBack.modemCan.imei);
            Assert.AreEqual(CellularProviderCode.ATT, canBack.modemCan.mno);
            Assert.AreEqual(TestIccidValidStart3 + testAdjustment.ToString(), canBack.simCan.iccid);
            Assert.AreEqual(TestApnValid1, canBack.apn);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set three sets of CMIU CAn data.
        /// Check that each set is correct
        /// </summary>
        [Test]
        public void SetGetCmiuCanNullTest()
        {
            uint testAdjustment = 1104;

            CmiuCan configNull = new CmiuCan();
            CmiuCan config = new CmiuCan();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            config.cmiuId = TestCmiuValidMin + testAdjustment;
            config.apn = TestApnValid2;
            config.modemCan.mno = CellularProviderCode.VZW;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidStart3 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidStart3 + testAdjustment.ToString();
            var result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            configNull.cmiuId = TestCmiuValidMin + testAdjustment;
            var resultNull = fpcClient.SetCmiuCan(configNull);
            Assert.AreEqual(StatusCode.OK, resultNull.statusCode);

            CmiuCan canBack = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan((int)TestCmiuValidMin + testAdjustment, ref canBack);
            //Verify content of the result
            Assert.AreEqual(StatusCode.OK, statusCode);
            Assert.AreEqual(TestCmiuValidMin + testAdjustment, canBack.cmiuId);
            Assert.AreEqual(TestVendor, canBack.modemCan.vendor);
            Assert.AreEqual(TestImeiValidStart3 + testAdjustment.ToString(), canBack.modemCan.imei);
            Assert.AreEqual(CellularProviderCode.VZW, canBack.modemCan.mno);
            Assert.AreEqual(TestIccidValidStart3 + testAdjustment.ToString(), canBack.simCan.iccid);
            Assert.AreEqual(TestApnValid2, canBack.apn);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with invaid cellular provider.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCmiuCanFailInvalidProviderTest()
        {
            uint testAdjustment = 1006;

            CmiuCan config = new CmiuCan();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            ///Set CMIU Can with invalid cellular provider
            config.cmiuId = TestCmiuValidMid + testAdjustment;
            config.apn = TestApnValid1;
            config.modemCan.mno = (CellularProviderCode)5;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidStart2 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidStart2 + testAdjustment.ToString();

            /// Check return value is correct
            var result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.ErrorInvalidProvider, result.statusCode);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with invaid textual IMEI.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCmiuCanFailTextImeiTest()
        {
            uint testAdjustment = 1008;

            CmiuCan config = new CmiuCan();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            ///Set CMIU Can with invalid IMEI
            config.cmiuId = TestCmiuValidMid + testAdjustment;
            config.apn = TestApnValid1;
            config.modemCan.mno = CellularProviderCode.VZW;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = "TEST_IMEI_VALID_3";
            config.simCan.iccid = TestIccidValidStart3 + testAdjustment.ToString();

            /// Check return value is correct
            var result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, result.statusCode);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with invaid textual ICCID.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCmiuCanFailTextIccidTest()
        {
            uint testAdjustment = 1010;

            CmiuCan config = new CmiuCan();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            ///Set CMIU Can with invalid ICCID
            config.cmiuId = TestCmiuValidMid + testAdjustment;
            config.apn = TestApnValid1;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidStart3 + testAdjustment.ToString();
            config.simCan.iccid = "TEST_ICCID_VALID_3";

            /// Check return value is correct
            var result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, result.statusCode);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with IMEI that is too long.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCmiuCanLongImeiTest()
        {
            uint testAdjustment = 1012;

            CmiuCan config = new CmiuCan();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            StatusCode result = StatusCode.ErrorUnspecified;

            ///Set CMIU Can with invalid IMEI
            config.cmiuId = TestCmiuValidMid + testAdjustment;
            config.apn = TestApnValid1;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiLong;
            config.simCan.iccid = TestIccidValidStart3 + testAdjustment.ToString();

            /// Check return value is correct
            result = fpcClient.SetCmiuCan(config).statusCode;
            Assert.AreEqual(StatusCode.ErrorInvalidImei, result);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with ICCID that is too long.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCmiuCanLongIccidTest()
        {
            uint testAdjustment = 1014;

            CmiuCan config = new CmiuCan();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            StatusCode result = StatusCode.ErrorUnspecified;

            ///Set CMIU Can with invalid ICCID
            config.cmiuId = TestCmiuValidMid + testAdjustment;
            config.apn = TestApnValid1;
            config.modemCan.mno = CellularProviderCode.VZW;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidStart3 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidLong;

            /// Check return value is correct
            result = fpcClient.SetCmiuCan(config).statusCode;
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, result);
        }


        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with IMEI that is too long.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCmiuCanShortImeiTest()
        {
            uint testAdjustment = 1016;

            CmiuCan config = new CmiuCan();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            StatusCode result = StatusCode.ErrorUnspecified;

            ///Set CMIU Can with invalid IMEI
            config.cmiuId = TestCmiuValidMid + testAdjustment;
            config.apn = TestApnValid1;
            config.modemCan.mno = CellularProviderCode.VZW;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiShort;
            config.simCan.iccid = TestIccidValidStart3 + testAdjustment.ToString();

            /// Check return value is correct
            result = fpcClient.SetCmiuCan(config).statusCode;
            Assert.AreEqual(StatusCode.ErrorInvalidImei, result);
        }

        /// <summary>
        /// Create FPC Client, connect to FPC Server, set CMIU CAN data with ICCID that is too long.
        /// Check that the CMIU CAN remains unchanged after failed set
        /// </summary>
        [Test]
        public void SetGetCmiuCanShortIccidTest()
        {
            uint testAdjustment = 1018;

            CmiuCan config = new CmiuCan();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            StatusCode result = StatusCode.ErrorUnspecified;

            ///Set CMIU Can with invalid ICCID
            config.cmiuId = TestCmiuValidMid + testAdjustment;
            config.apn = TestApnValid1;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidStart3 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidShort;

            /// Check return value is correct
            result = fpcClient.SetCmiuCan(config).statusCode;
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, result);
        }


        /// <summary>
        /// Create FPC Client, connect to FPC Server, set three sets of CMIU CAn data.
        /// Check that each set is correct
        /// </summary>
        [Test]
        public void SetGetCmiuCanOutOfRangeCmiuIdTest()
        {
            uint testAdjustment = 1220;

            CmiuCan config = new CmiuCan();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            /// Check return value is OK
            config.cmiuId = TestCmiuValidMid + testAdjustment;
            config.apn = TestApnValid1;
            config.modemCan.mno = CellularProviderCode.VZW;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidStart2 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidStart2 + testAdjustment.ToString();
            /// Check return value is OK
            var result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            CmiuCan canBack = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan((int)TestCmiuValidMid + testAdjustment, ref canBack);
            //Verify content of the result
            Assert.AreEqual(TestCmiuValidMid + testAdjustment, canBack.cmiuId);
            Assert.AreEqual(TestVendor, canBack.modemCan.vendor);
            Assert.AreEqual(TestImeiValidStart2 + testAdjustment.ToString(), canBack.modemCan.imei);
            Assert.AreEqual(CellularProviderCode.VZW, canBack.modemCan.mno);
            Assert.AreEqual(TestIccidValidStart2 + testAdjustment.ToString(), canBack.simCan.iccid);
            Assert.AreEqual(TestApnValid1, canBack.apn);

            config.cmiuId = TestCmiuValidMin + testAdjustment;
            config.modemCan.imei = TestImeiValidStart1 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidStart1 + testAdjustment.ToString();
            result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            config.cmiuId = TestCmiuLow;
            result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.ErrorInvalidCmiuId, result.statusCode);

            config.cmiuId = TestCmiuValidMax;
            config.modemCan.imei = TestImeiValidStart3 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidStart3 + testAdjustment.ToString();
            result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            config.cmiuId = TestCmiuHigh;
            result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.ErrorInvalidCmiuId, result.statusCode);

            config.cmiuId = TestCmiuValidMid;
            config.modemCan.imei = TestImeiValidStart4 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidStart4 + testAdjustment.ToString();
            result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            config.cmiuId = TestCmiuOdd;
            result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.ErrorInvalidCmiuId, result.statusCode);
        }

        /// <summary>
        /// Check functionality of rangeTestCmiuCan
        /// </summary>
        [Test]
        public void RangeTestCmiuCanTest()
        {
            uint testAdjustment = 1322;

            CmiuCan config = new CmiuCan();

            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            /// Check return value is OK
            config.cmiuId = TestCmiuValidMid + testAdjustment;
            config.apn = TestApnValid2;
            config.modemCan.mno = CellularProviderCode.ATT;
            config.modemCan.vendor = TestVendor;
            config.modemCan.imei = TestImeiValidStart1 + testAdjustment.ToString();
            config.simCan.iccid = TestIccidValidStart1 + testAdjustment.ToString();
            /// Check return value is OK
            var result = fpcClient.SetCmiuCan(config);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            CmiuCan canBack = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan((int)TestCmiuValidMid + testAdjustment, ref canBack);
            //Verify content of the result
            Assert.AreEqual(StatusCode.OK, statusCode);
            Assert.AreEqual(TestCmiuValidMid + testAdjustment, canBack.cmiuId);
            Assert.AreEqual(TestVendor, canBack.modemCan.vendor);
            Assert.AreEqual(TestImeiValidStart1 + testAdjustment.ToString(), canBack.modemCan.imei);
            Assert.AreEqual(CellularProviderCode.ATT, canBack.modemCan.mno);
            Assert.AreEqual(TestIccidValidStart1 + testAdjustment.ToString(), canBack.simCan.iccid);
            Assert.AreEqual(TestApnValid2, canBack.apn);

            // Test fail null CellularConfig;
            CmiuCan cmiuCan = new CmiuCan();
            result = fpcClient.SetCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidCmiuId, result.statusCode);

            // Test OK - as CMIUID is set but rest is null
            cmiuCan.cmiuId = TestCmiuValidMid + testAdjustment;
            result = fpcClient.SetCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.OK, result.statusCode);

            // Test OK - Iccid
            cmiuCan.modemCan.imei = TestImeiValidStart1 + testAdjustment.ToString();
            cmiuCan.simCan.iccid = TestIccidValidStart1 + testAdjustment.ToString();
            result = fpcClient.SetCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.OK, result.statusCode);
            // Test Fail - Alpha Iccid
            cmiuCan.simCan.iccid = TestIccidAlpha;
            result = fpcClient.SetCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, result.statusCode);
            // Test Fail - Long Iccid
            cmiuCan.simCan.iccid = TestIccidLong;
            result = fpcClient.SetCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, result.statusCode);
            // Test Fail - Short Iccid
            cmiuCan.simCan.iccid = TestIccidShort;
            result = fpcClient.SetCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidIccid, result.statusCode);

            // Test OK - Imei
            cmiuCan.simCan.iccid = TestIccidValidStart1 + testAdjustment.ToString();
            cmiuCan.modemCan.imei = TestImeiValidStart1 + testAdjustment.ToString();
            result = fpcClient.SetCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.OK, result.statusCode);
            // Test Fail - Alpha Imei
            cmiuCan.modemCan.imei = TestImeiAlpha;
            result = fpcClient.SetCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, result.statusCode);
            // Test Fail - Long Imei
            cmiuCan.modemCan.imei = TestImeiLong;
            result = fpcClient.SetCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, result.statusCode);
            // Test Fail - Short Imei
            cmiuCan.modemCan.imei = TestImeiShort;
            result = fpcClient.SetCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.ErrorInvalidImei, result.statusCode);

            // Test OK - Provider
            cmiuCan.modemCan.imei = TestImeiValidStart1 + testAdjustment.ToString();
            result = fpcClient.SetCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.OK, result.statusCode);
            // Test OK - Valid Provider
            cmiuCan.modemCan.mno = CellularProviderCode.VZW;
            result = fpcClient.SetCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.OK, result.statusCode);
            // Test OK - Valid Provider
            cmiuCan.modemCan.mno = CellularProviderCode.ATT;
            result = fpcClient.SetCmiuCan(cmiuCan);
            Assert.AreEqual(StatusCode.OK, result.statusCode);
        }


        /// <summary>
        /// Create FPC Client, connect to FPC Server, set three sets of CMIU CAn data.
        /// Check that each set is correct
        /// </summary>
        [Test]
        public void SetGetCmiuCanVirginCmiuTest()
        {
            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            CmiuCan canBack = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan((int)TestCmiuVirgin, ref canBack);
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, statusCode);
        }
    }
}
