﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Threading;
using NeptuneCloudConnector.Exception;
using NUnit.Framework;
using FpcClient;
using FpcClient.JsonModel;
using FpcClient.Common.StatusCodes;
using FpcClient.Common.ApnInformationStructures;
using FpcClient.Common.CellularProviderCodes;

namespace FpcClient.Tests
{
    [TestFixture()]
    public class FpcClientApnInformationTests
    {
        private const string VALID_PARTNER_ID = "eb750ee73b5cf9d04ec78a0b23616e9b";

        /// <summary>
        /// Check functionality with null fpcClient
        /// </summary>
        [Test]
        public void GetApnStringNullClient()
        {
            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   DON'T Initialise client

            //get new config
            var nullApnInfo = new ApnInfo();
            nullApnInfo.mno = CellularProviderCode.VZW;
            var statusCode = fpcClient.GetApnString(ref nullApnInfo);
            Assert.AreEqual(statusCode, StatusCode.ErrorApiNotInitialised);
        }


        /// <summary>
        /// Check functionality of RegisterCmiu and GetCmiuLifecycle
        /// </summary>
        [Test]
        public void GetApnStringValidClient()
        {
            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(VALID_PARTNER_ID);
            Assert.AreEqual(StatusCode.OK, testCode);

            //get new config
            var newApnInfo = new ApnInfo();
            newApnInfo.mno = CellularProviderCode.VZW;
            var resultCode = fpcClient.GetApnString(ref newApnInfo);
            Assert.AreEqual(StatusCode.OK, resultCode);
            Assert.AreEqual(newApnInfo.mno, CellularProviderCode.VZW);
            string oldApn = newApnInfo.apn;

            newApnInfo.mno = CellularProviderCode.ATT;
            resultCode = fpcClient.GetApnString(ref newApnInfo);
            Assert.AreEqual(StatusCode.OK, resultCode);
            Assert.AreEqual(newApnInfo.mno, CellularProviderCode.ATT);
            Assert.AreNotEqual(newApnInfo.apn, oldApn);
        }
    }
}
