﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using NUnit.Framework;
using FpcClient.JsonModel;
using NeptuneCloudConnector.Exception;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;
using System.Threading;
using FpcClient.Common.CmiuCanConfigurationStructures;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.Common.LifecycleStates;


namespace FpcClient.Tests
{
    [TestFixture()]
    public class FpcClientInitialisationTest
    {
        private const string PartnerId = "eb750ee73b5cf9d04ec78a0b23616e9b";

        /// <summary>
        /// Check functionality of GetServerDetails
        /// </summary>
        [Test]
        public void GetFpcServerDetailTest()
        {
            ///   * Create client
            ///     * FpcClient()
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.NotNull(fpcClient);

            ///   * Initialise client
            ///     * InitFpcClient(string partnerId);
            var testCode = fpcClient.InitFpcClient(PartnerId);
            Assert.AreEqual(StatusCode.OK, testCode);

            ServerDetails serverDetails1 = new ServerDetails();
            var result1 = fpcClient.GetServerDetails(ref serverDetails1);
            Thread.Sleep(2000);
            ServerDetails serverDetails2 = new ServerDetails();
            var result2 = fpcClient.GetServerDetails(ref serverDetails2);

            /// Verify content of the result
            Assert.AreEqual(StatusCode.OK, result1);
            Assert.AreEqual(StatusCode.OK, result2);
            Assert.AreEqual(serverDetails1.environment, serverDetails2.environment);
            Assert.AreEqual(serverDetails1.server, serverDetails2.server);
            Assert.AreEqual(serverDetails1.version, serverDetails2.version);
            Assert.Greater(serverDetails2.currentTimeUtc, serverDetails1.currentTimeUtc);
        }
    }
}
