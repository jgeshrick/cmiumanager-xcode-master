﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Moq;
using Ntg.BpcCore.JsonModel;
using Ntg.BpcCore.ServiceProxies;
using Ntg.BpcWebService.Controllers;
using Ntg.BpcWebService.Models;
using Ntg.BpcWebService.Models.Find;
using NUnit.Framework;

namespace Ntg.BpcWebService.UnitTests.Controllers
{
    [TestFixture]
    public class MiuListApiControllerTest
    {
        private Mock<IMiuListMultiServerProxy> miuListProxyMock;
        private Mock<ITimeZoneFormatter> timeZoneFormatterMock;
        private MiuListApiController controller;

        [SetUp]
        public void SetUp()
        {
            // Create mocks
            miuListProxyMock = new Mock<IMiuListMultiServerProxy>();
            timeZoneFormatterMock = new Mock<ITimeZoneFormatter>();
            
            // Object under test
            controller = new MiuListApiController(miuListProxyMock.Object, timeZoneFormatterMock.Object);
        }

        /// <summary>
        /// Simple verification that GetMiuDetailsAsync returns the data from the supplied 
        /// <see cref="IMiuListMultiServerProxy"/> with HTTP status 200.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task GetMiuDetailsAsync_WhenCalled_ReturnsResultFromIMiuListClient()
        {
            // Arrange
            var expectedLastInsertDate = DateTime.Parse("2016-01-01T10:00:00-05:00");
            var miuDetail = new MiuDetail
            {
                SiteId = 0,
                MiuId = 123456,
                LastInsertDate = "2016-01-01T10:00:00-05:00"
            };
            var miuListCollection = new MiuListCollection {Mius = new List<MiuDetail>{miuDetail}};
            var environmentName = "Auto Testing";
            var autoTestingResult = new SingleServerResult<MiuListCollection>(environmentName, miuListCollection);
            var proxyResult = new MultiServerResult<MiuListCollection>(new[] { autoTestingResult });
            
            miuListProxyMock
                .Setup(m => m.GetMiuListAsync(It.IsAny<string>()))
                .ReturnsAsync(proxyResult);

            // Act
            HttpResponseMessage response = await controller.WithMockedHttpRequest(c => c.GetMiuDetailsAsync("123456"));

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode); // Check the HTTP status
            FindResultCollection findMiuResults;
            Assert.IsTrue(response.TryGetContentValue(out findMiuResults));

            var results = findMiuResults.Results.ToList();
            Assert.IsNotNull(results);
            Assert.AreEqual(1, results.Count);
            Assert.AreEqual("Auto Testing", results.First().EnvironmentName);
            Assert.AreEqual(0, results.First().SiteId);
            Assert.AreEqual("123456", results.First().MiuId);
            Assert.AreEqual(expectedLastInsertDate, results.First().LastInsertDate);
        }

        /// <summary>
        /// Verify that GetMiuDetailsAsync returns the environment that most recently heard from the MIU 
        /// as the first result.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task GetMiuDetailsAsync_WhenCalled_ReturnsMostRecentlyHeardFromFirst()
        {
            // Arrange
            var miuDetail1 = new MiuDetail
            {
                SiteId = 0,
                MiuId = 123456,
                LastInsertDate = "2016-01-01T10:00:00-05:00"
            };
            var miuListCollection1 = new MiuListCollection { Mius = new List<MiuDetail> { miuDetail1 } };
            var environmentName1 = "Auto Testing";
            var result1 = new SingleServerResult<MiuListCollection>(environmentName1, miuListCollection1);

            var miuDetail2 = new MiuDetail
            {
                SiteId = 0,
                MiuId = 123456,
                LastInsertDate = "2016-02-01T10:00:00-05:00"
            };
            var miuListCollection2 = new MiuListCollection { Mius = new List<MiuDetail> { miuDetail2 } };
            var environmentName2 = "Development1";
            var result2 = new SingleServerResult<MiuListCollection>(environmentName2, miuListCollection2);
            
            var proxyResult = new MultiServerResult<MiuListCollection>(new[] { result1, result2 });
            
            miuListProxyMock
                .Setup(m => m.GetMiuListAsync(It.IsAny<string>()))
                .ReturnsAsync(proxyResult);

            // Act
            HttpResponseMessage response = await controller.WithMockedHttpRequest(c => c.GetMiuDetailsAsync("123456"));

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode); // Check the HTTP status
            FindResultCollection findMiuResults;
            Assert.IsTrue(response.TryGetContentValue(out findMiuResults));

            var results = findMiuResults.Results.ToList();
            Assert.IsNotNull(results);
            Assert.AreEqual(2, results.Count);
            Assert.AreEqual("Development1", results.First().EnvironmentName);
        }

        /// <summary>
        /// Verify that GetMiuDetailsAsync returns the environment that most recently heard from the MIU 
        /// as the first result.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task GetMiuDetailsAsync_WhenCalled_ReturnsNotHeardFromLast()
        {
            // Arrange
            var miuDetail1 = new MiuDetail
            {
                SiteId = 0,
                MiuId = 123456,
                FirstInsertDate = null,
                LastInsertDate = null
            };
            var miuListCollection1 = new MiuListCollection { Mius = new List<MiuDetail> { miuDetail1 } };
            var environmentName1 = "Auto Testing";
            var result1 = new SingleServerResult<MiuListCollection>(environmentName1, miuListCollection1);

            var miuDetail2 = new MiuDetail
            {
                SiteId = 0,
                MiuId = 123456,
                LastInsertDate = "2016-02-01T10:00:00-05:00"
            };
            var miuListCollection2 = new MiuListCollection { Mius = new List<MiuDetail> { miuDetail2 } };
            var environmentName2 = "Development1";
            var result2 = new SingleServerResult<MiuListCollection>(environmentName2, miuListCollection2);

            var proxyResult = new MultiServerResult<MiuListCollection>(new[] { result1, result2 });

            miuListProxyMock
                .Setup(m => m.GetMiuListAsync(It.IsAny<string>()))
                .ReturnsAsync(proxyResult);

            // Act
            HttpResponseMessage response = await controller.WithMockedHttpRequest(c => c.GetMiuDetailsAsync("123456"));

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode); // Check the HTTP status
            FindResultCollection findMiuResults;
            Assert.IsTrue(response.TryGetContentValue(out findMiuResults));

            var results = findMiuResults.Results.ToList();
            Assert.IsNotNull(results);
            Assert.AreEqual(2, results.Count);
            Assert.AreEqual("Development1", results.First().EnvironmentName);
        }
    }
}
