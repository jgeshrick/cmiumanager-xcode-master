﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CsvHelper;
using Moq;
using Ntg.BpcCore.JsonModel;
using Ntg.BpcCore.ServiceProxies;
using Ntg.BpcCore.Settings;
using Ntg.BpcWebService.Controllers;
using Ntg.BpcWebService.Models;
using Ntg.BpcWebService.Models.CanReport;
using NUnit.Framework;

namespace Ntg.BpcWebService.UnitTests.Controllers
{
    [TestFixture]
    public class CanReportControllerTest
    {
        private CanReportController controller;
        private Mock<IMiuListMultiServerProxy> miuListMultiServerProxyMock;
        private ITimeZoneFormatter timeZoneFormatter;

        [SetUp]
        public void InitializeTest()
        {
            miuListMultiServerProxyMock = new Mock<IMiuListMultiServerProxy>();
            timeZoneFormatter = new TimeZoneFormatter("UTC");

            // Set up mock environments
            miuListMultiServerProxyMock
                .Setup(m => m.GetConfiguredEnvironments())
                .Returns(new List<MdceEnvironment>
                {
                    new MdceEnvironment {Name = "env1", IsEnabled = true},
                    new MdceEnvironment {Name = "env2", IsEnabled = true}
                });

            controller = new CanReportController(miuListMultiServerProxyMock.Object, timeZoneFormatter);
        }

        /// <summary>
        /// If all servers respond, expect the most recently heard to be shown.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task PostTextList_AllServersRespond()
        {
            var results400000102 = new MultiServerResult<MiuListCollection>();
            results400000102.Results.Add("env1", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = "2017-01-01", SiteId = 0 } } });
            results400000102.Results.Add("env2", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = "2017-02-02", SiteId = 0 } } });

            miuListMultiServerProxyMock.Setup(m => m.GetMiuListAsync("400000102", DeviceIdType.MiuId, It.IsAny<IEnumerable<string>>())).ReturnsAsync(results400000102);

            var result = await controller.PostTextList(new PostTextRequestModel {MiuListText = "400000102"});
            var fileContentResult = (FileContentResult)result;

            var canResultCollection = new CanResultCollection();
            using (var ms = new MemoryStream(fileContentResult.FileContents))
            using (var streamReader = new StreamReader(ms))
            using (var csvReader = new CsvReader(streamReader))
            {
                csvReader.Configuration.HasHeaderRecord = false;

                // Read environment details
                ReadQueriedEnvironments(csvReader, canResultCollection);

                // Read report data
                ReadReportResults(csvReader, canResultCollection);
            }

            var returnedMiuResult = canResultCollection.Results.Single(r => r.MiuId == 400000102);

            Assert.AreEqual("env2", returnedMiuResult.Environment);
            Assert.IsTrue(canResultCollection.QueriedEnvironments.Any(e => e.EnvironmentName == "env1" && e.WasResponseOk));
            Assert.IsTrue(canResultCollection.QueriedEnvironments.Any(e => e.EnvironmentName == "env2" && e.WasResponseOk));
        }

        /// <summary>
        /// If one of the MDCE service calls fails, the report should show that the environment couldn't be queried.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task PostTextList_OneServerFails()
        {
            var results400000102 = new MultiServerResult<MiuListCollection>();
            results400000102.Results.Add("env1", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = "2017-01-01", SiteId = 0 } } });
            results400000102.Errors.Add("env2", "Service error");

            miuListMultiServerProxyMock.Setup(m => m.GetMiuListAsync("400000102", DeviceIdType.MiuId, It.IsAny<IEnumerable<string>>())).ReturnsAsync(results400000102);

            var result = await controller.PostTextList(new PostTextRequestModel { MiuListText = "400000102" });
            var fileContentResult = (FileContentResult)result;

            var canResultCollection = new CanResultCollection();
            using (var ms = new MemoryStream(fileContentResult.FileContents))
            using (var streamReader = new StreamReader(ms))
            using (var csvReader = new CsvReader(streamReader))
            {
                csvReader.Configuration.HasHeaderRecord = false;

                // Read environment details
                ReadQueriedEnvironments(csvReader, canResultCollection);

                // Read report data
                ReadReportResults(csvReader, canResultCollection);
            }

            var returnedMiuResult = canResultCollection.Results.Single(r => r.MiuId == 400000102);

            Assert.AreEqual("env1", returnedMiuResult.Environment);
            Assert.IsTrue(canResultCollection.QueriedEnvironments.Any(e => e.EnvironmentName == "env1" && e.WasResponseOk));
            Assert.IsTrue(canResultCollection.QueriedEnvironments.Any(e => e.EnvironmentName == "env2" && !e.WasResponseOk && e.ErrorDetails.Equals("Service error", StringComparison.OrdinalIgnoreCase)));
        }

        /// <summary>
        /// If one of the MDCE environments returns no results for any of the queried MIU IDs, it should still appear in the queried environments list.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task PostTextList_OneServerReturnsNoResults()
        {
            var results400000102 = new MultiServerResult<MiuListCollection>();
            results400000102.Results.Add("env1", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = "2017-01-01", SiteId = 0 } } });

            miuListMultiServerProxyMock.Setup(m => m.GetMiuListAsync("400000102", DeviceIdType.MiuId, It.IsAny<IEnumerable<string>>())).ReturnsAsync(results400000102);

            var result = await controller.PostTextList(new PostTextRequestModel { MiuListText = "400000102" });
            var fileContentResult = (FileContentResult)result;

            var canResultCollection = new CanResultCollection();
            using (var ms = new MemoryStream(fileContentResult.FileContents))
            using (var streamReader = new StreamReader(ms))
            using (var csvReader = new CsvReader(streamReader))
            {
                csvReader.Configuration.HasHeaderRecord = false;

                // Read environment details
                ReadQueriedEnvironments(csvReader, canResultCollection);

                // Read report data
                ReadReportResults(csvReader, canResultCollection);
            }

            var returnedMiuResult = canResultCollection.Results.Single(r => r.MiuId == 400000102);

            Assert.AreEqual("env1", returnedMiuResult.Environment);
            Assert.IsTrue(canResultCollection.QueriedEnvironments.Any(e => e.EnvironmentName == "env1" && e.WasResponseOk));
            Assert.IsTrue(canResultCollection.QueriedEnvironments.Any(e => e.EnvironmentName == "env2" && e.WasResponseOk));
        }

        /// <summary>
        /// If one or more environments returned a result for the MIU but it has never been heard from, the environment 
        /// should be shown as "(not heard)" and the other the fields (except MIU ID) should be blanked out.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task PostTextList_MiuHasNeverCalledIn()
        {
            var results400000102 = new MultiServerResult<MiuListCollection>();
            results400000102.Results.Add("env1", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = null, SiteId = 0 } } });
            results400000102.Results.Add("env2", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = null, SiteId = 0 } } });

            miuListMultiServerProxyMock.Setup(m => m.GetMiuListAsync("400000102", DeviceIdType.MiuId, It.IsAny<IEnumerable<string>>())).ReturnsAsync(results400000102);

            var result = await controller.PostTextList(new PostTextRequestModel { MiuListText = "400000102" });
            var fileContentResult = (FileContentResult)result;

            var canResultCollection = new CanResultCollection();
            using (var ms = new MemoryStream(fileContentResult.FileContents))
            using (var streamReader = new StreamReader(ms))
            using (var csvReader = new CsvReader(streamReader))
            {
                csvReader.Configuration.HasHeaderRecord = false;

                // Read environment details
                ReadQueriedEnvironments(csvReader, canResultCollection);

                // Read report data
                ReadReportResults(csvReader, canResultCollection);
            }

            var returnedMiuResult = canResultCollection.Results.Single(r => r.MiuId == 400000102);

            Assert.IsTrue(returnedMiuResult.Environment.Equals("(not heard)", StringComparison.OrdinalIgnoreCase));
            Assert.IsNullOrEmpty(returnedMiuResult.Iccid);
            Assert.IsNullOrEmpty(returnedMiuResult.Imei);
            Assert.IsNull(returnedMiuResult.LastHeard);
            Assert.IsTrue(canResultCollection.QueriedEnvironments.Any(e => e.EnvironmentName == "env1" && e.WasResponseOk));
            Assert.IsTrue(canResultCollection.QueriedEnvironments.Any(e => e.EnvironmentName == "env2" && e.WasResponseOk));
        }

        /// <summary>
        /// Returns error details if an exception is thrown during the query.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task PostTextList_ExceptionThrownByRepository()
        {
            miuListMultiServerProxyMock.Setup(m => m.GetMiuListAsync(It.IsAny<string>(), DeviceIdType.MiuId, It.IsAny<IEnumerable<string>>())).ThrowsAsync(new ApplicationException("TEST"));

            var result = await controller.PostTextList(new PostTextRequestModel { MiuListText = "400000102" });

            var viewResult = (ViewResult) result;

            var model = (PostTextRequestModel)viewResult.Model;

            Assert.IsTrue(model.ErrorFromLastSubmission.Contains("TEST"));
        }

        /// <summary>
        /// Returns the user to the Index view when an empty request is submitted.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task PostTextList_EmptyRequest()
        {
            miuListMultiServerProxyMock.Setup(m => m.GetMiuListAsync(It.IsAny<string>(), DeviceIdType.MiuId, It.IsAny<IEnumerable<string>>())).ThrowsAsync(new ApplicationException("TEST"));

            var result = await controller.PostTextList(new PostTextRequestModel { MiuListText = "" });

            var viewResult = (ViewResult)result;

            Assert.AreEqual("Index", viewResult.ViewName);
        }

        /// <summary>
        /// Shows the ICCID and/or IMEI if all their values are consistent across environments for an MIU that hasn't ever called in.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task PostTextList_MiuHasNeverCalledIn_ServersHaveSameIccidAndImei()
        {
            var results400000102 = new MultiServerResult<MiuListCollection>();
            results400000102.Results.Add("env1", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = null, SiteId = 0, Iccid = "89148000001111111111", Imei = "990000111111111", Msisdn = "13344111111" } } });
            results400000102.Results.Add("env2", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = null, SiteId = 0, Iccid = "89148000001111111111", Imei = "990000111111111", Msisdn = "13344111111" } } });

            miuListMultiServerProxyMock.Setup(m => m.GetMiuListAsync("400000102", DeviceIdType.MiuId, It.IsAny<IEnumerable<string>>())).ReturnsAsync(results400000102);

            var result = await controller.PostTextList(new PostTextRequestModel { MiuListText = "400000102" });
            var fileContentResult = (FileContentResult)result;

            var canResultCollection = new CanResultCollection();
            using (var ms = new MemoryStream(fileContentResult.FileContents))
            using (var streamReader = new StreamReader(ms))
            using (var csvReader = new CsvReader(streamReader))
            {
                csvReader.Configuration.HasHeaderRecord = false;

                // Read environment details
                ReadQueriedEnvironments(csvReader, canResultCollection);

                // Read report data
                ReadReportResults(csvReader, canResultCollection);
            }

            var returnedMiuResult = canResultCollection.Results.Single(r => r.MiuId == 400000102);

            Assert.AreEqual("89148000001111111111", returnedMiuResult.Iccid);
            Assert.AreEqual("990000111111111", returnedMiuResult.Imei);
            Assert.AreEqual("13344111111", returnedMiuResult.Msisdn);
        }

        /// <summary>
        /// Shows the ICCID and/or IMEI if any servers report them for an MIU that hasn't ever called in.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task PostTextList_MiuHasNeverCalledIn_IccidAndImeiReportedInSomeResponses()
        {
            var results400000102 = new MultiServerResult<MiuListCollection>();
            results400000102.Results.Add("env1", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = null, SiteId = 0, Imei = "990000111111111" } } });
            results400000102.Results.Add("env2", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = null, SiteId = 0, Iccid = "89148000001111111111" } } });
            results400000102.Results.Add("env3", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = null, SiteId = 0, Msisdn = "13344111111" } } });

            miuListMultiServerProxyMock.Setup(m => m.GetMiuListAsync("400000102", DeviceIdType.MiuId, It.IsAny<IEnumerable<string>>())).ReturnsAsync(results400000102);

            var result = await controller.PostTextList(new PostTextRequestModel { MiuListText = "400000102" });
            var fileContentResult = (FileContentResult)result;

            var canResultCollection = new CanResultCollection();
            using (var ms = new MemoryStream(fileContentResult.FileContents))
            using (var streamReader = new StreamReader(ms))
            using (var csvReader = new CsvReader(streamReader))
            {
                csvReader.Configuration.HasHeaderRecord = false;

                // Read environment details
                ReadQueriedEnvironments(csvReader, canResultCollection);

                // Read report data
                ReadReportResults(csvReader, canResultCollection);
            }

            var returnedMiuResult = canResultCollection.Results.Single(r => r.MiuId == 400000102);

            Assert.AreEqual("89148000001111111111", returnedMiuResult.Iccid);
            Assert.AreEqual("990000111111111", returnedMiuResult.Imei);
            Assert.AreEqual("13344111111", returnedMiuResult.Msisdn);
        }

        /// <summary>
        /// Highlights any results in which the ICCID or IMEI are reported differently across environments.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task PostTextList_MiuHasNeverCalledIn_IccidAndImeiDoNotMatchAcrossEnvironments()
        {
            var results400000102 = new MultiServerResult<MiuListCollection>();
            results400000102.Results.Add("env1", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = null, SiteId = 0, Iccid = "89148000001111111111", Imei = "990000111111111", Msisdn = "13344111111" } } });
            results400000102.Results.Add("env2", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = null, SiteId = 0, Iccid = "89148000002222222222", Imei = "990000222222222", Msisdn = "13344222222" } } });

            miuListMultiServerProxyMock.Setup(m => m.GetMiuListAsync("400000102", DeviceIdType.MiuId, It.IsAny<IEnumerable<string>>())).ReturnsAsync(results400000102);

            var result = await controller.PostTextList(new PostTextRequestModel { MiuListText = "400000102" });
            var fileContentResult = (FileContentResult)result;

            var canResultCollection = new CanResultCollection();
            using (var ms = new MemoryStream(fileContentResult.FileContents))
            using (var streamReader = new StreamReader(ms))
            using (var csvReader = new CsvReader(streamReader))
            {
                csvReader.Configuration.HasHeaderRecord = false;

                // Read environment details
                ReadQueriedEnvironments(csvReader, canResultCollection);

                // Read report data
                ReadReportResults(csvReader, canResultCollection);
            }

            var returnedMiuResult = canResultCollection.Results.Single(r => r.MiuId == 400000102);

            Assert.AreEqual("MISMATCHED", returnedMiuResult.Iccid);
            Assert.AreEqual("MISMATCHED", returnedMiuResult.Imei);
            Assert.AreEqual("MISMATCHED", returnedMiuResult.Msisdn);
            Assert.IsTrue(returnedMiuResult.MismatchedIds.IndexOf("ICCID", StringComparison.OrdinalIgnoreCase) != -1);
            Assert.IsTrue(returnedMiuResult.MismatchedIds.IndexOf("IMEI", StringComparison.OrdinalIgnoreCase) != -1);
            Assert.IsTrue(returnedMiuResult.MismatchedIds.IndexOf("MSISDN", StringComparison.OrdinalIgnoreCase) != -1);
        }

        /// <summary>
        /// Returns error details if an exception is thrown during the query.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task PostCsvList_ExceptionThrownWhileProcessingCsv()
        {
            var result = await controller.PostCsvList(new FailingHttpPostedFile());

            var viewResult = (ViewResult)result;

            var model = (PostTextRequestModel)viewResult.Model;

            Assert.IsTrue(model.ErrorFromLastSubmission.Contains("CSV ERROR"));
        }

        /// <summary>
        /// Returns error details if an exception is thrown during the query.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task PostCsvList_ExceptionThrownByRepository()
        {
            miuListMultiServerProxyMock.Setup(m => m.GetMiuListAsync(It.IsAny<string>(), DeviceIdType.MiuId, It.IsAny<IEnumerable<string>>())).ThrowsAsync(new ApplicationException("REPO ERROR"));

            var result = await controller.PostTextList(new PostTextRequestModel { MiuListText = "400000102" });

            var viewResult = (ViewResult)result;

            var model = (PostTextRequestModel)viewResult.Model;

            Assert.IsTrue(model.ErrorFromLastSubmission.Contains("REPO ERROR"));
        }

        /// <summary>
        /// Returns the user to the Index view when no file is submitted.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task PostCsvList_NoFileInRequest()
        {
            var result = await controller.PostCsvList(null);

            var viewResult = (ViewResult)result;

            Assert.AreEqual("Index", viewResult.ViewName);
        }

        /// <summary>
        /// Verify that all mIU IDs in the CSV input are processed.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task PostCsvList_AllServersRespond()
        {
            var results400000102 = new MultiServerResult<MiuListCollection>();
            results400000102.Results.Add("env1", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = "2017-01-01", SiteId = 0 } } });
            results400000102.Results.Add("env2", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000102, LastInsertDate = "2017-02-02", SiteId = 0 } } });

            var results400000202 = new MultiServerResult<MiuListCollection>();
            results400000202.Results.Add("env1", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000202, LastInsertDate = "2017-04-01", SiteId = 0 } } });
            results400000202.Results.Add("env2", new MiuListCollection { Mius = new List<MiuDetail> { new MiuDetail { MiuId = 400000202, LastInsertDate = "2017-03-02", SiteId = 0 } } });

            miuListMultiServerProxyMock.Setup(m => m.GetMiuListAsync("400000102", DeviceIdType.MiuId, It.IsAny<IEnumerable<string>>())).ReturnsAsync(results400000102);
            miuListMultiServerProxyMock.Setup(m => m.GetMiuListAsync("400000202", DeviceIdType.MiuId, It.IsAny<IEnumerable<string>>())).ReturnsAsync(results400000202);

            var csv = "MIU ID\r\n400000102\r\n400000202\r\n";
            var result = await controller.PostCsvList(new HttpPostedCsvFile(csv));
            var fileContentResult = (FileContentResult)result;

            var canResultCollection = new CanResultCollection();
            using (var ms = new MemoryStream(fileContentResult.FileContents))
            using (var streamReader = new StreamReader(ms))
            using (var csvReader = new CsvReader(streamReader))
            {
                csvReader.Configuration.HasHeaderRecord = false;

                // Read environment details
                ReadQueriedEnvironments(csvReader, canResultCollection);

                // Read report data
                ReadReportResults(csvReader, canResultCollection);
            }

            var returnedMiuResult1 = canResultCollection.Results.Single(r => r.MiuId == 400000102);
            var returnedMiuResult2 = canResultCollection.Results.Single(r => r.MiuId == 400000202);

            Assert.AreEqual("env2", returnedMiuResult1.Environment);
            Assert.AreEqual("env1", returnedMiuResult2.Environment);
            Assert.IsTrue(canResultCollection.QueriedEnvironments.Any(e => e.EnvironmentName == "env1" && e.WasResponseOk));
            Assert.IsTrue(canResultCollection.QueriedEnvironments.Any(e => e.EnvironmentName == "env2" && e.WasResponseOk));
        }

        private static void ReadReportResults(CsvReader csvReader, CanResultCollection canResultCollection)
        {
            csvReader.Read(); // Skip header

            while (csvReader.Read())
            {
                if (string.IsNullOrEmpty(csvReader[0]))
                {
                    continue;
                }

                DateTime lastHeard;
                DateTime? lastHeardNullable;
                if (DateTime.TryParse(csvReader[1], out lastHeard))
                {
                    lastHeardNullable = lastHeard;
                }
                else
                {
                    lastHeardNullable = null;
                }

                canResultCollection.Results.Add(new CanResult
                {
                    MiuId = int.Parse(RemoveExcelCharacters(csvReader[0])),
                    Environment = csvReader[1],
                    LastHeard = lastHeardNullable,
                    Iccid = RemoveExcelCharacters(csvReader[3]),
                    Imei = RemoveExcelCharacters(csvReader[4]),
                    Msisdn = RemoveExcelCharacters(csvReader[5]),
                    MismatchedIds = csvReader[6]
                });
            }
        }

        private static void ReadQueriedEnvironments(CsvReader csvReader, CanResultCollection canResultCollection)
        {
            csvReader.Read(); // Skip header

            while (csvReader.Read())
            {
                if (string.IsNullOrEmpty(csvReader[0]))
                {
                    break;
                }

                canResultCollection.QueriedEnvironments.Add(new QueriedEnvironment
                {
                    EnvironmentName = csvReader[0],
                    WasResponseOk = csvReader[1].StartsWith("Y", StringComparison.OrdinalIgnoreCase) ? true : false,
                    ErrorDetails = csvReader[2]
                });
            }
        }

        private static string RemoveExcelCharacters(string field)
        {
            return field.Replace("\"", "").Replace("=", "");
        }

        /// <summary>
        /// Simulates a posted file that causes an exception while being read.
        /// </summary>
        private class FailingHttpPostedFile : HttpPostedFileBase
        {
            public override Stream InputStream
            {
                get { throw new ApplicationException("CSV ERROR"); }
            }
        }

        /// <summary>
        /// Wraps the supplied text as an HTTP-posted CSV file.
        /// </summary>
        private class HttpPostedCsvFile : HttpPostedFileBase
        {
            private readonly Stream stream;

            public HttpPostedCsvFile(string csvData)
            {
                stream = new MemoryStream(new UTF8Encoding().GetBytes(csvData));
            }

            public override Stream InputStream
            {
                get { return stream; }
            }

            public override string ContentType
            {
                get { return "text/csv"; }
            }

            public override int ContentLength
            {
                get { return (int)stream.Length; }
            }
        }
    }
}