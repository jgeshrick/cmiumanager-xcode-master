﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using Moq;
using Ntg.BpcCore.Settings;
using Ntg.BpcWebService.Controllers;
using Ntg.BpcWebService.Models;
using NUnit.Framework;

namespace Ntg.BpcWebService.UnitTests.Controllers
{
    [TestFixture]
    public class ConfigApiControllerTest
    {
        private const string EnvironmentName = "Unit Tests";
        private MdceEnvironment testEnvironment;
        private Mock<IEnvironmentsConfig> environmentsConfigMock;
        private Mock<IEnvironmentSettingsRepository> environmentSettingsRepositoryMock;
        private ConfigApiController configApiController;

        [SetUp]
        public void SetUp()
        {
            // Prepare a single mocked environment configuration.
            testEnvironment = new MdceEnvironment
            {
                Name = EnvironmentName,
                BaseUrl = "http://localhost",
                IsEnabled = true
            };
            var environmentList = new List<MdceEnvironment> {testEnvironment};

            environmentsConfigMock = new Mock<IEnvironmentsConfig>();
            environmentsConfigMock.SetupGet(m => m.EnvironmentList).Returns(environmentList);

            // Settings repository mock with default behaviors
            environmentSettingsRepositoryMock = new Mock<IEnvironmentSettingsRepository>();
            
            // Object under test
            configApiController = new ConfigApiController(environmentsConfigMock.Object, environmentSettingsRepositoryMock.Object);
        }

        /// <summary>
        /// Verify that the PostPartnerString even is raised when the partner string setting is changed.
        /// </summary>
        [Test]
        public void PostPartnerString_WhenCalled_RaisesPartnerStringChangedEvent()
        {
            // Arrange
            const string expected = "12345";
            string actual = null;
            testEnvironment.ParnerStringChanged += (sender, args) => { actual = args.NewPartnerString; };
            var partnerStringSetting = new PartnerStringSetting
            {
                EnvironmentName = EnvironmentName,
                PartnerString = expected
            };

            // Act
            configApiController.PostPartnerString(partnerStringSetting);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}