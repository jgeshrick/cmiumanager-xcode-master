﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Security.Principal;
using Moq;
using Ntg.BpcWebService.Authorization;
using NUnit.Framework;

namespace Ntg.BpcWebService.UnitTests.Authorization
{
    [TestFixture]
    public class AccessTypeResolverTest
    {
        private Mock<IPrincipal> principalMock;
        private AccessTypeResolver accessTypeResolver;
        private const string ExampleAdminRolesSetting = @"DOMAIN\BpcAdmin, DOMAIN\FindMyMiuAdmin";

        [SetUp]
        public void SetUp()
        {
            principalMock = new Mock<IPrincipal>();
        }

        /// <summary>
        /// If no admin roles have been configured, IsAdminUser(IPrincipal) returns false.
        /// </summary>
        [Test]
        public void IsAdminUser_WhenNoAdminRolesAreConfigured_ReturnsFalse()
        {
            // Arrange
            accessTypeResolver = new AccessTypeResolver(string.Empty);
            principalMock.Setup(m => m.IsInRole(It.IsAny<string>())).Returns(true);

            // Act
            var isAdmin = accessTypeResolver.IsAdminUser(principalMock.Object);

            // Assert
            Assert.IsFalse(isAdmin);
        }

        /// <summary>
        /// If the user is not a member of a BPC admin security group, IsAdminUser(IPrincipal) returns false.
        /// </summary>
        [Test]
        public void IsAdminUser_WhenUserIsNotInAnyAdminRole_ReturnsFalse()
        {
            // Arrange
            accessTypeResolver = new AccessTypeResolver(ExampleAdminRolesSetting);
            principalMock.Setup(m => m.IsInRole(It.IsAny<string>())).Returns(false);

            // Act
            var isAdmin = accessTypeResolver.IsAdminUser(principalMock.Object);

            // Assert
            Assert.IsFalse(isAdmin);
        }

        /// <summary>
        /// If the user is a member of a BPC admin security group, IsAdminUser(IPrincipal) returns true.
        /// </summary>
        [Test]
        public void IsAdminUser_WhenUserIsAdminRoleMember_ReturnsTrue()
        {
            // Arrange
            accessTypeResolver = new AccessTypeResolver(ExampleAdminRolesSetting);
            principalMock.Setup(m => m.IsInRole(It.IsAny<string>())).Returns(false);
            principalMock.Setup(m => m.IsInRole(@"DOMAIN\BpcAdmin")).Returns(true);

            // Act
            var isAdmin = accessTypeResolver.IsAdminUser(principalMock.Object);

            // Assert
            Assert.IsTrue(isAdmin);
        }
    }
}
