﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using Ntg.BpcWebService.Models;
using NUnit.Framework;

namespace Ntg.BpcWebService.UnitTests.Models
{
    [TestFixture]
    public class TimeZoneFormatterTest
    {
        private const string CentralStandardTimeId = "Central Standard Time";

        /// <summary>
        /// Verify conversion from DateTime to string (including daylight savings).
        /// </summary>
        [Test]
        public void ToConfiguredTimeZone_WithUtcDateTime_ReturnsExpectedLocalTime()
        {
            // Arrange
            var formatter = new TimeZoneFormatter(CentralStandardTimeId);
            DateTime utcDateTimeInUsWinter = new DateTime(2000, 01, 01, 12, 00, 00, DateTimeKind.Utc); // Midday, Jan 1st, 2000
            DateTime utcDateTimeInUsSummer = new DateTime(2000, 06, 01, 12, 00, 00, DateTimeKind.Utc); // Midday, June 1st, 2000
            const string expectedCst = "2000-01-01 06:00:00 -06:00"; // CST offset
            const string expectedCdt = "2000-06-01 07:00:00 -05:00"; // CDT offset

            // Act
            var cstResult = formatter.ToConfiguredTimeZone(utcDateTimeInUsWinter);
            var cdtResult = formatter.ToConfiguredTimeZone(utcDateTimeInUsSummer);

            // Assert
            Assert.AreEqual(expectedCst, cstResult);
            Assert.AreEqual(expectedCdt, cdtResult);
        }

        /// <summary>
        /// Verify that null is returned when the DateTime input is null.
        /// </summary>
        [Test]
        public void ToConfiguredTimeZone_WithNullDateTime_ReturnsNull()
        {
            // Arrange
            var formatter = new TimeZoneFormatter(CentralStandardTimeId);

            // Act
            var result = formatter.ToConfiguredTimeZone((DateTime?) null);

            // Assert
            Assert.IsNull(result);
        }

        /// <summary>
        /// Verify conversion from a UTC string to local string (including daylight savings).
        /// </summary>
        [Test]
        public void ToConfiguredTimeZone_WithDateTimeOffsetString_ReturnsExpectedLocalTime()
        {
            // Arrange
            var formatter = new TimeZoneFormatter(CentralStandardTimeId);
            string utcDateTimeInUsWinter = "2000-01-01 12:00:00 +00:00";
            string utcDateTimeInUsSummer = "2000-06-01 12:00:00 +00:00";
            const string expectedCst = "2000-01-01 06:00:00 -06:00"; // CST offset
            const string expectedCdt = "2000-06-01 07:00:00 -05:00"; // CDT offset

            // Act
            var cstResult = formatter.ToConfiguredTimeZone(utcDateTimeInUsWinter);
            var cdtResult = formatter.ToConfiguredTimeZone(utcDateTimeInUsSummer);

            // Assert
            Assert.AreEqual(expectedCst, cstResult);
            Assert.AreEqual(expectedCdt, cdtResult);
        }

        /// <summary>
        /// Verify conversion from a non-UTC string to local string (including daylight savings).
        /// </summary>
        [Test]
        public void ToConfiguredTimeZone_WithNonUtcString_ReturnsExpectedLocalTime()
        {
            // Arrange
            var formatter = new TimeZoneFormatter(CentralStandardTimeId);
            string nonUtcDateTimeInUsWinter = "2000-01-01 04:00:00 -08:00"; // PST
            string nonUtcDateTimeInUsSummer = "2000-06-01 05:00:00 -07:00"; // PDT
            const string expectedCst = "2000-01-01 06:00:00 -06:00"; // CST offset
            const string expectedCdt = "2000-06-01 07:00:00 -05:00"; // CDT offset

            // Act
            var cstResult = formatter.ToConfiguredTimeZone(nonUtcDateTimeInUsWinter);
            var cdtResult = formatter.ToConfiguredTimeZone(nonUtcDateTimeInUsSummer);

            // Assert
            Assert.AreEqual(expectedCst, cstResult);
            Assert.AreEqual(expectedCdt, cdtResult);
        }

        /// <summary>
        /// Verify that UTC is assumed if a string is supplied without a time offset.
        /// </summary>
        [Test]
        public void ToConfiguredTimeZone_WithDateTimeString_AssumesUtcInput()
        {
            // Arrange
            var formatter = new TimeZoneFormatter(CentralStandardTimeId);
            const string dateTimeStringWithoutOffset = "2000-01-01 12:00:00";
            const string expected = "2000-01-01 06:00:00 -06:00";

            // Act
            var cstResult = formatter.ToConfiguredTimeZone(dateTimeStringWithoutOffset);

            // Assert
            Assert.AreEqual(expected, cstResult);
        }

        /// <summary>
        /// Verify that the original string is returned if it doesn't parse to a DateTime.
        /// </summary>
        [Test]
        public void ToConfiguredTimeZone_WithNonDateTimeAsInput_ReturnsOriginalString()
        {
            // Arrange
            var formatter = new TimeZoneFormatter(CentralStandardTimeId);
            const string nonDateTime = "Other Data";

            // Act
            var result = formatter.ToConfiguredTimeZone(nonDateTime);

            // Assert
            Assert.AreEqual(nonDateTime, result);
        }

        /// <summary>
        /// Verify that null is returned if the supplied string is null.
        /// </summary>
        [Test]
        public void ToConfiguredTimeZone_WithNullString_ReturnsNull()
        {
            // Arrange
            var formatter = new TimeZoneFormatter(CentralStandardTimeId);

            // Act
            var result = formatter.ToConfiguredTimeZone((string) null);

            // Assert
            Assert.IsNull(result);
        }
    }
}