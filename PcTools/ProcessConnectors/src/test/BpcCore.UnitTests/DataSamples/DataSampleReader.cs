﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace Ntg.BpcCore.UnitTests.DataSamples
{
    /// <summary>
    /// A helper class to read embedded JSON files.
    /// </summary>
    public static class DataSampleReader
    {
        /// <summary>
        /// Helper method to deserialize the embedded JSON resource file with the supplied fileName.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static T Get<T>(string fileName)
        {
            string json = GetText(fileName);

            if (json != null)
            {
                var result = JsonConvert.DeserializeObject<T>(json);
                return result;
            }

            return default(T);
        }

        /// <summary>
        /// Helper method to return the string content of the embedded resource file with the supplied fileName.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetText(string fileName)
        {
            string embeddedPath = string.Format("Ntg.BpcCore.UnitTests.DataSamples.{0}", fileName);
            var assembly = Assembly.GetExecutingAssembly();

            string text = null;
            using (Stream stream = assembly.GetManifestResourceStream(embeddedPath))
            {
                if (stream != null)
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        text = reader.ReadToEnd();
                    }
                }
            }

            return text;
        }
    }
}