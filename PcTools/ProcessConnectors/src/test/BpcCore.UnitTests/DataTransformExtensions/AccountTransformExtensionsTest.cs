﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using System.Linq;
using Ntg.BpcCore.DataTransformExtensions.CustomerData;
using Ntg.BpcCore.Entities.CustomerData;
using Ntg.BpcCore.UnitTests.DataSamples;
using NUnit.Framework;

namespace Ntg.BpcCore.UnitTests.DataTransformExtensions
{
    [TestFixture]
    public class AccountTransformExtensionsTest
    {
        /// <summary>
        /// Verify that a distributor account is mapped to the Distributers list only.
        /// </summary>
        [Test, Ignore("Temporary workaround breaks this test.")] // TODO: Temporary workaround breaks this test.
        public void ToCrmReference_WhenParentIdIsNull_MapsToDistributersList()
        {
            // Arrange
            var accounts = DataSampleReader.Get<List<Account>>("Account.Distributor.A2ZNXA200000.json");

            // Act
            var crmData = accounts.ToCrmReference();

            // Assert
            Assert.AreEqual(1, crmData.Distributers.Count);
            Assert.AreEqual(0, crmData.Utilities.Count);
        }

        /// <summary>
        /// Verify that a utility account is mapped to the Utilities list only.
        /// </summary>
        [Test]
        public void ToCrmReference_WhenParentIdIsNotNull_MapsToUtilitiesList()
        {
            // Arrange
            var accounts = DataSampleReader.Get<List<Account>>("Account.Utility.A2ZNXA400002.json");

            // Act
            var crmData = accounts.ToCrmReference();

            // Assert
            Assert.AreEqual(1, crmData.Utilities.Count);
            Assert.AreEqual(0, crmData.Distributers.Count);
        }

        /// <summary>
        /// Verify that a distributer has its list of customer numbers populated as expected.
        /// </summary>
        [Test]
        public void ToCrmReference_WhenParentCustomerNumberIsNotNull_PopulatesCustomerNumbersOnParent()
        {
            // Arrange
            var accounts = DataSampleReader.Get<List<Account>>("Account.WithParentCustomerNumber.json");

            // Act
            var crmData = accounts.ToCrmReference();

            var distributor = crmData.Distributers.Single(d => d.CustomerNumber == "02216500");

            // Assert
            Assert.AreEqual(1, distributor.Info.CustomerNumbers.Count); // Expect a single customer number
            Assert.AreEqual("10902000", distributor.Info.CustomerNumbers.Single()); // Verify it's the correct customer number
        }

        /// <summary>
        /// Verify that a distributer does not have any customer numbers if no other accounts reference it as a parent.
        /// </summary>
        [Test]
        public void ToCrmReference_WhenDistributerCustomerNumberIsNotReferencedAsParent_DoesNotHaveCustomerNumbers()
        {
            // Arrange
            var accounts = DataSampleReader.Get<List<Account>>("Account.WithParentCustomerNumber.json");

            // Act
            var crmData = accounts.ToCrmReference();

            // Get the expected objects from the CrmReference instance.
            var distributor = crmData.Distributers.Single(d => d.CustomerNumber == "02281300");

            // Assert
            Assert.AreEqual(0, distributor.Info.CustomerNumbers.Count); // The distributor shouldn't have any customer numbers
        }
    }
}
