﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using Ntg.BpcCore.Settings;
using NUnit.Framework;

namespace Ntg.BpcCore.UnitTests.Settings
{
    [TestFixture]
    public class ScheduleConfigurationTest
    {
        [Test]
        public void GetNext_ReturnsExpectedDailyEventTime()
        {
            // Arrange - daily schedule at 08:00 UTC
            var sc = new ScheduleConfiguration
            {
                DataConnectorType = DataConnectorType.CostRevenueDataConnector,
                ScheduleType = ScheduleType.Daily,
                StartTimeAsString = "08:00:00"
            };

            var afternoonBefore = DateTimeOffset.Parse("2016-11-30T15:00:00Z").UtcDateTime;
            var justBefore = DateTimeOffset.Parse("2016-12-01T07:59:59Z").UtcDateTime;
            var atSameTime = DateTimeOffset.Parse("2016-12-01T08:00:00Z").UtcDateTime;
            var justAfter = DateTimeOffset.Parse("2016-12-01T08:00:01Z").UtcDateTime;

            var expectedToday = DateTimeOffset.Parse("2016-12-01T08:00:00Z").UtcDateTime;
            var expectedTomorrow = DateTimeOffset.Parse("2016-12-02T08:00:00Z").UtcDateTime;

            // Act
            var nextDateTime1 = sc.GetNext(afternoonBefore);
            var nextDateTime2 = sc.GetNext(justBefore);
            var nextDateTime3 = sc.GetNext(atSameTime);
            var nextDateTime4 = sc.GetNext(justAfter);

            // Assert
            Assert.AreEqual(expectedToday, nextDateTime1);
            Assert.AreEqual(expectedToday, nextDateTime2);
            Assert.AreEqual(expectedTomorrow, nextDateTime3);
            Assert.AreEqual(expectedTomorrow, nextDateTime4);
        }

        [Test]
        public void GetNext_ReturnsExpectedDailyEventTime_WithRepetitionConfig()
        {
            // Arrange - daily schedule at 08:00 UTC, repeats every 10 minutes for an hour.
            var sc = new ScheduleConfiguration
            {
                DataConnectorType = DataConnectorType.CostRevenueDataConnector,
                ScheduleType = ScheduleType.Daily,
                StartTimeAsString = "08:00:00",
                RepeatIntervalAsString = "0:10:00",
                RepeatDurationAsString = "1:00:00"
            };

            var beforeFirstTick = DateTimeOffset.Parse("2016-12-01T23:00:00Z").UtcDateTime;
            var beforeSecondTick = DateTimeOffset.Parse("2016-12-02T08:08:57Z").UtcDateTime;
            var beforeLastTick = DateTimeOffset.Parse("2016-12-02T08:51:00Z").UtcDateTime;
            var afterLastTick = DateTimeOffset.Parse("2016-12-02T09:01:00Z").UtcDateTime;

            var expectedFirstTick = DateTimeOffset.Parse("2016-12-02T08:00:00Z").UtcDateTime;
            var expectedSecondTick = DateTimeOffset.Parse("2016-12-02T08:10:00Z").UtcDateTime;
            var expectedLastTick = DateTimeOffset.Parse("2016-12-02T09:00:00Z").UtcDateTime;
            var expectedNextDayTick = DateTimeOffset.Parse("2016-12-03T08:00:00Z").UtcDateTime;

            // Act
            var nextDateTime1 = sc.GetNext(beforeFirstTick);
            var nextDateTime2 = sc.GetNext(beforeSecondTick);
            var nextDateTime3 = sc.GetNext(beforeLastTick);
            var nextDateTime4 = sc.GetNext(afterLastTick);

            // Assert
            Assert.AreEqual(expectedFirstTick, nextDateTime1);
            Assert.AreEqual(expectedSecondTick, nextDateTime2);
            Assert.AreEqual(expectedLastTick, nextDateTime3);
            Assert.AreEqual(expectedNextDayTick, nextDateTime4);
        }

        [Test]
        public void GetNext_ReturnsExpectedMonthlyEventTime()
        {
            // Arrange - monthly schedule on 1st of each month at 08:00 UTC
            var sc = new ScheduleConfiguration
            {
                DataConnectorType = DataConnectorType.CostRevenueDataConnector,
                ScheduleType = ScheduleType.Monthly,
                DaysOfMonthAsString = "1",
                StartTimeAsString = "08:00:00"
            };

            var monthBefore = DateTimeOffset.Parse("2016-11-30T07:00:00Z").UtcDateTime;
            var justBefore = DateTimeOffset.Parse("2016-12-01T07:59:59Z").UtcDateTime;
            var atSameTime = DateTimeOffset.Parse("2016-12-01T08:00:00Z").UtcDateTime;
            var justAfter = DateTimeOffset.Parse("2016-12-01T08:00:01Z").UtcDateTime;

            var expectedThisMonth = DateTimeOffset.Parse("2016-12-01T08:00:00Z").UtcDateTime;
            var expectedNextMonth = DateTimeOffset.Parse("2017-01-01T08:00:00Z").UtcDateTime;

            // Act
            var nextDateTime1 = sc.GetNext(monthBefore);
            var nextDateTime2 = sc.GetNext(justBefore);
            var nextDateTime3 = sc.GetNext(atSameTime);
            var nextDateTime4 = sc.GetNext(justAfter);

            // Assert
            Assert.AreEqual(expectedThisMonth, nextDateTime1);
            Assert.AreEqual(expectedThisMonth, nextDateTime2);
            Assert.AreEqual(expectedNextMonth, nextDateTime3);
            Assert.AreEqual(expectedNextMonth, nextDateTime4);
        }

        [Test]
        public void GetNext_ReturnsExpectedMonthlyEventTime_WithRepetitionConfig()
        {
            // Arrange - monthly schedule on 1st, 5th and 8th of each month at 08:00 UTC, repeats every 10 minutes for an hour.
            var sc = new ScheduleConfiguration
            {
                DataConnectorType = DataConnectorType.CostRevenueDataConnector,
                ScheduleType = ScheduleType.Monthly,
                DaysOfMonthAsString = "1, 5, 8",
                StartTimeAsString = "08:00:00",
                RepeatIntervalAsString = "0:10:00",
                RepeatDurationAsString = "1:00:00"
            };

            var beforeFirstTick = DateTimeOffset.Parse     ("2016-11-30T23:00:00Z").UtcDateTime;
            var beforeSecondTick = DateTimeOffset.Parse    ("2016-12-01T08:08:57Z").UtcDateTime;
            var beforeLastTick = DateTimeOffset.Parse      ("2016-12-01T08:51:00Z").UtcDateTime;
            var afterLastTickOfDay = DateTimeOffset.Parse  ("2016-12-01T09:01:00Z").UtcDateTime;
            var afterLastTickOfMonth = DateTimeOffset.Parse("2016-12-08T09:01:00Z").UtcDateTime;

            var expectedFirstTick = DateTimeOffset.Parse         ("2016-12-01T08:00:00Z").UtcDateTime;
            var expectedSecondTick = DateTimeOffset.Parse        ("2016-12-01T08:10:00Z").UtcDateTime;
            var expectedLastTick = DateTimeOffset.Parse          ("2016-12-01T09:00:00Z").UtcDateTime;
            var expectedNextDayOfMonthTick = DateTimeOffset.Parse("2016-12-05T08:00:00Z").UtcDateTime;
            var expectedNextMonthTick = DateTimeOffset.Parse     ("2017-01-01T08:00:00Z").UtcDateTime;

            // Act
            var nextDateTime1 = sc.GetNext(beforeFirstTick);
            var nextDateTime2 = sc.GetNext(beforeSecondTick);
            var nextDateTime3 = sc.GetNext(beforeLastTick);
            var nextDateTime4 = sc.GetNext(afterLastTickOfDay);
            var nextDateTime5 = sc.GetNext(afterLastTickOfMonth);

            // Assert
            Assert.AreEqual(expectedFirstTick, nextDateTime1);
            Assert.AreEqual(expectedSecondTick, nextDateTime2);
            Assert.AreEqual(expectedLastTick, nextDateTime3);
            Assert.AreEqual(expectedNextDayOfMonthTick, nextDateTime4);
            Assert.AreEqual(expectedNextMonthTick, nextDateTime5);
        }

        [Test]
        public void ToString_ReturnsExpectedDailyScheduleDescription()
        {
            // Arrange - daily schedule at 08:00 UTC, repeats every 10 minutes for an hour.
            var sc = new ScheduleConfiguration
            {
                DataConnectorType = DataConnectorType.CostRevenueDataConnector,
                ScheduleType = ScheduleType.Daily,
                StartTimeAsString = "08:00:00",
                RepeatIntervalAsString = "0:10:00",
                RepeatDurationAsString = "1:00:00"
            };

            // Act
            string result = sc.ToString();

            // Assert
            Assert.AreEqual("Daily at 8:00:00 UTC, repeating every 0:10:00 until 9:00:00 UTC", result);
        }

        [Test]
        public void ToString_ReturnsExpectedMonthlyScheduleDescription()
        {
            // Arrange - monthly schedule on 1st, 5th and 8th of each month at 08:00 UTC, repeats every 10 minutes for an hour.
            var sc = new ScheduleConfiguration
            {
                DataConnectorType = DataConnectorType.CostRevenueDataConnector,
                ScheduleType = ScheduleType.Monthly,
                DaysOfMonthAsString = "1, 5, 8",
                StartTimeAsString = "08:00:00",
                RepeatIntervalAsString = "0:10:00",
                RepeatDurationAsString = "1:00:00"
            };

            // Act
            string result = sc.ToString();

            // Assert
            Assert.AreEqual("Monthly on the 1, 5, 8 of each month at 8:00:00 UTC, repeating every 0:10:00 until 9:00:00 UTC", result);
        }
    }
}