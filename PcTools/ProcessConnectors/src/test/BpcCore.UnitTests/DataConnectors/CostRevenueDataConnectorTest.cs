﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using Ntg.BpcCore.DataConnectors;
using Ntg.BpcCore.Domain;
using Ntg.BpcCore.JsonModel;
using Ntg.BpcCore.JsonModel.ConfigHistory;
using Ntg.BpcCore.JsonModel.LifecycleHistory;
using Ntg.BpcCore.JsonModel.OwnerHistory;
using Ntg.BpcCore.Settings;
using NUnit.Framework;

namespace Ntg.BpcCore.UnitTests.DataConnectors
{
    [TestFixture]
    public class CostRevenueDataConnectorTest
    {
        private Mock<IMdceEnvironmentBpcRestClient> bpcRestClientMock;
        private Mock<ICostRevenueDataRepository> crDataRepositoryMock;
        private IDataConnector dataConnector;

        [SetUp]
        public void InitializeTest()
        {
            // Mocks
            bpcRestClientMock = new Mock<IMdceEnvironmentBpcRestClient>();
            crDataRepositoryMock = new Mock<ICostRevenueDataRepository>();

            var testEnvironment = new MdceEnvironment
            {
                Name = "TEST",
                IsEnabled = true,
                BaseUrl = "http://localhost:9999"
            };

            bpcRestClientMock.SetupGet(e => e.Environment).Returns(testEnvironment);

            bpcRestClientMock
                .Setup(m => m.GetConfigurationHistoryAsync(It.IsAny<int?>()))
                .ReturnsAsync(new List<SequentialMiuConfig>());

            bpcRestClientMock
                .Setup(m => m.GetMiuOwnerHistoryAsync(It.IsAny<int?>()))
                .ReturnsAsync(new SequentialMiuOwnerCollection{OwnerHistory = new List<SequentialMiuOwner>()});

            bpcRestClientMock
                .Setup(m => m.GetLifecycleHistoryAsync(It.IsAny<int?>()))
                .ReturnsAsync(new List<SequentialMiuLifecycleState>());

            // Object under test
            dataConnector = new CostRevenueDataConnector(bpcRestClientMock.Object, crDataRepositoryMock.Object);
        }

        [Test]
        public void Execute_AddsExpectedMiuDetailsToRepository()
        {
            InitializeRestClientsMock();

            // Arrange the ICostRevenueDataRepository mock to record what's being sent to the database.
            var receivedEntities = new List<MiuDetail>();
            crDataRepositoryMock
                .Setup(r => r.SetMiuDetailsAsync(It.IsAny<IEnumerable<MiuDetail>>()))
                .Callback<IEnumerable<MiuDetail>>(miuDetails => { receivedEntities.AddRange(miuDetails); })
                .Returns(Task.FromResult(default(object)));

            // Execute the data connector
            var result = dataConnector.Execute();

            // Assert that the expect 3 MIUs were sent to the C&R repository
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Value);

            Assert.AreEqual(3, receivedEntities.Count);

            Assert.IsTrue(receivedEntities.Any(e => e.MiuId == 400000100));
            Assert.IsTrue(receivedEntities.Any(e => e.MiuId == 400000200));
            Assert.IsTrue(receivedEntities.Any(e => e.MiuId == 400000300));
        }

        private void InitializeRestClientsMock()
        {
            var testEnvironment = new MdceEnvironment
            {
                Name = "TestEnvironment", IsEnabled = true, BaseUrl = "http://localhost"
            };

            bpcRestClientMock.Setup(c => c.GetMiuListAsync(MiuListResponseType.IncludeCellularIdentities)).ReturnsAsync(new MiuListCollection
            {
                Mius = new List<MiuDetail>
                {
                    new MiuDetail {MiuId = 400000100, SiteId = 0},
                    new MiuDetail {MiuId = 400000200, SiteId = 1},
                    new MiuDetail {MiuId = 400000300, SiteId = 2}
                }
            });

            bpcRestClientMock.SetupGet(c => c.Environment).Returns(testEnvironment);
        }
    }
}