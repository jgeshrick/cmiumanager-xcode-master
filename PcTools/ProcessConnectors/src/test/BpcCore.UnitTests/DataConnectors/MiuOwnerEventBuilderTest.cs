﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using Ntg.BpcCore.DataConnectors;
using Ntg.BpcCore.Entities.CostRevenueData;
using NUnit.Framework;

namespace Ntg.BpcCore.UnitTests.DataConnectors
{
    [TestFixture]
    public class MiuOwnerEventBuilderTest
    {
        private MiuOwnerEventBuilder builder;

        [SetUp]
        public void InitializeTest()
        {
            builder = new MiuOwnerEventBuilder();
        }

        [Test]
        public void ProcessMiuOwnerHistory_WhenAllConfigsHaveBeenConsidered_DoesNotAddSiteIdChanges()
        {
            var ownerHistory = Get3ProcessedRecords();
            builder.ProcessMiuOwnerHistory(ownerHistory);

            Assert.IsEmpty(builder.SiteIdChanges);
            Assert.IsEmpty(builder.UpdatedMiuOwnerHistory);
        }

        [Test]
        public void ProcessMiuOwnerHistory_With3NewRecords_AddsExpectedSiteIdChanges()
        {
            var ownerHistory = Get3NewRecordsWithChanges();
            builder.ProcessMiuOwnerHistory(ownerHistory);

            // Expect two site ID changes matching the supplied values
            var changes = builder.SiteIdChanges.OrderBy(c => c.Timestamp).ToList();
            Assert.AreEqual(1, builder.SiteIdChanges.Count(c => c.SiteId == 0));
            Assert.AreEqual(1, builder.SiteIdChanges.Count(c => c.SiteId == 1));

            // The first site ID change should align with the first entry
            Assert.AreEqual(ownerHistory[0].MiuId, changes[0].MiuId);
            Assert.AreEqual(ownerHistory[0].SiteId, changes[0].SiteId);
            Assert.AreEqual(ownerHistory[0].Timestamp, changes[0].Timestamp);

            // The second site ID change should align with the third entry
            Assert.AreEqual(ownerHistory[2].MiuId, changes[1].MiuId);
            Assert.AreEqual(ownerHistory[2].SiteId, changes[1].SiteId);
            Assert.AreEqual(ownerHistory[2].Timestamp, changes[1].Timestamp);

            // All rows should have a value on ConsideredForTransferQueue.
            Assert.IsEmpty(builder.UpdatedMiuOwnerHistory.Where(c => !c.ConsideredForTransferQueue.HasValue));
        }

        [Test]
        public void ProcessMiuOwnerHistory_WhenNewChangeIsInserted_AddsExpectedSiteIdChange()
        {
            var ownerHistory = GetNewOwnerInsertedBetweenExistingProcessedRecords();
            builder.ProcessMiuOwnerHistory(ownerHistory);

            // Expect one owner change to site ID 5
            var changes = builder.SiteIdChanges.OrderBy(c => c.Timestamp).ToList();
            Assert.AreEqual(1, changes.Count);
            Assert.AreEqual(5, changes[0].SiteId);

            // The 2nd and 4th records should be marked as dirty:
            // - The 4th record in the list (chronologically the 2nd) is the new one
            // - The 2nd record in the list (now chronologically the 3rd) has been considered for the queue again
            Assert.AreEqual(2, builder.UpdatedMiuOwnerHistory.Count());
            Assert.IsTrue(builder.UpdatedMiuOwnerHistory.Any(c => c == ownerHistory[3]));
            Assert.IsTrue(builder.UpdatedMiuOwnerHistory.Any(c => c == ownerHistory[1]));
        }

        [Test]
        public void ProcessMiuOwnerHistory_WhenNewChangeIsInsertedBetweenDuplicates_AddsExpectedSiteIdChanges()
        {
            var ownerHistory = GetNewOwnerInsertedBetweenExistingProcessedRecordsWithDuplicate();
            builder.ProcessMiuOwnerHistory(ownerHistory);

            // Expect one owner change to site ID 5
            var changes = builder.SiteIdChanges.OrderBy(c => c.Timestamp).ToList();
            Assert.AreEqual(2, changes.Count);
            Assert.AreEqual(5, changes[0].SiteId);
            Assert.AreEqual(10, changes[1].SiteId);

            // The 2nd, 3rd and 4th records should be marked as dirty
            Assert.AreEqual(3, builder.UpdatedMiuOwnerHistory.Count());
            Assert.IsTrue(builder.UpdatedMiuOwnerHistory.Any(c => c == ownerHistory[1]));
            Assert.IsTrue(builder.UpdatedMiuOwnerHistory.Any(c => c == ownerHistory[2]));
            Assert.IsTrue(builder.UpdatedMiuOwnerHistory.Any(c => c == ownerHistory[3]));
        }

        [Test]
        public void ProcessMiuOwnerHistory_WithSingleUnprocessedRecord_CreatesSingleEventRecord()
        {
            var ownerHistory = ScaffoldTestRecords(1).ToList();
            builder.ProcessMiuOwnerHistory(ownerHistory);

            // Expect one owner change to site ID 5
            var changes = builder.SiteIdChanges.OrderBy(c => c.Timestamp).ToList();
            Assert.AreEqual(1, changes.Count);
            Assert.AreEqual(0, changes[0].SiteId);

            // The 2nd, 3rd and 4th records should be marked as dirty
            Assert.AreEqual(1, builder.UpdatedMiuOwnerHistory.Count());
            Assert.IsTrue(builder.UpdatedMiuOwnerHistory.Any(c => c == ownerHistory[0]));
        }

        /// <summary>
        /// Gets a set of 3 configurations, all with ConsideredForTransferQueue=true.
        /// </summary>
        /// <returns></returns>
        private static List<MiuOwnerHistory> Get3ProcessedRecords()
        {
            var testRecords = ScaffoldTestRecords(3)
                .Select(r =>
                {
                    r.ConsideredForTransferQueue = DateTime.UtcNow;
                    return r;
                })
                .ToList();

            return testRecords;
        }

        /// <summary>
        /// Gets a set of 3 unprocessed configurations, with one mode change and one site ID change.
        /// </summary>
        /// <returns></returns>
        private static List<MiuOwnerHistory> Get3NewRecordsWithChanges()
        {
            var testRecords = ScaffoldTestRecords(3).ToList();

            // Change site ID in the last record.
            testRecords.Last().SiteId = 1;

            return testRecords;
        }

        /// <summary>
        /// Simulate sequence of site IDs (* = new): 0, 5*, 10, 20
        /// </summary>
        private static List<MiuOwnerHistory> GetNewOwnerInsertedBetweenExistingProcessedRecords()
        {
            var testRecords = ScaffoldTestRecords(4).ToList();

            testRecords[0].SiteId = 0;
            testRecords[0].ConsideredForTransferQueue = DateTime.UtcNow;
            testRecords[0].SiteIdChangeRaised = DateTime.UtcNow;

            testRecords[1].SiteId = 10;
            testRecords[1].ConsideredForTransferQueue = DateTime.UtcNow;
            testRecords[1].SiteIdChangeRaised = DateTime.UtcNow;

            testRecords[2].SiteId = 20;
            testRecords[2].ConsideredForTransferQueue = DateTime.UtcNow;
            testRecords[2].SiteIdChangeRaised = DateTime.UtcNow;

            // Simulate insertion of a site ID between 2 that have already been processed.
            testRecords[3].SiteId = 5;
            testRecords[3].Timestamp = testRecords[0].Timestamp.AddSeconds(30);

            return testRecords;
        }

        /// <summary>
        /// Simulate sequence of site IDs (* = new): 10, 5*, 10, 20
        /// </summary>
        private static List<MiuOwnerHistory> GetNewOwnerInsertedBetweenExistingProcessedRecordsWithDuplicate()
        {
            var testRecords = ScaffoldTestRecords(4).ToList();

            testRecords[0].SiteId = 10;
            testRecords[0].ConsideredForTransferQueue = DateTime.UtcNow;
            testRecords[0].SiteIdChangeRaised = DateTime.UtcNow;

            testRecords[1].SiteId = 10;
            testRecords[1].ConsideredForTransferQueue = DateTime.UtcNow;

            testRecords[2].SiteId = 20;
            testRecords[2].ConsideredForTransferQueue = DateTime.UtcNow;
            testRecords[2].SiteIdChangeRaised = DateTime.UtcNow;

            // Simulate insertion of a site ID between 2 that have already been processed.
            testRecords[3].SiteId = 5;
            testRecords[3].Timestamp = testRecords[0].Timestamp.AddSeconds(30);

            return testRecords;
        }

        private static IEnumerable<MiuOwnerHistory> ScaffoldTestRecords(int count)
        {
            for (int i = 0; i < count; i++)
            {
                yield return new MiuOwnerHistory
                {
                    MiuId = 400000000,
                    SiteId = 0,
                    Timestamp = new DateTime(2017, 01, 01, 09, 00, 00, 00, DateTimeKind.Utc).AddMinutes(i),
                    SequenceId = 1000 + i
                };
            }
        }
    }
}