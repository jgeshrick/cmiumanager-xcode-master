﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using Ntg.BpcCore.DataConnectors;
using Ntg.BpcCore.Entities.CostRevenueData;
using NUnit.Framework;

namespace Ntg.BpcCore.UnitTests.DataConnectors
{
    [TestFixture]
    public class MiuConfigEventBuilderTest
    {
        private MiuConfigEventBuilder builder;

        [SetUp]
        public void InitializeTest()
        {
            builder = new MiuConfigEventBuilder();
        }

        [Test]
        public void ProcessConfigurationHistory_WhenAllConfigsHaveBeenConsidered_DoesNotAddModeChanges()
        {
            var configs = Get3ProcessedRecords();
            builder.ProcessConfigurationHistory(configs);

            Assert.IsEmpty(builder.ModeChanges);
            Assert.IsEmpty(builder.UpdatedConfigHistory);
        }

        [Test]
        public void ProcessConfigurationHistory_WhenAllConfigsHaveBeenConsidered_DoesNotAddSiteIdChanges()
        {
            var configs = Get3ProcessedRecords();
            builder.ProcessConfigurationHistory(configs);

            Assert.IsEmpty(builder.UpdatedConfigHistory);
        }

        [Test]
        public void ProcessConfigurationHistory_With3NewRecords_AddsExpectedModeChanges()
        {
            var configs = Get3NewRecordsWithChanges();
            builder.ProcessConfigurationHistory(configs);

            // Expect two mode changes matching the supplied values
            var changes = builder.ModeChanges.OrderBy(c => c.Timestamp).ToList();
            Assert.AreEqual(1, changes.Count(c => c.ModeName == "Basic"));
            Assert.AreEqual(1, changes.Count(c => c.ModeName == "Advanced"));

            // The first mode change should align with the first config entry
            Assert.AreEqual(configs[0].MiuId, changes[0].MiuId);
            Assert.AreEqual(configs[0].RecordingIntervalMinutes, changes[0].RecordingIntervalMins);
            Assert.AreEqual(configs[0].ReportingIntervalMinutes, changes[0].ReportingIntervalMins);
            Assert.AreEqual(configs[0].Timestamp, changes[0].Timestamp);

            // The second mode change should align with the second config entry
            Assert.AreEqual(configs[1].MiuId, changes[1].MiuId);
            Assert.AreEqual(configs[1].RecordingIntervalMinutes, changes[1].RecordingIntervalMins);
            Assert.AreEqual(configs[1].ReportingIntervalMinutes, changes[1].ReportingIntervalMins);
            Assert.AreEqual(configs[1].Timestamp, changes[1].Timestamp);

            // All rows should have a value on ConsideredForTransferQueue.
            Assert.IsEmpty(builder.UpdatedConfigHistory.Where(c => !c.ConsideredForTransferQueue.HasValue));
        }

        [Test]
        public void ProcessConfigurationHistory_WhenNewChangeIsInserted_AddsExpectedModeChanges()
        {
            var configs = GetAdvancedModeInsertedBetweenBasicAndPro();
            builder.ProcessConfigurationHistory(configs);

            // Expect one mode change to "Advanced"
            var changes = builder.ModeChanges.OrderBy(c => c.Timestamp).ToList();
            Assert.AreEqual(1, changes.Count);
            Assert.AreEqual("Advanced", changes[0].ModeName);

            // The first mode change should align with the first config entry
            Assert.AreEqual(configs[2].MiuId, changes[0].MiuId);
            Assert.AreEqual(configs[2].RecordingIntervalMinutes, changes[0].RecordingIntervalMins);
            Assert.AreEqual(configs[2].ReportingIntervalMinutes, changes[0].ReportingIntervalMins);
            Assert.AreEqual(configs[2].Timestamp, changes[0].Timestamp);

            // The 2nd and 3rd records should be marked as dirty
            Assert.AreEqual(2, builder.UpdatedConfigHistory.Count());
            Assert.IsTrue(builder.UpdatedConfigHistory.Any(c => c == configs[1]));
            Assert.IsTrue(builder.UpdatedConfigHistory.Any(c => c == configs[2]));
        }

        /// <summary>
        /// Verify that if the configuration change was not reported by the CMIU, a mode change is not reported.
        /// </summary>
        [Test]
        public void ProcessConfigurationHistory_WhenChangeIsNotReceivedFromMiu_DoesNotCreateModeChange()
        {
            var configs = Get3NewRecordsWithUnconfirmedChanges();
            builder.ProcessConfigurationHistory(configs);

            // There should be no mode changes since none have been confirmed by the CMIU
            Assert.IsEmpty(builder.ModeChanges);
        }

        /// <summary>
        /// Verifies that only the rows that need to be considered for the transfer queue are processed.
        /// </summary>
        [Test]
        public void WhenNewConfigurationIsInsertedOutOfSync_OnlyConsidersThisAndTheFollowingConfigurationForTheTransferQueue()
        {
            var configs = GetAdvancedModeInsertedBetweenBasicAndProWithTrailOff();
            builder.ProcessConfigurationHistory(configs);

            // Only the 2nd and 3rd rows in the sequence are expected to be updated
            var updatedConfigs = builder.UpdatedConfigHistory.ToList();
            Assert.IsFalse(updatedConfigs.Any(c => c.SequenceId == 1000));
            Assert.IsTrue(updatedConfigs.Any(c => c.SequenceId == 1001));
            Assert.IsTrue(updatedConfigs.Any(c => c.SequenceId == 1002));
            Assert.IsFalse(updatedConfigs.Any(c => c.SequenceId == 1003));
        }

        /// <summary>
        /// Gets a set of 3 configurations, all with ConsideredForTransferQueue=true.
        /// </summary>
        /// <returns></returns>
        private static List<MiuConfigHistory> Get3ProcessedRecords()
        {
            var testRecords = ScaffoldTestRecords(3)
                .Select(r =>
                {
                    r.ConsideredForTransferQueue = DateTime.UtcNow;
                    return r;
                })
                .ToList();

            return testRecords;
        }

        /// <summary>
        /// Gets a set of 3 unprocessed configurations, with one mode change and one site ID change.
        /// </summary>
        /// <returns></returns>
        private static List<MiuConfigHistory> Get3NewRecordsWithChanges()
        {
            var testRecords = ScaffoldTestRecords(3).ToList();

            // Change mode after the 1st record.
            testRecords.Skip(1).ToList().ForEach(r =>
            {
                r.RecordingIntervalMinutes = 60;
                r.ReportingIntervalMinutes = 240;
            });

            return testRecords;
        }

        /// <summary>
        /// Gets a set of 3 unprocessed configurations, with one mode change and one site ID change, but none of the records was received from the MIU.
        /// </summary>
        /// <returns></returns>
        private static List<MiuConfigHistory> Get3NewRecordsWithUnconfirmedChanges()
        {
            var testRecords = ScaffoldTestRecords(3).ToList();

            testRecords.ForEach(r => r.ReceivedFromMiu = false);

            // Change mode after the 1st record.
            testRecords.Skip(1).ToList().ForEach(r =>
            {
                r.RecordingIntervalMinutes = 60;
                r.ReportingIntervalMinutes = 240;
            });

            return testRecords;
        }

        /// <summary>
        /// Gets a set of 3 configurations, in which the middle entry (chronologically) has been inserted with a mode change.
        /// </summary>
        /// <returns></returns>
        private static List<MiuConfigHistory> GetAdvancedModeInsertedBetweenBasicAndPro()
        {
            var testRecords = ScaffoldTestRecords(3).ToList();

            // 1st record in sequence already processed
            testRecords[0].Timestamp = new DateTime(2017, 01, 01, 01, 00, 00, 00, DateTimeKind.Utc);
            testRecords[0].ConsideredForTransferQueue = DateTime.UtcNow;
            testRecords[0].ModeChangeRaised = DateTime.UtcNow;

            // 2nd record in sequence already processed, sets Pro mode
            testRecords[1].RecordingIntervalMinutes = 15;
            testRecords[1].ReportingIntervalMinutes = 60;
            testRecords[1].Timestamp = new DateTime(2017, 03, 03, 03, 00, 00, 00, DateTimeKind.Utc);
            testRecords[1].ConsideredForTransferQueue = DateTime.UtcNow;
            testRecords[1].ModeChangeRaised = DateTime.UtcNow;

            // 3rd record in sequence (but now 2nd chronologically) not processed, sets Advanced mode
            testRecords[2].Timestamp = new DateTime(2017, 02, 02, 02, 00, 00, 00, DateTimeKind.Utc);
            testRecords[2].RecordingIntervalMinutes = 60;
            testRecords[2].ReportingIntervalMinutes = 240;

            return testRecords;
        }

        /// <summary>
        /// As per GetAdvancedModeInsertedBetweenBasicAndPro but with an extra processed row at the end.
        /// </summary>
        /// <returns></returns>
        private static List<MiuConfigHistory> GetAdvancedModeInsertedBetweenBasicAndProWithTrailOff()
        {
            var testRecords = ScaffoldTestRecords(4).ToList();

            // 1st record in sequence already processed
            testRecords[0].Timestamp = new DateTime(2017, 01, 01, 01, 00, 00, 00, DateTimeKind.Utc);
            testRecords[0].ConsideredForTransferQueue = DateTime.UtcNow;
            testRecords[0].ModeChangeRaised = DateTime.UtcNow;

            // 2nd record in sequence already processed, sets Pro mode
            testRecords[1].RecordingIntervalMinutes = 15;
            testRecords[1].ReportingIntervalMinutes = 60;
            testRecords[1].Timestamp = new DateTime(2017, 03, 03, 03, 00, 00, 00, DateTimeKind.Utc);
            testRecords[1].ConsideredForTransferQueue = DateTime.UtcNow;
            testRecords[1].ModeChangeRaised = DateTime.UtcNow;

            // 3rd record in sequence (but now 2nd chronologically) not processed, sets Advanced mode
            testRecords[2].Timestamp = new DateTime(2017, 02, 02, 02, 00, 00, 00, DateTimeKind.Utc);
            testRecords[2].RecordingIntervalMinutes = 60;
            testRecords[2].ReportingIntervalMinutes = 240;

            // 4th record in sequence (4th chronologically) already processed.
            testRecords[3].Timestamp = new DateTime(2017, 04, 04, 04, 00, 00, 00, DateTimeKind.Utc);
            testRecords[3].RecordingIntervalMinutes = 15;
            testRecords[3].ReportingIntervalMinutes = 60;
            testRecords[3].ConsideredForTransferQueue = DateTime.UtcNow;

            return testRecords;
        }

        private static IEnumerable<MiuConfigHistory> ScaffoldTestRecords(int count)
        {
            for (int i = 0; i < count; i++)
            {
                yield return new MiuConfigHistory
                {
                    MiuId = 400000000,
                    Timestamp = new DateTime(2017, 01, 01, 09, 00, 00, 00, DateTimeKind.Utc).AddMinutes(i),
                    RecordingIntervalMinutes = 60,
                    ReportingIntervalMinutes = 1440,
                    ReceivedFromMiu = true,
                    SequenceId = 1000 + i
                };
            }
        }
    }
}