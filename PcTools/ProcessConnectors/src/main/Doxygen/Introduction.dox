/**

\mainpage Introduction

\section dpurp Document Purpose

This document defines the Factory Process Connector (FPC) Client API and provides indication of how it is intended to be used.  For the main details of the FPC Client API please see the section titled "_IFpcClient Interface Reference".


\section dover Overview

The FPC Client is responsible for providing connectivity between CMIU factory test applications (e.g. FTS and CVS) and Galileo, the cloud-based managed services platform.  The FPC server is part of Galileo, and receives connections from FPC client instances.  Galileo runs within Amazon Web Services (AWS) infrastructure.


\section drelp Release Procedure

FpcClient library releases are available via TeamCity at https://neptune-teamcity.sagentia.com/.  For a login, please contact rupert.menzies@sagentia.com.

The primary release artifact - FpcClient v$(SW_VERSION).zip - can be found under the MSPD | PcTools | FpcClient build configuration at https://neptune-teamcity.sagentia.com/viewType.html?buildTypeId=Mspd_Trunk_PcTools_FpcClient.  This version number of the release (displayed in TeamCity) is embedded into the assembly version information of the FpcClient.Merged.dll.

Other artifacts available in that build configuration are:
- This document
- FpcClientEnvironmentsXmlFiles.zip
- Various other zip archives used internally for TeamCity builds

For testing and development purposes, a stubbed version of FpcClient is available in TeamCity under MSPD | PcTools | FpcClientStub build configuration at https://neptune-teamcity.sagentia.com/viewType.html?buildTypeId=Mspd_Trunk_PcTools_FpcClientStub.  This provides the API defined in this document, but is non-functional.  For more information please see 10065-1_DO04_0024.
  
\section ddep Dependencies and Pre-requisites

The FpcClient.Merged.dll file contains all FpcClient functionality and dependency libraries.  It is a merged DLL comprising the following files:

  -  FpcClient.dll
  -  NeptuneCloudConnector35.dll
  -  log4net.dll
  -  Newtonsoft.Json.dll
  -  RestSharp.dll

In order for the FPC Client to run on a Windows machine you must have the Microsoft .NET Framework v3.5 installed. See https://www.microsoft.com/en-gb/download/details.aspx?id=21 for details on how to install this pre-requisite.


\section denvo Environment
  
In order for the FPC Client to run, a file detailing the server environment must be located in the same location as the active application.

*NB  When running a pre-compiled LabVIEW executable this file must be in the same folder as the executable.  When running a solution from within LabVIEW this file must be in the same folder as the LabVIEW executable (eg. C:\Program Files (x86)\National Instruments\LabVIEW 2011).

This file must be named environments.xml and must contain an XML element for the environment name.


\n<MdceEnvironments xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
\n  <Environment>
\n    <Name></Name>
\n  </Environment>
\n</MdceEnvironments>

Examples of this file for the various Galileo environments can be found in the TeamCity artifacts for the FpcClient build configuration in FpcClientEnvironmentsXmlFiles.zip.


\section dlogn Logging
  
In order for the file-based logging functionality of FpcClient to work, a file containing the configuration of the log functionality is required.  This file must be must be named log4net.xml and be located in a folder labelled Resources.

The file supplied within the FpcClient.zip artifacts in TeamCity should be sufficient for both development and early production testing.

*NB  When running a pre-compiled LabVIEW executable this folder must be in the same folder as the executable.  When running a solution from within LabVIEW this folder must be in the same folder as the LabVIEW executable (eg. C:\Program Files (x86)\National Instruments\LabVIEW 2011).  

\section ddur Duration Estimates

Some of the FPC client library API calls result in HTTP transactions that run against a server running in AWS.  Such calls can take hundreds of milliseconds to complete (in the best case), which may be significant for high-volume testing.  Where functions have such behaviour, this document provides estimated durations based on performance observed to date in the development test system.

Execution duration is likely to vary in the production systems; the current values are provided for indication purposes only.
  

\section drefd Reference Design

A LabVIEW reference design to illustrate the intended usage of the FPC client library is also available in TeamCity under MSPD | PcTools | Trinity at https://neptune-teamcity.sagentia.com/viewType.html?buildTypeId=Mspd_Trunk_PcTools_Trinity.  This tool serves four key purposes:
- A Reference design for the FPC client library
- An integration test interface that can be used for automated testing of the FPC client library
- A means to de-risk LabVIEW/.NET interop
- An interim tool for the NTG managed services team to manage CAN data 

To use the tool, simply download the versioned zip file artifact from the build configuration and run Trinity.exe.  The source code for the tool is available in SVN at svn://redsea:3691/MSPD/trunk/PcTools/Trinity.  See the Trinity release note 10065-1_DO04_0023 for further information.

For development purposes, there is also a release of Trinity avaiable packaged with the stubbed variant of the FPC client library.  This can be downloaded from the MSPD | PcTools | TrinityStub build configuration at https://neptune-teamcity.sagentia.com/viewType.html?buildTypeId=Mspd_Trunk_PcTools_TrinityStub.  This release can be used to illustrate the behaviour of the stubbed FPC client library.

  
\section rhist Revision History

Document Revision|S/W Version|Issue Date|Prepared By|Reason/Description
--------|----------|----------|-----------|------------------
$(DOC_REVISION)|v$(SW_VERSION)|$(DOC_DATE)|Simon Prentice|First draft.


\section intro Glossary of Terms
Term|Description
--------|------------------
FPC|Factory Process Connector
Partner Key| Partner Key or Partner Id is an authenticated key provided by the FPC server administrator.  This sets the permission rights for the FPC client.
FTS|Functional Test Station
CVS|Connection Verification Station
FFTS|Final Functional Test Station
CMIU|Cellular Meter Interface Unit
IMEI|International Mobile Station Equipment Identity
ICCID|Integrated Circuit Card Identifier
IMSI|International Mobile Subscriber Identity
AWS|Amazon Web Services
API|Application Programming Interface
APN|Access Point Name
REST|Representational State Transfer
JSON|JavaScript Object Notation
XML|EXtensible Markup Language
CAN|Collection of Associated Numbers

*/
