# -*- coding: utf-8 -*-
"""

/*************************************************************************
 * (c) Sagentia Limited, 2013.  All rights reserved.
 *
 * This software is the property of Sagentia Limited and may
 * not be copied or reproduced otherwise than on to a single hard disk for
 * backup or archival purposes.  The source code is confidential
 * information and must not be disclosed to third parties or used without
 * the express written permission of Sagentia Limited.
 *
 *************************************************************************/

Created on Fri Jan 11 10:08:47 2013

@author: pb1

"""

import os
import glob
import sys

gpath = sys.argv[1]

cFiles = glob.glob(os.path.join(gpath,'*.tex'))

print "Looking for tables in " + os.path.join(gpath,'*.tex')

# Go through every tex file,terminating the first row of every table with \endhead
for fName in cFiles:
    
    rep = 0
    
    # open the file and read it in
    print '  File: ' + os.path.relpath(fName) + '.'
    f = open(fName, 'r')
    lines = f.readlines()
    for line in xrange(len(lines)): # for each line in the file
        if lines[line].find(r"\begin{TabularC}") > -1:
            for inTable in xrange(line,len(lines)):
                if lines[inTable].find(r"\end{TabularC}") > -1:
                    break
                if lines[inTable].find(r"&{\bf") > -1:
                    lines[inTable] = lines[inTable].strip("\r\n") + r"\endhead" + "\n"
                    rep = 1
                    break

    # reopen the file and delete original content
    f.close()
    
    if 1 == rep:
        f = open(fName, 'w+')
    
        # Write the clean tex
        f.writelines(lines)
        f.close()
    
print 'Table patched.'

print ''
                    
            
        
