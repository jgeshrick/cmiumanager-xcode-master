#************************************************************************* 
 # (c) Sagentia Limited, 2005.  All rights reserved.   
 #  
 # This software is the property of Sagentia Limited and may 
 # not be copied or reproduced otherwise than on to a single hard disk for 
 # backup or archival purposes.  The source code is confidential  
 # information and must not be disclosed to third parties or used without  
 # the express written permission of Sagentia Limited. 
 # 
 # Filename : ErrorHandler.py
 # Created  : 27 - Aug - 14
 # Author   : WJD1
 #
 # $Header$
 #************************************************************************/ 


import sys

class ErrorHandler:

    def __init__(self):
        self.errors =  []
        self.errors.append("")
        self.errors.append("File not found")
        self.errors.append("File wrong format")
        self.errors.append("Insufficient Arguments")
        self.errors.append("Linker map file error (possibly missing)")
    
    def newError(self, newErr):
        self.errors.append(newErr)
        return len(self.errors)-1 #index of error
        
    def ERROR(self, errIndex):
        '''
        if( errIndex == 0):
            sys.stdout.write("##teamcity[buildStatus status='SUCCESS' text=' " + self.errors[errIndex] + " '] \n")
        else:
            sys.stdout.write("##teamcity[buildStatus status='FAILURE' text=' " + self.errors[errIndex] + " '] \n")
        '''
        
        sys.exit(errIndex)