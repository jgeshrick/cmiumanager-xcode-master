import glob

from ErrorHandler import ErrorHandler
errors = ErrorHandler()

def ReplaceText(templateFile, outFile, toReplace, replaceWith):
    if len(glob.glob(templateFile)) == 0:
        errors.ERROR(1)

    with open(templateFile) as t:
        lines = t.readlines()
        print ("   Read template: " + templateFile)

    with open(outFile,"w+") as o:
        print ("   Opened output file: " + outFile)
        for line in lines:
            if toReplace in line:
                print "Line: " + line
            o.write(line.replace(toReplace, replaceWith))

    print "   Replacement complete"
