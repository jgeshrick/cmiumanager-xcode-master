
import re
import sys

import ReplaceText
import CompositeTypeBase

from ErrorHandler import ErrorHandler
errors = ErrorHandler()

class CEnum(CompositeTypeBase.CCompositeType):

    ## Gets the members of the tag as a list
    def ParseMembers(self, lines):
    
        # Compile regexes for names, values and comments
        nameRe = re.compile(r"\s*(?P<name>[a-zA-z0-9_]*)")
        valueRe = re.compile(r"[^=]*=\s*(?P<value>((0x)?[0-9a-fA-F]*))")
        commentRe = re.compile(r"[^/**<]*(/\*\*<)+ *(?P<comment>.*)")

        cName = ""
        cValue = ""
        cComment = ""

        # Go through the enum finding members and their comments
        for lineId in xrange(len(lines)):
            if lines[lineId].strip(" ").startswith(r"/**"):
                cComment = lines[lineId].strip("\t\r\n */")
            else:
                nameMatch = nameRe.match(lines[lineId])
                if nameMatch != None:
                    cName = nameMatch.group('name').strip(",")
                valueMatch = valueRe.match(lines[lineId])
                if valueMatch != None:
                    cValue = valueMatch.group('value')
                commentMatch = commentRe.match(lines[lineId])
                if commentMatch != None:
                    cComment = commentMatch.group('comment').strip("*/ \r\n\t")

            cComment = cComment.replace("\n"," ").replace("\r"," ")

            if cName != "":
                self.members.append((cName,cValue,cComment))
                cName = ""
                cValue = ""
                cComment = ""

    ## returns a compiled regular expression allowing the identification of the
    ## enumeration tagName.
    def GetDeclarationRe(self, tagName):
        return re.compile(r"(?:typedef enum.*)+(" + tagName + ")")

    ## Returns a markdown table detailing the members of the enumeration
    def GetAsTable(self):
        longestName = 0
        longestValue = len('Value')
        outStr = ""
        for member in self.members:
            longestName = max(len(member[0]), longestName)
            longestValue = max(len(member[1]), longestValue)

        outStr = ("| {0:{1}s} | ".format('Name',longestName) +
                  "{0:{1}s} | ".format('Value',longestValue) + "Comment |\n" +
                  "|-|-|-|\n")

        for member in self.members:
            outStr = (outStr + "| {0:{1}s} | ".format(member[0],longestName) +
                      "{0:{1}s} | ".format(member[1],longestValue) +
                      member[2] + " |\n")
        return outStr

if __name__=='__main__':
    
    if len(sys.argv) < 7:
        errors.ERROR(3)

    cFileName = sys.argv[1]
    name = sys.argv[2]
    cEnumTag = sys.argv[3]
    templateFile = sys.argv[4]
    outFile = sys.argv[5]
    outRep = sys.argv[6]

    print ("Documenting enumeration " + name +
           " in file " + cFileName)

    print ("Parsing enumeration...")
    parsedEnum = CEnum(cFileName, name, cEnumTag)

    print ("Printing members to file: " + outFile + " replacing " + outRep +
           " in " + templateFile + '.')
    print parsedEnum.GetAsTable()
    ReplaceText.ReplaceText(templateFile, outFile,
                outRep, parsedEnum.comment + "\n" + parsedEnum.GetAsTable())

