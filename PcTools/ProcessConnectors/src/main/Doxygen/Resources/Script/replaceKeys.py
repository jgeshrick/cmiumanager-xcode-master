# -*- coding: utf-8 -*-
"""

/*************************************************************************
 * (c) Sagentia Limited, 2013.  All rights reserved.
 *
 * This software is the property of Sagentia Limited and may
 * not be copied or reproduced otherwise than on to a single hard disk for
 * backup or archival purposes.  The source code is confidential
 * information and must not be disclosed to third parties or used without
 * the express written permission of Sagentia Limited.
 *
 *************************************************************************/

Created on Fri Jan 11 10:08:47 2013

@author: pb1

"""

import os
import glob
import sys

# repStrs is a tuple of 2 element tuples. The first element is the text to replace
# the second is what to replace it with
repStrs = ('.eps',
           '.jpg'),

if len(sys.argv) > 1:
    print 'Adding configuration file ' + str(sys.argv[1])
    f = open(str(sys.argv[1]), 'r')
    lines = f.readlines()
    for l in lines : # for each line in the file
        l = l.replace('\n','')
        repStrs += eval('(' + l + '),')
        print 'Added key: ' + l
    f.close()

gpath = sys.argv[2]

print 'Replacing strings in ' + gpath + '...'

cFiles = glob.glob(os.path.join(gpath,'*.tex'))
cFiles += glob.glob(os.path.join(gpath,'*.sty'))

# Go through every tex file, changing graphics from .eps to .jpg
for fName in cFiles:
    
    rep = 0
    
    # open the file and read it in
    print '  File: ' + os.path.relpath(fName) + '.'
    f = open(fName, 'r')
    lines = f.readlines()
    for l in xrange(len(lines)) : # for each line in the file
        for repStr in repStrs: # replace each repStr
            if repStr[0] in lines[l]:
                print ('    Line ' + str(l) + ': Replacing ' + 
                          repStr[0] + ' with ' + repStr[1] + '.')
                lines[l] = lines[l].replace(repStr[0],repStr[1])
                rep = 1

    # reopen the file and delete original content
    f.close()
    
    if 1 == rep:
        f = open(fName, 'w+')
    
        # Write the clean tex
        f.writelines(lines)
        f.close()
    
print 'All strings replaced.'

print ''
                    
            
        
