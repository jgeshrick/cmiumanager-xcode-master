
import re
import sys
import glob

import ReplaceText

from ErrorHandler import ErrorHandler
errors = ErrorHandler()

class CCompositeType(object):
    comment = ""
    members=[]

    def __init__(self,cFileName, name, tagName):
        if len(glob.glob(cFileName)) < 1:
            errors.ERROR(1)

        # Load the C file containing the enumerated type
        with open(cFileName) as cFile:
            lines = cFile.readlines()

        # Combine multiline comments in lines, this does not remove the line
        # breaks
        lines = self.ReduceCommentLines(lines)

        # find the beginning and end of the declaration
        beginLine = None
        endLine = None

        declarationRe = self.GetDeclarationRe(tagName)

        # find the type declaration, this may be the EnumName_tag form
        for lineId in xrange(len(lines)):
            # Find the declaration of the enum tag
            m = declarationRe.match(lines[lineId])
            if m != None:
                beginLine = lineId
                print "Found start of declaration of " + tagName + ":"
                print str(lineId) + ": " + lines[beginLine]

        if None == beginLine:
            raise AssertionError("Tag " + tagName +
                                 " not found in file " + cFileName)

        # Get the comment
        self.comment = ""
        commentLine = lines[beginLine-1].strip()
        if (commentLine[:3] == "/**") and (commentLine[-2:] == "*/"):
               self.comment = commentLine.strip("/* ")

        print "Comment found:\n" + self.comment

        # Skip past the opening brace
        for lineId in xrange(beginLine,len(lines)):
            if lines[lineId].find("{") >= 0:
                beginLine = lineId + 1
                print "Members begin at line " + str(beginLine)
                break

        # find the end of the enum
        for lineId in xrange(beginLine,len(lines)):
            if lines[lineId].find("}") >= 0:
                endLine = lineId + 1
                print "Members end at line " + str(endLine)
                break

        self.ParseMembers(lines[beginLine:endLine])

    # Get all multiline comments in the file into single entries in lines
    # and remove (not in comment) whitespace lines. This simplifies later
    # parsing.
    def ReduceCommentLines(self, lines):

        for lineId in xrange(len(lines)):

            # Combine comment lines
            if lines[lineId].find("/**") >= 0:
                while lines[lineId].find("*/") < 0:
                    lines[lineId] = (lines[lineId] +
                                     lines[lineId + 1].strip(" *\t"))
                    if (lines[lineId + 1].strip(" \n\t\r")[-2:] == "*/"):
                        lines[lineId] = lines[lineId] + "*/"
                    lines.pop(lineId+1)
                lines[lineId] = lines[lineId].rstrip("\n/*") + "*/"

            # As we will be deleting lines the current length of the file has
            # to be re-tested at each iteration
            if lineId + 1 >= len(lines):
                break

            # Remove whitespace lines
            if len(lines[lineId+1].strip(" \t\r\n")) == 0:
                lines.pop(lineId+1)

            # As we will be deleting lines the current length of the file has
            # to be re-tested at each iteration
            if lineId + 1 >= len(lines):
                break

        return lines

    ## Gets the members of the tag as a list
    def ParseMembers(self, lines):
        raise AssertionError("ParseMembers should always be overridden")

    ## returns a compiled regular expression allowing the identification of the
    ## enumeration tagName.
    def GetDeclarationRe(self, tagName):
        raise AssertionError("GetDeclarationRe should always be overridden")

    ## Returns a markdown table detailing the members of the enumeration
    def GetAsTable(self):
        raise AssertionError("GetAsTable should always be overridden")
