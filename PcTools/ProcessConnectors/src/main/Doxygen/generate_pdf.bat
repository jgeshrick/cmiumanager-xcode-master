@if [%1]==[] goto usage
@if [%2]==[] goto usage
@if [%3]==[] goto usage
@if [%4]==[] goto usage
@if [%5]==[] goto usage
@if [%6]==[] goto usage

set DOC_VERSION=%~1
set DOC_VERSION_ESCAPED=%DOC_VERSION:_=\_%
set DOC_REVISION=%~2
set DOC_REVISION_ESCAPED=%DOC_REVISION:_=\_%
set APP_VERSION=%~3
set DOC_DATE=%~4
set DOC_DATE_ESCAPED=%DOC_DATE:_=\_%
set DOC_NAME=%~5
set SW_VERSION=%~6
set SW_VERSION_ESCAPED=%SW_VERSION:_=\_%
set PDF_OUT=%DOC_NAME%-%DOC_VERSION%.pdf
set OUT_DIR=..\..\..\target\doxygen

mkdir "%OUT_DIR%"

echo %DOC_VERSION_ESCAPED% > "%OUT_DIR%\documentVersion.tex"
echo %DOC_REVISION_ESCAPED% > "%OUT_DIR%\documentRevision.tex"
echo %DOC_DATE% > "%OUT_DIR%\documentDate.tex"
echo %DOC_DATE_ESCAPED% > "%OUT_DIR%\documentDateEsc.tex"
echo %APP_VERSION% > "%OUT_DIR%\versionNumber.tex"
echo %DOC_NAME% > "%OUT_DIR%\documentName.tex"
echo %SW_VERSION_ESCAPED% > "%OUT_DIR%\swVersion.tex"


doxygen > "%OUT_DIR%\doxygen_stdout.log" 2> "%OUT_DIR%\doxygen_stderr.log"

copy /Y resources\LaTeX\*.* "%OUT_DIR%\latex\"
pushd "%OUT_DIR%\latex"
call make > %OUT_DIR%\make_stdout.log 2> %OUT_DIR%\make_stderr.log
popd

del "%OUT_DIR%\%PDF_OUT%"

type "%OUT_DIR%\doxygen_stderr.log"

copy "%OUT_DIR%\latex\refman.pdf" "%OUT_DIR%\%PDF_OUT%"

goto done

:usage
@echo off
echo Usage: generate_pdf.bat docversion docrevision appversion date name
echo E.g. generate_pdf.bat 01 B(DRAFT) 1.2.3.4 "12/16/2014" api-doc 1234.5678.90ab.cdef

:done
