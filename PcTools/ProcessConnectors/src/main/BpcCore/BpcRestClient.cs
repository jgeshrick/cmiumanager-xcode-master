﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using Ntg.BpcCore.JsonModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using NeptuneCloudConnector.Exception;
using NeptuneCloudConnector.JsonModel;
using Ntg.BpcCore.JsonModel.ConfigHistory;
using Ntg.BpcCore.JsonModel.CrmReference;
using Ntg.BpcCore.JsonModel.LifecycleHistory;
using Ntg.BpcCore.JsonModel.MiuConfig;
using Ntg.BpcCore.JsonModel.OwnerHistory;
using Ntg.BpcCore.ServiceProxies;

namespace Ntg.BpcCore
{
    public class BpcRestClient : NeptuneCloudConnector.NeptuneCloudRestClient
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string DATE_FORMAT_ISO_8601 = "yyyy-MM-dd";
        private Token currentToken;
        private const short TOKEN_EXPIRE_TIME_JITTER_MIN = 10;
 
        public BpcRestClient(string partnerString, string siteId, string mdceIntegrationUrl)
            : base(partnerString, siteId, mdceIntegrationUrl)
        {
        }

        /// <summary>
        /// Retrieve sequential MIU configuration history using IF18v2.
        /// </summary>
        public async Task<List<SequentialMiuConfig>> GetConfigurationHistoryAsync(int? lastSequenceId)
        {
            var token = GetAuthenticationToken();
            var parameters = new List<string> {"token", token.AuthToken};

            if (lastSequenceId.HasValue)
            {
                parameters.Add("last_sequence");
                parameters.Add(lastSequenceId.ToString());
            }

            var configHistory = await DoGetAsync<List<SequentialMiuConfig>>(
                2, "miu/config_history", parameters.ToArray());

            return configHistory;
        }

        /// <summary>
        /// Retrieve sequential MIU owner history using IF48.
        /// </summary>
        public async Task<SequentialMiuOwnerCollection> GetMiuOwnerHistoryAsync(int? lastSequenceId)
        {
            var token = GetAuthenticationToken();
            var parameters = new List<string> { "token", token.AuthToken };

            if (lastSequenceId.HasValue)
            {
                parameters.Add("last_sequence");
                parameters.Add(lastSequenceId.ToString());
            }

            var ownerHistoryCollection = await DoGetAsync<SequentialMiuOwnerCollection>(
                "miu/owner_history", parameters.ToArray());

            return ownerHistoryCollection;
        }

        /// <summary>
        /// Retrieve sequential MIU lifecycle state history using IF46.
        /// </summary>
        public async Task<List<SequentialMiuLifecycleState>> GetLifecycleHistoryAsync(int? lastSequenceId)
        {
            var token = GetAuthenticationToken();
            var parameters = new List<string> { "token", token.AuthToken };

            if (lastSequenceId.HasValue)
            {
                parameters.Add("last_sequence");
                parameters.Add(lastSequenceId.Value.ToString());
            }

            var lifecycleHistory = await DoGetAsync<List<SequentialMiuLifecycleState>>(
                "miu/lifecycle_history", parameters.ToArray());

            return lifecycleHistory;
        }

        /// <summary>
        /// Set reference data about utilities and distributers.
        /// Get CRM Reference Data (IF42)
        /// </summary>
        public Boolean SetCrmReferenceData(CrmReference referenceData)
        {
            bool result;
            Log.Debug(10005, string.Format("SetCrmReferenceData: Base URL: {0}, distributors: {1}, utilities: {2}.",
                base.mdceIntegrationBaseUrl, referenceData.Distributers.Count, referenceData.Utilities.Count));

            var token = GetAuthenticationToken();
            result = DoPut("/refdata", referenceData, "token", token.AuthToken);
            
            return result;
        }

        /// <summary>
        /// Append reference data about utilities and distributers.
        /// Get CRM Reference Data (IF42)
        /// </summary>
        public Boolean AppendCrmReferenceData(CrmReference referenceData)
        {
            Log.Debug(10005, string.Format("SetCrmReferenceData: Base URL: {0}, distributors: {1}, utilities: {2}.",
                base.mdceIntegrationBaseUrl, referenceData.Distributers.Count, referenceData.Utilities.Count));

            var token = GetAuthenticationToken();
            DoPost("/refdata", referenceData, "token", token.AuthToken);

            return true;
        }

        /// <summary>
        /// Gets all Customers from MDCE.
        /// </summary>
        /// <returns></returns>
        public CrmReference GetCrmReferenceData()
        {
            Log.Debug(10006, string.Format("GetCrmReferenceData: Base URL: {0}.", base.mdceIntegrationBaseUrl));
            var token = GetAuthenticationToken();
            var referenceData = DoGet<CrmReference>("/refdata", "token", token.AuthToken);

            Log.Debug(10007, string.Format("GetCrmReferenceData: Base URL: {0}, distributors returned: {1}, utilities returned: {2}.",
                base.mdceIntegrationBaseUrl, referenceData.Distributers.Count, referenceData.Utilities.Count));

            return referenceData;
        }

        /// <summary>
        /// Get a Single MIU config from mdce integration server
        /// </summary>
        /// <param name="miuId">miu id</param>
        /// <returns></returns>
        public MiuConfig GetSingleMiuConfig(int miuId)
        {
            var token = GetAuthenticationToken();

            var miuConfig = DoGet<MiuConfig>("miu/" + Convert.ToString(miuId) + "/config", "token", token.AuthToken);

            return miuConfig;
        }

        /// <summary>
        /// This will return all MIU’s for the token’s site id.  If the tokens site_id is 0, then all MIU’s in the MDCE will be returned.
        /// </summary>
        /// <returns></returns>
        public MiuConfigList GetMultipleMiuConfig()
        {
            var token = GetAuthenticationToken();

            var miuConfigList = DoGet<MiuConfigList>("miu/config", "token", token.AuthToken);

            return miuConfigList;
        }

        /// <summary>
        /// Set Miu config.
        /// </summary>
        /// <param name="miuId">id of the miu</param>
        /// <param name="jsonMiuConfig">new miu config</param>
        /// <returns>A value indicating whether the operation was successful.</returns>
        public Boolean SetMiuConfig(int miuId, MiuConfig jsonMiuConfig)
        {
            var token = GetAuthenticationToken();

            var result = DoPut("miu/" + Convert.ToString(miuId) + "/config", jsonMiuConfig,
                "token", token.AuthToken);

            return result;
        }

        /// <summary>
        /// Attempt to recycle the token if it has not expire
        /// </summary>
        /// <returns>An instance of <see cref="Token"/> containing authorization details.</returns>
        public override Token GetAuthenticationToken()
        {
            if ((this.currentToken == null) || 
                !this.currentToken.Authenticated ||
                (this.currentToken.Expires.HasValue && this.currentToken.Expires.Value.AddMinutes(-TOKEN_EXPIRE_TIME_JITTER_MIN) < DateTime.UtcNow))
            {
                //either no token has been issued or it has expired.
                try
                {
                    this.currentToken = base.GetAuthenticationToken();
                    if (currentToken.Authenticated)
                        Log.Info(10022, string.Format("Authenticated against MDCE. Service endpoint: \"{0}\", expires: \"{1}\".", mdceIntegrationBaseUrl, currentToken.Expires));
                    else
                        Log.Warn(10023, string.Format("Authentication against MDCE endpoint failed. Service endpoint: \"{0}\".", mdceIntegrationBaseUrl));
                }
                catch (System.Exception exception)
                {
                    Log.Error(10024, string.Format("An exception was encountered while authenticating against MDCE. Service endpoint: \"{0}\".", mdceIntegrationBaseUrl), exception);
                    
                    throw;
                }
            }

            return this.currentToken;
        }

        /// <summary>
        /// Gets a list of MIUs that match the supplied search criteria.
        /// </summary>
        /// <param name="findId">The MIU ID, ICCID, IMEI or EUI of the device</param>
        /// <returns>The collection of MIU details matching the query.</returns>
        public async Task<MiuListCollection> GetMiuListAsync(string findId)
        {
            var token = GetAuthenticationToken();

            var miuDetail = await DoGetAsync<MiuListCollection>("miu_list/find",
                "find_id", findId, "return_identities", "Y", "token", token.AuthToken);

            return miuDetail;
        }

        /// <summary>
        /// Gets a list of MIUs that match the supplied search criteria.
        /// </summary>
        /// <param name="findId">The MIU ID, ICCID, IMEI or EUI of the device</param>
        /// <param name="idType">The ID type specifier on which to search.</param>
        /// <returns>The collection of MIU details matching the query.</returns>
        public async Task<MiuListCollection> GetMiuListAsync(string findId, DeviceIdType idType)
        {
            var token = GetAuthenticationToken();

            var miuDetail = await DoGetAsync<MiuListCollection>("miu_list",
                idType.GetUriParameterKey(), findId, "return_identities", "Y", "token", token.AuthToken);

            return miuDetail;
        }

        /// <summary>
        /// Gets a list of all MIUs using the <code>MiuListResponseType.Basic</code> option.
        /// </summary>
        /// <returns>The collection of MIU details matching the query.</returns>
        public async Task<MiuListCollection> GetMiuListAsync()
        {
            return await GetMiuListAsync(MiuListResponseType.Basic);
        }

        /// <summary>
        /// Gets a list of all MIUs.
        /// </summary>
        /// <param name="responseType">Sets whether cellular/LoRa details should be included in the response.</param>
        /// <returns>The collection of MIU details matching the query.</returns>
        public async Task<MiuListCollection> GetMiuListAsync(MiuListResponseType responseType)
        {
            var token = GetAuthenticationToken();

            var returnIdentitiesParameter = responseType == MiuListResponseType.IncludeCellularIdentities ? "Y" : "N";

            var miuDetail = await DoGetAsync<MiuListCollection>("miu_list", 
                "token", token.AuthToken, "return_identities", returnIdentitiesParameter);

            return miuDetail;
        }

        public async Task<SetMiuLifecycleHistoryResult> SetMiuLifecycleHistoryAsync(SetMiuLifecycleHistoryRequest lifecycleHistoryRequest)
        {
            var token = GetAuthenticationToken();

            var result = await DoPostAsync<SetMiuLifecycleHistoryRequest, SetMiuLifecycleHistoryResult>(
                "miu/lifecycle_history", lifecycleHistoryRequest, "token", token.AuthToken);

            return result;
        }

        /// <summary>
        /// Updates the partner string and expires any existing token.
        /// </summary>
        /// <param name="newPartnerString">The new partner string.</param>
        protected override void ChangePartnerString(string newPartnerString)
        {
            // Expire any existing token immediately
            if (currentToken != null)
            {
                currentToken.Expires = DateTimeOffset.UtcNow.AddDays(-1);
            }

            base.ChangePartnerString(newPartnerString);
        }
    }
}
