﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

namespace Ntg.BpcCore
{
    /// <summary>
    /// Enumeration of the data connector type that is managed by the BPC Core Service
    /// </summary>
    public enum DataConnectorType
    {
        CustomerDataConnector,
        CostRevenueDataConnector,
        MiuLifecycleDataConnector
    }
}
