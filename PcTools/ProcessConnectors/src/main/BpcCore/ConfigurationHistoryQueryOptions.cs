﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;

namespace Ntg.BpcCore
{
    public class ConfigurationHistoryQueryOptions
    {
        /// <summary>
        /// Include MIU IDs equal to or greater than fromMiuId.
        /// </summary>
        public int? FromMiuId;

        /// <summary>
        /// Include MIU IDs equal to or less than toMiuId.
        /// </summary>
        public int? ToMiuId;

        /// <summary>
        /// Gets or sets the start date from which to include configuration history.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date until which to include configuration history.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets whether to include or exclude billable MIUs.
        /// </summary>
        public bool? Billable { get; set; }

        /// <summary>
        /// When non-null, indicates that the results should be filtered by whether or not the configuration was reported by the MIU.
        /// </summary>
        public bool? ReceivedFromMiu { get; set; }
    }
}