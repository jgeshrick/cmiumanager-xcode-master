﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Ntg.BpcCore.JsonModel;
using Ntg.BpcCore.Settings;

namespace Ntg.BpcCore.ServiceProxies
{
    /// <summary>
    /// Provides methods for calling the MIU List MDCE API interface.
    /// </summary>
    public class MiuListMultiServerProxy : IMiuListMultiServerProxy
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IEnumerable<IMdceEnvironmentBpcRestClient> restClients;

        /// <summary>
        /// Creates a new instance of <see cref="MiuListMultiServerProxy"/>.
        /// </summary>
        /// <param name="restClients">The BPC REST Clients through which the API calls will be made.</param>
        public MiuListMultiServerProxy(IBpcRestClients restClients)
        {
            this.restClients = restClients.RestClients.Where(c => c.Value.Environment.IsEnabled).Select(c => c.Value);
        }

        /// <summary>
        /// Gets all queryable MDCE environments.
        /// </summary>
        public IEnumerable<MdceEnvironment> GetConfiguredEnvironments()
        {
            return restClients.Select(r => r.Environment);
        }

        /// <summary>
        /// Gets MIU details from all configured environments for the supplied MIU identity information.
        /// </summary>
        /// <returns>An instance of <see cref="MultiServerResult{T}"/> containing the results of the calls to all service instances.</returns>
        public async Task<MultiServerResult<MiuListCollection>> GetMiuListAsync(string findId)
        {
            return await GetMiuListAsync(findId, new List<string>());
        }

        public async Task<MultiServerResult<MiuListCollection>> GetMiuListAsync(string findId, IEnumerable<string> excludedEnvironments)
        {
            IEnumerable<Task<SingleServerResult<MiuListCollection>>> miuListTasks = restClients
                .Where(r => !excludedEnvironments.Contains(r.Environment.Name)) // Apply exclusions
                .Select(async restClient =>
                {
                    try
                    {
                        return new SingleServerResult<MiuListCollection>(restClient,
                            await restClient.GetMiuListAsync(findId));
                    }
                    catch (System.Exception exception)
                    {
                        Log.Error(10032, string.Format("GetMiuListAsync: Service error. Environment name: \"{0}\", Find ID: {1}",
                            restClient.Environment.Name, findId), exception);

                        return new SingleServerResult<MiuListCollection>(restClient)
                        {
                            Error = exception.Message
                        };
                    }
                });

            MultiServerResult<MiuListCollection> allResults = new MultiServerResult<MiuListCollection>(await Task.WhenAll(miuListTasks));

            return allResults;
        }

        public async Task<MultiServerResult<MiuListCollection>> GetMiuListAsync(string findId, DeviceIdType idType, IEnumerable<string> excludedEnvironments)
        {
            IEnumerable<Task<SingleServerResult<MiuListCollection>>> miuListTasks = restClients
                .Where(r => !excludedEnvironments.Contains(r.Environment.Name)) // Apply exclusions
                .Select(async restClient =>
            {
                try
                {
                    return new SingleServerResult<MiuListCollection>(restClient,
                        await restClient.GetMiuListAsync(findId, idType));
                }
                catch (System.Exception exception)
                {
                    Log.Error(10032, string.Format("GetMiuListAsync: Service error. Environment name: \"{0}\", Find ID: {1}",
                        restClient.Environment.Name, findId), exception);

                    return new SingleServerResult<MiuListCollection>(restClient)
                    {
                        Error = exception.Message
                    };
                }
            });

            MultiServerResult<MiuListCollection> allResults = new MultiServerResult<MiuListCollection>(await Task.WhenAll(miuListTasks));

            return allResults;
        }
    }
}