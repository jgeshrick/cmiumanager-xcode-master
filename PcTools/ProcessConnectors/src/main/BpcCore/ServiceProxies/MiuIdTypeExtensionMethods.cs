//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;

namespace Ntg.BpcCore.ServiceProxies
{
    /// <summary>
    /// Extension methods for the <see cref="DeviceIdType"/> enumeration.
    /// </summary>
    public static class MiuIdTypeExtensionMethods
    {
        /// <summary>
        /// Gets the query string parameter key for the supplied <see cref="DeviceIdType"/>.
        /// </summary>
        /// <param name="deviceIdType"></param>
        /// <returns></returns>
        public static string GetUriParameterKey(this DeviceIdType deviceIdType)
        {
            switch (deviceIdType)
            {
                case DeviceIdType.MiuId:
                {
                    return "miu_id";
                }
                case DeviceIdType.Imei:
                {
                    return "imei";
                }
                case DeviceIdType.Iccid:
                {
                    return "iccid";
                }
                default:
                {
                    throw new ArgumentException(string.Format("Unsupported device identity type: {0}", deviceIdType),
                        "deviceIdType");
                }
            }
        }
    }
}