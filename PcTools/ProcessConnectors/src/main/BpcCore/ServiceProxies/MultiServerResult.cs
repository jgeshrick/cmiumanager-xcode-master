﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using System.Linq;

namespace Ntg.BpcCore.ServiceProxies
{
    /// <summary>
    /// Encapsulates the results from calls to 0 or more service instances.
    /// </summary>
    /// <typeparam name="T">The type of the results expected from the calls to the service instances.</typeparam>
    public class MultiServerResult<T>
    {
        public IDictionary<string, T> Results { get; private set; }

        /// <summary>
        /// Gets a dictionary of errors keyed to the environment name that caused the error.
        /// </summary>
        public IDictionary<string, string> Errors { get; private set; }

        public MultiServerResult()
        {
            Results = new Dictionary<string, T>();
            Errors = new Dictionary<string, string>();
        }

        public MultiServerResult(IEnumerable<SingleServerResult<T>> results)
        {
            var singleServerResults = results as SingleServerResult<T>[] ?? results.ToArray();

            Results = singleServerResults
                .Where(r => r.Error == null)
                .ToDictionary(r => r.EnvironmentName, r => r.Result);

            Errors = singleServerResults
                .Where(r => r.Error != null)
                .ToDictionary(r => r.EnvironmentName, r => r.Error);
        }
    }
}