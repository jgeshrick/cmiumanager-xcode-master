﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

namespace Ntg.BpcCore.ServiceProxies
{
    /// <summary>
    /// Defines the supported types of MIU identifier.
    /// </summary>
    public enum DeviceIdType
    {
        /// <summary>
        /// The MIU ID.
        /// </summary>
        MiuId,
        /// <summary>
        /// The SIM card's IMEI number.
        /// </summary>
        Imei,
        /// <summary>
        /// The SIM card's ICCID number.
        /// </summary>
        Iccid
    }
}