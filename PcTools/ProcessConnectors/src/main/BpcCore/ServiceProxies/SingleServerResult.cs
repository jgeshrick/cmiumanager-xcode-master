﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

namespace Ntg.BpcCore.ServiceProxies
{
    /// <summary>
    /// Represents a service result from a single MDCE server instance.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SingleServerResult<T>
    {
        /// <summary>
        /// Gets or sets the environment name from which the result originated.
        /// </summary>
        public string EnvironmentName { get; set; }

        /// <summary>
        /// Gets or sets the result of the service call.
        /// </summary>
        public T Result { get; set; }

        /// <summary>
        /// Gets or sets details of any error that has occurred while communicating with the server.
        /// </summary>
        /// <remarks>
        /// Null indicates no error; non-null indicates that an error condition was encountered.
        /// </remarks>
        public string Error { get; set; }

        public SingleServerResult(string environmentName)
        {
            EnvironmentName = environmentName;
        }

        public SingleServerResult(string environmentName, T result)
        {
            Result = result;
            EnvironmentName = environmentName;
        }

        public SingleServerResult(IMdceEnvironmentBpcRestClient restClient) :
            this(restClient.Environment.Name)
        {
        }

        public SingleServerResult(IMdceEnvironmentBpcRestClient restClient, T result) :
            this(restClient.Environment.Name, result)
        {
        }
    }
}