﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using System.Threading.Tasks;
using Ntg.BpcCore.JsonModel;
using Ntg.BpcCore.Settings;

namespace Ntg.BpcCore.ServiceProxies
{
    /// <summary>
    /// Provides methods for calling the MIU List MDCE API interface.
    /// </summary>
    public interface IMiuListMultiServerProxy
    {
        /// <summary>
        /// Gets all queryable MDCE environments.
        /// </summary>
        /// <returns></returns>
        IEnumerable<MdceEnvironment> GetConfiguredEnvironments();

        /// <summary>
        /// Gets MIU details from all configured environments for the supplied MIU identity information.
        /// </summary>
        /// <returns></returns>
        Task<MultiServerResult<MiuListCollection>> GetMiuListAsync(string findId);

        /// <summary>
        /// Gets MIU details from all configured environments for the supplied MIU identity information, 
        /// excluding any environments in the supplied exclusion list.
        /// </summary>
        /// <returns></returns>
        Task<MultiServerResult<MiuListCollection>> GetMiuListAsync(string toString, DeviceIdType idType, IEnumerable<string> excludedEnvironments);
    }
}