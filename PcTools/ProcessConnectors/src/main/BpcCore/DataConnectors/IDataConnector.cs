﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

namespace Ntg.BpcCore.DataConnectors
{
    /// <summary>
    /// The common interface for data services.
    /// TODO: Rename to reflect that this interface represents ETL operations, not "services".
    /// </summary>
    public interface IDataConnector
    {
        /// <summary>
        /// Gets or sets the name of the data service instance.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets a value indicating whether the data service is running.
        /// </summary>
        bool IsExecuting { get; }

        /// <summary>
        /// Gets the type enumeration of the service.
        /// </summary>
        DataConnectorType DataConnectorType { get; }

        /// <summary>
        /// Starts the data service process.
        /// </summary>
        /// <returns>True if the process completed successfully; returns null if the data service is already executing; otherwise, returns false.</returns>
        bool? Execute();
    }
}