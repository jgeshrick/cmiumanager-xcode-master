﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using Ntg.BpcCore.Entities.CustomerData;

namespace Ntg.BpcCore.DataConnectors
{
    public class AccountListEventArgs : EventArgs
    {
        public IList<Account> AccountList { get; set; }

        public AccountListEventArgs(IList<Account> accountList)
        {
            AccountList = accountList;
        }
    }
}