﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using Ntg.BpcCore.Entities.CostRevenueData;

namespace Ntg.BpcCore.DataConnectors
{
    /// <summary>
    /// Identifies owner and CMIU configuration changes based on a sequence of MIU configuration events.
    /// </summary>
    /// <remarks>
    /// Configuration changes aren't guaranteed to arrive in chronological order, so it is intended that 
    /// this class creates appropriate event data when configurations are added out of sync.
    /// </remarks>
    public class MiuConfigEventBuilder
    {
        private List<ChangeTrackedCmiuMode> changeTrackedCmiuModes;
        private List<ModeChange> modeChanges;

        /// <summary>
        /// Gets any rows with changes that need to be persisted back to the data store.
        /// </summary>
        public IEnumerable<MiuConfigHistory> UpdatedConfigHistory
        {
            get { return changeTrackedCmiuModes.Where(c => c.IsDirty).Select(c => c.Config); }
        }

        /// <summary>
        /// Gets any new mode changes that have been generated as a result of calling BuildChangeEvents().
        /// </summary>
        public IEnumerable<ModeChange> ModeChanges
        {
            get { return modeChanges; }
        }

        /// <summary>
        /// Processes the supplied configuration history entries to gather any new changes that need to be passed to QAD.
        /// </summary>
        public void ProcessConfigurationHistory(IEnumerable<MiuConfigHistory> sourceConfigHistory)
        {
            if (sourceConfigHistory == null)
            {
                throw new ArgumentNullException("sourceConfigHistory");
            }

            var configHistory = sourceConfigHistory.ToList();

            if (configHistory.Select(c => c.MiuId).Distinct().Count() > 1)
            {
                throw new ArgumentException("The supplied data contains more than one MIU ID", "sourceConfigHistory");
            }

            changeTrackedCmiuModes = configHistory
                .Where(c => c.ReceivedFromMiu) // Only include CMIU modes received from the MIU
                .OrderBy(c => c.Timestamp)
                .ThenBy(c => c.SequenceId)
                .Select(c => new ChangeTrackedCmiuMode(c))
                .ToList();

            modeChanges = new List<ModeChange>();

            BuildChangeEvents();
        }

        /// <summary>
        /// Processes the configuration history entries to look for new transfer queue events.
        /// </summary>
        private void BuildChangeEvents()
        {
            ChangeTrackedCmiuMode previousMode = null;
            foreach (var current in changeTrackedCmiuModes)
            {
                if (ShouldConsiderForModeChange(current, previousMode))
                {
                    // If the mode changed and it hasn't already been added to the transfer queue, add it.
                    if (!current.ModeChangeRaised.HasValue &&
                        !current.HasSameMiuModeAs(previousMode))
                    {
                        AddModeChange(current);
                    }
                }

                previousMode = current;
            }

            changeTrackedCmiuModes.ForEach(c => {c.UpdateConfig();});
        }

        /// <summary>
        /// Examines the current and previous configuration history entries to determine whether the current entry 
        /// should be considered as a source for a transfer queue "Mode Change" event. If the current entry is to be 
        /// considered, its ConsideredForTransferQueue timestamp is changed to the current UTC time.
        /// </summary>
        /// <param name="current">The current change-tracked configuration history entry.</param>
        /// <param name="previous">The previous change-tracked configuration history entry, or null if none exists.</param>
        /// <returns>A boolean value indicating whether the current entry should be considered for the transfer queue.</returns>
        private static bool ShouldConsiderForModeChange(ChangeTrackedCmiuMode current, ChangeTrackedCmiuMode previous)
        {
            bool considerForTransferQueue = false;

            if (!current.ConsideredForTransferQueue.HasValue)
            {
                // Always consider a record for the queue if it hasn't been already.
                considerForTransferQueue = true;
            }
            else if (previous != null && previous.IsModeChangeRaisedDirty)
            {
                // Consider a record for the queue if a change was identified in the previous entry.
                considerForTransferQueue = true;
            }

            if (considerForTransferQueue)
            {
                current.ConsideredForTransferQueue = DateTime.UtcNow;
            }

            return considerForTransferQueue;
        }

        /// <summary>
        /// Adds a mode change entry to the modeChanges list based on the supplied MIU configuration entry.
        /// </summary>
        /// <param name="entry">The MIU configuration entry as the source of a mode change event.</param>
        private void AddModeChange(ChangeTrackedCmiuMode entry)
        {
            entry.ModeChangeRaised = DateTime.UtcNow;

            modeChanges.Add(ModeChange.From(entry.Config));
        }

        /// <summary>
        /// Helper class to track CMIU mode changes.
        /// </summary>
        private class ChangeTrackedCmiuMode
        {
            public DateTime? ModeChangeRaised
            {
                get { return Config.ModeChangeRaised; }
                set
                {
                    IsDirty = true;
                    IsModeChangeRaisedDirty = true;
                    Config.ModeChangeRaised = value;
                }
            }

            public bool IsModeChangeRaisedDirty { get; private set; }

            public ChangeTrackedCmiuMode(MiuConfigHistory config)
            {
                this.config = config;
                consideredForTransferQueue = config.ConsideredForTransferQueue;
            }

            /// <summary>
            /// Compares the CMIU modes of two <see cref="ChangeTrackedCmiuMode"/> instances.
            /// </summary>
            /// <param name="other">The configuration being compared against.</param>
            /// <returns>A boolean value indicating whether the other instance has the same CMIU mode; if <paramref name="other"/> is null, returns false.</returns>
            public bool HasSameMiuModeAs(ChangeTrackedCmiuMode other)
            {
                if (other == null)
                {
                    return false;
                }

                if (Config.RecordingIntervalMinutes != other.Config.RecordingIntervalMinutes ||
                    Config.ReportingIntervalMinutes != other.Config.ReportingIntervalMinutes)
                {
                    return false;
                }

                return true;
            }

            private DateTime? consideredForTransferQueue;

            private readonly MiuConfigHistory config;

            public MiuConfigHistory Config
            {
                get { return config; }
            }

            public DateTime? ConsideredForTransferQueue
            {
                get { return consideredForTransferQueue; }
                set
                {
                    IsDirty = true;
                    consideredForTransferQueue = value;
                }
            }

            public bool IsDirty { get; protected set; }

            /// <summary>
            /// Updates the ConsideredForTransferQueue on the config instance if the tracked value has been set to a higher value.
            /// </summary>
            public void UpdateConfig()
            {
                if (IsDirty && consideredForTransferQueue.HasValue)
                {
                    if (!config.ConsideredForTransferQueue.HasValue || consideredForTransferQueue > config.ConsideredForTransferQueue)
                    {
                        config.ConsideredForTransferQueue = consideredForTransferQueue;
                    }
                }
            }

        }
    }
}