﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using Ntg.BpcCore.Entities.CostRevenueData;

namespace Ntg.BpcCore.DataConnectors
{
    /// <summary>
    /// Identifies owner and MIU site ID changes based on a sequence of MIU owner events.
    /// </summary>
    /// <remarks>
    /// Configuration changes aren't guaranteed to arrive in chronological order, so it is intended that 
    /// this class creates appropriate event data when site IDs are added out of sync.
    /// </remarks>
    public class MiuOwnerEventBuilder
    {
        private List<ChangeTrackedSiteId> changeTrackedSiteIds;
        private List<SiteIdChange> siteIdChanges;

        /// <summary>
        /// Gets any rows with changes that need to be persisted back to the data store.
        /// </summary>
        public IEnumerable<MiuOwnerHistory> UpdatedMiuOwnerHistory
        {
            get { return changeTrackedSiteIds.Where(c => c.IsDirty).Select(c => c.MiuOwner); }
        }

        /// <summary>
        /// Gets any new site ID changes that have been generated as a result of calling BuildChangeEvents().
        /// </summary>
        public IEnumerable<SiteIdChange> SiteIdChanges
        {
            get { return siteIdChanges; }
        }

        /// <summary>
        /// Processes the supplied MIU owner history entries to gather any new changes that need to be passed to QAD.
        /// </summary>
        public void ProcessMiuOwnerHistory(IEnumerable<MiuOwnerHistory> sourceOwnerHistory)
        {
            if (sourceOwnerHistory == null)
            {
                throw new ArgumentNullException("sourceOwnerHistory");
            }

            var ownerHistory = sourceOwnerHistory.ToList();

            if (ownerHistory.Select(c => c.MiuId).Distinct().Count() > 1)
            {
                throw new ArgumentException("The supplied data contains more than one MIU ID", "sourceOwnerHistory");
            }

            changeTrackedSiteIds = ownerHistory
                .OrderBy(c => c.Timestamp)
                .ThenBy(c => c.SequenceId)
                .Select(c => new ChangeTrackedSiteId(c))
                .ToList();

            siteIdChanges = new List<SiteIdChange>();

            BuildChangeEvents();
        }

        /// <summary>
        /// Processes the owner history entries to look for new transfer queue events.
        /// </summary>
        private void BuildChangeEvents()
        {
            ChangeTrackedSiteId previousSiteId = null;
            foreach (var current in changeTrackedSiteIds)
            {
                if (ShouldConsiderForSiteIdChange(current, previousSiteId))
                {
                    // If the site ID changed and it hasn't already been added to the transfer queue, add it.
                    if (!current.SiteIdChangeRaised.HasValue &&
                        !current.HasSameSiteIdAs(previousSiteId))
                    {
                        AddSiteIdChange(current);
                    }
                }

                previousSiteId = current;
            }

            changeTrackedSiteIds.ForEach(c => {c.UpdateConfig();});
        }

        /// <summary>
        /// Examines the current and previous configuration history entries to determine whether the current entry 
        /// should be considered as a source for a transfer queue "ISB Move EU to New Customer" event. If the current 
        /// entry is to be considered, its ConsideredForTransferQueue timestamp is changed to the current UTC time.
        /// </summary>
        /// <param name="current">The current change-tracked configuration history entry.</param>
        /// <param name="previous">The previous change-tracked configuration history entry, or null if none exists.</param>
        /// <returns>A boolean value indicating whether the current entry should be considered for the transfer queue.</returns>
        private static bool ShouldConsiderForSiteIdChange(ChangeTrackedSiteId current, ChangeTrackedSiteId previous)
        {
            bool considerForTransferQueue = false;

            if (!current.ConsideredForTransferQueue.HasValue)
            {
                // Always consider a record for the queue if it hasn't already.
                considerForTransferQueue = true;
            }
            else if (previous != null && previous.IsSiteIdChangeRaisedDirty)
            {
                // Consider a record for the queue if the previous entry was changed.
                considerForTransferQueue = true;
            }

            if (considerForTransferQueue)
            {
                current.ConsideredForTransferQueue = DateTime.UtcNow;
            }

            return considerForTransferQueue;
        }

        /// <summary>
        /// Adds a site ID change entry to the modeChanges list based on the supplied MIU configuration entry.
        /// </summary>
        /// <param name="entry">The MIU configuration entry as the source of a site ID change event.</param>
        private void AddSiteIdChange(ChangeTrackedSiteId entry)
        {
            entry.SiteIdChangeRaised = DateTime.UtcNow;

            siteIdChanges.Add(SiteIdChange.From(entry.MiuOwner));
        }

        /// <summary>
        /// Helper class to track site ID changes.
        /// </summary>
        private class ChangeTrackedSiteId
        {
            public DateTime? SiteIdChangeRaised
            {
                get { return MiuOwner.SiteIdChangeRaised; }
                set
                {
                    IsDirty = true;
                    IsSiteIdChangeRaisedDirty = true;
                    MiuOwner.SiteIdChangeRaised = value;
                }
            }

            public bool IsSiteIdChangeRaisedDirty { get; private set; }

            public ChangeTrackedSiteId(MiuOwnerHistory miuOwner)
            {
                this.miuOwner = miuOwner;
                consideredForTransferQueue = this.miuOwner.ConsideredForTransferQueue;
            }

            /// <summary>
            /// Compares the site IDs of two <see cref="ChangeTrackedSiteId"/> instances.
            /// </summary>
            /// <param name="other">The configuration being compared against.</param>
            /// <returns>A boolean value indicating whether the other instance has the same site ID; if <paramref name="other"/> is null, returns false.</returns>
            public bool HasSameSiteIdAs(ChangeTrackedSiteId other)
            {
                if (other == null || MiuOwner.SiteId != other.MiuOwner.SiteId)
                {
                    return false;
                }

                return true;
            }

            private DateTime? consideredForTransferQueue;

            private readonly MiuOwnerHistory miuOwner;

            public MiuOwnerHistory MiuOwner
            {
                get { return miuOwner; }
            }

            public DateTime? ConsideredForTransferQueue
            {
                get { return consideredForTransferQueue; }
                set
                {
                    IsDirty = true;
                    consideredForTransferQueue = value;
                }
            }

            public bool IsDirty { get; protected set; }

            /// <summary>
            /// Updates the ConsideredForTransferQueue on the MIU owner instance if the tracked value has been set to a higher value.
            /// </summary>
            public void UpdateConfig()
            {
                if (IsDirty)
                {
                    if (consideredForTransferQueue.HasValue)
                    {
                        if (!miuOwner.ConsideredForTransferQueue.HasValue || consideredForTransferQueue > miuOwner.ConsideredForTransferQueue)
                        {
                            miuOwner.ConsideredForTransferQueue = consideredForTransferQueue;
                        }
                    }
                }
            }
        }
    }
}