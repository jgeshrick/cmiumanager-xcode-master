﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Ntg.BpcCore.Domain;

namespace Ntg.BpcCore.DataConnectors
{
    /// <summary>
    /// Cost and revenue data connector implementation.
    /// </summary>
    public class CostRevenueDataConnector : IDataConnector
    {
        private const string EtlStatusKey = "State";

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IMdceEnvironmentBpcRestClient mdceRestClient;
        private readonly ICostRevenueDataRepository crRepository;

        public CostRevenueDataConnector(IMdceEnvironmentBpcRestClient mdceRestClient, ICostRevenueDataRepository crRepository)
        {
            if (!mdceRestClient.Environment.IsEnabled)
            {
                var message = string.Format("The supplied MDCE client \"{0}\" is disabled.", this.mdceRestClient.Environment.Name);
                Log.Error(10046, message);
                throw new InvalidOperationException(message);
            }

            this.mdceRestClient = mdceRestClient;
            this.crRepository = crRepository;
            Name = "Cost and Revenue Data Connector";
            DataConnectorType = DataConnectorType.CostRevenueDataConnector;
        }

        public string Name { get; set; }
        
        public bool IsExecuting { get; private set; }
        
        public DataConnectorType DataConnectorType { get; private set; }
        
        public bool? Execute()
        {
            if (IsExecuting)
            {
                Log.Debug(10040, "CostRevenueDataConnector is already executing. Ignoring the call to Execute().");
                return null;
            }

            IsExecuting = true;
            try
            {
                // Sync from MDCE to C&R database
                Log.Info(10041, string.Format("Connecting to environment {0}.", mdceRestClient.Environment.Name));
                RefreshMiuDetails().GetAwaiter().GetResult();
                UpdateConfigHistory().GetAwaiter().GetResult();
                UpdateOwnerHistory().GetAwaiter().GetResult();
                UpdateLifecycleHistory().GetAwaiter().GetResult();
                WriteNewConfigEventsToTransferQueueAsync().GetAwaiter().GetResult();
                WriteNewOwnerEventsToTransferQueue().GetAwaiter().GetResult();

                Log.Info(10042, "Completed C&R data update.");
                crRepository.SetEtlStatusAsync(EtlStatusKey, "OK").GetAwaiter().GetResult();

                return true;
            }
            catch (System.Exception ex)
            {
                Log.Error(10043, "Failed during C&R data update.", ex);

                try
                {
                    crRepository.SetEtlStatusAsync(EtlStatusKey, "Error").GetAwaiter().GetResult();
                }
                catch(System.Exception innerEx)
                {
                    Log.Error(10047, "Unable to set ETL Status.", innerEx);
                }

                return false;
            }
            finally
            {
                IsExecuting = false;
            }
        }

        /// <summary>
        /// Refreshes the MIU Details using the full MIU list from MDCE.
        /// </summary>
        /// <returns></returns>
        private async Task RefreshMiuDetails()
        {
            await crRepository.SetEtlStatusAsync(EtlStatusKey, "Loading MIU details");

            // Get the full list of MIUs from the environment.
            var miuList = await mdceRestClient.GetMiuListAsync(MiuListResponseType.IncludeCellularIdentities);

            if (miuList.Mius != null)
            {
                await crRepository.SetEtlStatusAsync(EtlStatusKey, "Updating MIU details");
                await crRepository.SetMiuDetailsAsync(miuList.Mius);
            }
        }

        /// <summary>
        /// Appends any new MIU configuration history items from MDCE to the data store.
        /// </summary>
        /// <returns></returns>
        private async Task UpdateConfigHistory()
        {
            await crRepository.SetEtlStatusAsync(EtlStatusKey, "Updating configuration history");
            var lastSequenceId = await crRepository.GetMaxMiuConfigHistorySequenceIdAsync();

            Log.Info(10048, string.Format("CostRevenueDataConnector.UpdateConfigHistory: Starting from sequence ID {0}.", lastSequenceId.HasValue ? lastSequenceId.ToString() : "<null>"));

            while (true)
            {
                var miuConfigHistory = await mdceRestClient.GetConfigurationHistoryAsync(lastSequenceId);

                if (miuConfigHistory.Count == 0)
                {
                    break;
                }

                lastSequenceId = miuConfigHistory.Max(h => h.SequenceId);
                await crRepository.AppendMiuConfigHistoryAsync(miuConfigHistory);
                Log.Info(10049, string.Format("CostRevenueDataConnector.UpdateConfigHistory: Appended up to sequence ID {0}.", lastSequenceId));
            }
        }

        private async Task UpdateOwnerHistory()
        {
            await crRepository.SetEtlStatusAsync(EtlStatusKey, "Updating MIU owner history");
            var lastSequenceId = await crRepository.GetMaxMiuOwnerHistorySequenceIdAsync();

            Log.Info(10064, string.Format("CostRevenueDataConnector.UpdateOwnerHistory: Starting from sequence ID {0}.", lastSequenceId.HasValue ? lastSequenceId.ToString() : "<null>"));
            
            while (true)
            {
                var miuOwnerHistoryCollection = await mdceRestClient.GetMiuOwnerHistoryAsync(lastSequenceId);
                var miuOwnerHistory = miuOwnerHistoryCollection.OwnerHistory;

                if (miuOwnerHistory.Count == 0)
                {
                    break;
                }

                lastSequenceId = miuOwnerHistory.Max(h => h.SequenceId);
                await crRepository.AppendMiuOwnerHistoryAsync(miuOwnerHistory);
                Log.Info(10065, string.Format("CostRevenueDataConnector.UpdateOwnerHistory: Appended up to sequence ID {0}.", lastSequenceId));
            }
        }

        /// <summary>
        /// Appends any new MIU lifecycle state changes from MDCE to the data store.
        /// </summary>
        /// <returns></returns>
        private async Task UpdateLifecycleHistory()
        {
            await crRepository.SetEtlStatusAsync(EtlStatusKey, "Updating lifecycle history");
            var lastSequenceId = await crRepository.GetMaxMiuLifecycleHistorySequenceIdAsync();

            Log.Info(10060, string.Format("CostRevenueDataConnector.UpdateLifecycleHistory: Starting from sequence ID {0}.", lastSequenceId.HasValue ? lastSequenceId.ToString() : "<null>"));

            while (true)
            {
                var miuLifecycleHistory = await mdceRestClient.GetLifecycleHistoryAsync(lastSequenceId);

                if (miuLifecycleHistory.Count == 0)
                {
                    break;
                }

                lastSequenceId = miuLifecycleHistory.Max(h => h.SequenceId);
                await crRepository.AppendMiuLifecycleHistoryAsync(miuLifecycleHistory);
                Log.Info(10061, string.Format("CostRevenueDataConnector.UpdateLifecycleHistory: Appended up to sequence ID {0}.", lastSequenceId));
            }
        }

        /// <summary>
        /// Scans for new MIU configuration history items and raises events on the AD transfer queue where any configuration changes are detected.
        /// </summary>
        /// <returns></returns>
        private async Task WriteNewConfigEventsToTransferQueueAsync()
        {
            await crRepository.SetEtlStatusAsync(EtlStatusKey, "Adding new MIU config events to Transfer Queue");
            var miuConfigEventBuilder = new MiuConfigEventBuilder();
            Log.Info(10062, "CostRevenueDataConnector.WriteNewConfigEventsToTransferQueue.");

            var miuIds = await crRepository.GetMiuIdsWithPendingConfigurationHistoryAsync();
            foreach (var miuId in miuIds)
            {
                try
                {
                    var configHistory = await crRepository.GetMiuConfigHistoryByMiuIdAsync(miuId);
                    miuConfigEventBuilder.ProcessConfigurationHistory(configHistory);

                    // Write events to queue and update timestamps on config history records
                    await crRepository.WriteTransferQueueEventsAsync(
                        miuConfigEventBuilder.ModeChanges,
                        miuConfigEventBuilder.UpdatedConfigHistory);
                }
                catch (System.Exception ex)
                {
                    Log.Error(
                        10063,
                        string.Format("CostRevenueDataConnector.WriteNewConfigEventsToTransferQueue: exception encountered while processing MIU ID {0}.", miuId), 
                        ex);
                }
            }
        }

        /// <summary>
        /// Scans for new MIU owner history items and raises events on the AD transfer queue where any site ID changes are detected.
        /// </summary>
        /// <returns></returns>
        private async Task WriteNewOwnerEventsToTransferQueue()
        {
            await crRepository.SetEtlStatusAsync(EtlStatusKey, "Adding new MIU owner events to Transfer Queue");
            var miuOwnerEventBuilder = new MiuOwnerEventBuilder();
            Log.Info(10066, "CostRevenueDataConnector.WriteNewOwnerEventsToTransferQueue.");

            var miuIds = await crRepository.GetMiuIdsWithPendingOwnerHistoryAsync();
            foreach (var miuId in miuIds)
            {
                try
                {
                    var miuOwnerHistory = await crRepository.GetMiuOwnerHistoryByMiuIdAsync(miuId);
                    miuOwnerEventBuilder.ProcessMiuOwnerHistory(miuOwnerHistory);

                    // Write events to queue and update timestamps on MIU owner history records
                    await crRepository.WriteTransferQueueEventsAsync(
                        miuOwnerEventBuilder.SiteIdChanges,
                        miuOwnerEventBuilder.UpdatedMiuOwnerHistory);
                }
                catch (System.Exception ex)
                {
                    Log.Error(
                        10067,
                        string.Format("CostRevenueDataConnector.WriteNewOwnerEventsToTransferQueue: exception encountered while processing MIU ID {0}.", miuId),
                        ex);
                }
            }
        }
    }
}
