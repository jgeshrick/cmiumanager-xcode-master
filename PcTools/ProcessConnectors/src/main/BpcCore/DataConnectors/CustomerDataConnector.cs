﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Ntg.BpcCore.DataTransformExtensions;
using Ntg.BpcCore.DataTransformExtensions.CustomerData;
using Ntg.BpcCore.Domain;
using Ntg.BpcCore.JsonModel.CrmReference;

namespace Ntg.BpcCore.DataConnectors
{
    /// <summary>
    /// Implement Sales Logix synchronization service.
    /// </summary>
    public class CustomerDataConnector: IDataConnector
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly object SyncLock = new object();
        private readonly ICustomerDataRepository customerRepository;
        private readonly IBpcRestClients bpcRestClients;
        private bool isExecuting;
        private const int BatchSize = 100;

        /// <summary>
        /// An event handler to allow inspection of the account list by observers immediately after it has loaded.
        /// </summary>
        public event EventHandler<AccountListEventArgs> AccountListReceived;

        public CustomerDataConnector(IBpcRestClients bpcRestClients, ICustomerDataRepository customerRepository)
        {
            this.bpcRestClients = bpcRestClients;
            this.customerRepository = customerRepository;
            Name = "Customer Data Connector";
        }

        /// <summary>
        /// Gets or sets the name of the data service instance.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets a value indicating whether the data service is running.
        /// </summary>
        public bool IsExecuting
        {
            get
            {
                lock (SyncLock)
                {
                    return isExecuting;
                }
            }
            private set
            {
                lock (SyncLock)
                {
                    isExecuting = value;
                }
            }
        }

        /// <summary>
        /// Gets the type enumeration of the service.
        /// </summary>
        public DataConnectorType DataConnectorType { get { return DataConnectorType.CustomerDataConnector; } }

        /// <summary>
        /// Periodic schedule task to retrieve customer data from SLX MS SQL database and push to MDCE via REST.
        /// </summary>
        /// <returns></returns>
        public bool? Execute()
        {
            if (IsExecuting)
            {
                Log.Debug(10017, "CustomerDataConnector is already executing. Ignoring the call to Execute().");
                return null;
            }

            IsExecuting = true;
            try
            {
                // Load account data from SLX
                var customerList = customerRepository.GetAccountList();
                
                // Notify observers
                OnAccountListReceived(new AccountListEventArgs(customerList));

                // Convert into JSON objects
                var crmRefData = customerList.ToCrmReference();

                // Push to each MDCE environment
                Log.Info(10018, "Starting CRM Reference data update.");

                // TODO: Use async pattern
                var results = bpcRestClients.RestClients
                    .Where(client => client.Value.Environment.IsEnabled)
                    .Select(client =>
                    {
                        Log.Info(10019, string.Format("Setting CRM Reference data on MDCE environment: \"{0}\", base URL: \"{1}\".",
                            client.Value.Environment.Name, client.Value.Environment.BaseUrl));

                        try
                        {
                            // Determine whether the server supports PATCH requests for refdata, and use a single request if it doesn't.
                            if (CanBatchUpload(client))
                            {
                                return UploadCrmReferenceDataBatched(client, crmRefData);
                            }
                            else
                            {
                                return client.Value.SetCrmReferenceData(crmRefData);
                            }
                        }
                        catch (System.Exception ex)
                        {
                            Log.Error(10150, string.Format("Failed during CRM update on {0}.", client.Value.Environment.Name), ex);

                            return false;
                        }
                    })
                    .ToList();

                Log.Info(10020, "Completed CRM Reference data update.");
                
                return results.All(r => r);
            }
            catch (System.Exception ex)
            {
                Log.Error(10021, "Failed during CRM update.", ex);

                return false;
            }
            finally
            {
                IsExecuting = false;
            }
        }

        /// <summary>
        /// Attempt to determine whether the server supports the PATCH verb on refdata.
        /// </summary>
        private static bool CanBatchUpload(KeyValuePair<string, IMdceEnvironmentBpcRestClient> client)
        {
            try
            {
                // Attempt to append an empty batch of data. Service will return error if PATCH not supported.
                var empty = new CrmReference();
                client.Value.AppendCrmReferenceData(empty);
            }
            catch (System.Exception)
            {
                return false;
            }

            return true;
        }

        private static bool UploadCrmReferenceDataBatched(KeyValuePair<string, IMdceEnvironmentBpcRestClient> client, CrmReference crmRefData)
        {
            bool result = true;

            // Utilities
            for (int i = 0; i < crmRefData.Utilities.Count; i += BatchSize)
            {
                var batch = new CrmReference
                {
                    Utilities = crmRefData.Utilities.Skip(i).Take(BatchSize).ToList()
                };

                result &= (i == 0) ? client.Value.SetCrmReferenceData(batch) : client.Value.AppendCrmReferenceData(batch);
            }

            // Distributors
            for (int i = 0; i < crmRefData.Distributers.Count; i += BatchSize)
            {
                var batch = new CrmReference
                {
                    Distributers = crmRefData.Distributers.Skip(i).Take(BatchSize).ToList()
                };

                result &= client.Value.AppendCrmReferenceData(batch);
            }

            return result;
        }

        /// <summary>
        /// Notifies observers that the SLX accounts list has been loaded.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnAccountListReceived(AccountListEventArgs e)
        {
            var handler = AccountListReceived;
            if (handler != null)
                handler(this, e);
        }
    }
}
