﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Ntg.BpcCore.Domain;
using Ntg.BpcCore.Entities.LifecycleData;
using Ntg.BpcCore.JsonModel.LifecycleHistory;

namespace Ntg.BpcCore.DataConnectors
{
    /// <summary>
    /// Passes MIU lifecycle events from QAD to MDCE.
    /// </summary>
    public class MiuLifecycleDataConnector: IDataConnector
    {
        /// <summary>
        /// The maximum number of transactions per batch.
        /// </summary>
        private const int BatchSize = 100;

        private const string MiuLifecycleHistoryTransactionType = "12";

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IMdceEnvironmentBpcRestClient mdceRestClient;
        private readonly ITransferQueueRepository transferQueueRepository;

        public MiuLifecycleDataConnector(IMdceEnvironmentBpcRestClient mdceRestClient, ITransferQueueRepository transferQueueRepository)
        {
            this.mdceRestClient = mdceRestClient;
            this.transferQueueRepository = transferQueueRepository;
            Name = "MIU Lifecycle Data Connector";
            DataConnectorType = DataConnectorType.MiuLifecycleDataConnector;
        }

        public string Name { get; set; }
        public bool IsExecuting { get; private set; }
        public bool Success { get; private set; }
        public DataConnectorType DataConnectorType { get; private set; }
        public bool? Execute()
        {
            if (IsExecuting)
            {
                Log.Debug(10100, "MiuLifecycleDataConnector is already executing. Ignoring the call to Execute().");
                return null;
            }

            Log.Info(10101, "MiuLifecycleDataConnector.Execute was called.");
            IsExecuting = true;
            try
            {
                bool success = true;

                while (true)
                {
                    var transactions = transferQueueRepository
                        .GetOpenTransactionsAsync(MiuLifecycleHistoryTransactionType, BatchSize)
                        .GetAwaiter()
                        .GetResult();

                    Log.Info(10102, string.Format("MiuLifecycleDataConnector.Execute: Received {0} transactions.", transactions.Count));

                    if (!transactions.Any())
                    {
                        break;
                    }

                    success &= ProcessTransactions(transactions).GetAwaiter().GetResult();
                }

                Log.Info(10103, "Completed lifecycle data update.");
                Success = success;

                return success;
            }
            catch (System.Exception ex)
            {
                Log.Error(10104, "Failed during MIU lifecycle data update.", ex);

                return false;
            }
            finally
            {
                IsExecuting = false;
            }
        }

        /// <summary>
        /// Process the supplied transactions.
        /// </summary>
        /// <param name="transactions"></param>
        /// <returns>True if all transactions were successfully processed; false if any failures were encountered.</returns>
        private async Task<bool> ProcessTransactions(List<QadTransaction> transactions)
        {
            // Set to pending
            transactions.ForEach(t => t.Status = "P");
            await transferQueueRepository.UpdateTransactionStatusAsync(transactions);

            int i = 0;
            var transactionMap = new Dictionary<int, QadTransaction>();
            transactions.ForEach(t => transactionMap.Add(++i, t));

            // Prepare MDCE request
            var serviceRequest = new SetMiuLifecycleHistoryRequest {LifecycleStates = new List<HistoricMiuLifecycleState>()};

            foreach (var kvp in transactionMap)
            {
                var referenceId = kvp.Key;
                var transaction = kvp.Value;
                string error;
                var jsonLifecycleState = transaction.GetHistoricMiuLifecycleState(referenceId, out error);
                if (jsonLifecycleState == null)
                {
                    transaction.Status = "E";
                    transaction.Message = error;
                    transaction.OutDate = DateTime.UtcNow;
                }
                else
                {
                    serviceRequest.LifecycleStates.Add(jsonLifecycleState);
                }
            }

            // Send to MDCE
            SetMiuLifecycleHistoryResult serviceResult;
            try
            {
                serviceResult = await mdceRestClient.SetMiuLifecycleHistoryAsync(serviceRequest);
            }
            catch (System.Exception ex)
            {
                Log.Error(10104, "MiuLifecycleDataConnector.ProcessTransactions encountered exception while contacting MDCE.", ex);
                transactions.Where(t => t.Status.Equals("P", StringComparison.OrdinalIgnoreCase)).ToList().ForEach(t =>
                {
                    t.Status = "E";
                    t.Message = "MDCE connection error";
                    t.OutDate = DateTime.UtcNow;
                });

                try
                {
                    transferQueueRepository.UpdateTransactionStatusAsync(transactions).GetAwaiter().GetResult();
                }
                catch (System.Exception innerEx)
                {
                    Log.Error(10104, "MiuLifecycleDataConnector.ProcessTransactions encountered exception while updated transaction status.", innerEx);
                }

                throw;
            }

            // Update transaction status
            foreach (var errorResponse in serviceResult.ErrorDetails)
            {
                if (transactionMap.ContainsKey(errorResponse.ReferenceId))
                {
                    transactionMap[errorResponse.ReferenceId].Status = "E";
                    transactionMap[errorResponse.ReferenceId].Message = errorResponse.Error;
                    transactionMap[errorResponse.ReferenceId].OutDate = DateTime.UtcNow;
                }
            }

            transactions.Where(t => t.Status.Equals("P", StringComparison.OrdinalIgnoreCase)).ToList().ForEach(t =>
            {
                t.Status = "C";
                t.OutDate = DateTime.UtcNow;
                t.Message = null;
            });

            transferQueueRepository.UpdateTransactionStatusAsync(transactions).GetAwaiter().GetResult();

            return !transactions.Any(t => t.Status.Equals("E", StringComparison.OrdinalIgnoreCase));
        }
    }
}
