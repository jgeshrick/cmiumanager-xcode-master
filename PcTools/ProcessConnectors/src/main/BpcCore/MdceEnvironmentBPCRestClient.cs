﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Threading.Tasks;
using Ntg.BpcCore.JsonModel.LifecycleHistory;
using Ntg.BpcCore.Settings;

namespace Ntg.BpcCore
{
    public class MdceEnvironmentBpcRestClient : BpcRestClient, IMdceEnvironmentBpcRestClient
    {
        public MdceEnvironment Environment { get; private set; }

        public MdceEnvironmentBpcRestClient(MdceEnvironment environment, IEnvironmentSettingsRepository environmentSettingsRepository):
            base(environmentSettingsRepository.GetBpcPartnerString(environment.Name), Constants.BpcSiteId.ToString(), environment.BaseUrl)
        {
            this.Environment = environment;

            // Ensure any changes to the partner string are put into effect immediately.
            this.Environment.ParnerStringChanged += OnParnerStringChanged;
        }

        private void OnParnerStringChanged(object sender, ParnerStringChangedEventArgs e)
        {
            ChangePartnerString(e.NewPartnerString);
        }
    }
}
