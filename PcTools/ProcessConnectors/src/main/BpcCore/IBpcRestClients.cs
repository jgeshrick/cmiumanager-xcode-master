﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;

namespace Ntg.BpcCore
{
    /// <summary>
    /// When implemented, provides access to MDCE ReST clients by environment name.
    /// </summary>
    public interface IBpcRestClients
    {
        /// <summary>
        /// Gets a read-only dictionary of MDCE ReST clients by environment name.
        /// </summary>
        IReadOnlyDictionary<string, IMdceEnvironmentBpcRestClient> RestClients { get; }
    }
}