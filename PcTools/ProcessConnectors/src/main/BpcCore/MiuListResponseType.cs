//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

namespace Ntg.BpcCore
{
    /// <summary>
    /// Used to select whether extended MIU details should be included in the MIU List response.
    /// </summary>
    public enum MiuListResponseType
    {
        /// <summary>
        /// Indicates that only the MIU ID, Site ID and INSERT timestamps are required.
        /// </summary>
        Basic,
        /// <summary>
        /// Indicates that cellular/LoRa identities should be included in the response.
        /// </summary>
        IncludeCellularIdentities
    }
}