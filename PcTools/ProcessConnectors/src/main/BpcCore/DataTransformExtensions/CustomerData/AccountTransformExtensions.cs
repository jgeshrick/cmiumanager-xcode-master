﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Ntg.BpcCore.Entities.CustomerData;
using Ntg.BpcCore.JsonModel.CrmReference;

namespace Ntg.BpcCore.DataTransformExtensions.CustomerData
{
    public static class AccountTransformExtensions
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Internal function to transform SLX data to CRM Reference POCO.
        /// </summary>
        /// <param name="accounts"></param>
        /// <returns></returns>
        public static CrmReference ToCrmReference(this IList<Account> accounts)
        {
            var customerNumberLookup = accounts.GetCustomerNumberDictionary();

            var crmRef = new CrmReference()
            {
                Distributers = ExtractDistributerList(accounts, customerNumberLookup),
                Utilities = ExtractUtilitiesList(accounts, customerNumberLookup)
            };

            return crmRef;
        }

        /// <summary>
        /// Take Account list and transform JSON Json POCO Distributer List.
        /// </summary>
        /// <param name="accounts"></param>
        /// <param name="customerNumberLookup"></param>
        /// <returns></returns>
        private static List<Distributer> ExtractDistributerList(IEnumerable<Account> accounts, Dictionary<string, List<string>> customerNumberLookup)
        {
            //construct distributor.. direct customer
            //TODO: is distributor without a parent customer id?

            //group account entries together by same account id
            var distributerList = accounts
                .Where(account => string.IsNullOrEmpty(account.ParentAccountId)) //assuming distributer does not have a parent account id
                .GroupBy(account => account.AccountId)  //group same accountId together
                .Select(acctGroup =>    //apply transformation
                {
                    var distributor = new Distributer();

                    var account = acctGroup.ElementAt(0);
                    distributor.CustomerNumber = account.CustomerNumber;

                    var distributorInfo = new DistributerInfo();
                    distributor.Info = distributorInfo;

                    //TODO: Check all json fields are defined in POCO and filled in here
                    distributorInfo.AccountId = account.AccountId;
                    distributorInfo.AccountName = account.AccountName;
                    distributorInfo.Address1 = account.Address1;
                    distributorInfo.Address2 = account.Address2;
                    distributorInfo.Address3 = account.Address3;

                    distributorInfo.City = account.City;
                    distributorInfo.State = account.State;
                    distributorInfo.PostalCode = account.PostalCode;
                    distributorInfo.Country = account.Country;

                    distributorInfo.MainPhone = account.MainPhone;
                    distributorInfo.Fax = account.Fax;
                    distributorInfo.SalesTerritoryOwner = account.SalesTerritoryOwner;

                    distributorInfo.AccountManager = account.AccountManager;
                    distributorInfo.RegionalManager = account.RegionalManager;

                    distributorInfo.Type = account.AccountType;
                    distributorInfo.SubType = account.AccountSubtype;

                    distributorInfo.Status = account.AccountStatus;

                    distributorInfo.Contact = acctGroup
                        .Where(a => a.IsPrimaryContact())
                        .Select(a => new Contact()
                        {
                            Name = string.Format("{0} {1}", a.ContactFirstName, a.ContactLastName),
                            Title = a.ContactTitle,
                            WorkPhone = a.ContactWorkPhone
                        })
                        .FirstOrDefault();   //get first contact which is marked primary

                    //get list of secondary contacts
                    distributorInfo.ActiveSecondaryContacts = acctGroup
                        .Where(a => !a.IsPrimaryContact() && a.IsContactActive())
                        .Select(a => new Contact()
                        {
                            Name = string.Format("{0} {1}", a.ContactFirstName, a.ContactLastName),
                            Title = a.ContactTitle,
                            WorkPhone = a.ContactWorkPhone
                        }).
                        ToList();

                    List<string> customerNumbers;
                    if (account.CustomerNumber != null && customerNumberLookup.TryGetValue(account.CustomerNumber, out customerNumbers))
                        distributorInfo.CustomerNumbers = customerNumbers;

                    distributorInfo.ParentCustomerNumber = account.ParentCustomerNumber;

                    return distributor;
                })
                .ToList();

            return distributerList;
        }

        /// <summary>
        /// Take Account list and transform into JSON POCO Utility List.
        /// </summary>
        /// <param name="accountList"></param>
        /// <param name="customerNumberLookup"></param>
        /// <returns></returns>
        private static List<Utility> ExtractUtilitiesList(IEnumerable<Account> accountList, Dictionary<string, List<string>> customerNumberLookup)
        {
            //FIXME.. we do not know how to identify utility- currently just generate utility from all accounts
            var utilityList = accountList
                // TODO: As a temporary workaround for "distributor" site IDs not being shown in MDCE's MIU List Site ID drop-down list, 
                // TODO: distributors are also sent to MDCE as utilities, with their parent customer ID set to their distributor record. 
                // TODO: Needs discussion with JD.
                //.Where(account => !string.IsNullOrEmpty(account.ParentAccountId))
                .GroupBy(account => account.AccountId)  //group same accountId together
                .Select(acctGroup =>    //apply transformation
                {
                    var utility = new Utility();

                    var account = acctGroup.ElementAt(0);

                    utility.SiteId = Int32.Parse(account.SiteId);

                    var utilityInfo = new UtilityInfo();
                    utility.Info = utilityInfo;

                    //TODO: Check all json fields are defined in POCO and filled in here
                    //utilityInfo.AccountId = account.AccountId;

                    utilityInfo.SystemId = account.SystemId;
                    utilityInfo.AccountName = account.AccountName;
                    utilityInfo.CustomerNumber = account.CustomerNumber;

                    utilityInfo.Address1 = account.Address1;
                    utilityInfo.Address2 = account.Address2;
                    utilityInfo.Address3 = account.Address3;
                    utilityInfo.City = account.City;
                    utilityInfo.State = account.State;
                    utilityInfo.PostalCode = account.PostalCode;
                    utilityInfo.Country = account.Country;

                    utilityInfo.MainPhone = account.MainPhone;
                    utilityInfo.Fax = account.Fax;

                    utilityInfo.SalesTerritoryOwner = account.SalesTerritoryOwner;

                    // Do we want user IDs for these?
                    utilityInfo.AccountManager = account.AccountManager;
                    utilityInfo.RegionalManager = account.RegionalManager;

                    utilityInfo.Type = account.AccountType;
                    utilityInfo.SubType = account.AccountSubtype;
                    utilityInfo.Status = account.AccountStatus;

                    // Do we want contact ID?
                    utilityInfo.Contact = acctGroup
                        .Where(a => a.IsPrimaryContact())
                        .Select(a => new Contact()
                        {
                            Name = string.Format("{0} {1}", a.ContactFirstName, a.ContactLastName),
                            Title = a.ContactTitle,
                            WorkPhone = a.ContactWorkPhone
                        })
                        .FirstOrDefault();   //get first contact which is marked primary

                    //get list of secondary contacts
                    // Do we want contact ID?
                    utilityInfo.ActiveSecondaryContacts = acctGroup
                        .Where(a => !a.IsPrimaryContact() && a.IsContactActive())
                        .Select(a => new Contact()
                        {
                            Name = string.Format("{0} {1}", a.ContactFirstName, a.ContactLastName),
                            Title = a.ContactTitle,
                            WorkPhone = a.ContactWorkPhone
                        })
                        .ToList();

                    List<string> customerNumbers;
                    if (account.CustomerNumber != null && customerNumberLookup.TryGetValue(account.CustomerNumber, out customerNumbers))
                        utilityInfo.CustomerNumbers = customerNumbers;

                    // Do we need parent account ID?
                    // TODO: See above comment regarding temporary workaround for "distributor" site IDs.
                    if (string.IsNullOrEmpty(account.ParentAccountId))
                    {
                        utilityInfo.ParentCustomerNumber = account.CustomerNumber; // Distributor workaround
                    }
                    else
                    {
                        utilityInfo.ParentCustomerNumber = account.ParentCustomerNumber;
                    }

                    return utility;
                })
                .ToList();

            return utilityList;
        }

        /// <summary>
        /// Derives a Dictionary of Customer Numbers by ParentCustomerNumber from the set of accounts.
        /// </summary>
        public static Dictionary<string, List<string>> GetCustomerNumberDictionary(this IList<Account> accountList)
        {
            var dictionary = new Dictionary<string, List<string>>();

            // Group the accounts by ParentCustomerNumber
            var groupedAccounts = accountList
                .Where(a => !string.IsNullOrEmpty(a.ParentCustomerNumber) && !string.IsNullOrEmpty(a.CustomerNumber))
                .GroupBy(a => a.ParentCustomerNumber);

            foreach (var groupedAccount in groupedAccounts)
            {
                var customerNumbers = groupedAccount
                    .Select(c => c.CustomerNumber)
                    .Distinct()
                    .ToList();

                dictionary[groupedAccount.Key] = customerNumbers;
            }

            return dictionary;
        }
    }
}
