﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;

namespace Ntg.BpcCore.Entities.CustomerData
{
    /// <summary>
    /// Data mapping for running the query in GetQueryString() on SalesLogix database.
    /// </summary>
    public class Account
    {
        public string AccountId { get; set; }
        public string CustomerNumber { get; set; }
        public string AccountName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string MainPhone { get; set; }
        public string Fax { get; set; }
        public string SalesTerritoryOwner { get; set; }
        public string AccountManagerId { get; set; }
        public string AccountManager { get; set; }
        public string RegionalManagerId { get; set; }
        public string RegionalManager { get; set; }
        public string AccountType { get; set; }
        public string AccountSubtype { get; set; }
        public string AccountStatus { get; set; }

        public string ContactId { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactWorkPhone { get; set; }
        public string ContactTitle { get; set; }
        public string ContactStatus { get; set; }
        public string ContactIsPrimary { get; set; }

        public string ParentAccountId { get; set; }
        public string ParentCustomerNumber { get; set; }
        public string SiteId { get; set; }
        public string SystemId { get; set; }

        /// <summary>
        /// Check whether the ContactStatus entry is active
        /// </summary>
        /// <returns>true if active</returns>
        public bool IsContactActive()
        {
            return string.Equals(this.ContactStatus, "active", StringComparison.OrdinalIgnoreCase);
        }

        public bool IsPrimaryContact()
        {
            return string.Equals(this.ContactIsPrimary, "T", StringComparison.OrdinalIgnoreCase);
        }

        public static string GetQueryString()
        {
            return AccountsAndContactsQuery;
        }

        #region QUERY_STRING

        /// <summary>
        /// This is the query string for getting the relevant records from SalesLogix. The column names
        /// must match the Class Properties defined in this class so that Dapper mapper can map the correct 
        /// SQL results back to the corresponding class properties.
        /// </summary>
        private const string AccountsAndContactsQuery = @"
            SELECT 

            ACCOUNT.ACCOUNTID as AccountId,
            ACCOUNT.INTERNALACCOUNTNO as CustomerNumber, 
            ACCOUNT.ACCOUNT as AccountName,
            ADDRESS.ADDRESS1, 
            ADDRESS.ADDRESS2, 
            ADDRESS.ADDRESS3, 
            ADDRESS.CITY, 
            ADDRESS.STATE, 
            ADDRESS.POSTALCODE, 
            ADDRESS.COUNTRY, 
            ACCOUNT.MAINPHONE, 
            ACCOUNT.FAX, 
            ACCOUNT.SECCODEID as SalesTerritoryOwner,
            ACCOUNT.ACCOUNTMANAGERID as AccountManagerId, 
            USERINFO_ACCOUNTMANAGER.USERNAME as AccountManager,   -- Getting Account manager name from UserInfo table
            ACCOUNT.REGIONALMANAGERID as RegionalManagerId, 
            USERINFO_REGIONALMANAGER.USERNAME as RegionalManager,   -- Getting Regional manager name from UserInfo table
            Account.TYPE as AccountType, 
            ACCOUNT.SUBTYPE as AccountSubtype, 
            ACCOUNT.STATUS as AccountStatus,

            CONTACT.CONTACTID,
            CONTACT.FIRSTNAME as ContactFirstName, 
            CONTACT.LASTNAME as ContactLastName, 
            CONTACT.WORKPHONE as ContactWorkPhone, 
            CONTACT.TITLE as ContactTitle, 
            CONTACT.ISPRIMARY as ContactIsPrimary,
            CONTACT.STATUS as ContactStatus,

            ACCOUNT.PARENTID as ParentAccountId,
            ACCOUNT_PARENT.INTERNALACCOUNTNO as ParentCustomerNumber,

            ACCOUNT_ADDON.APPLICENSECITY as SiteId,		--Looks like the same as ZIPCODE, so should be OK
            ACCOUNT_ADDON.APPSYSTEMID as SystemId

            FROM sysdba.ACCOUNT as ACCOUNT
            LEFT JOIN sysdba.ACCOUNT_ADDON ON ACCOUNT.ACCOUNTID = ACCOUNT_ADDON.ACCOUNTID
            LEFT JOIN sysdba.ADDRESS ON ACCOUNT.ADDRESSID = ADDRESS.ADDRESSID
            LEFT JOIN sysdba.CONTACT ON ACCOUNT.ACCOUNTID = CONTACT.ACCOUNTID
            LEFT JOIN sysdba.USERINFO AS USERINFO_ACCOUNTMANAGER ON ACCOUNT.ACCOUNTMANAGERID = USERINFO_ACCOUNTMANAGER.USERID
            LEFT JOIN sysdba.USERINFO AS USERINFO_REGIONALMANAGER ON ACCOUNT.REGIONALMANAGERID = USERINFO_REGIONALMANAGER.USERID
            LEFT JOIN sysdba.ACCOUNT AS ACCOUNT_PARENT ON ACCOUNT.PARENTID = ACCOUNT_PARENT.ACCOUNTID

            ORDER BY ACCOUNT.ACCOUNTID
            ";

        #endregion
    }
}
