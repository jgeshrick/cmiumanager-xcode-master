﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Data;

namespace Ntg.BpcCore.Entities.LifecycleData
{
    /// <summary>
    /// Models a transaction on the QAD transfer queue.
    /// </summary>
    public class QadTransaction
    {
        /// <summary>
        /// The transaction type identifier.
        /// </summary>
        public string TransactionType { get; private set; }
        
        /// <summary>
        /// The created timestamp of the transaction.
        /// </summary>
        public DateTime? InDate { get; private set; }
        
        /// <summary>
        /// The transaction 'Key'.
        /// </summary>
        public string Key { get; private set; }
        
        /// <summary>
        /// The processed timestamp of the transaction.
        /// </summary>
        public DateTime? OutDate { get; set; }
        
        /// <summary>
        /// The transaction status indicator. "O"=Open; "P"=Pending; "E"=Error; "C"=Closed; "X"=Cancel.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// The transaction content.
        /// </summary>
        public string Content { get; private set; }
        
        /// <summary>
        /// The transaction source.
        /// </summary>
        public string Source { get; set; }
        
        /// <summary>
        /// An error message for a failed transaction.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Converts the supplied DataTable into a set of QadTransaction instances.
        /// </summary>
        public static IEnumerable<QadTransaction> FromDataTable(DataTable table)
        {
            foreach (DataRow row in table.Rows)
            {
                yield return GetQadTransaction(row);
            }
        }

        /// <summary>
        /// Converts the supplied DataRow into a new QadTransaction instance.
        /// </summary>
        private static QadTransaction GetQadTransaction(DataRow row)
        {
            var tran = new QadTransaction
            {
                TransactionType = row["Type"] as string,
                InDate = row["InDate"] == DBNull.Value ? (DateTime?) null : (DateTime) row["InDate"],
                Key = row["Key"] as string,
                OutDate = row["OutDate"] == DBNull.Value ? (DateTime?) null : (DateTime) row["OutDate"],
                Status = row["Status"] as string,
                Content = row["Content"] as string,
                Source = row["Src"] as string,
                Message = row["Msg"] as string
            };

            return tran;
        }
    }
}