﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Ntg.BpcCore.JsonModel.LifecycleHistory;

namespace Ntg.BpcCore.Entities.LifecycleData
{
    /// <summary>
    /// Provides helper methods related to MIU lifecycle state data.
    /// </summary>
    public static class LifecycleModelExtensions
    {
        /// <summary>
        /// Creates a new DataTable from the supplied QadTransactions for the purpose of status update.
        /// </summary>
        public static DataTable ToDataTableForStatusUpdate(this IEnumerable<QadTransaction> transactions)
        {
            var dt = new DataTable
            {
                Columns =
                {
                    new DataColumn("Type", typeof(string)),
                    new DataColumn("InDate", typeof(DateTime)),
                    new DataColumn("Key", typeof(string)),
                    new DataColumn("OutDate", typeof(DateTime)),
                    new DataColumn("Status", typeof(string)),
                    new DataColumn("Msg", typeof(string))
                }
            };

            foreach (var transaction in transactions)
            {
                dt.Rows.Add(transaction.TransactionType, transaction.InDate, transaction.Key,
                    transaction.OutDate, transaction.Status, transaction.Message);
            }

            return dt;
        }

        public static HistoricMiuLifecycleState GetHistoricMiuLifecycleState(this QadTransaction transaction, int referenceId, out string error)
        {
            string[] parameters = (transaction.Content ?? string.Empty)
                .Split('|')
                .Select(p => p.Trim())
                .ToArray();

            // Validation & parsing steps
            if (parameters.Length != 3)
            {
                error = string.Format("Expected 3 parameters in transaction content, but found {0} parameter(s).", parameters.Length);
                return null;
            }

            // MIU ID
            int miuId;
            if (!int.TryParse(parameters[0], out miuId))
            {
                error = string.Format("Invalid MIU ID: \"{0}\".", parameters[0]);
                return null;
            }

            // Lifecycle State
            string lifecycleState = parameters[1];
            if (string.IsNullOrWhiteSpace(lifecycleState))
            {
                error = "Lifecycle state is missing from transaction content";
                return null;
            }

            // Timestamp
            DateTime timestamp;
            // The timestamp is parsed with DateTimeStyles.AssumeLocal here and then re-specified as UTC
            // afterwards. With DateTimeStyles.AssumeUniversal, the time is parsed as UTC but it then 
            // generates the DateTime with DateTimeKind.Local in the local time zone.
            if (!DateTime.TryParse(parameters[2], CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out timestamp))
            {
                error = string.Format("The timestamp \"{0}\" could not be parsed.", parameters[2]);
                return null;
            }

            timestamp = DateTime.SpecifyKind(timestamp, DateTimeKind.Utc);

            var jsonModel = new HistoricMiuLifecycleState(miuId, lifecycleState, timestamp, referenceId);
            error = null;

            return jsonModel;
        }
    }
}