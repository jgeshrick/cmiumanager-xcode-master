﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Data;
using Ntg.BpcCore.JsonModel;
using Ntg.BpcCore.JsonModel.ConfigHistory;
using Ntg.BpcCore.JsonModel.LifecycleHistory;
using Ntg.BpcCore.JsonModel.OwnerHistory;

namespace Ntg.BpcCore.Entities.CostRevenueData
{
    /// <summary>
    /// Provides extension methods to the Cost and Revenue data models.
    /// </summary>
    public static class CostRevenueModelExtensions
    {
        /// <summary>
        /// Converts the supplied MIU details to a <see cref="DataTable"/> that can be submitted to a T-SQL stored procedure.
        /// </summary>
        /// <returns></returns>
        public static DataTable ToDataTable(this IEnumerable<MiuDetail> miuDetails)
        {
            var dt = new DataTable
            {
                Columns =
                {
                    new DataColumn("MiuId", typeof(int)),
                    new DataColumn("SiteId", typeof(int)),
                    new DataColumn("MiuType", typeof(string)),
                    new DataColumn("NetworkProvider", typeof(string)),
                    new DataColumn("Iccid", typeof(string)),
                    new DataColumn("Msisdn", typeof(string)),
                    new DataColumn("Eui", typeof(string)),
                }
            };

            foreach (var miu in miuDetails)
            {
                dt.Rows.Add(miu.MiuId, miu.SiteId, miu.MiuType, miu.NetworkProvider, miu.Iccid, miu.Msisdn, miu.Eui);
            }

            return dt;
        }

        /// <summary>
        /// Converts the supplied MIU configuration states to a <see cref="DataTable"/> that can be submitted to a T-SQL stored procedure.
        /// </summary>
        /// <returns></returns>
        public static DataTable ToDataTable(this IEnumerable<SequentialMiuConfig> configHistory)
        {
            var dt = new DataTable
            {
                Columns =
                {
                    new DataColumn("SequenceId", typeof(int)),
                    new DataColumn("MiuId", typeof(int)),
                    new DataColumn("ReceivedFromMiu", typeof(bool)),
                    new DataColumn("Timestamp", typeof(DateTime)),
                    new DataColumn("RecordIntervalMins", typeof(int)),
                    new DataColumn("ReportIntervalMins", typeof(int)),
                    new DataColumn("ModeChangeRaised", typeof(DateTime)),
                    new DataColumn("ConsideredForTransferQue", typeof(DateTime))
                }
            };

            foreach (var config in configHistory)
            {
                dt.Rows.Add(
                    config.SequenceId, config.MiuId, config.ReceivedFromMiu, config.Timestamp, 
                    config.RecordingIntervalMinutes, config.ReportingIntervalMinutes, 
                    DBNull.Value, DBNull.Value);
            }

            return dt;
        }

        public static DataTable ToDataTable(this IEnumerable<SequentialMiuOwner> miuOwnerHistory)
        {
            var dt = new DataTable
            {
                Columns =
                {
                    new DataColumn("SequenceId", typeof(int)),
                    new DataColumn("MiuId", typeof(int)),
                    new DataColumn("Timestamp", typeof(DateTime)),
                    new DataColumn("SiteId", typeof(int)),
                    new DataColumn("SiteIdChangeRaised", typeof(DateTime)),
                    new DataColumn("ConsideredForTransferQue", typeof(DateTime))
                }
            };

            foreach (var owner in miuOwnerHistory)
            {
                dt.Rows.Add(
                    owner.SequenceId, owner.MiuId, owner.Timestamp, owner.SiteId, DBNull.Value, DBNull.Value);
            }

            return dt;
        }

        /// <summary>
        /// Converts the supplied MIU lifecycle states to a <see cref="DataTable"/> that can be submitted to a T-SQL stored procedure.
        /// </summary>
        /// <returns></returns>
        public static DataTable ToDataTable(this IEnumerable<SequentialMiuLifecycleState> lifecycleStates)
        {
            var dt = new DataTable
            {
                Columns =
                {
                    new DataColumn("SequenceId", typeof(int)),
                    new DataColumn("MiuId", typeof(int)),
                    new DataColumn("Timestamp", typeof(DateTime)),
                    new DataColumn("LifecycleState", typeof(string))
                }
            };

            foreach (var state in lifecycleStates)
            {
                dt.Rows.Add(state.SequenceId, state.MiuId, state.Timestamp, state.State);
            }

            return dt;
        }

        /// <summary>
        /// Converts the supplied MIU configuration history entries to a <see cref="DataTable"/> that can be submitted to a T-SQL stored procedure.
        /// </summary>
        /// <returns></returns>
        public static DataTable ToDataTable(this IEnumerable<MiuConfigHistory> miuConfigHistory)
        {
            var dt = new DataTable
            {
                Columns =
                {
                    new DataColumn("SequenceId", typeof(int)),
                    new DataColumn("MiuId", typeof(int)),
                    new DataColumn("ReceivedFromMiu", typeof(bool)),
                    new DataColumn("Timestamp", typeof(DateTime)),
                    new DataColumn("RecordIntervalMins", typeof(int)),
                    new DataColumn("ReportIntervalMins", typeof(int)),
                    new DataColumn("ModeChangeRaised", typeof(DateTime)),
                    new DataColumn("ConsideredForTransferQue", typeof(DateTime))
                }
            };

            foreach (var config in miuConfigHistory)
            {
                dt.Rows.Add(
                    config.SequenceId, config.MiuId, config.ReceivedFromMiu, config.Timestamp, 
                    config.RecordingIntervalMinutes, config.ReportingIntervalMinutes, config.ModeChangeRaised, 
                    config.ConsideredForTransferQueue);
            }

            return dt;
        }

        /// <summary>
        /// Converts the supplied MIU owner history entries to a <see cref="DataTable"/> that can be submitted to a T-SQL stored procedure.
        /// </summary>
        /// <returns></returns>
        public static DataTable ToDataTable(this IEnumerable<MiuOwnerHistory> miuOwnerHistory)
        {
            var dt = new DataTable
            {
                Columns =
                {
                    new DataColumn("SequenceId", typeof(int)),
                    new DataColumn("MiuId", typeof(int)),
                    new DataColumn("Timestamp", typeof(DateTime)),
                    new DataColumn("SiteId", typeof(int)),
                    new DataColumn("SiteIdChangeRaised", typeof(DateTime)),
                    new DataColumn("ConsideredForTransferQue", typeof(DateTime))
                }
            };

            foreach (var miuOwner in miuOwnerHistory)
            {
                dt.Rows.Add(
                    miuOwner.SequenceId, miuOwner.MiuId, miuOwner.Timestamp, miuOwner.SiteId,
                    miuOwner.SiteIdChangeRaised, miuOwner.ConsideredForTransferQueue);
            }

            return dt;
        }

        /// <summary>
        /// Converts the supplied data record to an instance of <see cref="MiuConfigHistory"/>.
        /// </summary>
        /// <param name="rs"></param>
        /// <returns></returns>
        public static MiuConfigHistory ToMiuConfigHistory(this IDataRecord rs)
        {
            var config = new MiuConfigHistory
            {
                SequenceId = (int)rs["SequenceId"],
                MiuId = (int)rs["MiuId"],
                ReceivedFromMiu = (bool)rs["ReceivedFromMiu"],
                Timestamp = (DateTime)rs["Timestamp"],
                RecordingIntervalMinutes = (int)rs["RecordIntervalMins"],
                ReportingIntervalMinutes = (int)rs["ReportIntervalMins"],
                ModeChangeRaised = rs.GetDateTime("ModeChangeRaised"),
                ConsideredForTransferQueue = rs.GetDateTime("ConsideredForTransferQue")
            };

            return config;
        }

        /// <summary>
        /// Converts the supplied data record to an instance of <see cref="MiuOwnerHistory"/>.
        /// </summary>
        /// <param name="rs"></param>
        /// <returns></returns>
        public static MiuOwnerHistory ToMiuOwnerHistory(this IDataRecord rs)
        {
            var config = new MiuOwnerHistory
            {
                SequenceId = (int)rs["SequenceId"],
                MiuId = (int)rs["MiuId"],
                Timestamp = (DateTime)rs["Timestamp"],
                SiteId = (int)rs["SiteId"],
                SiteIdChangeRaised = rs.GetDateTime("SiteIdChangeRaised"),
                ConsideredForTransferQueue = rs.GetDateTime("ConsideredForTransferQue")
            };

            return config;
        }

        /// <summary>
        /// Gets a nullable DateTime from the data record.
        /// </summary>
        /// <param name="rs">The data record.</param>
        /// <param name="columnName">The column name to read.</param>
        /// <returns>The DateTime value, or null if the <see cref="IDataRecord"/> column contained DBNull.</returns>
        public static DateTime? GetDateTime(this IDataRecord rs, string columnName)
        {
            return rs[columnName] == DBNull.Value ? (DateTime?)null : (DateTime)rs[columnName];
        }
    }
}