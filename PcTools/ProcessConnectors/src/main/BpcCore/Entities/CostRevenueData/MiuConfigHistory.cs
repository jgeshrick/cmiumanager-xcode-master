﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;

namespace Ntg.BpcCore.Entities.CostRevenueData
{
    /// <summary>
    /// Data model for an entry in the MIU configuration history.
    /// </summary>
    public class MiuConfigHistory
    {
        /// <summary>
        /// The unique sequence ID of the configuration state.
        /// </summary>
        public int SequenceId { get; set; }

        /// <summary>
        /// The MIU ID.
        /// </summary>
        public int MiuId { get; set; }

        /// <summary>
        /// Indicates whether the configuration state was reported by the MIU.
        /// </summary>
        public bool ReceivedFromMiu { get; set; }

        /// <summary>
        /// The timestamp of the configuration state.
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// The recording interval.
        /// </summary>
        public int RecordingIntervalMinutes { get; set; }

        /// <summary>
        /// The reporting interval.
        /// </summary>
        public int ReportingIntervalMinutes { get; set; }

        /// <summary>
        /// When non-null, the timestamp at which a QAD transfer queue "Mode Change" event was created for this configuration state.
        /// </summary>
        public DateTime? ModeChangeRaised { get; set; }

        /// <summary>
        /// When non-null, the timestamp at which this configuration state was processed for possible QAD transfer queue events.
        /// </summary>
        public DateTime? ConsideredForTransferQueue { get; set; }
    }
}