﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;

namespace Ntg.BpcCore.Entities.CostRevenueData
{
    /// <summary>
    /// Data model for an entry in the MIU owner history.
    /// </summary>
    public class MiuOwnerHistory
    {
        /// <summary>
        /// The unique sequence ID of the configuration state.
        /// </summary>
        public int SequenceId { get; set; }

        /// <summary>
        /// The MIU ID.
        /// </summary>
        public int MiuId { get; set; }

        /// <summary>
        /// The timestamp of the configuration state.
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// The owner site ID.
        /// </summary>
        public int SiteId { get; set; }

        /// <summary>
        /// When non-null, the timestamp at which a QAD transfer queue "ISB Move EUto New Customer" event was created for this configuration state.
        /// </summary>
        public DateTime? SiteIdChangeRaised { get; set; }

        /// <summary>
        /// When non-null, the timestamp at which this configuration state was processed for possible QAD transfer queue events.
        /// </summary>
        public DateTime? ConsideredForTransferQueue { get; set; }
    }
}