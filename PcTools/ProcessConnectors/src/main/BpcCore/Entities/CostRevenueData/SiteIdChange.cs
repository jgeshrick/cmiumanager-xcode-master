﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;

namespace Ntg.BpcCore.Entities.CostRevenueData
{
    /// <summary>
    /// Represents a transition to a new customer Site ID.
    /// </summary>
    public class SiteIdChange
    {
        public int MiuId { get; set; }

        public int SiteId { get; set; }

        public DateTime Timestamp { get; set; }

        public static SiteIdChange From(MiuOwnerHistory miuOwner)
        {
            var siteIdChange = new SiteIdChange
            {
                MiuId = miuOwner.MiuId,
                SiteId = miuOwner.SiteId,
                Timestamp = miuOwner.Timestamp
            };

            return siteIdChange;
        }
    }
}