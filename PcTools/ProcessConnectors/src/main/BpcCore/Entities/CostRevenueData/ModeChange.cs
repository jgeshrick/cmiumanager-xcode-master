﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;

namespace Ntg.BpcCore.Entities.CostRevenueData
{
    /// <summary>
    /// Data model for a transition to a new CMIU mode.
    /// </summary>
    public class ModeChange
    {
        private const int DefaultReadingIntervalMins = 15;

        /// <summary>
        /// The MIU ID.
        /// </summary>
        public int MiuId { get; set; }

        /// <summary>
        /// The reading interval.
        /// </summary>
        public int ReadingIntervalMins { get; set; }

        /// <summary>
        /// The recording interval.
        /// </summary>
        public int RecordingIntervalMins { get; set; }

        /// <summary>
        /// The reporting interval.
        /// </summary>
        public int ReportingIntervalMins { get; set; }

        /// <summary>
        /// The mode name.
        /// </summary>
        public string ModeName { get; set; }

        /// <summary>
        /// The timestamp of the CMIU mode.
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Creates a new <see cref="ModeChange"/> using the values in the supplied <see cref="MiuConfigHistory"/> instance.
        /// </summary>
        /// <param name="config">The data source for the new object.</param>
        /// <returns>A new <see cref="ModeChange"/> initialized from <paramref name="config"/>.</returns>
        public static ModeChange From(MiuConfigHistory config)
        {
            var modeChange = new ModeChange
            {
                MiuId = config.MiuId,
                Timestamp = config.Timestamp,
                ReadingIntervalMins = DefaultReadingIntervalMins,
                RecordingIntervalMins = config.RecordingIntervalMinutes,
                ReportingIntervalMins = config.ReportingIntervalMinutes,
                ModeName = GetCmiuModeName(config.RecordingIntervalMinutes, config.ReportingIntervalMinutes)
            };

            return modeChange;
        }

        /// <summary>
        /// Gets the CMIU mode name based on the supplied recording and reporting intervals.
        /// </summary>
        /// <param name="recordingIntervalMins">The recording interval in minutes.</param>
        /// <param name="reportingIntevalMins">The reporting interval in minutes.</param>
        /// <returns>The CMIU mode name, or "Unknown" if the supplied values do not match a known mode.</returns>
        private static string GetCmiuModeName(int recordingIntervalMins, int reportingIntevalMins)
        {
            if (recordingIntervalMins == 60 && reportingIntevalMins == 1440)
            {
                return "Basic";
            }

            if (recordingIntervalMins == 60 && reportingIntevalMins == 240)
            {
                return "Advanced";
            }

            if (recordingIntervalMins == 15 && reportingIntevalMins == 60)
            {
                return "Pro";
            }

            return "Unknown";
        }
    }
}