﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using System.Threading.Tasks;
using Ntg.BpcCore.Entities.LifecycleData;

namespace Ntg.BpcCore.Domain
{
    public interface ITransferQueueRepository
    {
        Task<List<QadTransaction>> GetOpenTransactionsAsync(string transactionType, int limit);

        Task UpdateTransactionStatusAsync(IEnumerable<QadTransaction> transactions);
    }
}