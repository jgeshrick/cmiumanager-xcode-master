//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Ntg.BpcCore.Entities.LifecycleData;

namespace Ntg.BpcCore.Domain
{
    public class TransferQueueRepository : ITransferQueueRepository
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private readonly string connectionString;

        /// <summary>
        /// Creates a new repository instance.
        /// </summary>
        /// <param name="connectionString">The SQL Server connection string.</param>
        public TransferQueueRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public async Task<List<QadTransaction>> GetOpenTransactionsAsync(string transactionType, int limit)
        {
            Log.Debug(10080, string.Format("TransferQueueRepository.GetOpenTransactions: Txn type: {0}; Limit: {1}.", transactionType, limit));
            var results = CreateQadTransactionDataTable();
            
            using (var conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();

                var command = new SqlCommand("bpc.prGetOpenTransactions", conn) { CommandType = CommandType.StoredProcedure };
                command.Parameters.AddWithValue("@TransactionType", transactionType);
                command.Parameters.AddWithValue("@MaxRows", limit);

                using (var da = new SqlDataAdapter(command))
                {
                    da.Fill(results);
                }
            }

            return QadTransaction.FromDataTable(results).ToList();
        }

        public async Task UpdateTransactionStatusAsync(IEnumerable<QadTransaction> transactions)
        {
            Log.Debug(10081, "TransferQueueRepository.UpdateTransactionStatus was called.");
            
            using (var conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();

                var command = new SqlCommand("bpc.prUpdateTransactionStatus", conn) { CommandType = CommandType.StoredProcedure };
                command.Parameters.AddWithValue("@TransactionStatus", transactions.ToDataTableForStatusUpdate());

                await command.ExecuteNonQueryAsync();
            }
        }

        private static DataTable CreateQadTransactionDataTable()
        {
            return new DataTable
            {
                Columns =
                {
                    new DataColumn("Type", typeof(string)),
                    new DataColumn("InDate", typeof(DateTime)),
                    new DataColumn("Key", typeof(string)),
                    new DataColumn("OutDate", typeof(DateTime)),
                    new DataColumn("Status", typeof(string)),
                    new DataColumn("Content", typeof(string)),
                    new DataColumn("Src", typeof(string)),
                    new DataColumn("Msg", typeof(string))
                }
            };
        }
    }
}