﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using log4net;
using Ntg.BpcCore.Entities.CustomerData;

namespace Ntg.BpcCore.Domain
{
    /// <summary>
    /// Concrete implementation of customer data service for accessing SLX customer data.
    /// Dapper.NET is an Open-Source, lightweight object-relational-mapper (ORM) that is simple to use,
    /// and is compatible with any database which implements a provider for .NET
    /// (i.e.: provides an implementation of the IDbConnection interface).
    /// Dapper is implemented as a series of Extension Methods which can be called on any object which implements the IDbConnection interface.
    /// </summary>
    public class DapperCustomerDataRepository: ICustomerDataRepository
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Database connection string. If using windows authentication, use "Data Source=UKHARPORT110;Initial Catalog=SalesLogix;Integrated Security=True".
        /// For SQL authentication, use: Server=sandy;Database=SalesLogix;User Id={user};Password={password}
        /// </summary>
        private readonly string connectionString;

        public DapperCustomerDataRepository(string databaseConnectionString)
        {
            connectionString = databaseConnectionString;
        }

        /// <summary>
        /// Query the SalesLogix database to retrieve a records of Account and map to POCO
        /// </summary>
        /// <returns></returns>
        public IList<Account> GetAccountList()
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var accountList = (List<Account>)conn.Query<Account>(Account.GetQueryString());

                return accountList;
            }
        }
    }
}
