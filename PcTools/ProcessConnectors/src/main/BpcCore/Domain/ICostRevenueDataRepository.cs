﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using System.Threading.Tasks;
using Ntg.BpcCore.Entities.CostRevenueData;
using Ntg.BpcCore.JsonModel;
using Ntg.BpcCore.JsonModel.ConfigHistory;
using Ntg.BpcCore.JsonModel.LifecycleHistory;
using Ntg.BpcCore.JsonModel.OwnerHistory;
using MiuConfigHistory = Ntg.BpcCore.Entities.CostRevenueData.MiuConfigHistory;

namespace Ntg.BpcCore.Domain
{
    /// <summary>
    /// Provides access to the Cost & Revenue database.
    /// </summary>
    public interface ICostRevenueDataRepository
    {
        /// <summary>
        /// Commits the supplied MIU details to the data store.
        /// </summary>
        /// <param name="miuDetails">The MIU details to commit.</param>
        Task SetMiuDetailsAsync(IEnumerable<MiuDetail> miuDetails);

        /// <summary>
        /// Sets a key-value pair in the ETL status store.
        /// </summary>
        /// <param name="key">The key of the key-value pair.</param>
        /// <param name="value">The ETL status.</param>
        Task SetEtlStatusAsync(string key, string value);

        /// <summary>
        /// Gets the value for the provided key from the ETL status store.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The string value associated with the supplied key, or null if the key is not present.</returns>
        Task<string> GetEtlStatusAsync(string key);

        /// <summary>
        /// Gets the last MiuConfigHistory SequenceId that was inserted.
        /// </summary>
        /// <returns></returns>
        Task<int?> GetMaxMiuConfigHistorySequenceIdAsync();

        /// <summary>
        /// Gets the last MiuOwnerHistory SequenceId that was inserted.
        /// </summary>
        /// <returns></returns>
        Task<int?> GetMaxMiuOwnerHistorySequenceIdAsync();

        /// <summary>
        /// Gets the last MiuLifecycleHistory SequenceId that was inserted.
        /// </summary>
        /// <returns></returns>
        Task<int?> GetMaxMiuLifecycleHistorySequenceIdAsync();

        /// <summary>
        /// Appends the supplied MIU configuration history entries to the database.
        /// </summary>
        /// <param name="configHistory">The MIU configuration history to append.</param>
        Task AppendMiuConfigHistoryAsync(IEnumerable<SequentialMiuConfig> configHistory);

        /// <summary>
        /// Appends the supplied MIU owner history entries to the database.
        /// </summary>
        /// <param name="miuOwnerHistory">The MIU owner history to append.</param>
        Task AppendMiuOwnerHistoryAsync(List<SequentialMiuOwner> miuOwnerHistory);

        /// <summary>
        /// Appends the supplied MIU lifecycle history entries to the database.
        /// </summary>
        /// <param name="lifecycleStates"></param>
        Task AppendMiuLifecycleHistoryAsync(IEnumerable<SequentialMiuLifecycleState> lifecycleStates);

        /// <summary>
        /// Gets all MIU IDs for which there are configuration history entries that have not yet been considered for 
        /// entry into the QAD transfer queue.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<int>> GetMiuIdsWithPendingConfigurationHistoryAsync();

        /// <summary>
        /// Gets all MIU IDs for which there are owner history entries that have not yet been considered for 
        /// entry into the QAD transfer queue.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<int>> GetMiuIdsWithPendingOwnerHistoryAsync();

        /// <summary>
        /// Gets all MIU configuration history entries by MIU ID.
        /// </summary>
        /// <param name="miuId"></param>
        /// <returns></returns>
        Task<IEnumerable<MiuConfigHistory>> GetMiuConfigHistoryByMiuIdAsync(int miuId);

        /// <summary>
        /// Gets all MIU owner history entries by MIU ID.
        /// </summary>
        /// <param name="miuId"></param>
        /// <returns></returns>
        Task<IEnumerable<MiuOwnerHistory>> GetMiuOwnerHistoryByMiuIdAsync(int miuId);

        /// <summary>
        /// Writes the supplied events to the Transfer Queue and updates timestamps on the supplied configuration history records.
        /// </summary>
        /// <param name="modeChanges">The CMIU mode changes to be written to the transfer queue.</param>
        /// <param name="updatedConfigHistory">The affected configuration history entries with updated timestamps.</param>
        Task WriteTransferQueueEventsAsync(IEnumerable<ModeChange> modeChanges, IEnumerable<MiuConfigHistory> updatedConfigHistory);

        /// <summary>
        /// Writes the supplied events to the Transfer Queue and updates timestamps on the supplied configuration history records.
        /// </summary>
        /// <param name="siteIdChanges">The site ID changes to be written to the transfer queue.</param>
        /// <param name="updatedOwnerHistory">The affected configuration history entries with updated timestamps.</param>
        Task WriteTransferQueueEventsAsync(IEnumerable<SiteIdChange> siteIdChanges, IEnumerable<MiuOwnerHistory> updatedOwnerHistory);
    }
}