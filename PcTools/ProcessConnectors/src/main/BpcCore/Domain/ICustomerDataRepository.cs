﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using Ntg.BpcCore.Entities.CustomerData;

namespace Ntg.BpcCore.Domain
{
    /// <summary>
    /// Provides customer account data from a CRM data source.
    /// </summary>
    public interface ICustomerDataRepository
    {
        /// <summary>
        /// Gets a list of all customer accounts from the CRM data source.
        /// </summary>
        /// <returns></returns>
        IList<Account> GetAccountList();

    }
}
