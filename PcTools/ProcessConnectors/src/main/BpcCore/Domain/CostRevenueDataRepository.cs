﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using log4net;
using Ntg.BpcCore.Entities.CostRevenueData;
using Ntg.BpcCore.JsonModel;
using Ntg.BpcCore.JsonModel.ConfigHistory;
using Ntg.BpcCore.JsonModel.LifecycleHistory;
using Ntg.BpcCore.JsonModel.OwnerHistory;
using MiuConfigHistory = Ntg.BpcCore.Entities.CostRevenueData.MiuConfigHistory;

namespace Ntg.BpcCore.Domain
{
    /// <summary>
    /// Provides access to the Cost & Revenue database.
    /// </summary>
    public class CostRevenueDataRepository : ICostRevenueDataRepository
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private readonly string connectionString;

        /// <summary>
        /// Creates a new repository instance.
        /// </summary>
        /// <param name="connectionString">The SQL Server connection string.</param>
        public CostRevenueDataRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Commits the supplied MIU details to the database.
        /// </summary>
        /// <param name="miuDetails">The MIU history to commit.</param>
        public async Task SetMiuDetailsAsync(IEnumerable<MiuDetail> miuDetails)
        {
            Log.Debug(10050, "CostRevenueDataRepository.SetMiuDetailsAsync was called.");
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var command = new SqlCommand("bpc.prSetMiuDetails", conn) {CommandType = CommandType.StoredProcedure};
                command.Parameters.AddWithValue("@MiuDetails", miuDetails.ToDataTable());

                await command.ExecuteNonQueryAsync();
            }
        }

        /// <summary>
        /// Gets the last MiuConfigHistory SequenceId that was inserted.
        /// </summary>
        /// <returns></returns>
        public async Task<int?> GetMaxMiuConfigHistorySequenceIdAsync()
        {
            Log.Debug(10053, "CostRevenueDataRepository.GetMaxMiuConfigHistorySequenceId was called.");
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var command = new SqlCommand("bpc.prGetMaxMiuConfigHistorySequenceId", conn) { CommandType = CommandType.StoredProcedure };

                object result = await command.ExecuteScalarAsync();
                if (DBNull.Value.Equals(result))
                {
                    return null;
                }

                return (int?)result;
            }
        }

        public async Task<int?> GetMaxMiuOwnerHistorySequenceIdAsync()
        {
            Log.Debug(10076, "CostRevenueDataRepository.GetMaxMiuOwnerHistorySequenceId was called.");
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var command = new SqlCommand("bpc.prGetMaxMiuOwnerHistorySequenceId", conn) { CommandType = CommandType.StoredProcedure };

                object result = await command.ExecuteScalarAsync();
                if (DBNull.Value.Equals(result))
                {
                    return null;
                }

                return (int?)result;
            }
        }

        /// <summary>
        /// Gets the last MiuLifecycleHistory SequenceId that was inserted.
        /// </summary>
        /// <returns></returns>
        public async Task<int?> GetMaxMiuLifecycleHistorySequenceIdAsync()
        {
            Log.Debug(10054, "CostRevenueDataRepository.GetMaxMiuLifecycleHistorySequenceId was called.");
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var command = new SqlCommand("bpc.prGetMaxMiuLifecycleHistorySequenceId", conn) { CommandType = CommandType.StoredProcedure };

                object result = await command.ExecuteScalarAsync();
                if (DBNull.Value.Equals(result))
                {
                    return null;
                }

                return (int?)result;
            }
        }

        /// <summary>
        /// Appends the supplied MIU configuration history entries to the database.
        /// </summary>
        /// <param name="configHistory">The MIU configuration history to append.</param>
        public async Task AppendMiuConfigHistoryAsync(IEnumerable<SequentialMiuConfig> configHistory)
        {
            Log.Debug(10055, "CostRevenueDataRepository.AppendMiuConfigHistoryAsync was called.");
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var command = new SqlCommand("bpc.prAppendMiuConfigHistory", conn) { CommandType = CommandType.StoredProcedure };
                command.Parameters.AddWithValue("@MiuConfigHistory", configHistory.ToDataTable());

                await command.ExecuteNonQueryAsync();
            }
        }

        /// <summary>
        /// Appends the supplied MIU owner history entries to the database.
        /// </summary>
        /// <param name="miuOwnerHistory">The MIU owner history to append.</param>
        public async Task AppendMiuOwnerHistoryAsync(List<SequentialMiuOwner> miuOwnerHistory)
        {
            Log.Debug(10075, "CostRevenueDataRepository.AppendMiuOwnerHistoryAsync was called.");
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var command = new SqlCommand("bpc.prAppendMiuOwnerHistory", conn) { CommandType = CommandType.StoredProcedure };
                command.Parameters.AddWithValue("@MiuOwnerHistory", miuOwnerHistory.ToDataTable());

                await command.ExecuteNonQueryAsync();
            }
        }

        /// <summary>
        /// Appends the supplied MIU lifecycle state events to the database.
        /// </summary>
        /// <param name="lifecycleStates">The MIU lifecycle state events to add.</param>
        /// <returns></returns>
        public async Task AppendMiuLifecycleHistoryAsync(IEnumerable<SequentialMiuLifecycleState> lifecycleStates)
        {
            Log.Debug(10056, "CostRevenueDataRepository.AppendMiuLifecycleHistoryAsync was called.");
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var command = new SqlCommand("bpc.prAppendMiuLifecycleHistory", conn) { CommandType = CommandType.StoredProcedure };
                command.Parameters.AddWithValue("@MiuLifecycleHistory", lifecycleStates.ToDataTable());

                await command.ExecuteNonQueryAsync();
            }
        }

        /// <summary>
        /// Sets a key-value pair in the EtlStatus table.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public async Task SetEtlStatusAsync(string key, string value)
        {
            Log.Debug(10051, "CostRevenueDataRepository.SetEtlStatusAsync was called.");
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var command = new SqlCommand("bpc.prSetEtlStatus", conn) {CommandType = CommandType.StoredProcedure};
                command.Parameters.AddWithValue("@Key", key);
                command.Parameters.AddWithValue("@Value", (object)value ?? DBNull.Value);

                await command.ExecuteNonQueryAsync();
            }
        }

        /// <summary>
        /// Gets the EtlStatus value for the provided key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The string value associated with the supplied key, or null if the key is not present.</returns>
        public async Task<string> GetEtlStatusAsync(string key)
        {
            Log.Debug(10052, "CostRevenueDataRepository.GetEtlStatusAsync was called.");
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var command = new SqlCommand("bpc.prGetEtlStatus", conn) { CommandType = CommandType.StoredProcedure };
                command.Parameters.AddWithValue("@Key", key);

                return await command.ExecuteScalarAsync() as string;
            }
        }

        /// <summary>
        /// Gets all MIU IDs for which there are configuration history entries that have not yet been considered for 
        /// entry into the QAD transfer queue.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<int>> GetMiuIdsWithPendingConfigurationHistoryAsync()
        {
            Log.Debug(10057, "CostRevenueDataRepository.GetMiuIdsWithPendingConfigurationHistory was called.");
            var miuIdList = new List<int>();

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var command = new SqlCommand("bpc.prGetPendingMiusForTransferQueue", conn) { CommandType = CommandType.StoredProcedure };
                command.Parameters.AddWithValue("@TableName", "MiuConfigHistory");

                using (var rs = await command.ExecuteReaderAsync())
                {
                    while (rs.Read())
                    {
                        miuIdList.Add(rs.GetInt32(0));
                    }
                }
            }

            return miuIdList;
        }

        /// <summary>
        /// Gets all MIU IDs for which there are owner history entries that have not yet been considered for 
        /// entry into the QAD transfer queue.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<int>> GetMiuIdsWithPendingOwnerHistoryAsync()
        {
            Log.Debug(10057, "CostRevenueDataRepository.GetMiuIdsWithPendingOwnerHistory was called.");
            var miuIdList = new List<int>();

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var command = new SqlCommand("bpc.prGetPendingMiusForTransferQueue", conn) { CommandType = CommandType.StoredProcedure };
                command.Parameters.AddWithValue("@TableName", "MiuOwnerHistory");

                using (var rs = await command.ExecuteReaderAsync())
                {
                    while (rs.Read())
                    {
                        miuIdList.Add(rs.GetInt32(0));
                    }
                }
            }

            return miuIdList;
        }

        /// <summary>
        /// Gets all MIU configuration history entries by MIU ID.
        /// </summary>
        /// <param name="miuId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<MiuConfigHistory>> GetMiuConfigHistoryByMiuIdAsync(int miuId)
        {
            Log.Debug(10058, string.Format("CostRevenueDataRepository.GetMiuConfigHistoryByMiuId was called. miuId={0}.", miuId));
            var miuIdList = new List<MiuConfigHistory>();

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var command = new SqlCommand("bpc.prGetMiuConfigHistoryByMiuId", conn) { CommandType = CommandType.StoredProcedure };
                command.Parameters.AddWithValue("@MiuId", miuId);

                using (var rs = await command.ExecuteReaderAsync())
                {
                    while (rs.Read())
                    {
                        miuIdList.Add(rs.ToMiuConfigHistory());
                    }
                }
            }

            Log.Debug(10059, string.Format("CostRevenueDataRepository.GetMiuConfigHistoryByMiuId: found {0} result(s) for miuId {1}.", miuIdList.Count, miuId));

            return miuIdList;
        }

        /// <summary>
        /// Gets all MIU owner history entries by MIU ID.
        /// </summary>
        /// <param name="miuId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<MiuOwnerHistory>> GetMiuOwnerHistoryByMiuIdAsync(int miuId)
        {
            Log.Debug(10078, string.Format("CostRevenueDataRepository.GetMiuOwnerHistoryByMiuId was called. miuId={0}.", miuId));
            var ownerList = new List<MiuOwnerHistory>();

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var command = new SqlCommand("bpc.prGetMiuOwnerHistoryByMiuId", conn) { CommandType = CommandType.StoredProcedure };
                command.Parameters.AddWithValue("@MiuId", miuId);

                using (var rs = await command.ExecuteReaderAsync())
                {
                    while (rs.Read())
                    {
                        ownerList.Add(rs.ToMiuOwnerHistory());
                    }
                }
            }

            Log.Debug(10079, string.Format("CostRevenueDataRepository.GetMiuOwnerHistoryByMiuId: found {0} result(s) for miuId {1}.", ownerList.Count, miuId));

            return ownerList;
        }

        /// <summary>
        /// Writes the supplied events to the Transfer Queue and updates timestamps on the supplied configuration history records.
        /// </summary>
        /// <param name="modeChanges">The CMIU mode changes to be written to the transfer queue.</param>
        /// <param name="updatedConfigHistory">The affected configuration history entries with updated timestamps.</param>
        public async Task WriteTransferQueueEventsAsync(IEnumerable<ModeChange> modeChanges, IEnumerable<MiuConfigHistory> updatedConfigHistory)
        {
            var modeChangeCount = 0;

            Log.Debug(10070, "CostRevenueDataRepository.WriteTransferQueueEvents was called.");

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        // Add mode changes
                        foreach (var modeChange in modeChanges)
                        {
                            modeChangeCount++;
                            var command = new SqlCommand("dbo.prTransactionQueChangeCmiuMode", conn, transaction);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@MiuId", modeChange.MiuId);
                            command.Parameters.AddWithValue("@CmiuMode", modeChange.ModeName);
                            command.Parameters.AddWithValue("@ReadingIntervalMins", modeChange.ReadingIntervalMins);
                            command.Parameters.AddWithValue("@RecordingIntervalMins", modeChange.RecordingIntervalMins);
                            command.Parameters.AddWithValue("@ReportingIntervalMins", modeChange.ReportingIntervalMins);
                            command.Parameters.AddWithValue("@UtcTimestamp", modeChange.Timestamp);
                            await command.ExecuteNonQueryAsync();
                        }

                        Log.Debug(10072, string.Format("CostRevenueDataRepository.WriteTransferQueueEvents: Adding {0} mode changes.", modeChangeCount));

                        // Update timestamps on affected config history records.
                        var configHistoryDataTable = updatedConfigHistory.ToDataTable();
                        var chCommand = new SqlCommand("bpc.prUpdateMiuConfigHistoryTimestamps", conn, transaction);
                        chCommand.CommandType = CommandType.StoredProcedure;
                        chCommand.Parameters.AddWithValue("@MiuConfigHistory", configHistoryDataTable);
                        await chCommand.ExecuteNonQueryAsync();

                        Log.Debug(10074, string.Format(
                            "CostRevenueDataRepository.WriteTransferQueueEvents: Updating timestamps on {0} configuration history records.", configHistoryDataTable.Rows.Count));

                        transaction.Commit();
                    }
                    catch (System.Exception)
                    {
                        Log.Error(10071, "CostRevenueDataRepository.WriteTransferQueueEvents: Exception occurred; rolling back transaction.");
                        transaction.Rollback();

                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Writes the supplied events to the Transfer Queue and updates timestamps on the supplied configuration history records.
        /// </summary>
        /// <param name="siteIdChanges">The site ID changes to be written to the transfer queue.</param>
        /// <param name="updatedOwnerHistory">The affected configuration history entries with updated timestamps.</param>
        public async Task WriteTransferQueueEventsAsync(IEnumerable<SiteIdChange> siteIdChanges, IEnumerable<MiuOwnerHistory> updatedOwnerHistory)
        {
            var siteIdChangeCount = 0;

            Log.Debug(10110, "CostRevenueDataRepository.WriteTransferQueueEvents was called.");

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        // Add site ID changes
                        foreach (var siteIdChange in siteIdChanges)
                        {
                            siteIdChangeCount++;
                            var command = new SqlCommand("dbo.prTransactionQueChangeMiuOwner", conn, transaction);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@MiuId", siteIdChange.MiuId);
                            command.Parameters.AddWithValue("@NewSiteId", siteIdChange.SiteId);
                            command.Parameters.AddWithValue("@UtcTimestamp", siteIdChange.Timestamp);
                            await command.ExecuteNonQueryAsync();
                        }

                        Log.Debug(10111, string.Format("CostRevenueDataRepository.WriteTransferQueueEvents: Adding {0} site ID changes.", siteIdChangeCount));

                        // Update timestamps on affected owner history records.
                        var miuOwnerHistoryDataTable = updatedOwnerHistory.ToDataTable();
                        var chCommand = new SqlCommand("bpc.prUpdateMiuOwnerHistoryTimestamps", conn, transaction);
                        chCommand.CommandType = CommandType.StoredProcedure;
                        chCommand.Parameters.AddWithValue("@MiuOwnerHistory", miuOwnerHistoryDataTable);
                        await chCommand.ExecuteNonQueryAsync();

                        Log.Debug(10112, string.Format(
                            "CostRevenueDataRepository.WriteTransferQueueEvents: Updating timestamps on {0} MIU owner history records.", miuOwnerHistoryDataTable.Rows.Count));

                        transaction.Commit();
                    }
                    catch (System.Exception)
                    {
                        Log.Error(10113, "CostRevenueDataRepository.WriteTransferQueueEvents: Exception occurred; rolling back transaction.");
                        transaction.Rollback();

                        throw;
                    }
                }
            }
        }
    }
}