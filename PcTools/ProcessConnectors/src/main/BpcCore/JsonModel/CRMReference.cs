﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;

namespace Ntg.BpcCore.JsonModel.CrmReference
{
    public class CrmReference
    {
        private List<Distributer> distributers;
        private List<Utility> utilities;

        [DeserializeAs(Name = "distributers")]
        [JsonProperty("distributers")]
        public List<Distributer> Distributers
        {
            get { return distributers ?? (distributers = new List<Distributer>()); }
            set { distributers = value; }
        }

        [DeserializeAs(Name = "utilities")]
        [JsonProperty("utilities")]
        public List<Utility> Utilities
        {
            get { return utilities ?? (utilities = new List<Utility>()); }
            set { utilities = value; }
        }

        /// <summary>
        /// This uses RestSharp internal JsonDeserialiser to convert a json string to a business object
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public static CrmReference FromJson(string jsonString)
        {
            IRestResponse rsp = new RestResponse();
            rsp.Content = jsonString;

            JsonDeserializer deser = new JsonDeserializer();
            var crmReference = deser.Deserialize<CrmReference>(rsp);

            return crmReference;

        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class Contact
    {
        [DeserializeAs(Name = "name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [DeserializeAs(Name = "work_phone")]
        [JsonProperty("work_phone")]
        public string WorkPhone { get; set; }

        [DeserializeAs(Name = "title")]
        [JsonProperty("title")]
        public string Title { get; set; }
    }

    public class DistributerInfo
    {
        private List<string> customerNumbers;
        private List<Contact> activeSecondaryContacts;
        private Contact contact;

        [DeserializeAs(Name = "account_id")]
        [JsonProperty("account_id")]
        public string AccountId { get; set; }

        [DeserializeAs(Name = "account_name")]
        [JsonProperty("account_name")]
        public string AccountName { get; set; }

        [DeserializeAs(Name = "address1")]
        [JsonProperty("address1")]
        public string Address1 { get; set; }

        [DeserializeAs(Name = "address2")]
        [JsonProperty("address2")]
        public string Address2 { get; set; }

        [DeserializeAs(Name = "address3")]
        [JsonProperty("address3")]
        public string Address3 { get; set; }

        [DeserializeAs(Name = "city")]
        [JsonProperty("city")]
        public string City { get; set; }

        [DeserializeAs(Name = "state")]
        [JsonProperty("state")]
        public string State { get; set; }

        [DeserializeAs(Name = "postal_code")]
        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }

        [DeserializeAs(Name = "country")]
        [JsonProperty("country")]
        public string Country { get; set; }

        [DeserializeAs(Name = "main_phone")]
        [JsonProperty("main_phone")]
        public string MainPhone { get; set; }

        [DeserializeAs(Name = "fax")]
        [JsonProperty("fax")]
        public string Fax { get; set; }

        [DeserializeAs(Name = "sales_territory_owner")]
        [JsonProperty("sales_territory_owner")]
        public string SalesTerritoryOwner { get; set; }

        [DeserializeAs(Name = "account_manager")]
        [JsonProperty("account_manager")]
        public string AccountManager { get; set; }

        [DeserializeAs(Name = "regional_manager")]
        [JsonProperty("regional_manager")]
        public string RegionalManager { get; set; }

        [DeserializeAs(Name = "type")]
        [JsonProperty("type")]
        public string Type { get; set; }

        [DeserializeAs(Name = "sub_type")]
        [JsonProperty("sub_type")]
        public string SubType { get; set; }

        [DeserializeAs(Name = "status")]
        [JsonProperty("status")]
        public string Status { get; set; }

        [DeserializeAs(Name = "primary_contact")]
        [JsonProperty("primary_contact")]
        public Contact Contact
        {
            get { return contact ?? (contact = new Contact()); }
            set { contact = value; }
        }

        [DeserializeAs(Name = "active_secondary_contacts")]
        [JsonProperty("active_secondary_contacts")]
        public List<Contact> ActiveSecondaryContacts
        {
            get { return activeSecondaryContacts ?? (activeSecondaryContacts = new List<Contact>()); }
            set { activeSecondaryContacts = value; }
        }

        [DeserializeAs(Name = "customer_numbers")]
        [JsonProperty("customer_numbers")]
        public List<string> CustomerNumbers
        {
            get { return customerNumbers ?? (customerNumbers = new List<string>()); }
            set { customerNumbers = value; }
        }

        [DeserializeAs(Name = "parent_customer_number")]
        [JsonProperty("parent_customer_number")]
        public string ParentCustomerNumber { get; set; }
    }

    public class Distributer
    {
        [DeserializeAs(Name = "customer_number")]
        [JsonProperty("customer_number")]
        public string CustomerNumber { get; set; }

        [DeserializeAs(Name = "info")]
        [JsonProperty("info")]
        public DistributerInfo Info { get; set; }
    }

    public class UtilityInfo
    {
        private List<string> customerNumbers;
        private List<Contact> activeSecondaryContacts;
        private Contact contact;
        //[DeserializeAs(Name = "account_id")]
        //[JsonProperty("account_id")]
        //public string AccountId { get; set; }

        [DeserializeAs(Name = "system_id")]
        [JsonProperty("system_id")]
        public string SystemId { get; set; }

        [DeserializeAs(Name = "account_name")]
        [JsonProperty("account_name")]
        public string AccountName { get; set; }

        [DeserializeAs(Name = "customer_number")]
        [JsonProperty("customer_number")]
        public string CustomerNumber { get; set; }

        [DeserializeAs(Name = "address1")]
        [JsonProperty("address1")]
        public string Address1 { get; set; }

        [DeserializeAs(Name = "address2")]
        [JsonProperty("address2")]
        public string Address2 { get; set; }

        [DeserializeAs(Name = "address3")]
        [JsonProperty("address3")]
        public string Address3 { get; set; }

        [DeserializeAs(Name = "city")]
        [JsonProperty("city")]
        public string City { get; set; }

        [DeserializeAs(Name = "state")]
        [JsonProperty("state")]
        public string State { get; set; }

        [DeserializeAs(Name = "postal_code")]
        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }

        [DeserializeAs(Name = "country")]
        [JsonProperty("country")]
        public string Country { get; set; }

        [DeserializeAs(Name = "main_phone")]
        [JsonProperty("main_phone")]
        public string MainPhone { get; set; }

        [DeserializeAs(Name = "fax")]
        [JsonProperty("fax")]
        public string Fax { get; set; }

        [DeserializeAs(Name = "sales_territory_owner")]
        [JsonProperty("sales_territory_owner")]
        public string SalesTerritoryOwner { get; set; }

        [DeserializeAs(Name = "account_manager")]
        [JsonProperty("account_manager")]
        public string AccountManager { get; set; }

        [DeserializeAs(Name = "regional_manager")]
        [JsonProperty("regional_manager")]
        public string RegionalManager { get; set; }

        [DeserializeAs(Name = "type")]
        [JsonProperty("type")]
        public string Type { get; set; }

        [DeserializeAs(Name = "sub_type")]
        [JsonProperty("sub_type")]
        public string SubType { get; set; }

        [DeserializeAs(Name = "status")]
        [JsonProperty("status")]
        public string Status { get; set; }

        [DeserializeAs(Name = "primary_contact")]
        [JsonProperty("primary_contact")]
        public Contact Contact
        {
            get { return contact ?? (contact = new Contact()); }
            set { contact = value; }
        }

        [DeserializeAs(Name = "active_secondary_contacts")]
        [JsonProperty("active_secondary_contacts")]
        public List<Contact> ActiveSecondaryContacts
        {
            get { return activeSecondaryContacts ?? (activeSecondaryContacts = new List<Contact>()); }
            set { activeSecondaryContacts = value; }
        }

        [DeserializeAs(Name = "customer_numbers")]
        [JsonProperty("customer_numbers")]
        public List<string> CustomerNumbers
        {
            get { return customerNumbers ?? (customerNumbers = new List<string>()); }
            set { customerNumbers = value; }
        }

        [DeserializeAs(Name = "parent_customer_number")]
        [JsonProperty("parent_customer_number")]
        public string ParentCustomerNumber { get; set; }
    }

    public class Utility
    {
        [DeserializeAs(Name = "site_id")]
        [JsonProperty("site_id")]
        public int SiteId { get; set; }

        [DeserializeAs(Name = "info")]
        [JsonProperty("info")]
        public UtilityInfo Info { get; set; }
    }

}
