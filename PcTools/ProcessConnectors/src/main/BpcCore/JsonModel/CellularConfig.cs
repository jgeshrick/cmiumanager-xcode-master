﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using RestSharp;
using RestSharp.Deserializers;
using System;
using Newtonsoft.Json;

namespace BpcCore.JsonModel.CellularConfig
{
    public class CellularConfig
    {
        [DeserializeAs(Name = "miu_id")]
        [JsonProperty("miu_id")]
        public int MiuId { get; set; }

        [DeserializeAs(Name = "modem_config")]
        [JsonProperty("modem_config")]
        public ModemConfig ModemConfig { get; set; }

        [DeserializeAs(Name = "sim_config")]
        [JsonProperty("sim_config")]
        public SimConfig SimConfig { get; set; }

        /// <summary>
        /// This uses RestSharp internal JsonDeserialiser to convert a json string to a business object
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public static CellularConfig fromJSON(String jsonString)
        {
            IRestResponse rsp = new RestResponse();
            rsp.Content = jsonString;

            JsonDeserializer deser = new JsonDeserializer();
            var cellularConfig = deser.Deserialize<CellularConfig>(rsp);

            return cellularConfig;
        }
    }

    public class ModemConfig
    {
        [DeserializeAs(Name = "vendor")]
        [JsonProperty("vendor")]
        public string Vendor { get; set; }

        [DeserializeAs(Name = "firmware_revision")]
        [JsonProperty("firmware_revision")]
        public string FirmwareRevision { get; set; }

        [DeserializeAs(Name = "imei")]
        [JsonProperty("imei")]
        public string Imei { get; set; }
    }

    public class SimConfig
    {
        [DeserializeAs(Name = "iccid")]
        [JsonProperty("iccid")]
        public string Iccid { get; set; }

        [DeserializeAs(Name = "imsi")]
        [JsonProperty("imsi")]
        public string Imsi { get; set; }

        [DeserializeAs(Name = "msidn")]
        [JsonProperty("msidn")]
        public string Msidn { get; set; }
    }
}