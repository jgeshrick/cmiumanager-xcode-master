﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using RestSharp.Deserializers;

namespace Ntg.BpcCore.JsonModel
{
    public class MdceErrorResponse
    {
        [DeserializeAs(Name = "ref_id")]
        public int ReferenceId { get; set; }

        [DeserializeAs(Name = "error")]
        public string Error { get; set; }
    }
}