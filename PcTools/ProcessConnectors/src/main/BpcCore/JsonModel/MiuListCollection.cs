﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using Newtonsoft.Json;

namespace Ntg.BpcCore.JsonModel
{
    /// <summary>
    /// JSON object for multiple MIUs.
    /// </summary>
    public class MiuListCollection
    {
        /// <summary>
        /// The list of MIUs.
        /// </summary>
        [JsonProperty("mius")]
        public List<MiuDetail> Mius { get; set; }
    }
}