﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using Newtonsoft.Json;

namespace Ntg.BpcCore.JsonModel
{
    /// <summary>
    /// JSON object for a single MIU.
    /// </summary>
    public class MiuDetail
    {
        /// <summary>
        /// Gets or sets the MIU ID.
        /// </summary>
        [JsonProperty("miu_id")]
        public int MiuId { get; set; }

        /// <summary>
        /// Gets or sets the Site ID.
        /// </summary>
        [JsonProperty("site_id")]
        public int SiteId { get; set; }

        /// <summary>
        /// Gets or sets the ICCID of a cellular device.
        /// </summary>
        [JsonProperty("iccid")]
        public string Iccid { get; set; }

        /// <summary>
        /// Gets or sets the MSISDN of a cellular device.
        /// </summary>
        [JsonProperty("msisdn")]
        public string Msisdn { get; set; }

        /// <summary>
        /// Gets or sets the network provider for a LoRa or cellular device.
        /// </summary>
        [JsonProperty("network_provider")]
        public string NetworkProvider { get; set; }

        /// <summary>
        /// Gets or sets the MIU type.
        /// </summary>
        [JsonProperty("miu_type")]
        public string MiuType { get; set; }

        /// <summary>
        /// Gets or sets the IMEI.
        /// </summary>
        [JsonProperty("imei")]
        public string Imei { get; set; }

        /// <summary>
        /// Gets or sets the EUI.
        /// </summary>
        [JsonProperty("eui")]
        public string Eui { get; set; }

        /// <summary>
        /// Gets or sets the first insert date.
        /// </summary>
        [JsonProperty("first_insert_date")]
        public string FirstInsertDate { get; set; }

        /// <summary>
        /// Gets or sets the last insert date.
        /// </summary>
        [JsonProperty("last_insert_date")]
        public string LastInsertDate { get; set; }
    }
}