//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using RestSharp.Deserializers;

namespace Ntg.BpcCore.JsonModel.MiuConfig
{
    public class Billing
    {

        [DeserializeAs(Name = "site_id")]
        public int SiteId { get; set; }

        [DeserializeAs(Name = "distributer_id")]
        public int DistributerId { get; set; }

        [DeserializeAs(Name = "billable")]
        public string Billable { get; set; }

        [DeserializeAs(Name = "billing_plan_id")]
        public int? BillingPlanId { get; set; }
    }
}