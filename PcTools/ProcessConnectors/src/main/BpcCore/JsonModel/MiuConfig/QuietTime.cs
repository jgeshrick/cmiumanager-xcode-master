//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using RestSharp.Deserializers;

namespace Ntg.BpcCore.JsonModel.MiuConfig
{
    public class QuietTime
    {

        [DeserializeAs(Name = "quiet_start_mins")]
        public int QuietStartMins { get; set; }

        [DeserializeAs(Name = "quiet_end_mins")]
        public int QuietEndMins { get; set; }
    }
}