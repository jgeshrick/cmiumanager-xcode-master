﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;

namespace Ntg.BpcCore.JsonModel
{
    public class MiuConfigList
    {
        [DeserializeAs(Name = "miuconfigs")]
        public List<MiuConfig.MiuConfig> Mius { get; set; }

        /// <summary>
        /// This uses RestSharp internal JsonDeserialiser to convert a json string to a business object
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public static MiuConfigList fromJSON(string jsonString)
        {
            IRestResponse rsp = new RestResponse();
            rsp.Content = jsonString;

            JsonDeserializer deser = new JsonDeserializer();
            var miuConfigList = deser.Deserialize<MiuConfigList>(rsp);

            return miuConfigList;
        }
    }
}