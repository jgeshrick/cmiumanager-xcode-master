﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using Newtonsoft.Json;

namespace Ntg.BpcCore.JsonModel.OwnerHistory
{
    /// <summary>
    /// A single MIU owner event with a sequence ID.
    /// </summary>
    public class SequentialMiuOwner
    {
        [JsonProperty("sequence_id")]
        public int SequenceId { get; set; }

        [JsonProperty("miu_id")]
        public int MiuId { get; set; }

        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }

        [JsonProperty("site_id")]
        public int SiteId { get; set; }
    }
}