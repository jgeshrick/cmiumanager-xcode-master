﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using Newtonsoft.Json;

namespace Ntg.BpcCore.JsonModel.OwnerHistory
{
    /// <summary>
    /// A collection of historic MIU owner events.
    /// </summary>
    public class SequentialMiuOwnerCollection
    {
        [JsonProperty("owner_history")]
        public List<SequentialMiuOwner> OwnerHistory { get; set; }
    }
}