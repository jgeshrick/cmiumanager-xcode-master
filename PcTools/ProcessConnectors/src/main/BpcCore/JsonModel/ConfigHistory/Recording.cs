//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using RestSharp.Deserializers;

namespace Ntg.BpcCore.JsonModel.ConfigHistory
{
    public class Recording
    {
        [DeserializeAs(Name = "start_time_mins")]
        public int? StartTimeMins { get; set; }

        [DeserializeAs(Name = "interval_mins")]
        public int? IntervalMins { get; set; }
    }
}