﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using Newtonsoft.Json;

namespace Ntg.BpcCore.JsonModel.ConfigHistory
{
    /// <summary>
    /// A single MIU configuration event with a sequence ID.
    /// </summary>
    public class SequentialMiuConfig
    {
        [JsonProperty("sequence_id")]
        public int SequenceId { get; set; }

        [JsonProperty("miu_id")]
        public int MiuId { get; set; }

        [JsonProperty("received_from_miu")]
        public bool ReceivedFromMiu { get; set; }

        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }

        [JsonProperty("recording_interval_minutes")]
        public int RecordingIntervalMinutes { get; set; }

        [JsonProperty("reporting_interval_minutes")]
        public int ReportingIntervalMinutes { get; set; }
    }
}