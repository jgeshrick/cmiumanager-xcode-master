//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using RestSharp.Deserializers;

namespace Ntg.BpcCore.JsonModel.ConfigHistory
{
    /// <summary>
    /// Models the JSON structure containing the components of an MIU configuration.
    /// </summary>
    public class MiuConfigDetail
    {
        [DeserializeAs(Name = "billing")]
        public Billing Billing { get; set; }

        [DeserializeAs(Name = "recording")]
        public Recording Recording { get; set; }

        [DeserializeAs(Name = "reporting")]
        public Reporting Reporting { get; set; }

        [DeserializeAs(Name = "quiet_time")]
        public QuietTime QuietTime { get; set; }
    }
}