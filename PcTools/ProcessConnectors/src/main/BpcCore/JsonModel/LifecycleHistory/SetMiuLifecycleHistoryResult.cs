﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using RestSharp.Deserializers;

namespace Ntg.BpcCore.JsonModel.LifecycleHistory
{
    public class SetMiuLifecycleHistoryResult
    {
        [DeserializeAs(Name = "succeeded")]
        public int SucceededCount { get; set; }

        [DeserializeAs(Name = "failed")]
        public int ErrorCount { get; set; }

        [DeserializeAs(Name = "error_details")]
        public List<MdceErrorResponse> ErrorDetails { get; set; }
    }
}