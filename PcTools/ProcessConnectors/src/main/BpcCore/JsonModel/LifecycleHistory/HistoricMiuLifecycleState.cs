﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using Newtonsoft.Json;

namespace Ntg.BpcCore.JsonModel.LifecycleHistory
{
    public class HistoricMiuLifecycleState
    {
        public HistoricMiuLifecycleState()
        {
        }

        public HistoricMiuLifecycleState(int miuId, string lifecycleState, DateTime timestamp, int referenceId)
        {
            MiuId = miuId;
            LifecycleState = lifecycleState;
            ReferenceId = referenceId;
            SetTimestamp(timestamp);
        }

        [JsonProperty("ref_id")]
        public int ReferenceId { get; set; }

        [JsonProperty("miu_id")]
        public int MiuId { get; set; }

        [JsonProperty("lifecycle_state")]
        public string LifecycleState { get; set; }

        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }

        private void SetTimestamp(DateTime dateTime)
        {
            // Assume UTC by default
            if (dateTime.Kind == DateTimeKind.Unspecified)
            {
                dateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
            }

            Timestamp = dateTime.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }
    }
}