﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using RestSharp.Deserializers;

namespace Ntg.BpcCore.JsonModel.LifecycleHistory
{
    public class SequentialMiuLifecycleState
    {
        [DeserializeAs(Name = "sequence_id")]
        public int SequenceId { get; set; }

        [DeserializeAs(Name = "miu_id")]
        public int MiuId { get; set; }

        [DeserializeAs(Name = "timestamp")]
        public DateTime Timestamp { get; set; }

        [DeserializeAs(Name = "state")]
        public string State { get; set; }
    }
}