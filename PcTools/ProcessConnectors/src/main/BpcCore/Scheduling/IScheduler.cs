using System;
using System.Collections.Generic;
using System.Web.Hosting;

namespace Ntg.BpcCore.Scheduling
{
    public interface IScheduler : IRegisteredObject
    {
        /// <summary>
        /// Gets the list of scheduled services.
        /// </summary>
        IList<ScheduledActivity> ScheduledServices { get; }

        /// <summary>
        /// Starts the scheduler on all registered services.
        /// </summary>
        void EnableScheduledServices();

        /// <summary>
        /// Stops the scheduler on all registered services.
        /// </summary>
        void DisableScheduledServices();

        /// <summary>
        /// Helper method to get a service by <see cref="DataConnectorType"/> enumeration.
        /// </summary>
        /// <param name="dataConnectorType"></param>
        /// <returns></returns>
        ScheduledActivity GetScheduledService(DataConnectorType dataConnectorType);

        /// <summary>
        /// Gets the last time a data service was executed.
        /// </summary>
        DateTime? LastExecutionTime { get; }
    }
}