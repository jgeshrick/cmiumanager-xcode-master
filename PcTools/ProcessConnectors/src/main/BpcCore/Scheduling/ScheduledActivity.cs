﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using log4net;
using Ntg.BpcCore.DataConnectors;
using Ntg.BpcCore.Settings;

namespace Ntg.BpcCore.Scheduling
{
    /// <summary>
    /// Provides scheduling for a single <see cref="IDataConnector"/> instance.
    /// </summary>
    public class ScheduledActivity
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IServiceSettingsRepository settingsRepository;
        private readonly Timer timer;
        private bool isEnabled;
        private readonly List<ScheduleConfiguration> scheduleConfigurations;
        private Task executingTask;
        private DateTime? nextExecutionTime;

        /// <summary>
        /// Creates a new instance of <see cref="ScheduledActivity"/>.
        /// </summary>
        public ScheduledActivity(IDataConnector dataConnector, IServiceSettingsRepository settingsRepository)
        {
            this.settingsRepository = settingsRepository;
            if (dataConnector == null)
            {
                throw new ArgumentNullException("dataConnector");
            }

            timer = new Timer(1000) {AutoReset = true};
            timer.Elapsed += CheckSchedule;
            isEnabled = false;
            DataConnector = dataConnector;
            scheduleConfigurations = new List<ScheduleConfiguration>();
            LoadSettings();
        }

        /// <summary>
        /// Returns a string indicating the state of the data service.
        /// </summary>
        public string Status
        {
            get
            {
                if (DataConnector.IsExecuting)
                {
                    return "Executing";
                }
                else if (IsEnabled)
                {
                    return "Started";
                }
                else
                {
                    return "Stopped";
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the data service execution 
        /// schedule is enabled.
        /// </summary>
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                if (value)
                    // Ensure NextExecutionTime has a value
                {
                    if (!nextExecutionTime.HasValue || nextExecutionTime < DateTime.UtcNow)
                    {
                        nextExecutionTime = DateTime.UtcNow;
                    }
                }

                isEnabled = value;
                timer.Enabled = value;
                SaveSettings();
            }
        }

        /// <summary>
        /// Gets or sets the data service execution interval. When set, the new value is 
        /// ignored if the interval is less than one second.
        /// </summary>
        public List<ScheduleConfiguration> ScheduleConfigurations
        {
            get { return scheduleConfigurations; }
        }

        /// <summary>
        /// Gets or sets the last known time the data service was executed.
        /// </summary>
        public DateTime? LastExecutedTime { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the most recent execution run was successful.
        /// </summary>
        public bool? Succeeded { get; set; }

        /// <summary>
        /// Gets the time the data service is next scheduled to be executed. Returns 
        /// <code>null</code> if the schedule is disabled.
        /// </summary>
        public DateTime? NextExecutionTime
        {
            get { return IsEnabled ? nextExecutionTime : null; }
        }

        /// <summary>
        /// Gets the data service controlled by this scheduler.
        /// </summary>
        public IDataConnector DataConnector { get; private set; }

        /// <summary>
        /// Checks whether the scheduled execution time has been reached and executes the 
        /// data service if it has.
        /// </summary>
        private void CheckSchedule(object sender, ElapsedEventArgs e)
        {
            if (NextExecutionTime == null)
            {
                return;
            }

            if (DateTime.UtcNow > NextExecutionTime.Value)
            {
                // From the configured schedules, get the earliest next scheduled run time.
                nextExecutionTime = scheduleConfigurations
                    .Select(sc => sc.GetNext(DateTime.UtcNow))
                    .Where(dt => dt.HasValue)
                    .OrderBy(dt => dt.Value)
                    .FirstOrDefault();

                SaveSettings();
                Log.Debug(10000, string.Format("Ready to execute data service {0}. Next scheduled time: {1}.", DataConnector.Name, nextExecutionTime));
                
                // Don't execute if already in progress
                if (DataConnector.IsExecuting)
                {
                    Log.Info(10001, string.Format(
                        "Data service {0} is already executing. Next scheduled time: {1}.",
                        DataConnector.Name, nextExecutionTime));
                }
                else
                {
                    Log.Info(10026, string.Format("Starting scheduled execution of data service \"{0}\".", DataConnector.Name));
                    ExecuteNow();
                }
            }
        }

        /// <summary>
        /// Starts the process on the data service.
        /// </summary>
        public void ExecuteNow()
        {
            if (DataConnector.IsExecuting)
            {
                return;
            }

            LastExecutedTime = DateTime.UtcNow;
            SaveSettings();
            // Run the process asynchronously
            executingTask = Task.Run(() =>
            {
                bool? succeeded;

                try
                {
                    succeeded = DataConnector.Execute();
                }
                catch (System.Exception exception)
                {
                    Log.Error(10002, string.Format("Execution of data service {0} failed.", DataConnector.DataConnectorType), exception);
                    succeeded = false;
                }

                if (succeeded.HasValue)
                {
                    Succeeded = succeeded;
                }

                // Update again after proess has completed
                SaveSettings();
            });
        }

        /// <summary>
        /// Stores the values of IsEnabled, LastExecutedTime and NextExecutionTime using the current <see cref="IServiceSettingsRepository"/>.
        /// </summary>
        public void SaveSettings()
        {
            settingsRepository.SaveSettings(DataConnector.DataConnectorType, isEnabled, LastExecutedTime, Succeeded, nextExecutionTime);
        }

        /// <summary>
        /// Loads the values of IsEnabled, LastExecutedTime and NextExecutionTime using the current <see cref="IServiceSettingsRepository"/>.
        /// </summary>
        private void LoadSettings()
        {
            DateTime? lastExecutedTime;
            DateTime? tempNextExecutionTime;
            bool? succeeded;
            bool enabled;
            settingsRepository.LoadSettings(DataConnector.DataConnectorType, out enabled, out lastExecutedTime, out succeeded, out tempNextExecutionTime);
            // Ensure times are loaded as UTC
            LastExecutedTime = lastExecutedTime == null ? (DateTime?)null : DateTime.SpecifyKind(lastExecutedTime.Value, DateTimeKind.Utc);
            Succeeded = succeeded;
            IsEnabled = enabled;
        }
    }
}