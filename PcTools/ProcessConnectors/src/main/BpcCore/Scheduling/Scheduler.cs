﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Hosting;
using System.Web.Security;
using log4net;
using Ntg.BpcCore.DataConnectors;
using Ntg.BpcCore.Domain;
using Ntg.BpcCore.Settings;
using RestSharp;

namespace Ntg.BpcCore.Scheduling
{
    /// <summary>
    /// Provides scheduling for data connectors.
    /// </summary>
    public class Scheduler : IScheduler
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IEnvironmentsConfig environmentsConfig;
        private readonly IBpcRestClients bpcRestClients;
        private readonly IServiceSettingsRepository serviceSettingsRepository;

        /// <summary>
        /// Gets the list of scheduled services.
        /// </summary>
        public IList<ScheduledActivity> ScheduledServices { get; private set; }

        /// <summary>
        /// Creates a new instance of <see cref="Scheduler"/>.
        /// </summary>
        public Scheduler(IEnvironmentsConfig environmentsConfig, IBpcRestClients bpcRestClients,
            IServiceSettingsRepository serviceSettingsRepository)
        {
            this.environmentsConfig = environmentsConfig;
            this.bpcRestClients = bpcRestClients;
            this.serviceSettingsRepository = serviceSettingsRepository;

            var primaryMdceEnvironmentRestClient = bpcRestClients.RestClients
                    .Single(c => c.Key.Equals(environmentsConfig.PrimaryMdceEnvironmentName))
                    .Value;

            // TODO: Consider refactoring data services initialization & config.

            // Add an instance of each configured data connector
            ScheduledServices = new List<ScheduledActivity>();

            // Customer data repository
            var dapperCustomerDataRepository = new DapperCustomerDataRepository(environmentsConfig.CustomerDataServer);

            // Customer data connector
            var customerConnectorActivity = new ScheduledActivity(new CustomerDataConnector(bpcRestClients, dapperCustomerDataRepository), serviceSettingsRepository);
            customerConnectorActivity.ScheduleConfigurations.AddRange(
                environmentsConfig.ScheduleConfigurations.Where(s => s.DataConnectorType == DataConnectorType.CustomerDataConnector));
            ScheduledServices.Add(customerConnectorActivity);

            // Lifecycle data connector
            var transferQueueRepository = new TransferQueueRepository(environmentsConfig.CostAndRevenueServerDataServer);
            var qadConnectorActivity = new ScheduledActivity(new MiuLifecycleDataConnector(primaryMdceEnvironmentRestClient, transferQueueRepository), serviceSettingsRepository);
            qadConnectorActivity.ScheduleConfigurations.AddRange(
                environmentsConfig.ScheduleConfigurations.Where(s => s.DataConnectorType == DataConnectorType.MiuLifecycleDataConnector));
            ScheduledServices.Add(qadConnectorActivity);

            // Cost & revenue data connector
            var crDataRepository = new CostRevenueDataRepository(environmentsConfig.CostAndRevenueServerDataServer);
            var crConnectorActivity = new ScheduledActivity(new CostRevenueDataConnector(primaryMdceEnvironmentRestClient, crDataRepository), serviceSettingsRepository);
            crConnectorActivity.ScheduleConfigurations.AddRange(
                environmentsConfig.ScheduleConfigurations.Where(s => s.DataConnectorType == DataConnectorType.CostRevenueDataConnector));
            ScheduledServices.Add(crConnectorActivity);
        }

        /// <summary>
        /// Starts the scheduler on all registered services.
        /// </summary>
        public void EnableScheduledServices()
        {
            foreach (var scheduledService in ScheduledServices)
            {
                scheduledService.IsEnabled = true;
            }
        }

        /// <summary>
        /// Stops the scheduler on all registered services.
        /// </summary>
        public void DisableScheduledServices()
        {
            foreach (var scheduledService in ScheduledServices)
            {
                scheduledService.IsEnabled = false;
            }
        }

        /// <summary>
        /// Gets a specific service by type enumeration.
        /// </summary>
        public ScheduledActivity GetScheduledService(DataConnectorType dataConnectorType)
        {
            return ScheduledServices.FirstOrDefault(s => s.DataConnector.DataConnectorType == dataConnectorType);
        }

        /// <summary>
        /// Gets the last time a data service was executed.
        /// </summary>
        public DateTime? LastExecutionTime
        {
            get
            {
                return ScheduledServices
                    .Select(s => s.LastExecutedTime)
                    .Where(t => t.HasValue)
                    .OrderByDescending(t => t)
                    .FirstOrDefault();
            }
        }

        /// <summary>
        /// Implementation of the <see cref="IRegisteredObject"/>.<code>Stop(bool)</code> interface method.
        /// </summary>
        /// <param name="immediate"></param>
        public void Stop(bool immediate)
        {
            // TODO: Implement data connector termination command
        }
    }
}