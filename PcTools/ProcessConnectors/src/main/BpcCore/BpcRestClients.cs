﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using System.Linq;
using Ntg.BpcCore.Settings;

namespace Ntg.BpcCore
{
    /// <summary>
    /// Contains a list of configured Bpc Rest Clients mapped to the MDCE environments
    /// </summary>
    public class BpcRestClients : IBpcRestClients
    {
        /// <summary>
        /// Creates a new instance of <see cref="BpcRestClients"/>, initialized from the provided list of 
        /// MDCE environments and the provided <see cref="IEnvironmentSettingsRepository"/>.
        /// </summary>
        /// <param name="mdceEnvironmentList"></param>
        /// <param name="environmentSettingsRepository"></param>
        public BpcRestClients(IEnvironmentsConfig mdceEnvironmentList, IEnvironmentSettingsRepository environmentSettingsRepository)
        {
            RestClients =
                mdceEnvironmentList.EnvironmentList
                    .Select(env => (IMdceEnvironmentBpcRestClient) new MdceEnvironmentBpcRestClient(env, environmentSettingsRepository))
                    .ToDictionary(v => v.Environment.Name, v => v);
        }

        /// <summary>
        /// Gets a read-only dictionary of MDCE ReST clients by environment name.
        /// </summary>
        public IReadOnlyDictionary<string, IMdceEnvironmentBpcRestClient> RestClients { get; private set; }
    }
}