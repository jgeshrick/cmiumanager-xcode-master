﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using Ntg.BpcCore.Scheduling;

namespace Ntg.BpcCore.Settings
{
    /// <summary>
    /// Provides methods for storage and retrieval of runtime settings for a <see cref="ScheduledActivity"/> instance.
    /// </summary>
    public interface IServiceSettingsRepository
    {
        /// <summary>
        /// Saves the provided service runtime settings.
        /// </summary>
        /// <param name="dataConnectorType">Indicates which service settings are being saved.</param>
        /// <param name="isEnabled"></param>
        /// <param name="lastExecutedTime"></param>
        /// <param name="succeeded"></param>
        /// <param name="nextExecutionTime"></param>
        void SaveSettings(DataConnectorType dataConnectorType, bool isEnabled, DateTime? lastExecutedTime, bool? succeeded, DateTime? nextExecutionTime);

        /// <summary>
        /// Loads the service runtime settings.
        /// </summary>
        /// <param name="dataConnectorType">Indicates which service settings to load.</param>
        /// <param name="isEnabled"></param>
        /// <param name="lastExecutedTime"></param>
        /// <param name="succeeded"></param>
        /// <param name="nextExecutionTime"></param>
        void LoadSettings(DataConnectorType dataConnectorType, out bool isEnabled, out DateTime? lastExecutedTime, out bool? succeeded, out DateTime? nextExecutionTime);
    }
}