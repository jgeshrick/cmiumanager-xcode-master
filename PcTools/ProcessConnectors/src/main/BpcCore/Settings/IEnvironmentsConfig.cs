﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;

namespace Ntg.BpcCore.Settings
{
    /// <summary>
    /// When implemented, provides configuration related to the connected environments.
    /// </summary>
    public interface IEnvironmentsConfig
    {
        /// <summary>
        /// MDCE environment specification
        /// </summary>
        List<MdceEnvironment> EnvironmentList { get; set; }

        /// <summary>
        /// SalesLogix MS SQL server name
        /// </summary>
        string CustomerDataServer { get; set; }

        string CostAndRevenueServerDataServer { get; set; }

        /// <summary>
        /// The name of the MDCE environment that will be synchronized with QAD.
        /// </summary>
        string PrimaryMdceEnvironmentName { get; set; }

        /// <summary>
        /// Service schedule settings.
        /// </summary>
        List<ScheduleConfiguration> ScheduleConfigurations { get; set; }
    }
}