﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using log4net;
using Microsoft.Win32;

namespace Ntg.BpcCore.Settings
{
    /// <summary>
    /// An <see cref="IServiceSettingsRepository"/> implementation that persists to the Windows Registry.
    /// </summary>
    public class RegistryServiceSettingsRepository : IServiceSettingsRepository
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Saves the provided service runtime settings.
        /// </summary>
        /// <param name="dataConnectorType">Indicates which service settings are being saved.</param>
        /// <param name="isEnabled"></param>
        /// <param name="lastExecutedTime"></param>
        /// <param name="succeeded"></param>
        /// <param name="nextExecutionTime"></param>
        public void SaveSettings(DataConnectorType dataConnectorType, bool isEnabled, DateTime? lastExecutedTime, bool? succeeded,
            DateTime? nextExecutionTime)
        {
            string hkcuPath = GetHkcuRegistryPath(dataConnectorType);

            using (var key = Registry.CurrentUser.CreateSubKey(hkcuPath))
            {
                if (key != null)
                {
                    key.SetValue("IsEnabled", isEnabled.ToString());
                    if (lastExecutedTime.HasValue)
                    {
                        key.SetValue("LastExecutedTime", lastExecutedTime.Value.ToString("O"));
                    }
                    else
                    {
                        key.DeleteValue("LastExecutedTime", false);
                    }

                    if (succeeded.HasValue)
                    {
                        key.SetValue("Succeeded", succeeded.ToString());
                    }
                    else
                    {
                        key.DeleteValue("Succeeded", false);
                    }

                    if (nextExecutionTime.HasValue)
                    {
                        key.SetValue("NextExecutionTime", nextExecutionTime.Value.ToString("O"));
                    }
                    else
                    {
                        key.DeleteValue("NextExecutionTime", false);
                    }

                    Log.Debug(10036, string.Format(
                        "SaveSettings: Data connector: {0}; IsEnabled: {1}; LastExecutedTime: {2}; Succeeded: {3}; NextExecutionTime: {4}.",
                        dataConnectorType, isEnabled, lastExecutedTime, succeeded, nextExecutionTime));
                }
                else
                {
                    Log.Error(10037, string.Format("SaveSettings: Registry key was not created: HKCU\\{0}", hkcuPath));
                }
            }
        }

        /// <summary>
        /// Loads the service runtime settings.
        /// </summary>
        /// <param name="dataConnectorType">Indicates which service settings to load.</param>
        /// <param name="isEnabled"></param>
        /// <param name="lastExecutedTime"></param>
        /// <param name="succeeded"></param>
        /// <param name="nextExecutionTime"></param>
        public void LoadSettings(DataConnectorType dataConnectorType, out bool isEnabled, out DateTime? lastExecutedTime, out bool? succeeded,
            out DateTime? nextExecutionTime)
        {
            bool wasRegistryKeyPresent = false;
            using (var key = Registry.CurrentUser.OpenSubKey(GetHkcuRegistryPath(dataConnectorType)))
            {
                isEnabled = false;
                lastExecutedTime = null;
                succeeded = null;
                nextExecutionTime = null;

                // Attempt to read from the registry
                if (key != null)
                {
                    wasRegistryKeyPresent = true;

                    bool.TryParse(key.GetValue("IsEnabled") as string, out isEnabled);
                    DateTime parsedDateTime;
                    bool parsedBoolean;
                    if (DateTime.TryParse(key.GetValue("LastExecutedTime") as string, out parsedDateTime))
                        lastExecutedTime = parsedDateTime;

                    if (bool.TryParse(key.GetValue("Succeeded") as string, out parsedBoolean))
                        succeeded = parsedBoolean;

                    if (DateTime.TryParse(key.GetValue("NextExecutionTime") as string, out parsedDateTime))
                        nextExecutionTime = parsedDateTime;
                }
            }

            Log.Debug(10035, string.Format(
                "LoadSettings: Data connector: {0}; Configuration loaded from Registry: {1}; IsEnabled: {2}; LastExecutedTime: {3}; Succeeded: {4}; NextExecutionTime: {5}.",
                dataConnectorType, wasRegistryKeyPresent, isEnabled, lastExecutedTime, succeeded, nextExecutionTime));
        }

        /// <summary>
        /// Gets the HKCU registry path under which the encrypted partner string is stored.
        /// </summary>
        /// <returns></returns>
        private string GetHkcuRegistryPath(DataConnectorType dataConnectorType)
        {
            return string.Format("Software\\Neptune\\BPC\\DataServices\\{0}", dataConnectorType);
        }
    }
}