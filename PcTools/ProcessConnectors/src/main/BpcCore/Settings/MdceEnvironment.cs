//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;

namespace Ntg.BpcCore.Settings
{
    /// <summary>
    /// XML-based configuration settings for an MDCE environment connection.
    /// </summary>
    [Serializable]
    public class MdceEnvironment
    {
        private string baseUrl;

        /// <summary>
        /// Raised when the partner string for the environment is changed.
        /// </summary>
        public event EventHandler<ParnerStringChangedEventArgs> ParnerStringChanged;
        
        /// <summary>
        /// Gets or sets the environment name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Base URL for the environment, eg https://auto-test.mdce-nonprod.neptunensight.com:8444
        /// </summary>
        public string BaseUrl
        {
            get { return baseUrl; }
            set
            {
                baseUrl = value;
                if (baseUrl != null) // Remove trailing / characters
                {
                    baseUrl = baseUrl.TrimEnd('/');
                }
            }
        }

        /// <summary>
        /// Specify whether this environment will be active in participating in the Bpc operations and activity 
        /// syncing with the Bpc related MS SQL database.
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Notifies subscribers that the partner string has changed for this MDCE environment.
        /// </summary>
        /// <param name="e"></param>
        public void OnParnerStringChanged(ParnerStringChangedEventArgs e)
        {
            var handler = ParnerStringChanged;
            if (handler != null)
                handler(this, e);
        }
    }
}