//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using log4net;
using Microsoft.Win32;

namespace Ntg.BpcCore.Settings
{
    /// <summary>
    /// A Registry based implementation of <see cref="IEnvironmentSettingsRepository"/>.
    /// </summary>
    public class RegistryEnvironmentSettingsRepository : IEnvironmentSettingsRepository
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Reads the BPC Partner String from the encrypted registry setting for the provided environment name.
        /// </summary>
        /// <param name="environmentName">The environment name for which the partner string is being read.</param>
        /// <returns>
        /// The decrypted Partner String, or null if the value is not present or cannot be decrypted.
        /// </returns>
        public string GetBpcPartnerString(string environmentName)
        {
            if (string.IsNullOrEmpty(environmentName))
                return null;

            // Read the registry value
            string partnerString = null;
            try
            {
                string encryptedString = null;
                using (var key = Registry.CurrentUser.OpenSubKey(GetHkcuRegistryPath(environmentName)))
                {
                    if (key != null)
                        encryptedString = key.GetValue("PartnerString") as string;
                }

                // Decrypt
                if (!string.IsNullOrEmpty(encryptedString))
                {
                    byte[] encryptedWithSalt = Convert.FromBase64String(encryptedString);
                    byte[] entropy = encryptedWithSalt.Take(16).ToArray();
                    byte[] encrypted = encryptedWithSalt.Skip(16).ToArray();
                    byte[] decrypted = ProtectedData.Unprotect(encrypted, entropy, DataProtectionScope.LocalMachine);
                    partnerString = Encoding.UTF8.GetString(decrypted);
                }
            }
            catch (System.Exception exception)
            {
                Log.Error(10003,
                    string.Format("The PartnerString for environment \"{0}\" could not be decrypted.", environmentName),
                    exception);
            }

            return partnerString;
        }

        /// <summary>
        /// Returns a value indicating whether a Partner String for the current environment 
        /// could be read from configuration.
        /// </summary>
        /// <param name="environmentName">The environment name for which the partner string is being read.</param>
        /// <returns>
        /// True if a Partner String was found; otherwise, false.
        /// </returns>
        public bool HasBpcPartnerString(string environmentName)
        {
            return !string.IsNullOrEmpty(GetBpcPartnerString(environmentName));
        }

        /// <summary>
        /// Writes the supplied BPC Partner String to an encrypted registry setting for the provided environment name.
        /// </summary>
        /// <param name="partnerString">The Partner String to be encrypted and written to the registry.</param>
        /// <param name="environmentName">The environment name for which the partner string is being written.</param>
        public void SetBpcPartnerString(string partnerString, string environmentName)
        {
            if (string.IsNullOrEmpty(environmentName))
                return;

            byte[] entropy = Guid.NewGuid().ToByteArray();
            byte[] unencrypted = Encoding.UTF8.GetBytes(partnerString);
            byte[] encrypted = ProtectedData.Protect(unencrypted, entropy, DataProtectionScope.LocalMachine);
            byte[] encryptedWithSalt = entropy.Concat(encrypted).ToArray();
            string encryptedString = Convert.ToBase64String(encryptedWithSalt);
            using (var key = Registry.CurrentUser.CreateSubKey(GetHkcuRegistryPath(environmentName), RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                if (key != null)
                    key.SetValue("PartnerString", encryptedString);
            }
        }

        /// <summary>
        /// Gets the HKCU registry path under which the encrypted partner string is stored.
        /// </summary>
        /// <returns></returns>
        private string GetHkcuRegistryPath(string name)
        {
            return string.Format("Software\\Neptune\\BPC\\Environments\\{0}", name);
        }
    }
}