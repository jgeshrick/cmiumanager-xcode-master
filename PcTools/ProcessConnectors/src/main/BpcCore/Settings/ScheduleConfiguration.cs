﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Ntg.BpcCore.DataConnectors;

namespace Ntg.BpcCore.Settings
{
    /// <summary>
    /// Defines a single recurring execution schedule for an <see cref="IDataConnector"/> instance.
    /// </summary>
    [Serializable]
    public class ScheduleConfiguration
    {
        public ScheduleConfiguration()
        {
            DaysOfMonth = new List<int>();
        }

        /// <summary>
        /// Gets or sets the type of data service being scheduled.
        /// </summary>
        public DataConnectorType DataConnectorType { get; set; }

        /// <summary>
        /// The offset from 0:00:00 UTC at which to start the activity.
        /// </summary>
        [XmlIgnore]
        public TimeSpan StartTime { get; set; }

        /// <summary>
        /// When set, defines the day-interval on which to run the activity.
        /// </summary>
        public ScheduleType ScheduleType { get; set; }

        /// <summary>
        /// Gets the days of the month on which to run the activity when using a monthly schedule.
        /// </summary>
        [XmlIgnore]
        public IReadOnlyList<int> DaysOfMonth { get; private set; }

        /// <summary>
        /// The time to leave between repeating the activity.
        /// </summary>
        [XmlIgnore]
        public TimeSpan RepeatInterval { get; set; }

        /// <summary>
        /// When RepeatInterval is non-zero, the duration from StartTime during which the activity may be repeated.
        /// </summary>
        [XmlIgnore]
        public TimeSpan RepeatDuration { get; set; }

        /// <summary>
        /// For XML serialization.
        /// </summary>
        [XmlElement("StartTime")]
        public string StartTimeAsString
        {
            get { return StartTime.ToString("g"); }
            set { StartTime = TimeSpan.Parse(value); }
        }

        /// <summary>
        /// For XML serialization.
        /// </summary>
        [XmlElement("RepeatInterval")]
        public string RepeatIntervalAsString
        {
            get { return RepeatInterval.ToString("g"); }
            set { RepeatInterval = TimeSpan.Parse(value); }
        }

        /// <summary>
        /// For XML serialization.
        /// </summary>
        [XmlElement("RepeatDuration")]
        public string RepeatDurationAsString
        {
            get { return RepeatDuration.ToString("g"); }
            set { RepeatDuration = TimeSpan.Parse(value); }
        }

        /// <summary>
        /// For XML serialization. Gets or sets a comma-separated list of days of the month.
        /// </summary>
        [XmlElement("DaysOfMonth")]
        public string DaysOfMonthAsString
        {
            get { return string.Join(", ", DaysOfMonth); }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    DaysOfMonth = new List<int>();
                }
                else
                {
                    DaysOfMonth = value
                        .Split(',')
                        .Select(v => v.Trim())
                        .Select(v =>
                        {
                            var dayOfMonth = int.Parse(v);
                            if (dayOfMonth < 1 || dayOfMonth > 31)
                            {
                                throw new ArgumentOutOfRangeException("value",
                                    "DaysOfMonth configuration can only contain values in the range 1-31.");
                            }

                            return dayOfMonth;
                        })
                        .ToList();
                }
            }
        }

        /// <summary>
        /// Gets the time of the next scheduled event based on the supplied last event time.
        /// </summary>
        /// <param name="last">The last event time.</param>
        /// <returns>The next scheduled event time, or null if none is scheduled.</returns>
        public DateTime? GetNext(DateTime last)
        {
            if (ScheduleType == ScheduleType.Daily)
            {
                return GetNextInDailySchedule(last);
            }
            else
            {
                return GetNextInMonthlySchedule(last);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(ScheduleType);
            if (ScheduleType == ScheduleType.Monthly)
            {
                sb.AppendFormat(" on the {0} of each month", DaysOfMonthAsString);
            }

            sb.AppendFormat(" at {0} UTC", StartTimeAsString);

            if (RepeatInterval != TimeSpan.Zero && RepeatDuration != TimeSpan.Zero)
            {
                sb.AppendFormat(", repeating every {0} until {1} UTC", 
                    RepeatIntervalAsString, (StartTime + RepeatDuration).ToString("g"));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Gets the next execution time in a daily schedule that follows the supplied DateTime.
        /// </summary>
        /// <param name="last">The last execution date & time.</param>
        /// <returns></returns>
        private DateTime? GetNextInDailySchedule(DateTime last)
        {
            DateTime? candidate = null;
            int dayOffset = 0;

            while (!candidate.HasValue)
            {
                // Calculate the start of the schedule to test, including any day offset.
                var startOfDay = StartOfDay(last.AddDays(dayOffset));
                var scheduledStart = startOfDay.Add(StartTime);

                // Build up a list of possible start times based on the repetition values.
                List<DateTime> scheduledStarts = new List<DateTime> { scheduledStart };

                if (RepeatInterval != TimeSpan.Zero && RepeatDuration != TimeSpan.Zero)
                {
                    var limit = scheduledStart.Add(RepeatDuration);
                    DateTime next = scheduledStart;
                    while ((next = next.Add(RepeatInterval)) <= limit)
                    {
                        scheduledStarts.Add(next);
                    }
                }

                // Get the first candidate after the supplied last run DateTime, or null if none is after.
                candidate = FirstAfter(scheduledStarts, last);

                // If a candidate wasn't found, check the next day.
                if (!candidate.HasValue)
                {
                    dayOffset++;
                }
            }

            return candidate;
        }

        /// <summary>
        /// Gets the next execution time in a monthly schedule that follows the supplied DateTime.
        /// </summary>
        /// <param name="last">The last execution date & time.</param>
        /// <returns></returns>
        private DateTime? GetNextInMonthlySchedule(DateTime last)
        {
            DateTime? candidate = null;
            int dayOfMonthIndex = 0;
            int monthOffset = 0;

            while (!candidate.HasValue)
            {
                // Calculate the start of the schedule to test, including offsets.
                var startOfMonth = DayOfMonth(last.AddMonths(monthOffset), DaysOfMonth[dayOfMonthIndex]);
                var scheduledStart = startOfMonth.Add(StartTime);

                // Build up a list of possible start times based on the repetition values.
                List<DateTime> scheduledStarts = new List<DateTime> {scheduledStart};

                if (RepeatInterval != TimeSpan.Zero && RepeatDuration != TimeSpan.Zero)
                {
                    var limit = scheduledStart.Add(RepeatDuration);
                    DateTime next = scheduledStart;
                    while ((next = next.Add(RepeatInterval)) <= limit)
                    {
                        scheduledStarts.Add(next);
                    }
                }

                // Get the first candidate after the supplied last run DateTime, or null if none is after.
                candidate = FirstAfter(scheduledStarts, last);

                // If a candidate wasn't found, check the next step in the schedule.
                if (!candidate.HasValue)
                {
                    dayOfMonthIndex++;
                    if (dayOfMonthIndex >= DaysOfMonth.Count)
                    {
                        monthOffset++;
                        dayOfMonthIndex = 0;
                    }
                }
            }

            return candidate;
        }

        private static DateTime? FirstAfter(IEnumerable<DateTime> dateTimes, DateTime testDateTime)
        {
            return dateTimes.Select(dt => new DateTime?(dt)).FirstOrDefault(dt => dt > testDateTime);
        }

        private static DateTime StartOfDay(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 0, 0, 0, dateTime.Kind);
        }

        private static DateTime DayOfMonth(DateTime dateTime, int day)
        {
            return new DateTime(dateTime.Year, dateTime.Month, day, 0, 0, 0, dateTime.Kind);
        }
    }
}