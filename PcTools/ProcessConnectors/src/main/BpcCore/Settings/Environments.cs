﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Ntg.BpcCore.Settings
{
    /// <summary>
    /// Load and persist MDCE environments.
    /// </summary>
    [Serializable]
    public class Environments : IEnvironmentsConfig
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// MDCE environment specification
        /// </summary>
        public List<MdceEnvironment> EnvironmentList { get; set; }

        /// <summary>
        /// SalesLogix MS SQL server name
        /// </summary>
        public string CustomerDataServer { get; set; }

        public string CostAndRevenueServerDataServer { get; set; }

        /// <summary>
        /// The name of the MDCE environment that will be synchronized with QAD.
        /// </summary>
        public string PrimaryMdceEnvironmentName { get; set; }

        /// <summary>
        /// Service schedule settings.
        /// </summary>
        public List<ScheduleConfiguration> ScheduleConfigurations { get; set; }

        public Environments()
        {
            this.EnvironmentList = new List<MdceEnvironment>();
            this.ScheduleConfigurations = new List<ScheduleConfiguration>();
        }

        /// <summary>
        /// Deserialize MDCE environment variables from XML file.
        /// </summary>
        /// <param name="environmentSettingsFilename">The path to the environment settings file.</param>
        /// <param name="excludedEnvironments">A comma-separated list of environments to exclude by environment name.</param>
        /// <returns>
        /// The deserialized environments configuration that excludes any environments named 
        /// in the <code>excludedEnvironments</code> parameter.
        /// </returns>
        public static IEnvironmentsConfig ReadFromXml(
            string environmentSettingsFilename, string excludedEnvironments = null)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Environments));

            IEnvironmentsConfig env = null;

            Log.Debug(10016, "Loading config file: " + environmentSettingsFilename);

            using (StreamReader reader = new StreamReader(environmentSettingsFilename))
            {
                env = serializer.Deserialize(reader.BaseStream) as Environments;
            }

            // Apply exclusions
            if (env != null && !string.IsNullOrEmpty(excludedEnvironments))
            {
                var exclusionList = excludedEnvironments.Split(',').Select(e => e.Trim());
                foreach (var exclusion in exclusionList)
                {
                    env.EnvironmentList = env.EnvironmentList
                        .Where(e => !e.Name.Equals(exclusion, StringComparison.OrdinalIgnoreCase))
                        .ToList();
                }
            }

            return env;
        }

        /// <summary>
        /// Persist MDCE environment to XML file.
        /// </summary>
        public void WriteToXml(string environmentSettingsFilename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Environments));
            using (StreamWriter writer = new StreamWriter(environmentSettingsFilename))
            {
                serializer.Serialize(writer.BaseStream, this);
            }
        }
    }
}
