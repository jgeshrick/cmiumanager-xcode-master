//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;

namespace Ntg.BpcCore.Settings
{
    /// <summary>
    /// Contains details of a change to the Partner String for a single MDCE environment.
    /// </summary>
    public class ParnerStringChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the new partner string.
        /// </summary>
        public string NewPartnerString { get; private set; }

        public ParnerStringChangedEventArgs(string newPartnerString)
        {
            NewPartnerString = newPartnerString;
        }
    }
}