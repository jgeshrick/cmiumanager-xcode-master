﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

namespace Ntg.BpcCore.Settings
{
    /// <summary>
    /// Provides methods for storage and retrieval of partner strings.
    /// </summary>
    public interface IEnvironmentSettingsRepository
    {
        /// <summary>
        /// Reads the BPC Partner String from the encrypted registry setting for the provided environment name.
        /// </summary>
        /// <param name="environmentName">The environment name for which the partner string is being read.</param>
        /// <returns>
        /// The decrypted Partner String, or null if the value is not present or cannot be decrypted.
        /// </returns>
        string GetBpcPartnerString(string environmentName);

        /// <summary>
        /// Returns a value indicating whether a Partner String for the current environment 
        /// could be read from configuration.
        /// </summary>
        /// <param name="environmentName">The environment name for which the partner string is being read.</param>
        /// <returns>
        /// True if a Partner String was found; otherwise, false.
        /// </returns>
        bool HasBpcPartnerString(string environmentName);

        /// <summary>
        /// Writes the supplied BPC Partner String to an encrypted registry setting for the provided environment name.
        /// </summary>
        /// <param name="partnerString">The Partner String to be encrypted and written to the registry.</param>
        /// <param name="environmentName">The environment name for which the partner string is being written.</param>
        void SetBpcPartnerString(string partnerString, string environmentName);
    }
}