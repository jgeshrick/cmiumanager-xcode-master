﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

namespace Ntg.BpcCore.Settings
{
    public static class Constants
    {
        /// <summary>
        /// The Site ID for use by BPC - always 0
        /// </summary>
        public const int BpcSiteId = 0;
    }
}
