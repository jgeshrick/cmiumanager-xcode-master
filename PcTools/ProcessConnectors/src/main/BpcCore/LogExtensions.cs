﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using log4net;

namespace Ntg.BpcCore
{
    /// <summary>
    /// Provides log4net method wrappers that include an event ID parameter.
    /// </summary>
    public static class LogExtensions
    {
        /// <summary>
        /// Log a message object with the log4net.Core.Level.Debug level.
        /// </summary>
        public static void Debug(this ILog log, int eventId, string message)
        {
            ThreadContext.Properties["EventId"] = eventId;
            log.Debug(message);
            ThreadContext.Properties["EventId"] = 0;
        }

        /// <summary>
        /// Log a message object with the log4net.Core.Level.Info level.
        /// </summary>
        public static void Info(this ILog log, int eventId, string message)
        {
            ThreadContext.Properties["EventId"] = eventId;
            log.Info(message);
            ThreadContext.Properties["EventId"] = 0;
        }

        /// <summary>
        /// Log a message object with the log4net.Core.Level.Warn level.
        /// </summary>
        public static void Warn(this ILog log, int eventId, string message)
        {
            ThreadContext.Properties["EventId"] = eventId;
            log.Warn(message);
            ThreadContext.Properties["EventId"] = 0;
        }

        /// <summary>
        /// Log a message object with the log4net.Core.Level.Error level.
        /// </summary>
        public static void Error(this ILog log, int eventId, string message)
        {
            ThreadContext.Properties["EventId"] = eventId;
            log.Error(message);
            ThreadContext.Properties["EventId"] = 0;
        }

        /// <summary>
        /// Log a message object with the log4net.Core.Level.Error level including the stack trace of the System.Exception passed as a parameter.
        /// </summary>
        public static void Error(this ILog log, int eventId, string message, System.Exception exception)
        {
            ThreadContext.Properties["EventId"] = eventId;
            log.Error(message, exception);
            ThreadContext.Properties["EventId"] = 0;
        }

        /// <summary>
        /// Log a message object with the log4net.Core.Level.Fatal level.
        /// </summary>
        public static void Fatal(this ILog log, int eventId, string message)
        {
            ThreadContext.Properties["EventId"] = eventId;
            log.Fatal(message);
            ThreadContext.Properties["EventId"] = 0;
        }

        /// <summary>
        /// Log a message object with the log4net.Core.Level.Fatal level including the stack trace of the System.Exception passed as a parameter.
        /// </summary>
        public static void Fatal(this ILog log, int eventId, string message, System.Exception exception)
        {
            ThreadContext.Properties["EventId"] = eventId;
            log.Fatal(message, exception);
            ThreadContext.Properties["EventId"] = 0;
        }
    }
}