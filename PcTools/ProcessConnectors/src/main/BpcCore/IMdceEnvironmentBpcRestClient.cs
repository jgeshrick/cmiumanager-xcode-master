//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NeptuneCloudConnector.JsonModel;
using Ntg.BpcCore.JsonModel;
using Ntg.BpcCore.JsonModel.ConfigHistory;
using Ntg.BpcCore.JsonModel.CrmReference;
using Ntg.BpcCore.JsonModel.LifecycleHistory;
using Ntg.BpcCore.JsonModel.MiuConfig;
using Ntg.BpcCore.JsonModel.OwnerHistory;
using Ntg.BpcCore.ServiceProxies;
using Ntg.BpcCore.Settings;

namespace Ntg.BpcCore
{
    public interface IMdceEnvironmentBpcRestClient
    {
        MdceEnvironment Environment { get; }
        string AuthenticationToken { get; }
        string PartnerString { get; }
        string SiteId { get; }

        /// <summary>
        /// Retrieve sequential MIU configuration history using IF18v2.
        /// </summary>
        Task<List<SequentialMiuConfig>> GetConfigurationHistoryAsync(int? lastSequenceId);

        /// <summary>
        /// Retrieve sequential MIU owner history using IF48.
        /// </summary>
        Task<SequentialMiuOwnerCollection> GetMiuOwnerHistoryAsync(int? lastSequenceId);

        /// <summary>
        /// Retrieve configuration history about an MIU or list of MIU�s
        /// Get Configuration History (IF18)
        /// </summary>
        Task<List<SequentialMiuLifecycleState>> GetLifecycleHistoryAsync(int? lastSequenceId);

        /// <summary>
        /// Set reference data about utilities and distributers using HTTP PUT.
        /// Get CRM Reference Data (IF42)
        /// </summary>
        Boolean SetCrmReferenceData(CrmReference referenceData);

        /// <summary>
        /// Append reference data about utilities and distributers using HTTP POST.
        /// Get CRM Reference Data (IF42)
        /// </summary>
        Boolean AppendCrmReferenceData(CrmReference referenceData);

        /// <summary>
        /// Gets all Customers from MDCE.
        /// </summary>
        /// <returns></returns>
        CrmReference GetCrmReferenceData();

        /// <summary>
        /// Get a Single MIU config from mdce integration server
        /// </summary>
        /// <param name="miuId">miu id</param>
        /// <returns></returns>
        MiuConfig GetSingleMiuConfig(int miuId);

        /// <summary>
        /// This will return all MIU�s for the token�s site id.  If the tokens site_id is 0, then all MIU�s in the MDCE will be returned.
        /// </summary>
        /// <returns></returns>
        MiuConfigList GetMultipleMiuConfig();

        /// <summary>
        /// Set Miu config.
        /// </summary>
        /// <param name="miuId">id of the miu</param>
        /// <param name="jsonMiuConfig">new miu config</param>
        /// <returns>A value indicating whether the operation was successful.</returns>
        Boolean SetMiuConfig(int miuId, MiuConfig jsonMiuConfig);

        /// <summary>
        /// Attempt to recycle the token if it has not expire
        /// </summary>
        /// <returns>An instance of <see cref="Token"/> containing authorization details.</returns>
        Token GetAuthenticationToken();

        /// <summary>
        /// Gets a list of MIUs that match the supplied search criteria.
        /// </summary>
        /// <param name="findId">The MIU ID, ICCID, IMEI or EUI of the device</param>
        /// <returns>The collection of MIU details matching the query.</returns>
        Task<MiuListCollection> GetMiuListAsync(string findId);

        /// <summary>
        /// Gets a list of MIUs that match the supplied search criteria.
        /// </summary>
        /// <param name="findId">The MIU ID, ICCID, IMEI or EUI of the device</param>
        /// <param name="idType">The device ID type on which to search.</param>
        /// <returns>The collection of MIU details matching the query.</returns>
        Task<MiuListCollection> GetMiuListAsync(string findId, DeviceIdType idType);

        /// <summary>
        /// Gets a list of all MIUs.
        /// </summary>
        /// <param name="responseType">Sets whether cellular/LoRa details should be included in the response.</param>
        /// <returns>The collection of MIU details matching the query.</returns>
        Task<MiuListCollection> GetMiuListAsync(MiuListResponseType responseType);

        /// <summary>
        /// Gets a list of all MIUs.
        /// </summary>
        /// <returns>The collection of MIU details matching the query.</returns>
        Task<MiuListCollection> GetMiuListAsync();

        /// <summary>
        /// Request a session authentication token from mdce-integration
        /// Receive a partner key, validates it, and generate a token that is good for a configurable length of time.
        /// Get Token (Authenticate) (IF03)
        /// </summary>
        Task<Token> GetAuthenticationTokenAsync();

        /// <summary>
        /// Adds MIU lifecycle history to the MDCE history.
        /// </summary>
        /// <param name="lifecycleHistoryRequest">Details of the MIU lifecycle events to be added.</param>
        /// <returns>The completed status of the request, including details of any failures.</returns>
        Task<SetMiuLifecycleHistoryResult> SetMiuLifecycleHistoryAsync(SetMiuLifecycleHistoryRequest lifecycleHistoryRequest);
    }
}