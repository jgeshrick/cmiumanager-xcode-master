﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Diagnostics;
using FpcClient;
using FpcClient.Settings;
using FpcClient.JsonModel;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.JsonModel.LoraConfig;
using FpcClient.JsonModel.LoraAppKey;
using FpcClient.JsonModel.ApnInformation;
using FpcClient.Common;
using FpcClient.Common.StatusCodes;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.Common.LifecycleStates;
using FpcClient.Common.ConfigurationStructures;
using FpcClient.Common.CmiuLifecycleStructures;
using FpcClient.Common.CmiuCanConfigurationStructures;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.ApnInformationStructures;
using FpcClient.Common.SetCmiuCanResponse;
using FpcClient.Common.SetL900CanResponse;
using FpcClient.Common.L900LoRaDetailsStructures;

namespace FpcClientLibrary
{
    /// <summary>
    /// Concrete implementation of FPC service
    /// </summary>
    public class FpcClient : _IFpcClient
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        internal const UInt16 MinErrorCode = 100;

        #region Client Properties
        /// <summary>
        /// Instance of FpcRestClient
        /// </summary>
        private FpcRestClient fpcRestClient;

        /// <summary>
        /// Copy of Mdce Environment settings read from file
        /// </summary>
        private MdceEnvironment environment;

        /// <summary>
        /// PartnerId passed in from caller
        /// </summary>
        private string partnerId;

        /// <summary>
        /// Environment (URL) defined for the FPC Server
        /// </summary>
        public string Environment
        {
            get
            {
                return environment.Name;
            }
        }

        /// <summary>
        /// The minimum value of an error status code.
        /// Status codes of this value or higher are considered to be errors.
        /// Zero is always OK.
        /// Values between these two are considered to be warnings.
        /// </summary>
        public UInt16 MinimumErrorCode
        {
            get
            {
                return MinErrorCode;
            }
        }

        /// <summary>
        /// The FPC Client assembly version.
        /// </summary>
        public string AssemblyVersion
        {
            get
            {
                return GetClientAssemblyVersion();
            }
        }
        #endregion //Client Properties

        #region Client Constructor
        /// <summary>
        /// FpcClient constructor
        /// </summary>
        public FpcClient()
        {
            log.Debug("Empty Fpc Client constructed");
        }
        #endregion //Client Constructor

        #region Client Initialisation Functions
        /// <summary>
        /// This function is required to enable caller to pass the partner key to the dll
        /// so that it can authenticate with FPC server
        /// </summary>
        /// <param name="partnerId">PartnerId string</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode InitFpcClient(string partnerId)
        {
            StatusCode statusCode = StatusCode.ErrorUnspecified;
            try
            {
                statusCode = InitPartnerId(partnerId);
                if (StatusCode.OK == statusCode)
                {
                    statusCode = ParseConfigurationFile();
                    if (StatusCode.OK == statusCode)
                    {
                        statusCode = InitialiseClient(partnerId);
                        if (StatusCode.OK == statusCode)
                        {
                            log.DebugFormat("Empty FPC Service constructed.\\nServer Name {0}",this.Environment);
                        }
                    }
                }
            }
            catch (Exception)
            {
                log.Debug("InitFpcClient Exception Status Code - ErrorServiceInit");

                //Swallow exception and return valid status code
                //This is not the OO way of doing things but it does mean we can return a sane value to LabView
                return StatusCode.ErrorServiceInit;
            }
            log.DebugFormat("InitFpcClient Status Code - {0}", statusCode.ToString());
            return statusCode;
        }

        /// <summary>
        /// Set the partner id of the FPC client
        /// </summary>
        /// <param name="partnerId">PartnerId string</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        private StatusCode InitPartnerId(string partnerId)
        {
            try
            {
                this.partnerId = partnerId;
            }
            catch (Exception)
            {
                //Swallow exception and return valid status code
                //This is not the OO way of doing things but it does mean we can return a sane value to LabView
                return StatusCode.ErrorPartnerId;
            }
            return StatusCode.OK;
        }

        /// <summary>
        /// Internal function used to read (and store locally) the environment configuration from a configuration file.
        /// </summary>
        /// <returns>StatusCode indicating success or type of failure</returns>
        private StatusCode ParseConfigurationFile()
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;

            try
            {
                var mdceEnvironments = MdceEnvironments.ReadFromXml();
                if ((null != mdceEnvironments.Environment) && (null != mdceEnvironments.Environment.Name))
                {
                    this.environment = mdceEnvironments.Environment;
                    returnCode = StatusCode.OK;
                }
                else
                {
                    returnCode = StatusCode.ErrorConfigFile;
                }
            }
            catch (Exception)
            {
                //Swallow exception and return valid status code
                //This is not the OO way of doing things but it does mean we can return a sane value to LabView
                returnCode = StatusCode.ErrorConfigFile;
            }
            log.DebugFormat("ParseConfigurationFile - {0}", returnCode);
            return returnCode;
        }

        /// <summary>
        /// Setup FPC client REST connection to FPC server
        /// </summary>
        /// <param name="partnerId">PartnerId string</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        private StatusCode InitialiseClient(string partnerId)
        {
            try
            {
                this.fpcRestClient = new FpcRestClient(
                    partnerId,
                    "0",
                    this.environment.Name,
                    "");
            }
            catch (Exception)
            {
                //Swallow exception and return valid status code
                //This is not the OO way of doing things but it does mean we can return a sane value to LabView
                return StatusCode.ErrorClientInit;
            }
            return StatusCode.OK;
        }
        #endregion //Client Initialisation Functions

        #region Client Configuration Functions
        /// <summary>
        /// Checks to see whether the CMIU specified is ready for network activity.
        /// </summary>
        /// <param name="cmiuId">Id of cmiu</param>
        /// <returns>StatusCode indicating if CMUI is active or a relevant failure code</returns>
        public StatusCode IsCmiuActivated(uint cmiuId)
        {
            //TODO: Function not yet implemented.
            return StatusCode.ErrorNotImplemented;
        }

        /// <summary>
        /// Provide caller with a means to register CMIU state with the FPC server.
        /// Set the life-cycle state to PrePrePot prior to commencing the pre-pot test.
        /// @duration 371-1748ms        
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to register</param>
        /// <returns>StatusCode to reflect result of function call</returns>
        public StatusCode RegisterCmiuBeforePrepotTest(uint cmiuId)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;

            if (null == this.fpcRestClient)
            {
                returnCode = StatusCode.ErrorApiNotInitialised;
            }
            else if (true != Utilities.ValidateCmiuId(cmiuId))
            {
                returnCode = StatusCode.ErrorInvalidCmiuId;
            }
            else
            {
                var newLifecycleState = new CmiuLifecycleState() { State = new State() };
                newLifecycleState.MiuId = cmiuId;
                newLifecycleState.State.Id = LifecycleState.PrePrePot.ToString();
                newLifecycleState.State.TimeStamp = DateTime.UtcNow;
                returnCode = this.fpcRestClient.SetCmiuLifecycleState(newLifecycleState);
            }
            return returnCode;
        }

        /// <summary>
        /// Provide caller with a means to register CMIU state with the FPC server.
        /// Set the life-cycle state to PrePot after a successful pre-pot test.
        /// @duration 368-1730ms        
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to register</param>
        /// <returns>StatusCode to reflect result of function call</returns>
        public StatusCode RegisterCmiuAfterSuccessfulPrepotTest(uint cmiuId)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;

            if (null == this.fpcRestClient)
            {
                returnCode = StatusCode.ErrorApiNotInitialised;
            }
            else if (true != Utilities.ValidateCmiuId(cmiuId))
            {
                returnCode = StatusCode.ErrorInvalidCmiuId;
            }
            else
            {
                var newLifecycleState = new CmiuLifecycleState() { State = new State() };
                newLifecycleState.MiuId = cmiuId;
                newLifecycleState.State.Id = LifecycleState.PrePot.ToString();
                newLifecycleState.State.TimeStamp = DateTime.UtcNow;
                returnCode = this.fpcRestClient.SetCmiuLifecycleState(newLifecycleState);
            }
            return returnCode;
        }

        /// <summary>
        /// Provide caller with a means to register CMIU state with the FPC server.
        /// Set the life-cycle state to PostPot after a successful post-pot test.
        /// @duration 372-897ms        
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to register</param>
        /// <returns>StatusCode to reflect result of function call</returns>
        public StatusCode RegisterCmiuAfterSuccessfulPostpotTest(uint cmiuId)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;

            if (null == this.fpcRestClient)
            {
                returnCode = StatusCode.ErrorApiNotInitialised;
            }
            else if (true != Utilities.ValidateCmiuId(cmiuId))
            {
                returnCode = StatusCode.ErrorInvalidCmiuId;
            }
            else
            {
                var newLifecycleState = new CmiuLifecycleState() { State = new State() };
                newLifecycleState.MiuId = cmiuId;
                newLifecycleState.State.Id = LifecycleState.PostPot.ToString();
                newLifecycleState.State.TimeStamp = DateTime.UtcNow;
                returnCode = this.fpcRestClient.SetCmiuLifecycleState(newLifecycleState);
            }
            return returnCode;
        }

        /// <summary>
        /// Provide caller with a means to check the CMIU state with the FPC server.
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to check</param>
        /// <param name="cmiuLifecycleState">CmiuLifecycleState object to populate</param>
        /// <returns>StatusCode to reflect result of function call</returns>
        public StatusCode GetCmiuLifecycle(uint cmiuId, ref CmiuLifecycleStruct cmiuLifecycleState)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;

            if (null == this.fpcRestClient)
            {
                returnCode = StatusCode.ErrorApiNotInitialised;
            }
            else if (true != Utilities.ValidateCmiuId(cmiuId))
            {
                returnCode = StatusCode.ErrorInvalidCmiuId;
            }
            else
            {
                try
                {
                    CmiuLifecycleState cmiuLifecycle = new CmiuLifecycleState();
                    returnCode = this.fpcRestClient.GetCmiuLifecycleState(cmiuId, ref cmiuLifecycle);
                    if (StatusCode.OK == returnCode)
                    {
                        cmiuLifecycleState.miuId = cmiuLifecycle.MiuId;
                        cmiuLifecycleState.state.id = cmiuLifecycle.State.Id;
                        cmiuLifecycleState.state.timestamp = cmiuLifecycle.State.TimeStamp;
                    }

                }
                catch (Exception)
                {
                    returnCode = StatusCode.ErrorApiNotInitialised;
                    throw;
                }
            }
            return returnCode;
        }

        /// <summary>
        /// Provide caller with a means to get the status message behind a status code.
        /// </summary>
        /// <param name="statusCode">Code to parse into string</param>
        /// <returns>String containing status message</returns>
        public string GetStatusCodeString(StatusCode statusCode)
        {
            log.DebugFormat("GetStatusCodeString = {0}", statusCode.ToString());
            var returnString = statusCode.ToString();
            return returnString;
        }

        /// <summary>
        /// Provide caller with a means to get the APN network identifier string for a specific MNO.
        /// This should be called before every call to SetCmiuCan() because
        /// CMIUs on the same network can have different APNs.
        /// @duration 168-897ms        
        /// </summary>
        /// <param name="apnInfo">ApnInformation object to parse and populate</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode GetApnString(ref ApnInfo apnInfo)
        {
            var apnInformation = new ApnInformation();
            StatusCode returnCode = StatusCode.ErrorUnspecified;

            if (null == this.fpcRestClient)
            {
                returnCode = StatusCode.ErrorApiNotInitialised;
            }
            else
            {
                returnCode = Utilities.ValidateMno(apnInfo.mno);
                if (StatusCode.OK == returnCode)
                {
                    apnInformation.Mno = apnInfo.mno.ToString();
                    returnCode = this.fpcRestClient.GetApnInformation(ref apnInformation);
                }
                if (StatusCode.OK == returnCode)
                {
                    if (null != apnInformation.Mno)
                    {
                        returnCode = Utilities.ParseCellularProviderCode(apnInformation.Mno, ref apnInfo.mno);
                    }
                    if (null != apnInformation.Apn)
                    {
                        apnInfo.apn = apnInformation.Apn;
                    }
                }
            }
            return returnCode;
        }

        /// <summary>
        /// Provide caller with a means to get the FPC client assembly version
        /// </summary>
        /// <returns>String containing assembly version</returns>
        private string GetClientAssemblyVersion()
        {
            string assemblyFileLocation = System.Reflection.Assembly.GetExecutingAssembly().Location;
            FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(assemblyFileLocation);

            log.DebugFormat("FPC Client ({0}) Version = {1}",
                            assemblyFileLocation,
                            myFileVersionInfo.FileVersion);

            return myFileVersionInfo.FileVersion;
        }
        #endregion //Client Configuration Functions

        #region Client REST Functions
        #region Set Manufacturing Information
        /// <summary>
        /// Allow FPC client to set manufacturing information of a CMIU
        /// </summary>
        /// <param name="manufacturingInformation">ManufacturingInformationStruct object containing data to set</param>
        /// <returns>StatusCode indicating success or otherwise</returns>
        public StatusCode SetMfgInfo(ManufacturingInformationStruct manufacturingInformation)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;

            if (null == this.fpcRestClient)
            {
                returnCode = StatusCode.ErrorApiNotInitialised;
            }
            else if (true != Utilities.ValidateCmiuId(manufacturingInformation.miuId))
            {
                returnCode = StatusCode.ErrorInvalidCmiuId;
            }
            else
            {
                var manuInfo = new ManufacturingInformation();
                if ((null != manufacturingInformation.deviceDetails) && (null != manufacturingInformation.deviceDetails.activationTimestamp))
                {
                    manuInfo.DeviceDetails.ActivationTimestamp = manufacturingInformation.deviceDetails.activationTimestamp;
                }
                if (null != manufacturingInformation.miuId)
                {
                    manuInfo.MiuId = manufacturingInformation.miuId;
                }

                returnCode = Utilities.ValidateManufacturingInformation(manuInfo);
                if (StatusCode.OK == returnCode)
                {
                    returnCode = this.fpcRestClient.SetManufacturingInformation(manuInfo);
                }
            }
            return returnCode;
        }
        #endregion //Set Manufacturing Information

        #region GET/SET Cellular Config
        /// <summary>
        /// Set CAN details of a CMIU.
        /// @duration 350-670ms        
        /// </summary>
        /// <param name="cmiuCan">CmiuCan object containing data to set</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public SetCmiuCanResponse SetCmiuCan(CmiuCan cmiuCan)
        {
            log.DebugFormat("cmiuCancmiuCan = {0}", cmiuCan.ToString());
            StatusCode returnCode = StatusCode.ErrorUnspecified;
            SetCmiuCanResponse response = new SetCmiuCanResponse();

            if (null == this.fpcRestClient)
            {
                returnCode = StatusCode.ErrorApiNotInitialised;
                response.statusCode = returnCode;
            }
            else if (true != Utilities.ValidateCmiuId(cmiuCan.cmiuId))
            {
                returnCode = StatusCode.ErrorInvalidCmiuId;
                response.statusCode = returnCode;
            }
            else
            {
                CellularConfig cellConfig = new CellularConfig() { ModemConfig = new ModemConfig(),
                                                                   SimConfig = new SimConfig()};
                if (null != cmiuCan.modemCan.vendor)
                {
                    cellConfig.ModemConfig.Vendor = cmiuCan.modemCan.vendor;
                }
                if (null != cmiuCan.modemCan.imei)
                {
                    cellConfig.ModemConfig.Imei = cmiuCan.modemCan.imei.ToString();
                }
                if (null != cmiuCan.modemCan.mno)
                {
                    cellConfig.ModemConfig.Mno = cmiuCan.modemCan.mno.ToString();
                }
                if (null != cmiuCan.simCan.iccid)
                {
                    cellConfig.SimConfig.Iccid = cmiuCan.simCan.iccid.ToString();
                }
                if (null != cmiuCan.cmiuId)
                {
                    cellConfig.MiuId = cmiuCan.cmiuId;
                }
                if (null != cmiuCan.apn)
                {
                    cellConfig.Apn = cmiuCan.apn;
                }

                response.statusCode = Utilities.RangeTestCellularConfig(cellConfig);
                if (StatusCode.OK == response.statusCode)
                {
                    response = this.fpcRestClient.SetCellularConfig(cellConfig);
                }
            }


            return response;
        }

        /// <summary>
        /// Allow FPC client to retrieve details of a CMIU.
        /// @duration 200ms
        /// </summary>
        /// <param name="cmiuId">Id of CMIU</param>
        /// <param name="cmiuCan">CmiuCan object to populate</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode GetCmiuCan(uint cmiuId, ref CmiuCan cmiuCan)
        {
            var cellularConfig = new CellularConfig();
            StatusCode returnCode = StatusCode.ErrorUnspecified;

            if (null == this.fpcRestClient)
            {
                returnCode = StatusCode.ErrorApiNotInitialised;
            }
            else if (true != Utilities.ValidateCmiuId(cmiuId))
            {
                returnCode = StatusCode.ErrorInvalidCmiuId;
            }
            else
            {
                returnCode = this.fpcRestClient.GetCellularConfig(cmiuId, ref cellularConfig);
                if (StatusCode.OK == returnCode)
                {
                    returnCode = Utilities.RangeTestCellularConfig(cellularConfig);
                }
                if (StatusCode.OK == returnCode)
                {
                    cmiuCan.cmiuId = (uint)cellularConfig.MiuId;
                    if (null != cellularConfig.Apn)
                    {
                        cmiuCan.apn = cellularConfig.Apn;
                    }
                    if (null != cellularConfig.ModemConfig.Vendor)
                    {
                        cmiuCan.modemCan.vendor = cellularConfig.ModemConfig.Vendor;
                    }
                    if (null != cellularConfig.ModemConfig.Mno)
                    {
                        returnCode = Utilities.ParseCellularProviderCode(cellularConfig.ModemConfig.Mno, ref cmiuCan.modemCan.mno);
                    }
                    if (null != cellularConfig.ModemConfig.Imei)
                    {
                        cmiuCan.modemCan.imei = cellularConfig.ModemConfig.Imei;
                    }
                    if (null != cellularConfig.SimConfig.Iccid)
                    {
                        cmiuCan.simCan.iccid = cellularConfig.SimConfig.Iccid;
                    }
                }
            }
            return returnCode;
        }
        #endregion //Get/Set Cellular Config

        #region SET L900 CAN/KEY

        /// <summary>
        /// SetL900Can - Sets the LoRaWAN details for a L900
        /// </summary>
        /// <returns>Status</returns>
        public SetL900CanResponse SetL900Can(L900Can l900Can)
        {
            LoraConfig loraConfig = new LoraConfig();

            loraConfig.MiuId = l900Can.miuId;
            loraConfig.DevEui = l900Can.devEui;
            loraConfig.AppEui = l900Can.appEui;

            return this.fpcRestClient.SetL900Config(loraConfig);
        }

        /// <summary>
        /// SetL900AppKey - Stores the L900 App Key in the database
        /// </summary>
        /// <returns>Status</returns>
        public StatusCode SetL900AppKey(L900KeyDetails l900KeyDetails)
        {
            LoraAppKey loraAppKey = new LoraAppKey();

            loraAppKey.loraDeviceDetailsId = l900KeyDetails.loraDeviceDetailsId;
            loraAppKey.AppKey = l900KeyDetails.appKey;

            return this.fpcRestClient.SetL900AppKey(loraAppKey);
        }

        #endregion //Set L900 CAN/KEY

        #region GET Server Detail
        /// <summary>
        /// GetServerDetail
        /// </summary>
        /// <returns>ServerDetails</returns>
        public StatusCode GetServerDetails(ref ServerDetails serverDetails)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;

            ServerInformation serverInformation = new ServerInformation();
            returnCode = this.fpcRestClient.GetFPCServerDetail(ref serverInformation);
            if (StatusCode.OK == returnCode)
            {
                serverDetails.version = serverInformation.Version;
                serverDetails.environment = serverInformation.Environment;
                serverDetails.server = serverInformation.BaseUrl;
                serverDetails.currentTimeUtc = serverInformation.CurrentTimeUtc.ToString("U");
            }
            return returnCode;
        }
        #endregion //GET Server Detail
        #endregion //Client REST Functions
    }
}
