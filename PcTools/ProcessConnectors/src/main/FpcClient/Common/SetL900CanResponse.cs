﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using System.Collections.Generic;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;

namespace FpcClient.Common.SetL900CanResponse
{
    public struct SetL900CanResponse
    {
        public StatusCode statusCode;

        public long appKeyIndex;
    }
}

