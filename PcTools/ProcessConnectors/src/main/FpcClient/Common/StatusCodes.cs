﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;

namespace FpcClient.Common.StatusCodes
{
    /// <summary>
    /// Status codes used by almost all API functions should use this as a return code to
    /// provide helpful diagnostics information if a command doesn't run to plan.
    /// </summary>
    public enum StatusCode
    {
        /// Status OK
        OK,
        
        //WARNINGS
        ///  The record was updated and any previous values may have been lost 
        WarningUpdatedRecord,

        /// Response from stubbed functions (not strictly an error)
        WarningStubNotImplemented,

        //ERRORS
        //This MUST always be first item in list of errors as it sets the boundary between WARNINGS and ERRORS
        /// Not Implemented
        ErrorNotImplemented = FpcClientLibrary.FpcClient.MinErrorCode,
        
        /// One or more initialisation steps are required before the main API functions can be called
        ErrorApiNotInitialised,
        /// Log file not initialised
        ErrorLogFileNotInitialised,

        /// Unknown server error
        ErrorServerUnknown,
        /// Server not available
        ErrorServerUnavailable,
        
        /// Web service request failed for unspecified reason
        ErrorServerResponseUnknown,
        /// Web service request was completed and response body is empty
        ErrorServerResponseNoContent,
        /// Web service request was bad
        ErrorServerResponseBadRequest,
        /// Either the token is not recognized, wasn’t issued for the same IP, or has expired.
        ErrorServerResponseUnauthorized,
        /// The entity identified by the URL does not exist
        ErrorServerResponseNotFound,

        /// The format of the URI could not be determined
        ErrorInvalidUri,

        /// Timed out waiting for response from server
        ErrorServerTimeout,

        /// Error setting partner Id
        ErrorPartnerId,
        /// Error reading configuration file
        ErrorConfigFile,
        /// Error initialising client
        ErrorClientInit,
        /// Error initialising service
        ErrorServiceInit,
        /// Error registering CMIU state
        ErrorRegisterState,
        /// Status NOT OK
        ErrorUnspecified,

        /// Invalid CMIU Id
        ErrorInvalidCmiuId,
        /// Invalid provider
        ErrorInvalidProvider,
        /// Invalid IMEI
        ErrorInvalidImei,
        /// Invalid ICCID
        ErrorInvalidIccid,

        /// Invalid L900 ID
        ErrorInvalidL900Id,
        /// Invalid Dev EUI
        ErrorInvalidDevEui,
        /// Invalid App EUI
        ErrorInvalidAppEui,

        /// ICCID is already used
        ErrorDuplicateIccid,
        /// IMEI is already used
        ErrorDuplicateImei,
        /// Something other than ICCID or IMEI is already used
        ErrorDuplicateData,

        /// Invalid CMIU lifecycle state
        ErrorInvalidLifecycleState,

        /// Invalid MNO request value
        ErrorInvalidMnoRequest,

        /// Issue with Rest Client Post method
        ErrorRestDoPost,
    }
}
