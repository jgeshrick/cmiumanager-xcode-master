﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using System.Collections.Generic;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;

namespace FpcClient.Common.ApnInformationStructures
{
    /// <summary>
    /// APN information
    /// This is made mutable to enable LabView to create and complete the data
    /// </summary>
    public struct ApnInfo
    {
        /// <summary>
        /// APN string
        /// The Access Point Name
        /// </summary>
        public string apn;
        /// <summary>
        /// Cellular provider
        /// </summary>
        public CellularProviderCode mno;
    }
}

