﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using System.Collections.Generic;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;

namespace FpcClient.Common.ConfigurationStructures
{
    /// <summary>
    /// Information about the test
    /// </summary>
    public struct ManufacturingInformationStruct
    {
        /// <summary>
        /// string miuId
        /// </summary>
        public uint miuId;

        public List<TestStationStruct> tests;

        public ReworkStruct rework;

        public DeviceDetailsStruct deviceDetails;
    }

    /// <summary>
    /// Subset of information about the test station
    /// </summary>
    public struct StationInfoStruct
    {
        public string type;

        public string bay;

        public string rackId;

        public string fixtureId;

        public DateTimeOffset calibrationDate;

        public string labviewApplicationVersion;

        public string labviewPlatformVersion;

        public string nccVersion;

        public string osDescription;

        public string btleDongleDriverVersion;
    }

    /// <summary>
    /// Subset of information about the test station
    /// </summary>
    public struct TestStationStruct
    {
        public DateTimeOffset testTimestamp;

        public StationInfoStruct stationInfo;

        public List<string> log;
    }

    /// <summary>
    /// Information about rework of this device
    /// </summary>
    public struct ReworkStruct
    {
        public int count;
    }

    /// <summary>
    /// Information about the images on the device
    /// </summary>
    public struct ImagesStruct
    {
        public string modemLibrary;

        public string cmiuApplication;

        public string cmiuBootloader;

        public string cmiuConfiguration;
    }

    /// <summary>
    /// Information about the cellular  modem (note: other data sent via IF40)
    /// </summary>
    public class ModemStruct
    {
        public string firmwareRevision;
    }

    /// <summary>
    /// Information about Bluetooth chip
    /// </summary>
    public class BluetoothStruct
    {
        public string macAddress;
    }

    /// <summary>
    /// Information about the CMIU device
    /// </summary>
    public class DeviceDetailsStruct
    {
        public DateTimeOffset creationTimestamp;

        public DateTimeOffset firstTestTimestamp;

        public DateTimeOffset lastTestTimestamp;

        public DateTimeOffset registrationTimestamp;

        public DateTimeOffset activationTimestamp;

        public ImagesStruct images;

        public ModemStruct modem;

        public BluetoothStruct bluetooth;
    }
}

