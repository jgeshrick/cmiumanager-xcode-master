﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using System.Collections.Generic;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;

namespace FpcClient.Common.CmiuCanConfigurationStructures
{
    /// <summary>
    /// Cellular configuration info about the CMIU’s cell modem and SIM
    /// This is made mutable to enable LabView to create and complete the data
    /// </summary>
    public struct CmiuCan
    {
        /// <summary>
        /// CMIU Id
        /// These values are 9-digits, even values only and between 400000000 and 599999999
        /// </summary>
        public uint cmiuId;
        /// <summary>
        /// APN
        /// The Access Point Name
        /// </summary>
        public string apn;
        /// <summary>
        /// Modem CAN data
        /// </summary>
        public ModemCan modemCan;
        /// <summary>
        /// SIM Can data
        /// </summary>
        public SimCan simCan;
    }

    /// <summary>
    /// Cellular configuration info about the CMIU’s cell modem only
    /// This is made mutable to enable LabView to create and complete the data
    /// </summary>
    public struct ModemCan
    {
        /// <summary>
        /// Cellular provider
        /// </summary>
        public CellularProviderCode mno;
        /// <summary>
        /// Modem vendor
        /// </summary>
        public string vendor;
        /// <summary>
        /// Modem IMEI
        /// 
        /// Maximum length is 15 digits
        /// </summary>
        public string imei;
    }

    /// <summary>
    /// Cellular configuration info about the CMIU’s sim only
    /// This is made mutable to enable LabView to create and complete the data
    /// </summary>
    public struct SimCan
    {
        /// <summary>
        /// SIM ICCID
        /// Maximum length is 20 digits
        /// </summary>
        public string iccid;
    }
}

