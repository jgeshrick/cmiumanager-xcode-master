﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;

namespace FpcClient.Common.CellularProviderCodes
{
    /// <summary>
    /// Cellular provider codes.
    /// 
    /// Add new/relevant codes when they become apparent 
    /// </summary>
    public enum CellularProviderCode
    {
        /// Verizon
        VZW,
        /// AT&T		
        ATT,   
    }
}

