﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using System.Collections.Generic;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;

namespace FpcClient.Common.L900LoRaDetailsStructures
{
    /// <summary>
    /// The L900 CAN represents the LoRaWAN information for a L900
    /// </summary>
    public struct L900Can
    {
        /// <summary>
        /// MIU ID
        /// These values are 9-digits
        /// </summary>
        public uint miuId;

        /// <summary>
        /// DevEUI
        /// The EUI of a L900
        /// </summary>
        public string devEui;

        /// <summary>
        /// AppEUI
        /// The EUI of the app on the L900 //TODO - check this is correct
        /// </summary>
        public string appEui;
    }

    /// <summary>
    /// The L900Key represents the relationship between a L900 MIU ID and the App Key
    /// </summary>
    public struct L900KeyDetails
    {
        /// <summary>
        /// The database row number for the lora device details
        /// These values are 9-digits
        /// </summary>
        public long loraDeviceDetailsId;

        /// <summary>
        /// AppKey
        /// The application key for the L900
        /// </summary>
        public string appKey;
    }
}

