﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using System.Collections.Generic;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.StatusCodes;

namespace FpcClient.Common.ServerConfigurationStructures
{
    /// <summary>
    /// Server application information. Including environment name, server name, software version
    /// </summary>
    public struct ServerDetails
    {
        /// 
        /// Server version
        /// </summary>
        public string version;
        /// <summary>
        /// Server environment
        /// </summary>
        public string environment;
        /// <summary>
        /// Server name
        /// </summary>
        public string server;
        /// <summary>
        /// Server current time in UTC format
        /// </summary>
        public string currentTimeUtc;
    }
}

