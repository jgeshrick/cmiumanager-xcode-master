﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;
using System.Collections.Generic;

namespace FpcClient.Common.CmiuLifecycleStructures
{
    /// <summary>
    /// CMIU lifecycle information. Including CMIU Id, Lifecycle state, Timestamp
    /// </summary>
    public struct CmiuLifecycleStruct
    {
        /// <summary>
        /// string miuId
        /// </summary>
        public uint miuId;

        public StateStruct state;
    }

    /// <summary>
    /// Subset of information about the lifecycle state
    /// </summary>
    public struct StateStruct
    {
        /// <summary>
        /// Lifecycle sate identifier
        /// </summary>
        public string id;

        /// <summary>
        /// Date/Time of lifecycle state change
        /// </summary>
        public DateTimeOffset timestamp;
    }
}

