﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Text.RegularExpressions;
using NeptuneCloudConnector.Exception;
using FpcClient;
using FpcClient.Settings;
using FpcClient.JsonModel;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.JsonModel.LoraConfig;
using FpcClient.JsonModel.LoraAppKey;
using FpcClient.Common;
using FpcClient.Common.StatusCodes;
using FpcClient.Common.LifecycleStates;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.CmiuCanConfigurationStructures;

namespace FpcClient.Common
{
    public static class Utilities
    {
        static System.Diagnostics.Stopwatch watch;
        private const Int32 IccidLength = 20;  // Defined length is 20 characters
        private const Int32 ImeiLength = 15;   // Defined length is 15 characters
        private const Int32 MinCmiuId = 400000000;
        private const Int32 MaxCmiuId = 599999998;
        private const Int32 MinL900Id = 700000000;
        private const Int32 MaxL900Id = 799999999;
        private const String OUI = "CC3ADF";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Starts stopwatch running
        /// </summary>
        static public void StartStopWatch()
        {
            // start the stopwatch
            watch = System.Diagnostics.Stopwatch.StartNew();
        }

        /// <summary>
        /// Stops stopwatch running
        /// </summary>
        /// <returns>Duration in milliseconds since the stopwatch was started.</returns>
        static public long StopStopWatch()
        {
            // stop the stopwatch
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            return elapsedMs;
        }

        /// <summary>
        /// Check cellular provider code is valid
        /// </summary>
        /// <param name="mnoString">String containing the data to check against valid codes</param>
        /// <param name="cellularProviderCode">CellularProviderCode identifed from the string.</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        static public StatusCode ParseCellularProviderCode(string mnoString, ref CellularProviderCode cellularProviderCode)
        {
            StatusCode returnCode = StatusCode.ErrorInvalidProvider;

            try
            {
                CellularProviderCode providerValue = (CellularProviderCode)Enum.Parse(typeof(CellularProviderCode), mnoString);
                if (Enum.IsDefined(typeof(CellularProviderCode), providerValue))
                {
                    cellularProviderCode = providerValue;
                    returnCode = StatusCode.OK;
                }
                else
                {
                    returnCode = StatusCode.ErrorInvalidProvider;
                }
            }
            catch (ArgumentException)
            {
                log.DebugFormat("'{0}' is not a member of the enumeration.", mnoString);
            }
            return returnCode;
        }

        /// <summary>
        /// Check MNO is valid string
        /// </summary>
        /// <param name="mno">CellularProviderCode to check</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        static public StatusCode ValidateMno(CellularProviderCode mno)
        {
            StatusCode returnVal = StatusCode.OK;

            if (null == mno)
            {
                returnVal = StatusCode.ErrorInvalidMnoRequest;
            }
            else if (true != System.Enum.IsDefined(typeof(CellularProviderCode), mno))
            {
                returnVal = StatusCode.ErrorInvalidMnoRequest;
            }
            return returnVal;
        }

        /// <summary>
        /// Check CellularConfig for acceptable values
        /// </summary>
        /// <param name="cellularConfig">CellularConfig structure to parse</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        static public StatusCode RangeTestCellularConfig(CellularConfig cellularConfig)
        {
            StatusCode returnVal = StatusCode.OK;

            ///Validate that CMIUIDs are 9-digits, even values only and between 400000000 and 599999999
            if (true != ValidateCmiuId(cellularConfig.MiuId))
            {
                returnVal = StatusCode.ErrorInvalidCmiuId;
            }
            else if ((null != cellularConfig.ModemConfig) && (null != cellularConfig.ModemConfig.Imei) && (cellularConfig.ModemConfig.Imei.Length != ImeiLength))
            {
                returnVal = StatusCode.ErrorInvalidImei;
            }
            else if ((null != cellularConfig.SimConfig) && (null != cellularConfig.SimConfig.Iccid) && (cellularConfig.SimConfig.Iccid.Length != IccidLength))
            {
                returnVal = StatusCode.ErrorInvalidIccid;
            }
            else if ((null != cellularConfig.ModemConfig) && (null != cellularConfig.ModemConfig.Mno) && (true != System.Enum.IsDefined(typeof(CellularProviderCode), cellularConfig.ModemConfig.Mno)))
            {
                returnVal = StatusCode.ErrorInvalidProvider;
            }
            else
            {
                if ((null != cellularConfig.ModemConfig) && (null != cellularConfig.ModemConfig.Imei))
                {
                    ///Validate that the IMEI is 15 decimal digits
                    //cellularConfig.ModemConfig.Imei;
                    foreach (char c in cellularConfig.ModemConfig.Imei)
                    {
                        // Check for numeric characters (allow negative in this case but no hex digits). 
                        // Though we use int.TryParse in the previous example and this one, int.TryParse does NOT
                        // allow a sign character (-) AND hex digits at the same time.
                        // Include initial space because it is harmless.
                        if ((c < '0') || (c > '9'))
                        {
                            returnVal = StatusCode.ErrorInvalidImei;
                            break;
                        }
                    }
                }

                if ((null != cellularConfig.SimConfig) && (null != cellularConfig.SimConfig.Iccid))
                {
                    ///Validate that the ICCID is up to 20 digits long
                    //cellularConfig.SimConfig.Iccid;
                    foreach (char c in cellularConfig.SimConfig.Iccid)
                    {
                        // Check for numeric characters (allow negative in this case but no hex digits). 
                        // Though we use int.TryParse in the previous example and this one, int.TryParse does NOT
                        // allow a sign character (-) AND hex digits at the same time.
                        // Include initial space because it is harmless.
                        if ((c < '0') || (c > '9'))
                        {
                            returnVal = StatusCode.ErrorInvalidIccid;
                            break;
                        }
                    }
                }
            }
            return returnVal;
        }

        /// <summary>
        /// Check LoraCan for acceptable values
        /// </summary>
        /// <param name="LoraCan">Lora CAN structure to parse</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        static public StatusCode RangeTestLoraCan(LoraConfig loraCan)
        {
            StatusCode returnVal = StatusCode.OK;

            ///Validate that CMIUIDs are 9-digits, even values only and between 700000000 and 799999999 
            if (true != ValidateL900Id(loraCan.MiuId))
            {
                returnVal = StatusCode.ErrorInvalidL900Id;
            }
            else if (!loraCan.DevEui.StartsWith(OUI, StringComparison.CurrentCultureIgnoreCase) ||
                !Regex.IsMatch(loraCan.DevEui, @"^[0-9a-fA-F]{16}$"))
            {
                returnVal = StatusCode.ErrorInvalidDevEui;
            }
            else if (!loraCan.AppEui.StartsWith(OUI, StringComparison.CurrentCultureIgnoreCase) ||
                !Regex.IsMatch(loraCan.AppEui, @"^[0-9a-fA-F]{16}$"))
            {
                returnVal = StatusCode.ErrorInvalidAppEui;
            }

            return returnVal;
        }

        /// <summary>
        /// Check LoraAppKey for acceptable values
        /// </summary>
        /// <param name="LoraAppKey">Lora App Key structure to parse</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        static public StatusCode RangeTestLoraAppKey(LoraAppKey loraAppKey)
        {
            StatusCode returnVal = StatusCode.OK;

            if (!Regex.IsMatch(loraAppKey.AppKey, @"^[0-9a-fA-F]+$")) //check the app key is all hex
            {
                returnVal = StatusCode.ErrorInvalidAppEui;
            }

            return returnVal;
        }

        /// <summary>
        /// Check CmiuCan for acceptable values
        /// </summary>
        /// <param name="cmiuCan">CmiuCan structure to parse</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        static public StatusCode RangeTestCmiuCan(CmiuCan cmiuCan)
        {
            StatusCode returnVal = StatusCode.OK;

            ///Validate that CMIUIDs are 9-digits, even values only and between 400000000 and 599999999
            if (true != ValidateCmiuId(cmiuCan.cmiuId))
            {
                returnVal = StatusCode.ErrorInvalidCmiuId;
            }
            else if ((null != cmiuCan.modemCan.imei) && (cmiuCan.modemCan.imei.Length != ImeiLength))
            {
                returnVal = StatusCode.ErrorInvalidImei;
            }
            else if ((null != cmiuCan.simCan.iccid) && (cmiuCan.simCan.iccid.Length != IccidLength))
            {
                returnVal = StatusCode.ErrorInvalidIccid;
            }
            else if ((null != cmiuCan.modemCan.mno) && (true != System.Enum.IsDefined(typeof(CellularProviderCode), cmiuCan.modemCan.mno)))
            {
                returnVal = StatusCode.ErrorInvalidProvider;
            }
            else
            {
                if (null != cmiuCan.modemCan.imei)
                {
                    ///Validate that the IMEI is 15 decimal digits
                    //cellularConfig.ModemConfig.Imei;
                    foreach (char c in cmiuCan.modemCan.imei)
                    {
                        // Check for numeric characters (allow negative in this case but no hex digits). 
                        // Though we use int.TryParse in the previous example and this one, int.TryParse does NOT
                        // allow a sign character (-) AND hex digits at the same time.
                        // Include initial space because it is harmless.
                        if ((c < '0') || (c > '9'))
                        {
                            returnVal = StatusCode.ErrorInvalidImei;
                            break;
                        }
                    }
                }

                if (null != cmiuCan.simCan.iccid)
                {
                    ///Validate that the ICCID is up to 20 digits long
                    //cellularConfig.SimConfig.Iccid;
                    foreach (char c in cmiuCan.simCan.iccid)
                    {
                        // Check for numeric characters (allow negative in this case but no hex digits). 
                        // Though we use int.TryParse in the previous example and this one, int.TryParse does NOT
                        // allow a sign character (-) AND hex digits at the same time.
                        // Include initial space because it is harmless.
                        if ((c < '0') || (c > '9'))
                        {
                            returnVal = StatusCode.ErrorInvalidIccid;
                            break;
                        }
                    }
                }
            }
            return returnVal;
        }

        /// <summary>
        /// Check validity of CMIU Id
        /// These values are 9-digits, even values only and between 400000000 and 599999999
        /// </summary>
        /// <param name="cmiuId">CMIU Id value to test</param>
        /// <returns>bool indicating true for OK, and false otherwise</returns>
        static public bool ValidateCmiuId(uint cmiuId)
        {
            bool returnVal = true;
            if (0 != (cmiuId % 2))
            {
                returnVal = false;
            }
            else if (cmiuId < MinCmiuId)
            {
                returnVal = false;
            }
            else if (cmiuId > MaxCmiuId)
            {
                returnVal = false;
            }
            return returnVal;
        }

        /// <summary>
        /// Check validity of L900 Id
        /// These values are 9-digits, even values only and between 700000000 and 799999999
        /// </summary>
        /// <param name="l900Id">L900 ID value to test</param>
        /// <returns>bool indicating true for OK, and false otherwise</returns>
        static public bool ValidateL900Id(uint l900Id)
        {
            bool returnVal = true;

            if (l900Id < MinL900Id)
            {
                returnVal = false;
            }
            else if (l900Id > MaxL900Id)
            {
                returnVal = false;
            }
            else if(l900Id % 2 != 0)
            {
                returnVal = false;
            }

            return returnVal;
        }

        /// <summary>
        /// Check validity of CMIU lifecycle state
        /// </summary>
        /// <param name="cmiuLifecycleState">CmiuLifecycleState value to test</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        static public StatusCode ValidateLifecycleState(CmiuLifecycleState cmiuLifecycleState)
        {
            StatusCode returnCode = StatusCode.ErrorInvalidLifecycleState;

            if (true != Utilities.ValidateCmiuId(cmiuLifecycleState.MiuId))
            {
                returnCode = StatusCode.ErrorInvalidCmiuId;
            }
            else if ((null != cmiuLifecycleState.State)
                && (null != cmiuLifecycleState.State.Id)
                && (null != cmiuLifecycleState.State.TimeStamp))
            {
                if (Enum.IsDefined(typeof(LifecycleState), cmiuLifecycleState.State.Id))
                {
                    returnCode = StatusCode.OK;
                }
            }
            return returnCode;
        }

        /// <summary>
        /// Check validity of manufacturing information
        /// </summary>
        /// <param name="manufacturingInformation">ManufacturingInformation value to test</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        static public StatusCode ValidateManufacturingInformation(ManufacturingInformation manufacturingInformation)
        {
            StatusCode returnCode = StatusCode.OK;
            if (true != Utilities.ValidateCmiuId(manufacturingInformation.MiuId))
            {
                returnCode = StatusCode.ErrorInvalidCmiuId;
            }
            return returnCode;
        }

        /// <summary>
        /// Interrogate REST error to determine appropriate error status code
        /// </summary>
        /// <param name="restException">RestException object to parse</param>
        /// <returns>StatusCode indicating type of failure</returns>
        static public StatusCode InterrogateRestError(RestException restException)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;

            if (restException is ResponseErrorRestException)
            {
                ResponseErrorRestException responseErrorRestException = (ResponseErrorRestException)restException;
                switch (responseErrorRestException.responseError)
                {
                    case System.Net.HttpStatusCode.NoContent:
                        returnCode = StatusCode.ErrorServerResponseNoContent;
                        break;
                    case System.Net.HttpStatusCode.BadRequest:
                        returnCode = StatusCode.ErrorServerResponseBadRequest;
                        break;
                    case System.Net.HttpStatusCode.Unauthorized:
                        returnCode = StatusCode.ErrorServerResponseUnauthorized;
                        break;
                    case System.Net.HttpStatusCode.Forbidden:
                        returnCode = StatusCode.ErrorInvalidLifecycleState;
                        break;
                    case System.Net.HttpStatusCode.NotFound:
                        returnCode = StatusCode.ErrorServerResponseNotFound;
                        break;
                    case System.Net.HttpStatusCode.Conflict:
                        if (restException.Message.Contains("IMEI"))
                        {
                            returnCode = StatusCode.ErrorDuplicateImei;
                        }
                        else if (restException.Message.Contains("ICCID"))
                        {
                            returnCode = StatusCode.ErrorDuplicateIccid;
                        }
                        else if (restException.Message.Contains("MNO"))
                        {
                            returnCode = StatusCode.ErrorInvalidMnoRequest;
                        }
                        else
                        {
                            returnCode = StatusCode.ErrorDuplicateData;
                        }
                        break;
                    default:
                        returnCode = StatusCode.ErrorServerResponseUnknown;
                        break;
                }
            }
            else if (restException is ServerErrorRestException)
            {
                returnCode = StatusCode.ErrorServerUnavailable;
            }
            else
            {
                returnCode = StatusCode.ErrorServerResponseUnknown;
            }
            return returnCode;
        }
    }
}




