﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClient;
using System;

namespace FpcClient.Common.LifecycleStates
{
    /// <summary>
    /// Lifecycle state codes set on the CMIU by the test station to reflect their lifecycle state.
    /// </summary>
    public enum LifecycleState
    {
        /// Newborn
        Newborn,
        /// Pre activating
        PreActivating,
        /// Pre activated
        PreActivated,
        /// Activated
        Activated,
        /// Heard
        Heard,
        /// Pre_pre_pot
        PrePrePot,
        /// Pre_pot
        PrePot,
        /// Post_pot
        PostPot,
        /// Confirmed
        Confirmed,
        /// Development
        Development,
        /// Scanned
        Scanned,
        /// Shipped
        Shipped,
        /// Unclaimed
        Unclaimed,
        /// Claimed
        Claimed,
        /// RMAed
        RmaEd,
        /// Dead
        Dead,
        /// Rogue
        Rogue,
        /// Aging
        Aging,
        /// Unspecified (Error condition)
        Unspecified,
    }
}
