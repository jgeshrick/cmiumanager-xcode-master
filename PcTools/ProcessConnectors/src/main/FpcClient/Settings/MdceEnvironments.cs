﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace FpcClient.Settings
{
    /// <summary>
    /// Load and persist MDCE environments.
    /// </summary>
    [Serializable()]
    public class MdceEnvironments
    {
        /// <summary>
        /// ENVIRONMENT_SETTINGS_FILENAME
        /// </summary>
        private const string ENVIRONMENT_SETTINGS_FILENAME = @"\environments.xml";

        /// <summary>
        /// Specify the MDCE environment where the FPC will connect to.
        /// </summary>
        public MdceEnvironment Environment {get;set;}

        /// <summary>
        /// Deserialise MDCE environment variables from xml file
        /// </summary>
        /// <returns>MdceEnvironments</returns>
        public static MdceEnvironments ReadFromXml()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MdceEnvironments));
            MdceEnvironments env = null;
            using (StreamReader reader = new StreamReader(GetApplicationPath() + ENVIRONMENT_SETTINGS_FILENAME))
            {
                env = serializer.Deserialize(reader.BaseStream) as MdceEnvironments;
            }
            return env;
        }

        /// <summary>
        /// Persist MDCE environment to xml file
        /// </summary>
        public void WriteToXml()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MdceEnvironments));
            using (StreamWriter writer = new StreamWriter(GetApplicationPath() + ENVIRONMENT_SETTINGS_FILENAME))
            {
                serializer.Serialize(writer.BaseStream, this);
            }
        }

        /// <summary>
        /// Get the active application path
        /// </summary>
        /// <returns>String containing the active application path</returns>
        private static string GetApplicationPath()
        {
            var assemblyCodeBase = AppDomain.CurrentDomain.BaseDirectory;
            return assemblyCodeBase.ToString();
        }
    }

    /// <summary>
    /// Describes an MDCE environment object
    /// </summary>
    [Serializable()]
    public class MdceEnvironment
    {
        /// <summary>
        /// Environment name -  Base URL for the environment, eg https://auto-test.mdce-nonprod.neptunensight.com:8444
        /// </summary>
        public string Name {get;set;}
    }
}
