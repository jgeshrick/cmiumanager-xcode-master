This folder contains a list of alternative FpcClient configuration files that can be used in place of the master file in the folder above this one.

These files are all intended to contain the same default values as each other required to get FpcClient up and running; the only difference is the address of the environment to which they connect.