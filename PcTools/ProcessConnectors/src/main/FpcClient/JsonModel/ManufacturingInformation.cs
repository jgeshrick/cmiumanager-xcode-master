﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using RestSharp;
using RestSharp.Deserializers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace FpcClient.JsonModel
{
    /// <summary>
    /// Defining manufacturing information stored in the FPC database.
    /// Required for - Get Manufacturing Information (IF44)
    /// </summary>
    public class ManufacturingInformation
    {
        [DeserializeAs(Name = "miu_id")]
        [JsonProperty("miu_id")]
        public uint MiuId { get; set; }

        [DeserializeAs(Name = "tests")]
        [JsonProperty("tests")]
        public List<TestStation> Tests { get; set; }

        [DeserializeAs(Name = "rework")]
        [JsonProperty("rework")]
        public Rework Rework { get; set; }

        [DeserializeAs(Name = "device_details")]
        [JsonProperty("device_details")]
        public DeviceDetails DeviceDetails { get; set; }

        /// <summary>
        /// This uses RestSharp internal JsonDeserialiser to convert a json string to a business object
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public static ManufacturingInformation FromJSON(String jsonString)
        {
            IRestResponse rsp = new RestResponse();
            rsp.Content = jsonString;

            JsonDeserializer deser = new JsonDeserializer();
            var miuConfig = deser.Deserialize<ManufacturingInformation>(rsp);

            return miuConfig;
        }
    }

    /// <summary>
    /// Information about the test station
    /// </summary>
    public class StationInfo
    {
        [DeserializeAs(Name = "type")]
        [JsonProperty("type")]
        public string Type { get; set; }

        [DeserializeAs(Name = "bay")]
        [JsonProperty("bay")]
        public string Bay { get; set; }

        [DeserializeAs(Name = "rack_id")]
        [JsonProperty("rack_id")]
        public string RackId { get; set; }

        [DeserializeAs(Name = "fixture_id")]
        [JsonProperty("fixture_id")]
        public string FixtureId { get; set; }

        [DeserializeAs(Name = "calibration_date")]
        [JsonProperty("calibration_date")]
        public DateTimeOffset CalibrationDate { get; set; }

        [DeserializeAs(Name = "labview_application_version")]
        [JsonProperty("labview_application_version")]
        public string LabviewApplicationVersion { get; set; }

        [DeserializeAs(Name = "labview_platform_version")]
        [JsonProperty("labview_platform_version")]
        public string labviewPlatformVersion { get; set; }

        [DeserializeAs(Name = "ncc_version")]
        [JsonProperty("ncc_version")]
        public string NccVersion { get; set; }

        [DeserializeAs(Name = "os_description")]
        [JsonProperty("os_description")]
        public string OsDescription { get; set; }

        [DeserializeAs(Name = "btle_dongle_driver_version")]
        [JsonProperty("btle_dongle_driver_version")]
        public string BtleDongleDriverVersion { get; set; }
    }

    /// <summary>
    /// Subset of information about the test station
    /// </summary>
    public class TestStation
    {
        [DeserializeAs(Name = "test_timestamp")]
        [JsonProperty("test_timestamp")]
        public DateTimeOffset TestTimestamp { get; set; }

        [DeserializeAs(Name = "station_info")]
        [JsonProperty("station_info")]
        public StationInfo StationInfo { get; set; }

        [DeserializeAs(Name = "log")]
        [JsonProperty("log")]
        public List<String> Log { get; set; }
    }

    /// <summary>
    /// Information about rework of this device
    /// </summary>
    public class Rework
    {
        [DeserializeAs(Name = "count")]
        [JsonProperty("count")]
        public int Count { get; set; }
    }

    /// <summary>
    /// Information about the images on the device
    /// </summary>
    public class Images
    {
        [DeserializeAs(Name = "modem_library")]
        [JsonProperty("modem_library")]
        public string ModemLibrary { get; set; }

        [DeserializeAs(Name = "cmiu_application")]
        [JsonProperty("cmiu_application")]
        public string CmiuApplication { get; set; }

        [DeserializeAs(Name = "cmiu_bootloader")]
        [JsonProperty("cmiu_bootloader")]
        public string CmiuBootloader { get; set; }

        [DeserializeAs(Name = "cmiu_configuration")]
        [JsonProperty("cmiu_configuration")]
        public string CmiuConfiguration { get; set; }
    }

    /// <summary>
    /// Information about the cellular  modem (note: other data sent via IF40)
    /// </summary>
    public class Modem
    {
        [DeserializeAs(Name = "firmware_revision")]
        [JsonProperty("firmware_revision")]
        public string FirmwareRevision { get; set; }
    }

    /// <summary>
    /// Information about Bluetooth chip
    /// </summary>
    public class Bluetooth
    {
        [DeserializeAs(Name = "mac_address")]
        [JsonProperty("mac_address")]
        public string MacAddress { get; set; }
    }

    /// <summary>
    /// Information about the CMIU device
    /// </summary>
    public class DeviceDetails
    {
        [DeserializeAs(Name = "creation_timestamp")]
        [JsonProperty("creation_timestamp")]
        public DateTimeOffset CreationTimestamp { get; set; }

        [DeserializeAs(Name = "first_test_timestamp")]
        [JsonProperty("first_test_timestamp")]
        public DateTimeOffset FirstTestTimestamp { get; set; }

        [DeserializeAs(Name = "last_test_timestamp")]
        [JsonProperty("last_test_timestamp")]
        public DateTimeOffset LastTestTimestamp { get; set; }

        [DeserializeAs(Name = "registration_timestamp")]
        [JsonProperty("registration_timestamp")]
        public DateTimeOffset RegistrationTimestamp { get; set; }

        [DeserializeAs(Name = "activation_timestamp")]
        [JsonProperty("activation_timestamp")]
        public DateTimeOffset ActivationTimestamp { get; set; }

        [DeserializeAs(Name = "images")]
        [JsonProperty("images")]
        public Images Images { get; set; }

        [DeserializeAs(Name = "modem")]
        [JsonProperty("modem")]
        public Modem Modem { get; set; }

        [DeserializeAs(Name = "bluetooth")]
        [JsonProperty("bluetooth")]
        public Bluetooth Bluetooth { get; set; }
    }

}
