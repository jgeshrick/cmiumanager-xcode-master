﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using RestSharp;
using RestSharp.Deserializers;
using System;
using Newtonsoft.Json;

namespace FpcClient.JsonModel.CmiuLifecycleState
{
    /// <summary>
    /// Defining lifecycle state information stored in the FPC database.
    /// Required for - Get CMIU lifecycle state (IF19)
    /// </summary>
    public class CmiuLifecycleState
    {
        [DeserializeAs(Name = "miu_id")]
        [JsonProperty("miu_id")]
        public uint MiuId { get; set; }

        [DeserializeAs(Name = "state")]
        [JsonProperty("state")]
        public State State { get; set; }

        /// <summary>
        /// This uses RestSharp internal JsonDeserialiser to convert a json string to a business object
        /// </summary>
        /// <param name="jsonString">Json string to convert into CmiuLifecycleState</param>
        /// <returns>CmiuLifecycleState</returns>
        public static CmiuLifecycleState FromJSON(String jsonString)
        {
            IRestResponse rsp = new RestResponse();
            rsp.Content = jsonString;

            JsonDeserializer deser = new JsonDeserializer();
            var cmiuLifecycleState = deser.Deserialize<CmiuLifecycleState>(rsp);

            return cmiuLifecycleState;
        }
    }

    public class State
    {
        [DeserializeAs(Name = "id")]
        [JsonProperty("id")]
        public string Id { get; set; }

        [DeserializeAs(Name = "timestamp")]
        [JsonProperty("timestamp")]
        public DateTimeOffset TimeStamp { get; set; }
    }
}