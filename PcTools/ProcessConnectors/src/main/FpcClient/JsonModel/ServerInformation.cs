﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using RestSharp.Deserializers;

namespace FpcClient.JsonModel
{
    /// <summary>
    /// Information regarding the Fpc server
    /// Required for - Get Fpc Server Detail (IF20)
    /// </summary>
    public class ServerInformation
    {
        /// <summary>
        /// Fpc Server software version
        /// </summary>
        [DeserializeAs(Name = "version")]
        public string Version { get; set; }

        /// <summary>
        /// Environment
        /// </summary>
        [DeserializeAs(Name = "environment")]
        public string Environment { get; set; }

        /// <summary>
        /// BaseUrl
        /// </summary>
        [DeserializeAs(Name = "server")]
        public string BaseUrl { get; set; }

        /// <summary>
        /// CurrentTimeUtc
        /// </summary>
        [DeserializeAs(Name = "current_time_utc")]
        public DateTime CurrentTimeUtc { get; set; }
    }
}
