﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using RestSharp;
using RestSharp.Deserializers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace FpcClient.JsonModel.ApnInformation
{
    /// <summary>
    /// Defining APN information stored in the FPC database.
    /// Required for - GetAPNInformation (IF45)
    /// </summary>
    public class ApnInformation
    {
        [DeserializeAs(Name = "apn")]
        [JsonProperty("apn")]
        public string Apn { get; set; }

        [DeserializeAs(Name = "mno")]
        [JsonProperty("mno")]
        public string Mno { get; set; }
    }
}
