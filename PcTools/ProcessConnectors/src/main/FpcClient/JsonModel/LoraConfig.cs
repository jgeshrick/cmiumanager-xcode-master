﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using RestSharp;
using RestSharp.Deserializers;
using System;
using Newtonsoft.Json;

namespace FpcClient.JsonModel.LoraConfig
{
    /// <summary>
    /// Defining the LoRa configuration
    /// </summary>
    public class LoraConfig
    {
        [DeserializeAs(Name = "miu_id")]
        [JsonProperty("miu_id")]
        public uint MiuId { get; set; }

        [DeserializeAs(Name = "dev_eui")]
        [JsonProperty("dev_eui")]
        public string DevEui { get; set; }

        [DeserializeAs(Name = "app_eui")]
        [JsonProperty("app_eui")]
        public string AppEui { get; set; }

        /// <summary>
        /// This uses RestSharp internal JsonDeserialiser to convert a json string to a business object
        /// </summary>
        /// <param name="jsonString">The JSON string to parse into a LoraConfig object</param>
        /// <returns>A LoraConfig object</returns>
        public static LoraConfig FromJSON(String jsonString)
        {
            IRestResponse rsp = new RestResponse();
            rsp.Content = jsonString;

            JsonDeserializer deser = new JsonDeserializer();
            var loraCan = deser.Deserialize<LoraConfig>(rsp);

            return loraCan;
        }
    }
}