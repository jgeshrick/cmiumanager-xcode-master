﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using RestSharp;
using RestSharp.Deserializers;
using System;
using Newtonsoft.Json;

namespace FpcClient.JsonModel.LoraAppKey
{
    /// <summary>
    /// Defining the LoRa App Key
    /// </summary>
    public class LoraAppKey
    {
        [DeserializeAs(Name = "lora_device_details_id")]
        [JsonProperty("lora_device_details_id")]
        public long loraDeviceDetailsId { get; set; }

        [DeserializeAs(Name = "app_key")]
        [JsonProperty("app_key")]
        public string AppKey { get; set; }

        /// <summary>
        /// This uses RestSharp internal JsonDeserialiser to convert a json string to a business object
        /// </summary>
        /// <param name="jsonString">The JSON string to parse into the LoraAppKey object</param>
        /// <returns>A LoraAppKey object</returns>
        public static LoraAppKey FromJSON(String jsonString)
        {
            IRestResponse rsp = new RestResponse();
            rsp.Content = jsonString;

            JsonDeserializer deser = new JsonDeserializer();
            var loraCan = deser.Deserialize<LoraAppKey>(rsp);

            return loraCan;
        }
    }
}