﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************


using NeptuneCloudConnector.JsonModel;
using NeptuneCloudConnector.Exception;
using System;
using FpcClient.JsonModel;
using FpcClient.JsonModel.CellularConfig;
using FpcClient.JsonModel.LoraConfig;
using FpcClient.JsonModel.LoraAppKey;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.JsonModel.ApnInformation;
using FpcClient.Common;
using FpcClient.Common.StatusCodes;
using FpcClient.Common.LifecycleStates;
using FpcClient.Common.SetCmiuCanResponse;
using FpcClient.Common.SetL900CanResponse;

namespace FpcClient
{
    /// <summary>
    /// FPC implementation of common Neptune cloud Rest client
    /// </summary>
    public class FpcRestClient : NeptuneCloudConnector.NeptuneCloudRestClient
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Token currentToken;
        private const short TokenExpireTimeJitterMin = 10;
        private const short FpcRestRetries = 3;
        private const string DateFormatIso8601 = "yyyy-MM-dd";

        #region FpcRestClient
        /// <summary>
        /// FpcRestClient
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="siteId"></param>
        /// <param name="mdceIntegrationUrl"></param>
        public FpcRestClient(string partnerId, string siteId, string mdceIntegrationUrl)
            : base(partnerId, siteId, mdceIntegrationUrl)
        {
            log.DebugFormat("Started with siteId {0}, url: {1}", siteId, mdceIntegrationBaseUrl);
        }

        public FpcRestClient(string partnerId, string siteId, string mdceIntegrationUrl, string urlSubString)
            : base(partnerId, siteId, mdceIntegrationUrl, urlSubString)
        {
            log.DebugFormat("Started with siteId {0}, url: {1}", siteId, mdceIntegrationBaseUrl);
        }
        #endregion FpcRestClient

        #region AuthenticationToken (IF03)
        /// <summary>
        /// Attempt to recycle the token if it has not expire
        /// </summary>
        /// <returns>Authenticated token or exception</returns>
        public StatusCode GetAuthenticationToken(ref Token token)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;
            try
            {
                if ((this.currentToken == null) ||
                    !this.currentToken.Authenticated ||
                    (this.currentToken.Expires.HasValue && this.currentToken.Expires.Value.AddMinutes(TokenExpireTimeJitterMin) < DateTime.UtcNow))
                {
                    //either no token has been issued or it has expired.
                    this.currentToken = base.GetAuthenticationToken();
                }
                token = this.currentToken;
                returnCode = StatusCode.OK;
            }
            catch (RestException restException)
            {
                returnCode = Utilities.InterrogateRestError(restException);
            }
            catch (System.UriFormatException)
            {
                returnCode = StatusCode.ErrorInvalidUri;
            }
            return returnCode;
        }
        #endregion //AuthenticationToken (IF03)

        #region Cellular_Config (IF39, IF40)
        /// <summary>
        /// Retrieve cellular configuration info about the MIU’s cell modem and SIM.
        /// Get MIU Cellular Config (IF39)
        /// @duration 170-360ms
        /// </summary>
        /// <param name="cmiuId">Id of CMIU</param>
        /// <param name="cellularConfig">CellularConfig object to populate</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode GetCellularConfig(uint cmiuId, ref CellularConfig cellularConfig)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;
            Token token = new Token();
            int loopCount = 0;

            if (true != Utilities.ValidateCmiuId(cmiuId))
            {
                returnCode = StatusCode.ErrorInvalidCmiuId;
            }
            else
            {
                while ((loopCount < FpcRestRetries) && (returnCode != StatusCode.OK))
                {
                    loopCount++;
                    try
                    {
                        returnCode = this.GetAuthenticationToken(ref token);
                    }
                    catch (RestException restException)
                    {
                        returnCode = Utilities.InterrogateRestError(restException);
                    }
                    catch (System.UriFormatException)
                    {
                        returnCode = StatusCode.ErrorInvalidUri;
                    }

                    if (StatusCode.OK == returnCode)
                    {
                        try
                        {
                            log.DebugFormat("DoGet: cellular-config - {0}", Convert.ToString(cmiuId));

                            cellularConfig = DoGet<CellularConfig>("miu/" + Convert.ToString(cmiuId) + "/cellular-config", "token", token.AuthToken);
                            returnCode = StatusCode.OK;
                        }
                        catch (RestException restException)
                        {
                            returnCode = Utilities.InterrogateRestError(restException);
                        }
                    }
                }
            }
            return returnCode;
        }

        /// <summary>
        /// Change configuration info in the MDCE for a CMIU.
        /// The actual config on the CMIU cannot be modified, 
        /// so this is just a means to provide information about the CMIU to the MDCE.
        /// Set MIU Cellular Config (IF40)
        /// Return success/fail string
        /// @duration 350-670ms
        /// <param name="config">New cellular config to set</param>
        /// </summary>
        /// <param name="cellularConfig">CellularConfig data to set</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public SetCmiuCanResponse SetCellularConfig(CellularConfig config)
        {
            SetCmiuCanResponse response = new SetCmiuCanResponse();
            response.statusCode = StatusCode.ErrorUnspecified;
            Token token = new Token();
            bool success = false;
            int loopCount = 0;

            response.statusCode = Common.Utilities.RangeTestCellularConfig(config);
            /// Ensure data in is within range
            if (StatusCode.OK == response.statusCode)
            {
                while ((loopCount < FpcRestRetries) && (success != true))
                {
                    loopCount++;
                    try
                    {
                        response.statusCode = this.GetAuthenticationToken(ref token);
                    }
                    catch (RestException restException)
                    {
                        response.statusCode = Utilities.InterrogateRestError(restException);
                    }
                    catch (System.UriFormatException)
                    {
                        response.statusCode = StatusCode.ErrorInvalidUri;
                    }

                    if (StatusCode.OK == response.statusCode)
                    {
                        try
                        {
                            log.DebugFormat("DoPost: cellular-config - {0}", Convert.ToString(config.MiuId));

                            var result = DoPost("miu/" + Convert.ToString(config.MiuId) + "/cellular-config", config,
                                "token", token.AuthToken);

                            if (result.StatusCode == System.Net.HttpStatusCode.OK || result.StatusCode == System.Net.HttpStatusCode.NoContent)
                            {
                                response.statusCode = StatusCode.OK;
                                success = true;
                            }
                            else
                            {
                                response.statusCode = StatusCode.ErrorRestDoPost;
                                response.errorString = result.StatusDescription;
                                success = false;
                            }
                        }
                        catch (RestException restException)
                        {
                            response.statusCode = Utilities.InterrogateRestError(restException);
                            response.errorString = restException.Message;
                        }
                    }
                }
            }

            return response;
        }
        #endregion //Cellular_Config (IF39, IF40)
        
        #region LoRa details
        /// <summary>
        /// <param name="config">New lora config to set</param>
        /// </summary>
        /// <param name="LoraConfig">loraConfig data to set</param>
        /// <returns>L900CanResponse indicating success or type of failure, and the app key index</returns>
        public SetL900CanResponse SetL900Config(LoraConfig loraConfig)
        {
            SetL900CanResponse response = new SetL900CanResponse(); 
            response.statusCode = StatusCode.ErrorUnspecified;
            Token token = new Token();
            bool success = false;
            int loopCount = 0;

            /// Ensure data is within range
            response.statusCode = Common.Utilities.RangeTestLoraCan(loraConfig);

            if (StatusCode.OK == response.statusCode)
            {
                while ((loopCount < FpcRestRetries) && (success != true))
                {
                    loopCount++;

                    try
                    {
                        response.statusCode = this.GetAuthenticationToken(ref token);
                    }
                    catch (RestException restException)
                    {
                        response.statusCode = Utilities.InterrogateRestError(restException);
                    }
                    catch (System.UriFormatException)
                    {
                        response.statusCode = StatusCode.ErrorInvalidUri;
                    }

                    if (StatusCode.OK == response.statusCode)
                    {
                        try
                        {
                            log.DebugFormat("DoPost: lora-can - {0}", Convert.ToString(loraConfig.MiuId));

                            var result = DoPost("miu/" + Convert.ToString(loraConfig.MiuId) + "/lora-config", loraConfig,
                                "token", token.AuthToken);

                            if (result.StatusCode == System.Net.HttpStatusCode.OK || result.StatusCode == System.Net.HttpStatusCode.NoContent)
                            {
                                response.statusCode = StatusCode.OK;
                                response.appKeyIndex = (long)Convert.ToInt64(result.Content.ToString());
                                success = true;
                            }
                            else
                            {
                                response.statusCode = StatusCode.ErrorRestDoPost;
                                success = false;
                            }
                        }
                        catch (RestException restException)
                        {
                            response.statusCode = Utilities.InterrogateRestError(restException);
                            //TODO - log the response ??
                        }
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// <param name="loraAppKey">LoRa app key to set</param>
        /// </summary>
        /// <param name="LoraAppKey">loraAppKey data to set</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode SetL900AppKey(LoraAppKey loraAppKey)
        {
            StatusCode response = StatusCode.ErrorUnspecified;
            Token token = new Token();
            bool success = false;
            int loopCount = 0;

            /// Ensure data is within range
            response = Common.Utilities.RangeTestLoraAppKey(loraAppKey);

            if (StatusCode.OK == response)
            {
                while ((loopCount < FpcRestRetries) && (success != true))
                {
                    loopCount++;

                    try
                    {
                        response = this.GetAuthenticationToken(ref token);
                    }
                    catch (RestException restException)
                    {
                        response = Utilities.InterrogateRestError(restException);
                    }
                    catch (System.UriFormatException)
                    {
                        response = StatusCode.ErrorInvalidUri;
                    }

                    if (StatusCode.OK == response)
                    {
                        try
                        {
                            log.DebugFormat("DoPost: lora-app-key - {0}", Convert.ToString(loraAppKey.loraDeviceDetailsId));

                            var result = DoPost("miu/lora-app-key", loraAppKey, "token", token.AuthToken);

                            if (result.StatusCode == System.Net.HttpStatusCode.OK || result.StatusCode == System.Net.HttpStatusCode.NoContent)
                            {
                                response = StatusCode.OK;
                                success = true;
                            }
                            else
                            {
                                response = StatusCode.ErrorRestDoPost;
                                success = false;
                            }
                        }
                        catch (RestException restException)
                        {
                            response = Utilities.InterrogateRestError(restException);
                            //TODO - log the response ??
                        }
                    }
                }
            }

            return response;
        }
        #endregion //LoRa details


        #region Server detail (IF20)
        /// <summary>
        /// Get Fpc Server Detail (IF20)
        /// @duration 180-900ms
        /// </summary>
        /// <param name="serverInformation">ServerInformation object to populate</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode GetFPCServerDetail(ref ServerInformation serverInformation)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;
            Token token = new Token();
            int loopCount = 0;

            while ((loopCount < FpcRestRetries) && (returnCode != StatusCode.OK))
            {
                loopCount++;
                try
                {
                    returnCode = this.GetAuthenticationToken(ref token);
                }
                catch (RestException restException)
                {
                    returnCode = Utilities.InterrogateRestError(restException);
                }
                catch (System.UriFormatException)
                {
                    returnCode = StatusCode.ErrorInvalidUri;
                }

                if (StatusCode.OK == returnCode)
                {
                    try
                    {
                        log.Debug("DoGet: token");

                        serverInformation = DoGet<ServerInformation>("info", "token", token.AuthToken);
                        returnCode = StatusCode.OK;
                    }
                    catch (RestException restException)
                    {
                        returnCode = Utilities.InterrogateRestError(restException);
                    }
                }
            }
            log.DebugFormat("Done GetFpcServerInformation {0}", serverInformation.ToString());
            return returnCode;
        }
        #endregion Server detail (IF20)

        #region CMIU lifecycle state (IF19, IF43)
        /// <summary>
        /// Retrieve lifecycle information from the FPC.
        /// Get CMIU lifecycle state (IF19)
        /// @duration 166-793ms        
        /// </summary>
        /// <param name="cmiuId">Id of CMIU</param>
        /// <param name="cmiuLifecycleState">CmiuLifecycleState object to populate</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode GetCmiuLifecycleState(uint cmiuId, ref CmiuLifecycleState cmiuLifecycleState)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;
            Token token = new Token();
            int loopCount = 0;

            if (true != Utilities.ValidateCmiuId(cmiuId))
            {
                returnCode = StatusCode.ErrorInvalidCmiuId;
            }
            else
            {
                while ((loopCount < FpcRestRetries) && (returnCode != StatusCode.OK))
                {
                    loopCount++;
                    try
                    {
                        returnCode = this.GetAuthenticationToken(ref token);
                    }
                    catch (RestException restException)
                    {
                        returnCode = Utilities.InterrogateRestError(restException);
                    }
                    catch (System.UriFormatException)
                    {
                        returnCode = StatusCode.ErrorInvalidUri;
                    }

                    if (StatusCode.OK == returnCode)
                    {
                        try
                        {
                            log.DebugFormat("DoGet: lifecycle-state - {0}", Convert.ToString(cmiuId));

                            var localCmiuLifecycleState = DoGet<CmiuLifecycleState>("miu/" + Convert.ToString(cmiuId) + "/lifecycle-state", "token", token.AuthToken);
                            if (null == localCmiuLifecycleState)
                            {
                                returnCode = StatusCode.ErrorInvalidLifecycleState;
                            }
                            if (StatusCode.OK == Utilities.ValidateLifecycleState(localCmiuLifecycleState))
                            {
                                cmiuLifecycleState = localCmiuLifecycleState;
                                returnCode = StatusCode.OK;
                            }
                            else
                            {
                                returnCode = StatusCode.ErrorInvalidLifecycleState;
                            }
                        }
                        catch (RestException restException)
                        {
                            returnCode = Utilities.InterrogateRestError(restException);
                        }
                    }
                }
            }
            return returnCode;
        }

        /// <summary>
        /// Change lifecycle state in the MDCE for a CMIU.
        /// Set CMIU lifecycle state (IF43)
        /// Return success/fail string
        /// @duration 371-1958ms        
        /// <param name="config">new cellular config</param>
        /// </summary>
        /// <param name="cmiuLifecycleState">CmiuLifecycleState data to set</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode SetCmiuLifecycleState(CmiuLifecycleState lifecycleState)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;
            Token token = new Token();
            int loopCount = 0;

            while ((loopCount < FpcRestRetries) && (returnCode != StatusCode.OK))
            {
                loopCount++;
                try
                {
                    returnCode = this.GetAuthenticationToken(ref token);
                }
                catch (RestException restException)
                {
                    returnCode = Utilities.InterrogateRestError(restException);
                }
                catch (System.UriFormatException)
                {
                    returnCode = StatusCode.ErrorInvalidUri;
                }

                if (StatusCode.OK == returnCode)
                {
                    try
                    {
                        log.DebugFormat("DoPost: lifecycle-state - {0}", Convert.ToString(lifecycleState.MiuId));

                        var result = DoPost("miu/" + Convert.ToString(lifecycleState.MiuId) + "/lifecycle-state", lifecycleState,
                            "token", token.AuthToken);
                        if (result.StatusCode == System.Net.HttpStatusCode.NoContent)
                        {
                            returnCode = StatusCode.OK;
                        }
                        else
                        {
                            returnCode = StatusCode.ErrorRestDoPost;
                        }
                    }
                    catch (RestException restException)
                    {
                        returnCode = Utilities.InterrogateRestError(restException);
                    }
                }
            }
            return returnCode;
        }
        #endregion //CMIU lifecycle state (IF19, IF43)

        #region Manufacturing Information (IF44, IF11)
        /// <summary>
        /// Retrieve lifecycle information from the FPC.
        /// Get Manufacturing Information (IF44)
        /// @duration 194-497ms        
        /// </summary>
        /// <param name="cmiuId">Id of CMIU</param>
        /// <param name="manufacturingInformation">ManufacturingInformation object to populate</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode GetManufacturingInformation(uint cmiuId, ref ManufacturingInformation manufacturingInformation)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;
            Token token = new Token();
            int loopCount = 0;

            if (true != Utilities.ValidateCmiuId(cmiuId))
            {
                returnCode = StatusCode.ErrorInvalidCmiuId;
            }
            else
            {
                while ((loopCount < FpcRestRetries) && (returnCode != StatusCode.OK))
                {
                    loopCount++;
                    try
                    {
                        returnCode = this.GetAuthenticationToken(ref token);
                    }
                    catch (RestException restException)
                    {
                        returnCode = Utilities.InterrogateRestError(restException);
                    }
                    catch (System.UriFormatException)
                    {
                        returnCode = StatusCode.ErrorInvalidUri;
                    }

                    if (StatusCode.OK == returnCode)
                    {
                        try
                        {
                            log.DebugFormat("DoGet: manufacturing-information - {0}", Convert.ToString(cmiuId));

                            manufacturingInformation = DoGet<ManufacturingInformation>("miu/" + Convert.ToString(cmiuId) + "/manufacturing-information", "token", token.AuthToken);
                            returnCode = Utilities.ValidateManufacturingInformation(manufacturingInformation);
                        }
                        catch (RestException restException)
                        {
                            returnCode = Utilities.InterrogateRestError(restException);
                        }
                    }
                }
            }
            return returnCode;
        }

        /// <summary>
        /// Set Manufacturing Information (IF11)
        /// This web service will be used by the NeptuneCloudConnector to store manufacturing information in the Fpc database.
        /// </summary>
        /// <param name="cmiuId">Id of CMIU</param>
        /// <param name="manufacturingInformation">ManufacturingInformation object to populate</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode SetManufacturingInformation(ManufacturingInformation manufacturingInformation)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;
            Token token = new Token();
            int loopCount = 0;

            StatusCode testCode = Utilities.ValidateManufacturingInformation(manufacturingInformation);
            if (StatusCode.OK != testCode)
            {
                returnCode = testCode;
            }
            else
            {
                while ((loopCount < FpcRestRetries) && (returnCode != StatusCode.OK))
                {
                    loopCount++;
                    try
                    {
                        returnCode = this.GetAuthenticationToken(ref token);
                    }
                    catch (RestException restException)
                    {
                        returnCode = Utilities.InterrogateRestError(restException);
                    }
                    catch (System.UriFormatException)
                    {
                        returnCode = StatusCode.ErrorInvalidUri;
                    }

                    if (StatusCode.OK == returnCode)
                    {
                        try
                        {
                            log.DebugFormat("DoPost: manufacturing-information - {0}", Convert.ToString(manufacturingInformation.MiuId));

                            var result = DoPost("miu/" + Convert.ToString(manufacturingInformation.MiuId) + "/manufacturing-information", manufacturingInformation,
                                "token", token.AuthToken);
                            if (result.StatusCode == System.Net.HttpStatusCode.NoContent)
                            {
                                returnCode = StatusCode.OK;
                            }
                            else
                            {
                                returnCode = StatusCode.ErrorRestDoPost;
                            }
                        }
                        catch (RestException restException)
                        {
                            returnCode = Utilities.InterrogateRestError(restException);
                        }
                    }
                }
            }
            return returnCode;
        }
        #endregion //Manufacturing Information (IF44, IF11)


        #region APN Information (IF45)
        /// <summary>
        /// Get APN Information (IF45)
        /// This should be called before every call to Set MIU Cellular Config (IF40) because
        /// CMIUs on the same network can have different APNs.
        /// @duration 163-992ms        
        /// </summary>
        /// <param name="apnInfo">ApnInformation object to populate</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode GetApnInformation(ref ApnInformation apnInfo)
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;
            Token token = new Token();
            int loopCount = 0;

            while ((loopCount < FpcRestRetries) && (returnCode != StatusCode.OK))
            {
                loopCount++;
                try
                {
                    returnCode = this.GetAuthenticationToken(ref token);
                }
                catch (RestException restException)
                {
                    returnCode = Utilities.InterrogateRestError(restException);
                }
                catch (System.UriFormatException)
                {
                    returnCode = StatusCode.ErrorInvalidUri;
                }

                if (StatusCode.OK == returnCode)
                {
                    try
                    {
                        apnInfo = DoGet<ApnInformation>("apn-info", "token", token.AuthToken, "mno", apnInfo.Mno);
                        returnCode = StatusCode.OK;
                    }
                    catch (RestException restException)
                    {
                        returnCode = Utilities.InterrogateRestError(restException);
                    }
                }
            }
            return returnCode;
        }
        #endregion APN Information (IF45)
    }
}
