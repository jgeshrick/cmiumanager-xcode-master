﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using FpcClient.JsonModel;
using FpcClient.Common.StatusCodes;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.Common.LifecycleStates;
using FpcClient.Common.ConfigurationStructures;
using FpcClient.Common.CmiuLifecycleStructures;
using FpcClient.Common.CmiuCanConfigurationStructures;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.ApnInformationStructures;
using FpcClient.Common.SetCmiuCanResponse;

namespace FpcClientLibrary
{
    /// <summary>
    /// 
    /// This is the public interface for the FPC client.
    /// It exposes high level methods to be called by the host for access to a
    /// REST service from the Factory Process Connector Server in Amazon Web Services. \n
    /// This interface will be called by various test stations, and it is anticipated that they will be Labview applications.
    /// The expected test stations are:
    ///   -  Functional Test Station (FTS)
    ///   -  Connection Validation Station (CVS)
    ///   -  Pre-pot Station
    ///   -  Post-pot Station
    ///   -  Final Functional Test Station (FFTS)
    /// 
    /// Workflows            {#title}
    /// =====
    /// 
    /// Each test station is expected to use the FPC client library API as specified below.
    /// It is expected that some of these functions will be called repeatedly and out-of-order (as a result of
    /// production rework, for example).  As long as the Client is correctly initialised, these calls have been
    /// designed to work regardless of the order in which they are called. Where out-of-order calls are made
    /// that are not supported, the returned StatusCode values will be used to indicate this.
    /// 
    /// Functional Test Station (FTS)        {#section3}
    /// ---------
    /// 
    /// The expected workflow for the FTS is as follows:
    /// -# StatusCode InitFpcClient(String partnerId) - Initialise the client
    /// -# StatusCode SetCmiuCan(CmiuCan cmiuCan) - Set CAN information for the CMIU under test
    /// -# String GetStatusCodeString(StatusCode statusCode) - Decode the status code returned by step 2
    /// 
    /// Connection Validation Station (CVS)        {#section4}
    /// ---------
    /// 
    /// The expected workflow for the CVS is as follows:
    /// -# StatusCode InitFpcClient(String partnerId) - Initialise the client
    /// -# StatusCode IsCmiuActivated(uint cmiuId) - Check to see whether the CMIU specified is ready for network activity
    /// 
    /// Pre-pot Station        {#section5}
    /// ---------
    /// 
    /// The workflow for the pre-pot station is as follows:
    /// -# StatusCode InitFpcClient(String partnerId) - Initialise the client
    /// -# StatusCode RegisterCmiuBeforePrepotTest(uint cmiuId) - Set the life-cycle state to Pre-Pre-Pot prior to commencing the pre-pot test
    /// -# StatusCode RegisterCmiuAfterSuccessfulPrepotTest(uint cmiuId) - Set the life-cycle state to Pre-Pot after a successful pre-pot test
    /// 
    /// Post-pot Station        {#section6}
    /// ---------
    /// 
    /// The workflow for the post-pot station is as follows:
    /// -# StatusCode InitFpcClient(String partnerId) - Initialise the client
    /// -# StatusCode RegisterCmiuAfterSuccessfulPostpotTest(uint cmiuId) - Set the life-cycle state to Post-Pot after a successful post-pot test
    /// 
    /// Final Functional Test Station (FFTS)       {#section7}
    /// ---------
    /// 
    /// The workflow for the FFTS is as follows:
    /// -# StatusCode InitFpcClient(string partnerId) - Initialise the client
    /// -# TBC
    /// 
    /// Initialisation            {#title2}
    /// =====
    /// 
    /// The following steps must be followed prior to calling any other functions:
    ///   * Create the client - FpcClient()
    ///   * Initialise the client - InitFpcClient()
    /// It is worth noting that, as with other functions, InitFpcClient() can be called multiple times with the same partnerId;
    /// it can be used as a means to re-initilaise a client or simply to re-parse the environments configuration file.\n 
    /// 
    /// 
    /// Limits and Ranges            {#title3}
    /// =====
    /// 
    /// Some of the data parameters have formatting, range and limit constraints.
    /// These are checked by the FPC Client before transmission.
    /// Details of those which are validated are shown below.
    /// 
    ///   * CMIUID
    ///     - Must be exactly 9 digits
    ///     - Must be an even number
    ///     - Must be between 400000000 and 599999999 (inclusive)
    /// 
    ///   * IMEI
    ///     - Must be exactly 15 digits
    ///     - Must be numeric values only (no alpha characters)
    /// 
    ///   * ICCID
    ///     - Must be exactly 20 digits
    ///     - Must be numeric values only (no alpha characters)
    /// 
    ///   * Cellular Provider Code, Vendor
    ///     - If set it must be one of the values detailed in the section "Package FpcClient.Common.CellularProviderCodes".
    /// 
    ///   * CMIU Lifecycle State
    ///     - CMIUID must conform to rules for CMIUID as above
    ///     - If State Id is set it must be one of the values detailed in the section "Package FpcClient.Common.LifecycleStates".
    /// 
    ///   * ManufacturingInformation
    ///     - CMIUID must conform to rules for CMIUID as above
    /// \n 
    /// 
    /// Errors and Status Codes            {#title4}
    /// =====
    /// 
    /// Most of the functions return a status code to communicate the success, or otherwise, of the attempted actions.
    /// The current list of status codes is detailed in the section "Package FpcClient.Common.StatusCodes".
    /// 
    /// ---------
    /// </summary>
    public interface _IFpcClient
    {
        #region Service Configuration and Initialisation Functions
        /// <summary>
        /// This function performs the steps required to initialise the FPC client and perform authentication with the FPC server.
        /// This must be called prior to calling any FPC client functions.
        /// For more details refer to the "Initialisation" section.
        /// </summary>
        /// <param name="partnerId">PartnerId string</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        StatusCode InitFpcClient(string partnerId);

        /// <summary>
        /// Checks to see whether the CMIU specified is ready for network activity.
        /// @duration TODO: Document estimated duration
        /// </summary>
        /// <param name="cmiuId">Id of CMIU</param>
        /// <returns>StatusCode indicating if CMUI is active or a relevant failure code</returns>
        StatusCode IsCmiuActivated(uint cmiuId);

        /// <summary>
        /// Provide caller with a means to register CMIU state with the FPC server.
        /// Set the life-cycle state to PrePrePot prior to commencing the pre-pot test.
        /// @duration 371-1748ms        
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to register</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        StatusCode RegisterCmiuBeforePrepotTest(uint cmiuId);

        /// <summary>
        /// Provide caller with a means to register CMIU state with the FPC server.
        /// Set the life-cycle state to PrePot after a successful pre-pot test.
        /// @duration 368-1730ms        
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to register</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        StatusCode RegisterCmiuAfterSuccessfulPrepotTest(uint cmiuId);

        /// <summary>
        /// Provide caller with a means to register CMIU state with the FPC server.
        /// Set the life-cycle state to PostPot after a successful post-pot test.
        /// @duration 372-897ms        
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to register</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        StatusCode RegisterCmiuAfterSuccessfulPostpotTest(uint cmiuId);

        /// <summary>
        /// Provide caller with a means to check the CMIU state with the FPC server.
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to check</param>
        /// <param name="cmiuLifecycleState">CmiuLifecycleStruct object to populate</param>
        /// <returns>StatusCode to reflect result of function call</returns>
        StatusCode GetCmiuLifecycle(uint cmiuId, ref CmiuLifecycleStruct cmiuLifecycleState);

        /// <summary>
        /// Provide caller with a means to get the status message behind a status code.
        /// </summary>
        /// <param name="statusCode">Code to parse into string</param>
        /// <returns>String containing status code text</returns>
        string GetStatusCodeString(StatusCode statusCode);

        /// <summary>
        /// Provide caller with a means to get the APN network identifier string for a specific MNO.
        /// This should be called before every call to SetCmiuCan() because
        /// CMIUs on the same network can have different APNs.
        /// @duration 168-897ms        
        /// </summary>
        /// <param name="apnInfo">APN Information including MNO to send</param>
        /// <returns>String containing APN string</returns>
        StatusCode GetApnString(ref ApnInfo apnInfo);

        #endregion //Service Configuration and Initialisation Functions

        #region Client REST Functions
        #region GET/SET Cellular Config
        /// <summary>
        /// Set CAN details of a CMIU.
        /// @duration 350-670ms        
        /// </summary>
        /// <param name="cmiuCan">CmiuCan object containing data to set</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        SetCmiuCanResponse SetCmiuCan(CmiuCan cmiuCan);

        /// <summary>
        /// Allow FPC client to retrieve details of a CMIU.
        /// @duration 200ms
        /// </summary>
        /// <param name="cmiuId">Id of CMIU</param>
        /// <param name="cmiuCan">CmiuCan object to populate</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        StatusCode GetCmiuCan(uint cmiuId, ref CmiuCan cmiuCan);
        #endregion //Get/Set Cellular Config

        #region GET Server Detail
        /// <summary>
        /// Retrieve server information.
        /// Basic server details include - Server version, Server environment, Server name, Server current time in UTC format
        /// See ServerDetails Struct Reference for more details
        /// @duration 180-920ms
        /// </summary>
        /// <param name="serverDetails">ServerDetails object to populate</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        StatusCode GetServerDetails(ref ServerDetails serverDetails);
        #endregion //GET Server Detail

        #region Set Manufacturing Information
        /// <summary>
        /// Allow FPC client to set manufacturing information of a CMIU
        /// </summary>
        /// <param name="manufacturingInformation">ManufacturingInformationStruct object containing data to set</param>
        /// <returns>StatusCode indicating success or otherwise</returns>
        StatusCode SetMfgInfo(ManufacturingInformationStruct manufacturingInformation);
        #endregion //Set Manufacturing Information

        #endregion //Client REST Functions

        #region Public Data
        /// <summary>
        /// Get the environment (URL) defined for the FPC Server
        /// </summary>
        string Environment { get; }

        /// <summary>
        /// Get the minimum value of an error status code.
        /// Status codes of this value or higher are considered to be errors.
        /// Zero is always OK.
        /// Values between these two are considered to be warnings.
        /// </summary>
        UInt16 MinimumErrorCode { get; }

        /// <summary>
        /// Get the FPC Client assembly version.
        /// </summary>
        string AssemblyVersion { get; }
        #endregion //Public Data
    }
}