﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Diagnostics;
using System.Collections.Generic;
using FpcClient;
using FpcClient.Settings;
using FpcClient.JsonModel;
using FpcClient.JsonModel.CmiuLifecycleState;
using FpcClient.JsonModel.LoraConfig;
using FpcClient.JsonModel.LoraAppKey;
using FpcClient.Common;
using FpcClient.Common.StatusCodes;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.Common.LifecycleStates;
using FpcClient.Common.ConfigurationStructures;
using FpcClient.Common.CmiuLifecycleStructures;
using FpcClient.Common.CmiuCanConfigurationStructures;
using FpcClient.Common.CellularProviderCodes;
using FpcClient.Common.ApnInformationStructures;
using FpcClient.Common.SetCmiuCanResponse;
using FpcClient.Common.L900LoRaDetailsStructures;

namespace FpcClientLibrary
{
    /// <summary>
    /// Implements a Stub for Fpc core service.
    /// </summary>/// 
    public class FpcClient : _IFpcClient
    {
        private bool forceStubTestFail = false;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        internal const UInt16 MinErrorCode = 100;
        private const string TestImeiValid = "123456789012345";
        private const string TestIccidValid = "24680246802468024680";
        private const string TestVendor = "Telit";
        private const string TestApn = "NTG.GW10.VZWENTP";

        private List<CmiuCan> cmius;

        /// <summary>
        /// Instance of FpcRestClient
        /// </summary>
        //private FpcRestClient fpcRestClient;

        /// <summary>
        /// MdceEnvironment
        /// </summary>
        private MdceEnvironment environment;

        /// <summary>
        /// Environment (URL) defined for the FPC Server
        /// </summary>
        public string Environment
        {
            get
            {
                return environment.Name;
            }
        }

        /// <summary>
        /// The minimum value of an error status code.
        /// Status codes of this value or higher are considered to be errors.
        /// Zero is always OK.
        /// Values between these two are considered to be warnings.
        /// </summary>
        public UInt16 MinimumErrorCode
        {
            get
            {
                return MinErrorCode;
            }
        }

        /// <summary>
        /// The FPC Client assembly version.
        /// </summary>
        public string AssemblyVersion
        {
            get
            {
                return GetClientAssemblyVersion();
            }
        }

        /// <summary>
        /// FpcClient constructor
        /// </summary>
        public FpcClient()
        {
            log.Debug("Stub FpcClient");
            cmius = new List<CmiuCan>();
        }

        #region Client REST Functions

        #region Set Manufacturing Information
        /// <summary>
        /// Allow FPC client to set manufacturing information of a CMIU
        /// </summary>
        /// <param name="manufacturingInformation">ManufacturingInformationStruct object containing data to set</param>
        /// <returns>StatusCode indicating success or otherwise</returns>
        public StatusCode SetMfgInfo(ManufacturingInformationStruct manufacturingInformation)
        {
            log.Debug("Stub SetMfgInfo");
            /// Return NotImplemented
            return (StatusCode.WarningStubNotImplemented);
        }
        #endregion //Set Manufacturing Information
        
        #region GET/SET Cellular Config
        /// <summary>
        /// Allow Fpc client to set details of a CMIU.
        /// </summary>
        /// <returns>boolean indicating success or failure</returns>
        public SetCmiuCanResponse SetCmiuCan(CmiuCan cmiuCan)
        {
            log.Debug("Stub SetCmiuCan");
            /// Return NotImplemented
            SetCmiuCanResponse setCmiuCanResponse = new SetCmiuCanResponse();
            setCmiuCanResponse.statusCode = StatusCode.WarningStubNotImplemented;
            return setCmiuCanResponse;
        }

        /// <summary>
        /// Allow Fpc client to retrieve details of a CMIU.
        /// </summary>
        /// <returns>Return struct containing cmiu detail</returns>
        public StatusCode GetCmiuCan(uint cmiuId, ref CmiuCan cmiuCan)
        {
            log.DebugFormat("Stub GetCmiuCan - {0}", cmiuId);

            cmiuCan.apn = TestApn;
            cmiuCan.modemCan.imei = TestImeiValid;
            cmiuCan.modemCan.mno = CellularProviderCode.VZW; ;
            cmiuCan.modemCan.vendor = TestVendor;
            cmiuCan.simCan.iccid = TestIccidValid;

            /// Return NotImplemented
            return (StatusCode.WarningStubNotImplemented);
        }
        #endregion //Get/Set Cellular Config

        #region SET L900 CAN/KEY

        /// <summary>
        /// SetL900Can - Sets the LoRaWAN details for a L900
        /// </summary>
        /// <returns>Status</returns>
        public StatusCode SetL900Can(L900Can l900Can)
        {
            return StatusCode.WarningStubNotImplemented;
        }

        /// <summary>
        /// SetL900AppKey - Stores the L900 App Key in the database
        /// </summary>
        /// <returns>Status</returns>
        public StatusCode SetL900AppKey(L900KeyDetails l900KeyDetails)
        {
            return StatusCode.WarningStubNotImplemented;
        }

        #endregion //Set L900 CAN/KEY

        #region GET Server Detail
        /// <summary>
        /// Get the server details
        /// </summary>
        /// <returns>ServerDetails</returns>
        public StatusCode GetServerDetails(ref ServerDetails serverDetails)
        {
            log.Debug("Stub GetServerDetails");

            serverDetails.server = environment.Name;
            serverDetails.environment = environment.ToString();
            serverDetails.version = "1.2.3.4";
            serverDetails.currentTimeUtc = DateTime.UtcNow.ToString("d");

            /// Return NotImplemented
            return (StatusCode.WarningStubNotImplemented);
        }
        #endregion //GET Server Detail
        
        #endregion //Client REST Functions

        #region Service Configuration and Initialisation Functions
        /// <summary>
        /// This function performs the steps required to initialise the FPC client and perform authentication with the FPC server.
        /// </summary>
        /// <param name="partnerId">PartnerId string</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode InitFpcClient(String partnerId)
        {
            log.DebugFormat("Stub InitFpcClient - {0}", partnerId);
            /// Return result of parsing the configuration file
            /// Required to prove that the function has done something
            return (ParseConfigurationFile());
        }

        /// <summary>
        /// This function is required to enable LabVIEW to pass the partner key to the dll
        /// so that it can authenticate with Fpc server
        /// </summary>
        /// <param name="partnerId">PartnerId string</param>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode InitPartnerId(String partnerId)
        {
            log.DebugFormat("Stub InitPartnerId - {0}", partnerId);
            /// Return NotImplemented
            return (StatusCode.WarningStubNotImplemented);
        }

        /// <summary>
        /// Read (and store locally) environment configuration
        /// </summary>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode ParseConfigurationFile()
        {
            StatusCode returnCode = StatusCode.ErrorUnspecified;

            try
            {
                var mdceEnvironments = MdceEnvironments.ReadFromXml();
                if ((null != mdceEnvironments.Environment) && (null != mdceEnvironments.Environment.Name))
                {
                    this.environment = mdceEnvironments.Environment;
                    returnCode = StatusCode.OK;
                }
                else
                {
                    returnCode = StatusCode.ErrorConfigFile;
                }
            }
            catch (Exception)
            {
                //Swallow exception and return valid status code
                //This is not the OO way of doing things but it does mean we can return a sane value to LabView
                returnCode = StatusCode.ErrorConfigFile;
            }
            log.DebugFormat("Stub ParseConfigurationFile - {0}", returnCode);
            return returnCode;
        }

        /// <summary>
        /// Setup FPC client REST connection to FPC server
        /// </summary>
        /// <returns>StatusCode indicating success or type of failure</returns>
        public StatusCode InitialiseClient(String partnerId)
        {
            log.DebugFormat("Stub InitialiseClient - {0}", partnerId);
            /// Return NotImplemented
            return (StatusCode.WarningStubNotImplemented);
        }

        /// <summary>
        /// Checks to see whether the CMIU specified is ready for network activity.
        /// </summary>
        /// <param name="cmiuId">Id of CMIU</param>
        /// <returns>StatusCode indicating if CMUI is active or a relevant failure code</returns>
        public StatusCode IsCmiuActivated(uint cmiuId)
        {
            log.DebugFormat("Stub IsCmiuActivated - {0}", cmiuId);
            /// Return NotImplemented
            return (StatusCode.WarningStubNotImplemented);
        }

        /// <summary>
        /// Provide LabVIEW with a means to register CMIU state with the Fpc server.
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to register</param>
        /// <returns></returns>
        public StatusCode RegisterCmiuBeforePrepotTest(uint cmiuId)
        {
            log.DebugFormat("Stub RegisterCmiuBeforePrepotTest - {0}", cmiuId);
            /// Return NotImplemented
            return (StatusCode.WarningStubNotImplemented);
        }

        /// <summary>
        /// Provide LabVIEW with a means to register CMIU state with the Fpc server.
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to register</param>
        public StatusCode RegisterCmiuAfterSuccessfulPrepotTest(uint cmiuId)
        {
            log.DebugFormat("Stub RegisterCmiuAfterSuccessfulPrepotTest - {0}", cmiuId);
            /// Return NotImplemented
            return (StatusCode.WarningStubNotImplemented);
        }

        /// <summary>
        /// Provide LabVIEW with a means to register CMIU state with the Fpc server.
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to register</param>
        public StatusCode RegisterCmiuAfterSuccessfulPostpotTest(uint cmiuId)
        {
            log.DebugFormat("Stub RegisterCmiuAfterSuccessfulPostpotTest - {0}", cmiuId);
            /// Return NotImplemented
            return (StatusCode.WarningStubNotImplemented);
        }

                /// <summary>
        /// Provide caller with a means to check the CMIU state with the FPC server.
        /// </summary>
        /// <param name="cmiuId">Id of CMIU to check</param>
        /// <param name="cmiuLifecycleState">CmiuLifecycleState object to populate</param>
        /// <returns>StatusCode to reflect result of function call</returns>
        public StatusCode GetCmiuLifecycle(uint cmiuId, ref CmiuLifecycleStruct cmiuLifecycleState)
        {
            log.DebugFormat("Stub GetCmiuLifecycle - {0}", cmiuId);

            cmiuLifecycleState.miuId = cmiuId;
            cmiuLifecycleState.state.id = LifecycleState.PrePrePot.ToString();
            cmiuLifecycleState.state.timestamp = DateTime.UtcNow;

            /// Return NotImplemented
            return (StatusCode.WarningStubNotImplemented);
        }

        /// <summary>
        /// Provide LabVIEW with a means to get the status message behind a status code.
        /// </summary>
        /// <param name="statusCode">Code to parse into string</param>
        public string GetStatusCodeString(StatusCode statusCode)
        {
            log.DebugFormat("Stub GetStatusCodeString = {0}", statusCode.ToString());
            string returnString = statusCode.ToString();
            return returnString;
        }
        
        /// <summary>
        /// Provide caller with a means to get the APN network identifier string for a specific MNO.
        /// </summary>
        /// <param name="mno">Code to parse into string</param>
        /// <returns>String containing APN string</returns>
        public StatusCode GetApnString(ref ApnInfo apnInfo)
        {
            log.DebugFormat("Stub GetApnString - {0}", apnInfo.mno.ToString());
            apnInfo.apn = apnInfo.mno.ToString();
            /// Return NotImplemented
            return (StatusCode.WarningStubNotImplemented);
        }

        /// <summary>
        /// Provide caller with a means to get the FPC client assembly version
        /// </summary>
        /// <returns>String containing assembly version</returns>
        public string GetClientAssemblyVersion()
        {
            log.Debug("Stub GetClientAssemblyVersion");
            string assemblyFileLocation = System.Reflection.Assembly.GetExecutingAssembly().Location;
            FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(assemblyFileLocation);

            log.DebugFormat("FPC Client ({0}) Version = {1}",
                            assemblyFileLocation,
                            myFileVersionInfo.FileVersion);

            return myFileVersionInfo.FileVersion;
        }
        #endregion Service Configuration and Initialisation Functions
    }
}
