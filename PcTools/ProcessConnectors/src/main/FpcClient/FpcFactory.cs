﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using FpcClientLibrary;

namespace FpcClient
{
    public static class FpcFactory
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Factory for creating a FpcClient
        /// </summary>
        public static _IFpcClient CreateFpcClient()
        {
            log.Debug("Create Fpc Service.");
            return new FpcClientLibrary.FpcClient();
        }
    }
}
