﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Threading.Tasks;
using NeptuneCloudConnector.JsonModel;

namespace NeptuneCloudConnector
{
    public partial class NeptuneCloudRestClient
    {
        /// <summary>
        /// Request a session authentication token from mdce-integration
        /// Receive a partner key, validates it, and generate a token that is good for a configurable length of time.
        /// Get Token (Authenticate) (IF03)
        /// </summary>
        public virtual async Task<Token> GetAuthenticationTokenAsync()
        {
            return await DoGetAsync<Token>("token", "site_id", SiteId, "partner", PartnerString);
        }
    }
}
