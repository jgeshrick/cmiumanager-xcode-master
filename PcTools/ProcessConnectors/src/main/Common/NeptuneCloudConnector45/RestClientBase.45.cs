﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serializers;
using JsonSerializer = NeptuneCloudConnector.serializer.JsonSerializer;

namespace NeptuneCloudConnector
{
    public abstract partial class RestClientBase
    {
        /// <summary>
        /// Rest Get method wrapper
        /// <typeparam name="T">Client code must declare and instantiate a constructed type</typeparam>
        /// <param name="resource">Message type</param>
        /// <param name="parameterNameValuePair">Parameter key value pairs</param>
        /// </summary>
        protected async Task<T> DoGetAsync<T>(string resource, params string[] parameterNameValuePair)
            where T : new()
        {
            var client = new RestClient(mdceIntegrationBaseUrl);
            var request = new RestRequest(resource, Method.GET);
            var parameterNameValuePairLength = parameterNameValuePair.Length;
            log.Debug("DoGet");

            if ((parameterNameValuePairLength > 0) &&
                (parameterNameValuePairLength % 2 == 0))
            {
                for (int i = 0; i < parameterNameValuePair.Length; i += 2)
                {
                    request.AddParameter(parameterNameValuePair[i], parameterNameValuePair[i + 1]);
                }
            }

            // execute the request
            var response = await SendRequestAsync<T>(client, request);
            var content = response.Content; // raw content as string
            return response.Data;
        }

        /// <summary>
        /// Rest Get method wrapper
        /// <typeparam name="T">Client code must declare and instantiate a constructed type</typeparam>
        /// <param name="resource">Message type</param>
        /// <param name="parameterNameValuePair">Parameter key value pairs</param>
        /// </summary>
        protected async Task<T> DoGetAsync<T>(int apiVersion, string resource, params string[] parameterNameValuePair)
            where T : new()
        {
            var client = new RestClient(string.Format("{0}v{1}/", mdceIntegrationUnversionedBaseUrl, apiVersion));
            var request = new RestRequest(resource, Method.GET);
            var parameterNameValuePairLength = parameterNameValuePair.Length;
            log.Debug("DoGet");

            if ((parameterNameValuePairLength > 0) &&
                (parameterNameValuePairLength % 2 == 0))
            {
                for (int i = 0; i < parameterNameValuePair.Length; i += 2)
                {
                    request.AddParameter(parameterNameValuePair[i], parameterNameValuePair[i + 1]);
                }
            }

            // execute the request
            var response = await SendRequestAsync<T>(client, request);
            var content = response.Content; // raw content as string
            return response.Data;
        }

        /// <summary>
        /// Rest Post method wrapper
        /// <typeparam name="T">Client code must declare and instantiate a constructed type</typeparam>
        /// </summary>
        /// <param name="resource">Message type</param>
        /// <param name="requestObject">Data to send</param>
        /// <param name="parameterNameValuePair">Parameter key value pairs</param>
        protected async Task<bool> DoPostAsync<T>(string resource, T requestObject, params string[] parameterNameValuePair)
            where T : new()
        {
            var client = new RestClient(mdceIntegrationBaseUrl);
            var request = new RestRequest(resource, Method.POST);
            request.JsonSerializer = new JsonSerializer();
            log.Debug("DoPost");
            var parameterNameValuePairLength = parameterNameValuePair.Length;

            if ((parameterNameValuePairLength > 0) &&
                (parameterNameValuePairLength % 2 == 0))
            {
                for (int i = 0; i < parameterNameValuePair.Length; i += 2)
                {
                    request.AddQueryParameter(parameterNameValuePair[i], parameterNameValuePair[i + 1]);
                }
            }

            //add request content
            if (requestObject != null)
            {
                request.AddJsonBody(requestObject);
            }

            // execute the request
            var response = await SendRequestAsync(client, request);
            var content = response.Content; // raw content as string
            return true;
        }

        /// <summary>
        /// Rest Post method wrapper
        /// <typeparam name="TRequest">Client code must declare and instantiate a constructed type</typeparam>
        /// </summary>
        /// <param name="resource">Message type</param>
        /// <param name="requestObject">Data to send</param>
        /// <param name="parameterNameValuePair">Parameter key value pairs</param>
        protected async Task<TResponse> DoPostAsync<TRequest, TResponse>(string resource, TRequest requestObject, params string[] parameterNameValuePair)
            where TResponse : new()
        {
            if (requestObject == null)
            {
                throw new ArgumentNullException("requestObject");
            }

            var client = new RestClient(mdceIntegrationBaseUrl);
            var request = new RestRequest(resource, Method.POST);
            request.JsonSerializer = new NewtonsoftSerializer();
            log.Debug("DoPostAsync<TRequest, TResponse>");
            var parameterNameValuePairLength = parameterNameValuePair.Length;

            if ((parameterNameValuePairLength > 0) &&
                (parameterNameValuePairLength % 2 == 0))
            {
                for (int i = 0; i < parameterNameValuePair.Length; i += 2)
                {
                    request.AddQueryParameter(parameterNameValuePair[i], parameterNameValuePair[i + 1]);
                }
            }

            //add request content
            request.AddJsonBody(requestObject);

            // execute the request
            var response = await SendRequestAsync<TResponse>(client, request);

            return response.Data;
        }

        private class NewtonsoftSerializer : ISerializer
        {
            public NewtonsoftSerializer()
            {
                ContentType = "application/json";
            }

            public string Serialize(object obj)
            {
                return JsonConvert.SerializeObject(obj);
            }

            public string RootElement { get; set; }
            public string Namespace { get; set; }
            public string DateFormat { get; set; }
            public string ContentType { get; set; }
        }

        /// <summary>
        /// Rest Put method wrapper
        /// <typeparam name="T">Client code must declare and instantiate a constructed type</typeparam>
        /// </summary>
        /// <param name="resource">Message type</param>
        /// <param name="requestObject">Data to send</param>
        /// <param name="parameterNameValuePair">Parameter key value pairs</param>
        protected async Task<bool> DoPutAsync<T>(string resource, T requestObject, params string[] parameterNameValuePair)
            where T : new()
        {
            var client = new RestClient(mdceIntegrationBaseUrl);
            var request = new RestRequest(resource, Method.PUT)
            {
                Timeout = (int)TimeSpan.FromMinutes(10).TotalMilliseconds,
                JsonSerializer = new JsonSerializer()
            };

            log.Debug("DoPut");
            var parameterNameValuePairLength = parameterNameValuePair.Length;

            if ((parameterNameValuePairLength > 0) &&
                (parameterNameValuePairLength % 2 == 0))
            {
                for (int i = 0; i < parameterNameValuePair.Length; i += 2)
                {
                    request.AddQueryParameter(parameterNameValuePair[i], parameterNameValuePair[i + 1]);
                }
            }

            //add request content
            if (requestObject != null)
            {
                request.AddJsonBody(requestObject);
            }

            // execute the request
            var response = await SendRequestAsync(client, request);
            var content = response.Content; // raw content as string
            return true;
        }

        /// <summary>
        /// Sets the partner string to a new value.
        /// </summary>
        protected virtual void ChangePartnerString(string newPartnerString)
        {
            PartnerString = newPartnerString;
        }

        /// <summary>
        /// Wrap a HTTP REST request by executing the request, examine the response and throw an exception if
        /// there is an error executing the request.
        /// <typeparam name="T">Client code must declare and instantiate a constructed type</typeparam>
        /// </summary>
        /// <param name="client">REST client</param>
        /// <param name="request">Message request</param>
        /// <returns></returns>
        private static async Task<IRestResponse<T>> SendRequestAsync<T>(RestClient client, RestRequest request)
            where T : new()
        {
            var response = await client.ExecuteTaskAsync<T>(request);

            log.DebugFormat("Request url: {0}", RemovePartnerString(response.ResponseUri));
            // Don't log token for security reasons
            if (response.Content.Contains("auth_token"))
            {
                log.Debug("Response content: auth_token");
            }
            else
            {
                log.DebugFormat("Response content: {0}", response.Content);
            }
            CheckResponseIsOkElseThrowRestException(response);
            return response;
        }

        /// <summary>
        /// Wrap a HTTP REST request by executing the request, examine the response and throw an exception if
        /// there is an error executing the request.
        /// </summary>
        /// <param name="client">REST client</param>
        /// <param name="request">Message request</param>
        private static async Task<IRestResponse> SendRequestAsync(RestClient client, RestRequest request)
        {
            var response = await client.ExecuteTaskAsync(request);

            log.DebugFormat("Request url: {0}", RemovePartnerString(response.ResponseUri));
            // Don't log token for security reasons
            if (response.Content.Contains("auth_token"))
            {
                log.Debug("Response content: auth_token");
            }
            else
            {
                log.DebugFormat("Response content: {0}", response.Content);
            }

            CheckResponseIsOkElseThrowRestException(response);
            return response;
        }
    }
}
