﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeptuneCloudConnector.JsonModel
{
    /// <summary>
    /// Authenticated token response from server
    /// </summary>
    public class Token
    {
        public bool Authenticated { get; set; }

        public string AuthToken { get; set; }

        public DateTimeOffset? AuthenticatedAt { get; set; }

        public DateTimeOffset? Expires { get; set; }
    }
}
