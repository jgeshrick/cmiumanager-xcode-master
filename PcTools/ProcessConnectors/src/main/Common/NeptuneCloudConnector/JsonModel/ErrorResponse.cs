﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

namespace NeptuneCloudConnector.JsonModel
{
    /// <summary>
    /// POCO representing REST exception response
    /// </summary>
    public class ErrorResponse
    {
        public int ResultCode { get; set; }
        public string ErrorString { get; set; }

        public override string ToString()
        {
            return " Result code: " + this.ResultCode +
                " Error string: " + this.ErrorString;
        }
    }
}
