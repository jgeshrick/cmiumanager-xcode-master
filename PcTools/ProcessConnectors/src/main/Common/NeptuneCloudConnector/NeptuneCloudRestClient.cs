﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************
using NeptuneCloudConnector.JsonModel;
using System;

namespace NeptuneCloudConnector
{
    /// <summary>
    /// Concrete implementation of Rest client
    /// </summary>
    public partial class NeptuneCloudRestClient : RestClientBase
    {
        private const string DateFormatIso8601 = "yyyy-MM-dd";
        public string AuthenticationToken { get; private set; }

        /// <summary>
        /// Constructor for REST client
        /// </summary>
        /// <param name="partnerString">The Partner String</param>
        /// <param name="siteId">Site Id string</param>
        /// <param name="mdceIntegrationUrl">MDCE server URL string</param>
        public NeptuneCloudRestClient(string partnerString, string siteId, string mdceIntegrationUrl)
            : base(partnerString, siteId, mdceIntegrationUrl)
        {
        }

        /// <summary>
        /// Constructor for REST client
        /// </summary>
        /// <param name="partnerString">The Partner String</param>
        /// <param name="siteId">Site Id string</param>
        /// <param name="mdceIntegrationUrl">MDCE server base URL string</param>
        /// <param name="urlSubString">MDCE server URL sub-string</param>
        public NeptuneCloudRestClient(string partnerString, string siteId, string mdceIntegrationUrl, string urlSubString)
            : base(partnerString, siteId, mdceIntegrationUrl, urlSubString)
        {
        }

        #region Token
        /// <summary>
        /// Request a session authentication token from mdce-integration
        /// Receive a partner key, validates it, and generate a token that is good for a configurable length of time.
        /// Get Token (Authenticate) (IF03)
        /// </summary>
        public virtual Token GetAuthenticationToken()
        {
            return DoGet<Token>("token", "site_id", SiteId, "partner", PartnerString);
        }
        #endregion //Token
    }
}
