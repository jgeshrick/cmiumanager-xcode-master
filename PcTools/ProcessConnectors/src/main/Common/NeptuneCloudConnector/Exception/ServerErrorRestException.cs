﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;

namespace NeptuneCloudConnector.Exception
{
    /// <summary>
    /// Thrown on error handling REST transaction.
    /// </summary>
    public class ServerErrorRestException : RestException
    {
        public ServerErrorRestException()
        {
        }

        public ServerErrorRestException(string message)
            : base(message)
        {
        }

        public ServerErrorRestException(string message, System.Exception inner)
            : base(message, inner)
        {
        }
    }
}
