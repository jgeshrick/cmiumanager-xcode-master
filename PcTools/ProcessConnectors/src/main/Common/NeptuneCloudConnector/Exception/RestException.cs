﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;

namespace NeptuneCloudConnector.Exception
{
    /// <summary>
    /// Thrown on error handling REST transaction.
    /// </summary>
    [Serializable]
    public class RestException : System.Exception
    {
        public RestException()
        {
        }

        public RestException(string message)
            : base(message)
        {
        }

        public RestException(string message, System.Exception inner)
            : base(message, inner)
        {
        }
    }
}
