﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Specialized;
using System.Text;
using NeptuneCloudConnector.Exception;
using NeptuneCloudConnector.JsonModel;
using NeptuneCloudConnector.serializer;
using RestSharp;
using RestSharp.Deserializers;
using RestSharp.Extensions.MonoHttp;

namespace NeptuneCloudConnector
{
    /// <summary>
    /// Encapsulation of RESTful operation.
    /// </summary>
    public abstract partial class RestClientBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected readonly string mdceIntegrationBaseUrl;
        protected readonly string mdceIntegrationUnversionedBaseUrl;

        protected const string RestUrlUnversionedBasePath = "/mdce/api/";
        protected const string RestUrlBasePathSubString = "/mdce/api/v1/";

        public string PartnerString { get; private set; }
        public string SiteId { get; private set; }

        /// <summary>
        /// Constructor for REST client base class
        /// </summary>
        /// <param name="partnerString">PartnerString</param>
        /// <param name="siteId">Site Id string</param>
        /// <param name="mdceIntegrationUrl">MDCE server URL string</param>
        public RestClientBase(string partnerString, string siteId, string mdceIntegrationUrl)
            // ReSharper disable once IntroduceOptionalParameters.Global
            : this(partnerString, siteId, mdceIntegrationUrl, RestUrlBasePathSubString, RestUrlUnversionedBasePath)
        {
        }

        /// <summary>
        /// Constructor for REST client base class
        /// </summary>
        /// <param name="partnerString">PartnerString</param>
        /// <param name="siteId">Site Id string</param>
        /// <param name="mdceIntegrationUrl">MDCE server base URL string</param>
        /// <param name="urlSubstring">MDCE server URL sub-string</param>
        public RestClientBase(string partnerString, string siteId, string mdceIntegrationUrl, string urlSubstring)
            : this(partnerString, siteId, mdceIntegrationUrl, urlSubstring, urlSubstring)
        {
        }

        /// <summary>
        /// Constructor for REST client base class
        /// </summary>
        /// <param name="partnerString">PartnerString</param>
        /// <param name="siteId">Site Id string</param>
        /// <param name="mdceIntegrationUrl">MDCE server base URL string</param>
        /// <param name="urlSubstring">MDCE server URL sub-string</param>
        /// <param name="unversionedUrlSubstring">MDCE server URL sub-string without an API version</param>
        public RestClientBase(string partnerString, string siteId, string mdceIntegrationUrl, string urlSubstring, string unversionedUrlSubstring)
        {
            PartnerString = partnerString;
            SiteId = siteId;
            mdceIntegrationBaseUrl = mdceIntegrationUrl + urlSubstring;
            mdceIntegrationUnversionedBaseUrl = unversionedUrlSubstring != null ? mdceIntegrationUrl + unversionedUrlSubstring : null;
        }

        /// <summary>
        /// Rest Get method wrapper
        /// <typeparam name="T">Client code must declare and instantiate a constructed type</typeparam>
        /// <param name="resource">Message type</param>
        /// <param name="parameterNameValuePair">Parameter key value pairs</param>
        /// </summary>
        protected T DoGet<T>(string resource, params string[] parameterNameValuePair)
            where T : new()
        {
            var client = new RestClient(mdceIntegrationBaseUrl);
            var request = new RestRequest(resource, Method.GET);
            var parameterNameValuePairLength = parameterNameValuePair.Length;
            log.Debug("DoGet");

            if ((parameterNameValuePairLength > 0) &&
                (parameterNameValuePairLength % 2 == 0))
            {
                for (int i = 0; i < parameterNameValuePair.Length; i += 2)
                {
                    request.AddParameter(parameterNameValuePair[i], parameterNameValuePair[i + 1]);
                }
            }

            // execute the request
            var response = SendRequest<T>(client, request);
            var content = response.Content; // raw content as string
            return response.Data;
        }

        /// <summary>
        /// Rest Post method wrapper
        /// <typeparam name="T">Client code must declare and instantiate a constructed type</typeparam>
        /// </summary>
        /// <param name="resource">Message type</param>
        /// <param name="requestObject">Data to send</param>
        /// <param name="parameterNameValuePair">Parameter key value pairs</param>
        protected IRestResponse DoPost<T>(string resource, T requestObject, params string[] parameterNameValuePair)
            where T : new()
        {
            var client = new RestClient(mdceIntegrationBaseUrl);
            var request = new RestRequest(resource, Method.POST);
            request.JsonSerializer = new JsonSerializer();
            log.Debug("DoPost");
            var parameterNameValuePairLength = parameterNameValuePair.Length;

            if ((parameterNameValuePairLength > 0) &&
                (parameterNameValuePairLength % 2 == 0))
            {
                for (int i = 0; i < parameterNameValuePair.Length; i += 2)
                {
                    request.AddQueryParameter(parameterNameValuePair[i], parameterNameValuePair[i + 1]);
                }
            }

            //add request content
            if (requestObject != null)
            {
                request.AddJsonBody(requestObject);
            }

            // execute the request
            IRestResponse response = SendRequest(client, request);
            return response;
        }

        /// <summary>
        /// Rest Put method wrapper
        /// <typeparam name="T">Client code must declare and instantiate a constructed type</typeparam>
        /// </summary>
        /// <param name="resource">Message type</param>
        /// <param name="requestObject">Data to send</param>
        /// <param name="parameterNameValuePair">Parameter key value pairs</param>
        protected bool DoPut<T>(string resource, T requestObject, params string[] parameterNameValuePair)
            where T : new()
        {
            var client = new RestClient(mdceIntegrationBaseUrl);
            var request = new RestRequest(resource, Method.PUT)
            {
                Timeout = (int) TimeSpan.FromMinutes(10).TotalMilliseconds,
                JsonSerializer = new JsonSerializer()
            };

            log.Debug("DoPut");
            var parameterNameValuePairLength = parameterNameValuePair.Length;

            if ((parameterNameValuePairLength > 0) &&
                (parameterNameValuePairLength % 2 == 0))
            {
                for (int i = 0; i < parameterNameValuePair.Length; i += 2)
                {
                    request.AddQueryParameter(parameterNameValuePair[i], parameterNameValuePair[i + 1]);
                }
            }

            //add request content
            if (requestObject != null)
            {
                request.AddJsonBody(requestObject);
            }

            // execute the request
            var response = SendRequest(client, request);
            var content = response.Content; // raw content as string
            return true;
        }

        /// <summary>
        /// Wrap a HTTP REST request by executing the request, examine the response and throw an exception if
        /// there is an error executing the request.
        /// <typeparam name="T">Client code must declare and instantiate a constructed type</typeparam>
        /// </summary>
        /// <param name="client">REST client</param>
        /// <param name="request">Message request</param>
        private static IRestResponse<T> SendRequest<T>(RestClient client, RestRequest request)
            where T : new()
        {
            var response = client.Execute<T>(request);

            log.DebugFormat("Request url: {0}", RemovePartnerString(response.ResponseUri));
            // Don't log token for security reasons
            if (response.Content.Contains("auth_token"))
            {
                log.Debug("Response content: auth_token");   
            }
            else
            {
                log.DebugFormat("Response content: {0}", response.Content);
            }
            CheckResponseIsOkElseThrowRestException(response);
            return response;
        }

        /// <summary>
        /// Wrap a HTTP REST request by executing the request, examine the response and throw an exception if
        /// there is an error executing the request.
        /// </summary>
        /// <param name="client">REST client</param>
        /// <param name="request">Message request</param>
        private static IRestResponse SendRequest(RestClient client, RestRequest request)
        {
            var response = client.Execute(request);

            log.DebugFormat("Request url: {0}", RemovePartnerString(response.ResponseUri));
            // Don't log token for security reasons
            if (response.Content.Contains("auth_token"))
            {
                log.Debug("Response content: auth_token");
            }
            else
            {
                log.DebugFormat("Response content: {0}", response.Content);
            }

            CheckResponseIsOkElseThrowRestException(response);
            return response;
        }

        /// <summary>
        /// Check HTTP reponse from server, parse for certain errors and throw a restexception if appropriate
        /// </summary>
        /// <param name="response">HTTP response from server</param>
        private static bool CheckResponseIsOkElseThrowRestException(IRestResponse response)
        {
            if (response.ErrorException != null)
            {
                var error = ExtractMdceErrorResponseString(response);

                log.Error("Error in REST: " + response.ErrorMessage + error.ToString(), response.ErrorException);

                throw new ServerErrorRestException("Error in REST operation: " + response.ErrorMessage, response.ErrorException);
            }
            else if (response.StatusCode != System.Net.HttpStatusCode.OK && 
                response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                var error = ExtractMdceErrorResponseString(response);
                var errorMessage = string.Format("HTTP response status is not OK! Response Status: {0} [{1}]", response.StatusCode, (int)response.StatusCode);
                log.Error(errorMessage + error.ToString(), response.ErrorException);

                ResponseErrorRestException restException = new ResponseErrorRestException(errorMessage + error.ToString(), response.ErrorException);
                restException.responseError = response.StatusCode;
                throw restException;
            }
            return true;
        }

        /// <summary>
        /// Extract error from server response
        /// </summary>
        /// <param name="response">HTTP response from server</param>
        /// <returns>String with deserialized error.</returns>
        private static string ExtractMdceErrorResponseString(IRestResponse response)
        {
            JsonDeserializer deser = new JsonDeserializer();
            try
            {
                var error = deser.Deserialize<ErrorResponse>(response);
                return error.ToString();
            }
            catch (System.Exception ex)
            {
                log.Error("Error deserializing error response. Response content " + response.Content, ex);

                return response.Content;
            }
        }

        /// <summary>
        /// Returns a copy of the supplied URI with any occurrences of the partner string removed 
        /// from the query string.
        /// </summary>
        private static Uri RemovePartnerString(Uri uri)
        {
            if (uri == null || string.IsNullOrEmpty(uri.Query))
            {
                return uri;
            }

            Uri modifiedUri = uri;
            bool wasModified = false;
            NameValueCollection queryParameters = HttpUtility.ParseQueryString(uri.Query);

            // Remove occurrences of the partner string
            var keys = queryParameters.AllKeys;
            if (queryParameters.Count != 0)
            {
                foreach (string key in keys)
                {
                    if ("partner".Equals(key, StringComparison.OrdinalIgnoreCase))
                    {
                        queryParameters[key] = "********";
                        wasModified = true;
                    }
                }
            }

            // Rebuild if the partner string was encountered
            if (wasModified)
            {
                modifiedUri = new Uri(string.Format("{0}?{1}", uri.GetLeftPart(UriPartial.Path), queryParameters));
            }

            return modifiedUri;
        }
    }
}
