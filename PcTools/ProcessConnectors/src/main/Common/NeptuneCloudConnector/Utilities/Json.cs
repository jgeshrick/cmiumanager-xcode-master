﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using Newtonsoft.Json;

namespace NeptuneCloudConnector.Utilities
{
    /// <summary>
    /// Utility class for JSON conversion
    /// </summary>
    public static class Json
    {
        public static string ToJson(object objToConvertToJson)
        {
            return JsonConvert.SerializeObject(objToConvertToJson);
        }
    }
}
