//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Security.Principal;

namespace Ntg.BpcWebService.Authorization
{
    /// <summary>
    /// Provides methods used to determine a user's access level.
    /// </summary>
    public interface IAccessTypeResolver
    {
        /// <summary>
        /// Determines whether the supplied user has membership of any of the configured administrative roles.
        /// </summary>
        /// <param name="user">The user principal under test.</param>
        /// <returns>True if the supplied user is a member of a BPC administrative security group; otherwise, false.</returns>
        bool IsAdminUser(IPrincipal user);
    }
}