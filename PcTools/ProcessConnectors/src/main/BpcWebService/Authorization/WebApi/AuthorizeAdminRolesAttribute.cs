﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Configuration;
using System.Web.Http;

namespace Ntg.BpcWebService.Authorization.WebApi
{
    /// <summary>
    /// Specifies an authorization filter based on the roles configured in appSettings/AdminRoles.
    /// </summary>
    public class AuthorizeAdminRolesAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Initializes the base <see cref="AuthorizeAttribute"/> using the roles configured in appSettings/AdminRoles.
        /// </summary>
        public AuthorizeAdminRolesAttribute()
        {
            base.Roles = ConfigurationManager.AppSettings.Get("AdminRoles");
        }
    }
}