﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using log4net;
using Ntg.BpcCore;

namespace Ntg.BpcWebService.Authorization
{
    /// <summary>
    /// Determines a user's access level.
    /// </summary>
    public class AccessTypeResolver : IAccessTypeResolver
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IReadOnlyList<string> adminRoles;

        /// <summary>
        /// Creates a new instance of <see cref="AccessTypeResolver"/> using the supplied list of administrative roles.
        /// </summary>
        /// <param name="adminRolesSetting">
        /// A comma-separated list of Active Directory security groups whose members are BPC administrators.
        /// </param>
        public AccessTypeResolver(string adminRolesSetting)
        {
            if (string.IsNullOrEmpty(adminRolesSetting))
            {
                adminRoles = null;
                Log.Info(10033, "AccessTypeResolver.ctor: No BPC administrator roles are configured.");
            }
            else
            {
                adminRoles = adminRolesSetting.Split(',').Select(r => r.Trim()).ToList();
                Log.Info(10034, string.Format("AccessTypeResolver.ctor: Initialized with BPC administrator roles: {0}.", adminRolesSetting));
            }
        }

        /// <summary>
        /// Determines whether the supplied user has membership of any of the configured administrative roles.
        /// </summary>
        /// <param name="user">The user principal under test.</param>
        /// <returns>True if the supplied user is a member of a BPC administrative security group; otherwise, false.</returns>
        public bool IsAdminUser(IPrincipal user)
        {
            bool isAdminUser = false;

            if (user != null && adminRoles != null)
            {
                foreach (string role in adminRoles)
                {
                    if (user.IsInRole(role))
                    {
                        isAdminUser = true;
                        break;
                    }
                }
            }

            return isAdminUser;
        }
    }
}