﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

// Page initialization
$(function () {
    // Wire up partner string click events
    $(".start-all-services").click(function (e) { controlAllServices(e, "start"); e.preventDefault(); });
    $(".stop-all-services").click(function (e) { controlAllServices(e, "stop"); e.preventDefault(); });
    $(".setPartnerString").click(function (e) { setPartnerStringClicked(e); e.preventDefault(); });
    $(".applyPartnerString").click(function (e) { applyPartnerStringClicked(e); e.preventDefault(); });
    $(".cancelPartnerString").click(function (e) { cancelPartnerStringClicked(e); e.preventDefault(); });
    $(".start-service").click(function (e) { controlService(e, "start"); e.preventDefault(); });
    $(".stop-service").click(function (e) { controlService(e, "stop"); e.preventDefault(); });
    $(".run-service").click(function (e) { controlService(e, "run"); e.preventDefault(); });

    refreshServerStatus();
});

function setPartnerStringClicked(event) {
    var div = $(event.target).closest("div.partnerStringEditor");
    $(event.target).addClass("bpc-hidden");
    $(div).find("span").removeClass("partnerStringInput");
}

function cancelPartnerStringClicked(event) {
    var div = $(event.target).closest("div.partnerStringEditor");
    $(div).find("input[type=password]").val("");
    $(div).find("span").addClass("partnerStringInput");
    $(div).find(".setPartnerString").removeClass("bpc-hidden");
}

function applyPartnerStringClicked(event) {
    var name = $(event.target).closest("div.partnerStringEditor").attr("environment-name");
    var div = $(event.target).closest("div.partnerStringEditor");
    var partnerString = $(div).find("input[type=password]").val();
    $(div).find(".setPartnerString").removeClass("bpc-hidden");

    $(div).find("input[type=password]").val("");
    $(div).find("span").addClass("partnerStringInput");

    var requestData = {
        "environment_name": name,
        "partner_string": partnerString
    };

    $.ajax({
        url: getApplicationRoot() + "api/v1/config/partnerstring",
        type: "post",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(requestData),
        success: function() {
            $("span.partnerStringStatus[environment-name='" + name + "']").text("OK");
        },
        error: function(jqXhr, textStatus, errorThrown) {
            $("span.partnerStringStatus[environment-name='" + name + "']").text("ERROR");
        }
    });
}

// Request the status of all services
function refreshServerStatus() {
    $.ajax({
        url: getApplicationRoot() + "api/v1/services",
        type: "get",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: handleServerStatusOk,
        error: handleServerStatusError
    });

    setTimeout(refreshServerStatus, 5000);
}

// Update the status screen based on a response from api/v1/services
function handleServerStatusOk(data, textStatus, jqXhr) {
    $("#started-stopped-services").attr("class", "bpc-inline");
    $("#status-error").attr("class", "bpc-hidden");

    $("span#last-exec-time").text(data.last_execution_time);
    $("span#are-services-executing").text(data.are_services_executing);
    $("span#started-services-count").text(data.started_count);
    $("span#stopped-services-count").text(data.stopped_count);

    for (var key in data.services) {
        var service = data.services[key];
        var serviceType = service.service_type;
        var div = $("div[service-type='" + serviceType + "']");
        $(div).find(".service-status").text(service.status + " " + service.run_duration);
        $(div).find(".service-exec-schedule").text(service.execution_schedule);
        $(div).find(".service-last-exec").text(service.last_executed_time);
        $(div).find(".service-last-exec-result").text(service.last_execution_result);
        $(div).find(".service-next-exec").text(service.next_execution_time);
    };
}

// Show error detail if the call to api/v1/services fails
function handleServerStatusError(jqXhr, textStatus, errorThrown) {
    $("#started-stopped-services").attr("class", "bpc-hidden");
    $("#status-error").attr("class", "bpc-inline");
    $("#status-error-info").text("[" + textStatus + "] Status update failure. " + errorThrown);
}

// Send a service control command for all services (start/stop)
function controlAllServices(event, command) {
    var parameters = {
        "command": command
    };

    // Send the command and request a status update
    $.ajax({
        url: getApplicationRoot() + "api/v1/services/",
        type: "post",
        data: JSON.stringify(parameters),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: handleServerStatusOk,
        error: handleServerStatusError
    });
}

// Send a service control command (start/stop/run)
function controlService(event, command) {
    var div = $(event.target).closest("div.service-status");
    var serviceType = $(div).attr("service-type");
    var parameters = {
        "command": command
    };

    // Send the command and request a status update
    $.ajax({
        url: getApplicationRoot() + "api/v1/services/" + serviceType,
        type: "post",
        data: JSON.stringify(parameters),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: handleServerStatusOk,
        error: handleServerStatusError
    });
}
