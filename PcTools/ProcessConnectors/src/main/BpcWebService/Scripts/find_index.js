﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

// Page initialization
$(function () {

    // Wire up element events
    $("#find-endpoint-id").change(function () { endpointIdChanged(); });
    $(".find-input").keyup(function () { endpointIdChanged(); });
    $("#find-endpoint-go").click(function () { goButtonClicked(); });
    $(".find-input").keypress(function(e) {
        // Submit when enter is pressed
        if (e.which === 13) {
            e.preventDefault();
            $("#find-endpoint-go").click();
        }
    });
});

// Sets a cookie to record the last selected ID type
function endpointIdChanged() {

    // Change the selected MIU type based on the number of digits in the entered ID
    var enteredId = $("#find-endpoint-id").val();
    var endpointTypeInput = $("#find-endpoint-id-type");
    var enteredIdType = getMiuIdType(enteredId);
    endpointTypeInput.val(enteredIdType);
    var idTypeText = $("#find-endpoint-id-type option:selected").text();
    $("#endpoint-id-type-span").text(idTypeText);
    $("#find-endpoint-go").prop("disabled", enteredIdType === "");
}

// Detect the type of ID the user has entered
function getMiuIdType(number) {

    // Check for null string
    if (!number) {
        return "";
    }

    // Strip out slashes, dashes and spaces
    var digits = number
        .replace(/\\/g, "")
        .replace(/\//g, "")
        .replace(/-/g, "") // Hyphen
        .replace(/–/g, "") // En-dash
        .replace(/—/g, "") // Em-dash
        .replace(/ /g, "");

    // Check for EUI format: 16 hex digits
    if (digits.match(/^[0-9a-fA-F]{16}$/)) {
        return "Eui";
    }

    // Check for non-numeric characters
    if (digits.match(/[^\d]/)) {
        return "";
    }

    // Check for known lengths of digits
    if (digits.length >= 9 && digits.length <= 10 && parseInt(digits) <= 2147483647) {
        return "MiuId";
    } else if (digits.length === 15 || digits.length === 16) {
        // Could be an EUI but, in reality, the OUI part of the EUI contains hex digits.
        return "Imei";
    } else if (digits.length >= 19 && digits.length <= 20) {
        return "Iccid";
    } else {
        return "";
    }
}

function goButtonClicked() {

    var id = $("#find-endpoint-id").val();
    var idType = $("#find-endpoint-id-type option:selected").val();
    var idTypeText = $("#find-endpoint-id-type option:selected").text();

    // Show input parameters
    $("#find-results-id-type").text(idTypeText);
    $("#find-results-id").text(id);

    // Show the results div
    $("#find-results-div").attr("class", "");

    showSpinner();

    // Begin the search
    $.ajax({
        url: getApplicationRoot() + "api/v1/miu_list?find_id=" + encodeURIComponent(id),
        type: "get",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: handleFindResultsOk,
        error: handleFindResultsError
    });
}

function showSpinner() {
    var opts = {
        lines: 13,
        length: 4,
        width: 2,
        radius: 5,
        corners: 1,
        className: 'spinner',
        zIndex: 2e9,
        top: '50%',
        left: '50%',
        visibility: true,
        color: '#aaa'
    };

    var target = document.getElementById('spinner');
    target.style.display = "inline";
    new Spinner(opts).spin(target);
}

function hideSpinner() {
    var target = document.getElementById('spinner');
    target.innerHTML = "";
}

// Show Find results
function handleFindResultsOk(data, textStatus, jqXhr) {
    hideSpinner();

    $("#find-results-div").attr("class", "bpc-inline");
    $("#status-error").attr("class", "bpc-hidden");
    var tbody = $("#find-results-table").children("tbody");
    tbody.empty();
    $.each(data.results,
        function (i, row) {
            var html =
                "<tr>" +
                "<td>" + row.environment_name + "</td>" +
                "<td>" + row.miu_id + "</td>" +
                "<td>" + row.iccid + "</td>" +
                "<td>" + row.imei + "</td>" +
                "<td>" + row.eui + "</td>" +
                "<td>" + row.last_insert_date_string + "</td>" +
                "</tr>";
            tbody.append($(html));
        });
    $.each(data.errors,
        function (i, row) {
            var error = $('<div/>').text(row.error).html();
            var html =
                "<tr>" +
                "<td><span class=\"bpc-error\" title=\"" + error + "\">" + row.environment_name + " (error)</span></td>" +
                "<td><span class=\"bpc-error\">--</span></td>" +
                "<td><span class=\"bpc-error\">--</span></td>" +
                "<td><span class=\"bpc-error\">--</span></td>" +
                "<td><span class=\"bpc-error\">--</span></td>" +
                "</tr>";
            tbody.append($(html));
        });
}

// Show error detail
function handleFindResultsError(jqXhr, textStatus, errorThrown) {
    hideSpinner();

    $("#find-results-div").attr("class", "bpc-hidden");
    $("#status-error").attr("class", "bpc-inline");
    $("#status-error-info").text("[" + textStatus + "] The search could not be completed. " + errorThrown);
}
