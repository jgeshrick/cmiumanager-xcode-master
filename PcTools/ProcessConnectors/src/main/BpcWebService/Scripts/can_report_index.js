﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

// Page initialization
$(function () {
    $("input:radio[name='group']")
        .change(function() {
            showSelectedReportInputType(this);
        });

    showSelectedReportInputType();
});

function showSelectedReportInputType() {
    if ($("input[name='group']:checked").val() === "csv") {
        $("#csv-section").css("display", "block");
        $("#text-section").css("display", "none");
    } else {
        $("#csv-section").css("display", "none");
        $("#text-section").css("display", "block");
    }
}