//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Configuration;
using System.IO;
using Microsoft.Practices.Unity;
using Ntg.BpcCore;
using Ntg.BpcCore.Domain;
using Ntg.BpcCore.Scheduling;
using Ntg.BpcCore.ServiceProxies;
using Ntg.BpcCore.Settings;
using Ntg.BpcWebService.Authorization;
using Ntg.BpcWebService.Models;

namespace Ntg.BpcWebService.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();
            string configFilePath = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory) + @"\App_Data\environments.xml";
            string excludedEnvironments = ConfigurationManager.AppSettings.Get("ExcludedEnvironments");

            container.RegisterType<IScheduler, Scheduler>(new ContainerControlledLifetimeManager());
            container.RegisterType<IEnvironmentsConfig, Environments>(new ContainerControlledLifetimeManager(), new InjectionFactory(
                uc => Environments.ReadFromXml(configFilePath, excludedEnvironments)));

            container.RegisterType<IBpcRestClients, BpcRestClients>(new ContainerControlledLifetimeManager());
            container.RegisterType<IServiceSettingsRepository, RegistryServiceSettingsRepository>(new ContainerControlledLifetimeManager());
            container.RegisterType<IEnvironmentSettingsRepository, RegistryEnvironmentSettingsRepository>(new ContainerControlledLifetimeManager());
            container.RegisterType<IMiuListMultiServerProxy, MiuListMultiServerProxy>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICostRevenueDataRepository, CostRevenueDataRepository>(
                new ContainerControlledLifetimeManager(), new InjectionFactory(
                    uc => Environments.ReadFromXml(configFilePath, excludedEnvironments).CostAndRevenueServerDataServer));
            
            // Initialize the AccessTypeResolver using current configuration.
            container.RegisterType<IAccessTypeResolver, AccessTypeResolver>(new ContainerControlledLifetimeManager(), new InjectionFactory(
                uc => new AccessTypeResolver(ConfigurationManager.AppSettings.Get("AdminRoles"))));

            // Initialize the TimeZoneFormatter using current configuration.
            container.RegisterType<ITimeZoneFormatter, TimeZoneFormatter>(new ContainerControlledLifetimeManager(), new InjectionFactory(
                uc => new TimeZoneFormatter(ConfigurationManager.AppSettings.Get("TimeZoneName"))));
        }
    }
}
