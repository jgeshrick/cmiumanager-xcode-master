﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using Ntg.BpcCore;

namespace Ntg.BpcWebService.Handlers
{
    /// <summary>
    /// Provides request and response logging for the REST APIs.
    /// </summary>
    public class WebApiLoggingHandler : DelegatingHandler
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        /// <summary>
        /// Defines the service calls that will not be logged.
        /// </summary>
        private static readonly Tuple<HttpMethod, string>[] ExcludedServiceCalls = 
        {
            new Tuple<HttpMethod, string>(HttpMethod.Get, "api/v1/services")
        };

        /// <summary>
        /// Defines the service calls whose request data will be filtered.
        /// </summary>
        private static readonly Tuple<HttpMethod, string, Func<string, string>>[] FilteredServiceCalls = 
        {
            new Tuple<HttpMethod, string, Func<string, string>>(
                HttpMethod.Post, "api/v1/config/partnerstring", WebApiLoggingFilters.CensorPartnerStringSetting)
        };

        /// <summary>
        /// Adds request and response logging to the REST API call.
        /// </summary>
        /// <param name="request">The HTTP request object.</param>
        /// <param name="cancellationToken">A cancellation token to cancel operation.</param>
        /// <returns>The task object representing the asynchronous operation.</returns>
        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            string requestBody = await request.Content.ReadAsStringAsync();
            bool canLog = IsLoggingAllowed(request);

            // Log request
            if (canLog)
            {
                Log.Debug(10030, string.Format("Request: {0} {1}. Correlation ID: {2}. \r\n\tContent: {3}",
                    request.Method, request.RequestUri, request.GetCorrelationId(), ApplyRequestBodyFilters(request, requestBody)));
            }

            // Execute the API request
            var result = await base.SendAsync(request, cancellationToken);

            // Log response
            if (canLog)
            {
                if (result.Content != null)
                {
                    var responseBody = await result.Content.ReadAsStringAsync();
                    Log.Debug(10031, string.Format("Response: Correlation ID: {0}. Status: {1} ({2}). \r\n\tContent: {3}", 
                        request.GetCorrelationId(), (int)result.StatusCode, result.StatusCode, responseBody));
                }
            }

            return result;
        }

        private static bool IsLoggingAllowed(HttpRequestMessage requestMessage)
        {
            string servicePath = requestMessage.RequestUri.PathAndQuery;

            // Disallow logging of any request whose path and HTTP method matches any in the exclusions list
            return !ExcludedServiceCalls.Any(esc => 
                esc.Item1 == requestMessage.Method &&
                servicePath.EndsWith(esc.Item2, StringComparison.OrdinalIgnoreCase));
        }

        private static string ApplyRequestBodyFilters(HttpRequestMessage requestMessage, string requestBody)
        {
            string servicePath = requestMessage.RequestUri.PathAndQuery;

            // Check whether any filters should be applied to this request
            var filterMethods = FilteredServiceCalls
                .Where(fsc => fsc.Item1 == requestMessage.Method &&
                              servicePath.EndsWith(fsc.Item2, StringComparison.OrdinalIgnoreCase))
                .Select(fsc => fsc.Item3);

            // Apply filters
            string filteredRequestBody = requestBody;
            foreach (var filterMethod in filterMethods)
            {
                filteredRequestBody = filterMethod(filteredRequestBody);
            }

            return filteredRequestBody;
        }
    }
}