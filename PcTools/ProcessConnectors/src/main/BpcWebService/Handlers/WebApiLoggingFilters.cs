﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using Newtonsoft.Json;
using Ntg.BpcWebService.Models;

namespace Ntg.BpcWebService.Handlers
{
    /// <summary>
    /// Provides methods for processing REST API message content before logging.
    /// </summary>
    internal static class WebApiLoggingFilters
    {
        private const string Redacted = "********";

        /// <summary>
        /// Redacts the partner string from the provided <see cref="PartnerStringSetting"/>.
        /// </summary>
        /// <param name="message">
        /// The body content of the HTTP message, which is expected to be deserializable from JSON to 
        /// an instance of <see cref="PartnerStringSetting"/>.
        /// </param>
        /// <returns>
        /// If redactions have been made, a new JSON-serialized copy of the message with the redactions 
        /// included; otherwise, the original message content.
        /// </returns>
        internal static string CensorPartnerStringSetting(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
                return message;

            string returnedMessage;
            var partnerStringSetting = JsonConvert.DeserializeObject<PartnerStringSetting>(message);
            if (partnerStringSetting != null)
            {
                partnerStringSetting.PartnerString = Redacted;
                returnedMessage = JsonConvert.SerializeObject(partnerStringSetting);
            }
            else
            {
                returnedMessage = message;
            }

            return returnedMessage;
        }
    }
}