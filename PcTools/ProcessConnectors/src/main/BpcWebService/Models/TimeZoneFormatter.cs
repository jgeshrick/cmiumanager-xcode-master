﻿using System;
using System.Text.RegularExpressions;
using log4net;
using Ntg.BpcCore;

namespace Ntg.BpcWebService.Models
{
    /// <summary>
    /// Converts dates and times to a specific string format and time zone.
    /// </summary>
    public class TimeZoneFormatter : ITimeZoneFormatter
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly TimeZoneInfo timeZoneInfo;

        /// <summary>
        /// Creates a new instance of <see cref="TimeZoneFormatter"/> using the specified time zone.
        /// </summary>
        /// <param name="timeZoneId">
        /// The time zone identifier, which must the <code>Id</code> property of one of the 
        /// <see cref="TimeZoneInfo"/> instances returned by the static 
        /// <see cref="TimeZoneInfo"/>.<code>GetSystemTimeZones()</code> method.
        /// </param>
        public TimeZoneFormatter(string timeZoneId)
        {
            try
            {
                timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                
                Log.Debug(10038, string.Format("Using time zone: {0}.", timeZoneInfo.Id));
            }
            catch (Exception exception)
            {
                timeZoneInfo = TimeZoneInfo.Local;
                
                Log.Error(
                    10039, 
                    string.Format(
                        "Unable to instantiate time zone with identifier \"{0}\". Defaulting to local time zone \"{1}\".", 
                        timeZoneId, timeZoneInfo.Id), 
                    exception);
            }
        }

        /// <summary>
        /// Converts the provided string to a date & time string showing the offset for the configured time zone.
        /// </summary>
        /// <param name="dateTimeWithOffset">The date & time string to be converted.</param>
        /// <returns>
        /// If the provided string was successfully parsed, the converted string representation of the 
        /// date & time with offset; otherwise, the original value of <code>dateTimeWithOffset</code>.
        /// </returns>
        public string ToConfiguredTimeZone(string dateTimeWithOffset)
        {
            string result = dateTimeWithOffset;

            if (!string.IsNullOrWhiteSpace(dateTimeWithOffset))
            {
                dateTimeWithOffset = EnsureOffset(dateTimeWithOffset);
                DateTimeOffset dateTimeOffset;
                if (DateTimeOffset.TryParse(dateTimeWithOffset, out dateTimeOffset))
                {
                    var convertedTime = TimeZoneInfo.ConvertTime(dateTimeOffset, timeZoneInfo);

                    result = string.Format("{0:yyyy-MM-dd HH:mm:ss zzz}", convertedTime);
                }
            }

            return result;
        }

        /// <summary>
        /// Converts the DateTime to a string showing the offset for the configured time zone.
        /// </summary>
        /// <param name="dateTimeUtc">The DateTime to be converted.</param>
        /// <returns>
        /// If the provided DateTime? has a value, the converted string representation of the 
        /// date & time with offset; otherwise, <code>null</code>.
        /// </returns>
        public string ToConfiguredTimeZone(DateTime? dateTimeUtc)
        {
            string result = null;

            if (dateTimeUtc.HasValue)
            {
                DateTimeOffset dateTimeOffset = new DateTimeOffset(dateTimeUtc.Value, TimeSpan.Zero);
                DateTimeOffset convertedTime = TimeZoneInfo.ConvertTime(dateTimeOffset, timeZoneInfo);

                result = string.Format("{0:yyyy-MM-dd HH:mm:ss zzz}", convertedTime);
            }

            return result;
        }

        /// <summary>
        /// Checks for a time offset or UTC indicator in the supplied date-time string and appends UTC if one is not found.
        /// </summary>
        private string EnsureOffset(string dateTimeWithOffset)
        {
            if (dateTimeWithOffset.EndsWith("Z"))
            {
                return dateTimeWithOffset;
            }

            var regex = new Regex(@"[\+-][0-9]{1,2}:[0-9]{2}$");
            if (regex.IsMatch(dateTimeWithOffset))
            {
                return dateTimeWithOffset;
            }

            return dateTimeWithOffset.Trim() + "Z";
        }
    }
}