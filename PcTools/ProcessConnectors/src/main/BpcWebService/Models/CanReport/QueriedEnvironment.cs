﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

namespace Ntg.BpcWebService.Models.CanReport
{
    /// <summary>
    /// Status information for each queried MDCE environment.
    /// </summary>
    public class QueriedEnvironment
    {
        public string EnvironmentName { get; set; }
        
        public bool WasResponseOk { get; set; }
        
        public string ErrorDetails { get; set; }
        
        public string Empty { get { return string.Empty; } }

        public string WasResponseOkDisplayValue
        {
            get { return WasResponseOk ? "Y" : "N"; }
        }
    }
}