﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;

namespace Ntg.BpcWebService.Models.CanReport
{
    /// <summary>
    /// Contains an MIU result for inclusion in the CAN report.
    /// </summary>
    public class CanResult
    {
        /// <summary>
        /// The MIU ID.
        /// </summary>
        public int MiuId { get; set; }
        
        /// <summary>
        /// The environment that heard most recently from MIU.
        /// </summary>
        public string Environment { get; set; }
        
        /// <summary>
        /// The last-heard timestamp.
        /// </summary>
        public DateTime? LastHeard { get; set; }
        
        /// <summary>
        /// The ICCID of a CMIU.
        /// </summary>
        public string Iccid { get; set; }
        
        /// <summary>
        /// The IMEI of a CMIU.
        /// </summary>
        public string Imei { get; set; }

        /// <summary>
        /// The MSISDN of a CMIU.
        /// </summary>
        public string Msisdn { get; set; }

        /// <summary>
        /// Shows any cellular ID field that have different values between MDCE environments.
        /// </summary>
        public string MismatchedIds { get; set; }

        /// <summary>
        /// Gets the MIU ID formatted for CSV output.
        /// </summary>
        public string MiuIdPrefixed
        {
            get { return PrefixNumericValueForExcelOutput(MiuId.ToString()); }
        }

        /// <summary>
        /// Gets the last-heard timestamp formatted for CSV output.
        /// </summary>
        public string LastHeardDisplay
        {
            get { return LastHeard.HasValue ? LastHeard.Value.ToString("u") : string.Empty; }
        }

        /// <summary>
        /// Gets the ICCID formatted for CSV output.
        /// </summary>
        public string IccidPrefixed
        {
            get { return PrefixNumericValueForExcelOutput(Iccid); }
        }

        /// <summary>
        /// Gets the IMEI formatted for CSV output.
        /// </summary>
        public string ImeiPrefixed
        {
            get { return PrefixNumericValueForExcelOutput(Imei); }
        }

        /// <summary>
        /// Gets the MSISDN formatted for CSV output.
        /// </summary>
        public string MsisdnPrefixed
        {
            get { return PrefixNumericValueForExcelOutput(Msisdn); }
        }

        /// <summary>
        /// Formats the input as an Excel formula so that long numbers are displayed correctly when the CSV output is opened in Excel.
        /// </summary>
        private string PrefixNumericValueForExcelOutput(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return value;
            }

            return string.Format("=\"{0}\"", value);
        }
    }
}