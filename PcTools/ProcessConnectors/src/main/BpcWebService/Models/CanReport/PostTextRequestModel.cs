﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

namespace Ntg.BpcWebService.Models.CanReport
{
    /// <summary>
    /// Request model for the CAN report text-based query.
    /// </summary>
    public class PostTextRequestModel
    {
        /// <summary>
        /// The raw text input that is expected to contain a list of MIU IDs.
        /// </summary>
        public string MiuListText { get; set; }

        /// <summary>
        /// When assigned, contains any error message from the previous submission.
        /// </summary>
        public string ErrorFromLastSubmission { get; set; }
    }
}