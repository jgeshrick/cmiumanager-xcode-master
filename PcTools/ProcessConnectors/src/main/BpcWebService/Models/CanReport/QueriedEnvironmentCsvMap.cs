//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using CsvHelper.Configuration;

namespace Ntg.BpcWebService.Models.CanReport
{
    /// <summary>
    /// A helper class to provide CSV formatting for MDCE environment status information in the CAN report.
    /// </summary>
    public sealed class QueriedEnvironmentCsvMap : CsvClassMap<QueriedEnvironment>
    {
        public QueriedEnvironmentCsvMap()
        {
            Map(env => env.EnvironmentName).Index(0).Name("Environment");
            Map(env => env.WasResponseOkDisplayValue).Index(1).Name("Server Responses OK?");
            Map(env => env.ErrorDetails).Index(2).Name("Error Details");

            // Pad with empty columns to match CanResult rows
            Map(env => env.Empty).Index(3).Name("");
            Map(env => env.Empty).Index(4).Name("");
            Map(env => env.Empty).Index(5).Name("");
            Map(env => env.Empty).Index(6).Name("");
        }
    }
}