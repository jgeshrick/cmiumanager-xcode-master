﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using CsvHelper.Configuration;

namespace Ntg.BpcWebService.Models.CanReport
{
    /// <summary>
    /// A helper class to provide CSV formatting for line items in the CAN report.
    /// </summary>
    public sealed class CanResultCsvMap : CsvClassMap<CanResult>
    {
        public CanResultCsvMap()
        {
            Map(env => env.MiuIdPrefixed).Index(0).Name("MIU ID");
            Map(env => env.Environment).Index(1).Name("Most Recent Environment");
            Map(env => env.LastHeardDisplay).Index(2).Name("Last Heard");
            Map(env => env.IccidPrefixed).Index(3).Name("ICCID");
            Map(env => env.ImeiPrefixed).Index(4).Name("IMEI");
            Map(env => env.MsisdnPrefixed).Index(5).Name("MSISDN");
            Map(env => env.MismatchedIds).Index(6).Name("Mismatched Cellular IDs");
        }
    }
}