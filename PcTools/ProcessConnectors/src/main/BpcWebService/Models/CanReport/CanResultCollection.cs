//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;

namespace Ntg.BpcWebService.Models.CanReport
{
    /// <summary>
    /// Models the query results required to build a CAN report.
    /// </summary>
    public class CanResultCollection
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CanResultCollection()
        {
            Results = new List<CanResult>();
            QueriedEnvironments = new List<QueriedEnvironment>();
        }

        /// <summary>
        /// The line items to include in the report.
        /// </summary>
        public List<CanResult> Results { get; private set; }
        
        /// <summary>
        /// Status information for each queried MDCE environment.
        /// </summary>
        public List<QueriedEnvironment> QueriedEnvironments { get; private set; }
    }
}