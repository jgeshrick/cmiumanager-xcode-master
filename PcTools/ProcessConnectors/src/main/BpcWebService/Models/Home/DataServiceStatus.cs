﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using Newtonsoft.Json;

namespace Ntg.BpcWebService.Models.Home
{
    /// <summary>
    /// Status information for a data service.
    /// </summary>
    public class DataServiceStatus
    {
        /// <summary>
        /// Gets or sets the type of the service being monitored.
        /// </summary>
        [JsonProperty("service_type")]
        public string Type { get; set; }

        /// <summary>
        /// The status to show in the front end.
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// The duration for which the data service has been executing.
        /// </summary>
        [JsonProperty("run_duration")]
        public string RunDuration { get; set; }

        /// <summary>
        /// The integration process execution interval.
        /// </summary>
        [JsonProperty("execution_schedule")]
        public string ExecutionSchedule { get; set; }

        /// <summary>
        /// The last recorded time the integration process ran.
        /// </summary>
        [JsonProperty("last_executed_time")]
        public string LastExecutedTime { get; set; }

        /// <summary>
        /// The next time the integration process is schedules to run.
        /// </summary>
        [JsonProperty("next_execution_time")]
        public string NextExecutionTime { get; set; }

        /// <summary>
        /// Whether the last run was successful.
        /// </summary>
        [JsonProperty("last_execution_result")]
        public string LastExecutionResult { get; set; }
    }
}