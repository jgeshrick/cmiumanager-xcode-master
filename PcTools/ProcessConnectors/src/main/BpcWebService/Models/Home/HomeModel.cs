﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using Ntg.BpcCore.Scheduling;

namespace Ntg.BpcWebService.Models.Home
{
    /// <summary>
    /// Provides the model for initial population of the /Home/Index view.
    /// </summary>
    public class HomeModel
    {
        public IScheduler Scheduler { get; set; }
        public IList<MdceEnvironmentInfo> EnvironmentsConfig { get; set; }
        public bool IsAdminUser { get; set; }
    }
}