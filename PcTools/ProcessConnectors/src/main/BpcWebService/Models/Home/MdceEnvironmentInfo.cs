﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

namespace Ntg.BpcWebService.Models.Home
{
    /// <summary>
    /// MDCE environment configuration and state information for admin/status display.
    /// </summary>
    public class MdceEnvironmentInfo
    {
        /// <summary>
        /// Gets or sets a value indicating whether the partner string has been set for the environment.
        /// </summary>
        public bool HasPartnerString { get; set; }

        /// <summary>
        /// Gets or sets the environment name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the base MDCE service URL.
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the environment is enabled.
        /// </summary>
        public bool IsEnabled { get; set; }
    }
}