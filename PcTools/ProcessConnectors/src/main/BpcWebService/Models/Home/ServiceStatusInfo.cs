﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using Newtonsoft.Json;

namespace Ntg.BpcWebService.Models.Home
{
    /// <summary>
    /// Status information for the data services and connected MDCE environments.
    /// </summary>
    public class ServiceStatusInfo
    {
        [JsonProperty("services")]
        public List<DataServiceStatus> DataServiceStatuses { get; set; }

        [JsonProperty("started_count")]
        public int StartedCount { get; set; }

        [JsonProperty("stopped_count")]
        public int StoppedCount { get; set; }

        [JsonProperty("last_execution_time")]
        public string LastExecutionTime { get; set; }

        [JsonProperty("are_services_executing")]
        public bool AreServicesExecuting { get; set; }
    }
}