﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Web;
using System.Web.Hosting;

namespace Ntg.BpcWebService.Models
{
    public class TestService: IRegisteredObject
    {
        public Timer timer;

        public int Counter { get; set; }
        public string Comment { get; set; }

        public TestService()
        {
            Start();
        }

        public void Start()
        {
            Counter = 0;

            timer = new Timer(1000)
            {
                AutoReset = true,
                Enabled = false
            };

            timer.Elapsed += timer_Elapsed;

            timer.Enabled = true;
           
        }

        public void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Counter++;
        }

        public void Stop(bool immediate)
        {
            if (timer != null && timer.Enabled)
            {
                timer.Stop();
                timer.Enabled = false;
                timer.Elapsed -= timer_Elapsed;

                timer.Dispose();
            }
        }
    }
}