using System;

namespace Ntg.BpcWebService.Models
{
    /// <summary>
    /// Defines methods for converting dates and times to a specific string format and time zone.
    /// </summary>
    public interface ITimeZoneFormatter
    {
        string ToConfiguredTimeZone(string dateTimeWithOffset);
        
        string ToConfiguredTimeZone(DateTime? dateTimeUtc);
    }
}