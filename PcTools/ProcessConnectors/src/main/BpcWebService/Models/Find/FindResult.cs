﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using Newtonsoft.Json;
using Ntg.BpcCore.JsonModel;

namespace Ntg.BpcWebService.Models.Find
{
    /// <summary>
    /// Represents a single 'Find my MIU' result.
    /// </summary>
    public class FindResult
    {
        public FindResult()
        {
        }

        /// <summary>
        /// Creates a new instance of <see cref="FindResult"/> initialized with data from the supplied parameters.
        /// </summary>
        public FindResult(string environmentName, MiuDetail miuDetail, ITimeZoneFormatter timeZoneFormatter)
        {
            EnvironmentName = environmentName;
            if (miuDetail == null)
                throw new ArgumentNullException("miuDetail");

            MiuId = miuDetail.MiuId.ToString();
            SiteId = miuDetail.SiteId;
            Iccid = miuDetail.Iccid ?? string.Empty;
            Imei = miuDetail.Imei ?? string.Empty;
            Eui = miuDetail.Eui ?? string.Empty;
            Msisdn = miuDetail.Msisdn ?? string.Empty;
            DateTime lastInsertDate;
            if (DateTime.TryParse(miuDetail.LastInsertDate, out lastInsertDate))
            {
                LastInsertDate = lastInsertDate;
            }

            LastInsertDateString = timeZoneFormatter.ToConfiguredTimeZone(miuDetail.LastInsertDate) ?? string.Empty;
        }

        /// <summary>
        /// Gets or sets the name of the MDCE environment.
        /// </summary>
        [JsonProperty("environment_name")]
        public string EnvironmentName { get; set; }

        /// <summary>
        /// Gets or sets the MIU ID.
        /// </summary>
        [JsonProperty("miu_id")]
        public string MiuId { get; set; }

        /// <summary>
        /// Gets or sets the Site ID.
        /// </summary>
        [JsonProperty("site_id")]
        public int SiteId { get; set; }

        /// <summary>
        /// Gets or sets the ICCID.
        /// </summary>
        [JsonProperty("iccid")]
        public string Iccid { get; set; }

        /// <summary>
        /// Gets or sets the IMEI.
        /// </summary>
        [JsonProperty("imei")]
        public string Imei { get; set; }

        /// <summary>
        /// Gets or sets the EUI.
        /// </summary>
        [JsonProperty("eui")]
        public string Eui { get; set; }

        /// <summary>
        /// Gets or sets the MSISDN.
        /// </summary>
        [JsonProperty("msisdn")]
        public string Msisdn { get; set; }

        /// <summary>
        /// Gets or sets the last MIU insert date.
        /// </summary>
        [JsonProperty("last_insert_date")]
        public DateTime? LastInsertDate { get; set; }

        /// <summary>
        /// Gets or sets the last MIU insert date shown with the currently configured time zone offset.
        /// </summary>
        [JsonProperty("last_insert_date_string")]
        public string LastInsertDateString { get; set; }
    }
}