﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using Newtonsoft.Json;

namespace Ntg.BpcWebService.Models.Find
{
    /// <summary>
    /// Contains a set of Find my MIU results and any errors encountered during the search.
    /// </summary>
    public class FindResultCollection
    {
        [JsonProperty("results")]
        public FindResult[] Results { get; set; }

        [JsonProperty("errors")]
        public ServiceError[] Errors { get; set; }
    }
}