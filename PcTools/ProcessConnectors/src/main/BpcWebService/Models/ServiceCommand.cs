﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using Newtonsoft.Json;

namespace Ntg.BpcWebService.Models
{
    /// <summary>
    /// Used to send service control commands to the ServicesController.
    /// </summary>
    public class ServiceCommand
    {
        /// <summary>
        /// The command to send.
        /// </summary>
        [JsonProperty("command")]
        public string Command { get; set; }
    }
}