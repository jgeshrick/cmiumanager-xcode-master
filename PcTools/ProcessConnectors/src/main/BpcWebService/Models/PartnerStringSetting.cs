﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using Newtonsoft.Json;

namespace Ntg.BpcWebService.Models
{
    /// <summary>
    /// Contains the fields required for a request to set the partner string for a given MDCE environment.
    /// </summary>
    public class PartnerStringSetting
    {
        /// <summary>
        /// The MDCE environment name whose Partner String is to be set.
        /// </summary>
        [JsonProperty(PropertyName = "environment_name")]
        public string EnvironmentName { get; set; }

        /// <summary>
        /// The Partner String to be written to the secure configuration.
        /// </summary>
        [JsonProperty(PropertyName = "partner_string")]
        public string PartnerString { get; set; }
    }
}