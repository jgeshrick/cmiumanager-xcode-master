﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using Newtonsoft.Json;

namespace Ntg.BpcWebService.Models
{
    /// <summary>
    /// Contains information about a service communication error.
    /// </summary>
    public class ServiceError
    {
        /// <summary>
        /// Gets or sets the name of the MDCE environment to which the error relates.
        /// </summary>
        [JsonProperty("environment_name")]
        public string EnvironmentName { get; set; }

        /// <summary>
        /// Gets or sets the error details.
        /// </summary>
        [JsonProperty("error")]
        public string Error { get; set; }

        /// <summary>
        /// Creates a new instance of <see cref="ServiceError"/>.
        /// </summary>
        public ServiceError(string environmentName, string error)
        {
            EnvironmentName = environmentName;
            Error = error;
        }
    }
}