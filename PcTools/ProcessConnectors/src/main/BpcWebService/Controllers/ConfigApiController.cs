﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Linq;
using System.Web.Http;
using log4net;
using Ntg.BpcCore;
using Ntg.BpcCore.Settings;
using Ntg.BpcWebService.Authorization.WebApi;
using Ntg.BpcWebService.Models;

namespace Ntg.BpcWebService.Controllers
{
    [RoutePrefix("api/v1/config")]
    public class ConfigApiController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IEnvironmentsConfig environmentsConfig;
        private readonly IEnvironmentSettingsRepository environmentSettingsRepository;

        public ConfigApiController(IEnvironmentsConfig environmentsConfig, IEnvironmentSettingsRepository environmentSettingsRepository)
        {
            this.environmentsConfig = environmentsConfig;
            this.environmentSettingsRepository = environmentSettingsRepository;
        }

        /// <summary>
        /// Writes the partner string configuration for a given environment name.
        /// </summary>
        [AuthorizeAdminRoles]
        [Route("partnerString")]
        [HttpPost]
        public IHttpActionResult PostPartnerString(PartnerStringSetting partnerStringSetting)
        {
            Log.Debug(10012, string.Format("PostPartnerString() called. Environment name: \"{0}\".", partnerStringSetting.EnvironmentName));

            var environment = environmentsConfig.EnvironmentList.SingleOrDefault(e => e.Name == partnerStringSetting.EnvironmentName);

            if (environment == null)
            {
                Log.Warn(10013, string.Format("The supplied environment name is not recognized. Environment name: \"{0}\".", partnerStringSetting.EnvironmentName));
                return NotFound();
            }
            else
            {
                environmentSettingsRepository.SetBpcPartnerString(partnerStringSetting.PartnerString, environment.Name);
                environment.OnParnerStringChanged(new ParnerStringChangedEventArgs(partnerStringSetting.PartnerString));

                return Ok();
            }
        }
    }
}
