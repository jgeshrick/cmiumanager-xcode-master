﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CsvHelper;
using log4net;
using Ntg.BpcCore;
using Ntg.BpcCore.JsonModel;
using Ntg.BpcCore.ServiceProxies;
using Ntg.BpcWebService.Models;
using Ntg.BpcWebService.Models.CanReport;
using Ntg.BpcWebService.Models.Find;

namespace Ntg.BpcWebService.Controllers
{
    /// <summary>
    /// CAN report MVC controller.
    /// </summary>
    public class CanReportController : Controller
    {
        private const string MismatchedText = "MISMATCHED";

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IMiuListMultiServerProxy miuListClient;
        private readonly ITimeZoneFormatter timeZoneFormatter;

        public CanReportController(IMiuListMultiServerProxy miuListClient, ITimeZoneFormatter timeZoneFormatter)
        {
            this.miuListClient = miuListClient;
            this.timeZoneFormatter = timeZoneFormatter;
        }

        /// <summary>
        /// Gets the view for Find/Index.
        /// </summary>
        public ActionResult Index()
        {
            Log.Debug(10120, "CanReport/Index() was called.");

            return View();
        }

        /// <summary>
        /// Generates a CAN report based on the supplied CSV list of MIU IDs.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PostCsvList(HttpPostedFileBase file)
        {
            Log.Debug(10130, "CanReport/PostCsvList() was called.");

            // Check for no file data.
            if (file == null)
            {
                Log.Info(10133, "CanReportController.PostCsvList: No file data received. Redirecting back to Index.");

                return View("Index");
            }

            // Get the list of MIU IDs from the first column of the supplied CSV file.
            var miuIds = new List<int>();
            try
            {
                using (var fileReader = new StreamReader(file.InputStream))
                using (var reader = new CsvReader(fileReader))
                {
                    reader.Configuration.HasHeaderRecord = false;
                    while (reader.Read())
                    {
                        var rawData = reader.GetField<string>(0);
                        int miuId;
                        if (int.TryParse(rawData, out miuId))
                        {
                            miuIds.Add(miuId);
                        }
                    }
                }

                miuIds = miuIds.Distinct().ToList();
            }
            catch (Exception ex)
            {
                var displayError = string.Format("There was a problem with the uploaded file: {0}", ex.Message);
                Log.Error(10131, "CanReportController.PostCsvList: Error processing uploaded file.", ex);
                var model = new PostTextRequestModel{ErrorFromLastSubmission = displayError};

                return View("Index", model);
            }

            try
            {
                var canResults = await GetCanResultsAsync(miuIds);

                var csvContent = GetReportCsvContent(canResults);
                var csvData = new UTF8Encoding().GetBytes(csvContent);

                return File(csvData, "text/csv", "CanReport.csv");
            }
            catch (Exception ex)
            {
                var displayError = string.Format("An error was encountered while running the query: {0}", ex.Message);
                Log.Error(10132, "CanReportController.PostCsvList: Error running query.", ex);
                var model = new PostTextRequestModel { ErrorFromLastSubmission = displayError };

                return View("Index", model);
            }
        }

        /// <summary>
        /// Generates a CAN report based on the supplied list of MIU IDs supplied as text.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PostTextList(PostTextRequestModel requestModel)
        {
            Log.Debug(10140, "CanReport/PostTextList() was called.");

            // Check for no file data.
            if (requestModel == null || string.IsNullOrWhiteSpace(requestModel.MiuListText))
            {
                Log.Info(10143, "CanReportController.PostTextList: No empty request submitted. Redirecting back to Index.");

                return View("Index");
            }

            // Get the MIU IDs from the text entry.
            try
            {
                var miuIds = new List<int>();
                using (var reader = new StringReader(requestModel.MiuListText))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        int miuId;
                        if (int.TryParse(line, out miuId))
                        {
                            miuIds.Add(miuId);
                        }
                    }
                }

                miuIds = miuIds.Distinct().ToList();

                var canResults = await GetCanResultsAsync(miuIds);

                var csvContent = GetReportCsvContent(canResults);
                var csvData = new UTF8Encoding().GetBytes(csvContent);

                return File(csvData, "text/csv", "CanReport.csv");
            }
            catch (Exception ex)
            {
                var displayError = string.Format("An error was encountered while running the query: {0}", ex.Message);
                Log.Error(10141, "CanReportController.PostTextList: Error running text-based query.", ex);
                var model = new PostTextRequestModel { ErrorFromLastSubmission = displayError };

                return View("Index", model);
            }
        }

        /// <summary>
        /// Generates a CSV file that can be used as a template for uploading MIU IDs to the CAN Report page.
        /// </summary>
        public ActionResult GetCsvTemplate()
        {
            // Generate a CSV with a single header row.
            string templateBody;

            using (var textWriter = new StringWriter())
            {
                var csv = new CsvWriter(textWriter);
                csv.WriteField("MIU ID");
                csv.NextRecord();
                templateBody = textWriter.ToString();
            }

            var csvData = new UTF8Encoding().GetBytes(templateBody);

            return File(csvData, "text/csv", "CanReportUploadTemplate.csv");
        }

        /// <summary>
        /// Writes out the supplied query results to a CSV-formatted string with two sections: environment details (including error details); query results.
        /// </summary>
        /// <param name="canResults">The CAN report results for conversion to CSV.</param>
        /// <returns>A string containing the raw CSV content.</returns>
        private static string GetReportCsvContent(CanResultCollection canResults)
        {
            string environmentsHeader;

            using (var textWriter = new StringWriter())
            {
                var csv = new CsvWriter(textWriter);
                csv.Configuration.RegisterClassMap<QueriedEnvironmentCsvMap>();
                csv.WriteRecords(canResults.QueriedEnvironments);
                environmentsHeader = textWriter.ToString();
            }

            string reportBody;

            using (var textWriter = new StringWriter())
            {
                var csv = new CsvWriter(textWriter);
                csv.Configuration.RegisterClassMap<CanResultCsvMap>();
                csv.WriteRecords(canResults.Results);
                reportBody = textWriter.ToString();
            }

            var csvContent = environmentsHeader + ",,,,,,," + Environment.NewLine + reportBody;
            return csvContent;
        }

        /// <summary>
        /// Queries the configured MDCE environments for records of the supplied MIU IDs.
        /// </summary>
        /// <param name="miuIds">A list of MIU IDs to query.</param>
        /// <returns>An instance of <see cref="CanResultCollection"/> populated with details of queried environments and the MIU report data.</returns>
        private async Task<CanResultCollection> GetCanResultsAsync(List<int> miuIds)
        {
            var result = new CanResultCollection();

            // Include all queried environments in the report.
            foreach (var mdceEnvironment in miuListClient.GetConfiguredEnvironments())
            {
                result.QueriedEnvironments.Add(new QueriedEnvironment { EnvironmentName = mdceEnvironment.Name, WasResponseOk = true });
            }

            foreach (var miuId in miuIds)
            {
                // Ignore any failing environments after the first failure. This is intended to avoid repeatedly hitting an MDCE service 
                // that may be offline, and therefore might cause whole CAN report to time out.
                var excludedEnvironments = result.QueriedEnvironments.Where(qe => !qe.WasResponseOk).Select(qe => qe.EnvironmentName);

                MultiServerResult<MiuListCollection> miuDetails = await miuListClient.GetMiuListAsync(miuId.ToString(), DeviceIdType.MiuId, excludedEnvironments);

                // Begin excluding an environment when an error is encountered, and record the error.
                foreach (var errorResult in miuDetails.Errors)
                {
                    var errorEnvironment = result.QueriedEnvironments.SingleOrDefault(qe => qe.EnvironmentName == errorResult.Key);

                    if (errorEnvironment == null)
                    {
                        errorEnvironment = new QueriedEnvironment { EnvironmentName = errorResult.Key };
                        result.QueriedEnvironments.Add(errorEnvironment);
                    }

                    errorEnvironment.ErrorDetails = errorResult.Value;
                    errorEnvironment.WasResponseOk = false;
                }

                // Add the "most-recently-heard" MIU details to the results list.
                var latestMiuResult = miuDetails.Results
                    .SelectMany(r => r.Value.Mius, (r, miu) => new FindResult(r.Key, miu, timeZoneFormatter))
                    .OrderByDescending(f => f.LastInsertDate)
                    .FirstOrDefault();

                if (latestMiuResult != null)
                {
                    string iccid;
                    string imei;
                    string msisdn;
                    var iccids = miuDetails.Results.Values.SelectMany(r => r.Mius, (r, miu) => miu.Iccid).Where(id => !string.IsNullOrEmpty(id)).Distinct().ToList();
                    var imeis = miuDetails.Results.Values.SelectMany(r => r.Mius, (r, miu) => miu.Imei).Where(id => !string.IsNullOrEmpty(id)).Distinct().ToList();
                    var msisdns = miuDetails.Results.Values.SelectMany(r => r.Mius, (r, miu) => miu.Msisdn).Where(id => !string.IsNullOrEmpty(id)).Distinct().ToList();
                    bool isIccidMismatched = iccids.Count > 1;
                    bool isImeiMismatched = imeis.Count > 1;
                    bool isMsisdnMismatched = msisdns.Count > 1;
                    var mismatchedIds = new List<string>();

                    if (latestMiuResult.LastInsertDate.HasValue)
                    {
                        // Show the cellular/LoRa details from the most recent environment if the MIU has called in.
                        iccid = latestMiuResult.Iccid;
                        imei = latestMiuResult.Imei;
                        msisdn = latestMiuResult.Msisdn;
                    }
                    else
                    {
                        iccid = isIccidMismatched ? MismatchedText : iccids.FirstOrDefault();
                        imei = isImeiMismatched ? MismatchedText : imeis.FirstOrDefault();
                        msisdn = isMsisdnMismatched ? MismatchedText : msisdns.FirstOrDefault();
                    }

                    if (isIccidMismatched)
                    {
                        mismatchedIds.Add("ICCID");
                    }

                    if (isImeiMismatched)
                    {
                        mismatchedIds.Add("IMEI");
                    }

                    if (isMsisdnMismatched)
                    {
                        mismatchedIds.Add("MSISDN");
                    }

                    result.Results.Add(new CanResult
                    {
                        MiuId = Convert.ToInt32(latestMiuResult.MiuId),
                        Environment = latestMiuResult.LastInsertDate.HasValue ? latestMiuResult.EnvironmentName : "(not heard)",
                        LastHeard = latestMiuResult.LastInsertDate,
                        Iccid = iccid,
                        Imei = imei,
                        Msisdn = msisdn,
                        MismatchedIds = string.Join(", ", mismatchedIds)
                    });
                }
            }

            return result;
        }
    }
}