﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using Ntg.BpcCore;
using Ntg.BpcCore.Scheduling;
using Ntg.BpcCore.Settings;
using Ntg.BpcWebService.Authorization;
using Ntg.BpcWebService.Models.Home;

namespace Ntg.BpcWebService.Controllers
{
    /// <summary>
    /// The Home controller.
    /// </summary>
    public class HomeController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IScheduler scheduler;
        private readonly IEnvironmentsConfig environmentsConfig;
        private readonly IEnvironmentSettingsRepository environmentSettingsRepository;
        private readonly IAccessTypeResolver accessTypeResolver;

        /// <summary>
        /// Creates a new instance of <see cref="HomeController"/>.
        /// </summary>
        public HomeController(IScheduler scheduler, IEnvironmentsConfig environmentsConfig, IEnvironmentSettingsRepository environmentSettingsRepository, IAccessTypeResolver accessTypeResolver)
        {
            this.scheduler = scheduler;
            this.environmentsConfig = environmentsConfig;
            this.environmentSettingsRepository = environmentSettingsRepository;
            this.accessTypeResolver = accessTypeResolver;
        }

        /// <summary>
        /// Gets the view for Home/Index.
        /// </summary>
        public ActionResult Index()
        {
            Log.Debug(10014, "Home/Index() was called.");
            
            ViewBag.Message = "Neptune TG BPC Service Web Client";

            List<MdceEnvironmentInfo> mdceEnvironmentInfos = environmentsConfig.EnvironmentList.Select(e =>
                new MdceEnvironmentInfo
                {
                    Name = e.Name,
                    IsEnabled = e.IsEnabled,
                    BaseUrl = e.BaseUrl,
                    HasPartnerString = environmentSettingsRepository.HasBpcPartnerString(e.Name)
                })
                .ToList();

            var model = new HomeModel
            {
                Scheduler = scheduler,
                EnvironmentsConfig = mdceEnvironmentInfos,
                IsAdminUser = accessTypeResolver.IsAdminUser(System.Web.HttpContext.Current.User)
            };

            return View(model);
        }
    }
}
