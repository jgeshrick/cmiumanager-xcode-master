﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using log4net;
using Ntg.BpcWebService.Models;
using Ntg.BpcWebService.Models.Home;
using Ntg.BpcCore;
using Ntg.BpcCore.Scheduling;
using Ntg.BpcWebService.Authorization.WebApi;

namespace Ntg.BpcWebService.Controllers
{
    [RoutePrefix("api/v1/services")]
    public class ServicesApiController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IScheduler scheduler;
        private readonly ITimeZoneFormatter timeZoneFormatter;

        public ServicesApiController(IScheduler scheduler, ITimeZoneFormatter timeZoneFormatter)
        {
            this.scheduler = scheduler;
            this.timeZoneFormatter = timeZoneFormatter;
        }

        /// <summary>
        /// Gets current service status information.
        /// </summary>
        [Route("")]
        [HttpGet]
        public IHttpActionResult GetStatus()
        {
            Log.Debug(10008, "GetStatus() called.");

            var serviceStatus = GetServiceStatusInfo();

            return Ok(serviceStatus);
        }

        /// <summary>
        /// Allows service commands to be sent to start/stop/run all data services.
        /// </summary>
        [AuthorizeAdminRoles]
        [Route("")]
        [HttpPost]
        public IHttpActionResult PostCommand(ServiceCommand serviceCommand)
        {
            Log.Debug(10009, string.Format("PostCommand() called. Command: \"{0}\".", serviceCommand.Command));

            if (scheduler != null)
            {
                if (serviceCommand.Command.Equals("start", StringComparison.OrdinalIgnoreCase))
                {
                    scheduler.EnableScheduledServices();
                }
                else if (serviceCommand.Command.Equals("stop", StringComparison.OrdinalIgnoreCase))
                {
                    scheduler.DisableScheduledServices();
                }
                else if (serviceCommand.Command.Equals("run", StringComparison.OrdinalIgnoreCase))
                {
                    Log.Warn(10025, string.Format("Service command \"{0}\" is only supported for a single service.", serviceCommand.Command));
                }
                else
                {
                    Log.Warn(10010, string.Format("Unsupported service command: \"{0}\".", serviceCommand.Command));
                }
            }

            // Get updated services status
            var serviceStatus = GetServiceStatusInfo();

            return Ok(serviceStatus);
        }

        /// <summary>
        /// Allows service commands to be sent to start/stop/run a data service.
        /// </summary>
        [AuthorizeAdminRoles]
        [Route("{serviceType}")]
        [HttpPost]
        public IHttpActionResult PostSingleServiceCommand(string serviceType, ServiceCommand serviceCommand)
        {
            Log.Debug(10009, string.Format("PostSingleServiceCommand() called. serviceType: \"{0}\", command: \"{1}\".", serviceType, serviceCommand.Command));

            DataConnectorType dataConnectorType;
            if (Enum.TryParse(serviceType, out dataConnectorType))
            {
                var scheduledService = scheduler.GetScheduledService(dataConnectorType);
                if (scheduledService != null)
                {
                    if (serviceCommand.Command.Equals("start", StringComparison.OrdinalIgnoreCase))
                    {
                        scheduledService.IsEnabled = true;
                    }
                    else if (serviceCommand.Command.Equals("stop", StringComparison.OrdinalIgnoreCase))
                    {
                        scheduledService.IsEnabled = false;
                    }
                    else if (serviceCommand.Command.Equals("run", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!scheduledService.DataConnector.IsExecuting)
                        {
                            Log.Info(10027, string.Format("Received command to execute data service \"{0}\".", scheduledService.DataConnector.Name));
                            scheduledService.ExecuteNow();
                        }
                    }
                    else
                    {
                        Log.Warn(10010, string.Format("Unsupported service command: \"{0}\".", serviceCommand.Command));
                    }
                }
            }

            // Get updated services status
            var serviceStatus = GetServiceStatusInfo();

            return Ok(serviceStatus);
        }

        /// <summary>
        /// Gets a service status summary.
        /// </summary>
        private ServiceStatusInfo GetServiceStatusInfo()
        {
            string lastServiceStart = timeZoneFormatter.ToConfiguredTimeZone(scheduler.LastExecutionTime) ?? string.Empty;
            var serviceStatusInfo = new ServiceStatusInfo
            {
                DataServiceStatuses = new List<DataServiceStatus>(),
                // Summary information
                StartedCount = scheduler.ScheduledServices.Count(s => s.IsEnabled),
                StoppedCount = scheduler.ScheduledServices.Count(s => !s.IsEnabled),
                LastExecutionTime = lastServiceStart,
                AreServicesExecuting = scheduler.ScheduledServices.Any(s => s.DataConnector.IsExecuting)
            };

            // Per-service details
            foreach (var scheduledService in scheduler.ScheduledServices)
            {
                string lastExecutedTime = timeZoneFormatter.ToConfiguredTimeZone(scheduledService.LastExecutedTime) ?? string.Empty;
                string nextExecutionTime = timeZoneFormatter.ToConfiguredTimeZone(scheduledService.NextExecutionTime) ?? string.Empty;
                string lastExecutionResult = string.Empty;
                if (scheduledService.Succeeded.HasValue)
                    lastExecutionResult = scheduledService.Succeeded.Value ? "OK" : "Failed";

                var dataServiceStatus = new DataServiceStatus
                {
                    Status = scheduledService.Status,
                    LastExecutionResult = lastExecutionResult,
                    ExecutionSchedule = string.Join("<br />", scheduledService.ScheduleConfigurations.Select(sc => sc.ToString())),
                    LastExecutedTime = lastExecutedTime,
                    NextExecutionTime = nextExecutionTime,
                    Type = scheduledService.DataConnector.DataConnectorType.ToString(),
                    RunDuration = string.Empty
                };
                serviceStatusInfo.DataServiceStatuses.Add(dataServiceStatus);
                
                // Render the run duration as hh:mm:ss
                if (scheduledService.DataConnector.IsExecuting && scheduledService.LastExecutedTime.HasValue)
                {
                    var duration = DateTime.UtcNow - scheduledService.LastExecutedTime.Value;
                    dataServiceStatus.RunDuration = string.Format("{0:D2}:{1:D2}:{2:D2}", (int)duration.TotalHours, duration.Minutes, duration.Seconds);
                }
            }

            return serviceStatusInfo;
        }
    }
}
