﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Web.Mvc;
using log4net;
using Ntg.BpcCore;

namespace Ntg.BpcWebService.Controllers
{
    /// <summary>
    /// The Find controller.
    /// </summary>
    public class FindController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Gets the view for Find/Index.
        /// </summary>
        public ActionResult Index()
        {
            Log.Debug(10028, "Find/Index() was called.");

            return View();
        }
    }
}