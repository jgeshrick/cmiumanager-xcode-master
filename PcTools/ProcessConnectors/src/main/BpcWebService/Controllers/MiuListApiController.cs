﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2017 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using log4net;
using Ntg.BpcCore;
using Ntg.BpcCore.JsonModel;
using Ntg.BpcCore.ServiceProxies;
using Ntg.BpcWebService.Models;
using Ntg.BpcWebService.Models.Find;

namespace Ntg.BpcWebService.Controllers
{
    /// <summary>
    /// Provides Web API methods to access the MIU List.
    /// </summary>
    [RoutePrefix("api/v1/miu_list")]
    public class MiuListApiController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IMiuListMultiServerProxy miuListClient;
        private readonly ITimeZoneFormatter timeZoneFormatter;
        
        /// <summary>
        /// Creates a new instance of <see cref="MiuListApiController"/>
        /// </summary>
        public MiuListApiController(IMiuListMultiServerProxy miuListClient, ITimeZoneFormatter timeZoneFormatter)
        {
            this.miuListClient = miuListClient;
            this.timeZoneFormatter = timeZoneFormatter;
        }

        /// <summary>
        /// Gets MIU details for an MIU with a specific identity.
        /// </summary>
        [Route("")]
        [HttpGet]
        public async Task<IHttpActionResult> GetMiuDetailsAsync([FromUri(Name = "find_id")] string findId)
        {
            Log.Debug(10029, string.Format("GetMiuDetails was called. Find ID: \"{0}\".", findId));

            MultiServerResult<MiuListCollection> miuDetails = await miuListClient.GetMiuListAsync(findId);
            
            // Flatten the results from all environments into an ordered array of FindResult
            var findResults = miuDetails.Results
                .SelectMany(r => r.Value.Mius, (r, miu) => new FindResult(r.Key, miu, timeZoneFormatter))
                .OrderByDescending(f => f.LastInsertDate)
                .ToArray();

            var errors = miuDetails.Errors
                .Select(e => new ServiceError(e.Key, e.Value))
                .ToArray();

            var results = new FindResultCollection
            {
                Results = findResults,
                Errors = errors
            };

            return Ok(results);
        }
    }
}