﻿CREATE TYPE [bpc].[MiuOwnerHistoryTableType] AS TABLE
(
    [SequenceId]                INT       NOT NULL,
    [MiuId]                     INT       NOT NULL,
    [Timestamp]                 DATETIME  NOT NULL,
    [SiteId]                    INT       NOT NULL,
    [SiteIdChangeRaised]        DATETIME  NULL,
    [ConsideredForTransferQue]  DATETIME  NULL);

