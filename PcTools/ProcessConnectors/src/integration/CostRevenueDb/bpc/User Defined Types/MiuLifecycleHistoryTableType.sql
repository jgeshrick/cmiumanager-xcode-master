﻿CREATE TYPE [bpc].[MiuLifecycleHistoryTableType] AS TABLE (
    [SequenceId]      INT           NOT NULL,
    [MiuId]           INT           NOT NULL,
    [Timestamp]       DATETIME      NOT NULL,
    [LifecycleState]  VARCHAR (20)  NOT NULL);
