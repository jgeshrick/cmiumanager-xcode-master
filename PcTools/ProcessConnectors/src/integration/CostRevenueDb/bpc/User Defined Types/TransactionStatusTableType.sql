﻿-- Table type used for updating transactions as they are processed
CREATE TYPE [bpc].[TransactionStatusTableType] AS TABLE (
    [Type]     VARCHAR(5)    NULL,
    [InDate]   DATETIME      NULL,
    [Key]      VARCHAR(500)  NULL,
    [OutDate]  DATETIME      NULL,
    [Status]   VARCHAR(5)    NULL,
    [Msg]      VARCHAR(200)  NULL
);
