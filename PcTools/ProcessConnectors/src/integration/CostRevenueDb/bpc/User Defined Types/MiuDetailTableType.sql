﻿CREATE TYPE [bpc].[MiuDetailTableType] AS TABLE (
    [MiuId]           INT           NOT NULL,
    [SiteId]          INT           NOT NULL,
    [MiuType]         VARCHAR (5)   NULL,
    [NetworkProvider] VARCHAR (45)  NULL,
    [Iccid]           VARCHAR (20)  NULL,
    [Msisdn]          VARCHAR (20)  NULL,
    [Eui]             VARCHAR (33)  NULL);
