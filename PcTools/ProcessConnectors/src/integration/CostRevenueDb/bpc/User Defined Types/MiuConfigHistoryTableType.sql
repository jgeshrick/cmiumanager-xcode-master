﻿CREATE TYPE [bpc].[MiuConfigHistoryTableType] AS TABLE (
    [SequenceId]                INT       NOT NULL,
    [MiuId]                     INT       NOT NULL,
    [ReceivedFromMiu]           BIT       NOT NULL,
    [Timestamp]                 DATETIME  NOT NULL,
    [RecordIntervalMins]        INT       NOT NULL,
    [ReportIntervalMins]        INT       NOT NULL,
    [ModeChangeRaised]          DATETIME  NULL,
    [ConsideredForTransferQue]  DATETIME  NULL);
