﻿CREATE TABLE [bpc].[EtlStatus]
(
    [Key] NVARCHAR(50) NOT NULL PRIMARY KEY, 
    [Value] NVARCHAR(50) NOT NULL, 
    [LastUpdated] DATETIME2(0) NOT NULL
)
