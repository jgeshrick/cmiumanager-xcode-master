﻿-- For testing purposes only
CREATE TABLE [bpc].[TestTransferQue]
(
	[tranType] [nvarchar](5) NULL,
	[tranInDate] [datetime] NULL,
	[tranKey] [varchar](500) NULL,
	[tranOutDate] [datetime] NULL,
	[tranStatus] [nvarchar](5) NULL,
	[tranContent] [nvarchar](500) NULL,
	[tranSrc] [nvarchar](50) NULL,
	[tranMsg] [nvarchar](200) NULL
)
