﻿CREATE TABLE [bpc].[MiuOwnerHistory]
(
    [SequenceId]                INT       NOT NULL,
    [MiuId]                     INT       NOT NULL,
    [Timestamp]                 DATETIME  NOT NULL,
    [SiteId]                    INT       NOT NULL,
    [SiteIdChangeRaised]        DATETIME  NULL,
    [ConsideredForTransferQue]  DATETIME  NULL,
    [Created]                   DATETIME  NOT NULL,
    [LastUpdated]               DATETIME  NOT NULL,
    CONSTRAINT [PK_MiuOwnerHistory] 
        PRIMARY KEY CLUSTERED ([SequenceId] ASC),
    CONSTRAINT [FK_MiuOwnerHistory_MiuDetail] 
        FOREIGN KEY ([MiuId]) REFERENCES [bpc].[MiuDetail] ([MiuId])
)
