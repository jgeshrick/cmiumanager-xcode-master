﻿CREATE TABLE [bpc].[MiuConfigHistory]
(
    [SequenceId]                INT       NOT NULL,
    [MiuId]                     INT       NOT NULL,
    [ReceivedFromMiu]           BIT       NOT NULL,
    [Timestamp]                 DATETIME  NOT NULL,
    [RecordIntervalMins]        INT       NULL,
    [ReportIntervalMins]        INT       NULL,
    [ModeChangeRaised]          DATETIME  NULL,
    [ConsideredForTransferQue]  DATETIME  NULL,
    [Created]                   DATETIME  NOT NULL,
    [LastUpdated]               DATETIME  NOT NULL,
    CONSTRAINT [PK_MiuConfigHistory] 
        PRIMARY KEY CLUSTERED ([SequenceId] ASC),
    CONSTRAINT [FK_MiuConfigHistory_MiuDetail] 
        FOREIGN KEY ([MiuId]) REFERENCES [bpc].[MiuDetail] ([MiuId])
)
GO
