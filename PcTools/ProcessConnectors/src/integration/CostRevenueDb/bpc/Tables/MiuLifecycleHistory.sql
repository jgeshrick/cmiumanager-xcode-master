﻿CREATE TABLE [bpc].[MiuLifecycleHistory]
(
    [SequenceId]      INT           NOT NULL,
    [MiuId]           INT           NOT NULL,
    [Timestamp]       DATETIME      NOT NULL,
    [LifecycleState]  VARCHAR (20)  NOT NULL,
    [Created]         DATETIME      NOT NULL,
    [LastUpdated]     DATETIME      NOT NULL,
    CONSTRAINT [PK_MiuLifecycleHistory] 
        PRIMARY KEY CLUSTERED ([SequenceId] ASC),
    CONSTRAINT [FK_MiuLifecycleHistory_MiuDetail] 
        FOREIGN KEY ([MiuId]) REFERENCES [bpc].[MiuDetail] ([MiuId])
)
GO
