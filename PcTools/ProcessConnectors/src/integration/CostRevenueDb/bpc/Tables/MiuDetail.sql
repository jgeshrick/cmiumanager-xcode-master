﻿CREATE TABLE [bpc].[MiuDetail] (
    [MiuId]           INT           NOT NULL,
    [SiteId]          INT           NOT NULL DEFAULT 0,
    [MiuType]         VARCHAR (5)   NULL,
    [NetworkProvider] VARCHAR (45)  NULL,
    [Iccid]           VARCHAR (20)  NULL,
    [Msisdn]          VARCHAR (20)  NULL,
    [Eui]             VARCHAR (33)  NULL,
    [Created]         DATETIME      NOT NULL,
    [LastUpdated]     DATETIME      NOT NULL,
    CONSTRAINT [PK_MiuDetail] PRIMARY KEY CLUSTERED ([MiuId] ASC)
);

