﻿CREATE PROCEDURE [bpc].[prGetOpenTransactions]
(
    @TransactionType VARCHAR(5),
    @MaxRows INT = 1000
)
AS
BEGIN

    SELECT TOP (@MaxRows)
        [tranType] AS [Type], 
        [tranInDate] AS [InDate], 
        [tranKey] AS [Key], 
        [tranOutDate] AS [OutDate], 
        [tranStatus] AS [Status], 
        [tranContent] AS [Content], 
        [tranSrc] AS [Src], 
        [tranMsg] AS [Msg]
    FROM
        [dbo].[TransferQue]
    WHERE
        tranType = @TransactionType
        AND tranStatus = 'O'
    ORDER BY
        tranInDate ASC

END
