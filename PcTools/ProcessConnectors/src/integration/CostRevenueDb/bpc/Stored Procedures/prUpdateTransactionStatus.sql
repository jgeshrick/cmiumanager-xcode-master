﻿CREATE PROCEDURE [bpc].[prUpdateTransactionStatus]
(
    @TransactionStatus [bpc].[TransactionStatusTableType] READONLY
)
AS
BEGIN

    UPDATE tq
    SET
        tq.[tranOutDate] = uts.[OutDate],
        tq.[tranStatus] = uts.[Status],
        tq.[tranMsg] = uts.[Msg]
    FROM dbo.TransferQue tq
    INNER JOIN @TransactionStatus uts 
        ON uts.[Key] = tq.[tranKey]

END
