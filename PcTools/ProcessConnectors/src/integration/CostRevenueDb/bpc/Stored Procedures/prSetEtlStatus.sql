﻿-- Sets the ETL status for a specific MDCE environment.
CREATE PROCEDURE [bpc].[prSetEtlStatus]
    @Key NVARCHAR(50),
    @Value NVARCHAR(50)

AS
    
    MERGE bpc.EtlStatus s
    USING (SELECT @Key AS [Key], @Value AS [Value]) AS us
    ON us.[Key] = s.[Key]
    WHEN MATCHED THEN 
        UPDATE SET [Value] = us.[Value], [LastUpdated] = SYSUTCDATETIME()
    WHEN NOT MATCHED THEN 
        INSERT VALUES (us.[Key], us.[Value], SYSUTCDATETIME());

RETURN 0
