﻿CREATE PROCEDURE [bpc].[prGetEtlStatus]
    @Key NVARCHAR(50)
AS
BEGIN
    
    SELECT TOP 1 Value FROM
    (
        SELECT Value FROM EtlStatus
        UNION ALL
        SELECT NULL AS Value
    ) AS result
    
END
