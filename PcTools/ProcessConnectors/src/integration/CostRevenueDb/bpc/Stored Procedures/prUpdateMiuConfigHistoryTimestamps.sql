﻿-- Updates the timestamps on the supplied MIU configuration history records.
CREATE PROCEDURE [bpc].[prUpdateMiuConfigHistoryTimestamps]
(
    @MiuConfigHistory bpc.MiuConfigHistoryTableType READONLY
)
AS
BEGIN

    UPDATE ch 
    SET
        ch.ConsideredForTransferQue = uch.ConsideredForTransferQue,
        ch.ModeChangeRaised = uch.ModeChangeRaised,
        ch.LastUpdated = SYSUTCDATETIME()
    FROM bpc.MiuConfigHistory ch
    INNER JOIN @MiuConfigHistory as uch ON uch.SequenceId = ch.SequenceId

END
