﻿-- Inserts the supplied MIU owner history entries
CREATE PROCEDURE bpc.prAppendMiuOwnerHistory
(
    @MiuOwnerHistory bpc.MiuOwnerHistoryTableType READONLY
)
AS
BEGIN
    
    BEGIN TRAN

    -- Ensure that an MiuDetail record exists for the incoming history items.
    MERGE INTO bpc.MiuDetail AS md
    USING
    (
        SELECT MiuId FROM @MiuOwnerHistory
    ) AS umd 
    ON
        umd.MiuId = md.MiuId
    WHEN NOT MATCHED BY TARGET THEN
        INSERT (MiuId, Created, LastUpdated) 
        VALUES (umd.MiuId, SYSUTCDATETIME(), SYSUTCDATETIME());

    -- Insert the MIU configuration history
    INSERT INTO bpc.MiuOwnerHistory
    (
        [SequenceId], [MiuId], [Timestamp], [SiteId], [Created], [LastUpdated]
    )
    SELECT 
        [SequenceId],
        [MiuId],
        [Timestamp],
        [SiteId],
        SYSUTCDATETIME(),
        SYSUTCDATETIME()
    FROM @MiuOwnerHistory

    COMMIT TRAN

END
