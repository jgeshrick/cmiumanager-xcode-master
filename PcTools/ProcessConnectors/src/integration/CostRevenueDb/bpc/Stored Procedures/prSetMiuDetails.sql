﻿-- Merges the supplied MIU details into the MiuDetails table.
CREATE PROCEDURE [bpc].[prSetMiuDetails]
    @MiuDetails bpc.MiuDetailTableType READONLY
AS
BEGIN
    
    BEGIN TRAN

    MERGE INTO bpc.MiuDetail AS md
    USING
    (
        SELECT 
            [MiuId],
            [SiteId],
            [MiuType],
            [NetworkProvider],
            [Iccid],
            [Msisdn],
            [Eui]
        FROM
            @MiuDetails
    ) AS umd
    ON
        umd.[MiuId] = md.[MiuId]
    WHEN MATCHED THEN
        UPDATE SET
            md.[SiteId] = umd.[SiteId],
            md.[MiuType] = umd.[MiuType],
            md.[NetworkProvider] = umd.[NetworkProvider],
            md.[Iccid] = umd.[Iccid],
            md.[Msisdn] = umd.[Msisdn],
            md.[Eui] = umd.[Eui],
            md.[LastUpdated] = SYSUTCDATETIME()

    WHEN NOT MATCHED BY TARGET THEN
        INSERT
        (
            [MiuId], [SiteId], [MiuType], [NetworkProvider], [Iccid], [Msisdn], [Eui], [Created], [LastUpdated]
        )
        VALUES
        (
            umd.[MiuId],
            umd.[SiteId],
            umd.[MiuType],
            umd.[NetworkProvider],
            umd.[Iccid],
            umd.[Msisdn],
            umd.[Eui],
            SYSUTCDATETIME(),
            SYSUTCDATETIME()
        );

    COMMIT TRAN

END
