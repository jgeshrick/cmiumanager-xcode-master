﻿-- Updates the timestamps on the supplied MIU owner history records.
CREATE PROCEDURE [bpc].[prUpdateMiuOwnerHistoryTimestamps]
(
    @MiuOwnerHistory bpc.MiuOwnerHistoryTableType READONLY
)
AS
BEGIN

    UPDATE ch 
    SET
        ch.ConsideredForTransferQue = uch.ConsideredForTransferQue,
        ch.SiteIdChangeRaised = uch.SiteIdChangeRaised,
        ch.LastUpdated = SYSUTCDATETIME()
    FROM bpc.MiuOwnerHistory ch
    INNER JOIN @MiuOwnerHistory as uch ON uch.SequenceId = ch.SequenceId

END
