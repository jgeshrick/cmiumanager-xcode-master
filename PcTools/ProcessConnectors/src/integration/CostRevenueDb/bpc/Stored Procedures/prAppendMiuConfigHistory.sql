﻿-- Inserts the supplied MIU configuration history entries
CREATE PROCEDURE bpc.prAppendMiuConfigHistory
(
    @MiuConfigHistory bpc.MiuConfigHistoryTableType READONLY
)
AS
BEGIN
    
    BEGIN TRAN

    -- Ensure that an MiuDetail record exists for the incoming history items.
    MERGE INTO bpc.MiuDetail AS md
    USING
    (
        SELECT MiuId FROM @MiuConfigHistory
    ) AS umd 
    ON
        umd.MiuId = md.MiuId
    WHEN NOT MATCHED BY TARGET THEN
        INSERT (MiuId, Created, LastUpdated) 
        VALUES (umd.MiuId, SYSUTCDATETIME(), SYSUTCDATETIME());

    -- Insert the MIU configuration history
    INSERT INTO bpc.MiuConfigHistory
    (
        [SequenceId], [MiuId], [ReceivedFromMiu], [Timestamp], 
        [RecordIntervalMins], [ReportIntervalMins], [Created], [LastUpdated]
    )
    SELECT 
        [SequenceId],
        [MiuId],
        [ReceivedFromMiu],
        [Timestamp],
        [RecordIntervalMins],
        [ReportIntervalMins],
        SYSUTCDATETIME(),
        SYSUTCDATETIME()
    FROM @MiuConfigHistory

    COMMIT TRAN

END
