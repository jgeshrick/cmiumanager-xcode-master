﻿CREATE PROCEDURE [bpc].[prGetMiuConfigHistoryByMiuId]
    @MiuId INT
AS
BEGIN

    SELECT [SequenceId]
          ,[MiuId]
          ,[ReceivedFromMiu]
          ,[Timestamp]
          ,[RecordIntervalMins]
          ,[ReportIntervalMins]
          ,[ModeChangeRaised]
          ,[ConsideredForTransferQue]
          ,[Created]
          ,[LastUpdated]
    FROM [bpc].[MiuConfigHistory]
    WHERE [MiuId] = @MiuId
    ORDER BY [Timestamp]

END
