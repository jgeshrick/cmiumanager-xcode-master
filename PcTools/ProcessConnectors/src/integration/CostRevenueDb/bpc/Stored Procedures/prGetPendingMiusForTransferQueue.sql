﻿-- Selects all MIU IDs for which there are configuration/owner history entries that have not been considered 
-- for entry into the QAD transfer queue.
CREATE PROCEDURE [bpc].[prGetPendingMiusForTransferQueue]
    @TableName VARCHAR(50)
AS
BEGIN

    IF @TableName = 'MiuConfigHistory'

        SELECT DISTINCT MiuId
        FROM [bpc].[MiuConfigHistory]
        WHERE ConsideredForTransferQue IS NULL

    ELSE IF @TableName = 'MiuOwnerHistory'

        SELECT DISTINCT MiuId
        FROM [bpc].[MiuOwnerHistory]
        WHERE ConsideredForTransferQue IS NULL

END