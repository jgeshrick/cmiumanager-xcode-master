﻿-- Inserts the supplied MIU lifecycle history entries
CREATE PROCEDURE bpc.prAppendMiuLifecycleHistory
(
    @MiuLifecycleHistory bpc.MiuLifecycleHistoryTableType READONLY
)
AS
BEGIN
    
    BEGIN TRAN

    -- Ensure that an MiuDetail record exists for the incoming history items.
    MERGE INTO bpc.MiuDetail AS md
    USING
    (
        SELECT MiuId FROM @MiuLifecycleHistory
    ) AS umd 
    ON
        umd.MiuId = md.MiuId
    WHEN NOT MATCHED BY TARGET THEN
        INSERT (MiuId, Created, LastUpdated) 
        VALUES (umd.MiuId, SYSUTCDATETIME(), SYSUTCDATETIME());

    -- Insert the MIU lifecycle history
    INSERT INTO bpc.MiuLifecycleHistory
    (
        [SequenceId], [MiuId], [Timestamp], [LifecycleState], [Created], [LastUpdated]
    )
    SELECT 
        [SequenceId],
        [MiuId],
        [Timestamp],
        [LifecycleState],
        SYSUTCDATETIME(),
        SYSUTCDATETIME()
    FROM @MiuLifecycleHistory

    COMMIT TRAN

END
