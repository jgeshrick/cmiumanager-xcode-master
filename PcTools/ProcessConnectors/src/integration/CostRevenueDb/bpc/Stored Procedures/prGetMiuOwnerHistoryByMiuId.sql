﻿CREATE PROCEDURE [bpc].[prGetMiuOwnerHistoryByMiuId]
    @MiuId INT
AS
BEGIN

    SELECT [SequenceId]
          ,[MiuId]
          ,[Timestamp]
          ,[SiteId]
          ,[SiteIdChangeRaised]
          ,[ConsideredForTransferQue]
          ,[Created]
          ,[LastUpdated]
    FROM [bpc].[MiuOwnerHistory]
    WHERE [MiuId] = @MiuId
    ORDER BY [Timestamp]

END
