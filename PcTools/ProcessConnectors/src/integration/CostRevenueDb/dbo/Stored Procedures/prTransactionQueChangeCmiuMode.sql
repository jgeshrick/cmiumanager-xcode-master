﻿-- Adds a CMIU mode change event to the QAD transfer queue
CREATE PROCEDURE [dbo].[prTransactionQueChangeCmiuMode]
(
    @MiuId INT,
    @CmiuMode VARCHAR(20),
    @ReadingIntervalMins INT,
    @RecordingIntervalMins INT,
    @ReportingIntervalMins INT,
    @UtcTimestamp DATETIME
)
AS
BEGIN

    -- TODO: This is a test implementation only.
    DECLARE 
        @Key VARCHAR(500), 
        @Content VARCHAR(500)

    SELECT @Key = CAST (@MiuId AS VARCHAR) + '|' + @CmiuMode;

    SELECT 
        @Content = CAST (@MiuId AS VARCHAR) + '|' + @CmiuMode + '|' + 
        CAST (@ReadingIntervalMins AS VARCHAR) + '|' + CAST (@RecordingIntervalMins AS VARCHAR) + '|' + 
        CAST (@ReportingIntervalMins AS VARCHAR) + '|' + FORMAT(@UtcTimestamp,'yyyy-MM-dd HH:mm:ss')

    INSERT INTO [bpc].[TestTransferQue] ([tranType], [tranInDate], [tranKey], [tranStatus], [tranContent], [tranSrc])
    VALUES ('04', SYSUTCDATETIME(), @Key, 'O', @Content, 'BPC C&R Data Connector')

END
