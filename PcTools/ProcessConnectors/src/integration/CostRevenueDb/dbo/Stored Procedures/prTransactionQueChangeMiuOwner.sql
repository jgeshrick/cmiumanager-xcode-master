﻿-- Adds an site ID change event to the QAD transfer queue
CREATE PROCEDURE [dbo].[prTransactionQueChangeMiuOwner]
(
    @MiuId INT,
    @NewSiteId INT,
    @UtcTimestamp DATETIME
)
AS
BEGIN

    -- TODO: This is a test implementation only.
    DECLARE 
        @Key VARCHAR(500), 
        @Content VARCHAR(500)

    SELECT @Key = CAST (@MiuId AS VARCHAR) + '|' + CAST (@NewSiteId AS VARCHAR);

    SELECT @Content = CAST (@MiuId AS VARCHAR) + '|' + CAST (@NewSiteId AS VARCHAR) + '|' + FORMAT(@UtcTimestamp,'yyyy-MM-dd HH:mm:ss')

    INSERT INTO [bpc].[TestTransferQue] ([tranType], [tranInDate], [tranKey], [tranStatus], [tranContent], [tranSrc])
    VALUES ('07', SYSUTCDATETIME(), @Key, 'O', @Content, 'BPC C&R Data Connector')

END
