﻿-- As defined by QAD.
CREATE TABLE [dbo].[TransferQue](
	[tranType] VARCHAR(5) NULL,
	[tranInDate] [datetime] NULL,
	[tranKey] [varchar](500) NOT NULL,
	[tranOutDate] [datetime] NULL,
	[tranStatus] VARCHAR(50) NULL,
	[tranContent] VARCHAR(500) NULL,
	[tranSrc] VARCHAR(50) NULL,
	[tranMsg] VARCHAR(200) NULL, 
    CONSTRAINT [PK_TransferQue] PRIMARY KEY ([tranKey])
) ON [PRIMARY]
