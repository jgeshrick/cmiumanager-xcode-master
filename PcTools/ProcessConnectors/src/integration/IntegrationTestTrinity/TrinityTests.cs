﻿
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************
using System;
using FpcClient.Settings;
using FpcClient.JsonModel.CellularConfig;
using System.Threading;
using FpcClient.Common.StatusCodes;
using FpcClientLibrary;
using NUnit.Framework;
using FpcClient.Common.CmiuCanConfigurationStructures;
using FpcClient.Common.ConfigurationStructures;
using FpcClient.Common.ServerConfigurationStructures;
using FpcClient.Common.CellularProviderCodes;

namespace FpcClient.IntegrationTestTrinity
{
    [TestFixture()]
    public class TrinityTests
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Partner key for partner number 2, site 0 (non-production environments)
        private const string PARTNER_ID_1 = "eb750ee73b5cf9d04ec78a0b23616e9b";
        private const string testPartnerIdFail = "PartnerIdFail";
        private const uint TEST_CMUID_TOO_HIGH = 567890000;

        private const uint TEST_CMUID = 400000228;

        private const string TEST_IMEI_LONG             = "12345678901234567";
        private const string TEST_ICCID_LONG            = "123456789012345678901234";

        private const string TEST_IMEI_VALIDV_SHORT  = "45432109876";
        private const string TEST_IMEI_VALIDA_SHORT  = "45432109876";
        private const string TEST_IMEI_VALIDA_SHORT2 = "45432109876";

        private const string TEST_ICCID_VALIDV_SHORT = "4543210987654321";
        private const string TEST_ICCID_VALIDA_SHORT = "4543210987654321";
        private const string TEST_ICCID_VALIDA_SHORT2 = "4543210987654321";

        /// <summary>
        /// Set CMUI CAN for Id 4666 through Trinity.exe (LabVIEW commander)
        /// Get CMUI CAN for Id 4666 through FPCClient.dll
        /// Compare values for test result
        /// </summary>
        [Test()]
        public void TestTrinitySetVariableCmiuVzw()
        {
            uint testAdjustment = 1800;
            uint cmiuId = TEST_CMUID + testAdjustment;

            string imei = TEST_IMEI_VALIDV_SHORT + testAdjustment.ToString();
            string iccid = TEST_ICCID_VALIDV_SHORT + testAdjustment.ToString();

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "Trinity.exe";
            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;
            
            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            Int32 exitCode = process.ExitCode;
            Int32 expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            var testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            CmiuCan oldCellularConfig = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(imei, oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(iccid, oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(cmiuId, oldCellularConfig.cmiuId);
        }

        /// <summary>
        /// Set CMUI CAN for Id 4666 through Trinity.exe (LabVIEW commander)
        /// Firstly set invlaid IMEI string, secondly set invlaid ICCID string.
        /// In each case check that the item is unchanged.
        /// Then set invalid CMIUD and ensure Trinity does not crash.
        /// </summary>
        [Test()]
        public void TestTrinitySetDisallowedTextVariablesVzw()
        {
            uint testAdjustment = 1802;
            uint cmiuId = TEST_CMUID + testAdjustment;


            string imei = TEST_IMEI_VALIDV_SHORT + testAdjustment.ToString();
            string iccid = TEST_ICCID_VALIDV_SHORT + testAdjustment.ToString();

            log.DebugFormat("TestTrinitySetDisallowedTextVariables = {0} {1} {2}", cmiuId.ToString(), imei, iccid);

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "Trinity.exe";
            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            var assemblyCodeBase = AppDomain.CurrentDomain.BaseDirectory;
            var myPath = assemblyCodeBase.ToString();

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            Int32 exitCode = process.ExitCode;
            Int32 expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            var testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            CmiuCan oldCellularConfig = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(imei, oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(iccid, oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = cmiuId.ToString() + " " + "ERROR" + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            CmiuCan newCellularConfig = new CmiuCan();
            statusCode = fpcClient.GetCmiuCan(cmiuId, ref newCellularConfig);
            Assert.AreEqual(newCellularConfig.modemCan.imei.ToString(), oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(newCellularConfig.simCan.iccid.ToString(), oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(newCellularConfig.cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + "ERROR";
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            statusCode = fpcClient.GetCmiuCan(cmiuId, ref newCellularConfig);
            Assert.AreEqual(newCellularConfig.modemCan.imei.ToString(), oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(newCellularConfig.simCan.iccid.ToString(), oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(newCellularConfig.cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = "ERROR" + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = %s", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);
        }

        /// <summary>
        /// Set CMUI CAN for Id 4666 through Trinity.exe (LabVIEW commander)
        /// Firstly set invlaid negative IMEI, secondly set invlaid negative ICCID.
        /// In each case check that the item is unchanged.
        /// Then set negative CMIUD and ensure Trinity does not crash.
        /// </summary>
        [Test()]
        public void TestTrinitySetDisallowedNegativeVariablesVzw()
        {
            uint testAdjustment = 1804;
            uint cmiuId = TEST_CMUID + testAdjustment;

            string imei = TEST_IMEI_VALIDV_SHORT + testAdjustment.ToString();
            string iccid = TEST_ICCID_VALIDV_SHORT + testAdjustment.ToString();

            log.DebugFormat("TestTrinitySetDisallowedTextVariables = {0} {1} {2}", cmiuId.ToString(), imei, iccid);

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "Trinity.exe";
            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            Int32 exitCode = process.ExitCode;
            Int32 expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            var testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            CmiuCan oldCellularConfig = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(imei, oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(iccid, oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = cmiuId.ToString() + " " + "-1234" + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            CmiuCan newCellularConfig = new CmiuCan();
            statusCode = fpcClient.GetCmiuCan(cmiuId, ref newCellularConfig);
            Assert.AreEqual(newCellularConfig.modemCan.imei.ToString(), oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(newCellularConfig.simCan.iccid.ToString(), oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(newCellularConfig.cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + "-5678";
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            statusCode = fpcClient.GetCmiuCan(cmiuId, ref newCellularConfig); 
            Assert.AreEqual(newCellularConfig.modemCan.imei.ToString(), oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(newCellularConfig.simCan.iccid.ToString(), oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(newCellularConfig.cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = "-9630" + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = %s", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);
        }


        /// <summary>
        /// Set CMUI CAN for Id 4666 through Trinity.exe (LabVIEW commander)
        /// Firstly set invlaid long IMEI, secondly set invlaid long ICCID.
        /// In each case check that the item is unchanged.
        /// Then set high CMIUD and ensure Trinity does not crash.
        /// </summary
        [Test()]
        public void TestTrinitySetDisallowedRangeVariablesVzw()
        {
            uint testAdjustment = 1806;
            uint cmiuId = TEST_CMUID + testAdjustment;

            string imei = TEST_IMEI_VALIDV_SHORT + testAdjustment.ToString();
            string iccid = TEST_ICCID_VALIDV_SHORT + testAdjustment.ToString();

            log.DebugFormat("TestTrinitySetDisallowedTextVariables = {0} {1} {2}", cmiuId.ToString(), imei, iccid);

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "Trinity.exe";
            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            Int32 exitCode = process.ExitCode;
            Int32 expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            var testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            CmiuCan oldCellularConfig = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(imei, oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(iccid, oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = cmiuId.ToString() + " " + "TEST_IMEI_LONG" + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            CmiuCan newCellularConfig = new CmiuCan();
            statusCode = fpcClient.GetCmiuCan(cmiuId, ref newCellularConfig);
            Assert.AreEqual(newCellularConfig.modemCan.imei.ToString(), oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(newCellularConfig.simCan.iccid.ToString(), oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(newCellularConfig.cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + "TEST_ICCID_LONG";
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            statusCode = fpcClient.GetCmiuCan(cmiuId, ref newCellularConfig);
            Assert.AreEqual(newCellularConfig.modemCan.imei.ToString(), oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(newCellularConfig.simCan.iccid.ToString(), oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(newCellularConfig.cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = "TEST_CMUID_TOO_HIGH" + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = %s", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);
        }



        /// <summary>
        /// Set CMUI CAN for Id 4666 through Trinity.exe (LabVIEW commander)
        /// Get CMUI CAN for Id 4666 through FPCClient.dll
        /// Compare values for test result
        /// </summary>
        [Test()]
        public void TestTrinitySetVariableCmiuAtt()
        {
            uint testAdjustment = 1808;
            uint cmiuId = TEST_CMUID + testAdjustment;

            string imei = TEST_IMEI_VALIDA_SHORT + testAdjustment.ToString();
            string iccid = TEST_ICCID_VALIDA_SHORT + testAdjustment.ToString();

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "Trinity.exe";
            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            Int32 exitCode = process.ExitCode;
            Int32 expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            var testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            CmiuCan oldCellularConfig = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(imei, oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(iccid, oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(cmiuId, oldCellularConfig.cmiuId);
        }

        /// <summary>
        /// Set CMUI CAN for Id 4666 through Trinity.exe (LabVIEW commander)
        /// Firstly set invlaid IMEI string, secondly set invlaid ICCID string.
        /// In each case check that the item is unchanged.
        /// Then set invalid CMIUD and ensure Trinity does not crash.
        /// </summary>
        [Test()]
        public void TestTrinitySetDisallowedTextVariablesAtt()
        {
            uint testAdjustment = 1810;
            uint cmiuId = TEST_CMUID + testAdjustment;

            string imei = TEST_IMEI_VALIDA_SHORT + testAdjustment.ToString();
            string iccid = TEST_ICCID_VALIDA_SHORT + testAdjustment.ToString();

            log.DebugFormat("TestTrinitySetDisallowedTextVariables = {0} {1} {2}", cmiuId.ToString(), imei, iccid);

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "Trinity.exe";
            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            var assemblyCodeBase = AppDomain.CurrentDomain.BaseDirectory;
            var myPath = assemblyCodeBase.ToString();

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            Int32 exitCode = process.ExitCode;
            Int32 expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            var testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            CmiuCan oldCellularConfig = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(imei, oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(iccid, oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = cmiuId.ToString() + " " + "ERROR" + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            CmiuCan newCellularConfig = new CmiuCan();
            statusCode = fpcClient.GetCmiuCan(cmiuId, ref newCellularConfig);
            Assert.AreEqual(newCellularConfig.modemCan.imei.ToString(), oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(newCellularConfig.simCan.iccid.ToString(), oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(newCellularConfig.cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + "ERROR";
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            statusCode = fpcClient.GetCmiuCan(cmiuId, ref newCellularConfig);
            Assert.AreEqual(newCellularConfig.modemCan.imei.ToString(), oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(newCellularConfig.simCan.iccid.ToString(), oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(newCellularConfig.cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = "ERROR" + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = %s", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);
        }

        /// <summary>
        /// Set CMUI CAN for Id 4666 through Trinity.exe (LabVIEW commander)
        /// Firstly set invlaid negative IMEI, secondly set invlaid negative ICCID.
        /// In each case check that the item is unchanged.
        /// Then set negative CMIUD and ensure Trinity does not crash.
        /// </summary>
        [Test()]
        public void TestTrinitySetDisallowedNegativeVariablesAtt()
        {
            uint testAdjustment = 1812;
            uint cmiuId = TEST_CMUID + testAdjustment;

            string imei = TEST_IMEI_VALIDA_SHORT + testAdjustment.ToString();
            string iccid = TEST_ICCID_VALIDA_SHORT + testAdjustment.ToString();

            log.DebugFormat("TestTrinitySetDisallowedTextVariables = {0} {1} {2}", cmiuId.ToString(), imei, iccid);

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "Trinity.exe";
            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000); //Wait up to a minute
            process.Kill();

            Int32 exitCode = process.ExitCode;
            Int32 expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            var testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            CmiuCan oldCellularConfig = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(imei, oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(iccid, oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = cmiuId.ToString() + " " + "-1234" + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            CmiuCan newCellularConfig = new CmiuCan();
            statusCode = fpcClient.GetCmiuCan(cmiuId, ref newCellularConfig);
            Assert.AreEqual(newCellularConfig.modemCan.imei.ToString(), oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(newCellularConfig.simCan.iccid.ToString(), oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(newCellularConfig.cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + "-5678";
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            statusCode = fpcClient.GetCmiuCan(cmiuId, ref newCellularConfig);
            Assert.AreEqual(newCellularConfig.modemCan.imei.ToString(), oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(newCellularConfig.simCan.iccid.ToString(), oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(newCellularConfig.cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = "-9630" + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = %s", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);
        }


        /// <summary>
        /// Set CMUI CAN for Id 4666 through Trinity.exe (LabVIEW commander)
        /// Firstly set invlaid long IMEI, secondly set invlaid long ICCID.
        /// In each case check that the item is unchanged.
        /// Then set high CMIUD and ensure Trinity does not crash.
        /// </summary
        [Test()]
        public void TestTrinitySetDisallowedRangeVariablesAtt()
        {
            uint testAdjustment = 1814;
            uint cmiuId = TEST_CMUID + testAdjustment;

            string imei = TEST_IMEI_VALIDA_SHORT + testAdjustment.ToString();
            string iccid = TEST_ICCID_VALIDA_SHORT + testAdjustment.ToString();

            log.DebugFormat("TestTrinitySetDisallowedTextVariables = {0} {1} {2}", cmiuId.ToString(), imei, iccid);

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "Trinity.exe";
            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            Int32 exitCode = process.ExitCode;
            Int32 expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            var testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            CmiuCan oldCellularConfig = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(imei, oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(iccid, oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = cmiuId.ToString() + " " + "TEST_IMEI_LONG" + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            CmiuCan newCellularConfig = new CmiuCan();
            statusCode = fpcClient.GetCmiuCan(cmiuId, ref newCellularConfig);
            Assert.AreEqual(newCellularConfig.modemCan.imei.ToString(), oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(newCellularConfig.simCan.iccid.ToString(), oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(newCellularConfig.cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + "TEST_ICCID_LONG";
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            statusCode = fpcClient.GetCmiuCan(cmiuId, ref newCellularConfig);
            Assert.AreEqual(newCellularConfig.modemCan.imei.ToString(), oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(newCellularConfig.simCan.iccid.ToString(), oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(newCellularConfig.cmiuId, oldCellularConfig.cmiuId);

            startInfo.Arguments = "TEST_CMUID_TOO_HIGH" + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = %s", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);
        }




        /// <summary>
        /// Set CMUI CAN through Trinity.exe (LabVIEW commander)
        /// Get CMUI CAN through FPCClient.dll
        /// Compare values for test result
        /// Set CMIU CAN using new CMIU Id and new IMEI but duplicate ICCID
        /// Get CMUI CAN through FPCClient.dll
        /// Check status for error as record should not have been created
        /// </summary>
        [Test()]
        public void TestTrinitySetDuplicateIccid()
        {
            uint testAdjustment = 1820;
            uint cmiuId = TEST_CMUID + testAdjustment;

            string imei = TEST_IMEI_VALIDA_SHORT + testAdjustment.ToString();
            string iccid = TEST_ICCID_VALIDA_SHORT + testAdjustment.ToString();

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "Trinity.exe";
            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            Int32 exitCode = process.ExitCode;
            Int32 expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            var testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            CmiuCan oldCellularConfig = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(imei, oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(iccid, oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(cmiuId, oldCellularConfig.cmiuId);

            // Set using new CMIU Id and IMEI but duplicate ICCID
            imei = TEST_IMEI_VALIDA_SHORT2 + testAdjustment.ToString();
            testAdjustment = 1822;
            cmiuId = TEST_CMUID + testAdjustment;


            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            oldCellularConfig = new CmiuCan();
            statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, statusCode);
        }





        /// <summary>
        /// Set CMUI CAN through Trinity.exe (LabVIEW commander)
        /// Get CMUI CAN through FPCClient.dll
        /// Compare values for test result
        /// Set CMIU CAN using new CMIU Id and new ICCID but duplicate IMEI
        /// Get CMUI CAN through FPCClient.dll
        /// Check status for error as record should not have been created
        /// </summary>
        [Test()]
        public void TestTrinitySetDuplicateImei()
        {
            uint testAdjustment = 1824;
            uint cmiuId = TEST_CMUID + testAdjustment;

            string imei = TEST_IMEI_VALIDA_SHORT + testAdjustment.ToString();
            string iccid = TEST_ICCID_VALIDA_SHORT + testAdjustment.ToString();

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "Trinity.exe";
            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            Int32 exitCode = process.ExitCode;
            Int32 expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            var testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            CmiuCan oldCellularConfig = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(imei, oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(iccid, oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(cmiuId, oldCellularConfig.cmiuId);

            // Set using new CMIU Id and IMEI but duplicate ICCID
            testAdjustment = 1826;
            iccid = TEST_ICCID_VALIDA_SHORT2 + testAdjustment.ToString();
            cmiuId = TEST_CMUID + testAdjustment;


            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            oldCellularConfig = new CmiuCan();
            statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, statusCode);
        }




        /// <summary>
        /// Set CMUI CAN through Trinity.exe (LabVIEW commander)
        /// Get CMUI CAN through FPCClient.dll
        /// Compare values for test result
        /// Set CMIU CAN using new CMIU Id and new ICCID but duplicate IMEI
        /// Get CMUI CAN through FPCClient.dll
        /// Check status for error as record should not have been created
        /// </summary>
        [Test()]
        public void TestTrinitySetDuplicateImeiIccid()
        {
            uint testAdjustment = 1850;
            uint cmiuId = TEST_CMUID + testAdjustment;

            string imei = TEST_IMEI_VALIDA_SHORT + testAdjustment.ToString();
            string iccid = TEST_ICCID_VALIDA_SHORT + testAdjustment.ToString();

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "Trinity.exe";
            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            Int32 exitCode = process.ExitCode;
            Int32 expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            var fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            var testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            CmiuCan oldCellularConfig = new CmiuCan();
            var statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(imei, oldCellularConfig.modemCan.imei.ToString());
            Assert.AreEqual(iccid, oldCellularConfig.simCan.iccid.ToString());
            Assert.AreEqual(cmiuId, oldCellularConfig.cmiuId);

            // Set using new CMIU Id and IMEI but duplicate ICCID
            testAdjustment = 1852;
            cmiuId = TEST_CMUID + testAdjustment;


            startInfo.Arguments = cmiuId.ToString() + " " + imei + " " + iccid;
            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(60000);
            process.Kill();

            exitCode = process.ExitCode;
            expectedCode = 0;
            Console.WriteLine("exitCode = {0}", exitCode.ToString());

            Assert.AreEqual(expectedCode, exitCode);

            ///   * Create client
            fpcClient = FpcFactory.CreateFpcClient();
            Assert.IsNotNull(fpcClient);

            ///   * Initialise client
            testCode = fpcClient.InitFpcClient(PARTNER_ID_1);
            Assert.AreEqual(StatusCode.OK, testCode);

            oldCellularConfig = new CmiuCan();
            statusCode = fpcClient.GetCmiuCan(cmiuId, ref oldCellularConfig);
            Assert.AreEqual(StatusCode.ErrorServerResponseNotFound, statusCode);
        }

    }
}
