﻿namespace BpcCoreTestHarness
{
    partial class TestMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_GetConfigurationHistory = new System.Windows.Forms.Button();
            this.bt_GetConfigurationHistoryAllInDate = new System.Windows.Forms.Button();
            this.bt_GetConfigurationHistoryForSiteId1 = new System.Windows.Forms.Button();
            this.bt_GetCellularConfigFixedId = new System.Windows.Forms.Button();
            this.bt_GetCellularConfigFixedAllNotNull = new System.Windows.Forms.Button();
            this.bt_SetCellularConfig = new System.Windows.Forms.Button();
            this.bt_SetCRMReferenceData = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.returndata_textbox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // bt_GetConfigurationHistory
            // 
            this.bt_GetConfigurationHistory.BackColor = System.Drawing.Color.Cornsilk;
            this.bt_GetConfigurationHistory.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bt_GetConfigurationHistory.Location = new System.Drawing.Point(13, 13);
            this.bt_GetConfigurationHistory.Name = "bt_GetConfigurationHistory";
            this.bt_GetConfigurationHistory.Size = new System.Drawing.Size(235, 23);
            this.bt_GetConfigurationHistory.TabIndex = 0;
            this.bt_GetConfigurationHistory.Text = "Get Configuration History";
            this.bt_GetConfigurationHistory.UseVisualStyleBackColor = false;
            this.bt_GetConfigurationHistory.Click += new System.EventHandler(this.button1_Click);
            // 
            // bt_GetConfigurationHistoryAllInDate
            // 
            this.bt_GetConfigurationHistoryAllInDate.BackColor = System.Drawing.Color.Cornsilk;
            this.bt_GetConfigurationHistoryAllInDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bt_GetConfigurationHistoryAllInDate.Location = new System.Drawing.Point(13, 43);
            this.bt_GetConfigurationHistoryAllInDate.Name = "bt_GetConfigurationHistoryAllInDate";
            this.bt_GetConfigurationHistoryAllInDate.Size = new System.Drawing.Size(235, 23);
            this.bt_GetConfigurationHistoryAllInDate.TabIndex = 1;
            this.bt_GetConfigurationHistoryAllInDate.Text = "Get Configuration History All In Date";
            this.bt_GetConfigurationHistoryAllInDate.UseVisualStyleBackColor = false;
            this.bt_GetConfigurationHistoryAllInDate.Click += new System.EventHandler(this.button2_Click);
            // 
            // bt_GetConfigurationHistoryForSiteId1
            // 
            this.bt_GetConfigurationHistoryForSiteId1.BackColor = System.Drawing.Color.Cornsilk;
            this.bt_GetConfigurationHistoryForSiteId1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bt_GetConfigurationHistoryForSiteId1.Location = new System.Drawing.Point(13, 73);
            this.bt_GetConfigurationHistoryForSiteId1.Name = "bt_GetConfigurationHistoryForSiteId1";
            this.bt_GetConfigurationHistoryForSiteId1.Size = new System.Drawing.Size(235, 23);
            this.bt_GetConfigurationHistoryForSiteId1.TabIndex = 2;
            this.bt_GetConfigurationHistoryForSiteId1.Text = "Get Configuration History For Site Id 1";
            this.bt_GetConfigurationHistoryForSiteId1.UseVisualStyleBackColor = false;
            this.bt_GetConfigurationHistoryForSiteId1.Click += new System.EventHandler(this.button3_Click);
            // 
            // bt_GetCellularConfigFixedId
            // 
            this.bt_GetCellularConfigFixedId.BackColor = System.Drawing.Color.Cornsilk;
            this.bt_GetCellularConfigFixedId.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bt_GetCellularConfigFixedId.Location = new System.Drawing.Point(13, 103);
            this.bt_GetCellularConfigFixedId.Name = "bt_GetCellularConfigFixedId";
            this.bt_GetCellularConfigFixedId.Size = new System.Drawing.Size(235, 23);
            this.bt_GetCellularConfigFixedId.TabIndex = 3;
            this.bt_GetCellularConfigFixedId.Text = "Get Cellular Config Fixed Id";
            this.bt_GetCellularConfigFixedId.UseVisualStyleBackColor = false;
            this.bt_GetCellularConfigFixedId.Click += new System.EventHandler(this.button4_Click);
            // 
            // bt_GetCellularConfigFixedAllNotNull
            // 
            this.bt_GetCellularConfigFixedAllNotNull.BackColor = System.Drawing.Color.Cornsilk;
            this.bt_GetCellularConfigFixedAllNotNull.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bt_GetCellularConfigFixedAllNotNull.Location = new System.Drawing.Point(13, 133);
            this.bt_GetCellularConfigFixedAllNotNull.Name = "bt_GetCellularConfigFixedAllNotNull";
            this.bt_GetCellularConfigFixedAllNotNull.Size = new System.Drawing.Size(235, 23);
            this.bt_GetCellularConfigFixedAllNotNull.TabIndex = 4;
            this.bt_GetCellularConfigFixedAllNotNull.Text = "Get Cellular Config Fixed All Not Null";
            this.bt_GetCellularConfigFixedAllNotNull.UseVisualStyleBackColor = false;
            this.bt_GetCellularConfigFixedAllNotNull.Click += new System.EventHandler(this.button5_Click);
            // 
            // bt_SetCellularConfig
            // 
            this.bt_SetCellularConfig.BackColor = System.Drawing.Color.Cornsilk;
            this.bt_SetCellularConfig.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bt_SetCellularConfig.Location = new System.Drawing.Point(13, 163);
            this.bt_SetCellularConfig.Name = "bt_SetCellularConfig";
            this.bt_SetCellularConfig.Size = new System.Drawing.Size(235, 23);
            this.bt_SetCellularConfig.TabIndex = 5;
            this.bt_SetCellularConfig.Text = "Set Cellular Config";
            this.bt_SetCellularConfig.UseVisualStyleBackColor = false;
            this.bt_SetCellularConfig.Click += new System.EventHandler(this.button6_Click);
            // 
            // bt_SetCRMReferenceData
            // 
            this.bt_SetCRMReferenceData.BackColor = System.Drawing.Color.Cornsilk;
            this.bt_SetCRMReferenceData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bt_SetCRMReferenceData.Location = new System.Drawing.Point(13, 193);
            this.bt_SetCRMReferenceData.Name = "bt_SetCRMReferenceData";
            this.bt_SetCRMReferenceData.Size = new System.Drawing.Size(235, 23);
            this.bt_SetCRMReferenceData.TabIndex = 6;
            this.bt_SetCRMReferenceData.Text = "Set CRM Reference Data";
            this.bt_SetCRMReferenceData.UseVisualStyleBackColor = false;
            this.bt_SetCRMReferenceData.Click += new System.EventHandler(this.button7_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.BurlyWood;
            this.textBox1.Location = new System.Drawing.Point(277, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(272, 20);
            this.textBox1.TabIndex = 10;
            // 
            // returndata_textbox
            // 
            this.returndata_textbox.BackColor = System.Drawing.Color.Wheat;
            this.returndata_textbox.Location = new System.Drawing.Point(277, 43);
            this.returndata_textbox.Multiline = true;
            this.returndata_textbox.Name = "returndata_textbox";
            this.returndata_textbox.ReadOnly = true;
            this.returndata_textbox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.returndata_textbox.Size = new System.Drawing.Size(272, 258);
            this.returndata_textbox.TabIndex = 20;
            // 
            // TestMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 313);
            this.Controls.Add(this.returndata_textbox);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.bt_SetCRMReferenceData);
            this.Controls.Add(this.bt_SetCellularConfig);
            this.Controls.Add(this.bt_GetCellularConfigFixedAllNotNull);
            this.Controls.Add(this.bt_GetCellularConfigFixedId);
            this.Controls.Add(this.bt_GetConfigurationHistoryForSiteId1);
            this.Controls.Add(this.bt_GetConfigurationHistoryAllInDate);
            this.Controls.Add(this.bt_GetConfigurationHistory);
            this.Name = "TestMain";
            this.Text = "BpcCore Test Harness";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_GetConfigurationHistory;
        private System.Windows.Forms.Button bt_GetConfigurationHistoryAllInDate;
        private System.Windows.Forms.Button bt_GetConfigurationHistoryForSiteId1;
        private System.Windows.Forms.Button bt_GetCellularConfigFixedId;
        private System.Windows.Forms.Button bt_GetCellularConfigFixedAllNotNull;
        private System.Windows.Forms.Button bt_SetCellularConfig;
        private System.Windows.Forms.Button bt_SetCRMReferenceData;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox returndata_textbox;
    }
}

