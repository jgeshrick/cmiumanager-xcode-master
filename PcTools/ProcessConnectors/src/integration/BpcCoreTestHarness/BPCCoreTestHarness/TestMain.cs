﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BpcCoreTestHarness
{
    public partial class TestMain : Form
    {
        private const int TEST_MIU_ID = 400000110;

        #region TEST_JSON_STRING
        /// <summary>
        /// Json string extracted from ETI CRM Reference Data (IF42)
        /// </summary>
        public static string JSON_CRM_REFERENCE = @"
{
	""distributers"": [
		{
			""customer_number"": ""08225300"",
			""info"": {
				""account_name"": ""Distributer X"",
                          ""customer_number"": ""8070000"",
				""account_id"": ""A3QEYA100000"",
				""address1"": ""Distributer Land"",
				""city"": ""Distributer Town"",
				""country"": ""USA"",
                          ""main_phone"": ""(334) 123-4567"",
				""fax"": ""(334) 123-4568"",
				""sales_territory_owner"": ""24020"",
				""account_manager"": ""Smith, Kevin"",
				""regional_manager"": ""Elliott, Mitch"",
				""type"": ""Distributer"",
				""sub_type"": """",
				""status"": ""Active"",
				""primary_contact"": {
					""name"": ""Stributer, Dee"",
					""work_phone"": ""(334) 123-4569"",
					""title"": """"					
				},
				""active_secondary_contacts"": [
					{
						""name"": ""de Stribouteur, M."",
						""work_phone"": ""(334) 123-4570"",
						""title"": ""Project Manager""					
					}
				],
				""customer_numbers"": [
					""743""
				]
			}
		}
	],
	""utilities"": [
		{
			""site_id"": 36801
			""info"": {
				""account_id"": ""A3QEYA100000"",
                          ""customer_number"": ""8070000"",
				""system_id"": ""127"",
				""account_name"": ""Opelika, City Of"",
				""address1"": ""Water Works Board"",
				""address2"": ""PO Box #1029"",
				""city"": "" Opelika "",
				""postal_code"": ""36801"",
				""state"": ""AL"",
				""country"": ""USA"",
				""main_phone"": ""(334) 705-5500"",
				""fax"": ""(334) 754-3487"",
				""sales_territory_owner"": ""24020"",
				""account_manager"": ""Smith, Kevin"",
				""regional_manager"": ""Elliott, Mitch"",
				""type"": ""Municipal - Neg"",
				""sub_type"": ""Key Account"",
				""status"": ""Active"",
				""primary_contact"": {
					""name"": ""Hilyer, Dan"",
					""work_phone"": ""(334) 705-5505"",
					""title"": """"					
				},
				""active_secondary_contacts"": [
					{
						""name"": ""Lee, Alan"",
						""work_phone"": ""(334) 705-5500"",
						""title"": ""Project Manager""					
					},
					{
						""name"": ""Wood, Robin"",
						""work_phone"": ""(334) 749-7589"",
						""title"": """"					
					}
				],
				""customer_numbers"": [
					""742""
				],
				""parent_account_number"": ""123456""
			}
		},
		{
			""site_id"": 30223
			""info"": {
				""account_id"": ""A3QEYA100000"",
				""system_id"": ""128"",
				""account_name"": ""Griffin, City Of"",
                          ""customer_number"": ""8070000"",
				""address1"": ""229 North Expressway"",
				""address2"": ""PO Box T"",
				""city"": ""Griffin "",
				""postal_code"": ""36801"",
				""state"": ""GA"",
				""country"": ""USA"",
                          ""main_phone"": ""(770) 229-6423"",
				""fax"": ""(770) 754-3487"",
				""sales_territory_owner"": ""24040"",
				""account_manager"": ""Cone, Dallas"",
				""regional_manager"": ""Elliott, Mitch"",
				""type"": ""End User"",
				""sub_type"": """",
				""status"": ""Active"",
				""primary_contact"": {
					""name"": ""Keller, Brant"",
					""work_phone"": ""(770) 229-6423"",
					""title"": """"					
				},
				""active_secondary_contacts"": [
				],
				""customer_numbers"": [
					""1234567""
				],
				""parent_customer_number"": ""08225300""
			}
		}		
	
	]
}
";
        #endregion

        public TestMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// GetConfigurationHistory
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            returndata_textbox.Text = "";
            this.Refresh();
            try
            {
                //TODO: Add functionality here
                string result = "TODO";
                textBox1.Text = "PASS";
                returndata_textbox.Text = result;
            }
            catch (Exception)
            {
                textBox1.Text = "FAIL";
                throw;
            }
        }

        /// <summary>
        /// GetConfigurationHistoryAllInDate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            returndata_textbox.Text = "";
            this.Refresh();
            try
            {
                //TODO: Add functionality here
                string result = "TODO";
                textBox1.Text = "PASS";
            }
            catch (Exception)
            {
                textBox1.Text = "FAIL";
                throw;
            }
        }

        /// <summary>
        /// GetConfigurationHistoryForSiteId1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            returndata_textbox.Text = "";
            this.Refresh();
            try
            {
                //TODO: Add functionality here
                string result = "TODO";
                textBox1.Text = "PASS";
                returndata_textbox.Text = result;
            }
            catch (Exception)
            {
                textBox1.Text = "FAIL";
                throw;
            }
        }

        /// <summary>
        /// GetCellularConfigFixedId
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            returndata_textbox.Text = "";
            this.Refresh();
            try
            {
                //TODO: Add functionality here
                string result = "TODO";
                textBox1.Text = "PASS";
                returndata_textbox.Text = result;
            }
            catch (Exception)
            {
                textBox1.Text = "FAIL";
                throw;
            }
        }

        /// <summary>
        /// GetCellularConfigTestFixedAllNotNull
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            returndata_textbox.Text = "";
            this.Refresh();
            try
            {
                //TODO: Add functionality here
                string result = "TODO";
                textBox1.Text = "PASS";
                returndata_textbox.Text = result;
            }
            catch (Exception)
            {
                textBox1.Text = "FAIL";
                throw;
            }
        }

        /// <summary>
        /// SetCellularConfigTest
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            returndata_textbox.Text = "";
            this.Refresh();
            try
            {
                //TODO: Add functionality here
                string result = "TODO";
                textBox1.Text = "PASS";
                returndata_textbox.Text = result;
            }
            catch (Exception)
            {
                textBox1.Text = "FAIL";
                throw;
            }
        }

        /// <summary>
        /// SetCRMReferenceData
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            returndata_textbox.Text = "";
            this.Refresh();
            try
            {
                //TODO: Add functionality here
                string result = "TODO";
                textBox1.Text = "PASS";
                returndata_textbox.Text = result.ToString();
            }
            catch (Exception)
            {
                textBox1.Text = "FAIL";
                throw;
            }
        }
    }
}
