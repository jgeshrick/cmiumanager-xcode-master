﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using NUnit.Framework;
using NeptuneCloudConnector.Exception;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeptuneCloudConnector.Tests
{
    [TestFixture()]
    public class NeptuneCloudConnectorTest
    {
        private const string SiteId = "0";
        private const string PartnerString1 = "c2a31f64e9430f1c8b85ccbbb776ad75";

        /// <summary>
        /// Verify the service authentication process using a list of known services.
        /// </summary>
        [Test]
        public void GetAuthenticationTokenTestId1()
        {
            // Auto Testing isn't always available, so try some other servers before asserting failure.
            var serviceInfoList = new List<ServiceInfo>
            {
                new ServiceInfo("Auto Testing", "https://auto-test.mdce-nonprod.neptunensight.com:8444/", "mdce/api/v1/"),
                new ServiceInfo("Development1", "https://dev1.mdce-nonprod.neptunensight.com:8444/", "mdce/api/v1/"),
                new ServiceInfo("Integration", "https://int.mdce-nonprod.neptunensight.com:8444/", "mdce/api/v1/")
            };

            var wasConnectionMade = false;

            foreach (var serviceInfo in serviceInfoList)
            {
                try
                {
                    string url = serviceInfo.ServiceUrl;
                    string subUrl = serviceInfo.ServicePath;

                    Console.WriteLine("Trying {0}...", serviceInfo.Environment);
                    Console.WriteLine("NCC_INTEGRATION_URL environment variable = {0}", url);
                    Console.WriteLine("NCC_INTEGRATION_SUBURL environment variable = {0}", subUrl);

                    var pcei = new NeptuneCloudRestClient(PartnerString1, SiteId, url, subUrl);

                    var response = pcei.GetAuthenticationToken();

                    Assert.IsTrue(response.Authenticated);
                    Assert.IsNotEmpty(response.AuthToken);
                    Assert.IsNotNull(response.Expires);
                    Assert.IsNotNull(response.AuthenticatedAt);
                    Assert.AreEqual(16, response.AuthToken.Length);

                    Console.WriteLine("Authentication against {0} was successful.", serviceInfo.Environment);

                    wasConnectionMade = true;

                    // Break as soon as a connection to one of the environments succeeds
                    break;
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine("Connection to {0} failed. Exception detail: {1}", serviceInfo.Environment, ex);
                }
            }

            if (!wasConnectionMade)
            {
                Assert.Fail("The authentication attempts were unsuccessful.");
            }
        }

        [Test]
        public void GetHttp500ResponseTest()
        {
            bool testResult = false;
            var pcei = new NeptuneCloudRestClient(PartnerString1, SiteId, "http://wrong-url");

            try
            {
                var token = pcei.GetAuthenticationToken();
            }
            catch (RestException)
            {
                testResult = true;
            }
            Assert.IsTrue(testResult);
        }

        private class ServiceInfo
        {
            public string Environment { get; set; }
            public string ServiceUrl { get; set; }
            public string ServicePath { get; set; }

            public ServiceInfo(string environment, string serviceUrl, string servicePath)
            {
                Environment = environment;
                ServiceUrl = serviceUrl;
                ServicePath = servicePath;
            }
        }
    }
}
