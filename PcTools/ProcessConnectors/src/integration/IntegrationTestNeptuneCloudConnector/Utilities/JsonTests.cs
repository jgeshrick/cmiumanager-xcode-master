﻿//**************************************************************************
//
//    Neptune Technology Group
//    Copyright 2016 as unpublished work.
//    All rights reserved
//
//    The information contained herein is confidential
//    property of Neptune Technology Group. The use, copying, transfer
//    or disclosure of such information is prohibited except by express
//    written agreement with Neptune Technology Group.
//
//**************************************************************************

using NUnit.Framework;
using NeptuneCloudConnector.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeptuneCloudConnector.JsonModel;


namespace NeptuneCloudConnector.Utilities.Tests
{
    [TestFixture()]
    public class JsonTests
    {
        [Test()]
        public void ToJsonTest()
        {
            var token = new Token();
            token.Authenticated = true;
            token.AuthenticatedAt = new DateTime(2016, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            token.AuthToken = "1234567890";
            token.Expires = new DateTime(2016, 12, 25, 0, 0, 0, DateTimeKind.Utc);

            var json = Utilities.Json.ToJson(token);

            const string expectedJson = "{\"Authenticated\":true,\"AuthToken\":\"1234567890\",\"AuthenticatedAt\":\"2016-01-01T00:00:00+00:00\",\"Expires\":\"2016-12-25T00:00:00+00:00\"}";
            Assert.AreEqual(expectedJson, json);
        }
    }
}