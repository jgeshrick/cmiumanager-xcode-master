/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.tools.packettool;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.util.HexUtils;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class PacketTableModel extends AbstractTableModel
{

    private final List<String> bytes = new ArrayList<>();
    private final List<String> info = new ArrayList<>();
    private final List<String> json = new ArrayList<>();
    private final List<String> debugString = new ArrayList<>();

    private final ObjectMapper jackson = new ObjectMapper();

    public void clear()
    {
        bytes.clear();
        info.clear();
        json.clear();
        debugString.clear();
    }

    @Override
    public int getRowCount()
    {
        return bytes.size();
    }

    @Override
    public int getColumnCount()
    {
        return 4;
    }

    @Override
    public String getColumnName(int columnIndex)
    {
        switch (columnIndex)
        {
            case 0: return "Hex";
            case 1: return "Info";
            case 2: return "JSON";
            case 3: return "Debug";
            default: throw new IllegalArgumentException("column " + columnIndex);
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        switch (columnIndex)
        {
            case 0: return bytes.get(rowIndex);
            case 1: return info.get(rowIndex);
            case 2: return json.get(rowIndex);
            case 3: return debugString.get(rowIndex);
            default: throw new IllegalArgumentException("column " + columnIndex);
        }
    }


    public void addPacketType(TaggedDataPacketType packetType)
    {
        bytes.add(HexUtils.byteArrayToHex(new byte[]{packetType.getId()}));
        info.add(packetType.getShortDescription());
        try
        {
            json.add(jackson.writeValueAsString(packetType));
        }
        catch (JsonProcessingException e)
        {
            json.add(e.toString());
        }
        debugString.add(packetType.toString());
    }

    public void addTaggedData(TaggedData td)
    {
        bytes.add(HexUtils.byteArrayToHex(td.serialize()));
        final TagId tagId = td.getTagId();
        info.add(tagId.toString());
        String tagJson;
        try
        {
            tagJson = td.toJsonPretty();
        }
        catch (Exception e)
        {
            tagJson = "ERROR!\n" + e.toString();
        }
        json.add(tagJson);
        String debugStringValue;
        try
        {
            debugStringValue = td.toString();
        }
        catch (Exception e)
        {
            debugStringValue = "ERROR!\n" + e.toString();
        }
        debugString.add(debugStringValue);
    }

    public void addError(Exception e)
    {
        bytes.add("");
        info.add("Error");
        json.add("");
        debugString.add(e.toString());
    }

}
