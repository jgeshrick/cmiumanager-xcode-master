/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.tools.packettool;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by abh1 on 24/03/2016.
 */
public class ExcelExporter
{
    public void export(File selectedFile, String packetParserVersion, String inputParsed, PacketTableModel hexParseTableContents, PacketTableModel secureParseTableContents) throws IOException
    {
        //Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Input");
        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        cell.setCellValue("Hex input");
        cell = row.createCell(1);
        cell.setCellValue(inputParsed);
        row = sheet.createRow(2);
        cell = row.createCell(0);
        cell.setCellValue("Packet parser version");
        cell = row.createCell(1);
        cell.setCellValue(packetParserVersion);

        makeSheet(workbook, "Parse result", hexParseTableContents);

        if (secureParseTableContents != null && secureParseTableContents.getRowCount() > 0)
        {
            makeSheet(workbook, "Secure block", secureParseTableContents);
        }

        //Write the workbook in file system
        try (final FileOutputStream out = new FileOutputStream(selectedFile))
        {
            workbook.write(out);
        }
    }

    private XSSFSheet makeSheet(XSSFWorkbook workbook, String worksheetName, PacketTableModel contents)
    {
        XSSFSheet sheet = workbook.createSheet(worksheetName);
        Row row = sheet.createRow(0);
        Cell cell;
        for (int col = 0; col < 4; col++)
        {
            cell = row.createCell(col);
            cell.setCellValue(contents.getColumnName(col));
        }

        for (int rowNum = 0; rowNum < contents.getRowCount(); rowNum++)
        {
            row = sheet.createRow(rowNum + 1);
            for (int col = 0; col < 4; col++)
            {
                cell = row.createCell(col);
                cell.setCellValue(contents.getValueAt(rowNum, col).toString());
            }
        }
        return sheet;
    }
}
