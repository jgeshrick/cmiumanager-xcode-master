/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.tools.packettool;

import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.SecureBlockArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.common.packet.parser.ParserUtil;
import com.neptunetg.common.packet.parser.TagSequenceParser;
import com.neptunetg.common.util.HexUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.File;

public class PacketToolMainWindow
{
    private final JFrame window;
    private final GridBagLayout layout;
    private final JComboBox hexParseTagDropdown;
    private final PacketTableModel hexParseTableContents;
    private final JTable hexParseTable;
    private final JComboBox secureParseTagDropdown;
    private final PacketTableModel secureParseTableContents;
    private final JTable secureParseTable;
    private final JTextArea hexInputTextBox;
    private String inputParsed;

    public PacketToolMainWindow()
    {
        window = new JFrame("Packet tool");
        window.setSize(1000, 600);
        window.setLocationRelativeTo(null);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        layout = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        c.weightx = 1;
        c.weighty = 0;

        window.setLayout(layout);

        JLabel label = new JLabel("Packet parser version: " + ParserUtil.getLibraryVersionNumber());
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.NONE;
        window.add(label, c);

        label = new JLabel("Input hex here:");
        c.gridx = 0;
        c.gridy++;
        window.add(label, c);
        hexInputTextBox = new JTextArea(1, 300);
        hexInputTextBox.setWrapStyleWord(false);
        hexInputTextBox.setLineWrap(true);
        JScrollPane scrollPane = new JScrollPane(hexInputTextBox);
        scrollPane.setMinimumSize(new Dimension(500, 50));
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 1;
        window.add(scrollPane, c);
        JButton parseButton = new JButton("Parse as packet");
        parseButton.addActionListener(e ->
                {
                    doParse(true);
                }
        );
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 1;
        c.weighty = 0;
        window.add(parseButton, c);
        parseButton = new JButton("Parse as tag sequence");
        parseButton.addActionListener(e ->
                {
                    doParse(false);
                }
        );
        c.gridx = 1;
        window.add(parseButton, c);

        label = new JLabel("Parse results for input");
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 1;
        window.add(label, c);

        hexParseTagDropdown = new JComboBox<String>();

        c.gridx = 1;
        c.gridwidth = 1;
        window.add(hexParseTagDropdown, c);

        hexParseTableContents = new PacketTableModel();
        hexParseTable = new JTable(hexParseTableContents);
        scrollPane = createScrollableResultTable(hexParseTable);
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 2;
        window.add(scrollPane, c);
        hexParseTagDropdown.addActionListener(e ->
                {
                    final Rectangle rect = hexParseTable.getCellRect(hexParseTagDropdown.getSelectedIndex(), 0, true);
                    hexParseTable.scrollRectToVisible(rect);
                }
        );

        label = new JLabel("Parse results for inner secure data");
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 0;
        window.add(label, c);

        secureParseTagDropdown = new JComboBox<String>();
        c.gridx = 1;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.BOTH;
        window.add(secureParseTagDropdown, c);

        secureParseTableContents = new PacketTableModel();
        secureParseTable = new JTable(secureParseTableContents);
        scrollPane = createScrollableResultTable(secureParseTable);
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 2;
        window.add(scrollPane, c);
        secureParseTagDropdown.addActionListener(e ->
                {
                    final Rectangle rect = secureParseTable.getCellRect(secureParseTagDropdown.getSelectedIndex(), 0, true);
                    secureParseTable.scrollRectToVisible(rect);
                }
        );

        JButton exportButton = new JButton("Export to Excel");
        exportButton.addActionListener(e ->
                {
                    doExport();
                }
        );
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 1;
        c.weighty = 0;
        window.add(exportButton, c);
    }

    private JScrollPane createScrollableResultTable(JTable resultTable)
    {

        final Dimension minTableSize = new Dimension(400, 200);

        resultTable.setDefaultRenderer(String.class, new PacketTableRenderer());
        resultTable.getColumnModel().getColumn(0).setPreferredWidth(60);
        resultTable.getColumnModel().getColumn(1).setPreferredWidth(60);
        resultTable.getColumnModel().getColumn(2).setPreferredWidth(200);
        resultTable.getColumnModel().getColumn(3).setPreferredWidth(200);
        final JScrollPane scrollPane = new JScrollPane(resultTable);
        scrollPane.setMinimumSize(minTableSize);

        final AdjustmentListener slowScrollListener = e -> {
            if (e.getAdjustmentType() == AdjustmentEvent.TRACK){
                e.getAdjustable().setUnitIncrement(15);
            }
        };
        scrollPane.getVerticalScrollBar().addAdjustmentListener(slowScrollListener);

        return scrollPane;
    }

    private void doParse(boolean asPacket)
    {
        inputParsed = hexInputTextBox.getText();
        try
        {
            hexParseTableContents.clear();
            hexParseTagDropdown.removeAllItems();
            secureParseTableContents.clear();
            secureParseTagDropdown.removeAllItems();

            final byte[] data = HexUtils.parseHexToByteArray(inputParsed);
            final Iterable<TaggedData> tagSequence;
            if (asPacket)
            {
                TaggedPacket packet = PacketParser.parseTaggedPacket(data);
                hexParseTableContents.addPacketType(packet.getPacketType());
                hexParseTagDropdown.addItem("Packet type " + packet.getPacketType().toString());
                tagSequence = packet.getTags();
            }
            else
            {
                TagSequenceParser parser = new TagSequenceParser();
                tagSequence = parser.parseTagSequence(data);
            }
            byte[] secureData = null;
            for (TaggedData td : tagSequence)
            {
                hexParseTagDropdown.addItem("Tag " + td.getTagId().toString());
                hexParseTableContents.addTaggedData(td);
                if (td instanceof SecureBlockArrayData) {
                    secureData = ((SecureBlockArrayData) td).getEncryptedDataCopy();
                }
            }
            hexParseTableContents.fireTableDataChanged();
            try
            {
                if (secureData != null)
                {
                    TagSequenceParser parser = new TagSequenceParser();
                    final TagSequence secureTagSequence = parser.parseTagSequence(secureData);
                    for (TaggedData td : secureTagSequence)
                    {
                        secureParseTagDropdown.addItem("Tag " + td.getTagId().toString());
                        secureParseTableContents.addTaggedData(td);
                    }
                }
            }
            catch (Exception e)
            {
                secureParseTableContents.addError(e);
            }
            secureParseTableContents.fireTableDataChanged();

        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(window,
                    "<html><body><p style='width: 400px;'>" + e.toString() + "</p></body></html>", "Error", JOptionPane.ERROR_MESSAGE);
        }
        layout.invalidateLayout(window);
        window.invalidate();
        window.repaint();
    }

    private void doExport()
    {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Save Excel file");
        if (fileChooser.showSaveDialog(window) == JFileChooser.APPROVE_OPTION)
        {

            try
            {
                File selectedFile = fileChooser.getSelectedFile();
                if (!selectedFile.getName().toUpperCase().endsWith(".XLSX"))
                {
                    selectedFile = new File(selectedFile.getAbsolutePath() + ".xlsx");
                }
                ExcelExporter exporter = new ExcelExporter();
                exporter.export(selectedFile, ParserUtil.getLibraryVersionNumber(),
                        inputParsed, hexParseTableContents, secureParseTableContents);
                Desktop.getDesktop().open(selectedFile);
            }
            catch (Exception e)
            {
                JOptionPane.showMessageDialog(window,
                        "<html><body><p style='width: 400px;'>" + e.toString() + "</p></body></html>", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    public void show()
    {
        window.setVisible(true);

    }
}
