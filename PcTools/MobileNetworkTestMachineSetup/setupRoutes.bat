::Batch file for setting up routing on VZW and AT&T test machines
::
::by WJD1

@ECHO OFF

::Set the number of the interface to use.
::You can find this by using "route print"
SET interfaceNum=""

::Uncomment following line to use production environment
::SET prod=True


SET prodIp="10.123.0.0"
SET nonProdIp="10.120.0.0"
SET envIpMask="255.255.0.0"

IF "%prod%"=="True" GOTO SetupProd
GOTO SetupNonProd

:SetupProd
ROUTE ADD %prodIp% MASK %envIpMask% 0.0.0.0 METRIC 1 IF %interfaceNum% /p
GOTO end

:SetupNonProd
ROUTE ADD %nonProdIp% MASK %envIpMask% 0.0.0.0 METRIC 1 IF %interfaceNum% /p
GOTO end

:end
pause
::End of the batch file
