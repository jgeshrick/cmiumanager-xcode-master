
#ifndef BTLE_SERVER_VERSION_H
#define BTLE_SERVER_VERSION_H

#include "CommonTypes.h"
#include "TagTypes.h"

/**
 * @brief  The only accessor; this returns a pointer to the memory that
 *         stores the version data.
 *
 * @retval Version_t The version
 */
const S_IMAGE_VERSION_INFO* BtleServerVersionGet(void);

#endif
