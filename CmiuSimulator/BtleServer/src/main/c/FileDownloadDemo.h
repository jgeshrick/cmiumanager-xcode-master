/******************************************************************************
*******************************************************************************
**
**         Filename: FileDownloadDemo.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**       Sample app for use by C# test harness and as interface for Python
**
**
**
**
** Revision History:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/**
 * @addtogroup FileDownloadDemo
 * @{
 */

/*!***************************************************************************
 * @file FileDownloadDemo.h
 *
 * @brief Sample app for use by C# test harness and as interface for Python
 * @author Duncan Willis
 * @date 2015.03.05
 * @version 1.0
 *****************************************************************************/


#ifndef __FILE_DL_DEMO_H__
#define __FILE_DL_DEMO_H__



/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <stdint.h>
#include "CommonTypes.h"
#include "TransportManager.h"
#include "TcpSessionManager.h"


// Demo for http firmware server
#define HTTP_AWS_SERVER         "52.26.146.127"  
#define HTTP_PORT               80



/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/



/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/

 
DLL_EXPORT int32_t      FileDownloaderBeginSession(const char* imageUrl);
DLL_EXPORT uint32_t     FileDownloaderGetProgress(int dummy);
DLL_EXPORT bool         FileDownloaderIsBusy(int dummy);
DLL_EXPORT int32_t      FileDownloaderCancelSession(int dummy);
DLL_EXPORT int32_t      FileDownloaderCancelOnDisconnect(int dummy);
void                    FileDownloadDemoTick(uint32_t tickIntervalMs);
 

uint32_t FileDownloadHandleHttpResponse(uint8_t* buffer, uint32_t numBytes);

#endif //__FILE_DL_DEMO_H__


/**
 * @}
 */
