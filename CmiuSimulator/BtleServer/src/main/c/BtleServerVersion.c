/**
 * @brief This file is used by TeamCity to auto-generate files containing dynamic
 * version information.
 */
#include "BtleServerVersion.h"

/**
 * @brief The version numbers filled in at build time (created by build server)
 */
static const S_IMAGE_VERSION_INFO versionBtleServer =
{
    0x1,
    0x2,
    0x0,
    0x0
};

/**
 * @brief  Returns the version (created by build server)
 * @return A const pointer to the version
 */
const S_IMAGE_VERSION_INFO* BtleServerVersionGet(void)
{
    return(&versionBtleServer);
}
