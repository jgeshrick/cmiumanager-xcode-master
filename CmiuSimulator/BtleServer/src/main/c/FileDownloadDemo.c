/******************************************************************************
*******************************************************************************
**
**         Filename: FileDownloadDemo.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: Demonstration for file download over BTLE
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
 

 
/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "TransportManager.h"
#include "TcpSessionManager.h"
#include "BtleCommsInterface.h"
#include "DebugTrace.h"
#include "FileDownloader.h"
#include "FileDownloadDemo.h"

 
 // Suppress warnings pertaining to Keil only.
#pragma push
#pragma diag_suppress 167 // Keil args of function pointer do not match
#pragma diag_suppress 513 // Keil args of function pointer do not match - cannot be assigned
 
 
/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/


/**
** @brief The object that manages the overall session, state machine example
**/
static TCP_SESSION_MANAGER                  sm;


/**
** @brief The object that manages the interface 
**/
static TRANSPORT_MANAGER                    tmBtle;


/**
** @brief The driver to the BTLE interface
**/
static BTLE_COMMS                           btleComms; 


/**
** The target address etc to connect to
**/
CONNECTION_PARAMS                           connectParams;


/**
** Object to get files for f/w upgrades
**/
static FILE_DOWNLOADER                      fileDownloader;


static void OnFileTansferComplete(TCP_SESSION_MANAGER* pSm, void* pM);


/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/




/******************************************************************************
**    
**           @param[in] pDl - The downloader context
**           @param[in] b A byte to write from the file
**           @param[in] byteIndex the 0-based index of the byte 
**          
**              @return None
**  @brief
**         Description: Handle file data here (eg write to flash)
******************************************************************************/
void HandleReceivedFileByte(struct FILE_DOWNLOADER* pDl,
    uint8_t b, 
    uint32_t byteIndex)
{
    // Print a dot as we rx every 100 bytes
   if((byteIndex % 100) == 0) DebugTrace(".%u.", byteIndex);
}


/******************************************************************************
**    
**           @param[in] dummy - Needed for DLL (not used)
**          
**              @return 0
**  @brief
**         Description: Start a session, to get a file via series of HTTP Gets
******************************************************************************/
DLL_EXPORT int32_t FileDownloaderBeginSession(const char* imageUrl)
{
    // Buffer for HTTP line parsing
    static uint8_t  mybuf[HTTP_MAX_LINE_LEN];

    FILE_DOWNLOADER_INIT_PARAMS fdInitParams;


    // Init a Transport interface and associate a Transport Manager with it.
    // For BTLE no TCP params are needed.
    TransportManagerInitConnectionParams(&connectParams, "", 0);

    BtleCommsInit(&btleComms, NULL);
    TransportManagerInit(&tmBtle, btleComms.pInterface, (void*)&btleComms);

    // Init to run the session, using the given connection and protocol
    TcpSessionManagerInit(&sm, 
        &tmBtle,
        &connectParams);

    fdInitParams.buffer             = mybuf;
    fdInitParams.bufferSize         = sizeof(mybuf);
    fdInitParams.fileName           = imageUrl; //"c:\\work\\testImage.hex"; //"c:\\work\\smallTestImage.hex";
    fdInitParams.blockSize          = 64; //256; // warning - limited size due to RAM shortage (rx fifo size)
    fdInitParams.maxRequestRetries  = 3;
    fdInitParams.requestTimeoutMs   = 10000;
    fdInitParams.OnReceivedByte     = &HandleReceivedFileByte;
    
    FileDownloaderInit(&fileDownloader, &fdInitParams);

    TcpSessionManagerBegin(&sm,
        (void*)&fileDownloader,
        &FileDownloaderBegin,
        &FileDownloaderRun,
        OnFileTansferComplete); 

    return 0;
}

extern void OnEndFileDownload(const FILE_DOWNLOADER* pDownloader);

/******************************************************************************
**    
**           @param[in] pSm The TCP Session 
**           @param[in] pManager The FILE_DOWNLOADER manager 
**              @return None
**  @brief
**         Description: Called at completion of file download.
**
******************************************************************************/
static void OnFileTansferComplete(TCP_SESSION_MANAGER* pSm, void* pManager)
{
    FILE_DOWNLOADER* pFileDownloader = (FILE_DOWNLOADER*)pManager;
    
    DebugTrace("\nFile xfr done callback\n");
    
    FileDownloaderDebugPrintResult(pFileDownloader);  

    OnEndFileDownload(pFileDownloader);
}


/******************************************************************************
**    
**           @param[in] dummy Not used
**              @return Percent of transfer complete
**  @brief
**         Description: Gets the progress info
**
******************************************************************************/
DLL_EXPORT uint32_t FileDownloaderGetProgress(int dummy)
{
    return FileDownloaderGetProgressPercent(&fileDownloader, NULL, NULL);
}

/******************************************************************************
**    
**           @param[in] dummy Not used
**              @return Percent of transfer complete
**  @brief
**         Description: Gets the progress info
**
******************************************************************************/
DLL_EXPORT bool FileDownloaderIsBusy(int dummy)
{
    return TcpSessionManagerIsBusy(&sm);
}


/******************************************************************************
**    
**           @param[in] dummy - Needed for DLL (not used)
**          
**              @return 0
**  @brief
**         Description: Cancel a session.
******************************************************************************/
DLL_EXPORT int32_t   FileDownloaderCancelSession(int dummy)
{
    FileDownloaderCancel(&fileDownloader, FILE_DOWNLOADER_USER_CANCELLED);
    return 0;
}


/******************************************************************************
**    
**           @param[in] dummy - Needed for DLL (not used)
**          
**              @return 0
**  @brief
**         Description: Cancel a session on disconnect event.
******************************************************************************/
DLL_EXPORT int32_t   FileDownloaderCancelOnDisconnect(int dummy)
{
    FileDownloaderCancel(&fileDownloader, FILE_DOWNLOADER_DISCONNECTED);
    return 0;
}


/******************************************************************************
**    
**           @param[in] tickIntervalMs Time between ticks in ms
**              @return None
**  @brief
**         Description: Main Tick for MQTT -  the overall Session (i.e our demo)
**
******************************************************************************/
void FileDownloadDemoTick(uint32_t tickIntervalMs)
{
    TcpSessionManagerTick(&sm, tickIntervalMs);
}


/******************************************************************************
**    
**           @param[in] buffer Data from BTLE towards client
**              @return None
**  @brief
**        Helper to put received BTLE data (HTTP Responses from UARC) into HTTP Client's comms
**      Dirty, in that it access ringbuffer directly and not via a function
** - but this is a demo.
******************************************************************************/
uint32_t FileDownloadHandleHttpResponse(uint8_t* buffer, uint32_t numBytes)
{
    uint32_t free = 0;
    
    if (numBytes > 0)
    {
        
        free = RingBufferFree(btleComms.pRxRingBuffer);
        if (free >= numBytes)
            {        
                RingBufferWrite(btleComms.pRxRingBuffer, buffer, numBytes);
            }
            else
            {
                DebugTrace("ERROR too much data from server, Free=%u, required=%u\n", free, numBytes);
            }
    }
    
    return numBytes;
}

#pragma pop

/**
 * @}
 */
