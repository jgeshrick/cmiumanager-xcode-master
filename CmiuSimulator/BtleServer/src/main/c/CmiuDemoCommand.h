
/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

 
/**
 * @addtogroup CmuiDemo 
 * @{
 */


/**
 * @file   .c
 *
 * @brief   example interface between BTLE Nordic app, and UartController
 * @author Duncan Willis
 * @date 2015.04.23
 * @version 1.0
 */



#ifndef CMUI_C_H_H_
#define CMUI_C_H_H_

#include <stdint.h>

void CmiuMessageHandler(const struct UARC_MSG* pRxMsg);


#endif 
