/******************************************************************************
*******************************************************************************
**
**         Filename: BtleCommsInterface.h
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/**
 * @addtogroup ModemStub
 * @{
 */

/*!***************************************************************************
 * @file ModemStub.h
 *
 * @brief  Handles the connect on a BTLE interface
 * @author Duncan Willis
 * @date 2015.03.19
 * @version 1.0
 *****************************************************************************/


#ifndef _BLE_COMMS_H_
#define _BLE_COMMS_H_


/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include "CommonTypes.h"
#include "RingBuffer.h"
#include "TransportInterface.h"


/*===========================================================================*/
/*  D E F I N I T I O N S                                                    */
/*===========================================================================*/


/**
** Internal operating state
**/
typedef enum E_MODEM_STATE
{
    MS_RESET,
    MS_CONNECT_BEGIN,
    MS_CONNECT_WAIT,    
    MS_DISCONNECT_BEGIN,
    MS_DISCONNECT_WAIT,
}
E_MODEM_STATE;

/**
** The driver object
**/
typedef struct BTLE_COMMS
{
    // This contains the function set for the device
    const TRANSPORT_INTERFACE* pInterface;

    // Below are implementational details, add / subtract as necessary
    E_MODEM_STATE           state;
    E_CONNECTION_COMMAND    cmd;
    void*                   pCmdParams; 
    int                     sock;


    RingBufferT* pRxRingBuffer;   // Buffer for data FROM cloud
    RingBufferT* pTxRingBuffer;   // Buffer for data TO cloud

} BTLE_COMMS;


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/

 
/**
** Public functions - common interface
**/
int32_t BtleCommsInit(void* pModem, const void* initParams);

#endif
