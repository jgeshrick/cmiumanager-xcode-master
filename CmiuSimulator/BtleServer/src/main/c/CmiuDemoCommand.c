/*****************************************************************************
******************************************************************************
**
**         Filename: CMIUDemo.c
**    
**          Author: Duncan Willis
**          Created: 27 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file  .c
*
* @brief Code t
*  
* 
******************************************************************************/

#include <stdint.h>
#include <string.h>
#include "UartController.h"
#include "RingBuffer.h"
#include "FileDownloadDemo.h"
#include "BtleServerVersion.h"
#include "Tpbp.h"
#include "CmiuDemo.h"
 
 
 extern   UART_CONTROLLER  uc;

// Instantiate a Parser object
static S_TPBP_PARSER parser;
 
 
 
 // Note - 512 won't fit
#define TAGGED_PACKET_BUFFER_SIZE 300

/* Buffer for assembling packed tags within secure data block */
//static uint8_t secureDataBuffer[TAGGED_PACKET_BUFFER_SIZE];


/* Buffer for decryption and packing  */
 static uint8_t workingBuffer[TAGGED_PACKET_BUFFER_SIZE];

 
// Stores the received setting to fake a non-volatile setting (is only non-vol while power is applied!)
static S_INTERVAL_RECORDING_CONFIG    intervalRecordingConfigStored;             // 34
static S_RECORDING_REPORTING_INTERVAL recordingReportingIntervalStored;          // 48



// Buffer for image URL
char textBuffer[TPBP_URL_BUFFER_LENGTH];
static bool HandleCommandPacket(S_TPBP_PARSER* const outerPacketParser);
static bool CrackCommandPacket(E_COMMAND commandId, S_TPBP_PARSER* const secureTagSequenceParser);
static bool HandleCommandImageUpdate(S_TPBP_PARSER* secureTagSequenceParser);

static bool  HandleCommandGetSignalQualityPacket(S_TPBP_PARSER* pParser);
    
static bool HandleCommandPublishDetailedConfigPacket(S_TPBP_PARSER* pParser);
static bool HandleCommandSetRecordingReportingInterval(S_TPBP_PARSER* pParser);

static void PopulatePacketHeader(S_CMIU_PACKET_HEADER* pPacketHeader);
static bool SendGeneralResponse(const uint8_t commandByte, uint16_t errorCode);
static bool SendDetailedConfigPacket(void);
static void fakeEncrypt(const uint8_t* const dataIn, uint8_t* const encryptedDataOut, const uint32_t byteCount);
static void fakeDecrypt(const uint8_t* const encryptedDataIn, uint8_t* const dataOut, const uint32_t byteCount);


// Try block. If f fails, then the block exits with the ok variable set to false.
#define BEGIN_TRY(ok)   (ok)=false; while(true){
#define END_TRY(ok)     (ok)=true; break;}      // while (true)
#define TRY(f)   if     ((f) == false) break;

extern uint32_t txCompleteEventCount ; // debug



 /*****************************************************************************
 * @brief
 *    
 *
 * @param [in] pRxMsg 
 *  The received  message from UARC, Uart Controller
 *
 * @return None
 *
// *****************************************************************************/
void CmiuMessageHandler(const struct UARC_MSG* pRxMsg)
{    
    E_PACKET_TYPE_ID packetType = E_PACKET_TYPE_COUNT; // Default to n/a
 
    // Init a parser to the received data
    Tpbp_ParserInit(&parser, pRxMsg->pPayload, pRxMsg->length);

    // Read the packet type ID
    packetType = Tpbp_ParserRead_PacketTypeId(&parser);
    
    DebugTrace("\nParsing packet: Packet type %u\n", (uint32_t)packetType);
    //DebugTrace("\nEv count =%u\n",txCompleteEventCount);

    switch(packetType)
    {
        case E_PACKET_TYPE_COMMAND:
            {
                HandleCommandPacket(&parser);
            }
            break;
        
         /** A detailed config packet */
        case E_PACKET_TYPE_DETAILED_CONFIG:
        /** Regular meter reading data packet */
        case  E_PACKET_TYPE_INTERVAL_DATA:
        /** A basic config packet */
        case  E_PACKET_TYPE_BASIC_CONFIG:
        /** This packet indicates an event has occured */
        case   E_PACKET_TYPE_EVENT:
        /** This packet is used to send the time */
        case  E_PACKET_TYPE_TIME: 
        case E_PACKET_TYPE_RESPONSE:
            break;
        
        default:
            break;
    }
}


 /*****************************************************************************
 * @brief Handle incoming Packet of type E_PACKET_TYPE_COMMAND
 *    
 *
 * @param [in] outerPacketParser The Parser inited to start of the command Packet 
 *   
 *
 * @return true if handled OK.
 *
// *****************************************************************************/
static bool HandleCommandPacket(S_TPBP_PARSER* const outerPacketParser)
{    
    bool                isOk                    = true;
    S_TPBP_TAG          tag;
    S_MQTT_BLE_UART     packetHeader;
    uint32_t            secureDataSizeInBytes   = 0;
    S_TPBP_PARSER       secureTagSequenceParser;
    E_COMMAND           commandId               = E_COMMAND_COUNT;
 
    DebugTrace("--- HandleCommandPacket---\n");
    
    BEGIN_TRY(isOk)
     
        TRY(Tpbp_ParserReadTag(outerPacketParser, &tag));
        TRY(tag.tagNumber == E_TAG_NUMBER_UART_PACKET_HEADER) ;
        TRY(Tpbp_ParserRead_UartPacketHeader(outerPacketParser, &packetHeader));
        TRY(Tpbp_ParserReadTag(outerPacketParser, &tag));
        TRY(tag.tagNumber == E_TAG_NUMBER_SECURE_DATA) 
        
        // Get the secure bytes into a separate buffer for decryption
        TRY(Tpbp_ParserGetBytes(outerPacketParser, workingBuffer, tag.dataSize));
 
        secureDataSizeInBytes = tag.dataSize;             
        DebugTrace("secureDataSizeInBytes  = %u\n", secureDataSizeInBytes);
     
        // In-place "Decrypt" secure data (do nothing)
        fakeDecrypt(workingBuffer, workingBuffer, secureDataSizeInBytes);
        
        // Init a parser to the decrypted data
        Tpbp_ParserInit(&secureTagSequenceParser, workingBuffer, secureDataSizeInBytes);

        TRY(Tpbp_ParserReadTag(&secureTagSequenceParser, &tag));
        TRY(tag.tagNumber == E_TAG_NUMBER_COMMAND);

        // Get command number and call handler
        commandId = (E_COMMAND)Tpbp_Unpack(&secureTagSequenceParser, tag.dataSize);

        DebugTrace("Command Packet; CmdId = %u\n", commandId);
        TRY(CrackCommandPacket(commandId, &secureTagSequenceParser));
       
    END_TRY(isOk)
    
    DebugTrace("Command packet processed successfully? %s\n", isOk ? "Yes" : "No");

    return isOk;
}
 

 /*****************************************************************************
 * @brief Helper to call relevant command handler (for type E_PACKET_TYPE_COMMAND)
 *    
 *
 * @param [in] commandId  The command Type ID
 * @param [in] secureTagSequenceParser  The parser initialised to the tags of the command
 *  
 *
 * @return true if handler was called and was OK, else false
 *
// *****************************************************************************/
static bool CrackCommandPacket(E_COMMAND commandId, 
    S_TPBP_PARSER* const secureTagSequenceParser)
{
    bool    isOk = true;
   
    switch (commandId)
    {
    /* 0 Firmware update */
    case E_COMMAND_UPDATE_IMAGE:
            isOk &= HandleCommandImageUpdate(secureTagSequenceParser);
            break;
          
   /** 1	Reboot CMIU */
   case E_COMMAND_REBOOT:
            
        break;
        
    /** 2	Mag swipe Emulation */
    case E_COMMAND_MAGSWIPE:
            
        break;
    
   case  E_COMMAND_SLEEP:
            
        break;
   
    /** 4	Enter normal operating mode from sleep state */
    case E_COMMAND_WAKE:
            
        break;
    /** 5	Enter operation mode */
    case E_COMMAND_ENTER_OPERATION:
            
        break;
    /** 6	Erase DataLog */
    case E_COMMAND_ERASE_MEMORY_IMAGE:
            
        break;
    
    /** 7	Set MQTT Broker Address */
    case E_COMMAND_SET_BROKER_ADDRESS:
            
        break;
    
    /** 8	Set Fallback MQTT Broker Address */
    case E_COMMAND_SET_FALLBACK_BROKER_ADDRESS:
            
        break;
    
    /** 9	Set Current Time & Date */
    case E_COMMAND_SET_CLOCK:
            
        break;
    
    /** 10	Set the Recording and Reporting Intervals */
    case E_COMMAND_SET_RECORDING_AND_REPORTING_INTERVALS:
            isOk &= HandleCommandSetRecordingReportingInterval(secureTagSequenceParser);
        break;
    
    /** 11	Publish Detailed Config Packet */
    case E_COMMAND_PUBLISH_REQUESTED_PACKET:
        isOk &= HandleCommandPublishDetailedConfigPacket(secureTagSequenceParser);      
        break;
    
    /** 12	Publish Basic Config Packet */
    case E_COMMAND_PUBLISH_BASIC_CONFIG_PACKET:
            
        break;
    
    /** 13	Publish Interval Data Packet */
    case  E_COMMAND_PUBLISH_INTERVAL_PACKET:
            
        break;
   
    /** 31	Request Current CMIU LTE Signal Quality */
    case  E_COMMAND_GET_CMIU_SIGNAL_QUALITY:
        isOk &= HandleCommandGetSignalQualityPacket(secureTagSequenceParser);
        break;
    
    
    default:
            isOk = false;
            break;
        }
        
    return isOk;
}
    

 /*****************************************************************************
 * @brief command handler (for coomand type E_COMMAND_UPDATE_IMAGE)
 *    
 *
 * @param [in] pParser  The parser initialised to the tags of the command
 *  
 *
 * @return true if handler was OK, else false
 *
// *****************************************************************************/
static bool HandleCommandImageUpdate(S_TPBP_PARSER* secureTagSequenceParser)
{
    bool                    isOk                    = true;
    S_TPBP_TAG              tag;
    S_IMAGE_METADATA        imageMetadata;
    S_IMAGE_VERSION_INFO    imageVersionInfo;
    uint64_t                applyAtTime;
    char*                   imageUrl;
         
    DebugTrace("HandleCommandImageUpdate:\n");
    
    isOk &= Tpbp_ParserReadTag(secureTagSequenceParser, &tag);
    if (tag.tagNumber != E_TAG_NUMBER_TIMESTAMP) 
    {
        isOk = false;
    }
     
    if (isOk == true)
    {
        applyAtTime = Tpbp_Unpack64(secureTagSequenceParser);
        isOk &= Tpbp_ParserReadTag(secureTagSequenceParser, &tag);
        if (tag.tagNumber != E_TAG_NUMBER_IMAGE) 
        {
            isOk = false;
        }
    }
  
    if (isOk == true)
    {
        isOk &= Tpbp_ParserRead_String(secureTagSequenceParser, &tag, textBuffer, TPBP_URL_BUFFER_LENGTH);
        imageUrl = textBuffer;
    }
      
    if (isOk == true)
    {
        isOk &= Tpbp_ParserReadTag(secureTagSequenceParser, &tag);
        if (tag.tagNumber != E_TAG_NUMBER_IMAGE_METADATA) 
        {
            isOk = false;
        }
    }
    
    if (isOk == true)
    {
        isOk &= Tpbp_ParserRead_ImageMetadata(secureTagSequenceParser, &imageMetadata);
    }
    if (isOk == true)
    {
        isOk &= Tpbp_ParserReadTag(secureTagSequenceParser, &tag);
        if (tag.tagNumber != E_TAG_NUMBER_FIRMWARE_REVISION) 
        {
            isOk = false;
        }
    }
    
    if (isOk == true)
    {
        isOk &= Tpbp_ParserRead_ImageVersionInfo(secureTagSequenceParser, &imageVersionInfo);
    }
    //the rest is just 0s so ignore

    DebugTrace("Image update packet parsed successfully? %s\n", isOk ? "Yes" : "No");

    if (isOk == true)
    {
        DebugTrace("Image update to be applied at UNIX time: %lld,\nDownload from URL %s, image type %d, start address 0x%08X, length %d, version %x.%x.%x.%x\n", 
           applyAtTime, imageUrl, imageMetadata.imageType, imageMetadata.startAddress, imageMetadata.imageSize,
           imageVersionInfo.versionMajorBcd, imageVersionInfo.versionMinorBcd, imageVersionInfo.versionYearMonthDayBcd, imageVersionInfo.versionBuildBcd);
        
           DebugTrace("\r\nBegin file request...\r\n");
    
           FileDownloaderBeginSession(imageUrl);
        
    }
    
    SendGeneralResponse(E_COMMAND_UPDATE_IMAGE, E_ERROR_NONE);
    
    return isOk;
}


 /*****************************************************************************
 * @brief command handler (for command type E_COMMAND_PUBLISH_DETAILED_CONFIG_PACKET)
 *    
 *
 * @param [in] pParser  The parser initialised to the tags of the command
 *  
 *
 * @return true if handler was OK, else false
 *
// *****************************************************************************/
static bool HandleCommandPublishDetailedConfigPacket(S_TPBP_PARSER* pParser)
{
    bool isOk = true;
    
    DebugTrace("HandleCommandPublishDetailedConfigPacket: \n");

    // Respond to the request
		/* Remove??- as we do not think it is applicable?   */
	 // removed 11 Aug. Note old iPad app will fail with this removed.	SendGeneralResponse(E_COMMAND_PUBLISH_DETAILED_CONFIG_PACKET, E_ERROR_NONE);

    // Send the data
    SendDetailedConfigPacket();
	
    return isOk;
}

/*****************************************************************************
 * @brief command handler (for command type E_COMMAND_GET_CMIU_SIGNAL_QUALITY)
 *    
 *
 * @param [in] pParser  The parser initialised to the tags of the command
 *  
 *
 * @return true if handler was OK, else false
 *
// *****************************************************************************/
static bool  HandleCommandGetSignalQualityPacket(S_TPBP_PARSER* pParser)
{
    bool isOk = true;

    DebugTrace("Signal Quality Command received:\n");
    
	//TODO: Change this to specific function to send required tag as well.
    SendGeneralResponse(E_COMMAND_GET_CMIU_SIGNAL_QUALITY, E_ERROR_NONE);
    
    return isOk;
}

 /*****************************************************************************
 * @brief command handler (for command type E_COMMAND_SET_RECORDING_AND_REPORTING_INTERVALS )
 *    
 *
 * @param [in] pParser  The parser initialised to the tags of the command
 *  
 *
 * @return true if handler was OK, else false // 34 or 48. Confusion in document.
 *
 * This function receives command to set RecRepInterval, and "stores" those
 * values for later Get via Detailed Config Pkt. Intended to test iOS  / CMIT
// *****************************************************************************/
static bool HandleCommandSetRecordingReportingInterval(S_TPBP_PARSER* pParser)
{
    bool isOk = false;
    bool isFound;
    S_TPBP_TAG tag; 
    
    
    DebugTrace("HandleCommandSetRecordingReportingInterval: \n");
    
    // S_INTERVAL_RECORDING_CONFIG (34) present?
    isFound = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_INTERVAL_RECORDING, &tag);
    
    if (isFound)
    {
        isOk = Tpbp_ParserRead_IntervalRecordingConfig(pParser, &intervalRecordingConfigStored);    
        
        
        DebugTrace("Tag S_INTERVAL_RECORDING_CONFIG (%u) Received. Parse %s:\n", 
            E_TAG_NUMBER_INTERVAL_RECORDING, isOk ? "OK":"FAIL");
    }
     
    // S_RECORDING_REPORTING_INTERVAL (48) present?
    isFound = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_RECORDING_REPORTING_INTERVAL, &tag);
    
    if (isFound)
    {
        // Read tag 48
        isOk = Tpbp_ParserRead_RecordingReportingInterval(pParser, &recordingReportingIntervalStored);    

        DebugTrace("Tag S_RECORDING_REPORTING_INTERVAL (%u) Received. Parse %s:\n", 
            E_TAG_NUMBER_RECORDING_REPORTING_INTERVAL, isOk ? "OK":"FAIL");
        
        //
        // Transcribe 48 to 34, which will be returned in the Detailed Config request. 
        // No thanks to spec which is contradictory and has bad names
        //

        // Dev num    
        intervalRecordingConfigStored.deviceNumber              = 0;
        
        // Interval format
        intervalRecordingConfigStored.intervalFormat            = 0; // No idea what this means. Undocumented.
        
        // Recording Interval ??? 
        intervalRecordingConfigStored.recordingIntervalMin      = recordingReportingIntervalStored.recordingIntervalMins;   // No idea why it's called logInterval. Both claim to be in minutes.
        
        // Reporting Interval ??
        intervalRecordingConfigStored.reportingIntervalHrs      = recordingReportingIntervalStored.reportingIntervalHours;  
       
        // Recording start time ??
        intervalRecordingConfigStored.intervalStartTime         = recordingReportingIntervalStored.recordingStartMins;   // I think this is ok... both U16 min after midnight
       
        // Most recent reading
        intervalRecordingConfigStored.intervalLastRecordDateTime  = 0;
    }
    
    
    
    DebugTrace("Dump of intervalRecordingConfigStored:-\n");
        DebugTrace("recordingIntervalMin %u:\n", intervalRecordingConfigStored.recordingIntervalMin);
        DebugTrace("reportingIntervalHrs %u:\n", intervalRecordingConfigStored.reportingIntervalHrs);
        DebugTrace("intervalStartTime  %u:\n", intervalRecordingConfigStored.intervalStartTime);
    DebugTrace("\n");
    
    ///@todo move this and others into the cracker function.
    SendGeneralResponse(E_COMMAND_SET_RECORDING_AND_REPORTING_INTERVALS, E_ERROR_NONE);
    
    return isOk;
}



 /*****************************************************************************
 * @brief Make a general response and send it
 *    
 *
 * @param [in] pParser  The parser initialised to the tags of the command
 *  @param commandByte The command id we're responding to
 *
 * @return true if handler was OK, else false
 *
// *****************************************************************************/
static bool SendGeneralResponse(const uint8_t commandByte, uint16_t errorCode)
{
    bool            isOk  = true;
    S_TPBP_PACKER   packer;
    uint8_t*        pSecureData;
    uint32_t        numSecureBytes = 0;
    uint32_t        numBytesToSend = 0;
 
    S_CMIU_PACKET_HEADER packetHeader = {0};
     
    PopulatePacketHeader(&packetHeader);
    
    Tpbp_PackerInit(&packer, workingBuffer, sizeof(workingBuffer));
    
        // Break out with isOk set false if any function returns false, else fall through
    BEGIN_TRY(isOk)
        // Begin Packet
        TRY(Tpbp_Packer_BeginPacket(&packer, E_PACKET_TYPE_RESPONSE));    
        TRY(Tpbp_PackerAdd_CmiuPacketHeader(&packer, &packetHeader));  
        
        // Secure region
        TRY(Tpbp_Packer_BeginSecurePack(&packer));
        TRY(Tpbp_PackerAdd_Command(&packer,  commandByte));
        TRY(Tpbp_PackerAdd_ErrorCode(&packer, errorCode    ));
        
        pSecureData = Tpbp_Packer_EndSecurePack(&packer, &numSecureBytes);    
        fakeEncrypt(pSecureData, pSecureData, numSecureBytes); // In-place encryption
        
        // End of packet
        TRY(Tpbp_Packer_EndPacket(&packer));        
    END_TRY(isOk) 
    
    numBytesToSend = Tpbp_PackerGetCount(&packer);
    
    DebugTrace("Sending E_PACKET_TYPE_RESPONSE, eCode = %u; %u bytes\n", errorCode, numBytesToSend);

    UartControllerSend(&uc, UARC_MT_RESP, workingBuffer, numBytesToSend);
 
    return isOk;
}



 /*****************************************************************************
 * @brief Make a Detail Conf and send it
 *    
 *
 * @param  
 *  @param commandByte The command id we're resonding to
 *
 * @return true if handler was OK, else false
 *
// *****************************************************************************/
static bool SendDetailedConfigPacket(void)
{
    bool            isOk  = false;
    S_TPBP_PACKER   packer;
    uint8_t*        pSecureData;
    uint32_t        numSecureBytes = 0;
    uint32_t        numBytesToSend = 0;
    uint32_t        numBytesSent = 0;
 
    S_CMIU_PACKET_HEADER            packetHeader = {0};
    S_CMIU_DIAGNOSTICS              cmiuDiagnostics = {0};          // 23
    S_CMIU_STATUS                   statusFlags = {0};              // 26
    S_REPORTED_DEVICE_CONFIG        reportedDeviceConfig = {0};     // 27
    S_CONNECTION_TIMING_LOG         timingLog = {0};                // 31             
    S_CMIU_BASIC_CONFIGURATION      basicConfig = {0};              // 33
 //   S_INTERVAL_RECORDING_CONFIG     intervalRecordingConfig = intervalRecordingConfigStored;    // 34
    S_CMIU_INFORMATION              cmiuInfo = {0};                 // 44
    S_IMAGE_VERSION_INFO            imageVersionHardware = {0x39, 0x38, 0x31323334, 0x1234};     // 38
    S_IMAGE_VERSION_INFO            imageVersionBootloader = {0x37, 0x38, 0x31323334, 0x1234};   // 40
    S_IMAGE_VERSION_INFO            imageVersionConfig = {0};       // 41
    S_IMAGE_VERSION_INFO            imageVersionArb = {0};          // 42
    S_IMAGE_VERSION_INFO            imageVersionBle = {0};          // 43  
    S_IMAGE_VERSION_INFO            imageVersionEncryption = {0};   // 49
     
    const S_IMAGE_VERSION_INFO* pImageVersionFirmware =  BtleServerVersionGet(); // 39
   
     
    PopulatePacketHeader(&packetHeader);
    
    Tpbp_PackerInit(&packer, workingBuffer, sizeof(workingBuffer));
    
    // Break out with isOk set false if any function returns false, else fall through
    BEGIN_TRY(isOk)
         
        // Begin Packet
        TRY(Tpbp_Packer_BeginPacket                     (&packer, E_PACKET_TYPE_DETAILED_CONFIG));    
        TRY(Tpbp_PackerAdd_CmiuPacketHeader             (&packer, &packetHeader));          // 1
        
        // Secure region
        TRY(Tpbp_Packer_BeginSecurePack(&packer));
        
        TRY(Tpbp_PackerAdd_CmiuStatus                   (&packer, &statusFlags));            // 26
        TRY(Tpbp_PackerAdd_CmiuBasicConfiguration       (&packer, &basicConfig));            // 33
        TRY(Tpbp_PackerAdd_IntervalRecordingConfig      (&packer, &intervalRecordingConfigStored));// 34 
        TRY(Tpbp_PackerAdd_CmiuDiagnostics              (&packer, &cmiuDiagnostics));        // 23 
        TRY(Tpbp_PackerAdd_ReportedDeviceConfig         (&packer, &reportedDeviceConfig));   // 27 
        TRY(Tpbp_PackerAddErrorLog                      (&packer, "This is the error log")); // 30 
        TRY(Tpbp_PackerAdd_ConnectionTimingLog          (&packer, &timingLog));              // 31 
        TRY(Tpbp_PackerAdd_CmiuInformation              (&packer, &cmiuInfo));               // 44 
      
        TRY(Tpbp_PackerAdd_ImageVersionInfoHardware     (&packer, &imageVersionHardware));   // 38
        TRY(Tpbp_PackerAdd_ImageVersionInfoFirmware     (&packer, pImageVersionFirmware));   // 39
        TRY(Tpbp_PackerAdd_ImageVersionInfoBootloader   (&packer, &imageVersionBootloader)); // 40
        TRY(Tpbp_PackerAdd_ImageVersionInfoConfig       (&packer, &imageVersionConfig));     // 41
        TRY(Tpbp_PackerAdd_ImageVersionInfoArb          (&packer, &imageVersionArb));        // 42
        TRY(Tpbp_PackerAdd_ImageVersionInfoBle          (&packer, &imageVersionBle));        // 43
        TRY(Tpbp_PackerAdd_ImageVersionInfoEncryption   (&packer, &imageVersionEncryption)); // 49
  /*      TRY(Tpbp_PackerAdd_NetworkOperators             (&packer, "nw op status"));          // 3
        TRY(Tpbp_PackerAdd_NetworkPerformance           (&packer, "nw perf"));               // 4
        TRY(Tpbp_PackerAdd_RegistrationStatus           (&packer, "reg Status"));            // 5
        TRY(Tpbp_PackerAdd_MqttBrokerAddress            (&packer, "mqttAddressString"));     // 6
        TRY(Tpbp_PackerAdd_FallbackMqttBrokerAddress    (&packer, "fallbackAddressString")); // 7
        TRY(Tpbp_PackerAdd_AssignedCmiuAddress          (&packer, "myCmiuAddressString"));   // 8
        TRY(Tpbp_PackerAdd_ModemHardwareRevision        (&packer, "my revisionString"));     // 15
        TRY(Tpbp_PackerAdd_ModemModelId                 (&packer, "my modemIdString"));      // 16
        TRY(Tpbp_PackerAdd_ModemManufacturerId          (&packer, "my modemManuString"));    // 17
        TRY(Tpbp_PackerAdd_ModemSoftwareRevision        (&packer, "my modemSwRevString"));   // 18
        TRY(Tpbp_PackerAdd_ModemSerialNumber            (&packer, "my modemSerialString"));  // 19
        TRY(Tpbp_PackerAdd_MSISDNRequest                (&packer, "MSISDN Stuff"));          // 37
        
        TRY(Tpbp_PackerAdd_ModemSimImsiNumber           (&packer, "my modemImsiString"));    // 20
        TRY(Tpbp_PackerAdd_ModemSimId                   (&packer, "my simIdString"));        // 21
        TRY(Tpbp_PackerAddPinPukPuk2                    (&packer, "my pinPukString"));       // 22
        TRY(Tpbp_PackerAddBleLastUser                   (&packer, "my lastUserString"));     // 24
        TRY(Tpbp_PackerAdd_LastLoggedInDate             (&packer, 0xABABCDCD11223344ull));   // 25
        */
        // End of secure block  
        pSecureData = Tpbp_Packer_EndSecurePack(&packer, &numSecureBytes);    
        fakeEncrypt(pSecureData, pSecureData, numSecureBytes); // In-place encryption
        
        // End of packet
        TRY(Tpbp_Packer_EndPacket(&packer));
        
    END_TRY(isOk)
    
    isOk = true;
    if (true == isOk)
    {    
        numBytesToSend = Tpbp_PackerGetCount(&packer);

        DebugTrace("Sending E_PACKET_TYPE_DETAILED_CONFIG ; Packet=%u bytes\n", numBytesToSend);

        numBytesSent = UartControllerSend(&uc, UARC_MT_RESP, workingBuffer, numBytesToSend);
        
        if (0 == numBytesSent)
        {
                DebugTrace("Send FAILED - TX FIFO too small\n"); 
        }
    }
    else
    {
        DebugTrace("FAILED to pack E_PACKET_TYPE_DETAILED_CONFIG\n");
    }
    
    return isOk;
}



/*
    Demo to populate the packetHeader Tag  with dummy data and fake time
*/
static void PopulatePacketHeader(S_CMIU_PACKET_HEADER* pPacketHeader)
{
    static uint8_t toggle = 0;
    
    pPacketHeader->cmiuId               = 0x1234ABCD;
    if (toggle)
    {
        pPacketHeader->cellularRssiAndBer   = 0x1299;
    }
    else
    {
        pPacketHeader->cellularRssiAndBer   = 0x1704;
    }
    pPacketHeader->keyInfo              = 1;
    pPacketHeader->timeAndDate          = GetUptimeMs() / 1000ull; // fake time
    
    toggle ^= 1;
}

/*
Placeholder for AES256 encryption method
*/
static void fakeEncrypt(const uint8_t* const dataIn, uint8_t* const encryptedDataOut, const uint32_t byteCount)
{
    uint32_t i;
    for (i = 0u; i < byteCount; i++)
    {
   //     encryptedDataOut[i] = dataIn[i] ^ 0x55; ///do nothing!
    }
}

/*
Placeholder for AES256 decryption method
*/
static void fakeDecrypt(const uint8_t* const encryptedDataIn, uint8_t* const dataOut, const uint32_t byteCount)
{
    uint32_t i;
    for (i = 0u; i < byteCount; i++)
    {
      //  dataOut[i] = encryptedDataIn[i] ^ 0x55;  ///do nothing!
    }
}




