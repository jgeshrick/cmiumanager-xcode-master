
/** @file
 *
 * @brief    UART over BLE application main file CMIU -- Note Virtual comm port is 38400 baud CTS/RTS 
 *
 * This file contains the source code for a sample application that uses the Nordic UART service.
 * This application uses the @ref srvlib_conn_params module.
 */

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "nrf51_bitfields.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_conn_params.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "app_gpiote.h"
#include "app_button.h"
#include "ble_nus.h"
#include "simple_uart.h"
#include "app_util_platform.h"
#include "bsp.h"
#include "CmiuDemo.h"
#include "app_trace.h"
#include "DebugTrace.h"
#include "RingBuffer.h"
#include "BtleServerVersion.h"
#include "FileDownloadDemo.h"

#define BONDING_API
#define ENABLE_DEBUG_LOG_SUPPORT 1

// These enable use of bonding 
#ifdef BONDING_API
#include "device_manager.h"
#include "device_manager_cnfg.h"
#include "pstorage.h"
#include "pstorage_platform.h"
#endif



//
// define this to disable sleep - note timeout max seems to be 180s (?)
//
#define DISABLE_SLEEP
 
 
#define IS_SRVC_CHANGED_CHARACT_PRESENT 0                                           /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/


#define WAKEUP_BUTTON_ID                0                                           /**< Button 1 used to wake up the application. */


#ifdef DEFAULT_DEVICE_NAME
    #define DEVICE_NAME                     "AB123456"                           /**< Name of device. Will be included in the advertising data. */
#elif defined MID_ALPHA_TRIAL_BRADDLEY
	#define DEVICE_NAME                     "Braddley's CMIU Simulator"
#elif defined MID_ALPHA_TRIAL_BRANDON
	#define DEVICE_NAME                     "Brandon's CMIU Simulator"
#elif defined MID_ALPHA_TRIAL_CHARLES
	#define DEVICE_NAME                     "Charles's CMIU Simulator"
#elif defined MID_ALPHA_TRIAL_ERIC
	#define DEVICE_NAME                     "Eric's CMIU Simulator"
#elif defined MID_ALPHA_TRIAL_SPARE
	#define DEVICE_NAME                     "Spare CMIU Simulator"
#elif defined MID_ALPHA_TRIAL_TIM
	#define DEVICE_NAME                     "Tim's CMIU Simulator"
#else
    #error "No device name specified!"
#endif


#define APP_ADV_INTERVAL                64                                          /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      (180)                                       /**< The advertising timeout (in units of seconds). */

#define APP_TIMER_PRESCALER             0                                           /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_MAX_TIMERS            (1 + 2 + BSP_APP_TIMERS_NUMBER)             /**< Maximum number of simultaneously created timers. */
#define APP_TIMER_OP_QUEUE_SIZE         4                                           /**< Size of timer operation queues. */

#define APP_GPIOTE_MAX_USERS            1                                           /**< Maximum number of simultaneously gpiote users. */

#define MIN_CONN_INTERVAL                9 //16                                          /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               60                                          /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                           /**< slave latency. */
#define CONN_SUP_TIMEOUT                400                                         /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER)  /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER) /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define BUTTON_DETECTION_DELAY          APP_TIMER_TICKS(50, APP_TIMER_PRESCALER)    /**< Delay from a GPIOTE event until a button is reported as pushed (in number of timer ticks). */

#define SEC_PARAM_TIMEOUT               30                                          /**< Timeout for Pairing Request or Security Request (in seconds). */
#define SEC_PARAM_BOND                  1                                           /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                           /**< Man In The Middle protection not required. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                        /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                           /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                           /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                          /**< Maximum encryption key size. */


#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

static ble_gap_sec_params_t             m_sec_params;                               /**< Security requirements for this application. */
static uint16_t                         m_conn_handle = BLE_CONN_HANDLE_INVALID;    /**< Handle of the current connection. */
       ble_nus_t                        m_nus;                                      /**< Structure to identify the Nordic UART Service. */



bool        ble_attempt_to_send(uint8_t * data, uint8_t length);
extern      RingBufferT  txRingBuffer;   // Buffer for data TO BTLE
uint32_t    txCompleteEventCount = 0;
static bool ble_buffer_available = true;
static bool tx_complete = false;
static void TxLoop(void);

extern      ble_uuid128_t uuidCopy;
void        FormatHexes(char buf[], uint32_t bufSize, uint8_t data[], uint32_t dataCount);
static void PrintStartupInfo(void);
static void device_manager_init(bool erase_bonds);

    
#ifndef SMALL_TRACE_BUFFER
    #error "Must define SMALL_TRACE_BUFFER if you want this to fit..."
#endif

/**@brief       Assert macro callback function.
 *
 * @details     This function will be called in case of an assert in the SoftDevice.
 *
 * @warning     This handler is an example only and does not fit a final product. You need to
 *              analyze how your product is supposed to react in case of Assert.
 * @warning     On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in]   line_num   Line number of the failing ASSERT call.
 * @param[in]   file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


/**@brief   Function for the GAP initialization.
 *
 * @details This function will setup all the necessary GAP (Generic Access Profile)
 *          parameters of the device. It also sets the permissions and appearance.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
    
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *) DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief   Function for the Advertising functionality initialization.
 *
 * @details Encodes the required advertising data and passes it to the stack.
 *          Also builds a structure to be passed to the stack when starting advertising.
 */
static void advertising_init(void)
{
    uint32_t      err_code;
    ble_advdata_t advdata;
    ble_advdata_t scanrsp;
    uint8_t       flags = BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;
    
    ble_uuid_t adv_uuids[] = {{BLE_UUID_NUS_SERVICE, m_nus.uuid_type}};

    memset(&advdata, 0, sizeof(advdata));
    advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    advdata.include_appearance      = false;
    advdata.flags.size              = sizeof(flags);
    advdata.flags.p_data            = &flags;

    memset(&scanrsp, 0, sizeof(scanrsp));
    scanrsp.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    scanrsp.uuids_complete.p_uuids  = adv_uuids;
    
    err_code = ble_advdata_set(&advdata, &scanrsp);
    APP_ERROR_CHECK(err_code);
}


/**@brief    Function for handling the data from the Nordic UART Service.
 *
 * @details  This function will process the data received from the Nordic UART BLE Service and send
 *           it to the UART module.
 */
/**@snippet [Handling the data received over BLE] */
void nus_data_handler(ble_nus_t * p_nus, uint8_t * p_data, uint16_t dataCount)
{    
    uint8_t b;
    
    #if 0                   // Optional longer debug - warning - takes too long.
    char buf[120];
    char bufNum[120];
    bool bShowRaw = true;   // aid for debugging if needed
    #endif
    
	uint32_t timeMs=(uint32_t)GetUptimeMs();	
	
    for (int i = 0; i < dataCount; i++)
    {
        b = p_data[i];        
        CmiuDemoOnRxByte(b);            // CMUI Demo - add to rx ringbuf
    }
    
	#if 0
    FormatHexes(bufNum, sizeof(bufNum), p_data,   dataCount);
    snprintf(buf, sizeof(buf), "%04u:RX %s\n", timeMs, bufNum);

    simple_uart_putstring((const uint8_t*)buf);
    
       if (bShowRaw) simple_uart_put('\r');
       if (bShowRaw) simple_uart_put('\n');
	#endif
}
/**@snippet [Handling the data received over BLE] */


void FormatHexes(char buf[], uint32_t bufSize, uint8_t data[], uint32_t dataCount)
{
    int i;
    char bufTmp[20];
    char ch;
    
    
    bufTmp[0] = 0;
    
    ASSERT(dataCount <= 20);
    ASSERT(bufSize > 80);
    
    snprintf(buf, bufSize, "Count = %2u: 0x", dataCount);
    
    for (i = 0; i < dataCount; i++)
    {
        snprintf(bufTmp, sizeof(bufTmp), "%02X ", data[i]);        
        strncat(buf, bufTmp, 3);
    }
    
    strncat(buf, " ", bufSize); 
    for (i = 0; i < dataCount; i++)
    {
        ch = (data[i] > ' ' && data[i] <= 'z') ? data[i] : '.';
        snprintf(bufTmp, sizeof(bufTmp), "%c", ch);
   
        strncat(buf, bufTmp, 2);        
        
        if (strlen(buf) >= bufSize - 3 )
        {
            break;
        }
    }
    
    
    strcat(buf, "\r\n");
}    




/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    uint32_t         err_code;
    ble_nus_init_t   nus_init;
    
    memset(&nus_init, 0, sizeof(nus_init));

    nus_init.data_handler = nus_data_handler;
    
    err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing security parameters.
 */
static void sec_params_init(void)
{
    m_sec_params.timeout      = SEC_PARAM_TIMEOUT;
    m_sec_params.bond         = SEC_PARAM_BOND;
    m_sec_params.mitm         = SEC_PARAM_MITM;
    m_sec_params.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    m_sec_params.oob          = SEC_PARAM_OOB;  
    m_sec_params.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    m_sec_params.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
}


/**@brief       Function for handling an event from the Connection Parameters Module.
 *
 * @details     This function will be called for all events in the Connection Parameters Module
 *              which are passed to the application.
 *
 * @note        All this function does is to disconnect. This could have been done by simply setting
 *              the disconnect_on_fail config parameter, but instead we use the event handler
 *              mechanism to demonstrate its use.
 *
 * @param[in]   p_evt   Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;
    
    if(p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief       Function for handling errors from the Connection Parameters module.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;
    
    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;
    
    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for starting advertising.
 */
static void advertising_start(void)
{
    uint32_t             err_code;
    ble_gap_adv_params_t adv_params;
    
    // Start advertising
    memset(&adv_params, 0, sizeof(adv_params));
    
    adv_params.type        = BLE_GAP_ADV_TYPE_ADV_IND;
    adv_params.p_peer_addr = NULL;
    adv_params.fp          = BLE_GAP_ADV_FP_ANY;
    adv_params.interval    = APP_ADV_INTERVAL;
    adv_params.timeout     = APP_ADV_TIMEOUT_IN_SECONDS;

    err_code = sd_ble_gap_adv_start(&adv_params);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
    APP_ERROR_CHECK(err_code);
}


/**@brief       Function for the Application's S110 SoftDevice event handler.
 *
 * @param[in]   p_ble_evt   S110 SoftDevice event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    uint32_t                         err_code;
    static ble_gap_evt_auth_status_t m_auth_status;
    ble_gap_enc_info_t *             p_enc_info;
    
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            err_code = bsp_indication_set(BSP_INDICATE_BONDING); // was BSP_INDICATE_CONNECTED);= full on
            DebugTrace("BLE_GAP_EVT_CONNECTED\n");
            APP_ERROR_CHECK(err_code);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;

            break;
            
        case BLE_GAP_EVT_DISCONNECTED:
            err_code = bsp_indication_set(BSP_INDICATE_IDLE);
            DebugTrace("BLE_GAP_EVT_DISCONNECTED\n");
            APP_ERROR_CHECK(err_code);
            m_conn_handle = BLE_CONN_HANDLE_INVALID;

            advertising_start();

            // Prevent misleading log message - only print if downloading
            if (FileDownloaderIsBusy(0)  )
            {
                DebugTrace("Cancelling file download\n");
            }
            FileDownloaderCancelOnDisconnect(0);
        
            break;
            
        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, 
                                                   BLE_GAP_SEC_STATUS_SUCCESS, 
                                                   &m_sec_params);
            APP_ERROR_CHECK(err_code);
            break;
            
        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_AUTH_STATUS:
            m_auth_status = p_ble_evt->evt.gap_evt.params.auth_status;
            break;
            
        case BLE_GAP_EVT_SEC_INFO_REQUEST:
            
            DebugTrace("BLE_GAP_EVT_SEC_INFO_REQUEST\n");
            p_enc_info = &m_auth_status.periph_keys.enc_info;
            if (p_enc_info->div == p_ble_evt->evt.gap_evt.params.sec_info_request.div)
            {
                DebugTrace("Keys found");
                err_code = sd_ble_gap_sec_info_reply(m_conn_handle, p_enc_info, NULL);
                APP_ERROR_CHECK(err_code);
            }
            else
            {
                DebugTrace("No keys found for this device");
                err_code = sd_ble_gap_sec_info_reply(m_conn_handle, NULL, NULL);
                APP_ERROR_CHECK(err_code);
            }
            break;

        case BLE_GAP_EVT_TIMEOUT:
        
        #ifdef DISABLE_SLEEP
            DebugTrace("\nAdvertising timeout after %u, sleep disabled\n", APP_ADV_TIMEOUT_IN_SECONDS);
        
            // restart it.
            advertising_start();
            break;
        #else                 
            if (p_ble_evt->evt.gap_evt.params.timeout.src == BLE_GAP_TIMEOUT_SRC_ADVERTISEMENT)
            { 
                err_code = bsp_indication_set(BSP_INDICATE_IDLE);
                APP_ERROR_CHECK(err_code);
                // Configure buttons with sense level low as wakeup source.
                err_code = bsp_buttons_enable(1 << WAKEUP_BUTTON_ID);
                APP_ERROR_CHECK(err_code);
                // Go to system-off mode (this function will not return; wakeup will cause a reset)
                err_code = sd_power_system_off();    
                APP_ERROR_CHECK(err_code);
            }
            break;
        #endif // DISABLE_SLEEP
            
       case BLE_EVT_TX_COMPLETE:
            // DebugTrace("BLE_EVT_TX_COMPLETE\n");
            if(!ble_buffer_available) tx_complete = true;
            txCompleteEventCount++;
            break;

            
        default:
            // No implementation needed.
            break;
    }
}


/**@brief       Function for dispatching a S110 SoftDevice event to all modules with a S110
 *              SoftDevice event handler.
 *
 * @details     This function is called from the S110 SoftDevice event interrupt handler after a
 *              S110 SoftDevice event has been received.
 *
 * @param[in]   p_ble_evt   S110 SoftDevice event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    ble_conn_params_on_ble_evt(p_ble_evt);
    ble_nus_on_ble_evt(&m_nus, p_ble_evt);
    on_ble_evt(p_ble_evt);
    
    #ifdef BONDING_API
    dm_ble_evt_handler(p_ble_evt); //Add this line for bond 
    #endif
}



/**@brief   Function for the S110 SoftDevice initialization.
 *
 * @details This function initializes the S110 SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    uint32_t err_code;
    
    // Initialize SoftDevice.
    SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_XTAL_20_PPM, false);

    // Enable BLE stack 
    ble_enable_params_t ble_enable_params;
    memset(&ble_enable_params, 0, sizeof(ble_enable_params));
    ble_enable_params.gatts_enable_params.service_changed = IS_SRVC_CHANGED_CHARACT_PRESENT;
    err_code = sd_ble_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);
    
    // Subscribe for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}



/**@brief  Function for placing the application in low power state while waiting for events.
 */
static void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}


/**@brief  Function for initializing the UART module. at 38400
 */
static void uart_init(void)
{
    /**@snippet [UART Initialization] */
    simple_uart_config(RTS_PIN_NUMBER, TX_PIN_NUMBER, CTS_PIN_NUMBER, RX_PIN_NUMBER, HWFC);
    
    NRF_UART0->INTENSET = UART_INTENSET_RXDRDY_Enabled << UART_INTENSET_RXDRDY_Pos;
    
    NVIC_SetPriority(UART0_IRQn, APP_IRQ_PRIORITY_LOW);
    NVIC_EnableIRQ(UART0_IRQn);
    /**@snippet [UART Initialization] */
}


/**@brief   Function for handling UART interrupts.
 *
 * @details This function will receive a single character from the UART and append it to a string.
 *          The string will be be sent over BLE when the last character received was a 'new line'
 *          i.e '\n' (hex 0x0D) or if the string has reached a length of @ref NUS_MAX_DATA_LENGTH.
 * @note WARNING - this function nobbled - see note in code below.
 */
void UART0_IRQHandler(void)
{
    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint8_t index = 0;
    uint32_t err_code;

    /**@snippet [Handling the data received over UART] */

    data_array[index] = simple_uart_get();
    index++;
    err_code = err_code;
    
    if ((data_array[index - 1] == '\n') || (index >= (BLE_NUS_MAX_DATA_LEN - 1)))
    {
        
        // This demo has no need to accept chars from serial port and send them
        // so remove this feature. Problem is that PuTTY emits a string (by default) on ^E
        // and this occcasionally ends up inserted into a payload!
#if 0        
        err_code = ble_nus_send_string(&m_nus, data_array, index + 1);
        if (err_code != NRF_ERROR_INVALID_STATE)
        {
            APP_ERROR_CHECK(err_code);
        }
#endif

        index = 0;
    }

    /**@snippet [Handling the data received over UART] */
}



void app_trace_dump(uint8_t * p_buffer, uint32_t len)
{
    // 
}
 
 
/**@brief  Application main function.
 */
int main(void)
 {
    uint32_t err_code;
     
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_MAX_TIMERS, APP_TIMER_OP_QUEUE_SIZE, false);
    APP_GPIOTE_INIT(APP_GPIOTE_MAX_USERS);
    ble_stack_init();
    uart_init();

    err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS, APP_TIMER_TICKS(100, APP_TIMER_PRESCALER), NULL);
    APP_ERROR_CHECK(err_code);
    err_code = bsp_buttons_enable(0x0f);// was 1 << WAKEUP_BUTTON_ID);
    APP_ERROR_CHECK(err_code);

     
   //  app_trace_init();
     
    gap_params_init();
    services_init();
    advertising_init();
    conn_params_init();
    sec_params_init();
   

    device_manager_init(true);
    
    advertising_start();
    

    PrintStartupInfo();
    
    // The main simulator app
	CmuiDemoInit();
  
    // Enter main loop
    for (;;)
    {	
        TxLoop(); // Process data send out over BTLE

        power_manage();
        
        LEDS_OFF(BSP_LED_1_MASK); // Nobble Led *2* on PCB (2 is 1)
    }
}
 

// from https://github.com/NordicSemiconductor/nrf51-UART-examples/blob/master/app_uart_library_example_with_ble/main.c
static void TxLoop(void)
{
    uint32_t txCount;
    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint8_t numToSend = 0;
    uint8_t newbyte;
    
    /*Stop reading new data if there are no ble buffers available */
    if (ble_buffer_available)
    {
            // Transfer data to be sent from the tx Ringbuffer to the BTLE stack
            txCount = RingBufferUsed(&txRingBuffer);
       
            
            while( txCount > 0) 
            { 
                txCount--; 
                
                if (numToSend < BLE_NUS_MAX_DATA_LEN)
                {
                    newbyte = RingBufferReadOne(&txRingBuffer);
                    data_array[numToSend++] =  newbyte;
                }
                else
                {
                    ASSERT(0);    
                }
                
                if (numToSend >= (BLE_NUS_MAX_DATA_LEN))
				{
                    ble_buffer_available = ble_attempt_to_send(&data_array[0],numToSend);
                    // DebugTrace("%04u Tx %u\n", (uint32_t)GetUptimeMs(), numToSend);                 
                    if(ble_buffer_available) numToSend = 0;
                    
                    break;
				}

                 // Flush at end of data cluster (pkt)
                if (0 == txCount)
                {
                    ble_buffer_available = ble_attempt_to_send(&data_array[0], numToSend);
					// DebugTrace("%04u:FLUSH Tx %u\n", (uint32_t)GetUptimeMs(), numToSend);                  	
									
                    if (ble_buffer_available) numToSend = 0;
                    
                } 
            } // while txCount
        } // if buffer available
    
    /* Re-transmission if ble_buffer_available was set to false*/
    if (tx_complete)
    {
        tx_complete = false;
            
        ble_buffer_available = ble_attempt_to_send(&data_array[0], numToSend);
        DebugTrace("RETRY Tx %u\n", numToSend);                
        
        if  (ble_buffer_available) 
        {
            numToSend = 0;
        }
    } 
}



// from https://github.com/NordicSemiconductor/nrf51-UART-examples/blob/master/app_uart_library_example_with_ble/main.c
bool ble_attempt_to_send(uint8_t* data, uint8_t length)
{
    uint32_t err_code;
    
    err_code = ble_nus_send_string(&m_nus, data, length);
    
    if (err_code == BLE_ERROR_NO_TX_BUFFERS)
    {
        /* ble tx buffer full*/
        return false;
    }                   
    else if (err_code != NRF_ERROR_INVALID_STATE)
	{
        APP_ERROR_CHECK(err_code);   
    }
    
    return true;
}


// Endless loop for debug
void HardFault_Handler(void)
{
    int i = 0x55;
    simple_uart_putstring((const uint8_t*)"\r\n\r\nHARD FAULT\r\n");
    while(i)
    {
        i ^= 1;
    }
}


#if 0
   void appAssert(int n) 
 {
     volatile int i;
     
     for(;;)
     {
      simple_uart_putstring((const uint8_t*)"\r\n\r\nASSERT FAIL\r\n");
   
         i = 10000000;
         while (--i){;}
         }
 }
 #endif

 
 
 ///
 
static void PrintStartupInfo(void)
 {
    uint32_t i = 0;
         
	const S_IMAGE_VERSION_INFO *version;
    version = BtleServerVersionGet();
     
    for(i = 0; i < 20; i++)  DebugTrace("\r\n");
    for(i = 0; i < 80; i++)  DebugTrace("*");
    
    DebugTrace("\r\n\r\nCMIUBtleServerSimulator starting v%x.%x.%x.%x...\r\n",
							 version->versionMajorBcd,
							 version->versionMinorBcd,
							 version->versionYearMonthDayBcd,
							 version->versionBuildBcd);
                             
    DebugTrace("\r\nBuild Time: %s : %s\r\n", __DATE__, __TIME__);
                             
    DebugTrace("DEVICE_NAME: %s\r\n", DEVICE_NAME);
    DebugTrace("\nPress button 3 to req file d/l; Button 4 sends asyc msgs...\r\n");
    #ifdef DISABLE_SLEEP
    DebugTrace("ADVERTISING_TIMEOUT Disabled\n");
    #else
    DebugTrace("ADVERTISING_TIMEOUT_IN_SECONDS  = %us\n", APP_ADV_TIMEOUT_IN_SECONDS);
    #endif		
    
    DebugTrace("UUIDBase = %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n",
    uuidCopy.uuid128[15],uuidCopy.uuid128[14],uuidCopy.uuid128[13],uuidCopy.uuid128[12],
    uuidCopy.uuid128[11],uuidCopy.uuid128[10],uuidCopy.uuid128[ 9],uuidCopy.uuid128[ 8],
    uuidCopy.uuid128[ 7],uuidCopy.uuid128[ 6],uuidCopy.uuid128[ 5],uuidCopy.uuid128[ 4],
    uuidCopy.uuid128[ 3],uuidCopy.uuid128[ 2],uuidCopy.uuid128[ 1],uuidCopy.uuid128[ 0]);
     
    DebugTrace("\r\n");
}
 


#ifdef BONDING_API
static dm_application_instance_t         m_app_handle;                              /**< Application identifier allocated by device manager */



/**@brief Function for handling the Device Manager events.
 *
 * @param[in] p_evt  Data associated to the device manager event.
 */
static uint32_t device_manager_evt_handler(dm_handle_t const * p_handle,
                                           dm_event_t const  * p_event,
                                           uint32_t        event_result)
{
    APP_ERROR_CHECK(event_result);

    return NRF_SUCCESS;
}



/**@brief Function for the Device Manager initialization.
 *
 * @param[in] erase_bonds  Indicates whether bonding information should be cleared from
 *                         persistent storage during initialization of the Device Manager.
 */
static void device_manager_init(bool erase_bonds)
{
    uint32_t               err_code;
    dm_init_param_t        init_param = {.clear_persistent_data = erase_bonds};
    dm_application_param_t register_param;

    // Initialize persistent storage module.
    err_code = pstorage_init();
    APP_ERROR_CHECK(err_code);

    err_code = dm_init(&init_param);
    APP_ERROR_CHECK(err_code);

    memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));

    register_param.sec_param.bond         = SEC_PARAM_BOND;
    register_param.sec_param.mitm         = SEC_PARAM_MITM;
    register_param.sec_param.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    register_param.sec_param.oob          = SEC_PARAM_OOB;
    register_param.sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    register_param.sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
    register_param.evt_handler            = device_manager_evt_handler;
    register_param.service_type           = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

    err_code = dm_register(&m_app_handle, &register_param);
    APP_ERROR_CHECK(err_code);
}

#endif


/** 
 * @}
 */
