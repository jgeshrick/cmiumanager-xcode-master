/*****************************************************************************
******************************************************************************
**
**         Filename: CMIUDemo.c
**    
**          Author: Duncan Willis
**          Created: 27 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***************************************************************************
* @file CMIUDemo.c
*
* @brief Code to demonstrate the UART Controller common code
*  
* 
******************************************************************************/

#include <stdint.h>
#include <string.h>
#include "app_timer.h"
#include "app_button.h"
#include "ble_nus.h"
#include "simple_uart.h"
#include "bsp.h"
#include "UartController.h"
#include "RingBuffer.h"
#include "FileDownloadDemo.h"
#include "CmiuDemoCommand.h"
#include "CmiuDemo.h"
 
#include "FileDownloader.h" 
// Ringbuffers
// These need to be seen both by the UART ISR, and by the modem object, which will 
// keep a reference to them
#define RING_BUFFER_SIZE_RX             256  // depends on tick rate / data rate etc.
#define RING_BUFFER_SIZE_TX             360

static uint8_t                          rxData[RING_BUFFER_SIZE_RX];
static uint8_t                          txData[RING_BUFFER_SIZE_TX];
static RingBufferT                      rxRingBuffer;   // Buffer for data FROM BTLE
  RingBufferT                           txRingBuffer;   // Buffer for data TO BTLE


static uint32_t                         constTickIntervalMs = 10;
static app_timer_id_t                   tickTimer;
extern ble_nus_t                        m_nus; // see main.c
UART_CONTROLLER                         uc;
static UARC_MSG_READER                  mr;

// Max size of an rx payload 
#define RX_PAYLOAD_BUFFER_SIZE          256 // 1024     //512/////@todo was 2k
// Assembly buffer for reader
static uint8_t                          payloadBuffer[RX_PAYLOAD_BUFFER_SIZE];


// Dummy rolling uptime counter
static uint64_t msSinceStartup = 0;


uint32_t rxByteCount = 0;
uint32_t rxByteCountOld = 0;

bool ble_attempt_to_send(uint8_t * data, uint8_t length);


// BUTTON 4! nearest board corner
#define BUTTON__4                       3 
#define BUTTON__3                       2 
static bool led4State                   = false;

static void tickTimerHandler(void* p);
static void OnReceiveMessage(const struct UARC_MSG* pRxMsg, const void *context);
static void PingMessageHandler(const struct  UARC_MSG*  pRxMsg);
static void AsyncMessageHandler(const struct  UARC_MSG*  pRxMsg);
static void HttpResponseHandler(const struct  UARC_MSG*  pRxMsg);
static void CommandMessageHandler(const struct  UARC_MSG*  pRxMsg);
static void UnknownMessageHandler(const struct  UARC_MSG*  pRxMsg);
static void PrintMessage(const struct  UARC_MSG*  pRxMsg);
/* Reserved static void PrintTxMessage(const struct UARC_MSG* pTxMsg);*/

static void BeginFileDemo(void);

/*****************************************************************************
 * @brief
 *   Init the Uart Controller demo (Part of code to demonstrate the UC)
 * 
 * @param None
 *
 * @return 
 *   None
 *
 *****************************************************************************/
void CmuiDemoInit()
{		
    uint32_t err_code;
    UART_CONTROLLER_INIT_PARAMS initParams = {0};
    
    RingBufferInit(&rxRingBuffer, rxData, sizeof(rxData));
    RingBufferInit(&txRingBuffer, txData, sizeof(txData));

    initParams.pRbReceive                   = &rxRingBuffer;
    initParams.pRbTransmit                  = &txRingBuffer;
    initParams.pMessageReader               = &mr;
    initParams.pfnRxHandler                 = &OnReceiveMessage;
    initParams.sendInactivityThresholdMs    = 2000;
    
    // Init reader to assemble incoming messages
    UartMessageReaderInit (&mr, payloadBuffer, sizeof(payloadBuffer));
    
    UartControllerInit(&uc, &initParams);

    err_code = app_timer_create(&tickTimer, APP_TIMER_MODE_REPEATED, tickTimerHandler);
    APP_ERROR_CHECK(err_code);
    
    err_code = app_timer_start(tickTimer, APP_TIMER_TICKS(constTickIntervalMs, APP_TIMER_PRESCALER_COPY), tickTimerHandler);
    APP_ERROR_CHECK(err_code);
}


// Simple uptime
uint64_t GetUptimeMs(void)
{
    return msSinceStartup;
}


/*****************************************************************************
 * @brief
 *   Tick process to handle periodic data transfer / flashing LEDs / Send on 
 *   button press.
 *
 * @param Not used (from Nordic demo)
 *
 * @return 
 *   None
 *
 *****************************************************************************/
static void tickTimerHandler(void* p)
{
    static uint32_t prevCrcErrorCount = 0;
    static uint32_t prevUnsyncCount = 0;
    static uint32_t i = 0;
    bool button4Pushed = false; 
    bool button3Pushed = false;    
    static bool doFileTransfer = false;
  
    unsigned char dc;
    
    msSinceStartup += constTickIntervalMs;
    
    // Tick UARC
    UartControllerTick(&uc, constTickIntervalMs);

    
    if (mr.crcErrorCount != prevCrcErrorCount)
    {
         DebugTrace("\n\nERROR RX CRC error count %u, lostSync = %u\n",
            mr.crcErrorCount,
            mr.unsyncCount);
        prevCrcErrorCount = mr.crcErrorCount;
    }
     
    
    if (msSinceStartup %1000 == 0)
    {
        if (mr.unsyncCount != prevUnsyncCount)
        {
             DebugTrace("\n\nERROR RX  lostSync = %u\n",   mr.unsyncCount);
             prevUnsyncCount = mr.unsyncCount;
            
              DebugTrace("\n RxBytes= %u;delta=%u CRC error count %u \n",
            rxByteCount,
            rxByteCount - rxByteCountOld,
            mr.crcErrorCount);
            
            rxByteCountOld = rxByteCount;
        }
    }   
    
    // Tick file downloader
    FileDownloadDemoTick(constTickIntervalMs);
    
    // Handle Debug Trace output to embedded uart USB VCP
    while (DebugTraceGetChars(&dc,1))
    {
        // Add explicit CR for PuTTY
        if (dc == '\n') simple_uart_put('\r');
        
        simple_uart_put(dc);
    }  
    
    // Send msg on button press / LED flashing
    if (i++ & (1<<5))
    {
        //
        // Button 3 starts a file transfer
        //
        bsp_button_is_pressed(BUTTON__3,  &button3Pushed);  
        if (true == button3Pushed)
        {
            if (false == doFileTransfer)
            {
                doFileTransfer = true;
                BeginFileDemo();
            }
        }
        else
        {
            doFileTransfer = false;
        }
        
        //
        // Button 4 sends async msgs while pressed
        //
        bsp_button_is_pressed(BUTTON__4,  &button4Pushed);  
 
        {
            LEDS_OFF(BSP_LED_1_MASK | BSP_LED_2_MASK);
        }
      
      if (true == button4Pushed)
      {
          UartControllerSend(&uc, UARC_MT_ASYNC, 
            (const uint8_t*) "BUTTON 4 PRESSED!", 
            17);
          
          LEDS_ON(BSP_LED_2_MASK); // = LED3
      }
      
    }
    else
    {
        // Turn on LED 2
        LEDS_ON(BSP_LED_1_MASK);
    }
}


/// Sends a request to the Simulator to start requesting file blocks over HTTP
static void BeginFileDemo()
{
    char buf[100];
 
    snprintf(buf, sizeof(buf), "$FILE_DOWNLOAD_BEGIN$\n"); 
    UartControllerSend(&uc, UARC_MT_ASYNC, (const uint8_t*) buf, strlen(buf)+1);
    
    //int zero = 0;
    
    FileDownloaderBeginSession(0);
}


/// When complete, send machine readable status for process by Int. Test
void OnEndFileDownload(const FILE_DOWNLOADER* pDownloader)
{
    char buf[100];
    
    ASSERT(NULL != pDownloader);  
 
    snprintf(buf, sizeof(buf), "$FILE_DOWNLOAD_RESULTS$,%u,%u,%u,%u,%u,%u,%u\n",
            (uint32_t)pDownloader->errorState, 
            pDownloader->byteCount,
            pDownloader->timeElapsedMs / 1000ul, 
            pDownloader->requestSize,   
            pDownloader->totalContentLength,
            pDownloader->numRequests,
            pDownloader->numFailedRequests); 

    UartControllerSend(&uc, UARC_MT_ASYNC, (const uint8_t*) buf, strlen(buf)+1);
}




/*****************************************************************************
 * @brief
 *   Handles a char that arrived from BTLE. Puts it into ringbuf for later 
 * processing.
 *
 * @param [in] b 
 *  The received byte
 *
 * @return None
 *
 *****************************************************************************/
void CmiuDemoOnRxByte(uint8_t b)
{
    RingBufferWriteOne(&rxRingBuffer, b);
   
    // Enable line below to print incoming rx data
    //DebugTrace(&b);
    
    if(rxRingBuffer.Overflow)
    {
        DebugTrace("\n\nError RX Overflow\n");
    }
    
    rxByteCount++;
}


/*****************************************************************************
 * @brief
 *   Handles a message once received and checked
 *
 * @param [in] pRxMsg 
 *  The received message
 *
 * @return None
 *
// *****************************************************************************/
static void OnReceiveMessage(const struct  UARC_MSG*  pRxMsg, const void *context)
{
    (void)context;
    
    PrintMessage(pRxMsg);
    
    switch(pRxMsg->type)
    {
        case UARC_MT_NULL:
            break;
        
        case UARC_MT_CMD:
            CommandMessageHandler(pRxMsg);
            break;
        
        case UARC_MT_PING:
            PingMessageHandler(pRxMsg);
            break;        
        
        case UARC_MT_ASYNC:
            AsyncMessageHandler(pRxMsg);
            break;
                
        case UARC_MT_HTTP_RSP:
            HttpResponseHandler(pRxMsg);
            break;
        
        default:
            UnknownMessageHandler(pRxMsg);
            break;
    }
}


/*****************************************************************************
 * @brief
 *   Sends a response to the ping message 
 *
 * @param [in] pRxMsg 
 *  The received ping message
 *
 * @return None
 *
// *****************************************************************************/
static void PingMessageHandler(const struct  UARC_MSG*  pRxMsg)
{    
    UartControllerSend(&uc, 
    UARC_MT_GNIP,
    (const uint8_t*) pRxMsg->pPayload,  pRxMsg->length);

    simple_uart_putstring((const uint8_t*)"Ping response sent\r\n");
}


/*****************************************************************************
 * @brief
 *   Handle an async (no response required) message 
 *
 * @param [in] pRxMsg 
 *  The received ping message
 *
 * @return None
 *
// *****************************************************************************/
static void AsyncMessageHandler(const struct  UARC_MSG*  pRxMsg)
{    
    simple_uart_putstring((const uint8_t*)"Async msg received");
}


/*****************************************************************************
 * @brief
 *   Handle a  response to an HTTP get, coming from server 
 *
 * @param [in] pRxMsg 
 *  The received HTTP content
 *
 * @return None
 *
// *****************************************************************************/
static void HttpResponseHandler(const struct  UARC_MSG*  pRxMsg)
{    
    FileDownloadHandleHttpResponse(pRxMsg->pPayload, pRxMsg->length);
}


/*****************************************************************************
 * @brief
 *   Process a command and sends a response to it
 *
 * @param [in] pRxMsg 
 *  The received command message
 *
 * @return None
 *
// *****************************************************************************/
static void CommandMessageHandler(const struct  UARC_MSG*  pRxMsg)
{
    led4State = !led4State; //toggle LED4
        
    if (led4State)
    {
        LEDS_ON(BSP_LED_3_MASK);
    }
    else
    {
        LEDS_OFF(BSP_LED_3_MASK);
    }
    
    // Send to Tpbp layer
    CmiuMessageHandler(pRxMsg) ; 
}


/*****************************************************************************
 * @brief
 *   Sends a response to an unknown or unhandled message 
 *
 * @param [in] pRxMsg 
 *  The received  message
 *
 * @return None
 *
// *****************************************************************************/
static void UnknownMessageHandler(const struct  UARC_MSG*  pRxMsg)
{
    UartControllerSend(&uc, 
        UARC_MT_ERROR,
        (const uint8_t*) "",  0);

    simple_uart_putstring((const uint8_t*)"UnknownMessageHandler:Error response sent\r\n");
}


#define VERBOSE_DEBUG
/*****************************************************************************
 * @brief
 *   Debug print a message to UART output
 *
 * @param [in] pRxMsg 
 *  The received  message
 *
 * @return None
 *
// *****************************************************************************/
static void PrintMessage(const struct UARC_MSG* pRxMsg)
{    
    char bufStr[64];

    LEDS_ON(BSP_LED_2_MASK);
   
    UartControllerFormatMessage(pRxMsg, bufStr, sizeof (bufStr)); 
    
    #ifdef VERBOSE_DEBUG
    // simple_uart_putstring((const uint8_t*)"\r\n%04u UARC RX: ");
    DebugTrace((const char*)"\r\n%04u UARC RX: %s", (uint32_t)msSinceStartup, bufStr);
    //simple_uart_putstring((const uint8_t*)bufStr);
    //simple_uart_putstring((const uint8_t*)"\r\n");
    #endif
}

#if 0 // reserved for future use
static void PrintTxMessage(const struct UARC_MSG* pTxMsg)
{    
    char bufStr[64];
 
    UartControllerFormatMessage(pTxMsg, bufStr, sizeof (bufStr)); 
    
    #ifdef VERBOSE_DEBUG 
    DebugTrace((const char*)"\r\n%04u UARC TX: %s", (uint32_t)msSinceStartup, bufStr);
    #endif
}
#endif

 
