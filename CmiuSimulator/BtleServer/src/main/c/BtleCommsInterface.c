/******************************************************************************
*******************************************************************************
**
**         Filename: ModemStub.c
**    
**           Author: Duncan Willis
**          Created: 
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
 
/**
 * @addtogroup ModemStub
 * @{
 */

/*!***************************************************************************
 * @file  BtleCommsInterface.c
 *
 * @brief  Thin interface for HTTP file transfer and BTLE comms via uart controlller
 * @author Duncan Willis
 * @date 2015.03.19
 * @version 1.0
 *****************************************************************************/
 
 
/*===========================================================================*/
/*  I N C L U D E    F I L E S                                               */
/*===========================================================================*/


#include <stdio.h>
#include <string.h>

#include "TransportInterface.h"
#include "DebugTrace.h"
#include "BtleCommsInterface.h"

#include "UartController.h"


/*===========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                    */
/*===========================================================================*/


// The interface.
static int32_t ModemCommand(void* pCtx, E_CONNECTION_COMMAND cmd, void* cmdParams);
static void ModemTick(void* pCtx,  uint32_t tickIntervalMs);
static E_CONNECTION_STATE  ModemGetState(void* pCtx, E_QUERY query, void* pQueryResult);
static int32_t BtleSend(void* pCtx, const uint8_t* txData, uint32_t txCount);
static int32_t BtleRecv(void* pCtx, uint8_t* rxBuffer, uint32_t bufferSize);


static int ModemOpenHelper(void* pCtx);
static int ModemClose(void* pCtx);

// The object that sends the data in packets
extern UART_CONTROLLER                  uc;


static const TRANSPORT_INTERFACE modemInterface =
{
    BtleCommsInit,
    ModemCommand,
    ModemTick,
    ModemGetState,
    BtleRecv,
    BtleSend,
};


/**
* Ringbuffers
* These need to be seen both by the UART ISR, and by the modem object, which will 
* keep a reference to them
*/
#define RING_BUFFER_SIZE_RX 1024  // depends on tick rate / data rate etc.

static uint8_t rxData[RING_BUFFER_SIZE_RX];
static RingBufferT rxRingBuffer;    
static E_CONNECTION_STATE fakeState = CON_STATE_CONNECTION_OPEN_SOCKET_CLOSED;

/*===========================================================================*/
/*  F U N C T I O N S                                                        */
/*===========================================================================*/


/******************************************************************************
**
**     @param[in, out] pCtx The modem driver 
**          @param[in] initParams Optional pointer to init params
**             @return None
**  @brief
**         Description: Init the object - this should not block. Do any lengthy tasks
**                      as a command.
**
******************************************************************************/
int32_t BtleCommsInit(void* pCtx, const void* initParams)
{
    int32_t e = OK_STATUS;
    BTLE_COMMS* pModem = (BTLE_COMMS*)pCtx;

    pModem->pInterface = &modemInterface;
    pModem->cmd = CON_CMD_NULL;

    pModem->pRxRingBuffer = &rxRingBuffer;
    RingBufferInit(pModem->pRxRingBuffer, rxData, RING_BUFFER_SIZE_RX);
    return e;
}


/******************************************************************************
**    
**     @param[in, out] pCtx The modem driver
**          @param[in] cmd The command enumeration
**          @param[in] pCmdParamms Optional pointer to static data (may be null)
**             @return OK_STATUS if command accepted, else error code, or query result for CON_CMD_GET_RX_COUNT
**  @brief
**         Description: Run a command (eg CONNECT). For BTLE we fake 
**                      behaviour, since it's always connected.
**
******************************************************************************/
static int32_t ModemCommand(void* pCtx, E_CONNECTION_COMMAND cmd, void* pCmdParams)
{
    int32_t r = OK_STATUS;
    BTLE_COMMS* pModem = (BTLE_COMMS*)pCtx;
    
    switch (cmd)
           {
           case CON_CMD_OPEN_CONNECTION:
               fakeState = CON_STATE_CONNECTION_OPEN_SOCKET_CLOSED;
               break;         

           case CON_CMD_OPEN_SOCKET:
               fakeState = CON_STATE_SOCKET_OPEN;
               break;
    
           case CON_CMD_CLOSE_SOCKET:                 
               fakeState = CON_STATE_CONNECTION_OPEN_SOCKET_CLOSED;
               break;           
           
           case CON_CMD_CLOSE_CONNECTION:                 
               fakeState = CON_STATE_CONNECTION_CLOSED;
               break;
           
           case CON_CMD_GET_RX_COUNT:
               r = (int32_t)(RingBufferUsed(pModem->pRxRingBuffer));
               break;
           
           default:
               break;
       }
    
    return r;
}


/******************************************************************************
**    
**     @param[in, out] pCtx The modem driver
**          @param[in] tickIntervalMs The tick time in ms
**             @return None
**  @brief
**         Description: Run the state machine - must not block.  
**
******************************************************************************/
static void ModemTick(void* pCtx,  uint32_t tickIntervalMs)
{
       BTLE_COMMS* pModem = (BTLE_COMMS*)pCtx;
       
       switch (pModem->state)
       {
       case MS_RESET:
           switch (pModem->cmd)
           {
           case CON_CMD_NULL:
               break;

           case CON_CMD_OPEN_CONNECTION:
               pModem->cmd = CON_CMD_NULL;
               pModem->state = MS_CONNECT_BEGIN;
               break;

           case CON_CMD_CLOSE_CONNECTION:              
               pModem->cmd = CON_CMD_NULL;           
               pModem->state = MS_DISCONNECT_BEGIN;
               break;

           default:
               pModem->cmd = CON_CMD_NULL;
               break;
           }
           break;

       case MS_CONNECT_BEGIN:
            ModemOpenHelper(pCtx);
            pModem->state = MS_CONNECT_WAIT;
            break;       
       
       case MS_CONNECT_WAIT:
            // wait for conn if needed
            pModem->state = MS_RESET;
            break;
             
       case MS_DISCONNECT_BEGIN:
            ModemClose(pCtx);
            pModem->state = MS_DISCONNECT_WAIT;
            break;       
       
       case MS_DISCONNECT_WAIT:
            // wait for disconn if needed       
            pModem->state = MS_RESET;
            break;

       default:
           ASSERT(0);
           break;
       }
}

/******************************************************************************
**    
**     @param[in, out] pCtx The modem driver
**          @param[in] query An enummeration to get a query type
**         @param[out] pQueryResult Optional structure to be filled in 
**                                  with the query results
**             @return always returns OPEN
**  @brief
**         Description: Unused.
**
******************************************************************************/
static E_CONNECTION_STATE  ModemGetState(void* pCtx, E_QUERY query, void* pQueryResult)
{
    // Fake the state, as we are not actually opening / closing a connection in BTLE
    return fakeState;
}


/******************************************************************************
**    
**     @param[in, out] pCtx The BTLE driver
**          @param[in] txData The data to send
**          @param[in] txCount The number of bytes to send.      
**             @return numBytesWritten or 0 if none
**  @brief
**         Description: Packetise and send as http request msg 
**
******************************************************************************/
static int32_t BtleSend(void* pCtx, const uint8_t* txData, uint32_t txCount)
{
    int32_t numBytesWritten = 0;

    if (txCount > 0)
    {
       numBytesWritten = UartControllerSend(&uc, UARC_MT_HTTP_CMD, txData, txCount);

       // This line is a workaround solution for NP36 / MSPD-969 , connection dies in file download: 
//        UartControllerSend(&uc, UARC_MT_HEARTBEAT, "z", 1);
    }

	return numBytesWritten;
}


/******************************************************************************
**    
**      @param[in, out] pCtx The modem driver
**          @param[out] rxBuffer The buffer to copy into
**           @param[in] bufferSize The number of bytes to send.     
**              @return Return -1 for error,
**                      0 for no data (non block)
**                      N for N bytes copied into the supplied buffer
**  @brief
**         Description: Non-blocking receive function. 
**
******************************************************************************/
static int32_t BtleRecv(void* pCtx, uint8_t* rxBuffer, uint32_t bufferSize)
{
    BTLE_COMMS* pModem = (BTLE_COMMS*)pCtx;
    uint32_t rxCount = 0;

    // Data in ring buf to be read?
    rxCount = RingBufferUsed(pModem->pRxRingBuffer);

    // Limit transfer to size of supplied buffer
    if (rxCount > bufferSize) 
    {
        rxCount = bufferSize;
    }

    // Copy data if there is any.
    if (rxCount > 0)
    {
        RingBufferRead(pModem->pRxRingBuffer, rxBuffer, rxCount);
    }

    if ( rxCount > 0 )
    {
    //   DebugTrace("BTLE Bytes received: %d\n", rxCount);
    }
 
    return (int32_t)rxCount;
}


// Implementation stub function
static int32_t ModemOpenHelper(void* pCtx)
{
    // Nothing needed for BTLE demo - already open 
    return 0;
}


// Implementation stub function
static int32_t ModemClose(void* pCtx)
{
    int32_t rc = OK_STATUS;
    // Stub - In this function, start the action to begin disconnection.
    // The ticked state machine then needs to send AT commands etc, and poll until connection closed.
    return rc;
}

