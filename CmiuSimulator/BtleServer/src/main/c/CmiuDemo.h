
/******************************************************************************
 *
 *    Neptune Technology Group
 *    Copyright 2015 as unpublished work.
 *    All rights reserved
 *
 *    The information The information contained herein is confidential
 *    property of Neptune Technology Group. The use, copying, transfer
 *    or disclosure of such information is prohibited except by express
 *    written agreement with Neptune Technology Group.
 *
 *****************************************************************************/

 
/**
 * @addtogroup CmuiDemo 
 * @{
 */


/**
 * @file  CmuiDemo.c
 *
 * @brief   example interface between BTLE Nordic app, and UartController
 * @author Duncan Willis
 * @date 2015.04.23
 * @version 1.0
 */



#ifndef CMUI_DEMO_H_
#define CMUI_DEMO_H_

#include <stdint.h>

// see main.c
#define APP_TIMER_PRESCALER_COPY 0

void CmuiDemoInit(void);


void CmiuDemoOnRxByte(uint8_t b);

uint64_t GetUptimeMs(void);


#endif
