﻿


To use the BtleServer:

* Disconnect all Nordic devices from the PC
* Disconnect all Segger devices from the PC
* Install nRFGoStudio
* Plug in the nRF-DK PCBA via USB to the PC
* Start nRFGoStudio and check that the board is detected
* Install the softcore file to the dev kit (in CmiuSimulator\BtleServer\src\main\resources) via nRFGoStudio (see nRFgo Studio screenshot programming soft device.png)
* Grab CmiuBtleServerSimulator.hex from TeamCity in the UartController artifacts
* Install CmiuBtleServerSimulator.hex on the dev kit via nRFGoStudio
* Unplug the dev kit from the PC
* Plug in the Nordic dongle to the PC
* Power the dev kit from a USB power adaptor or other PC (don't use the PC because it will enumerate as a Nordic device that may confuse the TestHarness)
* Run the nRFDongleTestHarness app on the PC (see CMIT\src\integration\nRFDongleUart.sln)
* Connect
* Have fun!

-----------------------------------------------

CMIU BTLE Server Simulator
----------------------------

This is intended to serve as a development harness in lieu of a CMIU to run against 

This is a concept demo that runs on the dev board and shows the Nordic UART demo interfaced to the Common code UartController.
Uses Nordic Dev board pca10028

Based on   \Nordic\examples\ble_peripheral\experimental_ble_app_uart
Documentation can be found offline at: <keil_location>/ARM/Pack/NordicSemiconductor/nRF_Examples/7.2.0/documentation
Documentation can be found online at: http://developer.nordicsemi.com/nRF51_SDK/doc/

Here are the Nordic tools you will need to install.  You will need the master control panel and the SDK:
\\ntfsdocs\isoprojs\Projects\Neptune\MSPD\Working\Tools\Nordic

In addition you will need to install Keil uVision5 if you don’t have it already.  The one you want is mdk514.exe:
\\ukhardoc02\EmbeddedApps\Keil\ARM

Note on uVision 5 debug settings. Set (Project Options) to JLINK, and (under settings button SW (NOT JTAG)

Link PO.08 and PO.10 with around 3k3 resistor to enable flow control without needing to connect PuTTY etc. If F/C is not enabled the app hangs until CTS goes active!

Note: Dongle - unplug it before booting PC (else tries to boot from it) . Dongle does not coexist happily with dev board, so best to use different PCs.

Keil Project options to allow app code to coexist with core:
IROM1 Start:0x16000 Size:0x29000
IRAM1 Start:0x20002000 Size:0x2000
 
Program the softcore via nRFGo Studio

Use the MSVC app "nRFUart" with a dongle to talk to this (to be done - add uarc and demo features). O



Dongle
=============
Note the dongle is likely also need to be programmed.  
This can be done by selecting File>Flash Programming>MEFW nRF51822 v0.11.0.

Versions
==============
Neptune currently using v1.17.1.3252 nRFgo Studio and v3.9.0.6 Master Control Panel.


