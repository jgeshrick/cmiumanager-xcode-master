from datetime import datetime
from datetime import timedelta
import math

class UsageData:
    def __init__(self, filename):
        f = open(filename)
        lines = f.readlines()
        f.close()
        
        self.readings = list()
        for line in lines:
            byteArray = bytearray.fromhex(line.rstrip())
            flippedByteArray = bytearray([byteArray[3],
                                        byteArray[2],
                                        byteArray[1],
                                        byteArray[0]])
            self.readings.append(flippedByteArray)
        
        self.size = len(self.readings)
        
    def getNumberOfReadings(self):
        return self.size
        
    def getReading(self, readingNumber, readingIntervalMins):
        startDate = datetime(2012,1,1)
        nowDate = datetime.now() - timedelta(
                minutes=readingNumber * readingIntervalMins)
        dateDelta = nowDate - startDate
        dummyReading = int(dateDelta.total_seconds()/(60*15))

        totalMinutesToday = (nowDate.hour * 60) + nowDate.minute
        totalPeriods = int(math.floor(totalMinutesToday / 15))
        remainder = totalPeriods % 32

        leak = 0
        backflow = 0
        if (remainder == 1):
            leak = 1
        if (remainder == 2):
            backflow = 1
        if (remainder == 3):
            leak = 2
        if (remainder == 4):
            backflow = 2

        reading = bytearray(4)
        #Don't bother adding the 3 MSB of the reading to the first byte
        #as the dummy generated value does not flow over 3 bytes till about
        #the year 2500
        reading[0] = (backflow << 2) + leak
        reading[1] = (dummyReading >> 16) & 0xFF
        reading[2] = (dummyReading >> 8) & 0xFF
        reading[3] = (dummyReading) & 0xFF

        return reading

        #return below when using pre-generated readings
        #return self.readings[index]

a = UsageData("R900Readings.txt")
