'''
    Neptune Technology Group
    Copyright 2015 as unpublished work.
    All rights reserved

    The information contained herein is confidential
    property of Neptune Technology Group. The use, copying, transfer
    or disclosure of such information is prohibited except by express
    written agreement with Neptune Technology Group.
'''

import time
import threading

import CmiuSimWrapper as dllWrapper
import CmiuSimCmiu as CMIU

codeVersion = "0.12.160229.676"

'''
Starts the simulator running by populating a list of CMIU objects then running
runCMIUs().
'''
def startSim(args):
    idleThreadCount = threading.active_count()
    
    if args.numberOfCmius > 1:
        dllWrapper.EnableTsmDebugLog(False)
    
    CMIUs = list()
    for i in range(args.numberOfCmius):
        CMIUs.append(CMIU.CMIU((i*2)+args.cmiuStartNumber, args))
    CMIUs.sort(key=lambda time: time.updateTime)
    runCMIUs(CMIUs, args)

    while threading.active_count() != idleThreadCount:
        time.sleep(0.01)
    
'''
Calls the DoMqttTransaction method in CMIU objects when they are scheduled to
connect to the broker.
'''
def runCMIUs(CMIUs, args):
    if args.updateRepeat == 0:
        while(1):
            runCMIULoop(CMIUs, args)
    else:
        for repeatNumber in range(args.updateRepeat):
            runCMIULoop(CMIUs, args)
                
def runCMIULoop(CMIUs, args):
    startTime = time.time()
    for i in range(len(CMIUs)):
        while(startTime + CMIUs[i].updateTime > time.time()):
            time.sleep(0.005)
        CMIUs[i].DoMqttTransaction()
    while(startTime + args.updatePeriod > time.time()):
        time.sleep(0.005)










