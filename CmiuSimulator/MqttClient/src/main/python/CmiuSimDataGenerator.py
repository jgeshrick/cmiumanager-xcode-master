'''
    Neptune Technology Group
    Copyright 2015 as unpublished work.
    All rights reserved

    The information contained herein is confidential
    property of Neptune Technology Group. The use, copying, transfer
    or disclosure of such information is prohibited except by express
    written agreement with Neptune Technology Group.
'''

import sys
import random
import time
sys.path.insert(0, '../../../target')

import CmiuSimCInterface as CI

codeVersion = "${project.version}"

class CmiuData:
    '''
    Generates CMIU data so that each CMIU object starts from an intial state
    '''
    def __init__(self, ID, args):
        self.tags = {} #Dictionary of tagged data structures

        #Create CMIU packet header struct
        self.tags['CMIU_PACKET_HEADER'] = CI.S_CMIU_PACKET_HEADER()
        self.tags['CMIU_PACKET_HEADER'].cmiuId = ID
        self.tags['CMIU_PACKET_HEADER'].sequenceNumber = 1
        self.tags['CMIU_PACKET_HEADER'].keyInfo = 2
        self.tags['CMIU_PACKET_HEADER'].encryptionMethod = 0
        self.tags['CMIU_PACKET_HEADER'].tokenAesCrc = 4
        self.tags['CMIU_PACKET_HEADER'].networkFlags = 5
        self.tags['CMIU_PACKET_HEADER'].cellularRssiAndBer = (random.randrange(0, 31) +
                                                                    256 * random.randrange(0, 7))
        self.tags['CMIU_PACKET_HEADER'].timeAndDate = long(time.time())


        #Create CMIU configuration struct
        self.tags['CMIU_CONFIGURATION'] = CI.S_CMIU_CONFIGURATION()
        self.tags['CMIU_CONFIGURATION'].manufactureDate = long(time.time() -
                                        (100000 + random.randrange(0,15000000)))
        self.tags['CMIU_CONFIGURATION'].initialCallInTime = long(time.time())
        self.tags['CMIU_CONFIGURATION'].installationDate = long(time.time() -
                                                random.randrange(600,100000))
        self.tags['CMIU_CONFIGURATION'].dateLastMagSwipe = long(
                            self.tags['CMIU_CONFIGURATION'].installationDate +
                            random.randrange(10,500))
        self.tags['CMIU_CONFIGURATION'].hwRevision = 123
        self.tags['CMIU_CONFIGURATION'].firmwareRev = 234
        self.tags['CMIU_CONFIGURATION'].bootloaderVersion = 345
        self.tags['CMIU_CONFIGURATION'].minsToReportingWindowQuietTime = 456
        self.tags['CMIU_CONFIGURATION'].minsToReportingWindowQuietTimeEnd = 567
        self.tags['CMIU_CONFIGURATION'].batteryRemaining = 678
        self.tags['CMIU_CONFIGURATION'].cmiuType = 7
        self.tags['CMIU_CONFIGURATION'].callInInterval = args.reportingInterval
        self.tags['CMIU_CONFIGURATION'].callInWindow = 15
        self.tags['CMIU_CONFIGURATION'].reportingwindowNRetries = 0
        self.tags['CMIU_CONFIGURATION'].attachedDevices = 0
        self.tags['CMIU_CONFIGURATION'].magSwipes = random.randrange(1,3)
        self.tags['CMIU_CONFIGURATION'].timeErrorLastNetworkTimeAccess = random.randrange(-5, 5)


        self.tags['NETWORK_OPERATORS'] = CI.S_TPBP_TAG()
        self.tags['NETWORK_OPERATORS'].tagNumber = 3
        self.tags['NETWORK_OPERATORS'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['NETWORK_OPERATORS'].dataSize = 7
        self.tags['NETWORK_OPERATORS'].data = ((CI.c_byte*7).
                                        from_buffer(bytearray("Verizon", "ascii")))


        self.tags['NETWORK_PERFORMANCE'] = CI.S_TPBP_TAG()
        self.tags['NETWORK_PERFORMANCE'].tagNumber = 4
        self.tags['NETWORK_PERFORMANCE'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['NETWORK_PERFORMANCE'].dataSize = 106
        self.tags['NETWORK_PERFORMANCE'].data = ((CI.c_byte*106).
                                        from_buffer(bytearray("#RFSTS:\"311 480\",5230,-84,-57,-10,2C02,255,,128,19,0,0ADD502,\"311480148913416\",\"Verizon Wireless\",2,13,208", "ascii")))


        self.tags['REGISTRATION_STATUS'] = CI.S_TPBP_TAG()
        self.tags['REGISTRATION_STATUS'].tagNumber = 5
        self.tags['REGISTRATION_STATUS'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['REGISTRATION_STATUS'].dataSize = 23
        self.tags['REGISTRATION_STATUS'].data = ((CI.c_byte*23).
                                        from_buffer(bytearray("Dummy registration data", "ascii")))


        self.tags['MQTT_BROKER_ADDRESS'] = CI.S_TPBP_TAG()
        self.tags['MQTT_BROKER_ADDRESS'].tagNumber = 6
        self.tags['MQTT_BROKER_ADDRESS'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['MQTT_BROKER_ADDRESS'].dataSize = 15
        self.tags['MQTT_BROKER_ADDRESS'].data = ((CI.c_byte*15).
                                        from_buffer(bytearray("123.456.789.101", "ascii")))


        self.tags['FALLBACK_MQTT_BROKER_ADDRESS'] = CI.S_TPBP_TAG()
        self.tags['FALLBACK_MQTT_BROKER_ADDRESS'].tagNumber = 7
        self.tags['FALLBACK_MQTT_BROKER_ADDRESS'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['FALLBACK_MQTT_BROKER_ADDRESS'].dataSize = 15
        self.tags['FALLBACK_MQTT_BROKER_ADDRESS'].data = ((CI.c_byte*15).
                                        from_buffer(bytearray("112.131.415.161", "ascii")))


        self.tags['ASSIGNED_CMIU_IP_ADDRESS'] = CI.S_TPBP_TAG()
        self.tags['ASSIGNED_CMIU_IP_ADDRESS'].tagNumber = 8
        self.tags['ASSIGNED_CMIU_IP_ADDRESS'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['ASSIGNED_CMIU_IP_ADDRESS'].dataSize = 15
        self.tags['ASSIGNED_CMIU_IP_ADDRESS'].data = ((CI.c_byte*15).
                                        from_buffer(bytearray("718.192.021.222", "ascii")))

        self.tags['MODULE_FOTA_FTP_URL'] = CI.S_TPBP_TAG()
        self.tags['MODULE_FOTA_FTP_URL'].tagNumber = 9
        self.tags['MODULE_FOTA_FTP_URL'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['MODULE_FOTA_FTP_URL'].dataSize = 28
        self.tags['MODULE_FOTA_FTP_URL'].data = ((CI.c_byte*28).
                                        from_buffer(bytearray("www.APPLICATIONFTPUPDATE.com", "ascii")))

        self.tags['MODULE_FOTA_FTP_PORT'] = CI.S_TPBP_TAG()
        self.tags['MODULE_FOTA_FTP_PORT'].tagNumber = 10
        self.tags['MODULE_FOTA_FTP_PORT'].tagType = CI.E_TAG_TYPE_U16
        self.tags['MODULE_FOTA_FTP_PORT'].dataSize = 2
        self.tags['MODULE_FOTA_FTP_PORT'].data = (CI.c_uint16)(20)


        self.tags['MODULE_FOTA_FTP_USERNAME'] = CI.S_TPBP_TAG()
        self.tags['MODULE_FOTA_FTP_USERNAME'].tagNumber = 11
        self.tags['MODULE_FOTA_FTP_USERNAME'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['MODULE_FOTA_FTP_USERNAME'].dataSize = 13
        self.tags['MODULE_FOTA_FTP_USERNAME'].data = ((CI.c_byte*13).
                                        from_buffer(bytearray("dummyUsername", "ascii")))

        self.tags['MODULE_FOTA_FTP_PASSWORD'] = CI.S_TPBP_TAG()
        self.tags['MODULE_FOTA_FTP_PASSWORD'].tagNumber = 12
        self.tags['MODULE_FOTA_FTP_PASSWORD'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['MODULE_FOTA_FTP_PASSWORD'].dataSize = 13
        self.tags['MODULE_FOTA_FTP_PASSWORD'].data = ((CI.c_byte*13).
                                        from_buffer(bytearray("dummyPassword", "ascii")))
        '''
        DEPRECATED
        self.tags['MODULE_FOTA_USER_ID'] = CI.S_TPBP_TAG()
        self.tags['MODULE_FOTA_USER_ID'].tagNumber = 13
        self.tags['MODULE_FOTA_USER_ID'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['MODULE_FOTA_USER_ID'].dataSize = 4
        self.tags['MODULE_FOTA_USER_ID'].data = ((CI.c_byte*4).
                                        from_buffer(bytearray("myID", "ascii")))

        DEPRECATED
        self.tags['MODULE_FOTA_FTP_PASSWORD'] = CI.S_TPBP_TAG()
        self.tags['MODULE_FOTA_FTP_PASSWORD'].tagNumber = 14
        self.tags['MODULE_FOTA_FTP_PASSWORD'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['MODULE_FOTA_FTP_PASSWORD'].dataSize = 23
        self.tags['MODULE_FOTA_FTP_PASSWORD'].data = ((CI.c_byte*23).
                                        from_buffer(bytearray("dummy fota ftp password", "ascii")))
        '''

        self.tags['MODEM_HARDWARE_REVISION'] = CI.S_TPBP_TAG()
        self.tags['MODEM_HARDWARE_REVISION'].tagNumber = 15
        self.tags['MODEM_HARDWARE_REVISION'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['MODEM_HARDWARE_REVISION'].dataSize = 4
        self.tags['MODEM_HARDWARE_REVISION'].data = ((CI.c_byte*4).
                                        from_buffer(bytearray("1.05", "ascii")))


        self.tags['MODEM_MODEL_ID'] = CI.S_TPBP_TAG()
        self.tags['MODEM_MODEL_ID'].tagNumber = 16
        self.tags['MODEM_MODEL_ID'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['MODEM_MODEL_ID'].dataSize = 9
        self.tags['MODEM_MODEL_ID'].data = ((CI.c_byte*9).
                                        from_buffer(bytearray("LE910-SVG", "ascii")))


        self.tags['MODEM_MANUFACTURER_ID'] = CI.S_TPBP_TAG()
        self.tags['MODEM_MANUFACTURER_ID'].tagNumber = 17
        self.tags['MODEM_MANUFACTURER_ID'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['MODEM_MANUFACTURER_ID'].dataSize = 5
        self.tags['MODEM_MANUFACTURER_ID'].data = ((CI.c_byte*5).
                                        from_buffer(bytearray("Telit", "ascii")))


        self.tags['MODEM_SOFTWARE_REV'] = CI.S_TPBP_TAG()
        self.tags['MODEM_SOFTWARE_REV'].tagNumber = 18
        self.tags['MODEM_SOFTWARE_REV'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['MODEM_SOFTWARE_REV'].dataSize = 7
        self.tags['MODEM_SOFTWARE_REV'].data = ((CI.c_byte*7).
                                        from_buffer(bytearray("1.2.546", "ascii")))


        self.tags['MODEM_SERIAL_NUMBER'] = CI.S_TPBP_TAG()
        self.tags['MODEM_SERIAL_NUMBER'].tagNumber = 19
        self.tags['MODEM_SERIAL_NUMBER'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['MODEM_SERIAL_NUMBER'].dataSize = 13
        self.tags['MODEM_SERIAL_NUMBER'].data = ((CI.c_byte*13).
                                        from_buffer(bytearray("353238060161527", "ascii")))


        self.tags['SIM_IMSI'] = CI.S_TPBP_TAG()
        self.tags['SIM_IMSI'].tagNumber = 20
        self.tags['SIM_IMSI'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['SIM_IMSI'].dataSize = 15
        self.tags['SIM_IMSI'].data = ((CI.c_byte*15).
                                        from_buffer(bytearray("311480039222601", "ascii")))


        self.tags['SIM_ID'] = CI.S_TPBP_TAG()
        self.tags['SIM_ID'].tagNumber = 21
        self.tags['SIM_ID'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['SIM_ID'].dataSize = 20
        self.tags['SIM_ID'].data = ((CI.c_byte*20).
                                        from_buffer(bytearray("89148000000386225115", "ascii")))

        if ID == 400000210:
            self.tags['SIM_ID'].data = ((CI.c_byte*20).
                                        from_buffer(bytearray("89148000002036030226", "ascii")))


        self.tags['PIN_PUK_DEVICE_STATUS'] = CI.S_TPBP_TAG()
        self.tags['PIN_PUK_DEVICE_STATUS'].tagNumber = 22
        self.tags['PIN_PUK_DEVICE_STATUS'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['PIN_PUK_DEVICE_STATUS'].dataSize = 5
        self.tags['PIN_PUK_DEVICE_STATUS'].data = ((CI.c_byte*5).
                                        from_buffer(bytearray("READY", "ascii")))


        self.tags['CMIU_HW_DIAGNOSTICS'] = CI.S_TPBP_TAG()
        self.tags['CMIU_HW_DIAGNOSTICS'].tagNumber = 23
        self.tags['CMIU_HW_DIAGNOSTICS'].tagType = CI.E_TAG_TYPE_BYTE_RECORD
        self.tags['CMIU_HW_DIAGNOSTICS'].dataSize = 5
        self.tags['CMIU_HW_DIAGNOSTICS'].data = ((CI.c_byte*5).
                                        from_buffer(bytearray([1,2,3,4,5])))


        self.tags['BLE_LAST_USER_ID'] = CI.S_TPBP_TAG()
        self.tags['BLE_LAST_USER_ID'].tagNumber = 24
        self.tags['BLE_LAST_USER_ID'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['BLE_LAST_USER_ID'].dataSize = 6
        self.tags['BLE_LAST_USER_ID'].data = ((CI.c_byte*6).
                                        from_buffer(bytearray("NoUser", "ascii")))


        self.tags['BLE_LAST_USER_DATE'] = CI.S_TPBP_TAG()
        self.tags['BLE_LAST_USER_DATE'].tagNumber = 25
        self.tags['BLE_LAST_USER_DATE'].tagType = CI.E_TAG_TYPE_U64
        self.tags['BLE_LAST_USER_DATE'].dataSize = 8
        self.tags['BLE_LAST_USER_DATE'].data = ((CI.c_byte*8).
                                        from_buffer(bytearray([102,123,100,130,0,0,0,0])))


        self.tags['CMIU_STATUS'] = CI.S_CMIU_STATUS()
        self.tags['CMIU_STATUS'].cmiuType = 3
        self.tags['CMIU_STATUS'].numberOfDevices = 1
        self.tags['CMIU_STATUS'].cmiuFlags = 2


        self.tags['REPORTED_DEVICE_CONFIG'] = CI.S_REPORTED_DEVICE_CONFIG()
        self.tags['REPORTED_DEVICE_CONFIG'].deviceNumber = 32
        self.tags['REPORTED_DEVICE_CONFIG'].attachedDeviceId = 33
        self.tags['REPORTED_DEVICE_CONFIG'].deviceType = 34
        self.tags['REPORTED_DEVICE_CONFIG'].deviceFlags = 35


        self.tags['R900_INTERVAL_DATA'] = CI.S_TPBP_TAG()
        self.tags['R900_INTERVAL_DATA'].tagNumber = 28
        self.tags['R900_INTERVAL_DATA'].tagType = CI.E_TAG_TYPE_EXTENDED_U32_ARRAY
        self.tags['R900_INTERVAL_DATA'].dataSize = 4
        self.tags['R900_INTERVAL_DATA'].data = ((CI.c_byte*4).
                                        from_buffer(bytearray([1,2,3,4])))


        self.tags['EVENT'] = CI.S_TPBP_TAG()
        self.tags['EVENT'].tagNumber = 29
        self.tags['EVENT'].tagType = CI.E_TAG_TYPE_BYTE_RECORD
        self.tags['EVENT'].dataSize = 2
        self.tags['EVENT'].data = ((CI.c_byte*2).
                                    from_buffer(bytearray([41,42])))


        self.tags['ERROR_LOG'] = CI.S_TPBP_TAG()
        self.tags['ERROR_LOG'].tagNumber = 30
        self.tags['ERROR_LOG'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['ERROR_LOG'].dataSize = 7
        self.tags['ERROR_LOG'].data = ((CI.c_byte*7).
                                        from_buffer(bytearray("NoError", "ascii")))


        self.tags['CONNECTION_TIMING_LOG'] = CI.S_TPBP_TAG()
        self.tags['CONNECTION_TIMING_LOG'].tagNumber = 31
        self.tags['CONNECTION_TIMING_LOG'].tagType = CI.E_TAG_TYPE_BYTE_RECORD
        self.tags['CONNECTION_TIMING_LOG'].dataSize = 18
        self.tags['CONNECTION_TIMING_LOG'].data = ((CI.c_byte*18).
                                        from_buffer(bytearray([102,123,100,130,0,0,0,0,
                                                               4,5,
                                                               6,7,
                                                               8,9,
                                                               10,11,
                                                               12,13])))


        self.tags['COMMAND'] = CI.S_TPBP_TAG()
        self.tags['COMMAND'].tagNumber = 32
        self.tags['COMMAND'].tagType = CI.E_TAG_TYPE_BYTE
        self.tags['COMMAND'].dataSize = 1
        self.tags['COMMAND'].data = ((CI.c_byte).
                                    from_buffer(bytearray([0])))


        self.tags['BASIC_CMIU_CONFIG'] = CI.S_CMIU_BASIC_CONFIGURATION()
        self.tags['BASIC_CMIU_CONFIG'].initialCallInTime = 43
        self.tags['BASIC_CMIU_CONFIG'].callInInterval = args.reportingInterval
        self.tags['BASIC_CMIU_CONFIG'].callInWindow = 45
        self.tags['BASIC_CMIU_CONFIG'].reportingwindowNRetries = 46
        self.tags['BASIC_CMIU_CONFIG'].quietTimeStartMins = 47
        self.tags['BASIC_CMIU_CONFIG'].quietTimeEndMins = 48
        self.tags['BASIC_CMIU_CONFIG'].numAttachedDevices = 49
        self.tags['BASIC_CMIU_CONFIG'].eventMask = 50


        self.tags['INTERVAL_RECORDING_CONFIG'] = CI.S_INTERVAL_RECORDING_CONFIG()
        self.tags['INTERVAL_RECORDING_CONFIG'].deviceNumber = 1
        self.tags['INTERVAL_RECORDING_CONFIG'].intervalFormat = 0
        self.tags['INTERVAL_RECORDING_CONFIG'].recordingIntervalMin = args.recordingInterval
        self.tags['INTERVAL_RECORDING_CONFIG'].reportingIntervalHrs = args.reportingInterval
        self.tags['INTERVAL_RECORDING_CONFIG'].intervalStartTime = 0
        self.tags['INTERVAL_RECORDING_CONFIG'].intervalLastRecordDateTime = long(time.time())


        self.tags['MQTT_PACKET_HEADER'] = CI.S_MQTT_BLE_UART()
        self.tags['MQTT_PACKET_HEADER'].sequenceNumber = 51
        self.tags['MQTT_PACKET_HEADER'].keyInfo = 52
        self.tags['MQTT_PACKET_HEADER'].encryptionMethod = 53
        self.tags['MQTT_PACKET_HEADER'].token = 54


        self.tags['CURRENT_TIME_AND_DATE'] = CI.S_TPBP_TAG()
        self.tags['CURRENT_TIME_AND_DATE'].tagNumber = 36
        self.tags['CURRENT_TIME_AND_DATE'].tagType = CI.E_TAG_TYPE_U64
        self.tags['CURRENT_TIME_AND_DATE'].dataSize = 8
        self.tags['CURRENT_TIME_AND_DATE'].data = ((CI.c_byte*8).
                                                    from_buffer(bytearray([1,2,3,4,5,6,7,8])))


        self.tags['MSISDN_REQUEST'] = CI.S_TPBP_TAG()
        self.tags['MSISDN_REQUEST'].tagNumber = 37
        self.tags['MSISDN_REQUEST'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['MSISDN_REQUEST'].dataSize = 20
        self.tags['MSISDN_REQUEST'].data = ((CI.c_byte*20).
                                        from_buffer(bytearray("MSISDN request dummy", "ascii")))


        self.tags['HARDWARE_REVISION'] = CI.S_IMAGE_VERSION_INFO()
        self.tags['HARDWARE_REVISION'].versionMajorBcd = 55
        self.tags['HARDWARE_REVISION'].versionMinorBcd = 56
        self.tags['HARDWARE_REVISION'].versionYearMonthDayBcd = 57
        self.tags['HARDWARE_REVISION'].versionBuildBcd = args.revisionNumber


        self.tags['FIRMWARE_REVISION'] = CI.S_IMAGE_VERSION_INFO()
        self.tags['FIRMWARE_REVISION'].versionMajorBcd = 59
        self.tags['FIRMWARE_REVISION'].versionMinorBcd = 60
        self.tags['FIRMWARE_REVISION'].versionYearMonthDayBcd = 61
        self.tags['FIRMWARE_REVISION'].versionBuildBcd = args.revisionNumber


        self.tags['BOOTLOADER_REVISION'] = CI.S_IMAGE_VERSION_INFO()
        self.tags['BOOTLOADER_REVISION'].versionMajorBcd = 63
        self.tags['BOOTLOADER_REVISION'].versionMinorBcd = 64
        self.tags['BOOTLOADER_REVISION'].versionYearMonthDayBcd = 65
        self.tags['BOOTLOADER_REVISION'].versionBuildBcd = args.revisionNumber


        self.tags['CONFIG_REVISION'] = CI.S_IMAGE_VERSION_INFO()
        self.tags['CONFIG_REVISION'].versionMajorBcd = 67
        self.tags['CONFIG_REVISION'].versionMinorBcd = 68
        self.tags['CONFIG_REVISION'].versionYearMonthDayBcd = 69
        self.tags['CONFIG_REVISION'].versionBuildBcd = args.revisionNumber

        self.tags['ARB_CONFIG_REVISION'] = CI.S_IMAGE_VERSION_INFO()
        self.tags['ARB_CONFIG_REVISION'].versionMajorBcd = 71
        self.tags['ARB_CONFIG_REVISION'].versionMinorBcd = 72
        self.tags['ARB_CONFIG_REVISION'].versionYearMonthDayBcd = 73
        self.tags['ARB_CONFIG_REVISION'].versionBuildBcd = args.revisionNumber


        self.tags['BLE_CONFIG_REVISION'] = CI.S_IMAGE_VERSION_INFO()
        self.tags['BLE_CONFIG_REVISION'].versionMajorBcd = 75
        self.tags['BLE_CONFIG_REVISION'].versionMinorBcd = 77
        self.tags['BLE_CONFIG_REVISION'].versionYearMonthDayBcd = 78
        self.tags['BLE_CONFIG_REVISION'].versionBuildBcd = args.revisionNumber


        self.tags['ENCRYPTION_CONFIG_REVISION'] = CI.S_IMAGE_VERSION_INFO()
        self.tags['ENCRYPTION_CONFIG_REVISION'].versionMajorBcd = 93
        self.tags['ENCRYPTION_CONFIG_REVISION'].versionMinorBcd = 94
        self.tags['ENCRYPTION_CONFIG_REVISION'].versionYearMonthDayBcd = 95
        self.tags['ENCRYPTION_CONFIG_REVISION'].versionBuildBcd = args.revisionNumber


        self.tags['CMIU_INFORMATION'] = CI.S_CMIU_INFORMATION()
        self.tags['CMIU_INFORMATION'].manufactureDate = long(time.time())
        self.tags['CMIU_INFORMATION'].installationDate = long(time.time())
        self.tags['CMIU_INFORMATION'].dateLastMagSwipe = long(time.time())
        self.tags['CMIU_INFORMATION'].magSwipes = 80
        self.tags['CMIU_INFORMATION'].batteryRemaining = 81
        self.tags['CMIU_INFORMATION'].timeErrorLastNetworkTimeAccess = 82

        self.tags['IMAGE'] = CI.S_TPBP_TAG()
        self.tags['IMAGE'].tagNumber = 46
        self.tags['IMAGE'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['IMAGE'].dataSize = 22
        self.tags['IMAGE'].data = ((CI.c_byte*22).
                                from_buffer(bytearray("dummy image update URL")))


        self.tags['IMAGE_META_DATA'] = CI.S_IMAGE_METADATA()
        self.tags['IMAGE_META_DATA'].imageType = CI.E_IMAGE_TYPE()
        self.tags['IMAGE_META_DATA'].startAddress = 83
        self.tags['IMAGE_META_DATA'].imageSize = 84


        self.tags['RECORDING_AND_REPORTING_INTERVAL'] = CI.S_RECORDING_REPORTING_INTERVAL()
        self.tags['RECORDING_AND_REPORTING_INTERVAL'].reportingStartMins = 85
        self.tags['RECORDING_AND_REPORTING_INTERVAL'].reportingIntervalHours = args.reportingInterval
        self.tags['RECORDING_AND_REPORTING_INTERVAL'].reportingRetries = 87
        self.tags['RECORDING_AND_REPORTING_INTERVAL'].reportingTransmitWindowsMins = 88
        self.tags['RECORDING_AND_REPORTING_INTERVAL'].reportingQuietStartMins = 89
        self.tags['RECORDING_AND_REPORTING_INTERVAL'].reportingQuietEndMins = 90
        self.tags['RECORDING_AND_REPORTING_INTERVAL'].recordingStartMins = 91
        self.tags['RECORDING_AND_REPORTING_INTERVAL'].recordingIntervalMins = args.recordingInterval


        self.tags['TIME_AND_DATE'] = CI.S_TPBP_TAG()
        self.tags['TIME_AND_DATE'].tagNumber = 50
        self.tags['TIME_AND_DATE'].tagType = CI.E_TAG_TYPE_U64
        self.tags['TIME_AND_DATE'].dataSize = 8
        self.tags['TIME_AND_DATE'].data = ((CI.c_byte*8).
                                            from_buffer(bytearray([1,2,3,4,5,6,7,8])))


        self.tags['ERROR'] = CI.S_TPBP_TAG()
        self.tags['ERROR'].tagNumber = 51
        self.tags['ERROR'].tagType = CI.E_TAG_TYPE_U16
        self.tags['ERROR'].dataSize = 2
        self.tags['ERROR'].data = (CI.c_uint16)(0)


        self.tags['EOF'] = CI.S_TPBP_TAG()
        self.tags['EOF'].tagNumber = 255
        self.tags['EOF'].tagType = CI.E_TAG_TYPE_NONE
        self.tags['EOF'].dataSize = 0
        self.tags['EOF'].data = (CI.c_byte)(0) # never read


        self.tags['NEW_IMAGE_VERSION_INFO'] = CI.S_TPBP_TAG()
        self.tags['NEW_IMAGE_VERSION_INFO'].tagNumber = 71
        self.tags['NEW_IMAGE_VERSION_INFO'].tagType = CI.E_TAG_TYPE_CHAR_ARRAY
        self.tags['NEW_IMAGE_VERSION_INFO'].dataSize = 6
        self.tags['NEW_IMAGE_VERSION_INFO'].data = ((CI.c_byte*6).
                                        from_buffer(bytearray("v0.0.0", "ascii")))

        self.tags['PACKET_INSTIGATOR'] = CI.S_TPBP_TAG()
        self.tags['PACKET_INSTIGATOR'].tagNumber = 72
        self.tags['PACKET_INSTIGATOR'].tagType = CI.E_TAG_TYPE_BYTE
        self.tags['PACKET_INSTIGATOR'].dataSize = 1
        self.tags['PACKET_INSTIGATOR'].data = (CI.c_byte)(0)


    '''
    Modifies the CMIUs state data on each MQTT transaction, so that the CMIU
    can produce dynamic data.
    '''
    def generateNextState(self, args):
        self.tags['CMIU_PACKET_HEADER'].timeAndDate = long(time.time())
        self.tags['CMIU_PACKET_HEADER'].cellularRssiAndBer = (random.randrange(0, 31) +
                                                                    256 * random.randrange(0, 7))
        newReading = args.usageData.getReading(0, 15)
        newReading1 = args.usageData.getReading(1, 15)
        newReading2 = args.usageData.getReading(2, 15)
        newReading3 = args.usageData.getReading(3, 15)

        self.tags['R900_INTERVAL_DATA'].dataSize = len(newReading) * 4
        self.tags['R900_INTERVAL_DATA'].data = ((CI.c_byte * len(newReading)).
                                            from_buffer(newReading +
                                                newReading1 +
                                                newReading2 +
                                                newReading3))

        self.tags['INTERVAL_RECORDING_CONFIG'].intervalLastRecordDateTime = int(time.time())

        if args.cvs:
            self.tags['PACKET_INSTIGATOR'].data = (CI.c_byte)(1)
        elif args.ffts:
            self.tags['PACKET_INSTIGATOR'].data = (CI.c_byte)(2)

    '''
    Used to update an image version tag
    '''
    def updateImageVersionTag(self, tagName, tag):
        self.tags[tagName] = tag


    '''
    Used to update the reported modem software version
    '''
    def updateModemSoftwareVersion(self, version):
        self.tags['MODEM_SOFTWARE_REV'].dataSize = len(version)
        self.tags['MODEM_SOFTWARE_REV'].data = ((CI.c_byte*len(version)).
                                        from_buffer(bytearray(version, "ascii")))


    '''
    Set command response type
    '''
    def setCommandResponseType(self, commandType):
        self.tags['COMMAND'].data = ((CI.c_byte).
                                    from_buffer(bytearray([commandType])))


    '''
    Set command response error tag
    '''
    def setCommandResponseSuccessful(self, success):
        if success:
            self.tags['ERROR'].data = (CI.c_uint16)(0)
        else:
            self.tags['ERROR'].data = (CI.c_uint16)(1)
