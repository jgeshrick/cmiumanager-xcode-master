'''
    Neptune Technology Group
    Copyright 2015 as unpublished work.
    All rights reserved

    The information contained herein is confidential
    property of Neptune Technology Group. The use, copying, transfer
    or disclosure of such information is prohibited except by express
    written agreement with Neptune Technology Group.
'''
#Example script

import CmiuSimManager as sim

result = sim.runSimulator("mqtttestubuntu.cloudapp.net", updatePeriod = 0.1, numberOfCmius = 100, updateRepeat = 1, expectedReceivePublishes = 0)

print "average duration  : " + str(result.stats.avgDuration) + "ms"
print "duration range    : " + str(result.stats.rangeDuration) + "ms"


