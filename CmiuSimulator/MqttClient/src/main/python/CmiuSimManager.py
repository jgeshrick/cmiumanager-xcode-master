'''
    Neptune Technology Group
    Copyright 2015 as unpublished work.
    All rights reserved

    The information contained herein is confidential
    property of Neptune Technology Group. The use, copying, transfer
    or disclosure of such information is prohibited except by express
    written agreement with Neptune Technology Group.
'''

import sys
import argparse

import CmiuSimScheduler
import CmiuSimStatistics
import CmiuSimUsageDataGenerator

from CmiuSimWrapper import Wrapper

codeVersion = "${project.version}"

def main(argv):
    versionNo = "${project.version}"
    versionNo = versionNo.replace("[", "").replace("]", "")
    
    print "\n"
    print "##########################################################"
    print "##########################################################"
    print "#####                                                #####"
    print "#####    CMIU Simulator                              #####"
    print "#####                                                #####"
    print "#####    Version:  " + versionNo + str(' ' * (34-len(versionNo))) + "#####"
    print "#####                                                #####"
    print "#####                                                #####"
    print "#####    By WJD1                                     #####"
    print "#####                                                #####"
    print "##########################################################"
    print "##########################################################"
    print "\n"    
    
    packetTypeInfo =  "Packet Types Available: " + ", ".join([pt for pt in Wrapper.packetDefs]) 
    
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     epilog=packetTypeInfo)
    
    #Add required positional arguments
    parser.add_argument("brokerUrl", help="The IP Address or URL of the MQTT broker")
    
    #Add optional arguments
    parser.add_argument("-s", "--cmiuStartNumber", 
                        help="Starting CMIU ID number", type=int, default=500000000)
    parser.add_argument("-n", "--numberOfCmius", 
                        help="The number of CMIUs to simulate", type=int, default=1)
    parser.add_argument("-u", "--updatePeriod", 
                        help="MQTT update interval in minutes, can be fractional", type=float, default=1)
    parser.add_argument("-p", "--packetType", 
                        help="The packet type to send during a MQTT update", default="detailedConfigPacket")
    parser.add_argument("-r", "--updateRepeat",
                        help="How many times each CMIU should send a MQTT update, 0 for no limit", type=int, default=0)
    parser.add_argument("-v", "--verbose",
                        help="Display detailed log messages", action="store_true")
    parser.add_argument("-q", "--quiet",
                        help="Do not display log messages", action="store_true")
    parser.add_argument("-e", "--expectedReceivePublishes", type=int, default=0,
                        help="Maximum number of publishes we expect to receive")
    parser.add_argument("-R", "--recordResult",
                        help="Enable writing to results file", action="store_true")
    parser.add_argument("-N", "--revisionNumber",
                        help="set a value in the revision information", type=int, default=1)
    parser.add_argument("-f", "--commandNAck",
                        help="All Command received are responded to with a NAck", action="store_true")
    parser.add_argument("-F", "--commandFailure",
                        help="All commands received fail", action="store_true")
    parser.add_argument("-cvs", "--cvs",
                        help="Set packet instigator type to CSV", action="store_true")
    parser.add_argument("-ffts", "--ffts",
                        help="Set packet instigator type to FFTS", action="store_true")
    
    parser.add_argument("--recordingInterval",
                        help="Set recording interval", type=int, default=15)
    parser.add_argument("--reportingInterval",
                        help="Set reporting interval", type=int, default=60)
    parser.add_argument("--dumpfile", "--dumpfile",
                        help="dump packets to file in hex", nargs='?', type=argparse.FileType('w'), default=0)

    args = parser.parse_args()
    
    sys.stdout.flush()
    
    takeInArgumentsAndRun(args)


def runSimulator(brokerUrl, cmiuStartNumber = 500000000, numberOfCmius = 1, updatePeriod = 1,
                 packetType = "detailedConfigPacket", updateRepeat = 0, verbose = False,
                 quiet = False, expectedReceivePublishes = 0, revisionNumber = 1, recordingInterval = 15,
                 reportingInterval = 60):
    args = type('', (), {})()
    args.brokerUrl = brokerUrl
    args.cmiuStartNumber = cmiuStartNumber
    args.numberOfCmius = numberOfCmius
    args.updatePeriod = updatePeriod
    args.packetType = packetType
    args.updateRepeat = updateRepeat
    args.verbose = verbose
    args.quiet = quiet
    args.expectedReceivePublishes = expectedReceivePublishes
    args.revisionNumber = revisionNumber
    args.recordingInterval = recordingInterval
    args.reportingInterval = reportingInterval

    return takeInArgumentsAndRun(args)


def takeInArgumentsAndRun(args):
    args.usageData = CmiuSimUsageDataGenerator.UsageData("R900Readings.txt")
    args.updatePeriod = args.updatePeriod * 60.0
    if args.updatePeriod < 0.1:
        args.updatePeriod = 0.1
    args.result = CmiuSimStatistics.Results()
    CmiuSimScheduler.startSim(args)
    args.result.processResults()
    print "total CONNACKS: " + str(args.result.stats.totalConnAcks)
    print "total PUBS:     " + str(args.result.stats.totalPubs)
    print "total PUBACKS:  " + str(args.result.stats.totalPubAcks)
    if args.recordResult:
        args.result.saveResultsToFile("CmiuSimulatorResults.csv")
    return args.result

    
if __name__ == "__main__":
    main(sys.argv[1:])
    

