'''
    Neptune Technology Group
    Copyright 2015 as unpublished work.
    All rights reserved

    The information contained herein is confidential
    property of Neptune Technology Group. The use, copying, transfer
    or disclosure of such information is prohibited except by express
    written agreement with Neptune Technology Group.
'''

import threading
import sys
import random
import time

sys.path.insert(1,'../../lib/paho-mqtt-1.1/src/paho/mqtt')

import CmiuSimDataGenerator
import CmiuSimConnectionHandler


'''
The CMIU class contains all the information about the state of 
a CMIU, and also manages CMIU MQTT transactions. 

If you create an object from the CMIU class, say myCMIU, then the state 
information about myCMIU can be generated, and modified, such as the current
time and date. The myCMIU object can then be used to perform a transaction with
a MQTT broker, as if it were a CMIU.
'''

codeVersion = "${project.version}"

class CMIU:     
    def __init__(self, ID, args):
        #Initialise CMIU class scope variables
        self.ID = ID
        self.args = args
        self.updateTime = random.randrange(0, 1000*args.updatePeriod)/1000.0
        self.firstConnection = True
        self.counter = 0

        #Initialise CMIU data
        self.cmiuData = CmiuSimDataGenerator.CmiuData(self.ID, args)

    '''
    Starts an MQTT transaction with the CMIU object in a new thread
    '''
    def DoMqttTransaction(self):
        t = threading.Thread(target=self.MqttTransaction)
        t.daemon = True
        t.start()
        
    '''
    Performs a MQTT transaction including generating new data, building
    packets, and running the MQTT transaction itself.
    '''    
    def MqttTransaction(self):
        connection = CmiuSimConnectionHandler.ConnectionHandler(self.cmiuData, 
                                                                self.args, 
                                                                self.ID)
        sessionResult = connection.runConnection()

        self.counter += 1
        if self.counter == self.args.usageData.getNumberOfReadings()/4:
            self.counter = 0
        
        sessionResult.cmiu = self.ID
        self.args.result.rawSessionResults.append(sessionResult)


    
