'''
    Neptune Technology Group
    Copyright 2015 as unpublished work.
    All rights reserved

    The information contained herein is confidential
    property of Neptune Technology Group. The use, copying, transfer
    or disclosure of such information is prohibited except by express
    written agreement with Neptune Technology Group.
'''

import CmiuSimTsmCInterface as TsmCI
from collections import defaultdict

'''
This file contains function used to calculate statistics 
from a list of CMIU objects
'''

codeVersion = "0.12.160229.676"

class Results:
    def __init__(self):
        #simulator appends to this list for each MQTT transaction
        self.rawSessionResults = []
        self.results = defaultdict (list)

    def processResults(self):
        processedResults = []
        for i in range(len(self.rawSessionResults)):
            processedResults.append(Result(self.rawSessionResults[i]))
        for result in processedResults:
            self.results[result.cmiu].append(result)

        self.stats = Stats(self.results)

    def saveResultsToFile(self, filename):
        resultsFile = open(filename, 'w')
        for cmiuId in self.results:
            cmiuResults = self.results[cmiuId]
            for result in cmiuResults:
                if (result.receivedPUBACK > 0):
                    resultsFile.write(str(result.cmiu))
                    resultsFile.write(",")
                    resultsFile.write(str(result.epoch))
                    resultsFile.write("\n")
        resultsFile.close()
	


class Stats:
    def __init__(self, results):
        self.totalPubs = 0
        self.totalPubAcks = 0
        self.totalRecvPubs = 0
        self.totalConnAcks = 0
        self.avgDuration = 0

        durations = []
        counter = 0

        for cmiuNo in results:
            cmiu = results[cmiuNo] 
            for session in cmiu:
                counter += 1
                self.totalPubs += session.sentPUBLISH
                self.totalPubAcks += session.receivedPUBACK
                self.totalRecvPubs += session.receivedPUBLISH
                self.totalConnAcks += session.receivedCONNACK
                self.avgDuration += session.duration
                durations.append(session.duration)

        self.avgDuration /= counter
        self.rangeDuration = max(durations) - min(durations)


class Result:
    def __init__(self, sessionResult):
        self.sentCONNECT = sessionResult.sentCounts[TsmCI.CONNECT]
        self.sentCONNACK = sessionResult.sentCounts[TsmCI.CONNACK]
        self.sentPUBLISH = sessionResult.sentCounts[TsmCI.PUBLISH]
        self.sentPUBACK = sessionResult.sentCounts[TsmCI.PUBACK] 
        self.sentPUBREC = sessionResult.sentCounts[TsmCI.PUBREC]
        self.sentPUBREL = sessionResult.sentCounts[TsmCI.PUBREL]
        self.sentPUBCOMP = sessionResult.sentCounts[TsmCI.PUBCOMP]
        self.sentSUBSCRIBE = sessionResult.sentCounts[TsmCI.SUBSCRIBE]
        self.sentSUBACK = sessionResult.sentCounts[TsmCI.SUBACK]
        self.sentUNSUBSCRIBE = sessionResult.sentCounts[TsmCI.UNSUBSCRIBE]
        self.sentUNSUBACK = sessionResult.sentCounts[TsmCI.UNSUBACK]
        self.sentPINGREQ = sessionResult.sentCounts[TsmCI.PINGREQ]
        self.sentPINGRESP = sessionResult.sentCounts[TsmCI.PINGRESP]
        self.sentDISCONNECT = sessionResult.sentCounts[TsmCI.DISCONNECT]

        self.receivedCONNECT = sessionResult.receivedCounts[TsmCI.CONNECT]
        self.receivedCONNACK = sessionResult.receivedCounts[TsmCI.CONNACK]
        self.receivedPUBLISH = sessionResult.receivedCounts[TsmCI.PUBLISH]
        self.receivedPUBACK = sessionResult.receivedCounts[TsmCI.PUBACK] 
        self.receivedPUBREC = sessionResult.receivedCounts[TsmCI.PUBREC]
        self.receivedPUBREL = sessionResult.receivedCounts[TsmCI.PUBREL]
        self.receivedPUBCOMP = sessionResult.receivedCounts[TsmCI.PUBCOMP]
        self.receivedSUBSCRIBE = sessionResult.receivedCounts[TsmCI.SUBSCRIBE]
        self.receivedSUBACK = sessionResult.receivedCounts[TsmCI.SUBACK]
        self.receivedUNSUBSCRIBE = sessionResult.receivedCounts[TsmCI.UNSUBSCRIBE]
        self.receivedUNSUBACK = sessionResult.receivedCounts[TsmCI.UNSUBACK]
        self.receivedPINGREQ = sessionResult.receivedCounts[TsmCI.PINGREQ]
        self.receivedPINGRESP = sessionResult.receivedCounts[TsmCI.PINGRESP]
        self.receivedDISCONNECT = sessionResult.receivedCounts[TsmCI.DISCONNECT]

        self.cmiu = sessionResult.cmiu
        self.duration = sessionResult.duration
        self.epoch = sessionResult.epoch
