'''
    Neptune Technology Group
    Copyright 2015 as unpublished work.
    All rights reserved

    The information contained herein is confidential
    property of Neptune Technology Group. The use, copying, transfer
    or disclosure of such information is prohibited except by express
    written agreement with Neptune Technology Group.
'''

import CmiuSimWrapper
import re
import sys
import threading
import time
from ftplib import FTP

printLock = threading.Lock()

class ConnectionHandler:
    def __init__(self, cmiuData, args, ID):
        self.cmiuData = cmiuData
        self.args = args
        self.ID = ID

    def runConnection(self):
        self.cmiuData.generateNextState(self.args)

        mqttSession = CmiuSimWrapper.Wrapper()
        mqttSession.AddPacket(self.cmiuData, self.args, self.args.packetType)
        sessionResults = mqttSession.DoMqttTransaction(self.ID, self.args)

        if sessionResults.errorCode != 0:
            printLock.acquire()
            print "\nerror code: " + str(sessionResults.errorCode)
            print "retrying publish for CMIU: " + str(self.ID) + "\n"
            printLock.release()
            mqttSession = CmiuSimWrapper.Wrapper()
            mqttSession.AddPacket(self.cmiuData, self.args, self.args.packetType)
            sessionResults = mqttSession.DoMqttTransaction(self.ID, self.args)

        while 1:
            packet = mqttSession.GetPacket(self.cmiuData)
            if packet is None:
                break
            else:
                tags = packet[1]
                if packet[0] == 'commandPacket':  # If image update command received
                    if tags['COMMAND'].data[0] == 0:
                        url = str(bytearray(tags['IMAGE'].data))
                        matches = re.match("https?://([^/]*)(.*)", url)
                        print "\n\nCMIU " + str(self.ID) + " >> DOWNLOAD IMAGE COMMAND RECEIVED:"
                        print "\n"
                        print "          Server: " + matches.group(1)
                        print "            File: " + matches.group(2)
                        print "   Start Address: " + str(format(tags['IMAGE_META_DATA'].startAddress, '02x')) + " bytes"
                        print "      Image Size: " + str(format(tags['IMAGE_META_DATA'].imageSize, '02x')) + " bytes"
                        print "   Major Version: " + str(format(tags['FIRMWARE_REVISION'].versionMajorBcd, '02x'))
                        print "   Minor Version: " + str(format(tags['FIRMWARE_REVISION'].versionMinorBcd, '02x'))
                        print "    Version Date: " + str(format(tags['FIRMWARE_REVISION'].versionYearMonthDayBcd, '02x'))
                        print "   Build Version: " + str(format(tags['FIRMWARE_REVISION'].versionBuildBcd, '02x'))
                        print "\n\n"

                        if self.args.commandNAck:
                            mqttSession = CmiuSimWrapper.Wrapper()
                            self.cmiuData.setCommandResponseType(0)
                            self.cmiuData.setCommandResponseSuccessful(False)
                            mqttSession.AddPacket(self.cmiuData, self.args, 'commandResponsePacket')
                            mqttSession.DoMqttTransaction(self.ID, self.args)
                        else:
                            mqttSession = CmiuSimWrapper.Wrapper()
                            self.cmiuData.setCommandResponseType(0)
                            self.cmiuData.setCommandResponseSuccessful(True)
                            mqttSession.AddPacket(self.cmiuData, self.args, 'commandResponsePacket')
                            mqttSession.DoMqttTransaction(self.ID, self.args)

                            httpGet = CmiuSimWrapper.Wrapper()
                            httpGet.DoHttpGet(self.args, self.ID, matches.group(1), matches.group(2))

                            time.sleep(5)
                            mqttSession = CmiuSimWrapper.Wrapper()
                            if not self.args.commandFailure:
                                self.cmiuData.updateImageVersionTag('FIRMWARE_REVISION', tags['FIRMWARE_REVISION'])
                            mqttSession.AddPacket(self.cmiuData, self.args, 'detailedConfigPacket')
                            mqttSession.DoMqttTransaction(self.ID, self.args)

                    elif tags['COMMAND'].data[0] == 32:  # If FOTA command received
                        # Get the file using FOTA
                        print "\n\nCMIU " + str(self.ID) + " >> PERFORM MODEM FOTA COMMAND RECEIVED"
                        print "\n"
                        print "          Server: " + str(bytearray(tags['MODULE_FOTA_FTP_URL'].data).decode("ascii"))
                        print "            File: " + str(bytearray(tags['IMAGE'].data).decode("ascii"))
                        print "    FTP Username: " + str(bytearray(tags['MODULE_FOTA_FTP_USERNAME'].data).decode("ascii"))
                        print "    FTP Password: " + str(bytearray(tags['MODULE_FOTA_FTP_PASSWORD'].data).decode("ascii"))
                        print "  Expected Image: " + str(bytearray(tags['NEW_IMAGE_VERSION_INFO'].data).decode("ascii"))
                        print "\n\n"

                        if self.args.commandNAck:
                            mqttSession = CmiuSimWrapper.Wrapper()
                            self.cmiuData.setCommandResponseType(32)
                            self.cmiuData.setCommandResponseSuccessful(False)
                            mqttSession.AddPacket(self.cmiuData, self.args, 'commandResponsePacket')
                            mqttSession.DoMqttTransaction(self.ID, self.args)
                        else:
                            mqttSession = CmiuSimWrapper.Wrapper()
                            self.cmiuData.setCommandResponseType(32)
                            self.cmiuData.setCommandResponseSuccessful(True)
                            mqttSession.AddPacket(self.cmiuData, self.args, 'commandResponsePacket')
                            mqttSession.DoMqttTransaction(self.ID, self.args)

                            ftp = FTP(str(bytearray(tags['MODULE_FOTA_FTP_URL'].data).decode("ascii")),
                                    (str(bytearray(tags['MODULE_FOTA_FTP_USERNAME'].data)).decode("ascii")),
                                    (str(bytearray(tags['MODULE_FOTA_FTP_PASSWORD'].data)).decode("ascii")))
                            ftp.retrbinary("RETR " + str(bytearray(tags['IMAGE'].data).decode("ascii")),
                                    open(str(bytearray(tags['IMAGE'].data).decode("ascii")), 'wb').write)
                            ftp.quit()

                            time.sleep(5)
                            mqttSession = CmiuSimWrapper.Wrapper()
                            if not self.args.commandFailure:
                                self.cmiuData.updateModemSoftwareVersion(
                                        str(bytearray(tags['NEW_IMAGE_VERSION_INFO'].data).decode("ascii")))
                            mqttSession.AddPacket(self.cmiuData, self.args, 'detailedConfigPacket')
                            mqttSession.DoMqttTransaction(self.ID, self.args)
                        
                    else:
                        print "\n\nReceived other commands: command id = " + str(tags['COMMAND'].data[0])
                        print "\n\n"
                        
                    sys.stdout.flush()
                        
        return sessionResults
