'''
    Neptune Technology Group
    Copyright 2015 as unpublished work.
    All rights reserved

    The information contained herein is confidential
    property of Neptune Technology Group. The use, copying, transfer
    or disclosure of such information is prohibited except by express
    written agreement with Neptune Technology Group.
'''

import copy
import time 
import sys
import traceback
import threading
import binascii
sys.path.insert(1, '../../../target')

import CmiuSimCInterface as CI
import CmiuSimTsmCInterface as TsmCI


'''
This class wraps up the tagged packet builder parser C code functionality, so 
that whole packets can be easily built.

By passing a cmiuData object and the name of a packet (such as 
"detailed_config") to BuildPacket() the buffer is populated with the tagged 
data for the specified packet.
'''

printLock = threading.Lock()

codeVersion = "0.7.150922.414"

def EnableTsmDebugLog(enable):
    TsmCI.EnableDebugLog(enable)


class Wrapper:
    packetDefs = {'detailedConfigPacket':[1,254,26,33,34,23,27,30,31,44,38,39,40,41,42,43,3,4,5,6,7,8,15,16,17,18,19,37,20,21,22,24,25,72,254,255],
                  'revisionTest_missingRevs':[1,254,26,33,34,23,27,30,31,44,38,39,42,43,3,4,5,6,7,8,15,16,17,18,19,37,20,21,22,24,25,254,255], # Missing bootloader and config revision
                  'basicConfigPacket':[1,254,26,33,34,23,27,30,31,254,255],
                  'intervalDataPacket':[1, 254, 26, 27, 34, 28, 254, 255],  
                  'commandResponsePacket':[1,254,32,51,254,255],
                  'testPacket':[1,2],
                  'justPacketHeader':[1],
                  'justConfiguration':[2]}

    packetEnums = {'detailedConfigPacket':CI.E_PACKET_TYPE_DETAILED_CONFIG,
                   'revisionTest_missingRevs':CI.E_PACKET_TYPE_DETAILED_CONFIG,
                   'intervalDataPacket':CI.E_PACKET_TYPE_INTERVAL_DATA,
                   'basicConfigPacket':CI.E_PACKET_TYPE_BASIC_CONFIG,
                   'eventPacket':CI.E_PACKET_TYPE_EVENT,
                   'timePacket':CI.E_PACKET_TYPE_TIME,
                   'commandPacket':CI.E_PACKET_TYPE_COMMAND,
                   'commandResponsePacket':CI.E_PACKET_TYPE_RESPONSE,
                   'testPacket':200,
                   'justPacketHeader':201,
                   'justConfiguration':202}

    tagRefs    = {1:'CMIU_PACKET_HEADER',
                  2:'CMIU_CONFIGURATION',
                  3:'NETWORK_OPERATORS',
                  4:'NETWORK_PERFORMANCE',
                  5:'REGISTRATION_STATUS',
                  6:'MQTT_BROKER_ADDRESS',
                  7:'FALLBACK_MQTT_BROKER_ADDRESS',
                  8:'ASSIGNED_CMIU_IP_ADDRESS',
                  9:'MODULE_FOTA_FTP_URL',
                  10:'MODULE_FOTA_FTP_PORT',
                  11:'MODULE_FOTA_FTP_USERNAME',
                  12:'MODULE_FOTA_FTP_PASSWORD',
                  13:'MODULE_FOTA_USER_ID',
                  14:'MODULE_FOTA_FTP_PASSWORD',
                  15:'MODEM_HARDWARE_REVISION',
                  16:'MODEM_MODEL_ID',
                  17:'MODEM_MANUFACTURER_ID',
                  18:'MODEM_SOFTWARE_REV',
                  19:'MODEM_SERIAL_NUMBER',
                  20:'SIM_IMSI',
                  21:'SIM_ID',
                  22:'PIN_PUK_DEVICE_STATUS',
                  23:'CMIU_HW_DIAGNOSTICS',
                  24:'BLE_LAST_USER_ID',
                  25:'BLE_LAST_USER_DATE',
                  26:'CMIU_STATUS',
                  27:'REPORTED_DEVICE_CONFIG',
                  28:'R900_INTERVAL_DATA',
                  29:'EVENT',
                  30:'ERROR_LOG',
                  31:'CONNECTION_TIMING_LOG',
                  32:'COMMAND',
                  33:'BASIC_CMIU_CONFIG',
                  34:'INTERVAL_RECORDING_CONFIG',
                  35:'MQTT_PACKET_HEADER',
                  36:'CURRENT_TIME_AND_DATE',
                  37:'MSISDN_REQUEST',
                  38:'HARDWARE_REVISION',
                  39:'FIRMWARE_REVISION',
                  40:'BOOTLOADER_REVISION',
                  41:'CONFIG_REVISION',
                  42:'ARB_CONFIG_REVISION',
                  43:'BLE_CONFIG_REVISION', 
                  44:'CMIU_INFORMATION',
                  46:'IMAGE',
                  47:'IMAGE_META_DATA',
                  48:'RECORDING_AND_REPORTING_INTERVAL',
                  49:'ENCRYPTION_CONFIG_REVISION',
                  50:'TIME_AND_DATE',
                  51:'ERROR',
                  71:'NEW_IMAGE_VERSION_INFO',
                  72:'PACKET_INSTIGATOR',
                  254:'SECURE_DATA_BLOCK',
                  255:'EOF'}
             
    #For custom packers and parsers         
    packerRefs = {'CMIU_PACKET_HEADER':CI.Tpbp_PackerAdd_CmiuPacketHeader,
                  'CMIU_CONFIGURATION':CI.Tpbp_PackerAdd_CmiuConfiguration,
                  'CMIU_STATUS':CI.Tpbp_PackerAdd_CmiuStatus,
                  'INTERVAL_RECORDING_CONFIG':CI.Tpbp_PackerAdd_IntervalRecordingConfig,
                  'BASIC_CMIU_CONFIG':CI.Tpbp_PackerAdd_CmiuBasicConfiguration,
                  'MQTT_PACKET_HEADER':CI.Tpbp_PackerAdd_UartPacketHeader,
                  'HARDWARE_REVISION':CI.Tpbp_PackerAdd_ImageVersionInfoHardware,
                  'FIRMWARE_REVISION':CI.Tpbp_PackerAdd_ImageVersionInfoFirmware,
                  'BOOTLOADER_REVISION':CI.Tpbp_PackerAdd_ImageVersionInfoBootloader,
                  'CONFIG_REVISION':CI.Tpbp_PackerAdd_ImageVersionInfoConfig,
                  'ARB_CONFIG_REVISION':CI.Tpbp_PackerAdd_ImageVersionInfoArb,
                  'BLE_CONFIG_REVISION':CI.Tpbp_PackerAdd_ImageVersionInfoBle,
                  'ENCRYPTION_CONFIG_REVISION':CI.Tpbp_PackerAdd_ImageVersionInfoEncryption,
                  'CMIU_INFORMATION':CI.Tpbp_PackerAdd_CmiuInformation,
                  'IMAGE_META_DATA':CI.Tpbp_PackerAdd_ImageMetadata,
                  'RECORDING_AND_REPORTING_INTERVAL':CI.Tpbp_PackerAdd_RecordingReportingInterval,
                  'REPORTED_DEVICE_CONFIG':CI.Tpbp_PackerAdd_ReportedDeviceConfig}
                
    parserRefs = {'CMIU_PACKET_HEADER':CI.Tpbp_ParserRead_CmiuPacketHeader,
                  'CMIU_CONFIGURATION':CI.Tpbp_ParserRead_CmiuConfiguration,
                  'CMIU_STATUS':CI.Tpbp_ParserRead_CmiuStatus,
                  'INTERVAL_RECORDING_CONFIG':CI.Tpbp_ParserRead_IntervalRecordingConfig,
                  'BASIC_CMIU_CONFIG':CI.Tpbp_ParserRead_CmiuBasicConfiguration,
                  'MQTT_PACKET_HEADER':CI.Tpbp_ParserRead_UartPacketHeader,
                  'HARDWARE_REVISION':CI.Tpbp_ParserRead_ImageVersionInfo,
                  'FIRMWARE_REVISION':CI.Tpbp_ParserRead_ImageVersionInfo,
                  'BOOTLOADER_REVISION':CI.Tpbp_ParserRead_ImageVersionInfo,
                  'CONFIG_REVISION':CI.Tpbp_ParserRead_ImageVersionInfo,
                  'ARB_CONFIG_REVISION':CI.Tpbp_ParserRead_ImageVersionInfo,
                  'BLE_CONFIG_REVISION':CI.Tpbp_ParserRead_ImageVersionInfo,
                  'ENCRYPTION_CONFIG_REVISION':CI.Tpbp_ParserRead_ImageVersionInfo,
                  'CMIU_INFORMATION':CI.Tpbp_ParserRead_CmiuInformation,
                  'IMAGE_META_DATA':CI.Tpbp_ParserRead_ImageMetadata,
                  'RECORDING_AND_REPORTING_INTERVAL':CI.Tpbp_ParserRead_RecordingReportingInterval,
                  'REPORTED_DEVICE_CONFIG':CI.Tpbp_ParserRead_ReportedDeviceConfig}

    def __init__(self):
        #Initialise MQTT session packers and parsers
        self.packers = list()
        self.parsers = list()
        for count in range(4):
            self.packers.append(Packer())
            self.parsers.append(Parser())

        self.packetCounter = 0
        self.parserCounter = 0


    def AddPacket(self, cmiuData, args, packetType):
        packer = self.packers[self.packetCounter]
        packetBuffer = self.packers[self.packetCounter].pBuffer
        self.packetCounter += 1

        tagNumbers = self.packetDefs[packetType]
        CI.Tpbp_PackerReset(CI.byref(packer.pPacker))

        #Set packet type
        packetBuffer[0] = self.packetEnums[packetType]
        packer.pPacker.writePosition = 1

        tagIndex = 0
        while tagIndex < len(tagNumbers):
            #Go through each tag number and add to buffer
            tagNum = tagNumbers[tagIndex]            
            tagName = self.tagRefs[tagNum]

            if tagName == 'SECURE_DATA_BLOCK':
                securePacker = Packer()
                tagIndex += 1
                while self.tagRefs[tagNumbers[tagIndex]] != 'SECURE_DATA_BLOCK':
                    tagNum = tagNumbers[tagIndex]
                    tagName = self.tagRefs[tagNum]
                    self.AddTagToBuffer(cmiuData, securePacker, tagName)
                    tagIndex += 1
                while securePacker.pPacker.writePosition % 16 != 0:
                    securePacker.pBuffer[securePacker.pPacker.writePosition] = 0
                    securePacker.pPacker.writePosition += 1                              
                CI.Tpbp_PackerAdd_SecureBlockArray(CI.byref(packer.pPacker),
                                                   CI.byref(securePacker.pBuffer),
                                                    securePacker.pPacker.writePosition)               
            else:
                self.AddTagToBuffer(cmiuData, packer, tagName)
            tagIndex += 1

        if args.dumpfile:
            printLock.acquire()
            #output packet in hex

            with open(args.dumpfile.name, "a") as myfile:
                myfile.write("CMIU: ")
                myfile.write(str(cmiuData.tags['CMIU_PACKET_HEADER'].cmiuId))
                myfile.write(", ")
                myfile.write(str(cmiuData.tags['CMIU_PACKET_HEADER'].timeAndDate))
                myfile.write("\n")
                for s in range(0, packer.pPacker.writePosition, 2):
                    e = s + 2
                    if e > packer.pPacker.writePosition:
                        e = packer.pPacker.writePosition
                    myfile.write(binascii.hexlify("".join(map(chr, packetBuffer[s:e]))))
                    myfile.write(" ")
                    if 0 == (e % 16 ):
                        myfile.write("\n")
                else:
                    if e % 16:
                        myfile.write("\n")
                myfile.write("\n")
                myfile.write(binascii.b2a_base64("".join(map(chr,packetBuffer[0:packer.pPacker.writePosition]))))
                myfile.write("\n")
            printLock.release()


    def AddTagToBuffer(self, cmiuData, packer, tagName):
        if tagName in self.packerRefs:
            self.packerRefs[tagName](CI.byref(packer.pPacker), 
                                     CI.byref(cmiuData.tags[tagName]))
        else:
            CI.Tpbp_PackerTagHelper(CI.byref(packer.pPacker),
                                    CI.byref(cmiuData.tags[tagName]))
            CI.Tpbp_PackerAddBytes(CI.byref(packer.pPacker),
                                   CI.byref(cmiuData.tags[tagName].data),
                                   cmiuData.tags[tagName].dataSize)


    '''
    Parses a packet from the packet buffer. 
    It uses the Tagged Packet Builder Parser C code.
    '''                     
    def GetPacket(self, cmiuData):
        self.parserCounter -= 1
        if self.parserCounter < 0:
            return None

        parser = self.parsers[self.parserCounter]
        tag = CI.S_TPBP_TAG()
        receivedTags = {}

        #Get packet type
        packetType = (key for key,value in self.packetEnums.items() if value==parser.pBuffer[0]).next()
        parser.pParser.readPosition = parser.pParser.readPosition + 1        
        
        while(CI.Tpbp_ParserReadTag(CI.byref(parser.pParser), CI.byref(tag))):
            if tag.tagNumber not in self.tagRefs:
                break
            
            tagName = self.tagRefs[tag.tagNumber]     
            if tagName != 'SECURE_DATA_BLOCK':
                tagData = copy.copy(cmiuData.tags[tagName])
                if tagName in self.parserRefs:
                    self.parserRefs[tagName](CI.byref(parser.pParser), CI.byref(tagData))
                else:
                    tagData.dataSize = tag.dataSize
                    tagData.data = (CI.c_byte * tag.dataSize)()
                    CI.Tpbp_ParserGetBytes(CI.byref(parser.pParser),
                                          CI.byref(tagData.data),
                                          tag.dataSize)
                                            
                receivedTags[tagName] = tagData
            
        return (packetType, receivedTags)
        

    '''
    Callback which takes a pointer to a struct and extracts the payload data
    into the packet buffer.
    '''    
    def PublishReceivedCallback(self, received):
        if (self.parserCounter > 3):
            printLock.acquire()
            print "Received MQTT publishes exceeded 4"
            printLock.release()
        else:
            for i in range(received.contents.dataLength):
                self.parsers[self.parserCounter].pBuffer[i] = received.contents.pData[i]
            self.parserCounter += 1


    '''
    Uses the TCP Session Manager C code to perform a full MQTT transaction, 
    from sending a packet in the packet buffer, to receiving packets and placing
    them into the packet buffer, ready for parsing.
    '''    
    def DoMqttTransaction(self, ID, args):
        #Create and fill in all needed structs for managing MQTT
        startTime = time.clock()

        mqttManager = TsmCI.MQTT_MANAGER()
        mqttManagerInitParams = TsmCI.MQTT_MANAGER_INIT_PARAMS()
        mqttManagerInitParams.pfOnPublishRxCallback = TsmCI.pfOnPublishMessageReceivedCallback(self.PublishReceivedCallback)
        
        #Create and fill in all needed structs for managing a MQTT session
        sessionManager = TsmCI.TCP_SESSION_MANAGER()
        sessionParams = TsmCI.MQTT_MANAGER_SESSION_PARAMS()
        sessionParams.mqttVersion = TsmCI.MQTT_VERSION_3_1_1
        sessionParams.clientID = TsmCI.String("CMIU/" + str(ID))
        sessionParams.keepAliveInterval = 20
        sessionParams.cleansession = 0
        sessionParams.subscriptionCount = 0
        sessionParams.pubToSendCount = 0
        sessionParams.timeoutForWaitMs = TsmCI.DEFAULT_MQTT_SESSION_TIMEOUT_MS
        sessionParams.expectedPubsToReceive = args.expectedReceivePublishes
        sessionResults = TsmCI.MQTT_MANAGER_SESSION_RESULTS()
        
        #Create and fill in all needed structs for managing the TCP connection
        socketWin = TsmCI.SOCKET_WIN()
        transportManager = TsmCI.TRANSPORT_MANAGER()
        connectionParams = TsmCI.CONNECTION_PARAMS()
        connectionParams.host = TsmCI.String(args.brokerUrl)
        connectionParams.port = 1883
        connectionParams.modemType = 0 #Winsock
        
        #Create and fill in all needed structs for MQTT pubs/subs
        commandsSub = TsmCI.MQTT_MANAGER_SUBSCRIPTION()
        commandsSub.topicFilter = TsmCI.String("CMIU/Command/" + str(ID))
        commandsSub.qos = 1
        reportPubs = list()
        for count in range(self.packetCounter):
            reportPub = TsmCI.MQTT_MANAGER_PUB()
            reportPub.topic = TsmCI.String("CMIU/Report/" + str(ID))
            reportPub.pData = self.packers[count].pBuffer
            reportPub.qos = TsmCI.E_MQTT_QOS_1
            reportPub.dataLength = self.packers[count].pPacker.writePosition
            reportPubs.append(reportPub)
        
        #Initialise the MQTT Manager / MQTT stack / MQTT Sub / MQTT Pub
        TsmCI.MqttManagerInit(TsmCI.byref(mqttManager),
                          TsmCI.byref(mqttManagerInitParams),
                          TsmCI.byref(sessionParams),
                          TsmCI.byref(sessionResults))
                            
        TsmCI.MqttManagerAddSubscription(TsmCI.byref(sessionParams),
                            TsmCI.byref(commandsSub))
        for count in range(self.packetCounter):
            TsmCI.MqttManagerAddPublish(TsmCI.byref(sessionParams),
                            TsmCI.byref(reportPubs[count]))
                

        #Initialises socket win and adds pointer to transportManager
        TsmCI.MqttInitForPython(TsmCI.byref(transportManager),
                            TsmCI.byref(socketWin))

        #Initialise TcpSessionManager
        TsmCI.TcpSessionManagerInit(TsmCI.byref(sessionManager),
                                     TsmCI.byref(transportManager),
                                     TsmCI.byref(connectionParams))
        
        # Begin to run the Mqtt Manager object within the TcpSessionManager
        TsmCI.MqttBeginSessionForPython(TsmCI.byref(sessionManager),
                             TsmCI.byref(mqttManager))
        
        buf = (TsmCI.c_char * 256)()                           
                             
        #Run the session until not busy
        TsmCI.TcpSessionManagerTick(TsmCI.byref(sessionManager), 10)
        while(TsmCI.TcpSessionManagerIsBusy(TsmCI.byref(sessionManager))):
            if (time.clock() - startTime) > 30:
                print "Connection timed out"
                break
            time.sleep(0.01)
            try:
                TsmCI.TcpSessionManagerTick(TsmCI.byref(sessionManager), 10) 
            except WindowsError as e:
                sys.stderr.write(traceback.format_exc())
                sys.exit(100)
            if args.verbose:
                n = TsmCI.DebugTraceGetChars(buf, 256)
                if n>0 :
                    a = buf[0:n-1]
                    print a

        duration = int((time.clock() - startTime)*1000)
        sessionResults.duration = duration
        sessionResults.epoch = int(time.time())
        sys.stdout.flush()

        #Print to console
        if not args.quiet:
            localTime = time.asctime(time.localtime(time.time()))
            printLock.acquire()

            print str(localTime) +"  CMIU " + str(ID) + ". " + \
                " Received CONNACK: " + str(sessionResults.receivedCounts[TsmCI.CONNACK]) + \
                " Sent PUBLISH: " + str(sessionResults.sentCounts[TsmCI.PUBLISH]) + \
                " Received PUBACK: " + str(sessionResults.receivedCounts[TsmCI.PUBACK])
            
            print "\n"
            printLock.release()

        sessionResults.errorCode = mqttManager.errorState
        
        return sessionResults


    def OnReceivedByte(self, fileDownloader, b, byteIndex):
        if (byteIndex % 5000) == 0:
            print str(byteIndex/1000) + "kB downloaded"
        pass
        #callback


    def DoHttpGet(self, args, ID, server,  filename):
        #Create and fill in all needed structs for managing the TCP connection
        socketWin = TsmCI.SOCKET_WIN()
        transportManager = TsmCI.TRANSPORT_MANAGER()
        tcpSessionManager = TsmCI.TCP_SESSION_MANAGER()
        connectionParams = TsmCI.CONNECTION_PARAMS()
        connectionParams.host = TsmCI.String(server)
        connectionParams.port = 80 
        connectionParams.modemType = 0 #Winsock

        #Initialises socket win and adds pointer to transportManager
        TsmCI.MqttInitForPython(TsmCI.byref(transportManager),
                            TsmCI.byref(socketWin))

        #Initialise Tcp Session Manager
        TsmCI.TcpSessionManagerInit(TsmCI.byref(tcpSessionManager),
                                     TsmCI.byref(transportManager),
                                     TsmCI.byref(connectionParams))

        httpBuffer = (TsmCI.c_uint8 * 2048)()

        #Do the HTTP
        fileDownloader = TsmCI.FILE_DOWNLOADER()
        fileDownloaderInitParams = TsmCI.FILE_DOWNLOADER_INIT_PARAMS()
        fileDownloaderInitParams.buffer = httpBuffer 
        fileDownloaderInitParams.bufferSize = 2048 
        fileDownloaderInitParams.fileName = TsmCI.String(filename)
        fileDownloaderInitParams.blockSize = 1024
        fileDownloaderInitParams.maxRequestRetries = 1
        fileDownloaderInitParams.requestTimeoutMs = 2000 
        fileDownloaderInitParams.OnReceivedByte = TsmCI.pfnOnReceivedByte(self.OnReceivedByte)
        TsmCI.FileDownloaderInit(TsmCI.byref(fileDownloader),
                                 TsmCI.byref(fileDownloaderInitParams))

        # Begin to run the File downloader Manager object within the TcpSessionManager
        TsmCI.FileDownloadBeginForPython(TsmCI.byref(tcpSessionManager),
                                           TsmCI.byref(fileDownloader))

        buf = (TsmCI.c_char * 256)()                           

        #Run the session until not busy
        TsmCI.TcpSessionManagerTick(TsmCI.byref(tcpSessionManager), 1)
        while(TsmCI.TcpSessionManagerIsBusy(TsmCI.byref(tcpSessionManager))):
            time.sleep(0.001)
            TsmCI.TcpSessionManagerTick(TsmCI.byref(tcpSessionManager), 1) 
            n = TsmCI.DebugTraceGetChars(buf, 256)
            if (n>0 and args.verbose):
                a = buf[0:n-1]
                print a
                
            sys.stdout.flush()
				
        print "\nCMIU " + str(ID) + " >> DOWNLOAD COMPLETE\n"
        
        sys.stdout.flush()


class Packer:
    def __init__(self):
        #initialise packer
        self.bufferSize = 2048
        self.pPacker = CI.S_TPBP_PACKER()
        self.pBuffer = (CI.c_uint8 * self.bufferSize)()
        CI.Tpbp_PackerInit(CI.byref(self.pPacker), 
                           CI.byref(self.pBuffer), 
                                 self.bufferSize)


class Parser:
    def __init__(self):
        #initialise parser
        self.bufferSize = 2048
        self.pParser = CI.S_TPBP_PARSER()
        self.pBuffer = (CI.c_uint8 * self.bufferSize)()
        CI.Tpbp_ParserInit(CI.byref(self.pParser), 
                           CI.byref(self.pBuffer), 
                                 self.bufferSize)


