:: Publish to EC2 pre-alpha MQTT broker at elastic ip 54.187.239.195, CmiuId = 500000, 1 CMIU, publish interval = 1 min, packet = detailedConfigPacket
@echo off
set ver=${project.version}
@echo Cmiu Simulator
@echo Build %ver%

@echo Python version (must be 32-bit to run the simulator)
python -V

@echo Publishing to broker:54.187.239.195 at 60 min interval
python CmiuSimManager.py 54.187.239.195 -s 500000 -n 1 -u 60 -p detailedConfigPacket
