:: Publish to EC2 pre-alpha MQTT broker at elastic ip 54.187.239.195, CmiuId = 500000, publish interval = 1 min, packet = detailedConfigPacket
@echo off
set ver=${project.version}
@echo Cmiu Simulator
@echo Build %ver%

@echo Python version (must be 32-bit to run the simulator)
python -V

@echo Publish interval data packet to auto-test broker every ~5 seconds in order to test image update mechanism
python CmiuSimManager.py 54.191.137.243 -s 499999999 -n 1 -u 0.08 -p intervalDataPacket
