:: Publish to EC2 pre-alpha MQTT broker at elastic ip 54.187.239.195, CmiuId = 500000, publish interval = 1 min, packet = detailedConfigPacket
@echo off
set ver=${project.version}
@echo Cmiu Simulator
@echo Build %ver%

@echo Python version (must be 32-bit to run the simulator)
python -V

@echo Publishing to 54.187.239.195, at 1 min interval
python CmiuSimManager.py 54.187.239.195 -s 500000 -n 1 -u 1 -p detailedConfigPacket
