import os
import sys
import unittest
os.chdir("./../../target/")
sys.path.insert(1, './')


import CmiuSimDataGenerator
import CmiuSimWrapper
import CmiuSimUsageDataGenerator

try:
    from teamcity import underTeamcity
    from teamcity.unittestpy import TeamcityTestRunner
except ImportError:
    def underTeamcity():
        return False

#Empty class for passing arguments
class args:
    pass
        
class TestCmiuSimulator(unittest.TestCase):
    def setUp(self):
        #Create C function wrapper object
        self.wrap = CmiuSimWrapper.Wrapper()

        #Create usageData 
        self.args = type('', (), {})()
        self.args.revisionNumber = 1
        self.args.recordingInterval = 15
        self.args.reportingInterval = 60
        self.args.cvs = False
        self.args.ffts = False
        self.args.dumpfile = 0
        self.args.usageData = CmiuSimUsageDataGenerator.UsageData("R900Readings.txt")

        #Create cmiu Data object
        self.cmiuData = CmiuSimDataGenerator.CmiuData(1, self.args)
    
    
    def tearDown(self):
        #Clear objects
        self.cmiuData = None
        self.wrap = None
    
    def testCorruptBytes(self):
        print "\n\n"
        print "Test building and then parsing a packet header with corrupt bytes..."
        
        print "Generate new packet data..."
        self.cmiuData.generateNextState(self.args)
        
        print "Pack packet header into buffer..."
        self.wrap.AddPacket(self.cmiuData, args, 'justPacketHeader')
        
        print "Corrupt a byte in timeAndDate..."
        self.wrap.packers[0].pBuffer[19] = 91

        for count in range(2000):
            self.wrap.parsers[0].pBuffer[count] = self.wrap.packers[0].pBuffer[count]
        self.wrap.parserCounter = 1
        
        print "Parse buffer in tagged data objects..."
        out = self.wrap.GetPacket(self.cmiuData)
        out = out[1] 
        
        print "Parsed Tagged Data:"
        for item in out:
            print "    " + str(item)
        
        print "Check that the corrupted field does not match the sent data..."
        
        self.assertNotEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].timeAndDate,
                         out['CMIU_PACKET_HEADER'].timeAndDate)
       
    
    def testPacketHeaderBuilderParser(self):
        print "\n\n"
        print "Test building and then parsing a packet header..."
        
        print "Generate new packet data..."
        self.cmiuData.generateNextState(self.args)
        
        print "Pack packet header into buffer..."
        self.wrap.AddPacket(self.cmiuData, args, 'justPacketHeader')

        for count in range(2000):
            self.wrap.parsers[0].pBuffer[count] = self.wrap.packers[0].pBuffer[count]
        self.wrap.parserCounter = 1
        
        print "Parse buffer in tagged data objects..."
        out = self.wrap.GetPacket(self.cmiuData)
        out = out[1]
        
        print "Parsed Tagged Data:"
        for item in out:
            print "    " + str(item)
        
        print "Check the parsed data matches the packet header used to build the packet..."
        
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].timeAndDate,
                         out['CMIU_PACKET_HEADER'].timeAndDate)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].cmiuId,
                         out['CMIU_PACKET_HEADER'].cmiuId)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].cellularRssiAndBer,
                         out['CMIU_PACKET_HEADER'].cellularRssiAndBer)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].sequenceNumber,
                         out['CMIU_PACKET_HEADER'].sequenceNumber)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].keyInfo,
                         out['CMIU_PACKET_HEADER'].keyInfo)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].encryptionMethod,
                         out['CMIU_PACKET_HEADER'].encryptionMethod)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].tokenAesCrc,
                         out['CMIU_PACKET_HEADER'].tokenAesCrc)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].networkFlags,
                         out['CMIU_PACKET_HEADER'].networkFlags)

                      
    def testConfigurationBuilderParser(self):
        print "\n\n"
        print "Test building and then parsing CMIU configuration tagged data..."
        
        print "Generate new packet data..."
        self.cmiuData.generateNextState(self.args)
        
        print "Pack configuration tagged data into buffer..."
        self.wrap.AddPacket(self.cmiuData, args, 'justConfiguration')

        for count in range(2000):
            self.wrap.parsers[0].pBuffer[count] = self.wrap.packers[0].pBuffer[count]
        self.wrap.parserCounter = 1

        print "Parse buffer in tagged data objects..."
        out = self.wrap.GetPacket(self.cmiuData)
        out = out[1]

        print "Parsed Tagged Data:"
        for item in out:
            print "    " + str(item)
        
        print "Check the parsed data matches the configuration data used to build the packet..."
        
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].manufactureDate,
                         out['CMIU_CONFIGURATION'].manufactureDate)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].initialCallInTime,
                         out['CMIU_CONFIGURATION'].initialCallInTime)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].installationDate,
                         out['CMIU_CONFIGURATION'].installationDate)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].dateLastMagSwipe,
                         out['CMIU_CONFIGURATION'].dateLastMagSwipe)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].hwRevision,
                         out['CMIU_CONFIGURATION'].hwRevision)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].firmwareRev,
                         out['CMIU_CONFIGURATION'].firmwareRev)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].bootloaderVersion,
                         out['CMIU_CONFIGURATION'].bootloaderVersion)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].minsToReportingWindowQuietTime,
                         out['CMIU_CONFIGURATION'].minsToReportingWindowQuietTime)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].minsToReportingWindowQuietTimeEnd,
                         out['CMIU_CONFIGURATION'].minsToReportingWindowQuietTimeEnd)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].batteryRemaining,
                         out['CMIU_CONFIGURATION'].batteryRemaining)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].cmiuType,
                         out['CMIU_CONFIGURATION'].cmiuType)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].callInInterval,
                         out['CMIU_CONFIGURATION'].callInInterval)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].callInWindow,
                         out['CMIU_CONFIGURATION'].callInWindow)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].reportingwindowNRetries,
                         out['CMIU_CONFIGURATION'].reportingwindowNRetries)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].attachedDevices,
                         out['CMIU_CONFIGURATION'].attachedDevices)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].magSwipes,
                         out['CMIU_CONFIGURATION'].magSwipes)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].timeErrorLastNetworkTimeAccess,
                         out['CMIU_CONFIGURATION'].timeErrorLastNetworkTimeAccess)

                             
    def testPacketWithMultipleTags(self): 
        print "\n\n"
        print "Test building and then parsing packet with multiple tags..."
        
        print "Generate new packet data..."
        self.cmiuData.generateNextState(self.args)
        
        print "Build and pack test packet into buffer..."
        self.wrap.AddPacket(self.cmiuData, args, 'testPacket')
        
        for count in range(2000):
            self.wrap.parsers[0].pBuffer[count] = self.wrap.packers[0].pBuffer[count]
        self.wrap.parserCounter = 1
        
        print "Parse buffer in tagged data objects..."
        out = self.wrap.GetPacket(self.cmiuData)
        out = out[1]
        
        print "Parsed Tagged Data:"
        for item in out:
            print "    " + str(item)
        
        print "Check the parsed data matches the data tags used to build the packet..."
        
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].timeAndDate,
                         out['CMIU_PACKET_HEADER'].timeAndDate)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].cmiuId,
                         out['CMIU_PACKET_HEADER'].cmiuId)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].cellularRssiAndBer,
                         out['CMIU_PACKET_HEADER'].cellularRssiAndBer)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].sequenceNumber,
                         out['CMIU_PACKET_HEADER'].sequenceNumber)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].keyInfo,
                         out['CMIU_PACKET_HEADER'].keyInfo)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].encryptionMethod,
                         out['CMIU_PACKET_HEADER'].encryptionMethod)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].tokenAesCrc,
                         out['CMIU_PACKET_HEADER'].tokenAesCrc)
        self.assertEqual(self.cmiuData.tags['CMIU_PACKET_HEADER'].networkFlags,
                         out['CMIU_PACKET_HEADER'].networkFlags)
                         
                         
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].manufactureDate,
                         out['CMIU_CONFIGURATION'].manufactureDate)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].initialCallInTime,
                         out['CMIU_CONFIGURATION'].initialCallInTime)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].installationDate,
                         out['CMIU_CONFIGURATION'].installationDate)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].dateLastMagSwipe,
                         out['CMIU_CONFIGURATION'].dateLastMagSwipe)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].hwRevision,
                         out['CMIU_CONFIGURATION'].hwRevision)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].firmwareRev,
                         out['CMIU_CONFIGURATION'].firmwareRev)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].bootloaderVersion,
                         out['CMIU_CONFIGURATION'].bootloaderVersion)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].minsToReportingWindowQuietTime,
                         out['CMIU_CONFIGURATION'].minsToReportingWindowQuietTime)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].minsToReportingWindowQuietTimeEnd,
                         out['CMIU_CONFIGURATION'].minsToReportingWindowQuietTimeEnd)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].batteryRemaining,
                         out['CMIU_CONFIGURATION'].batteryRemaining)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].cmiuType,
                         out['CMIU_CONFIGURATION'].cmiuType)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].callInInterval,
                         out['CMIU_CONFIGURATION'].callInInterval)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].callInWindow,
                         out['CMIU_CONFIGURATION'].callInWindow)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].reportingwindowNRetries,
                         out['CMIU_CONFIGURATION'].reportingwindowNRetries)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].attachedDevices,
                         out['CMIU_CONFIGURATION'].attachedDevices)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].magSwipes,
                         out['CMIU_CONFIGURATION'].magSwipes)
        self.assertEqual(self.cmiuData.tags['CMIU_CONFIGURATION'].timeErrorLastNetworkTimeAccess,
                         out['CMIU_CONFIGURATION'].timeErrorLastNetworkTimeAccess)
            
        
#Run the unit tests        
if __name__ == '__main__':
    if underTeamcity():
        runner = TeamcityTestRunner()
    else:
        runner = unittest.TextTestRunner(verbosity=1)
    unittest.main(testRunner=runner)
