 ::*****************************************************************************
 ::
 ::    Neptune Technology Group
 ::    Copyright 2015 as unpublished work.
 ::    All rights reserved
 ::
 ::    The information The information contained herein is confidential
 ::    property of Neptune Technology Group. The use, copying, transfer
 ::    or disclosure of such information is prohibited except by express
 ::    written agreement with Neptune Technology Group.
 ::
 ::*****************************************************************************/

:: This file should be run from CmiuSimulator\src\assembly

SET DIR_COMMON=../../../Common
SET DIR_TARGET=../../target
SET DIR_COMMON_DOS=%DIR_COMMON:/=\%
SET DIR_TARGET_DOS=%DIR_TARGET:/=\%

:: Copy library files to local target directory
xcopy /y %DIR_COMMON_DOS%\TaggedPacketBuilderParser\target\%TpbpLibraryFileName%\Release\%TpbpLibraryFile% %DIR_TARGET_DOS%\
xcopy /y %DIR_COMMON_DOS%\TcpSessionManager\target\TsmTestHarness\obj\x86\Release\%TsmLibraryFile% %DIR_TARGET_DOS%\

pushd ..\..

mvn clean package

popd
