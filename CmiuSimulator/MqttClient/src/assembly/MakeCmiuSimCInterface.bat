 ::*****************************************************************************
 ::
 ::    Neptune Technology Group
 ::    Copyright 2015 as unpublished work.
 ::    All rights reserved
 ::
 ::    The information The information contained herein is confidential
 ::    property of Neptune Technology Group. The use, copying, transfer
 ::    or disclosure of such information is prohibited except by express
 ::    written agreement with Neptune Technology Group.
 ::
 ::*****************************************************************************/

:: This file should be run from CmiuSimulator\src\main\python

SET DIR_LIB=../lib

SET DIR_COMMON=../../../../Common
SET DIR_TARGET=../../target/simulator
SET DIR_COMMON_DOS=%DIR_COMMON:/=\%
SET DIR_TARGET_DOS=%DIR_TARGET:/=\%

SET TpbpLibraryFileName=TpbpLib
SET TsmLibraryFileName=TsmDll
SET LibraryFileExt=dll
SET TpbpLibraryFile=%TpbpLibraryFileName%.%LibraryFileExt%
SET TsmLibraryFile=%TsmLibraryFileName%.%LibraryFileExt%


:: Copy library files to local target directory
xcopy /y %DIR_COMMON_DOS%\TaggedPacketBuilderParser\target\%TpbpLibraryFileName%\Release\%TpbpLibraryFile% %DIR_TARGET_DOS%\*
xcopy /y %DIR_COMMON_DOS%\TcpSessionManager\target\TsmTestHarness\obj\x86\Release\%TsmLibraryFile% %DIR_TARGET_DOS%\*

::
:: Makes the C Interface file for Tpbp
::
python %DIR_LIB%/CTypesGen/ctypesgen.py ^
-l %DIR_TARGET%/%TpbpLibraryFile% ^
-I %DIR_COMMON%/Types ^
%DIR_COMMON%/TaggedPacketBuilderParser/src/main/c/Tpbp.h ^
%DIR_COMMON%/Types/TagTypes.h ^
-o %DIR_TARGET%/CmiuSimCInterface.py

::
:: Makes the C Interface file for Tsm
::
python %DIR_LIB%/CTypesGen/ctypesgen.py ^
-l %DIR_TARGET%/%TsmLibraryFile% ^
-I %DIR_COMMON%/Types ^
-I %DIR_COMMON%/TcpSessionManager/src/main/c ^
-I %DIR_COMMON%/TcpSessionManager/src/integration/%TsmLibraryFileName%/c ^
-I ../../../../ThirdPartyTools/org.eclipse.paho.mqtt.embedded-c/MQTTPacket/src ^
%DIR_COMMON%/TcpSessionManager/src/integration/%TsmLibraryFileName%/c/Mqtt.h ^
%DIR_COMMON%/TcpSessionManager/src/main/c/TcpSessionManager.h ^
%DIR_COMMON%/TcpSessionManager/src/main/c/MqttManager.h ^
%DIR_COMMON%/Utility/src/main/c/DebugTrace.h ^
%DIR_COMMON%/TcpSessionManager/src/integration/TsmDll/c/SocketWin.h ^
-o %DIR_TARGET%/CmiuSimTsmCInterface.py
