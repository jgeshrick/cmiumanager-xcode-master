 ::*****************************************************************************
 ::
 ::    Neptune Technology Group
 ::    Copyright 2015 as unpublished work.
 ::    All rights reserved
 ::
 ::    The information The information contained herein is confidential
 ::    property of Neptune Technology Group. The use, copying, transfer
 ::    or disclosure of such information is prohibited except by express
 ::    written agreement with Neptune Technology Group.
 ::
 ::*****************************************************************************/

:: This file should be run from CmiuSimulator\src\assembly

SET DIR_LIB=../lib

SET DIR_COMMON=../../../../Common
SET DIR_TARGET=../../target/simulator
SET DIR_COMMON_DOS=%DIR_COMMON:/=\%
SET DIR_TARGET_DOS=%DIR_TARGET:/=\%


SET TpbpLibraryFileName=TpbpLib
SET TsmLibraryFileName=TsmDll
SET LibraryFileExt=dll
SET TpbpLibraryFile=%TpbpLibraryFileName%.%LibraryFileExt%
SET TsmLibraryFile=%TsmLibraryFileName%.%LibraryFileExt%

IF not exist %DIR_TARGET_DOS% (mkdir %DIR_TARGET_DOS%)

:: Copy library files to local target directory
xcopy /y %DIR_COMMON_DOS%\TaggedPacketBuilderParser\target\%TpbpLibraryFileName%\Release\%TpbpLibraryFile% %DIR_TARGET_DOS%\
xcopy /y %DIR_COMMON_DOS%\TcpSessionManager\target\TsmTestHarness\obj\x86\Release\%TsmLibraryFile% %DIR_TARGET_DOS%\

xcopy /y ..\main\python\*.py %DIR_TARGET_DOS%\
xcopy /y ..\main\python\*.txt %DIR_TARGET_DOS%\
xcopy /y ..\main\batch\*.bat %DIR_TARGET_DOS%\

call MakeCMIUSimCInterface.bat


