/******************************************************************************
*******************************************************************************
**
**         Filename: cmiuSpi.h
**
**           Author: Brian Arnberg
**          Created: 2015.12.22
**
** Revision History:
**       2015.12.22: First created. - BWA
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __CMIU_SPI_H
#define __CMIU_SPI_H

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
/** These were moved from app_ble.c */
#define BLE_ACI_MOSI  2
#define BLE_ACI_MISO  3
#define BLE_ACI_SCLK  4
#define BLE_ACI_REQN  6
#define BLE_ACI_RDYN  1
// our reset line is not on Port C so we cannot use the macros for it.
#define BLE_ACI_RESET 2
/** Flash line definitions. */
#define FLASH_MOSI  2
#define FLASH_MISO  3
#define FLASH_SCLK  4
#define FLASH_CS    5

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void cmiuSpi_init(void);
extern bool cmiuSpi_flash_enable(void);
extern void cmiuSpi_flash_disable(void);
extern bool cmiuSpi_is_flash_enabled(void);
extern bool cmiuSpi_btle_enable(void);
extern void cmiuSpi_btle_disable(void);
extern bool cmiuSpi_is_btle_enabled(void);
extern bool cmiuSpi_flash_write(uint8_t * const pData, uint32_t len);
extern bool cmiuSpi_flash_read(uint8_t * const pData, uint32_t len);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __CMIU_SPI_H

