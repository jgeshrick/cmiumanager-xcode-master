/******************************************************************************
*******************************************************************************
**
**         Filename: cmiuFlash.c
**
**           Author: Barry Huggins
**          Created: 12/23/14
**
**     Last Edit By: Brian Arnberg
**        Last Edit: 2015.07.13
**
** Revision History:
**       2015.07.13: Updated, added subroutines, renamed, made common. - BWA
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file cmiuFlash.c
*
* @brief This file contains the main routines to access the flash memory
*        module (currently a Micron M25PX16)
*
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdio.h>
#include <string.h>
#include "typedefs.h"
#include "cmiuFlash.h"
#include "cmiuSpi.h"

#ifndef CMIU_UNIT_TEST
#include "app_cmit_interface.h"
#endif

#include "em_device.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "efm32lg_uart.h"
#include "app_uart.h"
#include "app_timers.h"

/*=========================================================================*/
/*  L O C A L   G L O B A L   V A R I A B L E S                            */
/*=========================================================================*/
static uint32_t previousSysTickCtrl;
static uint32_t flash_user_counter;
static bool flash_is_initialized;



/***************************************************************************//**
 * @brief
 *  Set certain flash variables to the startup state.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void Flash_startup(void)
{
    /** Currently, there are no users. */
    flash_user_counter = 0;

    /** The flash in not initialized right now. */
    flash_is_initialized = false;
}

/***************************************************************************//**
 * @brief
 *   Initializes GPIO pins and USART2 for flash operation, then powers the chip.
 *
 * @note
 *  Store the current GPIO settings, then setup all the GPIO pins, setup USART2
 *  to operate the flash communications, and finally power on the flash chip.
 *  Also, all flash communications depend on the sysTick timer, so make sure
 *  that is enabled before this is called (and disabled after this is disabled).
 *
 * @param[in] None
 *
 * @return bool - true if flash is initialized; false otherwise
 ******************************************************************************/
bool Flash_init(void)
{
    /** Initialize the flash IFF the flash hasn't yet been initialized. */
    if (!flash_is_initialized)
    {
        uint32_t battAdc;

        /** Measure and retrieve battery voltage ADC value */
        app_adc_vddDiv3_setupAndMeasure();
        battAdc = app_adc_vddDiv3_retrieve();

        /** Make sure to only initialize if the battery is above the minimum
         * value. */
        if (MINIMUM_FLASH_VOLTAGE < battAdc)
        {
            /** Configure GPIO and USART for SPI communications with the flash
            * module. */
            (void) cmiuSpi_flash_enable();

            /** Power the flash chip */
            Flash_PowerOn();

            previousSysTickCtrl = app_timers_getMsecsStatus();

            if (SysTick_CTRL_ENABLE_Msk != (previousSysTickCtrl & SysTick_CTRL_ENABLE_Msk))
            {
                app_timers_enableMsecs();
            }

            /** The documentation indicates that we need to wait a max of 10ms
            * before we'll be allowed to write to the device, so lets wait that
            * long. */
            Timers_Delay (10);

            /** Indicate that flash is initialized. */
            flash_is_initialized = true;
        }
        else
        {
            /** The battery voltage is too low, so make sure the flash system
             * is completely disabled. */
            Flash_force_Deinit();
        }

    }
    else
    {
        /* Do nothing. */
    }

    /** If flash is now initialized, increment the number of flash users, but
     * don't exceed the maximum. */
    if (flash_is_initialized)
    {
        ++flash_user_counter;
        if (MAXIMUM_FLASH_USERS <= flash_user_counter)
        {
            flash_user_counter = MAXIMUM_FLASH_USERS;
        }
    }

    return flash_is_initialized;
}

/***************************************************************************//**
 * @brief
 *  Force flash to disable.
 *
 * @note
 *  Power off the flash chip and return the GPIO pins to the value they had
 *  before Flash_init was called.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void Flash_force_Deinit(void)
{
    flash_user_counter = 0;
    Flash_Deinit();
}

/***************************************************************************//**
 * @brief
 *  Undo Flash_init()
 *
 * @note
 *  Power off the flash chip and return the GPIO pins to the value they had
 *  before Flash_init was called.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void Flash_Deinit(void)
{
    /** If the flash_user_counter is in the range [1..MAXIMUM_FLASH_USERS], then
     * decrement it. Otherwise set it to zero. */
    if ((0u < flash_user_counter) && (MAXIMUM_FLASH_USERS > flash_user_counter))
    {
        --flash_user_counter;
    }
    else
    {
        /** Since the user counter value is out of range, let's go ahead and
         * set it to zero. */
        flash_user_counter = 0;
    }

    /** Disable flash IFF there are no more flash users and flash is currently
     * initialized. */
    if ((0u == flash_user_counter) && (flash_is_initialized))
    {
        /** Wait on WIP to clear if SPI for flash is still enabled. */
        if (cmiuSpi_is_flash_enabled())
        {
            /** Give WIP 100ms to clears (may take less if WIP is already clear) */
            (void) Flash_WaitOnWip(100);
        }

        if (SysTick_CTRL_ENABLE_Msk != (previousSysTickCtrl & SysTick_CTRL_ENABLE_Msk))
        {
            app_timers_disableMsecs();
        }

        /** Return the SPI settings to whatever they were prior to Flash_Init(). */
        cmiuSpi_flash_disable();

        /** Power off the flash chip */
        Flash_PowerOff();

        /** Lastly, indicate that flash is disabled. */
        flash_is_initialized = false;
    }
    else
    {
        /* Do nothing. */
    }
}

/***************************************************************************//**
 * @brief
 *   Enable power to the flash memory (currently the Micron M25PX16).
 *
 * @note
 *   This function enables power to the flash memory part.
 *   It does this by taking PA9 low.
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void Flash_PowerOn(void)
{
    /* Enable flash power (PA9) - active low */
    GPIO_PinOutClear(gpioPortA, 9);
}

/***************************************************************************//**
 * @brief
 *   Disable power to the flash memory (currently the Micron M25PX16).
 *
 * @note
 *   This function enables power to the flash memory part.
 *   It does this by taking PA9 high.
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void Flash_PowerOff(void)
{
    /* Disable flash power (PA9) - active low */
    GPIO_PinOutSet(gpioPortA, 9);
}

/***************************************************************************//**
 * @brief
 *   Return the power status of the flash memory (currently the Micron M25PX16).
 *
 * @note
 *   The power is controlled by PA9 which is active low.
 *
 *
 * @param[in] none
 *
 * @return BOOL - returns true if powered
 ******************************************************************************/
bool Flash_isPowered(void)
{
    bool flashPowered = true;

    // this signal is active low so if set then not powered.
    if (GPIO_PinOutGet(gpioPortA, 9))
    {
        flashPowered = false;
    }

    return flashPowered;
}

/***************************************************************************//**
 * @brief
 *   Enable writes to external flash.
 *
 * @note
 *   This function sends the "write enable" command to the external flash
 *   device. This should set the write enable bit in the external flash
 *   status register.
 *
 * @param[out] none
 *
 * @return bool - Indicates success (true) or failure (false).
 ******************************************************************************/
bool  Flash_WriteEnable(void)
{
    bool retval = false;

    if (flash_is_initialized)
    {
        uint8_t  cmd = SPI_FLASH_INS_WREN;

        uint32_t count;

        count = 5;

        /** Try to wait for WEL to set */
        do
        {
            /** Send the command */
            cmiuSpi_flash_write (&cmd, 1);

            retval = Flash_WaitOnWel(10);

            --count;
        }
        while ((false == retval) && (0u != count));
    }
    else
    {
        retval = false;
    }

    return retval;
}

/***************************************************************************//**
 * @brief
 *   Disable writes to external flash.
 *
 * @note
 *   This function sends the "write disable" command to the external flash
 *   device. This should clear the write enable bit in the external flash
 *   status register.
 *
 *
 * @param[out] none
 *
 * @return bool - Indicates success (true) or failure (false).
 ******************************************************************************/
bool  Flash_WriteDisable( void )
{
    if (flash_is_initialized)
    {
        uint8_t  cmd = SPI_FLASH_INS_WRDI;

        // Send the command
        cmiuSpi_flash_write (&cmd, 1);
    }
    else
    {
        /* Do nothing */
    }

    return flash_is_initialized;
}


/***************************************************************************//**
 * @brief
 *   Function to read the device identification from the flash memory part.
 *
 * @note
 *   This function reads the device identification from the flash memory part
 *   using the read identification command (0x9F).
 *
 *
 * @param[out] none
 *
 * @return uint16_t flash identification value
 ******************************************************************************/
uint16_t  Flash_ReadDeviceIdentification(void)
{
    uint16_t    identificationResult;

    if (flash_is_initialized)
    {
        uint8_t  cmd = SPI_FLASH_INS_RDID;
        uint8_t  identification[20];

        // Send the command
        cmiuSpi_flash_write (&cmd, 1);

        // Flush the buffer
        cmiuSpi_flash_read (identification, 3);

        // Read the result
        cmiuSpi_flash_read (identification, 20);

        // create the result ( memory type + memory capacity )
        identificationResult = identification[1];
        identificationResult <<= 8;
        identificationResult |= identification[2];
    }
    else
    {
        identificationResult = (uint16_t)flash_is_initialized;
    }

    return identificationResult;
}

/***************************************************************************//**
 * @brief
 *   Function to read the status register of the flash memory part.
 *
 * @note
 *   This function reads the status register of the flash memory part
 *   using the read status register command (0x05).
 *
 * @param[out] none
 *
 * @return uint8_t flash status register value
 ******************************************************************************/
uint8_t  Flash_ReadStatusRegister(void)
{
    uint8_t retval;

    if (flash_is_initialized)
    {
        uint8_t cmd = SPI_FLASH_INS_RDSR;
        uint8_t statusRegister[4] = {0xFF,0xFF,0xFF,0xFF};
        uint8_t counter;

        /** Try to read the status register only four times */
        for(counter = 4; (counter > 0)&&(statusRegister[3] & BIT6); --counter);
        {
            /** Send the command */
            cmiuSpi_flash_write (&cmd, 1);

            /** Read 4 bytes. The forth will be valid. */
            cmiuSpi_flash_read (statusRegister, 4);

            /** Short delay */
            Timers_Delay(1);
        }
        retval = statusRegister[3];
    }
    else
    {
        retval = 0xFFu;
    }

    return retval;
}

/***************************************************************************//**
 * @brief
 *   Wait until either the WIP (work in progress) bit is cleared or a certain
 *   time is passed.
 *
 * @note
 *   This function polls the status register of the flash device until it has
 *   waited the passed number of millseconds or the WIP bit has been cleared.
 *   The function returns true if the WIP bit cleared before the timeout,
 *   and returns false if the WIP bit does not clear.
 *
 * @param[in] delTicks - maximum number of milliseconds to wait
 *
 * @return bool - true, WIP eventually cleared; false, WIP didn't clear
 ******************************************************************************/
bool Flash_WaitOnWip(uint32_t delTicks)
{
    uint32_t endTime;
    uint8_t status;
    bool retVal;
    uint8_t firstStatus; /* This is only used in debugging. */

    retVal = false;
    status = 0xFF;
    firstStatus = 0xFF;

    endTime = Timers_GetMsTicks() + delTicks;

    /** Run this loop until the delay time is passed or retval is true */
    while((endTime > Timers_GetMsTicks()) && !(retVal))
    {
        /** Get the current status */
        status = Flash_ReadStatusRegister();

        /** If either the WIP bit or BIT6 is set, we need to poll
         ** the status register again. */
        if (0 != (status & (CHECK_MASK)))
        {
            /** Do nothing, then poll the register again. */
            if (0xFFu == firstStatus)
            {
                firstStatus = status;
            }
        }
        /** IF both the WIP bit and BIT6 are clear, we can stop waiting. */
        else
        {
            /** This lets us exit the loop and signal the calling function
             ** that WIP cleared before the timeout. */
            retVal = true;
        }
    }

    if (status & BIT6)
    {
        DEBUG_OUTPUT_TEXT("BIT6 never cleared\r\n");
    }
    else if (status & WIP)
    {
        DEBUG_OUTPUT_TEXT("WIP never cleared\r\n");
    }
    else if (status & WEL)
    {
        DEBUG_OUTPUT_TEXT("WEL never cleared\r\n");
    }

    return retVal;
}

/***************************************************************************//**
 * @brief
 *   Wait until either the WEL (write enable latch) bit is set or a certain
 *   time is passed.
 *
 * @note
 *   This function polls the status register of the flash device until the
 *   timeout is reached or the WEL bit of the status register has been set.
 *   The function returns true if the WEL bit set before the timeout, and
 *   returns false if the WEL bit does not set.
 *
 * @param[in] delTicks - maximum number of milliseconds to wait
 *
 * @return bool - true, WEL eventually set; false, WEL didn't clear
 ******************************************************************************/
bool Flash_WaitOnWel(uint32_t delTicks)
{
    uint32_t endTime;
    uint8_t status;
    bool retVal;

    retVal = false;
    status = 0xFF;

    endTime = Timers_GetMsTicks() + delTicks;

    /** Run this loop until the delay time is passed or retval is true */
    while((endTime > Timers_GetMsTicks()) && !(retVal))
    {
        /** Get the current status */
        status = Flash_ReadStatusRegister();

        /** If the WEL is clear or BIT6 is set or WIP is set,
         ** we need to poll the register again. */
        if (WEL != (status & CHECK_MASK))
        {
            /** Do nothing, then poll the register again. */
        }
        /** If WEL is set and BIT6 is clear, we can stop waiting. */
        else
        {
            /** This lets us exit the loop and signal the calling function
             ** that WEL set before the timeout. */
            retVal = true;
        }
    }

    /** Display an error message if WEL did not set. */
    if (false == retVal)
    {
        DEBUG_OUTPUT_TEXT("WEL DID NOT SET\r\n");
    }

    return retVal;
}

/***************************************************************************//**
 * @brief
 *   Execute the subsector erase command.
 *
 * @note
 *   This function attempts to erase the subsector associated with the address
 *   passed to the function. The Spec Sheet says the operation should take
 *   no longer than 150ms.
 *
 * @param[in] address - address of the subsector to erase
 *
 * @return bool - true, erase completed; false, erase didn't complete
 ******************************************************************************/
bool Flash_SubSectorErase(uint32_t address)
{
    uint8_t     commandAndAddress[4];
    bool retval;
    uint8_t status;
    uint32_t count;

    if (flash_is_initialized)
    {
        /** The WEL must be set before we are allowed to erase anything. */
        retval = Flash_WriteEnable();

        /** Send the erase command IFF the WriteEnable was successful. */
        if (retval)
        {
            /** Prepare the SSE command */
            commandAndAddress[0] = SPI_FLASH_INS_SSE;
            commandAndAddress[1] = (uint8_t)((address & 0x00FF0000)>>16);
            commandAndAddress[2] = (uint8_t)((address & 0x0000FF00)>>8);
            commandAndAddress[3] = (uint8_t)((address & 0x000000FF));

            /** We'll only try to send the SSE command 5 times. */
            count = 5;

            do
            {
                /** Send the SSE command */
                cmiuSpi_flash_write (commandAndAddress, 4);

                /* Let things settle before we try to poll */
                Timers_Delay(50);

                /** Get the status register to determine if the command took. */
                status = Flash_ReadStatusRegister();

                /** Decrement count to limit the number of times we try this
                 *  command. */
                --count;
            }
            /** If WEL is set (by itself) the command didn't take, so let's try to
             *  reissue the command. We'll only try to reissue the command if we
             *  haven't reduced count to 0. */
            while ((WEL == (status & (CHECK_MASK))) && (0u != count));

            /** Wait for the WIP to clear no longer than 150ms */
            retval = Flash_WaitOnWip(150);
        }
        else
        {
            DEBUG_OUTPUT_TEXT("WriteEnable failed.\r\n");
        }
    }
    else
    {
        retval = false;
    }

    return retval;
}

/***************************************************************************//**
 * @brief
 *   Execute the bulk erase command.
 *
 * @note
 *   This function attempts to erase the entire chip. According to documentation
 *   about the M25PX16, this can take up to 80 seconds, though the typical is
 *   about 15 seconds.
 *
 * @param[in] none
 *
 * @return bool - true, erase completed; false, erase didn't complete
 ******************************************************************************/
bool Flash_BulkErase(void)
{
    uint8_t     commandAndAddress[4];
    bool retval;

    if (flash_is_initialized)
    {
        /** The WEL must be set before we are allowed to erase anything. */
        retval = Flash_WriteEnable();

        /** Send the erase command IFF the WriteEnable was successful. */
        if (retval)
        {
            /** Prepare the BE command */
            commandAndAddress[0] = SPI_FLASH_INS_BE;

            /** Send the BE command */
            cmiuSpi_flash_write (commandAndAddress, 1);

            /* Let things settle before we try to poll */
            Timers_Delay(70);

            /** Wait for the WIP to clear no longer than 80s */
            retval = Flash_WaitOnWip(80000);
        }
    }
    else
    {
        retval = false;
    }

    return retval;
}

/***************************************************************************//**
 * @brief
 *   Execute the read command.
 *
 * @note
 *   This function attempts to read from the address provided and writes to the
 *   buffer provided. It assumes the WIP has already been verified to be cleared.
 *
 * @param[in]
 *  *buffer - a buffer for holding what is read from the flash chip.
 *
 * @param[in]
 *  address - address of the flash memory to read
 *
 * @param[in]
 *  length - number of bytes to read.
 *
 *
 * @return bool - true, read completed; false, read didn't complete
 ******************************************************************************/
bool Flash_ReadFromAddress(uint8_t *buffer, uint32_t address, uint32_t length)
{
    uint8_t commandAndAddress[4];
    bool    retval;

    if (flash_is_initialized)
    {
        commandAndAddress[0] = SPI_FLASH_INS_READ;
        commandAndAddress[1] = (uint8_t)((address & 0x00FF0000)>>16);
        commandAndAddress[2] = (uint8_t)((address & 0x0000FF00)>>8);
        commandAndAddress[3] = (uint8_t)((address & 0x000000FF));

        cmiuSpi_flash_write (commandAndAddress, 4);
        // we seem to first have to read the 4 bytes in response to our command
        cmiuSpi_flash_read (buffer, 4);

        cmiuSpi_flash_read (buffer, length);

        retval = true;
    }
    else
    {
        retval = false;
    }

    return retval;
}

/***************************************************************************//**
 * @brief
 *   Execute the page program command.
 *
 * @note
 *   This function attempts to write to the address provided from the data
 *   in the buffer. It assumes the WIP has already been verified to be cleared.
 *   According to the spec sheet, a page program (for the whole page), should
 *   take less than 5ms.
 *
 * @param[in]
 *  *buffer - a buffer holding what is to be written to the flash chip.
 *
 * @param[in]
 *  address - address of the page to program
 *
 * @param[in]
 *  length - number of bytes to write. This should be no larger than 256.
 *
 *
 * @return bool - true, write completed; false, write didn't complete
 ******************************************************************************/
bool Flash_WriteToAddress(uint8_t *buffer, uint32_t address, uint16_t length)
{
    uint8_t     commandAndAddress[4];
    bool retval = false;

    retval = Flash_WriteEnable();

    if (retval)
    {
        /** Prepare to send the page program command */
        commandAndAddress[0] = SPI_FLASH_INS_PP;
        commandAndAddress[1] = (uint8_t)((address & 0x00FF0000)>>16);
        commandAndAddress[2] = (uint8_t)((address & 0x0000FF00)>>8);
        commandAndAddress[3] = (uint8_t)((address & 0x000000FF));

        /** Issue Page Program command with buffer */
        cmiuSpi_flash_write (commandAndAddress, 4);
        cmiuSpi_flash_write (buffer, length);

        /** Wait 1ms to allow it to settle before polling */
        Timers_Delay(1);

        /** Wait for the WIP to clear no longer than 20ms */
        retval = Flash_WaitOnWip(20);
    }
    else
    {
        DEBUG_OUTPUT_TEXT("Did not execute PAGE PROGRAM.\r\n");
    }

    return retval;
}

/***************************************************************************//**
 * @brief
 *   Execute the page program command and verify that it succeeded.
 *
 * @note
 *   This function writes a buffer into external flash, then tries to verify
 *   that the buffer was successfully written. It does the verification by
 *   comparing what was written to what it reads out of external flash. We
 *   only want to try twice. Any more is too much more.
 *
 * @param[in]
 *  p_buffer - pointer to a buffer holding what is to be written to the flash chip.
 * @param[in]
 *  p_bufferVerify - pointer to a buffer used to verify that the command succeeded
 * @param[in]
 *  address - address of the page to program
 * @param[in]
 *  length - number of bytes to write. This should be no larger than 256.
 *
 * @return bool - true, write completed and was verified; false, write failed
 ******************************************************************************/
bool Flash_WriteAndVerify(uint8_t *p_buffer, uint8_t *p_bufferVerify, uint32_t address, uint16_t length)
{
    bool status;
    bool writtenOK;
    uint32_t retryCount;
    uint32_t i;

    retryCount = 0;
    status = true;
    writtenOK = false;

    /** Try to write to the flash. Give it two shots. */
    while ((retryCount < 2) && (false == writtenOK))
    {
        /* Protect the flash write from interrupts */
        USART_IntDisable(USART1, UART_IEN_TXBL);

        /* Do the write */
        status = Flash_WriteToAddress(&p_buffer[0], address, length);

        /* Enable interrupt on USART TX Buffer*/
        USART_IntEnable(USART1, UART_IEN_TXBL);

        /* Temporarily set to value of 'status' so that the validation
         * loop can run properly. */
        writtenOK = status;

        /* Now, try to verify what was written. */
        if (status)
        {
            status = Flash_ReadFromAddress (&p_bufferVerify[0], address, length);
            for ( i=0; i<length; ++i)
            {
                if (p_buffer[i] != p_bufferVerify[i])
                {
                    /* There is a mismatch, end the checking loop and set
                     * writtenOK to false so that we'll retry the write. */
                    writtenOK = false;
                    i = length;
                }
            }
        }

        ++retryCount;
    }

    // The status of writtenOK indicates whether we were successful. 
    return writtenOK;
}

/***************************************************************************//**
 * @brief
 *   Erase a range of addresses.
 *
 * @note
 *   This function attempts to erase all the subsectors from the given start
 *   address to the ending address. It will only execute if the starting address
 *   begins on a subsector boundary.
 *
 *
 * @param[in]
 *  startAddress - starting address of the range to erase
 *
 * @param[in]
 *  endAddress - ending address of the range to erase
 *
 *
 * @return bool - true, successful erase; false, erase failed
 ******************************************************************************/
bool Flash_EraseRange(uint32_t startAddress, uint32_t endAddress)
{
    bool retval = false;
    uint32_t eraseAddress;

    if ((startAddress & (EXTERNAL_SUBSECTOR_SIZE-1)) != 0x00000000)
    {
        /* Return failure. */
        retval = false;
    }
    else if (!flash_is_initialized)
    {
        retval = false;
    }
    else
    {
        /* First Erase Address will be the startAddress */
        eraseAddress = startAddress;

        /* Erase each subsector until we get to the endAddress */
        while( eraseAddress < endAddress )
        {
            /* First, attempt to erase the sub sector */
            retval = Flash_SubSectorErase(eraseAddress);

            /* Handle results of the erase operation */
            if (retval)
            {
                /* get ready to erase the next subsector */
                eraseAddress += EXTERNAL_SUBSECTOR_SIZE;
            }
            else
            {
                /* Force the loop to exit. */
                eraseAddress = endAddress;
            }
        }
    }

    /* All done! */
    return retval;
}

/***************************************************************************//**
 * @brief
 *   Function to test the write and read commands.
 *
 * @note
 *   This function writes a string to the external flash, waits a few seconds,
 *   then reads the out what is in flash. One expects the read value to be the
 *   same is the written value! *
 *
 * @param[out] none
 *
 * @return bool - Indicates success (true) or failure (false).
 ******************************************************************************/
bool    Flash_TestWriteRead (void)
{
    uint8_t    testBuffer[32];
    char    debugBuffer[64];
    #define EXTERN_ADDRESS  0x2FE0

    Uart_Uart1WriteString ("\n\rTesting flash read/write \n\r");

    /** Erase the subsector that we'll play with. */
    Flash_SubSectorErase(EXTERN_ADDRESS);

    /** Read the results of the erasure */
    DEBUG_INSTRUCTION(Flash_ReadFromAddress(testBuffer, EXTERN_ADDRESS, 16));
    DEBUG_INSTRUCTION(sprintf (debugBuffer, "...Erase Result: %s\n\r", testBuffer));
    DEBUG_OUTPUT_TEXT (debugBuffer);

    /** Write a message to the flash chip */
    memset(testBuffer, 0x00, sizeof(testBuffer));
    strcpy ((char *)testBuffer, "Test the Neptune Test String!");
    Flash_WriteToAddress(testBuffer, EXTERN_ADDRESS, 32);

    /** Read the result of the write */
    memset(testBuffer, 0x00, sizeof(testBuffer));
    memset(debugBuffer, 0x00, sizeof(debugBuffer));
    Flash_ReadFromAddress(testBuffer, EXTERN_ADDRESS, 32);
    sprintf (debugBuffer, "...Page Program Result: %s\n\r", testBuffer);
    Uart_Uart1WriteString (debugBuffer);

    Uart_Uart1WriteString ("...Flash Testing Complete!\n\r");

    return true;
}
