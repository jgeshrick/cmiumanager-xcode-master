/******************************************************************************
*******************************************************************************
**
**         Filename: cmiuFlash.h
**
**           Author: Barry Huggins
**          Created: 12/31/14
**
**     Last Edit By: Brian Arnberg
**        Last Edit: 2015.07.13
**
** Revision History:
**       2015.07.13: Updated, added subroutines, renamed, made common. - BWA
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __CMIU_FLASH_H
#define __CMIU_FLASH_H

#include "app_adc.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/

/******** InstructionsCode ********/
#define SPI_FLASH_INS_DUMMY 0xAA                        /* dummy byte*/
enum
{
    //Instruction set
    SPI_FLASH_INS_WREN        = 0x06,           /* write enable*/
    SPI_FLASH_INS_WRDI        = 0x04,           /* write disable*/
    SPI_FLASH_INS_RDID        = 0x9F,           /* Read Identification 20 bytes*/
    /*SPI_FLASH_INS_RDID1         = 0x9E,        Read Identification 3 bytes*/
    SPI_FLASH_INS_RDSR        = 0x05,           /* read status register*/
    SPI_FLASH_INS_WRSR        = 0x01,           /* write status register*/
    SPI_FLASH_INS_WRLR        = 0xE5,           /* Write to Lock Register*/
    SPI_FLASH_INS_RDLR        = 0xE8,           /* Read Lock Register*/
    SPI_FLASH_INS_READ        = 0x03,           /* read data bytes*/
    SPI_FLASH_INS_FAST_READ   = 0x0B,           /* read data bytes at higher speed*/
    SPI_FLASH_INS_DOFR        = 0x3B,           /*Dual output Fast Read*/
    SPI_FLASH_INS_DIFP        = 0xA2,           /*Dual input Fast Read*/
    SPI_FLASH_INS_ROTP        = 0x4B,           /*Read OTP(Read 64 Bytes of OTP area)*/
    SPI_FLASH_INS_POTP        = 0x42,           /*Program OTP(program 64 Bytes of OTP area)*/
    SPI_FLASH_INS_PP          = 0x02,           /* page program*/
    SPI_FLASH_INS_SSE         = 0x20,           /* Subsector erase*/
    SPI_FLASH_INS_SE          = 0xD8,           /* sector erase*/
    SPI_FLASH_INS_BE          = 0xC7,           /* bulk erase*/
    #ifndef NO_DEEP_POWER_DOWN_SUPPORT
    SPI_FLASH_INS_RDP         = 0xAB,           /* release from deep power-down*/
    SPI_FLASH_INS_DP          = 0xB9,           /* deep power-down*/
    #endif
};

/**** Status register bits */

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
/** Work in Progress Bit; set when the chip is
 ** writing or erasing something. */
#define WIP 0x01

/** Write Enable Latch; set when it's OK to
 ** erase or write to the flash. */
#define WEL 0x02

/** These are the bits that need to be included when verifying either
 ** WIP clearing or WEL setting. */
#define CHECK_MASK  (BIT6 | WEL | WIP)

/** Full size of the external flash memory. */
#define EXTERNAL_FULL_SIZE         2097152

/** There are 32 sectors in the external flash memory. */
#define EXTERNAL_SECTOR_SIZE       65536

/** There are 16 subsectors per sector of the external flash memory. */
#define EXTERNAL_SUBSECTOR_SIZE    4096

/** There are 16 pages per subsector. */
#define EXTERNAL_PAGE_SIZE         256

/** Max time to wait for the Work In Progress (WIP) bit */
#define FLASH_WAIT_ON_WIP_MSEC         10

/** Minimum operating voltage */
#define MINIMUM_FLASH_VOLTAGE   BATT_ADC_2V3

/** Maximum number of users allowed to access flash. Honestly, this is an
    arbitrary number.*/
#define MAXIMUM_FLASH_USERS 15


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
void    Flash_startup(void);
bool    Flash_init(void);
void    Flash_force_Deinit(void);
void    Flash_Deinit(void);
void    Flash_PowerOn(void);
void    Flash_PowerOff(void);
bool    Flash_isPowered(void);
bool    Flash_WriteEnable(void);
bool    Flash_WriteDisable(void);


uint16_t    Flash_ReadDeviceIdentification(void);
uint8_t     Flash_ReadStatusRegister(void);
bool    Flash_WaitOnWip(uint32_t delTicks);
bool    Flash_WaitOnWel(uint32_t delTicks);
bool    Flash_SubSectorErase(uint32_t address);
bool    Flash_BulkErase(void);
bool    Flash_ReadFromAddress(uint8_t *buffer, uint32_t address, uint32_t length);
bool    Flash_WriteToAddress(uint8_t *buffer, uint32_t address, uint16_t length);
bool    Flash_WriteAndVerify(uint8_t *buffer, uint8_t *bufferVerify, uint32_t address, uint16_t length);
bool    Flash_EraseRange(uint32_t startAddress, uint32_t endAddress);
bool    Flash_TestWriteRead (void);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __CMIU_FLASH_H

