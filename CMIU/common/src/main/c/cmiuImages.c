/******************************************************************************
*******************************************************************************
**
**         Filename: cmiuImages.c
**
**           Author: Brian Arnberg
**          Created: 2015.06.30
**
**     Last Edit By: Brian Arnberg
**        Last Edit: Moved to CMIU\common
**
**
** Revision History:
**       2015.06.30: First created.
**       2015.07.13: Moved to CMIU\Common and renamed internal files as necessary.
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file cmiuImages.c
*
* @brief This file contains the routines used to move images between
*        internal and external memory. It depends on the flash module.
*
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdio.h>
#include "typedefs.h"
#include "cmiuFlash.h"
#include "cmiuImages.h"
#include "MemoryMap.h"

#ifndef CMIU_UNIT_TEST
#include "app_cmit_interface.h"
#endif

#ifndef BOOTLOADER
#include "app_scratch_ram.h"
#endif

#include "em_msc.h"
#include "app_timers.h"
#include "app_uart.h"
//#include "app_cmit_interface.h"

/*=========================================================================*/
/*  L O C A L   G L O B A L   V A R I A B L E S                            */
/*=========================================================================*/
#ifdef BOOTLOADER
static uint8_t bigBuffer[INTERNAL_PAGE_SIZE] SCRATCH_RAM_VARS;
#else
static uint8_t* bigBuffer;
#endif

/*=========================================================================*/
/*  L O C A L   F U N C T I O N   P R O T O T Y P E S                      */
/*=========================================================================*/
static void cmiuImages_MscWritePage(uint32_t address, uint32_t length);

/*=========================================================================*/
/*  L O C A L   F U N C T I O N S                                          */
/*=========================================================================*/
/***************************************************************************//**
 * @brief
 *  Write up to a page from the global buffer to internal flash.
 *
 * @note
 *
 * @param[in]
 *  address - internal memory address where the buffer will be stored.
 *
 * @param[in]
 *  length - length of the buffer to write to the internal memory. May be no
 *  larger than one flash page (2048 bytes).
 *
 * @return None
 ******************************************************************************/
static void cmiuImages_MscWritePage(uint32_t address, uint32_t length)
{
    uint32_t i;
    uint32_t j;
    msc_Return_TypeDef mscRetval;

    if (length > FLASH_PAGE_SIZE)
    {
        length = FLASH_PAGE_SIZE;
    }

    /** Make 4 attempts to write the page. */
    for(i=4;i>0;--i)
    {

        /** Get ready to do stuff to internal flash! */
        MSC_Init();

        /** If this is the start of a new page, erase it before you write. */
        if (((address) & (FLASH_PAGE_SIZE - 1)) == 0)
        {
            /** Erase the page. */
            mscRetval = MSC_ErasePage((uint32_t *)address);
        }

        /** Write to the page.*/
        for (j=0; j < length; j+=4)
        {
            mscRetval = MSC_WriteWord((uint32_t *)(address+j), (bigBuffer+j), 4);
        }

        /** Alright, we're done doing stuff to internal memory,
         ** so lets kill the MSC */
        MSC_Deinit();

        if (mscRetval == 0)
        {
            /** This will be decremented to 0 by the for loop,
             ** thus killing the loop. */
            i = 1;
        }
    }
}


/*=========================================================================*/
/*  G L O B A L   F U N C T I O N S                                        */
/*=========================================================================*/
/***************************************************************************//**
 * @brief
 *  Store an image in internal memory to its backup location.
 *
 * @note
 *  This function stores an image in internal memory to external memory. The
 *  external memory has a page size of 256; thus the internal image must
 *  be broken into 256 byte chunks, then programmed. Each external memory
 *  subsector (4kB) must be erased before anything can be written to it.
 *  when the routine comes to the start of a subsector, it will erase the
 *  subsector before trying to write to any of its 16 pages.
 *
 * @param[in]
 *  internalImage - Image information for the image to backup
 *
 * @param[in]
 *  externalImage - Image information for where the function should store the
 *  backup.
 *
 * @return None
 ******************************************************************************/
void cmiuImages_putExternal(const ImageInformation_t *internalImage,
                            const ImageInformation_t *externalImage)
{
    uint32_t remainingBytes;
    uint32_t externalAddress;
    uint32_t internalAddress;
    uint16_t i;
    uint8_t  status;
    DEBUG_INSTRUCTION(uint32_t helper);
    DEBUG_INSTRUCTION(uint32_t helper2);

    #ifndef BOOTLOADER
    bigBuffer = app_scratch_ram_get_256();
    #endif

    /** Make sure the images match length */
    if (internalImage->Length != externalImage->Length)
    {
        remainingBytes = 0UL;
        externalAddress = 0xFFFFFFFF; /* This is an invalid address */
        internalAddress = 0xFFFFFFFF; /* This is an invalid address */
    }
    else
    {
        /** The length is always presented without the CRC, so that needs
         ** to be accounted for, here. */
        remainingBytes = (internalImage->Length) + (uint32_t)CRC_SIZE;
        externalAddress = (uint32_t)(externalImage->Address);
        internalAddress = (uint32_t)(internalImage->Address);
    }

    /** Exit subroutine on illegal address. */
    if ((BOOTLOADER_END_ADDRESS > internalAddress) ||
        ((APPLICATION_END_ADDRESS+1) < internalAddress) ||
        (BACKUP_CONFIG_START_ADDRESS > externalAddress) ||
        ((NEW_APP_END_ADDRESS+1) < externalAddress) ||
        ((uint32_t) externalImage == 0UL) ||
        ((uint32_t) externalImage == ~(0UL)) ||
        ((uint32_t) internalImage == 0UL) ||
        ((uint32_t) internalImage == ~(0UL)))
    {
        DEBUG_OUTPUT_TEXT("Get External Image Error.\r\n");
        return;
    }

    /** Initialize communications with the external flash */
    DEBUG_INSTRUCTION(bigBuffer[0] = (uint8_t)(internalImage->ImageType));
    DEBUG_OUTPUT_TEXT_AND_DATA("...Backup image number: ", bigBuffer, 1);
    DEBUG_INSTRUCTION(helper = Timers_GetMsTicks());
    Flash_init();

    /* Erase the destination image space */
    Flash_EraseRange(externalAddress, (externalAddress + remainingBytes));

    /* Write the image to external flash */
    while(remainingBytes > 0)
    {
        if (remainingBytes >= EXTERNAL_PAGE_SIZE)
        {
            /** Fill the buffer from internal memory */
            for(i=0;i<EXTERNAL_PAGE_SIZE;++i)
            {
                bigBuffer[i] = *(uint8_t *)internalAddress;
                ++internalAddress;
            }

            status = Flash_WriteToAddress(bigBuffer,externalAddress,EXTERNAL_PAGE_SIZE);

            if (!(status))
            {
                DEBUG_OUTPUT_TEXT_AND_DATA("This one failed: ",(uint8_t *)&externalAddress,4);
            }

            externalAddress += EXTERNAL_PAGE_SIZE;
            remainingBytes -= EXTERNAL_PAGE_SIZE;
        }
        else
        {
            /** Fill the buffer from internal memory */
            for(i=0;i<remainingBytes;++i)
            {
                bigBuffer[i] = *(uint8_t *)internalAddress;
                ++internalAddress;
            }

            Flash_WriteToAddress(bigBuffer,externalAddress,remainingBytes);

            remainingBytes = 0;
        }
    }

    /** Deitialize communications with the external flash */
    Flash_Deinit();
    DEBUG_INSTRUCTION(helper2 = Timers_GetMsTicks());
    DEBUG_INSTRUCTION(helper = helper2 - helper);
    DEBUG_OUTPUT_TEXT_AND_DATA("...Time: ", (uint8_t *)&helper,4);
}

/***************************************************************************//**
 * @brief
 *  Retrieve an image from external memory and store it in internal memory.
 *
 * @note
 *
 * @param[in]
 *  *externalImage - Image location information for the image that needs to be
 *  gotten from external memory.
 *
 * @param[in]
 *  *internalImage - Image location information for the internal image that will
 *  be overwritten with the image in external memory.
 *
 * @return None
 ******************************************************************************/
void cmiuImages_getExternal(const ImageInformation_t *externalImage,
                            const ImageInformation_t *internalImage)
{
    uint32_t remainingBytes;
    uint32_t externalAddress;
    uint32_t internalAddress;
    DEBUG_INSTRUCTION(uint32_t helper);
    DEBUG_INSTRUCTION(uint32_t helper2);

    /** Make sure the images match length */
    if (externalImage->Length != internalImage->Length)
    {
        remainingBytes = 0UL;
        externalAddress = 0xFFFFFFFF; /* This is an invalid address */
        internalAddress = 0xFFFFFFFF; /* This is an invalid address */
    }
    else
    {
        /** The length is always presented without the CRC, so that needs
         ** to be accounted for, here. */
        remainingBytes = (externalImage->Length) + (uint32_t)CRC_SIZE;
        externalAddress = (uint32_t)(externalImage->Address);
        internalAddress = (uint32_t)(internalImage->Address);
        #ifndef BOOTLOADER
        uint32_t length;
        bigBuffer = app_scratch_ram_get(&length);
        #endif
    }

    /** Exit subroutine on illegal address. */
    if ((BOOTLOADER_END_ADDRESS > internalAddress) ||
        ((APPLICATION_END_ADDRESS+1) < internalAddress) ||
        (BACKUP_CONFIG_START_ADDRESS > externalAddress) ||
        ((NEW_APP_END_ADDRESS+1) < externalAddress) ||
        ((uint32_t) externalImage == 0UL) ||
        ((uint32_t) externalImage == ~(0UL)) ||
        ((uint32_t) internalImage == 0UL) ||
        ((uint32_t) internalImage == ~(0UL)))
    {
        DEBUG_OUTPUT_TEXT("Get External Image Error.\r\n");
        return;
    }

    /** Initialize communications with the external flash */
    DEBUG_INSTRUCTION(bigBuffer[0] = (uint8_t)(externalImage->ImageType));
    DEBUG_OUTPUT_TEXT_AND_DATA("...Getting image number: ", bigBuffer, 1);
    DEBUG_INSTRUCTION(helper = Timers_GetMsTicks());
    Flash_init();

    /** Now that everything is setup, let's try to actually take the
     *  image from external memory and store it internally, page by
     *  page. */
    while(remainingBytes > 0)
    {
        /** Do the copy, depending on the remaining number of bytes. */
        if (remainingBytes >= READING_BLOCK_SIZE)
        {
            /** Fill the buffer from external memory */
            Flash_ReadFromAddress(bigBuffer, externalAddress, READING_BLOCK_SIZE);
            /** Store the buffer to internal memory */
            cmiuImages_MscWritePage(internalAddress, READING_BLOCK_SIZE);
            /** Prepare for the next block */
            remainingBytes -= READING_BLOCK_SIZE;
            externalAddress += READING_BLOCK_SIZE;
            internalAddress += READING_BLOCK_SIZE;
        }
        else
        {
            /** Fill the buffer from external memory */
            Flash_ReadFromAddress(bigBuffer, externalAddress, remainingBytes);
            /** Store the buffer to internal memory */
            cmiuImages_MscWritePage(internalAddress, remainingBytes);
            /** Set remainingBytes to "0" so that we exit this loop. */
            remainingBytes = 0;
        }
    }

    /** Deitialize communications with the external flash */
    Flash_Deinit();
    DEBUG_INSTRUCTION(helper2 = Timers_GetMsTicks());
    DEBUG_INSTRUCTION(helper = helper2 - helper);
    DEBUG_OUTPUT_TEXT_AND_DATA("...Time: ", (uint8_t *)&helper,4);
}
