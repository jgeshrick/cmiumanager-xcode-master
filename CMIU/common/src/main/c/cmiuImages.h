/******************************************************************************
*******************************************************************************
**
**         Filename: cmiuImages.h
**
**           Author: Brian Arnberg
**          Created: 2015.06.30
**
**     Last Edit By: Brian Arnberg
**        Last Edit: Moved to CMIU\common
**
**
** Revision History:
**       2015.06.30: First created.
**       2015.07.13: Moved to CMIU\Common and renamed internal files as necessary.
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __CMIU_IMAGES_H
#define __CMIU_IMAGES_H


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
#define INTERNAL_PAGE_SIZE 2048
/** The maximum size we can pass to the DMA controller is 1024, so we need to
 * limit this to 1024. */
#define READING_BLOCK_SIZE 1024



/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
void cmiuImages_putExternal(const ImageInformation_t *internalImage,
                            const ImageInformation_t *externalImage);
void cmiuImages_getExternal(const ImageInformation_t *externalImage,
                            const ImageInformation_t *internalImage);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __CMIU_IMAGES_H

