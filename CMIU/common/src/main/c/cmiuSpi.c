/******************************************************************************
*******************************************************************************
**
**         Filename: cmiuSpi.c
**
**           Author: Brian Arnberg
**          Created: 2015.12.22
**
** Revision History:
**       2015.12.22: First created. - BWA
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file cmiuSpi.c
*
* @brief This file is used to allow different modules to gracefully gain
*        control of SPI communications. Normally, it should be sufficient
*        to simply toggle the correct CS line. However, the different
*        modules are using the SPI bus differently (different endianess,
*        differnt baud rates, etc.), so we have to reconfigure the SPI
*        bus each time we want to transfer control.
* @note  This is currently designed to work with two periferal devices:
*        1. External Nand Flash (Micron M25PX16)
*        2. Nordic BTLE (nRF8001)
*
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdio.h>
#include <string.h>
#include "typedefs.h"
#include "cmiuSpi.h"

#ifndef CMIU_UNIT_TEST
#include "app_cmit_interface.h"
#endif

#include "em_device.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "em_cmu.h"
#include "efm32lg_uart.h"
#include "app_uart.h"

/*=========================================================================*/
/*  L O C A L   G L O B A L   V A R I A B L E S                            */
/*=========================================================================*/
static bool     btle_enabled;
static bool     flash_enabled;

/*=========================================================================*/
/*  P R I V A T E   F U N C T I O N   P R O T O T Y P E S                  */
/*=========================================================================*/
static void cmiuSpi_USART2_setup_flash(void);
static void cmiuSpi_USART2_setup_btle(void);

/*=========================================================================*/
/*  P U B L I C   F U N C T I O N S                                        */
/*=========================================================================*/
/***************************************************************************//**
 * @brief
 *   Make sure variables are initialized and that the proper clocks are
 *   enabled.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void cmiuSpi_init(void)
{
    /* Enable clock for USART2 */
    CMU_ClockEnable(cmuClock_USART2, true);

    /* By default btle and flash are both disabled. */
    btle_enabled = false;
    flash_enabled = false;
}

/***************************************************************************//**
 * @brief
 *   Initializes GPIO pins and USART2 for flash operation.
 *
 * @note
 *  Store the current GPIO settings, then setup all the GPIO pins, setup USART2
 *  to operate the flash communications, and finally power on the flash chip.
 *
 * @param[in] None
 *
 * @return flash_enabled - true means it was enabled; false means it wasn't
 *         enabled.
 ******************************************************************************/
bool cmiuSpi_flash_enable(void)
{
    /** Setup GPIO for USART2 - From autogen_init.c */
    /*********** USART 2 - SPI: PC2 = MOSI, PC3 = MISO, PC4 = SCK, PC5 = CS */
    /* To avoid false start, configure output US2_TX as high on PC2,
     * Pin PC2 is configured to Push-pull */
    GPIO_PinModeSet(gpioPortC, (FLASH_MOSI), gpioModePushPullDrive, 1);
    /* Pin PC3 is configured to Input enabled */
    GPIO_PinModeSet(gpioPortC, (FLASH_MISO), gpioModeInput, 0);
    /* Pin PC4 is configured to Push-pull */
    GPIO_PinModeSet(gpioPortC, (FLASH_SCLK), gpioModePushPullDrive, 0);
    /* To avoid false start, configure output US2_CS as high on PC5.
     * Pin PC5 (flash CS) is configured to Push-pull. */
    GPIO_PinModeSet(gpioPortC, (FLASH_CS), gpioModePushPullDrive, 1);

    /** Call the USART2 setup routine */
    cmiuSpi_USART2_setup_flash();

    /* set REQN to prevent the BLE chip from trying to use the SPI lines */
    GPIO_PinOutSet(gpioPortC, (BLE_ACI_REQN));

    /* Disable nRF8001 (PA2, RESET) - active low */
    GPIO_PinOutSet(gpioPortA, (BLE_ACI_RESET));

    flash_enabled = true;

    return flash_enabled;
}

/***************************************************************************//**
 * @brief
 *  Undo the changes made when cmiuSpi_flash_enable was called.
 *
 * @note
 *  This reverts the GPIO pins the their prior configuration and returns
 *  the USART settings to whatever they were before cmiuSpi_flash_enable was
 *  called.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void cmiuSpi_flash_disable(void)
{
    /** Set flash_enabled to false; flash will be considered disabled now. */
    flash_enabled = false;

    /** If btle was enabled, we'll need to reconfigure SPI comms for it. */
    if (btle_enabled)
    {
        GPIO_PinModeSet(gpioPortC,(BLE_ACI_MISO), gpioModeInput, 0);
        GPIO_PinModeSet(gpioPortC,(BLE_ACI_MOSI), gpioModePushPull, 0);
        GPIO_PinModeSet(gpioPortC,(BLE_ACI_SCLK), gpioModePushPull, 0);
        GPIO_PinModeSet(gpioPortC,(BLE_ACI_RDYN), gpioModeInputPull, 0);
        GPIO_PinModeSet(gpioPortC,(BLE_ACI_REQN), gpioModePushPull, 1);

        cmiuSpi_btle_enable();

        GPIO_PinOutClear(gpioPortC, (BLE_ACI_MISO));
        GPIO_PinOutClear(gpioPortC, (BLE_ACI_MOSI));
        GPIO_PinOutSet(gpioPortC, (BLE_ACI_REQN));
        GPIO_PinOutClear(gpioPortC, (BLE_ACI_SCLK));
    }
    else
    {
        /* Init USART2 registers to HW reset state. */
        USART_Reset(USART2);

        /** Return PortC and Port A to their previous values. */
        GPIO_PinModeSet(gpioPortC, (FLASH_MOSI), gpioModeDisabled, 0);
        GPIO_PinModeSet(gpioPortC, (FLASH_MISO), gpioModeDisabled, 0);
        GPIO_PinModeSet(gpioPortC, (FLASH_SCLK), gpioModeDisabled, 0);
        GPIO_PinModeSet(gpioPortC, (FLASH_CS), gpioModeDisabled, 0);
        
        /* set REQN to prevent the BLE chip from trying to use the SPI lines */
        GPIO_PinOutClear(gpioPortC, (BLE_ACI_REQN));

        /* Disable nRF8001 (PA2, RESET) - active low */
        GPIO_PinOutClear(gpioPortA, (BLE_ACI_RESET));
  
    }
}

/***************************************************************************//**
 * @brief
 *   Return whether the SPI connection for flash is enabled.
 *
 * @note
 *
 * @param[out] none
 *
 * @return bool - flash is enabled (true), flash disabled (false)
 ******************************************************************************/
bool cmiuSpi_is_flash_enabled(void)
{
    return flash_enabled;
}

/***************************************************************************//**
 * @brief
 *   Enable GPIO and USART2 for btle communications.
 *
 * @note
 *  This function checks whether SPI is configured for communication with the
 *  external flash device before it does anything. If flash is already setup,
 *  this does nothing (because flash has a higher priority than btle). If
 *  flash is not setup, this function configures USART2.
 *
 * @param[out] none
 *
 * @return btle_enabled - btle was enabled (true), btle was not enabled (false)
 ******************************************************************************/
bool cmiuSpi_btle_enable(void)
{
    /** Flash has higher priority, so we'll only enable btle stuff if flash
     *  is currently not enabled. */
    if (!(flash_enabled))
    {
        /** Configure USART2 for communication with the btle. */
        cmiuSpi_USART2_setup_btle();

        /** Set btle_enabled */
        btle_enabled = true;
    }

    return btle_enabled;
}

/***************************************************************************//**
 * @brief
 *   Disable GPIO and USART2 for btle communications.
 *
 * @note
 *  This disables USART2 communications and returns GPIO lines to their values
 *  prior to cmiuSpi_btle_enable.
 *
 * @param[out] none
 *
 * @return none
 ******************************************************************************/
void cmiuSpi_btle_disable(void)
{
    /* Init USART2 registers to HW reset state. */
    USART_Reset(USART2);

    /** Disable all the port lines associated with BTLE. */
    GPIO_PinModeSet(gpioPortC,(BLE_ACI_MISO), gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortC,(BLE_ACI_MOSI), gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortC,(BLE_ACI_SCLK), gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortC,(BLE_ACI_RDYN), gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortC,(BLE_ACI_REQN), gpioModeDisabled, 1);

    btle_enabled = false;
}

/***************************************************************************//**
 * @brief
 *   Return whether the SPI connection for btle is enabled.
 *
 * @note
 *
 * @param[out] none
 *
 * @return bool - btle is enabled (true), btle disabled (false)
 ******************************************************************************/
bool cmiuSpi_is_btle_enabled(void)
{
    return btle_enabled;
}

/***************************************************************************//**
 * @brief
 *   Function to write the specified data to the SPI output.
 *
 * @note
 *   This function writes the given data to the SPI output. This SPI output
 *   is on USART2.
 *
 *
 * @param[out] pData
 *               points to the data to be sent out the SPI
 * @param[out] len
 *               gives the number of bytes to be output
 *
 * @return bool - Indicates success (true) or failure (false).
 ******************************************************************************/
bool cmiuSpi_flash_write(uint8_t * const pData, uint32_t len)
{
    int16_t     i;

    for (i=0; i<len; i++)
    {
        USART_Tx(USART2, pData[i]);
    }

    return true;
}

/***************************************************************************//**
 * @brief
 *   Function to read the specified data from the SPI output.
 *
 * @note
 *   This function reads the given number of bytes from the SPI output. This SPI
 *   is read from  USART2.
 *
 *
 * @param[in] pData
 *               points to the buffer for the incoming data from the SPI
 * @param[out] len
 *               gives the number of bytes to be input
 *
 * @return bool - Indicates success (true) or failure (false).
 ******************************************************************************/
bool cmiuSpi_flash_read(uint8_t * const pData, uint32_t len)
{
    int16_t     i;

    for (i=0; i<len; i++)
    {
        pData[i] = USART_Rx(USART2);
    }

    return true;
}

/*=========================================================================*/
/*  P R I V A T E   F U N C T I O N S                                      */
/*=========================================================================*/
/***************************************************************************//**
 * @brief
 *  Configure USART2 for communications with the external flash.
 *
 * @note
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
static void cmiuSpi_USART2_setup_flash(void)
{
    USART_InitSync_TypeDef init = USART_INITSYNC_DEFAULT;

    init.baudrate     = 1000000;
    init.databits     = usartDatabits8;
    init.msbf         = 1;
    init.master       = 1;
    init.clockMode    = usartClockMode0;
    init.prsRxEnable  = 0;
    init.autoTx       = 1;

    USART_InitSync(USART2, &init);

    /* Enable signals TX, RX, CLK, CS */
    USART2->ROUTE |= USART_ROUTE_TXPEN | USART_ROUTE_RXPEN | USART_ROUTE_CLKPEN | USART_ROUTE_CSPEN;

    /* Use auto chip select, so that we don't have to manually
     * toggle the CS line. */
    USART2->CTRL |= USART_CTRL_AUTOCS;
}

/***************************************************************************//**
 * @brief
 *  Configure USART2 for communications with the external btle.
 *
 * @note
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
static void cmiuSpi_USART2_setup_btle(void)
{
    /** The following was pulled from hal_aci_tl.cpp */
    USART_InitSync_TypeDef initSync = {
    .enable = usartEnable,          //enable RX and TX
    .refFreq = 0,                   //use currently configured clock
    .baudrate = 115200,
    .databits = usartDatabits8,
    .master = 1,
    .msbf = 0,
    .clockMode = usartClockMode0,   //clock idle low, sample on rising edge
    .prsRxEnable = 0,
    .prsRxCh = usartPrsRxCh0,
    .autoTx = 0
  };

  USART_InitSync(USART2, &initSync);

  USART2->ROUTE |= USART_ROUTE_CSPEN | // used to use location #2
                   USART_ROUTE_RXPEN |
                   USART_ROUTE_TXPEN |
                   USART_ROUTE_CLKPEN;
}

