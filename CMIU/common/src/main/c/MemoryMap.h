/******************************************************************************
*******************************************************************************
**
**         Filename: MemoryMap.h
**
**           Author: Brian Arnberg
**          Created: 2015.06.25
**
**     Last Edit By:
**        Last Edit:
**
** Revision History:
**         xx/xx/xx:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of
**    Neptune Technology Group. The use, copying, transfer or disclosure
**    of such information is prohibited except by express written
**    agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*=========================================================================*/
/* D E F I N I T I O N S                                                   */
/*=========================================================================*/

#ifndef __MEMORY_MAP_H
#define __MEMORY_MAP_H

#define UI_FLASH_SIZE   0x40000

/** External Flash Page Size */
#define EXTERNAL_FLASH_PAGE 256
#define INFO_BLOCK_SIZE     16
#define CRC_SIZE            4

/** Attribute Names: The following are used to force certain variables into
 *  specific parts of memory. They also ensure that the linker always includes
 *  the variables in the output file. */
#define APPLICATION_INFO_ATTRIBUTE __attribute((section("ApplicationInfo"),used))
#define BOOTLOADER_SPECIAL_ATTRIBUTE __attribute((section("BootloaderSpecial"),used))
#define CMIU_ID_ATTRIBUTE __attribute((section("CmiuID"),used))
#define CMIU_ID_PUB_ATTRIBUTE __attribute((section("CmiuIdPub"),used))
#define CMIU_ID_SUB_ATTRIBUTE __attribute((section("CmiuIdSub"),used))
#define CMIU_ID_CLIENT_ATTRIBUTE __attribute((section("CmiuIdClient"),used))
#define BOOTLOADER_INFO_ATTRIBUTE __attribute((section("BootloaderInfo"),used))
#define CONFIGURATION_ATTRIBUTE __attribute((section("Configuration"),used))
#define IMAGE_LIST_ATTRIBUTE __attribute((section("ImageList"),used))
#define CONFIGURATION_INFO_ATTRIBUTE __attribute((section("ConfigurationInfo"),used))
#define BLE_CONFIG_ATTRIBUTE __attribute((section("BleConfig"),used))
#define BLE_CONFIG_INFO_ATTRIBUTE __attribute((section("BleConfigInfo"),used))
#define KEY_TABLE_ATTRIBUTE __attribute((section("KeyTable"),used))
#define KEY_TABLE_INFO_ATTRIBUTE __attribute((section("KeyTableInfo"),used))

/** RAM Attribute Names */
#define RESET_SOURCE_VAR    __attribute((section("ResetSource"),zero_init,used))
#define CRC_STATUS_VAR      __attribute((section("SumCrcStatus"),zero_init,used))
#define VERSION_STATUS_VAR  __attribute((section("SumVersionStatus"),zero_init,used))
#define DATALOG_VAR         __attribute((section("DatalogVariables"),zero_init,used))
#define SCRATCH_RAM_VARS    __attribute((section("ScratchRam"),zero_init))

/** RAM Mapping */
#define SHARED_RAM              0x20000000
#define SHARED_RAM_SIZE         0x00000080
#define APPLICATION_RAM         0x20000080
#define APPLICATION_RAM_SIZE    0x00005E40
#define SCRATCH_RAM             0x20006000
#define SCRATCH_RAM_SIZE        0x00001C00
#define STACK_START             0x20007C00
#define STACK_SIZE              0x00000400

/** RAM Mapped Variables */
#define TOTAL_SHARED_VARIABLES  3
#define LINKER_VARIABLE_BOUND   4
#define SHARED_VARIABLES_SIZE   ((TOTAL_SHARED_VARIABLES) * LINKER_VARIABLE_BOUND)

/** Bootloader */
#define BOOTLOADER_START_ADDRESS    0x0000
#define BOOTLOADER_END_ADDRESS      0x67FF
#define BOOTLOADER_SIZE             ((BOOTLOADER_END_ADDRESS - BOOTLOADER_START_ADDRESS)+1)
#define BOOTLOADER_INFO             (BOOTLOADER_END_ADDRESS - (INFO_BLOCK_SIZE-1))
#define BOOTLOADER_CRC              (BOOTLOADER_END_ADDRESS - (CRC_SIZE-1))
#define BOOTLOADER_TOTAL_RECORDS    (BOOTLOADER_SIZE/EXTERNAL_FLASH_PAGE)

/** MiuID and ID String Locations in ROM */
#define CMIU_ID_LOCATION        0x00006400
#define CMIU_ID_PUB_LOCATION    0x00006410
#define CMIU_ID_SUB_LOCATION    0x00006430
#define CMIU_ID_CLIENT_LOCATION 0x00006450

/****** INTERNAL IMAGES ************/
/** Application Configuration */
#define CONFIGURATION_START_ADDRESS 0x6800
#define CONFIGURATION_END_ADDRESS   0x6FFF
#define CONFIGURATION_SIZE          ((CONFIGURATION_END_ADDRESS - CONFIGURATION_START_ADDRESS)+1)
#define CONFIGURATION_INFO          (CONFIGURATION_END_ADDRESS - (INFO_BLOCK_SIZE-1))
#define CONFIGURATION_CRC           (CONFIGURATION_END_ADDRESS - (CRC_SIZE-1))
#define CONFIGURATION_TOTAL_RECORDS (CONFIGURATION_SIZE/EXTERNAL_FLASH_PAGE)

/** Image List Address (within Application Configuration Space) */
#define IMAGE_LIST_OFFSET           0x0400
#define IMAGE_LIST_ADDRESS          (CONFIGURATION_START_ADDRESS + IMAGE_LIST_OFFSET)
#define IMAGE_LIST_SIZE             (CONFIGURATION_SIZE - IMAGE_LIST_OFFSET)

/** Encryption Key Table */
#define KEY_TABLE_START_ADDRESS     0x7000
#define KEY_TABLE_END_ADDRESS       0x77FF
#define KEY_TABLE_SIZE              ((KEY_TABLE_END_ADDRESS - KEY_TABLE_START_ADDRESS)+1)
#define KEY_TABLE_INFO              (KEY_TABLE_END_ADDRESS - (INFO_BLOCK_SIZE-1))
#define KEY_TABLE_CRC               (KEY_TABLE_END_ADDRESS - (CRC_SIZE-1))
#define KEY_TABLE_TOTAL_RECORDS     (KEY_TABLE_SIZE/EXTERNAL_FLASH_PAGE)

/** BLE Configuration */
#define BLE_CONFIG_START_ADDRESS    0x7800
#define BLE_CONFIG_END_ADDRESS      0x7FFF
#define BLE_CONFIG_SIZE             ((BLE_CONFIG_END_ADDRESS - BLE_CONFIG_START_ADDRESS)+1)
#define BLE_CONFIG_INFO             (BLE_CONFIG_END_ADDRESS - (INFO_BLOCK_SIZE-1))
#define BLE_CONFIG_CRC              (BLE_CONFIG_END_ADDRESS - (CRC_SIZE-1))
#define BLE_CONFIG_TOTAL_RECORDS    (BLE_CONFIG_SIZE/EXTERNAL_FLASH_PAGE)

/** Application */
#define APPLICATION_START_ADDRESS   0x8000
#define APPLICATION_END_ADDRESS     0x3FFFF
#define APPLICATION_SIZE            ((APPLICATION_END_ADDRESS - APPLICATION_START_ADDRESS)+1)
#define APPLICATION_INFO            (APPLICATION_END_ADDRESS - (INFO_BLOCK_SIZE-1))
#define APPLICATION_CRC             (APPLICATION_END_ADDRESS - (CRC_SIZE-1))
#define APPLICATION_TOTAL_RECORDS   (APPLICATION_SIZE/EXTERNAL_FLASH_PAGE)

/****** BACKUP IMAGES ************/
/** Backup Application Configuration */
#define BACKUP_CONFIG_START_ADDRESS   0x1000
#define BACKUP_CONFIG_END_ADDRESS     0x1FFF
#define BACKUP_CONFIG_SIZE            (CONFIGURATION_SIZE)
#define BACKUP_CONFIG_INFO            ((BACKUP_CONFIG_START_ADDRESS + BACKUP_CONFIG_SIZE) - (INFO_BLOCK_SIZE))
#define BACKUP_CONFIG_CRC             ((BACKUP_CONFIG_START_ADDRESS + BACKUP_CONFIG_SIZE) - (CRC_SIZE))
#define BACKUP_CONFIG_TOTAL_RECORDS   (BACKUP_CONFIG_SIZE/EXTERNAL_FLASH_PAGE)

/** Backup Encryption Key Table */
#define BACKUP_KEY_TABLE_START_ADDRESS     0x2000
#define BACKUP_KEY_TABLE_END_ADDRESS       0x2FFF
#define BACKUP_KEY_TABLE_SIZE              (KEY_TABLE_SIZE)
#define BACKUP_KEY_TABLE_INFO              ((BACKUP_KEY_TABLE_START_ADDRESS+BACKUP_KEY_TABLE_SIZE) - (INFO_BLOCK_SIZE))
#define BACKUP_KEY_TABLE_CRC               ((BACKUP_KEY_TABLE_START_ADDRESS+BACKUP_KEY_TABLE_SIZE) - (CRC_SIZE))
#define BACKUP_KEY_TABLE_TOTAL_RECORDS     (BACKUP_KEY_TABLE_SIZE/EXTERNAL_FLASH_PAGE)

/** Backup BLE Configuration */
#define BACKUP_BLE_CONFIG_START_ADDRESS    0x3000
#define BACKUP_BLE_CONFIG_END_ADDRESS      0x3FFF
#define BACKUP_BLE_CONFIG_SIZE             (BLE_CONFIG_SIZE)
#define BACKUP_BLE_CONFIG_INFO             ((BACKUP_BLE_CONFIG_START_ADDRESS+BACKUP_BLE_CONFIG_SIZE) - (INFO_BLOCK_SIZE))
#define BACKUP_BLE_CONFIG_CRC              ((BACKUP_BLE_CONFIG_START_ADDRESS+BACKUP_BLE_CONFIG_SIZE) - (CRC_SIZE))
#define BACKUP_BLE_CONFIG_TOTAL_RECORDS    (BACKUP_BLE_CONFIG_SIZE/EXTERNAL_FLASH_PAGE)

/** Backup Application */
#define BACKUP_APP_START_ADDRESS   0x4000
#define BACKUP_APP_END_ADDRESS     0x3BFFF
#define BACKUP_APP_SIZE            (APPLICATION_SIZE)
#define BACKUP_APP_INFO            ((BACKUP_APP_START_ADDRESS+BACKUP_APP_SIZE) - (INFO_BLOCK_SIZE))
#define BACKUP_APP_CRC             ((BACKUP_APP_START_ADDRESS+BACKUP_APP_SIZE) - (CRC_SIZE))
#define BACKUP_APP_TOTAL_RECORDS   (BACKUP_APP_SIZE/EXTERNAL_FLASH_PAGE)

/****** NEW IMAGES ************/
/** New Application Configuration */
#define NEW_CONFIG_START_ADDRESS   0x3C000
#define NEW_CONFIG_END_ADDRESS     0x3CFFF
#define NEW_CONFIG_SIZE            (CONFIGURATION_SIZE)
#define NEW_CONFIG_INFO            ((NEW_CONFIG_START_ADDRESS + NEW_CONFIG_SIZE) - (INFO_BLOCK_SIZE))
#define NEW_CONFIG_CRC             ((NEW_CONFIG_START_ADDRESS + NEW_CONFIG_SIZE) - (CRC_SIZE))
#define NEW_CONFIG_TOTAL_RECORDS   (NEW_CONFIG_SIZE/EXTERNAL_FLASH_PAGE)

/** New Encryption Key Table */
#define NEW_KEY_TABLE_START_ADDRESS     0x3D000
#define NEW_KEY_TABLE_END_ADDRESS       0x3DFFF
#define NEW_KEY_TABLE_SIZE              (KEY_TABLE_SIZE)
#define NEW_KEY_TABLE_INFO              ((NEW_KEY_TABLE_START_ADDRESS+NEW_KEY_TABLE_SIZE) - (INFO_BLOCK_SIZE))
#define NEW_KEY_TABLE_CRC               ((NEW_KEY_TABLE_START_ADDRESS+NEW_KEY_TABLE_SIZE) - (CRC_SIZE))
#define NEW_KEY_TABLE_TOTAL_RECORDS     (NEW_KEY_TABLE_SIZE/EXTERNAL_FLASH_PAGE)

/** New BLE Configuration */
#define NEW_BLE_CONFIG_START_ADDRESS    0x3E000
#define NEW_BLE_CONFIG_END_ADDRESS      0x3EFFF
#define NEW_BLE_CONFIG_SIZE             (BLE_CONFIG_SIZE)
#define NEW_BLE_CONFIG_INFO             ((NEW_BLE_CONFIG_START_ADDRESS+NEW_BLE_CONFIG_SIZE) - (INFO_BLOCK_SIZE))
#define NEW_BLE_CONFIG_CRC              ((NEW_BLE_CONFIG_START_ADDRESS+NEW_BLE_CONFIG_SIZE) - (CRC_SIZE))
#define NEW_BLE_CONFIG_TOTAL_RECORDS    (NEW_BLE_CONFIG_SIZE/EXTERNAL_FLASH_PAGE)

/** New Application */
#define NEW_APP_START_ADDRESS   0x3F000
#define NEW_APP_END_ADDRESS     0x76FFF
#define NEW_APP_SIZE            (APPLICATION_SIZE)
#define NEW_APP_INFO            ((NEW_APP_START_ADDRESS+NEW_APP_SIZE) - (INFO_BLOCK_SIZE))
#define NEW_APP_CRC             ((NEW_APP_START_ADDRESS+NEW_APP_SIZE) - (CRC_SIZE))
#define NEW_APP_TOTAL_RECORDS   (NEW_APP_SIZE/EXTERNAL_FLASH_PAGE)

/****** DATALOG ************/
/** Datalog Record Storage */
/** Starting address of the datalog in external storage */
#define DATALOG_START       0x00077000
/** Datalog length is based on:
 *  13 months * 30days/month * 24hours/day * 4readings/hour * 4bytes/reading =
 *  149760 bytes. This comes out to 36.56 sub-sectors on the external flash.
 *  The sub-sector is the minimum size for erasing. Adding some padding 
 *  (~1.5 sub-sectors) yields 155648 bytes which lets us make sure to always
 *  have at least 37 fully populated (we erase the one we're about to use 
 *  before we start writing to it).
 *  */
#define DATALOG_LENGTH      0x00026000
#define DATALOG_END         ((DATALOG_START + DATALOG_LENGTH) - 1)
#define DATALOG_RECORD_SIZE   (4)
#define DATALOG_LAST_RECORD   ((DATALOG_END + 1) - DATALOG_RECORD_SIZE)
#define DATALOG_TOTAL_RECORDS ((DATALOG_LENGTH) / 4)

#endif
