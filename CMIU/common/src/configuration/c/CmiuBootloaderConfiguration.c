/******************************************************************************
*******************************************************************************
**
**         Filename: CmiuBootloaderConfiguration.c
**
**           Author: Brian Arnberg
**          Created: 2015.07.15
**
**     Last Edit By:
**        Last Edit:
**
** Revision History:
**       2015.07.15: Generated file.
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file CmiuBootloaderConfiguration.c
*
* @brief This file contains the bootloader configuration information.
*
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include "CmiuVersionInformation.h"
#include "CmiuBootloaderConfiguration.h"

/** This is the actual ID number for this CMIU. */
#define CMIU_ID_NUM 400000200
/** XSTR and STR are a macro pair courtesy gcc.gnu.org:
 * https://gcc.gnu.org/onlinedocs/gcc-4.6.2/cpp/Stringification.html
 *
 * To turn a number to a string, you need two layers of macro expansion.
 * Call XSTR(4) to get "4".
 */
#define XSTR(num)   STR(num)
/** See #XSTR for details. */
#define STR(num)    #num
/** Convert #CMIU_ID_NUM to a string for use below. */
#define CMIU_ID_STR XSTR(CMIU_ID_NUM)

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// Cmiu ID Number must be stored in the bootloader.
const U_MIU_ID uMiuId CMIU_ID_ATTRIBUTE =
{
    CMIU_ID_NUM
};

// Special strings must also be stored in the bootloader. 
const uint8_t strMiuPubTopic[] CMIU_ID_PUB_ATTRIBUTE = "CMIU/Report/" CMIU_ID_STR;
const uint8_t strMiuSubTopic[] CMIU_ID_SUB_ATTRIBUTE = "CMIU/Command/" CMIU_ID_STR;
const uint8_t strClientId[] CMIU_ID_CLIENT_ATTRIBUTE = "CMIU/" CMIU_ID_STR;


// Base Version and CRC32 of configuration space
// Must reside in the last 16 bytes of the configuration space (TBD).
// BOOTLOADER_INFO_ATTRIBUTE ensures that the variables is stored
// in the correct location.
const S_MIU_IMG_INFO sBootloaderImgInfo BOOTLOADER_INFO_ATTRIBUTE =
{
    /** The following values are defined in CmiuVersionInformation.h,
     ** which is meant to be updated once, automatically, by the build
     ** automation server. */
    .versionMajor = VERSION_MAJOR,
    .versionMinor = VERSION_MINOR,
    .versionYearMonthDay = VERSION_YYMMDD,
    .versionBuild = VERSION_BUILD,
    .imageCrc32 = DEFAULT_CRC
};

const ImageInformation_t backupImageLists[15] =
{
    {
        .Address = (uint8_t *)APPLICATION_START_ADDRESS,
        .Length = (APPLICATION_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(APPLICATION_INFO),
        .CrcAddress = (uint32_t *)(APPLICATION_CRC),
        .ImageType = CmiuApplication
    },
    {
        .Address = (uint8_t *)CONFIGURATION_START_ADDRESS,
        .Length = (CONFIGURATION_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(CONFIGURATION_INFO),
        .CrcAddress = (uint32_t *)(CONFIGURATION_CRC),
        .ImageType = CmiuConfiguration
    },
    {
        .Address = (uint8_t *)KEY_TABLE_START_ADDRESS,
        .Length = (KEY_TABLE_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(KEY_TABLE_INFO),
        .CrcAddress = (uint32_t *)(KEY_TABLE_CRC),
        .ImageType = CmiuEncryption
    },
    {
        .Address = (uint8_t *)BLE_CONFIG_START_ADDRESS,
        .Length = (BLE_CONFIG_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(BLE_CONFIG_INFO),
        .CrcAddress = (uint32_t *)(BLE_CONFIG_CRC),
        .ImageType = CmiuBleConfiguration
    },
    /** Backup images */
    {
        .Address = (uint8_t *)BACKUP_APP_START_ADDRESS,
        .Length = (BACKUP_APP_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(BACKUP_APP_INFO),
        .CrcAddress = (uint32_t *)(BACKUP_APP_CRC),
        .ImageType = BackupApplication
    },
    {
        .Address = (uint8_t *)BACKUP_CONFIG_START_ADDRESS,
        .Length = (BACKUP_CONFIG_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(BACKUP_CONFIG_INFO),
        .CrcAddress = (uint32_t *)(BACKUP_CONFIG_CRC),
        .ImageType = BackupConfiguration
    },
    {
        .Address = (uint8_t *)BACKUP_KEY_TABLE_START_ADDRESS,
        .Length = (BACKUP_KEY_TABLE_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(BACKUP_KEY_TABLE_INFO),
        .CrcAddress = (uint32_t *)(BACKUP_KEY_TABLE_CRC),
        .ImageType = BackupEncryption
    },
    {
        .Address = (uint8_t *)BACKUP_BLE_CONFIG_START_ADDRESS,
        .Length = (BACKUP_BLE_CONFIG_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(BACKUP_BLE_CONFIG_INFO),
        .CrcAddress = (uint32_t *)(BACKUP_BLE_CONFIG_CRC),
        .ImageType = BackupBleConfiguration
    },
    /** New Images */
    {
        .Address = (uint8_t *)NEW_APP_START_ADDRESS,
        .Length = (NEW_APP_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(NEW_APP_INFO),
        .CrcAddress = (uint32_t *)(NEW_APP_CRC),
        .ImageType = NewApplication
    },
    {
        .Address = (uint8_t *)NEW_CONFIG_START_ADDRESS,
        .Length = (NEW_CONFIG_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(NEW_CONFIG_INFO),
        .CrcAddress = (uint32_t *)(NEW_CONFIG_CRC),
        .ImageType = NewConfiguration
    },
    {
        .Address = (uint8_t *)NEW_KEY_TABLE_START_ADDRESS,
        .Length = (NEW_KEY_TABLE_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(NEW_KEY_TABLE_INFO),
        .CrcAddress = (uint32_t *)(NEW_KEY_TABLE_CRC),
        .ImageType = NewEncryption
    },
    {
        .Address = (uint8_t *)NEW_BLE_CONFIG_START_ADDRESS,
        .Length = (NEW_BLE_CONFIG_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(NEW_BLE_CONFIG_INFO),
        .CrcAddress = (uint32_t *)(NEW_BLE_CONFIG_CRC),
        .ImageType = NewBleConfiguration
    }
};





// At this point the remaining code in this file resides outside
// of the configuariton memory space.
/***************************************************************************//**
 * @brief
 *  Return a pointer to the starting address of an array of ImageInformation_t
 *  blocks.
 *
 * @note
 *  The ImageInformation_t array is located in CmiuAppConfiguration.c, and is
 *  stored in the ApplicationConfiguration image. The image array is basically
 *  a list of pointers to relevant memory locations. If the AppConfiguration
 *  image (which is generated by CmiuAppConfiguration.c and placed by the linker),
 *  gets corrupted, the array of pointers will be worthless and cause the
 *  bootloader to try to access some illegal address. So, this function is used
 *  to validate the image list in the AppConfiguration image file. If any address
 *  in the array is invalid, the bootloader will return a pointer to the backup
 *  list. Otherwise, it will return a pointer to the image list in the
 *  AppConfiguration image.
 *
 * @param[in] None
 *
 * @return
 *  correctArray - a pointer to the correct array.
 ******************************************************************************/
ImageInformation_t* getImageLists(void)
{
    ImageInformation_t* tempImageLists;
    uint32_t index;
    uint32_t testingAddress;
    /** This starts as true. If anything is bad, then it is set false. */
    bool testResult = true;

    tempImageLists = (ImageInformation_t*)(IMAGE_LIST_ADDRESS);

    /** Verify the starting address for the internal images. */
    for(index = 0; index < BackupApplication; ++index)
    {
        /** We only have to test the starting address of the
         * image information. If any of the other stuff is corrupt,
         * it will bear itself out when we run the other validation
         * tests. */
        testingAddress = (uint32_t)(tempImageLists[index].Address);

        /** The address must be in the correct address space,
         * and it must start on a 1K boundary.
         */
        if (((BOOTLOADER_END_ADDRESS+1) > testingAddress) ||
            ((APPLICATION_END_ADDRESS+1) < testingAddress) ||
            (!(0x00000000 == (testingAddress & 0x03FF))))
        {
            testResult = false;
        }
    }

    /** Verify the starting address for the external images. */
    for(index = BackupApplication; index < (NewBleConfiguration+1); ++index)
    {
        /** We only have to test the starting address of the
         * image information. If any of the other stuff is corrupt,
         * it will bear itself out when we run the other validation
         * tests. */
        testingAddress = (uint32_t)(tempImageLists[index].Address);

        /** The address just needs to start on a 1K boundary.
         */
        if (!(0x00000000 == (testingAddress & 0x03FF)))
        {
            testResult = false;
        }
    }


    /** If the image list in config is corrupt, change the
     * the tempImageLists to default and return that value.
     */
    if (!(testResult))
    {
        tempImageLists = (ImageInformation_t* )&backupImageLists[0];
    }

    return tempImageLists;
}
