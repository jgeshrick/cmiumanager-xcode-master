/******************************************************************************
*******************************************************************************
**
**         Filename: CmiuAppImageInfo.c
**
**           Author: Brian Arnberg
**          Created: 2015.06.22
**
**     Last Edit By:
**        Last Edit:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential
**    property of Neptune Technology Group. The user, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/

#ifndef __APP_IMAGE_INFORMATION_H
#define __APP_IMAGE_INFORMATION_H

#include <stdint.h>
#include "typedefs.h"
#include "MemoryMap.h"


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
extern const S_MIU_IMG_INFO sAppImgInfo;

#endif // __APP_IMAGE_INFORMATION_H

