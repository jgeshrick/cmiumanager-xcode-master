/******************************************************************************
*******************************************************************************
**
**         Filename: CmiuBootloaderConfiguration.h
**
**           Author: Brian Arnberg
**          Created: 2015.07.15
**
**     Last Edit By:
**        Last Edit:
**
** Revision History:
**       2015.07.15: Generated file.
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __BL_CONFIGURATION_H
#define __BL_CONFIGURATION_H


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
extern const S_MIU_IMG_INFO sBootloaderImgInfo;

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
ImageInformation_t* getImageLists(void);


#endif // __BL_CONFIGURATION_H

