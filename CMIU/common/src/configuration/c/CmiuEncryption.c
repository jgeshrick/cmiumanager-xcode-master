/******************************************************************************
*******************************************************************************
**
**         Filename: CmiuEncryption.c
**
**           Author: Brian Arnberg
**          Created: 2015.07.15
**
**     Last Edit By:
**        Last Edit:
**
** Revision History:
**       2015.07.15: Generated file.
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file CmiuEncryption.c
*
* @brief This file contains encryption information and tables.
*
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include "CmiuVersionInformation.h"
#include "CmiuEncryption.h"

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
const uint8_t encryptionPlaceHolder KEY_TABLE_ATTRIBUTE = 0xFF;

// Base Version and CRC32 of configuration space
// Must reside in the last 16 bytes of the ble configuration space (BWA).
// KEY_TABLE_INFO_ATTRIBUTE ensures that the variables is stored
// in the correct location.
const S_MIU_IMG_INFO sEncryptionImgInfo KEY_TABLE_INFO_ATTRIBUTE =
{
    /** The following values are defined in CmiuVersionInformation.h,
     ** which is meant to updated once, automatically, by the build
     ** automation server. */
    .versionMajor = VERSION_MAJOR,
    .versionMinor = VERSION_MINOR,
    .versionYearMonthDay = VERSION_YYMMDD,
    .versionBuild = VERSION_BUILD,
    .imageCrc32 = DEFAULT_CRC
};

