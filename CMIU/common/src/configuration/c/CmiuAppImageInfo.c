/******************************************************************************
*******************************************************************************
**
**         Filename: CmiuAppImageInfo.c
**
**           Author: Brian Arnberg
**          Created: 2015.06.22
**
**     Last Edit By:
**        Last Edit:
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file CmiuAppImageInfo.c
*
* @brief This file contains the image information for this application
*        and a getter function.
*
************************************************************************/
#include "CmiuVersionInformation.h"
#include "CmiuAppImageInfo.h"

/***************************************************************************//**
 * @brief
 *   Image information for the application.
 *
 * @note
 *  This variable holds the major version information for the image. The image
 *  variable type is in typedefs.h. A third application will be able to fill
 *  this variable.
 *
 ******************************************************************************/
const S_MIU_IMG_INFO sAppImgInfo APPLICATION_INFO_ATTRIBUTE =
{
    /** The following values are defined in CmiuVersionInformation.h,
     ** which is meant to updated once, automatically, by the build
     ** automation server. */
    .versionMajor = VERSION_MAJOR,
    .versionMinor = VERSION_MINOR,
    .versionYearMonthDay = VERSION_YYMMDD,
    .versionBuild = VERSION_BUILD,
    .imageCrc32 = DEFAULT_CRC
};
