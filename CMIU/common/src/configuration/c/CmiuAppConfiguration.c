/******************************************************************************
*******************************************************************************
**
**         Filename: CmiuAppConfiguration.c
**
**           Author: Troy Harstad
**          Created: 07/19/2010
**
**     Last Edit By: Brian Arnberg
**        Last Edit: 2015.07.15
**
** Revision History:
**         07/19/10: First created (Troy Harstad)
**         12/01/10: Updated to force ABL_PERFORM on by default.  Updated comments.
**         08/17/12: Updated for E-Coder V2
**         12/13/12: Updated for RV4 (Nick Sinas)
**       2015.07.15: Updating for CMIU (Brian Arnberg)
**
**      Code Review:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file CmiuAppConfiguration.c
*
* @brief This file contains the application configuration information and
*    other useful functions/variables. The variables are meant to be
*    inserted into a specific location in memory, which is accomplished
*    using the linker.
*
************************************************************************/

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "CmiuVersionInformation.h"
#include "CmiuAppConfiguration.h"
#include "cmiuFlash.h"
#include "Crc32Fast.h"
#include "app_rtc.h"
#ifndef CMIU_UNIT_TEST
#include "app_cmit_interface.h"
#endif


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// DEFAULT MIU CONFIGURATION FOR NORMAL MODE INTERVALS
const U_MIU_CONFIG_NORMAL uMIUConfigNormal CONFIGURATION_ATTRIBUTE =
{
//
// MIU Conifg
//
// Structure for Normal Mode MIU Configuration


    .sMIUConfigNormal.arbReadIntervalSecs =                900,         // Register Reading Interval = 15 minutes = 900 seconds
    .sMIUConfigNormal.detailedConfigPacketIntervalSecs =   86400,       // Detailed Config Packet Interval = 24 hours = 1440 minutes = 86400 seconds
    .sMIUConfigNormal.intervalDataPacketIntervalSecs =   86400,         // Interval Data Packet Interval = 60 minutes = 3600 seconds
    .sMIUConfigNormal.cellCallInIntervalSecs =   3600,                  // Cellular Connection Interval = 60 minutes = 3600 seconds
    .sMIUConfigNormal.cellCallInOffsetMins = 0,                         // Time for initial call-in defined in minutes after midnight UTC
    .sMIUConfigNormal.cellCallInWindowQtrHrs = 2,                       // Duration of randomized call-in window in 15 minute increments
    .sMIUConfigNormal.datalogIntervalSecs = 3600                        // Datalog Reading Storage Interval = 15 minutes = 900 seconds
    
    
};


// DEFAULT MIU CONFIGURATION FOR SWIPE MODE INTERVALS
const U_MIU_CONFIG_SWIPE uMIUConfigSwipe CONFIGURATION_ATTRIBUTE =
{
    .sMIUConfigSwipe.arbReadIntervalSecs =             30,              // wARBReadInterval UNUSED!!!
    .sMIUConfigSwipe.swipeStateTimerSecs =             1800             // Maximum duration of swipe state  = 30 minutes = 1800 seconds
                                                                        // (BLE timeout should kick in first if set above that)

};


// DEFAULT MIU SERVER CONFIGURATION
const U_MIU_SERVER_CONFIG uMiuMqttServerConfig CONFIGURATION_ATTRIBUTE =
{
#if defined( SERVER_DEV0 )
    // Dev0 MQTT broker server as of 6/30/2015
    .sMIUServerConfig.dwServerPortNumber =    1883,             // MQTT Port = 1883
    .sMIUServerConfig.abyServerAddress =      "10.120.0.174"    // Public IP  52.27.80.45
#elif defined( SERVER_DEV1 )
    // Dev 1
    .sMIUServerConfig.dwServerPortNumber =    1883,              // MQTT Port = 1883
    .sMIUServerConfig.abyServerAddress =      "broker.dev1.mdce"
#elif defined( SERVER_DV_TEST )
    // DV TEST
    .sMIUServerConfig.dwServerPortNumber =    1883,              // MQTT Port = 1883
    .sMIUServerConfig.abyServerAddress =      "broker.dvtest.mdce"
#elif defined( SERVER_SQA )
    // SQA Server
    .sMIUServerConfig.dwServerPortNumber =    1883,              // MQTT Port = 1883
    .sMIUServerConfig.abyServerAddress =      "broker.sqa.mdce"
#else
    // Production
    .sMIUServerConfig.dwServerPortNumber =    1883,              // MQTT Port = 1883
    .sMIUServerConfig.abyServerAddress =      "broker.prod.mdce"
#endif
};

// DEFAULT HTTP MIU SERVER CONFIGURATION
const U_MIU_SERVER_CONFIG uMiuHttpServerConfig CONFIGURATION_ATTRIBUTE =
{
#if defined( SERVER_DEV0 )
    // Dev 0
   .sMIUServerConfig.dwServerPortNumber =    80,               // HTTP Port = 80
   .sMIUServerConfig.abyServerAddress =      "10.120.0.174"    // Public IP  52.27.80.45
#elif defined( SERVER_DEV1 )
    // Dev 1
    .sMIUServerConfig.dwServerPortNumber =    80,                   // HTTP Port = 80
    .sMIUServerConfig.abyServerAddress =      "10.120.101.58"       // Public IP  52.27.100.200
#elif defined( SERVER_DV_TEST )
    // DV TEST
    .sMIUServerConfig.dwServerPortNumber =    80,                   // HTTP Port = 80
    .sMIUServerConfig.abyServerAddress =      "broker.dvtest.mdce"
#elif defined( SERVER_SQA )
    // SQA Server
    .sMIUServerConfig.dwServerPortNumber =    80,                   // HTTP Port = 80
    .sMIUServerConfig.abyServerAddress =      "broker.sqa.mdce"    
#else
    // Production
    .sMIUServerConfig.dwServerPortNumber =    80,                   // HTTP Port = 80
    .sMIUServerConfig.abyServerAddress =      "broker.prod.mdce"
#endif
};


// DEFAULT MIU BLE CONFIGURATION
const U_MIU_BLE_CONFIG uMIUBleConfig CONFIGURATION_ATTRIBUTE =
{
    #if (defined SERVER_DEV0) || (defined SERVER_DEV1) || (defined SERVER_DV_TEST)
    // output debug messages both to BLE and Serial
    .sMIUBleConfig.wDebugOutputBitmap = APP_CMIT_INTERFACE_DEBUG_ON_BTLE|APP_CMIT_INTERFACE_DEBUG_ON_UART,
    #else
    // only output debug messages to BLE (for production)
    .sMIUBleConfig.wDebugOutputBitmap = APP_CMIT_INTERFACE_DEBUG_ON_BTLE,
    #endif
    .sMIUBleConfig.wBleLinkStartupActiveDuration = 120,                        // BLE link advertises for 120 seconds at startup
    .sMIUBleConfig.wBleLinkActiveTimeout = 900                                 // BLE link stops if connection drops for 900 seconds
};

// Placeholder for hardware version
// Rev. A = 1(since A is 1st letter).
const uint16_t wHardwareVersion CONFIGURATION_ATTRIBUTE= 1;

const U_MIU_CARRIER_CONFIG uMiuCarrierConfig CONFIGURATION_ATTRIBUTE =
{
    // APN string for AT&T
#if (defined SERVER_DEV0) || (defined SERVER_DEV1) || (defined SERVER_DV_TEST) || (defined SERVER_SQA)
    .sMIUCarrierConfig.abyAttApnString = "neptune-nonprod01.com.attz",   
#else
    .sMIUCarrierConfig.abyAttApnString = "neptune-prod01.com.attz",   
#endif    
    // APN string for Verizon NOT currently used
    .sMIUCarrierConfig.abyVerizonApnString = "",   
    // Context ID number: 1 for AT&T and 3 for Verizon
    .sMIUCarrierConfig.wAttContextIdNumber = 1,
    .sMIUCarrierConfig.wVerizonContextIdNumber = 3,
};


/** Array of ImageInformation. It is useful to have this here,
 ** so that an image update in the future doesn't render a
 ** a bootloader useless. */
const ImageInformation_t ImageLists[15] IMAGE_LIST_ATTRIBUTE =
{
    {
        .Address = (uint8_t *)APPLICATION_START_ADDRESS,
        .Length = (APPLICATION_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(APPLICATION_INFO),
        .CrcAddress = (uint32_t *)(APPLICATION_CRC),
        .ImageType = CmiuApplication
    },
    {
        .Address = (uint8_t *)CONFIGURATION_START_ADDRESS,
        .Length = (CONFIGURATION_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(CONFIGURATION_INFO),
        .CrcAddress = (uint32_t *)(CONFIGURATION_CRC),
        .ImageType = CmiuConfiguration
    },
    {
        .Address = (uint8_t *)KEY_TABLE_START_ADDRESS,
        .Length = (KEY_TABLE_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(KEY_TABLE_INFO),
        .CrcAddress = (uint32_t *)(KEY_TABLE_CRC),
        .ImageType = CmiuEncryption
    },
    {
        .Address = (uint8_t *)BLE_CONFIG_START_ADDRESS,
        .Length = (BLE_CONFIG_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(BLE_CONFIG_INFO),
        .CrcAddress = (uint32_t *)(BLE_CONFIG_CRC),
        .ImageType = CmiuBleConfiguration
    },
    /** Backup images */
    {
        .Address = (uint8_t *)BACKUP_APP_START_ADDRESS,
        .Length = (BACKUP_APP_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(BACKUP_APP_INFO),
        .CrcAddress = (uint32_t *)(BACKUP_APP_CRC),
        .ImageType = BackupApplication
    },
    {
        .Address = (uint8_t *)BACKUP_CONFIG_START_ADDRESS,
        .Length = (BACKUP_CONFIG_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(BACKUP_CONFIG_INFO),
        .CrcAddress = (uint32_t *)(BACKUP_CONFIG_CRC),
        .ImageType = BackupConfiguration
    },
    {
        .Address = (uint8_t *)BACKUP_KEY_TABLE_START_ADDRESS,
        .Length = (BACKUP_KEY_TABLE_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(BACKUP_KEY_TABLE_INFO),
        .CrcAddress = (uint32_t *)(BACKUP_KEY_TABLE_CRC),
        .ImageType = BackupEncryption
    },
    {
        .Address = (uint8_t *)BACKUP_BLE_CONFIG_START_ADDRESS,
        .Length = (BACKUP_BLE_CONFIG_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(BACKUP_BLE_CONFIG_INFO),
        .CrcAddress = (uint32_t *)(BACKUP_BLE_CONFIG_CRC),
        .ImageType = BackupBleConfiguration
    },
    /** New Images */
    {
        .Address = (uint8_t *)NEW_APP_START_ADDRESS,
        .Length = (NEW_APP_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(NEW_APP_INFO),
        .CrcAddress = (uint32_t *)(NEW_APP_CRC),
        .ImageType = NewApplication
    },
    {
        .Address = (uint8_t *)NEW_CONFIG_START_ADDRESS,
        .Length = (NEW_CONFIG_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(NEW_CONFIG_INFO),
        .CrcAddress = (uint32_t *)(NEW_CONFIG_CRC),
        .ImageType = NewConfiguration
    },
    {
        .Address = (uint8_t *)NEW_KEY_TABLE_START_ADDRESS,
        .Length = (NEW_KEY_TABLE_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(NEW_KEY_TABLE_INFO),
        .CrcAddress = (uint32_t *)(NEW_KEY_TABLE_CRC),
        .ImageType = NewEncryption
    },
    {
        .Address = (uint8_t *)NEW_BLE_CONFIG_START_ADDRESS,
        .Length = (NEW_BLE_CONFIG_SIZE-4UL),
        .MiuImageInfo = (S_MIU_IMG_INFO *)(NEW_BLE_CONFIG_INFO),
        .CrcAddress = (uint32_t *)(NEW_BLE_CONFIG_CRC),
        .ImageType = NewBleConfiguration
    }
};

// bl_crc is set by the bootloader.  It lets us know if the CRC32
// of each memory space (APP1, CNFG) is valid or not.
volatile uint8_t bl_crc; // __attribute__((at(0x20000000)));



// For the application project the MIU ID is in the bootloader image
// info block.
#ifdef APP_PROJ
/* These definitions, as written, will require the FileCombiner
 * to overwrite the listed locations with the correct value. */
const U_MIU_ID * const puMiuId        = (U_MIU_ID *)CMIU_ID_LOCATION;
const uint8_t * const pstrMiuPubTopic = (uint8_t *)CMIU_ID_PUB_LOCATION;
const uint8_t * const pstrMiuSubTopic = (uint8_t *)CMIU_ID_SUB_LOCATION;
const uint8_t * const pstrClientId    = (uint8_t *)CMIU_ID_CLIENT_LOCATION;
#endif



// For the debug project the MIU ID is located here.
#ifdef DEBUG_PROJ
const U_MIU_ID uMiuId CONFIGURATION_ATTRIBUTE =
{
    400000202
};

const uint8_t strMiuPubTopic[] CONFIGURATION_ATTRIBUTE = "CMIU/Report/400000202";
const uint8_t strMiuSubTopic[] CONFIGURATION_ATTRIBUTE = "CMIU/Command/400000202";
const uint8_t strClientId[] CONFIGURATION_ATTRIBUTE =    "CMIU/400000202";

const U_MIU_ID * const puMiuId        = &uMiuId;
const uint8_t * const pstrMiuPubTopic = strMiuPubTopic;
const uint8_t * const pstrMiuSubTopic = strMiuSubTopic;
const uint8_t * const pstrClientId    = strClientId;

#endif


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/

// Base Version and CRC32 of configuration space
// Must reside in the last 16 bytes of the configuration space (TBD).
// CONFIGURATION_INFO_ATTRIBUTE ensures that the variable is stored
// in the correct location.
const S_MIU_IMG_INFO sConfigImgInfo CONFIGURATION_INFO_ATTRIBUTE =
{
    /** The following values are defined in CmiuVersionInformation.h,
     ** which is meant to updated once, automatically, by the build
     ** automation server. */
    .versionMajor = VERSION_MAJOR,
    .versionMinor = VERSION_MINOR,
    .versionYearMonthDay = VERSION_YYMMDD,
    .versionBuild = VERSION_BUILD,
    .imageCrc32 = DEFAULT_CRC
};

// Function that returns the value of bl_crc
uint8_t CmiuAppConfiguration_BLCRC_Retrieve(void)
{
    return bl_crc;
}


