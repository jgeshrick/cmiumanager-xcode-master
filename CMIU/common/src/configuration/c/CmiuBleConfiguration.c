/******************************************************************************
*******************************************************************************
**
**         Filename: CmiuBleConfiguration.c
**
**           Author: Brian Arnberg
**          Created: 2015.07.15
**
**     Last Edit By:
**        Last Edit:
**
** Revision History:
**       2015.07.15: Generated file.
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
**
******************************************************************************
******************************************************************************/

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include "CmiuVersionInformation.h"
#include "CmiuBleConfiguration.h"


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
const uint8_t bleConfigPlaceHolder BLE_CONFIG_ATTRIBUTE = 0xFF;

// Base Version and CRC32 of configuration space
// Must reside in the last 16 bytes of the ble configuration space (BWA).
// BLE_CONFIG_INFO_ATTRIBUTE ensures that the variables are stored
// in the correct location.
const S_MIU_IMG_INFO sBleConfigImgInfo BLE_CONFIG_INFO_ATTRIBUTE =
{
    /** The following values are defined in CmiuVersionInformation.h,
     ** which is meant to updated once, automatically, by the build
     ** automation server. */
    .versionMajor = VERSION_MAJOR,
    .versionMinor = VERSION_MINOR,
    .versionYearMonthDay = VERSION_YYMMDD,
    .versionBuild = VERSION_BUILD,
    .imageCrc32 = DEFAULT_CRC
};

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h



