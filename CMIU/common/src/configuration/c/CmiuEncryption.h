/******************************************************************************
*******************************************************************************
**
**         Filename: CmiuEncryption.h
**    
**           Author: Brian Arnberg
**          Created: 2015.07.15
**
**     Last Edit By:
**        Last Edit:
**
** Revision History:
**       2015.07.15: Generated file.
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __KEY_TABLE_H
#define __KEY_TABLE_H



/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
//None

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/



/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
extern const S_MIU_IMG_INFO sConfigImgInfo;


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __KEY_TABLE_H

