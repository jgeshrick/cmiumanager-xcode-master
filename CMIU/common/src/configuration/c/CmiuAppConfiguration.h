/******************************************************************************
*******************************************************************************
**
**         Filename: AppConfiguration.h
**    
**           Author: Troy Harstad
**          Created: 8/17/2012
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: This is the header file for AppConfiguration.c source file.  Only
**                   externally accessible functions are prototyped here.
**
** Revision History:
**          7/19/10: First created (Troy Harstad)
**          8/17/12: Updated for E-Coder V2
**
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef HEADER_APPCONFIGURATION_H
#define HEADER_APPCONFIGURATION_H

#include "typedefs.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern uint8_t AppConfiguration_BLCRC_Retrieve(void);
extern void AppConfiguration_Init(void);
extern void AppConfiguration_InitHW(void);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
extern const U_MIU_CONFIG_NORMAL uMIUConfigNormal;
extern const U_MIU_CONFIG_SWIPE uMIUConfigSwipe;
extern const U_MIU_SERVER_CONFIG uMiuMqttServerConfig;
extern const U_MIU_SERVER_CONFIG uMiuHttpServerConfig;
extern const U_MIU_BLE_CONFIG uMIUBleConfig;
extern const U_MIU_CARRIER_CONFIG uMiuCarrierConfig;
extern const uint16_t wHardwareVersion;
extern const ImageInformation_t ImageLists[15];
extern const S_MIU_IMG_INFO sConfigImgInfo;
extern const U_MIU_ID * const puMiuId;
extern const uint8_t * const pstrMiuPubTopic;
extern const uint8_t * const pstrMiuSubTopic;
extern const uint8_t * const pstrClientId;

/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif
