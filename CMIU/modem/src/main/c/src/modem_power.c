/******************************************************************************
*******************************************************************************
**
**         Filename: modem_power.c
**    
**           Author: Barry Huggins
**          Created: 12/23/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file modem_power.c
*
* @brief This file contains the routines to manage the power for the 
*        CMIU module. 
* 
************************************************************************/

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "em_device.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "modem.h"
#include "modem_power.h"
#include "app_timers.h"
#include "app_gpio.h"
#include "modem_uart.h"
#include "app_cmit_interface.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
#define     MODEM_POWER_DELAY_COUNTER       0x100
#define     MODEM_POWER_DELAY_NUMBER_CYCLES  125

/***************************************************************************//**
 * @brief
 *   Function to power on the modem (currently a Telit LE-910)
 *
 * @note
 *   This function powers on the Telit modem. To do this it does the following steps:
 *       enable modem power by taking PA10 high
 *       enable the UART level shifter by taking PD8 low
 *       turn on the modem by driving PD4 high for 1 second and then low
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void modem_power_modemPower_on(void)
{
    
    volatile uint32_t       startDelay;
    uint16_t                i;
    
    DEBUG_OUTPUT_TEXT ("power on modem\n");
    
    // enable modem power (PA10) - active high 
    // to avoid a current spike (over 3 A) during modem power on, we now cycle the power while
    //   the capacitor is charging. A number of tests were run and it was determined
    //   that 125 steps did the best at smoothing the startup current. At the current
    //   clock speed, the 125 steps take approximately 50 ms total. 
    for(i=0; i<MODEM_POWER_DELAY_NUMBER_CYCLES; i++) 
    {
        // power on and then delay
        GPIO_PinOutSet(gpioPortA, 10);
        for(startDelay = MODEM_POWER_DELAY_COUNTER; startDelay>0; startDelay--)
        { 
            __NOP(); 
        } 
        
        // power off and then delay
        GPIO_PinOutClear(gpioPortA, 10);
        for(startDelay = MODEM_POWER_DELAY_COUNTER; startDelay>0; startDelay--) 
        { 
            __NOP(); 
        } 
    }
    
    GPIO_PinOutSet(gpioPortA, 10);
    
    /* enable level shifter - active low */
    GPIO_PinOutClear(gpioPortD, 8);
    Timers_Loop_Delay (100);
    
    /* modem on/off - active low */
 //   GPIO->P[3].DOUT |= (1 << 4);
    
    /* modem on/off - drive high for 1 sec */
    GPIO_PinOutSet(gpioPortD, 4);
    Timers_Loop_Delay (1100);
    
    GPIO_PinOutClear(gpioPortD, 4);

    /* enable DTR (PE11) - active high */
    GPIO_PinOutSet(gpioPortE, 11);

}

/***************************************************************************//**
 * @brief
 *   Function to power off the modem (currently a Telit LE-910)
 *
 * @note
 *   This function powers off the Telit modem. To do this it does the following steps:
 *       enable modem power by taking PA10 high
 *       enable the UART level shifter by taking PD8 low
 *       turn on the modem by driving PD4 high for 1 second and then low
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void modem_power_modemPower_off(void)
{
    /* disable level shifter (PD8) - active low */
    GPIO_PinOutSet(gpioPortD, 8);
    Timers_Loop_Delay (100);

    // check to see if the modem is powered on so we do not power it on instead of off
    if (modem_power_modemIsPowered())
    {
        /* modem on/off - drive high for 2 sec */
        GPIO_PinOutSet(gpioPortD, 4);
        Timers_Loop_Delay (2100);
    
        GPIO_PinOutClear(gpioPortD, 4);
    }

    /* disable modem power (PA10) - active high */
    GPIO_PinOutClear(gpioPortA, 10);

    // disable the 1 msec timer tick to avoid waking every msec
#ifndef CMIU_UNIT_TEST        
    app_timers_disableMsecs();
#endif // CMIU_UNIT_TEST        
}


/***************************************************************************//**
 * @brief
 *   Determines if the modem (currently a Telit LE-910) is currently powered on.
 *
 * @note
 *   This function determines if the Telit modem is powered on and running.
 *   It does this by looking at PD3
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the modem is powered on
 ******************************************************************************/
bool modem_power_modemIsPowered(void)
{
    uint32_t     gpioInput;
    
    gpioInput = GPIO_PinInGet(gpioPortD, 3);

    if (gpioInput)
    {
        return true;
    }
    else
    {
        return false;
    }
}



