/******************************************************************************
*******************************************************************************
**
**         Filename: modem.c
**    
**           Author: Barry Huggins
**          Created: 12/23/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file modem.c
*
* @brief This file contains the code used to manage the LTE cellular module
*        (currently a Telit LE-910)
* 
************************************************************************/

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include "stdtypes.h"
#include "em_device.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "em_int.h"
#include "efm32lg_uart.h"
#include "modem.h"
#include "modem_uart.h"
#include "app_timers.h"
#include "app_rtc.h"
#include "modem_power.h"
#include "app_gpio.h"
#include "CmiuAppConfiguration.h"
#include "modem_diags.h"
#include "app_cmit_interface.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
// Theses need to be seen both by the UART ISR, and by the modem object,  
// which will keep a reference to them
// TH-150715 REDUCED NEXT TWO FROM 4096 TO 2048 TO MAKE ROOM FOR SOME RAM !!!!
#define RING_BUFFER_SIZE_RX 2048  // depends on tick rate / data rate etc.
#define RING_BUFFER_SIZE_TX 1024
// Some AT&T modems are taking a long time to register
#define MODEM_RETRY_TIME_LIMIT_MSEC   80000

#define MODEM_OMA_DM_TIME_LIMIT_MSEC   60000

// We have now learned that the APN push from the Verizon network often takes
//  longer than 60 seconds. Therefore, we use a 5 minute time limit for that.
#define MODEM_APN_UPDATE_TIME_LIMIT_MSEC   300000
#define MODEM_POWER_OFF_TIME_LIMIT_MSEC   20000
#define MODEM_CLOCK_YEAR_LIMIT_MIN    10
#define MODEM_CLOCK_YEAR_LIMIT_MAX    50
// allow up to 10 minutes for the modem delta file to download
#define MODEM_FOTA_DOWNLOAD_TIME_LIMIT_MSEC   600000
// allow up to 15 minutes for the modem update
#define MODEM_FOTA_UPDATE_TIME_LIMIT_MSEC   900000
// timeout value for modem connection
#define MODEM_CONNECT_DELAY_MSEC 5000


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/

static int32_t ModemInit(void *pCtx, const void* initParams);
static int32_t ModemCommand(void *pCtx, 
                            E_CONNECTION_COMMAND cmd, 
                            void* cmdParams);
static void ModemTick(void *pCtx, uint32_t tickIntervalMs);
static E_CONNECTION_STATE  ModemGetState(void *pCtx, 
                                         E_QUERY query, 
                                         void* pQueryResult);
static int32_t ModemSend(void *pCtx, const uint8_t* txData, uint32_t txCount);
static int32_t ModemRecv(void *pCtx, uint8_t* rxBuffer, uint32_t bufferSize);
static int32_t ModemOpenHelper(void);
static int32_t ModemClose(void);
static uint16_t  modem_checkResponse(void);
static bool modem_getLine(char *data, uint16_t len, uint32_t timeoutEndTimeMs);
static bool modem_getModemInfo(void);
static bool modem_ReconnectIfNeeded(void);
static void modem_carrierAssert_processCmd(ASSERT_LTE_CARRIER_PARAMS *pAssertLteParams);
static bool modem_setApn_processCmd(UPDATE_APN_PARAMS   *pUpdateApnParams);
static E_MODEM_CARRIERS modem_getModemModel(void);
static bool  contextIdReady (void);
static bool modem_omaDmComplete(void);
static void modem_checkForOmaDmStart(void);
static bool modem_getLineNoWait(char *data, uint16_t len);
static void modem_checkTypeOtaMsg (uint8_t *msgPtr);
static bool modem_getModemSignal(void);



/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/

// Ringbuffers
uint8_t rxData[RING_BUFFER_SIZE_RX];
uint8_t txData[RING_BUFFER_SIZE_TX];
RingBufferT rxRingBuffer;   // Buffer for data FROM cloud
RingBufferT txRingBuffer;   // Buffer for data TO cloud

uint32_t  totalRxCount;
uint32_t  isrRxCount;

static MODEM_MANAGER  modemManager; 

static char    replyBuff[128];

static    uint8_t  noCarrierString[] = "\r\nno carrier";
static    const uint8_t  attModemModel[] = "LE910-NAG";
static    const uint8_t  verizonModemModel[] = "LE910-SVG";
static    const uint8_t  otaevString[] = "OTAEV:";

uint8_t   uartRxBufferOverflow; 
uint8_t   uartRxMultipleChars;

static uint32_t   powerOffTimeLimit;


const TRANSPORT_INTERFACE modemInterface =
{
    ModemInit,
    ModemCommand,
    ModemTick,
    ModemGetState,
    ModemRecv,
    ModemSend,
};


/***************************************************************************//*
 * @brief
 *   Initialize the cellular modem system (currently the Telit LE-910)
 *
 * @note
 *   All this does is disable power to the modem.
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void modem_init(void)
{
    int     i =0;
    MODEM_MANAGER   *pModem = &modemManager;

    // disable power on the Telit module using PA10 - active high
    GPIO_PinOutClear(gpioPortA, 10);
    
    /* disable level shifter PD8 - active low */
    GPIO_PinOutSet(gpioPortD, 8);
    
    pModem->modemFailure = MODEM_ERROR_NONE;
    pModem->state = MS_RESET;  
    pModem->connState = DATA_CONN_STATE_DELAY_MODEM_POWER;
    pModem->noCarrierCmpIdx = 0;
    pModem->carrier = MODEM_CARRIER_NONE; 
    pModem->contextId = 0;

    
    totalRxCount = 0;
    isrRxCount = 0;
    uartRxBufferOverflow = 0;
    uartRxMultipleChars = 0;
    
    
    // Note: This reference means that the Modem must be a singleton 
    // - the limitation being that the UART ISR needs access to the 
    // ringbuffers, so they must be outside the modem classs.
    // These buffers must be setup before we receive an TX or RX interrupt
    pModem->pRxRingBuffer = &rxRingBuffer;
    pModem->pTxRingBuffer = &txRingBuffer;
    RingBufferInit(pModem->pRxRingBuffer, rxData, RING_BUFFER_SIZE_RX);
    RingBufferInit(pModem->pTxRingBuffer, txData, RING_BUFFER_SIZE_TX);
 
    pModem->startUpDelayCnt = 0;
    
    // work around to make sure that NO CARRIER is not in our image file.
    while (noCarrierString[i++])
    {
        noCarrierString[i] = toupper(noCarrierString[i]);
    }
}

/***************************************************************************//**
 * @brief
 *   Determines if the modem (currently a Telit LE-910) is currently registered
 *     on the network.
 *
 * @note
 *   This function determines if the Telit modem is registerd on a data network.
 *   It does this by looking at the results of the AT+CGREG? command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the modem is registered
 ******************************************************************************/
bool    modem_isRegistered()
{
    char    *tempPtr;
    bool    registered = false;
    
    memset (replyBuff, 0, sizeof(replyBuff));
    if (modem_sendATGetReply("AT+CGREG?", "CGREG:", replyBuff, 
                             sizeof(replyBuff), 2000))
    {
        DEBUG_OUTPUT_TEXT ("got OK\n\r");
    tempPtr = strstr (replyBuff, "CGREG:");
    
    if (tempPtr)
    {
        DEBUG_OUTPUT_TEXT (tempPtr);
        // response is in format +CGREG: 0,1
        tempPtr += 9; // skip 9 chars to get to registration value
        if ((*tempPtr == '1') || (*tempPtr == '5'))
            registered = true;
    }
    }
    
    return registered;
}

/***************************************************************************//**
 * @brief
 *   Shuts down the modem (currently a Telit LE-910).
 *
 * @note
 *   This function shuts down the Telit modem.
 *   It does this by sending the AT#SHDN command. The command returns OK
 *     but then takes additional time to power off. The powermon line
 *     should be checked (power_modemIsPowered) to determine when the
 *     modem is actually powered off.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command was successful
 ******************************************************************************/
bool    modem_shutdown (void)
{
    Timers_Delay(50);  
    
    if (modem_sendAT ("AT#SHDN", "OK", 2000))
    {
        DEBUG_OUTPUT_TEXT ("shutting down modem\n");
        modem_diag_saveModemTaskTimeStart(MODEM_DIAG_TIME_DISCONNECT);
        
        // change baud rate back to 115200 to be ready for next modem session
        modem_uart0_changeBaudRate(115200); 

        powerOffTimeLimit = Timers_GetMsTicks() + MODEM_POWER_OFF_TIME_LIMIT_MSEC;

        return true;
    }
    else
    {
        powerOffTimeLimit = Timers_GetMsTicks() + MODEM_POWER_OFF_TIME_LIMIT_MSEC;
        return false;
    }
    
}

/***************************************************************************//**
 * @brief
 *   Determine the modem UART speed.
 *
 * @note
 *   This function tries several modem speeds until it can talk to the modem.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command was successful
 ******************************************************************************/
bool    modem_setModemPortSpeed (void)
{
    bool result = false;
    
    // first try the current speed which is 115200
    if (modem_sendAT ("AT", "OK", 1000))
    {
        result = true;
        if (!modem_uart0_isBaudRate57k())
        {
            if (modem_sendAT ("AT+IPR=57600", "OK", 2000))
            {
                DEBUG_OUTPUT_TEXT ("changing to 57600\n");
                modem_uart0_changeBaudRate(57600);
                Timers_Delay (500);
                result = true;
            }
        }
    }
    else
    {
        modem_uart0_changeBaudRate(57600);
        Timers_Delay (500);
        if (modem_sendAT ("AT", "OK", 1000))
        {
            DEBUG_OUTPUT_TEXT ("running at 57600\n");
             result = true;
        }
        else
        {
            modem_uart0_changeBaudRate(115200);
            Timers_Delay (500);
        }
    }
    
    // if we have set the port speed do any other modem setup.
    if (result)
    {
        result = modem_setup();
    }
    
    return result;
}

/***************************************************************************//**
 * @brief
 *   Sets up the the modem (currently a Telit LE-910) for a data call.
 *
 * @note
 *   This function sets up the Telit modem in preparation of making a data call.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command was successful
 ******************************************************************************/
bool    modem_setup (void)
{
    bool result = true;
    MODEM_MANAGER   *pModem = &modemManager;
    
    if (!modem_sendAT ("AT+CMEE=2", "OK", 10000))
    {
        result = false;
    }

    // set the ESC char delay (blank time before "+++") to .5 sec
    modem_sendAT ("ATS12=25", "OK", 2000);
    
    // enable automatic time update and unsolicited #NITZ messages
    modem_sendAT ("AT#NITZ=7,1", "OK", 2000);

    // configure to return time as UTC time
    modem_sendAT ("AT#CCLKMODE=1", "OK", 2000);

    // configure DTR to drop the data call
    modem_sendAT ("AT&D2", "OK", 2000);

    // determine the modem type if it is not already known
    if (pModem->carrier == MODEM_CARRIER_NONE)
    {
        pModem->carrier = modem_getModemModel();
        if (pModem->carrier == MODEM_CARRIER_ATT)
        {
            DEBUG_OUTPUT_TEXT ("Carrier set to AT&T\n\r");
            pModem->contextId = 
                      uMiuCarrierConfig.sMIUCarrierConfig.wAttContextIdNumber;
            
            // Set modem to prevent fallback to 2G
            modem_sendAT ("AT+WS46=31", "OK", 2000);
        }
        else if (pModem->carrier == MODEM_CARRIER_VERIZON)
        {
            DEBUG_OUTPUT_TEXT ("Carrier set to Verizon\n\r");
            pModem->contextId = 
                   uMiuCarrierConfig.sMIUCarrierConfig.wVerizonContextIdNumber;
        }
        else
        {
            DEBUG_OUTPUT_TEXT ("Carrier set FAILURE!!\n\r");
            result = false;
        }
            
    }
        
    return result;
}


/***************************************************************************//**
 * @brief
 *   Waits for the specified response UART 0 with a timeout.
 *
 * @note
 *   This function waits for the specified response from UART 0. It also returns  
 *    the single line of the response containing the specified response. It will  
 *    also retun after theindicated number of msecs. The RX buffer should be 
 *    flushed before calling.
 *       
 *
 * @param[in]  uart        
 *   UART interface
 * @param[in]  resp        
 *   Response to be matched
 * @param[out] databuf     
 *   Data buffer for read data.  If NULL no data returned.
 * @param[in]  datalen     
 *   Length of data buffer.
 * @param[in]  timeout_ms  
 *   Timeout in msec.
 *
 * @return bool true if the expected response is received.
 ******************************************************************************/
bool modem_waitForResponse(char *resp, char *databuf, 
                           uint8_t datalen, uint16_t timeout_ms)
{
    uint16_t psize;
    bool rc = true;
    char recvbuf[80];
    char *p;
    bool result = false; 
    uint8_t  *tempPtr;
    uint32_t    timeoutEndTimeMs = Timers_GetMsTicks() + timeout_ms;

    // if no buffer given then use local storage
    if (databuf == NULL)
    {
        p = recvbuf;
        psize = sizeof(recvbuf);
    }
    else
    {
        p = databuf;
        psize = datalen;
    }

    memset(p, 0, psize);

    // keep looking until we timeout trying to get a line
    while (rc)
    {
        // get a line of input; a false return indicates a timeout
        rc = modem_getLine((char *)p, psize-1, timeoutEndTimeMs);

        if (rc)
        {
        DEBUG_OUTPUT_TEXT (p);

        // check for an OTAEV message on this line.
        tempPtr = (uint8_t *) strstr ((char *)p, (char *)otaevString);
        
        if (tempPtr)
        {
            modem_checkTypeOtaMsg (tempPtr);
        }

        // check for the expected response
        if (strstr((char *)p, (char *)resp) != NULL)
        {
                result = true;
                break; 
            }
        }
        
        // clear previous lines from buffer
        memset(p, 0, psize);
        
        // check for a timeout
        if (Timers_GetMsTicks() > timeoutEndTimeMs)
        {
            DEBUG_OUTPUT_TEXT ("AT command timeout!!\n");
            break;
        }
    }

  return result;
}

/***************************************************************************//**
 * @brief
 *   Waits for the specified message from UART 0 with a timeout.
 *
 * @note
 *   This function waits for the specified message from UART 0. It also returns  
 *    the single line of the response containing the specified response. It will  
 *    also retun after theindicated number of msecs. 
 *       
 *
 * @param[in]  resp        
 *   Response to be matched
 * @param[out] databuf     
 *   Data buffer for read data.  If NULL no data returned.
 * @param[in]  datalen     
 *   Length of data buffer.
 * @param[in]  timeout_ms  
 *   Timeout in msec.
 *
 * @return bool true if the expected response is received.
 ******************************************************************************/
bool modem_waitForMessage(uint8_t *resp, uint8_t *databuf, 
                          uint8_t datalen, uint16_t timeout_ms)
{
    uint16_t psize;
    bool stillLooking = true;
    uint8_t recvbuf[80];
    uint8_t *p;
    bool result = false; 
    uint32_t    timeoutEndTimeMs = Timers_GetMsTicks() + timeout_ms;

    // if no buffer given then use local storage
    if (databuf == NULL)
    {
        p = recvbuf;
        psize = sizeof(recvbuf);
    }
    else
    {
        p = databuf;
        psize = datalen;
    }

    memset(p, 0, psize);

    // keep looking until we timeout trying to get a line
    while (stillLooking)
    {
        // clear previous lines from buffer
        memset(p, 0, psize);

        // get a line of input; a false return indicates a timeout
        stillLooking = modem_getLine((char *)p, psize-1, timeoutEndTimeMs);

        if (stillLooking)
        {
            DEBUG_OUTPUT_TEXT ((char *)p);

            if (strstr((char *)p, (char *)resp) != NULL)
            {
                result = true;
                stillLooking = false; 
            }
        }
                
        // check for a timeout
        if (Timers_GetMsTicks() > timeoutEndTimeMs)
        {
            stillLooking = false;
        }
    }

  return result;
}


/***************************************************************************//**
 * @brief
 *   Sends the specified AT command UART 0 and waits for the specified response.
 *
 * @note
 *   This function waits for the specified response from UART 0. It will also retun  
 *    after the indicated number of msecs. 
 *       
 *
 * @param[in]  str          
 *   Pointer to the string command for the modem.
 * @param[in]  expectResp   
 *   Pointer to the expected string response from modem.
 *   For no expected response, pass NULL.
 * @param[in]  timeout_ms   
 *   Timeout to wait for expected response.
 *   If no expected response (NULL) then this is ignored.
 *
 * @return bool indicates if the expected response is found.
 ******************************************************************************/
 bool modem_sendAT(char* str, char* expectResp, uint32_t timeout_ms)
{
    char resp[240];
    bool retb=false;        // did we find the expected response?

    // check the input buffer for OMA messages
    modem_checkForOmaDmStart();

    // send the provided AT command
    ModemSend(&modemManager, (uint8_t *)str, strlen (str));
    ModemSend(&modemManager, (uint8_t*)"\r\n", 2);

    if (expectResp == NULL)
    {
        return true;
    }
    else 
    {
        // we check for a response
        retb = modem_waitForResponse(expectResp, resp, 
                                     sizeof(resp)-1, timeout_ms);
    } 

    return retb;

} // modem_sendAT


/***************************************************************************//**
 * @brief
 *   Sends the specified AT command UART 0 and waits for the specified response.
 *   It also returns the response which includes the match.
 *
 * @note
 *   This function waits for the specified response from UART 0. It also returns  
 *    the response containing the specified response. It will also retun after  
 *    the indicated number of msecs. 
 *       
 *
 * @param[in]  str          
 *   Pointer to the string command for the modem.
 * @param[in]  expectResp   
 *   Pointer to the expected string response from modem.
 *   For no expected response, pass NULL.
 * @param[out] responseBuff
 *   Pointer to the buffer to contain the response.
 * @param[in]  responseBuffLen
 *   Size of the buffer to hold the response.
 * @param[in]  timeout_ms   
 *   Timeout to wait for expected response.
 *   If no expected response (NULL) then this is ignored.
 *
 * @return bool indicates if the expected response is found.
 ******************************************************************************/
 bool modem_sendATGetReply(char* str, char* expectResp, char *responseBuff, 
                           uint8_t responseBuffLen, uint32_t timeout_ms)
{
    bool retb=false;        // did we find the expected response?

    // check the input buffer for OMA messages 
    modem_checkForOmaDmStart();
    
    // send the provided AT command
    ModemSend(&modemManager, (uint8_t *)str, strlen (str));
    ModemSend(&modemManager, (uint8_t*)"\r\n", 2);

    if (expectResp == NULL)
    {
        retb = true;
    }
    else 
    {
        // we check for a response
        retb = modem_waitForResponse(expectResp, responseBuff, 
                                     responseBuffLen, timeout_ms);
    } //  response

    return retb;

} // modem_sendATGetReply

/***************************************************************************//**
 * @brief
 *   Sends the AT command to get the modem firmware version.
 *
 * @note    
 *
 * @param[out] responseBuff
 *   Pointer to the buffer to contain the response.
 * @param[in]  responseBuffLen
 *   Size of the buffer to hold the response.
 *
 * @return bool indicates if the expected response is found.
 ******************************************************************************/
 bool modem_getFirmwareVersion(char *responseBuff, uint8_t responseBuffLen)
{
    bool result = false;        // did we find the expected response?
    char    stringCgmrResponse[64];
    
    // issue the AT#CGMR command to get the current firmware version from the modem.
    if (modem_sendATGetReply ("AT#CGMR", "#CGMR:", stringCgmrResponse, 
                              sizeof(stringCgmrResponse), 2000))
	{
        char    *tempPtr;
        int     i;
        
        tempPtr = strstr(stringCgmrResponse, "#CGMR:");
        if (tempPtr)
        {
            // skip the 7 chars in the "#CGMR: " string
            tempPtr += 7;
            i = 0;
            
            // copy over the string until we reach the end of the line
            while ((tempPtr) && (*tempPtr != '\r') && (i < responseBuffLen))
            {
                responseBuff[i++] = *tempPtr;
                tempPtr++;
                
                // check to see if we have reached the end of the string
                if (*tempPtr == '\r')
                {
                    responseBuff[i] = 0;
                    result = true;
                }
            }
            
        }
    }
    return result;
}

/***************************************************************************//**
 * @brief
 *   Sends the AT command to get the modem model identification.
 *
 * @note    
 *
 * @return bool indicates if the modem model is one of the supported values.
 ******************************************************************************/
static E_MODEM_CARRIERS modem_getModemModel(void)
{
    E_MODEM_CARRIERS carrier = MODEM_CARRIER_NONE;        
    uint8_t    stringCgmmResponse[64];
    uint8_t    stringModemModel[32];
    
    memset (stringModemModel, 0, sizeof(stringModemModel));
    
    // issue the AT#CGMM command to get the current firmware version from the modem.
    if (modem_sendATGetReply ("AT#CGMM", "#CGMM:", (char *)stringCgmmResponse, 
                              sizeof(stringCgmmResponse), 2000))
	{
        char    *tempPtr;
        int     i;
        
        tempPtr = strstr((char *)stringCgmmResponse, "#CGMM:");
        if (tempPtr)
        {
            // skip the 7 chars in the "#CGMM: " string
            tempPtr += 7;
            i = 0;
            
            // copy over the string until we reach the end of the line
            while ((tempPtr) && 
                   (*tempPtr != '\r') && 
                   (i < sizeof(stringModemModel)))
            {
                stringModemModel[i++] = *tempPtr++;
                
                // check to see if we have reached the end of the string
                if (*tempPtr == '\r')
                {
                    stringModemModel[i] = 0;
                    if (strncmp((char *)stringModemModel, 
                                (char *)attModemModel, 
                                strlen((char *)attModemModel)) == NULL)
                    {
                        carrier = MODEM_CARRIER_ATT;
                    }
                    else if (strncmp((char *)stringModemModel, 
                                     (char *)verizonModemModel, 
                                     strlen((char *)verizonModemModel)) == NULL)
                    {
                        carrier = MODEM_CARRIER_VERIZON;
                    }
                }
            }
            
        }
    }
    
    return carrier;
}


/***************************************************************************//**
 * @brief
 *   gets the current time from the Telit LE-910 modem.
 *
 * @note
 *   This function gets the current time information from the Telit LE-910 modem
 *   using the AT#CCLK? command. This time is set by the Verizon network when
 *   the modem registers. The response to the command is something like:
 *      #CCLK: "15/02/24,13:03:26-20,0"
 *       
 *
 * @param[out] currTime - points to a tm structure to contain the current time
 * @param[out] timezone - points to a variable to provide the time zone offset
 *                        in 1/4 hour units
 *
 * @return bool indicating if the command was successful
 ******************************************************************************/
bool modem_getCurrentTime (struct tm 	*pCurrTime, int8_t   *timezone)
{
    int16_t     tempInt;
    char        stringCclkBuffer[64];
    
    DEBUG_OUTPUT_TEXT("SENDING CCLK\n\n\r");
    // issue the AT#CCLK command to get the current local time from the modem.
    if (modem_sendATGetReply ("AT#CCLK?", "#CCLK:", stringCclkBuffer, 
                              sizeof(stringCclkBuffer), 2000))
	{
        char    tempBuffer[24];
        DEBUG_OUTPUT_TEXT(stringCclkBuffer);
        
        // extract each part of the time using fixed locations since 
        //  this result is a fixed size string
		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_TIMEZONE]);
		*timezone = (int8_t) tempInt;
        sprintf (tempBuffer, "timezone: %d\n", *timezone);
        DEBUG_OUTPUT_TEXT (tempBuffer);
        
		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_ISDST]);
		pCurrTime->tm_isdst = tempInt;

		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_YEAR]);
        // year is since 1900 in mktime so add 100 if using mktime
		pCurrTime->tm_year = tempInt;    
        
		// use the year to determine if the date has been set by the network
        // if the time is not set by the network, we see the year be 0 or 99
		if ((tempInt < MODEM_CLOCK_YEAR_LIMIT_MIN) || 
            (tempInt > MODEM_CLOCK_YEAR_LIMIT_MAX))
        {
			return false;
        }

		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_MONTH]);
        // 0-based for mktime so subtract 1 if using mktime
		pCurrTime->tm_mon = tempInt;        
		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_DAY]);
		pCurrTime->tm_mday = tempInt;
		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_HOUR]);
		pCurrTime->tm_hour = tempInt;
		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_MINUTE]);
		pCurrTime->tm_min = tempInt;
		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_SECOND]);
		pCurrTime->tm_sec = tempInt;
        
        return true;
        
    }

    return false;
    
}


/***************************************************************************//**
 * @brief
 *   sets the RTC time based on the time from the LTE network.
 *
 * @note
 *   This function calls the routine to get the time from the LTE network and
 *   if successful it sets the RTC time.
 *       
 *
 * @param[in] none
 * @param[out] none
 *
 * @return bool indicating if the command was successful
 ******************************************************************************/
bool     modem_setRTC(void)
{
    struct tm   currTime;
    int8_t      timezone;
    char        debugString[80];
    time_t      timeT;
    U_NEPTUNE_TIME  neptuneTime; 
    
    if (modem_getCurrentTime(&currTime, &timezone))
    {
        sprintf (debugString, "Date: %d/%d/%d time: %d:%d:%d timezone: %d dst: %d\n", 
                 currTime.tm_mon, currTime.tm_mday, currTime.tm_year,
                 currTime.tm_hour, currTime.tm_min, currTime.tm_sec, 
                 timezone, currTime.tm_isdst);
        
        DEBUG_OUTPUT_TEXT (debugString);
        
        // have to add 100 since unix time assumes year since 1900
        currTime.tm_year += 100;  
        
        // subtract 1 from month since months should be 0-11
        currTime.tm_mon -= 1;
        
        timeT = mktime (&currTime);
        
        sprintf (debugString, "UTC time %s\n", ctime(&timeT));
        DEBUG_OUTPUT_TEXT (debugString);
        
        neptuneTime.S64 = timeT;
        
        app_rtc_time_set (&neptuneTime);
        
        return true;  
    }
    else
        return false;
    
}

/***************************************************************************//**
 * @brief
 *   simple routine to make sure the context is setup properly.
 *
 * @note
 *   This step became more complicated since for Verizon we do not set
 *    APN but wait for it to be updated. For AT&T we set the APN.
 *       
 *
 * @param[in] none
 * @param[out] none
 *
 * @return bool indicating if the context is setup correctly
 ******************************************************************************/
static bool  contextIdReady (void)
{
    bool result = false;
    MODEM_MANAGER   *pModem = &modemManager;
    char    tempBuffer[128];

    if (pModem->carrier == MODEM_CARRIER_ATT)
    {
        // for AT&T we set the APN to the value in the configuration data
        sprintf (tempBuffer, "at+cgdcont=%d,\"IPV4V6\",\"%s\",\"\",0,0",
                 pModem->contextId,
                 uMiuCarrierConfig.sMIUCarrierConfig.abyAttApnString);
        if (modem_sendAT(tempBuffer, "OK", 1000))
        {
            result = true;
        }
    }
    else if (pModem->carrier == MODEM_CARRIER_VERIZON)
    {
        // no longer set the APN but make sure it is updated from the default
        sprintf (tempBuffer, "+CGDCONT: %1d", pModem->contextId);
        if (modem_sendATGetReply("AT+CGDCONT?", tempBuffer, replyBuff, 
                                 sizeof(replyBuff), 5000)) 
        {
            DEBUG_OUTPUT_TEXT ("APN: ");
            DEBUG_OUTPUT_TEXT (replyBuff);
            DEBUG_OUTPUT_TEXT ("<<<\n\r");
            // look at the APN for the specified context
            if ((strstr(replyBuff, ",\"IPV4V6\",\"vzwinternet") == NULL) &&
                (strstr(replyBuff, ",\"IPV4V6\",\"VZWINTERNET") == NULL))
            {
                result = true;
            }
        }
    }
    
    return result;
}

/***************************************************************************//**
 * @brief
 *   State machine to setup a data connection on the Telit LE-910 modem.
 *
 * @note
 *   This function uses a small state machine to go through the steps to set
 *   up a data connection over the LTE network.
 *   Currently the commands needed are:
 *      AT+CGDCONT=3,"IP","vzwinternet","",0,0
 *          define the PDP context (only needs to be done once)
 *      AT#SCFG=3,3,300,90,600,50
 *          set the packet size and TX timeout values
 *      AT#SGACT=3,1
 *          activate context 3
 *      AT#SD=3,0,<port number>,<server address>,255,1,0
 *          connect to the server using TCP and online data mode
 *       
 *
 * @param[in] dataConnEvent - event to be processed
 * @param[in] eventDataPtr - points to any event data
 * @param[in] eventDataSize - size of the event data
 *
 * @return bool - true indicates data connection is 
 ******************************************************************************/
bool    modem_dataConnSetup (uint8_t  dataConnEvent, uint8_t  *eventDataPtr, 
                             uint16_t   eventDataSize)
{
    bool result = false;
    MODEM_MANAGER   *pModem = &modemManager;
    static uint32_t   retryTimeLimit;
    char    commandBuffer[128];

    
    switch (pModem->connState)
    {
        case DATA_CONN_STATE_DELAY_MODEM_POWER:
            // delay to allow the modem to power up and initialize
            if (++(pModem->startUpDelayCnt) >= 1000)   
            {
                DEBUG_OUTPUT_TEXT ("check registration\n");
                pModem->connState = DATA_CONN_STATE_DETERMINE_MODEM_SPEED;
                pModem->startUpDelayCnt = 0;
                retryTimeLimit = Timers_GetMsTicks() + 
                                              MODEM_RETRY_TIME_LIMIT_MSEC;
            }
            break;
        case DATA_CONN_STATE_DETERMINE_MODEM_SPEED:
            if (++(pModem->startUpDelayCnt) >= 100)  
            {
                pModem->startUpDelayCnt = 0;
                if (modem_setModemPortSpeed())
                {
                    pModem->connState = DATA_CONN_STATE_WAIT_MODEM_REGISTER;
                }
                else if (Timers_GetMsTicks() > retryTimeLimit)
                {
                    DEBUG_OUTPUT_TEXT ("Modem FAILURE!\n");
                    // give up and power down since we cannot send commands
                    powerOffTimeLimit = Timers_GetMsTicks() + 
                                           MODEM_POWER_OFF_TIME_LIMIT_MSEC;
                    pModem->state = MS_DISCONNECT_WAIT;
                    pModem->modemFailure = MODEM_ERROR_UART_FAILED;
                }
            }
            break;
        case DATA_CONN_STATE_WAIT_MODEM_REGISTER:
            // do not need to check registration every tick (10 msec)
            if (++(pModem->startUpDelayCnt) >= 100)  
            {
                pModem->startUpDelayCnt = 0;
                // stay in this state until the modem is registered on a network
                if (modem_isRegistered())
                {
                    modem_diag_saveModemTaskTimeDuration (MODEM_DIAG_TIME_REGISTRATION);
                    DEBUG_OUTPUT_TEXT("modem registered\r\n"); 
                    pModem->connState = DATA_CONN_STATE_DEFINE_CONTEXT;
                    
                    retryTimeLimit = Timers_GetMsTicks() + 
                                             MODEM_APN_UPDATE_TIME_LIMIT_MSEC;
                    
                }
                else
                {
                    if (Timers_GetMsTicks() > retryTimeLimit)
                    {
                        // give up and go ahead and gather diag info
                        modem_diag_incrementCount (MODEM_DIAG_FAIL_REGISTRATION);
                        pModem->state = MS_GET_INFO;
                        pModem->modemFailure = MODEM_ERROR_REGISTER_FAILED;
                    }
                }
            }
            break;        
        case DATA_CONN_STATE_DEFINE_CONTEXT:
            // do not need to check registration every tick (10 msec)
            if (++(pModem->startUpDelayCnt) >= 200)  
            {
                pModem->startUpDelayCnt = 0;
                // for AT&T set the APN 
                // for Verizon do not set it 
                //     but make sure it is updated from the default
                if (contextIdReady())
                {
                    pModem->connState = DATA_CONN_STATE_CONFIG_CONTEXT;
                    pModem->currStateWaitTime = 1;
                    DEBUG_OUTPUT_TEXT ("sent cgdcont\n");
                    retryTimeLimit = Timers_GetMsTicks() + 
                                            MODEM_RETRY_TIME_LIMIT_MSEC;
                }
                else if (Timers_GetMsTicks() > retryTimeLimit)
                {
                    DEBUG_OUTPUT_TEXT ("APN update timeout!!!\n");

                    // give up and go ahead and gather diag info
                    modem_diag_incrementCount (MODEM_DIAG_FAIL_REGISTRATION);
                    pModem->state = MS_GET_INFO;
                    pModem->modemFailure = MODEM_ERROR_REGISTER_FAILED;
                }
            }
            break;
        case DATA_CONN_STATE_CONFIG_CONTEXT:
            // use the configured context ID
            sprintf (commandBuffer, "AT#SCFG=3,%d,1400,90,450,1",
                        pModem->contextId);
            if (modem_sendAT(commandBuffer, "OK", 1000))
            {
                pModem->connState = DATA_CONN_STATE_ACTIVATE_CONTEXT;
                pModem->currStateWaitTime = 1;
                DEBUG_OUTPUT_TEXT ("sent scfg\n");
                retryTimeLimit = Timers_GetMsTicks() + 
                                          MODEM_RETRY_TIME_LIMIT_MSEC;
            }
            else if (Timers_GetMsTicks() > retryTimeLimit)
            {
                // give up and go ahead and gather diag info
                pModem->state = MS_GET_INFO;
                pModem->modemFailure = MODEM_ERROR_CONFIG_FAILED;
            }
            break;
        case DATA_CONN_STATE_ACTIVATE_CONTEXT:
            modem_diag_saveModemTaskTimeStart(MODEM_DIAG_TIME_CONTEXT_ACTIVATION);
            // use the configured context ID
            sprintf (commandBuffer, "AT#SGACT=%d,1",
                        pModem->contextId);
            if (modem_sendATGetReply(commandBuffer, "#SGACT: 10", replyBuff, 
                                     sizeof(replyBuff), 5000))  
            {
                modem_diag_saveModemTaskTimeDuration(MODEM_DIAG_TIME_CONTEXT_ACTIVATION);
                // save the IP address in the modem diags buffer
                modem_diag_saveIpAddress (replyBuff);
                pModem->connState = DATA_CONN_STATE_CONNECT;
                pModem->currStateWaitTime = 1;
                retryTimeLimit = Timers_GetMsTicks() + 
                                                    MODEM_RETRY_TIME_LIMIT_MSEC;
                
                DEBUG_OUTPUT_TEXT ("sent sgact\n");
            }
            else
            {
                // check to see if the context is already open
                sprintf (commandBuffer, "#SGACT: %d,",
                        pModem->contextId);
                if (modem_sendATGetReply("AT#SGACT?", commandBuffer, replyBuff, 
                                         sizeof(replyBuff), 10000))
                {
                    sprintf (commandBuffer, "%d,1",
                            pModem->contextId);
                    if (strstr (replyBuff, commandBuffer) != NULL)
                    {
                        DEBUG_OUTPUT_TEXT ("already active\n");
                        modem_diag_saveModemTaskTimeDuration(MODEM_DIAG_TIME_CONTEXT_ACTIVATION);
                        pModem->connState = DATA_CONN_STATE_CONNECT;
                        pModem->currStateWaitTime = 1;
                        retryTimeLimit = Timers_GetMsTicks() + 
                                                  MODEM_RETRY_TIME_LIMIT_MSEC;
                    }
                }
                if (Timers_GetMsTicks() > retryTimeLimit)
                {
                    // give up and go ahead and gather diag info
                    modem_diag_incrementCount (MODEM_DIAG_FAIL_CONTEXT_ACTIVATION);
                    pModem->state = MS_GET_INFO;
                    pModem->modemFailure = MODEM_ERROR_CONTEXT_ACTIVATION_FAILED;
                }
            }
            break;
        case DATA_CONN_STATE_ACTIVATE_CONTEXT_WAIT_RESPONSE:
            switch (dataConnEvent)
            {
                case DATA_CONN_EVENT_TIMEOUT:
                    DEBUG_OUTPUT_TEXT ("context activation TIME OUT\n");
                    break;
                case DATA_CONN_EVENT_INC_DATA:
                    if (strstr ((char*)eventDataPtr, "OK") != NULL)
                    {
                        pModem->connState = DATA_CONN_STATE_CONNECT; 
                        pModem->currStateWaitTime = 1;
                        DEBUG_OUTPUT_TEXT ("context activated\n");
                    }
                    else
                        DEBUG_OUTPUT_TEXT ("not yet\n");
                    break;
                default:
                    break;
            }
            break;
        case DATA_CONN_STATE_CONNECT:
        {
            char    commandBuffer[128];
            // do not try to connect so quickly
            if (++(pModem->startUpDelayCnt) >= 100)  
            {
                CONNECTION_PARAMS *pConnParams;
                // use the server and port number from the command parameters
                pConnParams = (CONNECTION_PARAMS *)pModem->pCmdParams;
                sprintf (commandBuffer, "at#sd=3,0,%d,\"%s\",255,1,0", 
                         pConnParams->port, pConnParams->host);
                DEBUG_OUTPUT_TEXT ("send #SD\n");
                // save connection info in case we need to reconnect
                pModem->noCarrierCmpIdx = 0;
                pModem->sawNoCarrier = false;
                pModem->connPort = pConnParams->port;
                strncpy (pModem->connServerAddress, pConnParams->host, 
                         sizeof (pModem->connServerAddress));
                
                modem_diag_saveModemTaskTimeStart(MODEM_DIAG_TIME_SERVER_CONNECT);
                if (modem_sendAT(commandBuffer, "CONNECT", MODEM_CONNECT_DELAY_MSEC))
                {
                    modem_diag_saveModemTaskTimeDuration(MODEM_DIAG_TIME_SERVER_CONNECT);
                    pModem->connState = DATA_CONN_STATE_CONNECTED;
                    pModem->currStateWaitTime = 1;
                    result = true;
                    modem_diag_saveModemTaskTimeStart (MODEM_DIAG_TIME_DATA_TRANSFER);
                    DEBUG_OUTPUT_TEXT ("sent #sd\n");
                }
                else
                {
                    if (Timers_GetMsTicks() > retryTimeLimit)
                    {
                        // give up and go ahead and gather diag info
                        modem_diag_incrementCount (MODEM_DIAG_FAIL_SERVER_CONNECT);
                        pModem->state = MS_GET_INFO;
                        pModem->modemFailure = MODEM_ERROR_CONNECT_FAILED;
                    }
                }
            }
            break;
        }
        case DATA_CONN_STATE_CONNECT_WAIT_RESPONSE:
            if (modem_checkResponse())
            {
                if (strstr ((char *)pModem->incDataBuffer, "CONNECT") != NULL)
                {
                    modem_diag_saveModemTaskTimeDuration(MODEM_DIAG_TIME_SERVER_CONNECT);
                    pModem->connState = DATA_CONN_STATE_CONNECTED;
                    pModem->currStateWaitTime = 1;
                    DEBUG_OUTPUT_TEXT ("CONNECTED 3\n");
                    modem_diag_saveModemTaskTimeStart (MODEM_DIAG_TIME_DATA_TRANSFER);
                }
            }
            else if (--pModem->currStateWaitTime == 0)
            {
                DEBUG_OUTPUT_TEXT ("Connect timeout\n");
            }
            break;
        case DATA_CONN_STATE_CONNECTED:
        {
            
            DEBUG_OUTPUT_TEXT ("CONNECTED 2\n");
            pModem->currStateWaitTime = 3;
            break;
        }
        case DATA_CONN_STATE_DISCONNECTED:
            // change back to the initial state to start a new connection
            pModem->connState = DATA_CONN_STATE_DELAY_MODEM_POWER;
            break; 
        default:
            break;
    }
    return result;
}

/***************************************************************************//**
 * @brief
 *   This function contains a basic state machine to wait for the modem to 
 *    wait for the modem to register. 
 *
 * @note
 *       
 *
 * @param[in] none
 * @param[out] none
 *
 * @return int8_t - a negative number indicates an error
 *                  0 indicates still trying
 *                  1 indicates the modem is registered
 ******************************************************************************/
int8_t    modem_processModemStartup()
{
    int8_t   result = 0;
    MODEM_MANAGER   *pModem = &modemManager;
    static uint32_t   substateTimeLimit;

    
    switch (pModem->subState)
    {
        case MSS_WAIT_FOR_POWER_UP:
            // delay to allow the modem to power up and initialize
            if (++(pModem->startUpDelayCnt) >= 1000)   
            {
                DEBUG_OUTPUT_TEXT ("check registration\n");
                pModem->subState = MSS_DETERMINE_MODEM_SPEED;
                pModem->startUpDelayCnt = 0;
                substateTimeLimit = Timers_GetMsTicks() + 
                                              MODEM_RETRY_TIME_LIMIT_MSEC;
            }
            break;
        case MSS_DETERMINE_MODEM_SPEED:
            if (++(pModem->startUpDelayCnt) >= 100)  
            {
                pModem->startUpDelayCnt = 0;
                if (modem_setModemPortSpeed())
                {
                    pModem->subState = MSS_WAIT_FOR_REGISTRATION;
                }
                else
                {
                    if (Timers_GetMsTicks() > substateTimeLimit)
                    {
                        result = -1;
                    }
                }
            }
            break;
        case MSS_WAIT_FOR_REGISTRATION:
            // do not need to check registration every tick (10 msec)
            if (++(pModem->startUpDelayCnt) >= 100)  
            {
                pModem->startUpDelayCnt = 0;
                // stay in this state until the modem is registered on a network
                if (modem_isRegistered())
                {
                    modem_diag_saveModemTaskTimeDuration (MODEM_DIAG_TIME_REGISTRATION);
                    DEBUG_OUTPUT_TEXT("modem registered\r\n"); 
                    result = 1;
                    
                }
                else
                {
                    if (Timers_GetMsTicks() > substateTimeLimit)
                    {
                        result = -1;
                    }
                }
            }
            break;
    }
    return result;
}

/***************************************************************************//**
 * @brief
 *   This function contains a basic state machine to do a modem upgrade using 
 *    FOTA. The details of what to use for the upgrade are saved in the command
 *    parameters which is type UPDATE_MODEM_FIRMWARE_PARAMS.
 *
 * @note
 *       
 *
 * @param[in] none
 * @param[out] none
 *
 * @return int8_t - a negative number indicates an error
 *                  0 indicates still trying
 *                  1 indicates the modem has been upgraded
 ******************************************************************************/
int8_t    modem_processModemUpdate()
{
    int8_t   result;
    MODEM_MANAGER   *pModem = &modemManager;
    static uint32_t   substateTimeLimit;
    UPDATE_MODEM_FIRMWARE_PARAMS   *pFotaParams;
    uint8_t            messageBuff[64];
    static bool     downloadSuccess;
    uint8_t            debugBuff[64];
    uint8_t            *tempPtr;
    
    result = 0;
    switch (pModem->subState)
    {
        case MSS_START_MODEM_UPDATE:
            substateTimeLimit = Timers_GetMsTicks() + MODEM_RETRY_TIME_LIMIT_MSEC;
            pModem->subState = MSS_DOWNLOAD_FILE;
            pModem->startUpDelayCnt = 0; 
            break;
            
        case MSS_DOWNLOAD_FILE:
        {
            char    commandBuffer[150];

            // delay before command (10 msec tick)
            if (++(pModem->startUpDelayCnt) >= 1200)  
            {
                pModem->startUpDelayCnt = 0;
                pFotaParams = (UPDATE_MODEM_FIRMWARE_PARAMS *)pModem->pCmdParams;
                sprintf (commandBuffer, 
                         "AT#FTPGETOTAENH=\"%s\",%d,\"%s\",\"%s\",\"%s\"", 
                        pFotaParams->ftpServer,
                        pFotaParams->ftpPort, 
                        pFotaParams->fotaFilename,
                        pFotaParams->username,
                        pFotaParams->password);
                    
                DEBUG_OUTPUT_TEXT ("Sending FOTA command\n\r");
                if (modem_sendAT(commandBuffer, "OK", 3000))
                {
                    pModem->subState = MSS_WAIT_FOR_DOWNLOAD_COMPLETE;
                    substateTimeLimit = Timers_GetMsTicks() + 
                                           MODEM_FOTA_DOWNLOAD_TIME_LIMIT_MSEC;
                    pModem->startUpDelayCnt = 0; 
                    downloadSuccess = false;
                    DEBUG_OUTPUT_TEXT ("OK FOTA command\n\r");
                }
                else if (Timers_GetMsTicks() > substateTimeLimit)
                {
                    DEBUG_OUTPUT_TEXT ("FTP command timeout\n\r");
                    result = -1;
                }
            }

                
        }
        break;
        case MSS_WAIT_FOR_DOWNLOAD_COMPLETE:
            // check every 3 second (10 msec tick)
            if (++(pModem->startUpDelayCnt) >= 300)  
            {
                pModem->startUpDelayCnt = 0;
                
                // check for a timeout
                if (Timers_GetMsTicks() > substateTimeLimit)
                {
                    DEBUG_OUTPUT_TEXT ("Download timeout\n\r");
                    result = -1;
                }
                
                if (modem_waitForMessage((uint8_t *)otaevString, messageBuff, 
                                         sizeof(messageBuff), 100))
                {
                    sprintf ((char *)debugBuff, "progress: %s\n\r", messageBuff);
                    DEBUG_OUTPUT_TEXT ((char *)debugBuff);
                    if (strstr((char *)messageBuff, "#DREL"))
                    {
                        if (downloadSuccess)
                        {
                            DEBUG_OUTPUT_TEXT ("Download complete\n\r");
                            modem_sendAT("at#otaup=2", "OK", 3000);
                            pModem->subState = MSS_WAIT_FOR_UPDATE_COMPLETE;
                            substateTimeLimit = Timers_GetMsTicks() + 
                                                MODEM_FOTA_UPDATE_TIME_LIMIT_MSEC;
                        }
                        else
                        {
                            DEBUG_OUTPUT_TEXT ("Download failed\n\r");
                            pModem->subState = MSS_START_MODEM_UPDATE;
                            result = -1;
                        }
                    }
                    else
                    {
                        tempPtr = messageBuff + 6; // skip the #OTAEV
                        tempPtr = (uint8_t *)strchr((char *)tempPtr, '#');
                        if (tempPtr)
                        {
                            tempPtr++;
                            sprintf ((char *)debugBuff, "code: %s\n\r", tempPtr);
                            DEBUG_OUTPUT_TEXT((char *)debugBuff);
                            if (strncmp ((char *)tempPtr, "919", 3) == NULL)
                            {
                                downloadSuccess = true;
                            }
                        }
                    }
                }
            }
            break;
        case MSS_WAIT_FOR_UPDATE_COMPLETE:
            // check every 5 second (10 msec tick)
            if (++(pModem->startUpDelayCnt) >= 500)  
            {
                pModem->startUpDelayCnt = 0;
            
                // check for a timeout
                if (Timers_GetMsTicks() > substateTimeLimit)
                {
                    DEBUG_OUTPUT_TEXT ("Update failed\n\r");
                    result = -1;
                }

                // check to see if the version is the new version yet
                pFotaParams = (UPDATE_MODEM_FIRMWARE_PARAMS *)pModem->pCmdParams;
                
                // check the modem speed in case the update changed it 
                if (!modem_sendAT("AT", "OK", 1000))
                {
                    modem_setModemPortSpeed();
                }
                memset (messageBuff, 0, sizeof(messageBuff));
                if (modem_getFirmwareVersion ((char *)messageBuff, 
                                              sizeof(messageBuff)))
                {
                    // do not use strncmp as one version may be a substring 
                    if (strcmp ((char *)messageBuff, 
                                (char *)pFotaParams->newFirmwareVersion) == 0)
                    {
                        DEBUG_OUTPUT_TEXT("UPGRADE SUCCESSFUL\n\r");
                        result = 1;
                    }
                }
            }
            
            break;
    }
    return result;
}


/***************************************************************************//**
 * @brief
 *   drops the data connection.
 *
 * @note
 *   This function drops the data connection on the LTE network. Currently,
 *   we plan to do this by using the +++ escape code.
 *       
 *
 * @param[in] none
 * @param[out] none
 *
 * @return bool indicating if the command was successful
 ******************************************************************************/
bool modem_dropDataConnection()
{
//    char    responseBuff[64];
    bool    result = false;
    int     guardCtr = 0;

//#define USE_PLUSPLUSPLUS
#ifdef USE_PLUSPLUSPLUS    
    Timers_Loop_Delay (600);
    ModemSend(&modemManager, (uint8_t*)"+++", 3);
    Timers_Loop_Delay (600);
    
    result = modem_waitForResponse("OK", responseBuff, sizeof(responseBuff), 3000);
#else
    
    /* enable DTR (PE11) - data connection drops on transition to low */
    // should already be high
//    GPIO_PinOutSet(gpioPortE, 11);
    
//    Timers_Loop_Delay (300);
    GPIO_PinOutClear(gpioPortE, 11);
    DEBUG_OUTPUT_TEXT ("DTR low\n");
    
    Timers_Delay(500);  // delay to drop the data call
    
    // based on suggestion from Telit we should transition back to high 
    GPIO_PinOutSet(gpioPortE, 11);
    Timers_Delay(500);  // delay to drop the data call

    while ((!result) & (guardCtr++ < 10))
    {
        if (modem_sendAT("AT", "OK", 1000))
        {
            result = true;
            DEBUG_OUTPUT_TEXT ("data call ended\n");
        }
    }
#endif    

    return result;
}

/***************************************************************************//**
 * @brief
 *   drops the data connection.
 *
 * @note
 *   This function drops the data connection on the LTE network. Currently,
 *   we plan to do this by using the +++ escape code.
 *       
 *
 * @param[in] none
 * @param[out] none
 *
 * @return bool indicating if the command was successful
 ******************************************************************************/
uint16_t  modem_checkResponse()
{
    uint32_t    incBytesAvail = 0;
    char        debugBuffer[128];
    MODEM_MANAGER   *pModem = &modemManager;
    
    incBytesAvail = RingBufferUsed(pModem->pRxRingBuffer);
    if (incBytesAvail)
    {
//        char    tempStr[8];
//        uint8_t i;
        
        memset (pModem->incDataBuffer, 0, sizeof(pModem->incDataBuffer));
        RingBufferRead (pModem->pRxRingBuffer, pModem->incDataBuffer, 
                        incBytesAvail);
        sprintf (debugBuffer, "inc data: %d - %s\n", incBytesAvail, 
                 pModem->incDataBuffer);
        DEBUG_OUTPUT_TEXT (debugBuffer);
//#define DISPLAY_DATA_IN_HEX        
#ifdef DISPLAY_DATA_IN_HEX        
        sprintf (debugBuffer, "inc data: %d -", incBytesAvail);
        for (i=0; i<incBytesAvail; i++)
        {
    		sprintf (tempStr, "%02x ", pModem->incDataBuffer[i]);
    		strcat (debugBuffer, tempStr);
    		
    		if (!((i+1) % 16))   // want new line every 16 bytes
    		{
        		strcat (debugBuffer, "\n");
        		DEBUG_OUTPUT_TEXT (debugBuffer);
        		memset (debugBuffer, 0, sizeof(debugBuffer));
    		}   
            strcat (debugBuffer, "\n");
            DEBUG_OUTPUT_TEXT (debugBuffer);
        }
#endif         
    }

    return incBytesAvail;
}


/****************************************************************************
 *
 *  This starts the portion which contains the common interface to the modem 
 *    routines to be usedby the common code base.
 *
 ****************************************************************************/
 
const MODEM_MANAGER* ModemGet(void)
{
    return &modemManager;
}

/******************************************************************************
*******************************************************************************
**
**            Filename:  ModemStub.c
**       Function Name:  ModemInit
**    
**              Author: Duncan Willis
**             Created: 17 March 2015
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: pModem - The modem driver 
**                      initParams Optional pointer to init params
**
**    Function Returns: None
**
**         Description: Init the object - this should not block. Do any 
**                      lengthy tasks as a command.
**
******************************************************************************/
int32_t ModemInit(void *pCtx, const void* initParams)
{
    int32_t e = OK_STATUS;
    MODEM_MANAGER   *pModem = &modemManager;

    pModem->pInterface = &modemInterface;
    pModem->cmd = CON_CMD_NULL;
    
    pModem->omaDmInProgress = false;


    /// @todo Put non-blocking inits here (eg clearing buffers, setting up 
    /// the object.
    
    pModem->connState = DATA_CONN_STATE_DELAY_MODEM_POWER;
    
    return e;
}

/*******************************************************************************
**
**            Filename: ModemStub.c
**       Function Name: ModemCommand
**    
**              Author: Duncan Willis
**             Created: 17 March 2015
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: pCtx - The modem driver
**                      cmd The command enumeration
**                      pCmdParamms Optional pointer to static data (may be null)
**
**    Function Returns: OK_STATUS if command accepted, else error code.
**
**         Description: Run a command (eg CONNECT)
**
******************************************************************************/
static int32_t ModemCommand(void *pCtx, E_CONNECTION_COMMAND cmd, 
                            void* pCmdParams)
{
    int32_t r = OK_STATUS;

    MODEM_MANAGER   *pModem = &modemManager;

    if (cmd == CON_CMD_GET_RX_COUNT)
    {
        if (uartRxMultipleChars)
        {
            DEBUG_OUTPUT_TEXT_AND_DATA ("multi chars: ", &uartRxMultipleChars, 1);
            uartRxMultipleChars = 0;
        }
        if (uartRxBufferOverflow)
        {
            DEBUG_OUTPUT_TEXT_AND_DATA ("OVERFLOW: ", &uartRxBufferOverflow, 1);
            uartRxBufferOverflow = 0;
        }

        // for this command we return the number of bytes in the modem RX buffer
        return (int) RingBufferUsed(pModem->pRxRingBuffer);
    }
    
    /// @todo - check that the cmd is allowable, or not already busy for example
    if (pModem->cmd == CON_CMD_NULL)
    {
        pModem->cmd = cmd;
        pModem->pCmdParams = pCmdParams;
    }
    else
    {
        if ((pModem->cmd == CON_CMD_OPEN_CONNECTION) && (cmd == CON_CMD_OPEN_SOCKET))
        {
            DEBUG_OUTPUT_TEXT ("modem open socket\n");
            pModem->cmd = cmd;
        }            
        else
        {
            r = ECODE_BAD;
        }
    }

    return r;
}

/******************************************************************************
*******************************************************************************
**
**            Filename: ModemStub.c
**       Function Name: ModemTick
**    
**              Author: Duncan Willis
**             Created: 17 March 2015
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: pCtx - The modem driver
**                      tickIntervalMs The tick time in ms
**
**    Function Returns: None
**
**         Description: Run the state machine - must not block.  
******************************************************************************/
static void ModemTick(void *pCtx, uint32_t tickIntervalMs)
{
    MODEM_MANAGER   *pModem = &modemManager;
    int8_t          startupResult;
       

    switch (pModem->state)
        {
       case MS_RESET:
       case MS_IDLE:
           switch (pModem->cmd)
           {
           case CON_CMD_NULL:
               break;

           case CON_CMD_OPEN_CONNECTION:
               pModem->cmd = CON_CMD_NULL;
               pModem->state = MS_CONNECT_BEGIN;
               modem_diag_saveConnectionTime ();
               break;

           case CON_CMD_CLOSE_CONNECTION:              
               pModem->cmd = CON_CMD_NULL;
               pModem->state = MS_DISCONNECT_BEGIN;
               DEBUG_OUTPUT_TEXT ("close conn: 1\n");
               break;

           case CON_CMD_GET_INFO:
               pModem->cmd = CON_CMD_NULL;
               pModem->state = MS_GET_INFO;
               break;
           
           case CON_CMD_ASSERT_LTE:
               modem_carrierAssert_processCmd(
                               (ASSERT_LTE_CARRIER_PARAMS *)pModem->pCmdParams);
               pModem->cmd = CON_CMD_NULL;
               break;

           case CON_CMD_GET_APN:
               modem_diag_getCurrentApn();
               pModem->cmd = CON_CMD_NULL;
               break;

           case CON_CMD_SET_APN:
               modem_setApn_processCmd((UPDATE_APN_PARAMS *)pModem->pCmdParams);
               pModem->cmd = CON_CMD_NULL;
               break;

           case CON_CMD_UPDATE_MODEM_FIRMWARE:
               pModem->cmd = CON_CMD_NULL;
               pModem->state = MS_WAIT_REGISTERED;
               ModemOpenHelper();
               pModem->subState = MSS_WAIT_FOR_POWER_UP;
               pModem->nextTask = MODEM_TASKS_FOTA;
               pModem->nextState = MS_MODEM_FOTA;
               pModem->startUpDelayCnt = 0;
               break;
               
           case CON_CMD_SHUTDOWN_MODEM:
               pModem->cmd = CON_CMD_NULL;
               pModem->state = MS_START_SHUTDOWN;
               break;
               
           case CON_CMD_GET_SIGNAL_QUALITY:
               pModem->cmd = CON_CMD_NULL;
               pModem->state = MS_GET_SIGNAL_QUALITY;
               break;

           default:
               pModem->cmd = CON_CMD_NULL;
               break;
           }
           break;

       case MS_CONNECT_BEGIN:
            ModemOpenHelper();
            pModem->state = MS_CONNECT_WAIT;
            break;       
       
       case MS_CONNECT_WAIT:
            // wait for conn if needed
            if (modem_dataConnSetup(0, NULL, 0))
            {    
                pModem->state = MS_CONNECTION_OPEN_SOCKET_CLOSED;
                pModem->stateTimeLimit = Timers_GetMsTicks() + 
                                              MODEM_RETRY_TIME_LIMIT_MSEC;
            }
            break;
            
       case MS_CONNECTION_OPEN_SOCKET_CLOSED:
            if (pModem->sawNoCarrier)
            {
                modem_ReconnectIfNeeded();
            }
            // advance to connected state when we get the open socket command
            if (pModem->cmd == CON_CMD_OPEN_SOCKET)
            {
                DEBUG_OUTPUT_TEXT ("open socket cmd\n");
                pModem->state = MS_CONNECTED;
                pModem->cmd = CON_CMD_NULL;
            }
            else if (pModem->cmd == CON_CMD_CLOSE_CONNECTION)
            {
                pModem->cmd = CON_CMD_NULL;
                // disconnect the data session to the MQTT server
                pModem->state = MS_DISCONNECT_BEGIN;
               DEBUG_OUTPUT_TEXT ("close conn: 2\n");
            }
            // check for a timeout
            if (Timers_GetMsTicks() > pModem->stateTimeLimit)
            {
                DEBUG_OUTPUT_TEXT ("socket closed timeout!\n\r");
                pModem->state = MS_GET_INFO;
            }
            break;

       case MS_CONNECTED:
            if (pModem->cmd == CON_CMD_CLOSE_CONNECTION)
            {
                
                pModem->cmd = CON_CMD_NULL;
                // disconnect the data session to the MQTT server
                pModem->state = MS_DISCONNECT_BEGIN;
               DEBUG_OUTPUT_TEXT ("close conn: 3\n");
            }
            else if (pModem->cmd == CON_CMD_OPEN_CONNECTION)
            {
                // Clear this open out so we can get the close
                pModem->cmd = CON_CMD_NULL;
            }
            else if (pModem->cmd == CON_CMD_CLOSE_SOCKET)
            {
                pModem->state = MS_CONNECTION_OPEN_SOCKET_CLOSED;
                pModem->cmd = CON_CMD_NULL;
                DEBUG_OUTPUT_TEXT ("close socket cmd\n");
                pModem->stateTimeLimit = Timers_GetMsTicks() + 
                                                MODEM_RETRY_TIME_LIMIT_MSEC;
            }
            else if (pModem->sawNoCarrier)
            {
                //modem_ReconnectIfNeeded();
                pModem->cmd = CON_CMD_CLOSE_SOCKET; 
            }
            break;
            
       case MS_DISCONNECT_BEGIN:
            if (ModemClose() == OK_STATUS)
            {
                // set rtc from network time -  
                //  for now we are setting every time but need to determine plan
                modem_setRTC();
                pModem->state = MS_GET_INFO;
            }
            break;       
       
       case MS_DISCONNECT_WAIT:
            // wait for disconn if needed
            if ((!modem_power_modemIsPowered()) || 
                (powerOffTimeLimit < Timers_GetMsTicks()))
            {
                DEBUG_OUTPUT_TEXT ("modem powered off\n");
                modem_diag_saveModemTaskTimeDuration(MODEM_DIAG_TIME_DISCONNECT);
                // power off the modem which also disables the level shifters
                modem_power_modemPower_off();
                app_gpio_cellComm_disable();
                pModem->state = MS_RESET;
                pModem->cmd = CON_CMD_NULL;
            }
            break;

       case MS_GET_INFO:
            if (pModem->omaDmInProgress)
            {
                pModem->state = MS_WAIT_OMA_DM;
                pModem->stateTimeLimit = Timers_GetMsTicks() + 
                                              MODEM_OMA_DM_TIME_LIMIT_MSEC;

            }

            else if (modem_getModemInfo())
            {
                if (((GET_CAN_DATA_PARAMS *)pModem->pCmdParams != NULL) &&
                     ((GET_CAN_DATA_PARAMS *)pModem->pCmdParams)->leaveModemOn == true)
                {
                    DEBUG_OUTPUT_TEXT ("leave modem on\n");
                    pModem->state = MS_IDLE;
                }
                else
                {
                    if (modem_shutdown())
                    {
                        pModem->state = MS_DISCONNECT_WAIT;
                    }
                    else
                    {
                        // the graceful shutdown of the modem failed. 
                        //  Therefore, just kill power
                        modem_power_modemPower_off();
                        pModem->state = MS_DISCONNECT_WAIT;
                    }
                }
            }
            break;
            
       case MS_WAIT_REGISTERED:
           startupResult = modem_processModemStartup();
           if ( startupResult == 1)
           {
               pModem->state = pModem->nextState;
               pModem->subState = MSS_START_MODEM_UPDATE; 
               pModem->nextState = MS_IDLE;
           }
           else if (startupResult < 0)
           {
               // give up so shutdown
               if (modem_shutdown())
               {
                   pModem->state = MS_DISCONNECT_WAIT;
               }
               else
               {
                   // the graceful shutdown of the modem failed. 
                   //  Therefore, just kill power
                   modem_power_modemPower_off();
                   pModem->state = MS_DISCONNECT_WAIT;
               }
           }
           else
           {
               ; // do nothing
           }
           break;
           
       case MS_MODEM_FOTA:
           startupResult = modem_processModemUpdate();
           // check to see if need more time or if complete (success or fail)
           if (startupResult != 0)
           {
               pModem->state = MS_GET_INFO; // start the modem shutdown
           }
           break;
           
       case MS_WAIT_OMA_DM:
           if (modem_omaDmComplete())
           {
               DEBUG_OUTPUT_TEXT ("OMA DM complete\n\r");
               pModem->state = MS_GET_INFO;
               pModem->omaDmInProgress = false;
           }
           break;
            
       case MS_START_SHUTDOWN:
            if (pModem->omaDmInProgress)
            {
                pModem->state = MS_WAIT_OMA_DM;
                pModem->stateTimeLimit = Timers_GetMsTicks() + 
                                                 MODEM_OMA_DM_TIME_LIMIT_MSEC;

            }
            else
            {
                DEBUG_OUTPUT_TEXT ("START shutdown\n\r");
                if (modem_shutdown())
                {
                    pModem->state = MS_DISCONNECT_WAIT;
                }
                else
                {
                    // the graceful shutdown of the modem failed. 
                    //  Therefore, just kill power
                    modem_power_modemPower_off();
                    pModem->state = MS_DISCONNECT_WAIT;
                }
            }
            break;

       case MS_GET_SIGNAL_QUALITY:
           modem_getModemSignal ();
           pModem->state = MS_IDLE;
           break;
            
       default:
           //ASSERT(0);
           break;
       }
}


/******************************************************************************
*******************************************************************************
**
**            Filename: ModemStub.c
**       Function Name: ModemGetState
**    
**              Author: Duncan Willis
**             Created: 17 March 2015
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: pCtx - The modem driver
**                      query An enummeration to get a query type
**                      pQueryResult [out] Optional structure to be filled in 
**                      with the query results
**    Function Returns: None
**
**         Description: Run the state machine - must not block
******************************************************************************/
static E_CONNECTION_STATE  ModemGetState(void *pCtx, E_QUERY query, 
                                         void* pQueryResult)
{
    MODEM_MANAGER   *pModem = &modemManager;
    E_CONNECTION_STATE      connectionState;
    QUERY_STRING_RESULT     *pStringResult = (QUERY_STRING_RESULT *) pQueryResult;
    QUERY_NUMBER_RESULT     *pNumberResult = (QUERY_NUMBER_RESULT *) pQueryResult;
    UU64        numberResult;

    switch (query)
    {
    case QRY_CONNECTION_STATE:
        switch (pModem->state)
        {
            case MS_CONNECTED:
            case MS_DISCONNECT_WAIT:     
            case MS_DISCONNECT_BEGIN:
            case MS_GET_INFO:
            case MS_WAIT_OMA_DM:        
            case MS_MODEM_FOTA:
                if (pModem->modemFailure == MODEM_ERROR_NONE)
                {
                    connectionState = CON_STATE_SOCKET_OPEN;
                }
                else
                {
                    connectionState = CON_STATE_ERROR;
                }
                break;
            case MS_CONNECTION_OPEN_SOCKET_CLOSED:
            case MS_START_SHUTDOWN:
                connectionState = CON_STATE_CONNECTION_OPEN_SOCKET_CLOSED;
                break;
            case MS_CONNECT_WAIT:
            case MS_WAIT_REGISTERED:
                connectionState = CON_STATE_OPENING_CONNECTION;
                break;
            case MS_IDLE:
            case MS_RESET:
                connectionState = CON_STATE_CONNECTION_CLOSED;
                break;
            default:
                // decide if the caller needs to distinguish CLOSED, 
                //  UNINITIALIZED and ERROR
                connectionState = CON_STATE_CONNECTION_CLOSED;
                break;
        }
        break;
    
    case QRY_GET_REGISTRATION_INFO_STR:      
    case QRY_GET_CURRENT_OPERATOR_STR:
    case QRY_GET_MODEM_HARWARE_REV_STR:
    case QRY_GET_MODEM_MODEL_ID_STR:
    case QRY_GET_MODEM_MANUFACTURER_ID_STR:
    case QRY_GET_MODEM_SOFTWARE_REV_STR:
    case QRY_GET_MODEM_SERIAL_NUMBER_STR:
    case QRY_GET_IMSI_STR:
    case QRY_GET_SIM_ID_STR:
    case QRY_GET_PIN_STATUS_STR:
    case QRY_GET_AVAILABLE_OPERATORS_STR:
    case QRY_GET_NETWORK_PERFORMANCE_STR:
    case QRY_GET_CURRENT_IP_ADDRESS_STR:
    case QRY_GET_CURRENT_APN_STR:
    case QRY_GET_MSISDN_STR:
        modem_diag_queryStringValue (query, pStringResult->buffer, 
                                     pStringResult->buffSize);
        break;
    
    case QRY_GET_TIME_REGISTRATION:
    case QRY_GET_TIME_CONTEXT_ACTIVATION:
    case QRY_GET_TIME_SERVER_CONNECT:
    case QRY_GET_TIME_DATA_TRANSFER:
    case QRY_GET_TIME_DISCONNECT:
    case QRY_GET_RSSI:
    case QRY_GET_CONNECTION_TIME:
        modem_diag_queryNumberResult (query, &numberResult);
        pNumberResult->numberResult = numberResult;
        break;

    default :
        //ASSERT(0);
        break;
    }

    return connectionState;
}


/******************************************************************************
*******************************************************************************
**
**            Filename: ModemStub.c
**       Function Name: ModemSend
**    
**              Author: Duncan Willis
**             Created: 17 March 2015
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: pCtx - The modem driver
**                      txData - The data to send
**                      txCount The number of bytes to send. 
**                       
**    Function Returns: rc - bytes sent or -1 (for error)
**
**         Description: Send data (typically non blocking - add to ringbuffer)  
******************************************************************************/
int32_t ModemSend(void *pCtx, const uint8_t* txData, uint32_t txCount)
{
    MODEM_MANAGER   *pModem = &modemManager;
    int32_t rc = 0;

    /** Copy the data to a ringbuffer so the UART can send it via an ISR */
    if (txCount > 0)
    {
        RingBufferWrite(pModem->pTxRingBuffer, txData, txCount);
        rc = txCount; // return number of bytes queued
    }

    if (RingBufferHasOverflowed(pModem->pTxRingBuffer))
    {
        USART_IntDisable(USART0, UART_IEN_TXBL);
        RingBufferFlush (pModem->pTxRingBuffer);
        rc = -1;
    }
    else
    {
        /* Enable interrupt on USART TX Buffer if there is no overflow. */
        USART_IntEnable(USART0, UART_IEN_TXBL);
    }

    return rc;
}



/******************************************************************************
*******************************************************************************
**
**            Filename: ModemStub.c
**       Function Name: ModemRecv
**    
**              Author: Duncan Willis
**             Created: 17 March 2015
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: pCtx - The modem driver
**                      rxBuffer - The buffer to copy into
**                      bufferSize The number of bytes to send. 
**                       
**    Function Returns: Return -1 for error,
**                      0 for no data (non block)
                        N for N bytes copied into the supplied buffer
**
**         Description: Non-blocking receive function. 
******************************************************************************/
static int32_t ModemRecv(void *pCtx, uint8_t* rxBuffer, uint32_t bufferSize)
{
    MODEM_MANAGER   *pModem = &modemManager;
    uint32_t rxCount = 0;

    if (uartRxMultipleChars)
    {
        DEBUG_OUTPUT_TEXT_AND_DATA ("multi chars: ", &uartRxMultipleChars, 1);
        uartRxMultipleChars = 0;
    }
    if (uartRxBufferOverflow)
    {
        DEBUG_OUTPUT_TEXT_AND_DATA ("OVERFLOW: ", &uartRxBufferOverflow, 1);
        uartRxBufferOverflow = 0;
    }
    
    // Data in ring buf to be read?
    rxCount = RingBufferUsed(pModem->pRxRingBuffer);

    // Limit transfer to size of supplied buffer
    if (rxCount > bufferSize) 
    {
        rxCount = bufferSize;
    }

    // Copy data if there is any.
    if (rxCount > 0)
    {
        RingBufferRead(pModem->pRxRingBuffer, rxBuffer, rxCount);
        
    }
    
    totalRxCount += rxCount;


    if ( rxCount > 0 )
    {
 //       MqttTrace("MODEM Bytes received: %d\n", rxCount);
    }
        

    return (int32_t)rxCount;
}

// Implementation stub function
static int32_t ModemOpenHelper()
{
    MODEM_MANAGER   *pModem = &modemManager;

    // Stub - In this function, start the action to begin connection.
    // The ticked state machine then needs to send AT commands etc, 
    //  and poll until connection made. 
    modem_power_modemPower_on();
    app_gpio_cellComm_enable();
    
    // set back to starting state in case previous failure did not 
    pModem->startUpDelayCnt = 0;
    pModem->connState = DATA_CONN_STATE_DELAY_MODEM_POWER;
    modem_diag_saveModemTaskTimeStart(MODEM_DIAG_TIME_REGISTRATION);
    pModem->modemFailure = MODEM_ERROR_NONE;

    return 0;
}


// Implementation stub function
static int32_t ModemClose()
{
    MODEM_MANAGER   *pModem = &modemManager;
    int32_t rc = OK_STATUS;  
    DEBUG_OUTPUT_TEXT ("modem close\n");
    
    Timers_Loop_Delay(500);  // delay before closing session
    
    modem_diag_saveModemTaskTimeDuration (MODEM_DIAG_TIME_DATA_TRANSFER);
    // drop the data connection
    if (pModem->connState == DATA_CONN_STATE_CONNECTED)
        {
        if (modem_dropDataConnection())
        {
            pModem->connState = DATA_CONN_STATE_DISCONNECTED;
        }
        else
        {
            DEBUG_OUTPUT_TEXT ("let server disconnect\n");
        }
    }
    
    pModem->connState = DATA_CONN_STATE_DISCONNECTED;

    return rc;
}

/***************************************************************************//**
 * @brief
 *   Reads a string from UART 0.
 *
 * @note
 *   This function reads a string from UART 0. The string is terminated if a  
 *    new line char is received. 
 *       
 *
 * @param[out] data       
 *   Pointer to character to contain read data
 * @param[in]  len        
 *   Length of buffer pointed to by data
 * @param[in]  timeoutEndTimeMs       
 *   Provides the ending timeout value in msec
 *
 * @return bool if at least 1 char is returned.
 ******************************************************************************/
 bool modem_getLine(char *data, uint16_t len, uint32_t timeoutEndTimeMs)
{
    uint32_t count = 0;
    char ch=0;

    // new code using circular buffer
    *data = '\0';
    // read what we can and return
    while (count < (len - 1))
    { 
        ch=0;
        if (RingBufferUsed (&rxRingBuffer))
        {
            ch = RingBufferReadOne (&rxRingBuffer);
            if (ch == '\n')
            {

                // go ahead and store LF so it looks correct on print statement
                *data = ch;
                data++;
                *data = '\0';
                count++;

                break;
            }
            else if (ch <= 0x7F)
            {
                *data = ch;
                data++;
                *data = '\0';
                count++;
            }
        }

        // check for a timeout
        if (Timers_GetMsTicks() > timeoutEndTimeMs)
            break;

    }

    return (bool) (count > 0);
}


/***************************************************************************//**
 * @brief
 *   UART0 RX IRQ Handler.
 *
 * @note
 *   This function is the input handler for UART0. 
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void USART0_RX_IRQHandler(void)
{
    MODEM_MANAGER   *pModem = &modemManager;
    
     if (USART0->IF & UART_IF_RXOF)
     {
         uartRxBufferOverflow++;
         USART_IntClear(USART0, USART_IF_RXOF);
     }
        
    if (USART0->IF & UART_IF_RXDATAV)
    {
        /* Copy data into RX Buffer */
        uint8_t rxChar = USART_Rx(USART0);
        RingBufferWrite(&rxRingBuffer, (const uint8_t*)&rxChar, 1);
            if (rxChar == noCarrierString[pModem->noCarrierCmpIdx])
            {
                pModem->noCarrierCmpIdx++;
                if (pModem->noCarrierCmpIdx >= strlen((char *)noCarrierString))
                {
                    DEBUG_OUTPUT_TEXT ("disconnect detected\n\n");    
                    pModem->sawNoCarrier = true;
                    // reset to start comparing again
                    pModem->noCarrierCmpIdx = 0;
                }
            }
            else
            {
                // reset to start comparing again
                pModem->noCarrierCmpIdx = 0;
            }
        
//         if (RingBufferHasOverflowed (&rxRingBuffer))
//         {
//             DEBUG_OUTPUT_TEXT ("Modem RX buffer overflow!!!\n");
//         }
        
        isrRxCount++;
    }
    
    /* Check for RX data valid interrupt */
    while (USART0->IF & UART_IF_RXDATAV)
    {
        uartRxMultipleChars++; 
        /* Copy data into RX Buffer */
        uint8_t rxChar = USART_Rx(USART0);
        RingBufferWrite(&rxRingBuffer, (const uint8_t*)&rxChar, 1);
        
//         if (RingBufferHasOverflowed (&rxRingBuffer))
//         {
//             DEBUG_OUTPUT_TEXT ("Modem RX buffer overflow!!!\n");
//         }
        
        isrRxCount++;
    }
    
    USART_IntClear(USART0, USART_IF_RXDATAV);
}

/***************************************************************************//**
 * @brief
 *   UART0 TX IRQ Handler.
 *
 * @note
 *   This function is the output interrupt handler for UART0. 
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void USART0_TX_IRQHandler(void)
{
    /* Check TX buffer level status */
    if (USART0->IF & UART_IF_TXBL)
    {
        uint8_t txChar;
        if (RingBufferUsed(&txRingBuffer))
        {
            RingBufferRead(&txRingBuffer,  &txChar, 1);
            USART_Tx(USART0, txChar);
        }

        /* Disable Tx interrupt if no more bytes in queue */
        if (RingBufferEmpty(&txRingBuffer))
        {
            USART_IntDisable(USART0, UART_IEN_TXBL);
        }
    }
}

/***************************************************************************//**
 * @brief
 *   Gets of the diagnostic modem info.
 *
 * @note
 *   this routine assumes that the modems is already powered on.
 *       
 *
 * @param[in]  none       
 *
 * @return bool return true when all information has been retrieved or 
 *                          when we give up.
 *******************************************************************************/
static bool modem_getModemInfo(void)
{
    bool    result;
    
    result = true;  // for now we do not want to retry 
    
    if (modem_setModemPortSpeed())
    {
        if (!modem_diag_getBasicModem())
            DEBUG_OUTPUT_TEXT ("Error getting basic modem info\n");
        if (!modem_diag_getSimInfo())
            DEBUG_OUTPUT_TEXT ("Error getting SIM info\n");
        if (!modem_diag_getNetworkInfo())
            DEBUG_OUTPUT_TEXT ("Error getting network info\n");
            
        result = true;
    }

    return result;
}

/***************************************************************************//**
 * @brief
 *   Gets of the signal strength for the modem.
 *
 * @note
 *   this routine assumes that the modems is already powered on.
 *       
 *
 * @param[in]  none       
 *
 * @return bool return true when the signal strength has been retrieved
*               or when we give up.
 *******************************************************************************/
static bool modem_getModemSignal(void)
{
    bool    result;
    
    result = true;  // for now we do not want to retry 
    
    if (modem_setModemPortSpeed())
    {
        if (!modem_diag_getSignalStrength())
        {
            result = false;
        }
    }

    return result;
}
/***************************************************************************//**
 * @brief
 *   Reconnects the data call if necessary.
 *
 * @note
 *   This function checks to see if a data call should be connected (either
 *   MS_CONNECTED or MS_CONNECTION_OPEN_SOCKET_CLOSED). If so it reconnects
 *   using the last connection information.
 *       
 *
 * @param[in]  none       
 *
 * @return bool return true if reconnected.
 *******************************************************************************/
static bool modem_ReconnectIfNeeded()
{
    char commandBuffer[128];
    MODEM_MANAGER   *pModem = &modemManager;
    
    if ((pModem->state == MS_CONNECTED) || 
        (pModem->state == MS_CONNECTION_OPEN_SOCKET_CLOSED))
    {
        DEBUG_OUTPUT_TEXT ("Attempt data reconnect\n");
        Timers_Delay(1000);
        modem_sendAT("AT#SLASTCLOSURE=3", "OK", 5000);
        
        sprintf (commandBuffer, "at#sd=3,0,%d,\"%s\",255,1,0", pModem->connPort, 
                 pModem->connServerAddress);
        DEBUG_OUTPUT_TEXT (commandBuffer);
        DEBUG_OUTPUT_TEXT ("\n");
        if (modem_sendAT(commandBuffer, "CONNECT", 5000))
        {
            DEBUG_OUTPUT_TEXT ("CONNECTED\n");
            pModem->sawNoCarrier = false;
            pModem->state = MS_CONNECTED;
            return true;
        }
    }
    return false;
}

/***************************************************************************//**
 * @brief
 *   This function uses the test AT commands to configure and start the RF 
 *     transmitter as specified..
 *
 * @note
 *       
 *
 * @param[in]  rfBand
 *   desired LTE band (1, 2, 3, 4, 5, 7, 8, 17, or 20)
 * @param[in]  rfChannel
 *   desired channel in the selected band
 * @param[in]  rfPowerLevel
 *   indicates the TX gain for the transmit
 *
 * @return bool return true if transmission started.
 *******************************************************************************/
bool modem_carrierAssert_start(uint8_t rfBand, uint16_t rfChannel, 
                               uint8_t   rfPowerLevel)
{
    bool    goodSoFar = true;
    char    commandBuffer[64];
    
    
    goodSoFar = modem_setModemPortSpeed();
    
    if (goodSoFar)
    {
        goodSoFar = modem_sendAT("AT#TestMode=\"TM\"", "OK", 10000);
    }
    
    if (goodSoFar)
    {
        // select the Band for testing. 
        // This must be one of (1, 2, 3, 4, 5, 7, 8, 17, or 20)
        snprintf(commandBuffer, sizeof(commandBuffer), 
                 "AT#TestMode=\"SETLTEBAND %d\"", rfBand); 
        goodSoFar = modem_sendAT(commandBuffer, "OK", 3000);
    }
    
    if (goodSoFar)
    {
        // select the channel within the selected band. 
        snprintf(commandBuffer, sizeof(commandBuffer), 
                 "AT#TestMode=\"CH %d\"", rfChannel);
        goodSoFar = modem_sendAT(commandBuffer, "OK", 3000);
    }

    if (goodSoFar)
    {
        // start the transmission. 
        goodSoFar = modem_sendAT("AT#TestMode=\"TCH\"", "OK", 3000);
    }

    if (goodSoFar)
    {
        // select the power level which also depends on the band. 
        snprintf(commandBuffer, sizeof(commandBuffer), 
                 "AT#TestMode=\"TXGAIN %d\"", rfPowerLevel);
        goodSoFar = modem_sendAT(commandBuffer, "OK", 3000);
    }

    return goodSoFar;
}


/***************************************************************************//**
 * @brief
 *   This function uses the test AT commands to stop the RF 
 *     transmitter and exit test mode.
 *
 * @note
 *       
 *
 * @param[in]  none
 *
 * @return bool return true if transmission stopped.
 *******************************************************************************/
bool modem_carrierAssert_stop(void)
{
    bool    goodSoFar = true;
    
    goodSoFar = modem_sendAT("AT#TestMode=\"ESC\"", "OK", 1000);
    
    if (goodSoFar)
    {
        // start the transmission. 
        goodSoFar = modem_sendAT("AT#TestMode=\"OM\"", "OK", 10000);
    }

    return goodSoFar;
}

/***************************************************************************//**
 * @brief
 *   This function processes the assert LTE carrier modem command. It then calls 
 *     the routines start and stop the carrier.
 *
 * @note
 *       
 *
 * @param[in]  pAssertLteParams - provides the parameters for the test mode
 *             The parameters are band, channel and power level 
 *
 * @return none
 *******************************************************************************/
void modem_carrierAssert_processCmd(ASSERT_LTE_CARRIER_PARAMS *pAssertLteParams)
{
    if (pAssertLteParams->rfBand)
    {
        modem_carrierAssert_start(pAssertLteParams->rfBand, 
                                  pAssertLteParams->rfChannel, 
                                  pAssertLteParams->rfPowerLevel);
    }
    else
    {
        modem_carrierAssert_stop();
    }
}

/***************************************************************************//**
 * @brief
 *   This function processes the set APN modem command. 
 *
 * @note
 *       
 *
 * @param[in]  uint8_t  string containing the new APN value
 *
 * @return none
 *******************************************************************************/
bool modem_setApn_processCmd(UPDATE_APN_PARAMS   *pUpdateApnParams)
{
    uint8_t  tempBuffer[128];
    MODEM_MANAGER   *pModem = &modemManager;
    bool     result = false;
    
    sprintf ((char *)tempBuffer, "at+cgdcont=%d,\"IPV4V6\",\"%s\",\"\",0,0",
                 pModem->contextId,
                 pUpdateApnParams->apnString);
    if (modem_sendAT((char *)tempBuffer, "OK", 1000))
    {
        result = true;
    }
    
    return result;
}


/*************************************************************************//**
 * @brief
 *   This function determines if the modem is powered on and ready to issue 
 *     AT commands.  
 *
 * @note
 *       
 *
 * @param[in]  none
 *
 * @return true - indicates the modem is powered on ready for commands.  
 *****************************************************************************/
bool modem_isReadyForCommands(void)
{
    bool        modemReady;
    
    modemReady = false;
    
    if (modem_power_modemIsPowered())
    {
        modemReady = modem_setModemPortSpeed();
    }
    
    return modemReady;
}

/***************************************************************************//**
 * @brief
 *   This function returns the context ID that should be used. It is 
 *    different for AT&T versus Verizon.
 *
 * @note
 *       
 *
 * @param[in]  none
 *
 * @return uint8_t - context ID to be used for this carrier.  
 ******************************************************************************/
uint8_t     modem_contextId(void)
{
    MODEM_MANAGER   *pModem = &modemManager;

    return pModem->contextId;
}

/***************************************************************************//**
 * @brief
 *   This function returns the current carrier.  
 *    Currently the only supported carriers are Verizon and AT&T.
 *
 * @note
 *       
 *
 * @param[in]  none
 *
 * @return E_MODEM_CARRIERS - enum value of the current carrier.  
 ******************************************************************************/
E_MODEM_CARRIERS     modemCarrier(void)
{
    MODEM_MANAGER   *pModem = &modemManager;

    return pModem->carrier;
}

/***************************************************************************//**
 * @brief
 *   This function determines if the current OMA DM session is complete by 
 *    looking for the complete message. It also will return true if the 
 *    session has timed out.
 *
 * @note
 *       
 *
 * @param[in]  none
 *
 * @return bool - true indicates that the OMA DM session is complete or 
 *                has taken too long  
 ******************************************************************************/
bool modem_omaDmComplete(void)
{
    bool   result = false;
    MODEM_MANAGER   *pModem = &modemManager;
    uint8_t     dataBuff[80];
    
    // check every 3 second (10 msec tick)
    if (++(pModem->startUpDelayCnt) >= 300)  
    {
        pModem->startUpDelayCnt = 0;
           
        // check for a timeout
        if (Timers_GetMsTicks() > pModem->stateTimeLimit)
        {
            DEBUG_OUTPUT_TEXT ("OMA DM timeout\n\r");
            result = true;
        }
                
        if (modem_waitForMessage((uint8_t *)otaevString, dataBuff, 
                                 sizeof(dataBuff), 100))
        {
            if (strstr((char *)dataBuff, "#DREL"))
            {
                DEBUG_OUTPUT_TEXT ("OMA DM complete\n\r");
                result = true;
            }
        }
                
    }
    
    return result; 
}

/***************************************************************************//**
 * @brief
 *   This function takes an OTAEV message and checks what type it is. It 
 *    then sets a flag indicating if an OMA session is in progress.
 *
 * @note
 *
 * @param[in] msgPtr - points to the OTAEV message 
 *
 * @return none
 ******************************************************************************/
static void modem_checkTypeOtaMsg (uint8_t *msgPtr)
{
    uint8_t     *tempPtr;
    MODEM_MANAGER   *pModem = &modemManager;
            
    tempPtr = msgPtr;
    
    // display the message
    DEBUG_OUTPUT_TEXT ("OMA DM message:\n\r    ");
    DEBUG_OUTPUT_TEXT ((char *)tempPtr);
    tempPtr += strlen ((char *)otaevString);  // skip the #OTAEV
            
    // look for the message indicating that OMA DM has started
    if (strstr ((char *)tempPtr, "#906"))
    {
        DEBUG_OUTPUT_TEXT ("OMA DM session start\n\r");
        pModem->omaDmInProgress = true;
    }
            
    // It is not likely but we should also look to see if the
    //  OMA DM session has completed. 
    if (strstr ((char *)tempPtr, "#DREL"))
    {
        DEBUG_OUTPUT_TEXT ("OMA DM session stop\n\r");
        pModem->omaDmInProgress = false;
    }
}

/***************************************************************************//**
 * @brief
 *   This function looks at any data in the input buffer to see if it contains 
 *    a message indicating that an OMA DM session is starting or ending.
 *
 * @note
 *   If it finds this message, it sets a flag indicating that an OMA DM 
 *    session is in progress. 
 *
 * @param[in]  none
 *
 * @return none
 ******************************************************************************/
void modem_checkForOmaDmStart(void)
{
    uint8_t     readBuffer[80];
    uint32_t    charsToRead;
    uint8_t     *tempPtr;
    bool        charsAvail;   
    
    // check to see if we have data in the buffer
    charsToRead = RingBufferUsed (&rxRingBuffer);
    if (charsToRead < strlen ((char *)otaevString))
    {
        // if there are a few chars then we need to flush them
        if (charsToRead)
        {
            RingBufferFlush (&rxRingBuffer);
        }
        return; 
    }
 
    // get one line of input if available
    charsAvail = modem_getLineNoWait ((char *)readBuffer, sizeof(readBuffer)); 
    while (charsAvail)
    {
        // look for the #OTAEV string
        tempPtr = (uint8_t *) strstr ((char *)readBuffer, (char *)otaevString);
        
        if (tempPtr)
        {
            modem_checkTypeOtaMsg (tempPtr);
        }
               
        // check for more data in the buffer
        charsAvail = modem_getLineNoWait ((char *)readBuffer, 
                                          sizeof(readBuffer)); 
    }

}

/***************************************************************************//**
 * @brief
 *   Reads a string from UART 0 but does not wait.
 *
 * @note
 *   This function reads a string from UART 0. The string is terminated if a  
 *    new line char is received. 
 *       
 *
 * @param[out] data       
 *   Pointer to character to contain read data
 * @param[in]  len        
 *   Length of buffer pointed to by data
 *
 * @return bool if at least 1 char is returned.
 ******************************************************************************/
 bool modem_getLineNoWait(char *data, uint16_t len)
{
    uint32_t count = 0;
    char ch=0;

    // new code using circular buffer
    *data = '\0';
    // read what we can and return
    while ((count < (len - 1)) && (RingBufferUsed (&rxRingBuffer)))
    { 
        ch=0;
        ch = RingBufferReadOne (&rxRingBuffer);
        if (ch == '\n')
        {

            // go ahead and store <LF> so it looks correct on print statement
            *data = ch;
            data++;
            
            // NULL terminate the string
            *data = '\0';
            count++;

            break;
        }
        else if (ch <= 0x7F)
        {
            *data = ch;
            data++;
            count++;
        }

    }

    return (bool) (count > 0);
}

