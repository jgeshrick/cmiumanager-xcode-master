/******************************************************************************
*******************************************************************************
**
**         Filename: modem_uart.c
**    
**           Author: Barry Huggins
**          Created: 12/23/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file modem_uart.c
*
* @brief This file contains some UART utility routines for the CMIU project.
* 
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "stdtypes.h"
#include "em_device.h"
#include "em_usart.h"
#include "em_assert.h"
#include "em_int.h"
#include "efm32lg_uart.h"
#include "modem_uart.h"
#include "app_ble.h"
#include "app_cmit_interface.h"



/***************************************************************************//**
 * @brief
 *   UART0 setup function.
 *
 * @note
 *   This function sets up the interrupts for UART0.
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void modem_uart0_setup(void)
{   
    
    /* Prepare UART Rx and Tx interrupts */
    USART_IntClear(USART0, _UART_IFC_MASK);
    USART_IntEnable(USART0, UART_IEN_RXDATAV);
    NVIC_ClearPendingIRQ(USART0_RX_IRQn); 
    NVIC_ClearPendingIRQ(USART0_TX_IRQn); 
    NVIC_EnableIRQ(USART0_RX_IRQn); 
    NVIC_EnableIRQ(USART0_TX_IRQn); 

    /* Enable UART */
    USART_Enable(USART0, usartEnable);
}


void modem_USART0_setup(void)
{
  USART_InitAsync_TypeDef init = USART_INITASYNC_DEFAULT;

  init.enable       = usartEnable;
  init.refFreq      = 0;
  init.baudrate     = 115200;
  init.oversampling = usartOVS16;
  init.databits     = usartDatabits8;
  init.parity       = usartNoParity;
  init.stopbits     = usartStopbits1;
  init.mvdis        = false;
  init.prsRxEnable  = false;
  init.prsRxCh      = usartPrsRxCh0;


  USART_InitAsync(USART0, &init);
}


/***************************************************************************//**
 * @brief
 *   Changes the baudrate for UART 0.
 *
 * @note
 *       
 *
 * @param[in] newBaudrate - indicates the new desired baud rate
 *
 * @return none
 ******************************************************************************/
void modem_uart0_changeBaudRate(uint32_t newBaudrate)
{
        
    USART_Enable (USART0, usartDisable);

    USART_BaudrateAsyncSet(USART0, 0, newBaudrate, usartOVS16);

    USART_Enable (USART0, usartEnable);  
}

/***************************************************************************//**
 * @brief
 *   Determines if the current baudrate for UART 0 is already 57600.
 *
 * @note
 *   The check looks for a rate of 55000 to 60000 because the calculated
 *   will not be exactly 57600.  We really just need to know if it was set 
 *   to 57600 or 115200
 *
 * @param[in] none
 *
 * @return bool - indicating the current UART 0 baud rate is 57600
 ******************************************************************************/
bool    modem_uart0_isBaudRate57k(void)
{
    uint32_t    currBaud;
    
    currBaud = USART_BaudrateGet(USART0);
    
    if ((currBaud > 55000) && (currBaud < 60000))
    {
        return true;
    }
    else
    {
        return false;
    }
}

