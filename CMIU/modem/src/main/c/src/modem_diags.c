/******************************************************************************
*******************************************************************************
**
**         Filename: modem_diags.c
**    
**           Author: Barry Huggins
**          Created: 3/23/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file modem_diags.c
*
* @brief This file contains the code used to collect diagnostics about the 
*        LTE cellular module (currently a Telit LE-910)
* 
************************************************************************/

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include "stdtypes.h"
#include "em_device.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "modem.h"
#include "modem_uart.h"
#include "app_timers.h"
#include "app_rtc.h"
#include "CmiuAppConfiguration.h"
#include "modem_diags.h"
#include "app_cmit_interface.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/

// These buffers are used to store the results of the different modem 
//   diagnostic commands. The commands with the larger responses use 
//   a larger buffer so that all the buffers do not have to be large.
static MODEM_DIAG_LARGE_BUFF_T     modemDiagLargeCmdResp[MODEM_DIAG_LARGE_BUFF_NUM_CMDS];
static MODEM_DIAG_BUFF_T           modemDiagCmdResp[MODEM_DIAG_BUFF_NUM_CMDS];

static S_MODEM_DIAG_TIME_TABLE_T   modemDiagTaskTimeTable;
static uint16_t                    modemSignalStrength;
static U_NEPTUNE_TIME              modemConnectionTime;
static uint32_t                    modemDiagFailCounTable[MODEM_DIAG_FAIL_NUM_ITEMS];

// This buffer holds the reply to an AT command.
char    replyBuff[200];

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static char  *modem_skipCommandEcho (char *responsePtr);


/***************************************************************************//**
 * @brief
 *   Initialize the modem diagnostic storage tables
 *
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void modem_diag_init(void)
{
    int     i;
    
    // Initialize the buffers used to store the modem diagnostic info
    memset (modemDiagLargeCmdResp, 0, sizeof(modemDiagLargeCmdResp));
    memset (modemDiagCmdResp, 0, sizeof(modemDiagCmdResp));
    
    for (i = 0; i < MODEM_DIAG_LARGE_BUFF_NUM_CMDS; i++)
    {
        strcpy (modemDiagLargeCmdResp[i], "Not available");
    }
    
    for (i = 0; i < MODEM_DIAG_BUFF_NUM_CMDS; i++)
    {
        strcpy (modemDiagCmdResp[i], "Not available");
    }
    
    for (i = 0; i < MODEM_DIAG_FAIL_NUM_ITEMS; i++)
    {
        modemDiagFailCounTable[i] = 0;
    }
}

/***************************************************************************//**
 * @brief
 *   Gets the current signal strength (RSSI).
 *     
 *
 * @note
 *   This function uses the AT+CSQ command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool modem_diag_getSignalStrength(void)
{
    char    *token;
    uint16_t  rssi;
    uint16_t  ber;
    
    if (modem_sendATGetReply("AT+CSQ", "+CSQ:", replyBuff, 
                             sizeof(replyBuff), 3000))
    {
        token = strstr (replyBuff, "+CSQ:");
        if (token)
        {
            token += 6;
            rssi = atoi (token);
            
            // skip to the start of the next number (BER)
            while (*token != ',')
            {
                token++;
            }
            token++; // skip the ','
            ber = atoi (token);
            
            modemSignalStrength = (rssi << 8) | ber;
                        
            return true;
        }
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the available LTE operator info.
 *     
 *
 * @note
 *   This function uses the AT+COPS=? command. This command takes approximately
 *   2 seconds so we do not wait for a response. It provides a list of available
 *   LTE networks.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
static bool modem_diag_getAvailOperator(void)
{
    if (modem_sendATGetReply("AT+COPS=?", "+COPS:", replyBuff, 
                              sizeof(replyBuff), 30000))
    {
        strncpy (modemDiagLargeCmdResp[MODEM_DIAG_LARGE_BUFF_AVAIL_OPERS], 
                 replyBuff, 
                 sizeof (modemDiagLargeCmdResp[MODEM_DIAG_LARGE_BUFF_AVAIL_OPERS]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current APN setting.
 *     
 *
 * @note
 *   This function uses the AT+CGCONT? command. The context ID number
 *    varies between the carriers so it calls a function to get it. 
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool modem_diag_getCurrentApn(void)
{
    char        *foundStr = NULL;
    uint8_t     i;
    uint8_t     expectedResp[64];
    
    // set the context ID based on the carrier
    sprintf ((char *)expectedResp, "+CGDCONT: %1d,", modem_contextId());
    if (modem_sendATGetReply("AT+CGDCONT?", (char *)expectedResp, replyBuff, 
                              sizeof(replyBuff), 5000)) 
    {
         foundStr = strstr(replyBuff, (char *)expectedResp);
         foundStr += strlen((char *)expectedResp); // skip past the first comma
        // skip over the "IPV4V6" string
        while ((*foundStr != ',') && (*foundStr != 0))
        {
            foundStr++; 
        }
        // skip over the comma
        foundStr++;
        
        if (*foundStr == '"')
        {
            foundStr++;
            i = 0;
            while (*foundStr != '"')
            {
                modemDiagCmdResp[MODEM_DIAG_BUFF_APN_VALUE][i] = *foundStr; 
                i++;
                foundStr++;
            }
            // NULL terminate string
            modemDiagCmdResp[MODEM_DIAG_BUFF_APN_VALUE][i] = 0; 
            sprintf ((char *)expectedResp, "APN: >>>%s<<<\n\r", 
                            modemDiagCmdResp[MODEM_DIAG_BUFF_APN_VALUE]);
            DEBUG_OUTPUT_TEXT ((char *)expectedResp);
            return true;
        }
    }
    
    return false;
}


/***************************************************************************//**
 * @brief
 *   Gets the current MSISDN (phone number) setting.
 *     
 *
 * @note
 *   This function uses the AT+CNUM command. 
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
static bool modem_diag_getCurrentMsisdn(void)
{
    char        *foundStr = NULL;
    uint8_t     i;
    
    if (modemCarrier() == MODEM_CARRIER_ATT)
    {
        return true;
    }
    
    if (modem_sendATGetReply("AT+CNUM", "+CNUM:", replyBuff, 
                              sizeof(replyBuff), 2000)) 
    {
        foundStr = strstr(replyBuff, ": ");
        foundStr += 2; // skip past the colon and space
        // skip over the "Line 1" string
        while ((*foundStr != ',') && (*foundStr != 0))
        {
            foundStr++; 
        }
        // skip over the comma
        foundStr++;
        
        if (*foundStr == '"')
        {
            foundStr++;
            i = 0;
            while (*foundStr != '"')
            {
                modemDiagCmdResp[MODEM_DIAG_BUFF_MSISDN][i] = *foundStr; 
                i++;
                foundStr++;
            }
            // NULL terminate the string
            modemDiagCmdResp[MODEM_DIAG_BUFF_MSISDN][i] = 0; 
            
            return true;
        }
    }
    
    return false;
}


/***************************************************************************//**
 * @brief
 *   Gets the current LTE network performance.
 *     
 *
 * @note
 *   This function uses the AT#RFSTS command. 
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
static bool modem_diag_getNetworkPerfInfo(void)
{
    char   *respPtr;
    if (modem_sendATGetReply("AT#RFSTS", "#RFSTS:", replyBuff, 
                             sizeof(replyBuff), 3000))
    {
        respPtr = modem_skipCommandEcho(replyBuff);
        strncpy (modemDiagLargeCmdResp[MODEM_DIAG_LARGE_BUFF_NETWORK_PERF], 
                 respPtr, 
                 sizeof (modemDiagLargeCmdResp[MODEM_DIAG_LARGE_BUFF_NETWORK_PERF]));
        return true;
    }
    
    return false;
}


/***************************************************************************//**
 * @brief
 *   Gets the current modem (currently a Telit LE-910) registration info.
 *     
 *
 * @note
 *   This function uses the AT+CGREG? command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
static bool modem_diag_getRegistrationInfo(void)
{
    char  *respPtr;
    
    if (modem_sendATGetReply("AT+CGREG?", "CGREG:", replyBuff, 
                              sizeof(replyBuff), 3000))
    {
        respPtr = modem_skipCommandEcho(replyBuff);
        strncpy (modemDiagCmdResp[MODEM_DIAG_BUFF_REGISTRATION], respPtr, 
                  sizeof (modemDiagCmdResp[MODEM_DIAG_BUFF_REGISTRATION]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current LTE operator info.
 *     
 *
 * @note
 *   This function uses the AT+COPS? command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
static bool modem_diag_getCurrentOperator(void)
{
    char  *respPtr;
    
    if (modem_sendATGetReply("AT+COPS?", "+COPS:", replyBuff, 
                             sizeof(replyBuff), 3000))
    {
        respPtr = modem_skipCommandEcho(replyBuff);
        strncpy (modemDiagCmdResp[MODEM_DIAG_BUFF_CURRENT_OPER], respPtr, 
                 sizeof (modemDiagCmdResp[MODEM_DIAG_BUFF_CURRENT_OPER]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the modem hardware revision.
 *     
 *
 * @note
 *   This function uses the AT#HWREV command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
static bool modem_diag_getHardwareRev(void)
{
    if (modem_sendATGetReply("AT#HWREV", ".", replyBuff, sizeof(replyBuff), 
                             1000))
    {
        strncpy (modemDiagCmdResp[MODEM_DIAG_BUFF_HARDWARE_REV], replyBuff, 
                 sizeof (modemDiagCmdResp[MODEM_DIAG_BUFF_HARDWARE_REV]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current modem (currently a Telit LE-910) model identification.
 *     
 *
 * @note
 *   This function uses the AT+CGMM command. 
 *   This command is unusual because the response does not include the command.
 *   However, for the current devices we know the modem will be an LE910.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
static bool modem_diag_getModelId(void)
{
    if (modem_sendATGetReply("AT+CGMM", "LE910", replyBuff, sizeof(replyBuff), 
                             3000))
    {
        strncpy (modemDiagCmdResp[MODEM_DIAG_BUFF_MODEL_ID], replyBuff, 
                 sizeof (modemDiagCmdResp[MODEM_DIAG_BUFF_MODEL_ID]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current modem (currently a Telit LE-910) manufacturer ID.
 *     
 *
 * @note
 *   This function uses the AT#CGMI command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
static bool modem_diag_getManufacturerId(void)
{
    char    *respPtr;
    
    if (modem_sendATGetReply("AT#CGMI", "CGMI:", replyBuff, sizeof(replyBuff), 
                             3000))
    {
        respPtr = modem_skipCommandEcho(replyBuff);
        strncpy (modemDiagCmdResp[MODEM_DIAG_BUFF_MANU_ID], respPtr, 
                 sizeof (modemDiagCmdResp[MODEM_DIAG_BUFF_MANU_ID]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current modem (currently a Telit LE-910) software revision.
 *     
 *
 * @note
 *   This function uses the AT+CGMR command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
static bool modem_diag_getSoftwareRev(void)
{
    // This routine gets the firmware version and extracts it from the 
    //  command response.
    if (modem_getFirmwareVersion (modemDiagCmdResp[MODEM_DIAG_BUFF_SOFTWARE_REV], 
                        sizeof (modemDiagCmdResp[MODEM_DIAG_BUFF_SOFTWARE_REV])))
    {
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current modem (currently a Telit LE-910) serial number. This is 
 *   also known as the IMEI
 *     
 *
 * @note
 *   This function uses the AT#CGSN command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
static bool modem_diag_getSerialNum(void)
{
    char    *respPtr;
    
    if (modem_sendATGetReply("AT#CGSN", "CGSN:", replyBuff, sizeof(replyBuff), 
                             3000))
    {
        respPtr = modem_skipCommandEcho(replyBuff);
        strncpy (modemDiagCmdResp[MODEM_DIAG_BUFF_SERIAL_NUM], respPtr, 
                 sizeof (modemDiagCmdResp[MODEM_DIAG_BUFF_SERIAL_NUM]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current SIM IMSI.
 *     
 *
 * @note
 *   This function uses the AT#CIMI command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
static bool modem_diag_getSimImsi(void)
{
    char    *respPtr;
    
    if (modem_sendATGetReply("AT#CIMI", "#CIMI:", replyBuff, sizeof(replyBuff), 
                             3000))
    {
        respPtr = modem_skipCommandEcho(replyBuff);
        strncpy (modemDiagCmdResp[MODEM_DIAG_BUFF_IMSI], respPtr, 
                 sizeof (modemDiagCmdResp[MODEM_DIAG_BUFF_IMSI]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current SIM Identification (CCID).
 *     
 *
 * @note
 *   This function uses the AT#CCID command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
static bool modem_diag_getSimId(void)
{
    char    *respPtr;
    
    if (modem_sendATGetReply("AT#CCID", "#CCID:", replyBuff, sizeof(replyBuff), 
                             3000))
    {
        respPtr = modem_skipCommandEcho(replyBuff);
        strncpy (modemDiagCmdResp[MODEM_DIAG_BUFF_SIM_ID], respPtr, 
                 sizeof (modemDiagCmdResp[MODEM_DIAG_BUFF_SIM_ID]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current modem (currently a Telit LE-910) SIM status.
 *     
 *
 * @note
 *   This function uses the AT+CPIN? command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
static bool modem_diag_getPinStatus(void)
{
    char    *respPtr;
    
    if (modem_sendATGetReply("AT+CPIN?", "+CPIN:", replyBuff, sizeof(replyBuff), 
                             3000))
    {
        respPtr = modem_skipCommandEcho(replyBuff);
        strncpy (modemDiagCmdResp[MODEM_DIAG_BUFF_PIN_STATUS], respPtr, 
                 sizeof (modemDiagCmdResp[MODEM_DIAG_BUFF_PIN_STATUS]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Saves the start time for the specified modem task.
 *     
 *
 * @note
 *   Currently, this routine saves the start time in msec even though it is in
 *   resolution of seconds, since we do not have a msec interrupt running.
 *       
 *
 * @param[in] modemTaskType - indicates which modem task
 *
 * @return none
 ******************************************************************************/
void modem_diag_saveModemTaskTimeStart(E_MODEM_DIAG_TIME_TASKS_T modemTaskType)
{
    char  tempBuff[64];
    
    // save the time when this modem task is started.
    modemDiagTaskTimeTable.modemTaskTime[modemTaskType].startTime = 
                                                          Timers_GetMsTicks();
    
    // bhtemp debug This is to try and determine why some durations seem 
    //        longer than expected
    if ((modemTaskType == MODEM_DIAG_TIME_REGISTRATION) ||
        (modemTaskType == MODEM_DIAG_TIME_DATA_TRANSFER) ||
        (modemTaskType == MODEM_DIAG_TIME_DISCONNECT))
    {
        sprintf (tempBuff, "start time: item %d time: %ld\n\r", modemTaskType,
                 modemDiagTaskTimeTable.modemTaskTime[modemTaskType].startTime);
        DEBUG_OUTPUT_TEXT (tempBuff);
    }
}

/***************************************************************************//**
 * @brief
 *   Saves the connection time.
 *     
 *
 * @note
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void modem_diag_saveConnectionTime()
{
    // save the current time as the connection time
    modemConnectionTime = app_rtc_time_get();
    
}



/***************************************************************************//**
 * @brief
 *   Saves the duration for the specified modem task.
 *     
 *
 * @note
 *   Currently, this routine computes the duration in msec even though it is in
 *   resolution of seconds, since we do not have a msec interrupt running.
 *       
 *
 * @param[in] modemTaskType - indicates which modem task
 *
 * @return none
 ******************************************************************************/
void modem_diag_saveModemTaskTimeDuration(E_MODEM_DIAG_TIME_TASKS_T modemTaskType)
{
    uint32_t   taskEndTime; 
    char  tempBuff[64];
    
    // get the time when this modem task completes
    taskEndTime = Timers_GetMsTicks();
    
    // save the time when this modem task is started.
    modemDiagTaskTimeTable.modemTaskTime[modemTaskType].duration = 
            taskEndTime - 
                   modemDiagTaskTimeTable.modemTaskTime[modemTaskType].startTime;
    
    // bhtemp debug This is to try and determine why some durations seem 
    //        longer than expected
    if ((modemTaskType == MODEM_DIAG_TIME_REGISTRATION) ||
        (modemTaskType == MODEM_DIAG_TIME_DATA_TRANSFER) ||
        (modemTaskType == MODEM_DIAG_TIME_DISCONNECT))
    {
        sprintf (tempBuff, "finish time: item %d time: %ld dur: %ld\n\r", 
                 modemTaskType, 
                 modemDiagTaskTimeTable.modemTaskTime[modemTaskType].startTime,
                        modemDiagTaskTimeTable.modemTaskTime[modemTaskType].duration);
        DEBUG_OUTPUT_TEXT (tempBuff);
    }
    
}

/***************************************************************************//**
 * @brief
 *   Increments the indicated modem failed counter.
 *     
 *
 * @note
 *   This function currently is used for modem failure counts. There is no
 *    reason other non-failures could not be added.
 *       
 *
 * @param[in] none
 *
 * @return bool returns false if any of the commands fails
 ******************************************************************************/
void  modem_diag_incrementCount (E_MODEM_DIAG_FAIL_ITEMS_T  modemFailItem)
{
    if (modemFailItem < MODEM_DIAG_FAIL_NUM_ITEMS)
    {
        modemDiagFailCounTable[modemFailItem]++;
    }
}

/***************************************************************************//**
 * @brief
 *   Gets the current basic modem information by calling the individual routines.
 *     
 *
 * @note
 *   This function calls the other modem_diag_getXXXX routines. These routines
 *    get the information from the modem and store it in a table. 
 *       
 *
 * @param[in] none
 *
 * @return bool returns false if any of the commands fails
 ******************************************************************************/
bool modem_diag_getBasicModem(void)
{
    bool    result = true;
  
    if (!modem_diag_getHardwareRev())
    {
        result = false;
    }
    
    if (!modem_diag_getModelId())
    {
        result = false;
    }
    
    if (!modem_diag_getManufacturerId())
    {
        result = false;
    }
    
    if (!modem_diag_getSoftwareRev())
    {
        result = false;
    }
    
    if (!modem_diag_getSerialNum())
    {
        result = false;
    }
    
    return result;
}

/***************************************************************************//**
 * @brief
 *   Gets the current basic network information by calling the individual routines.
 *     
 *
 * @note
 *   This function calls the other modem_diag_getXXXX routines. These routines
 *    get the information from the modem and store it in a table. 
 *       
 *
 * @param[in] none
 *
 * @return bool returns false if any of the commands fails
 ******************************************************************************/
bool modem_diag_getNetworkInfo(void)
{
    bool    result = true;
    
    if (modem_isRegistered())
    {
        if (!modem_diag_getNetworkPerfInfo())
        {
            result = false;
        }
    }
    else
    {
        strcpy (modemDiagLargeCmdResp[MODEM_DIAG_LARGE_BUFF_NETWORK_PERF], 
                "Not available");
    }
    
    if (!modem_diag_getRegistrationInfo())
    {
        result = false;
    }
    
    if (!modem_diag_getCurrentOperator())
    {
        result = false;
    }
    
    if (!modem_diag_getSignalStrength())
    {
        result = false;
    }
    
    if (!modem_diag_getCurrentApn())
    {
        result = false;
    }

    
    // bhtodo: change this to not wait on the response as it can be slow
    //         We cannot change this until we do not go into EM2. Likely
    //         have to add a new return value indicating in progress.
//     if (!modem_diag_getAvailOperator())
//     {
//         result = false;
//     }
    
    return result;
}

/***************************************************************************//**
 * @brief
 *   Gets the current basic SIM information by calling the individual routines.
 *     
 *
 * @note
 *   This function calls the other modem_diag_getXXXX routines. These routines
 *    get the information from the modem and store it in a table. 
 *       
 *
 * @param[in] none
 *
 * @return bool returns false if any of the commands fails
 ******************************************************************************/
bool modem_diag_getSimInfo(void)
{
    bool    result = true;
    
    if (!modem_diag_getSimImsi())
    {
        result = false;
    }
    
    if (!modem_diag_getSimId())
    {
        result = false;
    }
    
    if (!modem_diag_getPinStatus())
    {
        result = false;
    }
    
    if (!modem_diag_getCurrentMsisdn())
    {
        result = false;
    }
            
    return result;
}

/***************************************************************************//**
 * @brief
 *   Saves the IP address string into the structure of responses.
 *   The routine will get the IP address from the specified string.
 *   
 *   NOTE: The string will be in the format:
 *           #SGACT: 10.128.2.222,
 *       
 *
 * @param[in] responsePtr - points to the IP address to be saved
 *
 * @return bool indicating if the string contained an IP address response.
 ******************************************************************************/
bool modem_diag_saveIpAddress(char *responsePtr)
{
    char  *token;
    int    i = 0;
    
    token = strstr (responsePtr, "#SGACT:");
    
    if (token)
    {
        // skip to the start of the IP address
        token += 8;
        
        // copy over the chars until we reach the end of the IP address
        while ((*token != ',') && (*token != '\r') && (*token != ' '))
        {
            modemDiagCmdResp[MODEM_DIAG_BUFF_IP_ADDRESS][i++] = *token;
            token++;
        }
        
        // NULL terminate the string
        modemDiagCmdResp[MODEM_DIAG_BUFF_IP_ADDRESS][i++] = 0;
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Returns the requested modem query string value in the indicated buffer.
 *     
 *
 * @note
 *   The result is a NULL terminated string.
 *       
 *
 * @param[in] queryType - indicates which modem string value should be returned
 * @param[out] queryBuffer - points to the buffer to receive the result
 * @param[in] queryBufferSize - gives the size of the buffer 
 *
 * @return none
 ******************************************************************************/
void modem_diag_queryStringValue (E_QUERY queryType, 
                                  char  *queryBuffer, 
                                  uint16_t queryBufferSize)
{
    switch (queryType)
    {
        case QRY_GET_REGISTRATION_INFO_STR:
            strncpy (queryBuffer, modemDiagCmdResp[MODEM_DIAG_BUFF_REGISTRATION],
                     queryBufferSize);
            break;
            
        case QRY_GET_CURRENT_OPERATOR_STR:
            strncpy (queryBuffer, modemDiagCmdResp[MODEM_DIAG_BUFF_CURRENT_OPER], 
                     queryBufferSize);
            break;
            
        case QRY_GET_MODEM_HARWARE_REV_STR:
            strncpy (queryBuffer, modemDiagCmdResp[MODEM_DIAG_BUFF_HARDWARE_REV], 
                     queryBufferSize);
            break;
            
        case QRY_GET_MODEM_MODEL_ID_STR:
            strncpy (queryBuffer, modemDiagCmdResp[MODEM_DIAG_BUFF_MODEL_ID], 
                     queryBufferSize);
            break;
            
        case QRY_GET_MODEM_MANUFACTURER_ID_STR:
            strncpy (queryBuffer, modemDiagCmdResp[MODEM_DIAG_BUFF_MANU_ID], 
                     queryBufferSize);
            break;
            
        case QRY_GET_MODEM_SOFTWARE_REV_STR:
            strncpy (queryBuffer, modemDiagCmdResp[MODEM_DIAG_BUFF_SOFTWARE_REV], 
                     queryBufferSize);
            break;
            
        case QRY_GET_MODEM_SERIAL_NUMBER_STR:
            strncpy (queryBuffer, modemDiagCmdResp[MODEM_DIAG_BUFF_SERIAL_NUM], 
                     queryBufferSize);
            break;
            
        case QRY_GET_IMSI_STR:
            strncpy (queryBuffer, modemDiagCmdResp[MODEM_DIAG_BUFF_IMSI], 
                     queryBufferSize);
            break;
            
        case QRY_GET_SIM_ID_STR:
            strncpy (queryBuffer, modemDiagCmdResp[MODEM_DIAG_BUFF_SIM_ID], 
                     queryBufferSize);
            break;
            
        case QRY_GET_PIN_STATUS_STR:
            strncpy (queryBuffer, modemDiagCmdResp[MODEM_DIAG_BUFF_PIN_STATUS], 
                     queryBufferSize);
            break;
            
        case QRY_GET_AVAILABLE_OPERATORS_STR:
            strncpy (queryBuffer, 
                     modemDiagLargeCmdResp[MODEM_DIAG_LARGE_BUFF_AVAIL_OPERS], 
                     queryBufferSize);
            break;
            
        case QRY_GET_NETWORK_PERFORMANCE_STR:
            strncpy (queryBuffer, 
                     modemDiagLargeCmdResp[MODEM_DIAG_LARGE_BUFF_NETWORK_PERF], 
                     queryBufferSize);
            break;
        
        case QRY_GET_CURRENT_IP_ADDRESS_STR:
            strncpy (queryBuffer, 
                     modemDiagCmdResp[MODEM_DIAG_BUFF_IP_ADDRESS], 
                     queryBufferSize);
            break;

        case QRY_GET_CURRENT_APN_STR:
            strncpy (queryBuffer, modemDiagCmdResp[MODEM_DIAG_BUFF_APN_VALUE], 
                     queryBufferSize);
            break;

        case QRY_GET_MSISDN_STR:
            strncpy (queryBuffer, modemDiagCmdResp[MODEM_DIAG_BUFF_MSISDN], 
                     queryBufferSize);
            break;

        default:
            strcpy (queryBuffer, "");
            break;
    }
}


/***************************************************************************//**
 * @brief
 *   Returns the requested modem query numeric value.
 *     
 *
 *       
 *
 * @param[in] queryType - indicates which modem time value should be returned
 *
 * @return uint16_t - the query number result
 ******************************************************************************/
void modem_diag_queryNumberResult (E_QUERY queryType, UU64 *queryBuffer)
{
    switch (queryType)
    {
        case QRY_GET_TIME_REGISTRATION:
            queryBuffer->U16[0] = modemDiagTaskTimeTable.modemTaskTime
                                  [MODEM_DIAG_TIME_REGISTRATION].duration;
            break;
            
        case QRY_GET_TIME_CONTEXT_ACTIVATION:
            queryBuffer->U16[0] = modemDiagTaskTimeTable.modemTaskTime
                                  [MODEM_DIAG_TIME_CONTEXT_ACTIVATION].duration;
            break;
            
        case QRY_GET_TIME_SERVER_CONNECT:
            queryBuffer->U16[0] = modemDiagTaskTimeTable.modemTaskTime
                                  [MODEM_DIAG_TIME_SERVER_CONNECT].duration;
            break;
            
        case QRY_GET_TIME_DATA_TRANSFER:
            queryBuffer->U16[0] = modemDiagTaskTimeTable.modemTaskTime
                                  [MODEM_DIAG_TIME_DATA_TRANSFER].duration;
            break;
            
        case QRY_GET_TIME_DISCONNECT:
            queryBuffer->U16[0] = modemDiagTaskTimeTable.modemTaskTime
                                  [MODEM_DIAG_TIME_DISCONNECT].duration;
            break;
        
        case QRY_GET_RSSI:
            queryBuffer->U16[0] = modemSignalStrength;
            break;
        
        case QRY_GET_CONNECTION_TIME:
            queryBuffer->U64 = modemConnectionTime.S64;
            break;
            
        default:
            break;
    }
    
}

/***************************************************************************//**
 * @brief
 *   This routine returns a pointer to the passed in string but advances
 *    the pointer past the command echo. It does this by simply looking for 
 *    a ':' and skipping past that. It also will skip the following char
 *    if it is a space. 
 *       
 *
 * @param[in] responsePtr - points to the AT command response
 *
 * @return char * - points to the response just past the command echo
 ******************************************************************************/
static char  *modem_skipCommandEcho (char *responsePtr)
{
    char    *resultPtr;
    
    // find the first ':' which is the end of the command echo
    resultPtr = strchr (responsePtr, ':');
    if (resultPtr)
    {
        resultPtr++;
        
        // if there is a space after the ':' skip it also
        if (*resultPtr == ' ')
        {
            resultPtr++;
        }
    }
    else
    {
        // should not happen but if no ':' just return same string
        resultPtr = responsePtr;
    }
    
    return resultPtr;
}
