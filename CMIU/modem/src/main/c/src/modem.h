/******************************************************************************
*******************************************************************************
**
**         Filename: Modem.h
**    
**           Author: Barry Huggins
**          Created: 12/31/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/
#include <time.h>

// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __MODEM_H
#define __MODEM_H

//#include "../../../../../../Common/Utility/src/main/c/RingBuffer.h"
//#include "../../../../../../Common/TcpSessionManager/src/main/c/TransportManager.h"
#include "RingBuffer.h"
#include "TransportManager.h"
#include "typedefs.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
#define MAX_INCOMING_BUFFER_SIZE        512
#define MAX_SERVER_HOSTNAME             96

typedef enum
{
    DATA_CONN_EVENT_START,
    DATA_CONN_EVENT_TIMEOUT,
    DATA_CONN_EVENT_INC_DATA
} E_DATA_CONN_EVENT_T;

typedef enum
{
    DATA_CONN_STATE_DELAY_MODEM_POWER,
    DATA_CONN_STATE_DETERMINE_MODEM_SPEED,
    DATA_CONN_STATE_WAIT_MODEM_REGISTER,
    DATA_CONN_STATE_DEFINE_CONTEXT,
    DATA_CONN_STATE_CONFIG_CONTEXT,
    DATA_CONN_STATE_ACTIVATE_CONTEXT,
    DATA_CONN_STATE_ACTIVATE_CONTEXT_WAIT_RESPONSE,
    DATA_CONN_STATE_CONNECT,
    DATA_CONN_STATE_CONNECT_WAIT_RESPONSE,
    DATA_CONN_STATE_CONNECTED,
    DATA_CONN_STATE_DISCONNECTED,
} E_DATA_CONN_STATE_T;

typedef struct
{
    E_DATA_CONN_STATE_T     connState;
    uint8_t                 incDataBuffer[MAX_INCOMING_BUFFER_SIZE];
    uint16_t                currStateWaitTime;    
} S_DATA_CONN_STATE_INFO;

typedef enum
{
    MODEM_TASKS_NONE,
    MODEM_TASKS_FOTA,
} E_MODEM_TASKS;

/* provides the different carriers supported by the modem library */
typedef enum
{
    MODEM_CARRIER_NONE,
    MODEM_CARRIER_ATT,
    MODEM_CARRIER_VERIZON
} E_MODEM_CARRIERS;

/**
** Internal operating state
**/
typedef enum E_MODEM_STATE
{
    MS_RESET,
    MS_CONNECT_BEGIN,
    MS_CONNECT_WAIT, 
    MS_CONNECTED,
    MS_CONNECTION_OPEN_SOCKET_CLOSED,
    MS_DISCONNECT_BEGIN,
    MS_DISCONNECT_WAIT,
    MS_IDLE,
    MS_GET_INFO,
    MS_WAIT_REGISTERED,
    MS_START_SHUTDOWN,
    MS_MODEM_FOTA,
    MS_WAIT_OMA_DM,
    MS_GET_SIGNAL_QUALITY
}
E_MODEM_STATE;

typedef enum E_MODEM_SUBSTATE
{
    MSS_WAIT_FOR_POWER_UP,
    MSS_DETERMINE_MODEM_SPEED,
    MSS_WAIT_FOR_REGISTRATION,
    MSS_WAIT_FOR_POWER_DOWN, 
    MSS_START_MODEM_UPDATE,
    MSS_DOWNLOAD_FILE,
    MSS_WAIT_FOR_DOWNLOAD_COMPLETE,
    MSS_WAIT_FOR_UPDATE_COMPLETE
}
E_MODEM_SUBSTATE;

/**
** modem failure codes
**/
typedef enum E_MODEM_ERROR
{
    MODEM_ERROR_NONE = 0,
    MODEM_ERROR_REGISTER_FAILED,
    MODEM_ERROR_CONTEXT_ACTIVATION_FAILED, 
    MODEM_ERROR_CONNECT_FAILED, 
    MODEM_ERROR_UART_FAILED, 
    MODEM_ERROR_CONFIG_FAILED, 
}
E_MODEM_ERROR;

/**
** The driver object
**/
typedef struct MODEM_MANAGER
{
    // This contains the function set for the device
    const TRANSPORT_INTERFACE* pInterface;

    // Below are implementational details, add / subtract as necessary
    E_MODEM_STATE           state;
    E_MODEM_SUBSTATE        subState;
    E_MODEM_TASKS           nextTask;    // saves the task to be done when possible
    E_MODEM_STATE           nextState;   // the next state after the current operation
    E_CONNECTION_COMMAND    cmd;
    void*                   pCmdParams; 
    int                     sock;
    E_DATA_CONN_STATE_T     connState;
    uint8_t                 incDataBuffer[MAX_INCOMING_BUFFER_SIZE];
    uint16_t                currStateWaitTime;  
    uint16_t                modemFailure;
    uint16_t                startUpDelayCnt;   // counter to allow a delay as the modem starts
    uint32_t                stateTimeLimit;
    
    uint16_t                noCarrierCmpIdx;
    bool                    sawNoCarrier;
    int                     connPort;
    bool                    omaDmInProgress;
    E_MODEM_CARRIERS        carrier;
    uint8_t                 contextId;
    char                    connServerAddress[MIU_SERVER_ADDRESS_LENGTH_MAX];

    RingBufferT* pRxRingBuffer;   // Buffer for data FROM cloud
    RingBufferT* pTxRingBuffer;   // Buffer for data TO cloud

} MODEM_MANAGER;


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/

// These defines are used to get to the different fields in the #CCLK response.
//  The response line will come back with the '#' as the first char:
//          #CCLK: "15/02/12,16:22:29-20,0"
#define MODEM_CCLK_RESPONSE_START           8
#define MODEM_CCLK_RESPONSE_START_YEAR      (MODEM_CCLK_RESPONSE_START)
#define MODEM_CCLK_RESPONSE_START_MONTH     (MODEM_CCLK_RESPONSE_START_YEAR + 3)
#define MODEM_CCLK_RESPONSE_START_DAY       (MODEM_CCLK_RESPONSE_START_MONTH + 3)
#define MODEM_CCLK_RESPONSE_START_HOUR      (MODEM_CCLK_RESPONSE_START_DAY + 3)
#define MODEM_CCLK_RESPONSE_START_MINUTE    (MODEM_CCLK_RESPONSE_START_HOUR + 3)
#define MODEM_CCLK_RESPONSE_START_SECOND    (MODEM_CCLK_RESPONSE_START_MINUTE + 3)
#define MODEM_CCLK_RESPONSE_START_TIMEZONE  (MODEM_CCLK_RESPONSE_START_SECOND + 2)
#define MODEM_CCLK_RESPONSE_START_ISDST     (MODEM_CCLK_RESPONSE_START_TIMEZONE + 4)

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
const MODEM_MANAGER* ModemGet(void);
void    modem_init(void);
bool    modem_isRegistered(void);
bool    modem_shutdown (void);
bool    modem_setup (void);
bool    modem_sendAT(char* str, char* expectResp, uint32_t timeout_ms);
bool    modem_sendATGetReply(char* str, char* expectResp, char *responseBuff, uint8_t responseBuffLen, uint32_t timeout_ms);
bool    modem_getCurrentTime (struct tm 	*pCurrTime, int8_t   *timezone);
void    modem_testTimeFunction(void);
bool    modem_setRTC(void);
void    modem_process_tick(void);
bool    modem_carrierAssert_start(uint8_t  rfBand, uint16_t   rfChannel, uint8_t   rfPowerLevel);
bool    modem_carrierAssert_stop(void);
bool    modem_isReadyForCommands(void);
uint8_t   modem_contextId(void);
E_MODEM_CARRIERS     modemCarrier(void);
bool    modem_getFirmwareVersion(char *responseBuff, uint8_t responseBuffLen);



/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None

#endif // __MODEM_H

