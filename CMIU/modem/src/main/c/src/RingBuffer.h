/* ****************************************************************************
******************************************************************************
**
**         Filename: RingBuffer.h
**    
**          Author: Duncan Willis
**          Created: 24 Apr 2015
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
 
////////////////////////////////////////////////////////////////////////////////
/// @addtogroup Common
/// Module containing utilities and application common definitions.
/// @{
/// @addtogroup   Ringbuffer
/// Module implementing a ringbuffer for buffered IO.
/// @{
///////////////////////////////////////////////////////////////////////////



#ifndef __RINGBUFFER_H__
#define __RINGBUFFER_H__


#include <stdint.h>
#include "CommonTypes.h"


typedef struct 
{
    uint32_t Size;
    volatile uint32_t WriteIndex;
    volatile uint32_t ReadIndex;
    uint8_t* pucBuf;
    bool     Overflow;    
    volatile uint32_t highWatermark;
}
RingBufferT;



///////////////////////////////////////////////////////////////////////////
/// Exported Function Prototypes
///////////////////////////////////////////////////////////////////////////
 bool RingBufferFull(RingBufferT *pRingBuffer);
 bool RingBufferEmpty(RingBufferT *pRingBuffer);
 void RingBufferFlush(RingBufferT *pRingBuffer);
 uint32_t RingBufferUsed(RingBufferT *pRingBuffer);
 uint32_t RingBufferFree(RingBufferT *pRingBuffer);
 uint32_t RingBufferContigUsed(RingBufferT *pRingBuffer);
 uint32_t RingBufferContigFree(RingBufferT *pRingBuffer);
 uint32_t RingBufferSize(RingBufferT *pRingBuffer);
 uint8_t RingBufferReadOne(RingBufferT *pRingBuffer);

 void RingBufferRead(RingBufferT *pRingBuffer, uint8_t *pucData,
                        uint32_t Length);

 void RingBufferWriteOne(RingBufferT *pRingBuffer, uint8_t ucData);
 void RingBufferWrite(RingBufferT *pRingBuffer, const uint8_t *pucData,
                         uint32_t Length);

 void RingBufferAdvanceWrite(RingBufferT *pRingBuffer,
                                uint32_t NumBytes);
 void RingBufferAdvanceRead(RingBufferT *pRingBuffer,
                                uint32_t NumBytes);
 void RingBufferInit(RingBufferT *pRingBuffer, uint8_t *pucBuf,
                        uint32_t Size);
 bool RingBufferHasOverflowed(const RingBufferT* pRingBuffer);
 uint32_t RingBufferGetHighWatermark(const RingBufferT* pRingBuffer);

#endif // __RINGBUFFER_H__


 ////////////////////////////////////////////////////////////////////////////////
 /// @} // Close the Doxygen group.
 /// @}
 ////////////////////////////////////////////////////////////////////////////////
