:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::        Filename: PrepScript.bat
::          Author: Brian Arnberg
::            Date: 2015.07.17
::
::         Purpose: This script prepares the necessary files for the
::          FileCombiner post-compile build step.
::
::          Notes:  This script should be run by TeamCity prior to running
::              the Keil build. It should be run by a developer if he/she
::              recompiles any of the builds that are move below.
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

@SET FILE_COMBINER=..\..\..\..\..\..\PcTools\FileCombiner\FileCombiner\bin\Release\FileCombiner.exe
@SET BOOTLOADER_HEX=..\..\..\..\..\bootloader\target\obj\CmiuBootloader.hex

copy %FILE_COMBINER% .
copy %BOOTLOADER_HEX% .
