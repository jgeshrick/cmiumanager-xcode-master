set error_t=%2
set revision=%1

if [%error_t%]==[BADCRC_] set datetime=161616
if [%error_t%]==[VERSION_] set datetime=191919
if [%error_t%]==[] set datetime=123456

if [%revision%]==[] set revision=400

set build.number=0.12.%datetime%.%revision%

set f=CmiuApplication
set bin=%f%_%error_t%v%build.number%.bin
set s3f=s3://neptune-image-distribution/%f%
set s3p=%s3f%/%bin%
aws s3 cp CmiuApplication.bin %s3p%


set f=CmiuConfiguration
set bin=%f%_%error_t%v%build.number%.bin
set s3f=s3://neptune-image-distribution/%f%
set s3p=%s3f%/%bin%
aws s3 cp CmiuConfiguration.bin %s3p%
