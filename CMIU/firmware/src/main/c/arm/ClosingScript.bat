:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::        Filename: ClosingScript.bat
::          Author: Brian Arnberg
::            Date: 2015.06.25
::
::         Purpose: This script handles merging the hex file and outputing a
::                  binary file.
::
::     Description: This script sets up the arguments for and calls the File
::                  Combiner program. This decouples the command line from
::                  uVision, which makes build automation easier.
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
@SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
@ECHO OFF
@SET TARGET_NAME=%1
@SET OUTPUT_NAME=%2

:: We expect the target/output name to be non null.
:: We also expect the target/output name to not include filename extensions.
if [%TARGET_NAME%]==[] GOTO ERROR
if [%OUTPUT_NAME%]==[] GOTO ERROR
if [%build.number%]==[] GOTO SKIP_VERSIONED_NAME
@SET OUTPUT_NAME=%OUTPUT_NAME%_%build.number%



:SKIP_VERSIONED_NAME
@SET KEIL_OUTPUT_HEX=..\..\..\..\target\obj\%TARGET_NAME%\%TARGET_NAME%.hex

@SET FILE_COMBINER=FileCombiner.exe
@SET FILE_TYPE=CMIU
@SET BOOTLOADER_HEX=CmiuBootloader.hex
@SET APPLICATION_HEX=%KEIL_OUTPUT_HEX%
@SET KEYTABLE_HEX=".\DevelopmentKeys.hex"
@SET OUTPUT_DIR=..\..\..\..\target\obj\%OUTPUT_NAME%
@SET OUTPUT_HEX="%OUTPUT_DIR%\%OUTPUT_NAME%.hex"
@SET OUTPUT_BIN="%OUTPUT_DIR%\%OUTPUT_NAME%.bin"
@SET ARGUMENTS=%FILE_TYPE% %APPLICATION_HEX% %BOOTLOADER_HEX% %OUTPUT_HEX% %KEYTABLE_HEX%

:MAKE_OUTPUT_DIR
@IF NOT EXIST %OUTPUT_DIR% MKDIR %OUTPUT_DIR%

:RUN_FILE_COMBINER
%FILE_COMBINER% %ARGUMENTS%
@SET RETURN_ERROR=%ERRORLEVEL%
@IF NOT %RETURN_ERROR%==0 GOTO ERROR

@COPY CmiuBleConfigurationIncremented.bin "..\..\..\..\target\obj\CmiuBleConfiguration.bin"
:: @COPY CmiuApplication_PacketBranch.bin "..\..\..\..\target\obj\CmiuApplication.bin"

ENDLOCAL
EXIT /B

:ERROR
ECHO %FILE_COMBINER%(1): error: #1: FileCombiner failed for some reason.
ENDLOCAL
EXIT /B %RETURN_ERROR%

