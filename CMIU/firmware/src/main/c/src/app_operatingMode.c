/******************************************************************************
*******************************************************************************
**
**         Filename: app_operatingMode.c
**    
**           Author: Troy Harstad
**          Created: 4/13/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_operatingMode.c
*
* @brief This file contains the main application code to schedule and
* perform tasks.
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "typedefs.h"
#include "app_rtc.h"
#include "app_operatingMode.h"
#include "app_uart.h"
#include "app_cmiu_app.h"
#include "app_ble.h"
#include "app_cmiu_app.h"
#include "app_timers.h"
#include "app_cmit_interface.h"
#include "em_gpio.h"
#include "em_timer.h"
#include "em_wdog.h"
#include "em_emu.h"
#include "app_message.h"
#include "app_command.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static void app_operatingMode_state_swipe(void);
static bool app_operatingMode_stayInSwipeModeCheck(void);
static void app_operatingMode_state_normal(void);
static void app_operatingMode_state_cellular(void);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// CMIU Operating Mode State
static volatile E_CMIU_OPERATINGMODE_STATE operatingMode_state;



/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/

/**************************************************************************//**
 * @brief
 *   Initializes CMIU Application module
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_operatingMode_init(void)
{
    // Default to swipe operating mode state
    app_operatingMode_swipe_enter();
    
    // Default to false, is set to true every 10ms when in fast tick 
    // operating modes
    timer1TickFlag = false;
}



/**************************************************************************//**
 * @brief
 *   CMIU application state tick function
 *
 * @note
 *   This function ticks the App Module by running the current state,
 *   which is either Swipe or Normal.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
 void app_operatingMode_tick(void)
{
    
    switch(operatingMode_state)
    {
        
        case E_CMIU_OPERATINGMODE_STATE_SWIPE:
        {
            app_operatingMode_state_swipe();
            break;
        }          
        
        case E_CMIU_OPERATINGMODE_STATE_NORMAL:
        {
            app_operatingMode_state_normal();
            break;
        }
        
        case E_CMIU_OPERATINGMODE_STATE_CELLULAR:
        {
            app_operatingMode_state_cellular();
            break;
        }

        default:
        {
            // Should not get here so re-enter normal operating mode
            app_operatingMode_state_normal();
            break;          
        }
    }   

}

/**************************************************************************//**
 * @brief
 *   Swipe mode of operation
 *
 * @note
 *   Swipe state of operation.  Ticks both the CMIT interface and CMIU app.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_operatingMode_state_swipe(void)
{       
    // Stays in this while loop until a condition transitions the CMIU from 
    // swipe to normal operating mode.
    while(app_operatingMode_stayInSwipeModeCheck())
    {       
        // Wait until timer 1 tick flag set
        while (!timer1TickFlag)
        {
            // tick the cmit interface on each time change
            app_cmit_interface_tick_on_time_change();
        }
               
        timer1TickFlag = false;
        
        // Need to feed wdog since we can be in this loop for minutes
        WDOG_Feed();
           
        // Tick application
        app_cmiu_app_tick();
        
        // Tick command processing
        app_command_tick(CMIU_OPERATINGMODE_SWIPE_MSEC_IN_TICK);

    }
    
    // cleanup any items as we exit command mode
    app_command_end();
    
    app_cmiu_app_normal_enterAfterSwipe();
    
    // and enter normal operating mode
    app_operatingMode_normal_enter();
       
}


/**************************************************************************//**
 * @brief
 *   Check if the CMIU should stay in the swipe mode of operation
 *
 * @note
 *   Swipe mode of operation is used immediately after a reset to perform 
 *   BLE operation, commanded tasks, CMIT interface tasks.  If these are 
 *   complete or have timed out the CMIU should transition to normal operating
 *   mode.
 *  
 *   The CMIU should stay in swipe operating mode if BLE has timed out AND no more
 *   command processing is needed OR if the operatingMode_state was changed to
 *   something other than E_CMIU_OPERATINGMODE_STATE_SWIPE.
 * @param[in] None
 *
 * @return true if CMIU should stay in swipe mode, false if not
 *****************************************************************************/
static bool app_operatingMode_stayInSwipeModeCheck(void)
{
    bool stayInSwipe;
    
    // Default to false, some conditions may change this to true
    stayInSwipe = false;
    
    
    // If the command processor is still busy (not in idle state) then stay in swipe mode
    // !!!This isn't checking if the operating mode is still swipe, which means if another task
    // changes it will stll remain in swipe mode until the command processing is complete.
    if(!app_command_inIdleState())
    {       
        stayInSwipe = true;
    }
    
    // If the BLE link is still active (not timed out) AND another task hasn't changed
    // the operating mode from Swipe, stay in swipe mode
    if((operatingMode_state == E_CMIU_OPERATINGMODE_STATE_SWIPE) && 
       (app_ble_linkIsActive() == true))
    {
        stayInSwipe = true;
    }
       

    return stayInSwipe;
    
}


/**************************************************************************//**
 * @brief
 *   Normal mode of operation
 *
 * @note
 *   Normal state of operation.  Ticks both the CMIT interface and CMIU app.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
 static void app_operatingMode_state_normal(void)
{       
    // Tick CMIU Application module once per second        
    app_cmiu_app_tick();

    // tick the cmit interface
    app_cmit_interface_tick(CMIU_OPERATINGMODE_NORMAL_MSEC_IN_TICK);              
}


/**************************************************************************//**
 * @brief
 *   Cellular mode of operation
 *
 * @note
 *   Ticks cellular modules
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_operatingMode_state_cellular(void)
{
    while(operatingMode_state == E_CMIU_OPERATINGMODE_STATE_CELLULAR)
    {       
        // wait until modem tick flag is set
        while (!timer1TickFlag)
        {
        }
            
        timer1TickFlag = false;
           
        // Feed watchdog timer TODO: discuss this with the team as it seems risky
        WDOG_Feed();

        // Tick application
        app_cmiu_app_tick();
        
        // tick the cmit interface
        app_cmit_interface_tick(CMIU_OPERATINGMODE_CELLULAR_MSEC_IN_TICK);

        // Tick command processing
        if (!app_command_wait_till_end())
        {
            app_command_tick(CMIU_OPERATINGMODE_SWIPE_MSEC_IN_TICK);
        }

    }

    // Enable Timer 1 for 100 Hz tick generation for the command state machine
    app_timers_timer1_enable();
    operatingMode_state = E_CMIU_OPERATINGMODE_STATE_CELLULAR;

    /** Let's catch up on any command that still needs running. */
    while(!app_command_inIdleState())
    {
        // wait until modem tick flag is set
        while (!timer1TickFlag)
        {
        }

        timer1TickFlag = false;

        // tick the cmit interface
        app_cmit_interface_tick(CMIU_OPERATINGMODE_CELLULAR_MSEC_IN_TICK);

        // Tick command processing
        app_command_tick(CMIU_OPERATINGMODE_SWIPE_MSEC_IN_TICK);
    }

    // and enter normal operating mode
    app_operatingMode_normal_enter();
}


/**************************************************************************//**
 * @brief
 *   Function to enter swipe mode of operation
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_operatingMode_swipe_enter(void)
{
    // Enable Timer 1 for 100 Hz tick generation in swipe mode
    app_timers_timer1_enable();
    
    // Set to the normal state
    operatingMode_state = E_CMIU_OPERATINGMODE_STATE_SWIPE;
        
    DEBUG_OUTPUT_TEXT("Entering Swipe Operating Mode\r\n");
}



/**************************************************************************//**
 * @brief
 *   Function to enter normal mode of operation
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_operatingMode_normal_enter(void)
{
    // Disable Timer 1 for 100 Hz tick generation, not needed in this mode
    app_timers_timer1_disable();
    
    // Set to the normal state
    operatingMode_state = E_CMIU_OPERATINGMODE_STATE_NORMAL;
        
    DEBUG_OUTPUT_TEXT("Entering Normal Operating Mode\r\n");
}



/**************************************************************************//**
 * @brief
 *   Function to enter cellular mode of operation
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_operatingMode_cellular_enter(void)
{       
    // Enable Timer 1 for 100 Hz tick generation in cellular mode
    app_timers_timer1_enable();
    
    // Set to the cellular state
    operatingMode_state = E_CMIU_OPERATINGMODE_STATE_CELLULAR;

    /** Reset Command State Variables */
    app_command_resetState();
      
    DEBUG_OUTPUT_TEXT("Entering Cellular Operating Mode\r\n");
}


    
/**************************************************************************//**
 * @brief
 *      Function retrieve CMIU app state
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
E_CMIU_OPERATINGMODE_STATE app_operatingMode_state_get(void)
{
    return operatingMode_state;
}
    
    
   
    
