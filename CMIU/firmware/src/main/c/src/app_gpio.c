/******************************************************************************
*******************************************************************************
**
**         Filename: app_gpio.c
**    
**           Author: Troy Harstad
**          Created: 1/19/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_gpio.c
*
* @brief This file contains the application code used to setup/use GPIO's.
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include "app_gpio.h"
#include "app_power.h"
#include "em_gpio.h"
#include "typedefs.h"
#include "..\NordicAPI\hal_platform.h"



/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
uint32_t portModeLRestore[6];
uint32_t portModeHRestore[6];
uint32_t portValueRestore[6];
uint16_t gpioIndex;



/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/


/***************************************************************************//**
 * @brief
 *   Initialize GPIO's
 *
 * @note
 *   This function was created to initialize GPIO's.  Currently some are being
 *   initialized in autogen_init.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_gpio_init(void)
{

    // Setup PA.0 as output, since it will enable 5V power supply
    GPIO_PinModeSet(gpioPortA, GPIO_PIN_0, gpioModePushPull, GPIO_INIT_LOW);  

    // Setup PA.3 and PA.4 as outputs for debug usage
    GPIO_PinModeSet(gpioPortA, GPIO_PIN_3, gpioModePushPull, GPIO_INIT_LOW);
    GPIO_PinModeSet(gpioPortA, GPIO_PIN_4, gpioModePushPull, GPIO_INIT_LOW);

    // Setup PE.13 as output for ARB register data bias  
    GPIO_PinModeSet(ARB_BIAS_PORT, ARB_BIAS_PIN, gpioModePushPull, GPIO_INIT_LOW); 
    
    // Setup PE.14 as output for ARB register data input  
    GPIO_PinModeSet(ARB_DATA_PORT, ARB_DATA_PIN, gpioModeInput, GPIO_INIT_IGNORE); 
   
    // Setup PE.15 as output for ARB register clock  
    GPIO_PinModeSet(ARB_CLOCK_PORT, ARB_CLOCK_PIN, gpioModePushPull, GPIO_INIT_LOW);     
    
  
//*****************************************************************************
// THIS SECTION ADDED IN AN EFFORT TO REDUCE SLEEP CURRENT, REMOVE LATER!!!
    // Clear all outputs in an effort to lower current consumption
    GPIO->P[gpioPortA].DOUTCLR = 0xFF;
    GPIO->P[gpioPortB].DOUTCLR = 0xFF;
    GPIO->P[gpioPortC].DOUTCLR = 0xFF;
    GPIO->P[gpioPortD].DOUTCLR = 0xFF;
    GPIO->P[gpioPortE].DOUTCLR = 0xFF;
    GPIO->P[gpioPortF].DOUTCLR = 0xFF;    
        
    // Set PC.0 (BT_ENn)    
    GPIO_PinOutSet(gpioPortC, 0);   

    // Set PA.9 (VMEM_ENn)
    GPIO_PinOutSet(gpioPortA, 9);   
// END SECTION
//*****************************************************************************

    // Disable modem gpio by default
    app_gpio_cellComm_disable();
   
    
}


/***************************************************************************//**
 * @brief
 *   Enable ARB Mode on GPIO's
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_gpio_arbMode_enable(void)
{ 
    // Enable 5V power supply
    app_power_5v_enable();
    
    // Enable ARB data bias    
    GPIO_PinOutSet(ARB_BIAS_PORT, ARB_BIAS_PIN);   
    
    // Ensure ARB Clock line is low
    GPIO_PinOutClear(ARB_CLOCK_PORT, ARB_CLOCK_PIN);     
       
}

/***************************************************************************//**
 * @brief
 *   Enable ARB Mode on GPIO's
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_gpio_arbMode_disable(void)
{  
    // Disable 5V power supply
    app_power_5v_disable(); 

    // Disable ARB data bias    
    GPIO_PinOutClear(ARB_BIAS_PORT, ARB_BIAS_PIN);  

    // Ensure ARB Clock line is low
    GPIO_PinOutClear(ARB_CLOCK_PORT, ARB_CLOCK_PIN);     
    
}

/***************************************************************************//**
 * @brief
 *   Enable GPIO's used for modem UART communication after they were 
 *    disabled to avoid leaking power.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_gpio_cellComm_enable (void)
{
    /* Pin PC9 is configured to Push-pull for cell UART RTS*/
    GPIO_PinModeSet(gpioPortC, GPIO_PIN_9, gpioModePushPull, GPIO_INIT_LOW);

    /* Pin PC11 is configured to Push-pull for cell UART TX*/
    GPIO_PinModeSet(gpioPortC, GPIO_PIN_11, gpioModePushPull, GPIO_INIT_HIGH);

    /* Pin PE11 is configured to Push-pull for cell UART DTR*/
    GPIO_PinModeSet(gpioPortE, GPIO_PIN_11, gpioModePushPull, GPIO_INIT_HIGH);

    // check PD8 level shifter
}

/***************************************************************************//**
 * @brief
 *   Disable GPIO's used for modem UART communication to avoid leaking power.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_gpio_cellComm_disable (void)
{
    // Disable lines going to modem to reduce EM2 current from 16 uA to 3.4 uA.
      
    // Disable ports PC.9 and PC.11
    GPIO_PinModeSet(gpioPortC, GPIO_PIN_9, gpioModeDisabled, GPIO_INIT_LOW);
    GPIO_PinModeSet(gpioPortC, GPIO_PIN_11, gpioModeDisabled, GPIO_INIT_LOW);
    
    // Disable port PE.11
    GPIO_PinModeSet(gpioPortE, GPIO_PIN_11, gpioModeDisabled, GPIO_INIT_LOW);       
    
}

/***************************************************************************//**
 * @brief
 *   Enable GPIO's used for SPI (USART2) communication after they were 
 *    disabled to avoid leaking power. This SPI interface is used for both the 
 *    BLE device and the external flash. 
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
// this routine is not used as the BLE init sets up the GPIOs as needed
// void app_gpio_spiComm_enable (void)
// {
//  
//     // Enable ports PC.2 (MOSI) and PC.4 (SCLK)
//     pinMode(GPIO_PIN_2, OUTPUT);
//     pinMode(GPIO_PIN_4, OUTPUT);

//     
// }

/***************************************************************************//**
 * @brief
 *   Disable GPIO's used for SPI (USART2) communication after they were 
 *    disabled to avoid leaking power. This SPI interface is used for both the 
 *    BLE device and the external flash.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_gpio_spiComm_disable (void)
{
    // don't power the BLE radio via REQN 
    GPIO_PinOutClear(gpioPortC, 6);         

    // don't power the BLE radio via RESET 
    GPIO_PinOutClear(gpioPortA, 2);   

    // Disable lines going to the SPI to reduce EM2 current.
    // Disable ports PC.2 (MOSI) and PC.4 (SCLK)
    GPIO_PinModeSet(gpioPortC, GPIO_PIN_2, gpioModeDisabled, GPIO_INIT_LOW);
    GPIO_PinModeSet(gpioPortC, GPIO_PIN_4, gpioModeDisabled, GPIO_INIT_LOW);
              
}


/***************************************************************************//**
 * @brief
 *   Initialize the GPIO on port C as specified.
 *
 * @note
 *   This routine is used by the Nordic API
 *
 * @param[in] pin - port C pin number to be setup
 * @param[in] pinMode - mode for the specified pin 
 *
 * @return None
 ******************************************************************************/
void pinMode(uint8_t pin, uint8_t pinMode)
{
  switch(pinMode)
  {
    case INPUT:
      GPIO_PinModeSet(gpioPortC, pin, gpioModeInput, 0);
    break;
  
    case INPUT_PULLUP:
      GPIO_PinModeSet(gpioPortC, pin, gpioModeInputPull, 0);
    break;
  
    case OUTPUT:
      GPIO_PinModeSet(gpioPortC, pin, gpioModePushPull, 0);
    break;
  }
}

/***************************************************************************//**
 * @brief
 *   Helper routine to get the current state of the indicated port C pin.
 *
 * @note
 *   This routine is used by the Nordic API
 *
 * @param[in] pin - port C pin number to be setup
 *
 * @return None
 ******************************************************************************/
uint8_t digitalRead(uint8_t pin)
{
  return GPIO_PinInGet(gpioPortC, pin);
}

/***************************************************************************//**
 * @brief
 *   Helper routine to set the current state of the indicated port C pin.
 *
 * @note
 *   This routine is used by the Nordic API
 *
 * @param[in] pin - port C pin number to be setup
 * @param[in] value - indicates new setting for this GPIO
 *
 * @return None
 ******************************************************************************/
void digitalWrite(uint8_t pin, uint8_t value)
{
  if(value)
  {
    GPIO_PinOutSet(gpioPortC, pin);
  }
  else 
  {
    GPIO_PinOutClear(gpioPortC, pin);
  }
}
