/******************************************************************************
*******************************************************************************
**
**         Filename: app_dummy_readings.c
**    
**           Author: Duncan Willis
**          Created: 9 Nov 2015
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_dummy_readings.c
*
* @brief This file contains the application code used to build various
*    packets
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include <stdbool.h>
#include "typedefs.h"  

const uint32_t adwDummyReadings[] =  {
     0x10270000,   // Reading Value 10000,  Flag Value: No Leak, No Back Flow
     0x1A270000,   // Reading Value 10010,  Flag Value: No Leak, No Back Flow
     0x24270000,   // Reading Value 10020,  Flag Value: No Leak, No Back Flow
     0x24270000,   // Reading Value 10020,  Flag Value: No Leak, No Back Flow
     0x2E270000,   // Reading Value 10030,  Flag Value: No Leak, No Back Flow
     0x38270000,   // Reading Value 10040,  Flag Value: No Leak, No Back Flow
     0x38270000,   // Reading Value 10040,  Flag Value: No Leak, No Back Flow
     0x42270000,   // Reading Value 10050,  Flag Value: No Leak, No Back Flow
     0x4C270000,   // Reading Value 10060,  Flag Value: No Leak, No Back Flow
     0x4C270000,   // Reading Value 10060,  Flag Value: No Leak, No Back Flow
     0x56270000,   // Reading Value 10070,  Flag Value: No Leak, No Back Flow
     0x60270000,   // Reading Value 10080,  Flag Value: No Leak, No Back Flow
     0x60270000,   // Reading Value 10080,  Flag Value: No Leak, No Back Flow
     0x6A270000,   // Reading Value 10090,  Flag Value: No Leak, No Back Flow
     0x74270000,   // Reading Value 10100,  Flag Value: No Leak, No Back Flow
     0x74270000,   // Reading Value 10100,  Flag Value: No Leak, No Back Flow
     0x7E270000,   // Reading Value 10110,  Flag Value: No Leak, No Back Flow
     0x88270000,   // Reading Value 10120,  Flag Value: No Leak, No Back Flow
     0x88270000,   // Reading Value 10120,  Flag Value: No Leak, No Back Flow
     0x92270000,   // Reading Value 10130,  Flag Value: No Leak, No Back Flow
     0x9C270000,   // Reading Value 10140,  Flag Value: No Leak, No Back Flow
     0x9C270000,   // Reading Value 10140,  Flag Value: No Leak, No Back Flow
     0xA6270000,   // Reading Value 10150,  Flag Value: No Leak, No Back Flow
     0xB0270000,   // Reading Value 10160,  Flag Value: No Leak, No Back Flow
     0xB0270000,   // Reading Value 10160,  Flag Value: No Leak, No Back Flow
     0xBA270000,   // Reading Value 10170,  Flag Value: No Leak, No Back Flow
     0xC4270000,   // Reading Value 10180,  Flag Value: No Leak, No Back Flow
     0xC4270000,   // Reading Value 10180,  Flag Value: No Leak, No Back Flow
     0xCE270000,   // Reading Value 10190,  Flag Value: No Leak, No Back Flow
     0xD8270000,   // Reading Value 10200,  Flag Value: No Leak, No Back Flow
     0xD8270000,   // Reading Value 10200,  Flag Value: No Leak, No Back Flow
     0xE2270000,   // Reading Value 10210,  Flag Value: No Leak, No Back Flow
     0xEC270000,   // Reading Value 10220,  Flag Value: No Leak, No Back Flow
     0xEC270000,   // Reading Value 10220,  Flag Value: No Leak, No Back Flow
     0xF6270000,   // Reading Value 10230,  Flag Value: No Leak, No Back Flow
     0x00280000,   // Reading Value 10240,  Flag Value: No Leak, No Back Flow
     0x00280000,   // Reading Value 10240,  Flag Value: No Leak, No Back Flow
     0x0A280000,   // Reading Value 10250,  Flag Value: No Leak, No Back Flow
     0x14280000,   // Reading Value 10260,  Flag Value: No Leak, No Back Flow
     0x14280000,   // Reading Value 10260,  Flag Value: No Leak, No Back Flow
     0x1E280000,   // Reading Value 10270,  Flag Value: No Leak, No Back Flow
     0x28280000,   // Reading Value 10280,  Flag Value: No Leak, No Back Flow
     0x28280000,   // Reading Value 10280,  Flag Value: No Leak, No Back Flow
     0x32280000,   // Reading Value 10290,  Flag Value: No Leak, No Back Flow
     0x3C280000,   // Reading Value 10300,  Flag Value: No Leak, No Back Flow
     0x3C280000,   // Reading Value 10300,  Flag Value: No Leak, No Back Flow
     0x46280000,   // Reading Value 10310,  Flag Value: No Leak, No Back Flow
     0x50280000,   // Reading Value 10320,  Flag Value: No Leak, No Back Flow
     0x50280000,   // Reading Value 10320,  Flag Value: No Leak, No Back Flow
     0x5A280000,   // Reading Value 10330,  Flag Value: No Leak, No Back Flow
     0x64280000,   // Reading Value 10340,  Flag Value: No Leak, No Back Flow
     0x64280000,   // Reading Value 10340,  Flag Value: No Leak, No Back Flow
     0x6E280000,   // Reading Value 10350,  Flag Value: No Leak, No Back Flow
     0x78280000,   // Reading Value 10360,  Flag Value: No Leak, No Back Flow
     0x78280000,   // Reading Value 10360,  Flag Value: No Leak, No Back Flow
     0x82280000,   // Reading Value 10370,  Flag Value: No Leak, No Back Flow
     0x8C280000,   // Reading Value 10380,  Flag Value: No Leak, No Back Flow
     0x8C280000,   // Reading Value 10380,  Flag Value: No Leak, No Back Flow
     0x96280000,   // Reading Value 10390,  Flag Value: No Leak, No Back Flow
     0xA0280000,   // Reading Value 10400,  Flag Value: No Leak, No Back Flow
     0xA0280000,   // Reading Value 10400,  Flag Value: No Leak, No Back Flow
     0xAA280000,   // Reading Value 10410,  Flag Value: No Leak, No Back Flow
     0xB4280000,   // Reading Value 10420,  Flag Value: No Leak, No Back Flow
     0xB4280000,   // Reading Value 10420,  Flag Value: No Leak, No Back Flow
     0xBE280000,   // Reading Value 10430,  Flag Value: No Leak, No Back Flow
     0xC8280000,   // Reading Value 10440,  Flag Value: No Leak, No Back Flow
     0xC8280000,   // Reading Value 10440,  Flag Value: No Leak, No Back Flow
     0xD2280000,   // Reading Value 10450,  Flag Value: No Leak, No Back Flow
     0xDC280000,   // Reading Value 10460,  Flag Value: No Leak, No Back Flow
     0xDC280000,   // Reading Value 10460,  Flag Value: No Leak, No Back Flow
     0xE6280000,   // Reading Value 10470,  Flag Value: No Leak, No Back Flow
     0xF0280000,   // Reading Value 10480,  Flag Value: No Leak, No Back Flow
     0xF0280000,   // Reading Value 10480,  Flag Value: No Leak, No Back Flow
     0xFA280000,   // Reading Value 10490,  Flag Value: No Leak, No Back Flow
     0x04290001,   // Reading Value 10500,  Flag Value: Minor Leak, No Back Flow
     0x04290001,   // Reading Value 10500,  Flag Value: Minor Leak, No Back Flow
     0x0E290001,   // Reading Value 10510,  Flag Value: Minor Leak, No Back Flow
     0x18290001,   // Reading Value 10520,  Flag Value: Minor Leak, No Back Flow
     0x18290001,   // Reading Value 10520,  Flag Value: Minor Leak, No Back Flow
     0x22290001,   // Reading Value 10530,  Flag Value: Minor Leak, No Back Flow
     0x2C290001,   // Reading Value 10540,  Flag Value: Minor Leak, No Back Flow
     0x2C290001,   // Reading Value 10540,  Flag Value: Minor Leak, No Back Flow
     0x36290001,   // Reading Value 10550,  Flag Value: Minor Leak, No Back Flow
     0x40290001,   // Reading Value 10560,  Flag Value: Minor Leak, No Back Flow
     0x40290001,   // Reading Value 10560,  Flag Value: Minor Leak, No Back Flow
     0x4A290001,   // Reading Value 10570,  Flag Value: Minor Leak, No Back Flow
     0x54290001,   // Reading Value 10580,  Flag Value: Minor Leak, No Back Flow
     0x54290001,   // Reading Value 10580,  Flag Value: Minor Leak, No Back Flow
     0x5E290001,   // Reading Value 10590,  Flag Value: Minor Leak, No Back Flow
     0x68290001,   // Reading Value 10600,  Flag Value: Minor Leak, No Back Flow
     0x68290001,   // Reading Value 10600,  Flag Value: Minor Leak, No Back Flow
     0x72290001,   // Reading Value 10610,  Flag Value: Minor Leak, No Back Flow
     0x7C290001,   // Reading Value 10620,  Flag Value: Minor Leak, No Back Flow
     0x7C290001,   // Reading Value 10620,  Flag Value: Minor Leak, No Back Flow
     0x86290001,   // Reading Value 10630,  Flag Value: Minor Leak, No Back Flow
     0x90290001,   // Reading Value 10640,  Flag Value: Minor Leak, No Back Flow
     0xA4290001,   // Reading Value 10660,  Flag Value: Minor Leak, No Back Flow
     0xB8290001,   // Reading Value 10680,  Flag Value: Minor Leak, No Back Flow
     0xCC290001,   // Reading Value 10700,  Flag Value: Minor Leak, No Back Flow
     0xE0290001,   // Reading Value 10720,  Flag Value: Minor Leak, No Back Flow
     0xF4290001,   // Reading Value 10740,  Flag Value: Minor Leak, No Back Flow
     0x082A0001,   // Reading Value 10760,  Flag Value: Minor Leak, No Back Flow
     0x1C2A0001,   // Reading Value 10780,  Flag Value: Minor Leak, No Back Flow
     0x302A0001,   // Reading Value 10800,  Flag Value: Minor Leak, No Back Flow
     0x442A0001,   // Reading Value 10820,  Flag Value: Minor Leak, No Back Flow
     0x582A0001,   // Reading Value 10840,  Flag Value: Minor Leak, No Back Flow
     0x6C2A0001,   // Reading Value 10860,  Flag Value: Minor Leak, No Back Flow
     0x802A0001,   // Reading Value 10880,  Flag Value: Minor Leak, No Back Flow
     0x942A0001,   // Reading Value 10900,  Flag Value: Minor Leak, No Back Flow
     0xA82A0001,   // Reading Value 10920,  Flag Value: Minor Leak, No Back Flow
     0xBC2A0001,   // Reading Value 10940,  Flag Value: Minor Leak, No Back Flow
     0xD02A0001,   // Reading Value 10960,  Flag Value: Minor Leak, No Back Flow
     0xE42A0001,   // Reading Value 10980,  Flag Value: Minor Leak, No Back Flow
     0xF82A0001,   // Reading Value 11000,  Flag Value: Minor Leak, No Back Flow
     0x0C2B0001,   // Reading Value 11020,  Flag Value: Minor Leak, No Back Flow
     0x202B0001,   // Reading Value 11040,  Flag Value: Minor Leak, No Back Flow
     0x342B0001,   // Reading Value 11060,  Flag Value: Minor Leak, No Back Flow
     0x482B0001,   // Reading Value 11080,  Flag Value: Minor Leak, No Back Flow
     0x5C2B0001,   // Reading Value 11100,  Flag Value: Minor Leak, No Back Flow
     0x702B0001,   // Reading Value 11120,  Flag Value: Minor Leak, No Back Flow
     0x842B0001,   // Reading Value 11140,  Flag Value: Minor Leak, No Back Flow
     0x982B0001,   // Reading Value 11160,  Flag Value: Minor Leak, No Back Flow
     0xAC2B0001,   // Reading Value 11180,  Flag Value: Minor Leak, No Back Flow
     0xC02B0001,   // Reading Value 11200,  Flag Value: Minor Leak, No Back Flow
     0xD42B0001,   // Reading Value 11220,  Flag Value: Minor Leak, No Back Flow
     0xE82B0001,   // Reading Value 11240,  Flag Value: Minor Leak, No Back Flow
     0xFC2B0001,   // Reading Value 11260,  Flag Value: Minor Leak, No Back Flow
     0x102C0001,   // Reading Value 11280,  Flag Value: Minor Leak, No Back Flow
     0x242C0001,   // Reading Value 11300,  Flag Value: Minor Leak, No Back Flow
     0x382C0001,   // Reading Value 11320,  Flag Value: Minor Leak, No Back Flow
     0x4C2C0001,   // Reading Value 11340,  Flag Value: Minor Leak, No Back Flow
     0x602C0001,   // Reading Value 11360,  Flag Value: Minor Leak, No Back Flow
     0x742C0001,   // Reading Value 11380,  Flag Value: Minor Leak, No Back Flow
     0x882C0001,   // Reading Value 11400,  Flag Value: Minor Leak, No Back Flow
     0x9C2C0001,   // Reading Value 11420,  Flag Value: Minor Leak, No Back Flow
     0xB02C0001,   // Reading Value 11440,  Flag Value: Minor Leak, No Back Flow
     0xC42C0001,   // Reading Value 11460,  Flag Value: Minor Leak, No Back Flow
     0xD82C0001,   // Reading Value 11480,  Flag Value: Minor Leak, No Back Flow
     0xEC2C0001,   // Reading Value 11500,  Flag Value: Minor Leak, No Back Flow
     0x002D0001,   // Reading Value 11520,  Flag Value: Minor Leak, No Back Flow
     0x142D0001,   // Reading Value 11540,  Flag Value: Minor Leak, No Back Flow
     0x282D0001,   // Reading Value 11560,  Flag Value: Minor Leak, No Back Flow
     0x3C2D0001,   // Reading Value 11580,  Flag Value: Minor Leak, No Back Flow
     0x502D0001,   // Reading Value 11600,  Flag Value: Minor Leak, No Back Flow
     0x642D0001,   // Reading Value 11620,  Flag Value: Minor Leak, No Back Flow
     0x782D0001,   // Reading Value 11640,  Flag Value: Minor Leak, No Back Flow
     0x8C2D0001,   // Reading Value 11660,  Flag Value: Minor Leak, No Back Flow
     0xA02D0001,   // Reading Value 11680,  Flag Value: Minor Leak, No Back Flow
     0xB42D0001,   // Reading Value 11700,  Flag Value: Minor Leak, No Back Flow
     0xC82D0001,   // Reading Value 11720,  Flag Value: Minor Leak, No Back Flow
     0xDC2D0001,   // Reading Value 11740,  Flag Value: Minor Leak, No Back Flow
     0xF02D0001,   // Reading Value 11760,  Flag Value: Minor Leak, No Back Flow
     0x042E0001,   // Reading Value 11780,  Flag Value: Minor Leak, No Back Flow
     0x182E0001,   // Reading Value 11800,  Flag Value: Minor Leak, No Back Flow
     0x2C2E0001,   // Reading Value 11820,  Flag Value: Minor Leak, No Back Flow
     0x402E0001,   // Reading Value 11840,  Flag Value: Minor Leak, No Back Flow
     0x542E0001,   // Reading Value 11860,  Flag Value: Minor Leak, No Back Flow
     0x682E0001,   // Reading Value 11880,  Flag Value: Minor Leak, No Back Flow
     0x7C2E0001,   // Reading Value 11900,  Flag Value: Minor Leak, No Back Flow
     0x902E0001,   // Reading Value 11920,  Flag Value: Minor Leak, No Back Flow
     0xA42E0001,   // Reading Value 11940,  Flag Value: Minor Leak, No Back Flow
     0xB82E0001,   // Reading Value 11960,  Flag Value: Minor Leak, No Back Flow
     0xCC2E0001,   // Reading Value 11980,  Flag Value: Minor Leak, No Back Flow
     0xE02E0001,   // Reading Value 12000,  Flag Value: Minor Leak, No Back Flow
     0xF42E0001,   // Reading Value 12020,  Flag Value: Minor Leak, No Back Flow
     0x082F0001,   // Reading Value 12040,  Flag Value: Minor Leak, No Back Flow
     0x1C2F0001,   // Reading Value 12060,  Flag Value: Minor Leak, No Back Flow
     0x302F0001,   // Reading Value 12080,  Flag Value: Minor Leak, No Back Flow
     0x442F0001,   // Reading Value 12100,  Flag Value: Minor Leak, No Back Flow
     0x582F0001,   // Reading Value 12120,  Flag Value: Minor Leak, No Back Flow
     0x6C2F0001,   // Reading Value 12140,  Flag Value: Minor Leak, No Back Flow
     0x802F0001,   // Reading Value 12160,  Flag Value: Minor Leak, No Back Flow
     0x942F0001,   // Reading Value 12180,  Flag Value: Minor Leak, No Back Flow
     0xA82F0001,   // Reading Value 12200,  Flag Value: Minor Leak, No Back Flow
     0xBC2F0001,   // Reading Value 12220,  Flag Value: Minor Leak, No Back Flow
     0xD02F0001,   // Reading Value 12240,  Flag Value: Minor Leak, No Back Flow
     0xE42F0001,   // Reading Value 12260,  Flag Value: Minor Leak, No Back Flow
     0xF82F0001,   // Reading Value 12280,  Flag Value: Minor Leak, No Back Flow
     0x0C300001,   // Reading Value 12300,  Flag Value: Minor Leak, No Back Flow
     0x20300001,   // Reading Value 12320,  Flag Value: Minor Leak, No Back Flow
     0x34300001,   // Reading Value 12340,  Flag Value: Minor Leak, No Back Flow
     0x48300001,   // Reading Value 12360,  Flag Value: Minor Leak, No Back Flow
     0x5C300001,   // Reading Value 12380,  Flag Value: Minor Leak, No Back Flow
     0x70300001,   // Reading Value 12400,  Flag Value: Minor Leak, No Back Flow
     0x84300001,   // Reading Value 12420,  Flag Value: Minor Leak, No Back Flow
     0x98300001,   // Reading Value 12440,  Flag Value: Minor Leak, No Back Flow
     0xAC300001,   // Reading Value 12460,  Flag Value: Minor Leak, No Back Flow
     0xC0300001,   // Reading Value 12480,  Flag Value: Minor Leak, No Back Flow
     0xD4300001,   // Reading Value 12500,  Flag Value: Minor Leak, No Back Flow
     0xE8300002,   // Reading Value 12520,  Flag Value: Major Leak, No Back Flow
     0xFC300002,   // Reading Value 12540,  Flag Value: Major Leak, No Back Flow
     0x10310002,   // Reading Value 12560,  Flag Value: Major Leak, No Back Flow
     0x2E310002,   // Reading Value 12590,  Flag Value: Major Leak, No Back Flow
     0x4C310002,   // Reading Value 12620,  Flag Value: Major Leak, No Back Flow
     0x6A310002,   // Reading Value 12650,  Flag Value: Major Leak, No Back Flow
     0x88310002,   // Reading Value 12680,  Flag Value: Major Leak, No Back Flow
     0xA6310002,   // Reading Value 12710,  Flag Value: Major Leak, No Back Flow
     0xC4310002,   // Reading Value 12740,  Flag Value: Major Leak, No Back Flow
     0xE2310002,   // Reading Value 12770,  Flag Value: Major Leak, No Back Flow
     0x00320002,   // Reading Value 12800,  Flag Value: Major Leak, No Back Flow
     0x1E320002,   // Reading Value 12830,  Flag Value: Major Leak, No Back Flow
     0x3C320002,   // Reading Value 12860,  Flag Value: Major Leak, No Back Flow
     0x5A320002,   // Reading Value 12890,  Flag Value: Major Leak, No Back Flow
     0x78320002,   // Reading Value 12920,  Flag Value: Major Leak, No Back Flow
     0x96320002,   // Reading Value 12950,  Flag Value: Major Leak, No Back Flow
     0xB4320002,   // Reading Value 12980,  Flag Value: Major Leak, No Back Flow
     0xD2320002,   // Reading Value 13010,  Flag Value: Major Leak, No Back Flow
     0xF0320002,   // Reading Value 13040,  Flag Value: Major Leak, No Back Flow
     0x0E330002,   // Reading Value 13070,  Flag Value: Major Leak, No Back Flow
     0x2C330002,   // Reading Value 13100,  Flag Value: Major Leak, No Back Flow
     0x4A330002,   // Reading Value 13130,  Flag Value: Major Leak, No Back Flow
     0x68330002,   // Reading Value 13160,  Flag Value: Major Leak, No Back Flow
     0x86330002,   // Reading Value 13190,  Flag Value: Major Leak, No Back Flow
     0xA4330002,   // Reading Value 13220,  Flag Value: Major Leak, No Back Flow
     0xC2330002,   // Reading Value 13250,  Flag Value: Major Leak, No Back Flow
     0xE0330002,   // Reading Value 13280,  Flag Value: Major Leak, No Back Flow
     0xFE330002,   // Reading Value 13310,  Flag Value: Major Leak, No Back Flow
     0x1C340002,   // Reading Value 13340,  Flag Value: Major Leak, No Back Flow
     0x3A340002,   // Reading Value 13370,  Flag Value: Major Leak, No Back Flow
     0x58340002,   // Reading Value 13400,  Flag Value: Major Leak, No Back Flow
     0x76340002,   // Reading Value 13430,  Flag Value: Major Leak, No Back Flow
     0x94340002,   // Reading Value 13460,  Flag Value: Major Leak, No Back Flow
     0xB2340002,   // Reading Value 13490,  Flag Value: Major Leak, No Back Flow
     0xD0340002,   // Reading Value 13520,  Flag Value: Major Leak, No Back Flow
     0xEE340002,   // Reading Value 13550,  Flag Value: Major Leak, No Back Flow
     0x0C350002,   // Reading Value 13580,  Flag Value: Major Leak, No Back Flow
     0x2A350002,   // Reading Value 13610,  Flag Value: Major Leak, No Back Flow
     0x48350002,   // Reading Value 13640,  Flag Value: Major Leak, No Back Flow
     0x66350002,   // Reading Value 13670,  Flag Value: Major Leak, No Back Flow
     0x84350002,   // Reading Value 13700,  Flag Value: Major Leak, No Back Flow
     0xA2350002,   // Reading Value 13730,  Flag Value: Major Leak, No Back Flow
     0xC0350002,   // Reading Value 13760,  Flag Value: Major Leak, No Back Flow
     0xDE350002,   // Reading Value 13790,  Flag Value: Major Leak, No Back Flow
     0xFC350002,   // Reading Value 13820,  Flag Value: Major Leak, No Back Flow
     0x1A360002,   // Reading Value 13850,  Flag Value: Major Leak, No Back Flow
     0x38360002,   // Reading Value 13880,  Flag Value: Major Leak, No Back Flow
     0x56360002,   // Reading Value 13910,  Flag Value: Major Leak, No Back Flow
     0x74360002,   // Reading Value 13940,  Flag Value: Major Leak, No Back Flow
     0x92360002,   // Reading Value 13970,  Flag Value: Major Leak, No Back Flow
     0xB0360002,   // Reading Value 14000,  Flag Value: Major Leak, No Back Flow
     0xCE360002,   // Reading Value 14030,  Flag Value: Major Leak, No Back Flow
     0xEC360002,   // Reading Value 14060,  Flag Value: Major Leak, No Back Flow
     0x0A370002,   // Reading Value 14090,  Flag Value: Major Leak, No Back Flow
     0x28370002,   // Reading Value 14120,  Flag Value: Major Leak, No Back Flow
     0x46370002,   // Reading Value 14150,  Flag Value: Major Leak, No Back Flow
     0x64370002,   // Reading Value 14180,  Flag Value: Major Leak, No Back Flow
     0x82370002,   // Reading Value 14210,  Flag Value: Major Leak, No Back Flow
     0xA0370002,   // Reading Value 14240,  Flag Value: Major Leak, No Back Flow
     0xBE370002,   // Reading Value 14270,  Flag Value: Major Leak, No Back Flow
     0xDC370002,   // Reading Value 14300,  Flag Value: Major Leak, No Back Flow
     0xFA370002,   // Reading Value 14330,  Flag Value: Major Leak, No Back Flow
     0x18380002,   // Reading Value 14360,  Flag Value: Major Leak, No Back Flow
     0x36380002,   // Reading Value 14390,  Flag Value: Major Leak, No Back Flow
     0x54380002,   // Reading Value 14420,  Flag Value: Major Leak, No Back Flow
     0x72380002,   // Reading Value 14450,  Flag Value: Major Leak, No Back Flow
     0x90380002,   // Reading Value 14480,  Flag Value: Major Leak, No Back Flow
     0xAE380002,   // Reading Value 14510,  Flag Value: Major Leak, No Back Flow
     0xCC380002,   // Reading Value 14540,  Flag Value: Major Leak, No Back Flow
     0xEA380002,   // Reading Value 14570,  Flag Value: Major Leak, No Back Flow
     0x08390002,   // Reading Value 14600,  Flag Value: Major Leak, No Back Flow
     0x26390002,   // Reading Value 14630,  Flag Value: Major Leak, No Back Flow
     0x44390002,   // Reading Value 14660,  Flag Value: Major Leak, No Back Flow
     0x62390002,   // Reading Value 14690,  Flag Value: Major Leak, No Back Flow
     0x80390002,   // Reading Value 14720,  Flag Value: Major Leak, No Back Flow
     0x9E390002,   // Reading Value 14750,  Flag Value: Major Leak, No Back Flow
     0xBC390002,   // Reading Value 14780,  Flag Value: Major Leak, No Back Flow
     0xDA390002,   // Reading Value 14810,  Flag Value: Major Leak, No Back Flow
     0xF8390002,   // Reading Value 14840,  Flag Value: Major Leak, No Back Flow
     0x163A0002,   // Reading Value 14870,  Flag Value: Major Leak, No Back Flow
     0x343A0002,   // Reading Value 14900,  Flag Value: Major Leak, No Back Flow
     0x523A0002,   // Reading Value 14930,  Flag Value: Major Leak, No Back Flow
     0x703A0002,   // Reading Value 14960,  Flag Value: Major Leak, No Back Flow
     0x8E3A0002,   // Reading Value 14990,  Flag Value: Major Leak, No Back Flow
     0xAC3A0002,   // Reading Value 15020,  Flag Value: Major Leak, No Back Flow
     0xCA3A0002,   // Reading Value 15050,  Flag Value: Major Leak, No Back Flow
     0xE83A0002,   // Reading Value 15080,  Flag Value: Major Leak, No Back Flow
     0x063B0002,   // Reading Value 15110,  Flag Value: Major Leak, No Back Flow
     0x243B0002,   // Reading Value 15140,  Flag Value: Major Leak, No Back Flow
     0x423B0002,   // Reading Value 15170,  Flag Value: Major Leak, No Back Flow
     0x603B0002,   // Reading Value 15200,  Flag Value: Major Leak, No Back Flow
     0x7E3B0002,   // Reading Value 15230,  Flag Value: Major Leak, No Back Flow
     0x8A390009,   // Reading Value 14730,  Flag Value: Minor Leak, Major Backflow
     0xA8390009,   // Reading Value 14760,  Flag Value: Minor Leak, Major Backflow
     0xC6390009,   // Reading Value 14790,  Flag Value: Minor Leak, Major Backflow
     0xE4390009,   // Reading Value 14820,  Flag Value: Minor Leak, Major Backflow
     0x023A0009,   // Reading Value 14850,  Flag Value: Minor Leak, Major Backflow
     0x203A0009,   // Reading Value 14880,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0009,   // Reading Value 14910,  Flag Value: Minor Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0008,   // Reading Value 14910,  Flag Value: No Leak, Major Backflow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
     0x3E3A0000,   // Reading Value 14910,  Flag Value: No Leak, No Back Flow
};
