/******************************************************************************
*******************************************************************************
**
**         Filename: app_cellular.c
**    
**           Author: Troy Harstad
**          Created: 1/21/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_cellular.c
*
* @brief This file contains the application code to manage the CMIU when it
* is in the cellular mode. This mode is used when the cell modem is powered.
* 
************************************************************************/


/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "em_device.h"
#include "typedefs.h"
#include "app_rtc.h"
#include "app_cellular.h"
#include "app_arb.h"
#include "CmiuAppConfiguration.h"
#include "app_metrology.h"
#include "app_uart.h"
#include "em_usart.h"
#include "efm32lg_uart.h"
#include "app_operatingMode.h"
#include "app_packet_parser.h"
#include "app_packet_builder.h"
#include "MqttManager.h"
#include "TcpSessionManager.h"
#include "CmiuAppImageInfo.h"
#include "app_power.h"
#include "app_adc.h"
#include "app_scheduler.h"
#include "app_cmit_interface.h"
#include "filedownloader.h"
#include "app_command.h"
#include "cmiuFlash.h"
#include "em_wdog.h"
#include "app_datalog.h"
#include "app_scratch_ram.h"

// Modem Library Header Files
#include "modem.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/



/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
// Cellular states
static void app_cellular_state_safe(void);
static void app_cellular_state_online_enter(void);
static void app_cellular_state_online(void);

// State transitions
static void app_cellular_online_exit(void);
static void app_cellular_cellular_connected(void);

// Task handler functions
static void app_cellular_onPublishMessageReceivedCallback(const MQTT_MANAGER_PUB* pPubMsg);
static void app_cellular_onReceivedFileByteCallback(struct FILE_DOWNLOADER* pDl, uint8_t b, uint32_t byteIndex);
static void app_cellular_OnFileTansferCompleteCallback(TCP_SESSION_MANAGER* pSm, void* pManager);
static uint32_t app_cellular_cellular_connected_addPubCallback_detailedConfig(MQTT_MANAGER_PUB* pPubMsg);
static uint32_t app_cellular_cellular_connected_addPubCallback_intervalData(MQTT_MANAGER_PUB* pPubMsg);

// Other helper functions
static void app_cellular_ifDue_addIntervalDataPacket(void);
static void app_cellular_cellular_onComplete(TCP_SESSION_MANAGER* pSm, void* pManager);
static void app_cellular_onPuback(MQTT_MANAGER *pMm);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
PFN_STATE_VOID_VOID pfnCell_State;


static bool bConnectedInitComplete;
static bool needMoreTicks;


extern const TRANSPORT_INTERFACE modemInterface;
static TRANSPORT_MANAGER transportManager;


/**
** @brief The object that manages the overall session, state machine example
            -- This is the "demo app" to illustrate the use of the other classes
**/
static TCP_SESSION_MANAGER      sm;


/**
** @brief The object that runs the MQTT protocol (Wrapper around Paho)
**/
static MQTT_MANAGER             mm;

/**
** @brief The MQTT init vars (for future use)
**/
MQTT_MANAGER_INIT_PARAMS        mqttInitParams;// = {0};



CONNECTION_PARAMS               connectionParams;
MQTT_MANAGER_SESSION_PARAMS     mqttSessionParams;
MQTT_MANAGER_SESSION_RESULTS    mqttSessionResults;


MQTT_MANAGER_SUBSCRIPTION subCommand;

MQTT_MANAGER_PUB          pubDetailedConfig;
MQTT_MANAGER_PUB          pubIntervalData;
MQTT_MANAGER_PUB          pubResponse;

static int32_t detailedConfigPacketIndex;
static int32_t intervalPacketIndex;

/** Was the last packet of a specific type successful? */
static bool lastPacketPubacked[E_MQTT_PACKETS_COUNT];



// ADDED FOR IMAGE UDPATE
// Buffer for HTTP line parsing
static uint8_t                          buf[HTTP_WORKING_BUFFER_SIZE];
//HTTP_CLIENT_INIT_PARAMS                hmInitParams = {buf, sizeof(buf)};

static FILE_DOWNLOADER_INIT_PARAMS fdInitParams;

static FILE_DOWNLOADER   fileDownloader;

// Determines if CMIU is operating in MQTT or HTTP session (or idle)
static E_CELL_SESSION  cellSession;

static S_COMMAND_IMAGE_UPDATE *imageUpdate;

static uint8_t flashBuf[256];
static uint8_t flashVerifyBuffer[256];
static uint16_t flashIndex;
static bool resetRequested;     // Flag to force reset after image download

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/


const TRANSPORT_MANAGER* app_cellular_get_transport(void)
{
    return &transportManager;
}


/**************************************************************************//**
 * @brief
 *   Initializes cellular module
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_cellular_init(void)
{        
    // Default to safe state
    app_cellular_safe_enter();
    
    // Default to no ticks needed
    needMoreTicks = false;      
    
    // Enter idle cellular state
    cellSession = E_CELL_SESSION_IDLE;
    
    // No reset requested by default
    resetRequested = false;

    // There is no interval packet to check, yet
    intervalPacketIndex = -1;
    
    // There is no detailed config packet to check, yet
    detailedConfigPacketIndex = -1;
    
    // Initially, all previous packets are considered un-pubacked
    uint32_t i; //The type of i doesn't really matter
    for(i=0;i<E_MQTT_PACKETS_COUNT;++i)
    {
        lastPacketPubacked[i] = false;
    }
    
    // Init vars, this may not be needed
    memset(&subCommand, 0, sizeof(subCommand));
    memset(&pubDetailedConfig, 0, sizeof(pubDetailedConfig));
    memset(&pubIntervalData, 0, sizeof(pubIntervalData));
    memset(&pubResponse, 0, sizeof(pubIntervalData));

    TransportManagerInit(&transportManager, &modemInterface, (void *)ModemGet());
}



bool app_cellular_tick(void)
{
 
    // Tick cellular state machine
    if (NULL != pfnCell_State)
    {
        pfnCell_State();
    }
    
    return needMoreTicks;
}

/**************************************************************************//**
 * @brief
 *   Return whether the most recently sent packet of the supplied type was
 *   PUBACKed.
 *
 * @param[in] packetType - type of packet (DetailedConfig/IntervalData/Etc)
 *
 * @return None
 *****************************************************************************/
bool app_cellular_wasLastPackedPubacked(E_MQTT_PACKETS packetType)
{
    bool retval;
    if (E_MQTT_PACKETS_COUNT > packetType)
    {
        retval = lastPacketPubacked[packetType];
    }
    else
    {
        retval = false;
    }

    return retval;
}


void app_cellular_safe_enter(void)
{
    // Enter safe state
    pfnCell_State = app_cellular_state_safe;    
}


static void app_cellular_state_safe(void)
{   
    // Safe - do nothing state
}







/**************************************************************************//**
 * @brief
 *   Function to enter cellular state of operation
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cellular_state_online_enter(void)
{ 
    COMMAND_UPDATE_MODEM_FIRMWARE_PARAMS *deferredModemUpdateParamsPtr;  
    TRANSPORT_MANAGER* pTransport;
    static UPDATE_MODEM_FIRMWARE_PARAMS        updateModemParams;
    
    // Setup for HTTP session
    if(cellSession == E_CELL_SESSION_HTTP)
    {
        DEBUG_OUTPUT_TEXT("HTTP session starting \r\n");       
    
        // Retrieve image that needs updating, will retrieve first one found
        imageUpdate = app_command_imageUpdate_infoRetrieve();
    
        // Check error condition
        if(imageUpdate == NULL)
        {
            // No image update required at this point
            DEBUG_OUTPUT_TEXT("HTTP session ran but no image update required at this time \r\n");
            
            // Unschedule image update task
            app_scheduler_unscheduleTask(E_SCHEDULER_TASK_TYPE_IMAGEUPDATE);
                            
            // exit cellular state             
            app_cellular_online_exit(); 

            return;            
        }   

        else // Image exists to upgrade
        {    

            // Clear address offset used when programming bytes into external flash
            flashIndex = 0;  

            /** Initialize flash communications. */
            Flash_init();  

            // Erase existing image in external flash in prep for new image downloaded
            Flash_EraseRange(imageUpdate->extStartAddress, (imageUpdate->extStartAddress + imageUpdate->imgSize - 1));            
            /* This has potential to screw things up on a retry. Verify that the above extStartAddress gets updated. */            
            
            // Init a Transport interface and associate a Transport Manager with it.
            TransportManagerInitConnectionParams(&connectionParams,
                                                  uMiuHttpServerConfig.sMIUServerConfig.abyServerAddress,
                                                  uMiuHttpServerConfig.sMIUServerConfig.dwServerPortNumber);

            // Init to run the session, using the given connection and protocol
            TcpSessionManagerInit(&sm, &transportManager, &connectionParams);
            

            

            // Init file downloader params
            fdInitParams.buffer             = buf;
            fdInitParams.bufferSize         = sizeof(buf); 
            fdInitParams.fileName           = imageUpdate->imgFileUrl;      //"/image-distribution/CmiuConfiguration/CmiuConfiguration_v0.3.150715.28.bin";  
            fdInitParams.blockSize          = 1536; // 2048;  // was 256
            fdInitParams.maxRequestRetries  = 3; // was 1
            fdInitParams.requestTimeoutMs   = 6000;   
            fdInitParams.OnReceivedByte     = (pfnOnReceivedByte)&app_cellular_onReceivedFileByteCallback;

            FileDownloaderInit(&fileDownloader, &fdInitParams);

            // Begin tcp session
            TcpSessionManagerBegin(&sm,
                (void*)&fileDownloader,
                (pfnManagerBeginFunction)&FileDownloaderBegin,
                (pfnManagerRunFunction)&FileDownloaderRun,
                (pfnManagerCompletionFunction)&app_cellular_OnFileTansferCompleteCallback);
          
        }   
    }
    
    
    // Setup for MQTT session
    else if (cellSession == E_CELL_SESSION_MQTT)
    {
        DEBUG_OUTPUT_TEXT("MQTT session starting \r\n");
        
        // Call these functions once to kick off the modem open
        TransportManagerInitConnectionParams(&connectionParams,
                                              uMiuMqttServerConfig.sMIUServerConfig.abyServerAddress,
                                              uMiuMqttServerConfig.sMIUServerConfig.dwServerPortNumber);

        
        if(TransportManagerCommand(&transportManager, CON_CMD_OPEN_CONNECTION, &connectionParams) == ECODE_BAD)
        {
            // Init transport manager since trying to command it failed the first time
            TransportManagerInit(&transportManager, &modemInterface, (void *)ModemGet());
            
            DEBUG_OUTPUT_TEXT("Reinitializing transport manager since open connection command not accepted\r\n");
            
            if(TransportManagerCommand(&transportManager, CON_CMD_OPEN_CONNECTION, &connectionParams) == ECODE_BAD)
            {
                // Bail out since transport manager still not accepting a command to open the connection
                DEBUG_OUTPUT_TEXT("Transport manager still not accepting open connection command\r\n");            
                
                // exit cellular state             
                app_cellular_online_exit(); 

                return; 

            }    
        }
        
        // Init the MQTT protocol manager
        mqttInitParams.pfOnPublishRxCallback = app_cellular_onPublishMessageReceivedCallback;

        MqttManagerInitSessionParams(&mqttSessionParams);
        MqttManagerInitSessionResults(&mqttSessionResults);
        MqttManagerInit(&mm, &mqttInitParams, &mqttSessionParams, &mqttSessionResults); 

        // Init to run the session, using the given connection and mqtt protocol
        TcpSessionManagerInit(&sm, &transportManager, &connectionParams);             

    }
    
    // Setup for FOTA session   
    else if (cellSession == E_CELL_SESSION_FOTA)
    {
        // get access to the modem update parameters
        deferredModemUpdateParamsPtr = getDeferredModemUpdateParams();

        // Unschedule FOTA update task
        app_scheduler_unscheduleTask(E_SCHEDULER_TASK_TYPE_MODEMFOTA);

        DEBUG_OUTPUT_TEXT("FOTA session starting \r\n");
        pTransport = (TRANSPORT_MANAGER*)app_cellular_get_transport();

        updateModemParams.ftpServer = deferredModemUpdateParamsPtr->ftpServer;
        updateModemParams.ftpPort = deferredModemUpdateParamsPtr->ftpPort;
        updateModemParams.username = deferredModemUpdateParamsPtr->username;
        updateModemParams.password = deferredModemUpdateParamsPtr->password;
        updateModemParams.fotaFilename = deferredModemUpdateParamsPtr->fotaFilename;
        updateModemParams.newFirmwareVersion = deferredModemUpdateParamsPtr->newFirmwareVersion;
        
        // Send the command to the modem library
        TransportManagerCommand(pTransport, CON_CMD_UPDATE_MODEM_FIRMWARE, &updateModemParams);
        TransportManagerTick(pTransport, 10);
    }
    else 
    {
        DEBUG_OUTPUT_TEXT("Invalid cellular operating mode \r\n");
    }
    
    // Clear connected init flag, gets set once CMIU is connected
    bConnectedInitComplete = false;
    
    // Set next state
    pfnCell_State = app_cellular_state_online;   
}






/**************************************************************************//**
 * @brief
 *   Cellular state of operation
 *
 * @note
 *   Cellular state of operation.  This state ticks the TCP session manager
 *   to facilitate TCP communications using MQTT and the modem.
 *
 *   When the transport manager indicates the connetion is closed the cellular
 *   state is exited.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cellular_state_online(void)
{
    E_CONNECTION_STATE tmState;
    // counter used to limit the error messages. It can wrap if necessary.
    static  uint16_t    errorCnt = 1;
      
    // Tick the process
    TcpSessionManagerTick(&sm, 10);
        
    // Get transport manager state
    tmState = TransportManagerGetState(&transportManager, QRY_CONNECTION_STATE, NULL);

    // Once connected
    if ((tmState == CON_STATE_CONNECTION_OPEN_SOCKET_CLOSED ) && bConnectedInitComplete == false)
    {
        // connection is now open            
        app_cellular_cellular_connected();
        DEBUG_OUTPUT_TEXT("Celluar connected \r\n");        
    }
    
    
    // Error state
    if (tmState == CON_STATE_ERROR)
    {
        // exit cellular state   - CANT EXIT CELLULAR STATE YET BECAUSE MODEM NEEDS MORE TICKS TO SHUT DOWN!!!      
       // app_cellular_cellular_exit(); 
        bConnectedInitComplete = true;  // May have never reached the CON_STATE_OPEN state so 
                                        // this would still be false, which would prevent 
                                        // exiting the celluar state.
        
        // only display every 16th error since they flood.
        if ((errorCnt++ & 0x0F) == 1)
        {
            DEBUG_OUTPUT_TEXT("Cellular error \r\n");
        }
    }
       
    // Transport manager now closed
    // FOTA sessions never go through the connected state
    if ((tmState == CON_STATE_CONNECTION_CLOSED) && 
         ((cellSession == E_CELL_SESSION_FOTA) || (bConnectedInitComplete == true)))
    {
        // exit cellular state             
        app_cellular_online_exit();        
    } 
}



/**************************************************************************//**
 * @brief
 *   Function called when the transport manager indicates it has reached the
 *   connected state.  
 * @note
 *   This function sets up the pub, adds the pub to the MQTT manager, and 
 *   begins the TCP session.
 *
 *   Nothing is done here for HTTP and FOTA cell sessions.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cellular_cellular_connected(void)
{
    uint32_t bufferSize = 0;
    uint8_t* buffer = app_scratch_ram_get(&bufferSize);
    pfnManagerCompletionFunction    pfnOnComplete;
    
    // Indicate this function has been reached
    bConnectedInitComplete = true;
    
    // Make sure pfnOnComple points to something
    pfnOnComplete = (pfnManagerCompletionFunction)app_cellular_cellular_onComplete;
    
    // Make sure pfnOnPuback points to a valid function (app_cellular_onPuback).
    mm.pfnOnPuback = (pfnOnPubackCallback)app_cellular_onPuback;

    if(cellSession == E_CELL_SESSION_MQTT)
    {

        // Need to make sure we are subscribed to command packet
        subCommand.topicFilter = (char *)pstrMiuSubTopic;
        subCommand.qos = E_MQTT_QOS_1;

        MqttManagerAddSubscription(&mqttSessionParams, &subCommand);

        // Expect to get one (or more commands back)
        mqttSessionParams.expectedPubsToReceive = 1;
        //     
        //     // Clear clean session flag
        //     sessionParams.cleansession = false;

        mqttSessionParams.clientID = (char *)pstrClientId;
        
        // Check if detailed config packet due and build
        if(app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET))
        {   
            // Schedule next detailed config packet
            app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET, E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION, NULL, NULL);                             
            
            // Add a Pub, dataLength and pData provided in callback
            pubDetailedConfig.topic = (char *)pstrMiuPubTopic;
            pubDetailedConfig.qos = E_MQTT_QOS_1;
            pubDetailedConfig.onPopulate = app_cellular_cellular_connected_addPubCallback_detailedConfig;

            // Add the pub to the MQTT session
            detailedConfigPacketIndex = MqttManagerAddPublish(&mqttSessionParams, &pubDetailedConfig);

            // Clear the PUBACK state for this packet.
            lastPacketPubacked[E_MQTT_PACKETS_DETAILED_CONFIG] = false;
        }
        else
        {
            // Tell MQTT to publish an interval data packet (if it's due).
            app_cellular_ifDue_addIntervalDataPacket();
        }


        // Begin TCP session
        TcpSessionManagerBegin(&sm,
                               &mm,
                               (pfnManagerBeginFunction)&MqttManagerBegin,
                               (pfnManagerRunFunction)&MqttManagerRun,
                               pfnOnComplete);
        
    }
    
    
    // Check if this is an http session
    else if(cellSession == E_CELL_SESSION_HTTP)
    {
        // Do nothing
        return;
    }

    // Check if this is a modem FOTA session
    else if(cellSession == E_CELL_SESSION_FOTA)
    {
        // Do nothing
        return;
    }
                   
}

/****************************************************************************
 * @brief
 *  Try to add an interval data packet to be published by MQTT.
 *
 * @param[in] none
 *
 * @return none
 *****************************************************************************/
static void app_cellular_ifDue_addIntervalDataPacket(void)
{
    // Check if interval data packet due
    if(app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE_INTERVALDATA_PACKET))
    {
        // Schedule next interval data packet
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_INTERVALDATA_PACKET,
                                   E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION,
                                   NULL,
                                   NULL);

        // Add a Pub, dataLength and pData provided in callback
        pubIntervalData.topic = (char *)pstrMiuPubTopic;
        pubIntervalData.qos = E_MQTT_QOS_1;
        pubIntervalData.onPopulate = app_cellular_cellular_connected_addPubCallback_intervalData;

        // Add the pub to the MQTT session 
        intervalPacketIndex = MqttManagerAddPublish(&mqttSessionParams, &pubIntervalData);

        // Clear the PUBACK state for this packet.
        lastPacketPubacked[E_MQTT_PACKETS_INTERVAL_DATA] = false;
    }

}

/****************************************************************************
 * @brief
 *  Callback (called in MQTT session at point of Publish) to 
 *  populate a Pub with Detailed Config
 *
 * @param[in] pPubMsg The Pub to give the data to
 *
 * @return The number  of bytes written to the pub buffer, 0 if failed
 *****************************************************************************/
static uint32_t app_cellular_cellular_connected_addPubCallback_detailedConfig(MQTT_MANAGER_PUB* pPubMsg)
{    
    uint32_t bufferSize = 0;
    uint8_t* buffer;
    uint32_t packetLength;
    
    buffer = app_scratch_ram_get(&bufferSize);
    packetLength = app_packet_builder_build_detailedConfiguration(buffer, bufferSize);  
    pPubMsg->pData = buffer;
    
    return packetLength;
}


/****************************************************************************
 * @brief
 *  Callback (called in MQTT session at point of Publish) to 
 *  populate a Pub with interval data packet
 *
 * @param[in] pPubMsg The Pub to give the data to
 *
 * @return The number  of bytes written to the pub buffer, 0 if failed
 *****************************************************************************/
static uint32_t app_cellular_cellular_connected_addPubCallback_intervalData(MQTT_MANAGER_PUB* pPubMsg)
{
    uint32_t bufferSize = 0;
    uint8_t* buffer;
    uint32_t packetLength;
    
    buffer = app_scratch_ram_get(&bufferSize);
    packetLength = app_packet_builder_build_intervalData(buffer, bufferSize);  
    pPubMsg->pData = buffer;
    
    return packetLength;
}


/**************************************************************************//**
 * @brief
 *  Cleanup variables after the MQTT session is complete.
 *
 * @param[in] *pSm - pointer to the session variables
 * @param[in] *pManager - pointer to the manager function
 *
 * @return None
 *****************************************************************************/
static void app_cellular_cellular_onComplete(TCP_SESSION_MANAGER* pSm, void* pManager)
{
    bool packetResult;

    if (-1 != detailedConfigPacketIndex)
    {
        // Dereference the index
        detailedConfigPacketIndex = -1;
    }

    if (-1 != intervalPacketIndex)
    {
        packetResult = MqttManagerWasPublishDelivered(&mqttSessionResults,
                                                       intervalPacketIndex);

        // Cleanup the datalog variables, based on the MQTT result
        if(app_datalog_cleanBackLog(packetResult))
        {
            DEBUG_OUTPUT_TEXT(" ------------> FINAL DATALOG CLEANUP \r\n");
        }

        // Dereference the index
        intervalPacketIndex = -1;
    }
}

/**************************************************************************//**
 * @brief
 *  Run some checks after a puback is received
 *
 * @param[in] *pMm - pointer to the current MQTT_MANAGER object
 *
 * @return None
 *****************************************************************************/
static void app_cellular_onPuback(MQTT_MANAGER *pMm)
{
    bool packetResult;

    if (-1 != detailedConfigPacketIndex)
    {
        //Let's make sure that a PUBACK was received for the interval data
        //packet.
        packetResult = MqttManagerWasPublishDelivered(&mqttSessionResults,
                                                   detailedConfigPacketIndex);

        /** Store whether we got a puback here. */
        lastPacketPubacked[E_MQTT_PACKETS_DETAILED_CONFIG] = packetResult;

        if (packetResult)
        {
            // Dereference the index
            detailedConfigPacketIndex = -1;

            DEBUG_OUTPUT_TEXT(" ------------> DETAILED CONFIG PUBACK PROCESSED \r\n");

            // Tell MQTT to publish an interval data packet (if it's due).
            app_cellular_ifDue_addIntervalDataPacket();
        }
    }

    if (-1 != intervalPacketIndex)
    {
        //Let's make sure that a PUBACK was received for the interval data
        //packet.
        packetResult = MqttManagerWasPublishDelivered(&mqttSessionResults,
                                                       intervalPacketIndex);

        /** Store whether we got a puback here. */
        lastPacketPubacked[E_MQTT_PACKETS_INTERVAL_DATA] = packetResult;

        // If interval data packet has gotten a puback, let's
        // do cleanup.
        if (packetResult)
        {
            // Dereference the index
            intervalPacketIndex = -1;

            // Cleanup the datalog variables, based on the MQTT result
            if(app_datalog_cleanBackLog(packetResult))
            {
                if ((packetResult) && (0u != app_datalog_getMissedLogs()))
                {
                    //Decrement the currentSendPub and the pubToSendCount to get
                    //around the MQTT_MAX_PUBS_TO_SEND limit. This effectively
                    //causes the interval data packet to get re-published
                    //multiple times.
                    pMm->currentSendPub--;
                    mqttSessionParams.pubToSendCount--;

                    // No, readd the interval data packet to the MQTT manager
                    // and tell the MQTT manager to publish (again)
                    intervalPacketIndex = MqttManagerAddPublish(&mqttSessionParams,
                                                                &pubIntervalData);
                    pMm->state = E_MQTT_DO_PUBLISH;
                }

                DEBUG_OUTPUT_TEXT(" ------------> INTERVAL DATA PUBACK PROCESSED \r\n");
            }
        }
    }
}


/**************************************************************************//**
 * @brief
 *   Function called externally to kick of cellular session
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_cellular_online_enter(E_CELL_SESSION state)
{
    // Set state to begin cellular session
    pfnCell_State = app_cellular_state_online_enter;
    
    // Set flags so cellular state machine keeps getting ticks until complete
    needMoreTicks = true; 

    // Used passed session for cellular session
    cellSession = state;

    // Disable RTC Timestamp messages during cell sessions
    app_rtc_debugTextOnRTCIRQ_disable();
}

/****************************************************************************
 * @brief
 *   Function to request a system reset on cell session termination.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_cellular_request_reset(void)
{
    resetRequested = true;
}

/****************************************************************************
 * @brief
 *   Function to exit cellular state of operation
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cellular_online_exit(void)
{
    // Set state safe state
    app_cellular_safe_enter();
    
    // Cellular session complete, no more ticks needed
    needMoreTicks = false;

    // Re-enable RTC Timestamp messages
    app_rtc_debugTextOnRTCIRQ_enable();

    // FORCE RESET HERE FOR IMAGE UPDATE!!!
    if(resetRequested)
    {
        NVIC_SystemReset();
    }
}


/****************************************************************************
 * @brief
 *    Called by MQTT Manager on receive of PUBLISH message.
 *
 * @param[in] pPubMsg The received Publish message
 *
 * @return None
 * @todo Here, we check we received  what we published.
 *****************************************************************************/
static void app_cellular_onPublishMessageReceivedCallback(const MQTT_MANAGER_PUB* pPubMsg)
{
    APP_RX_MSG rxMsg;
    
    rxMsg.pData         = pPubMsg->pData;
    rxMsg.dataLength    = pPubMsg->dataLength;
    rxMsg.source        = CMIU_INTERFACE_CELLULAR;
    
    app_message_on_receive(&rxMsg);
}


/****************************************************************************
 * @brief
 *    Called to send a Pub as response to a received pub command
 *
 * @param[in]  buffer The data to send
 * @param[in]  dataLength The number of bytes to send
 *
 * @return None
 * @note This is called during an MQTT Session once a rx'd pub has been 
 * parsed and handled via app_message_on_receive()
 *****************************************************************************/
void app_cellular_publishResponse(const uint8_t* const buffer, uint32_t dataLength)
{
    pubResponse.topic       = (char *)pstrMiuPubTopic;
    pubResponse.qos         = E_MQTT_QOS_1;
    pubResponse.onPopulate  = NULL;
    
    pubResponse.pData       = buffer;
    pubResponse.dataLength  = dataLength;

    MqttManagerAddPublish(&mqttSessionParams, &pubResponse);
}


/****************************************************************************
 * @brief
 *    Called by MQTT Manager on receive of byte for file transfer 
 *
 * @param[in] pDl The Downloader object
 * @param[in] b The received byte
 * @param[in] byteIndex The 0-based index of the byte b.
 *
 * @return None
 *****************************************************************************/
static void app_cellular_onReceivedFileByteCallback(struct FILE_DOWNLOADER* pDl,
    uint8_t b, 
    uint32_t byteIndex)
{
    uint32_t externalAddress;
    bool writtenOK;

    // Get address to program byte into flash
    externalAddress = (imageUpdate->extStartAddress + byteIndex);

    flashIndex = (byteIndex & 0xFF);

#ifdef TESTING_WITH_THE_PATTERNED_FILE
    if (flashIndex != b)
    {
        char    tempBuff[64];
        if ((flashIndex & 0xF) == 0xF)  // limit messages; should use a counter or something
        {
        sprintf (tempBuff, "BYTE MISMATCH: idx: %x val: %x\n", flashIndex, b);
        DEBUG_OUTPUT_TEXT(tempBuff);
        }
    }
#endif //  TESTING_WITH_THE_PATTERNED_FILE


    // Load byte into buffer
    flashBuf[flashIndex] = b;

    // Program 256 bytes at a time
    if((externalAddress & 0x000000FF) == 0x000000FF)
    {
        writtenOK = Flash_WriteAndVerify(flashBuf, flashVerifyBuffer, (externalAddress - 0xFF), 256);

        if(!writtenOK)
        {
            DEBUG_OUTPUT_TEXT("FLASH WRITE ERROR\r\n");
            // The write failed, so shut this thing down.
            pDl->errorState = FILE_DOWNLOADER_USER_CANCELLED;
            pDl->state = E_FD_DONE_ERROR;
        }

        WDOG_Feed();
    }
}

    
/******************************************************************************
**    
**           @param[in] pSm The TCP Session 
**           @param[in] pManager The File Downloader object
**              @return Percent of transfer complete
**  @brief
**         Description: Gets the progress info
**
******************************************************************************/
static void app_cellular_OnFileTansferCompleteCallback(TCP_SESSION_MANAGER* pSm, void* pManager)
{
    
    FILE_DOWNLOADER* pFileDownloader = (FILE_DOWNLOADER*)pManager;
    
    /** Deinitialize flash communications. */
    Flash_Deinit();
    
    DEBUG_OUTPUT_TEXT("File tranfer complete callback"); 

    //Display results
    FileDownloaderDebugPrintResult(pFileDownloader);     

    // Check if no errors on file download
    if(pFileDownloader->errorState == FILE_DOWNLOADER_OK)
    {
        // Clear update required flag since file download complete
        imageUpdate->imgReqUpdateFlag = false;

        // Schedule any remaining images for update during later session
        app_command_imageUpdate_schedule();
 
        
        // Check if any remaining images needed updates, if so don't reset yet       
        // Retrieve image that needs updating, will retrieve first one found
        imageUpdate = app_command_imageUpdate_infoRetrieve();
    
        // Will be NULL if no images need update
        if(imageUpdate == NULL)   
        {                 
            // Set reset request flag, to allow bootlaoder to load new image/s.  The reset
            // will be done once cellular has disconnected.
            resetRequested = true; 
            
        }       
    }
       
    else    // File not successful, reshedule attempt
    {
        // Reschedule for four hours from now, should be config constant!!!
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_IMAGEUPDATE, E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, 14400, NULL);
    }   
}




