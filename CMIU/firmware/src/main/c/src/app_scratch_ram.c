/******************************************************************************
*******************************************************************************
**
**         Filename: app_scratch_ram.c
**
**           Author: Duncan Willis
**          Created: 9 Nov 2015
**
**     Last Edit By: Brian Arnberg (2015.12.30)
**        Last Edit: Updated the way the the buffers in scratch ram are
**                   organized and accessed.
**
**      Description: This modules controls access to shared buffers in
**                   the scratch ram region of memory space.
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_scratch_ram.c
*
* @brief Scratch RAM buffer
*
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include <stdio.h>
#include "typedefs.h"
#include "em_assert.h"
#include "app_scratch_ram.h"



// Structure holds various buffers in the scratch ram portion of memory.
// This holds 2k of scratch ram memory.
static shared_ram_t ram_buffer SCRATCH_RAM_VARS;

/*****************************************************************************
 * @brief
 *  Return the address of the first free 1kB block of ram controlled by this
 *  module.
 *
 * @note
 *  Right now, this name returns the address of the same memory block without
 *  checking whether it is actually free. Long term for this is to actually
 *  return a pointer to whichever block is free (which means I also plan on
 *  adding a `release' function).
 *
 * @param[in] pSize - pointer to the size of the buffer.
 *
 * @return pointer to a 1k block of free space in scratch ram
 ******************************************************************************/
uint8_t* app_scratch_ram_get(uint32_t* pSize)
{
    uint8_t* pAddress;

    if (NULL != pSize)
    {
        *pSize = sizeof(ram_buffer.buf1kB[0]);
    }

    /* Set the appropriate return address. */
    pAddress = ram_buffer.buf1kB[0].ab;

    /* Return the address. */
    return pAddress;
}

/*****************************************************************************
 * @brief
 *   Return the address to the first free 256B block of ram controlled by this
 *   module.
 *
 * @note
 *  Right now, this name returns the address of the same memory block without
 *  checking whether it is actually free. Long term for this is to actually
 *  return a pointer to whichever block is free (which means I also plan on
 *  adding a `release' function).
 *
 * @param[in] None
 *
 * @return pointer to a 256B block of free space in scratch ram
 ******************************************************************************/
uint8_t* app_scratch_ram_get_256(void)
{
    uint8_t* pAddress;

    /* Set the appropriate return address. */
    pAddress = ram_buffer.buf256B[4].ab;

    /* Return the address. */
    return pAddress;
}
