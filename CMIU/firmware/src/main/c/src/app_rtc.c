/******************************************************************************
*******************************************************************************
**
**         Filename: app_rtc.c
**    
**           Author: Troy Harstad
**          Created: 1/16/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_rtc.c
*
* @brief This file contains the application code used to setup/use the RTC
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include "typedefs.h"
#include "app_rtc.h"
#include "app_cmiu_app.h"
#include "em_cmu.h"
#include "em_rtc.h"
#include "em_gpio.h"
#include "app_uart.h"
#include "app_scheduler.h"
#include "app_cmit_interface.h"
#include "app_rmu.h"
#include "em_rmu.h"
#include "MemoryMap.h"

#include "time.h"
#include "DebugTrace.h"
#include <string.h>


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
// RTC is 32.768 kHz crystal
#define RTC_FREQ    32768 
   

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static void app_rtc_clock_inc(void);
static bool app_rtc_debugTextOnRTCIRQ_get(void);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// Variable that gets incremented once per second that can be used
// by other modules for multi-second timeouts.
volatile uint32_t secondsCount;// = 0;

// CMIU Time
volatile U_NEPTUNE_TIME cmiuTime;

// Time difference last time was updated
volatile U_NEPTUNE_TIME diffTime;

// Flag for surpressing (or not) debug output during the RTC IRQ.  Debug 
// output is surpressed during ARB reads so the clock output in not affected
// due to processing the debug output.
static bool bDebugTextOnRTCIRQ;

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/



/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/



/***************************************************************************//**
 * @brief
 *   Initializes RTC module
 *
 * @note
 *   Enables LFACLK and selects LFXO as clock source for RTC.  Sets up the RTC 
 *   to generate an interrupt every second.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_rtc_init(void)
{  
    // Only init if it was not a reset pin reset (mag swipe) and not a system 
    // request (software) reset.  Don't want to reset the time in either case.
    if(((app_rmu_resetCause_Get() & RMU_RSTCAUSE_EXTRST) != RMU_RSTCAUSE_EXTRST) &&
       ((app_rmu_resetCause_Get() & RMU_RSTCAUSE_SYSREQRST) != RMU_RSTCAUSE_SYSREQRST))
    {        
        // Default to Oct. 24th, 1980 12:00 GMT.        
        cmiuTime.S64 = 341236800;       
    }


    diffTime.S64 = 0;
    secondsCount = 0;
    
    
    RTC_Init_TypeDef rtcInit = RTC_INIT_DEFAULT;

    /* Enable LE domain registers */
    CMU_ClockEnable(cmuClock_CORELE, true);

    /* Enable LFXO as LFACLK in CMU. This will also start LFXO */
    CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);

    /* Set a clock divisor of 32 to reduce power consumption. */
    CMU_ClockDivSet(cmuClock_RTC, cmuClkDiv_32);

    /* Enable RTC clock */
    CMU_ClockEnable(cmuClock_RTC, true);

    /* Initialize RTC */
    rtcInit.enable   = false;  /* Do not start RTC after initialization is complete. */
    rtcInit.debugRun = false;  /* Halt RTC when debugging. */
    rtcInit.comp0Top = true;   /* Wrap around on COMP0 match. */
    RTC_Init(&rtcInit);

    /* Interrupt every second */
    RTC_CompareSet(0, ((RTC_FREQ / 32)) - 1 );
    NVIC_SetPriority(RTC_IRQn, (1 << __NVIC_PRIO_BITS) - 1); /* Lowest priority */
   
    /* Enable interrupt */
    NVIC_EnableIRQ(RTC_IRQn);
    RTC_IntEnable(RTC_IEN_COMP0);

    /* Start Counter */
    RTC_Enable(true);
    
    // Init time difference to 0
    diffTime.S64 = 0;
    
    // Enable debug output during RTC IRQ by default
    app_rtc_debugTextOnRTCIRQ_enable();
    
}





/***************************************************************************//**
 * @brief
 *   Set the CMIU time via pointer
 *
 * @param[in] Pointer to NEPTUNE_TIME_T that the time gets set from
 *
 * @return None
 ******************************************************************************/
void app_rtc_time_set(U_NEPTUNE_TIME * time)
{   
    U_NEPTUNE_TIME prevTime;
    
    // Store current time for later use
    prevTime.S64 = cmiuTime.S64;
    
    // Calculate time change, positive indicates new time is after prev time
    diffTime.S64 = time->S64 - prevTime.S64;
    
    DEBUG_TRACE("Time Change (Unix): %d\r\n", (int32_t)diffTime.S64);
    
    // New time is now CMIU time
    cmiuTime.S64 = time->S64;        
    
    // Check if new time is different to current time
    if(diffTime.S64 != 0)
    {
        // Reschedule tasks
        app_scheduler_timeChange((U_NEPTUNE_TIME*)&diffTime);                 
    }      
}



/***************************************************************************//**
 * @brief
 *   Get the CMIU time via pointer
 *
 * @param[in] Pointer to NEPTUNE_TIME_T that time gets loaded in to
 *
 * @return None
 ******************************************************************************/
U_NEPTUNE_TIME app_rtc_time_get(void)
{
    return cmiuTime;  
}



/***************************************************************************//**
 * @brief
 *   Increment the CMIU time by one second
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_rtc_clock_inc(void)
{
    // Increment CMIU time by one second
    cmiuTime.S64++;
    
    // Only output current time if not surpressed
    if (app_rtc_debugTextOnRTCIRQ_get())
    {
    
        DEBUG_OUTPUT_TEXT_AND_DATA("CMIU Time (Unix): ", (uint8_t*)&cmiuTime.S8[0], 4);
    
        // Print time every minute
        if (cmiuTime.S64 % 60 == 0)
        {
            app_rtc_DebugPrintTime64(cmiuTime);
        }
   }
}



/***************************************************************************//**
 * @brief
 *   Interrupt Service Routine for RTC
 *
 * @note
 *   Should be reached once per second
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void RTC_IRQHandler(void)
{
    // Clear RTC Comp 0 interrupt
    RTC_IntClear(RTC_IFC_COMP0);    
    
    // Increment clock by one second
    app_rtc_clock_inc();
    
    // Notify scheduler with new time 
    app_scheduler_intervalUpdate();    

    // Increment seconds counter
    secondsCount++;
    
}

/***************************************************************************//**
 * @brief
 *   Get the CMIU time in seconds.
 *
 * @param[in] none
 *
 * @return number of seconds that the CMIU has been running.
 ******************************************************************************/
uint32_t app_rtc_seconds_get()
{
    return secondsCount;  
}


/***************************************************************************//**
 * @brief
 *   Get the last time difference in seconds.
 *
 * @param[in] none
 *
 * @return number of seconds the time was adjusted on the last call to 
 *     app_rtc_time_set().
 ******************************************************************************/
U_NEPTUNE_TIME app_rtc_timeDiff_full_get(void)
{
    return diffTime;  
}



/***************************************************************************//**
 * @brief
 *   Get the last time difference in seconds.
 *
 * @param[in] none
 *
 * @return number of seconds the time was adjusted on the last call to 
 *     app_rtc_time_set().
 ******************************************************************************/
int8_t app_rtc_timeDiff_partial_get(void)
{
    U_NEPTUNE_TIME tempDiffTime;
    
    tempDiffTime.S64 =  diffTime.S64;
    
    // If time difference is positive and > 127 seconds, then use 127
    if(tempDiffTime.S64 > 127)       
    {
        return 127;
    }

    // If time difference is 0 up to 127 then use actual value of
    // tempDiffTime.S8[7] (LSB)
    else if ((tempDiffTime.S64 >= 0) && (tempDiffTime.S64 <= 127))
    {
        return tempDiffTime.S8[7];
    }
    
      
    // If time difference is negative and < -128, then use -128
    else if (tempDiffTime.S64 <= -128)
    {
        return -128;
    }
    
    // If time difference is -1 down to -128 then use actual value of
    // tempDiffTime.S64[7] (LSB), set negative bit 
    else  //((tempDiffTime.S64 < 0x00) && (tempDiffTime.S64 > -128))
    {
        return (tempDiffTime.S8[7] |= 0x80);
    }
    
    
}


/***************************************************************************//**
 * @brief
 *   Format the time into a string
 *
 * @param[in]  tt   Time as time_t
 * @param[out] buf  Time string output as formatted time
 * @param[in]  bufSize. size of output buffer.
 *
 * @return None
 ******************************************************************************/
void app_rtc_FormatTimeT(const time_t tt, char buf[], uint32_t bufSize)
{
    struct tm   timeinfo;
    const char* sTime;

    localtime_r( &tt, &timeinfo);
    sTime = asctime(&timeinfo);
        
    strncpy(buf, sTime, bufSize);
    buf[ bufSize - 1 ] = '\0';
}


/***************************************************************************//**
 * @brief
 *   Format the time into a string
 *
 * @param[in]  tt   Time as time_t
 * @param[out] buf  Time string output as formatted time
 * @param[in]  bufSize. size of output buffer.
 *
 * @return None
 ******************************************************************************/
void app_rtc_FormatTime(const U_NEPTUNE_TIME nt, char buf[], uint32_t bufSize)
{
    uint32_t    t32 = (uint32_t)nt.S64;

    app_rtc_FormatTimeT((time_t)t32, buf, bufSize);
}


/***************************************************************************//**
 * @brief
 *   DebugPrint the time 
 *
 * @param[in] Time as 64bit custom struct
 *
 * @return None
 ******************************************************************************/
void app_rtc_DebugPrintTime64(const U_NEPTUNE_TIME nt)
{
    char        buf[30];

    app_rtc_FormatTime(nt, buf, sizeof(buf));
 
    DebugTrace( "Time = %s\r", buf); 
}



/***************************************************************************//**
 * @brief
 *   Enable debug text from RTC IRQ - Debug output is surpressed during ARB 
 *   reads so the clock output in not affected due to processing the debug output.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_rtc_debugTextOnRTCIRQ_enable(void)
{
    bDebugTextOnRTCIRQ = true;
}


/***************************************************************************//**
 * @brief
 *   Disable debug text from RTC IRQ - Debug output is surpressed during ARB 
 *   reads so the clock output in not affected due to processing the debug output.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_rtc_debugTextOnRTCIRQ_disable(void)
{
    bDebugTextOnRTCIRQ = false;
}



/***************************************************************************//**
 * @brief
 *   Get bDebugTextOnRTCIRQ value
 *
 * @param[in] None
 *
 * @return bDebugTextOnRTCIRQ
 ******************************************************************************/
static bool app_rtc_debugTextOnRTCIRQ_get(void)
{
   return bDebugTextOnRTCIRQ;
}


