/******************************************************************************
*******************************************************************************
**
**         Filename: app_packet_builder.h
**    
**           Author: Troy Harstad
**          Created: 4/15/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_PACKET_BUILDER_H
#define __APP_PACKET_BUILDER_H

#include "MqttManager.h"
#include "TagTypes.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
 /** 88 bytes for MQTT + 16 bytes for tpbp buffering + lots of extra space */
#define INTERVAL_DATA_FOOTER 200u


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/

extern void app_packet_builder_init(void);
extern void app_packet_builder_set_packet_instigator(E_PACKET_INSTIGATOR const packetInstigator);
extern uint32_t app_packet_builder_build_detailedConfiguration(uint8_t* const buffer, uint32_t bufferSize );
extern uint32_t app_packet_builder_build_basicConfiguration(uint8_t* const buffer, uint32_t bufferSize);
extern uint32_t app_packet_builder_build_intervalData(uint8_t* const buffer, uint32_t bufferSize );
extern uint32_t app_packet_builder_build_canData(uint8_t* const buffer, uint32_t bufferSize);
extern uint32_t app_packet_builder_build_readConnectedDeviceResponse(uint8_t* const buffer, uint32_t bufferSize, E_ERROR_TAG   errorCode);
extern uint32_t app_packet_builder_build_signalQualityResponse(uint8_t* const buffer, uint32_t bufferSize);
extern uint32_t app_packet_builder_build_requestApnResponse(uint8_t* const buffer, uint32_t bufferSize);
extern uint32_t app_packet_builder_build_updateApnResponse(uint8_t* const buffer, uint32_t bufferSize);
extern uint32_t app_packet_builder_build_userDataPacket(
                                    S_RECORDING_REPORTING_INTERVAL intervals,
                                    uint8_t* const buffer, uint32_t bufferSize);
                                              
extern uint32_t app_packet_builder_build_commandResponse(
                                        uint8_t* const buffer,
                                        uint32_t bufferSize,
                                        E_COMMAND     command,
                                        E_ERROR_TAG   errorCode);
                                              


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_PACKET_BUILDER_H

