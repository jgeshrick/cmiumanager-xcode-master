/******************************************************************************
*******************************************************************************
**
**         Filename: app_scheduler.c
**    
**           Author: Troy Harstad
**          Created: 7/8/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_cmiu_app.c
*
* @brief This file contains the main application code to schedule and
* perform tasks.
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "typedefs.h"
#include "app_rtc.h"
#include "app_cmiu_app.h"
#include "app_arb.h"
#include "CmiuAppConfiguration.h"
#include "app_metrology.h"
#include "app_uart.h"
#include "app_rtc.h"
#include "app_operatingMode.h"
#include "app_packet_parser.h"
#include "app_packet_builder.h"
#include "MqttManager.h"
#include "TcpSessionManager.h"
#include "CmiuAppImageInfo.h"
#include "app_power.h"
#include "app_adc.h"
#include "app_scheduler.h"
#include "app_cmit_interface.h"
#include "app_rmu.h"
#include "em_rmu.h"

// Modem Library Header Files
#include "modem.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/



/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static U_NEPTUNE_TIME app_scheduler_findNextInterval(uint32_t timeAlignmentSeconds);
static U_NEPTUNE_TIME app_scheduler_scheduleTask_cellConnection(void);
static void DebugScheduleTime(const char* str, const S_TASK_SCHEDULER* pTask);
/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/

// TASKS
S_TASK_SCHEDULER  registerReadTask;
S_TASK_SCHEDULER  detailedConfigPacketTask;  
S_TASK_SCHEDULER  intervalPacketTask;
S_TASK_SCHEDULER  cellConnectionTask;
S_TASK_SCHEDULER  imageUpdateTask;
S_TASK_SCHEDULER  datalogTask;
S_TASK_SCHEDULER  modemFotaTask;


// Local CMIU time
static volatile U_NEPTUNE_TIME cmiuTimeTemp;

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/

/**************************************************************************//**
 * @brief
 *   Initializes CMIU Application module
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_scheduler_init(void)
{    
    uint32_t batteryAdc;
    
    // No tasks due by default
    // Only init if it was not a reset pin reset (mag swipe) and not a system 
    // request (software) reset.  Don't want to reset the change the task schedule
    // in either case
    if(((app_rmu_resetCause_Get() & RMU_RSTCAUSE_EXTRST) != RMU_RSTCAUSE_EXTRST) &&
       ((app_rmu_resetCause_Get() & RMU_RSTCAUSE_SYSREQRST) != RMU_RSTCAUSE_SYSREQRST))
    {
        app_scheduler_unscheduleTask(E_SCHEDULER_TASK_TYPE_REGISTER_READ);
        app_scheduler_unscheduleTask(E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET);
        app_scheduler_unscheduleTask(E_SCHEDULER_TASK_TYPE_INTERVALDATA_PACKET);
        app_scheduler_unscheduleTask(E_SCHEDULER_TASK_TYPE_CELLCONNECTION);
        app_scheduler_unscheduleTask(E_SCHEDULER_TASK_TYPE_IMAGEUPDATE); 
        app_scheduler_unscheduleTask(E_SCHEDULER_TASK_TYPE_DATALOG); 
        app_scheduler_unscheduleTask(E_SCHEDULER_TASK_TYPE_MODEMFOTA);  
        
    }    
    
    // Setup random value generator by seeding with CMIU ID and battery voltage
    app_adc_vddDiv3_setupAndMeasure();
    batteryAdc = app_adc_vddDiv3_retrieve();
    
    // Shift battery voltage to upper bits
    batteryAdc <<= 20;
    
    // Seed psuedo random number generator
    srand(batteryAdc + puMiuId->dwMiuId);
    
}


/**************************************************************************//**
 * @brief
 *      Function checks the current time against the scheduled times for various
 *      tasks to be performed by the CMIU.  This is to be called once per second
 *      by RTC interrupt. 
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_scheduler_intervalUpdate(void)
{        
    // Get current time
    cmiuTimeTemp = app_rtc_time_get();
    
    // Check if time to read a register
    if(cmiuTimeTemp.S64 >= registerReadTask.taskTime.S64) 
    {        
        registerReadTask.taskDue = true;
    }
    
    // Check if time to a transmit a detailed config packet
    if(cmiuTimeTemp.S64 >= detailedConfigPacketTask.taskTime.S64) 
    {        
        detailedConfigPacketTask.taskDue = true;
    }
    
    // Check if time to a transmit a interval data packet
    if(cmiuTimeTemp.S64 >= intervalPacketTask.taskTime.S64) 
    {        
        intervalPacketTask.taskDue = true;
    }    

    // Check if time to connect to cellular network to publish/subscribe packets
    if(cmiuTimeTemp.S64 >= cellConnectionTask.taskTime.S64) 
    {        
        cellConnectionTask.taskDue = true;
    }     
    
    // Check if time to perform image update
    if(cmiuTimeTemp.S64 >= imageUpdateTask.taskTime.S64) 
    {        
        imageUpdateTask.taskDue = true;
    }  
    
    // Check if time to perform datalog storage
    if(cmiuTimeTemp.S64 >= datalogTask.taskTime.S64) 
    {        
        datalogTask.taskDue = true;
    }      
    
    // Check if time to perform modem FOTA 
    if(cmiuTimeTemp.S64 >= modemFotaTask.taskTime.S64) 
    {        
        modemFotaTask.taskDue = true;
    }      
    
}   



/**************************************************************************//**
 * @brief
 *      This function schedules the next time for a task to be performed.  If 
 *      the inteval type is set to "E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION" then 
 *      it will use the time based in the CmiuAppConfiguration.c, if 
 *      E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, it will use the seconds value
 *      passed in the function as an offset to current time, if 
 *      E_SCHEDULER_INTERVAL_TYPE_FORCE_TIME, it will use the time passesd.
 *
 * @param[in] taskType          The task to schedule
 * @param[in] intervalType      The interval type to use
 * @param[in] forceSeconds      The number of seconds to schedule from current 
 * @param[in] time              The time to schedule the task
 *
 * @return None
 *****************************************************************************/
void app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE taskType, E_SCHEDULER_INTERVAL_TYPE intervalType, uint32_t forceSeconds, int64_t time)
{
    U_NEPTUNE_TIME tempTime;
    
    // Get time
    tempTime = app_rtc_time_get();
    
    
    switch (taskType)  
    {

        case E_SCHEDULER_TASK_TYPE_REGISTER_READ:
        {
            // Check if forcing time to schedule using offset
            if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET)
            {
                registerReadTask.taskTime.S64 = (tempTime.S64 + forceSeconds);
            }
            
            // Check if forcing time to schedule using time 
            else if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_TIME)
            {
                registerReadTask.taskTime.S64 = time;               
            }            
            
            else // E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION - Used fixed interval in configuration to schedule
            {
                registerReadTask.taskTime = (app_scheduler_findNextInterval(uMIUConfigNormal.sMIUConfigNormal.arbReadIntervalSecs)); 
            }
            
            DebugScheduleTime("Next Register Reading Scheduled for", &registerReadTask);
                                    
            // Clear task due flag
            registerReadTask.taskDue = false;              
            
            break;
        }
               
        case E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET:
        {
            // Check if forcing time to schedule using offset
            if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET)
            {
                detailedConfigPacketTask.taskTime.S64 = (tempTime.S64 + forceSeconds);
            }
            
            // Check if forcing time to schedule using time 
            else if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_TIME)
            {
                detailedConfigPacketTask.taskTime.S64 = time;               
            }            
            
            else // E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION - Used fixed interval in configuration to schedule
            {
                detailedConfigPacketTask.taskTime = (app_scheduler_findNextInterval(uMIUConfigNormal.sMIUConfigNormal.detailedConfigPacketIntervalSecs)); 
            }
              
            
            DebugScheduleTime("Next Detailed Config Packet Scheduled for", &detailedConfigPacketTask);
            //DEBUG_OUTPUT_TEXT_AND_DATA("Next Detailed Config Packet Scheduled for: ", (uint8_t*)&detailedConfigPacketTask.taskTime.S8[0], 4);
            
            // Clear task due flag
            detailedConfigPacketTask.taskDue = false;              
            
            break;
        }
        
        
        case E_SCHEDULER_TASK_TYPE_INTERVALDATA_PACKET:
        {
            // Check if forcing time to schedule using offset
            if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET)
            {
                intervalPacketTask.taskTime.S64 = (tempTime.S64 + forceSeconds);
            }
            
            // Check if forcing time to schedule using time 
            else if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_TIME)
            {
                intervalPacketTask.taskTime.S64 = time;               
            }            
            
            else // E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION - Used fixed interval in configuration to schedule
            {
                intervalPacketTask.taskTime = (app_scheduler_findNextInterval(uMIUConfigNormal.sMIUConfigNormal.intervalDataPacketIntervalSecs)); 
            }
          
            DebugScheduleTime("Next Interval Data Packet Scheduled for", &intervalPacketTask);
          
            //DEBUG_OUTPUT_TEXT_AND_DATA("Next Interval Data Packet Scheduled for: ", (uint8_t*)&intervalPacketTask.taskTime.S8[0], 4); 
            
            // Clear task due flag
            intervalPacketTask.taskDue = false;              
            
            break;
        }
        
        
        case E_SCHEDULER_TASK_TYPE_CELLCONNECTION:
        {
            // Check if forcing time to schedule using offset
            if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET)
            {
                cellConnectionTask.taskTime.S64 = (tempTime.S64 + forceSeconds);
            }
            
            // Check if forcing time to schedule using time 
            else if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_TIME)
            {
                cellConnectionTask.taskTime.S64 = time;               
            }            
            
            else // E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION - Used fixed interval in configuration to schedule
            {
                cellConnectionTask.taskTime = app_scheduler_scheduleTask_cellConnection(); 
            }
            
            
            DebugScheduleTime("Next Cell Connection Scheduled for", &cellConnectionTask);
           // DEBUG_OUTPUT_TEXT_AND_DATA("Next Cell Connection Scheduled for: ", (uint8_t*)&cellConnectionTask.taskTime.S8[0], 4);
                
            // Clear task due flag
            cellConnectionTask.taskDue = false;              
            
            break;
        }      
        
        
        case E_SCHEDULER_TASK_TYPE_IMAGEUPDATE:
        {
            // Check if forcing time to schedule using offset
            if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET)
            {
                imageUpdateTask.taskTime.S64 = (tempTime.S64 + forceSeconds);
            }
            
            // Check if forcing time to schedule using time 
            else if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_TIME)
            {
                imageUpdateTask.taskTime.S64 = time;               
            }            
            
            else // E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION - Used fixed interval in configuration to schedule
            {
                imageUpdateTask.taskTime = (app_scheduler_findNextInterval(uMIUConfigNormal.sMIUConfigNormal.cellCallInIntervalSecs)); 
            }
            
            
            DebugScheduleTime("Next Image Update Scheduled for", &imageUpdateTask);
           // DEBUG_OUTPUT_TEXT_AND_DATA("Next Image Update Scheduled for: ", (uint8_t*)&imageUpdateTask.taskTime.S8[0], 4);
                
            // Clear task due flag
            imageUpdateTask.taskDue = false;              
            
            break;
        }     


        case E_SCHEDULER_TASK_TYPE_DATALOG:
        {
            // Check if forcing time to schedule using offset
            if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET)
            {
                datalogTask.taskTime.S64 = (tempTime.S64 + forceSeconds);
            }
            
            // Check if forcing time to schedule using time 
            else if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_TIME)
            {
                datalogTask.taskTime.S64 = time;               
            }            
            
            else // E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION - Used fixed interval in configuration to schedule
            {
                datalogTask.taskTime = (app_scheduler_findNextInterval(uMIUConfigNormal.sMIUConfigNormal.datalogIntervalSecs)); 
            }
            
            
             DebugScheduleTime("Next Datalog Storage Scheduled for", &datalogTask);
           // DEBUG_OUTPUT_TEXT_AND_DATA("Next Datalog Storage Scheduled for: ", (uint8_t*)&datalogTask.taskTime.S8[0], 4); 
            
            // Clear task due flag
            datalogTask.taskDue = false;              
            
            break;
        }        
        
        // Modem Fota task
        case E_SCHEDULER_TASK_TYPE_MODEMFOTA:
        {
            // Check if forcing time to schedule using offset
            if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET)
            {
                modemFotaTask.taskTime.S64 = (tempTime.S64 + forceSeconds);
            }
            
            // Check if forcing time to schedule using time 
            else if(intervalType == E_SCHEDULER_INTERVAL_TYPE_FORCE_TIME)
            {
                modemFotaTask.taskTime.S64 = time;               
            }            
            
            else // E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION - Used fixed interval in configuration to schedule
            {
                modemFotaTask.taskTime = (app_scheduler_findNextInterval(uMIUConfigNormal.sMIUConfigNormal.cellCallInIntervalSecs)); 
            }
            
            
            DebugScheduleTime("Next modem FOTA Scheduled for", &imageUpdateTask);
                
            // Clear task due flag
            modemFotaTask.taskDue = false;              
            
            break;
        }     


        
    }
      
}




/**************************************************************************//**
 * @brief
 *      Function will randomly schedule cellular connection based on various inputs
 *
 * @note
 *      1.) Get current time
 *      2.) Use that to find next call-in interval within current day. So if it is 
 *          2:45 PM and the call-in interval is 4 hours, the next call-in interval 
 *          time would be 4PM.
 *      3.) Generate randomization value based on CMIU ID and battery ADC. 
 *      4.) Divide the random value against the maximum call-in window, add modulo to 
 *          call-in time (now 4PM + random offset)
 *      5.) Add "initial call-in time" (now 4PM + random offset + call-in offset)

 *
 * @param[in] None
 *
 * @return U_NEPTUNE_TIME next cell connection time
 *****************************************************************************/
static U_NEPTUNE_TIME app_scheduler_scheduleTask_cellConnection(void)
{
    U_NEPTUNE_TIME tempTime;
    uint32_t windowOffsetSecondsMax;
    uint32_t randVal;

    // Get next interval time
    tempTime = (app_scheduler_findNextInterval(uMIUConfigNormal.sMIUConfigNormal.cellCallInIntervalSecs));
    
    // Determine maximum randomized offset
    windowOffsetSecondsMax = (uMIUConfigNormal.sMIUConfigNormal.cellCallInWindowQtrHrs * 900);
    
    // Can't perform modulo on value of 0 so set random offset value to 0 here
    // if necessary
    if(windowOffsetSecondsMax == 0)
    {
        // No random offset value if max value is 0
        randVal = 0;      
    }        
    
    else // windowOffsetSecondsMax > 0
    {
        // Get random value
        randVal = rand();
        
        // Perform modulo to reduce random value to max window offset
        randVal = randVal % windowOffsetSecondsMax;            
    }       
    
    // Add random offset to next interval time
    tempTime.S64 += randVal;
    
    // Add call-in offset
    tempTime.S64 += (uMIUConfigNormal.sMIUConfigNormal.cellCallInOffsetMins * 60);
       
    return tempTime;
}




/**************************************************************************//**
 * @brief
 *      Function will unsechedule the passed task by setting the tasktime
 *      to the max value (hundred trillion years in future) and clearing the
 *      taskDue flag. 
 *
 * @param[in] E_SCHEDULER_TASK_TYPE is the task to unschedule
 *
 * @return None
 *****************************************************************************/
void app_scheduler_unscheduleTask(E_SCHEDULER_TASK_TYPE taskType)
{
    switch (taskType)  
    {
        case E_SCHEDULER_TASK_TYPE_REGISTER_READ:
        {
            registerReadTask.taskTime.S64 = RTC_MAX_TIME;
            registerReadTask.taskDue = false;
            break;
        }
               
        case E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET:
        {
            detailedConfigPacketTask.taskTime.S64 = RTC_MAX_TIME;
            detailedConfigPacketTask.taskDue = false;
            break;
        }        
        
        case E_SCHEDULER_TASK_TYPE_INTERVALDATA_PACKET:
        {
            intervalPacketTask.taskTime.S64 = RTC_MAX_TIME;
            intervalPacketTask.taskDue = false;
            break;
        }
                
        case E_SCHEDULER_TASK_TYPE_CELLCONNECTION:
        {
            cellConnectionTask.taskTime.S64 = RTC_MAX_TIME;
            cellConnectionTask.taskDue = false;
            break;
        }      
                
        case E_SCHEDULER_TASK_TYPE_IMAGEUPDATE:
        {
            imageUpdateTask.taskTime.S64 = RTC_MAX_TIME;
            imageUpdateTask.taskDue = false;
            break;
        }      

        case E_SCHEDULER_TASK_TYPE_DATALOG:
        {
            datalogTask.taskTime.S64 = RTC_MAX_TIME;
            datalogTask.taskDue = false;
            break;
        }           

        // Modem FOTA task
        case E_SCHEDULER_TASK_TYPE_MODEMFOTA:
        {
            modemFotaTask.taskTime.S64 = RTC_MAX_TIME;
            modemFotaTask.taskDue = false;
            break;
        }      
    }
  
}


// Function returns task due flag based on reques task type
bool app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE taskType)
{      
    switch(taskType)
    {   
     
        case E_SCHEDULER_TASK_TYPE_REGISTER_READ:
        {           
            return registerReadTask.taskDue;                               
        }
               
        case E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET:
        {
            return detailedConfigPacketTask.taskDue;                      
        }
        
        
        case E_SCHEDULER_TASK_TYPE_INTERVALDATA_PACKET:
        {
            return intervalPacketTask.taskDue;                      
        }
        
        
        case E_SCHEDULER_TASK_TYPE_CELLCONNECTION:
        {
         
            return cellConnectionTask.taskDue;                                 
        }
        
        case E_SCHEDULER_TASK_TYPE_IMAGEUPDATE:
        {
         
            return imageUpdateTask.taskDue;                                 
        }
        
        case E_SCHEDULER_TASK_TYPE_DATALOG:
        {
         
            return datalogTask.taskDue;                                 
        }

        case E_SCHEDULER_TASK_TYPE_MODEMFOTA:
        {
         
            return modemFotaTask.taskDue;                                 
        }
        
        default:
        {
            // Not valid task type
            return false;
        }
    }   
    
}

/**************************************************************************//**
 * @brief
 *  Function will return the task time for the requests task type. If an invalid
 *  task type is passed, the function will return the maximum RTC value which
 *  should indicate an error to the calling function.
 *
 * @param[in] E_SCHEDULER_TASK_TYPE is the task to unschedule
 *
 * @return task_time - Next time the passed task type will be executed.
 *****************************************************************************/
uint64_t app_scheduler_checkTaskTime(E_SCHEDULER_TASK_TYPE taskType)
{
    uint64_t task_time;
    switch(taskType)
    {
        case E_SCHEDULER_TASK_TYPE_REGISTER_READ:
        {
            task_time = registerReadTask.taskTime.S64;
            break;
        }
        case E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET:
        {
            task_time = detailedConfigPacketTask.taskTime.S64;
            break;
        }
        case E_SCHEDULER_TASK_TYPE_INTERVALDATA_PACKET:
        {
            task_time = intervalPacketTask.taskTime.S64;
            break;
        }
        case E_SCHEDULER_TASK_TYPE_CELLCONNECTION:
        {
            task_time = cellConnectionTask.taskTime.S64;
            break;
        }
        case E_SCHEDULER_TASK_TYPE_IMAGEUPDATE:
        {
            task_time = imageUpdateTask.taskTime.S64;
            break;
        }
        case E_SCHEDULER_TASK_TYPE_DATALOG:
        {
            task_time = datalogTask.taskTime.S64;
            break;
        }
        case E_SCHEDULER_TASK_TYPE_MODEMFOTA:
        {
            task_time = modemFotaTask.taskTime.S64;
            break;
        }
        default:
        {
            /** Invalid task type, so set tast time to maximum value. */
            task_time = RTC_MAX_TIME;
            break;
        }
    }

    return task_time;
}

/**************************************************************************//**
 * @brief
 *      This function will reschedule tasks based on a time change 
 *
 * @param[in] NEPTUNE_TIME_T indicates time change, which will now need
 *          to be accounted for by scheduled tasks.
 *
 *      David Hamilton and Troy Harstad decided that any time change 
 *      < 30 seconds would not need to be rescheduled.  If >= 30 seconds each
 *      task is rescheduled to the nearest next task interval (wall clock aligned).
 *      For instance if the it is 1:15:00 and the time changes to 1:16:00, a
 *      task that occurs once per hour would be rescheduled to 2:00:00, which 
 *      should have been when it was scheduled prior to the time change.
 *      If it is 1:15:00 and the time changes to 2:15:00 the task is rescheduled
 *      to 3:00:00, the next interval (wall clock aligned).
 *
 * @return None
 *****************************************************************************/
void app_scheduler_timeChange(U_NEPTUNE_TIME * timeChange)
{                
    // Only reschedule tasks if the time changed more than +/-30 seconds.
    if( (timeChange->S64 > 30) || (timeChange->S64 < -30))
    {     
        app_scheduler_rescheduleAll();
    }
    else
    {
        // Time change < +/- 30 seconds so do nothing
        DEBUG_OUTPUT_TEXT("Time Change < +/- 30 seconds, Nothing Rescheduled  \r\n"); 
    }
    
}

/**************************************************************************//**
 * @brief
 *   This forces all tasks to be rescheduled.
 * @note
 *   The logic here was originally in app_scheduler_timeChange(), but was pulled
 *   so that it could be accessed directly.
 *
 * @param[in] none
 *
 * @return None
 *****************************************************************************/
void app_scheduler_rescheduleAll(void)
{
    if(!registerReadTask.taskDue)
    {
        registerReadTask.taskTime  = (app_scheduler_findNextInterval(uMIUConfigNormal.sMIUConfigNormal.arbReadIntervalSecs));
        DebugScheduleTime("Register Reading Rescheduled for: ",&registerReadTask);
    }

    if(!detailedConfigPacketTask.taskDue)
    {
        detailedConfigPacketTask.taskTime = (app_scheduler_findNextInterval(uMIUConfigNormal.sMIUConfigNormal.detailedConfigPacketIntervalSecs));
        DebugScheduleTime("Detailed Config Packet Rescheduled for: ",&detailedConfigPacketTask);
    }

    if(!intervalPacketTask.taskDue)
    {
        intervalPacketTask.taskTime = (app_scheduler_findNextInterval(uMIUConfigNormal.sMIUConfigNormal.intervalDataPacketIntervalSecs));
        DebugScheduleTime("Interval Data Packet Rescheduled for: ",&intervalPacketTask);
    }

    if(!cellConnectionTask.taskDue)
    {
        cellConnectionTask.taskTime = app_scheduler_scheduleTask_cellConnection();
        DebugScheduleTime("Cell Connection Rescheduled for: ",&cellConnectionTask);
    }

    if(!imageUpdateTask.taskDue)
    {
        // Image updates are not performed at any wall-clock aligned interval so simply shift due time
        // 7/29/2015- This won't work in the case that the comamnd is received prior to the time being
        // received from the cellular network.  So disabling this time shift for now !!!
        //imageUpdateTask.taskTime.S64 += timeChange->S64;
        DebugScheduleTime("Not Rescheduling Image Update, still scheduled for: ",&imageUpdateTask);
    }

    if(!datalogTask.taskDue)
    {
        datalogTask.taskTime = (app_scheduler_findNextInterval(uMIUConfigNormal.sMIUConfigNormal.datalogIntervalSecs));
        DebugScheduleTime("Datalog Rescheduled for: ",&datalogTask);
    }

    if(!modemFotaTask.taskDue)
    {
        // Modem FOTA is not performed at any wall-clock aligned interval.
        // TREATING THIS SAME AS IMAGE UPDATE TASK ABOVE
        DebugScheduleTime("Not Rescheduling Modem FOTA, still scheduled for: ",&modemFotaTask);
    }
}



/**************************************************************************//**
 * @brief
 *      This function determines the next time that is wholely divisible a
 *      given number of seconds 
 *
 * @detail
 *      This is to be used when trying to align certain tasks with the clock
 *      For instance if you want to read a register at the top of the hour, 
 *      quarter past, half past, quarter til you would pass in 900 (seconds)
 *      and the next time divisible by 900 seconds is returned.
 *
 *
 * @param[in] timeAlignmentSeconds - the number of seconds to divide by
 *
 * @return NEPTUNE_TIME_T - next time that is divisible by timeAlignmentSeconds
 *****************************************************************************/
static U_NEPTUNE_TIME app_scheduler_findNextInterval(uint32_t timeAlignmentSeconds)
{
    uint64_t remainder;
    U_NEPTUNE_TIME tempTime;
    
    // Get current time
    cmiuTimeTemp = app_rtc_time_get(); 
    
    // Take modulo of timeAlignmentSeconds
    remainder = cmiuTimeTemp.S64 % timeAlignmentSeconds;      
    
    // Subtract that from current time
    tempTime.S64 = cmiuTimeTemp.S64 - remainder;
   
    // Add time alignment seconds back to reach next aligned interval time
    tempTime.S64 += timeAlignmentSeconds;
    
    return tempTime;       
}



static void DebugScheduleTime(const char* str, const S_TASK_SCHEDULER* pTask)
{
    char buf[30];   
    
    app_rtc_FormatTime(pTask->taskTime, buf, sizeof(buf));
    DebugTrace("% s : %s\r\n", str, buf);
}

    
    
    
