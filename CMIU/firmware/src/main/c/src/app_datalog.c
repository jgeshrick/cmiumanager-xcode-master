/******************************************************************************
*******************************************************************************
**
**         Filename: app_datalog.c
**
**           Author: Brian Arnberg
**          Created: 2015.10.21
**
**     Last Edit By:
**        Last Edit:
**
** Revision History:
**       2015.10.26: First created (Brian Arnberg)
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file bl_ivu.c
*
* @brief
*  This module is used to manage the storage and retrieval of datalog
*  records.
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdio.h>
#include "typedefs.h"
#include "em_msc.h"
#include "app_datalog.h"
#include "CmiuAppConfiguration.h"
#include "MemoryMap.h"
#include "cmiuFlash.h"
#include "app_timers.h"
#include "app_uart.h"
#include "em_usart.h"
#include "efm32lg_uart.h"
#include "app_cmit_interface.h"
#include "app_rtc.h" //needed for rmu, below.
#include "app_rmu.h"
#include "app_scratch_ram.h"

/*=========================================================================*/
/*  T Y P E D E F S                                                        */
/*=========================================================================*/

/*=========================================================================*/
/*  L O C A L   G L O B A L   V A R I A B L E S                            */
/*=========================================================================*/
/** Datalog variables.
 *  This variable keeps up with the current record, the address where it
 *  will be stored, the address of the last record sent to the server,
 *  and the number of logs that have yet to be sent.*/
static sDatalog_t sDatalog DATALOG_VAR;
/** This is the address of the last record the packet handler tried to send
 *  to the server. */
static uint32_t pendingLogAddress;
/** This is the pending number of missed logs. After the packet handler
 *  confirms successful transmission to the datalogger, this number will
 *  be copied into sDatalog.missedLogs.
 *  */
static uint32_t pendingMissedLogs;
/** Have any fatal errors occured in the datalog. If so, the datalog is
 *  potentially useless. */
static uint8_t byDatalogFatalErrorFlag;
/** This is used to determine whether to enable/disable USART interrupts. */
static uint32_t usartInterruptState;
/** This is the maximum number of datalogs we try to return during normal
    operation. */
static uint32_t maximum_datalogs;

/*=========================================================================*/
/*  P R I V A T E   F U N C T I O N S   P R O T O T Y P E S                */
/*=========================================================================*/
static bool app_datalog_startDatalogFlash(void);
static void app_datalog_stopDatalogFlash(void);
static bool app_datalog_fetchRecords(uint8_t * const pBuffer,
                              const uint32_t address,
                              const uint32_t range);
static bool app_datalog_validateVariables(void);
static bool app_datalog_zeroReadings(void);
static bool app_datalog_findRecordAddress(void);
static bool app_datalog_findAvailableSubSector(uint32_t * const pZeroAddress,
                                                uint8_t * const pBuffer);
static bool app_datalog_findFirstFreeRecord(uint32_t * const pAddress,
                                             uint8_t * const pBuffer);
static uint32_t app_datalog_calculateAddress(uint32_t records);
static void app_datalog_determineMaxLogs(void);

/*=========================================================================*/
/*  P U B L I C   F U N C T I O N S                                        */
/*=========================================================================*/
/***************************************************************************//**
 * @brief
 *  Initialize the variables used by the datalog
 *
 * @note
 *  This function scans the datalog to find the next available address for
 *  record storage. If it can find a valid address, it then tries to validate
 *  the other variables for self consistency. If it finds an address, but can't
 *  validate the other variables, it sets the variables to defaults. If it can't
 *  find an address, it ZERO's the datalog and set's variables to default
 *  starting values. IF the reset was a POR, it clears the backlog and stores
 *  a POR_INDICATOR in the current record slot. The final status of this
 *  initialization function will be stored as the datalog fatal error flag.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void    app_datalog_init(void)
{
    bool status;
    uint32_t resetCause;

    /** Get the reset cause for later use */
    resetCause = app_rmu_resetCause_Get();

    /** Calculate the maximum number of logs the datalog will ever try
        to return. */
    app_datalog_determineMaxLogs();

    /** First scan the datalog to find the record address */
    status = app_datalog_findRecordAddress();

    /** If we found an address, let's make sure the variables are self
     *  consistent. Otherwise, we'll just start a clean-slate */
    if (status)
    {
        if(!app_datalog_validateVariables())
        {
            sDatalog.logAddress = sDatalog.recordAddress;
            sDatalog.missedLogs = 0u;
            sDatalog.currentRecord = UNKNOWN_RECORD_ERROR;
        }
    }
    else
    {
        /** Try to zero the readings */
        status = app_datalog_zeroReadings();

        DEBUG_OUTPUT_TEXT (" ----> Reseting Datalog Flash\r\n");

        /** Set variables to default values */
        sDatalog.recordAddress = DATALOG_START;
        sDatalog.logAddress = sDatalog.recordAddress;
        sDatalog.missedLogs = 0u;
        sDatalog.currentRecord = UNKNOWN_RECORD_ERROR;
    }

    if (status)
    {
        /** If the reset was a POR we should clear the backlog and set
         * the POR indicator. */
        if (resetCause == RMU_RSTCAUSE_PORST)
        {
            /** Clear backlog and set current record to POR Indicator */
            sDatalog.logAddress = sDatalog.recordAddress;
            sDatalog.missedLogs = 0u;
            sDatalog.currentRecord = POR_INDICATOR_RECORD;

            /** Try to store the record. */
            status = app_datalog_storeCurrentRecord();
        }
    }

    byDatalogFatalErrorFlag = (status) ? false : true;
}

/***************************************************************************//**
 * @brief
 *  Re-initialize the datalog. Something has happened that requires some of the
 *  datalog variables to be re-calculated. After we update these variables,
 *  we'll go ahead and store a passed indicator value as a datalog record.
 *
 * @note
 *  This will at least be triggered after the 3 R's update, but it may be
 *  triggered by something else in the future.
 *
 * @param[in]
 *  special_record - the record indicates why the datalog was re-initialized.
 *
 * @return
 *  bool - true for success; false for failure.
 ******************************************************************************/
bool app_datalog_re_init(uint32_t special_record)
{
    bool status;

    /** Re-calculate the maximum datalogs. */
    app_datalog_determineMaxLogs();

    /** Set the current record based on what was passed. */
    status = app_datalog_setCurrentRecord(special_record);

    /** Store the current record. */
    if (status)
    {
        status = app_datalog_storeCurrentRecord();
    }

    return status;
}

/***************************************************************************//**
 * @brief
 *  Set the current datalog record. This is what will need to be explicitely
 *  put in the datalog storage location by a separate routine. The two routines
 *  are separate to ease timing.
 *
 * @note
 *
 * @param[in]
 *  newRecord - whatever value should be pushed to datalog storage next.
 *
 * @return
 *  bool - true for success; false for failure.
 ******************************************************************************/
bool app_datalog_setCurrentRecord(uint32_t newRecord)
{
    sDatalog.currentRecord = newRecord;

    return true;
}

/***************************************************************************//**
 * @brief
 *  Store the current datalog value into memory.
 *
 * @note
 *  This function stores the datalog record to the external flash memory. First
 *  it determines whether the next record address is the start of a new sub-
 *  sector. If it is, it goes ahead and erases the next sub-sector. If that
 *  operation is successful, it then tries to write the record into the
 *  datalog. If writing to the datalog is successful, it updates the datalog
 *  variables.
 *
 * @param[in]
 *
 * @return
 *  bool - true for success; false for failure.
 ******************************************************************************/
bool app_datalog_storeCurrentRecord(void)
{
    bool status;
    uint32_t nextRecordAddress;

    /** Determine the record address for after we store the current record. */
    nextRecordAddress = sDatalog.recordAddress + DATALOG_RECORD_SIZE;
    if ((DATALOG_LAST_RECORD) < nextRecordAddress)
    {
        nextRecordAddress = DATALOG_START;
    }

    status = app_datalog_startDatalogFlash();

    /** If the next record is the start of a new subSector, erase the
     *  new subsector before we write the current record. */
    if ((nextRecordAddress & (EXTERNAL_SUBSECTOR_SIZE-1)) == 0x00000000u)
    {
        status = Flash_SubSectorErase(nextRecordAddress);
        if (status)
        {
            uint32_t address = nextRecordAddress;
            uint32_t end_address = address + EXTERNAL_SUBSECTOR_SIZE;
            uint16_t length = 256;
            uint8_t * buffer = app_scratch_ram_get_256();

            while (status && (address < end_address))
            {
                uint32_t i;

                (void) Flash_ReadFromAddress (buffer, address, length);

                for ( i=0; i<length; ++i)
                {
                    if (0xFFu != buffer[i])
                    {
                        /* The erase failed if there is a mismatch. */
                        status = false;
                        i = length;
                    }
                }

                address += length;
            }
        }
    }

    if (status)
    {
        status = Flash_WriteToAddress((uint8_t *)&sDatalog.currentRecord,
                                      sDatalog.recordAddress,
                                      DATALOG_RECORD_SIZE);
    }

    /** Update the record address if everything else was a success. */
    if (status)
    {
        sDatalog.recordAddress += DATALOG_RECORD_SIZE;
        if (sDatalog.recordAddress > DATALOG_END)
        {
            sDatalog.recordAddress = DATALOG_START;
        }

        sDatalog.missedLogs += 1u;
        if (sDatalog.missedLogs >= maximum_datalogs)
        {
            sDatalog.missedLogs = maximum_datalogs;
        }
    }

    app_datalog_stopDatalogFlash();

    return status;
}

/***************************************************************************//**
 * @brief
 *  Get the backlog (datalog records that haven't been beamed to the server).
 *
 * @note
 *  This function depends on the local globals. It uses the missedLogs variable
 *  to determine the range of records that needs to be fetched. If the range
 *  that needs to be fetched is larger than the limit, it will only fetch as
 *  many records as will fit in the rangeLimit. Also, this function will
 *  return the oldest records from the backlog. So, if there are 10K records
 *  pending, and we've only got room for 500, it will return the 500 oldest
 *  records. Also, it will return those in the order they entered the datalog,
 *  so the oldest record will be the zeroth, the next oldest the first, etc.
 *
 * @param[in, out]
 *  paBacklog: pointer to an array of uint32_t records.
 * @param[in]
 *  rangeLimit: largest range this is allowed to fetch. This should be evenly
 *  divisible by 4 (DATALOG_RECORD_SIZE).
 *
 * @return
 *  recordsFetched: number of records fetched from backlog.
 ******************************************************************************/
uint32_t app_datalog_getBackLog(uint32_t * const paBackLog,
                                const uint32_t rangeLimit)
{
    uint32_t range;
    uint32_t recordsFetched;
    bool status;

    ASSERT(NULL != paBackLog);

    /** Variable setup */
    recordsFetched = 0u;
    range = (sDatalog.missedLogs * DATALOG_RECORD_SIZE);

    /** Make sure range is less than rangeLimit. */
    if (range > rangeLimit)
    {
        range = rangeLimit;
    }

    /** Make sure range is an even number of datalog records. */
    if (0u != (range % DATALOG_RECORD_SIZE))
    {
        range = range - (range % DATALOG_RECORD_SIZE);
    }

    /** Try to get the records from flash. */
    status = app_datalog_fetchRecords((uint8_t *)paBackLog,
                                      sDatalog.logAddress,
                                      range);

    /** Update the log address if the read was successful AND something
     *  was actually fetched (range greater than 0). */
    if ((status) && (range > 0))
    {
        recordsFetched = (range / DATALOG_RECORD_SIZE);

        pendingMissedLogs = sDatalog.missedLogs - recordsFetched;
        if (pendingMissedLogs > (DATALOG_TOTAL_RECORDS))
        {
            pendingMissedLogs = 0u;
        }

        pendingLogAddress = app_datalog_calculateAddress(pendingMissedLogs);
    }

    return (recordsFetched);
}

/***************************************************************************//**
 * @brief
 *  Update the log variables based on whether the message was successfully sent.
 *
 * @note
 *  This function depends on the local globals. This updates the variables
 *  associated with the backlog based on whether the message was successfully
 *  sent to the server. If the message was successfully sent, the backlog
 *  variables are updated, then verfied. If not, then the pending values
 *  are reverted to the old values.
 *
 * @param[in]
 *  messageSent: was the message update successful?
 *
 * @return
 *  status: true, successful; false, failed somewhere.
 ******************************************************************************/
bool app_datalog_cleanBackLog(bool messageSent)
{
    bool status;

    if (messageSent)
    {
        sDatalog.missedLogs = pendingMissedLogs;
        sDatalog.logAddress = app_datalog_calculateAddress(sDatalog.missedLogs);
        status = true;
    }
    else
    {
        pendingLogAddress = sDatalog.logAddress;
        pendingMissedLogs = sDatalog.missedLogs;
        status = true;
    }

    return status;
}

/***************************************************************************//**
 * @brief
 *  Get a range of records from the datalog and store them into the passed
 *  array.
 *
 * @note
 *  This function takes a record number and number of records and converts
 *  them to a starting address and address range, which is what the
 *  fetch records function uses. Really, this just grants public access
 *  to app_datalog_fetchRecords, which makes sure to handle the circular
 *  buffer nature of the datalog.
 *
 * @param[in]
 *  paRecord: pointer to an array of uint32_t records.
 * @param[in]
 *  startRecord: first record desired in the range.
 * @param[in]
 *  totalRecords: number of records desired.
 *
 * @return
 *  status: true, everything was successful; false, something failed somewhere.
 ******************************************************************************/
bool app_datalog_getRecordRange(uint32_t * const paRecord,
                                uint32_t startRecord,
                                uint32_t totalRecords)
{
    bool status;
    uint32_t address;
    uint32_t length;

    ASSERT(NULL != paRecord);
    ASSERT(totalRecords <= (DATALOG_LENGTH/DATALOG_RECORD_SIZE));

    address = DATALOG_START + (startRecord * DATALOG_RECORD_SIZE);
    length  = totalRecords * DATALOG_RECORD_SIZE;

    status  = app_datalog_fetchRecords((uint8_t *)paRecord, address, length);

    return status;
}

/***************************************************************************//**
 * @brief
 *  Take an array of records and reverse their order.
 *
 * @note
 *  This function is needed because tag28 expects the records to be in reverse
 *  chronological order (most recent record first), which is opposite to how
 *  they are stored.
 *
 * @param[in]
 *  paRecord: pointer to an array of uint32_t records.
 * @param[in]
 *  length: number of records in the array (not number of bytes).
 *
 * @return
 *  None
 ******************************************************************************/
void app_datalog_reverseArray(uint32_t * const paRecord, const uint32_t length)
{
    uint32_t top;
    uint32_t bottom;
    uint32_t tempRecord;

    ASSERT(NULL != paRecord);
    ASSERT(0 != length);

    for (top = 0, bottom = length-1; top < bottom; ++top, --bottom)
    {
        tempRecord = paRecord[top];
        paRecord[top] = paRecord[bottom];
        paRecord[bottom] = tempRecord;
    }
}


/***************************************************************************//**
 * @brief
 *  Return a reading record in the format that the datalog prefers.
 *
 * @note
 *  This function is adapted from R900v4A/Packet.c. It formats the ARB reading
 *  and flags into a single uint32_t that can be stored in the datalog. This
 *  preserves the flags and the reading together. From the tag documentation:
 *  ``First Uint8 = 3 MSB bits of reading,  4 Flags  and one spare. The spare
 *  and 4 Flags are LSBs of Uint8. Spare is MSB of the flags, followed by 2
 *  bits of Peak Backflow with the least 2 bits used for Leak Detection.
 *  Second Uint8 = Most significant Uint8 of reading value
 *  Third Uint8 = Middle Uint8 of reading value
 *  Fourth Uint8 = Least significant Uint8 of reading value
 *  ''
 *
 * @param[in]
 *  arbReading: Raw reading value from ARB.
 * @param[in]
 *  backflowFlag: The backflow flags from metrology. We only care about BIT0
 *  and BIT1.
 * @param[in]
 *  leakFlag: Leak detection flags from metrology. We only care about BIT0
 *  and BIT1.
 *
 * @return
 *  formattedReading = The formatted datalog reading.
 ******************************************************************************/
uint32_t app_datalog_formatRecord(uint32_t arbReading,
                                   uint8_t backflowFlag,
                                   uint8_t leakFlag)
{
    uint8_t abyFormattedReading[4];
    uint8_t tempTopByte;
    uint32_t formattedReading;

    abyFormattedReading[1] = ((arbReading & 0x00FF0000u) >> 16u);
    abyFormattedReading[2] = ((arbReading & 0x0000FF00u) >> 8u);
    abyFormattedReading[3] = (arbReading & 0x000000FFu);

    //First 3MSB are 3MSB of reading.
    tempTopByte  = ((arbReading & 0x07000000u) >> 24u);
    tempTopByte  = ((tempTopByte << 5) & 0xE0);

    //Bits 3 and 2 are the backflow flags.
    tempTopByte |= ((backflowFlag & 0x03) << 2);

    //Bits 1 and 0 are the leak flags.
    tempTopByte |= (leakFlag & 0x03);

    //Bit 4 is the spare, set last for sanity check.
    tempTopByte &= ~(0x10);


    abyFormattedReading[0] = tempTopByte;

    formattedReading = *(uint32_t *)abyFormattedReading;
    return formattedReading;
}

/***************************************************************************//**
 * @brief
 *  Get the current record from the datalog module.
 *
 * @note
 *  This refers to the most recent reading and has not necessarily been stored
 *  in the datalog storage location yet.
 *
 * @param[in]
 *
 * @return
 *  The value of the current record as a uint32_t.
 ******************************************************************************/
uint32_t app_datalog_getCurrentRecord(void)
{
    return sDatalog.currentRecord;
}

/***************************************************************************//**
 * @brief
 *  Get the address where the current record will be stored.
 *
 * @note
 *
 * @param[in]
 *
 * @return
 *  The address is a uint32_t.
 ******************************************************************************/
uint32_t app_datalog_getRecordAddress(void)
{
    return sDatalog.recordAddress;
}

/***************************************************************************//**
 * @brief
 *  Get the address of the last record sent to the server.
 *
 * @note
 *
 * @param[in]
 *
 * @return
 *  The address is a uint32_t.
 ******************************************************************************/
uint32_t app_datalog_getLogAddress(void)
{
    return sDatalog.logAddress;
}

/***************************************************************************//**
 * @brief
 *  Get the number of records that haven't yet been logged to the server.
 *
 * @note
 *
 * @param[in]
 *
 * @return
 *  The number is a uint32_t.
 ******************************************************************************/
uint32_t app_datalog_getMissedLogs(void)
{
    return sDatalog.missedLogs;
}


/***************************************************************************//**
 * @brief
 *  Get the datalog fatal flag status.
 *
 * @note
 *
 * @param[in]
 *
 * @return
 *  The flag status is bool. True means a fatal flag has occured.
 ******************************************************************************/
bool     app_datalog_getFatalFlag(void)
{
    return byDatalogFatalErrorFlag;
}

/***************************************************************************//**
 * @brief
 *  Get the timedate associated with the newest pending record.
 *
 * @note
 *  The latest record waiting to hit the server can be determined by subtracting
 *  a certain number of datalog intervals from the time of the next recording.
 *  For instance, if I have zero pending logs, that means that the latest log
 *  in the packet is the previous log, so I can just subtract the datalog interval
 *  from the next packet (next_time - 900). If I have multiple logs pending,
 *  then I need to multiply the pending number+1 by the datalog interval.
 *
 * @param[in]
 *
 * @return
 *  The timedate is in unix format, so uint64_t.
 ******************************************************************************/
uint64_t app_datalog_getLatestRecordTime(uint32_t interval, uint64_t next_time)
{
    static uint64_t lag_time;
    static uint64_t latest_time;

    /** The lag time is at least one datalog interval. */
    lag_time = (uint64_t)interval;

    /** Multiply the lag time by the number of pending missed logs + 1. */
    if (0u != pendingMissedLogs)
    {
        lag_time *= (pendingMissedLogs + 1);
    }

    /** The most recent datalog record in the pending packet is this. */
    latest_time = next_time - lag_time;

    return latest_time;
}


/*=========================================================================*/
/*  P R I V A T E   F U N C T I O N S                                      */
/*=========================================================================*/
/***************************************************************************//**
 * @brief
 *  Start the flash chip that stores the datalog.
 *
 * @note
 *
 * @param[in]
 *
 * @return bool - true if successful; false otherwise
 ******************************************************************************/
static bool app_datalog_startDatalogFlash(void)
{
    bool retval;

    /** Initialize the communications with the flash chip. */
    retval = Flash_init();

    /** Disable USART interrupts during sensitive communications. */
    usartInterruptState = USART_IntGetEnabled(USART1);
    if (UART_IEN_TXBL == (usartInterruptState & UART_IEN_TXBL))
    {
        USART_IntDisable(USART1, UART_IEN_TXBL);
    }

    return retval;
}

/***************************************************************************//**
 * @brief
 *  Stop the flash chip that stores the datalog.
 *
 * @note
 *
 * @param[in]
 *
 * @return
 ******************************************************************************/
static void app_datalog_stopDatalogFlash(void)
{
    /** Re-enable the USART interrupts that were disabled earlier. */
    if (UART_IEN_TXBL == (usartInterruptState & UART_IEN_TXBL))
    {
        USART_IntEnable(USART1, UART_IEN_TXBL);
    }

    /** De-init comms with the flash chip. */
    Flash_Deinit();
}

/***************************************************************************//**
 * @brief
 *  Get a range of datalog values from flash storage.
 *
 * @note
 *  This function takes a pointer to a storage buffer, source address and range.
 *  Based on the starting address and range, it determines the ending address
 *  of the range. If the ending address is outside of the legal boundary,
 *  the function assumes that this is a role-over type request and breaks the
 *  reading up into two different calls. The first call will try to read from
 *  the upper portion of the datalog (the addresses near the end). If that is
 *  successful, a second call will try to read from the lower address range.
 *  The values are loaded into the buffer as if they were a single range.
 *  This function depends on the cmiuFlash.c/h module in common code.
 *
 * @param[in]
 *  buffer: pointer to array of bytes for storing the fetched records
 * @param[in]
 *  address: starting address of the records to fetch. Should be the start
 *  of a record.
 * @param[in]
 *  range: range (number of bytes) to read out of the datalog. Should be
 *  mod 4 0.
 *
 * @return
 *  status: true, valid parameters and read success; false, failed somehow.
 ******************************************************************************/
bool app_datalog_fetchRecords(uint8_t * const pBuffer,
                              const uint32_t address,
                              const uint32_t range)
{
    uint32_t upperRange; /** Range at end of datalog */
    uint32_t lowerRange; /** Range at beginning of datalog */
    uint32_t endAddress;
    bool status;

    ASSERT(NULL != pBuffer);
    ASSERT(0u == (address % DATALOG_RECORD_SIZE));
    ASSERT(0u == (range % DATALOG_RECORD_SIZE));

    endAddress = address + range;

    app_datalog_startDatalogFlash();

    /** Try to read the address range from external ram into the buffer. Do this
     *  in one or two steps, depending on where the range is located. */
    if (endAddress <= DATALOG_END)
    {
        status = Flash_ReadFromAddress(pBuffer, address, range);
    }
    else
    {
        upperRange = (DATALOG_END + 1) - address;
        lowerRange = range - upperRange;

        /** Read upper range */
        status = Flash_ReadFromAddress(pBuffer, address, upperRange);

        if (status)
        {
            /** Make sure flash isn't busy. */
            status = Flash_WaitOnWip(2);
        }

        /** Read lower range */
        if (status)
        {
            status = Flash_ReadFromAddress((pBuffer+upperRange),
                                           (uint32_t)DATALOG_START,
                                           lowerRange);
        }
    }

    app_datalog_stopDatalogFlash();

    return status;
}

/***************************************************************************//**
 * @brief
 *  Validate that the datalog variables are within reasonable limits.
 *
 * @note
 *  This function first tries to validate the recordAddress. If this is valid,
 *  it goes on to check the missedLogs value. Based on that, it determines
 *  what the logAddress should be. If that doesn't match the logAddress as
 *  it is, we assume the values are corrupt and return failure.
 *
 * @param[in]
 *  None.
 *
 * @return
 *  status: true, all good; false, one of the variables is bad.
 ******************************************************************************/
static bool app_datalog_validateVariables(void)
{
    bool status;
    uint32_t expectedLogAddress;

    status = true;

    if ((sDatalog.recordAddress > DATALOG_END) ||
        (sDatalog.recordAddress < DATALOG_START))
    {
        status = false;
    }
    else
    {
        if (sDatalog.missedLogs > (DATALOG_TOTAL_RECORDS))
        {
            //If this happens, we just need to start over and cry!
            sDatalog.missedLogs = 0u;
        }

        expectedLogAddress = app_datalog_calculateAddress(sDatalog.missedLogs);

        if (expectedLogAddress != sDatalog.logAddress)
        {
            status = false;
        }
    }

    return status;
}

/***************************************************************************//**
 * @brief
 *  Zero all the readings in the datalog then erase the first sub-sector.
 *
 * @note
 *  First, setup a buffer with all zeros in it. Then, write all zeros to each
 *  page in the datalog. IFF that was successful, erase the first sub-sector
 *  in the datalog to prepare for writing. Also, because of the way the flash
 *  storage works, we can always set a flash page to all zeros without erasing
 *  said page.
 *
 * @param[in]
 *
 * @return
 *  status: true, all sectors were set to 0; false, something went wrong
 ******************************************************************************/
static bool app_datalog_zeroReadings(void)
{
    bool status;
    uint16_t i;
    uint32_t address;
    uint8_t retries;
    uint8_t buffer[EXTERNAL_PAGE_SIZE];

    app_datalog_startDatalogFlash();

    for (i = 0; i < EXTERNAL_PAGE_SIZE; ++i)
    {
        buffer[i] = 0u;
    }

    /** Write zeroes to each address in the datalog. */
    for (address = DATALOG_START;
         address < DATALOG_END;
         address += EXTERNAL_PAGE_SIZE)
    {
        status = Flash_WriteToAddress(buffer, address, EXTERNAL_PAGE_SIZE);
        if (!(status))
        {
            address -= EXTERNAL_PAGE_SIZE;
            retries += 1;
        }
        else
        {
            retries = 0;
        }
        if (retries > 2)
        {
            address = DATALOG_END;
            status = false;
        }
    }

    /** Erase the first sub sector IFF the previous steps succeeded. */
    if (status)
    {
        status = Flash_SubSectorErase(DATALOG_START);
    }

    app_datalog_stopDatalogFlash();

    return status;
}


/***************************************************************************//**
 * @brief
 *  Find the next record available for writing in the datalog.
 *
 * @note
 *  First, this function checks the ending record in each sub-sector for erased
 *  records. IF it finds exactly one sub-sector with an erased record, it
 *  searches throught that sub-sector to find the first free record, which is
 *  the first all FFs record. If both of these steps are successful, it sets
 *  the datalog record address to the address it found. If not, it sets the
 *  address to the start of the datalog and returns a failure indicator.
 *
 * @param[in]
 *
 * @return
 *  status: true, record found; false, record not found.
 ******************************************************************************/
static bool app_datalog_findRecordAddress(void)
{
    bool status;
    static uint32_t address;
    uint8_t buffer[EXTERNAL_PAGE_SIZE];

    app_datalog_startDatalogFlash();

    /** Find the available subsector. If the search is successful, this will
     *  set address to the start of the first available subsector.
     *  */
    status = app_datalog_findAvailableSubSector(&address, buffer);

    /** If the status of the last command is good and the address it found
     *  is valid, then try to find the first free record in that subsector.
     *  If one of these conditions fails, we'll fall through to the next
     *  check.
     *  */
    if ((status) && (address >= DATALOG_START) && (address < DATALOG_END))
    {
        /** Now, find the first available record in the sub-sector */
        status = app_datalog_findFirstFreeRecord(&address, buffer);
    }

    /** If the previous searches where successful and the address they
     *  found is valid, set the record address to the address found.
     *  Otherwise, we'll set the return status to false (just to be safe)
     *  and set the record address to the start of the datalog.
     *  */
    if ((status) && (address >= DATALOG_START) && (address < DATALOG_END))
    {
        sDatalog.recordAddress = address;
    }
    else
    {
        status = false;
        sDatalog.recordAddress = DATALOG_START;
    }

    app_datalog_stopDatalogFlash();

    return status;
}

/***************************************************************************//**
 * @brief
 *  Find the sub-sector with the next available record slot.
 *
 * @note
 *  This function reads the last record in each sub-sector and checks whether
 *  the sector is erased (== 0xFFFFFFFF). If the sector is erased, it sets
 *  the address of the sub-sector as the zeroAddress and increments an
 *  internal variable, found. If the algorithm finds exactly one available
 *  sub-sector, it returns true and sets the address to the start of the
 *  sub-sector. If the search algorithm finds more than one availabe
 *  sub-sector, it returns a failure and sets the zeroAddress to a bad value.
 *  If the search doesn't find any available sub-sectors, it returns a false
 *  and sets the address to an invalid value.
 *
 * @param[in, out]
 *  pZeroAddress: pointer to an address. If this function is successful, the
 *  address will be set to the starting address of the sub-sector with the next
 *  available record slot.  Otherwise, it will be an out-of-bounds address.
 *
 * @param[in, out]
 *  pBuffer: pointer to an array that this function can work with to store
 *  readings from the datalog storage.
 *
 * @return
 *  status - true if a single available sub-sector was found, false otherwise.
 ******************************************************************************/
static bool app_datalog_findAvailableSubSector(uint32_t * const pZeroAddress,
                                               uint8_t * const pBuffer)
{
    bool status;
    uint32_t address;
    uint32_t found;
    uint8_t retries;

    ASSERT(NULL != pZeroAddress);
    ASSERT(NULL != pBuffer);

    /** Explicitly initialize variables. */
    retries = 0;
    address = (DATALOG_START+EXTERNAL_SUBSECTOR_SIZE-DATALOG_RECORD_SIZE);
    found = 0;

    /** If flash isn't busy, then search thru the datalog and check the
     *  last record of each sub-sector. Whenever a record is erased,
     *  set *pZeroAddress to that record's address and increment found.
     *  Exit the loop when we've reached the end or we've found more than
     *  one erased record.
     *  */
    while ((address < DATALOG_END) && (2 > found))
    {
        /** Make sure flash isn't busy. */
        status = Flash_WaitOnWip(10);

        if (status)
        {
            status = Flash_ReadFromAddress(pBuffer, address,
                                      (DATALOG_RECORD_SIZE));
        }

        /** Set the retries variable based on the value of status */
        retries = ((status) ? 0 : (retries + 1));

        /** If the read was successful, check the record. */
        if (status)
        {
            if (*(uint32_t *)pBuffer == 0xFFFFFFFFu)
            {
                /** This is the address of the erased record in this
                 *  sub-sector. */
                *pZeroAddress = address;
                ++found;
            }
            address += EXTERNAL_SUBSECTOR_SIZE;
        }
        else
        {
            /** Try to read each record only twice. If I fail to
             *  read more than twice, then I will end this loop
             *  and status will fallthrough as false.
             *  */
            if (2 < retries)
            {
                address = DATALOG_END;
            }
        }
    }

    /** If I have found exactly one possible match and the read status is set
     *  to true, then set *pZeroAddress to the start of the sub-sector.
     *  Otherwise make sure the *pZeroAddress is set to an invalid value and
     *  the return status is false.
     *  */
    if ((1u == found) && (status))
    {
        *pZeroAddress &= ~(EXTERNAL_SUBSECTOR_SIZE - 1);
    }
    else
    {
        *pZeroAddress = 0xFFFFFFFFu;
        status = false;
    }

    return status;
}

/***************************************************************************//**
 * @brief
 *  Find the first free record in this particular sub-sector.
 *
 * @note
 *  Seach thru the sub-sector of the passed address until we find an erased
 *  record. Read each page out of the sub-sector. After reading the page,
 *  check each record in the page. Stop the search as soon as an erased
 *  record is found.
 *
 * @param[in, out]
 *  *pAddress: Address to the start of the sub-sector that we will search.
 *  At the end of the routine, this should point to the first available
 *  record (assuming success). If there is a failure, the address will be
 *  set to an invalid value.
 *
 * @param[in, out]
 *  pBuffer: pointer to a buffer we can use for storing records we read from
 *  the datalog.
 *
 * @return
 *  status: true if we found a free record, false otherwise.
 ******************************************************************************/
static bool app_datalog_findFirstFreeRecord(uint32_t * const pAddress,
                                            uint8_t * const pBuffer)
{
    bool status;
    bool found;
    uint32_t zeroAddress;
    uint32_t j;
    uint8_t retries;

    ASSERT(NULL != pAddress);
    ASSERT(NULL != pBuffer);

    /** Explicitly initialize variables. */
    zeroAddress = *pAddress;
    retries = 0;
    found = false;

    /** Make sure flash isn't busy. */
    status = Flash_WaitOnWip(10);

    while ((zeroAddress < (*pAddress + EXTERNAL_SUBSECTOR_SIZE)) &&
           (2 > retries) &&
           (!(found)))
    {
        status = Flash_ReadFromAddress(pBuffer,
                                       zeroAddress,
                                       (EXTERNAL_PAGE_SIZE));

        /** Set the retries variable based on the value of status */
        retries = ((status) ? 0 : (retries + 1));

        /** If the read was successful, check each record in the page.
         *  If a record isn't erased, increment zeroAddress by the
         *  record size (4). If the record is erased, exit this
         *  loop and set found to true, which will exit the loop
         *  that contains this loop.
         *  */
        if (status)
        {
            for (j = 0; j < (EXTERNAL_PAGE_SIZE); j+=DATALOG_RECORD_SIZE)
            {

                if (*(uint32_t *)(pBuffer+j) != 0xFFFFFFFF)
                {
                    zeroAddress += (DATALOG_RECORD_SIZE);
                }
                else
                {
                    j = EXTERNAL_PAGE_SIZE;
                    found = true;
                }
            }
        }
    }

    /** If we never found a zero, something went wrong (because this
     *  function should only be called if the last record in the sub-sector
     *  is erased). We're going to set the zeroAddress to that of some
     *  invalid value.
     *  */
    if (!(found))
    {
        zeroAddress = DATALOG_END + EXTERNAL_SUBSECTOR_SIZE;
        status = false;
    }

    *pAddress = zeroAddress;

    return status;
}

/***************************************************************************//**
 * @brief
 *  Calculate an the address of the record that precedes the current record
 *  address by the passed number of records.
 *
 * @note
 *  Take the input number of records and find that address of the record that is
 *  that many records in front of the current record. This is mostly used to
 *  validate the logAddress based on the number of missed logs.
 *
 * @param[in]
 *  records: the number of records prior to the current record address that
 *  we want to use for calculation.
 *
 * @return
 *  The address of the record that is x records in front of the current record.
 ******************************************************************************/
static uint32_t app_datalog_calculateAddress(uint32_t records)
{
    uint32_t calculatedAddress;

    ASSERT(records < (DATALOG_LENGTH/DATALOG_RECORD_SIZE));

    /** The calculated address is the address that is x number of
     *  records in front of the current address. */
    calculatedAddress = sDatalog.recordAddress
                      - (records*4);
    if (calculatedAddress < DATALOG_START)
    {
        calculatedAddress = ((DATALOG_END + 1u)
                            -(DATALOG_START - calculatedAddress));
    }

    /** Make sure the calculated address is on a proper boundary. */
    calculatedAddress &= ~(DATALOG_RECORD_SIZE-1);

    return calculatedAddress;
}

/***************************************************************************//**
 * @brief
 *  Determine maximum logs to return. Per convention, we will only return the
 *  last 96 days.
 *
 * @note
 *  This calculation depends on knowledge of the datalog storage rate.
 *
 * @param[in] None.
 *
 * @return None.
 ******************************************************************************/
static void app_datalog_determineMaxLogs(void)
{
    uint32_t max_logs;
    uint32_t logs_per_day;

    /** If the datalog storage rate is less than once per day, set logs_per_day
     * to 1. Otherwise, calculate the logs per day. */
    if (SECONDS_PER_DAY < uMIUConfigNormal.sMIUConfigNormal.datalogIntervalSecs)
    {
        logs_per_day = 1u;
    }
    else
    {
        logs_per_day = (SECONDS_PER_DAY);
        logs_per_day /= (uMIUConfigNormal.sMIUConfigNormal.datalogIntervalSecs);
    }

    /** Calculate the maximum logs by multiplying the maximum number of days by
     * the number of logs per day. */
    max_logs = MAX_DATALOG_DAYS * logs_per_day;

    /** Make sure max_logs never exceeds the maximum possible logs. */
    if (max_logs > DATALOG_TOTAL_RECORDS)
    {
        max_logs = DATALOG_TOTAL_RECORDS;
    }

    /** Set the local glocal variable, maximum_datalogs, the the value of
     * max_logs. */
    maximum_datalogs = max_logs;
}

