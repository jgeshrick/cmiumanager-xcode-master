/******************************************************************************
*******************************************************************************
**
**         Filename: app_rmu.c
**    
**           Author: Troy Harstad
**          Created: 4/30/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_rmu.c
*
* @brief This file contains the application code used to setup and use the
*        reset management unit (RMU).
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include "typedefs.h"
#include "app_rtc.h"
#include "app_uart.h"
#include "app_rmu.h"
#include "em_rmu.h"
#include "app_cmit_interface.h"
#include "DebugTrace.h"
#include "MemoryMap.h"



/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/

// The reset cause is set by the bootloader
volatile uint32_t resetCause RESET_SOURCE_VAR;
/** Each bit represents a specifc image, 1 indicates a valid CRC */
volatile uint32_t SumCrcStatus CRC_STATUS_VAR;
/** Each bit represents a specifc image, 1 indicates a valid Version */
volatile uint32_t SumVersionStatus VERSION_STATUS_VAR;

static uint8_t resetCounter;
static uint8_t magSwipeCounter;
static U_NEPTUNE_TIME magSwipeTime;


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/

/***************************************************************************//**
 * @brief
 *   Initializes rmu module.  Since the bootloader sets the reset source 
 *   variable (resetCause) the only thing done in this routine is to display it.  
 *
 * @note
 *  
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_rmu_init(void)
{
    uint32_t pwrCause = app_rmu_resetCause_Get();
    
    
    // Only init the reset counter on a power-up reset (POR)
    if((app_rmu_resetCause_Get() & RMU_RSTCAUSE_PORST) == RMU_RSTCAUSE_PORST)
    {        
        resetCounter = 0;     
        magSwipeCounter = 0;
    }
    
    // Increment mag swipe reset counter (external pin reset) and store time
    if((app_rmu_resetCause_Get() & RMU_RSTCAUSE_EXTRST) == RMU_RSTCAUSE_EXTRST)
    {
        // Allow rollover
        magSwipeCounter++;   
        
        // Store current time as latest mag swipe time
        magSwipeTime = app_rtc_time_get();
    }

    // reset counter incremented on every reset, allow rollover
    resetCounter++;
    
    // Clear reset cause
    RMU_ResetCauseClear();
 

    DEBUG_TRACE("Reset Source: 0x%08x\r\n", pwrCause);
}


/***************************************************************************//**
 * @brief
 *   Access function for reset cause, this is set by the bootloader
 * 
 * @param[in] None
 *
 * @return uint32_t resetCause
 ******************************************************************************/
uint32_t app_rmu_resetCause_Get(void)
{    
    return resetCause; 
}

/***************************************************************************//**
 * @brief
 *   Access function for version status of images, this is set by the bootloader
 *  
 * @param[in] None
 *
 * @return uint32_t SumVersionStatus
 ******************************************************************************/
uint32_t app_rmu_SumVersionStatus_Get(void)
{    
    return SumVersionStatus;  
}


/***************************************************************************//**
 * @brief
 *   Access function for CRC status of images, this is set by the bootloader
 *  
 * @param[in] None
 *
 * @return uint32_t SumCrcStatus
 ******************************************************************************/
uint32_t app_rmu_SumCrcStatus_Get(void)
{    
    return SumCrcStatus;  
}


/***************************************************************************//**
 * @brief
 *   Access function for mag swipe reset counter
 *  
 * @param[in] None
 *
 * @return uint8_t magSwipeCounter
 ******************************************************************************/
uint8_t app_rmu_magSwipeCounter_Get(void)
{    
    return magSwipeCounter;  
}



/***************************************************************************//**
 * @brief
 *   Access function for reset counter
 *  
 * @param[in] None
 *
 * @return uint8_t resetCounter
 ******************************************************************************/
uint8_t app_rmu_resetCounter_Get(void)
{    
    return resetCounter;  
}


/***************************************************************************//**
 * @brief
 *   Access function for most recent mag swipe time
 *  
 * @param[in] None
 *
 * @return U_NEPTUNE_TIME magSwipeTime
 ******************************************************************************/
U_NEPTUNE_TIME app_rmu_magSwipeTime_Get(void)
{    
    return magSwipeTime;  
}

