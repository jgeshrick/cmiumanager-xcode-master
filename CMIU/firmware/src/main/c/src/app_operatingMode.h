/******************************************************************************
*******************************************************************************
**
**         Filename: app_operatingMode.h
**    
**           Author: Troy Harstad
**          Created: 4/13/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_OPERATINGMODE_H
#define __APP_OPERATINGMODE_H



/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
typedef enum E_CMIU_OPERATINGMODE_STATE
{
    // "Swipe" operating state
    E_CMIU_OPERATINGMODE_STATE_SWIPE          = 0x00,

    // "Normal" operating state
    E_CMIU_OPERATINGMODE_STATE_NORMAL         = 0x01,

    // Cellular operating state
    E_CMIU_OPERATINGMODE_STATE_CELLULAR       = 0x02 
    
} E_CMIU_OPERATINGMODE_STATE;


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/

// Define the number of msec in a tick for each mode
#define CMIU_OPERATINGMODE_NORMAL_MSEC_IN_TICK     1000
#define CMIU_OPERATINGMODE_CELLULAR_MSEC_IN_TICK   10
#define CMIU_OPERATINGMODE_SWIPE_MSEC_IN_TICK      10

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_operatingMode_init(void);
extern void app_operatingMode_tick(void);

extern void app_operatingMode_swipe_enter(void);
extern void app_operatingMode_normal_enter(void);
extern void app_operatingMode_cellular_enter(void);

extern E_CMIU_OPERATINGMODE_STATE app_operatingMode_state_get(void);



/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/



/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/



/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_OPERATINGMODE_H

