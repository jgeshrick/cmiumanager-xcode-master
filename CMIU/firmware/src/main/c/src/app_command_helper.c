/******************************************************************************
*******************************************************************************
**
**         Filename: app_command_helper.c
**    
**           Author: Troy Harstad
**          Created: 12/15/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/************************************************************************
* @file app_command_helper.c
*
* @brief This file contains helper routines for command handling
* 
************************************************************************/


/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "em_msc.h"
#include "typedefs.h"
#include "TagTypes.h"
#include "app_rtc.h"
#include "DebugTrace.h"
#include "cmiuFlash.h"
#include "CmiuAppConfiguration.h"
#include "app_cmit_interface.h"
#include "app_scratch_ram.h"
#include "Crc32Fast.h"
#include "app_mathFunc.h"
#include "app_command_helper.h"
#include "cmiuSpi.h"
#include "modem_power.h"
#include "app_gpio.h"
#include "app_uart.h"
#include "em_usart.h"
#include "efm32lg_uart.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/

// buffer used to copy an image from internal flash to external flash
#define     COPY_BUFFER_SIZE    256

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static bool app_command_helper_memcmp(uint8_t * p_buf_1, uint8_t * p_buf_2, uint32_t len);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
uint8_t*    pRamBuffer;
uint8_t*    pRamBufferVerify;


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/



/*****************************************************************************
 * @brief
 *   updates the last block of the config image by updating the 
 *    build number and the CRC.
 *
 * @note
 *
 * @param[in] pBuffer - pointer to buffer containing the image info block
 *            blockSize - block size to be written to flash
 *            crcValue - CRC value of the previous blocks
 *
 * @return None
 ******************************************************************************/
void    app_command_helper_UpdateConfigImageInfo (uint8_t     *pBuffer,
                                                  uint16_t     blockSize,
                                                  uint32_t     crcValue)
{
    S_MIU_IMG_INFO    *imageInfoPtr;
    static volatile uint32_t crcResult;

    /** @TODO Decide what to set for the date as the RTC is not yet set */
    /** @ANSWER We don't need to change the date or version number, so we may
     * want to just get rid of this routine as a whole. I've gone ahead and
     * removed the change to version number. */
    imageInfoPtr = (S_MIU_IMG_INFO *)((pBuffer + blockSize) - sizeof(S_MIU_IMG_INFO));
    
    // calculate the CRC over the block but do not include the CRC in the calculation
    crcResult = FastCrc32Word(crcValue, pBuffer, (COPY_BUFFER_SIZE-4));
    // reverse the CRC
    crcResult = ReverseCrc32(crcResult);
    imageInfoPtr->imageCrc32 = crcResult;
    
}


/*****************************************************************************
 * @brief
 *  Based on the interval data provided, determine whether we actually
 *  need a new config image.
 *
 * @note
 *
 * @param[in] p_interval - pointer to the new interval settings
 *
 * @return bool - true means we need a new config image
 ******************************************************************************/
bool app_command_helper_DoWeNeedNewConfigImage(S_RECORDING_REPORTING_INTERVAL const * const p_interval)
{
    bool retval;

    S_RECORDING_REPORTING_INTERVAL old_vals;

    old_vals.reportingStartMins =
        uMIUConfigNormal.sMIUConfigNormal.cellCallInOffsetMins;
    old_vals.reportingIntervalHours =
        (uMIUConfigNormal.sMIUConfigNormal.intervalDataPacketIntervalSecs / 3600);
    old_vals.reportingTransmitWindowsMins =
        (uMIUConfigNormal.sMIUConfigNormal.cellCallInWindowQtrHrs * 15);
    old_vals.recordingIntervalMins =
        (uMIUConfigNormal.sMIUConfigNormal.datalogIntervalSecs / 60);

    /** If any of the values of the current configuration values doesn't
     * match one of the passed values, return true. */
    retval = (p_interval->reportingStartMins != old_vals.reportingStartMins);

    if (!retval) /** Stop checking if we already know we need a new image. */
    {
        retval = (p_interval->reportingIntervalHours != old_vals.reportingIntervalHours);
    }

    if (!retval) /** Stop checking if we already know we need a new image. */
    {
        retval = (p_interval->reportingTransmitWindowsMins != old_vals.reportingTransmitWindowsMins);
    }

    if (!retval) /** Stop checking if we already know we need a new image. */
    {
        retval = (p_interval->recordingIntervalMins != old_vals.recordingIntervalMins);
    }

    if (retval) /** Do this check if we think we need a new image. */
    {
        /** The reporting interval should not be zero. */
        retval = (0u != p_interval->reportingIntervalHours);
    }

    if (retval) /** Do this check if we think we need a new image. */
    {
        /** The recording interval should not be zero. */
        retval = (0u != p_interval->recordingIntervalMins);
    }

    return retval;
}

/*****************************************************************************
 * @brief
 *   Updates a few fields in the config image. It does this by copying the 
 *   current internal flash config image to the external new image and updating
 *   some fields.
 *
 * @note
 *   The following fields should be updated:
 *      reportingStartMins        
 *      reportingIntervalSecs 
 *      reportingTransmitWindowsQuarterHrs         
 *      recordingIntervalSecs
 *
 * @param[in] reportingStartMins - offset in mins for a cell reporting
 *            reportingIntervalSecs - how often to report data to server
 *            reportingTransmitWindowsQuarterHrs - duration of random interval
 *            recordingIntervalSecs - how often to record the data
 *
 * @return bool indicating success or failure
 ******************************************************************************/
bool    app_command_helper_UpdateNewConfigImage (uint16_t    reportingStartMins,
                                                   uint32_t    reportingIntervalSecs,
                                                   uint8_t     reportingTransmitWindowsQuarterHrs,
                                                   uint32_t    recordingIntervalSecs)
{
    uint32_t remainingBytes;
    uint32_t externalWriteAddress;
    uint32_t internalAddress;
    uint16_t i;
    uint32_t updateAddressReportingStartMins;
    uint32_t updateAddressReportingIntervalSecs;
    uint32_t updateAddressReportingTransmitWindowsQtrHrs;
    uint32_t updateAddressRecordingIntervalSecs;
    static volatile uint32_t crcResult;
    bool status = true;
    bool result = true;

    pRamBuffer = app_scratch_ram_get_256();
    #warning The next line needs to be updated when we fix scratch_ram_get!!!
    pRamBufferVerify = pRamBuffer + 256;

    remainingBytes = CONFIGURATION_SIZE;
    internalAddress = CONFIGURATION_START_ADDRESS; 
    externalWriteAddress = NEW_CONFIG_START_ADDRESS;
    
    updateAddressReportingIntervalSecs = (uint32_t)&uMIUConfigNormal.sMIUConfigNormal.intervalDataPacketIntervalSecs; // uint32
    updateAddressReportingStartMins = (uint32_t)&uMIUConfigNormal.sMIUConfigNormal.cellCallInOffsetMins;  // uint16
    updateAddressReportingTransmitWindowsQtrHrs = (uint32_t)&uMIUConfigNormal.sMIUConfigNormal.cellCallInWindowQtrHrs;  // uint8
    updateAddressRecordingIntervalSecs = (uint32_t)&uMIUConfigNormal.sMIUConfigNormal.datalogIntervalSecs;  // uint32
    i = 0;
    
    
    memset(pRamBuffer, 0, COPY_BUFFER_SIZE);
    crcResult = CRC_SEED;

    // for now, I will process it in COPY_BUFFER_SIZE (64) byte blocks
    while ((remainingBytes) && (i < CONFIGURATION_SIZE) && (true == result))
    {
        if (internalAddress == updateAddressReportingIntervalSecs)
        {
            *(uint32_t *)&pRamBuffer[i] = reportingIntervalSecs;
            i += sizeof(reportingIntervalSecs);
            internalAddress += sizeof(reportingIntervalSecs);
            remainingBytes -= sizeof(reportingIntervalSecs);
        }
        else if (internalAddress == updateAddressReportingStartMins)
        {
            *(uint16_t *)&pRamBuffer[i] = reportingStartMins;
            i += sizeof(reportingStartMins);
            internalAddress += sizeof(reportingStartMins);
            remainingBytes -= sizeof(reportingStartMins);
        }
        else if (internalAddress == updateAddressReportingTransmitWindowsQtrHrs)
        {
            *(uint8_t *)&pRamBuffer[i] = reportingTransmitWindowsQuarterHrs;
            i += sizeof(reportingTransmitWindowsQuarterHrs);
            internalAddress += sizeof(reportingTransmitWindowsQuarterHrs);
            remainingBytes -= sizeof(reportingTransmitWindowsQuarterHrs);
        }
        else if (internalAddress == updateAddressRecordingIntervalSecs)
        {
            *(uint32_t *)&pRamBuffer[i] = recordingIntervalSecs;
            i += sizeof(recordingIntervalSecs);
            internalAddress += sizeof(recordingIntervalSecs);
            remainingBytes -= sizeof(recordingIntervalSecs);
        }
        else
        {
            pRamBuffer[i] = *(uint8_t *)internalAddress;
            i++;
            internalAddress++;
            remainingBytes--;
        }
        if((i & (COPY_BUFFER_SIZE -1)) == 0)
        {
            if (externalWriteAddress < (NEW_CONFIG_START_ADDRESS + CONFIGURATION_SIZE - COPY_BUFFER_SIZE))
            {
                /** The length has to be evenly divisible by 4 to use the
                * FastCrc32Word routine. */
                crcResult = FastCrc32Word(crcResult, pRamBuffer, COPY_BUFFER_SIZE);
            }
            else
            {
                // last block so exclude the CRC from the calculation and update the image info
                app_command_helper_UpdateConfigImageInfo(pRamBuffer, COPY_BUFFER_SIZE, crcResult);
            }
            
            uint32_t retry_cnt;
            retry_cnt = 0;
            do
            {
                // Make sure the chip is ready, then send the write
                status = Flash_WaitOnWip(FLASH_WAIT_ON_WIP_MSEC);
                if (true == status)
                {
                    status = Flash_WriteAndVerify(pRamBuffer,
                                            pRamBufferVerify,
                                            externalWriteAddress,
                                            COPY_BUFFER_SIZE);
                }
                ++retry_cnt;
            }
            while ((false == status) && (2 > retry_cnt));

            if (!(status))
            {
                DEBUG_OUTPUT_TEXT_AND_DATA("This one failed: ",(uint8_t *)&externalWriteAddress,4);
                result = false;
            }
            // update the flash write address for the next block of COPY_BUFFER_SIZE (64)
            externalWriteAddress += COPY_BUFFER_SIZE;
            i = 0;
        }
    }
        
    return result; 
}

/*****************************************************************************
 * @brief
 *   Simple utility to power on the modem and setup the modem GPIO lines. 
 *
 * @note
 *
 * @param[in] none
 *
 * @return bool indicating success or failure
 ******************************************************************************/
void    app_command_helper_powerOnModem (void)
{
    // power up the modem
    modem_power_modemPower_on();
    app_gpio_cellComm_enable();
}

/*****************************************************************************
 * @brief
 *   Simple utility to power off the modem and disable the modem GPIO lines. 
 *
 * @note
 *
 * @param[in] none
 *
 * @return bool indicating success or failure
 ******************************************************************************/
void    app_command_helper_powerOffModem (void)
{
    // power down the modem
    modem_power_modemPower_off();
    app_gpio_cellComm_disable();
    
    DEBUG_TRACE ("Modem power off.\n\r");
}

/*****************************************************************************
 * @brief
 * Proto function follows. Current plan:
 * 1. Push SetIntervals command (or the configuration values?) to userData.
 * 2. On startup, check userData (in application) and either
 *      a. Run command, if that was stored, or
 *      b. Do something so that we prefer the values from userData.
 *
 * @note
 *  We have to use the Memory System Controller (MSC) to write/erase
 *  internal flash.
 *
 * @param[in] uint32_t * - pointer to address of what we push to user data
 * @param[in] uint32_t - number of bytes we're pushing to user data
 *
 * @return None
 ******************************************************************************/
void app_command_helper_push_to_user_data(uint32_t * p_buffer, uint32_t len)
{
    const uint32_t write = USER_DATA_START;
    uint8_t * read = (uint8_t *)p_buffer;
    uint32_t i;
    uint32_t j;
    bool writtenOK;
    msc_Return_TypeDef mscRetval;

    writtenOK = app_command_helper_memcmp((uint8_t *)write, read, len);

    /** Make 4 attempts to write the page. */
    i = 4;
    while ((mscReturnOk != mscRetval) && (!writtenOK) && (0u < i))
    {
        MSC_Init();
        mscRetval = MSC_ErasePage((uint32_t *)write);

        /** Write each part of the buffer IFF the previous step passed */
        for (j = 0; (j < len) && (mscReturnOk == mscRetval); j+=4)
        {
            mscRetval = MSC_WriteWord((uint32_t *)(write+j), (read+j), 4);
        }

        MSC_Deinit();
        --i;

        /** Make sure the write was successful. */
        writtenOK = app_command_helper_memcmp((uint8_t *)write, read, len);
    }
}


/*****************************************************************************
 * @brief
 *  Validate that two buffers of length len are the same.
 *
 * @note
 *  I know, there is already such a thing as memcmp, but it's way more than is
 *  needed for this.
 *
 * @param[in] uint8_t * - pointer to first buffer
 * @param[in] uint8_t * - pointer to second buffer
 * @param[in] uint32_t - number of bytes we're comparing
 *
 * @return bool - true, buffers match; false, buffers don't match
 ******************************************************************************/
static bool app_command_helper_memcmp(uint8_t * p_buf_1, uint8_t * p_buf_2, uint32_t len)
{
    uint32_t i;
    bool mem_matches;

    mem_matches = true;

    for ( i=0; i<len; ++i)
    {
        if (p_buf_1[i] != p_buf_2[i])
        {
            /* There is a mismatch, end the checking loop and set mem_matches
             * to false so that we can notify the calling routine. */
            mem_matches = false;
            i = len;
        }
    }

    return mem_matches;
}


/*****************************************************************************
 * @brief
 *  Validate that the image in internal memory has a valid CRC.
 *
 * @note
 *
 * @param[in] none
 *
 * @return bool - true, valid image; false, invalid image
 ******************************************************************************/
bool app_command_helper_validate_internal_config(void)
{
    bool result;
    static volatile uint32_t crcResult;
    static volatile uint32_t crcExpected;

    ImageInformation_t currentConfig = ImageLists[CmiuConfiguration];

    /** Get the expected CRC from internal flash */
    crcExpected = ReverseCrc32(*currentConfig.CrcAddress);
    /** Calculte the CRC of the internal image */
    if ((currentConfig.Length % 4) == 0)
    {
        /** The length has to be evenly divisible by 4 to use the
         * FastCrc32Word routine. */
        crcResult = FastCrc32Word(CRC_SEED,currentConfig.Address,currentConfig.Length);
    }
    else
    {
        /** This routine doesn't care what the length is or whether
         * it is evenly divisible by 4. */
        crcResult = FastCrc32Byte(CRC_SEED,currentConfig.Address,currentConfig.Length);
    }

    /** Compare the expected value to the result */
    result = (crcResult == crcExpected);

    return result;
}

/*****************************************************************************
 * @brief
 *   Corrupt the application configuration CRC so that the bootloader will
 *   accept the new config image without our needing to up-rev the config.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_command_helper_corrupt_internal_configuration(void)
{
    uint32_t * p_corrupt_address;
    uint8_t corrupt_buffer[4] = {0u, 0u, 0u, 0u};
    uint32_t i;
    msc_Return_TypeDef mscRetval;

    /** Make 4 attempts to corrupt the configuration CRC. */
    i = 4;
    do
    {
        p_corrupt_address = (uint32_t *)CONFIGURATION_CRC;
        MSC_Init();
        mscRetval = MSC_WriteWord(p_corrupt_address, corrupt_buffer, 4);
        MSC_Deinit();
        --i;
    }
    while ((mscReturnOk != mscRetval) && (0u < i));
}
