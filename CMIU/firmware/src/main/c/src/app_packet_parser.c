/******************************************************************************
*******************************************************************************
**
**         Filename: app_packet.c
**    
**           Author: Troy Harstad
**          Created: 4/15/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_packet_parser.c
*
* @brief This file contains the application code used to parse various
*    packets
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include <stdbool.h>
#include "typedefs.h"
#include "DebugTrace.h"
#include "Tpbp.h"
#include "app_message.h"
#include "app_command.h"
#include "app_scratch_ram.h"
#include "app_packet_builder.h"
#include "app_packet_parser.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/



/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/

static E_CODE app_packet_parser_parse_command(S_TPBP_PARSER* const pParser,  
                                            APP_TX_MSG*  pResponseMsg);

static E_CODE app_packet_parser_switch_command(S_TPBP_PARSER* const pParser,
                                            E_COMMAND commandId,
                                            APP_TX_MSG*  pResponseMsg);

static E_CODE app_packet_parser_parse_command_imageUpdate(S_TPBP_PARSER* pParser);
static E_CODE app_packet_parser_parse_command_sleepSeconds(S_TPBP_PARSER* pParser);
static E_CODE app_packet_parser_parse_command_toggleLteModem(S_TPBP_PARSER* pParser);
static E_CODE app_packet_parser_parse_command_readConnected(S_TPBP_PARSER* pParser, APP_TX_MSG* pResponseMsg);
static E_CODE app_packet_parser_parse_command_selectPowerMode(S_TPBP_PARSER* pParser);
static E_CODE app_packet_parser_parse_command_lteCarrierAssert(S_TPBP_PARSER* pParser);
static E_CODE app_packet_parser_parse_command_bleCarrierAssert(S_TPBP_PARSER* pParser);
static E_CODE app_packet_parser_parse_command_setIntervals(S_TPBP_PARSER* pParser,
                                                            APP_TX_MSG* pResponseMsg);
static E_CODE app_packet_parser_parse_command_publishReqestedPacket(S_TPBP_PARSER* pParser, APP_TX_MSG*  pResponseMsg);
static E_CODE app_packet_parser_parse_command_eraseMemory(S_TPBP_PARSER* pParser, APP_TX_MSG* pResponseMsg);
static E_CODE app_packet_parser_parse_command_modemFota(S_TPBP_PARSER* pParser);
static E_CODE app_packet_parser_parse_command_updateApn(S_TPBP_PARSER* pParser, APP_TX_MSG* pResponseMsg);
static E_CODE app_packet_parser_parse_command_runConnectivityTest(
                                S_TPBP_PARSER* pParser,
                                APP_TX_MSG* pResponseMsg);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/

///@todo DEW move the following to app_scratch_ram ??

/* Buffer for assembling secure data */
static uint8_t secureDataBuffer[PACKET_BUFFER_SIZE] SCRATCH_RAM_VARS;

// Buffer for encrypted secureDataBuffer
static uint8_t encryptedSecureDataBuffer[PACKET_BUFFER_SIZE] SCRATCH_RAM_VARS;


static char textBuffer[PACKET_BUFFER_SIZE] SCRATCH_RAM_VARS;

    
/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


#define PKT_FMT_STR "Received Packet %s\r\n"
#define DEBUG_PKT_TYPE(t) DEBUG_TRACE( PKT_FMT_STR, t) 

#define CMD_FMT_STR "Received Command %s\r\n"
#define DEBUG_CMD_TYPE(t) DEBUG_TRACE( CMD_FMT_STR, t) 


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/



/***************************************************************************//**
 * @brief
 *   Initializes packet module
 *
 * @note
 *   
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_packet_parser_init(void)
{    
}


/***************************************************************************//**
 * @brief
 *   Parses packet to appropriate parser based on packet type
 *
 * @note 
 *
 * @param[in] pointer to MQTT_MANAGER_PUB
 * @param [out] pResponseMsg The response filled in.  
 *
 * @return none. Result is in pResponseMsg
 ******************************************************************************/
E_CODE app_packet_parser_parse(const APP_RX_MSG* pRxMsg, APP_TX_MSG* pResponseMsg)
{
    E_CODE eCode = E_CODE_UNINITIALIZED;
    
    S_TPBP_PARSER parser;
    E_PACKET_TYPE_ID packetType;   

    Tpbp_ParserInit(&parser, pRxMsg->pData, pRxMsg->dataLength);

    packetType = Tpbp_ParserRead_PacketTypeId(&parser); 

    // Parse packet based on packet type
    switch (packetType)
    {        
        case E_PACKET_TYPE_DETAILED_CONFIG:
        {
            DEBUG_PKT_TYPE("E_PACKET_TYPE_DETAILED_CONFIG -Not supported");
            eCode = E_CODE_PKT_UNSUPPORTED;
            break;
        }      

        case E_PACKET_TYPE_BASIC_CONFIG:
        {
            DEBUG_PKT_TYPE("E_PACKET_TYPE_BASIC_CONFIG -Not supported");
            eCode = E_CODE_PKT_UNSUPPORTED;
            break;
        }

        case E_PACKET_TYPE_TIME:
        {         
            DEBUG_PKT_TYPE("E_PACKET_TYPE_TIME -Not supported");
            eCode = E_CODE_PKT_UNSUPPORTED;            
            break;
        }        

        case E_PACKET_TYPE_COMMAND:
        {             
            DEBUG_PKT_TYPE("E_PACKET_TYPE_COMMAND");
            eCode = app_packet_parser_parse_command(&parser, pResponseMsg);            
            break;            
        }   
                
        case E_PACKET_TYPE_EVENT:
        {
            DEBUG_PKT_TYPE("E_PACKET_TYPE_EVENT -Not supported");
            eCode = E_CODE_PKT_UNSUPPORTED;
            break;
        }        

        case E_PACKET_TYPE_INTERVAL_DATA:
        {
            DEBUG_PKT_TYPE("E_PACKET_TYPE_INTERVAL_DATA -Not supported");
            eCode = E_CODE_PKT_UNSUPPORTED;            
            break;
        }
        
        default:
        {
           
            eCode = E_CODE_PKT_INVALID_ID;
            break;            
        }   
    }          
    
    DEBUG_ERROR_CHECK(eCode, "Packet Parse error");
    return eCode;
}
 


/***************************************************************************//**
 * @brief
 *   Parses command packet
 *
 * @note 
 *
 * @param[in] pointer to S_TPBP_PARSER
 * @param[out] The response. Either some data tags, or error tag  OK/ERROR
 *
 * @return bool - true if packet parsed successfully, false if not
 ******************************************************************************/
static E_CODE app_packet_parser_parse_command(S_TPBP_PARSER* const pParser, 
                                                    APP_TX_MSG* pResponseMsg)
{ 
    E_CODE              eCode                   = E_CODE_CMD_INVALID;
    bool                isOk                    = true;
    S_TPBP_TAG          tag;
    S_MQTT_BLE_UART     packetHeader;
    uint32_t            secureDataSizeInBytes   = 0;
    S_TPBP_PARSER       secureTagSequenceParser;
    E_COMMAND           eCommandId               = E_COMMAND_COUNT;
    E_ERROR_TAG         errorTagCode             = E_ERROR_INVALID_CMD;
    
    

    // Check that Packet header tag is present
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_UART_PACKET_HEADER, &tag);
    }

    // Read packet header
    if (isOk)
    {
        isOk = Tpbp_ParserRead_UartPacketHeader(pParser, &packetHeader);
    }

    // Check that secure data tag is present
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_SECURE_DATA, &tag);
    }

    // Read secure data
    if (isOk) 
    {
        isOk = Tpbp_ParserGetBytes(pParser, encryptedSecureDataBuffer, tag.dataSize);
    }

    secureDataSizeInBytes = tag.dataSize;

    // "Decrypt" secure data (actually just copy)
    memcpy(secureDataBuffer, encryptedSecureDataBuffer, secureDataSizeInBytes);

    // Init a parser to the decrypted data
    Tpbp_ParserInit(&secureTagSequenceParser, secureDataBuffer, secureDataSizeInBytes);

    // Check if command tag is present
    if (isOk)
    { 
        isOk = Tpbp_ParserFindTag(&secureTagSequenceParser, E_TAG_NUMBER_COMMAND, &tag);
    }

    // Get command number       
    eCommandId = (E_COMMAND)Tpbp_Unpack(&secureTagSequenceParser, tag.dataSize);

    if (isOk)
    {
       eCode = app_packet_parser_switch_command(&secureTagSequenceParser, eCommandId, pResponseMsg);
    }
    else
    {   
       eCode = E_CODE_CMD_MALFORMED;
    }
    
    // If the response msg was *not* explicitly populated by a handler then make an OK/Error general response
    if (0 == pResponseMsg->dataLength)
    {       
        // Convert internal error code into OK/BAD as defined in ETI048
        ///@todo Ideally it would be better to respond with the more descriptive code
        errorTagCode = (E_CODE_OK == eCode) ? E_ERROR_NONE : E_ERROR_INVALID_CMD;
        
        
        // Build (but do not send) the response to the command.
        pResponseMsg->dataLength = app_packet_builder_build_commandResponse (pResponseMsg->pData,
                pResponseMsg->bufferSize,
                eCommandId, 
                errorTagCode);   
        
        // Check it built.
        if (0 == pResponseMsg->dataLength)
        {       
            eCode = E_CODE_PKT_BUILD_FAIL;
        }        
    }
    
    // Note: At this point, pResponseMsg now contains either requested data, or OK/Error content.
    DEBUG_ERROR_CHECK(eCode, "Command Parse error");
    return eCode;
}


/***************************************************************************//**
 * @brief
 *   Calls appropriate command handler
 *
 * @note 
 *
 * @param[in] pointer to S_TPBP_PARSER
 *
 * @return error code - For the command executed.
 ******************************************************************************/
static E_CODE app_packet_parser_switch_command(S_TPBP_PARSER* const pParser,
                                            E_COMMAND eCommandId,
                                            APP_TX_MSG*  pResponseMsg)
{ 
    E_CODE eCode = E_CODE_CMD_INVALID;
    GET_MODEM_INFO_PARAMS       params;
        
    switch (eCommandId)
    { 
    case E_COMMAND_REBOOT:
        DEBUG_CMD_TYPE("E_COMMAND_REBOOT");
        break;
        
    /** 2	Mag swipe Emulation */
    case E_COMMAND_MAGSWIPE:
        DEBUG_CMD_TYPE("E_COMMAND_MAGSWIPE");
        break;
    
    case E_COMMAND_UPDATE_IMAGE:
        DEBUG_CMD_TYPE("E_COMMAND_UPDATE_IMAGE");
        eCode = app_packet_parser_parse_command_imageUpdate(pParser);
        break;
    
    case E_COMMAND_ENTER_OPERATION:
        DEBUG_CMD_TYPE("E_COMMAND_ENTER_OPERATION");
        break;
    
    case E_COMMAND_SLEEP:
        DEBUG_CMD_TYPE("E_COMMAND_SLEEP");
        break;
    
    case E_COMMAND_SET_RECORDING_AND_REPORTING_INTERVALS:
        DEBUG_CMD_TYPE("E_COMMAND_SET_RECORDING_AND_REPORTING_INTERVALS");
        eCode = app_packet_parser_parse_command_setIntervals(pParser, pResponseMsg);
        break;
    
    case E_COMMAND_PUBLISH_REQUESTED_PACKET:
        DEBUG_CMD_TYPE("E_COMMAND_PUBLISH_REQUESTED_PACKET");
        eCode = app_packet_parser_parse_command_publishReqestedPacket(pParser, pResponseMsg);
        break;    
    
    case E_COMMAND_READ_CONNECTED_DEVICES:
        DEBUG_CMD_TYPE("E_COMMAND_READ_CONNECTED_DEVICES");
        eCode = app_packet_parser_parse_command_readConnected(pParser, pResponseMsg);
        break;
    
    case E_COMMAND_SLEEP_SECONDS:
        DEBUG_CMD_TYPE("E_COMMAND_SLEEP_SECONDS");
        eCode = app_packet_parser_parse_command_sleepSeconds(pParser);
        break;
    
    case E_COMMAND_SELECT_POWER_MODE:
        DEBUG_CMD_TYPE("E_COMMAND_SELECT_POWER_MODE");
        eCode = app_packet_parser_parse_command_selectPowerMode(pParser);
        break;
  
    case E_COMMAND_LTE_CARRIER_ASSERT:
        DEBUG_CMD_TYPE("E_COMMAND_LTE_CARRIER_ASSERT");
        eCode = app_packet_parser_parse_command_lteCarrierAssert(pParser);
        break;
    
    case E_COMMAND_GET_CAN_DATA:
        DEBUG_CMD_TYPE("E_COMMAND_GET_CAN_DATA");
         
        params.destination = pResponseMsg->destination;
        eCode = app_command_post_getCANData(&params) ? E_CODE_OK : E_CODE_BUSY;
        break;
    
    case E_COMMAND_BTLE_CARRIER_ASSERT:
        DEBUG_CMD_TYPE("E_COMMAND_BTLE_CARRIER_ASSERT");
        eCode = app_packet_parser_parse_command_bleCarrierAssert(pParser);
        break;
    
    case E_COMMAND_TOGGLE_LTE_MODEM:
        DEBUG_CMD_TYPE("RE_COMMAND_TOGGLE_LTE_MODEM");
        eCode = app_packet_parser_parse_command_toggleLteModem(pParser);
        break;

    case E_COMMAND_GET_DEBUG_LOG:
        DEBUG_CMD_TYPE("E_COMMAND_GET_DEBUG_LOG");
        break;
    
    case E_COMMAND_WAKE:
    case E_COMMAND_SET_CLOCK:
    case E_COMMAND_ERASE_MEMORY_IMAGE:
        DEBUG_CMD_TYPE("E_COMMAND_ERASE_MEMORY_IMAGE");
        eCode = app_packet_parser_parse_command_eraseMemory(pParser, pResponseMsg);
        break;

    case E_COMMAND_GET_CMIU_SIGNAL_QUALITY:
        DEBUG_CMD_TYPE("E_COMMAND_GET_CMIU_SIGNAL_QUALITY");

        params.destination = pResponseMsg->destination;
        eCode = app_command_post_getSignalQuality(&params) ? E_CODE_OK : E_CODE_BUSY;
        break;
    
    case E_COMMAND_MODEM_FOTA:
        DEBUG_CMD_TYPE("E_COMMAND_MODEM_FOTA");
        eCode = app_packet_parser_parse_command_modemFota(pParser);
        break; 

    case E_COMMAND_REQUEST_APN:
        DEBUG_CMD_TYPE("E_COMMAND_REQUEST_APN");
        params.destination = pResponseMsg->destination;
        eCode = app_command_post_requestApn(&params) ? E_CODE_OK : E_CODE_BUSY;
        break; 

    case E_COMMAND_UPDATE_APN:
        DEBUG_CMD_TYPE("E_COMMAND_UPDATE_APN");
        eCode = app_packet_parser_parse_command_updateApn(pParser, pResponseMsg);
        break;

    case E_COMMAND_RUN_CONNECTIVITY_TEST:
        DEBUG_CMD_TYPE("E_COMMAND_RUN_CONNECTIVITY_TEST");
        eCode = app_packet_parser_parse_command_runConnectivityTest(pParser, pResponseMsg);
        break;
    
    case E_COMMAND_SET_BROKER_ADDRESS:
    case E_COMMAND_SET_FALLBACK_BROKER_ADDRESS:
    default:
        DEBUG_TRACE("Received Unknown/ Unsupported Command ID=%u\r\n", (uint32_t)eCommandId);
        eCode = E_CODE_CMD_INVALID;
        break;
    }
    
    
    DEBUG_ERROR_CHECK(eCode, "Command Handler error");
    return eCode;
}




static E_CODE app_packet_parser_parse_command_imageUpdate(S_TPBP_PARSER* secureTagSequenceParser)
{
    E_CODE                  eCode   = E_CODE_CMD_PARSE_FAIL;
    bool                    isOk                    = true;
    S_TPBP_TAG              tag;
    S_IMAGE_METADATA        imageMetadata;
    S_IMAGE_VERSION_INFO    imageVersionInfo;
    uint64_t                applyAtTime;
    const char*             imageUrl;
    uint8_t                 imagesFound;
    uint8_t                 imageSucccessCount;
    
    
    // Counts number of images found in command and successfull images commanded
    imagesFound = 0;
    imageSucccessCount = 0;

    // Check that timestamp tag is present
    isOk &= Tpbp_ParserFindTag(secureTagSequenceParser, E_TAG_NUMBER_TIMESTAMP, &tag);
    
    if (isOk == true)
    {
        // Read timestamp tag
        applyAtTime = Tpbp_Unpack64(secureTagSequenceParser);
    }   
    
    // Handle multiple images if present in the command
    while(isOk)
    {               
        // Check that image name tag is present
        isOk &= Tpbp_ParserFindTag(secureTagSequenceParser, E_TAG_NUMBER_IMAGE, &tag);
        
        if (isOk == true)           
        {    
            // Found an image
            imagesFound++;
                            
            // Read image name      
            isOk &= Tpbp_ParserRead_String(secureTagSequenceParser, &tag, textBuffer, sizeof(textBuffer));
            imageUrl = textBuffer;
        }
        if (isOk == true)
        {
           // Check that image metadata tag is present 
            isOk &= Tpbp_ParserFindTag(secureTagSequenceParser, E_TAG_NUMBER_IMAGE_METADATA, &tag);
        }
        if (isOk == true)
        {
            // Read image metadata
            isOk &= Tpbp_ParserRead_ImageMetadata(secureTagSequenceParser, &imageMetadata);
        }
        if (isOk == true)
        {
            // Check image meta data for the type of image being updated
            // then check the correect image version tag is present
            switch (imageMetadata.imageType)
            {
                case E_IMAGE_TYPE_FIRMWARE:
                {
                    DEBUG_TRACE("Received Application Image Update Command\r\n");
                    // Check that firmware image version tag is present
                    isOk &= Tpbp_ParserFindTag(secureTagSequenceParser, E_TAG_NUMBER_FIRMWARE_REVISION, &tag);
                    break;               
                }
                            
                case E_IMAGE_TYPE_CONFIG:
                {
                    DEBUG_TRACE("Received Configuration Image Update Command\r\n");
                    // Check that firmware image version tag is present
                    isOk &= Tpbp_ParserFindTag(secureTagSequenceParser, E_TAG_NUMBER_CONFIG_REVISION, &tag);
                    break;               
                }
                
                
                case E_IMAGE_TYPE_TELIT_MODULE:
                {
                    DEBUG_TRACE("Received Telit Update Command\r\n");
                    // Check that firmware image version tag is present
                  //  isOk &= Tpbp_ParserFindTag(secureTagSequenceParser, E_TAG_NUMBER_TELI_REVISION, &tag);
                    break;               
                }
             
                
                case E_IMAGE_TYPE_BLE_CONFIG:
                {
                    DEBUG_TRACE("Received BLE Image Update Command\r\n");
                    // Check that firmware image version tag is present
                    isOk &= Tpbp_ParserFindTag(secureTagSequenceParser, E_TAG_NUMBER_BLE_CONFIG_REVISION, &tag);
                    break;               
                }
                
                
                case E_IMAGE_TYPE_ARB_CONFIG:
                {
                    DEBUG_TRACE("Received ARB Configuration Image Update Command\r\n");
                    // Check that firmware image version tag is present
                    isOk &= Tpbp_ParserFindTag(secureTagSequenceParser, E_TAG_NUMBER_ARB_REVISION, &tag);
                    break;               
                }
                
                case E_IMAGE_TYPE_ENCRYPTION:
                {
                    DEBUG_TRACE("Received Encryption Image Update Command\r\n");
                    // Check that firmware image version tag is present
                    isOk &= Tpbp_ParserFindTag(secureTagSequenceParser, E_TAG_NUMBER_FIRMWARE_REVISION, &tag);
                    break;               
                }                      
            }           
        }
        
        if (isOk == true)
        {
            // Read image version tag, tag found above
            isOk &= Tpbp_ParserRead_ImageVersionInfo(secureTagSequenceParser, &imageVersionInfo);
        }
        //the rest is just 0s so ignore
        
        if( isOk == true)
        {
            // Handle image update via command module
            isOk = app_command_imageUpdate_handler(&imageMetadata, &imageVersionInfo, applyAtTime, imageUrl);
        }
        
        if(isOk == true)
        {
            imageSucccessCount++;
        }
        
        // Check if another image is present        
    }
    
    // While loop above will clear isOk once it finally doesn't find any more images
    // in the command so need to report successfull parsing if the number of images
    // found in command is greater than zero and is equal to images updated via handler.
    if((imageSucccessCount > 0)  && (imageSucccessCount == imagesFound))
    {
        isOk = true;
    }
    

    eCode = isOk ? E_CODE_OK : E_CODE_CMD_PARSE_FAIL;    
    return eCode;
}



/***************************************************************************//**
 * @brief
 *   Parses Publish Requested Packet command packet. If the command parses correctly then
 *   it calls a command handler to publish a packet as requested.
 *
 * @note 
 *
 * @param[in] pointer to S_TPBP_PARSER
 *
 * @return eCode  if packet parsed successfully, or error code if not
 * @todo Review workaround for iOS app
 ******************************************************************************/
static E_CODE app_packet_parser_parse_command_publishReqestedPacket(S_TPBP_PARSER* pParser, 
    APP_TX_MSG*  pResponseMsg)
{
    bool                    isOk = true;
    S_TPBP_TAG              tag;
    uint8_t                 packetType = E_PACKET_TYPE_DETAILED_CONFIG;
    uint8_t                 destination;
    E_CODE                  eCode   = E_CODE_CMD_PARSE_FAIL;

    // Check that packet type tag (i.e what pkt type is wanted) is present  
    isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_PACKET_TYPE, &tag);
    
    // Workaround for backwards compatibilty to iOS CMIU Manager. It sends no tags (e.g for destination)
    // So send a DCP
    if (false == isOk)
    {
        packetType  = E_PACKET_TYPE_DETAILED_CONFIG;
        destination = pResponseMsg->destination;
        isOk = true;
    }
    else
    {
        // Read packet type tag
        if (isOk) 
        {
            isOk = Tpbp_ParserRead_Command_PacketType(pParser, &packetType);
        }
        
        // Check that destination tag is present
        if (isOk)
        {
            isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_DESTINATION, &tag);
        }

        // Read destination tag 
        if (isOk)
        {
            isOk = Tpbp_ParserRead_Command_Destination(pParser, &destination);
        }
    }
        
    if (isOk == true)
    {
        isOk = app_command_publishRequestedPacket_handler(pResponseMsg, packetType, destination); 
        eCode = isOk ? E_CODE_OK : E_CODE_CMD_FAIL;  
    }

    return eCode;
}



/***************************************************************************//**
 * @brief
 *   Parses Toggle LTE Modem command packet. If the command parses correctly then
 *   it calls a command handler to power on/off the modem as requested.
 *
 * @note 
 *
 * @param[in] pointer to S_TPBP_PARSER
 *
 * @return bool - true if packet parsed successfully, false if not
 ******************************************************************************/
static  E_CODE app_packet_parser_parse_command_toggleLteModem(S_TPBP_PARSER* pParser)
{
    bool                    isOk = true;
    S_TPBP_TAG              tag;
    TOGGLE_LTE_PARAMS       toggleLteParams;         
    E_CODE             eCode   = E_CODE_CMD_PARSE_FAIL;    
     
        // Check that start delay tag is present
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_STARTDELAY, &tag);
    }
   
    // Read start delay tag
//       startDelayInMsec = Tpbp_Unpack64(pParser);
    if (isOk) 
    {
        isOk = Tpbp_ParserRead_Command_StartDelay(pParser, &toggleLteParams.startDelayInMsec);
    }
    
    // Check that duration tag is present
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_DURATION, &tag);
    }

        // Read duration tag
//            durationInMsec = Tpbp_Unpack64(pParser);
    if (isOk) 
    {
        isOk = Tpbp_ParserRead_Command_Duration(pParser, &toggleLteParams.durationInMsec);
    }
    

        // Check that enable/disable tag is present
    if (isOk)
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_TOGGLELTEMODEM, &tag);
    }
    
            // Read enable/disable tag
//                enableDisableModem = (uint8_t) Tpbp_Unpack(pParser, 1);
    if (isOk)
    {
        isOk = Tpbp_ParserRead_Command_ToggleLTEModem(pParser, &toggleLteParams.enableDisableModem);
    }
    
    if (isOk == true)
    {
        isOk = app_command_post_toggleLTE(&toggleLteParams);
    }

    eCode = isOk ? E_CODE_OK : E_CODE_CMD_PARSE_FAIL;    
    return eCode;
}


/***************************************************************************//**
 * @brief
 *   Parses Sleep Seconds command packet. If the command parses correctly then
 *   it calls a command handler to sleep as requested.
 *
 * @note 
 *
 * @param[in] pointer to S_TPBP_PARSER
 *
 * @return bool - true if packet parsed successfully, false if not
 ******************************************************************************/
static E_CODE app_packet_parser_parse_command_sleepSeconds(S_TPBP_PARSER* pParser)
{
    bool                    isOk = true;
    S_TPBP_TAG              tag;
    SLEEP_SECONDS_PARAMS    sleepParams;
    E_CODE             eCode   = E_CODE_CMD_PARSE_FAIL;

    // Check that the start delay tag is present
    if (isOk) 
    {
            isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_STARTDELAY, &tag);
    }

    // Read start delay tag
    //        startDelayInMsec = Tpbp_Unpack64(pParser);
    if (isOk) 
    {
            isOk = Tpbp_ParserRead_Command_StartDelay(pParser, &sleepParams.startDelayInMsec);
    }
    
    // Check that duration tag is present
    if (isOk)
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_DURATION, &tag);
    }

    // Read duration tag
     //   durationInMsec = Tpbp_Unpack64(pParser);
    if (isOk) 
    {
        isOk = Tpbp_ParserRead_Command_Duration(pParser, &sleepParams.durationInMsec);
    }
        
         
    if (isOk == true)
    {
        isOk = app_command_post_sleepSeconds(&sleepParams);
    }

    eCode = isOk ? E_CODE_OK : E_CODE_CMD_PARSE_FAIL;    
    return eCode;
}



/***************************************************************************//**
 * @brief
 *   Parses Read Connected devices command packet. If the command parses correctly then
 *   it calls a command handler to sleep as requested.
 *
 * @note 
 *
 * @param[in] pointer to S_TPBP_PARSER
 *
 * @return bool - true if packet parsed successfully, false if not
 ******************************************************************************/
static E_CODE app_packet_parser_parse_command_readConnected(S_TPBP_PARSER* pParser,
                                                            APP_TX_MSG* pResponseMsg)
{
    bool                    isOk;
    S_TPBP_TAG              tag;
    E_CODE                  eCode;
    READ_DEVICES_PARAMS     readDevicesParams;

    S_COMMAND_READCONNECTEDEVICE sCommand;

    // Check that start delay tag is present
    isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_STARTDELAY, &tag);

    // Read start delay tag
    if (isOk)
    {
        isOk = Tpbp_ParserRead_Command_StartDelay(pParser, &readDevicesParams.startDelayInMsec);
    }

    // Check that the read connected tag is present
    if (isOk)
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_READCONNECTEDEVICE, &tag);
    }

    // Read read connected tag
    if (isOk)
    {
       isOk = Tpbp_ParserRead_Command_ReadConnectedDevice(pParser, &sCommand);
    }

    if (isOk == true)
    {
        readDevicesParams.pulse = sCommand.pulse;
        readDevicesParams.delayBetweenReadsMsec = sCommand.delayBetweenReadsMsec;
        readDevicesParams.numberOfRepeats = sCommand.numberOfRepeats;
        // This makes sure that subsequent responses are sent to the right
        // destination (BLE/Serial).
        readDevicesParams.destination = pResponseMsg->destination;

        isOk = app_command_post_readConnectedDevices(&readDevicesParams);

        eCode = isOk ? E_CODE_OK : E_CODE_CMD_FAIL;
    }
    else
    {
        eCode = E_CODE_CMD_PARSE_FAIL;
    }

    return eCode;
}


/***************************************************************************//**
 * @brief
 *   Parses the set recording and reporting intervals command packet. If the command parses correctly then
 *   command parses correctly then it calls a command handler to update the intervals.
 *
 * @note 
 *
 * @param[in] pointer to S_TPBP_PARSER
 * @param[in] pointer to APP_TX_MSG
 *
 * @return bool - true if packet parsed successfully, false if not
 ******************************************************************************/
static E_CODE app_packet_parser_parse_command_setIntervals(S_TPBP_PARSER* pParser,
                                                            APP_TX_MSG* pResponseMsg)
{
    bool                    isOk                    = true;
    S_TPBP_TAG              tag;
    S_RECORDING_REPORTING_INTERVAL                setIntervalParms;
    
    E_CODE             eCode   = E_CODE_CMD_PARSE_FAIL;
     
    
    // Check that recording and reporting interval tag is present
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_RECORDING_REPORTING_INTERVAL, &tag);
    }

    // Read recording and reporting interval tag
    if (isOk) 
    {
        isOk = Tpbp_ParserRead_RecordingReportingInterval (pParser, &setIntervalParms);
    }
    

    if (isOk == true)
    {
        isOk = app_command_post_setIntervals (&setIntervalParms, pResponseMsg);
        eCode = isOk ? E_CODE_OK : E_CODE_CMD_FAIL;    
    }


    return eCode;
}


/***************************************************************************//**
 * @brief
 *   Parses Select Power Mode command packet. If the command parses correctly then
 *   it calls a command handler to change the power as requested.
 *
 * @note 
 *
 * @param[in] pointer to S_TPBP_PARSER
 *
 * @return bool - true if packet parsed successfully, false if not
 ******************************************************************************/
static E_CODE app_packet_parser_parse_command_selectPowerMode(S_TPBP_PARSER* pParser)
{
    bool                    isOk = true;
    S_TPBP_TAG              tag;
    POWER_MODE_PARAMS       params;
    E_CODE             eCode   = E_CODE_CMD_PARSE_FAIL;
     
    
    // Check that start delay tag is present
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_STARTDELAY, &tag);
    }

    // Read start delay tag
    //       startDelayInMsec = Tpbp_Unpack64(pParser);
    if (isOk) 
    {
        isOk = Tpbp_ParserRead_Command_StartDelay(pParser, &params.startDelayInMsec);
    }
    
    // Check that duration tag is present
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_DURATION, &tag);
    }

    // Read duration tag
    //  durationInMsec = Tpbp_Unpack64(pParser);
    if (isOk) 
    {
        isOk = Tpbp_ParserRead_Command_Duration(pParser, &params.durationInMsec);
    }
    
    // Check that the select power mode tag is present
    if (isOk)
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_POWERMODE, &tag);
    }

    // Read select power mode tag
    if (isOk) 
    {
        isOk = Tpbp_ParserRead_Command_PowerMode(pParser, &params.powerMode);
    }
        
    if (isOk == true)
    {
        isOk = app_command_post_selectPowerMode(&params); 
    }

 
    eCode = isOk ? E_CODE_OK : E_CODE_CMD_PARSE_FAIL;    
    return eCode;
}


/***************************************************************************//**
 * @brief
 *   Parses LTE Carrier Assert command packet. If the command parses correctly then
 *   it calls a command handler to send the request to the modem.
 *
 * @note 
 *
 * @param[in] pointer to S_TPBP_PARSER
 *
 * @return bool - true if packet parsed successfully, false if not
 ******************************************************************************/
static E_CODE app_packet_parser_parse_command_lteCarrierAssert(S_TPBP_PARSER* pParser)
{    
    E_CODE             eCode   = E_CODE_CMD_PARSE_FAIL;
    bool                    isOk                    = true;
    S_TPBP_TAG              tag;
    LTE_CARRIER_ASSERT_PARAMS   params;
     
    
    // Check that start delay tag is present
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_STARTDELAY, &tag);
    }

    // Read start delay tag
    //       startDelayInMsec = Tpbp_Unpack64(pParser);
    if (isOk) 
    {
        isOk = Tpbp_ParserRead_Command_StartDelay(pParser, &params.startDelayInMsec);
    }
    
    // Check that duration tag is present
    if (isOk)
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_DURATION, &tag);
    }

    // Read duration tag
    //            durationInMsec = Tpbp_Unpack64(pParser);
    if (isOk)
    {
        isOk = Tpbp_ParserRead_Command_Duration(pParser, &params.durationInMsec);
    }
        
        
    // Check that the select power mode tag is present
    if (isOk)
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_RFTESTMODE, &tag);
    }

            // Read select power mode tag
    if (isOk) 
    {
        isOk = Tpbp_ParserRead_Command_RFTestMode(pParser, &params.rfTestMode);
    }
    
    if (isOk == true)
    {
        isOk = app_command_post_LTECarrierAsser(&params); 
    }

        
    eCode = isOk ? E_CODE_OK : E_CODE_CMD_PARSE_FAIL;    
    return eCode;
}

/***************************************************************************//**
 * @brief
 *   Parses BLE Carrier Assert command packet. If the command parses correctly then
 *   it calls a command handler to send the request to the modem.
 *
 * @note 
 *
 * @param[in] pointer to S_TPBP_PARSER
 *
 * @return bool - true if packet parsed successfully, false if not
 ******************************************************************************/
static E_CODE app_packet_parser_parse_command_bleCarrierAssert(S_TPBP_PARSER* pParser)
{    
    E_CODE             eCode   = E_CODE_CMD_PARSE_FAIL;
    bool                    isOk                    = true;
    S_TPBP_TAG              tag;
    BLE_CARRIER_ASSERT_PARAMS   params;
     
    
    // Check that start delay tag is present
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_STARTDELAY, &tag);
    }

    // Read start delay tag
    if (isOk) 
    {
        isOk = Tpbp_ParserRead_Command_StartDelay(pParser, &params.startDelayInMsec);
    }
    
    // Check that duration tag is present
    if (isOk)
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_DURATION, &tag);
    }

    // Read duration tag
    if (isOk)
    {
        isOk = Tpbp_ParserRead_Command_Duration(pParser, &params.durationInMsec);
    }
        
        
    // Check that the select power mode tag is present
    if (isOk)
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_COMMAND_BTLE_RFTESTMODE, &tag);
    }

    // Read BLE RF Test mode tag
    if (isOk) 
    {
        isOk = Tpbp_ParserRead_Command_Btle_RFTestMode(pParser, &params.bleTestMode);
    }
    
    if (isOk == true)
    {
        isOk = app_command_post_BLECarrierAssert(&params); 
    }

        
    eCode = isOk ? E_CODE_OK : E_CODE_CMD_PARSE_FAIL;    
    return eCode;
}




/***************************************************************************//**
 * @brief
 *   Parses Erase Memory command packet. If the command parses correctly then
 *   it calls a command handler to erase the memory as requested.
 *
 * @note 
 *
 * @param[in] pointer to S_TPBP_PARSER
 *
 * @return bool - true if packet parsed successfully, false if not
 ******************************************************************************/
static E_CODE app_packet_parser_parse_command_eraseMemory(S_TPBP_PARSER* pParser,
                                                          APP_TX_MSG* pResponseMsg)
{
    bool                    isOk = true;
    S_TPBP_TAG              tag;
    ERASE_MEMORY_PARAMS       params;
    E_CODE             eCode   = E_CODE_CMD_PARSE_FAIL;
     
      
    // Check that the erase memory tag is present 
    isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_MEMORY_IMAGE, &tag);

    // Read erase memory images tag
    if (isOk) 
    {
        isOk = Tpbp_ParserRead_MemoryImageTag(pParser, &params.imagesBitMap);
    }
        
    if (isOk)
    {
        // Post command
        params.destination = pResponseMsg->destination;
        params.retries = 0;
        isOk = app_command_post_eraseMemory(&params); 
    }

 
    eCode = isOk ? E_CODE_OK : E_CODE_CMD_PARSE_FAIL;    
    return eCode;
}

/***************************************************************************//**
 * @brief
 *   Parses modem FOTA command packet. If the command parses correctly then
 *   it calls a command handler to update the modem firmware as requested.
 *
 * @note 
 *
 * @param[in] pointer to S_TPBP_PARSER
 *
 * @return bool - true if packet parsed successfully, false if not
 ******************************************************************************/
static E_CODE app_packet_parser_parse_command_modemFota(S_TPBP_PARSER* pParser)
{
    bool                    isOk = true;
    S_TPBP_TAG              tag;
    COMMAND_UPDATE_MODEM_FIRMWARE_PARAMS       params;
    E_CODE             eCode   = E_CODE_CMD_PARSE_FAIL;
     
      
    // Check that the FTP DNS address tag is present 
    isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_FTP_DNS_ADDRESS, &tag);

    // Read server address tag
    if (isOk) 
    {
        // Read server address      
        isOk &= Tpbp_ParserRead_String(pParser, &tag, (char *)params.ftpServer, sizeof(params.ftpServer));
    }

    // Check that the FTP port number tag is present 
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_FTP_PORT_NUMBER, &tag);
    }

    // Read FTP port number tag
    if (isOk) 
    {
        // Read port number      
        isOk &= Tpbp_ParserRead_FtpPortNumber(pParser, &params.ftpPort);
    }

    // Check that the FTP user name tag is present 
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_FTP_USERNAME, &tag);
    }

    // Read FTP user name tag
    if (isOk) 
    {
        // Read user name      
        isOk &= Tpbp_ParserRead_String(pParser, &tag, (char *)params.username, sizeof(params.username));
    }

    // Check that the FTP password tag is present 
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_FTP_PASSWORD, &tag);
    }

    // Read password tag
    if (isOk) 
    {
        // Read password     
        isOk &= Tpbp_ParserRead_String(pParser, &tag, (char *)params.password, sizeof(params.password));
    }

    // Check that the FTP file name tag is present 
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_IMAGE, &tag);
    }

    // Read file name tag
    if (isOk) 
    {
        // Read file name      
        isOk &= Tpbp_ParserRead_String(pParser, &tag, (char *)params.fotaFilename, sizeof(params.fotaFilename));
    }

    // Check that the new version tag is present 
    if (isOk) 
    {
        isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_MODEM_FOTA_VERSION, &tag);
    }

    // Read new version name
    if (isOk) 
    {
        // Read new version     
        isOk &= Tpbp_ParserRead_String(pParser, &tag, (char *)params.newFirmwareVersion, sizeof(params.newFirmwareVersion));
    }
    
    if (isOk)
    {
        // Post command
        isOk = app_command_post_modemFota(&params); 
    }

 
    eCode = isOk ? E_CODE_OK : E_CODE_CMD_PARSE_FAIL;    
    return eCode;
}

/***************************************************************************//**
 * @brief
 *   Parses Update APN command packet. If the command parses correctly then
 *   it calls a command handler to update the APN in the modem as requested.
 *
 * @note 
 *
 * @param[in] pParser - pointer to S_TPBP_PARSER
 * @param[in] pResponseMsg - pointer to the response buffer
 *
 * @return bool - true if packet parsed successfully, false if not
 ******************************************************************************/
static E_CODE app_packet_parser_parse_command_updateApn(S_TPBP_PARSER* pParser,
                                                        APP_TX_MSG* pResponseMsg)
{
    bool                    isOk = true;
    S_TPBP_TAG              tag;
    COMMAND_UPDATE_APN_PARAMS       params;
    E_CODE             eCode   = E_CODE_CMD_PARSE_FAIL;
     
    params.destination = pResponseMsg->destination;
  
    // Check that the APN tag is present 
    isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_CMIU_APN, &tag);

    // Read APN tag
    if (isOk) 
    {
        // Read server address      
        isOk &= Tpbp_ParserRead_String(pParser, &tag, (char *)params.apnString, sizeof(params.apnString));
    }

    if (isOk)
    {
        // Post command
        isOk = app_command_post_updateApn(&params); 
    }

 
    eCode = isOk ? E_CODE_OK : E_CODE_CMD_PARSE_FAIL;    
    return eCode;
}

/***************************************************************************//**
 * @brief
 *   Parses the Run Connectivy Test command packet. If the command parses
 *   correctly, then it will call a command handler to run the connectivity
 *   test.
 *
 * @note
 *
 * @param[in] pParser - pointer to S_TPBP_PARSER
 * @param[in] pResponseMsg - pointer to the response buffer
 *
 * @return bool - true if packet parsed successfully, false if not
 ******************************************************************************/
static E_CODE app_packet_parser_parse_command_runConnectivityTest(
                                S_TPBP_PARSER* pParser,
                                APP_TX_MSG* pResponseMsg)
{
    bool                                isOk = true;
    S_TPBP_TAG                          tag;
    COMMAND_RUN_CONNECTIVITY_PARAMS     params;
    E_CODE                              eCode = E_CODE_CMD_PARSE_FAIL;

    params.destination = pResponseMsg->destination;

    // Check that the packet instigator tag is present
    isOk = Tpbp_ParserFindTag(pParser, E_TAG_NUMBER_PACKET_INSTIGATOR, &tag);

    // Read Packet Instigator Tag
    if (isOk)
    {
        // Read out the packet instigator
        isOk = Tpbp_ParserRead_PacketInstigator(pParser, (uint8_t *)&params.packetInstigator);
    }

    if (isOk)
    {
        // Post command
        isOk = app_command_post_runConnectivityTest(&params);
    }

    eCode = ((isOk) ? E_CODE_OK : E_CODE_CMD_PARSE_FAIL);
    return eCode;
}
