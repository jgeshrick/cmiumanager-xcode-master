/******************************************************************************
*******************************************************************************
**
**         Filename: app_exceptions.c
**
**           Author: Troy Harstad
**          Created: 2016.01.07
**
**     Last Edit By:
**        Last Edit:
**
** Revision History:
**       2016.01.07: First created (Troy Harstad)
**
**    Copyright 2016 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_exceptions.c
*
* @brief
*  This module is used to handle any/all ARM exceptions that occur that aren't
*  handled elsewhere.  Exceptions handled include interrupts, faults, and 
*  system exceptions.

************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include "em_device.h"
#include "typedefs.h"
#include "app_exceptions.h"
#include "debugtrace.h"
#include "app_uart.h"


/*=========================================================================*/
/*  T Y P E D E F S                                                        */
/*=========================================================================*/

/*=========================================================================*/
/*  L O C A L   G L O B A L   V A R I A B L E S                            */
/*=========================================================================*/


/*=========================================================================*/
/*  P R I V A T E   F U N C T I O N S   P R O T O T Y P E S                */
/*=========================================================================*/


/*=========================================================================*/
/*  P U B L I C   F U N C T I O N S                                        */
/*=========================================================================*/

/***************************************************************************//**
 * @brief
 *      Each unused exception/interrupt defined here to breakpoint if running
 *      on the debugger or reset otherwise.  
 *
 * @note
 *      Each of these are also defined in startup_efm32lg.s as WEAK functions
 *      that are infinite loops, which is not what we want.  Once defined here
 *      these functions will be used.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/

// ****************************************************************************
// ****************************************************************************
// S Y S T E M    E X C E P T I O N S
// ****************************************************************************
// ****************************************************************************
// Defined in startup_efm32lg.s
// void Reset_Handler(void)
// {
// }     

void NMI_Handler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}       

void HardFault_Handler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}     

void MemManage_Handler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}    

void BusFault_Handler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}    

void UsageFault_Handler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}  

void SVC_Handler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}  

void DebugMon_Handler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}   

void PendSV_Handler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}   

// Defined in app_timers.c
// void SysTick_Handler(void)
// {
// }   

// ****************************************************************************
// ****************************************************************************
// I N T E R R U P T S
// ****************************************************************************
// ****************************************************************************
// Defined in em_dma.c
// void DMA_IRQHandler(void)
// {
//     NVIC_SystemReset();
// }

void GPIO_EVEN_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

// Defined in app_timers.c
// void  TIMER0_IRQHandler(void)
// {   
// }

// Defined in modem library
// void USART0_RX_IRQHandler(void)
// {
// }

// Defined in modem library
// void USART0_TX_IRQHandler(void)
// {
// }

void USB_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void ACMP0_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void ADC0_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void DAC0_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void I2C0_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void I2C1_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void GPIO_ODD_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

// Defined in app_timer.c
// void TIMER1_IRQHandler(void)
// {
// }

void TIMER2_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void TIMER3_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

// Defined in app_uart.c
// void USART1_RX_IRQHandler(void)
// {
// }

// Defined in app_uart.c
// void USART1_TX_IRQHandler(void)
// {
// }

void LESENSE_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void USART2_RX_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void USART2_TX_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void UART0_RX_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void UART0_TX_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void UART1_RX_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void UART1_TX_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void LEUART0_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void LEUART1_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

// Defined in app_timers.c
// void LETIMER0_IRQHandler(void)
// {
//     NVIC_SystemReset();
// }

void PCNT0_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void PCNT1_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void PCNT2_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

// Defined in app_rtc.c
// void RTC_IRQHandler(void)
// {
// }

void BURTC_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void CMU_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void VCMP_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void LCD_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void MSC_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void AES_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void EBI_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

void EMU_IRQHandler(void)
{
    // Check if debugger connected
    if(CoreDebug->DHCSR & 1)
    {
        // Halt program execution
        __breakpoint(0);
    }
    else
    {
       // Force reset
       NVIC_SystemReset(); 
    }
}

