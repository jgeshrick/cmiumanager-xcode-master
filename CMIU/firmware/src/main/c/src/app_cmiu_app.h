/******************************************************************************
*******************************************************************************
**
**         Filename: app_cmiu_app.h
**    
**           Author: Troy Harstad
**          Created: 1/16/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_CMIU_APP_H
#define __APP_CMIU_APP_H



/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
typedef enum E_CMIU_APP_STATE
{
    // "Swipe" operating state
    E_CMIU_APP_STATE_SWIPE          = 0x00,

    // "Normal" operating state
    E_CMIU_APP_STATE_NORMAL         = 0x01,

    // Cellular operating state
    E_CMIU_APP_STATE_CELLULAR       = 0x02 
    
} E_CMIU_APP_STATE;

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_cmiu_app_init(void);
extern void app_cmiu_app_tick(void);
extern void app_cmiu_app_check_cell_session(void);
extern void app_cmiu_app_switch_to_swipe_mode(void);
extern void app_cmiu_app_intervalUpdate(void);
extern uint16_t app_cmiu_app_arbReadAge_retrieve(void);
extern E_CMIU_APP_STATE app_cmiu_app_state_get(void);
extern void app_cmiu_app_normal_enter(void);
extern U_NEPTUNE_TIME app_cmiu_app_scheduler_findNextInterval(uint32_t timeAlignmentSeconds);
extern void app_cmiu_app_normal_enterAfterSwipe(void);

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/



/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_CMIU_APP_H

