/******************************************************************************
*******************************************************************************
**
**         Filename: app_cmit_interface.h
**    
**           Author: Barry Huggins
**          Created: 6/11/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/

// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_CMIT_INTERFACE_H
#define __APP_CMIT_INTERFACE_H

#include "RingBuffer.h"
#include "TransportManager.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
#define BLE_NUS_MAX_DATA_LEN 20

#define     APP_CMIT_INTERFACE_DEBUG_ON_UART        1
#define     APP_CMIT_INTERFACE_DEBUG_ON_BTLE        2

/** Maximum number of attempts to empty the BLE buffer. */
#define     APP_CMIT_MAX_BLE_CLEAR_ATTEMPTS         2000


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
void app_cmit_interface_init(void);
void app_cmit_interface_tick (uint32_t tickIntervalMs);
void app_cmit_interface_tick_on_time_change(void);
void app_cmit_interface_tick_ble_empty(const uint32_t maxTries);
void app_cmit_interface_test_output (void);
void app_cmit_output_data (uint8_t *dataPtr, uint32_t dataSize);
void app_cmit_interface_WriteDebugString (const char *dataPtr);
void app_cmit_interface_WriteDebugStringAndData (const char *dataPtr, uint8_t *pbyDataBuffer, uint8_t byDataLength);
void app_cmit_interface_SendRespMessage (const uint8_t* pData, uint32_t dataLength);
void app_cmit_interface_SendRespMessage_Serial (const uint8_t* pData, uint32_t dataLength);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None

#endif // __APP_CMIT_INTERFACE_H

