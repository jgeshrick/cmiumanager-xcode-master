/******************************************************************************
*******************************************************************************
**
**         Filename: app_cellular.h
**    
**           Author: Troy Harstad
**          Created: 7/9/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_CELLULAR_H
#define __APP_CELLULAR_H

#include "TransportManager.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
typedef enum E_CELL_SESSION
{
    E_CELL_SESSION_MQTT,
    E_CELL_SESSION_HTTP,
    E_CELL_SESSION_FOTA,    
    E_CELL_SESSION_IDLE

} E_CELL_SESSION;

/** This is used for keeping up with packet types that we send via MQTT. */
typedef enum E_MQTT_PACKETS
{
    /** Detailed Config Packet */
    E_MQTT_PACKETS_DETAILED_CONFIG,
    /** Interval Data Packet */
    E_MQTT_PACKETS_INTERVAL_DATA,

    E_MQTT_PACKETS_COUNT
} E_MQTT_PACKETS;

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
const TRANSPORT_MANAGER* app_cellular_get_transport(void);
extern void app_cellular_init(void);
extern void app_cellular_safe_enter(void);
extern void app_cellular_online_enter(E_CELL_SESSION state);
extern void app_cellular_request_reset(void);
extern bool app_cellular_tick(void);
extern bool app_cellular_wasLastPackedPubacked(E_MQTT_PACKETS packetType);

void app_cellular_publishResponse(const uint8_t* const buffer, uint32_t dataLength);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_CELLULAR_H

