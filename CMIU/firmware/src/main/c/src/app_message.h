/******************************************************************************
*******************************************************************************
**
**         Filename: app_message.h
**    
**           Author: Duncan Willis
**          Created: 9 Nov 2015
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


#ifndef  __APP_MESSAGE_H
#define  __APP_MESSAGE_H

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


typedef enum CMIU_INTERFACE 
{
        CMIU_INTERFACE_CELLULAR = 0,
        CMIU_INTERFACE_BTLE = 1,
        CMIU_INTERFACE_UART = 2,
    
        CMIU_INTERFACE_COUNT
} CMIU_INTERFACE;



typedef struct APP_RX_MSG
{
    const uint8_t*  pData; 
    uint32_t        dataLength;
    CMIU_INTERFACE  source;
} APP_RX_MSG;



typedef struct APP_TX_MSG
{
    uint8_t*        pData; 
    uint32_t        bufferSize;
    uint32_t        dataLength;
    CMIU_INTERFACE  destination;
} APP_TX_MSG;



/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
 


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
void app_message_on_receive(APP_RX_MSG* pReceiveMsg);
void app_message_send_response(const APP_TX_MSG* pResponseMsg);




/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_MESSAGE_H
