/******************************************************************************
*******************************************************************************
**
**         Filename: app_cmiu.c
**    
**           Author: Troy Harstad
**          Created: 1/26/2015
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
** Revision History:
**        1/26/2015: First created
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_cmiu.c
*
* @brief Performs CMIU initialization, executes main loop
* 
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include "typedefs.h"
#include "efm32lg332f256.h"
#include "stdtypes.h"
#include "autogen_init.h"
#include "app_arb.h"
#include "cmiuFlash.h"
#include "cmiuSpi.h"
#include "app_ble.h"

#include "app_uart.h"
#include "app_timers.h"
#include "app_power.h"
#include "app_rtc.h"
#include "app_gpio.h"
#include "app_cmu.h"
#include "app_cmiu_app.h"
#include "app_error.h"
#include "app_metrology.h"
#include "app_ble.h"
#include "app_operatingMode.h"
#include "app_packet_builder.h"
#include "app_packet_parser.h"
#include "app_wdog.h"
#include "app_rmu.h"
#include "app_datalog.h"
#include "CmiuAppConfiguration.h"
#include "CmiuAppImageInfo.h"
#include "CmiuBleConfiguration.h"
#include "app_command.h"
#include "app_cellular.h"
#include "app_scheduler.h"
#include "app_adc.h"
#include "app_cmit_interface.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "em_timer.h"
#include "DebugTrace.h"

// Modem library header files
#include "modem.h"
#include "modem_diags.h"
#include "modem_power.h"
#include "modem_uart.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static void app_cmiu_init(void);
static void app_cmiu_displayVersions(void);
#if defined(DATALOG_TESTING)
static void app_cmiu_test_datalog(void);
#endif


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h
// Let the compiler check the typedefs.  For instance if int8_t ever becomes
// something other than 1 byte the [sizeof(int8_t) == 1] will become false,
// which means the union member int8_err will have an array size of zero.
// The compiler will give an error if the array size is set to zero.
// PC-lint will give several errors for this since it is an incomplete array
// and the mebers are not referenced so it is removed from the lint check using
// the #ifndef _lint directive.
#ifndef _lint
union {
    char int8_err   [sizeof(int8_t)    == 1];
    char uint8_err  [sizeof(uint8_t)   == 1];
    char int16_err  [sizeof(int16_t)   == 2];
    char uint16_err [sizeof(uint16_t)  == 2];
    char int32_err  [sizeof(int32_t)   == 4];
    char uint32_err [sizeof(uint32_t)  == 4];
    char uint64_err [sizeof(uint64_t)  == 8];
    char int64_err  [sizeof(int64_t)   == 8];
    char ptr_err    [sizeof(uintptr_t) == 4];
} forceErrorSometimes_t ; //Anonymous
#endif

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None



/***************************************************************************//**
 * @brief
 *   main function
 *
 * @note
 *   Executes main loop once per second, sleeps in EM2 duration of one second
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
int main(void)
{    
    /* Initialize chip */
    eADesigner_Init();
    gpio_Init();
    Uart_Uart1Setup();
    
    USART_Enable(USART2, usartEnable);   
  
    // Initialize the CMIT link before outputting any debug messages
    DebugTraceInit();
    app_cmit_interface_init();
    
    DEBUG_OUTPUT_TEXT("CMIU RESET \r\n"); 
    DEBUG_OUTPUT_TEXT_AND_DATA("CMIU ID: ", (uint8_t *)&puMiuId->dwMiuId,sizeof(U_MIU_ID));  
    
    // Display image version
    app_cmiu_displayVersions();

    // Initialize CMIU, this overwrites some autogen GPIO/Clock settings in an 
    // effort to configure the board to reach 2uA sleep current.
    app_cmiu_init();   
    
    // Moving this to the end since it causes the unit to hang upon reset if 
    // ringbuffer isn't initialized first.  Still need enable interrupt only when
    // needed!!!
    modem_uart0_setup();
    
    // Enable watchdog timer
    app_wdog_enable();

    // Infinite Loop
    while(1)
	{   

        // Set PA.4 upon wakeup
        DEBUG_INSTRUCTION(GPIO_PinOutSet(gpioPortA, 4));     

        // Toggle PA.3 on every wakeup (1 second)
        DEBUG_INSTRUCTION(GPIO_PinOutToggle(gpioPortA, 3));
        
        // Feed watchdog timer
        WDOG_Feed();
        
        // Tick operating mode
        #if defined(DATALOG_TESTING)
        app_cmiu_test_datalog();
        #else
        app_operatingMode_tick();
        #endif
        
        // Wait for UART to complete before entering sleep mode
        DEBUG_WAITFORUART();                        
        
        // Clear PA.4 before going to sleep
        DEBUG_INSTRUCTION(GPIO_PinOutClear(gpioPortA, 4));          
       
        // Enter EM2 and wake up up to one second later
        EMU_EnterEM2(true);    
        
	}   
}


/***************************************************************************//**
 * @brief
 *   Performs overall CMIU initialization - besides what is being done in
 *   autogen modules.
 *
 * @note
 *       
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_cmiu_init(void)
{
    uint32_t  startDelay;  
    
    // CMU should be initialized first since if a module clock is disabled, the
    // registers of that module are not accessible and reading from such 
    // registers may return undefined values. Writing to registers of clock 
    // disabled modules have no effect. One should normally avoid accessing
    // module registers of a module with a disabled clock.
    app_cmu_init();
    
    // Init RMU module
    app_rmu_init();
    
    // Initialize RTC module
    app_rtc_init();
    
    // Initialize GPIO module
    app_gpio_init();

    // Set the flash module to a known state
    Flash_startup();
    
    // Initialize adc module
    app_adc_init();
    
    // Initialize Timers module
    app_timers_init();
    
    // Initialize scheduler 
    app_scheduler_init();
    
    // Initialize ARB module
    ARB_Init();
    
    // Initialize Metrology module
    app_metrology_Init();
 
    // Initialize Error module 
    Error_Init();

    // Init SPI controller. This needs to be done before anything that uses
    // SPI (flash, datalog, ble) is enabled.
    cmiuSpi_init();

    // Initialize the datalog module
    app_datalog_init();
    
    // Initialize the bluetooth module
    app_ble_init();
       
    // Initialize App module   
    app_cmiu_app_init();

    // Initialize Operating Mode module  
    app_operatingMode_init();
    
    // Initialize packet modules
    app_packet_builder_init();
    app_packet_parser_init();
    
    // Initialize modem module
    modem_init();
	
	// Initialize modem diagnostics
    modem_diag_init();
    
    // Initialize command module
    app_command_init();
    
    // Initialize cellular module
    app_cellular_init();
    
    // Initialize wdog module
    app_wdog_init();
   
    // Delay before possible sleep so that debugger can attach to target
    for(startDelay = 0x1FFFFF; startDelay>0; startDelay--)
    {
        __NOP();
    } 
    
    DEBUG_OUTPUT_TEXT("CMIU Initialization is complete\n\r");
    
}

 

static void app_cmiu_displayVersions(void)
{
    const S_MIU_IMG_INFO *version;
    char versionToPrint[60];    
    
// Display App image version
    version = (S_MIU_IMG_INFO *) APPLICATION_INFO;

    sprintf(versionToPrint,
            "CMIU APP        v%x.%x.%x.%x\n\r",
            version->versionMajor,
            version->versionMinor,
            version->versionYearMonthDay,
            version->versionBuild);
    
    DEBUG_OUTPUT_TEXT(versionToPrint);
    
    
// Display Config image version
    version = (S_MIU_IMG_INFO *) CONFIGURATION_INFO;

    sprintf(versionToPrint,
            "CMIU CONFIG     v%x.%x.%x.%x\n\r",
            version->versionMajor,
            version->versionMinor,
            version->versionYearMonthDay,
            version->versionBuild);
    
    DEBUG_OUTPUT_TEXT(versionToPrint);
  

// Display Config image version
    version = (S_MIU_IMG_INFO *) KEY_TABLE_INFO;

    sprintf(versionToPrint,
            "CMIU ENCRYPTION v%x.%x.%x.%x\n\r",
            version->versionMajor,
            version->versionMinor,
            version->versionYearMonthDay,
            version->versionBuild);
    
    DEBUG_OUTPUT_TEXT(versionToPrint);

    
}

#if defined(DATALOG_TESTING)
/***************************************************************************//**
 * @brief
 *   Integrated test for validating the datalog.
 *
 * @note
 *   This is kinda ugly, but it tests whether the datalog record storage and
 *   retrieval functions are working correctly. It does this by setting a
 *   starting record to "1," then incrementing that record. This shoul be
 *   called once a second. Each time it is called it simulates a unit with
 *   the advanced configuration.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_cmiu_test_datalog(void)
{
    #include "app_scratch_ram.h"
    #include "app_datalog.h"

    static APP_TX_MSG testMsg;
    static uint32_t record_value = 1;
    static bool status = true;
    static uint8_t packet_dither = 1;

    testMsg.pData       = app_scratch_ram_get(&testMsg.bufferSize);
    testMsg.dataLength  = 0;
    testMsg.destination = CMIU_INTERFACE_UART;

    packet_dither = 15 + (uint8_t)(rand() % 3);

    Flash_init();

    uint8_t i;
    for (i = 0; i < packet_dither; ++i)
    {
        uint32_t storage_value = app_datalog_formatRecord(record_value++,
                                   0x00u,
                                   0x00);

        status = app_datalog_setCurrentRecord(storage_value);

        if (status)
        {
            status = app_datalog_storeCurrentRecord();
        }
    }

    testMsg.dataLength = app_packet_builder_build_intervalData(testMsg.pData,
                testMsg.bufferSize);

    status = ((0 < testMsg.dataLength) && (1u < (rand() % 100)));
    status = app_datalog_cleanBackLog(status);

    Flash_Deinit();

    app_message_send_response(&testMsg);

    app_cmit_interface_tick(CMIU_OPERATINGMODE_SWIPE_MSEC_IN_TICK);
}
#endif /** DATALOG_TESTING */



