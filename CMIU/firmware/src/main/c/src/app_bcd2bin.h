/******************************************************************************
*******************************************************************************
**
**         Filename: app_bcd2bin.h
**    
**           Author: Troy Harstad
**          Created: 1/20/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**        1/20/2015: First created
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/

// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_BCD2BIN_H
#define __APP_BCD2BIN_H



/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_bcd2bin(uint8_t *BCD, uint8_t start_digit, uint32_t *binary, uint8_t digits);
extern void app_BCDtoBIN(uint8_t *BCD, uint32_t *binary, uint8_t digits);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_BCD2BIN_H

