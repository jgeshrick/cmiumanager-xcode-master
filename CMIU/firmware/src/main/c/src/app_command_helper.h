/******************************************************************************
*******************************************************************************
**
**         Filename: app_command_helper.h
**    
**           Author: Troy Harstad
**          Created: 12/21/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_COMMAND_HELPER_H
#define __APP_COMMAND_HELPER_H


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
#define USER_DATA_START     0x0FE00000u
#define USER_DATA_LENGTH    0x00000800u
#define USER_DATA_END       ((USER_DATA_START + USER_DATA_LENGTH) - 1u)

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern bool app_command_helper_DoWeNeedNewConfigImage(S_RECORDING_REPORTING_INTERVAL const * const p_interval);

extern bool app_command_helper_UpdateNewConfigImage (uint16_t    reportingStartMins,
                                                   uint32_t    reportingIntervalSecs,
                                                   uint8_t     reportingTransmitWindowsQuarterHrs,
                                                   uint32_t    recordingIntervalSecs);

extern void app_command_helper_powerOnModem (void);
extern void app_command_helper_powerOffModem (void);

extern void app_command_helper_push_to_user_data(uint32_t * p_buffer, uint32_t len);
extern bool app_command_helper_validate_internal_config(void);
extern void app_command_helper_corrupt_internal_configuration(void);


#endif // __APP_COMMAND_HELPER_H

