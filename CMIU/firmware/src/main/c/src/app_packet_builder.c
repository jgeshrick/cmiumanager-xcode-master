/******************************************************************************
*******************************************************************************
**
**         Filename: app_packet_builder.c
**    
**           Author: Troy Harstad
**          Created: 4/15/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_packet.c
*
* @brief This file contains the application code used to build various
*    packets
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include <stdbool.h>
#include "typedefs.h"

#include "em_assert.h"
#include "TransportManager.h"
#include "CmiuAppConfiguration.h"
#include "CmiuAppImageInfo.h"
#include "app_rtc.h"
#include "app_adc.h"
#include "modem.h"
#include "Tpbp.h"
#include "TcpSessionManager.h"
#include "app_packet_builder.h"
#include "app_cmit_interface.h"
#include "app_rmu.h"
#include "app_arb.h"
#include "app_metrology.h"
#include "app_datalog.h"
#include "app_cellular.h"  ///@todo review, added by dew
#include "app_scratch_ram.h"
#include "app_scheduler.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


extern const uint32_t adwDummyReadings[];



/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
// Tag handler functions
static bool app_packet_builder_tagHandler_tag01_cmiuPacketHeader(S_TPBP_PACKER* const pPacker);
static bool app_packet_builder_tagHandler_tag23_cmiuDiagonstics(S_TPBP_PACKER* const pPacker);
static bool app_packet_builder_tagHandler_tag26_cmiuFlags(S_TPBP_PACKER* const pPacker);
static bool app_packet_builder_tagHandler_tag27_reportedDeviceConfiguration(S_TPBP_PACKER* const pPacker);
static bool app_packet_builder_tagHandler_tag28_r900IntervalDataAndFlags(S_TPBP_PACKER* const pPacker);
static bool app_packet_builder_tagHandler_tag31_connectionTimingLog(S_TPBP_PACKER* const pPacker);
static bool app_packet_builder_tagHandler_tag32_command(S_TPBP_PACKER* const pPacker,
                                                        E_COMMAND      const command);
static bool app_packet_builder_tagHandler_tag34_intervalRecordingConfiguration(S_TPBP_PACKER* const pPacker);
static bool app_packet_builder_tagHandler_tag35_packetHeader(S_TPBP_PACKER* const pPacker);
static bool app_packet_builder_tagHandler_tag39_firmwareRevision(S_TPBP_PACKER* const pPacker);
static bool app_packet_builder_tagHandler_tag40_bootloaderRevision(S_TPBP_PACKER* const pPacker);
static bool app_packet_builder_tagHandler_tag41_configRevision(S_TPBP_PACKER* const pPacker);
static bool app_packet_builder_tagHandler_tag44_cmiuInformation(S_TPBP_PACKER* const pPacker);
static bool app_packet_builder_tagHandler_tag48_recordingReportingInterval(
                                    S_TPBP_PACKER* const pPacker, 
                                    S_RECORDING_REPORTING_INTERVAL* pIntervals);
static bool app_packet_builder_tagHandler_tag51_errorCode(S_TPBP_PACKER* const pPacker,
                                                          E_ERROR_TAG    const errorCode);
static bool app_packet_builder_tagHandler_tag72_packetInstigator(S_TPBP_PACKER* const pPacker);

// Tag support functions
static uint16_t app_packet_builder_tagSupport_getflags(void);
static void app_packet_builder_prefetch_datalog(S_TPBP_PACKER* const pPacker,
                                        const E_PACKET_TYPE_ID packetTypeId);
static void app_packet_builder_get_intervals(S_RECORDING_REPORTING_INTERVAL* pIntervals);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
/* Buffer for assembling packets */
//static uint8_t sharedPacketBuffer[PACKET_BUFFER_SIZE] SCRATCH_RAM_VARS;
//extern uint8_t sharedPacketBuffer[] ;

// Instantiate a packer object for the detailed config packet
//static S_TPBP_PACKER detailedConfigPacker;

// Instantiate a packer object for the interval data packet
//static S_TPBP_PACKER packer;

// Instantiate a packer object for the general packet
//static S_TPBP_PACKER generalPacker;


// Structures for packet data
static S_CMIU_PACKET_HEADER sCmiuPacketHeader;
static S_CMIU_DIAGNOSTICS sDiagnostics;    
static S_CONNECTION_TIMING_LOG sConnectionTimingLog;
static S_INTERVAL_RECORDING_CONFIG sIntervalRecordingConfig;
static S_REPORTED_DEVICE_CONFIG sReportedDeviceConfig;
static S_CMIU_STATUS sCmiuStatusFlags;
static S_CMIU_INFORMATION sCmiuInformation;
static S_CMIU_BASIC_CONFIGURATION sCmiuBasicConfiguration;
static S_RECORDING_REPORTING_INTERVAL recordingReportingInterval;

// Global for Packet Instigator
static E_PACKET_INSTIGATOR gPacketInstigator;


// Query results    
char queryBuffer[256];
QUERY_STRING_RESULT queryResultStr;
QUERY_NUMBER_RESULT queryResultNum;    


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/



/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/



/***************************************************************************//**
 * @brief
 *   Initializes packet module
 *
 * @note
 *   
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_packet_builder_init(void)
{
    queryResultStr.buffer = queryBuffer;
    queryResultStr.buffSize = sizeof(queryBuffer);
    gPacketInstigator = E_PACKET_INSTIGATOR_NORMAL;
}

/***************************************************************************//**
 * @brief
 *   Sets the packet instigator for the app_packet_builder module.
 *
 * @note
 *   If the packet instigator is not a valid value, the value will default to 
 *   the E_PACKET_INSTIGATOR_NORMAL value. 
 *   
 *
 * @param[in] packetInstigator - packet instigator
 *
 * @return None
 ******************************************************************************/
void app_packet_builder_set_packet_instigator(E_PACKET_INSTIGATOR const packetInstigator)
{
    switch (packetInstigator)
    {
        case E_PACKET_INSTIGATOR_NORMAL:
        case E_PACKET_INSTIGATOR_CVS:
        case E_PACKET_INSTIGATOR_FFTS:
            gPacketInstigator = packetInstigator;
            break;
        default:
            gPacketInstigator = E_PACKET_INSTIGATOR_NORMAL;
            break;
    }
}



/***************************************************************************//**
 * @brief
 *   Build detailed configuration packet
 *
 * @note
 *   
 *
 * @param[in] pointer to TRANSPORT_MANAGER used to send packet
 *
 * @return Number of bytes written to buffer, 0 if failed
 ******************************************************************************/
uint32_t app_packet_builder_build_detailedConfiguration(uint8_t* const buffer, uint32_t bufferSize)
{ 
    uint32_t secureDataBytes;
    uint8_t* pSecureData;
    uint16_t battAdc;
    int8_t temp;
    S_TPBP_PACKER packer;
    uint32_t numBytes = 0;
    const TRANSPORT_MANAGER* pTransport;
    
    DEBUG_TRACE("Starting to build detailed config packet\r\n");
    
    
    pTransport = app_cellular_get_transport();
    
    
    // Init a packer object to start writing into the specified buffer. 
    // The buffer is zeroed.
    Tpbp_PackerInit(&packer, buffer, bufferSize);

       
    // Add packet type ID = E_PACKET_TYPE_DETAILED_CONFIG    
    if((Tpbp_Packer_BeginPacket(&packer, E_PACKET_TYPE_DETAILED_CONFIG)) == false)
    {
        // Error adding packet type to packet
        DEBUG_TRACE("ERROR adding packet type\r\n");
        return 0;   
    }
    
  
// ADD TAGS FOR DETAILED CONFIG PACKET

// ****************************************************************************
// FIRST TAG IS UNSECURE    
// ****************************************************************************
    
    // TAG 01 = CMIU Packet Header is not secure so go ahead and add it
    if(app_packet_builder_tagHandler_tag01_cmiuPacketHeader(&packer/*, pTransport*/) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU packet header\r\n");
        return 0;   
    }
    
    
// ****************************************************************************    
// NEXT TAGS ARE SECURE  
// **************************************************************************** 

    // Begin secure data packing
    Tpbp_Packer_BeginSecurePack(&packer);
    
    // TAG 03 = Network Operators
    TransportManagerGetState(pTransport, QRY_GET_AVAILABLE_OPERATORS_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_NETWORK_OPERATORS, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 3\r\n");
        return 0;  
    }   
    
    // TAG 04 = Network Performance       
    TransportManagerGetState(pTransport, QRY_GET_NETWORK_PERFORMANCE_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_NETWORK_PERFORMANCE, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 4\r\n");
        return 0;  
    }   
    
    // TAG 05 = Registration Status       
    TransportManagerGetState(pTransport, QRY_GET_REGISTRATION_INFO_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_REGISTRATION_STATUS, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 5\r\n");
        return 0;  
    }
       
    // TAG 06 = MQTT Broker IP/DNS           
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_HOST_ADDRESS,uMiuMqttServerConfig.sMIUServerConfig.abyServerAddress) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 6\r\n");
        return 0;  
    }    
    
    // TAG 08 = Assigned CMIU Address      
    TransportManagerGetState(pTransport, QRY_GET_CURRENT_IP_ADDRESS_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_ASSIGNED_CMIU_ADDRESS, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 8\r\n");
        return 0;  
    }
     
    // TAG 15 = Modem Hardware Revision       
    TransportManagerGetState(pTransport, QRY_GET_MODEM_HARWARE_REV_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_MODEM_HARDWARE_REVISION, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 15\r\n");
        return 0;  
    }    
    
    // TAG 16 = Modem Model Identification Code       
    TransportManagerGetState(pTransport, QRY_GET_MODEM_MODEL_ID_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_MODEM_MODEL_IDENTIFICATION_CODE, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 16\r\n");
        return 0;  
    }        
    
    // TAG 17 = Modem Manufacturer Identification Code       
    TransportManagerGetState(pTransport, QRY_GET_MODEM_MANUFACTURER_ID_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_MODEM_MANUFACTURER_ID_CODE, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 17\r\n");
        return 0;  
    }  

    // TAG 18 = Modem Software Revision Number       
    TransportManagerGetState(pTransport, QRY_GET_MODEM_SOFTWARE_REV_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_MODEM_SOFTWARE_REVISION, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 18\r\n");
        return 0;  
    }        
    
    // TAG 19 = Modem Serial Number (IMEI)        
    TransportManagerGetState(pTransport, QRY_GET_MODEM_SERIAL_NUMBER_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_IMEI, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 19\r\n");
        return 0;  
    }  

    // TAG 20 = SIM IMSI      
    TransportManagerGetState(pTransport, QRY_GET_IMSI_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_SIM_IMSI, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 20\r\n");
        return 0;  
    }      

    // TAG 21 = SIM Card ID     
    TransportManagerGetState(pTransport, QRY_GET_SIM_ID_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_SIM_CARD_ID, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 21\r\n");
        return 0;  
    }      
    
    // TAG 23 = CMIU Diagnostics
    if(app_packet_builder_tagHandler_tag23_cmiuDiagonstics(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU diagnostics\r\n");
        return 0;   
    } 
    
//**************************************************************************************
// TAGS 24 and 25 NOT SUPPORTED AT THIS TIME
/*
    
    // TAG 24 = BLE last user logged in - HARD CODED FOR PRE-ALPHA !!!
    if(Tpbp_PackerAdd_CharArray(&detailedConfigPacker, E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_ID, "No User") == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 24\r\n");
        return 0;  
    }     

    // TAG 25 = BLE last user logged in date - HARD CODED FOR PRE-ALPHA !!!
    if(Tpbp_PackerAdd_UInt64(&detailedConfigPacker, E_TAG_NUMBER_BLE_LAST_USER_LOGGED_IN_DATE, 1429056000) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 25\r\n");
        return 0;          
    }  
*/    
//***************************************************************************************
    
    // TAG 26 = CMIU Flags
    if(app_packet_builder_tagHandler_tag26_cmiuFlags(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU flags\r\n");
        return 0;   
    }    
    
    // TAG 27 = Reported Device Configuration
    if(app_packet_builder_tagHandler_tag27_reportedDeviceConfiguration(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding reported device config\r\n");
        return 0;   
    }        
    
    // TAG 30 = Error Log - HARD CODED FOR PRE-ALPHA, MID-ALPHA, AND BETA!!!
    // NO LONGER APPEARS IN ETI 48-01 FOR BETA !!!
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_ERROR_LOG, "No Error Test Message") == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 30\r\n");
        return 0;          
    }    

    // TAG 31 = Connection timing log
    if(app_packet_builder_tagHandler_tag31_connectionTimingLog(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding connection timing log\r\n");
        return 0;   
    }   

    // TAG 34 = Interval recording
    if(app_packet_builder_tagHandler_tag34_intervalRecordingConfiguration(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 34\r\n");
        return 0;   
    }         

    // TAG 39 = Firmware revision
    if(app_packet_builder_tagHandler_tag39_firmwareRevision(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 39\r\n");
        return 0;
    } 

    // TAG 40 = Bootloader revision
    if(app_packet_builder_tagHandler_tag40_bootloaderRevision(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 40\r\n");
        return 0;   
    } 
    
    // TAG 41 = Config revision
    if(app_packet_builder_tagHandler_tag41_configRevision(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 41\r\n");
        return 0;   
    } 
    
    // TAG 44 = CMIU Information
    if(app_packet_builder_tagHandler_tag44_cmiuInformation(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU Information\r\n");
        return 0;   
    }   
    
    // TAG 48 = Recording and Reporting Interval
    app_packet_builder_get_intervals(&recordingReportingInterval);
    if(app_packet_builder_tagHandler_tag48_recordingReportingInterval(&packer, &recordingReportingInterval) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 48\r\n");
        return 0;   
    }  
        
    // TAG 62 = Battery Voltage
    // Get voltage
    battAdc = app_adc_vddDiv3_mostRecentTx_retrieve();
    
    if(Tpbp_PackerAdd_BatteryVoltage(&packer, battAdc) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding battery ADC\r\n");
        return 0;   
    }  
    
  
    // TAG 63 = CMIU Temperature
    // Get temperature
    temp = app_adc_intTemp_mostRecentTx_retrieve();
    
    if(Tpbp_PackerAdd_Cmiu_Temperature(&packer, temp) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding internal temperature\r\n");
        return 0;   
    }  
    
    // TAG 72 = Packet Instigator
    // Get packet instigator
    if (app_packet_builder_tagHandler_tag72_packetInstigator(&packer) == false)
    {
        DEBUG_TRACE("ERROR adding packet instigator\r\n");
        return 0;
    }
    
    
// ****************************************************************************    
// END OF TAGS LOCATED IN SECURE BLOCK
// ****************************************************************************
    
    // End of secure data
    pSecureData = Tpbp_Packer_EndSecurePack(&packer, &secureDataBytes);
    
    if(pSecureData == NULL)
    {
        // Error ending secure block
        DEBUG_TRACE("ERROR ending secure block\r\n");
        return 0;  
    }

   
    // TO ENABLE ENCRYPTION NEED TO ENCRYPT secureDataBytes AT pSecureData!!!!
    
    
// ****************************************************************************    
// END OF FILE (EOF)
// ****************************************************************************   
    
    // Add EOF tag to packet
    if(Tpbp_Packer_EndPacket(&packer) == false)
    {        
        // Error adding EOF
        DEBUG_TRACE("ERROR adding EOF to packet\r\n");
        return 0;         
    }
    
    numBytes = Tpbp_PackerGetCount(&packer);
    
    DEBUG_TRACE("Successfully built detailed config packet (%u bytes)\r\n", numBytes);    
 
    return numBytes;      
}


/***************************************************************************//**
 * @brief
 *   Build basic configuration packet
 *
 * @note
 *   
 *
 * @param[in] pointer to TRANSPORT_MANAGER used to send packet
 *
 * @return Number of bytes written to buffer, 0 if failed
 ******************************************************************************/
uint32_t app_packet_builder_build_basicConfiguration(uint8_t* const buffer, uint32_t bufferSize)
{ 
    uint32_t secureDataBytes; 
    uint8_t* pSecureData;
    uint16_t battAdc;
    int8_t temp;
    S_TPBP_PACKER packer;
    uint32_t numBytes = 0;
    
    DEBUG_TRACE("Starting to build basic config packet\r\n");
    
  
    // Init a packer object to start writing into the specified buffer. 
    // The buffer is zeroed.
    Tpbp_PackerInit(&packer, buffer, bufferSize);

       
    // Add packet type ID = E_PACKET_TYPE_BASIC_CONFIG    
    if((Tpbp_Packer_BeginPacket(&packer, E_PACKET_TYPE_BASIC_CONFIG)) == false)
    {
        // Error adding packet type to packet
        DEBUG_TRACE("ERROR adding packet type\r\n");
        return 0;   
    }
    
  
// ADD TAGS FOR BASIC CONFIG PACKET

// ****************************************************************************
// FIRST TAG IS UNSECURE    
// ****************************************************************************
    
    // TAG 01 = CMIU Packet Header is not secure so go ahead and add it
    if(app_packet_builder_tagHandler_tag01_cmiuPacketHeader(&packer/*, pTransport*/) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU packet header\r\n");
        return 0;   
    }
    
    
// ****************************************************************************    
// NEXT TAGS ARE SECURE  
// **************************************************************************** 

    // Begin secure data packing
    Tpbp_Packer_BeginSecurePack(&packer);
    
    // TAG 26 = CMIU Status
    app_packet_builder_tagHandler_tag26_cmiuFlags(&packer);
    
    // TAG 48 = Device configuration and recording and reporting interval
    app_packet_builder_get_intervals(&recordingReportingInterval);    
    if(app_packet_builder_tagHandler_tag48_recordingReportingInterval(&packer, &recordingReportingInterval) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 48\r\n");
        return 0;   
    }

    // TAG 34 = CMIU data configuration
    if(app_packet_builder_tagHandler_tag34_intervalRecordingConfiguration(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 34\r\n");
        return 0;   
    }         
   
    // TAG 23 = CMIU Diagnostics
    if(app_packet_builder_tagHandler_tag23_cmiuDiagonstics(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU diagnostics\r\n");
        return 0;   
    } 
      
    // TAG 62 = Battery Voltage
    // Get voltage
    battAdc = app_adc_vddDiv3_mostRecentTx_retrieve();
    
    if(Tpbp_PackerAdd_BatteryVoltage(&packer, battAdc) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding battery ADC\r\n");
        return 0;   
    }  
      
    // TAG 63 = CMIU Temperature
    // Get temperature
    temp = app_adc_intTemp_mostRecentTx_retrieve();
    
    if(Tpbp_PackerAdd_Cmiu_Temperature(&packer, temp) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding internal temperature\r\n");
        return 0;   
    }  

    // TAG 27 = Reported Device Configuration
    if(app_packet_builder_tagHandler_tag27_reportedDeviceConfiguration(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding reported device config\r\n");
        return 0;   
    }        
    
    // TAG 31 = Connection timing log
    if(app_packet_builder_tagHandler_tag31_connectionTimingLog(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding connection timing log\r\n");
        return 0;   
    }   
    
    
// ****************************************************************************    
// END OF TAGS LOCATED IN SECURE BLOCK
// ****************************************************************************
    
    // End of secure data
    pSecureData = Tpbp_Packer_EndSecurePack(&packer, &secureDataBytes);
    
    if(pSecureData == NULL)
    {
        // Error ending secure block
        DEBUG_TRACE("ERROR ending secure block\r\n");
        return 0;  
    }

   
    // TO ENABLE ENCRYPTION NEED TO ENCRYPT secureDataBytes AT pSecureData!!!!
    
    

// ****************************************************************************    
// END OF FILE (EOF)
// ****************************************************************************   
    
    // Add EOF tag to packet
    if(Tpbp_Packer_EndPacket(&packer) == false)
    {        
        // Error adding EOF
        DEBUG_TRACE("ERROR adding EOF to packet\r\n");
        return 0;         
    }
    
    numBytes = Tpbp_PackerGetCount(&packer);
    
    DEBUG_TRACE("Successfully built basic config packet (%u bytes)\r\n", numBytes);    
 
    return numBytes;      
}


/***************************************************************************//**
 * @brief
 *   Build interval data packet
 *
 * @note
 *   
 *
 * @param[in] pointer to TRANSPORT_MANAGER used to send packet
 *
 * @return bool - true if packet built successfully, false if not
 ******************************************************************************/
uint32_t app_packet_builder_build_intervalData(uint8_t* const buffer, uint32_t bufferSize)
{  
    uint32_t secureDataBytes; 
    uint8_t* pSecureData; 
    S_TPBP_PACKER packer;
    uint32_t numBytes = 0; 
    DEBUG_TRACE("Starting to build interval data packet\r\n");

    Tpbp_PackerInit(&packer, buffer, bufferSize);
    app_packet_builder_prefetch_datalog(&packer, E_PACKET_TYPE_INTERVAL_DATA);
       
    // Add packet type ID = E_PACKET_TYPE_INTERVAL_DATA    
    if((Tpbp_Packer_BeginPacket(&packer, E_PACKET_TYPE_INTERVAL_DATA)) == false)
    {
        // Error adding packet type to packet
        DEBUG_TRACE("ERROR adding packet type\r\n");
        return 0;   
    }
    

// ADD TAGS FOR INTERVAL DATA PACKET

// ****************************************************************************
// FIRST TAG IS UNSECURE    
// ****************************************************************************
    
    // TAG 01 = CMIU Packet Header is not secure so go ahead and add it
    if(app_packet_builder_tagHandler_tag01_cmiuPacketHeader(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU packet header\r\n");
        return 0;   
    }
    
    
// ****************************************************************************    
// NEXT TAGS ARE SECURE   
// ****************************************************************************   
    
    // Begin secure data packing
    Tpbp_Packer_BeginSecurePack(&packer);
    
    
    // TAG 26 = CMIU Flags
    if(app_packet_builder_tagHandler_tag26_cmiuFlags(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU flags\r\n");
        return 0;   
    }  

     // TAG 27 = Reported Device Configuration
    if(app_packet_builder_tagHandler_tag27_reportedDeviceConfiguration(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding reported device config\r\n");
        return 0;   
    } 

    // TAG 34 = Interval Recording Configuration
    if(app_packet_builder_tagHandler_tag34_intervalRecordingConfiguration(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding interval recording config\r\n");
        return 0;   
    } 

    // TAG 28 = R900 Interval Data and Flags
    if(app_packet_builder_tagHandler_tag28_r900IntervalDataAndFlags(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding r900 interval data and flags\r\n");
        return 0;   
    } 
            
    
// ****************************************************************************    
// END OF TAGS LOCATED IN SECURE BLOCK - ADD SECURE BLOCK TO PACKET
// ****************************************************************************
    
    // End of secure data
    pSecureData = Tpbp_Packer_EndSecurePack(&packer, &secureDataBytes);
    
    if(pSecureData == NULL)
    {
        // Error ending secure block
        DEBUG_TRACE("ERROR ending secure block\r\n");
        return 0;  
    }

   
    // TO ENABLE ENCRYPTION NEED TO ENCRYPT secureDataBytes AT pSecureData!!!!
    

// ****************************************************************************    
// END OF FILE (EOF)
// ****************************************************************************   
    
    if(Tpbp_Packer_EndPacket(&packer) == false)
    {        
        DEBUG_TRACE("ERROR adding EOF to packet\r\n");
        return 0;         
    }
    
    numBytes = Tpbp_PackerGetCount(&packer);
    
    DEBUG_TRACE("Successfully built interval data packet (%u bytes)\r\n", numBytes);    

    return numBytes;   
}


/***************************************************************************//**
 * @brief
 *   Build CAN data packet
 *
 * @note
 *   This packet contains all the number identifiers from the CMIU and modem
 *
 * @param[in] pointer to the buffer for the response messsage
 * @param[in] size of the buffer
 *
 * @return uint32_t - number of bytes in the response message
 ******************************************************************************/
uint32_t app_packet_builder_build_canData(uint8_t* const buffer, uint32_t bufferSize)
{  
    uint32_t secureDataBytes; 
    uint8_t* pSecureData; 
    S_TPBP_PACKER packer;
    uint32_t numBytes = 0; 
    const TRANSPORT_MANAGER* pTransport;
       
    pTransport = app_cellular_get_transport();
    
    DEBUG_TRACE("Starting to build CAN data packet\r\n");

    Tpbp_PackerInit(&packer, buffer, bufferSize);
       
    // Add packet type ID = E_PACKET_TYPE_RESPONSE    
    if((Tpbp_Packer_BeginPacket(&packer, E_PACKET_TYPE_RESPONSE)) == false)
    {
        // Error adding packet type to packet
        DEBUG_TRACE("ERROR adding packet type\r\n");
        return 0;   
    }
    

// ADD TAGS FOR CAN DATA PACKET

// ****************************************************************************
// FIRST TAG IS UNSECURE    
// ****************************************************************************
    
    // TAG 01 = CMIU Packet Header is not secure so go ahead and add it
    if(app_packet_builder_tagHandler_tag01_cmiuPacketHeader(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU packet header\r\n");
        return 0;   
    }
    
    
// ****************************************************************************    
// NEXT TAGS ARE SECURE   
// ****************************************************************************   
    
    // Begin secure data packing
    Tpbp_Packer_BeginSecurePack(&packer);
    
    // TAG 32 = command
    if (app_packet_builder_tagHandler_tag32_command(&packer, E_COMMAND_GET_CAN_DATA) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding command\r\n");
        return 0;   
    }  

    // TAG 51 = error
    if(app_packet_builder_tagHandler_tag51_errorCode(&packer, E_ERROR_NONE) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding error code\r\n");
        return 0;   
    } 
    
    // TAG 68 CMIU ID
    if(Tpbp_PackerAdd_UInt(&packer, E_TAG_NUMBER_CMIU_ID, puMiuId->dwMiuId, sizeof(uint32_t)) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 68\r\n");
        return 0;  
    }  
    
    // TAG 19 = Modem Serial Number (IMEI)        
    TransportManagerGetState(pTransport, QRY_GET_MODEM_SERIAL_NUMBER_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_IMEI, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 19\r\n");
        return 0;  
    }  
    
    // TAG 21 = SIM Card ID     
    TransportManagerGetState(pTransport, QRY_GET_SIM_ID_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_SIM_CARD_ID, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 21\r\n");
        return 0;  
    }      

    // TAG 37 = MSISDN 
    TransportManagerGetState(pTransport, QRY_GET_MSISDN_STR, &queryResultStr);   

    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_MSISDN_REQUEST, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 37\r\n");
        return 0;  
    }      



    // TAG 20 = SIM IMSI      
    TransportManagerGetState(pTransport, QRY_GET_IMSI_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_SIM_IMSI, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 20\r\n");
        return 0;  
    }      

    
    // TAG 69 = APN
    TransportManagerGetState(pTransport, QRY_GET_CURRENT_APN_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_CMIU_APN, queryResultStr.buffer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 69\r\n");
        return 0;          
    }    
    
    
    
// ****************************************************************************    
// END OF TAGS LOCATED IN SECURE BLOCK - ADD SECURE BLOCK TO PACKET
// ****************************************************************************
    
    // End of secure data
    pSecureData = Tpbp_Packer_EndSecurePack(&packer, &secureDataBytes);
    
    if(pSecureData == NULL)
    {
        // Error ending secure block
        DEBUG_TRACE("ERROR ending secure block\r\n");
        return 0;  
    }

   
    // TO ENABLE ENCRYPTION NEED TO ENCRYPT secureDataBytes AT pSecureData!!!!
    

// ****************************************************************************    
// END OF FILE (EOF)
// ****************************************************************************   
    
    if(Tpbp_Packer_EndPacket(&packer) == false)
    {        
        DEBUG_TRACE("ERROR adding EOF to packet\r\n");
        return 0;         
    }
    
    numBytes = Tpbp_PackerGetCount(&packer);
    
    DEBUG_TRACE("Successfully built CAN data packet (%u bytes)\r\n", numBytes);    

    return numBytes;   
}

/***************************************************************************//**
 * @brief
 *   Build command response packet for ReadConnectedDevice command
 *
 * @note
 *
 *
 * @param[out] buffer to be populated
 * @param[in] bufferSize Size of buffer
 *
 * @return number of bytes written to buffer, or 0 if failed to pack OK.
 ******************************************************************************/
uint32_t app_packet_builder_build_readConnectedDeviceResponse(
    uint8_t* const buffer,
    uint32_t bufferSize,
    E_ERROR_TAG   errorCode)
{
    uint32_t secureDataBytes;
    uint8_t* pSecureData;
    S_TPBP_PACKER packer;
    uint32_t numBytes = 0;

    EFM_ASSERT(NULL != buffer);

    DEBUG_TRACE("Starting to build command response packet\r\n");

    Tpbp_PackerInit(&packer, buffer, bufferSize);

    // Add packet type ID = E_PACKET_TYPE_RESPONSE
    if((Tpbp_Packer_BeginPacket(&packer, E_PACKET_TYPE_RESPONSE)) == false)
    {
        // Error adding packet type to packet
        DEBUG_TRACE("ERROR adding packet type\r\n");
        return 0;
    }

// ****************************************************************************
// FIRST TAG IS UNSECURE
// ****************************************************************************

    // TAG 01 = CMIU Packet Header is not secure so go ahead and add it
    if(app_packet_builder_tagHandler_tag01_cmiuPacketHeader(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU packet header\r\n");
        return 0;
    }


// ****************************************************************************
// NEXT TAGS ARE SECURE
// ****************************************************************************

    // Begin secure data packing
    Tpbp_Packer_BeginSecurePack(&packer);


    // TAG 32 = command
    if (app_packet_builder_tagHandler_tag32_command(&packer, E_COMMAND_READ_CONNECTED_DEVICES) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding command\r\n");
        return 0;
    }

    // TAG 51 = error
    if(app_packet_builder_tagHandler_tag51_errorCode(&packer, errorCode) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding error code\r\n");
        return 0;
    }

    // TAG 27 = Reported Device Configuration
    if(app_packet_builder_tagHandler_tag27_reportedDeviceConfiguration(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding reported device config\r\n");
        return 0;
    }

// ****************************************************************************
// END OF TAGS LOCATED IN SECURE BLOCK - ADD SECURE BLOCK TO PACKET
// ****************************************************************************

    // End of secure data
    pSecureData = Tpbp_Packer_EndSecurePack(&packer, &secureDataBytes);

    if(pSecureData == NULL)
    {
        // Error ending secure block
        DEBUG_TRACE("ERROR ending secure block\r\n");
        return 0;
    }


    // TO ENABLE ENCRYPTION NEED TO ENCRYPT secureDataBytes AT pSecureData!!!!


// ****************************************************************************
// END OF FILE (EOF)
// ****************************************************************************

    // Add EOF tag to packet
    if(Tpbp_Packer_EndPacket(&packer) == false)
    {
        // Error adding EOF
        DEBUG_TRACE("ERROR adding EOF to packet\r\n");
        return 0;
    }

    numBytes = Tpbp_PackerGetCount(&packer);

    // All data added to packet successfully
    DEBUG_TRACE("Successfully built ReadConnectedDevice response packet (%u bytes)\r\n", numBytes);

    return numBytes;
}


/***************************************************************************//**
 * @brief
 *   Build command response packet for Get Signal Quality command
 *
 * @note
 *
 *
 * @param[out] buffer to be populated
 * @param[in] bufferSize Size of buffer
 *
 * @return number of bytes written to buffer, or 0 if failed to pack OK.
 ******************************************************************************/
uint32_t app_packet_builder_build_signalQualityResponse(
    uint8_t* const buffer,
    uint32_t bufferSize)
{
    uint32_t secureDataBytes;
    uint8_t* pSecureData;
    S_TPBP_PACKER packer;
    uint32_t numBytes = 0;
    const TRANSPORT_MANAGER* pTransport;
    

    EFM_ASSERT(NULL != buffer);
    
    pTransport = app_cellular_get_transport();

    DEBUG_TRACE("Starting to build command response packet\r\n");

    Tpbp_PackerInit(&packer, buffer, bufferSize);

    // Add packet type ID = E_PACKET_TYPE_RESPONSE
    if((Tpbp_Packer_BeginPacket(&packer, E_PACKET_TYPE_RESPONSE)) == false)
    {
        // Error adding packet type to packet
        DEBUG_TRACE("ERROR adding packet type\r\n");
        return 0;
    }

// ****************************************************************************
// FIRST TAG IS UNSECURE
// ****************************************************************************

    // TAG 01 = CMIU Packet Header is not secure so go ahead and add it
    if(app_packet_builder_tagHandler_tag01_cmiuPacketHeader(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU packet header\r\n");
        return 0;
    }


// ****************************************************************************
// NEXT TAGS ARE SECURE
// ****************************************************************************

    // Begin secure data packing
    Tpbp_Packer_BeginSecurePack(&packer);

        // TAG 32 = command
    if (app_packet_builder_tagHandler_tag32_command(&packer, E_COMMAND_GET_CMIU_SIGNAL_QUALITY) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding command\r\n");
        return 0;
    }

    // TAG 51 = error
    if(app_packet_builder_tagHandler_tag51_errorCode(&packer, E_ERROR_NONE) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding error code\r\n");
        return 0;
    }

    // TAG 04 = Network Performance       
    TransportManagerGetState(pTransport, QRY_GET_NETWORK_PERFORMANCE_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_NETWORK_PERFORMANCE, queryResultStr.buffer) == false)
    {
       // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 4\r\n");
        return 0;  
    }   
    
// ****************************************************************************
// END OF TAGS LOCATED IN SECURE BLOCK - ADD SECURE BLOCK TO PACKET
// ****************************************************************************

    // End of secure data
    pSecureData = Tpbp_Packer_EndSecurePack(&packer, &secureDataBytes);

    if(pSecureData == NULL)
    {
        // Error ending secure block
        DEBUG_TRACE("ERROR ending secure block\r\n");
        return 0;
    }


    // TO ENABLE ENCRYPTION NEED TO ENCRYPT secureDataBytes AT pSecureData!!!!


// ****************************************************************************
// END OF FILE (EOF)
// ****************************************************************************

    // Add EOF tag to packet
    if(Tpbp_Packer_EndPacket(&packer) == false)
    {
        // Error adding EOF
        DEBUG_TRACE("ERROR adding EOF to packet\r\n");
        return 0;
    }

    numBytes = Tpbp_PackerGetCount(&packer);

    // All data added to packet successfully
    DEBUG_TRACE("Successfully built Get Signal Quality response packet (%u bytes)\r\n", numBytes);

    return numBytes;
}

/***************************************************************************//**
 * @brief
 *   Build command response packet for Request Apn command
 *
 * @note
 *
 *
 * @param[out] buffer to be populated
 * @param[in] bufferSize Size of buffer
 *
 * @return number of bytes written to buffer, or 0 if failed to pack OK.
 ******************************************************************************/
uint32_t app_packet_builder_build_requestApnResponse(
    uint8_t* const buffer,
    uint32_t bufferSize)
{
    uint32_t secureDataBytes;
    uint8_t* pSecureData;
    S_TPBP_PACKER packer;
    uint32_t numBytes = 0;
    const TRANSPORT_MANAGER* pTransport;
    

    EFM_ASSERT(NULL != buffer);
    
    pTransport = app_cellular_get_transport();

    DEBUG_TRACE("Starting to build request APN command response packet\r\n");

    Tpbp_PackerInit(&packer, buffer, bufferSize);

    // Add packet type ID = E_PACKET_TYPE_RESPONSE
    if((Tpbp_Packer_BeginPacket(&packer, E_PACKET_TYPE_RESPONSE)) == false)
    {
        // Error adding packet type to packet
        DEBUG_TRACE("ERROR adding packet type\r\n");
        return 0;
    }

// ****************************************************************************
// FIRST TAG IS UNSECURE
// ****************************************************************************

    // TAG 01 = CMIU Packet Header is not secure so go ahead and add it
    if(app_packet_builder_tagHandler_tag01_cmiuPacketHeader(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU packet header\r\n");
        return 0;
    }


// ****************************************************************************
// NEXT TAGS ARE SECURE
// ****************************************************************************

    // Begin secure data packing
    Tpbp_Packer_BeginSecurePack(&packer);
    
    // TAG 32 = command
    if (app_packet_builder_tagHandler_tag32_command(&packer, E_COMMAND_REQUEST_APN) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding command\r\n");
        return 0;   
    }  

    // TAG 69 = APN
    TransportManagerGetState(pTransport, QRY_GET_CURRENT_APN_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_CMIU_APN, queryResultStr.buffer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 69\r\n");
        return 0;          
    }    

    
// ****************************************************************************
// END OF TAGS LOCATED IN SECURE BLOCK - ADD SECURE BLOCK TO PACKET
// ****************************************************************************

    // End of secure data
    pSecureData = Tpbp_Packer_EndSecurePack(&packer, &secureDataBytes);

    if(pSecureData == NULL)
    {
        // Error ending secure block
        DEBUG_TRACE("ERROR ending secure block\r\n");
        return 0;
    }


    // TO ENABLE ENCRYPTION NEED TO ENCRYPT secureDataBytes AT pSecureData!!!!


// ****************************************************************************
// END OF FILE (EOF)
// ****************************************************************************

    // Add EOF tag to packet
    if(Tpbp_Packer_EndPacket(&packer) == false)
    {
        // Error adding EOF
        DEBUG_TRACE("ERROR adding EOF to packet\r\n");
        return 0;
    }

    numBytes = Tpbp_PackerGetCount(&packer);

    // All data added to packet successfully
    DEBUG_TRACE("Successfully built Request APN response packet (%u bytes)\r\n", numBytes);

    return numBytes;
}


/***************************************************************************//**
 * @brief
 *   Build command response packet for Update APN command
 *
 * @note
 *
 *
 * @param[out] buffer to be populated
 * @param[in] bufferSize Size of buffer
 *
 * @return number of bytes written to buffer, or 0 if failed to pack OK.
 ******************************************************************************/
uint32_t app_packet_builder_build_updateApnResponse(
    uint8_t* const buffer,
    uint32_t bufferSize)
{
    uint32_t secureDataBytes;
    uint8_t* pSecureData;
    S_TPBP_PACKER packer;
    uint32_t numBytes = 0;
    const TRANSPORT_MANAGER* pTransport;
    

    EFM_ASSERT(NULL != buffer);
    
    pTransport = app_cellular_get_transport();

    DEBUG_TRACE("Starting to build update APN command response packet\r\n");

    Tpbp_PackerInit(&packer, buffer, bufferSize);

    // Add packet type ID = E_PACKET_TYPE_RESPONSE
    if((Tpbp_Packer_BeginPacket(&packer, E_PACKET_TYPE_RESPONSE)) == false)
    {
        // Error adding packet type to packet
        DEBUG_TRACE("ERROR adding packet type\r\n");
        return 0;
    }

// ****************************************************************************
// FIRST TAG IS UNSECURE
// ****************************************************************************

    // TAG 01 = CMIU Packet Header is not secure so go ahead and add it
    if(app_packet_builder_tagHandler_tag01_cmiuPacketHeader(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU packet header\r\n");
        return 0;
    }


// ****************************************************************************
// NEXT TAGS ARE SECURE
// ****************************************************************************

    // Begin secure data packing
    Tpbp_Packer_BeginSecurePack(&packer);
    
    // TAG 32 = command
    if (app_packet_builder_tagHandler_tag32_command(&packer, E_COMMAND_UPDATE_APN) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding command\r\n");
        return 0;   
    }  

    // TAG 69 = APN
    TransportManagerGetState(pTransport, QRY_GET_CURRENT_APN_STR, &queryResultStr);   
    
    if(Tpbp_PackerAdd_CharArray(&packer, E_TAG_NUMBER_CMIU_APN, queryResultStr.buffer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 69\r\n");
        return 0;          
    }    

    
// ****************************************************************************
// END OF TAGS LOCATED IN SECURE BLOCK - ADD SECURE BLOCK TO PACKET
// ****************************************************************************

    // End of secure data
    pSecureData = Tpbp_Packer_EndSecurePack(&packer, &secureDataBytes);

    if(pSecureData == NULL)
    {
        // Error ending secure block
        DEBUG_TRACE("ERROR ending secure block\r\n");
        return 0;
    }


    // TO ENABLE ENCRYPTION NEED TO ENCRYPT secureDataBytes AT pSecureData!!!!


// ****************************************************************************
// END OF FILE (EOF)
// ****************************************************************************

    // Add EOF tag to packet
    if(Tpbp_Packer_EndPacket(&packer) == false)
    {
        // Error adding EOF
        DEBUG_TRACE("ERROR adding EOF to packet\r\n");
        return 0;
    }

    numBytes = Tpbp_PackerGetCount(&packer);

    // All data added to packet successfully
    DEBUG_TRACE("Successfully built Update APN response packet (%u bytes)\r\n", numBytes);

    return numBytes;
}

/***************************************************************************//**
 * @brief
 *   Build the buffer to shove into the user data (for configuration retrieval).
 *
 * @note
 *   
 * @param[in] interval information
 * @param[in] buffer to be populated
 * @param[in] bufferSize Size of buffer
 *
 * @return number of bytes written to buffer, or 0 if failed to pack OK.
 ******************************************************************************/
uint32_t app_packet_builder_build_userDataPacket(
    S_RECORDING_REPORTING_INTERVAL intervals, 
    uint8_t* const buffer,
    uint32_t bufferSize)
{
    uint32_t secureDataBytes; 
    uint8_t* pSecureData; 
    S_TPBP_PACKER packer;
    uint32_t numBytes = 0;
    
    EFM_ASSERT(NULL != buffer);
    
    DEBUG_TRACE("Starting to build user data packet\r\n");

    Tpbp_PackerInit(&packer, buffer, bufferSize);
       
    // Add packet type ID = E_PACKET_TYPE_COMMAND
    // @TODO Add USER_DATA packet type (this will help for CatM).
    if((Tpbp_Packer_BeginPacket(&packer, E_PACKET_TYPE_COMMAND)) == false)
    {
        // Error adding packet type to packet
        DEBUG_TRACE("ERROR adding packet type\r\n");
        return 0;   
    }
    
    // ************************************************************************
    // FIRST TAG IS UNSECURE    
    // ************************************************************************
    
    // TAG 01 = CMIU Packet Header is not secure so go ahead and add it
    if(app_packet_builder_tagHandler_tag01_cmiuPacketHeader(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU packet header\r\n");
        return 0;   
    }
    
    // add uart packet header (unsecure) (tag35)
    // add timeanddate
    // TAG 35 = MQTT/BLE/UART packet header
    if (app_packet_builder_tagHandler_tag35_packetHeader(&packer) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding command\r\n");
        return 0;   
    }  
    
    
    // ************************************************************************
    // NEXT TAGS ARE SECURE   
    // ************************************************************************
    
    // Begin secure data packing
    Tpbp_Packer_BeginSecurePack(&packer);
    
    // TAG 32 = command
    if (app_packet_builder_tagHandler_tag32_command(&packer, E_COMMAND_SET_RECORDING_AND_REPORTING_INTERVALS) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding command\r\n");
        return 0;   
    }  
    
    
    
    // TAG 48 = Device configuration and recording and reporting interval  
    if(app_packet_builder_tagHandler_tag48_recordingReportingInterval(&packer, &intervals) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding tag 48\r\n");
        return 0;   
    }
             
    
    // ************************************************************************
    // END OF TAGS LOCATED IN SECURE BLOCK - ADD SECURE BLOCK TO PACKET
    // ************************************************************************
    
    // End of secure data
    pSecureData = Tpbp_Packer_EndSecurePack(&packer, &secureDataBytes);
    
    if(pSecureData == NULL)
    {
        // Error ending secure block
        DEBUG_TRACE("ERROR ending secure block\r\n");
        return 0;  
    }

   
    // TO ENABLE ENCRYPTION NEED TO ENCRYPT secureDataBytes AT pSecureData!!!!
    

    // ************************************************************************
    // END OF FILE (EOF)
    // ************************************************************************
    
    // Add EOF tag to packet
    if(Tpbp_Packer_EndPacket(&packer) == false)
    {        
        // Error adding EOF
        DEBUG_TRACE("ERROR adding EOF to packet\r\n");
        return 0;         
    }
    
    numBytes = Tpbp_PackerGetCount(&packer);
    
    // All data added to packet successfully
    DEBUG_TRACE("Successfully built user data.");
       
    return numBytes;
}


/***************************************************************************//**
 * @brief
 *   Build command response packet
 *
 * @note
 *   
 *
 * @param[out] buffer to be populated
 * @param[in] bufferSize Size of buffer
 * @param[in] command that this response is for
 * @param[in] error result of this command
 *
 * @return number of bytes written to buffer, or 0 if failed to pack OK.
 ******************************************************************************/
uint32_t app_packet_builder_build_commandResponse(
    uint8_t* const buffer,
    uint32_t bufferSize,
    E_COMMAND     command,
    E_ERROR_TAG   errorCode)
{  
    uint32_t secureDataBytes; 
    uint8_t* pSecureData; 
    S_TPBP_PACKER packer;
    uint32_t numBytes = 0;
    
    EFM_ASSERT(NULL != buffer);

    Tpbp_PackerInit(&packer, buffer, bufferSize);
       
    // Add packet type ID = E_PACKET_TYPE_RESPONSE    
    if((Tpbp_Packer_BeginPacket(&packer, E_PACKET_TYPE_RESPONSE)) == false)
    {
        // Error adding packet type to packet
        DEBUG_TRACE("ERROR adding packet type\r\n");
        return 0;   
    }
    
// ****************************************************************************
// FIRST TAG IS UNSECURE    
// ****************************************************************************
    
    // TAG 01 = CMIU Packet Header is not secure so go ahead and add it
    if(app_packet_builder_tagHandler_tag01_cmiuPacketHeader(&packer/*, pTransport*/) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding CMIU packet header\r\n");
        return 0;   
    }
    
    
// ****************************************************************************    
// NEXT TAGS ARE SECURE   
// ****************************************************************************   
    
    // Begin secure data packing
    Tpbp_Packer_BeginSecurePack(&packer);
    
    
    // TAG 32 = command
    if (app_packet_builder_tagHandler_tag32_command(&packer, command) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding command\r\n");
        return 0;   
    }  

    // TAG 51 = error
    if(app_packet_builder_tagHandler_tag51_errorCode(&packer, errorCode) == false)
    {
        // Error adding data to packet
        DEBUG_TRACE("ERROR adding error code\r\n");
        return 0;   
    } 
             
    
// ****************************************************************************    
// END OF TAGS LOCATED IN SECURE BLOCK - ADD SECURE BLOCK TO PACKET
// ****************************************************************************
    
    // End of secure data
    pSecureData = Tpbp_Packer_EndSecurePack(&packer, &secureDataBytes);
    
    if(pSecureData == NULL)
    {
        // Error ending secure block
        DEBUG_TRACE("ERROR ending secure block\r\n");
        return 0;  
    }

   
    // TO ENABLE ENCRYPTION NEED TO ENCRYPT secureDataBytes AT pSecureData!!!!
    

// ****************************************************************************    
// END OF FILE (EOF)
// ****************************************************************************   
    
    // Add EOF tag to packet
    if(Tpbp_Packer_EndPacket(&packer) == false)
    {        
        // Error adding EOF
        DEBUG_TRACE("ERROR adding EOF to packet\r\n");
        return 0;         
    }
    
    numBytes = Tpbp_PackerGetCount(&packer);
       
    return numBytes;      
}



/***************************************************************************//**
 * @brief
 *   Inserts CMIU Packet Header tag (TAG = 1) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer to load data in to
 * @param[in] pTransport - transport manager that will be used to send packet
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag01_cmiuPacketHeader(S_TPBP_PACKER* const pPacker)
{
    bool bResult;
    U_NEPTUNE_TIME currentTime;    
    const TRANSPORT_MANAGER* pTransport; 
    
    pTransport = app_cellular_get_transport();
  
    if (false == TransportManagerIsValid(pTransport)) return false;
    
    // Load CMIU Packet Header struct with data
    sCmiuPacketHeader.cmiuId = puMiuId->dwMiuId;
    sCmiuPacketHeader.sequenceNumber = 0;
    sCmiuPacketHeader.keyInfo = 0;
    sCmiuPacketHeader.encryptionMethod = 0;
    sCmiuPacketHeader.tokenAesCrc = 0;
    sCmiuPacketHeader.networkFlags = 0;
    
    
    // Obtain RSSI and load into struct
    TransportManagerGetState(pTransport, QRY_GET_RSSI, &queryResultNum);
    sCmiuPacketHeader.cellularRssiAndBer = queryResultNum.numberResult.U16[0];
    
    // Get current Time and load into struct
    currentTime = app_rtc_time_get();
    sCmiuPacketHeader.timeAndDate = currentTime.S64;
    
  
    // Add struct data into packet
    bResult = Tpbp_PackerAdd_CmiuPacketHeader(pPacker, &sCmiuPacketHeader);
    
    return bResult;  
}


/***************************************************************************//**
 * @brief
 *   Inserts CMIU Diagnostics tag (TAG = 23) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer to load data in to
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag23_cmiuDiagonstics(S_TPBP_PACKER* const pPacker)
{
    bool bResult;
            
    // Load Diagnostics struct
    sDiagnostics.diagnosticsResult      = 0;	
    sDiagnostics.processorResetCounter  = app_rmu_resetCounter_Get();   
  
    bResult = Tpbp_PackerAdd_CmiuDiagnostics(pPacker, &sDiagnostics);
    
    return bResult;       
}


/***************************************************************************//**
 * @brief
 *   Inserts CMIU flags tag (TAG = 26) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer to load data in to
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag26_cmiuFlags(S_TPBP_PACKER* const pPacker)
{
    bool bResult;
    
    sCmiuStatusFlags.cmiuType = 0;
    sCmiuStatusFlags.numberOfDevices = 1;
    sCmiuStatusFlags.cmiuFlags = 0;  
    
    bResult = Tpbp_PackerAdd_CmiuStatus(pPacker, &sCmiuStatusFlags);
    
    return bResult; 
}


/***************************************************************************//**
 * @brief
 *   Inserts reported device configuration tag (TAG = 27) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer to load data in to
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag27_reportedDeviceConfiguration(S_TPBP_PACKER* const pPacker)
{   
    bool bResult;
    UU64 tempId;
    S_REGISTER_DATA sARBReadingData;
    
    // Retrieve device information
    sARBReadingData = *ARB_RegisterData_Retrieve();

    // Load number of devices    
    sReportedDeviceConfig.deviceNumber = 1;
    
    // Load detected register ID, abyDetectedRegisterIDDecimal is five bytes
    // Init all bytes to zero
    tempId.U64 = 0x00000000UL;
    
    // Load five bytes of detected register ID into    
    tempId.U8[4] = sARBReadingData.abyDetectedRegisterIDDecimal[0];
   
    tempId.U8[3] = sARBReadingData.abyDetectedRegisterIDDecimal[1];
    tempId.U8[2] = sARBReadingData.abyDetectedRegisterIDDecimal[2];
    tempId.U8[1] = sARBReadingData.abyDetectedRegisterIDDecimal[3];
    tempId.U8[0] = sARBReadingData.abyDetectedRegisterIDDecimal[4];    
    
    // Load device ID into struct
    sReportedDeviceConfig.attachedDeviceId = tempId.U64;
        
// Load connected device
    // If the register type is E-Coder V2 but we detected an E-Coder V1
    // then we are really connected to an EV2
    if((sARBReadingData.byRegisterType == REGISTER_ECODER2) &&
       (sARBReadingData.byDetectedRegisterType == REGISTER_ECODER))
    {
        sReportedDeviceConfig.deviceType = REGISTER_ECODER2;
    }
    
    else // Else just use detected register type
    {
        sReportedDeviceConfig.deviceType = sARBReadingData.byDetectedRegisterType;     
    }

// Load Current Device Data
    sReportedDeviceConfig.currentDeviceData = sARBReadingData.dwReadingBinary.U32;

// Load device flags
    sReportedDeviceConfig.deviceFlags = app_packet_builder_tagSupport_getflags();
    
    // Add struct data into packet
    bResult = Tpbp_PackerAdd_ReportedDeviceConfig(pPacker, &sReportedDeviceConfig);
        
    return bResult;
}


/***************************************************************************//**
 * @brief
 *   This function handles the flags
 *                      
 * @note
 *   
 *
 * @param[in] none
 *
 * @return uint16_t - flags per ETI 05-03
 ******************************************************************************/
static uint16_t app_packet_builder_tagSupport_getflags(void)
{
    UU16    uwFlagsTemp;
    uint8_t byBFMinAgeTemp;
    uint8_t byBFMaxAgeTemp; 
    uint8_t byTemp; 
    S_REGISTER_DATA *psRegisterDataTemp;    

    // Retrieve the register data from the ARB module
    psRegisterDataTemp = ARB_RegisterData_Retrieve();
    
    // Clear out all flags to start
    uwFlagsTemp.U16 = 0UL;

//*****************************************************************************    
// Reserved for future use (1-bit) - BIT6
//*****************************************************************************  
// UNUSED AT THIS TIME - CLEARED AT ENTRY OF THIS FUNCTION

//*****************************************************************************    
// Load 35-day reverse flow (2-bits) - BIT5 and BIT4
//*****************************************************************************    

    // Retrieve values of BFMinAge and BFMaxAge from Ecoder module
    byBFMinAgeTemp = app_metrology_BFMinAge_Retrieve();
    byBFMaxAgeTemp = app_metrology_BFMaxAge_Retrieve();    
    
    // Set back flow indicator
    if(byBFMaxAgeTemp <= 35)
    {
        // Large Reverse Flow Event within last 35 days occurred
        uwFlagsTemp.U8[0] |= BIT5;
    }
    
    else if(byBFMinAgeTemp <= 35)
    {
        // Small Reverse Flow Event within last 35 days occurred
        uwFlagsTemp.U8[0] |= BIT4;
    }

    else
    {
        // No reverse flow event for last 35 days
        uwFlagsTemp.U8[0] &= ~(BIT5|BIT4);
    }  

//*****************************************************************************    
// Load 24-hour reverse flow (2-bits) - BIT3 and BIT2
//*****************************************************************************    

    // This function returns a byte that relates to the backflow flag
    // 00 = no reverse flow
    // 01 = small backflow event
    // 10 = large backflow event
    byTemp = app_metrology_24Hr_Backflow_Retrieve();
    
    // Shift left 2 times so it is in correct position    
    byTemp <<= 2;
    
    // Load into flags
    uwFlagsTemp.U8[0] |= (byTemp & 0x0C); 
    
    
//*****************************************************************************    
// Load leak state (last 96-intervals) (2-bits) - BIT1 and BIT0
//*****************************************************************************

    // This function returns a byte, which contains the number of days of leak in
    // 35 days info in three bits 4,3, and 2.  Bits 1 and 0 contain the Leak current
    // values (leak over the last 96 intervals).
    byTemp = app_metrology_LeakFlags_Retrieve();

    // Load into flags
    uwFlagsTemp.U8[0] |= (byTemp & 0x03); 

//*****************************************************************************    
// Load battery flags (2-bits) - BIT7 and BIT6
//*****************************************************************************
    // The only battery flag exists in the Ecoder data (Spare 2 flag)
    // and is only supported in Plus messages (EV1,EV2)
    if((psRegisterDataTemp->byDetectedRegisterType == REGISTER_ECODER2) ||
       (psRegisterDataTemp->byDetectedRegisterType == REGISTER_ECODER))
    {
        // If Spare 2 is set, the battery flag should be set to '01'
        if(psRegisterDataTemp->bySpare2 == 1)
        {
            // Set BIT6 and BIT 7 to indicate 19 years of life used
            uwFlagsTemp.U8[1] &= ~BIT7;            
            uwFlagsTemp.U8[1] |= BIT6;

        }
        
        // Spare 2 is not set, battery flag should be '00'
        else // (psRegisterData->bySpare2 == 0)
        {
            uwFlagsTemp.U8[1] &= ~BIT7;            
            uwFlagsTemp.U8[1] &= ~BIT6;
        }
    }


//*****************************************************************************    
// Load empty pipe flags (2-bits) - BIT5 and BIT4
//*****************************************************************************
// UNUSED AT THIS TIME - CLEARED AT ENTRY OF THIS FUNCTION

//*****************************************************************************    
// Load excessive flow flags (2-bits) - BIT3 and BIT2
//*****************************************************************************
// UNUSED AT THIS TIME - CLEARED AT ENTRY OF THIS FUNCTION

//*****************************************************************************    
// Load tamper flag pipe (1-bit) - BIT1
//*****************************************************************************
// UNUSED AT THIS TIME - CLEARED AT ENTRY OF THIS FUNCTION

//*****************************************************************************    
// Reserved for future use (1-bit) - BIT0
//*****************************************************************************
// UNUSED AT THIS TIME - CLEARED AT ENTRY OF THIS FUNCTION

 return uwFlagsTemp.U16;      
}



/***************************************************************************//**
 * @brief
 *   Inserts R900 interval data and flags tag (TAG = 28) into packet
 *
 * @note
 *  This pulls any messages that haven't been sent to the server out of the
 *  datalog's backlog. Therefore, it is important for the datalog task
 *  to be run prior to this function's being used.
 *
 * @param[in] pPacker - packer to load data in to
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag28_r900IntervalDataAndFlags(S_TPBP_PACKER* const pPacker)
{
    bool bResult;
    uint32_t readingAttempts;
    uint32_t maxBytes;
    uint32_t recordsToAdd;
    uint32_t r900Data[(PACKET_BUFFER_SIZE/DATALOG_RECORD_SIZE)];

    //maxBytes should be the available space minus a minimum reserve
    maxBytes = Tpbp_PackerGetAvailable(pPacker) - (INTERVAL_DATA_FOOTER);
    //ReadingAttempts is initially 2
    readingAttempts = 2;

    // Do the following until we get a good record or we've maxed out
    // the attempts.
    do
    {
        // Get any records that haven't been sent to the server
        recordsToAdd = app_datalog_getBackLog(r900Data, maxBytes);

        // Let's only reverse arrays that are greater than 1.
        if (1u < recordsToAdd)
        {
            app_datalog_reverseArray(r900Data, recordsToAdd);
        }

        // If the most recent reading that was retrieved is not
        // the most recent record that we stored, set bResult to false.
        // To be fair, if we have a lot of churn, the most recent reading
        // retrieved may not be what we most recently put in the datalog.
        bResult = (app_datalog_getCurrentRecord() == r900Data[0]);

        if (!bResult)
        {
            DEBUG_OUTPUT_TEXT("THIS DOESNT MATCH THE CURRENT RECORD!!!");
        }
    }
    while ((0u != readingAttempts--) && (!bResult));

    DEBUG_OUTPUT_TEXT_AND_DATA("Added this many R900 datalog records: ", (uint8_t*)&recordsToAdd, 4);

    // Add readings into packet
    bResult = Tpbp_PackerAdd_ExtendedUint32Array(pPacker,
                                       E_TAG_NUMBER_R900_INTERVAL_DATA,
                                       r900Data, recordsToAdd);

    return bResult;
}

/***************************************************************************//**
 * @brief
 *  This routine pre-fetches the datalog to get an ensure that the time in
 *  tag34 is correct.
 *
 * @note
 *
 *
 * @param[in] pPacker - packer to load data in to
 * @param[in] packetTypeId - type of packet that we're loading (just in case
 *                           start using tag34 and tag28 in other packets).
 *
 * @return none
 ******************************************************************************/
static void app_packet_builder_prefetch_datalog(S_TPBP_PACKER* const pPacker, const E_PACKET_TYPE_ID packetTypeId)
{
    static volatile uint32_t records_fetched;
    volatile uint32_t maxBytes;
    uint32_t r900Data[(PACKET_BUFFER_SIZE/DATALOG_RECORD_SIZE)];

    //maxBytes will start as the maximum available space
    maxBytes = pPacker->bufferSize;

    //Now, based on the packet type, reduce the maximum available space
    // by the space of every tag EXCEPT tag28.
    switch (packetTypeId)
    {
        case E_PACKET_TYPE_INTERVAL_DATA:
        {
            maxBytes -= (S_CMIU_PACKET_HEADER_PACKED_SIZE
                       + S_CMIU_STATUS_PACKED_SIZE
                       + S_REPORTED_DEVICE_CONFIG_PACKED_SIZE
                       + S_INTERVAL_RECORDING_CONFIG_PACKED_SIZE
                       + (17u)
                       + (INTERVAL_DATA_FOOTER));
            break;
        }
        default:
        {
            maxBytes = 0u;
        }
    }

    // Prefetch any records that haven't been sent to the server
    // This should only be the current record
    if (0u != maxBytes)
    {
        records_fetched = app_datalog_getBackLog(r900Data, maxBytes);
    }
}


/***************************************************************************//**
 * @brief
 *   Inserts Connection Timing Log tag (TAG = 31) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer to load data in to
 * @param[in] pTransport - transport manager that will be used to send packet
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag31_connectionTimingLog(S_TPBP_PACKER* const pPacker)
{
    bool bResult;
    const TRANSPORT_MANAGER* pTransport;
    
    // Get the current cellular transpot manager
    pTransport = app_cellular_get_transport();  
    
    // Obtain various timing values and load into diagnostics struct    
    TransportManagerGetState(pTransport, QRY_GET_CONNECTION_TIME, &queryResultNum);
    sConnectionTimingLog.dateOfConnection = queryResultNum.numberResult.U64;
     
    TransportManagerGetState(pTransport, QRY_GET_TIME_REGISTRATION, &queryResultNum);
    sConnectionTimingLog.timeFromPowerOnToNetworkRegistration = queryResultNum.numberResult.U16[0]; 
    
    TransportManagerGetState(pTransport, QRY_GET_TIME_CONTEXT_ACTIVATION, &queryResultNum);
    sConnectionTimingLog.timeFromNetworkRegistrationToContextActivation = queryResultNum.numberResult.U16[0];

    TransportManagerGetState(pTransport, QRY_GET_TIME_SERVER_CONNECT, &queryResultNum);
    sConnectionTimingLog.timeFromContextActivationToServerConnection = queryResultNum.numberResult.U16[0];

    TransportManagerGetState(pTransport, QRY_GET_TIME_DATA_TRANSFER, &queryResultNum);
    sConnectionTimingLog.timeToTransferMessageToServer = queryResultNum.numberResult.U16[0];

    TransportManagerGetState(pTransport, QRY_GET_TIME_DISCONNECT, &queryResultNum);
    sConnectionTimingLog.timeToDisconnectAndShutdown = queryResultNum.numberResult.U16[0];
  
    // Add struct data into packet
    bResult = Tpbp_PackerAdd_ConnectionTimingLog(pPacker, &sConnectionTimingLog);
    
    return bResult;  
}


/*****************************************************************************
 * @brief
 *   Inserts command tag (TAG = 32) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer to load data in to
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag32_command(S_TPBP_PACKER* const pPacker,
                                                        E_COMMAND      const command)
{
    bool bResult;
            
    bResult = Tpbp_PackerAdd_Command(pPacker, command);
       
    return bResult;
}


/*****************************************************************************
 * @brief
 *   Inserts interval recording configuration (TAG = 34) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer to load data in to
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag34_intervalRecordingConfiguration(S_TPBP_PACKER* const pPacker)
{
    bool bResult;
    U_NEPTUNE_TIME next_time;

    // Device Number is always 1 (until we support multiple devices)
    sIntervalRecordingConfig.deviceNumber       = 1;
    
    // We only support the R900 interval format, so this is hardcoded.
    sIntervalRecordingConfig.intervalFormat     = E_R900_INTERVAL_DATA_FORMAT;    

    // Convert the datalog interval to minutes.
    sIntervalRecordingConfig.recordingIntervalMin  = (uMIUConfigNormal.sMIUConfigNormal.datalogIntervalSecs / 60u);
    
    // Convert the interval data packet interval to hours.
    sIntervalRecordingConfig.reportingIntervalHrs  = (uMIUConfigNormal.sMIUConfigNormal.intervalDataPacketIntervalSecs / 3600u);
    
    // This isn't clearly defined, so we'll force it to 0.
    sIntervalRecordingConfig.intervalStartTime  = 0;
    
    // Get the next datalog time so that we can back out the latest recording.
    next_time.S64 = app_scheduler_checkTaskTime(E_SCHEDULER_TASK_TYPE_DATALOG);
    // The datalog module will calculate the latest datalog time based on the
    // next scheduled task time and the interval time (in seconds).
    sIntervalRecordingConfig.intervalLastRecordDateTime = app_datalog_getLatestRecordTime(uMIUConfigNormal.sMIUConfigNormal.datalogIntervalSecs, next_time.S64);

    // Add struct data into packet
    bResult = Tpbp_PackerAdd_IntervalRecordingConfig(pPacker, &sIntervalRecordingConfig);

    return bResult;
}

/*****************************************************************************
 * @brief
 *   Inserts MQTT/BLE/Uart packet header (TAG = 35) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer into which we load data
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag35_packetHeader(S_TPBP_PACKER* const pPacker)
{
    bool bResult;
    static S_MQTT_BLE_UART sHeaderInfo;

    /** These are just default numbers for now. */
    sHeaderInfo.encryptionMethod = 1;
    sHeaderInfo.sequenceNumber = 2;
    sHeaderInfo.token = 3;
    sHeaderInfo.keyInfo = 4;

    // Add struct data into packet
    bResult = Tpbp_PackerAdd_UartPacketHeader(pPacker, &sHeaderInfo);

    return bResult;
}


/***************************************************************************//**
 * @brief
 *   Inserts firmware revision (TAG = 39) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer to load data in to
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag39_firmwareRevision(S_TPBP_PACKER* const pPacker)
{
    bool bResult;
    
    // TPBP structure for image info is not the same as used for images so need to
    // create local structure and fill.
    S_IMAGE_VERSION_INFO sImageInfo;

    sImageInfo.versionMajorBcd          = sAppImgInfo.versionMajor;
    sImageInfo.versionMinorBcd          = sAppImgInfo.versionMinor;
    sImageInfo.versionYearMonthDayBcd   = sAppImgInfo.versionYearMonthDay;
    sImageInfo.versionBuildBcd          = sAppImgInfo.versionBuild;
    
    // Add struct data into packet    
    bResult = Tpbp_PackerAdd_ImageVersionInfoFirmware(pPacker, &sImageInfo);  
    
    return bResult;
    
}


/***************************************************************************//**
 * @brief
 *   Inserts bootloader image revision (TAG = 40) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer to load data in to
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag40_bootloaderRevision(S_TPBP_PACKER* const pPacker)
{
    bool bResult;
    
    // TPBP structure for image info is not the same as used for images so need to
    // create local structure and fill.
    S_IMAGE_VERSION_INFO sImageInfo;
    
    // Pointer to BL image info
    const S_MIU_IMG_INFO * psImageInfo  = (const S_MIU_IMG_INFO *) BOOTLOADER_INFO;

    sImageInfo.versionMajorBcd          = psImageInfo->versionMajor;
    sImageInfo.versionMinorBcd          = psImageInfo->versionMinor;
    sImageInfo.versionYearMonthDayBcd   = psImageInfo->versionYearMonthDay;
    sImageInfo.versionBuildBcd          = psImageInfo->versionBuild;
       
    // Add struct data into packet
    bResult = Tpbp_PackerAdd_ImageVersionInfoBootloader(pPacker, &sImageInfo);  
    
    return bResult;
    
}


/***************************************************************************//**
 * @brief
 *   Inserts config image revision (TAG = 41) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer to load data in to
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag41_configRevision(S_TPBP_PACKER* const pPacker)
{
    bool bResult;
    
    // TPBP structure for image info is not the same as used for images so need to
    // create local structure and fill.
    S_IMAGE_VERSION_INFO sImageInfo;

    sImageInfo.versionMajorBcd          = sConfigImgInfo.versionMajor;
    sImageInfo.versionMinorBcd          = sConfigImgInfo.versionMinor;
    sImageInfo.versionYearMonthDayBcd   = sConfigImgInfo.versionYearMonthDay;
    sImageInfo.versionBuildBcd          = sConfigImgInfo.versionBuild;
    
    // Add struct data into packet    
    bResult = Tpbp_PackerAdd_ImageVersionInfoConfig(pPacker, &sImageInfo);  
    
    return bResult;   
}


/***************************************************************************//**
 * @brief
 *   Inserts CMIU information (TAG = 44) into packet
 *
 * @param[in] pPacker - packer to load data in to
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag44_cmiuInformation(S_TPBP_PACKER* const pPacker)
{
    bool bResult;

    sCmiuInformation.installationDate   = 1441108800;  // 9/1/2015  Noon   
    sCmiuInformation.dateLastMagSwipe   = app_rmu_magSwipeTime_Get().S64;
    sCmiuInformation.magSwipes          = app_rmu_magSwipeCounter_Get();
    sCmiuInformation.batteryRemaining   = LOW16(app_adc_vddDiv3_retrieve());
    sCmiuInformation.timeErrorLastNetworkTimeAccess = LOW32(app_rtc_timeDiff_full_get().S64);
    
    // Add struct data into packet    
    bResult = Tpbp_PackerAdd_CmiuInformation(pPacker, &sCmiuInformation);  
    
    return bResult;         
}

/***************************************************************************//**
 * @brief
 *   Populates the recording and reporting interval struct to support tag 48.
 *
 * @note
 *   
 *
 * @param[in/out] pIntervals - pointer to recording and reporting intervals struct.
 *
 * @return none
 ******************************************************************************/
static void app_packet_builder_get_intervals(S_RECORDING_REPORTING_INTERVAL* pIntervals)
{
    pIntervals->reportingStartMins               = 
                                uMIUConfigNormal.sMIUConfigNormal.cellCallInOffsetMins;
    // use the interval reporting time and convert from seconds to hours
    pIntervals->reportingIntervalHours           = 
                                (uMIUConfigNormal.sMIUConfigNormal.intervalDataPacketIntervalSecs   / 3600);
    pIntervals->reportingRetries                 = 0; // not currently supported
    pIntervals->reportingTransmitWindowsMins     = 
                                (uMIUConfigNormal.sMIUConfigNormal.cellCallInWindowQtrHrs * 15);
    pIntervals->reportingQuietStartMins          = 0; // not currently supported
    pIntervals->reportingQuietEndMins            = 0; // not currently supported
    pIntervals->recordingStartMins               = 0; // not currently supported
    pIntervals->recordingIntervalMins            = 
                                uMIUConfigNormal.sMIUConfigNormal.datalogIntervalSecs / 60;
    pIntervals->recordingNumberAttachedDevices   = 1; // not currently supported, force to 1
    pIntervals->recordingEventMaskForDevice      = 0; // not currently supported
}

/***************************************************************************//**
 * @brief
 *   Inserts recording & reporting interval (TAG = 48) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer to load data in to
 * @param[in] pIntervals - a pointer to the intervals information for shoving
 * into tag 48.
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag48_recordingReportingInterval(
    S_TPBP_PACKER* const pPacker, 
    S_RECORDING_REPORTING_INTERVAL* pIntervals)
{
    bool bResult;
 
    // Add struct data into packet   
    bResult = Tpbp_PackerAdd_RecordingReportingInterval(pPacker, pIntervals);
        
    return bResult;  
}


/***************************************************************************//**
 * @brief
 *   Inserts error tag (TAG = 51) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer to load data in to
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag51_errorCode(S_TPBP_PACKER* const pPacker,
                                                          E_ERROR_TAG    const errorCode)
{
    bool bResult;
            
    bResult = Tpbp_PackerAdd_ErrorCode(pPacker, errorCode);
       
    return bResult;
}

/***************************************************************************//**
 * @brief
 *   Inserts packet instigator tag (TAG = 72) into packet
 *
 * @note
 *   
 *
 * @param[in] pPacker - packer to load data in to
 *
 * @return bool - true if tag added to packet successfully, false if not
 ******************************************************************************/
static bool app_packet_builder_tagHandler_tag72_packetInstigator(S_TPBP_PACKER* const pPacker)
{
    bool bResult;

    bResult = Tpbp_PackerAdd_PacketInstigator(pPacker, (uint8_t)gPacketInstigator);
    
    return bResult;
}
