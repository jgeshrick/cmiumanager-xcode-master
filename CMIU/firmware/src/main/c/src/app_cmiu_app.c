/******************************************************************************
*******************************************************************************
**
**         Filename: app_cmiu_app.c
**    
**           Author: Troy Harstad
**          Created: 1/21/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_cmiu_app.c
*
* @brief This file contains the main application code to schedule and
* perform tasks.
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "typedefs.h"
#include "app_rtc.h"
#include "app_cmiu_app.h"
#include "app_arb.h"
#include "CmiuAppConfiguration.h"
#include "app_metrology.h"
#include "app_uart.h"
#include "app_operatingMode.h"
#include "app_packet_parser.h"
#include "app_packet_builder.h"
#include "app_cmit_interface.h"
#include "MqttManager.h"
#include "TcpSessionManager.h"
#include "CmiuAppConfiguration.h"
#include "app_power.h"
#include "app_adc.h"
#include "app_cellular.h"
#include "app_scheduler.h"
#include "app_rmu.h"
#include "app_ble.h"
#include "em_rmu.h"
#include "app_datalog.h"
#include "app_command.h"

// Modem Library Header Files
#include "modem.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/



/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static void app_cmiu_app_state_normal(void);
static void app_cmiu_app_state_swipe(void);
static void app_cmiu_app_state_cellular(void);
static void app_cmiu_app_swipe_enter(void);
static void app_cmiu_app_cellular_enter(void);
static void app_cmiu_app_cellular_exit(void);

// Task handler functions
static void app_cmiu_app_taskHandler_readRegister(void);
static void app_cmiu_app_taskHandler_cellularConnection(void);
static bool app_cmiu_app_taskHandler_cellularConnection_checkBattAndTemp(void);
static void app_cmiu_app_taskHandler_imageUpdate(void);
static bool app_cmiu_app_taskHandler_imageUpdate_checkBattAndTemp(void);
static void app_cmiu_app_taskHandler_datalog(void);
static void app_cmiu_app_taskHandler_modemFota(void);   



/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/

// CMIU App State
static volatile E_CMIU_APP_STATE cmiuAppState;

// Number of seconds left in Swipe State, once it reaches
// 0 the pfnapp_cmiu_app_State will be set to Normal State
static volatile uint8_t cmiuAppSwipeStateTimer;

// Register read result
static volatile uint8_t byRegisterReadResult;

static volatile U_NEPTUNE_TIME cmiuSwipeEndTime;


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/

/**************************************************************************//**
 * @brief
 *   Initializes CMIU Application module
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_cmiu_app_init(void)
{
    // Only init if it was not a reset pin reset (mag swipe) and not a system 
    // request (software) reset.  Don't want to reset the time in either case.
    if(((app_rmu_resetCause_Get() & RMU_RSTCAUSE_EXTRST) != RMU_RSTCAUSE_EXTRST) &&
       ((app_rmu_resetCause_Get() & RMU_RSTCAUSE_SYSREQRST) != RMU_RSTCAUSE_SYSREQRST))
    {        
        // May not need to do anything different here!!!
        // Upon entering normal mode below a detailed config packet is sent based
        // on the reset cause.
    }

    // Default to swipe state
    app_cmiu_app_swipe_enter();
    
    // Default this to retry
    // This should be set by the register read occurring first
    byRegisterReadResult = REGISTER_READ_FAIL_RETRY;
}



/**************************************************************************//**
 * @brief
 *   CMIU application state tick function
 *
 * @note
 *   This function ticks the App Module by running the current state,
 *   which is either Swipe or Normal.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
 void app_cmiu_app_tick(void)
{   
    switch(cmiuAppState)
    {
        case E_CMIU_APP_STATE_SWIPE:
        {
            app_cmiu_app_state_swipe();
            break;
        }
        
        case E_CMIU_APP_STATE_NORMAL:
        {
            app_cmiu_app_state_normal();
            break;
        }
        
        case E_CMIU_APP_STATE_CELLULAR:
        {
            app_cmiu_app_state_cellular();
            break;
        }

        default:
        {
            // Should not get here so re-enter normal state
            app_cmiu_app_normal_enter();
            break;          
        }
    }   
    
}

/**************************************************************************//**
 * @brief
 *   Access app_cmiu_app_taskHandler_cellularConnection from outside this
 *   module.
 *
 * @note
 *   Using this routine to jump into a cell session is a bit hackerish.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_cmiu_app_check_cell_session(void)
{
    app_cmiu_app_taskHandler_cellularConnection();
}

/**************************************************************************//**
 * @brief
 *   Switch to swipe mode from another mode. This if certain commands take a
 *   really long time, or temporarily switch the system out of swipe mode.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_cmiu_app_switch_to_swipe_mode(void)
{
    /** Set to the swipe state */
    cmiuAppState = E_CMIU_APP_STATE_SWIPE;

    /** Bump the cmiuSwipeEndTime a bit because we probably just came from a
     * cell session. */
    cmiuSwipeEndTime = app_rtc_time_get();
    cmiuSwipeEndTime.S64 += uMIUConfigSwipe.sMIUConfigSwipe.swipeStateTimerSecs;

    /** We're also going to get the operating mode to switch back to swipe. */
    app_operatingMode_init();

    DEBUG_OUTPUT_TEXT("Switching back to CMIU App Swipe State\r\n");

}

/**************************************************************************//**
 * @brief
 *   Normal state of operation
 *
 * @note
 *   Normal state of operation.  Checks each individual task
 *   interval count has reached zero and performs that task
 *   if so.  The following tasks are performed here based on 
 *   the interval:
 *
 *       -Read register
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_state_normal(void)
{   
    
// ----------------------------------------------------------------------------
// TIME TO READ REGISTER?  --  PRIORITY X 
// ----------------------------------------------------------------------------    
    if(app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE_REGISTER_READ))
    {     
        // Schedule next register reading data packet
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_REGISTER_READ, E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION, NULL, NULL);
        
        // Handle read register task
        app_cmiu_app_taskHandler_readRegister();
              
    }      


    
// ----------------------------------------------------------------------------
// TIME TO MAKE CELLULAR CONNECTION  --  PRIORITY X 
// ----------------------------------------------------------------------------    
// Every 15 minutes the CMIU will determine if any cellular packets need to be
// transmitted and if so transition into cellular mode to do so.
// detailedConfigPacketDue and intervalDataPacketDue are cleared and rescheduled
// if necessary.
    if(app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE_CELLCONNECTION))
    {
        // Schedule next cellular connection
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_CELLCONNECTION, E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION, NULL, NULL);            
                
        // Handle cellular connection task
        app_cmiu_app_taskHandler_cellularConnection();
        
    }
    

// ----------------------------------------------------------------------------
// TIME TO UPDATE ONE OR MORE IMAGES --  PRIORITY X 
// ----------------------------------------------------------------------------    
    if(app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE_IMAGEUPDATE))
    {
        // Next image update is scheduled elesewhere if needed          
        //app_scheduler_unscheduleTask(E_SCHEDULER_TASK_TYPE_IMAGEUPDATE);  
        
        // Handle cellular connection task
        app_cmiu_app_taskHandler_imageUpdate();
        
    }
    
    
// ----------------------------------------------------------------------------
// TIME TO STORE READING IN DATALOG --  PRIORITY X 
// ----------------------------------------------------------------------------    
    if(app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE_DATALOG))
    {
        // Schedule next datalog according to configuration value
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_DATALOG, E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION, NULL, NULL);   
        
        // Handle datalog task
        app_cmiu_app_taskHandler_datalog();
        
    }
    
// ----------------------------------------------------------------------------
// TIME TO UPDATE THE MODEM --  PRIORITY X  
// ----------------------------------------------------------------------------    
    if(app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE_MODEMFOTA))
    {
        
        // Handle modem FOTA task
        app_cmiu_app_taskHandler_modemFota();
        
    }
    
    
 }

/**************************************************************************//**
 * @brief
 *   Swipe state of operation
 *
 * @note
 *   Swipe state of operation.  Checks each individual task
 *   interval count has reached zero and performs that task
 *   if so.  The following tasks are performed here based on 
 *   the interval:
 *
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_state_swipe(void)
{


// ----------------------------------------------------------------------------
// TIME TO READ REGISTER?  --  PRIORITY 4
// ----------------------------------------------------------------------------    
    if(app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE_REGISTER_READ))
    {

        // Debug mode only - wait for UART to complete sending data PROBABLY NOT NECESSRY SINCE NOT GOING TO SLEEP DURING READ!!!
		DEBUG_WAITFORUART();          
        
		// Read register
		byRegisterReadResult = ARB_ReadRegister();
		
        // Check register read status
        if(byRegisterReadResult == REGISTER_READ_PASS)
		{
             // Next read per configuration
			app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_REGISTER_READ, E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION, NULL, NULL);                               
        }
        
        // Autodetect stage failed, go to next state
        else if(byRegisterReadResult == REGISTER_READ_FAIL_RETRY)
		{
			// Retry in >2 seconds, do not build updated Ecoder packet yet
			app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_REGISTER_READ, E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, 3, NULL);           
		}     
        
        // Build updated Ecoder RF packet if register read passed or
        // there are no retries available
        else //(byRegisterReadResult == REGISTER_READ_FAIL_NORETRY)
        {
            
            // Next read per configuration
			app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_REGISTER_READ, E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION, NULL, NULL);  
                      
        }
        
    }    
 
// ----------------------------------------------------------------------------   
// Check if it is time to switch to Normal mode
// ----------------------------------------------------------------------------    
    if ((app_rtc_time_get().S64 > cmiuSwipeEndTime.S64) &&
        (false == app_command_testForever_retrieve()))

    {
        // Normally this wouldn't be reached since BLE handles its own timeout, which
        // causes end of swipe mode.
        
        // Enter normal app state and operating mode
        app_cmiu_app_normal_enterAfterSwipe();
        app_operatingMode_normal_enter();
        
        // Power off BLE if not advertising forever
        if(!app_ble_advertiseForever_retrieve())
        {
            app_ble_stop();
            DEBUG_OUTPUT_TEXT("Force off BLE\r\n");  
        }
               
        DEBUG_OUTPUT_TEXT("Swipe Mode Exit - Timeout Override\r\n");       
        
    } 
}




/**************************************************************************//**
 * @brief
 *   Perform read register task
 *
 * @note
 *   This function reads the register over the ARB interface and performs
 *   the metrology flags calculation.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_taskHandler_readRegister(void)
{
  // Debug mode only - wait for UART to complete sending data
		DEBUG_WAITFORUART(); 
        
		// Read register
		byRegisterReadResult = ARB_ReadRegister();
		
        // Check register read status
        if(byRegisterReadResult == REGISTER_READ_PASS)
		{
			// The register read was successful
			// Calculate the metrology flags 
			app_metrology_CalculateFlags();
            
//             // Build updated Ecoder packet
//             Packet_Tx_Ecoder_Build();  
//     
//             // Store latest reading into RAM log
//             Datalog_ReadingLog_StoreReadingToRAM();
            
		}
		
        else if(byRegisterReadResult == REGISTER_READ_FAIL_RETRY)
		{
			// Retry in 2 seconds minimum (so set to 3), do not build updated Ecoder packet yet
			app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_REGISTER_READ, E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, 3, NULL);

            // If the datalog is due now, reschedule it for the same offset.
            if (app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE_DATALOG))
            {
                app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_DATALOG,
                                           E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET,
                                           3, NULL);
            }

		}     
        
        // Build updated Ecoder RF packet if register read passed or
        // there are no retries available
        else  //(byRegisterReadResult == REGISTER_READ_FAIL_NORETRY)
        {
//             // No retries available so build updated Ecoder packet
//             Packet_Tx_Ecoder_Build();   
//             
//             // Store latest reading into RAM log - in this case an error
//             Datalog_ReadingLog_StoreReadingToRAM();
        }  
    
    
}




/**************************************************************************//**
 * @brief
 *   Cellular state of operation
 *
 * @note
 *   Cellular state of operation.  This state ticks the TCP session manager
 *   to facilitate TCP communications using MQTT and the modem.
 *
 *   When the transport manager indicates the connetion is closed the cellular
 *   state is exited.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_state_cellular(void)
{
    bool needMoreTicks;
    
    // Tick cellular state machine
    needMoreTicks = app_cellular_tick();
    
    // If no more ticks required then exit from cellular state
    if(needMoreTicks == false)
    {
        app_cmiu_app_cellular_exit();
    }
        
}







/**************************************************************************//**
 * @brief
 *   Function to enter cellular state of operation
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_cellular_enter(void)
{
    /** Set to the cellular state. */
    cmiuAppState = E_CMIU_APP_STATE_CELLULAR;
}



/**************************************************************************//**
 * @brief
 *   Function to exit cellular state of operation
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_cellular_exit(void)
{
    int8_t tempToStore;
    uint32_t battAdcToStore;
    
    
    // Set to the normal state
    cmiuAppState = E_CMIU_APP_STATE_NORMAL; 

    // Enter normal operating mode
    app_operatingMode_normal_enter();
    
    // Set cellular state machine back to safe state
    app_cellular_safe_enter();
    
    // Take a battery voltage measurement and temp measurement here since this 
    // is most likely the lowest they get after a cellular session
    app_adc_vddDiv3_setupAndMeasure();
    app_adc_intTemp_setupAndMeasure();
    
    // Retrieve the values
    battAdcToStore = app_adc_vddDiv3_retrieve();
    tempToStore = app_adc_intTemp_retrieve();
    
    // then store them as "mostRecentTx" values for later use in detailed
    // configuration packet
    app_adc_intTemp_mostRecentTx_store(tempToStore);
    app_adc_vddDiv3_mostRecentTx_store(battAdcToStore);
    
        
}



/**************************************************************************//**
 * @brief
 *   Function to enter normal state of operation after a reset
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_cmiu_app_normal_enter(void)
{
     // Set to the normal state
    cmiuAppState = E_CMIU_APP_STATE_NORMAL;   
    
}
    

/**************************************************************************//**
 * @brief
 *   Function to enter normal state of operation after a reset
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_cmiu_app_normal_enterAfterSwipe(void)
{
    // Set to the normal state
    cmiuAppState = E_CMIU_APP_STATE_NORMAL;  
    
// Schedule a detailed configuration packet for one hour from now
// if any reset type except mag swipe (EXTRST) 
    if((app_rmu_resetCause_Get() & RMU_RSTCAUSE_EXTRST) != RMU_RSTCAUSE_EXTRST)
    {                 
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET, 
                                   E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, 
                                   3600, 
                                   NULL);
        /** The cell session has to be scheduled to support the detailed
         * config packet. */
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_CELLCONNECTION, 
                                   E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, 
                                   3600,
                                   NULL);
    }
    
    else
    {
        // Schedule the first detailed config packet transmission using regular 
        // interval value in configuration
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET, 
                                   E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION, 
                                   NULL, 
                                   NULL);
        /** The cell session can be scheduled according to the configuration
         * since the detailed config packet is on a normal schedule. */
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_CELLCONNECTION,
                                   E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION,
                                   NULL,
                                   NULL);
    }
  
// The rest of the tasks can be scheduled according to their regularly scheduled
// interval value located in the configuration.   
    app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_REGISTER_READ, 
                                E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION, 
                                NULL,
                                NULL);
        
    app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_INTERVALDATA_PACKET, 
                                E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION, 
                                NULL, 
                                NULL);    
    
    app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_DATALOG, 
                                E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION, 
                                NULL, 
                                NULL);      
    



//     // !!!TEST ONLY DURING BETA DEVELOPMENT, TX FIRST PACKETS AND DATALOG SOON AFTER RESET!!!    
//     app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET, E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, 5, NULL);
//     app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_INTERVALDATA_PACKET, E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, 5, NULL);    
//     app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_CELLCONNECTION, E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, 5, NULL); 
//     app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_DATALOG, E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, 2, NULL); 

}



/**************************************************************************//**
 * @brief
 *   Function to enter swipe state of operation
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_swipe_enter(void)
{
        
    // Set to the swipe state
    cmiuAppState = E_CMIU_APP_STATE_SWIPE;
    
// Set the initial intervals values
    // Make sure the register gets read before the first transmssion. 
    // Schedule first register reading in swipe mode mode for 2 seconds from now    
    app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_REGISTER_READ, E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, 2, NULL);
    
    // Start BLE advertising in swipe mode
    app_ble_startup(false);
         
    // Set length of time to stay in swipe mode by getting current time and adding config value
    cmiuSwipeEndTime = app_rtc_time_get(); 
    cmiuSwipeEndTime.S64 += uMIUConfigSwipe.sMIUConfigSwipe.swipeStateTimerSecs;
      
    DEBUG_OUTPUT_TEXT("Entering CMIU App Swipe State\r\n");
}



/**************************************************************************//**
 * @brief
 *   Perform cellular connection task
 *
 * @note
 *   This function checks if any cellular activites are due and performs them
 *   if necessary.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_taskHandler_cellularConnection(void)
{   
    bool cellConnectAllowed;
    
    // Check if any cellular tasks due
    if(app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET) || 
       app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE_INTERVALDATA_PACKET))
    {
        DEBUG_OUTPUT_TEXT("One or more cellular packets due \r\n");   

        // Check if battery voltage and temperature are high enough to allow
        // network connection
        cellConnectAllowed = app_cmiu_app_taskHandler_cellularConnection_checkBattAndTemp();
        
        if(cellConnectAllowed)
        {      
            // Enter cellular application state
            app_cmiu_app_cellular_enter();

            // Enter cellular operating mode
            app_operatingMode_cellular_enter();   
            
            // Enter MQTT cellular state
            app_cellular_online_enter(E_CELL_SESSION_MQTT);
        }
        
        else // cell connection not allowed
        {
            // Each packet type currently due reschedule for next regularly scheduled interval
            if(app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET))
            {   
                DEBUG_OUTPUT_TEXT("Temp/Battery Too Low - Rescheduling Detailed Config Packet\r\n");  
                // Schedule next detailed config packet
                app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET, E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION, NULL, NULL);   
                    
            }
            
            if(app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE_INTERVALDATA_PACKET))
            {                  
                DEBUG_OUTPUT_TEXT("Temp/Battery Too Low - Rescheduling Interval Data Packet\r\n");  
                // Schedule next detailed config packet
                app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_INTERVALDATA_PACKET, E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION, NULL, NULL);              
            }
        }
    }   
    
    else
    {
        // No cellular tasks due so simply return
        DEBUG_OUTPUT_TEXT("No cellular packets due \r\n");   
    }
    
 
}



/**************************************************************************//**
 * @brief
 *   This function checks if the temperature and battery voltage are acceptable
 *   to attempt a MQTT cellular connection.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static bool app_cmiu_app_taskHandler_cellularConnection_checkBattAndTemp(void)
{
    bool connectAllowed;
    int8_t tempInC;
    uint32_t battAdc;    
    
    // Measure and retrieve temperature
    app_adc_intTemp_setupAndMeasure();
    tempInC = app_adc_intTemp_retrieve();
    
    // Measure and retrieve battery voltage ADC value
    app_adc_vddDiv3_setupAndMeasure();
    battAdc = app_adc_vddDiv3_retrieve();
    
    // At 3.5 V we can connect at any temperature
    if(battAdc >= BATT_ADC_3V5)
    {
        connectAllowed = true;
    }
    
    // At 3.44 V we can connect if temperature above -30 C
    else if((battAdc >= BATT_ADC_3V44) && (tempInC > -30))
    {
        connectAllowed = true;
    }
    
    // At 3.4 V we can connect if temperature above -20 C
    else if((battAdc >= BATT_ADC_3V4) && (tempInC > -20))
    {
        connectAllowed = true;
    }
    
    // At 3.3 V we can connect if temperature above -10 C
    else if((battAdc >= BATT_ADC_3V3) && (tempInC > -10))
    {
        connectAllowed = true;
    }    
    
    // At 3.2 V we can connect if temperature above 0 C
    else if((battAdc >= BATT_ADC_3V2) && (tempInC > 0))
    {
        connectAllowed = true;
    }  
    
    // At 3.15 V we can connect if temperature above 10 C
    else if((battAdc >= BATT_ADC_3V15) && (tempInC > 10))
    {
        connectAllowed = true;
    }  
    
    // At 3.1 V we can connect if temperature above 20 C
    else if((battAdc >= BATT_ADC_3V1) && (tempInC > 20))
    {
        connectAllowed = true;
    } 
    
    else    // battery and temperature combination too low to connect right now
    {
        connectAllowed = false;       
    }
    
    return connectAllowed;  
}


/**************************************************************************//**
 * @brief
 *   Perform image update task
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_taskHandler_imageUpdate(void)
{
    bool cellConnectAllowed;
    
    // Check if battery voltage and temperature are high enough to allow
    // network connection.
    cellConnectAllowed = app_cmiu_app_taskHandler_imageUpdate_checkBattAndTemp();
    
    if(cellConnectAllowed)
    {      
        // Enter cellular application state
        app_cmiu_app_cellular_enter();

        // Enter cellular operating mode
        app_operatingMode_cellular_enter();   
        
        // Enter online cellular state
        app_cellular_online_enter(E_CELL_SESSION_HTTP);
    }
    
    else // cell connection not allowed
    {       
        DEBUG_OUTPUT_TEXT("Temp/Battery Too Low - Rescheduling Image Update for 8 Hours From Now\r\n");  
        
        // Schedule next image update attempt 8 hours from now
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_IMAGEUPDATE, E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, 28800, NULL);            
    }   
}

/**************************************************************************//**
 * @brief
 *   Perform modem update task
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_taskHandler_modemFota(void)
{
    bool cellConnectAllowed;
    
    // Check if battery voltage and temperature are high enough to allow
    // network connection.
    cellConnectAllowed = app_cmiu_app_taskHandler_imageUpdate_checkBattAndTemp();
    
    if(cellConnectAllowed)
    {      
        // Enter cellular application state
        app_cmiu_app_cellular_enter();

        // Enter cellular operating mode
        app_operatingMode_cellular_enter();   
        
        // Enter online cellular state
        app_cellular_online_enter(E_CELL_SESSION_FOTA);
    }
    
    else // cell connection not allowed
    {       
        DEBUG_OUTPUT_TEXT("Temp/Battery Too Low - Rescheduling Modem FOTA for 8 Hours From Now\r\n");  
        
        // Schedule next modem FOTA attempt 8 hours from now
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_MODEMFOTA, E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, 28800, NULL);            
    }   
}



/**************************************************************************//**
 * @brief
 *   This function checks if the temperature and battery voltage are acceptable
 *   to attempt a image update.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static bool app_cmiu_app_taskHandler_imageUpdate_checkBattAndTemp(void)
{
    bool connectAllowed;
    int8_t tempInC;
    uint32_t battAdc;    
    
    // Measure and retrieve temperature
    app_adc_intTemp_setupAndMeasure();
    tempInC = app_adc_intTemp_retrieve();
    
    // Measure and retrieve battery voltage ADC value
    app_adc_vddDiv3_setupAndMeasure();
    battAdc = app_adc_vddDiv3_retrieve();
    
    // At 3.55 V we can connect at any temperature
    if(battAdc >= BATT_ADC_3V55)
    {
        connectAllowed = true;
    }
    
    // At 3.49 V we can connect if temperature above -30 C
    else if((battAdc >= BATT_ADC_3V49) && (tempInC > -30))
    {
        connectAllowed = true;
    }
    
    // At 3.45 V we can connect if temperature above -20 C
    else if((battAdc >= BATT_ADC_3V45) && (tempInC > -20))
    {
        connectAllowed = true;
    }
    
    // At 3.35 V we can connect if temperature above -10 C
    else if((battAdc >= BATT_ADC_3V35) && (tempInC > -10))
    {
        connectAllowed = true;
    }    
    
    // At 3.25 V we can connect if temperature above 0 C
    else if((battAdc >= BATT_ADC_3V25) && (tempInC > 0))
    {
        connectAllowed = true;
    }  
    
    // At 3.20 V we can connect if temperature above 10 C
    else if((battAdc >= BATT_ADC_3V2) && (tempInC > 10))
    {
        connectAllowed = true;
    }  
    
    // At 3.15 V we can connect if temperature above 20 C
    else if((battAdc >= BATT_ADC_3V15) && (tempInC > 20))
    {
        connectAllowed = true;
    } 
    
    else    // battery and temperature combination too low to connect right now
    {
        connectAllowed = false;       
    }
    
    return connectAllowed;  
}



/**************************************************************************//**
 * @brief
 *  Perform datalog task.
 *
 * @note
 *  This function is reached once per datalog interval. This will get the most
 *  recent reading from the ARB module, format the reading, and stuff the
 *  reading into the datalog storage. The last step, storing the reading,
 *  updates the backlog. It is therefore important that this is done prior
 *  to an interval data packet being generated.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_taskHandler_datalog(void)
{
    bool bResult;
    uint32_t record;
    S_REGISTER_DATA sARBReadingData;

    // Get ARB Reading
    sARBReadingData = *ARB_RegisterData_Retrieve();

    // Format the interval data and flags for the datalog
    record = app_datalog_formatRecord(sARBReadingData.dwReadingBinary.U32,
                                      app_metrology_24Hr_Backflow_Retrieve(),
                                      app_metrology_LeakFlags_Retrieve());

    // Send the formatted record to the datalog
    bResult = app_datalog_setCurrentRecord(record);

    // Conditionally advance the datalog system.
    if (bResult)
    {
        bResult = app_datalog_storeCurrentRecord();
    }
}


    
/**************************************************************************//**
 * @brief
 *      Function retrieve CMIU app state
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
E_CMIU_APP_STATE app_cmiu_app_state_get(void)
{
    return cmiuAppState;
}
    
    
    
    
