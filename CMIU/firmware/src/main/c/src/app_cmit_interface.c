/******************************************************************************
*******************************************************************************
**
**         Filename: app_cmit_interface.c
**    
**           Author: Barry Huggins
**          Created: 6/11/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_cmit_interface.c
*
* @brief This file contains the code used to manage the link to the CMIT.
*        This link may be either BLE or a serial UART.
* 
************************************************************************/

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include "stdtypes.h"
#include "DebugTrace.h"
#include "em_device.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "em_int.h"
#include "efm32lg_uart.h"
#include "app_cmit_interface.h"
#include "app_uart.h"
#include "app_timers.h"
#include "app_ble.h"
#include "UartController.h"
#include "CmiuAppConfiguration.h"
#include "MqttManager.h"
#include "app_packet_parser.h"
#include "MemoryMap.h"
#include "em_emu.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
#define RING_BUFFER_CMIT_SIZE_RX 512  // depends on tick rate / data rate etc.
#define RING_BUFFER_CMIT_SIZE_TX 3000

/* define the sizes for the buffers used for the serial output */
#define RING_BUFFER_CMIT_SERIAL_SIZE_RX 512  // depends on tick rate / data rate etc.
#define RING_BUFFER_CMIT_SERIAL_SIZE_TX 2600

/* BLE Heartbeat period */
#define BLE_HEARTBEAT_PERIOD            1000//ms

// Use this to keep sending debug messages as ASCII for now
//#define CMIT_SERIAL_PLAIN_ASCII ///@todo DEW
/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/

static bool app_cmit_interface_PushByte(uint8_t b, bool flush);
static bool app_cmit_interface_Serial_PushByte(uint8_t b, bool flush);
static void app_cmit_interface_CommandMessageHandler(const struct  UARC_MSG*  pRxMsg);
static void app_cmit_interface_PingMessageHandler(const struct  UARC_MSG*  pRxMsg);
static void app_cmit_interface_AsyncMessageHandler(const struct  UARC_MSG*  pRxMsg);
static void app_cmit_interface_HttpResponseHandler(const struct  UARC_MSG*  pRxMsg);
static void app_cmit_interface_UnknownMessageHandler(const struct  UARC_MSG*  pRxMsg);
static void app_cmit_interface_CommandMessageHandler_Serial(const struct  UARC_MSG*  pRxMsg);
static void app_cmit_interface_PingMessageHandler_Serial(const struct  UARC_MSG*  pRxMsg);
static void app_cmit_interface_AsyncMessageHandler_Serial(const struct  UARC_MSG*  pRxMsg);
static void app_cmit_interface_HttpResponseHandler_Serial(const struct  UARC_MSG*  pRxMsg);
static void app_cmit_interface_UnknownMessageHandler_Serial(const struct  UARC_MSG*  pRxMsg);



/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/


// Ringbuffers
uint8_t rxCmitData[RING_BUFFER_CMIT_SIZE_RX];
uint8_t txCmitData[RING_BUFFER_CMIT_SIZE_TX];
RingBufferT rxCmitRingBuffer;   // Buffer for data FROM CMIT link
RingBufferT txCmitRingBuffer;   // Buffer for data TO CMIT link

// Ringbuffers for the serial interface to the CMIT
uint8_t rxCmitSerialData[RING_BUFFER_CMIT_SERIAL_SIZE_RX];
uint8_t txCmitSerialData[RING_BUFFER_CMIT_SERIAL_SIZE_TX];
RingBufferT rxCmitSerialRingBuffer;   // Buffer for data FROM CMIT serial link
RingBufferT txCmitSerialRingBuffer;   // Buffer for data TO CMIT serial link

// buffer used for 20 byte UART message
static uint8_t serialMessageBuffer[BLE_NUS_MAX_DATA_LEN];
static uint8_t serialMessageIndex;

// buffer used for 20 byte UART over BTLE message
static uint8_t bleMessageBuffer[BLE_NUS_MAX_DATA_LEN];
static uint8_t bleMessageIndex;

static UART_CONTROLLER                  uc;
static UARC_MSG_READER                  mr;

static UART_CONTROLLER                  ucSerial;
//static UARC_MSG_READER                  mrSerial;

// Max size of an rx payload 
#define RX_PAYLOAD_BUFFER_SIZE          2048
// Assembly buffer for reader
static uint8_t                          payloadBuffer[RX_PAYLOAD_BUFFER_SIZE] SCRATCH_RAM_VARS;  
// bit map which determines if debug output should be sent out the BLE link or the debug UART
static      uint8_t app_interface_debug_destination_map;   // updated from the config data
// Is the BLE ring buffer empty?
static bool g_ble_ring_buffer_empty;


/*****************************************************************************
 * @brief
 *   Handles a message once received and checked
 *
 * @param [in] pRxMsg 
 *  The received message
 *
 * @return None
 *
// *****************************************************************************/
static void OnReceiveMessage(const struct  UARC_MSG*  pRxMsg, const void *context)
{
    (void)context;
        
    switch(pRxMsg->type)
    {
        case UARC_MT_NULL:
            break;
        
        case UARC_MT_CMD:
            app_cmit_interface_CommandMessageHandler(pRxMsg);
            break;
        
        case UARC_MT_PING:
            app_cmit_interface_PingMessageHandler(pRxMsg);
            break;        
        
        case UARC_MT_ASYNC:
            app_cmit_interface_AsyncMessageHandler(pRxMsg);
            break;
                
        case UARC_MT_HTTP_RSP:
            app_cmit_interface_HttpResponseHandler(pRxMsg);
            break;
        
        default:
            app_cmit_interface_UnknownMessageHandler(pRxMsg);
            break;
    }  
}


/*****************************************************************************
 * @brief
 *   Handles a message from the serial UART once received and checked
 *
 * @param [in] pRxMsg 
 *  The received message
 *
 * @return None
 *
// *****************************************************************************/
static void OnReceiveSerialMessage(const struct  UARC_MSG*  pRxMsg, const void *context)
{
    (void)context;
        
    switch(pRxMsg->type)
    {
        case UARC_MT_NULL:
            break;
        
        case UARC_MT_CMD:
            app_cmit_interface_CommandMessageHandler_Serial(pRxMsg);
            break;
        
        case UARC_MT_PING:
            app_cmit_interface_PingMessageHandler_Serial(pRxMsg);
            break;        
        
        case UARC_MT_ASYNC:
            app_cmit_interface_AsyncMessageHandler_Serial(pRxMsg);
            break;
                
        case UARC_MT_HTTP_RSP:
            app_cmit_interface_HttpResponseHandler_Serial(pRxMsg);
            break;
        
        default:
            app_cmit_interface_UnknownMessageHandler_Serial(pRxMsg);
            break;
    }  
}



/***************************************************************************//**
 * @brief
 *   Initialize the CMIT interface subsystem. This interface can be either
 *   a BLE link or a serial UART link to the CMIT
 *
 * @note
 *   All this does is initialize the ring buffers and initialize the BLE link.
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void app_cmit_interface_init(void)
{
    UART_CONTROLLER_INIT_PARAMS initParams = {0};
    UART_CONTROLLER_INIT_PARAMS initSerialParams = {0};

    // initialize the debug output bitmap; This value can also be updated via commands.
    app_interface_debug_destination_map = uMIUBleConfig.sMIUBleConfig.wDebugOutputBitmap;

    // These buffers must be setup before we receive an TX or RX interrupt
    RingBufferInit(&rxCmitRingBuffer, rxCmitData, RING_BUFFER_CMIT_SIZE_RX);
    RingBufferInit(&txCmitRingBuffer, txCmitData, RING_BUFFER_CMIT_SIZE_TX);

    memset (bleMessageBuffer, 0, sizeof(bleMessageBuffer));
    bleMessageIndex = 0;

    initParams.pRbReceive           = &rxCmitRingBuffer;
    initParams.pRbTransmit          = &txCmitRingBuffer;
    initParams.pMessageReader       = &mr;
    // try adding support for incoming messages
    initParams.pfnRxHandler         = &OnReceiveMessage;
    initParams.sendInactivityThresholdMs = BLE_HEARTBEAT_PERIOD;   
    initParams.context              = &uc;///@todo I recommend that the context is just the 'this' pointer in UartControllerInit function.
    
    // Init reader to use the assembly message
    UartMessageReaderInit (&mr, payloadBuffer, sizeof(payloadBuffer));
    
    UartControllerInit(&uc, &initParams);

#ifndef CMIT_SERIAL_PLAIN_ASCII
    // These buffers must be setup before we receive an TX or RX interrupt
    RingBufferInit(&rxCmitSerialRingBuffer, rxCmitSerialData, RING_BUFFER_CMIT_SERIAL_SIZE_RX);
    RingBufferInit(&txCmitSerialRingBuffer, txCmitSerialData, RING_BUFFER_CMIT_SERIAL_SIZE_TX);

    initSerialParams.pRbReceive           = &rxCmitSerialRingBuffer;
    initSerialParams.pRbTransmit          = &txCmitSerialRingBuffer;
    initSerialParams.pMessageReader       = &mr;
    // try adding support for incoming messages
    initSerialParams.pfnRxHandler         = &OnReceiveSerialMessage;
    initSerialParams.sendInactivityThresholdMs = 30000;   
    initSerialParams.context              = &ucSerial;

    UartControllerInit(&ucSerial, &initSerialParams);
    
    Uart_Uart1_RegisterRcvBuffer(&rxCmitSerialRingBuffer);
#else
    Uart_Uart1_RegisterRcvBuffer(&rxCmitRingBuffer);    
#endif  // CMIT_SERIAL_PLAIN_ASCII    

    app_ble_registerReceiveBuffer(&rxCmitRingBuffer);

    /** Initially, we'll indicate that the ble ring buffer is empty. */
    g_ble_ring_buffer_empty = true;

}


/***************************************************************************//**
 * @brief
 *   Tick routine CMIT interface subsystem. This routine takes the data from 
 *   the ring buffer and sends it out the BLE link.
 *
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void app_cmit_interface_tick (uint32_t tickIntervalMs)
{
    uint8_t txChar;
    uint32_t txCount = 0;
    bool    messageSent = false;

#ifndef CMIT_SERIAL_PLAIN_ASCII
    UartControllerTick(&ucSerial, tickIntervalMs);
    
    // Transfer data to be sent from the tx Ringbuffer to the serial stack
    txCount = RingBufferUsed(&txCmitSerialRingBuffer);

    // Get data from TX ringbuffer and send to UART buffer
    // TODO: may want to check that space is available in the serial buffer.
    while (txCount > 0)
    {
        txChar = RingBufferReadOne(&txCmitSerialRingBuffer);
        messageSent = app_cmit_interface_Serial_PushByte(txChar, false);
        txCount--;      
        
        // Flush at end of data cluster (pkt)
        if (0 == txCount)
        {
            messageSent = app_cmit_interface_Serial_PushByte(0, true);
        }            
    }
#endif  // CMIT_SERIAL_PLAIN_ASCII    
    
    UartControllerTick(&uc, tickIntervalMs);
    
    // do nothing if the BLE link is not active
    if ((!app_ble_linkIsActive()) || (!app_ble_isPowered()))
        return;
    
    // check to see if the BTL pipe is open and ready
    if (app_ble_TxIsOpen())
    {
        // Process any necessary bluetooth event so that we can free up data credit
        app_ble_process();

        // Transfer data to be sent from the tx Ringbuffer to the BTLE stack
        txCount = RingBufferUsed(&txCmitRingBuffer);

        if (0u != txCount)
        {
            /** There is something in the ringbuffer, so it's not empty! */
            g_ble_ring_buffer_empty = false;
        }

        // Get data from TX ringbuffer and send to BTLE send engine
        while ((txCount > 0) && (app_ble_buffersAvailable() != 0))
        {
            txChar = RingBufferReadOne(&txCmitRingBuffer);
            messageSent = app_cmit_interface_PushByte(txChar, false);
            txCount--;      
        
            // Flush at end of data cluster (pkt)
            if (0 == txCount)
            {
                messageSent = app_cmit_interface_PushByte(0, true);
                g_ble_ring_buffer_empty = true;
            }            

            // if a BTLE message was sent we may need to process some events to free up some buffers.
            if (messageSent)
            {
                // Process any necessary bluetooth event so we can free up a data credit
                app_ble_process();
            }
        }
    }
    
    // Process any necessary bluetooth event
    app_ble_process();
    while (app_ble_workToDo())
    {
        app_ble_process();
    }
}



/***************************************************************************//**
 * @brief
 *   Tick the cmit interface every time msTicks is updated.
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void app_cmit_interface_tick_on_time_change(void)
{
    static uint32_t prev_ms_ticks = 0;
    EMU_EnterEM1();
    uint32_t ms_ticks = Timers_GetMsTicks();
    if (ms_ticks != prev_ms_ticks)
    {
        uint32_t ms_diff = 0;
        ms_diff = (ms_ticks > prev_ms_ticks) ? (ms_ticks - prev_ms_ticks) : ((UINT32_MAX - prev_ms_ticks) + ms_ticks);
        app_cmit_interface_tick(ms_diff);
        prev_ms_ticks = ms_ticks;
    }
}


/***************************************************************************//**
 * @brief
 *   Call the CMIT tick routine until the BLE buffer is empty (or until a max
 *   number of attempts is reached).
 *
 * @param[in] maxTries - maximum number of attempts before giving up (so as
 *                       to not block forever).
 *
 * @return none
 ******************************************************************************/
void app_cmit_interface_tick_ble_empty(const uint32_t maxTries)
{
    uint32_t countDown = maxTries;
    while ((0u != countDown) && (false == g_ble_ring_buffer_empty))
    {
        app_cmit_interface_tick_on_time_change();
        --countDown;
    }
}


/*****************************************************************************
 * @brief
 *   Takes a char to be sent over air. When a packet's worth is available, 
 * send the packet. Also features optional loopback
 *
 * @param [in] b 
 *  The byte to tx (ignored if flush == true)
 *
 * @param [in] flush
 *  If false, queue b until a full BTLE pkt is ready - then send.
 *  If true, send all queued data immediately (b is ignored). Use at end of assembed pkt
 *
 * @return bool indicating if a buffer was sent
 *
 *****************************************************************************/
static bool app_cmit_interface_PushByte(uint8_t b, bool flush)
{
    bool    bufferSent = false;

    if (false == flush)
    {
        bleMessageBuffer[bleMessageIndex] = b;
        bleMessageIndex++;
    }
    else
    {
        if (0 == bleMessageIndex)
        {
           return bufferSent;
        }
    }
    
    if (bleMessageIndex >= (BLE_NUS_MAX_DATA_LEN) || flush)
        {
            app_ble_sendData (bleMessageBuffer, bleMessageIndex);
        
            bleMessageIndex = 0;
            bufferSent = true;
        } 
            
    return bufferSent;
}


/*****************************************************************************
 * @brief
 *   Takes a char to be sent over the UART. When a packet's worth is available, 
 * send the packet. 
 *
 * @note
 *   There is no reason why the packets being sent over the serial link need
 *   to be limited to 20 bytes, but it seemed to make sense to be consistent.
 *   It also allows for more common code in the CMIT application.
 *
 * @param [in] b 
 *  The byte to tx (ignored if flush == true)
 *
 * @param [in] flush
 *  If false, queue b until a full pkt is ready - then send.
 *  If true, send all queued data immediately (b is ignored). Use at end of assembed pkt
 *
 * @return bool indicating if a buffer was sent
 *
 *****************************************************************************/
static bool app_cmit_interface_Serial_PushByte(uint8_t b, bool flush)
{
    bool    bufferSent = false;

    if (false == flush)
    {
        serialMessageBuffer[serialMessageIndex] = b;
        serialMessageIndex++;
    }
    else
    {
        if (0 == bleMessageIndex)
        {
           return bufferSent;
        }
    }
    
    if (serialMessageIndex >= (BLE_NUS_MAX_DATA_LEN) || flush)
        {
            // Write text string out UART also
            Uart_Uart1PutData((char *)serialMessageBuffer, serialMessageIndex);    
        
            serialMessageIndex = 0;
            bufferSent = true;
        } 
            
    return bufferSent;
}



/*****************************************************************************
 * @brief
 *   process the command message 
 *
 * @param [in] pRxMsg 
 *  The received command message
 *
 * @return None
 *
// *****************************************************************************/
static void app_cmit_interface_CommandMessageHandler(const struct  UARC_MSG* pRxUartMsg)
{    
    APP_RX_MSG rxMsg;
    
    rxMsg.pData         = pRxUartMsg->pPayload;
    rxMsg.dataLength    = pRxUartMsg->length;
    rxMsg.source        = CMIU_INTERFACE_BTLE;
    
    app_message_on_receive(&rxMsg);
}


/*****************************************************************************
 * @brief
 *   Sends a response to the ping message 
 *
 * @param [in] pRxMsg 
 *  The received ping message
 *
 * @return None
 *
// *****************************************************************************/
static void app_cmit_interface_PingMessageHandler(const struct  UARC_MSG*  pRxMsg)
{    
    UartControllerSend(&uc, 
    UARC_MT_GNIP,
    (const uint8_t*) pRxMsg->pPayload,  pRxMsg->length);

    DEBUG_OUTPUT_TEXT("Ping response sent\r\n");
}

/*****************************************************************************
 * @brief
 *   Handle an async (no response required) message 
 *
 * @param [in] pRxMsg 
 *  The received ping message
 *
 * @return None
 *
// *****************************************************************************/
static void app_cmit_interface_AsyncMessageHandler(const struct  UARC_MSG*  pRxMsg)
{    
    DEBUG_OUTPUT_TEXT("Async msg received");
}


/*****************************************************************************
 * @brief
 *   Handle a  response to an HTTP get, coming from server 
 *
 * @param [in] pRxMsg 
 *  The received HTTP content
 *
 * @return None
 *
// *****************************************************************************/
static void app_cmit_interface_HttpResponseHandler(const struct  UARC_MSG*  pRxMsg)
{    
// TODO:    FileDownloadHandleHttpResponse(pRxMsg->pPayload, pRxMsg->length);
}


/*****************************************************************************
 * @brief
 *   Sends a response to an unknown or unhandled message 
 *
 * @param [in] pRxMsg 
 *  The received  message
 *
 * @return None
 *
// *****************************************************************************/
static void app_cmit_interface_UnknownMessageHandler(const struct  UARC_MSG*  pRxMsg)
{
    UartControllerSend(&uc, 
        UARC_MT_ERROR,
        (const uint8_t*) "",  0);

    DEBUG_OUTPUT_TEXT("UnknownMessageHandler:Error response sent\r\n");
}

/****************************************************************************
 *                       Serial link message handlers
 ****************************************************************************/
/*****************************************************************************
 * @brief
 *   process the command message from the serial link 
 *
 * @param [in] pRxMsg 
 *  The received command message
 *
 * @return None
 *
// *****************************************************************************/
static void app_cmit_interface_CommandMessageHandler_Serial(const struct  UARC_MSG* pRxUartMsg)
{    
    APP_RX_MSG rxMsg;
    
    rxMsg.pData         = pRxUartMsg->pPayload;
    rxMsg.dataLength    = pRxUartMsg->length;
    rxMsg.source        = CMIU_INTERFACE_UART;
    
    app_message_on_receive(&rxMsg);
}


/*****************************************************************************
 * @brief
 *   Sends a response to the ping message on the serial link 
 *
 * @param [in] pRxMsg 
 *  The received ping message
 *
 * @return None
 *
// *****************************************************************************/
static void app_cmit_interface_PingMessageHandler_Serial(const struct  UARC_MSG*  pRxMsg)
{    
    UartControllerSend(&ucSerial, 
    UARC_MT_GNIP,
    (const uint8_t*) pRxMsg->pPayload,  pRxMsg->length);

    DEBUG_OUTPUT_TEXT("Ping response sent\r\n");
}

/*****************************************************************************
 * @brief
 *   Handle an async (no response required) message on the serial link
 *
 * @param [in] pRxMsg 
 *  The received ping message
 *
 * @return None
 *
// *****************************************************************************/
static void app_cmit_interface_AsyncMessageHandler_Serial(const struct  UARC_MSG*  pRxMsg)
{    
    DEBUG_OUTPUT_TEXT("Async msg received");
}


/*****************************************************************************
 * @brief
 *   Handle a  response to an HTTP get, coming from server on the serial link
 *
 * @param [in] pRxMsg 
 *  The received HTTP content
 *
 * @return None
 *
// *****************************************************************************/
static void app_cmit_interface_HttpResponseHandler_Serial(const struct  UARC_MSG*  pRxMsg)
{    
// TODO:    FileDownloadHandleHttpResponse(pRxMsg->pPayload, pRxMsg->length);
}


/*****************************************************************************
 * @brief
 *   Sends a response to an unknown or unhandled message 
 *
 * @param [in] pRxMsg 
 *  The received  message
 *
 * @return None
 *
// *****************************************************************************/
static void app_cmit_interface_UnknownMessageHandler_Serial(const struct  UARC_MSG*  pRxMsg)
{
    UartControllerSend(&ucSerial, 
        UARC_MT_ERROR,
        (const uint8_t*) "",  0);

    DEBUG_OUTPUT_TEXT("UnknownMessageHandler:Error response sent\r\n");
}


/***************************************************************************//**
 * @brief
 *   Sends the specified message over the BTLE link.
 *       
 *
 * @param[in] dataPtr
 *   string to be output on debug interface
 *
 * @return none
 ******************************************************************************/
void app_cmit_interface_SendRespMessage (const uint8_t* pData, uint32_t dataLength)
{
    UartControllerSend (&uc, UARC_MT_RESP, (uint8_t *)pData, dataLength);    
}


/***************************************************************************//**
 * @brief
 *   Sends the specified message over the serial link.
 *       
 *
 * @param[in] dataPtr
 *   string to be output on debug interface
 *
 * @return none
 ******************************************************************************/
void app_cmit_interface_SendRespMessage_Serial (const uint8_t* pData, uint32_t dataLength)
{
    UartControllerSend (&ucSerial, UARC_MT_RESP, (uint8_t *)pData, dataLength);    
}



/***************************************************************************//**
 * @brief
 *   Outputs the specified debug string to the indicated link.
 *
 * @note
 *   This function looks at a configuration value to determine if the debug
 *   string should go out the BTLE link or the UART.
 *       
 *
 * @param[in] dataPtr
 *   string to be output on debug interface
 *
 * @return none
 ******************************************************************************/
void app_cmit_interface_WriteDebugString (const char *dataPtr)
{
    
    if (app_interface_debug_destination_map & APP_CMIT_INTERFACE_DEBUG_ON_BTLE)
    {
        // write text string to the BTLE link
        UartControllerSend (&uc, UARC_MT_ASYNC, (uint8_t *)dataPtr, strlen(dataPtr));
    }
    
    if (app_interface_debug_destination_map & APP_CMIT_INTERFACE_DEBUG_ON_UART)
    {
#ifdef CMIT_SERIAL_PLAIN_ASCII        
        // Write text string out UART also
        Uart_Uart1PutData(dataPtr, strlen(dataPtr));    
#else
        // write text string to the BTLE link
        UartControllerSend (&ucSerial, UARC_MT_ASYNC, (uint8_t *)dataPtr, strlen(dataPtr));
#endif // CMIT_SERIAL_PLAIN_ASCII        
    }
}

/***************************************************************************//**
* @brief
*   Outputs the specified debug string and data to the indicated link.
*
* @note
*   This function looks at a configuration value to determine if the debug
*   string should go out the BTLE link or the UART.
*       
 *
* @param[in] dataPtr - string to be output on debug interface
* @param[in] pbyDataBuffer - points to the data to be addded to output
* @param[in] byDataLength - gives the number of bytes in the data
*
* @return none
******************************************************************************/
void app_cmit_interface_WriteDebugStringAndData (const char *dataPtr, uint8_t *pbyDataBuffer, uint8_t byDataLength)
{
    UU32 toBeConverted;
   uint32_t i;
   // uint8_t str[10];
    char tempMessageBuffer[128];
    
    EFM_ASSERT(byDataLength < 5);
    
    toBeConverted.U32 = 0UL;
    
    // Move bytes 
    for(i=0; i<byDataLength; i++)
    {
        toBeConverted.U8[i] = pbyDataBuffer[i];       
    }
    
    snprintf(tempMessageBuffer, sizeof(tempMessageBuffer), "%s%u\r\n", dataPtr, toBeConverted.U32);
    
    app_cmit_interface_WriteDebugString(tempMessageBuffer);
}


