/******************************************************************************
*******************************************************************************
**
**         Filename: app_datalog.h
**
**           Author: Brian Arnberg
**          Created: 2015.10.21
**
**     Last Edit By:
**        Last Edit:
**
** Revision History:
**       2015.10.21: First created (Brian Arnberg)
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential
**    property of Neptune Technology Group. The user, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_DATALOG_H
#define __APP_DATALOG_H

#include "typedefs.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
/** This is backed up to the USER_DATA storage after a successful upload. */
typedef struct sTag_Datalog
{
    /** Most recent reading from ARB, not necessarily stored yet */
    uint32_t currentRecord;
    /** Address where current record will be stored. */
    uint32_t recordAddress;
    /** Address of oldest record that still needs to go to the server */
    uint32_t logAddress;
    /** Number of logging events missed. */
    uint32_t missedLogs;
} sDatalog_t;

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
/** Define special data log value to indicate the data log storage space is
 *  uninitialized. */
#define DATALOG_FIRSTREADING_UNINITIALIZED  0xFF123456

/** Define special data log value to indicate that the data log storage space
 *  was manipulated by the bootloader. This means that the data logged
 *  readings are garbage and need to be totally re-initialized. */
#define DATALOG_FIRSTREADING_BLMANIPULATED  0xFF654321

/** This specific pattern indicates that there was some sort of undefined
 *  error. */
#define UNKNOWN_RECORD_ERROR    0xFE123456u

/** This indicates that the CMIU experience a POR prior to this record being
 *  stored. We should ignore readings prior to this one. */
#define POR_INDICATOR_RECORD    0xDDDDDDDDu

/** This indicates that we've updated the recording interval. In other words,
 *  we can no longer guarantee the age of any record older than this. */
#define RECORDING_INTERVAL_CHANGE_INDICATOR  0xEEEEEEEEu

/** The default starting index for everything should be 0 */
#define DEFAULT_RECORD_ADDRESS  DATALOG_START
#define DEFAULT_LOG_ADDRESS     DATALOG_START

/** The maximum number of days worth of datalog records should be 96 days. */
#define MAX_DATALOG_DAYS 96u

/** The number of seconds in a day. Used to calculate the number of records
    to return if it has been a long time since we last called in. */
#define SECONDS_PER_DAY (60u*60u*24u)

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_datalog_init(void);
extern bool app_datalog_re_init(uint32_t special_record);
extern bool app_datalog_setCurrentRecord(uint32_t newRecord);
extern bool app_datalog_storeCurrentRecord(void);
extern uint32_t app_datalog_getBackLog(uint32_t * const paBackLog,
                                       const uint32_t rangeLimit);
extern bool app_datalog_cleanBackLog(bool messageSent);
extern bool app_datalog_getRecordRange(uint32_t * const paRecord,
                                       uint32_t startRecord,
                                       uint32_t totalRecords);
extern void app_datalog_reverseArray(uint32_t * const paRecord,
                                     const uint32_t length);
extern uint32_t app_datalog_formatRecord(uint32_t arbReading,
                                         uint8_t backflowFlag,
                                         uint8_t leakFlag);
extern uint32_t app_datalog_getCurrentRecord(void);
extern uint32_t app_datalog_getRecordAddress(void);
extern uint32_t app_datalog_getLogAddress(void);
extern uint32_t app_datalog_getMissedLogs(void);
extern bool     app_datalog_getFatalFlag(void);
extern uint64_t app_datalog_getLatestRecordTime(uint32_t interval,
                                                uint64_t next_time);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_DATALOG_H

