/******************************************************************************
*******************************************************************************
**
**         Filename: app_command.c
**
**           Author: Troy Harstad
**          Created: 7/1/15
**
**     Last Edit By:
**        Last Edit:
**
**      Description:
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The user, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/************************************************************************
* @file app_command.c
*
* @brief This file contains the application code for command handling
*
************************************************************************/


/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "typedefs.h"
#include "TagTypes.h"
#include "app_rtc.h"
#include "DebugTrace.h"
#include "app_scheduler.h"
#include "cmiuFlash.h"
#include "CmiuAppConfiguration.h"
#include "em_assert.h"
#include "em_rmu.h"
#include "em_wdog.h"
#include "app_rmu.h"
#include "app_packet_builder.h"
#include "app_timers.h"
#include "app_power.h"
#include "modem.h"
#include "modem_power.h"
#include "app_gpio.h"
#include "app_message.h"
#include "app_command.h"
#include "app_arb.h"
#include "app_cmit_interface.h"
#include "app_wdog.h"
#include "app_cellular.h"
#include "app_ble.h"
#include "app_scratch_ram.h"
#include "app_command_helper.h"
#include "cmiuSpi.h"
#include "app_datalog.h"
#include "app_cmiu_app.h"
#include "app_operatingMode.h"
#include "cmiuImages.h"
#include "app_uart.h"
#include "em_usart.h"
#include "efm32lg_uart.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
/** New Application firmware image */
#define NEW_APPLICATION             BIT8
/** New Read-only configuration parameter image */
#define NEW_CONFIGURATION           BIT9
/** New Encryption key image */
#define NEW_ENCRYPTION              BITA
/** New Bluetooth operational configuration image */
#define NEW_BLE_CONFIGURATION       BITB



#define DELAY_MSEC_FOR_MODEM_POWER_UP  15000
#define DELAY_MSEC_FOR_MODEM_POWER_OFF 15000
#define MSEC_FOR_MODEM_KEEP_POWERED    30000
#define DELAY_MSEC_BEFORE_CMIU_RESET   5000

#define USER_DATA_PACKET_SIZE (S_CMIU_PACKET_HEADER_PACKED_SIZE + \
                  S_CMIU_STATUS_PACKED_SIZE  + \
                  S_READING_RECORDING_PACKED_SIZE + \
                  0x20)

static const uint32_t c_2r_interstate_delay_ms = 500;

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static void app_command_basicConfig_handler(APP_TX_MSG* pResponseMsg);
static void app_command_detailedConfig_handler(APP_TX_MSG* pResponseMsg);
static void app_command_intervaldata_handler(APP_TX_MSG* pResponseMsg);

// General purpose states and state change functions
static void app_command_state_idle(void);
static void app_command_state_startDelay(void);
static void app_command_state_durationDelay(void);
static void app_command_state_modemShutdownDelay(void);

// Check User Data states (for config recovery after FOTA)
static void app_command_checkUserData_Enter(void);

// LTE Toggle states and state change functions
static void app_command_toggleLTE_Enter(void);
static void app_command_state_toggleLTE_TurnOn(void);
static void app_command_state_toggleLTE_TurnOff(void);

// Get LTE Signal Quality states and state change functions
static void app_command_getSignalQuality_Enter(void);
static void app_command_state_getSignalQuality_powerModem(void);
static void app_command_state_getSignalQuality_gatherResult(void);

// Get CAN+ Data states and state change functions
static void app_command_getCANData_Enter(void);
static void app_command_state_getCANData_powerModem(void);
static void app_command_state_getCANData_gatherResult(void);
static void app_command_state_getCANData_modemShutDown(void);

// LTE Carrier Assert states and state change functions
static void app_command_LTECarrierAssert_Enter(void);
static void app_command_state_LTECarrierAssert_powerModem(void);
static void app_command_state_LTECarrierAssert_TurnOn(void);
static void app_command_state_LTECarrierAssert_TurnOff(void);

// BLE Carrier Assert states and state change functions
static void app_command_BLECarrierAssert_Enter(void);
static void app_command_state_BLECarrierAssert_startupModule(void);
static void app_command_state_BLECarrierAssert_TurnOn(void);
static void app_command_state_BLECarrierAssert_TurnOff(void);

// Select Power Mode states and state change functions
static void app_command_selectPowerMode_Enter(void);
static void app_command_state_selectPowerMode_TurnOn(void);
static void app_command_state_selectPowerMode_TurnOff(void);

// Sleep seconds states and state change functions
static void app_command_sleepSeconds_Enter(void);
static void app_command_state_sleepSeconds_Sleep(void);
static void app_command_state_sleepSeconds_Wake(void);

// Read Connected Devices states and state change functions
static void app_command_readConnectedDevice_Enter(void);
static void app_command_state_readConnectedDevice_autodetect(void);
static void app_command_state_readConnectedDevice_Read(void);
static void app_command_state_readConnectedDevice_End(void);

// Erase Memory states and state change functions
static void app_command_eraseMemory_Enter(void);
static void app_command_state_eraseMemory_extFlashTurnOn(void);
static void app_command_state_eraseMemory_Erase(void);
static void app_command_state_eraseMemory_extFlashTurnOff(void);
static void app_command_state_eraseMemory_End(void);

// Set Recording and Reporting Intervals states and state change functions
static void app_command_setIntervals_Enter(void);
static void app_command_state_setIntervals_extFlashTurnOn(void);
static void app_command_state_setIntervals_programConfig(void);
static void app_command_state_setIntervals_updateDatalog(void);
static void app_command_state_setIntervals_disableFlash(void);
static void app_command_state_setIntervals_syncInternalConfig(void);
static void app_command_state_setIntervals_validateConfig(void);
static void app_command_state_setIntervals_handleError(void);
static void app_command_state_setIntervals_resetCmiu(void);

static void app_command_powerDownModem(void);
static void app_command_checkModemPower(void);

// Modem FOTA state functions
static void        app_command_state_modemUpdate_Enter(void);
static void        app_command_state_modemUpdate_Tick(void);

// Request APN states and state change functions
static void app_command_requestApn_Enter(void);
static void app_command_state_requestApn_powerModem(void);
static void app_command_state_requestApn_gatherResult(void);

// Update APN states and state change functions
static void app_command_updateApn_Enter(void);
static void app_command_state_updateApn_powerModem(void);
static void app_command_state_updateApn_update(void);

/* Run Connectivity Test states and state change functions */
static void app_command_runConnectivityTest_Enter(void);
static void app_command_state_runConnectivityTest_cellState(void);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
static S_COMMAND_IMAGE_UPDATE sConfigImageCommand;
static S_COMMAND_IMAGE_UPDATE sAppImageCommand;
static S_COMMAND_IMAGE_UPDATE sEncryptImageCommand;
static S_COMMAND_IMAGE_UPDATE sBleImageCommand;
static S_COMMAND_IMAGE_UPDATE sTelitImageCommand;
static S_COMMAND_IMAGE_UPDATE sArbImageCommand;

// Command param structs
static TOGGLE_LTE_PARAMS            deferredToggleLteParams;
static LTE_CARRIER_ASSERT_PARAMS    deferredCarrierAssertParams;
static BLE_CARRIER_ASSERT_PARAMS    deferredBleCarrierAssertParams;
static POWER_MODE_PARAMS            deferredPowerModeParams;
static SLEEP_SECONDS_PARAMS         deferredSleepSecondsParams;
static READ_DEVICES_PARAMS          deferredReadDevicesParams;
static ERASE_MEMORY_PARAMS          deferredEraseMemoryParams;
static GET_MODEM_INFO_PARAMS        deferredGetModemInfoParams;
static COMMAND_UPDATE_APN_PARAMS    deferredUpdateApnParams;
static COMMAND_RUN_CONNECTIVITY_PARAMS deferredRunConnectivityTestParams;
static S_RECORDING_REPORTING_INTERVAL deferredSetIntervalsParams;

// accessed through getDeferredModemUpdateParams
static COMMAND_UPDATE_MODEM_FIRMWARE_PARAMS deferredModemUpdateParams;

// Command state function pointer
static PFN_STATE_VOID_VOID pfnCommandCurrentState;
static PFN_STATE_VOID_VOID pfnCommandNextState;

static uint32_t timeElapsedMs;
static uint32_t startDelayEndMs;
static uint32_t durationDelayEndMs;
static uint32_t modemShutdownDelayEndMs;
static bool testForever;
static bool waitTillEnd;
static uint32_t modemPowerOnTimeMs;

// keeps up with when the modem is ready for AT commands
static bool modemReadyForCmds;

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/


/*****************************************************************************
 * @brief
 *   Initializes command module
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_command_init(void)
{
    // No commands due by default
    // Only init if it was not a reset pin reset (mag swipe) and not a system
    // request (software) reset.  If we've already received a command may want to
    // preserve through reset
    if(((app_rmu_resetCause_Get() & RMU_RSTCAUSE_EXTRST) != RMU_RSTCAUSE_EXTRST) &&
       ((app_rmu_resetCause_Get() & RMU_RSTCAUSE_SYSREQRST) != RMU_RSTCAUSE_SYSREQRST))
    {
        memset(&sConfigImageCommand,    0, sizeof(sConfigImageCommand));
        memset(&sAppImageCommand,       0, sizeof(sAppImageCommand));
        memset(&sEncryptImageCommand,   0, sizeof(sEncryptImageCommand));
        memset(&sBleImageCommand,       0, sizeof(sBleImageCommand));
        memset(&sTelitImageCommand,     0, sizeof(sTelitImageCommand));
        memset(&sArbImageCommand,       0, sizeof(sArbImageCommand));
    }

    app_command_resetState();
}

/*****************************************************************************
 * @brief
 *   Reset state variables for command module
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_command_resetState(void)
{
    /** Function pointer for 'next' state defaults to idle. */
    pfnCommandNextState = app_command_state_idle;

    if ((app_rmu_resetCause_Get() & RMU_RSTCAUSE_EXTRST) == RMU_RSTCAUSE_EXTRST)
    {
        /** Reset was a magswipe, so initially we idle. */
        pfnCommandCurrentState = app_command_state_idle;
    }
    else if (E_CMIU_OPERATINGMODE_STATE_CELLULAR == app_operatingMode_state_get())
    {
        /** We're actually in a cell session, so let's make the default state
         * the idle state. */
        pfnCommandCurrentState = app_command_state_idle;
    }
    else if ((app_rmu_resetCause_Get() & RMU_RSTCAUSE_PORST) == RMU_RSTCAUSE_PORST)
    {
        /** Reset was a POR (or FOTA), so we need to validate
         * the user data. */
        pfnCommandCurrentState = app_command_checkUserData_Enter;
    }
    else
    {
        /** By default, we want to IDLE. */
        pfnCommandCurrentState = app_command_state_idle;
    }

    // Init command processing variables
    timeElapsedMs = 0;
    startDelayEndMs = 0;
    durationDelayEndMs = 0;
    testForever = false;
    waitTillEnd = false;

    // set the modem on time to a default so we know
    //   when it was powered on for command processing
    modemPowerOnTimeMs = 0;
    modemReadyForCmds = false;
}

/***************************************************************************//**
 * @brief
 *  Executes the current command for one tick. This function basically calls
 *   the current command function.
 *
 * @note
 *
 *
 * @param[in] tickIntervalMs - number of msec since last called
 *
 * @return None
 ******************************************************************************/
void app_command_tick(uint32_t tickIntervalMs)
{
    // Update elapsed time
    timeElapsedMs += tickIntervalMs;

    if (NULL != pfnCommandCurrentState)
    {
        // Tick current state
        pfnCommandCurrentState();
    }

    if (E_CMIU_OPERATINGMODE_STATE_CELLULAR != app_operatingMode_state_get())
    {
        app_command_checkModemPower();
    }
}

/***************************************************************************//**
 * @brief
 *  Called as command mode is exitted to do any necessary cleanup.
 *
 * @note
 *
 *
 * @param[in] none
 *
 * @return None
 ******************************************************************************/
void app_command_end(void)
{
    // check to see if we should turn off the modem
    if (modemPowerOnTimeMs != 0)
    {
        DEBUG_TRACE ("Power down modem.\n\r");
        app_command_powerDownModem();

    }
}

/***************************************************************************//**
 * @brief
 *  Checks to see if the modem has been powered on without use for a long 
 *   period. If so then it will power down the modem.
 *
 * @note
 *
 *
 * @param[in] none
 *
 * @return None
 ******************************************************************************/
void app_command_checkModemPower(void)
{
    // check to see if we should turn off the modem
    if ((timeElapsedMs > (modemPowerOnTimeMs + MSEC_FOR_MODEM_KEEP_POWERED)) &&
        (modemPowerOnTimeMs != 0))
    {
        DEBUG_TRACE ("Power down modem as there have been no requests.\n\r");
        app_command_powerDownModem();

        // Set modem shutdown delay delay
        modemShutdownDelayEndMs = timeElapsedMs + DELAY_MSEC_FOR_MODEM_POWER_OFF;

        // Next state is modem shutdown state
        pfnCommandCurrentState = app_command_state_modemShutdownDelay;

    }
}
 
    
//*****************************************************************************
// GENERAL COMMAND STATES
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Function to be called in the idle command state. This routine does nothing.
 *
 * @note
 *
 *
 * @param[in] none
 *
 * @return None
 ******************************************************************************/
static void app_command_state_idle(void)
{
    // Safe, do nothing state
}

/***************************************************************************//**
 * @brief
 *  This routine checks to see if the start delay time has completed and if
 *   so, it updates the current state to the next state.
 *
 * @note
 *
 *
 * @param[in] none
 *
 * @return None
 ******************************************************************************/
static void app_command_state_startDelay(void)
{
    
    // check the delay time
    if(timeElapsedMs > startDelayEndMs)
    {
        // Next state becomes current state
        pfnCommandCurrentState = pfnCommandNextState;
    }

}

/***************************************************************************//**
 * @brief
 *  This routine checks to see if the modem is ready to accept AT commands.
 *   When it is ready we move on to the next state.
 *
 * @note
 *
 *
 * @param[in] none
 *
 * @return None
 ******************************************************************************/
static void app_command_state_waitForModem(void)
{
    // if the modem is not ready try an AT command
    if (!modemReadyForCmds)
    {
        if (modem_isReadyForCommands())
        {
            DEBUG_TRACE ("MODEM READY!\n\r");
            modemReadyForCmds = true;
        }
    }
    
    // once the modem is ready check the delay
    if ((modemReadyForCmds) && (timeElapsedMs > startDelayEndMs))
    {
        // Next state becomes current state
        pfnCommandCurrentState = pfnCommandNextState;
    }

}

/***************************************************************************//**
 * @brief
 *  This routine checks to see if the duration delay time has completed and if
 *   so, it updates the current state to the next state.
 *
 * @note
 *
 *
 * @param[in] none
 *
 * @return None
 ******************************************************************************/
static void app_command_state_durationDelay(void)
{
    if((timeElapsedMs > durationDelayEndMs) && (testForever == false))
    {
        // Next state becomes current state
        pfnCommandCurrentState = pfnCommandNextState;
    }
}

/***************************************************************************//**
 * @brief
 *  This routine checks to see if the modem is powered off or if the max 
 *   wait time has elapsed, and if so, it updates the current state to the 
 *   next state.
 *
 * @note
 *
 *
 * @param[in] none
 *
 * @return None
 ******************************************************************************/
static void app_command_state_modemShutdownDelay(void)
{
    TRANSPORT_MANAGER* pTransport;

    pTransport = (TRANSPORT_MANAGER*)app_cellular_get_transport();

    // tick the modem library to let it finish shutting down
    TransportManagerTick(pTransport, 10);

    // check to see if the modem has been powered down
    if (!modem_power_modemIsPowered())
    {
        // shut down so change to idle
        pfnCommandCurrentState = app_command_state_idle;
    }
    
    // shutdown time expired so force the modem off
    if(timeElapsedMs > modemShutdownDelayEndMs)
    {
        app_command_helper_powerOffModem();
        
        // change to idle state
        pfnCommandCurrentState = app_command_state_idle;
    }
}

//*****************************************************************************
// Check USER DATA States
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Prepare to check user data.
 *
 * @note
 *  This is used to determine whether the internal configuration needs to be
 *  updated to match the configuration in user data. This would typically be
 *  employed after a FOTA (but the check is cheap, so always do it!).
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_checkUserData_Enter(void)
{
    APP_RX_MSG rxMsg;

    rxMsg.pData         = (uint8_t *)USER_DATA_START;
    rxMsg.dataLength    = USER_DATA_LENGTH;
    rxMsg.source        = CMIU_INTERFACE_UART;

    /** Set this now so that it doesn't interfere with anything. */
    pfnCommandCurrentState = app_command_state_idle;

    /** This will eventually update the pfnCommandCurrentState value. */
    app_message_on_receive(&rxMsg);
}

//*****************************************************************************
// TOGGLE LTE STATES
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Prepare to run the toggle LTE command.
 *
 * @note
 *  This command powers on the LTE modem for the indicated time.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_toggleLTE_Enter(void)
{
    DEBUG_TRACE ("toggle LTE command- delay: %ld duration: %ld status: %d\n\r",
                (uint32_t)deferredToggleLteParams.startDelayInMsec,
                (uint32_t)deferredToggleLteParams.durationInMsec,
                deferredToggleLteParams.enableDisableModem);

    // Next state is start delay state
    pfnCommandCurrentState = app_command_state_startDelay;

    // Set start delay
    startDelayEndMs = timeElapsedMs + deferredToggleLteParams.startDelayInMsec;

    // State after start delay state is Turn On LTE state
    pfnCommandNextState = app_command_state_toggleLTE_TurnOn;

    // Check if duration is 0, which means stay in this mode forever
    if(deferredToggleLteParams.durationInMsec == 0)
    {
        testForever = true;
    }

    else
    {
        testForever = false;
    }
}


/***************************************************************************//**
 * @brief
 *  Powers on the LTE modem as part of the toggle LTE modem command. It then
 *   sets up the delay for the indicated duration.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_toggleLTE_TurnOn(void)
{
    // Power modem on - use common routine even though we do not need the GPIO
    //  access for this command
    app_command_helper_powerOnModem();

    // Set duration delay
    durationDelayEndMs = startDelayEndMs + deferredToggleLteParams.durationInMsec;

    // Next state is start delay state
    pfnCommandCurrentState = app_command_state_durationDelay;

    // Next state after duration delay is to Turn Off LTE
    pfnCommandNextState = app_command_state_toggleLTE_TurnOff;
}

/***************************************************************************//**
 * @brief
 *  Powers down the LTE modem as part of the toggle LTE modem command. It then
 *   sets command functions back to the idle routines.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_toggleLTE_TurnOff(void)
{
    
    // properly shutdown the modem by sending the command to cause a shutdown
    app_command_powerDownModem();

    // Set modem shutdown delay delay
    modemShutdownDelayEndMs = timeElapsedMs + DELAY_MSEC_FOR_MODEM_POWER_OFF;

    // Next state is modem shutdown state
    pfnCommandCurrentState = app_command_state_modemShutdownDelay;

}


//*****************************************************************************
// GET SIGNAL QUALITY STATES
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Prepare to run the get Signal Quality command.
 *
 * @note
 *  This command get the current signal quality (RSSI) from the LTE modem.
 *  If the modem is not powered on it must do that first.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_getSignalQuality_Enter(void)
{

    // the modem must be powered up if it is not on before responding to these AT commands.
    // A value of 0 indicates that the modem is not powered on and should be.
    if (modemPowerOnTimeMs == 0)
    {
        // Next state is to power up the modem, prior to the start delay
        pfnCommandCurrentState = app_command_state_getSignalQuality_powerModem;
    }
    else
    {
        // modem is already powered on
        pfnCommandCurrentState = app_command_state_getSignalQuality_gatherResult;
    }


}


/***************************************************************************//**
 * @brief
 *  Powers on the LTE modem as part of the get signal quality command. It then
 *   sets up the delay to allow the modem to initialize.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_getSignalQuality_powerModem(void)
{
    // Turn on modem prior to the start delay since it takes minimum of 15
    // seconds to turn on and be ready for an AT command we want to get this started
    app_command_helper_powerOnModem();

    // Next state is start delay state
    pfnCommandCurrentState = app_command_state_waitForModem;  
    modemReadyForCmds = false;

    // no minimum delay needed so set to the current time 
    startDelayEndMs = timeElapsedMs;

    // State after start delay state is to get the signal quality
    pfnCommandNextState = app_command_state_getSignalQuality_gatherResult;
}

/***************************************************************************//**
 * @brief
 *  Calls the modem library through the transport manager to get the signal
 *   quality. It specifies to leave the modem powered on in case there are
 *   additional requests.
 *
 * @note
 *  If there are no requests for signal quality for 30 seconds the modem will
 *   be powered off.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_getSignalQuality_gatherResult(void)
{
    TRANSPORT_MANAGER* pTransport;
    APP_TX_MSG responseMsg;
    // use the same structure for all CON_CMD_GET_INFO commands
    GET_CAN_DATA_PARAMS   getCanDataParams;

    // Initialise the response to a scratch buffer, and default to
    // sending to the port from whence it came.
    responseMsg.pData       = app_scratch_ram_get(&responseMsg.bufferSize);
    responseMsg.dataLength  = 0;
    responseMsg.destination = deferredGetModemInfoParams.destination;

    pTransport = (TRANSPORT_MANAGER*)app_cellular_get_transport();

    // save time of the most recent request
    modemPowerOnTimeMs = timeElapsedMs;

    // leave the modem on since likely to get more requests
    getCanDataParams.leaveModemOn = true;

    // The following could use CON_CMD_GET_SIGNAL_QUALITY instead of
    // CON_CMD_GET_INFO, but then the CMIU Manager won't have updated
    // modem information fields.
    TransportManagerCommand(pTransport, CON_CMD_GET_INFO, &getCanDataParams);

    // It takes 2 ticks to process this command. The first tick changes the
    //  the state from idle to the command state and the second tick actually
    //  processes the command.
    TransportManagerTick(pTransport, 10);
    TransportManagerTick(pTransport, 10);

    // Get signal quality data from the modem and build it into a response
    responseMsg.dataLength = app_packet_builder_build_signalQualityResponse(responseMsg.pData,
                responseMsg.bufferSize);

    if (responseMsg.dataLength > 0)
    {
        app_message_send_response((APP_TX_MSG*)&responseMsg);

    }


    // Command done, back to idle state
    pfnCommandCurrentState = app_command_state_idle;
    pfnCommandNextState = app_command_state_idle;
}

/***************************************************************************//**
 * @brief
 *  Prepare to run the command which updates the modem over the air. The routine
 *   send a command to the modem library.
 *
 * @note
 *  Currently the modem is a Telit LE910-SVG.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void        app_command_state_modemUpdate_Enter(void)
{
    TRANSPORT_MANAGER* pTransport;
    static UPDATE_MODEM_FIRMWARE_PARAMS        updateModemParams;

    pTransport = (TRANSPORT_MANAGER*)app_cellular_get_transport();

    updateModemParams.ftpServer = deferredModemUpdateParams.ftpServer;
    updateModemParams.ftpPort = deferredModemUpdateParams.ftpPort;
    updateModemParams.username = deferredModemUpdateParams.username;
    updateModemParams.password = deferredModemUpdateParams.password;
    updateModemParams.fotaFilename = deferredModemUpdateParams.fotaFilename;
    updateModemParams.newFirmwareVersion = deferredModemUpdateParams.newFirmwareVersion;
    
    // Send the command to the modem library
    TransportManagerCommand(pTransport, CON_CMD_UPDATE_MODEM_FIRMWARE, &updateModemParams);
    TransportManagerTick(pTransport, 10);

    // cycle through 
    pfnCommandCurrentState = app_command_state_modemUpdate_Tick;
    
}

/***************************************************************************//**
 * @brief
 *  Routine to continue the process of updating the modem firmware by providing
 *   time for the modem library to run.
 *
 * @note
 *  Currently the modem is a Telit LE910-SVG.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void        app_command_state_modemUpdate_Tick(void)
{
    TRANSPORT_MANAGER* pTransport;
    E_CONNECTION_STATE tmState;

    pTransport = (TRANSPORT_MANAGER*)app_cellular_get_transport();

    
    // Keep the modem processing
    TransportManagerTick(pTransport, 10);
    
    // Get transport manager state
    tmState = TransportManagerGetState(pTransport, QRY_CONNECTION_STATE, NULL);

    // update completed either successful or not
    if (tmState == CON_STATE_CONNECTION_CLOSED)
    {
        // finished 
        pfnCommandCurrentState = app_command_state_idle;
    }
}


//*****************************************************************************
// GET CAN+ Data STATES
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Prepare to run the get CAN+ Data command.
 *
 * @note
 *  This command gets all of the CAN+ (Collection of Assorted Numbers) from the
 *   modem. The routine must power on the modem if it is not already on.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_getCANData_Enter(void)
{
    DEBUG_TRACE ("get CAN+ Datacommand-\n\r");

    // the modem must be powered up if it is not on before responding to these AT commands
    if (modemPowerOnTimeMs == 0)
    {
        // Next state is to power up the modem, prior to the start delay
        pfnCommandCurrentState = app_command_state_getCANData_powerModem;
    }
    else
    {
        // modem is already powered on
        pfnCommandCurrentState = app_command_state_getCANData_gatherResult;
    }


}


/***************************************************************************//**
 * @brief
 *  Powers on the LTE modem as part of the get CAN+ data command. It then
 *   sets up the delay to allow the modem to initialize.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_getCANData_powerModem(void)
{
    // Turn on modem prior to the start delay since it takes minimum of 15
    // seconds to turn on and be ready for an AT command we want to get this started
    app_command_helper_powerOnModem();

    // Next state is start delay state
    pfnCommandCurrentState = app_command_state_waitForModem; 
    modemReadyForCmds = false;  

    // delay 5 seconds so that +CNUM will return the phone number 
    startDelayEndMs = timeElapsedMs + 5000;  

    // State after start delay state is gather the data from the modem
    pfnCommandNextState = app_command_state_getCANData_gatherResult;
}

/***************************************************************************//**
 * @brief
 *  Calls the modem library through the transport manager to get all of
 *   desired values from the modem. It then powers down the modem and
 *   and sets the current command functions back to idle.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_getCANData_gatherResult(void)
{
    TRANSPORT_MANAGER* pTransport;
    APP_TX_MSG responseMsg;
    // use the same structure for all CON_CMD_GET_INFO commands
    GET_CAN_DATA_PARAMS   getCanDataParams;

    // Initialise the response to a scratch buffer, and default to
    // sending to the port from whence it came.
    responseMsg.pData       = app_scratch_ram_get(&responseMsg.bufferSize);
    responseMsg.dataLength  = 0;
    responseMsg.destination = deferredGetModemInfoParams.destination;

    pTransport = (TRANSPORT_MANAGER*)app_cellular_get_transport();

    getCanDataParams.leaveModemOn = true;

    // Send the command to the modem library
    TransportManagerCommand(pTransport, CON_CMD_GET_INFO, &getCanDataParams);

    // It takes 2 ticks to process this command. The first tick changes the
    //  the state from idle to the command state and the second tick actually
    //  processes the command.
    TransportManagerTick(pTransport, 10);
    TransportManagerTick(pTransport, 10);

    // Get signal quality data from the modem and build it into a response
    responseMsg.dataLength = app_packet_builder_build_canData(responseMsg.pData,
                responseMsg.bufferSize);

    if (responseMsg.dataLength > 0)
    {
        app_message_send_response((APP_TX_MSG*)&responseMsg);

    }
    
    pfnCommandCurrentState = app_command_state_getCANData_modemShutDown;

}

/***************************************************************************//**
 * @brief
 *  Shuts down the LTE modem as part of the get CAN+ data command. 
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_getCANData_modemShutDown(void)
{
    // properly shutdown the modem by sending the command to cause a shutdown
    app_command_powerDownModem();

    // Set modem shutdown delay delay
    modemShutdownDelayEndMs = timeElapsedMs + DELAY_MSEC_FOR_MODEM_POWER_OFF;

    // Next state is modem shutdown state
    pfnCommandCurrentState = app_command_state_modemShutdownDelay;
}



//*****************************************************************************
// LTE CARRIER STATES
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Prepare to run the LTE Carrier Assert command.
 *
 * @note
 *  This command calls modem library routines to send the indicated carrier
 *   signal from the LTE modem.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_LTECarrierAssert_Enter(void)
{
    DEBUG_TRACE ("LTE Carrier Assert command- delay: %ld duration: %ld band: %d chan: %d gain: %d\n\r",
                (uint32_t)deferredCarrierAssertParams.startDelayInMsec,
                (uint32_t)deferredCarrierAssertParams.durationInMsec,
                deferredCarrierAssertParams.rfTestMode.rfBand,
                deferredCarrierAssertParams.rfTestMode.rfChannel,
                deferredCarrierAssertParams.rfTestMode.rfPowerLevel);

    // Next state is to power up the modem, prior to the start delay
    pfnCommandCurrentState = app_command_state_LTECarrierAssert_powerModem;

    // Check if duration is 0, which means stay in this mode forever
    if(deferredCarrierAssertParams.durationInMsec == 0)
    {
        testForever = true;
    }

    else
    {
        testForever = false;
    }
}

/***************************************************************************//**
 * @brief
 *  Powers on the LTE modem as part of the LTE assert carrier command. It then
 *   sets up the delay to allow the modem to initialize.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_LTECarrierAssert_powerModem(void)
{
    // Turn on modem prior to the start delay since it takes minimum of 15
    // seconds to turn on and be ready for an AT command we want to get this started
    app_command_helper_powerOnModem();

    // Next state is start delay state
    pfnCommandCurrentState = app_command_state_waitForModem; 
    modemReadyForCmds = false;  

    // use delay specified in the command
    startDelayEndMs = timeElapsedMs + deferredCarrierAssertParams.startDelayInMsec;

    // State after start delay state is Turn On LTE state
    pfnCommandNextState = app_command_state_LTECarrierAssert_TurnOn;
}


/***************************************************************************//**
 * @brief
 *  Calls the modem library through the transport manager to turn on the
 *   specified carrier signal. It then sets up the parameters to time
 *   the duration.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_LTECarrierAssert_TurnOn(void)
{
    TRANSPORT_MANAGER* pTransport;
    ASSERT_LTE_CARRIER_PARAMS   lteCarrierParams;

    pTransport = (TRANSPORT_MANAGER*)app_cellular_get_transport();

    DEBUG_TRACE("Assert LTE Carrier.\n\r");

    lteCarrierParams.rfBand = deferredCarrierAssertParams.rfTestMode.rfBand;
    lteCarrierParams.rfChannel = deferredCarrierAssertParams.rfTestMode.rfChannel;
    lteCarrierParams.rfPowerLevel = deferredCarrierAssertParams.rfTestMode.rfPowerLevel;

    TransportManagerCommand(pTransport, CON_CMD_ASSERT_LTE, &lteCarrierParams);

    TransportManagerTick(pTransport, 10);

    // Set duration delay
    durationDelayEndMs = startDelayEndMs + deferredCarrierAssertParams.durationInMsec;

    // Next state is start delay state
    pfnCommandCurrentState = app_command_state_durationDelay;

    // Next state after duration delay is to Turn Off LTE
    pfnCommandNextState = app_command_state_LTECarrierAssert_TurnOff;
}


/***************************************************************************//**
 * @brief
 *  Calls the modem library through the transport manager to turn off the
 *   specified carrier signal. It then powers off the modem and sets the
 *   command functions back to the idle functions.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_LTECarrierAssert_TurnOff(void)
{
    TRANSPORT_MANAGER* pTransport;
    ASSERT_LTE_CARRIER_PARAMS   lteCarrierParams;

    pTransport = (TRANSPORT_MANAGER*)app_cellular_get_transport();

    // Send another command with 0 parameters to stop the carrier
    memset(&lteCarrierParams, 0, sizeof(lteCarrierParams));
    TransportManagerCommand(pTransport, CON_CMD_ASSERT_LTE, &lteCarrierParams);

    TransportManagerTick(pTransport, 10);

    DEBUG_TRACE("Deassert LTE Carrier.\n\r");

    // properly shutdown the modem by sending the command to cause a shutdown
    app_command_powerDownModem();

    // Set modem shutdown delay delay
    modemShutdownDelayEndMs = timeElapsedMs + DELAY_MSEC_FOR_MODEM_POWER_OFF;

    // Next state is modem shutdown state
    pfnCommandCurrentState = app_command_state_modemShutdownDelay;
}

//*****************************************************************************
// BLE CARRIER STATES
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Prepare to run the BLE Carrier Assert command.
 *
 * @note
 *  This command calls routines in app_ble.c to start and stop the BLE signal.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_BLECarrierAssert_Enter(void)
{     
    DEBUG_TRACE ("BLE Carrier Assert command- delay: %ld duration: %ld cmd: %d freq: %d length: %d type: %d\n\r", 
                (uint32_t)deferredBleCarrierAssertParams.startDelayInMsec,
                (uint32_t)deferredBleCarrierAssertParams.durationInMsec, 
                deferredBleCarrierAssertParams.bleTestMode.rfCommand, 
                deferredBleCarrierAssertParams.bleTestMode.rfFrequency, 
                deferredBleCarrierAssertParams.bleTestMode.rfPacketLength, 
                deferredBleCarrierAssertParams.bleTestMode.rfPacketType);
    
    // Next state is to start up the BLE module, prior to the start delay
    pfnCommandCurrentState = app_command_state_BLECarrierAssert_startupModule;   
    
    // Check if duration is 0, which means stay in this mode forever
    if(deferredBleCarrierAssertParams.durationInMsec == 0)
    {
        testForever = true;
    }
    
    else
    {
        testForever = false;
    }
}

/***************************************************************************//**
 * @brief
 *  Turns on the BLE module in prep for the BLE Carrier Assert command.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_BLECarrierAssert_startupModule(void)
{   
    // stop the BLE process so we can start it back up in test mode
    app_ble_stop();

    // Next state is start delay state
    pfnCommandCurrentState = app_command_state_startDelay;   

    // Command start delay 
    startDelayEndMs = timeElapsedMs + 
                          deferredBleCarrierAssertParams.startDelayInMsec;      
      
    // State after start delay state is Turn On LTE state
    pfnCommandNextState = app_command_state_BLECarrierAssert_TurnOn;   
}


/***************************************************************************//**
 * @brief
 *  Starts the BLE module transmitting the indicating signal/packets.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_BLECarrierAssert_TurnOn(void)
{
    uint8_t     cmdMsb;
    uint8_t     cmdLsb;
 
    DEBUG_TRACE("Assert BLE Carrier.\n\r");

    // startup the Nordic BLE module; the true indicates test mode
    app_ble_startup(true);
    
    // build the 2 command bytes according to:
    // MSB 
    //      7     6     5     4     3     2     1     0
    //      | command   |         frequency           | 
    //
    // LSB 
    //      7     6     5     4     3     2     1     0
    //      |            length           |   type    | 
    cmdMsb = (((deferredBleCarrierAssertParams.bleTestMode.rfCommand & 0x3) << 6) |
              (deferredBleCarrierAssertParams.bleTestMode.rfFrequency & 0x3F));
    cmdLsb = (((deferredBleCarrierAssertParams.bleTestMode.rfPacketLength & 0x3F) << 2) |
              (deferredBleCarrierAssertParams.bleTestMode.rfPacketType & 0x3));
    
    if (app_ble_startTest (cmdMsb, cmdLsb))
    {
        // Set duration delay
        durationDelayEndMs = startDelayEndMs +
                         deferredBleCarrierAssertParams.durationInMsec;

        // Next state is start delay state
        pfnCommandCurrentState = app_command_state_durationDelay;

        // Next state after duration delay is to Turn Off LTE
        pfnCommandNextState = app_command_state_BLECarrierAssert_TurnOff;
    }
    else
    {
        // test failed so change to state to cleanup BLE
        pfnCommandCurrentState = app_command_state_BLECarrierAssert_TurnOff;
    }
}


/***************************************************************************//**
 * @brief
 *  Turns off the BLE module when complete with the BLE Carrier Assert command.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_BLECarrierAssert_TurnOff(void)
{
  
    DEBUG_TRACE("Deassert BLE Carrier.\n\r");

    // this turns off the BLE module and sets it inactive
	app_ble_stop();

    
    // startup the Nordic BLE module; the false indicates NOT test mode
    app_ble_startup(false);
    
    // Command done, back to idle state
    pfnCommandCurrentState = app_command_state_idle;    
    pfnCommandNextState = app_command_state_idle;
}



//*****************************************************************************
// SELECT POWER MODE STATES
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Prepare to run the select power mode command.
 *
 * @note
 *  This command powers on the indicated things for the indicated time.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_selectPowerMode_Enter(void)
{
    DEBUG_TRACE ("select power mode command- delay: %ld duration: %ld powerMode: %x\n\r",
        (uint32_t)deferredPowerModeParams.startDelayInMsec,
        (uint32_t)deferredPowerModeParams.durationInMsec,
        deferredPowerModeParams.powerMode);

    // Next state is start delay state
    pfnCommandCurrentState = app_command_state_startDelay;

    // Set start delay
    startDelayEndMs = timeElapsedMs + deferredPowerModeParams.startDelayInMsec;

    // State after start delay state is enable power mode
    pfnCommandNextState = app_command_state_selectPowerMode_TurnOn;

    // Check if duration is 0, which means stay in this mode forever
    if(deferredPowerModeParams.durationInMsec == 0)
    {
        testForever = true;
    }

    else
    {
        testForever = false;
    }
}


/***************************************************************************//**
 * @brief
 *  Powers on the indicated items. It then sets up the parameters to time
 *   the duration.
 *
 * @note
 *  The routine first saves the current power state of the components.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_selectPowerMode_TurnOn(void)
{
    DEBUG_TRACE("Power on indicated components.\n\r");
    app_power_setPowerMode(deferredPowerModeParams.powerMode);

    // Set duration delay
    durationDelayEndMs = startDelayEndMs + deferredPowerModeParams.durationInMsec;

    // Next state is start delay state
    pfnCommandCurrentState = app_command_state_durationDelay;

    // Next state after duration delay is to disable power mode
    pfnCommandNextState = app_command_state_selectPowerMode_TurnOff;
}

/***************************************************************************//**
 * @brief
 *  Powers of the indicated components if they were not previously powered on.
 *   It then sets the command functions to the idle functions.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_selectPowerMode_TurnOff(void)
{
    bool   modemNeedsShutdown;
    
    // Power things back off
    DEBUG_TRACE("Power off indicated components.\n\r");
    app_power_clearPowerMode(&modemNeedsShutdown);

    if (modemNeedsShutdown)
    {
        // properly shutdown the modem by sending the command to cause a shutdown
        app_command_powerDownModem();

        // Set modem shutdown delay delay
        modemShutdownDelayEndMs = timeElapsedMs + DELAY_MSEC_FOR_MODEM_POWER_OFF;

        // Next state is modem shutdown state
        pfnCommandCurrentState = app_command_state_modemShutdownDelay;
    }
    else
    {
        // Command done, back to idle state
        pfnCommandCurrentState = app_command_state_idle;
        pfnCommandNextState = app_command_state_idle;
    }
}


//*****************************************************************************
// SLEEP SECONDS STATES
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Prepare to run the sleep seconds command.
 *
 * @note
 *  This command causes the CMIU to sleep for the indicated time.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_sleepSeconds_Enter(void)
{

    DEBUG_TRACE ("sleep seconds command- delay: %ld duration: %ld\n\r",
                 (uint32_t)deferredSleepSecondsParams.startDelayInMsec,
                 (uint32_t)deferredSleepSecondsParams.durationInMsec);

    // Next state is start delay state
    pfnCommandCurrentState = app_command_state_startDelay;

    // Set start delay
    startDelayEndMs = timeElapsedMs + deferredSleepSecondsParams.startDelayInMsec;

    // State after start delay state is Sleep state
    pfnCommandNextState = app_command_state_sleepSeconds_Sleep;

    // Check if duration is 0, which means stay in this mode forever
    if(deferredSleepSecondsParams.durationInMsec == 0)
    {
        testForever = true;
    }

    else
    {
        testForever = false;
    }
}


/***************************************************************************//**
 * @brief
 *  Goes into sleep state (EM2 mode) for the specified time.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_sleepSeconds_Sleep(void)
{
    DEBUG_TRACE("Entering EM2\n\r");

    // Push all debug information now.
    app_cmit_interface_tick_ble_empty(APP_CMIT_MAX_BLE_CLEAR_ATTEMPTS);

    // Stop BLE here so it doesn't impact sleep current
    app_ble_stop();

    // Disable RTC interrupt so the device stays asleep the entire duration
    // It's acceptable that accurate time will be "lost"
    NVIC_DisableIRQ(RTC_IRQn);

    if (deferredSleepSecondsParams.durationInMsec == 0)
    {
        // disable the RTC trace messages
        app_rtc_debugTextOnRTCIRQ_disable();
        for(;;)
        {
            app_timers_leTimer_sleep_ms(120000);
            WDOG_Feed();
        }
    }
    else
    {
        // THIS STATE MUST BLOCK SO THAT DEVICE STAYS IN EM2
        // Sleep
        app_timers_leTimer_sleep_ms(deferredSleepSecondsParams.durationInMsec);
    }

    // Reenable RTC interrupt
    NVIC_EnableIRQ(RTC_IRQn);

    // Go to wake state
    pfnCommandCurrentState = app_command_state_sleepSeconds_Wake;

    DEBUG_TRACE("Exiting EM2\n\r");
}

/***************************************************************************//**
 * @brief
 *  Called when we come out of sleep state. This routine restarts BLE
 *  advertising and puts app_command back into idle state.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_sleepSeconds_Wake(void)
{
    // Start BLE advertising in swipe mode now that sleep is over
   app_ble_startup(false);

    
    // Command done, back to idle state
    pfnCommandCurrentState = app_command_state_idle;
    pfnCommandNextState = app_command_state_idle;
}

//*****************************************************************************
// READ CONNECTED DEVICES STATES
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Prepare to run the read connected devices command.
 *
 * @note
 *  From the documentation, the pulse is to be decoded thusly:
 *  ``0 = Use next appropriate, 0x0A = A, 0x0B = B, 0x0C = C, etc, else
 *  Autodetect.''
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_readConnectedDevice_Enter(void)
{
    DEBUG_TRACE (
            "read connected devices command- delay: \
             %ld pulse: %d delay between: %ld repeat: %ld\n\r",
            (uint32_t)deferredReadDevicesParams.startDelayInMsec,
            deferredReadDevicesParams.pulse,
            (uint32_t)deferredReadDevicesParams.delayBetweenReadsMsec,
            (uint32_t)deferredReadDevicesParams.numberOfRepeats);

    /** Initialize variables. */
    deferredReadDevicesParams.psARBReadingData = ARB_RegisterData_Retrieve();
    deferredReadDevicesParams.count = 0;

    /** Make sure we pass a valid pulse to the ARB module. */
    if (0x00 == deferredReadDevicesParams.pulse)
    {
        deferredReadDevicesParams.pulse = PLUS_INTERVAL_NEXT;
    }
    else if (0x0A == deferredReadDevicesParams.pulse)
    {
        deferredReadDevicesParams.pulse = PLUS_INTERVAL_A;
    }
    else if (0x0B == deferredReadDevicesParams.pulse)
    {
        deferredReadDevicesParams.pulse = PLUS_INTERVAL_B;
    }
    else if (0x0C == deferredReadDevicesParams.pulse)
    {
        deferredReadDevicesParams.pulse = PLUS_INTERVAL_C;
    }
    else if (0x0D == deferredReadDevicesParams.pulse)
    {
        deferredReadDevicesParams.pulse = PLUS_INTERVAL_D;
    }
    else if (0x0E == deferredReadDevicesParams.pulse)
    {
        deferredReadDevicesParams.pulse = PLUS_INTERVAL_E;
    }
    else if (0x0F == deferredReadDevicesParams.pulse)
    {
        deferredReadDevicesParams.pulse = PLUS_INTERVAL_F;
    }
    else
    {
        deferredReadDevicesParams.pulse = PLUS_INTERVAL_NEXT;
        ARB_ForceAutodetect();
    }

    // Next state is start delay state
    pfnCommandCurrentState = app_command_state_startDelay;

    // Set start delay
    startDelayEndMs = timeElapsedMs + deferredReadDevicesParams.startDelayInMsec;

    // Set durationDelay to startDelay for use by the reading state.
    // This makes it so the following states can just reference the
    // duration delay value.
    durationDelayEndMs = startDelayEndMs;

    // Determine whether to autodetect or read after the delay
    if (REGISTER_UNKNOWN == deferredReadDevicesParams.psARBReadingData->byRegisterType)
    {
        // After the start delay we need to try to autodetect
        pfnCommandNextState = app_command_state_readConnectedDevice_autodetect;
    }
    else
    {
        // Begin reading after the start delay
        pfnCommandNextState = app_command_state_readConnectedDevice_Read;
    }
}

/***************************************************************************//**
 * @brief
 *  Try to autodetect the connected device.
 *
 * @note
 *  From the documentation, the pulse is to be decoded thusly:
 *  ``0 = Use next appropriate, 0x0A = A, 0x0B = B, 0x0C = C, etc, else
 *  Autodetect.''
 *
 * @note
 *  This will try to autodetect the connected device. It will retry until
 *  a device is detected or the ARB engine decides it's a lost cause (NORETRY).
 *  When autodetection is complete (NORETRY or PASS) the next state is the
 *  read connected device state, which will still try to read the device, but
 *  will return errors is the read was no good.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_readConnectedDevice_autodetect(void)
{
    uint8_t ARB_result;
    APP_TX_MSG  responseMsg;
    E_ERROR_TAG   errorCode;

    // Initialise the response to a scratch buffer, and default to
    // sending to the port from whence it came.
    responseMsg.pData       = app_scratch_ram_get(&responseMsg.bufferSize);
    responseMsg.dataLength  = 0;
    responseMsg.destination = deferredReadDevicesParams.destination;

    // Next state is duration delay state
    pfnCommandCurrentState = app_command_state_durationDelay;

    // Set duration delay
    durationDelayEndMs = timeElapsedMs + deferredReadDevicesParams.delayBetweenReadsMsec;

    // Try to read the register
    ARB_result = ARB_ReadRegister();

    // Determine whether to autodetect or read after the delay
    if (REGISTER_READ_FAIL_NORETRY == ARB_result)
    // The autodetection has failed and cannot retry - go ahead and read
    {
        // Begin reading after the start delay
        pfnCommandNextState = app_command_state_readConnectedDevice_Read;

        // Send an error code because this command failed really hard
        errorCode = E_ERROR_INVALID_CMD;
    }
    else if (((REGISTER_READ_PASS == ARB_result) ||
        (REGISTER_READ_FAIL_RETRY == ARB_result)) &&
        (REGISTER_UNKNOWN == deferredReadDevicesParams.psARBReadingData->byRegisterType))
    // We've failed the autodetect, but we get to retry.
    {
        // After the delay we need to retry the autodetect
        pfnCommandNextState = app_command_state_readConnectedDevice_autodetect;

        // Set the error code to none, this is still normal operation.
        errorCode = E_ERROR_NONE;
    }
    else
    // We've successfully autodetected the device.
    {
        // Begin reading after the delay
        pfnCommandNextState = app_command_state_readConnectedDevice_Read;

        // Set the error code to none, this is still normal operation.
        errorCode = E_ERROR_NONE;
    }

    //Configure a response message (which includes tag27).
    responseMsg.dataLength =
            app_packet_builder_build_readConnectedDeviceResponse(
                        responseMsg.pData,
                        responseMsg.bufferSize,
                        errorCode);

    app_message_send_response((APP_TX_MSG*)&responseMsg);
}

/***************************************************************************//**
 * @brief
 *  Processes the read connected devices command.
 *
 * @note
 *  From the documentation, the pulse is to be decoded thusly:
 *  ``0 = Use next appropriate, 0x0A = A, 0x0B = B, 0x0C = C, etc, else
 *  Autodetect.''
 *
 * @note
 *  This should always cause a command response to be returned with tag27
 *  embedded in it. This state will execute numberOfRepeats+1 times, regardless
 *  to whether the connected device is successfully read.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_readConnectedDevice_Read(void)
{
    bool        result;
    APP_TX_MSG  responseMsg;
    E_ERROR_TAG   errorCode;

    // Initialise the response to a scratch buffer, and default to
    // sending to the port from whence it came.
    responseMsg.pData       = app_scratch_ram_get(&responseMsg.bufferSize);
    responseMsg.dataLength  = 0;
    responseMsg.destination = deferredReadDevicesParams.destination;

    if ((deferredReadDevicesParams.numberOfRepeats >= deferredReadDevicesParams.count))
    {
        if (PLUS_INTERVAL_NEXT != deferredReadDevicesParams.pulse)
        {
            result = ARB_ReadRegister_OnDemand(deferredReadDevicesParams.pulse,
                 deferredReadDevicesParams.psARBReadingData->byRegisterType);
        }
        else
        {
            /** If the command has asked us to use the next plus pulse, let's
             *  call ARB_ReadRegister() so that all of the normal ARB stuff is
             *  called. This let's a unit recover from an error that trigger
             *  an autodetect. */
            uint8_t ARB_result;
            ARB_result = ARB_ReadRegister();
            result = (REGISTER_READ_PASS == ARB_result) ? true : false;
        }

        DEBUG_TRACE ("Read result for read number %d: %d\n\r",
                    (deferredReadDevicesParams.count + 1),
                    result);

        ++deferredReadDevicesParams.count;
    }
    else
    {
        result = false;
    }

    if ((deferredReadDevicesParams.numberOfRepeats >= deferredReadDevicesParams.count))
    {
        // Set duration delay
        durationDelayEndMs = timeElapsedMs + deferredReadDevicesParams.delayBetweenReadsMsec;

        // Next state is duration delay state
        pfnCommandCurrentState = app_command_state_durationDelay;

        // Next state after duration delay is to read the connected device
        pfnCommandNextState = app_command_state_readConnectedDevice_Read;
    }
    else
    {
        // Next state after duration delay is to end the reads
        pfnCommandCurrentState = app_command_state_readConnectedDevice_End;
    }

    // Set the error code based on previous results
    errorCode = ((result) ? E_ERROR_NONE : E_ERROR_INVALID_CMD);

    //Configure a response message (which includes tag27).
    responseMsg.dataLength =
            app_packet_builder_build_readConnectedDeviceResponse(
                        responseMsg.pData,
                        responseMsg.bufferSize,
                        errorCode);

    app_message_send_response((APP_TX_MSG*)&responseMsg);
}

/***************************************************************************//**
 * @brief
 *  End the read connected devices command and return to the idle command
 *  states.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_readConnectedDevice_End(void)
{
    // Command done, back to idle state
    pfnCommandCurrentState = app_command_state_idle;
    pfnCommandNextState = app_command_state_idle;
}



//*****************************************************************************
// ERASE MEMORY STATES
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Prepare to run the erase memory command.
 *
 * @note
 *  This command erases the specified images from flash memory.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_eraseMemory_Enter(void)
{
    DEBUG_TRACE ("erase memory command- bitmask: %x\n\r",
        deferredEraseMemoryParams.imagesBitMap);

    // Next state is to power-up external flash
    pfnCommandCurrentState = app_command_state_eraseMemory_extFlashTurnOn;

}


/***************************************************************************//**
 * @brief
 *  Turns on the external flash and sets up the SPI communications to it.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_command_state_eraseMemory_extFlashTurnOn(void)
{
    // Initialize flash communications
    Flash_init();

    // Disable SPI portion of flash communications (so BTLE will still work).
    cmiuSpi_flash_disable();

    // Next state is eraseMemory
    pfnCommandCurrentState = app_command_state_eraseMemory_Erase;

    // After completing the erasure, disable external flash.
    pfnCommandNextState = app_command_state_eraseMemory_extFlashTurnOff;
}


/***************************************************************************//**
 * @brief
 *  Uses a loop to erase the indicated flash images.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_eraseMemory_Erase(void)
{

    ImageType_t image;
    bool imageFound;
    ImageInformation_t* tempImageLists;
    bool eraseStatus;
    APP_TX_MSG  responseMsg;
    E_ERROR_TAG   errorCode;

    // Initialise the response to a scratch buffer, and default to
    // sending to the port from whence it came.
    responseMsg.pData       = app_scratch_ram_get(&responseMsg.bufferSize);
    responseMsg.dataLength  = 0;
    responseMsg.destination = deferredEraseMemoryParams.destination;

    // Load image list addresses/info
    tempImageLists = (ImageInformation_t*)(IMAGE_LIST_ADDRESS);

    // Temporarily enable SPI portion of flash communications
    cmiuSpi_flash_enable();

    // 'Wake up' the flash chip.
    (void) Flash_WaitOnWip(FLASH_WAIT_ON_WIP_MSEC);

    // Check if anything to erase
    if((deferredEraseMemoryParams.imagesBitMap > 0) &&
       (deferredEraseMemoryParams.retries < 5u))
    {

        // !!! NOT CHECKING IF ANY INTERNAL FLASH IMAGES NEED TO BE ERASED
        // SO MAKE SURE THEY ARE CLEAR
        deferredEraseMemoryParams.imagesBitMap &= ~(0x000F);


        // Start at first backup image (BackupApplication) and go to last
        // image (NewBleConfiguration) looking for images that are being
        // commanded erased.
        image = BackupApplication;
        imageFound = false;


        while((imageFound == false) && (image <= NewBleConfiguration))
        {
            if(deferredEraseMemoryParams.imagesBitMap & (1 << image))
            {
                imageFound = true;
            }

            else
            {
                // Move to next image
                image++;
            }
        }

        if(imageFound)
        {
            uint32_t start_address = ((uint32_t)tempImageLists[image].Address);
            uint32_t end_address = ((uint32_t)tempImageLists[image].Address +  tempImageLists[image].Length + 4);

            // Erase found image, add four bytes to length since the length of
            // CRC must be added back.
            eraseStatus = Flash_EraseRange(start_address, end_address);

            // Validate the erasure
            if (eraseStatus)
            {
                uint32_t address = start_address;
                uint16_t length = 256;

                while (eraseStatus && (address < end_address))
                {
                    uint32_t i;

                    (void) Flash_ReadFromAddress (responseMsg.pData, address, length);

                    for ( i=0; i<length; ++i)
                    {
                        if (0xFFu != responseMsg.pData[i])
                        {
                            /* The erase failed if there is a mismatch. */
                            eraseStatus = false;
                            i = length;
                        }
                    }

                    address += length;
                }
            }

            if(eraseStatus)
            {
                // Clear image bit since sucessfully erased
                deferredEraseMemoryParams.imagesBitMap &= ~(1 << image);
                DEBUG_TRACE ("Passed erasing image: %d\n\r", image);
                errorCode = E_ERROR_NONE;
                deferredEraseMemoryParams.retries = 0;
            }

            else
            {
                DEBUG_TRACE ("Failed erasing image: %d\n\r", image);
                errorCode = E_ERROR_INVALID_CMD;
                ++deferredEraseMemoryParams.retries;
            }

            //Configure a response message.
            responseMsg.dataLength =
                    app_packet_builder_build_commandResponse(
                                responseMsg.pData,
                                responseMsg.bufferSize,
                                E_COMMAND_ERASE_MEMORY_IMAGE,
                                errorCode);

            app_message_send_response((APP_TX_MSG*)&responseMsg);

        }
    }

    else  // Image bitmap all zeros, nothing left to erase
    {
        // Go to the next state (configured by calling state)
        pfnCommandCurrentState = pfnCommandNextState;
    }

    // Disable SPI portino of flash communications (so BTLE will still work).
    cmiuSpi_flash_disable();

    // Stay in current state
}


/***************************************************************************//**
 * @brief
 *  Powers off the external flash.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_command_state_eraseMemory_extFlashTurnOff(void)
{
    // Temporarily enable SPI portion of flash communications
    // This is needed for Flash_Deinit to properly function.
    cmiuSpi_flash_enable();

    // Deitialize communications with the external flash
    Flash_Deinit();

    // Next state is the end state
    pfnCommandCurrentState = app_command_state_eraseMemory_End;

}


/***************************************************************************//**
 * @brief
 *  Called when the erase memory command is complete. This function sets the
 *   command functions back to the idle functions.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_eraseMemory_End(void)
{
    // Command done, back to idle state
    pfnCommandCurrentState = app_command_state_idle;
    pfnCommandNextState = app_command_state_idle;
}


//*****************************************************************************
// SET RECORDING and REPORTING INTERVAL STATES
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Prepare to run the set recording and reporting intervals command.
 *
 * @note
 *  This command updates a number of the CMIU recording and reporting times.
 *   It does this by updating the new configuration image and then resetting
 *   the CMIU.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_setIntervals_Enter(void)
{
    if(app_command_helper_DoWeNeedNewConfigImage(&deferredSetIntervalsParams))
    {
        DEBUG_TRACE ("set recording and reporting intervals command- start mins: %d interval hours: %d retries: %d transmit window: %d \n\r",
            deferredSetIntervalsParams.reportingStartMins, deferredSetIntervalsParams.reportingIntervalHours,
            deferredSetIntervalsParams.reportingRetries,   deferredSetIntervalsParams.reportingTransmitWindowsMins);

        DEBUG_TRACE ("       continued - quiet start mins: %d quiet end mins: %d start mins: %d interval mins: %d \n\r",
            deferredSetIntervalsParams.reportingQuietStartMins,   deferredSetIntervalsParams.reportingQuietEndMins,
            deferredSetIntervalsParams.recordingStartMins,        deferredSetIntervalsParams.recordingIntervalMins);

        /** We don't want to actually start ticking this until the
         * rest of cellular stuff is done. */
        if (E_CMIU_OPERATINGMODE_STATE_CELLULAR == app_operatingMode_state_get())
        {
            waitTillEnd = true;
        }

        // Next state is to power-up external flash
        pfnCommandCurrentState = app_command_state_setIntervals_extFlashTurnOn;

    }
    else
    {
        DEBUG_TRACE("The command values match the current config. Done.\n\r");
        pfnCommandCurrentState = app_command_state_idle;
    }
}


/***************************************************************************//**
 * @brief
 *  Turns on the external flash and sets up the SPI communications to it.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_command_state_setIntervals_extFlashTurnOn(void)
{
    // Initialize flash communications
    Flash_init();

    // Disable SPI portion of flash communications (so BTLE will still work).
    cmiuSpi_flash_disable();

    // Use the erase memory state to clear the new config image.
    pfnCommandCurrentState = app_command_state_eraseMemory_Erase;

    // After the Erasure is complete, start the program config.
    pfnCommandNextState = app_command_state_setIntervals_programConfig;
}


/***************************************************************************//**
 * @brief
 *  Updates the new config image in external flash. It does this by copying over
 *  the current image and updating the desired values. It then has to recalculate
 *  the CRC before writing it to flash.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_command_state_setIntervals_programConfig(void)
{
    bool result = false;
    // Temporarily enable SPI portion of flash communications
    cmiuSpi_flash_enable();

    result = app_command_helper_UpdateNewConfigImage (deferredSetIntervalsParams.reportingStartMins,
                                               deferredSetIntervalsParams.reportingIntervalHours * 3600,
                                               deferredSetIntervalsParams.reportingTransmitWindowsMins / 15,
                                               deferredSetIntervalsParams.recordingIntervalMins * 60);

    cmiuSpi_flash_disable();

    if (true == result)
    {
        // Next state is to update the datalog with the interval change
        pfnCommandNextState = app_command_state_setIntervals_updateDatalog;
    }
    else
    {
        // Skip all the next states and go straight to the cleanup routine
        pfnCommandNextState = app_command_state_setIntervals_handleError;
    }

    durationDelayEndMs = timeElapsedMs + c_2r_interstate_delay_ms;
    pfnCommandCurrentState = app_command_state_durationDelay;
}

/***************************************************************************//**
 * @brief
 *  Put a special record in the datalog to indicate that we've updated the
 *  recording interval.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_command_state_setIntervals_updateDatalog(void)
{
    uint32_t idx;
    bool result;

    // Temporarily enable SPI portion of flash communications
    cmiuSpi_flash_enable();

    // 'Wake up' the flash chip.
    idx = 0;
    do
    {
        // Make sure the chip is ready for a new command
        result = Flash_WaitOnWip(FLASH_WAIT_ON_WIP_MSEC);
        if (true == result)
        {
            // Update the datalog and store a record that indicates the
            // interval values were updated.
            result = app_datalog_re_init(RECORDING_INTERVAL_CHANGE_INDICATOR);
        }
        ++idx;
    }
    while ((false == result) && (3 > idx));

    cmiuSpi_flash_disable();

    /** Next state is to power off ext flash */
    pfnCommandNextState = app_command_state_setIntervals_disableFlash;
    durationDelayEndMs = timeElapsedMs + c_2r_interstate_delay_ms;
    pfnCommandCurrentState = app_command_state_durationDelay;
}

/***************************************************************************//**
 * @brief
 *  Disables access to the external flash. It does this by calling Flash_Deinit
 *   which can take up to 100msec.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_command_state_setIntervals_disableFlash(void)
{
    // Temporarily enable SPI portion of flash communications
    cmiuSpi_flash_enable();

    // Deitialize communications with the external flash
    Flash_Deinit();

    /** Now, synchronize the internal config with the new config. */
    pfnCommandNextState = app_command_state_setIntervals_syncInternalConfig;
    durationDelayEndMs = timeElapsedMs + c_2r_interstate_delay_ms;
    pfnCommandCurrentState = app_command_state_durationDelay;
}

/***************************************************************************//**
 * @brief
 *  Updates the internal configuration so that it matches the new configuration
 *  that we just pushed. This has to block to preven the scheduler from trying
 *  to access flash that is being worked on.
 *
 * @note
 *  This is blocking, and that's on purpose.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_setIntervals_syncInternalConfig(void)
{
    /** Corrupt internal application configuration. */
    ImageInformation_t newConfig;
    ImageInformation_t currentConfig;

    newConfig = ImageLists[NewConfiguration];
    currentConfig = ImageLists[CmiuConfiguration];

    /* Protect the flash write from interrupts */
    USART_IntDisable(USART1, UART_IEN_TXBL);

    /** This will be blocking, but it will call all the needed stuff. */
    cmiuImages_getExternal(&newConfig, &currentConfig);

    /* Enable interrupt on USART TX Buffer*/
    USART_IntEnable(USART1, UART_IEN_TXBL);

    /** Validate the updated config image. */
    pfnCommandNextState = app_command_state_setIntervals_validateConfig;
    durationDelayEndMs = timeElapsedMs + c_2r_interstate_delay_ms;
    pfnCommandCurrentState = app_command_state_durationDelay;
}

/***************************************************************************//**
 * @brief
 *  Validates the image to determine whether we need to reset the CMIU.
 *
 * @note
 *  We won't actually reset the CMIU IFF the application successfully pulled
 *  the new configuration into internal memory space.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_setIntervals_validateConfig(void)
{
    /** If the internal image is invalid, go to the bootloader. Otherwise,
     * continue forward. */
    if (false == app_command_helper_validate_internal_config())
    {
        /** Wait, then reset the CMIU. */
        durationDelayEndMs = timeElapsedMs + c_2r_interstate_delay_ms;
        pfnCommandCurrentState = app_command_state_durationDelay;
        pfnCommandNextState = app_command_state_setIntervals_handleError;
    }
    else
    {
        DEBUG_TRACE(" --->> Image update successful; reboot not needed.\n\r");

        /** Since the command was successful, store it in user data. */
        uint8_t * buffer = app_scratch_ram_get_256();
        uint32_t packetSize = app_packet_builder_build_userDataPacket(
                                                deferredSetIntervalsParams,
                                                buffer,
                                                256u);


        /** Push the set_interval command (as a whole) to the user data. */
        app_command_helper_push_to_user_data((uint32_t *)buffer, packetSize);

        /** Command done, back to idle state */
        pfnCommandCurrentState = app_command_state_idle;
        pfnCommandNextState = app_command_state_idle;

        /** We need to reschedule stuff since we just messed with the config. */
        app_scheduler_rescheduleAll();
    }

    waitTillEnd = false;
}

/***************************************************************************//**
 * @brief
 *   If there is a problem, cleanup here.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_setIntervals_handleError(void)
{
    DEBUG_TRACE(" --->> Image update failed. Reboot in 5s\n\r");

    /** First, make sure flash is disabled. */
    cmiuSpi_flash_enable();
    Flash_force_Deinit();

    /** Then, clean user data to prevent a reset loop. */
    uint8_t * buffer = app_scratch_ram_get_256();
    memset(buffer,0xFFu, USER_DATA_PACKET_SIZE);
    app_command_helper_push_to_user_data((uint32_t *)buffer,USER_DATA_PACKET_SIZE);

    /** Wait, then reset the CMIU. */
    durationDelayEndMs = timeElapsedMs + DELAY_MSEC_BEFORE_CMIU_RESET;
    pfnCommandCurrentState = app_command_state_durationDelay;
    pfnCommandNextState = app_command_state_setIntervals_resetCmiu;
}

/***************************************************************************//**
 * @brief
 *  Resets the CMIU. This should only happen if there was a problem and we
 *  need the bootloader to fix it.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_command_state_setIntervals_resetCmiu(void)
{
    /** Command done, back to idle state */
    pfnCommandCurrentState = app_command_state_idle;
    pfnCommandNextState = app_command_state_idle;
    waitTillEnd = false;

    if (E_CMIU_OPERATINGMODE_STATE_CELLULAR == app_operatingMode_state_get())
    {
        app_cellular_request_reset();
    }
    else
    {
        /** Let's try to empty everything in the debug log before we
         * reset. */
        app_cmit_interface_tick_ble_empty(APP_CMIT_MAX_BLE_CLEAR_ATTEMPTS);
        NVIC_SystemReset();
    }
}



/*****************************************************************************
 * @brief
 *   Post a command to be executed later, once the message handler
 *   has sent a response. This is for commands that block.
 *
 * @note
 *
 * @param[in] pParams The applicable params for the command
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_toggleLTE(const TOGGLE_LTE_PARAMS* pParams)
{
    if(app_command_inIdleState())
    {
        // Save command params
    deferredToggleLteParams = *pParams;

        // Enter Toggle LTE command state processing
        app_command_toggleLTE_Enter();

        return true;
    }

    else
    {
        // Another command executing
        return false;
    }
}

//*****************************************************************************
// REQUEST APN STATES
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Prepare to run the Request APN command.
 *
 * @note
 *  This command gets the current APN from the LTE modem.
 *  If the modem is not powered on it must do that first.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_requestApn_Enter(void)
{

    // the modem must be powered up if it is not on before responding to these AT commands.
    // A value of 0 indicates that the modem is not powered on and should be.
    if (modemPowerOnTimeMs == 0)
    {
        // Next state is to power up the modem, prior to the start delay
        pfnCommandCurrentState = app_command_state_requestApn_powerModem;
    }
    else
    {
        // modem is already powered on
        pfnCommandCurrentState = app_command_state_requestApn_gatherResult;
    }


}


/***************************************************************************//**
 * @brief
 *  Powers on the LTE modem as part of the request APN command. It then
 *   sets up the delay to allow the modem to initialize.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_requestApn_powerModem(void)
{
    // Turn on modem prior to the start delay since it takes minimum of 15
    // seconds to turn on and be ready for an AT command we want to get this started
    app_command_helper_powerOnModem();

    // Next state is wait for the modem state
    pfnCommandCurrentState = app_command_state_waitForModem;  
    modemReadyForCmds = false;

    // no minimum delay needed so set to the current time 
    startDelayEndMs = timeElapsedMs;

    // State after start delay state is to get the APN value
    pfnCommandNextState = app_command_state_requestApn_gatherResult;
}

/***************************************************************************//**
 * @brief
 *  Calls the modem library through the transport manager to get the current
 *   APN. 
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_requestApn_gatherResult(void)
{
    TRANSPORT_MANAGER* pTransport;
    APP_TX_MSG responseMsg;
    // use the same structure for all CON_CMD_GET_INFO commands
    GET_CAN_DATA_PARAMS   getCanDataParams;

    // Initialise the response to a scratch buffer, and default to
    // sending to the port from whence it came.
    responseMsg.pData       = app_scratch_ram_get(&responseMsg.bufferSize);
    responseMsg.dataLength  = 0;
    responseMsg.destination = deferredGetModemInfoParams.destination;

    pTransport = (TRANSPORT_MANAGER*)app_cellular_get_transport();

    // save time of the most recent request
    modemPowerOnTimeMs = timeElapsedMs;

    // power off the modem when done
    getCanDataParams.leaveModemOn = false;

    TransportManagerCommand(pTransport, CON_CMD_GET_APN, &getCanDataParams);

    // It takes 2 ticks to process this command. The first tick changes the
    //  the state from idle to the command state and the second tick actually
    //  processes the command.
    TransportManagerTick(pTransport, 10);
    TransportManagerTick(pTransport, 10);

    // Get the APN value from the modem and build it into a response
    responseMsg.dataLength = app_packet_builder_build_requestApnResponse(responseMsg.pData,
                responseMsg.bufferSize);

    if (responseMsg.dataLength > 0)
    {
        app_message_send_response((APP_TX_MSG*)&responseMsg);

    }

    // properly shutdown the modem by sending the command to cause a shutdown
    app_command_powerDownModem();

    // Set modem shutdown delay delay
    modemShutdownDelayEndMs = timeElapsedMs + DELAY_MSEC_FOR_MODEM_POWER_OFF;

    // Next state is modem shutdown state
    pfnCommandCurrentState = app_command_state_modemShutdownDelay;

}



//*****************************************************************************
// UPDATE APN STATES
//*****************************************************************************
/***************************************************************************//**
 * @brief
 *  Prepare to run the Update APN command.
 *
 * @note
 *  This command sets the current APN on the LTE modem.
 *  If the modem is not powered on it must do that first.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_updateApn_Enter(void)
{

    // the modem must be powered up if it is not on before responding to these AT commands.
    // A value of 0 indicates that the modem is not powered on and should be.
    if (modemPowerOnTimeMs == 0)
    {
        // Next state is to power up the modem, prior to the start delay
        pfnCommandCurrentState = app_command_state_updateApn_powerModem;
    }
    else
    {
        // modem is already powered on
        pfnCommandCurrentState = app_command_state_updateApn_update;
    }


}


/***************************************************************************//**
 * @brief
 *  Powers on the LTE modem as part of the update APN command. It then
 *   sets up the delay to allow the modem to initialize.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_updateApn_powerModem(void)
{
    // Turn on modem prior to the start delay since it takes minimum of 15
    // seconds to turn on and be ready for an AT command we want to get this started
    app_command_helper_powerOnModem();

    // Next state is waiting for the modem state
    pfnCommandCurrentState = app_command_state_waitForModem;  
    modemReadyForCmds = false;

    // no minimum delay needed so set to the current time 
    startDelayEndMs = timeElapsedMs;

    // State after start delay state is to set the APN value
    pfnCommandNextState = app_command_state_updateApn_update;
}

/***************************************************************************//**
 * @brief
 *  Calls the modem library through the transport manager to set the current
 *   APN. 
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_updateApn_update(void)
{
    TRANSPORT_MANAGER* pTransport;
    APP_TX_MSG responseMsg;
    UPDATE_APN_PARAMS   updateApnParams;
    
    updateApnParams.apnString = deferredUpdateApnParams.apnString;

    // Initialise the response to a scratch buffer, and default to
    // sending to the port from whence it came.
    responseMsg.pData       = app_scratch_ram_get(&responseMsg.bufferSize);
    responseMsg.dataLength  = 0;
    responseMsg.destination = deferredUpdateApnParams.destination;

    pTransport = (TRANSPORT_MANAGER*)app_cellular_get_transport();

    // save time of the most recent request
    modemPowerOnTimeMs = timeElapsedMs;


    TransportManagerCommand(pTransport, CON_CMD_SET_APN, &updateApnParams);

    // It takes 2 ticks to process this command. The first tick changes the
    //  the state from idle to the command state and the second tick actually
    //  processes the command.
    TransportManagerTick(pTransport, 10);
    TransportManagerTick(pTransport, 10);

    // Get the APN value from the modem and build it into a response
    responseMsg.dataLength = app_packet_builder_build_updateApnResponse(responseMsg.pData,
                responseMsg.bufferSize);

    if (responseMsg.dataLength > 0)
    {
        app_message_send_response((APP_TX_MSG*)&responseMsg);

    }

    // properly shutdown the modem by sending the command to cause a shutdown
    app_command_powerDownModem();

    // Set modem shutdown delay delay
    modemShutdownDelayEndMs = timeElapsedMs + DELAY_MSEC_FOR_MODEM_POWER_OFF;

    // Next state is modem shutdown state
    pfnCommandCurrentState = app_command_state_modemShutdownDelay;

}

/*****************************************************************************
**  RUN CONNECTIVITY TEST
*****************************************************************************/
/***************************************************************************//**
 * @brief
 *  Powers on the LTE modem as part of the run connectivity test. It then
 *   sets up the delay to allow the modem to initialize.
 *
 * @note
 *      Before jumping to the cell session:
 *      1. Make the detailed config packet due now.
 *      2. Update the timing intervals for task-due variables.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_runConnectivityTest_Enter(void)
{
    /** Make the detailed config packet due right now. */
    app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET,
                               E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET,
                               0u, NULL);
    /** Check the scheduler, this will ensure the task is listed as due. */
    app_scheduler_intervalUpdate();

    /** Prepare to go into a full-blown cell session. This will handle modem
     * power up and power down. */
    pfnCommandCurrentState = app_command_state_runConnectivityTest_cellState;
}

/***************************************************************************//**
 * @brief
 *  Operates a cell session as though we were in normal mode.
 *
 * @note
 *      1. The previous state made the detailed config task due.
 *      2. Get a cellular session started.
 *          a. The cellular state machine will handle power up.
 *          b. The cellular state machine will handle MQTT stuff.
 *          c. The cellular state machine will handle power down.
 *      3. Return to swipe mode when the cell session finished.
 *      4. Reset the packet instigator variable.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_command_state_runConnectivityTest_cellState(void)
{
    bool        result;
    APP_TX_MSG  responseMsg;
    E_ERROR_TAG   errorCode;

    /** Initialise the response to a scratch buffer, and default to
     * sending to the port from whence it came. */
    responseMsg.pData       = app_scratch_ram_get(&responseMsg.bufferSize);
    responseMsg.dataLength  = 0;
    responseMsg.destination = deferredRunConnectivityTestParams.destination;

    /** Then, get a cellular session started. */
    app_cmiu_app_check_cell_session();

    /** Make sure to set the current command state to idle so that we don't
     * have a circular situation.
     * TODO: Make this better in Cat-M. It's janky right now. - BWA */
    pfnCommandCurrentState = app_command_state_idle;

    /** Although the following is blocking, CMIT/CMIU Manager will still be able
     * to receive debug data. */
    while (E_CMIU_APP_STATE_CELLULAR == app_cmiu_app_state_get())
    {
        /** This will call into the cell session operating mode,
         * which will make sure to kick the dog and talk to CMIT.
         * Additionally, this will handle modem bring up and put down.
         */
        app_operatingMode_tick();
    }

    /** If the connection failed, the BLE comm buffer will be clogged up,
     * so let's clear it before we do anything else. */
    app_cmit_interface_tick_ble_empty(APP_CMIT_MAX_BLE_CLEAR_ATTEMPTS);

    /** Did we get a puback for the detailed config packet? */
    result = app_cellular_wasLastPackedPubacked(E_MQTT_PACKETS_DETAILED_CONFIG);

    /** I'm done, so re-enter swipe mode. */
    app_cmiu_app_switch_to_swipe_mode();
    /** Set the packet instigator back to its default value. */
    app_packet_builder_set_packet_instigator(E_PACKET_INSTIGATOR_NORMAL);

    /** Set the error code based on previous results */
    errorCode = ((result) ? E_ERROR_NONE : E_ERROR_INVALID_CMD);

    /** Configure and send response message (which includes tag??). */
    responseMsg.dataLength =
            app_packet_builder_build_commandResponse(
                        responseMsg.pData,
                        responseMsg.bufferSize,
                        E_COMMAND_RUN_CONNECTIVITY_TEST,
                        errorCode);

    app_message_send_response((APP_TX_MSG*)&responseMsg);

    /** Change to idle state. */
    pfnCommandCurrentState = app_command_state_idle;
}

/******************************************************************************
**  END RUN CONNECTIVITY TEST
******************************************************************************/


/*****************************************************************************
 * @brief
 *   Post a command to be executed later, once the message handler
 *   has sent a response. This is for command that block.
 *
 * @note
 *
 * @param[in] pParams The applicable params for the command
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_LTECarrierAsser(const LTE_CARRIER_ASSERT_PARAMS* pParams)
{
    if(app_command_inIdleState())
    {
        // Save command params
        deferredCarrierAssertParams = *pParams;

        // Enter LTE Carrier Assert command state processing
        app_command_LTECarrierAssert_Enter();

        return true;
    }

    else
    {
        // Another command executing
        return false;
    }
}


/*****************************************************************************
 * @brief
 *   Post a command to be executed later, once the message handler 
 *   has sent a response. This is for commands that block.
 *
 * @note
 *
 * @param[in] pParams The applicable params for the command
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_BLECarrierAssert(const BLE_CARRIER_ASSERT_PARAMS* pParams)
{
    if(app_command_inIdleState())
    {      
        // Save command params
        deferredBleCarrierAssertParams = *pParams;

        // Enter BLE Carrier Assert command state processing
        app_command_BLECarrierAssert_Enter();
        
        return true;
    }

    else
    {
        // Another command executing
        return false;
    }
}


/*****************************************************************************
 * @brief
 *   Post a command to be executed later, once the message handler
 *   has sent a response. This is for command that block.
 *
 * @note
 *
 * @param[in] pParams The applicable params for the command
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_selectPowerMode(const POWER_MODE_PARAMS* pParams)
{
    if(app_command_inIdleState())
    {
        // Save command params
    deferredPowerModeParams = *pParams;

        // Enter LTE Carrier Assert command state processing
        app_command_selectPowerMode_Enter();

        return true;
    }

    else
    {
        // Another command executing
        return false;
    }
}

/*****************************************************************************
 * @brief
 *   Post a command to be executed later, once the message handler
 *   has sent a response. This is for command that block.
 *
 * @note
 *
 * @param[in] pParams The applicable params for the command
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_sleepSeconds(const SLEEP_SECONDS_PARAMS* pParams)
{
    if(app_command_inIdleState())
    {
        // Save command params
        deferredSleepSecondsParams = *pParams;

        // Enter sleep seconds command state processing
        app_command_sleepSeconds_Enter();

        return true;
    }

    else
    {
        // Another command executing
        return false;
    }
}

/*****************************************************************************
 * @brief
 *   Post a command to be executed later, once the message handler
 *   has sent a response. This is for commands that block.
 *
 * @note
 *
 * @param[in] pParams The applicable params for the command
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_readConnectedDevices(const READ_DEVICES_PARAMS* pParams)
{
    bool retval;

    if(app_command_inIdleState())
    {
        // Save command params
        deferredReadDevicesParams = *pParams;

        // Enter Read Connected Device command state processing
        app_command_readConnectedDevice_Enter();

        // Return true
        retval = true;
    }

    else
    {
        // Another command executing
        retval = false;
    }

    return retval;
}


/*****************************************************************************
 * @brief
 *   Post a command to be executed later, once the message handler
 *   has sent a response. This is for command that block.
 *
 * @note
 *
 * @param[in] pParams The applicable params for the command
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_eraseMemory(const ERASE_MEMORY_PARAMS* pParams)
{
    if(app_command_inIdleState())
    {
        // Save command params
        deferredEraseMemoryParams = *pParams;

        // Enter Erase Memory command state processing
        app_command_eraseMemory_Enter();

        return true;
    }

    else
    {
        // Another command executing
        return false;
    }
}

/*****************************************************************************
 * @brief
 *   Post a command to be executed later, once the message handler
 *   has sent a response. This is for commands that block.
 *
 * @note
 *
 * @param[in] pParams The applicable params for the command
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_getSignalQuality(const GET_MODEM_INFO_PARAMS* pParams)
{
    if(app_command_inIdleState())
    {
        // Save command params
        deferredGetModemInfoParams = *pParams;

        // Enter Get Signal Quality command state processing
        app_command_getSignalQuality_Enter();

        return true;
    }

    else
    {
        // Another command executing
        return false;
    }
}


/*****************************************************************************
 * @brief
 *   Post a command to be executed later, once the message handler
 *   has sent a response. This is for commands that block.
 *
 * @note
 *
 * @param[in] pParams The applicable params for the command
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_getCANData(const GET_MODEM_INFO_PARAMS* pParams)
{
    if(app_command_inIdleState())
    {
        // Save command params
        deferredGetModemInfoParams = *pParams;

        // Enter Get Signal Quality command state processing
        app_command_getCANData_Enter();

        return true;
    }

    else
    {
        // Another command executing
        return false;
    }
}

/*****************************************************************************
 * @brief
 *   Post a modem update command to be executed later, once the message 
 *   handler has sent an immediate response. This is for commands that block.
 *
 * @note
 *
 * @param[in] pParams The applicable params for the command
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_modemFota(const COMMAND_UPDATE_MODEM_FIRMWARE_PARAMS* pParams)
{
    if(app_command_inIdleState())
    {
        // Save command params
        strcpy ((char *)deferredModemUpdateParams.ftpServer, (char *)pParams->ftpServer);
        deferredModemUpdateParams.ftpPort   = pParams->ftpPort;
        strcpy ((char *)deferredModemUpdateParams.username, (char *)pParams->username);
        strcpy ((char *)deferredModemUpdateParams.password, (char *)pParams->password);
        strcpy ((char *)deferredModemUpdateParams.fotaFilename, (char *)pParams->fotaFilename);
        strcpy ((char *)deferredModemUpdateParams.newFirmwareVersion, (char *)pParams->newFirmwareVersion);

        // if we are in a cellular state then schedule
        if (app_cmiu_app_state_get() == E_CMIU_APP_STATE_CELLULAR)
        {
            // Schedule image update task, only needs to be done once
            app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_MODEMFOTA, E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET, 20, NULL);
        }
        else
        {
         
            // Enter modem update command state processing
            app_command_state_modemUpdate_Enter();
        }

        return true;
    }

    else
    {
        // Another command executing
        return false;
    }
}

/*****************************************************************************
 * @brief
 *   Post a Request APN command to be executed later, once the message handler
 *   has sent a response. This is for commands that block.
 *
 * @note
 *
 * @param[in] pParams - command parameters which is just the destination 
 *                      for the response.
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_requestApn(const GET_MODEM_INFO_PARAMS* pParams)
{
    if(app_command_inIdleState())
    {
        // Save command params
        deferredGetModemInfoParams = *pParams;

        // Enter Request APN command state processing
        app_command_requestApn_Enter();

        return true;
    }

    else
    {
        // Another command executing
        return false;
    }
}


/*****************************************************************************
 * @brief
 *   Post an Update APN command to be executed later, once the message handler
 *   has sent a response. This is for commands that block.
 *
 * @note
 *
 * @param[in] pParams The applicable params for the command
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_updateApn(const COMMAND_UPDATE_APN_PARAMS* pParams)
{
    if(app_command_inIdleState())
    {
        // Save command params
        deferredUpdateApnParams = *pParams;

        // Enter Update APN command state processing
        app_command_updateApn_Enter();

        return true;
    }

    else
    {
        // Another command executing
        return false;
    }
}

/*****************************************************************************
 * @brief
 *   Post a Run Connectivity Test to be executed later, once the message
 *   handler has sent a response. This is for commands that block.
 *
 * @note
 *
 * @param[in] pParams The applicable params for the command
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_runConnectivityTest(const COMMAND_RUN_CONNECTIVITY_PARAMS* pParams)
{
    if(app_command_inIdleState())
    {
        /* Save command params. */
        deferredRunConnectivityTestParams = *pParams;

        /** Make sure the packet instigator is set. */
        app_packet_builder_set_packet_instigator(pParams->packetInstigator);

        /* Enter the Run Connectivity Test */
        app_command_runConnectivityTest_Enter();

        return true;
    }

    else
    {
        /* Another command executing */
        return false;
    }
}


/*****************************************************************************
 * @brief
 *   Post a command to be executed later, once the message handler
 *   has sent a response. This is for command that block.
 *
 * @note
 *
 * @param[in] pParams The applicable params for the command
 *
 * @return true if command initiated, false if not
 ******************************************************************************/
bool app_command_post_setIntervals(const S_RECORDING_REPORTING_INTERVAL* pParams,
                                   const APP_TX_MSG* pResponseMsg)
{
    if(app_command_inIdleState())
    {
        // Save command params
        deferredSetIntervalsParams = *pParams;
        // Prepare the erase range params (since we use it as part of this command)
        deferredEraseMemoryParams.imagesBitMap = (1 << NewConfiguration);
        deferredEraseMemoryParams.destination = pResponseMsg->destination;
        deferredEraseMemoryParams.retries = 0;

        // Enter Set Intervals command state processing
        app_command_setIntervals_Enter();

        return true;
    }

    else
    {
        // Another command executing
        return false;
    }
}

/*****************************************************************************
 * @brief
 *   Checks if in idle state
 *
 * @note
 *
 * @param[in] None
 *
 * @return bool returns true if in idle state (no active command) or NULL
 ******************************************************************************/
bool app_command_inIdleState(void)
{
    if((pfnCommandCurrentState == app_command_state_idle) ||
        pfnCommandCurrentState == NULL)
    {
        return true;
    }

    else
    {
        return false;
    }
}

/*****************************************************************************
 * @brief
 *   Returns whether we should wait till the end to tick a command.
 *
 * @note
 *
 * @param[in] None
 *
 * @return bool returns true if we should wait till the end of a session to tick
 *  the command state machine.
 ******************************************************************************/
bool app_command_wait_till_end(void)
{
    return waitTillEnd;
}


/*****************************************************************************
 * @brief
 *   Provides access to whether the CMIU has been commanded to remain in
 *   swipe mode indefinately.
 *
 * @note
 *
 * @param[out] none.
 *
 * @return bool - value of testForever
 ******************************************************************************/
bool app_command_testForever_retrieve(void)
{
    return testForever;
}


/*****************************************************************************
 * @brief
 *   Stores image update command information so that the image update processing
 *   occurs when scheduled.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
bool app_command_imageUpdate_handler(S_IMAGE_METADATA *imageMetadata,
                                     S_IMAGE_VERSION_INFO *imageVersionInfo,
                                     uint64_t applyAtTime,
                                     const char* imageUrl)
{

//     // Need to retrieve image version information to see if elibible for an upgrade
//     bool bl_ivu_ExternalVersion(const ImageInformation_t *ValidationImage)
// {
    static ImageType_t imageType;
    static S_MIU_IMG_INFO *imageVersion;
    uint32_t tempNewImageVersion;
    uint32_t tempExistImageVersion;
    uint32_t tempCrcStatus;
    uint32_t tempNewVersionStatus;
    uint32_t imageVersions[12];

    /* Fetch internal image versions for comparisons. */
    for (imageType = CmiuApplication; imageType <= CmiuBleConfiguration; ++imageType)
    {
        /** Get the version information from internal flash */
        imageVersion = ImageLists[imageType].MiuImageInfo;

        /** Combine the version information into a single variable */
        imageVersions[imageType] = ((imageVersion->versionMajor << 24) |
                                    (imageVersion->versionMinor << 16) |
                                    (imageVersion->versionBuild & 0x0000FFFF));
    }

    // Store information for later handling of a config image update
    if(imageMetadata->imageType == E_IMAGE_TYPE_CONFIG)
    {
        // Copy image meta data
        strcpy(&sConfigImageCommand.imgFileUrl[0], &imageUrl[0]);
        sConfigImageCommand.commandTime = applyAtTime;
        sConfigImageCommand.imgStartAddress = imageMetadata->startAddress;
        sConfigImageCommand.imgSize = imageMetadata->imageSize;
        sConfigImageCommand.extStartAddress = (uint32_t)ImageLists[NewConfiguration].Address;

        // Copy image version info
        sConfigImageCommand.imgVersion.versionMajorBcd = imageVersionInfo->versionMajorBcd;
        sConfigImageCommand.imgVersion.versionMinorBcd = imageVersionInfo->versionMinorBcd;
        sConfigImageCommand.imgVersion.versionYearMonthDayBcd = imageVersionInfo->versionYearMonthDayBcd;
        sConfigImageCommand.imgVersion.versionBuildBcd = imageVersionInfo->versionBuildBcd;

        // Check if new version is higer than existing
        // Convert Major, Minor, and build into single 32-bit
        tempNewImageVersion = ((sConfigImageCommand.imgVersion.versionMajorBcd << 24) |
                               (sConfigImageCommand.imgVersion.versionMinorBcd << 16) |
                               (sConfigImageCommand.imgVersion.versionBuildBcd & 0xFFFF));

        /* Grab version and CRC information for comparison */
        tempExistImageVersion = imageVersions[CmiuConfiguration];
        tempCrcStatus = app_rmu_SumCrcStatus_Get();
        tempNewVersionStatus = app_rmu_SumVersionStatus_Get();

        if((tempExistImageVersion < tempNewImageVersion) &&
           ((tempNewVersionStatus & NEW_CONFIGURATION) == 0))
        {
            // Set flag to true since new version is acceptable
            sConfigImageCommand.imgReqUpdateFlag = true;
        }
        else if (((tempCrcStatus & NEW_CONFIGURATION) == 0))
        {
            // Set flag to true since existing CRC is bad
            sConfigImageCommand.imgReqUpdateFlag = true;
        }
        else
        {
            // Image version not acceptable so don't perform an update
            sConfigImageCommand.imgReqUpdateFlag = false;
            DEBUG_TRACE("Image Unacceptable\r\n");
        }

        DEBUG_TRACE("Received Config Image\n\r");
        DEBUG_OUTPUT_TEXT_AND_DATA("Major Version: ", (uint8_t *)&sConfigImageCommand.imgVersion.versionMajorBcd,1);
        DEBUG_OUTPUT_TEXT_AND_DATA("Minor Version: ", (uint8_t *)&sConfigImageCommand.imgVersion.versionMinorBcd,1);
        DEBUG_OUTPUT_TEXT_AND_DATA("Date: ", (uint8_t *)&sConfigImageCommand.imgVersion.versionYearMonthDayBcd,4);
        DEBUG_OUTPUT_TEXT_AND_DATA("Build: ", (uint8_t *)&sConfigImageCommand.imgVersion.versionBuildBcd,4);
    }

    // Store information for later handling of a application image update
    if(imageMetadata->imageType == E_IMAGE_TYPE_FIRMWARE)
    {
        // Copy image meta data
        strcpy(&sAppImageCommand.imgFileUrl[0], &imageUrl[0]);
        sAppImageCommand.commandTime = applyAtTime;
        sAppImageCommand.imgStartAddress = imageMetadata->startAddress;
        sAppImageCommand.imgSize = imageMetadata->imageSize;
        sAppImageCommand.extStartAddress = (uint32_t)ImageLists[NewApplication].Address;

        // Copy image version info
        sAppImageCommand.imgVersion.versionMajorBcd = imageVersionInfo->versionMajorBcd;
        sAppImageCommand.imgVersion.versionMinorBcd = imageVersionInfo->versionMinorBcd;
        sAppImageCommand.imgVersion.versionYearMonthDayBcd = imageVersionInfo->versionYearMonthDayBcd;
        sAppImageCommand.imgVersion.versionBuildBcd = imageVersionInfo->versionBuildBcd;

        // Check if new version is higer than existing
        // Convert Major, Minor, and build into single 32-bit
        tempNewImageVersion = ((sAppImageCommand.imgVersion.versionMajorBcd << 24) |
                                (sAppImageCommand.imgVersion.versionMinorBcd << 16) |
                                (sAppImageCommand.imgVersion.versionBuildBcd & 0xFFFF));

        /* Grab version and CRC information for comparison */
        tempExistImageVersion = imageVersions[CmiuApplication];
        tempCrcStatus = app_rmu_SumCrcStatus_Get();
        tempNewVersionStatus = app_rmu_SumVersionStatus_Get();

        if((tempExistImageVersion < tempNewImageVersion) &&
           ((tempNewVersionStatus & NEW_APPLICATION) == 0))
        {
            // Set flag to true since new version is acceptable
            sAppImageCommand.imgReqUpdateFlag = true;
        }
        else if (((tempCrcStatus & NEW_APPLICATION) == 0))
        {
            // Set flag to true since existing CRC is bad
            sAppImageCommand.imgReqUpdateFlag = true;
        }
        else
        {
            // Image version not acceptable so don't perform an update
            sAppImageCommand.imgReqUpdateFlag = false;
            DEBUG_TRACE("Image Unacceptable\r\n");
        }


        DEBUG_TRACE("Received Application Image \n\r");
        DEBUG_OUTPUT_TEXT_AND_DATA("Major Version: ", (uint8_t *)&sAppImageCommand.imgVersion.versionMajorBcd,1);
        DEBUG_OUTPUT_TEXT_AND_DATA("Minor Version: ", (uint8_t *)&sAppImageCommand.imgVersion.versionMinorBcd,1);
        DEBUG_OUTPUT_TEXT_AND_DATA("Date: ", (uint8_t *)&sAppImageCommand.imgVersion.versionYearMonthDayBcd,4);
        DEBUG_OUTPUT_TEXT_AND_DATA("Build: ", (uint8_t *)&sAppImageCommand.imgVersion.versionBuildBcd,4);
    }


    // Store information for later handling of a encryption image update
    if(imageMetadata->imageType == E_IMAGE_TYPE_ENCRYPTION)
    {
        // Not currently supported in Mid-Alpha
        return false;
    }

    // Store information for later handling of a BLE image update
    if(imageMetadata->imageType == E_IMAGE_TYPE_BLE_CONFIG)
    {
        // Not currently supported in Mid-Alpha
        return false;
    }


    // Store information for later handling of a Telit  (
    if(imageMetadata->imageType == E_IMAGE_TYPE_TELIT_MODULE)
    {
        // Not sure what the CMIU is supposed to do to kick off Telit module firmware !!!!
        return false;
    }

    // Store information for later handling of ARB Config image update
    if(imageMetadata->imageType == E_IMAGE_TYPE_ARB_CONFIG)
    {
        // Not currently using a ARB Config image !!!!
        return false;
    }

    // Schedule
    return app_command_imageUpdate_schedule();
}



/***************************************************************************//**
 * @brief
 *   This function schedules the image update via app_scheduler
 *
 * @note
 *
 * @param[in] None
 *
 * @return True of image update was scheduled, false if no image updates scheduled
 ******************************************************************************/
bool app_command_imageUpdate_schedule(void)
{
    bool imageToUpdate = false;

    // Find the next image update to perform
    if(sConfigImageCommand.imgReqUpdateFlag == true)
    {
        // Check if new version >= old version
        // Clear imgReqUpdateFlag if not
        // Schedule update if is

        // Schedule image update task, only needs to be done once
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_IMAGEUPDATE, E_SCHEDULER_INTERVAL_TYPE_FORCE_TIME, NULL, sConfigImageCommand.commandTime);

        // At least one image to update
        imageToUpdate = true;
    }

    if(sAppImageCommand.imgReqUpdateFlag == true)
    {
        // Check if new version >= old version
        // Clear imgReqUpdateFlag if not
        // Schedule update if is

        // Schedule image update task, only needs to be done once
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_IMAGEUPDATE, E_SCHEDULER_INTERVAL_TYPE_FORCE_TIME, NULL, sAppImageCommand.commandTime);

        // At least one image to update
        imageToUpdate = true;
    }

    if(sEncryptImageCommand.imgReqUpdateFlag == true)
    {
        // Check if new version >= old version
        // Clear imgReqUpdateFlag if not
        // Schedule update if is

        // Schedule image update task, only needs to be done once
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_IMAGEUPDATE, E_SCHEDULER_INTERVAL_TYPE_FORCE_TIME, NULL, sEncryptImageCommand.commandTime);

        // At least one image to update
        imageToUpdate = true;
    }

    if(sBleImageCommand.imgReqUpdateFlag == true)
    {
        // Check if new version >= old version
        // Clear imgReqUpdateFlag if not
        // Schedule update if is

        // Schedule image update task, only needs to be done once
        app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE_IMAGEUPDATE, E_SCHEDULER_INTERVAL_TYPE_FORCE_TIME, NULL, sBleImageCommand.commandTime);

        // At least one image to update
        imageToUpdate = true;
    }

    if(sTelitImageCommand.imgReqUpdateFlag == true)
    {
        // Telit image not supported!!!
    }

    if(sArbImageCommand.imgReqUpdateFlag == true)
    {
        // Not currently using a ARB Config image !!!!
    }

    // Check if any images still need updating
    if(imageToUpdate == false)
    {
        // Take image update off the schedule
        app_scheduler_unscheduleTask(E_SCHEDULER_TASK_TYPE_IMAGEUPDATE);
        DEBUG_TRACE("No image updates need to be scheduled \n\r");
    }

    return imageToUpdate;
}


// Finds the next image due and returns the info on it
S_COMMAND_IMAGE_UPDATE* app_command_imageUpdate_infoRetrieve(void)
{
    if(sConfigImageCommand.imgReqUpdateFlag == true)
    {
        return &sConfigImageCommand;
    }

    if(sAppImageCommand.imgReqUpdateFlag == true)
    {
        return &sAppImageCommand;
    }

    if(sEncryptImageCommand.imgReqUpdateFlag == true)
    {
        return &sEncryptImageCommand;
    }

    if(sBleImageCommand.imgReqUpdateFlag == true)
    {
        return &sBleImageCommand;
    }

    if(sTelitImageCommand.imgReqUpdateFlag == true)
    {
        return &sTelitImageCommand;
    }

    if(sArbImageCommand.imgReqUpdateFlag == true)
    {
        return &sArbImageCommand;
    }

    return NULL;
}


/***************************************************************************//**
 * @brief
 *   Processes the publish requested packet command.
 *
 * @note
 *
 * @param[out] pResponseMsg The Packet, its length will be set to 0 on error
 * @param[in] packetType    type of packet
 * @param[in] destination   destination for packet
 *                          0 = MQTT, 1 = BLE & 2 = UART
 *
 * @return None. The pResponseMsg is expected to be populated
 ******************************************************************************/
bool app_command_publishRequestedPacket_handler(APP_TX_MSG*  pResponseMsg,
                                                uint8_t packetType,
                                                uint8_t destination)
{
    bool isOk = false;

    DEBUG_TRACE ("Command:Publish packet: type: %d dest: %d \n\r", packetType, destination);

    pResponseMsg->destination = (CMIU_INTERFACE)destination;
    switch (packetType)
    {
        case E_PACKET_TYPE_DETAILED_CONFIG:
            app_command_detailedConfig_handler(pResponseMsg);
            isOk = (pResponseMsg->dataLength > 0);
        break;

        case E_PACKET_TYPE_INTERVAL_DATA:
            app_command_intervaldata_handler(pResponseMsg);
            isOk = (pResponseMsg->dataLength > 0);
            break;

        case E_PACKET_TYPE_BASIC_CONFIG:
            app_command_basicConfig_handler(pResponseMsg);
            isOk = (pResponseMsg->dataLength > 0);
            break;

        default:
            pResponseMsg->dataLength = 0;
            break;
    }

    return isOk;
}


/***************************************************************************//**
 * @brief
 *   Shuts down the modem.
 *
 * @note
 *   It does this by requesting new information from the modem and indicating
 *    that the modem should be shut off when complete. It does not wait while
 *    the modem is shutting down.
 *
 * @param[in] none
 *
 * @return None
 ******************************************************************************/
static void app_command_powerDownModem(void)
{
    TRANSPORT_MANAGER* pTransport;
    GET_CAN_DATA_PARAMS   getCanDataParams;

    pTransport = (TRANSPORT_MANAGER*)app_cellular_get_transport();
    getCanDataParams.leaveModemOn = false;

    TransportManagerCommand(pTransport, CON_CMD_SHUTDOWN_MODEM, &getCanDataParams);

    // It takes 2 ticks to get the modem info and issue the shutdown command
    TransportManagerTick(pTransport, 10);
    TransportManagerTick(pTransport, 10);

    // clear the modem power on time since the modem is now off
    modemPowerOnTimeMs = 0;
}


/*****************************************************************************
 * @brief
 *   Processes the publish detailed config packet command.
 *
 * @note
 *
 * @param[out] pResponseMsg. The DCP to be sent as a response.
 *
 * @return None. On error pResponseMsg->dataLength will be 0
 ******************************************************************************/
static void app_command_detailedConfig_handler(APP_TX_MSG* pResponseMsg)
{
    EFM_ASSERT(NULL != pResponseMsg);

    // Build detailed config packet
    pResponseMsg->dataLength = app_packet_builder_build_detailedConfiguration(pResponseMsg->pData,
                pResponseMsg->bufferSize);
}


/*****************************************************************************
 * @brief
 *   Processes the publish basic config packet command.
 *
 * @note
 *
 * @param[out] pResponseMsg. The DCP to be sent as a response.
 *
 * @return None. On error pResponseMsg->dataLength will be 0
 ******************************************************************************/
static void app_command_basicConfig_handler(APP_TX_MSG* pResponseMsg)
{
    EFM_ASSERT(NULL != pResponseMsg);

    // Build detailed config packet
    pResponseMsg->dataLength = app_packet_builder_build_basicConfiguration(pResponseMsg->pData,
                pResponseMsg->bufferSize);
}



/*****************************************************************************
 * @brief
 *   Processes the publish interval data packet command.
 *
 * @note
 *
 * @param[out] pResponseMsg. The Interval data pkt to be sent as a response.
 *
 * @return None. On error pResponseMsg->dataLength will be 0
 ******************************************************************************/
static void app_command_intervaldata_handler(APP_TX_MSG* pResponseMsg)
{
    EFM_ASSERT(NULL != pResponseMsg);

    // Build detailed config packet
    pResponseMsg->dataLength = app_packet_builder_build_intervalData(pResponseMsg->pData,
                pResponseMsg->bufferSize);
}

/*****************************************************************************
 * @brief
 *   Provides access to the deferred modem update parameters.
 *
 * @note
 *   This is called by app_cellular when it needs these values.
 *
 * @param[out] none.
 *
 * @return pointer to the deferred modem update parameters.
 ******************************************************************************/
COMMAND_UPDATE_MODEM_FIRMWARE_PARAMS  *getDeferredModemUpdateParams (void)
{
    return &deferredModemUpdateParams;
}
