/******************************************************************************
*******************************************************************************
**
**         Filename: app_uart.c
**    
**           Author: Barry Huggins
**          Created: 12/23/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_uart.c
*
* @brief This file contains some UART utility routines for the CMIU project.
* 
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "stdtypes.h"
#include "em_device.h"
#include "em_usart.h"
#include "em_assert.h"
#include "em_int.h"
#include "efm32lg_uart.h"
#include "autogen_usart1.h"
#include "app_uart.h"
#include "app_timers.h"
#include "app_ble.h"
#include "app_cmit_interface.h"


/* Declare a circular buffer structure to use for Rx and Tx queues */
#define UART_BUFFERSIZE          640

#define RING_BUFFER_SIZE_UART_RX 64  // not currently used
#define RING_BUFFER_SIZE_UART_TX 640

// Ringbuffers
uint8_t rxUartData[RING_BUFFER_SIZE_UART_RX];
uint8_t txUartData[RING_BUFFER_SIZE_UART_TX];
RingBufferT rxUartRingBuffer;   // Buffer for data FROM debug UART
RingBufferT txUartRingBuffer;   // Buffer for data TO debug UART

#ifndef CMIT_SERIAL_PLAIN_ASCII
RingBufferT *rxSerialRingBuffer;   // pointer to buffer for incoming data
#endif // CMIT_SERIAL_PLAIN_ASCII

uint16_t    uartOverflowCnt;
uint16_t    uart1CharsRcv;

/***************************************************************************//**
 * @brief
 *   This function is used to output debug messages on UART1. This is called from
 *    the Nordic ACI code. Therefore, it must call Uart_Uart1WriteString as if it calls
 *    higher level routines it would generate additional debug messages.
 *
 * @note
 *       
 *
 * @param[in] printString - points to a string to be output
 *
 * @return none
 ******************************************************************************/
void debug_printf (char *printString)
{
#ifdef SWO_DEBUG
    printf (printString);
#else
    // avoid using CMIT interface as that is being redirected to BLE
    Uart_Uart1WriteString(printString);

#endif // SWO_DEBUG    
}

/***************************************************************************//**
 * @brief
 *   UART1 init function.
 *
 * @note
 *   This function inits the variables  for UART1.
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void Uart_Uart1Init(void)
{

    RingBufferInit(&rxUartRingBuffer, rxUartData, RING_BUFFER_SIZE_UART_RX);
    RingBufferInit(&txUartRingBuffer, txUartData, RING_BUFFER_SIZE_UART_TX);

    /* Make sure the interrup lock count starts at 0,
     * otherwise interrupts won't be enabled. */
    INT_LockCnt = 0;

    uartOverflowCnt = 0;
    uart1CharsRcv = 0;
}

/***************************************************************************//**
 * @brief
 *   UART1 setup function.
 *
 * @note
 *   This function sets up the interrupts for UART1.
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void Uart_Uart1Setup(void)
{
    Uart_Uart1Init();

    /* Prepare UART Rx and Tx interrupts */
    USART_IntClear(USART1, _UART_IFC_MASK);
    USART_IntEnable(USART1, UART_IEN_RXDATAV);
    NVIC_ClearPendingIRQ(USART1_RX_IRQn); 
    NVIC_ClearPendingIRQ(USART1_TX_IRQn); 
    NVIC_EnableIRQ(USART1_RX_IRQn); 
    NVIC_EnableIRQ(USART1_TX_IRQn); 

    /* Enable UART */
    USART_Enable(USART1, usartEnable);
}


/***************************************************************************//**
 * @brief
 *   Outputs the specified chars to UART 1.
 *
 * @note
 *   This function outputs the specified chars to UART1. If there is no space 
 *    for these chars then it will wait until space is available.
 *       
 *
 * @param[in] dataPtr
 *   characters to be output on UART 1
 * @param[in] dataLen
 *   number of chars to be output on UART 1
 *
 * @return none
 ******************************************************************************/
void Uart_Uart1PutData(const char *dataPtr, uint16_t dataLen)
{
    
    if (dataLen > 0)
    {
        RingBufferWrite(&txUartRingBuffer, (uint8_t *)dataPtr, dataLen);
    }
    /* Enable interrupt on USART TX Buffer*/
    USART_IntEnable(USART1, UART_IEN_TXBL);    

}

/***************************************************************************//**
 * @brief
 *   Outputs the specified string to UART 1.
 *
 * @note
 *   This function outputs the specified string to UART1. If there is no space 
 *    for these chars then it will wait until space is available.
 *       
 *
 * @param[in] dataPtr
 *   string to be output on UART 1
 *
 * @return none
 ******************************************************************************/
void Uart_Uart1WriteString (const char *dataPtr)
{

    // Write text string out UART
    Uart_Uart1PutData(dataPtr, strlen(dataPtr));    

}


/***************************************************************************//**
 * @brief
 *   UART1 RX IRQ Handler.
 *
 * @note
 *   This function is the input handler for UART1. 
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
//extern RingBufferT rxCmitRingBuffer;   // Buffer for data FROM CMIT link
void USART1_RX_IRQHandler(void)
{
    /* Check for RX data valid interrupt */
    if (USART1->IF & UART_IF_RXDATAV)
    {
        /* Copy data into RX Buffer */
        uint8_t rxData = USART_Rx(USART1);
        // bhtemp For now just skip the rxUartRingBuffer
        //RingBufferWriteOne(&rxUartRingBuffer, rxData);
        RingBufferWriteOne(rxSerialRingBuffer, rxData);
        // todo: need to move data to rxCmitRingBuffer
        uart1CharsRcv++;
    }
    
    /* Check for RX data valid interrupt */
    while (USART1->IF & UART_IF_RXDATAV)
    {
        /* Copy data into RX Buffer */
        uint8_t rxData = USART_Rx(USART1);
        RingBufferWriteOne(rxSerialRingBuffer, rxData);
        uart1CharsRcv++;
    }
    
    if (USART1->IF & UART_IF_RXOF)
    {
        USART_IntClear(USART1, USART_IF_RXOF);
        uartOverflowCnt++;
    }

}

/***************************************************************************//**
 * @brief
 *   UART1 TX IRQ Handler.
 *
 * @note
 *   This function is the output handler for UART1. 
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void USART1_TX_IRQHandler(void)
{
    /* Check TX buffer level status */
    if (USART1->IF & UART_IF_TXBL)
    {
        uint8_t txChar;
        if (RingBufferUsed(&txUartRingBuffer))
        {
            RingBufferRead(&txUartRingBuffer,  &txChar, 1);
            USART_Tx(USART1, txChar);
        }

        /* Disable Tx interrupt if no more bytes in queue */
        if (RingBufferEmpty(&txUartRingBuffer))
        {
            USART_IntDisable(USART1, UART_IEN_TXBL);
        }
    }
        
}


/***************************************************************************//**
 * @brief
 *   Returns status of UART1 Tx Completion
 *
 * @note
 *       
 *
 * @param[in] none    
 *
 * @return bool 
 *    true if UART Tx is complete (0 pending bytes and TXC bit indicates
 *    Tx buffer and shift register contains no more data).
 ******************************************************************************/
bool Uart_Uart1_TxComplete(void)
{
    if((RingBufferUsed(&txUartRingBuffer))  ||
        (!(USART1->STATUS & UART_STATUS_TXC)))
    {
        // Pending bytes remain or Tx is not complete
        return false;
    }
    
    else      
    {
        // No pending bytes and Tx is complete
        return true;
    }
}

/***************************************************************************//**
 * @brief
 *   Registers a ring buffer to receive incoming data
 *       
 *
 * @param[in] rxRingBuffer
 *   pointer to the receive ring buffer
 *
 * @return none
 ******************************************************************************/
void Uart_Uart1_RegisterRcvBuffer (RingBufferT *rxRingBuffer)
{
    rxSerialRingBuffer = rxRingBuffer;
}


