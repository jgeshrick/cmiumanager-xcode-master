/******************************************************************************
*******************************************************************************
**
**         Filename: bl_ivu.h
**
**           Author: Brian Arnberg
**          Created: 2015.06.11
**
**     Last Edit By:
**        Last Edit:
**
**         Comments:
**
** Revision History:
**       2015.06.11: First created (Brian Arnberg)
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential
**    property of Neptune Technology Group. The user, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __BL_IVU_H
#define __BL_IVU_H

#include "typedefs.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// Typedefs.h

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
/** How many total images are there? */
#define NUMBER_OF_IMAGES 12
/** The size of the block that we use for validation. */
#define IVU_BLOCK_SIZE 1024


/** Current Application firmware image */
#define CMIU_APPLICATION            BIT0
/** Current Read-only configuration parameter image */
#define CMIU_CONFIGURATION          BIT1
/** Current Encryption key image */
#define CMIU_ENCRYPTION             BIT2
/** Current Bluetooth operational configuration image */
#define CMIU_BLE_CONFIGURATION      BIT3
/** Backup Application firmware image */
#define BACKUP_APPLICATION          BIT4
/** Backup Read-only configuration parameter image */
#define BACKUP_CONFIGURATION        BIT5
/** Backup Encryption key image */
#define BACKUP_ENCRYPTION           BIT6
/** Backup Bluetooth operational configuration image */
#define BACKUP_BLE_CONFIGURATION    BIT7
/** New Application firmware image */
#define NEW_APPLICATION             BIT8
/** New Read-only configuration parameter image */
#define NEW_CONFIGURATION           BIT9
/** New Encryption key image */
#define NEW_ENCRYPTION              BITA
/** New Bluetooth operational configuration image */
#define NEW_BLE_CONFIGURATION       BITB

/** First internal application */
#define START_OF_CURRENT            0
/** First backup application */
#define START_OF_BACKUP             (START_OF_CURRENT+4)
/** First new application */
#define START_OF_NEW                (START_OF_BACKUP+4)

/** Which images, when valid, indicate the need to update? */
#define NEW_IMAGE_MASK  (NEW_APPLICATION | NEW_CONFIGURATION | NEW_ENCRYPTION | NEW_BLE_CONFIGURATION)
/** What are the minimum images needed to to safely launch the application? */
#define SAFE_TO_LAUNCH  (CMIU_APPLICATION | CMIU_CONFIGURATION | CMIU_BLE_CONFIGURATION)
/** Which images are backup images? */
#define BACKUP_MASK     (BACKUP_APPLICATION | BACKUP_CONFIGURATION | BACKUP_ENCRYPTION | BACKUP_BLE_CONFIGURATION)
/** Of the backup images, which ones correspond to the "SAFE_TO_LAUNCH" images? */
#define NEEDED_BACKUP_MASK     (BACKUP_APPLICATION | BACKUP_CONFIGURATION | BACKUP_BLE_CONFIGURATION)

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
void    bl_ivu_init(void);
bool    bl_ivu_InternalCrc(const ImageInformation_t *ValidationImage);
bool    bl_ivu_ExternalCrc(const ImageInformation_t *ValidationImage);
bool    bl_ivu_InternalVersion(const ImageInformation_t *ValidationImage);
bool    bl_ivu_ExternalVersion(const ImageInformation_t *ValidationImage);

uint32_t bl_ivu_GetSumCrcStatus(void);
uint32_t bl_ivu_GetSumVersionStatus(void);
uint32_t bl_ivu_GetSumImageStatus(void);

IMAGE_VALIDATION_INFORMATION bl_ivu_CheckImages(const ImageInformation_t *ValidationImage,
                                                uint8_t StartingImage,
                                                uint8_t LastImage);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __BL_IVU_H

