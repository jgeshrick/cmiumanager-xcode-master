/******************************************************************************
*******************************************************************************
**
**         Filename: app_power.c
**    
**           Author: Barry Huggins
**          Created: 12/23/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_power.c
*
* @brief This file contains the routines to manage the power for the 
*        CMIU module. 
* 
************************************************************************/

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "em_device.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "app_power.h"
#include "modem_power.h"
#include "app_timers.h"
#include "app_gpio.h"
#include "app_uart.h"
#include "app_cmit_interface.h"
#include "TagTypes.h"
#include "app_ble.h"
#include "cmiuFlash.h"
#include "app_command_helper.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/

// Saves the components which need to be powered down at the end of the select 
//   power mode duration to restore to the previous state.
uint8_t     powerDownMask; 

/***************************************************************************//**
 * @brief
 *   Function to enable 5V power supply (REG710NA-5)
 *
 * @note
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void app_power_5v_enable(void)
{
    // Enable 5V power supply
    GPIO_PinOutSet(gpioPortA, GPIO_PIN_0);
}

/***************************************************************************//**
 * @brief
 *   Function to disable 5V power supply (REG710NA-5)
 *
 * @note
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void app_power_5v_disable(void)
{
    // Disable 5V power supply
    GPIO_PinOutClear(gpioPortA, GPIO_PIN_0);
}

/***************************************************************************//**
 * @brief
 *   determines if the 5v register interface is currently powered
 *
 * @note
 *   The power is controlled by PA0 which is active high.
 *       
 *
 * @param[in] none
 *
 * @return BOOL - returns true if powered
 ******************************************************************************/
bool app_power_5v_isPowered(void)
{
    bool reg5v_Powered = false;
    
    // this signal is active low so if set then not powered.
    if (GPIO_PinOutGet(gpioPortA, 0))
    {
        reg5v_Powered = true;
    }

    
    return reg5v_Powered;
}


/***************************************************************************//**
 * @brief
 *   Sets the power configuration according to the specified bitmap.
 *
 *
 * @param[in] none
 *
 * @return bool indicating indicating success or failure
 ******************************************************************************/
bool app_power_setPowerMode(uint8_t powerMask)
{    
    powerDownMask = 0;
 
    // power on the modem if indicated
    if (powerMask & E_POWER_MODE_MODEM)
    {
        // first save the current power state so it can be restored
        if (!modem_power_modemIsPowered())
        {
            powerDownMask |= E_POWER_MODE_MODEM;
            // use the helper routine as it also sets up the GPIOs
            app_command_helper_powerOnModem();
        }
    }
    
    // power on the Nordic BTLE if indicated
    if (powerMask & E_POWER_MODE_BTLE)
    {
        if (!app_ble_isPowered())
        {
            powerDownMask |= E_POWER_MODE_BTLE;
            app_ble_power_on();
        }
    }
    
    // power on the flash if indicated
    if (powerMask & E_POWER_MODE_FLASH)
    {
        if (!Flash_isPowered())
        {
            powerDownMask |= E_POWER_MODE_FLASH;
            Flash_PowerOn();
        }

    }
    
    // power on the register interface if indicated
    if (powerMask & E_POWER_MODE_REGISTER)
    {
        if (!app_power_5v_isPowered())
        {
            powerDownMask |= E_POWER_MODE_REGISTER;
            app_power_5v_enable();
        }
    }
    
    return true;
}


/***************************************************************************//**
 * @brief
 *   Clears the power configuration. In other words we are going to power off
 *    all of the controllable subsystems.
 *
 *
 * @param[in] shutdownModem - parm indicating if the caller needs to shut
 *                            down the modem.
 *
 * @return bool indicating success or failure
 ******************************************************************************/
bool app_power_clearPowerMode(bool  *shutdownModem)
{
    // power down things that were not powered when we started 
    if (powerDownMask & E_POWER_MODE_MODEM)
    {
        // we need to indicate to the caller to do a proper modem shutdown
        *shutdownModem = true;
    }
    else
    {
        *shutdownModem = false;
    }
    
    if (powerDownMask & E_POWER_MODE_BTLE)
    {
        app_ble_power_off();
    }
    
    if (powerDownMask & E_POWER_MODE_FLASH)
    {
        Flash_PowerOff();
    }
    
    if (powerDownMask & E_POWER_MODE_REGISTER)
    {
        app_power_5v_disable();
    }
    
    return true;
}

