/******************************************************************************
*******************************************************************************
**
**         Filename: app_scratch_ram.h
**    
**           Author: Duncan Willis
**          Created: 9 Nov 2015
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_ram_buffer.h
*
* @brief Scratch RAM buffer
* 
************************************************************************/


/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "typedefs.h"
#include "Memorymap.h" 

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
/* size of buffer used to build packets, in bytes */
#define PACKET_BUFFER_SIZE (1024u)
#define TOTAL_SIZE  (2048u)
#define BIG_BUFFER_SIZE (PACKET_BUFFER_SIZE)
#define BUFFER_SIZE (256u)
#define BIG_BUFFER_COUNT (TOTAL_SIZE/BIG_BUFFER_SIZE)
#define TOTAL_BUFFER_COUNT (TOTAL_SIZE/BUFFER_SIZE)
#define BUFFERS_IN_BIG_BUFFERS (BIG_BUFFER_SIZE/BUFFER_SIZE)

/*=========================================================================*/
/*  T Y P E D E F S                                                        */
/*=========================================================================*/
typedef struct buffer_tag
{
    uint8_t ab[BUFFER_SIZE];
}
buffer_t;

typedef struct big_buffer_tag
{
    uint8_t  ab[BIG_BUFFER_SIZE];
}
big_buffer_t;

typedef union shared_ram_tag
{
    big_buffer_t buf1kB[BIG_BUFFER_COUNT];
    buffer_t     buf256B[TOTAL_BUFFER_COUNT];
}
shared_ram_t;

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern uint8_t* app_scratch_ram_get(uint32_t* pSize);
extern uint8_t* app_scratch_ram_get_256(void);

