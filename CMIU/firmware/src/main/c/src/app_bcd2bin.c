/******************************************************************************
*******************************************************************************
**
**         Filename: app_bcd2bin.c
**    
**           Author: Troy Harstad
**          Created: 1/20/2015
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
** Revision History:
**		  1/20/2015: First created
**
**
**      Code Review:
**
**    Neptune Technology Group
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property
**    of Neptune Technology Group. The use, copying, transfer
**    or disclosure of such information is prohibited except by express
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_bcd2bin.c
*
* @brief BCD to binary conversion functions
* 
************************************************************************/


/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include "app_bcd2bin.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
 

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/

/***************************************************************************//**
 * @brief
 *   X digits of BCD converted to 32-bit Binary 
 *
 * @note
 *   start_digit is 19 to 7
 *   Works with digits value of 8
 *
 * @param[in] BCD
 *   Pointer to 20 digit BCD array at most significient digit index
 *
 * @param[in] start_digit
 *   Digit (of 20) to start conversion
 *
 * @param[in] binary
 *   Pointer to converter 32-bit value
 *
 * @param[in] digits
 *   The number of digits to convert
 *
 * @return None
 ******************************************************************************/
void app_bcd2bin(uint8_t *BCD, uint8_t start_digit, uint32_t *binary, uint8_t digits)
{
	uint32_t bin;
	uint8_t i;
	bin = *(BCD+start_digit)*10 + *(BCD+(start_digit-1));
	for(i=2; i<digits; i++)
	{
		bin = bin*10 + *(BCD+(start_digit-i));
	}  
	
	*binary = bin;
}


/***************************************************************************//**
 * @brief
 *   X digits of BCD converted to 32-bit Binary 
 *
 * @note
 *   start_digit is 19 to 7
 *   Works with digits value of 8
 *
 * @param[in] BCD
 *   Pointer to 20 digit BCD array at most significient digit index
 *
 * @param[in] binary
 *   Pointer to converter 32-bit value
 *
 * @param[in] digits
 *   The number of digits to convert
 *
 * @return None
 ******************************************************************************/
void app_BCDtoBIN(uint8_t *BCD, uint32_t *binary, uint8_t digits)
{
	uint32_t bin;
	uint8_t i;
	bin = *(BCD)*10 + *(BCD+1);
	for(i=2; i<digits; i++)
	{
		bin = bin*10 + *(BCD+i);
	}  
	
	*binary = bin;
}


