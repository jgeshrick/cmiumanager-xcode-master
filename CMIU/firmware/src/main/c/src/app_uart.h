/******************************************************************************
*******************************************************************************
**
**         Filename: Uart.h
**    
**           Author: Barry Huggins
**          Created: 12/31/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __UART_H
#define __UART_H

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
#include <stdbool.h>
#include "RingBuffer.h"

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
void debug_printf (char *printString);
void Uart_Uart1PutData(const char * dataPtr, uint16_t dataLen);
void Uart_Uart1WriteString (const char *dataPtr);
uint32_t Uart_Uart1GetData(char * dataPtr, uint32_t dataLen);
uint32_t    app_uart_uart1_bytesAvailable(void);
void Uart_Uart1Setup(void);
bool Uart_Uart1_TxComplete(void);
void Uart_Uart1_RegisterRcvBuffer (RingBufferT *rxRingBuffer);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None

#endif // __UART_H

