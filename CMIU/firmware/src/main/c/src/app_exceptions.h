/******************************************************************************
*******************************************************************************
**
**         Filename: app_exceptions.h
**    
**           Author: Troy Harstad
**          Created: 1/7/2015
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**
**
**    Copyright 2016 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential 
**    property of Neptune Technology Group. The use, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_EXCEPTIONS_H
#define __APP_EXCEPTIONS_H


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/


#endif // __APP_EXCEPTIONS_H

