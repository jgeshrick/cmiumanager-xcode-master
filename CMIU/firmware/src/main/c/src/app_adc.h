/******************************************************************************
*******************************************************************************
**
**         Filename: app_adc.h
**    
**           Author: Troy Harstad
**          Created: 7/6/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_ADC_H
#define __APP_ADC_H



/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
// ADC values for various battery voltage levels 
// (ADCVAL / 4096) * 2.5 * 3 = Battery voltage
#define BATT_ADC_3V55   1938    // 3.55 V
#define BATT_ADC_3V5    1911    // 3.50 V
#define BATT_ADC_3V49   1906    // 3.49 V
#define BATT_ADC_3V45   1884    // 3.45 V
#define BATT_ADC_3V44   1878    // 3.44 V
#define BATT_ADC_3V4    1856    // 3.40 V
#define BATT_ADC_3V35   1829    // 3.35 V
#define BATT_ADC_3V3    1802    // 3.30 V
#define BATT_ADC_3V25   1774    // 3.25 V
#define BATT_ADC_3V2    1747    // 3.20 V
#define BATT_ADC_3V15   1720    // 3.15 V
#define BATT_ADC_3V1    1693    // 3.10 V
#define BATT_ADC_2V3    1257    // 2.30 V


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_adc_init(void);
extern void app_adc_vddDiv3_setupAndMeasure(void);
extern void app_adc_intTemp_setupAndMeasure(void);
extern uint32_t app_adc_vddDiv3_retrieve(void);
extern int8_t app_adc_intTemp_retrieve(void);



// ADC Temp and Battery Voltage associated with last cellular Tx attempt
extern void app_adc_intTemp_mostRecentTx_store(int8_t temp);
extern int8_t app_adc_intTemp_mostRecentTx_retrieve(void);
extern void app_adc_vddDiv3_mostRecentTx_store(uint32_t AdcVal);
extern uint32_t app_adc_vddDiv3_mostRecentTx_retrieve(void);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_ADC_H

