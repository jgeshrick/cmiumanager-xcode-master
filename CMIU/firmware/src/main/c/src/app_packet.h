/******************************************************************************
*******************************************************************************
**
**         Filename: app_packet.h
**    
**           Author: Troy Harstad
**          Created: 4/15/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_PACKET_H
#define __APP_PACKET_H

#include "MqttManager.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_packet_init(void);
extern bool app_packet_build_detailedConfiguration(TRANSPORT_MANAGER* pT);
extern bool app_packet_build_intervalData(TRANSPORT_MANAGER* pT);

extern bool app_packet_parse(const MQTT_MANAGER_PUB* pPub);


extern uint8_t* app_packet_getPacketBuffer_detailedConfig(void);
extern uint32_t app_packet_getPacketSize_detailedConfig(void);
extern uint8_t* app_packet_getPacketBuffer_intervalData(void);
extern uint32_t app_packet_getPacketSize_intervalData(void);



/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_PACKET_H

