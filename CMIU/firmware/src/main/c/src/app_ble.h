/******************************************************************************
*******************************************************************************
**
**         Filename: Bluetooth.h
**    
**           Author: Barry Huggins
**          Created: 12/31/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __BLUETOOTH_H
#define __BLUETOOTH_H

#include "RingBuffer.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
void app_ble_init(void);
void app_ble_startup(bool  testMode);
void app_ble_advertiseForever(bool  enable);
void app_ble_power_on(void);
void app_ble_power_off(void);
bool app_ble_isPowered(void);
bool app_ble_bluetoothInit(void);
bool app_ble_process(void);
bool app_ble_sendData (uint8_t  *dataPtr,  uint8_t   size);
bool app_ble_getBattery (void);
bool app_ble_getTemperature (void);
bool app_ble_radioIsActive(void);
bool app_ble_workToDo(void);
bool app_ble_startTest (uint8_t   dtmCmdMsb, uint8_t   dtmCmdLsb);
uint8_t     app_ble_buffersAvailable(void);
bool app_ble_TxIsOpen(void);
bool  app_ble_linkIsActive(void);
void app_ble_stop(void);
void app_ble_registerReceiveBuffer(RingBufferT *pReceiveBuffer);
bool app_ble_advertiseForever_retrieve(void);
bool app_ble_wakeup(void);
bool app_ble_sleep(void); 





/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None

#endif // __BLUETOOTH_H

