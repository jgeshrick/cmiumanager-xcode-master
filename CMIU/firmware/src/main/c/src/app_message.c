/******************************************************************************
*******************************************************************************
**
**         Filename: app_message.c
**    
**           Author: Duncan Willis
**          Created: 9 Nov 2015
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_message.c
*
* @brief Handle Incoming packets from MQTT/CMIT/BTLE and send responses
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include "CommonTypes.h"
#include "DebugTrace.h"
#include "em_assert.h"
#include "typedefs.h"
#include "app_packet_parser.h"
#include "app_cmit_interface.h"
#include "app_scratch_ram.h"
#include "app_command.h"
#include "app_cellular.h"
#include "app_message.h"



static const char* constInterfaceStrings[CMIU_INTERFACE_COUNT] = {"CELL", "BTLE", "UART"};



/******************************************************************************
 * @brief
 *   Handle incoming msg from any source, and sends response back to same interface
 *
 * @note
 *
 * @param[in] pReceiveMsg The incoming msg
 *
 * @return None
 ******************************************************************************/
void app_message_on_receive(APP_RX_MSG* pReceiveMsg)
{
    APP_TX_MSG responseMsg;
    E_CODE eCode = E_CODE_UNINITIALIZED;
    
    // Initialise the response to a scratch buffer, and default to 
    // sending to the port from whence it came.
    responseMsg.pData       = app_scratch_ram_get(&responseMsg.bufferSize);
    responseMsg.dataLength  = 0;
    responseMsg.destination = pReceiveMsg->source;
    

    EFM_ASSERT(NULL != pReceiveMsg);
    EFM_ASSERT(pReceiveMsg->source < CMIU_INTERFACE_COUNT);

    DEBUG_TRACE("app_message_on_receive, Src= %s : %u bytes\r\n", 
        constInterfaceStrings[pReceiveMsg->source], 
        pReceiveMsg->dataLength);

    eCode = app_packet_parser_parse(pReceiveMsg, &responseMsg);
    
    // Error if the handler did not create a response.
    if (0 == responseMsg.dataLength)
    {
        DEBUG_ERROR_CHECK(eCode, "Must have a response");
        ///@note This would likely be a critical failure, EFM_ASSERT(0);?
    }
    else
    {
        app_message_send_response(&responseMsg);
    }    
}


/******************************************************************************
 * @brief
 *   Sends response to approriate interface
 *
 * @note
 *
 * @param[in] pResponseMsg The response msg to send
 *
 * @return None
 ******************************************************************************/
void app_message_send_response(const APP_TX_MSG* pResponseMsg)
{    
    EFM_ASSERT(NULL != pResponseMsg);
    EFM_ASSERT(NULL != pResponseMsg->pData);

    DEBUG_TRACE("app_message_send_response, Dest=%s:%u bytes\r\n", 
        constInterfaceStrings[pResponseMsg->destination], 
        pResponseMsg->dataLength);
    
    switch (pResponseMsg->destination)
    {
        case CMIU_INTERFACE_CELLULAR:
            app_cellular_publishResponse(pResponseMsg->pData, 
                                         pResponseMsg->dataLength);
            break;
        
        case CMIU_INTERFACE_BTLE:
            app_cmit_interface_SendRespMessage(pResponseMsg->pData, 
                                               pResponseMsg->dataLength);
            break;
        
        case CMIU_INTERFACE_UART:
            app_cmit_interface_SendRespMessage_Serial(pResponseMsg->pData, 
                                                      pResponseMsg->dataLength);            
            break;
        
        default:
            EFM_ASSERT(0);
            break;        
    }
}


