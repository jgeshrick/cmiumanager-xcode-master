/******************************************************************************
*******************************************************************************
**
**         Filename: app_rmu.h
**    
**           Author: Troy Harstad
**          Created: 4/30/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_RMU_H
#define __APP_RMU_H



/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_rmu_init(void);
extern uint32_t app_rmu_resetCause_Get(void);
extern uint32_t app_rmu_SumVersionStatus_Get(void);
extern uint32_t app_rmu_SumCrcStatus_Get(void);
extern uint8_t app_rmu_magSwipeCounter_Get(void);
extern uint8_t app_rmu_resetCounter_Get(void);
extern U_NEPTUNE_TIME app_rmu_magSwipeTime_Get(void);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/



/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_RMU_H

