/******************************************************************************
*******************************************************************************
**
**         Filename: app_command.h
**    
**           Author: Troy Harstad
**          Created: 7/1/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_COMMAND_H
#define __APP_COMMAND_H

#include "TagTypes.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
typedef struct sTag_COMMAND_IMAGE_UPDATE
{
    bool                    imgReqUpdateFlag;   // Set to true if update required
    S_IMAGE_VERSION_INFO    imgVersion;         // Version of the image to update
    uint32_t                imgStartAddress;    // Start address of image (internal flash)
    uint32_t                imgSize;            // Bytes in image
    uint8_t                 imgType;            // Type of image (Config, Application, etc.)
    uint32_t                extStartAddress;    // Start address of image (external flash)
    char                    imgFileUrl[110];    // Image file url
    uint64_t                commandTime;        // Time to perform the image update
       
} S_COMMAND_IMAGE_UPDATE;





typedef struct TOGGLE_LTE_PARAMS
{    
    uint64_t                startDelayInMsec;
    uint64_t                durationInMsec;
    uint8_t                 enableDisableModem;
} TOGGLE_LTE_PARAMS;



typedef struct LTE_CARRIER_ASSERT_PARAMS
{    
    uint64_t                startDelayInMsec;
    uint64_t                durationInMsec;
    S_COMMAND_RFTESTMODE    rfTestMode;
} LTE_CARRIER_ASSERT_PARAMS;


typedef struct BLE_CARRIER_ASSERT_PARAMS
{    
    uint64_t                     startDelayInMsec;
    uint64_t                     durationInMsec;
    S_COMMAND_BTLE_RFTESTMODE    bleTestMode;
} BLE_CARRIER_ASSERT_PARAMS;


typedef struct POWER_MODE_PARAMS
{    
    uint64_t                startDelayInMsec;
    uint64_t                durationInMsec;
    uint8_t                 powerMode;
} POWER_MODE_PARAMS;


typedef struct SLEEP_SECONDS_PARAMS
{    
    uint64_t                startDelayInMsec;
    uint64_t                durationInMsec;
} SLEEP_SECONDS_PARAMS;


typedef struct READ_DEVICES_PARAMS
{
    uint64_t                startDelayInMsec;
    uint8_t                 pulse;
    uint64_t                delayBetweenReadsMsec;
    uint64_t                numberOfRepeats;
    CMIU_INTERFACE          destination;
    uint32_t                count;
    S_REGISTER_DATA*        psARBReadingData;
} READ_DEVICES_PARAMS;

typedef struct ERASE_MEMORY_PARAMS
{
    uint16_t  imagesBitMap;
    CMIU_INTERFACE          destination;
    uint8_t   retries;
}ERASE_MEMORY_PARAMS;

typedef struct GET_MODEM_INFO_PARAMS
{    
    CMIU_INTERFACE          destination;
} GET_MODEM_INFO_PARAMS;

/**
** @brief Parameters required for the Update Modem command
**/
#define UPDATE_MODEM_FIRMWARE_SERVER_STRING_SIZE        80
#define UPDATE_MODEM_FIRMWARE_USERNAME_STRING_SIZE        16
#define UPDATE_MODEM_FIRMWARE_PASSWORD_STRING_SIZE        24
#define UPDATE_MODEM_FIRMWARE_FILENAME_STRING_SIZE        64
#define UPDATE_MODEM_FIRMWARE_NEW_VERSION_STRING_SIZE        24

typedef struct COMMAND_UPDATE_MODEM_FIRMWARE_PARAMS
{
	uint8_t     ftpServer[UPDATE_MODEM_FIRMWARE_SERVER_STRING_SIZE];
	uint16_t    ftpPort;
    uint8_t     username[UPDATE_MODEM_FIRMWARE_USERNAME_STRING_SIZE];
    uint8_t     password[UPDATE_MODEM_FIRMWARE_PASSWORD_STRING_SIZE];
	uint8_t     fotaFilename[UPDATE_MODEM_FIRMWARE_FILENAME_STRING_SIZE];
    uint8_t     newFirmwareVersion[UPDATE_MODEM_FIRMWARE_NEW_VERSION_STRING_SIZE];
} COMMAND_UPDATE_MODEM_FIRMWARE_PARAMS;

/**
** @brief Parameters required for the Update APN command
**/
typedef struct COMMAND_UPDATE_APN_PARAMS
{    
    CMIU_INTERFACE          destination;
    uint8_t     apnString[APN_LENGTH_MAX];
} COMMAND_UPDATE_APN_PARAMS;

/**
** @brief Parameters required for the run connectivity command
**/
typedef struct COMMAND_RUN_CONNECTIVITY_PARAMS
{
    CMIU_INTERFACE      destination;
    E_PACKET_INSTIGATOR packetInstigator;
} COMMAND_RUN_CONNECTIVITY_PARAMS;

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/

extern void app_command_init(void);
extern void app_command_resetState(void);
void app_command_tick(uint32_t tickIntervalMs);
void app_command_end(void);


bool app_command_post_toggleLTE(const TOGGLE_LTE_PARAMS* pParams);
bool app_command_post_LTECarrierAsser(const LTE_CARRIER_ASSERT_PARAMS* pParams);
bool app_command_post_BLECarrierAssert(const BLE_CARRIER_ASSERT_PARAMS* pParams);
bool app_command_post_selectPowerMode(const POWER_MODE_PARAMS* pParams);
bool app_command_post_sleepSeconds(const SLEEP_SECONDS_PARAMS* pParams);
bool app_command_post_readConnectedDevices(const READ_DEVICES_PARAMS* pParams);
bool app_command_post_eraseMemory(const ERASE_MEMORY_PARAMS* pParams);
bool app_command_post_setIntervals(const S_RECORDING_REPORTING_INTERVAL* pParams,
                                   const APP_TX_MSG* pResponseMsg);
bool app_command_post_modemFota(const COMMAND_UPDATE_MODEM_FIRMWARE_PARAMS* pParams);
bool app_command_post_requestApn(const GET_MODEM_INFO_PARAMS* pParams);
bool app_command_post_updateApn(const COMMAND_UPDATE_APN_PARAMS* pParams);
bool app_command_post_runConnectivityTest(const COMMAND_RUN_CONNECTIVITY_PARAMS* pParams);

extern COMMAND_UPDATE_MODEM_FIRMWARE_PARAMS  *getDeferredModemUpdateParams (void);


extern bool app_command_imageUpdate_handler(S_IMAGE_METADATA *imageMetadata,
                                     S_IMAGE_VERSION_INFO *imageVersionInfo, 
                                     uint64_t applyAtTime, 
                                     const char* imageUrl);

S_COMMAND_IMAGE_UPDATE* app_command_imageUpdate_infoRetrieve(void);

bool app_command_imageUpdate_schedule(void);                                   
         
bool app_command_post_getSignalQuality(const GET_MODEM_INFO_PARAMS* pParams);
bool app_command_post_getCANData(const GET_MODEM_INFO_PARAMS* pParams);
         
bool app_command_publishRequestedPacket_handler(APP_TX_MSG*  pResponseMsg, uint8_t packetType,
                                        uint8_t destination);
                                         
extern bool app_command_inIdleState(void);
extern bool app_command_wait_till_end(void);
extern bool app_command_testForever_retrieve(void);

#endif // __APP_COMMAND_H

