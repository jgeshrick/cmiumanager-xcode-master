/******************************************************************************
*******************************************************************************
**
**         Filename: Error.c
**    
**           Author: Troy Harstad
**          Created: 08/04/2009
**
**     Last Edit By: Nick Sinas
**        Last Edit: 12/13/2012
**    
**      Description: The Error module contains all the routines and functions 
**                   necessary to track the error conditions of the R900i V2. 
**
**					 The various error conditions are to be handled in different
**					 ways.
**
**
** Revision History:
**         10/08/09: First created (Troy Harstad)
**         12/12/12: Updated for RV4 (Nick Sinas)
**
**	    Code Review:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/        
#include <stdint.h>
#include "typedefs.h"
#include "app_error.h"
#include "app_uart.h"
#include "app_cmit_interface.h"

 
/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/



/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static void Error_ErrorCode_Set(uint8_t byCode);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
static uint8_t byErrorCode;
static uint8_t byARBErrorCode;


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h



/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/

/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/


/******************************************************************************
**
**            Filename: Error.c
**        Routine Name: Error_InitHW
**    
**              Author: Troy Harstad
**             Created: 08/02/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function initializes the hardware used within the 
**                      Error.c source file.  
**
******************************************************************************/
void Error_InitHW(void)
{	
	
}


/******************************************************************************
*******************************************************************************
**
**            Filename: Error.c
**       Function Name: Error_Init
**    
**              Author: Troy Harstad
**             Created: 08/02/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function intializes the software for the Error.c
**						source file. 
**
******************************************************************************/
void Error_Init(void)
{
    Error_ErrorCode_Clear();
}


/******************************************************************************
*******************************************************************************
**
**            Filename: Error.c
**       Function Name: Error_ARB_XXXX
**    
**              Author: Nick Sinas
**             Created: 2/26/2013
**   
**  Function Arguments: None
**    Function Returns: None
**
**         Description: ERROR_ARB_TIMEOUT
**                      ERROR_ARB_MSG_TIMEOUT
**                      ERROR_ARB_DATA_PARITY
**                      ERROR_ARB_DATA_CHECKSUM
**                      ERROR_ARB_DATA_FORMAT
**                      ERROR_ARB_DATA_UNKNOWN_REG
**
******************************************************************************/
void Error_ARB_Data_Timeout(void)
{
 	Error_ErrorCode_ARB_Set(ERROR_ARB_TIMEOUT);
    //DEBUG_OUTPUT_TEXT("ERROR_ARB_TIMEOUT");   
}
void Error_ARB_Data_Msg_Timeout(void)
{
    Error_ErrorCode_ARB_Set(ERROR_ARB_MSG_TIMEOUT);
}
void Error_ARB_Data_Parity(void)
{
 	Error_ErrorCode_ARB_Set(ERROR_ARB_DATA_PARITY);
    //DEBUG_OUTPUT_TEXT("ERROR_ARB_DATA_PARITY");   
}
void Error_ARB_Data_Checksum(void)
{
 	Error_ErrorCode_ARB_Set(ERROR_ARB_DATA_CHECKSUM);
    //DEBUG_OUTPUT_TEXT("ERROR_ARB_DATA_CHECKSUM");   
}
void Error_ARB_Data_Format(void)
{
 	Error_ErrorCode_ARB_Set(ERROR_ARB_DATA_FORMAT);  
    //DEBUG_OUTPUT_TEXT("ERROR_ARB_DATA_FORMAT"); 
}
void Error_ARB_Data_Unknown_Register(void)
{
    Error_ErrorCode_ARB_Set(ERROR_ARB_DATA_UNKNOWN_REG);
    //DEBUG_OUTPUT_TEXT("ERROR_ARB_DATA_UNKNOWN_REG");
}

void Error_ARB_Data_Mismtach_RegisterType(void)
{
    Error_ErrorCode_ARB_Set(ERROR_ARB_DATA_MISMATCH_REG_TYPE);
    //DEBUG_OUTPUT_TEXT("ERROR_ARB_DATA_MISMATCH_REG");
}


void Error_ARB_Data_Mismtach_RegisterID(void)
{
    Error_ErrorCode_ARB_Set(ERROR_ARB_DATA_MISMATCH_REG_ID);
    //DEBUG_OUTPUT_TEXT("ERROR_ARB_DATA_MISMATCH_REG");
}

/******************************************************************************
*******************************************************************************
**
**            Filename: Error.c
**       Function Name: Error_ImageCRC_XXXX
**    
**              Author: Nick Sinas
**             Created: 6/25/2013
**   
**  Function Arguments: None
**    Function Returns: None
**
**         Description: ERROR_IMGCRC_CONFIGURATION
**                      
**
******************************************************************************/
void Error_ImageCRC_Configuration(void)
{
    Error_ErrorCode_Set(ERROR_IMGCRC_CONFIGURATION);
    DEBUG_OUTPUT_TEXT("ERROR_IMGCRC_CONFIGURATION");
}


/******************************************************************************
*******************************************************************************
**
**            Filename: Error.c
**       Function Name: Error_ErrorCode_Set
**                      Error_ErrorCode_Clear
**                      Error_ASIC_ErrorCode_Retrieve
**    
**              Author: Nick Sinas
**             Created: 02/26/2013
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: These functions set/clear/retrieve the error code. 
**
******************************************************************************/
void Error_ErrorCode_ARB_Set(uint8_t byCode)
{
	byARBErrorCode = byCode;
}
void Error_ErrorCode_Set(uint8_t byCode)
{
	byErrorCode = byCode;
}

void Error_ErrorCode_ARB_Clear(void)
{
	byARBErrorCode = ERROR_CODE_NO_ERROR;	
}
void Error_ErrorCode_Clear(void)
{
	byErrorCode = ERROR_CODE_NO_ERROR;
	byARBErrorCode = ERROR_CODE_NO_ERROR;	
}

uint8_t Error_ErrorCode_Retrieve(void)
{
    if(byARBErrorCode)
	{
		return byARBErrorCode;
	}

	return byErrorCode;
}

uint8_t Error_ErrorCode_ARB_Retrieve(void)
{	
	return byARBErrorCode;
}








