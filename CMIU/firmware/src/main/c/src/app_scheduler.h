/******************************************************************************
*******************************************************************************
**
**         Filename: app_scheduler.h
**    
**           Author: Troy Harstad
**          Created: 7/9/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_SCHEDULER_H
#define __APP_SCHEDULER_H



/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


typedef struct sTag_TASK_SCHEDULER
{
    volatile bool  taskDue;                  // Task due flag
    volatile U_NEPTUNE_TIME taskTime;        // Next time for task to be performed

} S_TASK_SCHEDULER;



/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/

typedef enum E_SCHEDULER_TASK_TYPE
{
    // Register read task
    E_SCHEDULER_TASK_TYPE_REGISTER_READ               = 0x00,
    
    // Detailed config packet task
    E_SCHEDULER_TASK_TYPE_DETAILEDCONFIG_PACKET       = 0x01,
    
    // Interval data packet task
    E_SCHEDULER_TASK_TYPE_INTERVALDATA_PACKET         = 0x02,
    
    // Cellular connection task
    E_SCHEDULER_TASK_TYPE_CELLCONNECTION              = 0x03,
    
    // Image update task
    E_SCHEDULER_TASK_TYPE_IMAGEUPDATE                 = 0x04,

    // Datalog task
    E_SCHEDULER_TASK_TYPE_DATALOG                     = 0x05,    

    // Modem Image update task 
    E_SCHEDULER_TASK_TYPE_MODEMFOTA                   = 0x06
    
} E_SCHEDULER_TASK_TYPE;


typedef enum E_SCHEDULER_INTERVAL_TYPE
{
    
    // Schedules per configuration interval
    E_SCHEDULER_INTERVAL_TYPE_CONFIGURATION             = 0x00,
    
    // Schedules based on offset from current time
    E_SCHEDULER_INTERVAL_TYPE_FORCE_OFFSET              = 0x01,
    
    // Schedules based on time value passed
    E_SCHEDULER_INTERVAL_TYPE_FORCE_TIME                = 0x02
       
    
} E_SCHEDULER_INTERVAL_TYPE;



/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_scheduler_init(void);
extern void app_scheduler_intervalUpdate(void);
extern void app_scheduler_timeChange(U_NEPTUNE_TIME * timeChange);
extern void app_scheduler_rescheduleAll(void);
extern void app_scheduler_scheduleTask(E_SCHEDULER_TASK_TYPE taskType, 
                                       E_SCHEDULER_INTERVAL_TYPE intervalType, 
                                       uint32_t forceSeconds, 
                                       int64_t time);
extern bool app_scheduler_checkTaskDue(E_SCHEDULER_TASK_TYPE taskType);
extern void app_scheduler_unscheduleTask(E_SCHEDULER_TASK_TYPE taskType);
extern uint64_t app_scheduler_checkTaskTime(E_SCHEDULER_TASK_TYPE taskType);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_SCHEDULER_H

