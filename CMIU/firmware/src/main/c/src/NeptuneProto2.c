/******************************************************************************
*******************************************************************************
**
**         Filename: NeptuneProto2.c
**    
**           Author: Barry Huggins
**          Created: 12/23/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file NeptuneProto2.c
*
* @brief Small file used during the development of the MSPD CMIU code
* 
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "typedefs.h"
#include "efm32lg332f256.h"
#include <stdbool.h>
#include "autogen_init.h"
#include "app_arb.h"
#include "app_flash.h"
#include "app_ble.h"
#include "app_modem.h"
#include "app_uart.h"
#include "app_timers.h"
#include "app_power.h"
#include "app_rtc.h"
#include "app_gpio.h"
#include "app_cmu.h"
#include "app_cmiu_app.h"
#include "app_error.h"
#include "app_metrology.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "em_timer.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
//#define MODEM_ENABLED
//#define TEST_FLASH
#define TEST_BLUETOOTH
//#define APP_TEST

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static void CMIU_Init(void);
bool   App_ProcessInputs (void);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h
// Let the compiler check the typedefs.  For instance if int8_t ever becomes
// something other than 1 byte the [sizeof(int8_t) == 1] will become false, 
// which means the union member int8_err will have an array size of zero.  
// The compiler will give an error if the array size is set to zero.
// PC-lint will give several errors for this since it is an incomplete array
// and the mebers are not referenced so it is removed from the lint check using
// the #ifndef _lint directive.
#ifndef	_lint
union {
    char int8_err   [sizeof(int8_t)    == 1];
    char uint8_err  [sizeof(uint8_t)   == 1];
    char int16_err  [sizeof(int16_t)   == 2];
    char uint16_err [sizeof(uint16_t)  == 2];
    char int32_err  [sizeof(int32_t)   == 4];
    char uint32_err [sizeof(uint32_t)  == 4];
    char uint64_err [sizeof(uint64_t)  == 8];
    char int64_err  [sizeof(int64_t)   == 8];
    char ptr_err    [sizeof(uintptr_t) == 4];
}; //Anonymous
#endif

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None


/***************************************************************************//**
 * @brief
 *   main function
 *
 * @note
 *   Executes main program
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
int main(void)
{
#ifdef MODEM_ENABLED
    char    readBuff[32];
    char    replyBuff[128];
    char    *tempPtr;
#endif // MODEM_ENABLED
    
#ifdef TEST_FLASH
    uint16_t    flashDeviceID;
    uint8_t     flashStatusRegister;
#endif // TEST_FLASH
    

#ifdef APP_TEST
    volatile uint32_t timerVal;
#endif // APP_TEST

    /* Initialize chip */
    eADesigner_Init();
    gpio_Init();
    Uart_Uart1Setup();
    Uart_Uart0Setup();
    
    USART_Enable(USART2, usartEnable);

    DEBUG_OUTPUT_TEXT("CMIU RESET \r\n");   
    
    // give time to erase if stuck in low power mode
    Timers_Delay (5000);   
    
    if (Power_ModemIsPowered())
        Uart_Uart1PutData("modem powered\n\r", 15);
    else
        Uart_Uart1PutData("modem not powered\n\r", 19);

#ifdef TEST_FLASH
    Timers_Delay (5000);
    Uart_Uart1PutData("enable flash\n\r", 14);
    Flash_PowerOn();
    Timers_Delay (3000);
    flashDeviceID = Flash_ReadDeviceIdentification();
    Timers_Delay (1000);
    flashStatusRegister = Flash_ReadStatusRegister();
    
    Timers_Delay (5000);
    Flash_TestWriteRead ();
#endif // TEST_FLASH    

#ifdef TEST_BLUETOOTH
    Timers_Delay (3000);
    Uart_Uart1WriteString("enable Nordic\n\r");
    // power on Bluetooth module
    Ble_BluetoothStartup();
    
    Timers_Delay (2000);
    //Ble_StartAciTest();
    Ble_BluetoothInit();
#endif // TEST_BLUETOOTH    
    
#ifdef MODEM_ENABLED
    Timers_Delay (1000);
    
    Power_ModemPowerOn();

    Timers_Loop_Delay (5000);

    if (Power_ModemIsPowered())
        Uart_Uart1PutData("modem powered\n\r", 15);
    else
        Uart_Uart1PutData("modem not powered\n\r", 19);
    
    Timers_Loop_Delay (5000);
            
    app_modem_isRegistered();
    
    Timers_Loop_Delay (5000);
    
    app_modem_testTimeFunction();
    
    while (1);
    
    // Change the SMS to text mode
    if (app_modem_sendAT("AT+CMGF=1", "OK", 3000))
        Uart_Uart1PutData ("CMFG OK\n\r", 9);

    Timers_Delay (2000);
#endif  //  MODEM_ENABLED   
        
#ifdef MODEM_ENABLED    
    Uart_Uart1PutData ("sending SMS\r\n", 13);
    
    Uart_Uart0PutData("AT+CMGS=\"9198012123\"\n\r",22);
    Timers_Loop_Delay (3000);
    Uart_Uart0PutData("hello \x1A", 7);
    
  	while (1)
	{
        uint8_t     tempChar;

        Timers_Loop_Delay (10000);
        Uart_Uart1PutData ("waiting\r\n", 9);
        if (Power_ModemIsPowered())
        {
            if (app_modem_sendATGetReply("AT+CSQ", "CSQ:", replyBuff, sizeof(replyBuff), 3000))
            {
                tempPtr = strstr (replyBuff, "CSQ:");
                Uart_Uart1PutData (tempPtr, strlen(tempPtr));
            }
        }

        // try getting a char from the debug UART
        if (Uart_Uart1IsCharReady())
        {
            tempChar = Uart_Uart1GetChar();
            if (tempChar == 's')
            {
                if (Power_ModemIsPowered())
                {
                    Uart_Uart1PutData ("shutting down modem\n\r", 21);
                    app_modem_shutdown ();
                }
                else
                    Uart_Uart1PutData ("modem already off\n\r", 19);
            }
        }

	}
}
#endif  //  MODEM_ENABLED   

   
    // Initialize CMIU, this overwrites some GPIO/Clock settings in an effort to
    // configure the board to reach 2uA sleep current.
//    CMIU_Init();

    while (1)
    {
        Ble_BluetoothProcess();
        
        App_ProcessInputs();
    }
}  // end main 

#ifdef APP_TEST
    
    // Initialize CMIU, this overwrites some GPIO/Clock settings in an effort to
    // configure the board to reach 2uA sleep current.
    CMIU_Init();
  
    
    while (1)
	{
        // Set PA.4 upon wakeup
        DEBUG_INSTRUCTION(GPIO->P[gpioPortA].DOUTSET = 1 << 4);        

        // Output "tick" on every wakeup
        DEBUG_OUTPUT_TEXT("tick\r\n");

        // Toggle PA.3 on every wakeup (1 second)
        DEBUG_INSTRUCTION(GPIO->P[gpioPortA].DOUTTGL = 1 << 3);

        // Tick CMIU Application module oncce per second
        app_cmiu_app_tick();

        // Wait for UART to complete before entering sleep mode
        DEBUG_WAITFORUART();  
        
        // Clear PA.4 before going to sleep
        DEBUG_INSTRUCTION(GPIO->P[gpioPortA].DOUTCLR = 1 << 4);        

         // Enter EM2 and wake up up to one second later
        EMU_EnterEM2(true); 


	}   

  
  
}




/***************************************************************************//**
 * @brief
 *   Performs overall CMIU initialization
 *
 * @note
 *       
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void CMIU_Init(void)
{
    uint32_t  startDelay;
    
    // CMU should be initialized first since if a module clock is disabled, the
    // registers of that module are not accessible and reading from such 
    // registers may return undefined values. Writing to registers of clock 
    // disabled modules have no effect. One should normally avoid accessing
    // module registers of a module with a disabled clock.
    app_cmu_init();
    
    // Initialize RTC module
    app_rtc_init();
    
    // Initialize GPIO module
    app_gpio_init();
    
    // Initialize timers module
    app_timers_init();
    
    // Initialize ARB module
    ARB_Init();
    
    // Initialize Metrology module
    app_metrology_Init();
 
    // Initialize Error module 
    Error_Init();
    
    // Initialize App module   
    app_cmiu_app_init();
   

    // Delay before possible sleep so that debugger can attach to target
    for(startDelay = 0x1FFFFF; startDelay>0; startDelay--)
    {
        __NOP();
    } 
    
    DEBUG_OUTPUT_TEXT("CMIU Initialization is complete\n\r");
    
}


bool   App_ProcessInputs ()
{
    char    tempChar;
    char    testBuffer[] = "hi bob";
    // try getting a char from the debug UART
    if (Uart_Uart1IsCharReady())
    {
        tempChar = Uart_Uart1GetChar();
        /****************
        if (tempChar == 'p')
        {
            char        debugBuffer[32];
            int   i;
            for (i=1; i<=4; i++)
            {
                if (lib_aci_is_pipe_available(&aci_state, i))
                {
                    sprintf(debugBuffer, "pipe %d avail\n", i);
                    debug_printf(debugBuffer);
                }
            }
            debug_printf("opening pipe\n");
            lib_aci_open_remote_pipe(&aci_state, PIPE_NORDIC_UART_OVER_BTLE_UART_RX_TX);
        }
        ****************/
        if (tempChar == 'b')
        {
            debug_printf("sending data\n"); 
            Ble_BluetoothSendData ((uint8_t *)testBuffer, strlen(testBuffer));
//            lib_aci_set_local_data(&aci_state, PIPE_NORDIC_UART_OVER_BTLE_UART_RX_TX, testBuffer, strlen(testBuffer));
        }
        else if (tempChar == 't')
        {
            //debug_printf("checking battery\n"); 
            //Ble_BluetoothGetBattery ();
            debug_printf("checking temp\n"); 
            Ble_BluetoothGetTemperature ();
        }
    }
    return true;
}    
 


