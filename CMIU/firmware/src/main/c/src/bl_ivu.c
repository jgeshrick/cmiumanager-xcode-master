/******************************************************************************
*******************************************************************************
**
**         Filename: bl_ivu.c
**
**           Author: Brian Arnberg
**          Created: 2015.06.11
**
**     Last Edit By:
**        Last Edit:
**
** Revision History:
**       2015.06.11: First created (Brian Arnberg)
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file bl_ivu.c
*
* @brief This module is used to validate the CRC-32 and version of each
*        image.
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include "typedefs.h"
#include "bl_ivu.h"
#include "cmiuFlash.h"
#include "app_timers.h"
#include "app_uart.h"
#include "app_cmit_interface.h"

#include "Crc32Fast.h"

/*=========================================================================*/
/*  L O C A L   G L O B A L   V A R I A B L E S                            */
/*=========================================================================*/
/** Each bit represents a specifc image, 1 indicates a valid CRC */
static uint32_t SumCrcStatus;
/** Each bit represents a specifc image, 1 indicates a valid Version */
static uint32_t SumVersionStatus;
/** Each bit represents a specifc image, 1 indicates valid version AND CRC */
static uint32_t SumImageStatus;
/** Version Information per image (there are 12 images right now) */
static uint16_t ListVersions[12] = {0UL,0UL,0UL,0UL,
                                    0UL,0UL,0UL,0UL,
                                    0UL,0UL,0UL,0UL};


/***************************************************************************//**
 * @brief
 *  Initialize the variables for the image validation unit (IVU)
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void    bl_ivu_init(void)
{
    /** Initialize each image CRC to invalid */
    SumCrcStatus        = 0x00000000;

    /** Initialize each image version to invalid */
    SumVersionStatus    = 0x00000000;

    /** The SumImageStatus is the AND of the CRC and Version */
    SumImageStatus      = (SumCrcStatus & SumVersionStatus);
}

/***************************************************************************//**
 * @brief
 *  Validate the CRC of an image located in internal flash.
 *
 * @note
 *  Internal images can be processed in one pass, or block by block. It is
 *  assumed that internal images are continuous and that they do not need
 *  to be processed block by block.
 *
 * @param[in]
 *  *ValidationImage - Pointer to image information.
 *
 * @return
 *  result - The CRC validation result. True means the CRC is good.
 ******************************************************************************/
bool bl_ivu_InternalCrc(const ImageInformation_t *ValidationImage)
{
    bool result;
    static volatile uint32_t crcResult;
    static volatile uint32_t crcExpected;

    /** Get the expected CRC from internal flash */
    crcExpected = ReverseCrc32(*ValidationImage->CrcAddress);
    /** Calculte the CRC of the internal image */
    if ((ValidationImage->Length % 4) == 0)
    {
        crcResult = FastCrc32Word(CRC_SEED,ValidationImage->Address,ValidationImage->Length);
    }
    else
    {
        crcResult = FastCrc32Byte(CRC_SEED,ValidationImage->Address,ValidationImage->Length);
    }

    /** Compare the expected value to the result */
    if (crcResult != crcExpected)
    {
        result = false;
    }
    else
    {
        result = true;
    }

    /** Debugging statements for help */
    DEBUG_OUTPUT_TEXT_AND_DATA("...CRC Valid: ", (uint8_t *)&result, 1);

    return result;
}

/***************************************************************************//**
 * @brief
 *  Validate the CRC of an image located in external flash.
 *
 * @note
 *  External images need to be processed block by block.
 *
 * @param[in]
 *  *ValidationImage - Pointer to image information.
 *
 * @return
 *  result - The CRC validation result. True means the CRC is good.
 ******************************************************************************/
bool bl_ivu_ExternalCrc(const ImageInformation_t *ValidationImage)
{
    bool result;
    static volatile uint32_t crcResult;
    static volatile uint32_t crcExpected;
    uint8_t crcBuffer[4];
    uint8_t processingBlock[IVU_BLOCK_SIZE];
    uint32_t remainingLength;
    uint32_t nextAddress;

    /** Setup starting variables */
    remainingLength = (ValidationImage)->Length;
    nextAddress = (uint32_t)ValidationImage->Address;

    /** Initialize flash communications. */
    Flash_init();

    (void) Flash_WaitOnWip(50);

    /** Get the expected CRC from internal flash */
    Flash_ReadFromAddress(crcBuffer, (uint32_t)(ValidationImage->CrcAddress), 4);
    crcExpected = (((crcBuffer[0] << 24) & 0xFF000000) |
                   ((crcBuffer[1] << 16) & 0x00FF0000) |
                   ((crcBuffer[2] <<  8) & 0x0000FF00) |
                   ((crcBuffer[3])       & 0x000000FF));

    (void) Flash_WaitOnWip(50);

    /** Initialize crcResult so that the crc engine can run properly. */
    crcResult = CRC_SEED;

    /** Process each block, feeding the result from one block to the next. */
    while(remainingLength > 0)
    {
        if (remainingLength > IVU_BLOCK_SIZE)
        {
            Flash_ReadFromAddress(processingBlock, (uint32_t)(nextAddress), IVU_BLOCK_SIZE);
            crcResult = FastCrc32Word(crcResult,&processingBlock[0],IVU_BLOCK_SIZE);
            remainingLength -= IVU_BLOCK_SIZE;
            nextAddress += IVU_BLOCK_SIZE;
        }
        else
        {
            Flash_ReadFromAddress(processingBlock, (uint32_t)(nextAddress), remainingLength);
            if ((remainingLength % 4) == 0)
            {
                crcResult = FastCrc32Word(crcResult,&processingBlock[0],remainingLength);
            }
            else
            {
                crcResult = FastCrc32Byte(crcResult,&processingBlock[0],remainingLength);
            }
            remainingLength = 0;
        }

    }

    /** Deinitialize flash communications. */
    Flash_Deinit();

    if (crcResult != crcExpected)
    {
        result = false;
    }
    else
    {
        result = true;
    }

    /** Debugging statements for help */
    DEBUG_OUTPUT_TEXT_AND_DATA("...CRC Valid: ", (uint8_t *)&result, 1);

    return result;
}

/***************************************************************************//**
 * @brief
 *  Validate the version of an image located in internal flash.
 *
 * @note
 *  Currently, a version is valid so long as it is neither 0x0000 nor 0xFFFF.
 *  The function gets the version, verifies that it is valid, then returns
 *  the result.
 *
 * @param[in]
 *  *ValidationImage - Pointer to image information.
 *
 * @return
 *  result - The version validation result. True means the version is good.
 ******************************************************************************/
bool bl_ivu_InternalVersion(const ImageInformation_t *ValidationImage)
{
    bool result;
    static uint16_t versionBuffer[2];
    static uint32_t version;

    /** Get the version information from internal flash */
    versionBuffer[0] = ValidationImage->MiuImageInfo->versionMajor;
    versionBuffer[1] = ValidationImage->MiuImageInfo->versionMinor;

    /** Combine the version information into a single variable */
    version = (uint32_t)((versionBuffer[0] << 16) | (versionBuffer[1]));

    /** Store the version to the list of version information */
    ListVersions[ValidationImage->ImageType] = version;

    /** Internal versions are valid if they are not NULL */
    if ((version > 0) && (version != ~(0UL)))
    {
        result = true;
    }
    else
    {
        result = false;
    }

    /** Debugging statements for help */
    DEBUG_OUTPUT_TEXT_AND_DATA("...Version Valid: ", (uint8_t *)&result, 1);

    return result;
}

/***************************************************************************//**
 * @brief
 *  Validate an image located in external flash.
 *
 * @note
 *  Currently, a version is valid so long as it is neither 0x0000 nor 0xFFFF.
 *  New images must have a version greater than the current version; backup
 *  images just need a valid version. The function reads the version, checks it
 *  based on its image type, then returns the validation result.
 *
 * @param[in]
 *  *ValidationImage - Pointer to image information.
 *
 * @return
 *  result - The image validation result. True means the version is good.
 ******************************************************************************/
bool bl_ivu_ExternalVersion(const ImageInformation_t *ValidationImage)
{
    bool result;
    static uint8_t versionBuffer[4];
    static uint32_t version;
    static ImageType_t imageType;

    /** Initialize local copy of ImageType */
    imageType = ValidationImage->ImageType;

    /** Initialize flash communications. */
    Flash_init();

    /** Get Version from FLASH */
    Flash_ReadFromAddress(versionBuffer, (uint32_t)(ValidationImage->MiuImageInfo), 4);

    /** Deinitialize flash communications. */
    Flash_Deinit();

    /** Calculate the version from the version buffer. */
    version = (uint32_t)((versionBuffer[0] << 16) |
                         (versionBuffer[1] << 24) |
                         (versionBuffer[2] << 8) |
                         (versionBuffer[3]));
    /** Store the version to the list of version information */
    ListVersions[imageType] = version;

    /** Only look at images that are not NULL. */
    if ((version > 0) && (version != ~(0UL)))
    {
        /** Validate version based on image type */
        if (imageType == NewApplication)
        {
            /** The new image is valid IFF it is greater than the version
             ** of the image currently running */
            if (version > ListVersions[CmiuApplication])
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
        else if (imageType == NewConfiguration)
        {
            /** The new image is valid IFF it is greater than the version
             ** of the image currently running */
            if (version > ListVersions[CmiuConfiguration])
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
        else if (imageType == NewEncryption)
        {
            /** The new image is valid IFF it is greater than the version
             ** of the image currently running */
            if (version > ListVersions[CmiuConfiguration])
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
        else if (imageType == NewBleConfiguration)
        {
            /** The new image is valid IFF it is greater than the version
             ** of the image currently running */
            if (version > ListVersions[CmiuConfiguration])
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
        else if ((imageType == BackupApplication) ||
                 (imageType == BackupConfiguration) ||
                 (imageType == BackupEncryption) ||
                 (imageType == BackupBleConfiguration))
        {
            /** A backup version is valid provided it is not NULL */
            result = true;
        }
        else
        {
            /** Even though the version is not NULL, it is invalid because
             ** the image type is wrong. */
            result = false;
        }
    }
    /** The version is NULL, so the result is FALSE */
    else
    {
        result = false;
    }

    /** Debugging statements for help */
    DEBUG_OUTPUT_TEXT_AND_DATA("...Version Valid: ", (uint8_t *)&result, 1);

    return result;
}


/***************************************************************************//**
 * @brief
 *  Get the SumCrcStatus variable.
 *
 * @note
 *  Each bit represents an image. If the bit is 1, that means the CRC
 *  information is valid.
 *
 * @param[in] None
 *
 * @return SumCrcStatus - CRC status for each image
 ******************************************************************************/
uint32_t bl_ivu_GetSumCrcStatus(void)
{
    return SumCrcStatus;
}

/***************************************************************************//**
 * @brief
 *  Get the SumVersionStatus variable.
 *
 * @note
 *  Each bit represents an image. If the bit is 1, that means the version
 *  information is valid.
 *
 * @param[in] None
 *
 * @return SumVersionStatus - Version status for each image
 ******************************************************************************/
uint32_t bl_ivu_GetSumVersionStatus(void)
{
    return SumVersionStatus;
}

/***************************************************************************//**
 * @brief
 *  Get the SumImageStatus variable.
 *
 * @note
 *  Each bit represents an image. If the bit is 1, that means both the version
 *  and the CRC are valid. Otherwise, at least one is invalid.
 *
 * @param[in] None
 *
 * @return SumImageStatus - Summary (version and CRC) status for each image
 ******************************************************************************/
uint32_t bl_ivu_GetSumImageStatus(void)
{
    return SumImageStatus;
}

/***************************************************************************//**
 * @brief
 *  Validate all known images.
 *
 * @note
 *  This function looks at all of the images in the list of images. The first
 *  few images are internal images, and the rest are external images. This
 *  function knows (based on some #defines) which ones are which, and validates
 *  based on image location.
 *
 * @param[in]
 *  *ValidationImage - pointer to the list of all images
 *
 * @param[in]
 *  StartingImage - What is the first image to validate?
 *
 * @param[in]
 *  LastImage - What is the last image to validate?
 *
 * @return
 *  result - what is the current overall image status?
 ******************************************************************************/
IMAGE_VALIDATION_INFORMATION bl_ivu_CheckImages(const ImageInformation_t *ValidationImage,
                                                uint8_t StartingImage,
                                                uint8_t LastImage)
{
    IMAGE_VALIDATION_INFORMATION result;
    uint8_t i;
    DEBUG_INSTRUCTION(uint32_t helper);
    DEBUG_INSTRUCTION(uint32_t helper2);

    /** Start the timers. */
    app_timers_enableMsecs();

    /** Check internal images (0-3) */
    for (i = StartingImage; (i < LastImage) && (i < START_OF_BACKUP); ++i)
    {
        DEBUG_OUTPUT_TEXT_AND_DATA("...Verifying image number: ", (uint8_t *)&i, 1);
        DEBUG_INSTRUCTION(helper = Timers_GetMsTicks());

        SumCrcStatus &= ~(1<<i);
        SumVersionStatus &= ~(1<<i);
        SumCrcStatus |= (bl_ivu_InternalCrc(&ValidationImage[i]) << i);
        SumVersionStatus |= (bl_ivu_InternalVersion(&ValidationImage[i]) << i);

        DEBUG_INSTRUCTION(helper2 = Timers_GetMsTicks());
        DEBUG_INSTRUCTION(helper = helper2 - helper);
        DEBUG_OUTPUT_TEXT_AND_DATA("...Time: ", (uint8_t *)&helper,4);
    }
    /** Check backup images (4-7) */
    for (i = START_OF_BACKUP; (i < LastImage) && (i < START_OF_NEW); ++i)
    {
        DEBUG_OUTPUT_TEXT_AND_DATA("...Verifying image number: ", (uint8_t *)&i, 1);
        DEBUG_INSTRUCTION(helper = Timers_GetMsTicks());

        SumCrcStatus &= ~(1<<i);
        SumVersionStatus &= ~(1<<i);
        SumCrcStatus |= (bl_ivu_ExternalCrc(&ValidationImage[i]) << i);
        SumVersionStatus |= (bl_ivu_ExternalVersion(&ValidationImage[i]) << i);

        DEBUG_INSTRUCTION(helper2 = Timers_GetMsTicks());
        DEBUG_INSTRUCTION(helper = helper2 - helper);
        DEBUG_OUTPUT_TEXT_AND_DATA("...Time: ", (uint8_t *)&helper,4);
    }
    /** Check New Images (8-11) */
    for (i = START_OF_NEW; (i < LastImage); ++i)
    {
        DEBUG_OUTPUT_TEXT_AND_DATA("...Verifying image number: ", (uint8_t *)&i, 1);
        DEBUG_INSTRUCTION(helper = Timers_GetMsTicks());

        SumCrcStatus &= ~(1<<i);
        SumVersionStatus &= ~(1<<i);
        SumCrcStatus |= (bl_ivu_ExternalCrc(&ValidationImage[i]) << i);
        SumVersionStatus |= (bl_ivu_ExternalVersion(&ValidationImage[i]) << i);

        DEBUG_INSTRUCTION(helper2 = Timers_GetMsTicks());
        DEBUG_INSTRUCTION(helper = helper2 - helper);
        DEBUG_OUTPUT_TEXT_AND_DATA("...Time: ", (uint8_t *)&helper,4);
    }

    /** Stop the timers. */
    app_timers_disableMsecs();

    /** The SumImageStatus is the AND of the CRC and Version */
    SumImageStatus      = (SumCrcStatus & SumVersionStatus);

    /** If at least one new image is valid, set NewImageValid */
    if ((SumImageStatus & (NEW_IMAGE_MASK)))
    {
        result = NewImageValid;
    }
    /** Each of the SafeToLaunch bits must be set */
    else if ((SumImageStatus & (SAFE_TO_LAUNCH)) == SAFE_TO_LAUNCH)
    {
        /** Each of the backup mask bits must be set */
        if ((SumImageStatus & (BACKUP_MASK)) == BACKUP_MASK)
        {
            result = SafeToLaunch;
        }
        /** At least one backup mask bit is clear */
        else
        {
            result = CorruptBackup;
        }
    }
    /** There are no new images valid and all necessary internals are corrupt.
     ** However, there are sufficient backup images to recover. */
    else if ((SumImageStatus & (NEEDED_BACKUP_MASK)) == NEEDED_BACKUP_MASK)
    {
        result = CorruptInternals;
    }
    /** All necessary files are corrupt, both internal and external */
    else
    {
        result = AllBad;
    }


    return result;
}
