Notes to Future User:
1. This is not production quality code.
2. This has not undergone extensive code reviews.
3. The intention was that it could be used as a basis or example of
   some of the tests we will want in our factory test system.

About the builds:
1. CmiuHardwareTest is a standalone application.
2. CmiuHardwareTest_mapped is the same application, but mapped so that
   it can be booted into from the CMIU bootloader.
