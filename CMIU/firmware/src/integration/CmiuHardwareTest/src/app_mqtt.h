/******************************************************************************
*******************************************************************************
**
**         Filename: app_mqtt.h
**    
**           Author: Barry Huggins
**          Created: 3/10/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_MQTT_H
#define __APP_MQTT_H



/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/

typedef enum
{
    MQTT_CONN_STATE_INIT,
    MQTT_CONN_STATE_DELAY_MODEM_POWER,
    MQTT_CONN_STATE_WAIT_MODEM_REGISTER,
    MQTT_CONN_STATE_START_DATA_CONNECTION,
    MQTT_CONN_STATE_WAIT_DATA_CONNECTION,
    MQTT_CONN_STATE_INIT_CONNECTION,
    MQTT_CONN_STATE_WAIT_CONNACK,
    MQTT_CONN_STATE_PUBLISH,
    MQTT_CONN_STATE_WAIT_PUBACK,
    MQTT_CONN_STATE_WAIT_MODEM_POWER_DOWN,
} E_MQTT_CONN_STATE_T;

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_mqtt_init(void);
void app_mqtt_setupConnection(void);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_RTC_H

