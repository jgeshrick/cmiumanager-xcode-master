/******************************************************************************
*******************************************************************************
**
**         Filename: app_mqtt.c
**    
**           Author: Barry Huggins
**          Created: 3/10/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_mqtt.c
*
* @brief This file contains the application code used to setup/use the MQTT 
*       Paho client.
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include "app_mqtt.h"
#include "app_cmiu_app.h"
#include "em_cmu.h"
#include "em_rtc.h"
#include "em_gpio.h"
#include "typedefs.h"
#include "app_uart.h"
#include "MQTTARM_M3.h"
#include "app_power.h"
#include "app_modem.h"
#include "../../../../../../ThirdPartyTools/org.eclipse.paho.mqtt.embedded-c/MQTTClient-C/src/MQTTClient.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
Network     cellNetwork;
Client      mqttClient;

// MQTT state
E_MQTT_CONN_STATE_T    mqttState = MQTT_CONN_STATE_INIT;

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/



/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/



/***************************************************************************//**
 * @brief
 *   Initializes the MQTT client subsystem
 *
 * @note
 *   
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_mqtt_init()
{
    NewNetwork (&cellNetwork);
    
    
    
}

/***************************************************************************//**
 * @brief
 *   Makes a connection for the MQTT client subsystem
 *
 * @note
 *   
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_mqtt_setupConnection()
{
    char    debugBuffer[64];
    static  int     delayCnt;

    sprintf (debugBuffer, "MQTT state: %d %d %d\n\r", mqttState, app_uart_uart0_bytesAvailable(), app_uart_uart0_totalReceived());
    DEBUG_OUTPUT_TEXT (debugBuffer);
    switch (mqttState)
    {
        case MQTT_CONN_STATE_INIT:
            // if the modem is not power on we must power it on and wait for it to register
            if (!app_power_modemIsPowered())
            {
                app_power_modemPower_on();
                DEBUG_OUTPUT_TEXT("power on modem\r\n"); 
            }
            delayCnt = 0;
            mqttState = MQTT_CONN_STATE_DELAY_MODEM_POWER;
            break;
        case MQTT_CONN_STATE_DELAY_MODEM_POWER:
            if (++delayCnt >= 5)
                mqttState = MQTT_CONN_STATE_WAIT_MODEM_REGISTER;
            break;
        case MQTT_CONN_STATE_WAIT_MODEM_REGISTER:
            // stay in this state until the modem is registered on the Verizon network
            if (app_modem_isRegistered())
            {
                DEBUG_OUTPUT_TEXT("modem registered\r\n"); 
                app_modem_startDataConnSetup();
                mqttState = MQTT_CONN_STATE_WAIT_DATA_CONNECTION;
            }
            break;
        case MQTT_CONN_STATE_START_DATA_CONNECTION:
            //app_mqtt_init();
            app_modem_startDataConnSetup();
            mqttState = MQTT_CONN_STATE_WAIT_DATA_CONNECTION;
            break;
        case MQTT_CONN_STATE_WAIT_DATA_CONNECTION:
            app_modem_process_tick();
            // do this until connected - dataConnInfo.connState = DATA_CONN_STATE_CONNECTED;
            break;
        case MQTT_CONN_STATE_INIT_CONNECTION:
            break;
        case MQTT_CONN_STATE_WAIT_CONNACK:
            break;
        case MQTT_CONN_STATE_PUBLISH:
            break;
        case MQTT_CONN_STATE_WAIT_PUBACK:
            break;
        case MQTT_CONN_STATE_WAIT_MODEM_POWER_DOWN:
            break;
                
        
                
    }
    
}





