/******************************************************************************
*******************************************************************************
**
**         Filename: Typedefs.h
**    
**           Author: Troy Harstad
**          Created: 04/20/2009
**
**     Last Edit By: Troy Harstad
**        Last Edit: 05/14/09
**    
**         Comments: This header file includes global typedefs and definitions. 
**
** Revision History:
**          4/20/09: First created (Troy Harstad)
**
**
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef HEADER_TYPEDEFS_H
#define HEADER_TYPEDEFS_H
#include <stdint.h>

// typedef signed char         int8_t;
// typedef unsigned char       uint8_t;
// typedef signed int          int16_t;
// typedef unsigned int        uint16_t;
// typedef signed long int     int32_t;
// typedef unsigned long int   uint32_t;
// typedef void *              uptr_t;


typedef void ( * PFN_STATE_VOID_VOID)(void);
typedef void ( * PFN_CALLBACK_VOID_VOID)(void);


typedef union UU16
{
   uint16_t U16;
   int16_t S16;
   uint8_t U8[2];
   int8_t S8[2];
} UU16;

typedef union UU32
{
   uint32_t U32;
   int32_t S32;
   UU16 UU16[2];
   uint16_t U16[2];
   int16_t S16[2];
   uint8_t U8[4];
   int8_t S8[4];
} UU32;



typedef union UU64
{
   uint64_t U64;
   uint32_t U32[2];
   UU32 UU32[2];
   uint16_t U16[4];
   uint8_t U8[8];
} UU64;


//
// Command related typedefs
//
// RF Test TX command structure
typedef struct sTag_COMMAND_RF_TX
{
    uint8_t byLength;
    uint8_t byCommandCode;
    uint8_t byDataType;
    uint8_t byModulation;
    uint8_t byChannel;
    uint8_t byPowerLevel;
    uint8_t byAntenna;
    uint8_t byPA;
    uint16_t wDuration;
    uint8_t byStartDelay;
    uint8_t byPA_Type;
} S_COMMAND_RF_TX;

// RF Test TX Packet command structure
typedef struct sTag_COMMAND_RF_PACKET_TX
{
    uint8_t byLength;
    uint8_t byCommandCode;
    uint8_t byPacketType;
    uint8_t byFrequency;
    uint8_t byHopping;
    uint8_t byAntenna;
    uint8_t byPA;
    uint16_t wNumTransmissions;
    uint8_t byPowerLevel;
    uint16_t wTxIntervalDelay;
    uint8_t byStartDelay;
    uint8_t byPA_Type;
} S_COMMAND_RF_PACKET_TX;

// RF Packet RX command structure
typedef struct sTag_COMMAND_RF_RX
{
    uint8_t byLength;
    uint8_t byCommandCode;
    uint8_t byAckWithRSSI;
    uint8_t byFrequency;
    uint8_t byAntenna;
    uint16_t wDuration;
    uint8_t byStartDelay;
    uint8_t byPA_Type;
} S_COMMAND_RF_RX;

// RF Receiver Test Mode
typedef struct sTag_COMMAND_RF_RXER_TM
{
    uint8_t byLength;
    uint8_t byCommandCode;
    uint8_t byAntenna;
    uint8_t byFrequency;
    uint8_t byPA_Type;
    uint16_t wDuration;
    uint8_t byStartDelay;    
} S_COMMAND_RF_RXER_TM;

// Sleep Seconds command structure
typedef struct sTag_COMMAND_SLEEP_SEC
{
    uint8_t byLength;
    uint8_t byCommandCode;
    uint8_t byNumSeconds;
} S_COMMAND_SLEEP_SEC;

// ARB Read command structure
typedef struct sTag_COMMAND_ARB_READ
{
    uint8_t byLength;
    uint8_t byCommandCode;
    uint16_t wTimesToRead;
    uint8_t byReadInterval;
    uint8_t byRegisterType;
    uint8_t byPlusPulse;
    uint8_t byStartDelay;
} S_COMMAND_ARB_READ;

// Authentication Verify
typedef struct sTag_COMMAND_AUTH_VERIFY
{
    uint8_t byLength;
    uint8_t byCommandCode;
    uint8_t byVerify0;
	uint8_t byVerify1;
	uint8_t byVerify2;
	uint8_t byVerify3;
} S_COMMAND_AUTH_VERIFY;

// Datalog 
typedef struct sTag_COMMAND_DATALOG_START
{
    uint8_t byLength;
    uint8_t byCommandCode;
    uint8_t byDatalogType;
	uint8_t byAntenna;
	uint8_t byStartDelay;
} S_COMMAND_DATALOG_START;

// Manufacturing Data
typedef struct sTag_COMMAND_MANUF_DATA
{
    uint8_t byLength;
    uint8_t byCommandCode;
    uint16_t wFlashAddr;
	uint8_t byNumBytes;
} S_COMMAND_MANUF_DATA;

// Flash Retrieve
typedef struct sTag_COMMAND_RESET_METROLOGY
{
    uint8_t byLength;
    uint8_t byCommandCode;
    uint16_t wResetKey;
} S_COMMAND_RESET_METROLGY;

// MIU Simulator
typedef struct sTag_COMMAND_MIU_SIMULATOR
{
    uint8_t byLength;
    uint8_t byCommandCode;
    uint8_t byOOKPowerLevel;
    uint8_t byOOKPAEnable;
    uint8_t byFSKPowerLevel;
    uint8_t byFSKPAEnable;
    uint8_t byFEM;
    uint8_t byAntenna;
    uint8_t byPacketTypeRatio;
    uint16_t wInterPacketDelay;
    uint32_t dwBaseID;
    uint16_t wVirtualGroups;
    uint8_t byEncryptionMethod;
} S_COMMAND_MIU_SIMULATOR;

// Command Union
typedef union uTag_COMMAND_DATA
{
    uint8_t abyCommandData[20];
    S_COMMAND_RF_TX sCommand_RF_TX;
    S_COMMAND_RF_RX sCommand_RF_RX;
    S_COMMAND_RF_PACKET_TX sCommand_RF_Packet_TX;
    S_COMMAND_SLEEP_SEC sCommand_Sleep_Sec;
    S_COMMAND_ARB_READ sCommand_ARB_Read;
	S_COMMAND_AUTH_VERIFY sCommand_Auth_Verify;
	S_COMMAND_DATALOG_START sCommand_Datalog_Start;
	S_COMMAND_MANUF_DATA sCommand_ManufacturingData;
	S_COMMAND_RESET_METROLGY sCommand_ResetMetrology;
	S_COMMAND_RF_RXER_TM sCommand_RFRXer_TestMode;
    S_COMMAND_MIU_SIMULATOR sCommand_MIU_Simulator;
}U_COMMAND_DATA;


//
// ARB realted typedefs
//
// ARB Data structure
typedef struct sTag_ARB_REGISTER_DATA
{
    uint8_t byRegisterType;                 // Register at autodetect
    uint8_t abyRegisterID[10];              // Register ID at autodetect
    uint8_t byRegisterIDLength;            // Register ID length at autodetect
    uint8_t byDetectedRegisterType;         // Register at latest reading 
	uint8_t abyReading[8];  
    uint8_t abyDetectedRegisterID[10];      // Register ID at latest reading
    uint8_t byDetectedRegisterIDLength;    // Register ID length at latest reading
    uint8_t abyDetectedRegisterIDDecimal[5];
    uint8_t byLeakStatusFlag;
    uint8_t bySpare1;
    uint8_t bySpare2; 
    UU32 dwReadingBinary;
} S_REGISTER_DATA;


//
// Datalog related typedefs
//
// Union and structure for Data Log data (4 bytes)
typedef union uTag_DATALOG_READING
{
	uint8_t abyDataLogReading[4];
	uint32_t dwDataLogReading;
}U_DATALOG_READING;



typedef struct sTag_MIU_CONFIG
{
	uint16_t wARBReadInterval;
    uint8_t byUARTCheckInterval;
	uint16_t wDatalogStoreInterval;
    uint8_t byDatalogType;    
    uint8_t byMobileReadTXInterval;
    uint8_t byMobileReadPowerLevel;
    uint16_t wFNReadTXInterval;
    uint8_t byFNReadPowerLevel;
    uint16_t wFNConfigTXInterval;
    uint8_t byFNConfigPowerLevel;
	uint8_t byCommandRXInterval;
	uint16_t wAuthSessionTimeout;
	uint8_t byUARTCmdTestModeTimeout;
    uint8_t byDatalogTxInterval;
    uint16_t wHPMobileReadTXInterval;
    uint8_t byHPMobileReadPowerLevel;
    uint16_t wAntDetInterval;
    uint8_t byPingRXInterval;
    uint8_t byRfCommandModeDuration;
}S_MIU_CONFIG;


// Structure for Swipe Mode MIU Configuration
typedef struct sTag_MIU_CONFIG_SWIPE
{
	uint16_t wARBReadInterval;
    uint8_t byUARTCheckInterval;
    uint8_t byMobileReadTXInterval;
    uint8_t  byMobileReadPowerLevel;
    uint16_t wFNReadTXInterval;
    uint8_t  byFNReadPowerLevel;
    uint16_t wFNConfigTXInterval;
    uint8_t  byFNConfigPowerLevel; 
    uint8_t byRV4SwipeStateTimer;
    uint8_t byCommandRXInterval;
}S_MIU_CONFIG_SWIPE;

#define MIU_SERVER_ADDRESS_LENGTH_MAX    32
typedef struct sTag_MIU_SERVER_CONFIG
{
	uint32_t dwServerPortNumber;
    uint8_t  abyServerAddress[MIU_SERVER_ADDRESS_LENGTH_MAX];
}S_MIU_SERVER_CONFIG;


// Union for Normal Mode MIU Config
typedef union uTag_MIU_CONFIG_NORMAL
{	
	uint8_t abyMIUConfig[sizeof(S_MIU_CONFIG)];
	S_MIU_CONFIG sMIUConfigNormal;
}U_MIU_CONFIG_NORMAL;


// Union for Swipe Mode MIU Config
typedef union uTag_MIU_CONFIG_SWIPE
{	
	uint8_t abyMIUConfigSwipe[sizeof(S_MIU_CONFIG_SWIPE)];
	S_MIU_CONFIG_SWIPE sMIUConfigSwipe;
}U_MIU_CONFIG_SWIPE;

typedef struct sTag_MIU_IMG_INFO
{
    uint16_t dontCare;
    uint8_t versionMajor;
    uint8_t versionMinor;
    uint32_t versionYearMonthDay;
    uint32_t versionBuild;
    uint32_t imageCrc32;
} S_MIU_IMG_INFO;

// Union for MIU Server Config
typedef union uTag_MIU_SERVER_CONFIG
{	
	uint8_t abyMIUServerConfig[sizeof(S_MIU_SERVER_CONFIG)];
	S_MIU_SERVER_CONFIG sMIUServerConfig;
}U_MIU_SERVER_CONFIG;

//
// Encryption
//
// Structure for Encryption
typedef struct sTag_ENCRYPTION
{
	uint8_t byNumberOfBlocks;
    uint8_t byEncryptMethod;    
    uint8_t byKeyNumber;
	uint8_t byPacketToken;
    uint8_t bySequenceNumber;
    uint8_t *pabyPlainText;
    uint8_t *pabyCipherText; 
}S_ENCRYPTION;


//
// Packet Structure
//
// Structure for Parsed Alert Packet
typedef struct sTag_PACKET_ALERT
{
	uint8_t byLength;
    uint8_t byPacketType;
	uint8_t bySpare;
    UU16    wCountDownValue;   
}S_PACKET_ALERT;

// Structure for Parsed Command Packet
typedef struct sTag_PACKET_COMMAND
{
	uint8_t byLength;
    uint8_t byPacketType;
    uint8_t byNetwork;    
	uint8_t bySequenceNumber;
    UU32    dwID;
    uint8_t byCommand;
    uint8_t abyArguments[11];
    uint8_t byIDByte;    
    uint8_t byPacketToken;
    uint8_t byKeyNumber;
    uint8_t byEncryptionMethod;
    UU16    wSpare;    
}S_PACKET_COMMAND;

// Structure for All RX Packets
typedef struct sTag_PACKET_RX
{
	uint8_t byPacketError;
    uint8_t byPacketType;
    S_PACKET_ALERT sPacket_Alert;
    S_PACKET_COMMAND sPacket_Command;   
}S_PACKET_RX;

// Structure for All FSK TX Packets
typedef struct sTag_PACKET_TX_FSK_PLAINTEXT_FIELDS
{
	uint8_t byLength;
    uint8_t byPacketType;
    uint8_t byNetwork;
    uint8_t bySequenceNumber;
    uint8_t byAge;
    uint8_t bySpareBit;
    uint8_t byPacketToken;
    uint8_t byKeyBits;
    uint8_t byEncryptionMethod;
    uint8_t byConnectedDevice;
    uint8_t bySpareBits;
    UU32 dwCRC32;
}S_PACKET_TX_FSK_PLAINTEXT_FIELDS;


// Structure for AppConfiguration constants, Version and CRC
typedef struct sTag_IMG_INFOBLK
{
	uint16_t wVersion;
	uint32_t dwCRC;
}S_IMG_INFOBLK;

// Structure for MIU ID so each byte can be accessed separately.
typedef struct sTag_MIU_ID
{
	uint8_t byMiuId0;
	uint8_t byMiuId1;
	uint8_t byMiuId2;
	uint8_t byMiuId3;
}S_MIU_ID;

// Union for MIU ID it can be accessed as a structure or an array of bytes
typedef union uTag_MIU_ID
{
	uint32_t dwMiuId;	
	uint8_t abyMiuId[sizeof(S_MIU_ID)];
	S_MIU_ID sMiuId;
}U_MIU_ID;	





//-----------------------------------------------------------------------------
// BIT defines added by Troy Harstad
//-----------------------------------------------------------------------------
#define BIT0       0x0001         
#define BIT1       0x0002 
#define BIT2       0x0004 
#define BIT3       0x0008 
#define BIT4       0x0010 
#define BIT5       0x0020 
#define BIT6       0x0040 
#define BIT7       0x0080
#define BIT8       0x0100
#define BIT9       0x0200
#define BITA       0x0400
#define BITB       0x0800
#define BITC       0x1000
#define BITD       0x2000
#define BITE       0x4000
#define BITF       0x8000





// Get low byte from word
#define LOBYTE(wordIn)  	((wordIn)&0xFF)

// Get high byte from word
#define HIBYTE(wordIn)		(((wordIn)>>8)&0xFF)

// Get low word from dword
#define LOWORD(dwordIn) 	((dwordIn)&0xFFFF)

// Get high word from dword
#define HIWORD(dwordIn)		(((dwordIn)>>16)&0xFFFF)

// Combine two bytes to make a word
#define MAKEWORD(loByte,hiByte)  (((((uint16_t)(hiByte))&0xFF)<<8)|((loByte)&0xFF))

// Combine two words to make a dword
#define MAKEDWORD(loWord,hiWord)  (((((uint32_t)(hiWord))&0xFFFF)<<16)|((loWord)&0xFFFF))

// Combine two bytes to make one byte, where each byte has their lower 4-bits (nibble) filled. 
#define MAKEBYTE(loByte,hiByte)		(((hiByte)<<4) | (loByte))

//-----------------------------------------------------------------------------


// #define TRUE  	1
// #define FALSE   0

#define PASS 	1
#define FAIL 	0

  



// CRYSTAL_ROBUSTNESS_TEST -- TEST PURPOSES ONLY
// Ff this is defined the RTC module is setup such that the 32.768 crystal 
// is set as the system clock then routed to P1.6 for duty cycle 
// measurement.  This is used to determine the robustness of the crystal setup.
// This affects the normal operation of the R900iV2
//#define CRYSTAL_ROBUSTNESS_TEST

// DEBUG MODE - Used to get verbose output from the UART that is conditionally compiled
// If this is defined, the UART will output from either of the 2 defined macros.
// DEBUG_OUTPUT_TEXT is for text, i.e. "This will get output on the UART"
// DEBUG_OUTPUT_DATA is for raw data bytes, i.e. &byDataByte, 1 - will output byDataByte to the UART 
#define DEBUG_MODE  
#ifdef DEBUG_MODE
#define DEBUG_OUTPUT_TEXT(c) Uart_Uart1WriteString(c)
//#define DEBUG_OUTPUT_DATA(p, l) UART0_TX_Buffer_Load_MessageData(p, l)  // Still needs supported
#define DEBUG_OUTPUT_TEXT_AND_DATA(c, p, l) Uart_Uart1WriteStringAndData(c, p, l)
#define DEBUG_CALL_FUNCTION(function) function() 
#define DEBUG_INSTRUCTION(instruction) instruction
#define DEBUG_OUTPUT_DATA(p, l)
#define DEBUG_WAITFORUART()  while(!Uart_Uart1_TxComplete())
    
    
#else
#define DEBUG_OUTPUT_TEXT(c) 
#define DEBUG_OUTPUT_DATA(p, l)
#define DEBUG_OUTPUT_TEXT_AND_DATA(c, p, l)
#define DEBUG_WAITFORUART()
#define DEBUG_CALL_FUNCTION(function) 
#define DEBUG_INSTRUCTION(instruction)
#endif


#endif

