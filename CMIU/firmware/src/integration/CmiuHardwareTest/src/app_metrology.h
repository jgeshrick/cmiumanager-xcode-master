/******************************************************************************
*******************************************************************************
**
**         Filename: app_metrology.h
**    
**           Author: Troy Harstad
**          Created: 08/04/2009
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: This is the header file for Ecoder.c source file.  
**					 Only externally accessible functions are prototyped here.
**
** Revision History:
**          8/04/09: First created (Troy Harstad)
**
**
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.
**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef HEADER_APP_METROLOGY_H
#define HEADER_APP_METROLOGY_H


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/

// Define possible values of rate direction (byRateFlowDirection) for use when displaying arrows on LCD
#define FLOW_DIRECTION_POSITIVE 	0
#define FLOW_DIRECTION_NEGATIVE		1
#define FLOW_DIRECTION_NO_FLOW		2

// Define possible values that are returned by Ecoder_Interval_Handler
#define ECODER_ERROR	1
#define ECODER_NO_ERROR	0

// Key to reset the metrology flags to be used with app_metrology_Clear_FlagsAndLogs()
#define METROLOGY_RESET_KEY	0x1986

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_metrology_Init(void);
extern void app_metrology_InitHW(void);

extern uint8_t app_metrology_Clear_FlagsAndLogs(uint16_t wKey);

extern void app_metrology_CalculateFlags(void);
extern bool app_metrology_BackflowFlags_Clear(void);

extern uint8_t app_metrology_NoFlowDaysFlags_Retrieve(void);
extern uint8_t app_metrology_LeakFlags_Retrieve(void);
extern uint8_t app_metrology_BFMinAge_Retrieve(void);
extern uint8_t app_metrology_BFMaxAge_Retrieve(void);
extern uint8_t app_metrology_24Hr_Backflow_Retrieve(void);
extern uint8_t app_metrology_24HourTimer_Retrieve(void);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// extern union 
// {
//    uint8_t buf[20];
//    UU64 in;
// }  U_b;  


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif
