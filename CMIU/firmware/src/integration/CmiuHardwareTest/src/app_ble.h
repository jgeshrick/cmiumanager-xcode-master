/******************************************************************************
*******************************************************************************
**
**         Filename: Bluetooth.h
**    
**           Author: Barry Huggins
**          Created: 12/31/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**		May142015-jef: added app_ble_get_address()
**		May122015-jef: added jefs_ble_process()
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __BLUETOOTH_H
#define __BLUETOOTH_H

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/






/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
bool app_ble_get_address(void);
uint32_t jefs_ble_process(bool advertise, bool verbose);
void app_ble_init(void);
void app_ble_startup(void);
void app_ble_power_on(void);
void app_ble_power_off(void);
void app_ble_sendEcho (void);
bool app_ble_bluetoothInit(void);
bool app_ble_process(void);
bool app_ble_sendData (uint8_t  *dataPtr,  uint8_t   size);
bool app_ble_getBattery (void);
bool app_ble_getTemperature (void);
bool app_ble_radioIsActive(void);
bool app_ble_initialized(void);



/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None

#endif // __BLUETOOTH_H

