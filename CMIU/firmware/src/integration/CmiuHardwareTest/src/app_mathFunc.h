
/******************************************************************************
*******************************************************************************
**
**         Filename: MathFunc.h
**    
**           Author: Troy Harstad
**          Created: 06/03/2009
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: This is the header file for MathFunc.c source file.  Only
**                   externally accessible functions are prototyped here.
**
** Revision History:
**          6/03/09: First created (Troy Harstad)
**
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.
**
**
******************************************************************************
******************************************************************************/
#include "stdint.h"
#include "typedefs.h"

// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef HEADER_MATHFUNC_H
#define HEADER_MATHFUNC_H


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void MathFunc_Mult32x32(UU32 mc, UU32 mp, UU64 *pd);
extern void MathFunc_AsciiToHex(uint8_t *pbyVal);
extern void MathFunc_HexToAscii(uint8_t *pbyVal);
extern void MathFunc_HexToEvenAscii(uint8_t *pbyVal);
extern void MathFunc_HexToEven(uint8_t *pbyVal);
extern uint16_t MatchFunc_DivideWithRound(uint16_t wDividend, uint16_t wDivisor);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// Declare a union to use inside of the bin2bcd module.  It is delcared here
// because this is the place it is used and you can't declare it inside of the 
// bin2bcd assembly file.
// Used when converting the  binary reading to BCD for display on LCD
extern union 
{
   uint8_t buf[20];
   UU64 in;
}  U_b; 


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None

#endif

