//#pragma SRC(Ecoder.ASM)
/******************************************************************************
*******************************************************************************
**
**         Filename: app_metrology.c
**    
**           Author: Nick Sinas
**          Created: 04/19/2013
**    
**      Description: 
**
**	    Code Review:
**
**    Copyright 2013 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/     
#include <stdint.h>
#include <stdbool.h>
#include "typedefs.h"
#include "app_metrology.h"
//#include "ADC.h"
//#include "Configuration.h"
#include "app_error.h"
//#include "Crossbar.h"
//#include "Timer.h"
//#include "Power.h"
#include "app_arb.h"
#include "app_uart.h"
//#include "UART.h"
//#include "Command.h"
 
/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/

// Define the possible values of CSIN located in virtual memory W7 bit 2
// Bit 2 is set if the net count is positive and cleared if negative
// For this to be used CSIN has to be shift right twice so it is the 
// least significant bit.
#define NETCOUNT_SIGN_POSITIVE	0
#define NETCOUNT_SIGN_NEGATIVE 	1


// Define the possible values of bLeakDay
#define LEAK_DAY_TRUE 	1
#define LEAK_DAY_FALSE  0

// Define the possible values of bNoFlowDay
#define NOFLOW_DAY_TRUE  	1
#define NOFLOW_DAY_FALSE 	0
 
// Define the possible values of bPrevNetCntSign and bNetCntSign
#define POSITIVE	0
#define NEGATIVE	1 

// Flow direction is not valid until 2 ASIC reads have occurred
#define FLOW_DIRECTION_VALID	2

// Define bits in sASIC_Data.wFlags
#define PREV_NET_CNT_SIGN_BIT       BIT0        //bPrevNetCntSign 
//Bits 8-9 Table
// 00: Leak icon off
// 01: Leak icon flashing
// 10: Leak icon on solid
// 11: Undefined
#define LEAK_CURRENT_BIT_0          BIT1        
#define LEAK_CURRENT_BIT_1          BIT2

//uMeterConfig.sMeterConfig.dwBFThreshMax 
#define BFTHRESH_MAX	100
#define BFTHRESH_MIN	1

//uMeterConfig.sMeterConfig.byLeakDayThresh
#define LEAKDAYTHRESH	50

//uMeterConfig.sMeterConfig.dwFlowThresh
#define FLOWTHRESH		1


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static void app_metrology_CalculateChange(void);
static void app_metrology_LeakCheck(void);
static void app_metrology_ShiftFlowLog(bool bFlowStatus);
static void app_metrology_ShiftNoFlowLog(bool bNoFlowStatus);
static void app_metrology_ShiftLeakLog(bool bLeakStatus);
static void app_metrology_ShiftSmallBackFlowLog(bool bFlowStatus);
static void app_metrology_ShiftLargeBackFlowLog(bool bFlowStatus);
static void app_metrology_CountFlowIntervals(void);
static void app_metrology_BackFlowCheck(void);
static void app_metrology_Leak35Day(void);
static void app_metrology_Check_24_Hour(void);
static void app_metrology_CountNoFlowDays(void);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
 uint32_t dwNetCount;	 	// Absolute value of (ASIC UpCounter - DownCounter)
 uint32_t dwChangeCount;	// Absolute value of (NetCount - PrevNetCount)
 uint8_t  abyVolume[20];	// 20 bytes, 20-digit BCD value in
								// the form 9999999999999999999
 uint8_t byFlowIntervals; 	// The number of 1's in FlowLog, ie. the number of
								// 15 minute intervals with water flowing in the
								// last 24 hours.
 uint8_t byNoFlowDays;		// The total number of consecutive days with No Flow
								// out of the last 35 days.

 uint8_t byLeakDays;		// The total number of days that the leak detection
								// current state of 01 was detected during the last
								// 35 days.


//
//
// Added to replace ASIC
//

//uASIC_Data.sASIC_Data.abyFlowLog[byIndex]
 uint8_t abyFlowLog[12];

//uASIC_Data.sASIC_Data.abyLeakLog
 uint8_t abyLeakLog[6];

//uASIC_Data.sASIC_Data.abyNoFlowLog
 uint8_t abyNoFlowLog[6];

//uASIC_Data.sASIC_Data.abySmallBackFlowLog
 uint8_t abySmallBackFlowLog[12];

//uASIC_Data.sASIC_Data.abyLargeBackFlowLog
 uint8_t abyLargeBackFlowLog[12];

//uASIC_Data.sASIC_Data.byA24HrTmr
 uint8_t byA24HrTmr;

//uASIC_Data.sASIC_Data.dwPrevNetCount
 uint32_t dwPrevNetCount;

//uASIC_Data.sASIC_Data.wFlags
 uint16_t wFlags;

//uASIC_Data.sASIC_Data.byBFMaxAge;
 uint8_t byBFMaxAge;

//uASIC_Data.sASIC_Data.byBFMinAge;
 uint8_t byBFMinAge;

// Register Data structure
 S_REGISTER_DATA sARBReadingData;


// Flags - Same as R900i Flags, which were orignally bits.
// The ones listed here are not stored back into ASIC
bool bNetCntSign;
bool bChangeCntSign;
bool bLeakDay;
bool bNoFlow;
bool bReverseFlow; 


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h





/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// LeakDaysTab is a table to look up the 3-bit value for the Days of Leak
// Bits based on the value of LeakDays, which has a range of 0 to 35.
static const uint8_t abyLeakDaysTable[36] =
{
	0x00,           	// Value for LeakDays =  0.
	0x04,           	// Value for LeakDays =  1.
	0x04,          		// Value for LeakDays =  2.
	0x08,           	// Value for LeakDays =  3.
	0x08,           	// Value for LeakDays =  4.
	0x08,          		// Value for LeakDays =  5.
	0x08,          		// Value for LeakDays =  6.
	0x08,         		// Value for LeakDays =  7.
	0x0C,          		// Value for LeakDays =  8.
	0x0C,         		// Value for LeakDays =  9.
	0x0C,          		// Value for LeakDays = 10.
	0x0C,          		// Value for LeakDays = 11.
	0x0C,          		// Value for LeakDays = 12.
	0x0C,         		// Value for LeakDays = 13.
	0x0C,         		// Value for LeakDays = 14.
	0x10,         		// Value for LeakDays = 15.
	0x10,         		// Value for LeakDays = 16.
	0x10,         		// Value for LeakDays = 17.
	0x10,           	// Value for LeakDays = 18.
	0x10,    			// Value for LeakDays = 19.
	0x10,       		// Value for LeakDays = 20.
	0x10,           	// Value for LeakDays = 21.
	0x14,         		// Value for LeakDays = 22.
	0x14,          		// Value for LeakDays = 23.
	0x14,         		// Value for LeakDays = 24.
	0x14,           	// Value for LeakDays = 25.
	0x14,           	// Value for LeakDays = 26.
	0x14,          		// Value for LeakDays = 27.
	0x14,           	// Value for LeakDays = 28.
	0x14,           	// Value for LeakDays = 29.
	0x14,           	// Value for LeakDays = 30.
	0x14,           	// Value for LeakDays = 31.
	0x14,           	// Value for LeakDays = 32.
	0x14,           	// Value for LeakDays = 33.
	0x14,           	// Value for LeakDays = 34.
	0x18           		// Value for LeakDays = 35.
};

// NoFlowDaysTab is a table to look up the 3-bit value for the Consecutive Days of No Flow
// Bits based on the value of NoFlowDays, which has a range of 0 to 35.
static const uint8_t abyNoFlowDaysTable[36] =
{
	0x00,           	// Value for NoFlowDays =  0.
	0x04,           	// Value for NoFlowDays =  1.
	0x04,          		// Value for NoFlowDays =  2.
	0x08,           	// Value for NoFlowDays =  3.
	0x08,           	// Value for NoFlowDays =  4.
	0x08,          		// Value for NoFlowDays =  5.
	0x08,          		// Value for NoFlowDays =  6.
	0x08,         		// Value for NoFlowDays =  7.
	0x0C,          		// Value for NoFlowDays =  8.
	0x0C,         		// Value for NoFlowDays =  9.
	0x0C,          		// Value for NoFlowDays = 10.
	0x0C,          		// Value for NoFlowDays = 11.
	0x0C,          		// Value for NoFlowDays = 12.
	0x0C,         		// Value for NoFlowDays = 13.
	0x0C,         		// Value for NoFlowDays = 14.
	0x10,         		// Value for NoFlowDays = 15.
	0x10,         		// Value for NoFlowDays = 16.
	0x10,         		// Value for NoFlowDays = 17.
	0x10,           	// Value for NoFlowDays = 18.
	0x10,    			// Value for NoFlowDays = 19.
	0x10,       		// Value for NoFlowDays = 20.
	0x10,           	// Value for NoFlowDays = 21.
	0x14,         		// Value for NoFlowDays = 22.
	0x14,          		// Value for NoFlowDays = 23.
	0x14,         		// Value for NoFlowDays = 24.
	0x14,           	// Value for NoFlowDays = 25.
	0x14,           	// Value for NoFlowDays = 26.
	0x14,          		// Value for NoFlowDays = 27.
	0x14,           	// Value for NoFlowDays = 28.
	0x14,           	// Value for NoFlowDays = 29.
	0x14,           	// Value for NoFlowDays = 30.
	0x14,           	// Value for NoFlowDays = 31.
	0x14,           	// Value for NoFlowDays = 32.
	0x14,           	// Value for NoFlowDays = 33.
	0x14,           	// Value for NoFlowDays = 34.
	0x18           		// Value for NoFlowDays = 35.
};


/******************************************************************************
**
**            Filename: app_metrology.c
**        Routine Name: app_metrology_InitHW
**    
**              Author: Troy Harstad
**             Created: 08/02/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function initializes the hardware used within the 
**                      Ecoder.c source file.  
**
******************************************************************************/
void app_metrology_InitHW(void)
{  	
}


/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_Init
**    
**              Author: Troy Harstad
**             Created: 08/02/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function intializes the software for the Ecoder.c
**						source file. 
**
******************************************************************************/
void app_metrology_Init(void)
{
	uint16_t wInitResetKey;


    // Clear the flags and logs
    wInitResetKey = METROLOGY_RESET_KEY;
    app_metrology_Clear_FlagsAndLogs(wInitResetKey);
    wInitResetKey = 0xFFFF;
    DEBUG_OUTPUT_TEXT("Flags cleared\r\n");
}


/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_Clear_FlagsAndLogs
**    
**              Author: Nick Sinas
**             Created: 04/22/2013
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
**    Function Returns: TRUE if successful
**                      FALSE if not successful (key does not match)
**
**         Description: This function intializes the logs and variables that
**						were previously handled by the ASIC
**
******************************************************************************/
uint8_t app_metrology_Clear_FlagsAndLogs(uint16_t wKey)
{
	uint8_t byIndex;

	if(wKey != METROLOGY_RESET_KEY)
	{
		return false;
	}

	byFlowIntervals = 0;
	byNoFlowDays = 0;	
	byLeakDays = 0;	
	
	// Bit Variables
	bLeakDay = LEAK_DAY_FALSE;
	bNoFlow = NOFLOW_DAY_FALSE;

	// Clear the flags
	wFlags = 0;

	dwPrevNetCount = 0x55D4A80; // This is set to 90000000 per RVFOUR-134 to help with backflow flags getting set
	dwNetCount = 0;
	dwChangeCount = 0;
	bChangeCntSign = POSITIVE;
	bReverseFlow = false;

	byA24HrTmr = 96;
	byBFMaxAge = 36;
	byBFMinAge = 36;

	// Clear out all logs
	for(byIndex = 0; byIndex < 12; byIndex++)
	{
		 abyFlowLog[byIndex] = 0;
		 abySmallBackFlowLog[byIndex] = 0;
		 abyLargeBackFlowLog[byIndex] = 0;
	}
	for(byIndex = 0; byIndex < 6; byIndex++)
	{
		 abyLeakLog[byIndex] = 0;
		 abyNoFlowLog[byIndex] = 0;
	}

	// No flog log inialized to non-zero first index
	// to prevent a no flow day appearing immediately
	abyNoFlowLog[0] = 0x02;

	wKey = 0xFFFF;

	return true;
}


/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_CalculateFlags
**    
**              Author: Nick Sinas
**             Created: 04/22/2013
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function calculates the change based on the reading
**						read from teh register. It then calculates the flow
**						direction and if there was backflow.
**						The leak and backflow flags are then calulated and tracked
**						in logs.
**
******************************************************************************/
void app_metrology_CalculateFlags(void)
{        	
    uint16_t wInitResetKey;
    
//     #ifdef DEBUG_MODE
// 	 uint8_t abyMetrologyData[23];
// 	 uint8_t byIndex;
// 	 uint8_t byCount;
//     #endif
	
	// Retrieve Reading
	sARBReadingData = *ARB_RegisterData_Retrieve();
    
    // Check if the detected register was a ProRead or ARBV, E-Coder
    // flags are not calculated for these register types.  The E-Coder 
    // flags are also cleared here in that case.
    if((sARBReadingData.byDetectedRegisterType == REGISTER_ARBV) ||
       (sARBReadingData.byDetectedRegisterType == REGISTER_PROREAD))
    {
        // Clear the flags and logs
        wInitResetKey = METROLOGY_RESET_KEY;
        app_metrology_Clear_FlagsAndLogs(wInitResetKey);
        wInitResetKey = 0xFFFF;
        
        DEBUG_OUTPUT_TEXT_AND_DATA("Detected Register Type: ", &sARBReadingData.byDetectedRegisterType, 1);
        DEBUG_OUTPUT_TEXT_AND_DATA("Reading: ", &sARBReadingData.dwReadingBinary.U8[0], 4);
        
        // Return here so that E-Coder flags are not calculated
        return;       
    }
         

	// Set reading to NetCount
	dwNetCount = sARBReadingData.dwReadingBinary.U32;
    
    DEBUG_OUTPUT_TEXT_AND_DATA("Detected Register Type: ", &sARBReadingData.byDetectedRegisterType, 1);
    DEBUG_OUTPUT_TEXT_AND_DATA("Reading: ", &sARBReadingData.dwReadingBinary.U8[0], 4);
    
	// Calculate Change
	app_metrology_CalculateChange();

	// Leak Check
	app_metrology_LeakCheck();

	// Backflow Check
	app_metrology_BackFlowCheck();

	// Leak 35 Day
	app_metrology_Leak35Day();

	// Count No Flow Days
	app_metrology_CountNoFlowDays();

	// Check 24 Hour
	app_metrology_Check_24_Hour();
	
// ****************************************************************************	
// #ifdef DEBUG_MODE

//     
// 	// DEBUG FOR TESTING !!!!
// 	//
// 	// Build Metrology Packet and Output
// 	//

// 	byIndex = 0;
// 	
// 	// Register Type
// 	abyMetrologyData[byIndex++] =  sARBReadingData.byRegisterType;
// 	
// 	// 24 Hour Timer
// 	abyMetrologyData[byIndex++] =  96 - byA24HrTmr;

// 	// Reading
// 	for(byCount = 0; byCount < 8; byCount++)
// 	{
// 		abyMetrologyData[byIndex++] = sARBReadingData.abyReading[byCount];
// 	}

// 	// ID
// 	for(byCount = 0; byCount < 10; byCount++)
// 	{
// 		abyMetrologyData[byIndex++] = sARBReadingData.abyDetectedRegisterID[byCount];
// 	}


// 	// Leak Flags 
// 	abyMetrologyData[byIndex++] = app_metrology_LeakFlags_Retrieve();

// 	// No Flow Flags
// 	abyMetrologyData[byIndex++] = app_metrology_NoFlowDaysFlags_Retrieve();

// 	// Backflow Flags
// 	if(app_metrology_BFMaxAge_Retrieve() <= 35)
// 	{
// 		abyMetrologyData[byIndex++] = BIT1;
// 	}
// 	else if(app_metrology_BFMinAge_Retrieve() <= 35)
// 	{
// 		abyMetrologyData[byIndex++] = BIT0;
// 	}
// 	else
// 	{
// 		abyMetrologyData[byIndex++] = 0;
// 	}

// 	// Ouput
// /////////	Command_SendInfo_Metrology(&abyMetrologyData[0], byIndex);
// #endif
// 	// END DEBUG !!!!
// // ****************************************************************************	
	
}


/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_CalculateCount
**    
**              Author: Nick Sinas
**             Created: 04/22/2013
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function calculates the change based on the 
**						current reading. The reading direction must first be 
**						calculated, then the reading change.
**						Once these are calculated, the flow direction must be 
**						determined. This is accomplished by comparing the 
**						reading change to 50,000,000 which is halfway, to the 
**						maximum possible change. This will tell us the true
**						change count sign and if there was reverse flow.
**						
**
******************************************************************************/
void app_metrology_CalculateChange(void)
{	
	// Check if current reading is greater than previous
	if(dwNetCount >= dwPrevNetCount)
	{
		// Check if no flow
		if(dwNetCount == dwPrevNetCount)
		{
			// No flow
			dwChangeCount = 0UL;
			bChangeCntSign = POSITIVE;
			bReverseFlow = false;
		}
		else
		{
			// Reading change 'Up'
			dwChangeCount =  dwNetCount - dwPrevNetCount;
			bChangeCntSign = POSITIVE;
			bReverseFlow = false;

			// Check if change was greater than or equal to 50,000,000
			if(dwChangeCount >= 50000000UL)
			{
				 // Reverse flow
				 dwChangeCount = ((dwPrevNetCount + 100000000UL) - dwNetCount);
				 bChangeCntSign = NEGATIVE;
				 bReverseFlow = true;
			}
		}
	}
	else //  Current < Previous
	{
		// Reading change 'Down'
		dwChangeCount =  dwPrevNetCount - dwNetCount;
		bChangeCntSign = NEGATIVE;
		bReverseFlow = true;

		// Check if the change was greater than 50,000,000
		if(dwChangeCount >= 50000000UL)
		{
			// Forward Flow
			dwChangeCount = ((dwNetCount + 100000000UL) - dwPrevNetCount);
			bChangeCntSign = POSITIVE;
			bReverseFlow = false;
		}
	}

	// Set PrevNetCount to equal current net count
	dwPrevNetCount = dwNetCount;

}


/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_Check_24_Hour
**    
**              Author: Troy Harstad
**             Created: 10/08/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: NONE
**    Function Returns: NONE
**
**         Description: This function is called every 15 minutes.  On entry
**						a 24 hour timer counter (byA24HrTmr) is decremented.
** 						Once this counter value reaches zero that means it 
**						is time to perform the once-every-24-hours flag checks,
**						which are then performed.
**						
**						This function requires the Ecoder Interval to be 15-minutes.
**
******************************************************************************/
static void app_metrology_Check_24_Hour(void)
{
	// Decrement 24 hour timer
	byA24HrTmr--;

	// If 24 hour time is complete perform additional checks
	if (byA24HrTmr > 0)
	{
		// If the byA24HrTmr hasn't reached zero yet then it is not time to
		// perform the 24 hour Ecoder flag checks.
		return;
	}	
		
	// Reset 24 hour time back to 96, since 96 15-minute intervals equals
	// 24 hours.
	byA24HrTmr = 96;	


	
// Flow intervals would have just been updated during 15-minute check	
// Test for Leak Day -- Set bLeakDay if the byFlowIntervals is larger or equal
// to the threshold.
	if (byFlowIntervals >= LEAKDAYTHRESH)
	{
	 	bLeakDay = LEAK_DAY_TRUE;
	}
	
	// Log current status of bLeakDay then clear it.
	app_metrology_ShiftLeakLog(bLeakDay);
	bLeakDay = LEAK_DAY_FALSE;

// Test for NoFLow -- If no interval has flow over the entire 96 intervals, 
// then set the NoFlow flag. 
	if(byFlowIntervals == 0)
	{
	 	bNoFlow = NOFLOW_DAY_TRUE;
	}

	// Log current status of bNoFlow then clear it.
	app_metrology_ShiftNoFlowLog(bNoFlow);
	bNoFlow = NOFLOW_DAY_FALSE;	

	// Update the 35-day no flow count
	app_metrology_CountNoFlowDays();

	// Update the 35-day leak count
	app_metrology_Leak35Day();


	// Increment the age of the last backflow > BFThreshMax by 1 day
	if (byBFMaxAge < 0xFF)
	{
		byBFMaxAge++;
	}
 	
	// Increment the age of the last backflow > BFThreshMin by 1 day
  	if (byBFMinAge < 0xFF)
	{
		byBFMinAge++;
	}

}



/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_LeakCheck
**    
**              Author: Troy Harstad
**             Created: 09/23/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
** 
**    Function Returns: None
**
**         Description: This function updates the current leak status flags.
**						
**						Every Ecoder interval, the change in counts is compared to a 
**						threshold value in order to determine if water is flowing
**						or not flowing.  If water is flowing, a 1 is shifted into 
**						the FlowLog.  If water is not flowing, a 0 is shifted
**   					into the FlowLog.  Then the number of 1 bits in the 
**						FlowLog is counted and stored in FlowIntervals.  
**						FlowIntervals is used by other routines to set the
**						appropriate leak flags.  Also, the LeakDay Flag is set 
**						if a LeakDay state is detected.  This flag is logged 
**						in the LeakLog once ever 24 hours.
**
******************************************************************************/
static void app_metrology_LeakCheck(void)
{
	bool bFlowStatus;	

	if (bChangeCntSign == NEGATIVE)
	{
		// Negative flow is not considered to be a leak
		// Clear bit and shift into flow log
		bFlowStatus = 0;
	}

	else // (bChangeCntSign == POSITIVE)
	{
	// First compare dwChangeCount to Flow threshold.
		if(dwChangeCount > FLOWTHRESH)
		{
		 	// Set bFlowStatus since DID meet flow threshold
			bFlowStatus = 1;
		}

		else
		{
		 	// Clear bFlowStatus since did not meet flow threshold
			bFlowStatus = 0;
		}
	}

	// Shift flow log
	app_metrology_ShiftFlowLog(bFlowStatus);

	// Calculate the new value for FlowIntervals
	app_metrology_CountFlowIntervals();


// Set the Leak Current bits

	// Use byFlowIntervals to determine if we had a a minor, major, or no leak day
	if (byFlowIntervals < 50)
	{
		// No leak so clear both bits
        wFlags &= ~LEAK_CURRENT_BIT_0;
        wFlags &= ~LEAK_CURRENT_BIT_1; 

    }

	else if ((byFlowIntervals <= 95) & (byFlowIntervals >= 50))
	{
		// Minor leak so set bit 0 and clear bit 1
        wFlags |= LEAK_CURRENT_BIT_0;
        wFlags &= ~LEAK_CURRENT_BIT_1;
        
	}

	else // byFlowIntervals == 96
	{
	 	// Major leak so clear bit 0 and set bit 1 
        wFlags &= ~LEAK_CURRENT_BIT_0;
        wFlags |= LEAK_CURRENT_BIT_1;  
	}
}




/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_BackFlowCheck
**    
**              Author: Troy Harstad
**             Created: 09/29/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
** 
**    
**    Function Returns: None
**
**         Description: This function updates the back flow status flags.
**
**						Every 15 minutes the ChangeCounts is compared to the
**						back flow threshold values in order to determine
**						the state of Back Flow.  The age of the corresponding
**						BackFlow state is set to 0 days if the threshold is met.
**
******************************************************************************/
static void app_metrology_BackFlowCheck(void)
{
	 uint8_t bySmallBF;
	 uint8_t byLargeBF;
	
	// Clear the BF Shift flags
	bySmallBF = 0;
	byLargeBF = 0;

	// If the change in counts is positive then simply return.  No need to
	// check the amount of backflow if flow was positive
	if(bChangeCntSign == POSITIVE)
	{
	 	// Shift in zeros into the log as the flow is postive
		app_metrology_ShiftSmallBackFlowLog(bySmallBF);
		app_metrology_ShiftLargeBackFlowLog(byLargeBF);

		return;
	}

	// To get here, byChangeCntSign must be NEGATIVE.  Since there was
	// negative flow this interval we need to check if it fell within our
	// backflow min and max thresholds and set flags accordingly if it did.

	// Check if dwChangeCount was bigger than the MAX Back Flow threshold
	if(dwChangeCount > BFTHRESH_MAX)
	{
		// Clear BackFlow Min and Max age since back flow exceeded both
		// thresholds.
	 	byBFMinAge = 0;
		byBFMaxAge = 0;
		// Update the small and large backflow log
		bySmallBF = 1;
		byLargeBF = 1;
				
	}

	// Check if dwChange Count fell between the MAX and MIN Back Flow thresholds.
    else if((dwChangeCount <= BFTHRESH_MAX) &&
			(dwChangeCount > BFTHRESH_MIN))
	{
	 	// Clear BackFlow Min since back flow exceeded the MIN threshold
		byBFMinAge = 0;
		// Update the small blackflow log
		bySmallBF = 1;
	}

	// Else dwChangeCount is less than either of the thresholds so just shift
	// to update the log
	app_metrology_ShiftSmallBackFlowLog(bySmallBF);
	app_metrology_ShiftLargeBackFlowLog(byLargeBF);
}



/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_ShiftFlowLog
**    
**              Author: Troy Harstad
**             Created: 09/29/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
** 
**    
**    Function Returns: None
**
**         Description: This function accepts the current Flow status bit and 
**						shifts it into the flow log.
**
**						byFlowLog[] contains 12-bytes, each bit represents whether 
**						flow occured for a given 15-minute interval.
**						abyFlowLog[0]  (MSbit) contains the oldest flow status bit.
**						abyFlowLog[11] (LSbit) contains the newest flow status bit.
**						abyFlowLog[] is used to determine Leak Status based on how many 15-minute
**						intervals of flow occur within a given day.
**
******************************************************************************/
static void app_metrology_ShiftFlowLog(bool bFlowStatus)
{
	 uint8_t i;
	bool bTempBit1, bTempBit2;

	// Store current flow status into temporary bit variable
	bTempBit1 = bFlowStatus;

	// Shift all 12 bytes over
	for(i=12; i>0; i--)
	{ 
		// Set tempBit2 based on upper bit of each byte in the log	so that
		// after shift it is preserved and it can be shifted into the next byte.
		if((abyFlowLog[i-1] & BIT7) == BIT7)
		{
		 	// MSBit of current byte in log was a '1'
			bTempBit2 = 1;
		}
		
		else
		{
		 	// MSBit of current byte in log was a '0'
			bTempBit2 = 0;
		}


		// Now have bTempBit2 and bTempBit1 so load bTempBit1 into log
		abyFlowLog[i-1] <<= 1;
		abyFlowLog[i-1] |= bTempBit1;

		// Store bTempBit2 so it can be OR'd in on next byte
		bTempBit1 = bTempBit2;

	}

}


/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_ShiftLeakLog
**    
**              Author: Troy Harstad
**             Created: 09/29/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
** 
**    
**    Function Returns: None
**
**         Description: This function accepts the current Leak status bit and 
**						shifts it into the leak log.
**
**						abyLeakLog[] contains 6-bytes, each bit represents whether 
**						flow occured for a given day.
** 
**						abyLeakLog[0] (MSbit) contains the oldest leak status bit.
**						abyLeakLog[5] (LSbit) contains the newest leak status bit.
**						abyLeakLog[] is used to determine Days of Leak based on 
**						how many days within the last 35 a leak day occured.
**
******************************************************************************/
static void app_metrology_ShiftLeakLog(bool bLeakStatus)
{
	 uint8_t i;
	bool bTempBit1, bTempBit2;

	// Store current leak status into temporary bit variable
	bTempBit1 = bLeakStatus;


	// Shift all 6 bytes over
	for(i=6; i>0; i--)
	{ 
		// Set tempBit2 based on upper bit of each byte in the log so that
		// after shift it is preserved and it can be shifted into the next byte.
		if((abyLeakLog[i-1] & BIT7) == BIT7)
		{
		 	// MSBit of current byte in log was a '1'
			bTempBit2 = 1;
		}
		
		else
		{
		 	// MSBit of current byte in log was a '0'
			bTempBit2 = 0;
		}


		// Now have bTempBit2 and bTempBit1	so OR bTempBit1 into log
		abyLeakLog[i-1] <<= 1;
		abyLeakLog[i-1] |= bTempBit1;

		// Store bTempBit2 so it can be OR'd into log on next byte
		bTempBit1 = bTempBit2;

	}

}



/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_ShiftNoFlowLog
**    
**              Author: Troy Harstad
**             Created: 09/29/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
** 
**    
**    Function Returns: None
**
**         Description: This function accepts the current No Flow status bit and 
**						shifts it into the No Flow log.
**
**						abyNoFlowLog[] contains 6-bytes, each bit represents whether 
**						that day was a "No Flow" day. 
** 
**						abyNoFlowLog[0] (MSbit) contains the oldest No Flow status bit.
**						abyNoFlowLog[5] (LSbit) contains the newest No Flow status bit.
**						abyNoFlowLog[] is used to determine Days of No Flow based on 
**						how many days within the last 35 a No Flow day occured.
**
******************************************************************************/
static void app_metrology_ShiftNoFlowLog(bool bNoFlowStatus)
{
	 uint8_t i;
	bool bTempBit1; 
	bool bTempBit2;

	// Store current No Flow status into temporary bit variable
	bTempBit1 = bNoFlowStatus;


	// Shift all 6 bytes over
	for(i=6; i>0; i--)
	{ 
		// Set tempBit2 based on upper bit of each byte in the log so that
		// after shift it is preserved and it can be shifted into the next byte.
		if((abyNoFlowLog[i-1] & BIT7) == BIT7)
		{
			// MSBit of current byte in log was a '1'
			bTempBit2 = 1;
		}
		
		else
		{
		 	// MSBit of current byte in log was a '0'
			bTempBit2 = 0;
		}


		// Now have bTempBit2 and bTempBit1	so OR bTempBit1 into log
		abyNoFlowLog[i-1] <<= 1;
		abyNoFlowLog[i-1] |= bTempBit1;
		
		// Store bTempBit2 so it can be OR'd into log on next byte
		bTempBit1 = bTempBit2;
	}
}


/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_ShiftSmallBackFlowLog
**    
**              Author: Troy Harstad
**             Created: 09/29/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
** 
**    
**    Function Returns: None
**
**         Description: This function accepts the current Flow status bit and shifts 
**						it into the backflow log.
**
**						abyBackFlowLog[] contains 12-bytes, each bit represents 
**						whether backflow occured for a given 15-minute interval. 
** 
**						abyBackFlowLog[0]  (MSbit) contains the oldest flow status bit.
**						abyBackFlowLog[11] (LSbit) contains the newest flow status bit.
**						abyBackFlowLog[] is used to determine 24hr back flow  
**
******************************************************************************/
static void app_metrology_ShiftSmallBackFlowLog(bool bFlowStatus)
{
	 uint8_t i;
	bool bTempBit1, bTempBit2;

	// Store current flow status into temporary bit variable
	bTempBit1 = bFlowStatus;

	// Shift all 12 bytes over
	for(i=12; i>0; i--)
	{ 
		// Set tempBit2 based on upper bit of each byte in the log	so that
		// after shift it is preserved and it can be shifted into the next byte.
		if((abySmallBackFlowLog[i-1] & BIT7) == BIT7)
		{
		 	// MSBit of current byte in log was a '1'
			bTempBit2 = 1;
		}
		
		else
		{
		 	// MSBit of current byte in log was a '0'
			bTempBit2 = 0;
		}


		// Now have bTempBit2 and bTempBit1 so load bTempBit1 into log
		abySmallBackFlowLog[i-1] <<= 1;
		abySmallBackFlowLog[i-1] |= bTempBit1;

		// Store bTempBit2 so it can be OR'd in on next byte
		bTempBit1 = bTempBit2;

	}

}


/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_ShiftLargeBackFlowLog
**    
**              Author: Troy Harstad
**             Created: 09/29/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
** 
**    
**    Function Returns: None
**
**         Description: This function accepts the current Flow status bit and shifts 
**						it into the backflow log. 
**
**						abyBackFlowLog[] contains 12-bytes, each bit represents 
**						whether backflow occured for a given 15-minute interval. 
** 
**						abyBackFlowLog[0]  (MSbit) contains the oldest flow status bit.
**						abyBackFlowLog[11] (LSbit) contains the newest flow status bit.
**						abyBackFlowLog[] is used to determine 24hr back flow  
**
******************************************************************************/ 
static void app_metrology_ShiftLargeBackFlowLog(bool bFlowStatus)
{
	 uint8_t i;
	bool bTempBit1, bTempBit2;

	// Store current flow status into temporary bit variable
	bTempBit1 = bFlowStatus;

	// Shift all 12 bytes over
	for(i=12; i>0; i--)
	{ 
		// Set tempBit2 based on upper bit of each byte in the log	so that
		// after shift it is preserved and it can be shifted into the next byte.
		if((abyLargeBackFlowLog[i-1] & BIT7) == BIT7)
		{
		 	// MSBit of current byte in log was a '1'
			bTempBit2 = 1;
		}
		
		else
		{
		 	// MSBit of current byte in log was a '0'
			bTempBit2 = 0;
		}


		// Now have bTempBit2 and bTempBit1 so load bTempBit1 into log
		abyLargeBackFlowLog[i-1] <<= 1;
		abyLargeBackFlowLog[i-1] |= bTempBit1;

		// Store bTempBit2 so it can be OR'd in on next byte
		bTempBit1 = bTempBit2;

	}

}


/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_CountFlowIntervals
**    
**              Author: Troy Harstad
**             Created: 09/29/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
** 
**    
**    Function Returns: None
**
**         Description: This function Counts the number of 1's in the abyFlowLog[12] 
**						 
**						Each bit of the 96 bits represents an 15-minute interval of flow 
** 						during the last 24-hour period 
**
******************************************************************************/ 
static void app_metrology_CountFlowIntervals(void)
{
 	 uint8_t byBitMask;
	 uint8_t byIndex;
	 uint8_t i;

	// Clear byFlowIntervals in preparation of counting the flow intervals
	byFlowIntervals = 0;	
	
	// Loop through the 12-bytes of flow log
	for (byIndex=0; byIndex<12; byIndex++)
	{
		// Set bit mask to the LSB		
		byBitMask = BIT0; 

		// Loop 8-bits in each byte
		for(i=0;i<8;i++)
		{	
			// Check each interval (bit) for flow
			if (abyFlowLog[byIndex] & byBitMask)
			{
		 		// Increment the flow intervals if the flow log indicates
				// flow in that interval.
				byFlowIntervals++;
			}
			
			// Shift the bit mask to check the next bit in the byte
			byBitMask <<= 1;
		}
	}
}


/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_CountNoFlowDays
**    
**              Author: Troy Harstad
**             Created: 10/1/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
** 
**    
**    Function Returns: None
**
**         Description: This function updates the current value of byNoFlowDays.
**						This is done by rotating abyNoFlowLog 
**						48 times.  The max number of consecutive 1's in the last
**		 				35-bits is stored in byNoFlowDays.
**
** VALIDATED 10/1/09
******************************************************************************/
static void app_metrology_CountNoFlowDays(void)
{  
	 uint8_t i;
	 uint8_t byIndex;
	 uint8_t byBitMask;
	 uint8_t byNumberBits;
	 uint8_t byNoFlowDaysTemp;


	// Init NoFlow Days to 0
	byNoFlowDays = 0;
	
	// Init NoFlow Days temp counter to 0
	byNoFlowDaysTemp = 0;
	
	// Init bit counter to 35 since we only check the status of the most recent
	// 35 days (bits) of the NoFlowLog
	byNumberBits = 35;
	
	// Count the '1' bits in 5 bytes
	for (byIndex=5; byIndex>0; byIndex--)
	{
		// Set bit mask to the LSB		
		byBitMask = BIT0; 

		// 8 bits per byte
		for(i=0;i<8;i++)
		{	
			// Only check while byNumberBits is between 35-1		
			if(byNumberBits != 0)
			{
				// Check the NoFlow Log for no flow but on this particular day
				if (abyNoFlowLog[byIndex] & byBitMask)
				{
		 			// Bit was set so increment temporary counter
					byNoFlowDaysTemp++;
				}
				
				else
				{
					// Bit was not set so check if the temp counter is larger than
					// the current value of byNoFlowDays.  If it is store the 
					// new larger value.
					if(byNoFlowDaysTemp > byNoFlowDays)
					{
					 	byNoFlowDays = byNoFlowDaysTemp;
					}
	
					// Clear temporary counter since a non noflow day was detected
					byNoFlowDaysTemp = 0;
				}
				
				// Shift mask to left to check next bit within byte
				byBitMask <<= 1;
	
				// Decrement bit counter since we are only checking 35-bits
				// once it reaches 0, it needs to stay at 0.
				if (byNumberBits > 0)
				{
					byNumberBits--;
				}
	
			}

		}
	}
	
	// Do final check to see if temporary NoFlowDays counter is the largest yet.
	// Store it if it is.
	if(byNoFlowDaysTemp > byNoFlowDays)
	{
	 	byNoFlowDays = byNoFlowDaysTemp;
	} 
}


/******************************************************************************
*******************************************************************************
**
**            Filename: app_metrology.c
**       Function Name: app_metrology_Leak35Day
**    
**              Author: Troy Harstad
**             Created: 10/1/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: None
** 
**    
**    Function Returns: None
**
**         Description: Leak35Day updates the current value of byLeakDays.
**						This is done by rotating LeakLog 48 times.
**						The number of 1's in the last 35 shifts is counted so
**		 				that LeakDays equals the number of Leak Days in the last 35 days.
**
******************************************************************************/
static void app_metrology_Leak35Day(void)
{
	 uint8_t byBitMask;
	 uint8_t byIndex;
	 uint8_t byNumberBits;
	 uint8_t i;


	// Init byLeakDays to 0
	byLeakDays = 0;
	
	// Init bit counter to 35 since we only check the status of the most recent
	// 35 days (bits) of the LeakLog
	byNumberBits = 35;
	

	for (byIndex=5; byIndex>0; byIndex--)
	{
		// Set bit mask to the LSB		
		byBitMask = BIT0; 

		
		for(i=0;i<8;i++)
		{	
			 		
			if(byNumberBits != 0)
			{
				
				if (abyLeakLog[byIndex] & byBitMask)
				{
		 			// Bit was set so increment leak days counter
					byLeakDays++;
				}
				
				
				// Shift mask to left to check next bit within byte
				byBitMask <<= 1;
	
				// Decrement bit counter since we are only checking 35-bits
				// once it reaches 0, it needs to stay at 0.
				if (byNumberBits > 0)
				{
					byNumberBits--;
				}
			}
		}
	}
}




//=========================
//
// Flag Retrieval Functions
//
//=========================


// This function returns a byte, which contains the three consecutive no flow
// days flags in bits 4,3, and 2.
uint8_t app_metrology_NoFlowDaysFlags_Retrieve(void)
{ 	
	return abyNoFlowDaysTable[byNoFlowDays];
}


// This function returns a byte that relates to the backflow flag
// 00 = no reverse flow
// 01 = small backflow event
// 10 = large backflow event
uint8_t app_metrology_24Hr_Backflow_Retrieve(void)
{
	 uint8_t byBFLog;
	 uint8_t byByteIndex;
	
	byBFLog = 0;

	// Loop through large backflow log looking for a set bit
	// If a bit is found to be set, exit loop, set return variable 
	for(byByteIndex = 0; ((byByteIndex < 12) && (byBFLog == 0)); byByteIndex++)
	{
		 if(abyLargeBackFlowLog[byByteIndex] > 0)
		 {
		 	// This wil exit the for loop and tell the small bf log check to skip
			byBFLog = BIT1;		 	
		 }
	}
	
	// Check if large backflow event was found, byBFLog will be set if large
	// event was found, if not then no large bf event was found, check small bf event 
	if(byBFLog == 0)
	{
		// Loop through small backflow log looking for set bit
		// If a bit is found to be set, exit loop, set return variable
		for(byByteIndex = 0; ((byByteIndex < 12) && (byBFLog == 0)); byByteIndex++)
		{
			if(abySmallBackFlowLog[byByteIndex] > 0)
		 	{
		 		// This wil exit the for loop
				byBFLog = BIT0;		 	
		 	}
		}		
	}

	return byBFLog;
}


 // This function returns a byte, which contains the number of days of leak in
 // 35 days info in three bits 4,3, and 2.  Bits 1 and 0 contain the Leak current
 // values (leak over the last 96 intervals).
uint8_t app_metrology_LeakFlags_Retrieve(void)
{ 	
	 uint8_t byLeakFlags;

	// First set bits 5,4, and 3 accordingly
	byLeakFlags = abyLeakDaysTable[byLeakDays];

	// Check if the Leak Status flags need to be overwritten because
	// the MIU is connected to a ECoder
	if((sARBReadingData.byRegisterType == REGISTER_ECODER) ||
	   (sARBReadingData.byRegisterType == REGISTER_ECODER2))
	{
		// Overwrite the Leak Status Flag from the value from the reading
		// First clear them
		byLeakFlags &= ~ BIT1;
		byLeakFlags &= ~ BIT0;
		// Overwrite
		byLeakFlags |= (sARBReadingData.byLeakStatusFlag & BIT1);
		byLeakFlags |= (sARBReadingData.byLeakStatusFlag & BIT0);
	}
	else // Register is not an ECoder, use calculated falgs
	{	
		// Then set bits 0 and 1 (leak detect current) accordingly
		if(wFlags & LEAK_CURRENT_BIT_1)
		{
			byLeakFlags |= BIT1;
		}
	
		if(wFlags & LEAK_CURRENT_BIT_0)
		{	
			byLeakFlags |= BIT0;
		}
	}
	
	return byLeakFlags;
    
}


// This is an access function for uASIC_Data.sASIC_Data.byBFMinAge, 
// which is an Ecoder variable.
uint8_t app_metrology_BFMinAge_Retrieve(void)
{
 	return byBFMinAge;
}


// This is an access function for uASIC_Data.sASIC_Data.byBFMaxAge, 
// which is an Ecoder variable.
uint8_t app_metrology_BFMaxAge_Retrieve(void)
{
 	return byBFMaxAge;
}



// This was added to clear the backflow flags, which are caused
// during manufacturing/test.  It clears both the 35-day flag
// and the 24 hour flags.  This is only accessible via an IR
// command
bool app_metrology_BackflowFlags_Clear(void)
{
     uint8_t byIndex;

 	// Clear backflow event by setting days since last backflow to >35
    byBFMaxAge = 36;
    byBFMinAge = 36;
    
    // Clear 24 hour backflow flags
    for(byIndex=0; byIndex<12; byIndex++)
    {
        abySmallBackFlowLog[byIndex] = 0;
        abyLargeBackFlowLog[byIndex] = 0;
    }
     
    return true;

}


// Function to get the 24 hour timer as a "count up" value
uint8_t app_metrology_24HourTimer_Retrieve(void)
{
    return (96 - byA24HrTmr);
}

