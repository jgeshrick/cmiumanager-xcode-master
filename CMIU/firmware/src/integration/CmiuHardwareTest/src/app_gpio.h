/******************************************************************************
*******************************************************************************
**
**         Filename: app_gpio.h
**    
**           Author: Troy Harstad
**          Created: 1/19/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_GPIO_H
#define __APP_GPIO_H

#include <stdint.h>

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
#define GPIO_PIN_0      0
#define GPIO_PIN_1      1
#define GPIO_PIN_2      2
#define GPIO_PIN_3      3
#define GPIO_PIN_4      4
#define GPIO_PIN_5      5
#define GPIO_PIN_6      6
#define GPIO_PIN_7      7
#define GPIO_PIN_8      8
#define GPIO_PIN_9      9
#define GPIO_PIN_10     10
#define GPIO_PIN_11     11
#define GPIO_PIN_12     12
#define GPIO_PIN_13     13
#define GPIO_PIN_14     14
#define GPIO_PIN_15     15


#define GPIO_INIT_LOW       0
#define GPIO_INIT_HIGH      1
#define GPIO_INIT_IGNORE    0



// ARB Data Bias on PE.13
#define ARB_BIAS_PIN       GPIO_PIN_13
#define ARB_BIAS_PORT      gpioPortE

// ARB Data Input on PE.14
#define ARB_DATA_PIN       GPIO_PIN_14
#define ARB_DATA_PORT      gpioPortE

// ARB Clock on PE.15
#define ARB_CLOCK_PIN       GPIO_PIN_15
#define ARB_CLOCK_PORT      gpioPortE

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_gpio_init(void);
extern void app_gpio_arbMode_enable(void);
extern void app_gpio_arbMode_disable(void);
void pinMode(uint8_t pin, uint8_t pinMode);
uint8_t digitalRead(uint8_t pin);
void digitalWrite(uint8_t pin, uint8_t value);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_GPIO_H

