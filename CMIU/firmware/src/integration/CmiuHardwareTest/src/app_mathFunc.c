/******************************************************************************
*******************************************************************************
**
**         Filename: MathFunc.c
**    
**           Author: Troy Harstad
**          Created: 06/03/2009
**
**     Last Edit By: Nick Sinas
**        Last Edit: 12/13/12
**    
**      Description: The MathFunc module contains math functions.  
**
**
** Revision History:
**         06/03/09: First created (Troy Harstad)
**         03/20/12: Added MathFunc_HexToEvenAscii() and MathFunc_EvenParity()
**         12/13/12: Updated for RV4 (Nick Sinas)
**
**	    Code Review:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/         
#include <stdint.h>
#include "app_mathFunc.h"
#include "stdint.h"

 
/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// Declare a union to use inside of the bin2bcd module.  It is delcared here
// because this is the place it is used and you can't declare it inside of the 
// bin2bcd assembly file.
// Used when converting the  binary reading to BCD for display on LCD
// union 
// {
//    uint8_t buf[20];
//    UU64 in;
// } U_b;

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/



/******************************************************************************
*******************************************************************************
**
**            Filename: MathFunc.c
**       Function Name: MathFunc_Mult32x32
**    
**              Author: SiLabs/Chris League
**             Created: 06/3/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: UU32 mc - multiplicand - 32bit union
**          			UU32 mp - multiplier - 32bit union
**    Function Returns: UU64 *pd - product - a pointer to a 64bit union, a union of two 32bit values
**
**         Description: This function multiplies two 32-bit values and leaves
**						the product in a 64-bit	value.
**
******************************************************************************/
void MathFunc_Mult32x32(UU32 mc, UU32 mp, UU64 *pd)
{

    uint64_t val;
    
    val = (mc.U32 * mp.U32);
    
   pd->U64 = val;
    
    
// 	UU32 pp[4];
// 	UU32 scratch;
// 	pp[3].U32 = (U32)mc.U16[1] * (U32)mp.U16[1];
// 	pp[2].U32 = (U32)mc.U16[0] * (U32)mp.U16[1];
// 	pp[1].U32 = (U32)mc.U16[1] * (U32)mp.U16[0];
// 	pp[0].U32 = (U32)mc.U16[0] * (U32)mp.U16[0];
// 	pd->U16[3] = pp[3].U16[1];
// 	scratch.U32 = (U32)pp[3].U16[0]+(U32)pp[2].U16[1]+(U32)pp[1].U16[1];
// 	pd->U16[2] = scratch.U16[1];
// 	scratch.U32 = (U32)scratch.U16[0]+(U32)pp[2].U16[0]+(U32)pp[1].U16[0]+(U32)pp[0].U16[1];
// 	pd->U16[1] = scratch.U16[1];
// 	pd->U16[0] = scratch.U16[0]+pp[0].U16[0];
} 


/******************************************************************************
*******************************************************************************
**
**            Filename: MathFunc.c
**       Function Name: MathFunc_AsciiToHex
**    
**              Author: Troy
**             Created: 06/3/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: *pbyVal - a pointer to an 8-bit ASCII character that will
**								  be converted to hex.
**    Function Returns: none
**
**         Description: This function converts a single ASCII character to a hex
**					   	value.  It also clears the parity bit (BIT7).
******************************************************************************/
void MathFunc_AsciiToHex(uint8_t *pbyVal)
{
	// Clear the parity bit
	*pbyVal &= ~(BIT7);
	
	// If value is between 0x30 and 0x39 then it is a number '0-9'.  To convert
	// to hex, subtract 0x30 from value.
	if((*pbyVal >= 0x30)&(*pbyVal <= 0x39))
	{
		*pbyVal -= 0x30;	
	
	}	

	// If value is between 0x41 and 0x46 then it is a letter 'A-F'.  To convert 
	// to hex, subtract 0x37 from value. 
	else if((*pbyVal >= 0x41)&(*pbyVal <= 0x46))
	{
		*pbyVal -= 0x37;

	}

	else
	{
	 	// Do nothing if ASCII is not '0-9' or 'A-F'
	}

}

/******************************************************************************
*******************************************************************************
**
**            Filename: MathFunc.c
**       Function Name: MathFunc_HexToAscii
**    
**              Author: Troy
**             Created: 06/3/2009
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: *pbyVal - a pointer to an hex value that will be converted
**						to an ASCII character.
**    Function Returns: none
**
**         Description: This function converts a hex value into a single ASCII
**						character.
**
******************************************************************************/
void MathFunc_HexToAscii(uint8_t *pbyVal)
{
	// Clear the parity bit
	*pbyVal &= ~(BIT7);
	
	// If value is between 0x00 and 0x09, to convert to ASCII, 
	// add 0x30 to the value.
	if(*pbyVal <= 0x09)
	{
		*pbyVal += 0x30;	
	
	}	

	// Else, it must be A-F so add 0x37 to get hex 
	else if ((*pbyVal >= 0x0A)&(*pbyVal <= 0x0F))
	{
		*pbyVal += 0x37;
	} 

	else
	{
	 // Do nothing if Hex value is not '0x00-0x0F'
	}
}


/******************************************************************************
*******************************************************************************
**
**            Filename: MathFunc.c
**       Function Name: MathFunc_HexToEvenAscii
**    
**              Author: Troy
**             Created: 02/28/2012
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: *pbyVal - a pointer to an hex value that will be converted
**						to an ASCII character.
**    Function Returns: none
**
**         Description: This function converts a hex value into a single ASCII
**						character with even parity.
**
******************************************************************************/
void MathFunc_HexToEvenAscii(uint8_t *pbyVal)
{
    static uint8_t byTempVal;
    static uint8_t byBit_Mask;
    static uint8_t byBitCount;
    static uint8_t byIndex;
	
	// If value is between 0x00 and 0x09, to convert to ASCII, 
	// add 0x30 to the value.
	if(*pbyVal <= 0x09)
	{
		*pbyVal += 0x30;	
	
	}	

	// Else, it must be A-F so add 0x37 to get hex 
	else if ((*pbyVal >= 0x0A)&(*pbyVal <= 0x0F))
	{
		*pbyVal += 0x37;
	} 

	else
	{
        // Do nothing if Hex value is not '0x00-0x0F'
        return;
	}


// Add parity bit to make even (if necessary).
    // First count '1' bits
    byTempVal = *pbyVal;
	
    // Set bit mask to the LSB		
	byBit_Mask = BIT0; 

    // Clear bit count
    byBitCount = 0;
   
    // Loop 8-bits in each byte
	for(byIndex=0; byIndex<8; byIndex++)
	{	
		// Check each interval (bit) for flow
		if (byTempVal & byBit_Mask)
		{
	 		// Increment bit counter.
			byBitCount++;
		}		
		// Shift the bit mask to check the next bit in the byte
		byBit_Mask <<= 1;
	}
        
    // Check if odd bit count, if it is odd, add parity bit to byte
    if(byBitCount & 0x01)
    {
        *pbyVal |= BIT7;   
    }

}



/******************************************************************************
*******************************************************************************
**
**            Filename: MathFunc.c
**       Function Name: MathFunc_EvenParity
**    
**              Author: Troy
**             Created: 03/20/2012
**
**        Last Edit By: 
**           Last Edit: 
**    
**  Function Arguments: *pbyVal - a pointer to an hex value that will have a
**                      the parity bit (BIT7) set if necessary to make it even
**                      parity.
**    Function Returns: none
**
**         Description: This function converts a hex value into a even parity
**						hex value.  Only accounts for lower 7 bits being data.
**
******************************************************************************/
void MathFunc_HexToEven(uint8_t *pbyVal)
{
    static uint8_t byTempVal;
    static uint8_t byBit_Mask;
    static uint8_t byBitCount;
    static uint8_t byIndex;

// Add parity bit to make even (if necessary).
    // First count '1' bits
    byTempVal = *pbyVal;
	
    // Set bit mask to the LSB		
	byBit_Mask = BIT0; 

    // Clear bit count
    byBitCount = 0;
   
    // Loop 8-bits in each byte
	for(byIndex=0; byIndex<8; byIndex++)
	{	
		// Check each interval (bit) for flow
		if (byTempVal & byBit_Mask)
		{
	 		// Increment bit counter.
			byBitCount++;
		}		
		// Shift the bit mask to check the next bit in the byte
		byBit_Mask <<= 1;
	}
        
    // Check if odd bit count, if it is odd, add parity bit to byte
    if(byBitCount & 0x01)
    {
        *pbyVal |= BIT7;   
    }

}


uint16_t MatchFunc_DivideWithRound(uint16_t wDividend, uint16_t wDivisor)
{
   static uint16_t wRetVal;

   wRetVal = wDividend / wDivisor;
   if ((wDividend % wDivisor) > (wDivisor / 2))
      wRetVal++;

   return(wRetVal);
}
