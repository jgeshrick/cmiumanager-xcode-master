/******************************************************************************
*******************************************************************************
**
**         Filename: app_configuration.h
**    
**           Author: Troy Harstad
**          Created: 1/21/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_CONFIGURATION_H
#define __APP_CONFIGURATION_H



/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_configuration_init(void);


extern void Configuration_Init(void);
extern void Configuration_InitHW(void);
extern bool Configuration_VerifyCRC32(void);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// INIT Configuration Union 
extern const U_MIU_CONFIG_NORMAL uMIUConfigNormal;
extern const U_MIU_CONFIG_SWIPE uMIUConfigSwipe;
extern const S_IMG_INFOBLK sConfigImgInfo;
extern const uint16_t wHardwareVersion;
extern const U_MIU_SERVER_CONFIG uMIUServerConfig; 

/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_CONFIGURATION_H

