/******************************************************************************
*******************************************************************************
**
**         Filename: test_cmiu.c
**    
**           Author: Jeff Frey
**           Created: 4/02/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**				- derivative of NeptuneProto2.c (Barry Huggins)
**				- menu driven test of hardware components on cmiu:
**						Flash Memory (WiP)
**						BTLE Radio (have not started)
**						Cellular Modem (complete)
**
**		Notes: 
**			Modfied other files from Barry's baseline: 
**				app_flash.c, autogen_init.c, app_power.c, app_modem.c
**
**
**
**
** Revision History:
** Apr212015-jef: [1] added Sleep Mode (EM2) Test
**						[2] copied app_cmiu_init() to here from app_cmiu.c
** Apr172015-jef: [1] modem tests complete
**	Apr102015-jef: 
**		[1] new UI to match Tim's HW test breakdown
**	Apr082015-jef: flesh out Flash Memory test
**	Apr072015-jef: 
**		[1] test framework created
**		[2] Flash Memory test installed into framework 
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file test_cmiu.c
*
* @brief quick & dirty test code to verify HW functionalty
* 
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "typedefs.h"
#include "efm32lg332f256.h"
#include <stdbool.h>
#include "autogen_init.h"
#include "app_arb.h"
#include "app_flash.h"
#include "app_ble.h"
#include "app_modem.h"
#include "app_uart.h"
#include "app_timers.h"
#include "app_power.h"
#include "app_rtc.h"
#include "app_gpio.h"
#include "app_cmu.h"
#include "app_cmiu_app.h"
#include "app_error.h"
#include "app_metrology.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "em_timer.h"
#include "em_int.h"
#include "em_rtc.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/

// (keep this) #define APP_TEST

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/

bool   App_ProcessInputs (void);


static bool testFlashId(bool);
static bool testFlashWr(bool);
static bool testBleId(bool);
static bool testBleLoopback(bool);
static bool testBlePair(bool);
static bool testBleComm(bool);
static bool testModemImei(bool);
static bool testModemSim(bool);
static bool testModemNetwork(bool);
static bool testModemCall(bool);
static void helpScreen(void);
static bool testSleepMode(bool);			// Apr212015-jef
static bool testSleepMode15Sec(bool);		// Apr222015-jef
static void app_cmiu_init(void);		// Apr212015-jef

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
uint16_t    flashDeviceID;
uint8_t     flashStatusRegister;
char        debugBuffer[64];
char    		replyBuff[128];


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h
// Let the compiler check the typedefs.  For instance if int8_t ever becomes
// something other than 1 byte the [sizeof(int8_t) == 1] will become false, 
// which means the union member int8_err will have an array size of zero.  
// The compiler will give an error if the array size is set to zero.
// PC-lint will give several errors for this since it is an incomplete array
// and the mebers are not referenced so it is removed from the lint check using
// the #ifndef _lint directive.
#ifndef	_lint
union {
    char int8_err   [sizeof(int8_t)    == 1];
    char uint8_err  [sizeof(uint8_t)   == 1];
    char int16_err  [sizeof(int16_t)   == 2];
    char uint16_err [sizeof(uint16_t)  == 2];
    char int32_err  [sizeof(int32_t)   == 4];
    char uint32_err [sizeof(uint32_t)  == 4];
    char uint64_err [sizeof(uint64_t)  == 8];
    char int64_err  [sizeof(int64_t)   == 8];
    char ptr_err    [sizeof(uintptr_t) == 4];
}; //Anonymous
#endif

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None


/***************************************************************************//**
 * @brief
 *   main function
 *
 * @note
 *   Executes main program
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
int main(void) {

	
#ifdef APP_TEST
    volatile uint32_t timerVal;
#endif // APP_TEST

    /* Initialize chip */

 	eADesigner_Init();
	gpio_Init();
	Uart_Uart1Setup();		// Apr022015-jef: ASYNC_57600/ monitor
	Uart_Uart0Setup();		// Apr022015-jef: ASYNC_tbr/ cellular modem
	USART_Enable(USART2, usartEnable);	// Apr022015-jef: SPI/ Flash & Bt radio
    
	// give time to erase if stuck in low power mode
	// Apr172015-jef: ?   Timers_Delay (5000);   

	app_cmiu_init();	// Apr222015-jef: modified
	
	app_timers_enableMsecs();	// Apr212015-jef
	
  	DEBUG_OUTPUT_TEXT("   ~~ system initialized ~~ \n\r");  


	/*******************************************************
	 * simple User Interface
	 *
	 *
	 */
		
	uint8_t     tempChar;
	bool 			verbose = TRUE;
	bool			testResult = FALSE;		// TRUE == pass
	
	while (1) {

		Timers_Delay (100);

		// Display User Screen
		DEBUG_OUTPUT_TEXT("\n\n\n\n\n\n\n\n\n\n\r");
		DEBUG_OUTPUT_TEXT("     Neptune Board Functional Test   \n\r");	
		DEBUG_OUTPUT_TEXT("    ------------------------------------------- \n\n\r");
		DEBUG_OUTPUT_TEXT("     'a'  Run ALL tests  \n\r");
		DEBUG_OUTPUT_TEXT("     'v'  toggle VERBOSE output \n\r");
			// Apr152015-jef		DEBUG_OUTPUT_TEXT("     'h'  HELP screen \n\r");
		DEBUG_OUTPUT_TEXT("     'b'  Flash: Read Chip ID \n\r");		
		DEBUG_OUTPUT_TEXT("     'c'  Flash: Write & Read \n\r");	
		DEBUG_OUTPUT_TEXT("     'd'  Modem: Power Up & Read IMEI \n\r");			
		DEBUG_OUTPUT_TEXT("     'e'  Modem: SIM Communication \n\r");
		DEBUG_OUTPUT_TEXT("     'f'  Modem: Network Access \n\r");
		DEBUG_OUTPUT_TEXT("     'g'  Modem: Make a Call \n\r");						
		DEBUG_OUTPUT_TEXT("     'i'  BLE: Read Chip ID \n\r");
		DEBUG_OUTPUT_TEXT("     'j'  BLE: Loopback \n\r");			
		DEBUG_OUTPUT_TEXT("     'k'  BLE: Pair \n\r");
		DEBUG_OUTPUT_TEXT("     'l'  BLE: Communicate \n\r");
		DEBUG_OUTPUT_TEXT(" \n\r      Must disconnect debugger to enter EM2 \n\r");		
		DEBUG_OUTPUT_TEXT("     's'  EM2: Sleep Mode for 15 Seconds (GPIO not optimized)\n\r");
		DEBUG_OUTPUT_TEXT("     't'  EM2: Sleep Mode (GPIO off)\n\r");
		
		DEBUG_OUTPUT_TEXT("\n\r");	
		if(verbose) DEBUG_OUTPUT_TEXT("    Verbose Mode: ON \n\r");		
		else DEBUG_OUTPUT_TEXT("    Verbose Mode: OFF \n\r");	
		DEBUG_OUTPUT_TEXT("\n\n\r");
		Timers_Delay (100);		

		// Get User Input
		while(Uart_Uart1IsCharReady()==FALSE) {};
		tempChar = Uart_Uart1GetChar();	
		testResult = FALSE;
		switch (tempChar) {		
			case 'a':	if((testResult=testFlashId(verbose))==FALSE) break;
							if((testResult=testFlashWr(verbose))==FALSE) break;
							if((testResult=testModemImei(verbose))==FALSE) break;
							if((testResult=testModemSim(verbose))==FALSE) break;
							if((testResult=testModemNetwork(verbose))==FALSE) break;
							testResult = testModemCall(verbose);
/*	Apr172015-jef: temporary		
							if((testResult=testBleId(verbose))==FALSE) break;
							if((testResult=testBleLoopback(verbose))==FALSE) break;
							if((testResult=testBlePair(verbose))==FALSE) break;
							if((testResult=testBleComm(verbose))==FALSE) break;
*/			
			break;
			case 'v':	verbose = !verbose ; continue;
				// Apr152015-jef				case 'h':	helpScreen(); continue;
			case 'b': 	testResult = testFlashId(verbose); break;
			case 'c': 	testResult = testFlashWr(verbose); break;
			case 'd': 	testResult = testModemImei(verbose); break;
			case 'e': 	testResult = testModemSim(verbose); break;
			case 'f': 	testResult = testModemNetwork(verbose); break;
			case 'g': 	testResult = testModemCall(verbose); break;
			case 'i': 	testResult = testBleId(verbose); break;
			case 'j': 	testResult = testBleLoopback(verbose); break;
			case 'k': 	testResult = testBlePair(verbose); break;
			case 'l': 	testResult = testBleComm(verbose); break;
			case 's': 	testResult = testSleepMode15Sec(verbose); break;
			case 't': 	testResult = testSleepMode(verbose); break;
			default:  
				DEBUG_OUTPUT_TEXT("  invalid character, try again\n\r"); 
				Timers_Delay (1500); //;
				continue;
		}
	if(testResult) DEBUG_OUTPUT_TEXT("   test result: PASS \n\r"); 
	else DEBUG_OUTPUT_TEXT("   test result: FAIL \n\r");
	DEBUG_OUTPUT_TEXT("\n\n\r press any key to continue \n\r"); 
 	while(Uart_Uart1IsCharReady()==FALSE) {};
	tempChar = Uart_Uart1GetChar();  // empty buffer

	} // end of while(1)

} // end of main()

		
/*******************************************************************************
 * Flash Memory Test - Read Chip ID
 * 	description:
 *
 * 	rev:
 * 		Apr102015-jef: complete but not optimized
 */
	
bool testFlashId(bool verbose) {

	uint8_t   	testBuffer[20];
	uint8_t   	commandAndAddress[4];
	uint16_t 	i; // foot soldier
	DEBUG_OUTPUT_TEXT("\n\r  Executing Flash Memory Test - Read Chip ID \n\r");
	Timers_Delay (1000); 	
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r    Power Up Flash Memory\n\r");	
	Flash_PowerOn(); 
	memset (testBuffer, 0xAA, sizeof(testBuffer));	
	// Send SPI command & retrieve data
	commandAndAddress[0] = SPI_FLASH_INS_RDID;  
	Flash_SpiWrite (commandAndAddress, 1);
	Flash_SpiRead (testBuffer, 3);	// SpiRead() doesn't flush buffer
	Flash_SpiRead (testBuffer, 20);
	if(verbose) {
		DEBUG_OUTPUT_TEXT("    Read Identification Information: \n\r");		
		sprintf (debugBuffer, "      Manufacturer ID: 0x%2x\n\r", testBuffer[0]);
		Uart_Uart1WriteString (debugBuffer);	
		sprintf (debugBuffer, "      Memory Type: 0x%2x\n\r", testBuffer[1]);
		Uart_Uart1WriteString (debugBuffer);		
		sprintf (debugBuffer, "      Memory Capacity: 0x%2x\n\r", testBuffer[2]);
		Uart_Uart1WriteString (debugBuffer);	
		sprintf (debugBuffer, "      CFD Length: 0x%2x\n\r", testBuffer[3]);
		Uart_Uart1WriteString (debugBuffer);	
		sprintf (debugBuffer, "      CFD Content: ");
		Uart_Uart1WriteString (debugBuffer);	
		for(i=4;i<20;i++)	{ 
			sprintf (debugBuffer, "%d,", testBuffer[i]); 
			Uart_Uart1WriteString (debugBuffer);
			Timers_Delay (10);
		}
		sprintf (debugBuffer, " \n\r");
		Uart_Uart1WriteString (debugBuffer);		
		DEBUG_OUTPUT_TEXT("    Power Down Flash Memory\n\n\r");
	}	
	Flash_PowerOff(); 	
	// parse & analyze result
	if( 	(testBuffer[0] == 0x20) && 
			(testBuffer[1] == 0x71) && 
			(testBuffer[2] == 0x15) && 
			(testBuffer[3] == 0x10) && 
			(testBuffer[4] == 0) && 
			(testBuffer[5] == 0) && 
			(testBuffer[6] == 0) && 
			(testBuffer[7] == 0) &&
			(testBuffer[8] == 0) && 
			(testBuffer[9] == 0) &&
			(testBuffer[10] == 0) &&
			(testBuffer[11] == 0) &&
			(testBuffer[12] == 0) &&
			(testBuffer[13] == 0) &&
			(testBuffer[14] == 0) &&
			(testBuffer[15] == 0) &&
			(testBuffer[16] == 0) &&
			(testBuffer[17] == 0) &&
			(testBuffer[18] == 0) &&
			(testBuffer[19] == 0) ) return TRUE; // pass
	else

return FALSE; // failure		
}
	 

/******************************************
 * Flash Memory Test - Write / Read
 * 	description:
 *
 * 	rev:
 *			Apr172015-jef: debugging verbose=off issue.
 * 		Apr142015-jef: Works if verbose=on. Will fix 
 *			(timing issue) later for verbose=off
 */
bool testFlashWr(bool verbose) {

	uint8_t   	testBuffer2[20];
	uint8_t 		commandAndAddress[4]; 
	uint8_t 	i; 		// foot soldier
	volatile uint32_t j,k;		// to create short time delays
	
	DEBUG_OUTPUT_TEXT("\n\r  Executing Flash Memory Test - Write / Read \n\r");
	Timers_Delay (1000); 
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r       Power Up Flash Memory\n\r");	
	Flash_PowerOn(); 

	// erase the memory locations intending to write-read
	if(verbose) DEBUG_OUTPUT_TEXT("       Executing Subsector Erase\n\r");			
	Timers_Delay(50);
	Flash_WriteEnable();
	Timers_Delay(2);	// Apr142015-jef: leave this at 2?
	commandAndAddress[0] = SPI_FLASH_INS_SSE; // turn this into subroutine
	commandAndAddress[1] = 0;
	commandAndAddress[2] = 0x02;
	commandAndAddress[3] = 0;
	Flash_SpiWrite (commandAndAddress, 4); 
	j=Timers_GetMsTicks();
	Timers_Delay(2);		
	Flash_WriteDisable();
	Timers_Delay(2);	
	

	commandAndAddress[0] = SPI_FLASH_INS_RDSR; // turn this into subroutine
	Flash_SpiWrite (commandAndAddress, 1);
	Flash_SpiRead (testBuffer2, 4); 
   testBuffer2[0] = testBuffer2[3] & 0x01;		
	while(testBuffer2[0] != 0) {
		Timers_Delay (3);
		Flash_SpiWrite (commandAndAddress, 1);
		Flash_SpiRead (testBuffer2, 2); 
		testBuffer2[0] = testBuffer2[1] & 0x01;		
	}
	k=Timers_GetMsTicks()-j;
	if(verbose) DEBUG_OUTPUT_TEXT("       Subsector Erase Command Completed\n\r");	

	
	Timers_Delay(2);	// Apr172015-jef: SPI command separation
	
	// check to make sure erase occurred
	memset (testBuffer2, 0xC3, sizeof(testBuffer2));
	commandAndAddress[0] = SPI_FLASH_INS_READ;
	commandAndAddress[1] = 0;
	commandAndAddress[2] = 0x02;
	commandAndAddress[3] = 0;
	Flash_SpiWrite (commandAndAddress, 4);
	Flash_SpiRead (testBuffer2, 4); // SpiRead() doesn't flush buffer
	Flash_SpiRead (testBuffer2, 16);
	Timers_Delay (50);

	if(verbose) {
		sprintf(debugBuffer, "       Flash contents: ");
		Uart_Uart1WriteString (debugBuffer);			
		for(i=0;i<15;i++)  {
			sprintf (debugBuffer, "%d,", testBuffer2[i]);
			Uart_Uart1WriteString (debugBuffer);	
		}
		sprintf (debugBuffer, "%d\n\r",testBuffer2[15]);
		Uart_Uart1WriteString (debugBuffer);
	}	

	for(i=0;i<16;i++) {
		if(testBuffer2[i] != 255) {
			if(verbose) DEBUG_OUTPUT_TEXT("       Failure: Subsector NOT Erased\n\r");		
			return FALSE; // failure				
		}
	}
	
	
	// write data to Flash
	if(verbose) DEBUG_OUTPUT_TEXT("       Write (0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15) to Flash \n\r");	
	for(i=0;i<16;i++) { testBuffer2[i]=i; }
	Flash_WriteEnable();	
	for(j=0;j<50;j++) k=j;  // Apr222015-jef: was 25
	commandAndAddress[0] = SPI_FLASH_INS_PP;
	commandAndAddress[1] = 0;
	commandAndAddress[2] = 0x02;
	commandAndAddress[3] = 0;
 	Flash_SpiWrite (commandAndAddress, 4);
	Flash_SpiWrite (testBuffer2, 16);  
	for(j=0;j<100;j++) k=j;  // Apr222015-jef: was 25// short delay needed for USART driver
   Flash_WriteDisable();
	for(j=0;j<50;j++) k=j;  // Apr222015-jef: was 25// short delay needed for USART driver
	
	commandAndAddress[0] = SPI_FLASH_INS_RDSR; // turn this into subroutine
	Flash_SpiWrite (commandAndAddress, 1);
	Flash_SpiRead (testBuffer2, 2); 
   testBuffer2[0] &= 0x01;		
	while(testBuffer2[0] != 0) {
	//	Timers_Delay (3);
		Flash_SpiWrite (commandAndAddress, 1);
		Flash_SpiRead (testBuffer2, 1); 
		testBuffer2[0] &= 0x01;		
	}	
	 
	
	Timers_Delay(500);	// worst case page write time = 5mS	


	// Cycle Power
	if(verbose) DEBUG_OUTPUT_TEXT("       Power Down Flash Memory\n\r");
	Flash_PowerOff(); 
	if(verbose) DEBUG_OUTPUT_TEXT("       Wait 2 Seconds\n\r");
	Timers_Delay (2000);		
	if(verbose) DEBUG_OUTPUT_TEXT("       Power Up Flash Memory\n\r");
	Flash_PowerOn(); 	


	
	// Read those locations that were written to 
	if(verbose) DEBUG_OUTPUT_TEXT("       Read Flash Contents\n\r");		
	memset (testBuffer2, 0xAA, sizeof(testBuffer2));
	commandAndAddress[0] = SPI_FLASH_INS_READ;
	commandAndAddress[1] = 0; 
	commandAndAddress[2] = 0x02;
	commandAndAddress[3] = 0;
	Flash_SpiWrite (commandAndAddress, 4);
	Flash_SpiRead (testBuffer2, 4); // SpiRead() doesn't flush buffer
	Flash_SpiRead (testBuffer2, 16);  

	if(verbose) {
		sprintf(debugBuffer, "       Flash contents: ");
		Uart_Uart1WriteString (debugBuffer);			
		for(i=0;i<15;i++)  {
			sprintf (debugBuffer, "%d,", testBuffer2[i]);
			Uart_Uart1WriteString (debugBuffer);	
		}
		sprintf (debugBuffer, "%d\n\r",testBuffer2[15]);
		Uart_Uart1WriteString (debugBuffer);
	}	

	// compare actual to expected
	for(i=0;i<16;i++) {
		if(testBuffer2[i] != (uint8_t)i) {
			if(verbose) {
				sprintf (debugBuffer, "Failure: expected= %d, actual= %d \n\r", i, testBuffer2[1]);
				Uart_Uart1WriteString (debugBuffer);
				Timers_Delay (50);		
			}
			return FALSE;
		}
	}	
	if(verbose) DEBUG_OUTPUT_TEXT("       Flash contents are correct\n\r");

	// Leave Flash Erased
	if(verbose) DEBUG_OUTPUT_TEXT("       Erasing Flash\n\r");		
	Timers_Delay(50);
	Flash_WriteEnable();
	Timers_Delay(50);
	commandAndAddress[0] = SPI_FLASH_INS_SSE; // turn this into subroutine
	commandAndAddress[1] = 0;
	commandAndAddress[2] = 0x02;
	commandAndAddress[3] = 0;
	Flash_SpiWrite (commandAndAddress, 4); 
	Timers_Delay(2);	
	Flash_WriteDisable();
	Timers_Delay(2);	
	flashStatusRegister = Flash_ReadStatusRegister();
	while(flashStatusRegister != 0) {
		Timers_Delay (50);
		flashStatusRegister = Flash_ReadStatusRegister();
	}
   if(verbose) DEBUG_OUTPUT_TEXT("       Erase Complete\n\r");			
   if(verbose) DEBUG_OUTPUT_TEXT("       Power Down Flash\n\r");  
	Flash_PowerOff();
	

	return TRUE;	// test passes
}

	
/******************************************
 * BLE Test - Read Chip ID
 * 	description:
 *
 * 	rev:
 * 		Apr102015-jef: skeleton
 */
bool testBleId(bool verbose) {
	DEBUG_OUTPUT_TEXT("\n\r  Executing BLE Test - Read Chip ID \n\r");
	DEBUG_OUTPUT_TEXT("     not implemented yet ! \n\r");
	Timers_Delay (1000);
	return FALSE;	// pass | fail status
}
		
/******************************************
 * BLE Test - Loopback
 * 	description:
 *
 * 	rev:
 * 		Apr102015-jef: skeleton
 */
bool testBleLoopback(bool verbose) {
	DEBUG_OUTPUT_TEXT("\n\r  Executing BLE Test - Loopback \n\r");
	DEBUG_OUTPUT_TEXT("     not implemented yet ! \n\r");
	Timers_Delay (1000);
	return FALSE;	// pass | fail status
}
		
			
/******************************************
 * BLE Test - Pair
 * 	description:
 *
 * 	rev:
 * 		Apr102015-jef: skeleton
 */
bool testBlePair(bool verbose) {
	DEBUG_OUTPUT_TEXT("\n\r  Executing BLE Test - Pair \n\r");
	DEBUG_OUTPUT_TEXT("     not implemented yet ! \n\r");
	Timers_Delay (1000);
	return FALSE;	// pass | fail status
}
		
	
/******************************************
 * BLE Test - Communicate
 * 	description:
 *
 * 	rev:
 * 		Apr102015-jef: skeleton
 */
bool testBleComm(bool verbose) {
	DEBUG_OUTPUT_TEXT("\n\r  Executing BLE Test - Communicate \n\r");
	DEBUG_OUTPUT_TEXT("     not implemented yet ! \n\r");
	Timers_Delay (1000);
	return FALSE;	// pass | fail status
}
		

		
	
/******************************************
 * Modem Test - Power Up / Read IMEI
 * 	description:
 *
 * 	rev:
 * 		Apr162015-jef: functions correctly
 */
bool testModemImei(bool verbose) {
	
	bool 	testResult = FALSE;
	uint8_t i=0;		// foot soldier
	
	DEBUG_OUTPUT_TEXT("\n\r  Executing Modem Test - Power Up / Read IMEI \n\r");
	Timers_Delay (1000);
	
	// power up
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Power Up Modem\n\r");	
 	app_power_modemPower_on();
	i=10;
	if(verbose) {
		sprintf (debugBuffer, "      Wait for Modem to Boot (sec) %2d  ", i);
		Uart_Uart1WriteString (debugBuffer);
	}
	while(i>0) {
		Timers_Delay (1000);		
		i--;
		if(verbose) {
			sprintf (debugBuffer, "\b\b\b\b%2d  ", i);
			Uart_Uart1WriteString (debugBuffer);		
		}
	}
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r");
	
	if(verbose) DEBUG_OUTPUT_TEXT("      Query Modem for IMEI\n\r");		
	testResult = app_modem_sendATGetReply("AT#CGSN", "CGSN:", replyBuff, sizeof(replyBuff), 3000);
	
	if(verbose) {
		sprintf (debugBuffer, "      IMEI: %s\n\r", replyBuff);
		Uart_Uart1WriteString (debugBuffer);
	}

	if(verbose) DEBUG_OUTPUT_TEXT("      Power Down Modem\n\r");	
	app_power_modemPower_off();
	
	return testResult;	// pass | fail status
}
		


/******************************************
 * Modem Test - SIM Communication
 * 	description:
 *
 * 	rev:
 * 		Apr162015-jef: functions correctly
 */
bool testModemSim(bool verbose) {

	bool 	testResult = FALSE;
	uint8_t i=0;		// foot soldier
	
	DEBUG_OUTPUT_TEXT("\n\r  Executing Modem Test - SIM Communication \n\r");
	Timers_Delay (1000);

	
	// power up
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Power Up Modem\n\r");	
 	app_power_modemPower_on();
	i=10;
	if(verbose) {
		sprintf (debugBuffer, "      Wait for Modem to Boot (sec) %2d  ", i);
		Uart_Uart1WriteString (debugBuffer);
	}
	while(i>0) {
		Timers_Delay (1000);		
		i--;
		if(verbose) {
			sprintf (debugBuffer, "\b\b\b\b%2d  ", i);
			Uart_Uart1WriteString (debugBuffer);		
		}
	}
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r");
	
	if(verbose) DEBUG_OUTPUT_TEXT("      Query Modem SIM for IMSI\n\r");		
	testResult = app_modem_sendATGetReply("AT#CIMI", "CIMI:", replyBuff, sizeof(replyBuff), 3000);
	
	if(verbose) {
		sprintf (debugBuffer, "      IMSI: %s\n\r", replyBuff);
		Uart_Uart1WriteString (debugBuffer);
	}

	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Power Down Modem\n\r");	
	app_power_modemPower_off();
	
	return testResult;	// pass | fail status	
	
}
		


/******************************************
 * Modem Test - Network Access
 * 	description:
 *
 * 	rev:
 * 		Apr162015-jef: functions correctly
 */
bool testModemNetwork(bool verbose) {
	char    replyBuff[128];
	char    *tmpPtr;
	uint8_t i=0;		// foot soldier
	
	DEBUG_OUTPUT_TEXT("\n\r  Executing Modem Test - Network Access \n\r");
	Timers_Delay (1000);
	
	// power up
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Power Up Modem\n\r");	
 	app_power_modemPower_on();
	i=10;
	if(verbose) {
		sprintf (debugBuffer, "      Wait for Modem to Boot (sec) %2d  ", i);
		Uart_Uart1WriteString (debugBuffer);
	}
	while(i>0) {
		Timers_Delay (1000);		
		i--;
		if(verbose) {
			sprintf (debugBuffer, "\b\b\b\b%2d  ", i);
			Uart_Uart1WriteString (debugBuffer);		
		}
	}
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r");
	
	// check for registration
	i=0;
	if(verbose) {
		sprintf (debugBuffer, "      Wait for Registration (sec) %2d  ", i);
		Uart_Uart1WriteString (debugBuffer);
	}
	while(app_modem_isRegistered()==FALSE) {
		Timers_Delay (1000);	
		i++;
		if(i==30) {
			if(verbose) {
				DEBUG_OUTPUT_TEXT("\n\r      Registration Timeout\n\r");	
				DEBUG_OUTPUT_TEXT("      Power Down Modem\n\r");	
			}
			app_power_modemPower_off();	
			return FALSE; 
		}
		if(verbose) {
			sprintf (debugBuffer, "\b\b\b\b%2d  ", i);
			Uart_Uart1WriteString (debugBuffer);
		}
	}
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Registration Successful\n\r");

	if(verbose) DEBUG_OUTPUT_TEXT("      Query Tower for RSSI\n\r");		
	app_modem_sendATGetReply("AT+CSQ", "CSQ:", replyBuff, sizeof(replyBuff), 3000);
   tmpPtr = strstr (replyBuff, "CSQ:");
	if(verbose) {
		sprintf (debugBuffer, "      %s", tmpPtr);
		Uart_Uart1WriteString (debugBuffer);
	}
	
	if(verbose) DEBUG_OUTPUT_TEXT("      Power Down Modem\n\r");		
	app_power_modemPower_off();
	return TRUE;	// pass
}
		


/******************************************
 * Modem Test - Make a Call
 * 	description:
 *
 * 	rev:
 * 		Apr102015-jef: WiP
 */
bool testModemCall(bool verbose) {

	char 	commandBuffer[64];

	uint8_t i=0;		// foot soldier
	bool	testPass = FALSE;
	

	DEBUG_OUTPUT_TEXT("\n\r  Executing Modem Test - Make a Call \n\r");	
	Timers_Delay (1000);
	
	// power up
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Power Up Modem\n\r");	
 	app_power_modemPower_on();
	i=10;
	if(verbose) {
		sprintf (debugBuffer, "      Wait for Modem to Boot (sec) %2d  ", i);
		Uart_Uart1WriteString (debugBuffer);
	}
	while(i>0) {
		Timers_Delay (1000);		
		i--;
		if(verbose) {
			sprintf (debugBuffer, "\b\b\b\b%2d  ", i);
			Uart_Uart1WriteString (debugBuffer);		
		}
	}
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r");
	
	// check for registration
	i=0;
	if(verbose) {
		sprintf (debugBuffer, "      Wait for Registration (sec) %2d  ", i);
		Uart_Uart1WriteString (debugBuffer);
	}
	while(app_modem_isRegistered()==FALSE) {
		Timers_Delay (1000);	
		i++;
		if(i==30) {
			if(verbose) {
				DEBUG_OUTPUT_TEXT("\n\r      Registration Timeout\n\r");	
				DEBUG_OUTPUT_TEXT("      Power Down Modem\n\r");	
			}
			app_power_modemPower_off();	
			return FALSE; 
		}
		if(verbose) {
			sprintf (debugBuffer, "\b\b\b\b%2d  ", i);
			Uart_Uart1WriteString (debugBuffer);
		}
	}
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Registration Successful\n\r");	
	
	
	// establish connection with server
			//	app_modem_sendAT("AT+CMEE=2", "OK", 1000);	// make modem errors verbose	
	if(verbose) DEBUG_OUTPUT_TEXT("      Set Up Data Connection: \n\r");	
	if(verbose) DEBUG_OUTPUT_TEXT("        Define PDP Context \n\r");	
	testPass = app_modem_sendAT("AT+CGDCONT=3,\"IP\",\"vzwinternet\",\"\",0,0", "OK", 1000);
	if(testPass==FALSE) { app_power_modemPower_off();	return FALSE; }	
	if(verbose) DEBUG_OUTPUT_TEXT("        Set Socket Configuration Parameters \n\r");	
	testPass = app_modem_sendAT("AT#SCFG=3,3,300,90,600,50", "OK", 1000);
	if(testPass==FALSE) { app_power_modemPower_off();	return FALSE; }		
	if(verbose) DEBUG_OUTPUT_TEXT("        Activate PDP Context \n\r");
	testPass = app_modem_sendAT("AT#SGACT=3,1", "OK", 5000);	
	if(testPass==FALSE) { app_power_modemPower_off();	return FALSE; }	
	if(verbose) DEBUG_OUTPUT_TEXT("        Open Remote Connection via Socket \n\r");
			//	sprintf (commandBuffer, "at#sd=3,0,7,\"www.google.com\",255,1,0");
	sprintf (commandBuffer, "at#sd=3,0,10510,\"modules.telit.com\",255,1,0");
	testPass = app_modem_sendAT(commandBuffer, "CONNECT", 10000);
	if(testPass) {
		if(verbose) DEBUG_OUTPUT_TEXT("        Data Connection Established \n\r");	
		if(verbose) DEBUG_OUTPUT_TEXT("        Dropping Connection \n\r");			
		app_modem_dropDataConnection();
	}
	app_power_modemPower_off();
 	return testPass;	// pass | fail status
}
		
	
/******************************************
 * Sleep Mode for 15 Seconds (EM2)
 * 	description:
 *
 * 	rev:
 * 		Apr212015-jef: WiP
 */
bool testSleepMode15Sec(bool verbose) {
	
	bool 	testResult = FALSE;
	uint32_t i=0, j=0;		// foot soldier
	
	DEBUG_OUTPUT_TEXT("\n\r  Executing Sleep Mode Test - EM2 for 15 Seconds\n\r");
	Timers_Delay (1000);
	

	DEBUG_OUTPUT_TEXT("\n\r      WAIT! - Synchronizing with RTC\n\r");	
	i=app_rtc_seconds_get();
	while(i==app_rtc_seconds_get()) { };

	DEBUG_OUTPUT_TEXT("\n\r      Entering EM2 - Measure Current Now!\n\r");		
	Timers_Delay (100);

	EMU_EnterEM2(true); 

	j=app_rtc_seconds_get();
	DEBUG_OUTPUT_TEXT("\n\r      Wake Up - Resume Full Power Operation \n\r");	
	app_timers_enableMsecs();	// Apr212015-jef

	// sprintf (debugBuffer, "      i= %d, j= %d  \n\r",i,j);
	// Uart_Uart1WriteString (debugBuffer);

	if(j==i+2) return TRUE;
	else return FALSE;

}
		
	
/******************************************
 * Sleep Mode (EM2)
 * 	description:
 *
 * 	rev:
 * 		Apr222015-jef: WiP
 */
bool testSleepMode(bool verbose) {
	
	uint32_t i;
	
	DEBUG_OUTPUT_TEXT("\n\r  Entering Sleep Mode\n\r");
	DEBUG_OUTPUT_TEXT("\n\r    Cycle power to recover\n\r");
	DEBUG_OUTPUT_TEXT("    GPIO will be turned off\n\r");
	DEBUG_OUTPUT_TEXT("    Interrupts disabled\n\r");	
	
	Timers_Delay (2000);
	

	DEBUG_OUTPUT_TEXT("\n\r      WAIT! - Synchronizing with RTC\n\r");	
	i=app_rtc_seconds_get();
	while(i==app_rtc_seconds_get()) { };

	DEBUG_OUTPUT_TEXT("\n\r      Entering EM2 - Measure Current Now \n\r");		
	Timers_Delay (100);

	// Disable all GPIO
	GPIO->P[gpioPortA].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortB].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortC].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortD].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortE].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortF].DOUTCLR = 0xFFFF; 
	
	GPIO->P[gpioPortA].MODEL = 0;
	GPIO->P[gpioPortB].MODEL = 0;
	GPIO->P[gpioPortC].MODEL = 0;
	GPIO->P[gpioPortD].MODEL = 0;
	GPIO->P[gpioPortE].MODEL = 0;
	GPIO->P[gpioPortF].MODEL = 0; 
	
	GPIO->P[gpioPortA].MODEH = 0;
	GPIO->P[gpioPortB].MODEH = 0;
	GPIO->P[gpioPortC].MODEH = 0;
	GPIO->P[gpioPortD].MODEH = 0;
	GPIO->P[gpioPortE].MODEH = 0;
	GPIO->P[gpioPortF].MODEH = 0; 
	
	RTC_Enable(FALSE);
	
//	INT_Disable();
	
	EMU_EnterEM2(true); 
	
	return FALSE;	// never get here
}
		
/******************************************
 * helpScreen
 * 	description:
 *
 * 	rev:
 * 		Apr152015-jef: skeleton
 */
void helpScreen(void) {
	uint8_t tempChar;
	DEBUG_OUTPUT_TEXT(" Help Screen Text \n\r");
	DEBUG_OUTPUT_TEXT("\n\n\r press any key to continue \n\r"); 
	while(Uart_Uart1IsCharReady()==FALSE) {};
	tempChar = Uart_Uart1GetChar();  // empty buffer
}

/***************************************************************************//**
 * @brief
 *   Performs overall CMIU initialization - besides what is being done in
 *   autogen modules.
 *
 * @note
 *       
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_cmiu_init(void)
{
    uint32_t  startDelay;
    

    
    // CMU should be initialized first since if a module clock is disabled, the
    // registers of that module are not accessible and reading from such 
    // registers may return undefined values. Writing to registers of clock 
    // disabled modules have no effect. One should normally avoid accessing
    // module registers of a module with a disabled clock.
 // Apr222015-jef    app_cmu_init();	// ok Apr212015-jef
    
	// Initialize RTC module
	app_rtc_init();  // ok Apr212015-jef
    
    // Initialize GPIO module
 //Apr222015-jef   app_gpio_init();	//ok Apr212015-jef
    
    // Initialize Timers module
 // Apr222015-jef    app_timers_init();	//ok Apr212015-jef
    
    // Initialize ARB module
    //remove Apr212015-jef  ARB_Init();
    
    // Initialize Metrology module
 // Apr222015-jef    app_metrology_Init();	//ok Apr212015-jef
 
    // Initialize Error module 
    //remove Apr212015-jef 	Error_Init();
    
    // Initialize the bluetooth module
 // Apr222015-jef   app_ble_init();	//ok Apr212015-jef
       
    // Initialize App module   
    //remove Apr212015-jef  app_cmiu_app_init();
   

    // Delay before possible sleep so that debugger can attach to target
  // Apr222015-jef   for(startDelay = 0x1FFFFF; startDelay>0; startDelay--)
  // Apr222015-jef   {
  // Apr222015-jef       __NOP();
  // Apr222015-jef   } 
    
  // Apr222015-jef   DEBUG_OUTPUT_TEXT("CMIU Initialization is complete\n\r");
    
}
		
// end-of-file
