/******************************************************************************
*******************************************************************************
**
**         Filename: app_configuration.c
**    
**           Author: Troy Harstad
**          Created: 1/21/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_configuration.c
*
* @brief This file contains the application code configuration constants.
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>     
#include <stdbool.h>
#include "typedefs.h"
#include "app_configuration.h"



/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/


 
/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
 

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None


// The following data constants are located in flash memory at address 0x0800.
// These are located at this address by using the CODE linker 
// directive ?CO?Configuration(0x0800) inside of the linker control file.
// The current processor is the C8051F965, which is a 64 kB part.


// DEFAULT MIU CONFIGURATION FOR NORMAL MODE INTERVALS
const U_MIU_CONFIG_NORMAL uMIUConfigNormal =
{
//
// MIU Conifg
//
// Structure for Normal Mode MIU Configuration


    .sMIUConfigNormal.wARBReadInterval =            10,         // wARBReadInterval = 15 minutes 60sec*15min = 0x384
    .sMIUConfigNormal.byUARTCheckInterval =         3,          // byUARTCheckInterval = every 1 second
    .sMIUConfigNormal.wDatalogStoreInterval =       0x0405,     // wDatalogStoreInterval = 60 minutes 60sec*60min = 0xE10
    .sMIUConfigNormal.byDatalogType =               6,          // byDatalogType
    .sMIUConfigNormal.byMobileReadTXInterval =      7,  	    // byMobileReadTXInterval = 14 seconds    
    .sMIUConfigNormal.byMobileReadPowerLevel =      8,          // byMobileReadPowerLevel
    .sMIUConfigNormal.wFNReadTXInterval =           0x090A,     // wFNReadTXInterval = 7.5 minutes 60sec*7.5 = 0x01C2
    .sMIUConfigNormal.byFNReadPowerLevel =          0x0B,       // byFNReadPowerLevel
    .sMIUConfigNormal.wFNConfigTXInterval =         0x0C0D,     // wFNConfigTXInterval is 6 hours 60*60*6 = 0x5460
    .sMIUConfigNormal.byFNConfigPowerLevel =        0x0E,       // byFNConfigPowerLevel
    .sMIUConfigNormal.byCommandRXInterval =         0x0F,       // byCommandRXInterval should occur 5 seconds after the wMobileReadTXInterval
    .sMIUConfigNormal.wAuthSessionTimeout =         0x1011,	    // wAuthSessionTimeout
    .sMIUConfigNormal.byUARTCmdTestModeTimeout =    0x12,	    // byUARTCmdTestModeTimeout, 60 seconds
    .sMIUConfigNormal.byDatalogTxInterval =         0x13,       // byDatalogTxInterval (ms) = 20 ms
    .sMIUConfigNormal.wHPMobileReadTXInterval =     0x1415,     // wHPMobileReadTXInterval = 60 minutes,  60 * 60 = 0x0E10
    .sMIUConfigNormal.byHPMobileReadPowerLevel =    0x16,       // byHPMobileReadPowerLevel   
    .sMIUConfigNormal.wAntDetInterval =             0x1718,     // wAntDetInterval 3600 seconds = 60 minutes = 0x0E10
    .sMIUConfigNormal.byPingRXInterval =            0x19,       // byPingRXInterval - Checks for ping every 7 seconds when in ping mode
    .sMIUConfigNormal.byRfCommandModeDuration =     0x1A        // byRfCommandModeDuration - Stays in command mode for 35 seconds
                                                                // once a ping is received.  
};


// DEFAULT MIU CONFIGURATION FOR SWIPE MODE INTERVALS
const U_MIU_CONFIG_SWIPE uMIUConfigSwipe =
{
    .sMIUConfigSwipe.wARBReadInterval =             10,         // wARBReadInterval = 30 seconds
    .sMIUConfigSwipe.byUARTCheckInterval =          0x03,       // byUARTCheckInterval = every 1 second 
    .sMIUConfigSwipe.byMobileReadTXInterval =       0x04,       // byMobileReadTXInterval = 2 seconds    
    .sMIUConfigSwipe.byMobileReadPowerLevel =       0x05,       // byMobileReadPowerLevel
    .sMIUConfigSwipe.wFNReadTXInterval =            0x0607,     // wFNReadTXInterval = 10 seconds (DISABLED FOR NOW)
    .sMIUConfigSwipe.byFNReadPowerLevel =           0x08,       // byFNReadPowerLevel
    .sMIUConfigSwipe.wFNConfigTXInterval =          0x090A,     // wFNConfigTXInterval = 10 seconds (DISABLED FOR NOW)
    .sMIUConfigSwipe.byFNConfigPowerLevel =         0x0B,       // byFNConfigPowerLevel
    .sMIUConfigSwipe.byRV4SwipeStateTimer =         15,         // byRV4SwipeStateTimer is 2 minutes (120 seconds) for now
    .sMIUConfigSwipe.byCommandRXInterval =          0x0D        // byCommandRXInterval should occur 1 second after the wMobileReadTXInterval
};

// DEFAULT MIU SERVER CONFIGURATION
const U_MIU_SERVER_CONFIG uMIUServerConfig =
{
    // Telit test server
//    .sMIUServerConfig.dwServerPortNumber =          10510,      // dwServerPortNumber = 
//    .sMIUServerConfig.abyServerAddress =            "modules.telit.com",  // abyServerAddress = 
    // Barry local test server
    .sMIUServerConfig.dwServerPortNumber =          5559,      // dwServerPortNumber = 
    .sMIUServerConfig.abyServerAddress =            "70.63.161.250",  // abyServerAddress = 
};


// Placeholder for hardware version
// Rev. A = 1(since A is 1st letter).
const uint16_t wHardwareVersion = 1;
	

// This data is used when initializing the ASIC.  The data here is moved
// directly over to the ASIC upon the reading of an uninitilized ASIC.
// This data is configured by the factory based on specific meter
// requirements.  This data contains the initial values for UpCount, 
// DownCount, Ecoder Flags, etc.



// Reserve the remaining portion of this flash page so that
// sConfigImgInfo resides in the last six bytes of the flash page.
const uint8_t byConfigArrayFill[(2048 -  sizeof(wHardwareVersion) -
                                        sizeof(uMIUConfigNormal) - 
                                        sizeof(uMIUConfigSwipe) -
                                        sizeof(uMIUServerConfig) -
                                        6)];
						                //sizeof(sConfigImgInfo))] = 0xCC;




// Base Version and CRC32 of configuratioin space
// Must reside in the last 6 bytes of the configuration space (0xFFA-0xFFF).
const S_IMG_INFOBLK sConfigImgInfo =
{ 
	0x0116,			// Base Version of Firmware, should match sAppImgInfo.wVersion
					// at the time of manufacture. 
	0x5C63236A		// CRC32 of Configuration Space
};


// At this point the remaining code in this file resides outside
// of the configuariton memory space.
void Configuration_Init(void)
{
// SEE BELOW AS TO WHY WE ARENT CHECKING CRC FOR CONFIG SPACE

// 	// Verify that the configuration table CRC is valid
// 	if(!Configuration_VerifyCRC32())
// 	{
// 		// Set error in error module so an error packet it transmitted
// 		Error_ImageCRC_Configuration();
// 	}
}

void Configuration_InitHW(void)
{
}
// Bootloader will not run App if bad CRC in Config or App so no need to check
// config CRC here.  May want to check for KeyTable CRC for R900v5 config packet.

    
    
// // This function checks that the configuration data is not corrupt
// bool Configuration_VerifyCRC32(void)
// {
// 	// The bootloader performs a CRC32 check on the configuration 
// 	// image and stores the result at bl_crc (BIT3).  If that bit is a '1'
// 	// then the CRC was valid. A '0' indicates and invalid CRC for the 
// 	// config table, which generates a fatal error.
// 	if ((AppConfiguration_BLCRC_Retrieve() & BIT3) == 0)
//     {     
// 		// CRC-32 Error
// 		return FALSE;
//     }
//     
//     // No CRC-32 Error
//     return TRUE;
// }





