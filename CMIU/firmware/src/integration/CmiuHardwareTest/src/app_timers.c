/******************************************************************************
*******************************************************************************
**
**         Filename: app_timers.c
**    
**           Author: Barry Huggins
**          Created: 12/23/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_timers.c
*
* @brief This file contains some basic timer routines for the CMIU.
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "stdtypes.h"
#include "em_device.h"
#include "em_usart.h"
#include "efm32lg_uart.h"
#include "app_timers.h"
#include "typedefs.h"
#include "em_gpio.h"
#include "em_timer.h"
#include "em_letimer.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "app_timers.h"
#include "app_uart.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
 // Timer 0 is used for generating a timer interrupt at specific intervals,
 // which is used for reading registers in the ARB module.
 //
 // Timer 0 is setup in Up-Count mode.  The counter counts up until it reaches
 // the value in TIMER0_TOP, where it is reset to 0 before counting up again.
 //
 // TIMER0_TOP = (HFCLK / HFCLK_Prescaler / Interrupt Interval) - 1
 // To generate a clock at 1200 Hz we need an interrupt at 2 * 1200 so:
 // TIMER0_TOP_1200  = (14MHz / 1 / (1200*2) - 1 = 5832.33
 // TIMER0_TOP_1600  = (14MHz / 1 / (1600*2) -1 = 4374
 // TIMER0_TOP_3200  = (14MHz / 1 / (3200*2) - 1 = 2186.5
 // TIMER0_TOP_19200 = (14MHz / 1 / (19200*2) - 1 = 363.58
   
#define TIMER0_TOP_1200HZ       5832        // 1200 Hz for E-Coder/ProRead (416 us INT interval)
#define TIMER0_TOP_1600HZ       4374        // 1600 Hz for ARBV 2nd Pass  (312 us INT interval)
#define TIMER0_TOP_3200HZ       2187        // 3200 Hz for ARBV 1st Pass  (156 us INT interval)
#define TIMER0_TOP_19200HZ      364         // 19.2 kHz for Dual Port (26 us INT interval)


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static void app_timers_timer0_setup(void);
static void app_timers_leTimer_setup(void);
static void Timers_Timer0_Callback_Safe(void);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
volatile static uint32_t msTicks; /* counts 1ms timeTicks */
volatile PFN_CALLBACK_VOID_VOID pfnTimer0_Int_Callback; 
volatile bool letimerComplete = true; 
volatile bool timer0_int_of_occurred = false;

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/



/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/



/***************************************************************************//**
 * @brief
 *   Initializes timer module
 *
 * @note
 *   
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_timers_init(void)
{
    // Setup Timer 0
    app_timers_timer0_setup();
    
    // Setup Low Energy Timer
    app_timers_leTimer_setup();
}



/***************************************************************************//**
 * @brief
 *   ISR for the system timer tick
 *
 * @note
 *   This function is the Interrupt Service Routine for the system timer tick.
 *   Currently it is called every 1 msec.
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void SysTick_Handler(void)
{
    msTicks++;       /* increment counter necessary in Delay()*/
}

/***************************************************************************//**
 * @brief
 *   Delays for the specified number of msecs.
 *
 * @note
 *   This function delays for the specified number of msecs.
 *       
 *
 * @param[in] dlyTicks
 *   Indicates the number of msecs to delay.
 *
 * @return none
 ******************************************************************************/
void Timers_Delay(uint32_t dlyTicks)
{

    uint32_t curTicks;

    curTicks = msTicks;
    while ((msTicks - curTicks) < dlyTicks) 
        ;

/*
    volatile uint32_t i,j;

    for (i=0; i<dlyTicks; i++)
        for (j=0; j<3300; j++)
            ; // do nothing
*/	 
}

/***************************************************************************//**
 * @brief
 *   Delays for the specified number of msecs.
 *
 * @note
 *   This function is used by the Nordic API and just calls our existing
 *   app_timers_delay.
 *       
 *
 * @param[in] dlyTicks
 *   Indicates the number of msecs to delay.
 *
 * @return none
 ******************************************************************************/
void delay(uint32_t dlyTicks)    
{
    Timers_Delay (dlyTicks);
}

/***************************************************************************//**
 * @brief
 *   Delays for approximately the specified number of msecs.
 *
 * @note
 *   This function just uses a loop since the msec ISR is not running.
 *       
 *
 * @param[in] dlyTicks
 *   Indicates the number of msecs to delay.
 *
 * @return none
 ******************************************************************************/
void Timers_Loop_Delay(uint32_t dlyTicks)
{
    uint32_t i,j;

    for (i=0; i<dlyTicks; i++)
        for (j=0; j<3300; j++)
            ; // do nothing
    
}

/***************************************************************************//**
 * @brief
 *   Function returns the current number of msecs.
 *
 * @note
 *   This function returns the current number of milliseconds. This counter 
 *     will wrap back to 0 approximately every 49 days.
 *       
 *
 * @param[in] none
 *
 * @return current count of the number of msecs.
 ******************************************************************************/
uint32_t Timers_GetMsTicks()
{
    return msTicks;
}

/***************************************************************************//**
 * @brief
 *   Enables the 1 msec tick ISR.
 *
 * @note
 *   This function enables the 1 msec ISR to keep us with the number of msecs. 
 *     This timer is only enabled during operations where the timing is needed.
 *       
 *   We may have to add intelligence to determine more than 1 system ever
 *     enables the 1 msec timer so we will know when to disable them.
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void app_timers_enableMsecs()
{
    SysTick->CTRL  |= SysTick_CTRL_ENABLE_Msk; 
}

/***************************************************************************//**
 * @brief
 *   Disables the 1 msec tick ISR.
 *
 * @note
 *   This function disables the 1 msec ISR to keep us with the number of msecs. 
 *     This timer is only enabled during operations where the timing is needed.
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void app_timers_disableMsecs()
{
    SysTick->CTRL  &= ~(SysTick_CTRL_ENABLE_Msk); 
}







/***************************************************************************//**
 * @brief
 *   Setup Timer 0
 *
 * @note
 *   This function sets up timer 0 to be used during ARB communcation, defaults
 *   to 1200 Hz but is not running.
 *       
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/ 
static void app_timers_timer0_setup(void)
{
    
    /* Enable clock for TIMER0 */
    CMU_ClockEnable(cmuClock_TIMER0, true);  
    
    
    /* Select TIMER0 parameters */  
    TIMER_Init_TypeDef timerInit =
    {
    .enable     = false,                   
    .debugRun   = false,                    
    .prescale   = timerPrescale1,           
    .clkSel     = timerClkSelHFPerClk, 
    .fallAction = timerInputActionNone, 
    .riseAction = timerInputActionNone, 
    .mode       = timerModeUp, 
    .dmaClrAct  = false,
    .quadModeX4 = false, 
    .oneShot    = false, 
    .sync       = false, 
    };

    /* Enable overflow interrupt */
    TIMER_IntEnable(TIMER0, TIMER_IF_OF);

    /* Enable TIMER0 interrupt vector in NVIC */
    NVIC_EnableIRQ(TIMER0_IRQn);

    /* Set TIMER Top value */
    app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_1200HZ);

    /* Configure TIMER */
    TIMER_Init(TIMER0, &timerInit);

    // Set to safe callback function
    Timers_Timer0_Int_Callback_Set(Timers_Timer0_Callback_Safe); 
}



/***************************************************************************//**
 * @brief
 *   Setup Timer 0 interrupt interval and enable Timer 0
 *
 * @note
 *   This function sets up timer 0 interrupt interval to support various ARB
 *   frequencies.  Timer 0 is enabled.   Assumes 14 MHz HFCLK
 *       
 *
 * @param[in] Timer0_IntInterval_TypeDef
 *
 * @return None
 ******************************************************************************/ 
void app_timers_timer0_setupIntInterval(Timer0_IntInterval_TypeDef intervalType)
{
    switch(intervalType)
    {
                
        case TIMER0_INT_INTVERAL_1200HZ:
        {
            TIMER_TopSet(TIMER0, TIMER0_TOP_1200HZ);   
            break;  
        }
        
        case TIMER0_INT_INTVERAL_1600HZ:
        {
            TIMER_TopSet(TIMER0, TIMER0_TOP_1600HZ); 
            break;            
        }
        
        case TIMER0_INT_INTVERAL_3200HZ:
        {
            TIMER_TopSet(TIMER0, TIMER0_TOP_3200HZ);   
            break;
        }
        case TIMER0_INT_INTVERAL_19200HZ:
        {
            TIMER_TopSet(TIMER0, TIMER0_TOP_19200HZ);   
            break;
        }        
        
        default:
        {
            // Default to what??!!!   
        }    
    }  
    
    // Enable Timer 0
    TIMER_Enable(TIMER0, true);
    
    // Enable Timer overflow (OF) interrupt
    TIMER_IntEnable(TIMER0, TIMER_IF_OF);
    
}



/***************************************************************************//**
 * @brief
 *   Disables Timer 0 and Timer 0 interrupt
 *
 * @note     
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/ 
void app_timers_timer0_disableIntInterval(void)
{
    // Enable Timer 0 overflow interrupt
    TIMER_IntDisable(TIMER0, TIMER_IF_OF);
    
    // Disable Timer 0
    TIMER_Enable(TIMER0, false);
    
}




/***************************************************************************//**
 * @brief
 *   Function to set the Timer 0 interrupt callback function
 *
 * @param[in] function pointer for timer 0 interrupt callback function
 *
 * @return None
 ******************************************************************************/
void Timers_Timer0_Int_Callback_Set(PFN_CALLBACK_VOID_VOID pfnCallBack)
{  
    pfnTimer0_Int_Callback = pfnCallBack; 
}



/***************************************************************************//**
 * @brief
 *   Safe Timer 0 callback function
 *
 * @note 
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void Timers_Timer0_Callback_Safe(void)
{ 
    // Safe, do nothing callback funtion
}



/***************************************************************************//**
 * @brief
 *   Setup Low Energy Timer
 *
 * @note
 *   This function sets up the low energy timer to run at the LFXO frequency 
 *   (32.768 kHz) in free mode when enabled.  By default the low energy timer
 *   is disabled by default though.  interrupt interval to support various ARB
 *   frequencies.   Assumes 14 MHz HFCLK
 *       
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_timers_leTimer_setup(void)
{
    /* Enable necessary clocks */
    CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);
    /* The CORELE clock is also necessary for the RTC and all
     low energy peripherals, but since this function
     is called before RTC_setup() the clock enable
     is only included here */
    CMU_ClockEnable(cmuClock_CORELE, true);
    CMU_ClockEnable(cmuClock_LETIMER0, true);  
    CMU_ClockEnable(cmuClock_GPIO, true);

    /* Set initial compare values for COMP0 */
    LETIMER_CompareSet(LETIMER0, 0, TIMER0_TOP_1200HZ);

    /* Set configurations for LETIMER 0 */
    const LETIMER_Init_TypeDef letimerInit = 
    {
    .enable         = false,                  /* Don't Start counting when init completed */
    .debugRun       = false,                  /* Counter shall not keep running during debug halt. */
    .rtcComp0Enable = false,                  /* Don't start counting on RTC COMP0 match. */
    .rtcComp1Enable = false,                  /* Don't start counting on RTC COMP1 match. */
    .comp0Top       = false,                  /* Load COMP0 register into CNT when counter underflows. COMP is used as TOP */
    .bufTop         = false,                  /* Don't load COMP1 into COMP0 when REP0 reaches 0. */
    .out0Pol        = 0,                      /* Idle value for output 0. */
    .out1Pol        = 0,                      /* Idle value for output 1. */
    .ufoa0          = letimerUFOANone,        /* No output on output 0 */
    .ufoa1          = letimerUFOANone,        /* No output on output 1*/
    .repMode        = letimerRepeatFree       /* Count until stopped by SW. */
    };

    /* Initialize LETIMER */
    LETIMER_Init(LETIMER0, &letimerInit); 
}






/***************************************************************************//**
 * @brief
 *   Setup Low Energy Timer Sleep Ticks
 *
 * @note
 *   This function can be used to put the device into EM2 for a number of
 *   32,768 clocks  
 *
 * @param[in] ticks
 *      The number of 32,768 clocks to be in EM2 power mode
 *
 * @return None
 ******************************************************************************/
void app_timers_leTimer_sleepTicks(uint16_t ticks)
{   
    // Enable CORELE clock, for some reason this is getting disabled but can't find why!!!
    CMU_ClockEnable(cmuClock_CORELE, true);
   
    // Clear LETIMER0 complete flag
    letimerComplete = FALSE;
    
    // Clear LETIMER0 counter to 0 
    LETIMER0->CMD = LETIMER_CMD_CLEAR;     
    
    // Set compare values for COMP0
    LETIMER_CompareSet(LETIMER0, 0, (0xFFFF - (ticks - 1)));
    
    // Clear any leftover LETIMER0 interrupts
    LETIMER_IntClear(LETIMER0, (LETIMER_IF_COMP1 | LETIMER_IF_COMP0 |LETIMER_IF_UF | LETIMER_IF_REP1 | LETIMER_IF_REP0));
    
    // Enable LETIMER0 COMP0 Interrupt
    LETIMER_IntEnable(LETIMER0, LETIMER_IF_COMP0);
    
    // Enable LETIMER0 interrupt vector in NVIC
    NVIC_EnableIRQ(LETIMER0_IRQn);
    
    // Enable LETIMER0
    LETIMER_Enable(LETIMER0, true);
        
    // Wait for debug UART bytes to complete since about to go to sleep
    DEBUG_WAITFORUART();
          
    // Wakes when LETIMER0 reaches 0
    while(letimerComplete == FALSE)
    {
        // Enter EM2 Power Mode - Restore clocks/oscillators on wake
        EMU_EnterEM2(true);  
            
    }
    
    // Disable LETIMER0
    LETIMER_Enable(LETIMER0, false); 
    
    // Disable LETIMER0 Interrupt
    LETIMER_IntDisable(LETIMER0, LETIMER_IF_COMP0);
    
    /* Enable LETIMER0 interrupt vector in NVIC */
    NVIC_DisableIRQ(LETIMER0_IRQn);
}



/***************************************************************************//**
 * @brief
 *   Function to clear ghost Timer 0 overflow flag (OF) ghost flag
 *
 * @note  
 *       
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_timer_timer0_clearIntFlag(void)
{  
    timer0_int_of_occurred = false;
}


/***************************************************************************//**
 * @brief
 *   Function to get ghost Timer 0 overflow flag (OF)
 *
 * @note
 *       
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
bool app_timer_timer0_intFlag_get(void)
{
    return timer0_int_of_occurred;
}



/***************************************************************************//**
 * @brief
 *   TIMER0_IRQHandler
 *
 * @note
 *   Interrupt Service Routine TIMER0 Interrupt Line
 *       
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void TIMER0_IRQHandler(void)
{ 
    /* Clear flag for TIMER0 overflow interrupt */
    TIMER_IntClear(TIMER0, TIMER_IF_OF);    
    
    timer0_int_of_occurred = true;
       
    // Call callback function
    //pfnTimer0_Int_Callback();     
    
}



/***************************************************************************//**
 * @brief
 *   LETIMER0_IRQHandler
 *
 * @note
 *   Interrupt Service Routine LETIMER0 Interrupt Line
 *       
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void LETIMER0_IRQHandler(void)
{    
    /* Clear flag for TIMER0 overflow interrupt */
    LETIMER_IntClear(LETIMER0, LETIMER_IF_COMP0);    
    
    // LE Timer overflow occurred
    letimerComplete = TRUE;
}



