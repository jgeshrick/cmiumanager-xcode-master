/******************************************************************************
*******************************************************************************
**
**         Filename: app_cmiu_app.c
**    
**           Author: Troy Harstad
**          Created: 1/21/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_cmiu_app.c
*
* @brief This file contains the main application code to schedule and
* perform tasks.
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "typedefs.h"
#include "app_cmiu_app.h"
#include "app_arb.h"
#include "app_configuration.h"
#include "app_metrology.h"
#include "app_uart.h"
#include "app_rtc.h"
#include "app_modem.h"
#include "app_mqtt.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static void app_cmiu_app_state_normal(void);
static void app_cmiu_app_state_swipe(void);
static void app_cmiu_app_normal_enter(void);
static void app_cmiu_app_swipe_enter(void);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// Time of next reading
static volatile U_NEPTUNE_TIME registerReadingTime;
static volatile bool registerReadingDue;

// Time of next reading
static volatile U_NEPTUNE_TIME diagnosticPacketTime;
static volatile bool diagnosticPacketDue;

// CMIU App State
static volatile uint8_t cmiu_app_state;

// Number of seconds left in Swipe State, once it reaches
// 0 the pfnapp_cmiu_app_State will be set to Normal State
static volatile uint8_t cmiuAppSwipeStateTimer;

// Register read result
static volatile uint8_t byRegisterReadResult;

// Local copy of CMIU time
static U_NEPTUNE_TIME appStateTime;

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/

/**************************************************************************//**
 * @brief
 *   Initializes CMIU Application module
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_cmiu_app_init(void)
{
    // Default to swipe state
    app_cmiu_app_swipe_enter();
    
    // Default this to retry so the packet is not built
    // This should be set by the register read occurring first
    byRegisterReadResult = REGISTER_READ_FAIL_RETRY;
}



/**************************************************************************//**
 * @brief
 *   CMIU application state tick function
 *
 * @note
 *   This function ticks the App Module by running the current state,
 *   which is either Swipe or Normal.
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
 void app_cmiu_app_tick(void)
{
    
    switch(cmiu_app_state)
    {
        case CMIU_APP_STATE_SWIPE:
        {
            app_cmiu_app_state_swipe();
            break;
        }
        
        case CMIU_APP_STATE_NORMAL:
        {
            app_cmiu_app_state_normal();
            break;
        }

        default:
        {
            // Should not get here so re-enter normal state
            app_cmiu_app_normal_enter();
            break;          
        }
    }   

}


/**************************************************************************//**
 * @brief
 *   Normal state of operation
 *
 * @note
 *   Normal state of operation.  Checks each individual task
 *   interval count has reached zero and performs that task
 *   if so.  The following tasks are performed here based on 
 *   the interval:
 *
 *       -Read register
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_state_normal(void)
{   
    // Get current time
    app_rtc_time_get(&appStateTime);
    
    
// ----------------------------------------------------------------------------
// TIME TO READ REGISTER?  --  PRIORITY X 
// ----------------------------------------------------------------------------    
    if(registerReadingDue)
    {
        // Reschedule next register reading     
        registerReadingTime.S64 = (appStateTime.S64 + uMIUConfigNormal.sMIUConfigNormal.wARBReadInterval);
        
        // Clear event due flag
        registerReadingDue = false;
        
        // Debug mode only - wait for UART to complete sending data
		DEBUG_WAITFORUART(); 
        
		// Read register
		byRegisterReadResult = ARB_ReadRegister();
		
        // Check register read status
        if(byRegisterReadResult == REGISTER_READ_PASS)
		{
			// The register read was successful
			// Calculate the metrology flags 
			app_metrology_CalculateFlags();
            
//             // Build updated Ecoder packet
//             Packet_Tx_Ecoder_Build();  
//     
//             // Store latest reading into RAM log
//             Datalog_ReadingLog_StoreReadingToRAM();
            
		}
		
        else if(byRegisterReadResult == REGISTER_READ_FAIL_RETRY)
		{
			// Retry in min 2 seconds, do not build updated Ecoder packet yet
			registerReadingTime.S64 = (appStateTime.S64 + 3);
		}     
        
        // Build updated Ecoder RF packet if register read passed or
        // there are no retries available
        else  //(byRegisterReadResult == REGISTER_READ_FAIL_NORETRY)
        {
//             // No retries available so build updated Ecoder packet
//             Packet_Tx_Ecoder_Build();   
//             
//             // Store latest reading into RAM log - in this case an error
//             Datalog_ReadingLog_StoreReadingToRAM();
        }
    }  

// ----------------------------------------------------------------------------
// TIME TO SEND DIAGNOSTIC PACKET?  --  PRIORITY X 
// ----------------------------------------------------------------------------     
    if (diagnosticPacketDue)
    {
//        app_mqtt_setupConnection();
    }

 }

/**************************************************************************//**
 * @brief
 *   Swipe state of operation
 *
 * @note
 *   Swipe state of operation.  Checks each individual task
 *   interval count has reached zero and performs that task
 *   if so.  The following tasks are performed here based on 
 *   the interval:
 *
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_state_swipe(void)
{
    // Get current time
    app_rtc_time_get(&appStateTime);

// ----------------------------------------------------------------------------
// TIME TO READ REGISTER?  --  PRIORITY 4
// ----------------------------------------------------------------------------    
    if(registerReadingDue)
    {
        // Reschedule next register reading     
        registerReadingTime.S64 = (appStateTime.S64 + uMIUConfigNormal.sMIUConfigNormal.wARBReadInterval);

        // Clear event due flag
        registerReadingDue = false;

        // Debug mode only - wait for UART to complete sending data
		DEBUG_WAITFORUART();          
        
		// Read register
		byRegisterReadResult = ARB_ReadRegister();
		
        // Check register read status
        if(byRegisterReadResult == REGISTER_READ_PASS)
		{
			// The register read was successful
            // Do no calcualte metrology flags in swipe mode

            // Build updated Ecoder packet
//             Packet_Tx_Ecoder_Build();
            
            // Ready to transmit now so schedule Mobile and FN messages immediately
            //byMobileReadTXInterval = 1;
            
            // Set the register to unknown to force an autodetect next read
            // only if there is enough time to complete it ( >20 seconds)
            if(cmiuAppSwipeStateTimer > 20)
            {
                ARB_ForceAutodetect();
            }                       
        }
        
        // Autodetect stage failed, go to next state
        else if(byRegisterReadResult == REGISTER_READ_FAIL_RETRY)
		{
			// Retry in >2 seconds, do not build updated Ecoder packet yet
            registerReadingTime.S64 = (appStateTime.S64 + 3);
		}     
        
        // Build updated Ecoder RF packet if register read passed or
        // there are no retries available
        else //(byRegisterReadResult == REGISTER_READ_FAIL_NORETRY)
        {
            // No retries available so build updated Ecoder packet
//             Packet_Tx_Ecoder_Build();
            
            // Ready to transmit now so schedule Mobile and FN messages immediately
            //byMobileReadTXInterval = 1;
            
            // Set the register to unknown to force an autodetect
            // only if there is enough time to complete it ( >20 seconds)
            if(cmiuAppSwipeStateTimer > 20)
            {
                ARB_ForceAutodetect();
            }
        }
        
    }    
 
// ----------------------------------------------------------------------------   
// Check if it is time to switch to Normal mode
// ----------------------------------------------------------------------------    
    // Putting this here means if the RTC is hijacked (for ARB or other) then
    // this will not be reached every second.  Putting this interval counter value
    // in app_cmiu_app_IntervalUpdate() seems like a waste since it is only used
    // for first two minutes of life.
    if(!cmiuAppSwipeStateTimer--)
    {
        DEBUG_OUTPUT_TEXT("Entering Normal Mode\r\n");
        app_cmiu_app_normal_enter();
    } 
}
    

/**************************************************************************//**
 * @brief
 *   Function to enter normal state of operation
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_cmiu_app_normal_enter(void)
{
    // Get current time
    app_rtc_time_get(&appStateTime);   


    // Set to the normal state
    cmiu_app_state = CMIU_APP_STATE_NORMAL;
    
    // Schedule first register reading in normal mode and clear event due flag     
    registerReadingTime.S64 = (appStateTime.S64 + uMIUConfigNormal.sMIUConfigNormal.wARBReadInterval);
    registerReadingDue = false;

    // schedule the first diagnostic packet to be sent in normal mode
    diagnosticPacketTime.S64 = (appStateTime.S64 + 10);  // bhtodo take the time from the configuration for testing change to 30 secs
    diagnosticPacketDue = false;
//    app_mqtt_init();
    
//     // Store reading value into RAM log when going from swipe mode to normal mode
//     Datalog_ReadingLog_StoreReadingToRAM();   
//     
//     // Set the number of packets that should be built that have the install
//     // bit set for the FSK Config and Reading packets.
//     Packet_Tx_FSK_Configuration_Build_InstallFlag_Set(1);
//     Packet_Tx_FSK_Reading_Build_InstallFlag_Set(4);

}



/**************************************************************************//**
 * @brief
 *   Function to enter swipe state of operation
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
static void app_cmiu_app_swipe_enter(void)
{
    // Get current time
    app_rtc_time_get(&appStateTime);
        
    // Set to the swipe state
    cmiu_app_state = CMIU_APP_STATE_SWIPE;
    
// Set the initial intervals values
    // Make sure the register gets read before the first transmssion. 
    // Schedule first register reading in swipe mode mode     
    registerReadingTime.S64 = (appStateTime.S64 + 2);
    registerReadingDue = false;  
    
    // Set length of time to stay in swipe mode
    cmiuAppSwipeStateTimer = uMIUConfigSwipe.sMIUConfigSwipe.byRV4SwipeStateTimer;
      
    DEBUG_OUTPUT_TEXT("Entering Swipe Mode\r\n");
}



/**************************************************************************//**
 * @brief
 *      Function checks the current time against the scheduled times for various
 *      tasks to be performed by the CMIU.  This is to be called once per second
 *      by RTC interrupt. 
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
void app_cmiu_app_intervalUpdate(void)
{      
    
    // Get current time
    app_rtc_time_get(&appStateTime);
    
    // Check if time to read a register
    if(appStateTime.S64 >= registerReadingTime.S64) 
{        
        registerReadingDue = true;
}

    if(appStateTime.S64 >= diagnosticPacketTime.S64)
    {
        diagnosticPacketDue = true;
    }

}   
  
       
    
/**************************************************************************//**
 * @brief
 *      Function retrieve CMIU app state
 *
 * @param[in] None
 *
 * @return None
 *****************************************************************************/
uint8_t app_cmiu_app_state_get(void)
    {
    return cmiu_app_state;
    }
    
    
    
    
