/******************************************************************************
*******************************************************************************
**
**         Filename: app_rtc.h
**    
**           Author: Troy Harstad
**          Created: 1/16/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_RTC_H
#define __APP_RTC_H



/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
//typedef int64_t NEPTUNE_TIME_T;


typedef union uTag_NEPTUNE_TIME
{
	int8_t  S8[8];
	int64_t S64;
}U_NEPTUNE_TIME;

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
#define RTC_FREQ    32768 

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_rtc_init(void);
extern void app_rtc_time_set(U_NEPTUNE_TIME * time);
extern void app_rtc_time_get(U_NEPTUNE_TIME * time);
uint32_t app_rtc_seconds_get(void);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_RTC_H

