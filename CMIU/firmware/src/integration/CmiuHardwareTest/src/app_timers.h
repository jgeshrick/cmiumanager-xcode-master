/******************************************************************************
*******************************************************************************
**
**         Filename: app_timers.h
**    
**           Author: Barry Huggins
**          Created: 12/31/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_TIMERS_H
#define __APP_TIMERS_H

#include "stdtypes.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
#include "typedefs.h"

/** Master mode transfer states. */
typedef enum
{
  TIMER0_INT_INTVERAL_1200HZ,     
  TIMER0_INT_INTVERAL_1600HZ,      
  TIMER0_INT_INTVERAL_3200HZ,   
  TIMER0_INT_INTVERAL_19200HZ
} Timer0_IntInterval_TypeDef;

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/

#define SLEEP_TICKS_100_US      3   //  (.0001 * 32768) = 3.2768
#define SLEEP_TICKS_200_US      6   //  (.0002 * 32768) = 6.55
#define SLEEP_TICKS_1_MS       33   // (.001 * 32768) = 32.768
#define SLEEP_TICKS_20_MS     656   // (.020 * 32768) = 655.36
#define SLEEP_TICKS_55_MS    1803   // (.055 * 32768) = 1802.24

// Used for plus pulse, found that by calculating it out each plus pulse was
// about 500 us too long, so shortened by ~500 us (value of 16).
#define SLEEP_TICKS_PLUS_PULSE_A    13  //(((1/1200) / 2) * 2 * 32768) =    27.3 - 15
#define SLEEP_TICKS_PLUS_PULSE_B    26  //(((1/1200) / 2) * 3 * 32768) =    40.9 - 15
#define SLEEP_TICKS_PLUS_PULSE_C    40  //(((1/1200) / 2) * 4 * 32768) =    54.6 - 15
#define SLEEP_TICKS_PLUS_PULSE_D    53  //(((1/1200) / 2) * 5 * 32768) =    68.2 - 15 
#define SLEEP_TICKS_PLUS_PULSE_E    67  //(((1/1200) / 2) * 6 * 32768) =    81.9 - 15
#define SLEEP_TICKS_PLUS_PULSE_F    81  //(((1/1200) / 2) * 7 * 32768) =    95.6 - 15 


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
void Timers_Delay(uint32_t dlyTicks);
void Timers_Loop_Delay(uint32_t dlyTicks);
uint32_t Timers_GetMsTicks(void);
void app_timers_enableMsecs(void);
void app_timers_disableMsecs(void);

extern void app_timers_init(void);

// Timer 0
extern void app_timers_timer0_setupIntInterval(Timer0_IntInterval_TypeDef intervalType);
extern void Timers_Timer0_Int_Callback_Set(PFN_CALLBACK_VOID_VOID pfnCallBack);
extern void app_timer_timer0_clearIntFlag(void);
extern bool app_timer_timer0_intFlag_get(void);
extern void app_timers_timer0_disableIntInterval(void);

// LETIMER
extern void app_timers_leTimer_sleepTicks(uint16_t ticks);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/



#endif // __APP_TIMERS_H

