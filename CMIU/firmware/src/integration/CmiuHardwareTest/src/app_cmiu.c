/******************************************************************************
*******************************************************************************
**
**         Filename: app_cmiu.c
**    
**           Author: Troy Harstad
**          Created: 1/26/2015
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
** Revision History:
**        1/26/2015: First created
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_cmiu.c
*
* @brief Performs CMIU initialization, executes main loop
* 
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include "typedefs.h"
#include "efm32lg332f256.h"
#include "stdtypes.h"
#include "autogen_init.h"
#include "app_arb.h"
#include "app_flash.h"
#include "app_ble.h"
#include "app_modem.h"
#include "app_uart.h"
#include "app_timers.h"
#include "app_power.h"
#include "app_rtc.h"
#include "app_gpio.h"
#include "app_cmu.h"
#include "app_cmiu_app.h"
#include "app_error.h"
#include "app_metrology.h"
#include "app_ble.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "em_timer.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static void app_cmiu_init(void);
static void app_cmiu_processInputs (void);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h
// Let the compiler check the typedefs.  For instance if int8_t ever becomes
// something other than 1 byte the [sizeof(int8_t) == 1] will become false, 
// which means the union member int8_err will have an array size of zero.  
// The compiler will give an error if the array size is set to zero.
// PC-lint will give several errors for this since it is an incomplete array
// and the mebers are not referenced so it is removed from the lint check using
// the #ifndef _lint directive.
#ifndef	_lint
union {
    char int8_err   [sizeof(int8_t)    == 1];
    char uint8_err  [sizeof(uint8_t)   == 1];
    char int16_err  [sizeof(int16_t)   == 2];
    char uint16_err [sizeof(uint16_t)  == 2];
    char int32_err  [sizeof(int32_t)   == 4];
    char uint32_err [sizeof(uint32_t)  == 4];
    char uint64_err [sizeof(uint64_t)  == 8];
    char int64_err  [sizeof(int64_t)   == 8];
    char ptr_err    [sizeof(uintptr_t) == 4];
}; //Anonymous
#endif

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None



/***************************************************************************//**
 * @brief
 *   main function
 *
 * @note
 *   Executes main loop once per second, sleeps in EM2 duration of one second
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
int main(void)
{ 

    /* Initialize chip */
    eADesigner_Init();
    gpio_Init();
    Uart_Uart1Setup();
    Uart_Uart0Setup();
    
    USART_Enable(USART2, usartEnable);
    
    DEBUG_OUTPUT_TEXT("CMIU RESET \r\n"); 

    // Initialize CMIU, this overwrites some autogen GPIO/Clock settings in an 
    // effort to configure the board to reach 2uA sleep current.
    app_cmiu_init();

   
    // Infinite Loop
    while (1)
	{
        // Set PA.4 upon wakeup
        DEBUG_INSTRUCTION(GPIO_PinOutSet(gpioPortA, 4));      

        // Toggle PA.3 on every wakeup (1 second)
        DEBUG_INSTRUCTION(GPIO_PinOutToggle(gpioPortA, 3));

        // Tick CMIU Application module once per second        
        app_cmiu_app_tick();
        
        // Process any necessary bluetooth event
        app_ble_process();
        
        // Look for any input from the UART
        app_cmiu_processInputs();

        // Wait for UART to complete before entering sleep mode
        DEBUG_WAITFORUART();  
        
        // Clear PA.4 before going to sleep
        DEBUG_INSTRUCTION(GPIO_PinOutClear(gpioPortA, 4));      

        GPIO_PinOutSet(gpioPortC, 9);  // enable modem flow control

         // Enter EM2 and wake up up to one second later
        EMU_EnterEM2(true);  

        GPIO_PinOutClear(gpioPortC, 9); // disable modem flow control

	}   
}


/***************************************************************************//**
 * @brief
 *   Performs overall CMIU initialization - besides what is being done in
 *   autogen modules.
 *
 * @note
 *       
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_cmiu_init(void)
{
    uint32_t  startDelay;
    

    
    // CMU should be initialized first since if a module clock is disabled, the
    // registers of that module are not accessible and reading from such 
    // registers may return undefined values. Writing to registers of clock 
    // disabled modules have no effect. One should normally avoid accessing
    // module registers of a module with a disabled clock.
    app_cmu_init();
    
    // Initialize RTC module
    app_rtc_init();
    
    // Initialize GPIO module
    app_gpio_init();
    
    // Initialize Timers module
    app_timers_init();
    
    // Initialize ARB module
    ARB_Init();
    
    // Initialize Metrology module
    app_metrology_Init();
 
    // Initialize Error module 
    Error_Init();
    
    // Initialize the bluetooth module
    app_ble_init();
       
    // Initialize App module   
    app_cmiu_app_init();
   

    // Delay before possible sleep so that debugger can attach to target
    for(startDelay = 0x1FFFFF; startDelay>0; startDelay--)
    {
        __NOP();
    } 
    
    DEBUG_OUTPUT_TEXT("CMIU Initialization is complete\n\r");
    
}

/***************************************************************************//**
 * @brief
 *   Looks for input on the debug UART. 
 *
 * @note
 *   Currently these are only debug commands but they will be extended to do
 *   more diagnostics
 *       
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void   app_cmiu_processInputs ()
{
    char    tempChar;
    char    testBuffer[] = "hi bob";
    // try getting a char from the debug UART
    if (Uart_Uart1IsCharReady())
    {
        tempChar = Uart_Uart1GetChar();
        if (tempChar == 'b')
        {
            DEBUG_OUTPUT_TEXT("sending data\n"); 
            app_ble_sendData ((uint8_t *)testBuffer, strlen(testBuffer));
        }
        else if (tempChar == 't')
        {
            //DEBUG_OUTPUT_TEXT("checking battery\n"); 
            //Ble_BluetoothGetBattery ();
            DEBUG_OUTPUT_TEXT("checking temp\n"); 
            app_ble_getTemperature ();
        }
        else if (tempChar == 'n')
        {
            app_ble_startup();
        }
        else if (tempChar == 'f')
        {
            uint16_t    flashDeviceID;
            uint8_t     flashStatusRegister;
            char        tempBuff[64];

            Uart_Uart1PutData("enable flash\n\r", 14);
            Flash_PowerOn();
            Timers_Loop_Delay (3000);
            flashDeviceID = Flash_ReadDeviceIdentification();
            Timers_Loop_Delay (1000);
            flashStatusRegister = Flash_ReadStatusRegister();
            
            sprintf (tempBuff, "flash - device ID: %x status: %x\n\r", flashDeviceID, flashStatusRegister);
            DEBUG_OUTPUT_TEXT (tempBuff);
    
            Timers_Loop_Delay (5000);
            Flash_TestWriteRead ();
        }
        else if (tempChar == 'p')
        {
            uint8_t     ch;
            DEBUG_OUTPUT_TEXT("modem passthrough\n");
            while (1)
            {
                if (Uart_Uart0IsCharReady())
                {
                    ch = Uart_Uart0GetChar();
                    Uart_Uart1PutChar(ch);
                }
                if (Uart_Uart1IsCharReady())
                {
                    ch = Uart_Uart1GetChar();
                    // check for ctrl-c to break
                    if (ch == 0x03)
                        break;
                    Uart_Uart0PutChar(ch);
                }
            }
        }
    }
  
}    

