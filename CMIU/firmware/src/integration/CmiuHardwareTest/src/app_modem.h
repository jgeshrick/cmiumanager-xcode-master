/******************************************************************************
*******************************************************************************
**
**         Filename: Modem.h
**    
**           Author: Barry Huggins
**          Created: 12/31/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/
#include <time.h>

// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __MODEM_H
#define __MODEM_H

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
#define MAX_INCOMING_BUFFER_SIZE        512

typedef enum
{
    DATA_CONN_EVENT_START,
    DATA_CONN_EVENT_TIMEOUT,
    DATA_CONN_EVENT_INC_DATA
} E_DATA_CONN_EVENT_T;

typedef enum
{
    DATA_CONN_STATE_DEFINE_CONTEXT,
    DATA_CONN_STATE_CONFIG_CONTEXT,
    DATA_CONN_STATE_ACTIVATE_CONTEXT,
    DATA_CONN_STATE_ACTIVATE_CONTEXT_WAIT_RESPONSE,
    DATA_CONN_STATE_CONNECT,
    DATA_CONN_STATE_CONNECT_WAIT_RESPONSE,
    DATA_CONN_STATE_CONNECTED,
} E_DATA_CONN_STATE_T;

typedef enum
{
    MODEM_CONN_SUCCESS = 0,
    MODEM_CONN_IN_PROGRESS = 1,
    MODEM_CONN_FAILURE_CONTEXT_ACTIVATION = -1,
    MODEM_CONN_FAILURE_CONNECT = -2,
} E_MODEM_CONN_RESULT_T;

typedef struct
{
    E_DATA_CONN_STATE_T     connState;
    E_MODEM_CONN_RESULT_T   modemConnResult;
    uint8_t                 incDataBuffer[MAX_INCOMING_BUFFER_SIZE];
    uint16_t                currStateWaitTime;    
} S_DATA_CONN_STATE_INFO;

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/

// These defines are used to get to the different fields in the #CCLK response.
//  The response line will come back with the '#' as the first char:
//          #CCLK: "15/02/12,16:22:29-20,0"
#define MODEM_CCLK_RESPONSE_START           8
#define MODEM_CCLK_RESPONSE_START_YEAR      (MODEM_CCLK_RESPONSE_START)
#define MODEM_CCLK_RESPONSE_START_MONTH     (MODEM_CCLK_RESPONSE_START_YEAR + 3)
#define MODEM_CCLK_RESPONSE_START_DAY       (MODEM_CCLK_RESPONSE_START_MONTH + 3)
#define MODEM_CCLK_RESPONSE_START_HOUR      (MODEM_CCLK_RESPONSE_START_DAY + 3)
#define MODEM_CCLK_RESPONSE_START_MINUTE    (MODEM_CCLK_RESPONSE_START_HOUR + 3)
#define MODEM_CCLK_RESPONSE_START_SECOND    (MODEM_CCLK_RESPONSE_START_MINUTE + 3)
#define MODEM_CCLK_RESPONSE_START_TIMEZONE  (MODEM_CCLK_RESPONSE_START_SECOND + 2)
#define MODEM_CCLK_RESPONSE_START_ISDST     (MODEM_CCLK_RESPONSE_START_TIMEZONE + 4)

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
void    app_modem_init(void);
bool    app_modem_isRegistered(void);
bool    app_modem_shutdown (void);
bool    app_modem_setup (void);
bool    app_modem_sendAT(char* str, char* expectResp, uint32_t timeout_ms);
bool    app_modem_sendATGetReply(char* str, char* expectResp, char *responseBuff, uint8_t responseBuffLen, uint32_t timeout_ms);
bool    app_modem_getCurrentTime (struct tm 	*pCurrTime, int8_t   *timezone);
void    app_modem_testTimeFunction(void);
bool    app_modem_setRTC(void);
void    app_modem_startDataConnSetup(void);
void    app_modem_process_tick(void);
bool    app_modem_dropDataConnection(void);



/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None

#endif // __MODEM_H

