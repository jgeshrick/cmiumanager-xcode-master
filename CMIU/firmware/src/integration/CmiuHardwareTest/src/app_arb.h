/******************************************************************************
*******************************************************************************
**
**         Filename: arb.h
**    
**           Author: Nick Sinas
**          Created: 12/13/2012
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: This is the header file for the ARB interface
**
** Revision History:
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/

// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef HEADER_ARB_H
#define HEADER_ARB_H

#include "typedefs.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/

// Register Read return values
#define REGISTER_READ_PASS			0x01
#define REGISTER_READ_FAIL_NORETRY	0x00
#define REGISTER_READ_FAIL_RETRY	0xF0


// Register types as specified in ETI 05-03
// Valid values are 0-15
#define REGISTER_ARBV          	0x00
#define REGISTER_PROREAD       	0x01
#define REGISTER_ECODER        	0x02
#define REGISTER_ECODER2       	0x03
//#define FUTURE                0x04
#define REGISTER_SENSUS_FIXED  	0x05
#define REGISTER_SENSUS_VAR    	0x06
#define REGISTER_ULTRASONIC    	0x07
//#define FUTURE                0x08
//#define FUTURE                0x09
//#define FUTURE                0x0A
//#define FUTURE                0x0B
//#define FUTURE                0x0C
//#define FUTURE                0x0D
//#define FUTURE                0x0E
#define REGISTER_UNKNOWN       	0x0F
#define REGISTER_USEDETECTED    0xEE


#define PLUS_INTERVAL_NEXT 	0xFF
#define PLUS_INTERVAL_A 	0x02
#define PLUS_INTERVAL_B 	0x03
#define PLUS_INTERVAL_C 	0x04
#define PLUS_INTERVAL_D 	0x05
#define PLUS_INTERVAL_E 	0x06
#define PLUS_INTERVAL_F 	0x07


// Define reading values for the different error types   
    //-Clocking timeout, no data from register or no register attached(this is the only one listed below)
    //-Data parity error
    //-Data checksum error
    //-Neptune format but not correct number of ETB�s (5)
    //-Sensus variable format with reading outside 34 byte buffer
    //-Sensus variable format with no �R� in reading data
    //-Neptune format with reading outside 34 byte buffer
    //-Unsupported format, first byte of data not supported (must  be STX, Sensus V or R)
    //-Data format code error, second byte of data not supported (must be 1-Basic, 2-Plus, 3-EV2)
    //-ARBV reading data that didn�t match a �1� or �0� bit pattern
#define READING_ERROR_GENERAL           0x07000000      
    
    //-Reading contains non-numeric data such as '-' or 'h' etc
#define READING_ERROR_DATAFORMAT        0x0700000F   
    
    //-Reading does not exist.  This is used in the case of the RAM reading log being
    // initialized on a reset.  Now way to have a 15-min and 12-hr old reading yet so
    // it is set to this.
#define READING_ERROR_READINGNOTTAKEN   0x07000010

    //-Reading error unknown
#define READING_ERROR_UNSPECIFIED       0x07FFFFFF


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void ARB_InitHW(void);
extern void ARB_Init(void);
extern uint8_t ARB_ReadRegister(void);
extern uint8_t ARB_ReadRegister_OnDemand(uint8_t byNewPlusInterval, uint8_t byNewRegisterType);
extern void ARB_ForceAutodetect(void);
extern S_REGISTER_DATA* ARB_RegisterData_Retrieve(void);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif
