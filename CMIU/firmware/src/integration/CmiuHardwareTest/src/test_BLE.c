/******************************************************************************
*******************************************************************************
**
**         Filename: test_Bt.c/test_EM2.c/test_cmiu.c
**    
**           Author: Jeff Frey
**           Created: 4/02/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**				- derivative of NeptuneProto2.c (Barry Huggins)
**				- menu driven test of hardware components on cmiu:
**						Flash Memory (WiP)
**						BTLE Radio (have not started)
**						Cellular Modem (complete)
**
**		Notes: 
**			Modfied other files from Barry's baseline: 
**				app_flash.c, autogen_init.c, app_power.c, app_modem.c
**
**
**
**
** Revision History:
**		Jun052015-jef: clean up
**		Jun012015-jef: 
**				[1] added version # to menu
** 			[2] chasing down SPI issue - BLE2Flash
**				[3] general code clean up
**		May212015-jef: ble loopback test complete
** 	May182015-jef: turn into global static variables: verbose, btrp, cmp, ble_ad
**		May152015-jef: add BLE device address & BLE loopback tests
**		May14/132015-jef: migrating to jefs_ble_process()
** 	Apr292015-jef: EM2 tests (s & t) perfected, complete
** 	APr272015-jef: added toggle_modem_pwr() & toggle_Bt_radio_pwr()
** 	Apr212015-jef: [1] added Sleep Mode (EM2) Test
**						[2] copied app_cmiu_init() to here from app_cmiu.c
** 	Apr172015-jef: [1] modem tests complete
**		Apr102015-jef: 
**		[1] new UI to match Tim's HW test breakdown
**		Apr082015-jef: flesh out Flash Memory test
**		Apr072015-jef: 
**		[1] test framework created
**		[2] Flash Memory test installed into framework 
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  	information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file test_cmiu.c
*
* @brief quick & dirty test code to verify HW functionality
* 
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "typedefs.h"
#include "efm32lg332f256.h"
#include <stdbool.h>
#include "autogen_init.h"
#include "app_arb.h"
#include "app_flash.h"
#include "app_ble.h"
#include "app_modem.h"
#include "app_uart.h"
#include "app_timers.h"
#include "app_power.h"
#include "app_rtc.h"
#include "app_gpio.h"
#include "app_cmu.h"
#include "app_cmiu_app.h"
#include "app_error.h"
#include "app_metrology.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "em_timer.h"
#include "em_int.h"
#include "em_rtc.h"
#include "..\NordicAPI\lib_aci.h"
#include "CmiuAppImageInfo.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/

// eventProcessed - May152015-jef
#define MASK_EVENTOPCODE		0xFF000000
#define MASK_OPERATINGMODE		0x00FF0000
#define MASK_CMDOPCODE			0x0000FF00
#define MASK_CMDSTATCODE		0x000000FF
#define SHIFT_EVENTOPCODE		24
#define SHIFT_OPERATINGMODE	16
#define SHIFT_CMDOPCODE			 8


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/

bool   App_ProcessInputs (void);


static bool testFlashId(void);
static bool testFlashWr(void);
static bool testBleId(void);
static bool testBleLoopback(void);
static bool testBlePair(void);
static bool testBleCommRd(void);
static bool testBleCommWr(void);
static bool testModemImei(void);
static bool testModemSim(void);
static bool testModemNetwork(void);
static bool testModemCall(void);
static bool testSleepMode(void);			// Apr212015-jef
static bool testSleepMode15Sec(void);		// Apr222015-jef
static void app_cmiu_init(void);		// Apr212015-jef
static bool toggle_Bt_radio_pwr(void);
static bool toggle_modem_pwr(void);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
uint16_t    flashDeviceID;
uint8_t     flashStatusRegister;
char        debugBuffer[64];
char    	replyBuff[128];


static 	bool	verbose = false;
static 	bool	btrp = false;		// Bluetooth Radio Powered
static 	bool	cmp = false;		// Cell Modem Powered
static 	bool	ble_ad = false;		// Bluetooth advertise   May152015-jef: was true prior

static S_MIU_IMG_INFO myImageInfo;

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h
// Let the compiler check the typedefs.  For instance if int8_t ever becomes
// something other than 1 byte the [sizeof(int8_t) == 1] will become false, 
// which means the union member int8_err will have an array size of zero.  
// The compiler will give an error if the array size is set to zero.
// PC-lint will give several errors for this since it is an incomplete array
// and the mebers are not referenced so it is removed from the lint check using
// the #ifndef _lint directive.
#ifndef	_lint
union {
    char int8_err   [sizeof(int8_t)    == 1];
    char uint8_err  [sizeof(uint8_t)   == 1];
    char int16_err  [sizeof(int16_t)   == 2];
    char uint16_err [sizeof(uint16_t)  == 2];
    char int32_err  [sizeof(int32_t)   == 4];
    char uint32_err [sizeof(uint32_t)  == 4];
    char uint64_err [sizeof(uint64_t)  == 8];
    char int64_err  [sizeof(int64_t)   == 8];
    char ptr_err    [sizeof(uintptr_t) == 4];
}; //Anonymous
#endif

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None


/***************************************************************************//**
 * @brief
 *   main function
 *
 * @note
 *   Executes main program
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
int main(void) {


	
#ifdef APP_TEST
    volatile uint32_t timerVal;
#endif // APP_TEST

    /* Initialize chip */

 	eADesigner_Init();
	gpio_Init();
	Uart_Uart1Setup();		// Apr022015-jef: ASYNC_57600/ monitor
	Uart_Uart0Setup();		// Apr022015-jef: ASYNC_tbr/ cellular modem
	USART_Enable(USART2, usartEnable);	// Apr022015-jef: SPI/ Flash & Bt radio

	CMU_ClockEnable(cmuClock_USB, true);
 
	// give time to erase if stuck in low power mode
	Timers_Delay (5000);   

	app_cmiu_init();	// Apr222015-jef: modified
	
	app_timers_enableMsecs();	// Apr212015-jef
	
	USB->CTRL=1<<16;	// Apr282015-jef: disable USB voltage regulator

  	DEBUG_OUTPUT_TEXT("   ~~ system initialized ~~ \n\r");
    
    sprintf (debugBuffer, "%s\n\r",sAppImgInfo);


	/*******************************************************
	 * simple User Interface
	 *
	 *
	 */
		
	uint8_t     tempChar;
	
	bool			testResult = false;	// TRUE == pass

	while (1) {

		Timers_Delay (100);

		// Display User Screen
		DEBUG_OUTPUT_TEXT("\n\n\n\n\n\n\n\n\n\n\r");
		DEBUG_OUTPUT_TEXT("     Neptune Board Functional Test  v_0.0.3 \n\r");	
		DEBUG_OUTPUT_TEXT("    ------------------------------------------- \n\n\r");
		DEBUG_OUTPUT_TEXT("     'v'  toggle VERBOSE output \n\r");
			// Apr152015-jef		DEBUG_OUTPUT_TEXT("     'h'  HELP screen \n\r");
		DEBUG_OUTPUT_TEXT("     'b'  Flash: Read Chip ID \n\r");		
		DEBUG_OUTPUT_TEXT("     'c'  Flash: Write & Read \n\r");	
		DEBUG_OUTPUT_TEXT("     'd'  Modem: Power Up & Read IMEI \n\r");			
		DEBUG_OUTPUT_TEXT("     'e'  Modem: SIM Communication \n\r");
		DEBUG_OUTPUT_TEXT("     'f'  Modem: Network Access \n\r");
		DEBUG_OUTPUT_TEXT("     'g'  Modem: Make a Call \n\r");						
		DEBUG_OUTPUT_TEXT("     'i'  BLE: Read Device Address \n\r");
		DEBUG_OUTPUT_TEXT("     'j'  BLE: Loopback \n\r");			
		DEBUG_OUTPUT_TEXT("     'k'  BLE: Pair (Advertise/Connect/Discovery)\n\r");
		DEBUG_OUTPUT_TEXT("     'l'  BLE: Communicate - Peripheral Read\n\r");
		DEBUG_OUTPUT_TEXT("     'm'  BLE: Communicate - Peripheral Write\n\r");
		DEBUG_OUTPUT_TEXT(" \n\r      Must disconnect debugger to enter EM2 \n\r");		
		DEBUG_OUTPUT_TEXT("     's'  EM2: Sleep Mode for 15 Seconds\n\r");
		DEBUG_OUTPUT_TEXT("     't'  EM2: Sleep Mode (no exit)\n\r");
		DEBUG_OUTPUT_TEXT(" \n\r      For Radio Emissions Testing \n\r");		
		DEBUG_OUTPUT_TEXT("     'y'  BLE: Toggle Bluetooth Radio Power On/Off \n\r");
		DEBUG_OUTPUT_TEXT("     'z'  Modem: Toggle Cell Radio Power On/Off \n\r");		
		
		
		DEBUG_OUTPUT_TEXT("\n\r");	
		if(verbose) DEBUG_OUTPUT_TEXT("    Verbose Mode: ON \n\r");		
		else DEBUG_OUTPUT_TEXT("    Verbose Mode: OFF \n\r");	
		if(btrp) DEBUG_OUTPUT_TEXT("    Bluetooth Radio Powered: ON \n\r");
		else DEBUG_OUTPUT_TEXT("    Bluetooth Radio Powered: OFF \n\r");	
		if(cmp) DEBUG_OUTPUT_TEXT("    Cell Modem Powered: ON \n\r");
		else DEBUG_OUTPUT_TEXT("    Cell Modem Powered: OFF \n\r");

		DEBUG_OUTPUT_TEXT("\n\n\r");
 		Timers_Delay (100);		

		// Get User Input
 		while(Uart_Uart1IsCharReady()==FALSE) {
			if(btrp == true) jefs_ble_process(ble_ad, verbose);
      };
 		tempChar = Uart_Uart1GetChar();	
		testResult = FALSE;
		switch (tempChar) {		

			case 'v':	verbose = !verbose ; continue;
			case 'b': 	testResult = testFlashId(); break;
			case 'c': 	testResult = testFlashWr(); break;
			case 'd': 	testResult = testModemImei(); cmp=false; break;
			case 'e': 	testResult = testModemSim(); cmp=false; break;
			case 'f': 	testResult = testModemNetwork(); cmp=false; break;
			case 'g': 	testResult = testModemCall(); cmp=false; break;
			case 'i': 	testResult = testBleId(); break;
			case 'j': 	testResult = testBleLoopback(); break;
			case 'k': 	testResult = testBlePair(); break;
			case 'l': 	testResult = testBleCommRd(); break;
			case 'm': 	testResult = testBleCommWr(); break;
			case 's': 	testResult = testSleepMode15Sec(); cmp=false; break;
			case 't': 	testResult = testSleepMode(); break;
			case 'y': 	testResult = toggle_Bt_radio_pwr(); break;
			case 'z': 	testResult = toggle_modem_pwr(); break;
			default:  
				DEBUG_OUTPUT_TEXT("  invalid character, try again\n\r"); 
				Timers_Delay (1500); //;
				continue;
		}
	if(testResult) DEBUG_OUTPUT_TEXT("   test result: PASS \n\r"); 
	else DEBUG_OUTPUT_TEXT("   test result: FAIL \n\r");
	DEBUG_OUTPUT_TEXT("\n\n\r press any key to continue \n\r"); 
  	while(Uart_Uart1IsCharReady()==FALSE) { 
   	if(btrp == true) jefs_ble_process(ble_ad, verbose);
	};
	tempChar = Uart_Uart1GetChar();  // empty buffer

	} // end of while(1)

} // end of main()

		
/*******************************************************************************
 * Flash Memory Test - Read Chip ID
 * 	description:
 *
 * 	rev:
 *		May182015-jef: call parameters removed, now static global(s)
 *		May042015-jef: added USART2 setup (to account for BLE I/F)
 * 		Apr102015-jef: complete but not optimized
 */
	
bool testFlashId(void) {

	uint8_t   	testBuffer[20];
	uint8_t   	commandAndAddress[4];
	uint16_t 	i; // foot soldier
	DEBUG_OUTPUT_TEXT("\n\r  Executing Flash Memory Test - Read Chip ID \n\r");
	
	USART2_setup();  // May262015-jef
	USART2->CTRL |= USART_CTRL_AUTOCS;	// May272015-jef
	USART2->ROUTE |= USART_ROUTE_TXPEN | USART_ROUTE_RXPEN | USART_ROUTE_CLKPEN | USART_ROUTE_CSPEN; // May272015-jef	
		
	Timers_Delay (1000); 	
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r    Power Up Flash Memory\n\r");	
	Flash_PowerOn(); 
	memset (testBuffer, 0xAA, sizeof(testBuffer));	
	// Send SPI command & retrieve data
	commandAndAddress[0] = SPI_FLASH_INS_RDID;  
	Flash_SpiWrite (commandAndAddress, 1);
	Flash_SpiRead (testBuffer, 3);	// SpiRead() doesn't flush buffer
	Flash_SpiRead (testBuffer, 20);
	if(verbose) {
		DEBUG_OUTPUT_TEXT("    Read Identification Information: \n\r");		
		sprintf (debugBuffer, "      Manufacturer ID: 0x%2x\n\r", testBuffer[0]);
		Uart_Uart1WriteString (debugBuffer);	
		sprintf (debugBuffer, "      Memory Type: 0x%2x\n\r", testBuffer[1]);
		Uart_Uart1WriteString (debugBuffer);		
		sprintf (debugBuffer, "      Memory Capacity: 0x%2x\n\r", testBuffer[2]);
		Uart_Uart1WriteString (debugBuffer);	
		sprintf (debugBuffer, "      CFD Length: 0x%2x\n\r", testBuffer[3]);
		Uart_Uart1WriteString (debugBuffer);	
		sprintf (debugBuffer, "      CFD Content: ");
		Uart_Uart1WriteString (debugBuffer);	
		for(i=4;i<20;i++)	{ 
			sprintf (debugBuffer, "%d,", testBuffer[i]); 
			Uart_Uart1WriteString (debugBuffer);
			Timers_Delay (10);
		}
		sprintf (debugBuffer, " \n\r");
		Uart_Uart1WriteString (debugBuffer);		
		DEBUG_OUTPUT_TEXT("    Power Down Flash Memory\n\n\r");
	}	
	Flash_PowerOff(); 	
	// parse & analyze result
	if( 	(testBuffer[0] == 0x20) && 
			(testBuffer[1] == 0x71) && 
			(testBuffer[2] == 0x15) && 
			(testBuffer[3] == 0x10) && 
			(testBuffer[4] == 0) && 
			(testBuffer[5] == 0) && 
			(testBuffer[6] == 0) && 
			(testBuffer[7] == 0) &&
			(testBuffer[8] == 0) && 
			(testBuffer[9] == 0) &&
			(testBuffer[10] == 0) &&
			(testBuffer[11] == 0) &&
			(testBuffer[12] == 0) &&
			(testBuffer[13] == 0) &&
			(testBuffer[14] == 0) &&
			(testBuffer[15] == 0) &&
			(testBuffer[16] == 0) &&
			(testBuffer[17] == 0) &&
			(testBuffer[18] == 0) &&
			(testBuffer[19] == 0) ) return TRUE; // pass
	else

return FALSE; // failure		
}
	 

/******************************************
 * Flash Memory Test - Write / Read
 * 	description:
 *
 * 	rev:
 *		May182015-jef: call parameters removed, now static global(s)
 *			May042015-jef: added USART2 setup (to account for BLE I/F)
 *			Apr172015-jef: debugging verbose=off issue.
 * 		Apr142015-jef: Works if verbose=on. Will fix 
 *			(timing issue) later for verbose=off
 */
bool testFlashWr(void) {

	uint8_t   	testBuffer2[20];
	uint8_t 		commandAndAddress[4]; 
	uint8_t 	i; 		// foot soldier
	volatile uint32_t j,k;		// to create short time delays
	
	DEBUG_OUTPUT_TEXT("\n\r  Executing Flash Memory Test - Write / Read \n\r");

	USART2_setup();	// May262015-jef
	USART2->CTRL |= USART_CTRL_AUTOCS;	// May272015-jef
	USART2->ROUTE |= USART_ROUTE_TXPEN | USART_ROUTE_RXPEN | USART_ROUTE_CLKPEN | USART_ROUTE_CSPEN; // May272015-jef	
	
	Timers_Delay (1000); 
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r       Power Up Flash Memory\n\r");	
	Flash_PowerOn(); 

	// erase the memory locations intending to write-read
	if(verbose) DEBUG_OUTPUT_TEXT("       Executing Subsector Erase\n\r");			
	Timers_Delay(50);
	Flash_WriteEnable();
	Timers_Delay(2);	// Apr142015-jef: leave this at 2?
	commandAndAddress[0] = SPI_FLASH_INS_SSE; // turn this into subroutine
	commandAndAddress[1] = 0;
	commandAndAddress[2] = 0x02;
	commandAndAddress[3] = 0;
	Flash_SpiWrite (commandAndAddress, 4); 
	j=Timers_GetMsTicks();
	Timers_Delay(2);		
	Flash_WriteDisable();
	Timers_Delay(2);	
	

	commandAndAddress[0] = SPI_FLASH_INS_RDSR; // turn this into subroutine
	Flash_SpiWrite (commandAndAddress, 1);
	Flash_SpiRead (testBuffer2, 4); 
   testBuffer2[0] = testBuffer2[3] & 0x01;		
	while(testBuffer2[0] != 0) {
		Timers_Delay (3);
		Flash_SpiWrite (commandAndAddress, 1);
		Flash_SpiRead (testBuffer2, 2); 
		testBuffer2[0] = testBuffer2[1] & 0x01;		
	}
	k=Timers_GetMsTicks()-j;
	if(verbose) DEBUG_OUTPUT_TEXT("       Subsector Erase Command Completed\n\r");	

	
	Timers_Delay(2);	// Apr172015-jef: SPI command separation
	
	// check to make sure erase occurred
	memset (testBuffer2, 0xC3, sizeof(testBuffer2));
	commandAndAddress[0] = SPI_FLASH_INS_READ;
	commandAndAddress[1] = 0;
	commandAndAddress[2] = 0x02;
	commandAndAddress[3] = 0;
	Flash_SpiWrite (commandAndAddress, 4);
	Flash_SpiRead (testBuffer2, 4); // SpiRead() doesn't flush buffer
	Flash_SpiRead (testBuffer2, 16);
	Timers_Delay (50);

	if(verbose) {
		sprintf(debugBuffer, "       Flash contents: ");
		Uart_Uart1WriteString (debugBuffer);			
		for(i=0;i<15;i++)  {
			sprintf (debugBuffer, "%d,", testBuffer2[i]);
			Uart_Uart1WriteString (debugBuffer);	
		}
		sprintf (debugBuffer, "%d\n\r",testBuffer2[15]);
		Uart_Uart1WriteString (debugBuffer);
	}	

	for(i=0;i<16;i++) {
		if(testBuffer2[i] != 255) {
			if(verbose) DEBUG_OUTPUT_TEXT("       Failure: Subsector NOT Erased\n\r");		
			return FALSE; // failure				
		}
	}
	
	
	// write data to Flash
	if(verbose) DEBUG_OUTPUT_TEXT("       Write (0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15) to Flash \n\r");	
	for(i=0;i<16;i++) { testBuffer2[i]=i; }
	Flash_WriteEnable();	
	for(j=0;j<50;j++) k=j;  // Apr222015-jef: was 25
	commandAndAddress[0] = SPI_FLASH_INS_PP;
	commandAndAddress[1] = 0;
	commandAndAddress[2] = 0x02;
	commandAndAddress[3] = 0;
 	Flash_SpiWrite (commandAndAddress, 4);
	Flash_SpiWrite (testBuffer2, 16);  
	for(j=0;j<100;j++) k=j;  // Apr222015-jef: was 25// short delay needed for USART driver
   Flash_WriteDisable();
	for(j=0;j<50;j++) k=j;  // Apr222015-jef: was 25// short delay needed for USART driver
	
	commandAndAddress[0] = SPI_FLASH_INS_RDSR; // turn this into subroutine
	Flash_SpiWrite (commandAndAddress, 1);
	Flash_SpiRead (testBuffer2, 2); 
   testBuffer2[0] &= 0x01;		
	while(testBuffer2[0] != 0) {
		Flash_SpiWrite (commandAndAddress, 1);
		Flash_SpiRead (testBuffer2, 1); 
		testBuffer2[0] &= 0x01;		
	}	
	 
	
	Timers_Delay(500);	// worst case page write time = 5mS	


	// Cycle Power
	if(verbose) DEBUG_OUTPUT_TEXT("       Power Down Flash Memory\n\r");
	Flash_PowerOff(); 
	if(verbose) DEBUG_OUTPUT_TEXT("       Wait 2 Seconds\n\r");
	Timers_Delay (2000);		
	if(verbose) DEBUG_OUTPUT_TEXT("       Power Up Flash Memory\n\r");
	Flash_PowerOn(); 	

	
	// Read those locations that were written to 
	if(verbose) DEBUG_OUTPUT_TEXT("       Read Flash Contents\n\r");		
	memset (testBuffer2, 0xAA, sizeof(testBuffer2));
	commandAndAddress[0] = SPI_FLASH_INS_READ;
	commandAndAddress[1] = 0; 
	commandAndAddress[2] = 0x02;
	commandAndAddress[3] = 0;
	Flash_SpiWrite (commandAndAddress, 4);
	Flash_SpiRead (testBuffer2, 4); // SpiRead() doesn't flush buffer
	Flash_SpiRead (testBuffer2, 16);  

	if(verbose) {
		sprintf(debugBuffer, "       Flash contents: ");
		Uart_Uart1WriteString (debugBuffer);			
		for(i=0;i<15;i++)  {
			sprintf (debugBuffer, "%d,", testBuffer2[i]);
			Uart_Uart1WriteString (debugBuffer);	
		}
		sprintf (debugBuffer, "%d\n\r",testBuffer2[15]);
		Uart_Uart1WriteString (debugBuffer);
	}	

	// compare actual to expected
	for(i=0;i<16;i++) {
		if(testBuffer2[i] != (uint8_t)i) {
			if(verbose) {
				sprintf (debugBuffer, "Failure: expected= %d, actual= %d \n\r", i, testBuffer2[1]);
				Uart_Uart1WriteString (debugBuffer);
				Timers_Delay (50);		
			}
			return FALSE;
		}
	}	
	if(verbose) DEBUG_OUTPUT_TEXT("       Flash contents are correct\n\r");

	// Leave Flash Erased
	if(verbose) DEBUG_OUTPUT_TEXT("       Erasing Flash\n\r");		
	Timers_Delay(50);
	Flash_WriteEnable();
	Timers_Delay(50);
	commandAndAddress[0] = SPI_FLASH_INS_SSE; // turn this into subroutine
	commandAndAddress[1] = 0;
	commandAndAddress[2] = 0x02;
	commandAndAddress[3] = 0;
	Flash_SpiWrite (commandAndAddress, 4); 
	Timers_Delay(2);	
	Flash_WriteDisable();
	Timers_Delay(2);	
	flashStatusRegister = Flash_ReadStatusRegister();
	while(flashStatusRegister != 0) {
		Timers_Delay (50);
		flashStatusRegister = Flash_ReadStatusRegister();
	}
   if(verbose) DEBUG_OUTPUT_TEXT("       Erase Complete\n\r");			
   if(verbose) DEBUG_OUTPUT_TEXT("       Power Down Flash\n\r");  
	Flash_PowerOff();
	

	return TRUE;	// test passes
}

	
/******************************************
 * BLE Test - Read Device Address
 * 	description:
 *
 * 	rev:
 *			May182015-jef: 
 *				- call parameters removed, now static global(s)
 *				- added power up/down
 * 		May152015-jef: working but w/o power control
 */
bool testBleId(void) {
	uint32_t eventProcessed = 0;
	uint8_t i = 0;
	
	DEBUG_OUTPUT_TEXT("\n\r  Executing BLE Test - Read Device Address \n\r");
//	May272015-jef USART2_setup_bluetooth();	// May262015-jef
	
	Timers_Delay(1000);

	// power on BLE radio
	btrp = true;
	ble_ad = false;
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Enable & Initialize BLE Radio\n\r");	
	Timers_Delay (1000);		
	app_ble_power_on();
	app_ble_bluetoothInit();
	while (!app_ble_initialized()) {  jefs_ble_process(ble_ad, verbose); } // May132015-jef app_ble_process(); }   
	if(verbose) DEBUG_OUTPUT_TEXT("      Radio Initialized\n\r");	
	Timers_Delay (1000);

	// read address
	if(verbose) 	DEBUG_OUTPUT_TEXT("      Get & Display Device Address\n\r");	
 	if(false == app_ble_get_address()) return false;
	eventProcessed = 0;
	i=0;
	while( (eventProcessed & MASK_CMDOPCODE) != (ACI_CMD_GET_DEVICE_ADDRESS<<SHIFT_CMDOPCODE) ) {
		eventProcessed = jefs_ble_process(ble_ad,verbose);
		if(i++ > 100) break;
	}
	if( (eventProcessed & MASK_CMDOPCODE) != (ACI_CMD_GET_DEVICE_ADDRESS<<SHIFT_CMDOPCODE) ) {
		// power off BLE radio
		app_ble_power_off();
		if(verbose) 		DEBUG_OUTPUT_TEXT("      BLE Radio Powered Off \n\r");
		Timers_Delay (1000);
		btrp = false;
		return false;
	}
 	else {	
		// power off BLE radio
		app_ble_power_off();
		if(verbose) DEBUG_OUTPUT_TEXT("      BLE Radio Powered Off \n\r");
		Timers_Delay (1000);
		btrp = false;
		return true;	// pass | fail status
	}
}
		
/******************************************
 * BLE Test - Loopback
 * 	description:
 *
 * 	rev:
 *			May182015-jef: 
 *				- call parameters removed, now static global(s)
 *				- added power up/down
 * 		May152015-jef: WiP
 */
bool testBleLoopback(void) {
	uint32_t eventProcessed = 0;
	uint16_t i = 0;
	uint32_t foo;
		
	DEBUG_OUTPUT_TEXT("\n\r  Executing BLE Test - Loopback \n\r");
	USART2_setup_bluetooth();	// May262015-jef	
	
	Timers_Delay(1000);

	// power on BLE radio
	btrp = true;
	ble_ad = false;
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Enable & Initialize BLE Radio\n\r");	
	Timers_Delay (1000);		
	app_ble_power_off();
	Timers_Delay(2000);
	app_ble_power_on();
	app_ble_bluetoothInit();
	while (!app_ble_initialized()) {  jefs_ble_process(ble_ad, verbose); } // May132015-jef app_ble_process(); }   
	if(verbose) DEBUG_OUTPUT_TEXT("      Radio Initialized\n\r");	
	Timers_Delay (1000);

	// initiate echo (loopback) test		
	if(verbose) 		DEBUG_OUTPUT_TEXT("      Initiate Loopback Test \n\r");
 	if(false == lib_aci_test(ACI_TEST_MODE_DTM_ACI)) return false;
	eventProcessed = 0;
	i=0;
	while((eventProcessed & MASK_OPERATINGMODE) != (ACI_DEVICE_TEST << SHIFT_OPERATINGMODE)) {
		eventProcessed = jefs_ble_process(ble_ad,verbose);
		if(i++ > 5000) break;   
	}
 	if( (eventProcessed & MASK_OPERATINGMODE) != (ACI_DEVICE_TEST << SHIFT_OPERATINGMODE)) {
		// power off BLE radio
		app_ble_power_off();
		if(verbose) 		DEBUG_OUTPUT_TEXT("      BLE Radio Powered Off \n\r");
		Timers_Delay (1000);
		btrp = false;
		return false;
	}
	if(verbose) DEBUG_OUTPUT_TEXT("entered TEST mode \r\n");
		
	// insert echo command & test here
	eventProcessed = 0;
	i=0;
	while((eventProcessed & MASK_EVENTOPCODE) != (ACI_EVT_ECHO << SHIFT_EVENTOPCODE)) {
		eventProcessed = jefs_ble_process(ble_ad,verbose);
		if(i++ > 5000) break;	// 10 typical
	}
	if( (eventProcessed & MASK_EVENTOPCODE) != (ACI_EVT_ECHO << SHIFT_EVENTOPCODE)) {
		lib_aci_test(ACI_TEST_MODE_EXIT);   // take it out of test mode
		// power off BLE radio
		app_ble_power_off();
		if(verbose) 		DEBUG_OUTPUT_TEXT("      BLE Radio Powered Off \n\r");
		Timers_Delay (1000);
		btrp = false;
		return false;
	}
	if(verbose) DEBUG_OUTPUT_TEXT("Loopback test completed \r\n");

	// power off BLE radio
	app_ble_power_off();
	if(verbose) 		DEBUG_OUTPUT_TEXT("      BLE Radio Powered Off \n\r");
	Timers_Delay (1000);
	btrp = false;
	return true;

}
		
			
/******************************************
 * BLE Test - Pair
 * 	description:
 *
 * 	rev:
 *		May182015-jef: call parameters removed, now static global(s)
 * 		Apr102015-jef: skeleton
 */
bool testBlePair(void) {
	DEBUG_OUTPUT_TEXT("\n\r  Executing BLE Test - Pair \n\r");
	USART2_setup_bluetooth();	// May262015-jef	
	Timers_Delay (1000);

		btrp = true;
	   ble_ad = true;
		DEBUG_OUTPUT_TEXT("\n\r      Enable & Initialize BLE Radio\n\r");	
		Timers_Delay (1000);		
		// app_ble_startup();
		app_ble_power_on();
		app_ble_bluetoothInit();	
		Timers_Delay(1000);
		while (!app_ble_initialized()) {  jefs_ble_process(ble_ad, verbose); } // May132015-jef app_ble_process(); }   
		jefs_ble_process(ble_ad, verbose);
		DEBUG_OUTPUT_TEXT("      BLE Radio Advertising\n\n\r");	

		DEBUG_OUTPUT_TEXT("  INSTRUCTIONS: \n\n\r");	

		DEBUG_OUTPUT_TEXT("  It is assumed the BLE radio will be paired with \n\r");	
		DEBUG_OUTPUT_TEXT("  Nordic's Master Control Panel via the Master Emulator \n\r");	
		DEBUG_OUTPUT_TEXT("  Device (PCA10000) on a USB port \n\n\r");			
		
		DEBUG_OUTPUT_TEXT("  On the Master Control Panel press: \"Start Discovery\". \n\r");	
		DEBUG_OUTPUT_TEXT("  The Neptune BLE radio device address should appear on the screen. \n\r");	
		DEBUG_OUTPUT_TEXT("  Click on the device address and press \"Select Device\".\n\r");		
		DEBUG_OUTPUT_TEXT("  On the next screen press: \"Connect\", then \"Discover services\". \n\n\r");			

		DEBUG_OUTPUT_TEXT("  The BLE radios are now paired \n\n\n\r");			
		Timers_Delay (1000);
	
	return TRUE;	// pass | fail status
}
		
	
/******************************************
 * BLE Test - Communicate Read
 * 	description:
 *
 * 	rev:
 *		May182015-jef: call parameters removed, now static global(s)
 * 		May012015-jef: WiP
 */
bool testBleCommRd(void) {
	
	char    testBuffer[] = "hello world";
	
	DEBUG_OUTPUT_TEXT("\n\r  Executing BLE Test - Communicate \n\r");
	Timers_Delay (1000);
	
	
	DEBUG_OUTPUT_TEXT("  It is assumed the BLE radio id currently  paired with \n\r");	
	DEBUG_OUTPUT_TEXT("  Nordic's Master Control Panel via the Master Emulator \n\r");	
	DEBUG_OUTPUT_TEXT("  Device (PCA10000) on a USB port \n\n\r");			
	
	DEBUG_OUTPUT_TEXT("  INSTRUCTIONS: \n\n\r");		
	
	DEBUG_OUTPUT_TEXT("  On the Master Control Panel in the \"Service Discovery\" window \n\r");	
	DEBUG_OUTPUT_TEXT("  highlight \"UART TX, Value\" by clicking on it. \n\r");	
	DEBUG_OUTPUT_TEXT("  Then type either text or hex values in the \"value\" field and\n\r");		
	DEBUG_OUTPUT_TEXT("  press \"Write\". \n\n\r");			

	DEBUG_OUTPUT_TEXT("  The alpha numeric values you typed in should apprear in the	\n\r");			
	DEBUG_OUTPUT_TEXT("  terminal window.	\n\n\n\r");		
	
	return true;	// pass | fail status
}
		
/******************************************
 * BLE Test - Communicate Write
 * 	description:
 *
 * 	rev:
 *		May262015-jef: added ble_wr_cnt
 *		May222015-jef: WiP
 *		May182015-jef: call parameters removed, now static global(s)
 */
 
static uint16_t ble_wr_cnt = 0;
 
bool testBleCommWr(void) {
	
//	char    testBuffer[] = "hello jef";
uint8_t testBuffer[20];
	
	DEBUG_OUTPUT_TEXT("\n\r  Executing BLE Test - Communicate Write \n\n\r");
	
	DEBUG_OUTPUT_TEXT("  It is assumed the BLE radio id currently  paired with \n\r");	
	DEBUG_OUTPUT_TEXT("  Nordic's Master Control Panel via the Master Emulator \n\r");	
	DEBUG_OUTPUT_TEXT("  Device (PCA10000) on a USB port \n\n\r");			
	
	DEBUG_OUTPUT_TEXT("  INSTRUCTIONS: \n\n\r");		
	
	DEBUG_OUTPUT_TEXT("  On the Master Control Panel in the \"Service Discovery\" window \n\r");	
	DEBUG_OUTPUT_TEXT("  highlight \"UART RX, Value\" by clicking on it then\n\r");	
	DEBUG_OUTPUT_TEXT("  press \"Read\". The value on the terminal window (in quotes below) \n\r");		
	DEBUG_OUTPUT_TEXT("  should appear next to \"UART RX, Value:\" in the Service Discovery window \n\r");			
	DEBUG_OUTPUT_TEXT("  To view ASCII text rather then numeric equivalents, check \n\r");		
	DEBUG_OUTPUT_TEXT("  \"Display as UTF8\" box. \n\n\n\r");

	
	sprintf (testBuffer, "BLE write count: %d", ++ble_wr_cnt);
	
	if (app_ble_sendData ((uint8_t *)testBuffer, strlen(testBuffer))) {
		DEBUG_OUTPUT_TEXT("  \"");
		DEBUG_OUTPUT_TEXT(testBuffer);
		DEBUG_OUTPUT_TEXT("\" String sent to BLE Master \n\r");		
		Timers_Delay (1000);
		return true;	// pass | fail status
	}
	
	return false;
	
}
		

		
	
/******************************************
 * Modem Test - Power Up / Read IMEI
 * 	description:
 *
 * 	rev:
 *		May182015-jef: call parameters removed, now static global(s)
 * 		Apr162015-jef: functions correctly
 */
bool testModemImei(void) {
	
	bool 	testResult = FALSE;
	uint8_t i=0;		// foot soldier
	
	DEBUG_OUTPUT_TEXT("\n\r  Executing Modem Test - Power Up / Read IMEI \n\r");
	Timers_Delay (1000);
	
	// power up
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Power Up Modem\n\r");	
 	app_power_modemPower_on();
	i=10;
	if(verbose) {
		sprintf (debugBuffer, "      Wait for Modem to Boot (sec) %2d  ", i);
		Uart_Uart1WriteString (debugBuffer);
	}
	while(i>0) {
		Timers_Delay (1000);		
		i--;
		if(verbose) {
			sprintf (debugBuffer, "\b\b\b\b%2d  ", i);
			Uart_Uart1WriteString (debugBuffer);		
		}
	}
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r");
	
	if(verbose) DEBUG_OUTPUT_TEXT("      Query Modem for IMEI\n\r");		
	testResult = app_modem_sendATGetReply("AT#CGSN", "CGSN:", replyBuff, sizeof(replyBuff), 3000);
	
	
	sprintf (debugBuffer, "      IMEI: %s\n\r", replyBuff);
	Uart_Uart1WriteString (debugBuffer);
	

	if(verbose) DEBUG_OUTPUT_TEXT("      Power Down Modem\n\r");	
	app_power_modemPower_off();
	
	return testResult;	// pass | fail status
}
		


/******************************************
 * Modem Test - SIM Communication
 * 	description:
 *
 * 	rev:
 *		May182015-jef: call parameters removed, now static global(s)
 * 		Apr162015-jef: functions correctly
 */
bool testModemSim(void) {

	bool 	testResult = FALSE;
	uint8_t i=0;		// foot soldier
	
	DEBUG_OUTPUT_TEXT("\n\r  Executing Modem Test - SIM Communication \n\r");
	Timers_Delay (1000);

	
	// power up
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Power Up Modem\n\r");	
 	app_power_modemPower_on();
	i=10;
	if(verbose) {
		sprintf (debugBuffer, "      Wait for Modem to Boot (sec) %2d  ", i);
		Uart_Uart1WriteString (debugBuffer);
	}
	while(i>0) {
		Timers_Delay (1000);		
		i--;
		if(verbose) {
			sprintf (debugBuffer, "\b\b\b\b%2d  ", i);
			Uart_Uart1WriteString (debugBuffer);		
		}
	}
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r");
	
	if(verbose) DEBUG_OUTPUT_TEXT("      Query Modem SIM for IMSI\n\r");		
	testResult = app_modem_sendATGetReply("AT#CIMI", "CIMI:", replyBuff, sizeof(replyBuff), 3000);
	
	
	sprintf (debugBuffer, "      IMSI: %s\n\r", replyBuff);
	Uart_Uart1WriteString (debugBuffer);
	

	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Power Down Modem\n\r");	
	app_power_modemPower_off();
	
	return testResult;	// pass | fail status	
	
}
		


/******************************************
 * Modem Test - Network Access
 * 	description:
 *
 * 	rev:
 *		May182015-jef: call parameters removed, now static global(s)
 * 		Apr162015-jef: functions correctly
 */
bool testModemNetwork(void) {
	char    replyBuff[128];
	char    *tmpPtr;
	uint8_t i=0;		// foot soldier
	
	DEBUG_OUTPUT_TEXT("\n\r  Executing Modem Test - Network Access \n\r");
	Timers_Delay (1000);
	
	// power up
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Power Up Modem\n\r");	
 	app_power_modemPower_on();
   i=10;
	if(verbose) {
		sprintf (debugBuffer, "      Wait for Modem to Boot (sec) %2d  ", i);
		Uart_Uart1WriteString (debugBuffer);
	}
	while(i>0) {
		Timers_Delay (1000);		
		i--;
		if(verbose) {
			sprintf (debugBuffer, "\b\b\b\b%2d  ", i);
			Uart_Uart1WriteString (debugBuffer);		
		}
	}
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r");
	
	// check for registration
	i=0;
	if(verbose) {
		sprintf (debugBuffer, "      Wait for Registration (sec) %2d  ", i);
		Uart_Uart1WriteString (debugBuffer);
	}
	while(app_modem_isRegistered()==FALSE) {
		Timers_Delay (1000);	
		i++;
		if(i==60) {
			if(verbose) {
				DEBUG_OUTPUT_TEXT("\n\r      Registration Timeout\n\r");	
				DEBUG_OUTPUT_TEXT("      Power Down Modem\n\r");	
			}
			app_power_modemPower_off();	
			return FALSE; 
		}
		if(verbose) {
			sprintf (debugBuffer, "\b\b\b\b%2d  ", i);
			Uart_Uart1WriteString (debugBuffer);
		}
	}
	DEBUG_OUTPUT_TEXT("\n\r      Registration Successful\n\r");

	if(verbose) DEBUG_OUTPUT_TEXT("      Query Tower for RSSI\n\r");		
	app_modem_sendATGetReply("AT+CSQ", "CSQ:", replyBuff, sizeof(replyBuff), 3000);
   tmpPtr = strstr (replyBuff, "CSQ:");
	if(verbose) {
		sprintf (debugBuffer, "      %s", tmpPtr);
		Uart_Uart1WriteString (debugBuffer);
	}
	
	if(verbose) DEBUG_OUTPUT_TEXT("      Power Down Modem\n\r");		
	app_power_modemPower_off();
	return TRUE;	// pass
}
		


/*********************************************************************
 * Modem Test - Make a Call
 * 	description:
 *
 * 	rev:
 *		May182015-jef: call parameters removed, now static global(s)
 *			May042015-jef: 
 *				[1] added AT#HWREV: CID parameters don't update without it
 *				[2] had to add back in "AT+CMEE=2" to make it work; doesn't seem
 *						stable at all.
 * 		Apr102015-jef: WiP
 */
bool testModemCall(void) {

	char 	commandBuffer[64];
	char    replyBuff[250];
	char    *tmpPtr;
	
	
	uint8_t i=0;		// foot soldier
	bool	testPass = FALSE;
	

	DEBUG_OUTPUT_TEXT("\n\r  Executing Modem Test - Make a Call \n\r");	
	Timers_Delay (1000);
	
	// power up
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r      Power Up Modem\n\r");	
 	app_power_modemPower_on();
	i=10;
	if(verbose) {
		sprintf (debugBuffer, "      Wait for Modem to Boot (sec) %2d  ", i);
		Uart_Uart1WriteString (debugBuffer);
	}
	while(i>0) {
		Timers_Delay (1000);		
		i--;
		if(verbose) {
			sprintf (debugBuffer, "\b\b\b\b%2d  ", i);
			Uart_Uart1WriteString (debugBuffer);		
		}
	}
	if(verbose) DEBUG_OUTPUT_TEXT("\n\r");
	
	// check for registration
	i=0;
	if(verbose) {
		sprintf (debugBuffer, "      Wait for Registration (sec) %2d  ", i);
		Uart_Uart1WriteString (debugBuffer);
	}
	while(app_modem_isRegistered()==FALSE) {
		Timers_Delay (1000);	
		i++;
		if(i==30) {
			if(verbose) {
				DEBUG_OUTPUT_TEXT("\n\r      Registration Timeout\n\r");	
				DEBUG_OUTPUT_TEXT("      Power Down Modem\n\r");	
			}
			app_power_modemPower_off();	
			return FALSE; 
		}
		if(verbose) {
			sprintf (debugBuffer, "\b\b\b\b%2d  ", i);
			Uart_Uart1WriteString (debugBuffer);
		}
	}
	DEBUG_OUTPUT_TEXT("\n\r      Registration Successful\n\r");	
	
	
	// establish connection with server
	
	
	// app_modem_sendAT("AT+CMEE=2", "OK", 1000);	// make modem errors verbose	

	if(verbose) {	 
		app_modem_sendATGetReply("AT+CGDCONT?", " ", replyBuff, sizeof(replyBuff), 5000);
		Uart_Uart1WriteString (replyBuff);
		Uart_Uart1WriteString ("\n\n\r");	
	}


	if(verbose) DEBUG_OUTPUT_TEXT("      Set Up Data Connection: \n\r");	
	if(verbose) DEBUG_OUTPUT_TEXT("        Define PDP Context \n\r");		
	
/* May042015-jef: see code below
	testPass = app_modem_sendAT("AT+CGDCONT=3,\"IP\",\"vzwinternet\",\"\",0,0", "OK", 1000);
	if(testPass==FALSE) { app_power_modemPower_off();	return FALSE; }	
	if(verbose) DEBUG_OUTPUT_TEXT("        Set Socket Configuration Parameters \n\r");	
	testPass = app_modem_sendAT("AT#SCFG=3,3,300,90,600,50", "OK", 1000);
	if(testPass==FALSE) { app_power_modemPower_off();	return FALSE; }		
	if(verbose) DEBUG_OUTPUT_TEXT("        Activate PDP Context \n\r");
	testPass = app_modem_sendAT("AT#SGACT=3,1", "OK", 5000);	
	if(testPass==FALSE) { app_power_modemPower_off();	return FALSE; }	
	if(verbose) DEBUG_OUTPUT_TEXT("        Open Remote Connection via Socket \n\r");
			//	sprintf (commandBuffer, "at#sd=3,0,7,\"www.google.com\",255,1,0");
	sprintf (commandBuffer, "at#sd=3,0,10510,\"modules.telit.com\",255,1,0");
*/

/*
	// May042015-jef: changed context identifier to 5 (to be different)
   app_modem_sendAT("AT#HWREV", "OK", 5000);	// May042015-jef: added this
	testPass = app_modem_sendAT("AT+CGDCONT=5,\"IP\",\"vzwinternet\",\"\",0,0", "OK", 1000);
	if(testPass==FALSE) { app_power_modemPower_off();	return FALSE; }	
	if(verbose) DEBUG_OUTPUT_TEXT("        Set Socket Configuration Parameters \n\r");	
	testPass = app_modem_sendAT("AT#SCFG=3,5,300,90,600,50", "OK", 1000);
	if(testPass==FALSE) { app_power_modemPower_off();	return FALSE; }		
	if(verbose) DEBUG_OUTPUT_TEXT("        Activate PDP Context \n\r");
	testPass = app_modem_sendAT("AT#SGACT=5,1", "OK", 5000);	
*/

	// Jun042015-jef: changed context identifier to 4 (to be different)
   // app_modem_sendAT("AT#HWREV", "OK", 5000);	// May042015-jef: added this
	 
	testPass = app_modem_sendAT("AT+CGDCONT=2,\"IP\",\"vzwinternet\",\"\",0,0", "OK", 1000);
	if(testPass==FALSE) { app_power_modemPower_off();	return FALSE; }	
	if(verbose) DEBUG_OUTPUT_TEXT("        Set Socket Configuration Parameters \n\r");	
	testPass = app_modem_sendAT("AT#SCFG=3,2,300,90,600,50", "OK", 1000);
	if(testPass==FALSE) { app_power_modemPower_off();	return FALSE; }		
	if(verbose) DEBUG_OUTPUT_TEXT("        Activate PDP Context \n\r");
	testPass = app_modem_sendAT("AT#SGACT=2,1", "OK", 5000);	
	
	if(verbose) {
		app_modem_sendATGetReply("AT+CGDCONT?", " ", replyBuff, sizeof(replyBuff), 5000);
			Uart_Uart1WriteString (replyBuff);
			Uart_Uart1WriteString ("\n\n\r");	
	}

	
	if(testPass==FALSE) { app_power_modemPower_off();	return FALSE; }	
	if(verbose) DEBUG_OUTPUT_TEXT("        Open Remote Connection via Socket \n\r");
	sprintf (commandBuffer, "at#sd=3,0,10510,\"modules.telit.com\",255,1,0");
	
	
	testPass = app_modem_sendAT(commandBuffer, "CONNECT", 10000);
	if(testPass) {
		DEBUG_OUTPUT_TEXT("        Data Connection Established \n\r");	
		if(verbose) DEBUG_OUTPUT_TEXT("        Dropping Connection \n\r");			
		testPass = app_modem_dropDataConnection();
	}
	app_power_modemPower_off();
	if((verbose==true) && (testPass==false)) DEBUG_OUTPUT_TEXT("      Connection Drop Failure\n\r");
 	return testPass;	// pass | fail status
}
		
	
/******************************************
 * Sleep Mode for 15 Seconds (EM2)
 * 	description:
 *
 * 	rev:
 *		May182015-jef: call parameters removed, now static global(s)
 * 		Apr292015-jef: refinement
 */
bool testSleepMode15Sec(void) {
	
	bool 	testResult = FALSE;
	uint32_t i=0, j=0;		// foot soldier
	uint16_t k;					// foot soldier
	uint32_t portModeLRestore[6];
	uint32_t portModeHRestore[6];
	uint32_t portValueRestore[6];

	
	DEBUG_OUTPUT_TEXT("\n\r  Executing Sleep Mode Test - EM2 for 15 Seconds\n\r");
	Timers_Delay (1000);
	
	// obtain port configurations
	for(k=0;k<6;k++) {
		portModeLRestore[k] = GPIO->P[k].MODEL;
		portModeHRestore[k] = GPIO->P[k].MODEH;
		portValueRestore[k] = GPIO->P[k].DOUT;	
	}

	DEBUG_OUTPUT_TEXT("\n\r      WAIT - Synchronizing with RTC\n\r");	
	i=app_rtc_seconds_get();
	while(i==app_rtc_seconds_get()) { };

	DEBUG_OUTPUT_TEXT("\n\r      Entering EM2 - Measure Current Now!\n\r");		
	Timers_Delay (100);

	
	// disable ports
	GPIO->P[gpioPortA].MODEL = 0;
	GPIO->P[gpioPortB].MODEL = 0;
	GPIO->P[gpioPortC].MODEL = 0;
	GPIO->P[gpioPortD].MODEL = 0;
	GPIO->P[gpioPortE].MODEL = 0;
	GPIO->P[gpioPortF].MODEL = 0; 
	
	GPIO->P[gpioPortA].MODEH = 0;
	GPIO->P[gpioPortB].MODEH = 0;
	GPIO->P[gpioPortC].MODEH = 0;
	GPIO->P[gpioPortD].MODEH = 0;
	GPIO->P[gpioPortE].MODEH = 0;
	GPIO->P[gpioPortF].MODEH = 0;

	// shut-off any internal PUPs 
	GPIO->P[gpioPortA].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortB].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortC].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortD].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortE].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortF].DOUTCLR = 0xFFFF;	


	// go into low energy mode
	EMU_EnterEM2(true); 

	j=app_rtc_seconds_get();

	// restore port configurations
	for(k=0;k<6;k++) {
		GPIO->P[k].DOUT = portValueRestore[k];	// Apr302015-jef optimization
		GPIO->P[k].MODEL =  portModeLRestore[k];
		GPIO->P[k].MODEH =  portModeHRestore[k];
	}

	DEBUG_OUTPUT_TEXT("\n\r      Wake Up - Resume Full Power Operation \n\r");	
	app_timers_enableMsecs();	// Apr212015-jef
	
	Uart_Uart1GetChar();  // empty buffer - from power cycle
	
	if(j>i+1) return TRUE;
	else return FALSE;

}
		
	
/******************************************
 * Sleep Mode (EM2)
 * 	description:
 *
 * 	rev:
 *		May182015-jef: call parameters removed, now static global(s)
 * 		Apr272015-jef: operational
 */
bool testSleepMode(void) {
	
	uint32_t i;
	
	DEBUG_OUTPUT_TEXT("\n\r  Entering Sleep Mode\n\r");
	DEBUG_OUTPUT_TEXT("\n\r    Cycle power to recover\n\r");
	DEBUG_OUTPUT_TEXT("    GPIO will be turned off\n\r");
	DEBUG_OUTPUT_TEXT("    Interrupts disabled\n\r");	
	
	Timers_Delay (2000);
	

	DEBUG_OUTPUT_TEXT("\n\r      WAIT! - Synchronizing with RTC\n\r");	
	i=app_rtc_seconds_get();
	while(i==app_rtc_seconds_get()) { };

	DEBUG_OUTPUT_TEXT("\n\r      Entering EM2 - Measure Current Now \n\r");		
	Timers_Delay (100);

	// Disable all GPIO

	GPIO->P[gpioPortA].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortB].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortC].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortD].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortE].DOUTCLR = 0xFFFF;
	GPIO->P[gpioPortF].DOUTCLR = 0xFFFF; 

	GPIO->P[gpioPortA].MODEL = 0;
	GPIO->P[gpioPortB].MODEL = 0;
	GPIO->P[gpioPortC].MODEL = 0;
	GPIO->P[gpioPortD].MODEL = 0;
	GPIO->P[gpioPortE].MODEL = 0;
	GPIO->P[gpioPortF].MODEL = 0; 
	
	GPIO->P[gpioPortA].MODEH = 0;
	GPIO->P[gpioPortB].MODEH = 0;
	GPIO->P[gpioPortC].MODEH = 0;
	GPIO->P[gpioPortD].MODEH = 0;
	GPIO->P[gpioPortE].MODEH = 0;
	GPIO->P[gpioPortF].MODEH = 0; 
	


	RTC_Enable(FALSE);
	
   EMU_EnterEM2(true); 
	
	return FALSE;	// never get here
}
/******************************************
 * Toggle Cellular Modem Power
 * 	description:
 *
 * 	rev:
 *		May182015-jef: call parameters removed, now static global(s)
 * 		Apr272015-jef: functions correctly
 */
bool toggle_modem_pwr(void) {
	
	bool 	testResult = FALSE;
	uint8_t i=0;		// foot soldier
	
	DEBUG_OUTPUT_TEXT("\n\r  Toggle Cell Modem Power ON/OFF \n\r");
	Timers_Delay (1000);
	
	// power up
	if(cmp==FALSE) {
		DEBUG_OUTPUT_TEXT("\n\r      Power Up Modem\n\r");	
		app_power_modemPower_on();
		i=10;
		sprintf (debugBuffer, "      Wait for Modem to Boot (sec) %2d  ", i);
		Uart_Uart1WriteString (debugBuffer);
		while(i>0) {
			Timers_Delay (1000);		
			i--;
			sprintf (debugBuffer, "\b\b\b\b%2d  ", i);
			Uart_Uart1WriteString (debugBuffer);		
		}
		DEBUG_OUTPUT_TEXT("\n\r");
		DEBUG_OUTPUT_TEXT("      Query Modem for IMEI\n\r");		
		testResult = app_modem_sendATGetReply("AT#CGSN", "CGSN:", replyBuff, sizeof(replyBuff), 3000);
		sprintf (debugBuffer, "      IMEI: %s\n\r", replyBuff);
		Uart_Uart1WriteString (debugBuffer);
		if(testResult==TRUE) cmp=TRUE;
	}
	else{
		DEBUG_OUTPUT_TEXT("      Power Down Modem\n\r");	
		app_power_modemPower_off();
		cmp=FALSE;
	}
	
	return true;	// cell modem power status
}

/******************************************
 * Toggle Bluetooth Radio Power
 * 	description:
 *
 * 	rev:
 *		May182015-jef: call parameters removed, now static global(s)
 *		May132015-jef migrate to jefs_ble_process()
 * 		Apr272015-jef: not implemented yet
 */
bool toggle_Bt_radio_pwr(void) {
	
	bool 	testResult = FALSE;
	uint8_t i=0;		// foot soldier
	
	DEBUG_OUTPUT_TEXT("\n\r  Toggle Bluetooth Radio ON/OFF \n\r");

	Timers_Delay (1000);
	USART2_setup_bluetooth();	// May262015-jef
		
	if(btrp == false) { 
		btrp = true;
		DEBUG_OUTPUT_TEXT("\n\r      Enable & Initialize BLE Radio\n\r");	
		Timers_Delay (1000);		
		app_ble_power_on();
		app_ble_bluetoothInit();
		while (!app_ble_initialized()) {  jefs_ble_process(ble_ad, verbose); } // May132015-jef app_ble_process(); }   
		DEBUG_OUTPUT_TEXT("      Radio Initialized\n\r");	
		Timers_Delay (1000);
	}
	else{
 		app_ble_power_off();
		DEBUG_OUTPUT_TEXT("      BLE Radio Powered Off \n\r");
		Timers_Delay (1000);
		btrp = false;
		ble_ad = false;
	}
	
	return true;	// Bluetooth Radio Status
}




/***************************************************************************//**
 * @brief
 *   Performs overall CMIU initialization - besides what is being done in
 *   autogen modules.
 *
 * @note
 *       
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_cmiu_init(void)
{
    uint32_t  startDelay;
    

    
    // CMU should be initialized first since if a module clock is disabled, the
    // registers of that module are not accessible and reading from such 
    // registers may return undefined values. Writing to registers of clock 
    // disabled modules have no effect. One should normally avoid accessing
    // module registers of a module with a disabled clock.
 // Apr222015-jef    app_cmu_init();	// ok Apr212015-jef
    
	// Initialize RTC module
	app_rtc_init();  // ok Apr212015-jef
    
    // Initialize GPIO module
	app_gpio_init();	//ok Apr212015-jef
    
    // Initialize Timers module
 // Apr222015-jef    app_timers_init();	//ok Apr212015-jef
    
    // Initialize ARB module
    //remove Apr212015-jef  ARB_Init();
    
    // Initialize Metrology module
 // Apr222015-jef    app_metrology_Init();	//ok Apr212015-jef
 
    // Initialize Error module 
    //remove Apr212015-jef 	Error_Init();
    
    // Initialize the bluetooth module
 // Apr222015-jef   app_ble_init();	//ok Apr212015-jef
       
    // Initialize App module   
    //remove Apr212015-jef  app_cmiu_app_init();
   
/* 27Apr15-jef: covered elsewhere
    // Delay before possible (EM4) sleep so that debugger can attach to target
  for(startDelay = 0x1FFFFF; startDelay>0; startDelay--) {
		__NOP();
  } 
  
  DEBUG_OUTPUT_TEXT("CMIU Initialization is complete\n\r");
*/  

}
		
// end-of-file
