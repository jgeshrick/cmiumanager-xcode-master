/*******************************************************************************
 * Copyright (c) 2014 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation and/or initial documentation
 *******************************************************************************/

#include "MQTTARM_M3.h"
//#include "lwip/tcp.h"
//#include "lwip/ip_addr.h"
//#include "lwip/dns.h"
//#include "ringbuf.h"

//Included to enable debugging
#include "stdio.h"
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "app_timers.h"
#include "app_uart.h"




#define IPADDR(a, b, c, d)  ((a << 24) | (b << 16) | (c << 8) | d)

unsigned long MilliTimer;
signed long waiting_for_connect;
signed long waiting_for_sent;
//CircularBuffer cb;


//We will only have one pcb so only one connection at a time.
//struct tcp_pcb * pcb;

/**************
 provided in app_timers.c by SysTick_Handler
void SysTickIntHandler(void) {
	MilliTimer++;
}
***************/

char expired(Timer* timer) {
//	long left = timer->end_time - MilliTimer;
	long left = timer->end_time - Timers_GetMsTicks();
	return (left < 0);
}


void countdown_ms(Timer* timer, unsigned int timeout) {
//	timer->end_time = MilliTimer + timeout;
	timer->end_time = Timers_GetMsTicks() + timeout;
}


void countdown(Timer* timer, unsigned int timeout) {
//	timer->end_time = MilliTimer + (timeout * 1000);
	timer->end_time = Timers_GetMsTicks() + (timeout * 1000);
}


int left_ms(Timer* timer) {
//	long left = timer->end_time - MilliTimer;
	long left = timer->end_time - Timers_GetMsTicks();
	return (left < 0) ? 0 : left;
}


void InitTimer(Timer* timer) {
	timer->end_time = 0;
	//timer->systick_period = 48000;
}

void Delay(long int ms_delay)
{
	Timer timer;	
	InitTimer(&timer);    
    countdown_ms(&timer, ms_delay);
    while (!expired(&timer))
    {
		}
}


int read(Network* n, unsigned char* buffer, int len, int timeout_ms) {
	
	int rc = 0;
	int recvLen = 0;

	
	//printf("Entered read\n");
	//Add code here to read from socket here
	//Read from a buffer which is updated by the data_recv_cb
  Delay(100);  
	//Return up to len bytes
    /*************
	char tmp;
	while (!cbIsEmpty(&cb)) 
	{
        cbRead(&cb, &tmp);
		    buffer[recvLen++] = tmp;
		    if (recvLen==len) {
					break;
				}
  }
    ****************/
    
    while (app_uart_uart0_bytesAvailable())
    {
        char    ch;
        Uart_Uart0GetData ((uint8_t *)&ch, 1);
        buffer[recvLen++] = ch;
        if (recvLen == len)
        {
            break;
        }
    }

	
	return recvLen;
}

/**************
err_t tcp_data_sent_cb(void * arg, struct tcp_pcb * tpcb,u16_t len) {
		
		//printf("TCP: BYTES SENT = %u\n",len);
		waiting_for_sent = 1;	
		return ERR_OK;
}
*************/

int write(Network* n, unsigned char* buffer, int len, int timeout_ms) {
	
int rc = 0;

	//printf("Entered write. Writing %d bytes\n",len);
	
	
	waiting_for_sent = 0;
	
    Uart_Uart0PutData (buffer, len);
	//Write data out here
    /******************
	tcp_sent(pcb, tcp_data_sent_cb);
	
	rc =  tcp_write(pcb, buffer, len, 1); //Copy to lwip buffer before writing out
	//printf("TCP_WRITE rc = %d\n",rc);
	rc = tcp_output(pcb);
  //printf("TCP_OUTPUT rc = %d\n",rc);
	
	
    **************/
	//for now assume that all bytes are just sent
	return len;
	
}


void disconnect(Network* n) {
	//Carry out any disconnect here
    /***************
	if (tcp_close(pcb)==ERR_OK) {
			//printf("SOCKET CLOSED\n");
	} else {
		printf("ERROR CLOSING SOCKET\n");
	}
    ************/
    // disconnect the data connection
    app_modem_dropDataConnection();
}


void NewNetwork(Network* n) {
	n->my_socket = 0;
	n->mqttread = read;
	n->mqttwrite = write;
	n->disconnect = disconnect;
}

/*****************
err_t tcp_connect_cb(void * arg, struct tcp_pcb * tpcb,err_t err)
{
	
	if (err==ERR_OK) {
		//printf("SOCKET CONNECTED\n");
		waiting_for_connect = 1;
	}
	
	return ERR_OK;
}

void tcp_err_cb(void * arg, err_t err) {
	printf("TCP ERROR CALLBACK, err = %d\n",err);
	waiting_for_connect = 2;
	waiting_for_sent = 2;
}
****************/


//
/*****************
err_t data_recv_cb(void * arg, struct tcp_pcb * tpcb, struct pbuf * p, err_t err)
{
		//printf("Data received from client. err = %d\n",err);
	
	  if (err==ERR_OK) {
			//printf("Length of received data: %u\n",p->len);
	
			tcp_recved(pcb,p->len);
			char *tmp;
			tmp = p->payload;
			
			for (int i=0;i<p->len;i++) {
        cbWrite(&cb, &tmp[i]);
			}
			
			
			free(p);
	
		}
	
		return ERR_OK;
}
***************/

// display ip_addr value as string
/***************
char *ip_ntoa(struct ip_addr adr) {
 struct in_addr ip;
 ip.s_addr = adr.addr;
 return(inet_ntoa(ip));
} // ip_ntoa
**********/


int ConnectNetwork(Network* n, char* addr, int port)
{
	int rc = 0;
//	struct ip_addr svr_ip;
	
 	/*************
	waiting_for_connect = 0;
	pcb = tcp_new();
	tcp_bind(pcb, IP_ADDR_ANY, 7000); //client port for outcoming connection
    tcp_arg(pcb, NULL);
	
	tcp_err(pcb,tcp_err_cb);
	
		
	
	//IP4_ADDR(&svr_ip,23,102,34,3);  //mqttdemo.cloudapp.net
	//IP4_ADDR(&svr_ip,74,125,230,120); //www.google.com
	//IP4_ADDR(&svr_ip,217,33,180,70);  //www.sagentia.com
	//IP4_ADDR(&svr_ip,85,119,83,194); //test.mosquito,com
	IP4_ADDR(&svr_ip,168,63,67,73); //winmqtttest.cloudapp.net
	
	
    printf("MQTT Broker: %s\n", ip_ntoa(svr_ip));

	//Carry out any network connection stuff here
    rc = tcp_connect(pcb, &svr_ip, port, tcp_connect_cb);

	if (rc==ERR_OK) {
	
		//Wait until we get a call back to say continue
		Timer timer;
		InitTimer(&timer);    
		countdown_ms(&timer, 10000);
		while(!expired(&timer) && !waiting_for_connect) {}
			
		rc = waiting_for_connect;		
		
		//Set up ring buffer
		cbInit(&cb, 1024);
		
		tcp_recv(pcb,data_recv_cb);

	} else {
		printf("Error in tcp_connect..rc = %d\n",rc);
	}
******************/
	
	return rc;
}
