/******************************************************************************
*******************************************************************************
**
**         Filename: app_modem.c
**    
**           Author: Barry Huggins
**          Created: 12/23/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_modem.c
*
* @brief This file contains the code used to manage the LTE cellular module
*        (currently a Telit LE-910)
* 
************************************************************************/

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include "stdtypes.h"
#include "em_device.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "efm32lg_uart.h"
#include "app_modem.h"
#include "app_uart.h"
#include "app_timers.h"
#include "app_rtc.h"
#include "app_configuration.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/

S_DATA_CONN_STATE_INFO  dataConnInfo = {DATA_CONN_STATE_DEFINE_CONTEXT, MODEM_CONN_SUCCESS, 0, 0};

/***************************************************************************//**
 * @brief
 *   Initialize the cellular modem system (currently the Telit LE-910)
 *
 * @note
 *   All this does is disable power to the modem.
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void app_modem_init(void)
{
    // disable power on the Telit module using PA10 - active high
    GPIO_PinOutClear(gpioPortA, 10);
    
    /* disable level shifter PD8 - active low */
    GPIO_PinOutSet(gpioPortD, 8);
    
}

/***************************************************************************//**
 * @brief
 *   Determines if the modem (currently a Telit LE-910) is currently registered
 *     on the network.
 *
 * @note
 *   This function determines if the Telit modem is registerd on the data network.
 *   It does this by looking at the results of the AT+CGREG? command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the modem is registered
 ******************************************************************************/
bool    app_modem_isRegistered()
{
    char    replyBuff[128];
    char    *tempPtr;
    bool    registered = false;
    
    memset (replyBuff, 0, sizeof(replyBuff));
    if (app_modem_sendATGetReply("AT+CGREG?", "CGREG:", replyBuff, sizeof(replyBuff), 3000))
 //Apr162015-jef       Uart_Uart1PutData ("got OK\n\r", 8);
    tempPtr = strstr (replyBuff, "CGREG:");
    
    if (tempPtr)
    {
  //Apr162015-jef       Uart_Uart1PutData (tempPtr, strlen(tempPtr));
        // response is in format +CGREG: 0,1
        tempPtr += 9; // skip 9 chars to get to registration value
        if ((*tempPtr == '1') || (*tempPtr == '5'))
            registered = true;
    }
    
    return registered;
}

/***************************************************************************//**
 * @brief
 *   Shuts down the modem (currently a Telit LE-910).
 *
 * @note
 *   This function shuts down the Telit modem.
 *   It does this by sending the AT#SHDN command. The command returns OK
 *     but then takes additional time to power off. The powermon line
 *     should be checked (app_power_modemIsPowered) to determine when the
 *     modem is actually powered off.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command was successful
 ******************************************************************************/
bool    app_modem_shutdown (void)
{
    if (app_modem_sendAT ("AT#SHDN", "OK", 10000))
    {
        return true;
    }
    else
        return false;
}

/***************************************************************************//**
 * @brief
 *   Sets up the the modem (currently a Telit LE-910) for a data call.
 *
 * @note
 *   This function sets up the Telit modem in preparation of making a data call.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command was successful
 ******************************************************************************/
bool    app_modem_setup (void)
{
    bool result = true;
    
    if (!app_modem_sendAT ("AT+CMEE=2", "OK", 10000))
    {
        result = false;
    }
    return result;
}


/***************************************************************************//**
 * @brief
 *   Waits for the specified response UART 0 with a timeout.
 *
 * @note
 *   This function waits for the specified response from UART 0. It also returns  
 *    the single line of the response containing the specified response. It will  
 *    also retun after theindicated number of msecs. The RX buffer should be 
 *    flushed before calling.
 *       
 *
 * @param[in]  uart        
 *   UART interface
 * @param[in]  resp        
 *   Response to be matched
 * @param[out] databuf     
 *   Data buffer for read data.  If NULL no data returned.
 * @param[in]  datalen     
 *   Length of data buffer.
 * @param[in]  timeout_ms  
 *   Timeout in msec.
 *
 * @return bool if at least 1 char is returned.
 ******************************************************************************/
bool app_modem_waitForResponse(char *resp, char *databuf, uint8_t datalen, uint16_t timeout_ms)
{
    uint16_t idx, psize;
    bool rc;
    char recvbuf[80];
    char *p;

    // if no buffer given then use local storage
    if (databuf == NULL)
    {
        p = recvbuf;
        psize = sizeof(recvbuf);
    }
    else
    {
        p = databuf;
        psize = datalen;
    }

    memset(p, 0, psize);

    idx = 0;
    while (idx < psize)
    {
        // leave room for null-terminator
        rc = Uart_Uart0GetString((char *)p+idx, psize-idx-1, timeout_ms);

        idx += strlen((char *)(p+idx));

        // insert newline after each string (if room)
        if (idx < psize)
        {
            p[idx] = '\n';
            idx++;
        }
        
 //Apr152015-jef     DEBUG_OUTPUT_TEXT (p);

        // if response string not found yet and we timeout on last read return with failure code
        if (rc == FALSE)
            return FALSE;

        if (strstr((char *)p, (char *)resp) != NULL)
        {
            return true;
        }
        
        // clear previous lines from buffer
        memset(p, 0, psize);
        idx = 0;

    }

  return true;
}


/***************************************************************************//**
 * @brief
 *   Sends the specified AT command UART 0 and waits for the specified response.
 *
 * @note
 *   This function waits for the specified response from UART 0. It will also retun  
 *    after the indicated number of msecs. 
 *       
 *
 * @param[in]  str          
 *   Pointer to the string command for the modem.
 * @param[in]  expectResp   
 *   Pointer to the expected string response from modem.
 *   For no expected response, pass NULL.
 * @param[in]  timeout_ms   
 *   Timeout to wait for expected response.
 *   If no expected response (NULL) then this is ignored.
 *
 * @return bool indicates if the expected response is found.
 ******************************************************************************/
 bool app_modem_sendAT(char* str, char* expectResp, uint32_t timeout_ms)
{
    char resp[80];
    bool retb=false;        // did we find the expected response?

	Uart_Uart0PutData ((uint8_t*)str, strlen (str));
    Uart_Uart0PutData ((uint8_t*)"\r\n", 2);

    if (expectResp == NULL)
    {
        return true;
    }
    else // we check for a response
    {

        retb = app_modem_waitForResponse(expectResp, resp, sizeof(resp)-1, timeout_ms);
    } //  response

    return retb;

} // app_modem_sendAT


/***************************************************************************//**
 * @brief
 *   Sends the specified AT command UART 0 and waits for the specified response.
 *   It also returns the response which includes the match.
 *
 * @note
 *   This function waits for the specified response from UART 0. It also returns  
 *    the response containing the specified response. It will also retun after the 
 *    indicated number of msecs. 
 *       
 *
 * @param[in]  str          
 *   Pointer to the string command for the modem.
 * @param[in]  expectResp   
 *   Pointer to the expected string response from modem.
 *   For no expected response, pass NULL.
 * @param[out] responseBuff
 *   Pointer to the buffer to contain the response.
 * @param[in]  responseBuffLen
 *   Size of the buffer to hold the response.
 * @param[in]  timeout_ms   
 *   Timeout to wait for expected response.
 *   If no expected response (NULL) then this is ignored.
 *
 * @return bool indicates if the expected response is found.
 ******************************************************************************/
 bool app_modem_sendATGetReply(char* str, char* expectResp, char *responseBuff, uint8_t responseBuffLen, uint32_t timeout_ms)
{
    bool retb=false;        // did we find the expected response?

	Uart_Uart0PutData ((uint8_t*)str, strlen (str));
    Uart_Uart0PutData ((uint8_t*)"\r", 1);

    if (expectResp == NULL)
    {
        retb = true;
    }
    else // we check for a response
    {
        retb = app_modem_waitForResponse(expectResp, responseBuff, responseBuffLen, timeout_ms);
    } //  response

    return retb;

} // app_modem_sendATGetReply

/***************************************************************************//**
 * @brief
 *   gets the current time from the Telit LE-910 modem.
 *
 * @note
 *   This function gets the current time information from the Telit LE-910 modem
 *   using the AT#CCLK? command. This time is set by the Verizon network when
 *   the modem registers. The response to the command is something like:
 *      #CCLK: "15/02/24,13:03:26-20,0"
 *       
 *
 * @param[out] currTime - points to a tm structure to contain the current time
 * @param[out] timezone - points to a variable to provide the time zone offset
 *                        in 1/4 hour units
 *
 * @return bool indicating if the command was successful
 ******************************************************************************/
bool app_modem_getCurrentTime (struct tm 	*pCurrTime, int8_t   *timezone)
{
    int16_t     tempInt;
    char        stringCclkBuffer[64];
    
    // issue the AT#CCLK command to get the current local time from the modem.
    if (app_modem_sendATGetReply ("AT#CCLK?", "#CCLK:", stringCclkBuffer, sizeof(stringCclkBuffer), 1000))
	{
        char    tempBuffer[24];
        DEBUG_OUTPUT_TEXT(stringCclkBuffer);
        
        // extract each part of the time using fixed locations since this result is a fixed size string
		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_TIMEZONE]);
		*timezone = (int8_t) tempInt;
        sprintf (tempBuffer, "timezone: %d\n", *timezone);
        DEBUG_OUTPUT_TEXT (tempBuffer);
        
		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_ISDST]);
		pCurrTime->tm_isdst = tempInt;

		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_YEAR]);
		pCurrTime->tm_year = tempInt;    // year is since 1900 in mktime so add 100 if using mktime
        
		// use the year to determine if the date has been set by the network
        // if the time is not set by the network, we see the year be 0 or 99
		if ((tempInt < 10) || (tempInt > 90))
        {
			return false;
        }
		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_MONTH]);
		pCurrTime->tm_mon = tempInt;        // 0-based for mktime so subtract 1 if using mktime
		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_DAY]);
		pCurrTime->tm_mday = tempInt;
		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_HOUR]);
		pCurrTime->tm_hour = tempInt;
		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_MINUTE]);
		pCurrTime->tm_min = tempInt;
		tempInt = atoi(&stringCclkBuffer[MODEM_CCLK_RESPONSE_START_SECOND]);
		pCurrTime->tm_sec = tempInt;
        
        return true;
    }

    return false;
    
}

/***************************************************************************//**
 * @brief
 *   Routine used to test the ability to get the time from the LTE network.
 *
 * @note
 *   This function calls the routine which gets the time from the LTE network and
 *   displays both the local time and UTC on the debug UART.
 *       
 *
 * @param[in] none
 * @param[out] none
 *
 * @return none
 ******************************************************************************/
void    app_modem_testTimeFunction(void)
{
    struct tm   currTime;
    int8_t      timezone;
    char        debugString[80];
    time_t      timeT;
    int32_t     timeZoneOffset;
    
    app_modem_getCurrentTime(&currTime, &timezone);
    sprintf (debugString, "Date: %d/%d/%d time: %d:%d:%d timezone: %d dst: %d\n", currTime.tm_mon, currTime.tm_mday, currTime.tm_year-100,
             currTime.tm_hour, currTime.tm_min, currTime.tm_sec, timezone, currTime.tm_isdst);
    
    DEBUG_OUTPUT_TEXT (debugString);
    
    // have to add 100 since unix time assumes year since 1900
    currTime.tm_year += 100;  
    
    // subtract 1 from month since months should be 0-11
    currTime.tm_mon -= 1;
    
    timeT = mktime (&currTime);
    
    // compute number of seconds offset from UTC. If timezone is negative then need to add seconds.
    timeZoneOffset = timezone;
    timeZoneOffset = timeZoneOffset * -900;
    timeT += timeZoneOffset;
    
    sprintf (debugString, "UTC time %s\n", ctime(&timeT));
    DEBUG_OUTPUT_TEXT (debugString);
    
}

/***************************************************************************//**
 * @brief
 *   sets the RTC time based on the time from the LTE network.
 *
 * @note
 *   This function calls the routine to get the time from the LTE network and
 *   if successful it sets the RTC time.
 *       
 *
 * @param[in] none
 * @param[out] none
 *
 * @return bool indicating if the command was successful
 ******************************************************************************/
bool     app_modem_setRTC(void)
{
    struct tm   currTime;
    int8_t      timezone;
    char        debugString[80];
    time_t      timeT;
    int32_t     timeZoneOffset;
    U_NEPTUNE_TIME  neptuneTime; 
    
    if (app_modem_getCurrentTime(&currTime, &timezone))
    {
        sprintf (debugString, "Date: %d/%d/%d time: %d:%d:%d timezone: %d dst: %d\n", currTime.tm_mon, currTime.tm_mday, currTime.tm_year-100,
                 currTime.tm_hour, currTime.tm_min, currTime.tm_sec, timezone, currTime.tm_isdst);
        
        DEBUG_OUTPUT_TEXT (debugString);
        
        // have to add 100 since unix time assumes year since 1900
        currTime.tm_year += 100;  
        
        // subtract 1 from month since months should be 0-11
        currTime.tm_mon -= 1;
        
        timeT = mktime (&currTime);
        
        // compute number of seconds offset from UTC. If timezone is negative then need to add seconds.
        timeZoneOffset = timezone;
        timeZoneOffset = timeZoneOffset * -900;
        timeT += timeZoneOffset;
        
        sprintf (debugString, "GMT time %s\n", ctime(&timeT));
        DEBUG_OUTPUT_TEXT (debugString);
        
        neptuneTime.S64 = timeT;
        
        app_rtc_time_set (&neptuneTime);
        
        return true;  
    }
    else
        return false;
    
}


/***************************************************************************//**
 * @brief
 *   State machine to setup a data connection on the Telit LE-910 modem.
 *
 * @note
 *   This function uses a small state machine to go through the steps to set
 *   up a data connection over the LTE network.
 *   Currently the commands needed are:
 *      AT+CGDCONT=3,"IP","vzwinternet","",0,0
 *          define the PDP context (only needs to be done once)
 *      AT#SCFG=3,3,300,90,600,50
 *          set the packet size and TX timeout values
 *      AT#SGACT=3,1
 *          activate context 3
 *      AT#SD=3,0,<port number>,<server address>,255,1,0
 *          connect to the server using TCP and online data mode
 *       
 *
 * @param[in] dataConnEvent - event to be processed
 * @param[in] eventDataPtr - points to any event data
 * @param[in] eventDataSize - size of the event data
 *
 * @return int16_t - 0 indicates the connection is completely made to the server
 *                   different negaive numbers indicate type of failue
******************************************************************************/
bool    app_modem_dataConnSetup (uint8_t  dataConnEvent, uint8_t     *eventDataPtr, uint16_t   eventDataSize)
{
    // initialize to in progress and then update if necessary
    dataConnInfo.modemConnResult = MODEM_CONN_IN_PROGRESS;
    
    switch (dataConnInfo.connState)
    {
        case DATA_CONN_STATE_DEFINE_CONTEXT:
            app_modem_sendAT("AT#HWREV", "OK", 5000);
            // bhtodo: change to get APN from some configuration
            if (app_modem_sendAT("AT+CGDCONT=3,\"IP\",\"vzwinternet\",\"\",0,0", "OK", 1000))
            {
                dataConnInfo.connState = DATA_CONN_STATE_CONFIG_CONTEXT;
                dataConnInfo.currStateWaitTime = 1;
                DEBUG_OUTPUT_TEXT ("sent cgdcont\n");
            }
            break;
        case DATA_CONN_STATE_CONFIG_CONTEXT:
            if (app_modem_sendAT("AT#SCFG=3,3,300,90,600,50", "OK", 1000))
            {
                dataConnInfo.connState = DATA_CONN_STATE_ACTIVATE_CONTEXT;
                dataConnInfo.currStateWaitTime = 1;
                DEBUG_OUTPUT_TEXT ("sent scfg\n");
            }
            break;
        case DATA_CONN_STATE_ACTIVATE_CONTEXT:
            if (app_modem_sendAT("AT#SGACT=3,1", "OK", 5000))
            {
                dataConnInfo.connState = DATA_CONN_STATE_CONNECT;
                dataConnInfo.currStateWaitTime = 1;
            // do not wait for response but cannot go into EM2 mode                
//            if (app_modem_sendAT("AT#SGACT=3,1", NULL, 1000))
//            {
//                dataConnInfo.connState = DATA_CONN_STATE_ACTIVATE_CONTEXT_WAIT_RESPONSE;
//                dataConnInfo.currStateWaitTime = 45;
                DEBUG_OUTPUT_TEXT ("sent sgact\n");
            }
            break;
        case DATA_CONN_STATE_ACTIVATE_CONTEXT_WAIT_RESPONSE:
            switch (dataConnEvent)
            {
                case DATA_CONN_EVENT_TIMEOUT:
                    DEBUG_OUTPUT_TEXT ("context activation TIME OUT\n");
                    dataConnInfo.modemConnResult = MODEM_CONN_FAILURE_CONTEXT_ACTIVATION;
                    break;
                case DATA_CONN_EVENT_INC_DATA:
                    if (strstr ((char*)eventDataPtr, "OK") != NULL)
                    {
                        dataConnInfo.connState = DATA_CONN_STATE_CONNECT; 
                        dataConnInfo.currStateWaitTime = 1;
                        DEBUG_OUTPUT_TEXT ("context activated\n");
                    }
                    else
                        DEBUG_OUTPUT_TEXT ("not yet\n");
                    break;
                default:
                    break;
            }
            break;
        case DATA_CONN_STATE_CONNECT:
        {
            char    commandBuffer[64];
            // use the server and port number from the configuration data
            sprintf (commandBuffer, "at#sd=3,0,%d,\"%s\",255,1,0", uMIUServerConfig.sMIUServerConfig.dwServerPortNumber,
                     uMIUServerConfig.sMIUServerConfig.abyServerAddress);
            if (app_modem_sendAT(commandBuffer, "CONNECT", 5000))
            {
                        dataConnInfo.connState = DATA_CONN_STATE_CONNECTED;
                        dataConnInfo.modemConnResult = MODEM_CONN_SUCCESS;
                        dataConnInfo.currStateWaitTime = 1;
                        DEBUG_OUTPUT_TEXT ("CONNECTED\n");
            // do not wait for a response so use NULL to indicate that
//            if (app_modem_sendAT(commandBuffer, NULL, 5000))
//            {
//                dataConnInfo.connState = DATA_CONN_STATE_CONNECT_WAIT_RESPONSE;
//                dataConnInfo.modemConnResult = MODEM_CONN_SUCCESS;
//                dataConnInfo.currStateWaitTime = 30;
//            if (app_modem_sendAT("at#sd=3,0,10510,\"modules.telit.com\",255,1,0", NULL, 1000))
//            {
//                dataConnInfo.connState = DATA_CONN_STATE_CONNECT_WAIT_RESPONSE;
//                dataConnInfo.currStateWaitTime = 45;
                DEBUG_OUTPUT_TEXT ("sent #sd\n");
            }
            break;
        }
        case DATA_CONN_STATE_CONNECT_WAIT_RESPONSE:
            switch (dataConnEvent)
            {
                case DATA_CONN_EVENT_TIMEOUT:
                    dataConnInfo.modemConnResult = MODEM_CONN_FAILURE_CONNECT;
                    DEBUG_OUTPUT_TEXT ("CONNECT TIMEOUT\n");
                    break;
                case DATA_CONN_EVENT_INC_DATA:
                    if (strstr ((char*)eventDataPtr, "CONNECT") != NULL)
                    {
                        dataConnInfo.connState = DATA_CONN_STATE_CONNECTED;
                        dataConnInfo.modemConnResult = MODEM_CONN_SUCCESS;
                        dataConnInfo.currStateWaitTime = 1;
                        DEBUG_OUTPUT_TEXT ("CONNECTED\n");
                    }
                    break;
                default:
                    break;
            }
            break;
        case DATA_CONN_STATE_CONNECTED:
        {
            uint32_t    incBytesAvail;
            char        debugBuffer[128];
            char        testBuffer[12];
            static int  cnt = 0;
            
            DEBUG_OUTPUT_TEXT ("CONNECTED 2\n");
 //           sprintf (testBuffer, "test: %d ", cnt++);
 //           Uart_Uart0PutData ((uint8_t *)testBuffer, strlen(testBuffer));
            Timers_Loop_Delay (100);
            incBytesAvail = app_uart_uart0_bytesAvailable();
            if (incBytesAvail)
            {
                Uart_Uart0GetData (dataConnInfo.incDataBuffer, incBytesAvail);
                sprintf (debugBuffer, "inc data 2: %d - %s\n", incBytesAvail, dataConnInfo.incDataBuffer);
                DEBUG_OUTPUT_TEXT (debugBuffer);
            }
            dataConnInfo.currStateWaitTime = 3;
            break;
        }
        default:
            break;
    }
    
    return dataConnInfo.modemConnResult;

}

/***************************************************************************//**
 * @brief
 *   starts the process to make a 4G data connection.
 *
 * @note
 *   This function initializes things and calls the data connection state
 *   machine to start setting up a data connection.
 *       
 *
 * @param[in] none
 * @param[out] none
 *
 * @return none
 ******************************************************************************/
void app_modem_startDataConnSetup()
{
    dataConnInfo.connState = DATA_CONN_STATE_DEFINE_CONTEXT;
    app_modem_dataConnSetup (DATA_CONN_EVENT_START, NULL, 0);
}

/***************************************************************************//**
 * @brief
 *   drops the data connection.
 *
 * @note
 *   This function drops the data connection on the LTE network. Currently,
 *   we plan to do this by using the +++ escape code.
 *       
 *
 * @param[in] none
 * @param[out] none
 *
 * @return bool indicating if the command was successful
 ******************************************************************************/
bool app_modem_dropDataConnection()
{
    char    responseBuff[64];
    bool    result = false;
    
    Uart_Uart0PutData ((uint8_t *)"+++", 3);
    
// Apr282015-jef    result = app_modem_waitForResponse("NO CARRIER", responseBuff, sizeof(responseBuff), 3000);
    result = app_modem_waitForResponse("OK", responseBuff, sizeof(responseBuff), 3000);
    return result;
}

/***************************************************************************//**
 * @brief
 *   checks to see if there is anything that needs to be done to continue
 *   setting up the 4G data connection.
 *
 * @note
 *   This function first looks for incoming data from the Telit modem and then
 *   checks to see if it is time to call the state machine again. 
 *       
 *
 * @param[in] none
 * @param[out] none
 *
 * @return none
 ******************************************************************************/
void  app_modem_process_tick()
{
    uint32_t    incBytesAvail;
    char        debugBuffer[128];
    
    incBytesAvail = app_uart_uart0_bytesAvailable();
    if (incBytesAvail)
    {
//        char    tempStr[8];
//        uint8_t i;
        
        memset (dataConnInfo.incDataBuffer, 0, sizeof(dataConnInfo.incDataBuffer));
        incBytesAvail = Uart_Uart0GetData (dataConnInfo.incDataBuffer, incBytesAvail);
        sprintf (debugBuffer, "inc data: %d - %s\n", incBytesAvail, dataConnInfo.incDataBuffer);
        DEBUG_OUTPUT_TEXT (debugBuffer);
//#define DISPLAY_DATA_IN_HEX        
#ifdef DISPLAY_DATA_IN_HEX        
        sprintf (debugBuffer, "inc data: %d -", incBytesAvail);
        for (i=0; i<incBytesAvail; i++)
        {
    		sprintf (tempStr, "%02x ", dataConnInfo.incDataBuffer[i]);
    		strcat (debugBuffer, tempStr);
    		
    		if (!((i+1) % 16))   // want new line every 16 bytes
    		{
        		strcat (debugBuffer, "\n");
        		DEBUG_OUTPUT_TEXT (debugBuffer);
        		memset (debugBuffer, 0, sizeof(debugBuffer));
    		}   
            strcat (debugBuffer, "\n");
            DEBUG_OUTPUT_TEXT (debugBuffer);
        }
#endif         
        app_modem_dataConnSetup (DATA_CONN_EVENT_INC_DATA, dataConnInfo.incDataBuffer, incBytesAvail);
    }
    else
    {
//        DEBUG_OUTPUT_TEXT ("check\n");
        dataConnInfo.currStateWaitTime--;
        if (dataConnInfo.currStateWaitTime == 0)
        {
            app_modem_dataConnSetup (DATA_CONN_EVENT_TIMEOUT, NULL, 0);
        }
    }
        
}
