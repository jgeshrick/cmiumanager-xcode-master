/******************************************************************************
*******************************************************************************
**
**         Filename: app_modem_diags.h
**    
**           Author: Barry Huggins
**          Created: 3/23/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/
#include <time.h>

// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_MODEM_DIAGS_H
#define __APP_MODEM_DIAGS_H

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
#define     MODEM_DIAG_LARGE_BUFF_SIZE      256
#define     MODEM_DIAG_BUFF_SIZE            48

typedef  char   MODEM_DIAG_LARGE_BUFF_T[MODEM_DIAG_LARGE_BUFF_SIZE];
typedef  char   MODEM_DIAG_BUFF_T[MODEM_DIAG_BUFF_SIZE];

typedef enum
{
    MODEM_DIAG_LARGE_BUFF_AVAIL_OPERS,
    MODEM_DIAG_LARGE_BUFF_NETWORK_PERF,
    MODEM_DIAG_LARGE_BUFF_NUM_CMDS
} E_MODEM_DIAG_LARGE_BUFF_T;

typedef enum
{
    MODEM_DIAG_BUFF_REGISTRATION,
    MODEM_DIAG_BUFF_CURRENT_OPER,
    MODEM_DIAG_BUFF_HARDWARE_REV,
    MODEM_DIAG_BUFF_MODEL_ID,
    MODEM_DIAG_BUFF_MANU_ID,
    MODEM_DIAG_BUFF_SOFTWARE_REV,
    MODEM_DIAG_BUFF_SERIAL_NUM,
    MODEM_DIAG_BUFF_IMSI,
    MODEM_DIAG_BUFF_SIM_ID,
    MODEM_DIAG_BUFF_PIN_STATUS,
    MODEM_DIAG_BUFF_NUM_CMDS
} E_MODEM_DIAG_BUFF_T;

typedef enum
{
    MODEM_DIAG_TIME_REGISTRATION,
    MODEM_DIAG_TIME_CONTEXT_ACTIVATION,
    MODEM_DIAG_TIME_SERVER_CONNECT,
    MODEM_DIAG_TIME_DATA_TRANSFER,
    MODEM_DIAG_TIME_DISCONNECT,
    MODEM_DIAG_TIME_NUM_TASKS
} E_MODEM_DIAG_TIME_TASKS_T;

typedef struct
{
    uint32_t    startTime;
    uint16_t    duration;
} S_MODEM_DIAG_TIME_T;

typedef struct
{   
    U_NEPTUNE_TIME  lastConnectionTime;
    S_MODEM_DIAG_TIME_T     modemTaskTime[MODEM_DIAG_TIME_NUM_TASKS];
} S_MODEM_DIAG_TIME_TABLE_T; 

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/



/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
void app_modem_diag_init(void);
bool app_modem_diag_getAvailOperator(void);
bool app_modem_diag_getNetworkPerfInfo(void);
bool app_modem_diag_getRegistrationInfo(void);
bool app_modem_diag_getCurrentOperator(void);
bool app_modem_diag_getHardwareRev(void);
bool app_modem_diag_getModelId(void);
bool app_modem_diag_getManufacturerId(void);
bool app_modem_diag_getSoftwareRev(void);
bool app_modem_diag_getSerialNum(void);
bool app_modem_diag_getSimImsi(void);
bool app_modem_diag_getSimId(void);
bool app_modem_diag_getPinStatus(void);

void app_modem_diag_saveModemTaskTimeStart(E_MODEM_DIAG_TIME_TASKS_T modemTaskType);
void app_modem_diag_saveModemTaskTimeDuration(E_MODEM_DIAG_TIME_TASKS_T modemTaskType);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None

#endif // __APP_MODEM_DIAG_H

