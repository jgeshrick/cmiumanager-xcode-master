/******************************************************************************
*******************************************************************************
**
**         Filename: app_cmu.c
**    
**           Author: Troy Harstad
**          Created: 1/20/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_cmu.c
*
* @brief This file contains the application code used to setup the CMU
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include "app_cmu.h"
#include "em_cmu.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/

/***************************************************************************//**
 * @brief
 *   Initializes CMU module
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_cmu_init(void)
{ 
    
//***************************************************************************** 
//  SETUP LFXO (Low Frequency Crystal Oscillator) - 32.768 kHz   
//*****************************************************************************    
    // Use crystal oscillator for LFXO, set to 70% boost
    CMU->CTRL &= ~(_CMU_CTRL_LFXOMODE_MASK | _CMU_CTRL_LFXOBOOST_MASK);
    CMU->CTRL |= (((CMU->CTRL &~_CMU_CTRL_LFXOMODE_MASK) | CMU_CTRL_LFXOMODE_XTAL) |
                  ((CMU->CTRL &~_CMU_CTRL_LFXOBOOST_MASK) | CMU_CTRL_LFXOBOOST_70PCENT));
    
    // Enable LFXO and wait for it to stabilize
    CMU_OscillatorEnable(cmuOsc_LFXO, true, true);        
    
//***************************************************************************** 
//  SETUP HFCLK (High Frequency Clock)   
//*****************************************************************************     
    // Select HFRCO as high frequency clock (HFCLK)
    CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFRCO);

    // Set HFRCO band to 14 MHz
    CMU_HFRCOBandSet(cmuHFRCOBand_14MHz);
    
    // HFCLK Divider set to 1
    CMU_ClockDivSet(cmuClock_HF, cmuClkDiv_1);
    
    // Enable HFCLK
    CMU_ClockEnable(cmuClock_HF, true);
    

//***************************************************************************** 
//  SETUP HFCORECLK (High Frequency Core Clock
//***************************************************************************** 
    // HFCORECLK Divider set to 1
    CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_1);
    
    // Enable HFCORECLK
    CMU_ClockEnable(cmuClock_CORE, true);      

    
//***************************************************************************** 
//  SETUP HFPERCLK (High Frequency Peripheral Clock)  
//*****************************************************************************        
    // HFPERCLK Divider set to 1
    CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_1);    
    
    // Enable HFPERCLK
    CMU_ClockEnable(cmuClock_HFPER, true);  


//***************************************************************************** 
//  SETUP LFACLK (Low Frequency A Clock)
//*****************************************************************************    
    // Select LFXO as low frequency A clock (LFACLK)
    CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);  

    // Enable LFACLK
    CMU_ClockEnable(cmuClock_LFA, true);


//***************************************************************************** 
//  SETUP LFBCLK (Low Frequency B Clock)   
//*****************************************************************************     
    // Select LFXO as low frequency B clock (LFBCLK)
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);   

    // Enable LFBCLK
    CMU_ClockEnable(cmuClock_LFB, true);
    

//***************************************************************************** 
//  SETUP ULFRCO (Ultra Low Frequency RC Oscillator)    
//*****************************************************************************   
    // Enable ULFRC, don't wait for it to stabilize
    CMU_OscillatorEnable(cmuOsc_ULFRCO, true, false);

 
    
//***************************************************************************** 
//  SETUP AUXCLK (Auxiliary Clock)    
//*****************************************************************************      
    
    
   


  
  
    /* Use crystal oscillator for LFXO */
    //CMU->CTRL |= CMU_CTRL_LFXOMODE_XTAL;
    /* LFXO setup */
    /* Note: This configuration is potentially unsafe. */
    /* Examine your crystal settings. */
    CMU->CTRL    = (CMU->CTRL & ~_CMU_CTRL_LFXOBOOST_MASK) | CMU_CTRL_LFXOBOOST_70PCENT;
          

    
    
    
    
    

          



    /* Enable GPIO clock */
    CMU_ClockEnable(cmuClock_GPIO, true);
  
  
  
    
    // Disable SysTick Timer since it will wake up processor every 1 msec
 // May042015-jef    SysTick->CTRL  &= ~(SysTick_CTRL_ENABLE_Msk);  
      
  
  
  
  
  
  
// Disable the following clocks
    CMU_ClockEnable(cmuClock_USB , false);  
  
  

    
}





