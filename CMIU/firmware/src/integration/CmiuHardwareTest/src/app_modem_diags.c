/******************************************************************************
*******************************************************************************
**
**         Filename: app_modem_diags.c
**    
**           Author: Barry Huggins
**          Created: 3/23/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_modem_diags.c
*
* @brief This file contains the code used to collect diagnostics about the 
*        LTE cellular module (currently a Telit LE-910)
* 
************************************************************************/

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include "stdtypes.h"
#include "em_device.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "efm32lg_uart.h"
#include "app_modem.h"
#include "app_uart.h"
#include "app_timers.h"
#include "app_rtc.h"
#include "app_configuration.h"
#include "app_modem_diags.h"

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/

// These buffers are used to store the results of the different modem 
//   diagnostic commands. The commands with the larger responses use 
//   a larger buffer so that all the buffers do not have to be large.
MODEM_DIAG_LARGE_BUFF_T     modemDiagLargeCommandResponse[MODEM_DIAG_LARGE_BUFF_NUM_CMDS];
MODEM_DIAG_BUFF_T           modemDiagCommandResponse[MODEM_DIAG_BUFF_NUM_CMDS];

S_MODEM_DIAG_TIME_TABLE_T   modemDiagTaskTimeTable;

// This buffer holds the reply to an AT command.
char    replyBuff[200];

/***************************************************************************//**
 * @brief
 *   Initialize the cellular modem system (currently the Telit LE-910)
 *
 * @note
 *   All this does is disable power to the modem.
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void app_modem_diag_init(void)
{
    // Initialize the buffers used to store the modem diagnostic info
    memset (modemDiagLargeCommandResponse, 0, sizeof(modemDiagLargeCommandResponse));
    memset (modemDiagCommandResponse, 0, sizeof(modemDiagCommandResponse));
    
}

/***************************************************************************//**
 * @brief
 *   Gets the available LTE operator info.
 *     
 *
 * @note
 *   This function uses the AT+COPS=? command. This command takes approximately
 *   2 seconds so we do not wait for a response. It provides a list of available
 *   LTE networks.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool app_modem_diag_getAvailOperator(void)
{
    if (app_modem_sendATGetReply("AT+COPS=?", "+COPS:", replyBuff, sizeof(replyBuff), 3000))
    {
        strncpy (modemDiagLargeCommandResponse[MODEM_DIAG_LARGE_BUFF_AVAIL_OPERS], replyBuff, 
                 sizeof (modemDiagLargeCommandResponse[MODEM_DIAG_LARGE_BUFF_AVAIL_OPERS]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current LTE network performance.
 *     
 *
 * @note
 *   This function uses the AT#RFSTS command. 
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool app_modem_diag_getNetworkPerfInfo(void)
{
    if (app_modem_sendATGetReply("AT#RFSTS", "#RFSTS:", replyBuff, sizeof(replyBuff), 3000))
    {
        strncpy (modemDiagLargeCommandResponse[MODEM_DIAG_LARGE_BUFF_NETWORK_PERF], replyBuff, 
                 sizeof (modemDiagLargeCommandResponse[MODEM_DIAG_LARGE_BUFF_NETWORK_PERF]));
        return true;
    }
    
    return false;
}


/***************************************************************************//**
 * @brief
 *   Gets the current modem (currently a Telit LE-910) registration info.
 *     
 *
 * @note
 *   This function uses the AT+CGREG? command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool app_modem_diag_getRegistrationInfo(void)
{
    if (app_modem_sendATGetReply("AT+CGREG?", "CGREG:", replyBuff, sizeof(replyBuff), 3000))
    {
        strncpy (modemDiagCommandResponse[MODEM_DIAG_BUFF_REGISTRATION], replyBuff, sizeof (modemDiagCommandResponse[MODEM_DIAG_BUFF_REGISTRATION]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current LTE operator info.
 *     
 *
 * @note
 *   This function uses the AT+COPS? command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool app_modem_diag_getCurrentOperator(void)
{
    if (app_modem_sendATGetReply("AT+COPS?", "+COPS:", replyBuff, sizeof(replyBuff), 3000))
    {
        strncpy (modemDiagCommandResponse[MODEM_DIAG_BUFF_CURRENT_OPER], replyBuff, sizeof (modemDiagCommandResponse[MODEM_DIAG_BUFF_CURRENT_OPER]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the modem hardware revision.
 *     
 *
 * @note
 *   This function uses the AT#HWREV command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool app_modem_diag_getHardwareRev(void)
{
    if (app_modem_sendATGetReply("AT#HWREV", "HWREV:", replyBuff, sizeof(replyBuff), 3000))
    {
        strncpy (modemDiagCommandResponse[MODEM_DIAG_BUFF_HARDWARE_REV], replyBuff, sizeof (modemDiagCommandResponse[MODEM_DIAG_BUFF_HARDWARE_REV]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current modem (currently a Telit LE-910) model identification.
 *     
 *
 * @note
 *   This function uses the AT+CGMM command. 
 *   This command is unusual because the response does not include the command.
 *   However, for the current devices we know the modem will be an LE910.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool app_modem_diag_getModelId(void)
{
    if (app_modem_sendATGetReply("AT+CGMM", "LE910", replyBuff, sizeof(replyBuff), 3000))
    {
        strncpy (modemDiagCommandResponse[MODEM_DIAG_BUFF_MODEL_ID], replyBuff, sizeof (modemDiagCommandResponse[MODEM_DIAG_BUFF_MODEL_ID]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current modem (currently a Telit LE-910) manufacturer ID.
 *     
 *
 * @note
 *   This function uses the AT#CGMI command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool app_modem_diag_getManufacturerId(void)
{
    if (app_modem_sendATGetReply("AT#CGMI", "CGMI:", replyBuff, sizeof(replyBuff), 3000))
    {
        strncpy (modemDiagCommandResponse[MODEM_DIAG_BUFF_MANU_ID], replyBuff, sizeof (modemDiagCommandResponse[MODEM_DIAG_BUFF_MANU_ID]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current modem (currently a Telit LE-910) software revision.
 *     
 *
 * @note
 *   This function uses the AT+CGMR command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool app_modem_diag_getSoftwareRev(void)
{
    if (app_modem_sendATGetReply("AT+CGMR", "CGMR:", replyBuff, sizeof(replyBuff), 3000))
    {
        strncpy (modemDiagCommandResponse[MODEM_DIAG_BUFF_SOFTWARE_REV], replyBuff, sizeof (modemDiagCommandResponse[MODEM_DIAG_BUFF_SOFTWARE_REV]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current modem (currently a Telit LE-910) serial number. This is 
 *   also known as the IMEI
 *     
 *
 * @note
 *   This function uses the AT#CGSN command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool app_modem_diag_getSerialNum(void)
{
    if (app_modem_sendATGetReply("AT#CGSN", "CGSN:", replyBuff, sizeof(replyBuff), 3000))
    {
        strncpy (modemDiagCommandResponse[MODEM_DIAG_BUFF_SERIAL_NUM], replyBuff, sizeof (modemDiagCommandResponse[MODEM_DIAG_BUFF_SERIAL_NUM]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current SIM IMSI.
 *     
 *
 * @note
 *   This function uses the AT#CIMI command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool app_modem_diag_getSimImsi(void)
{
    if (app_modem_sendATGetReply("AT#CIMI", "#CIMI:", replyBuff, sizeof(replyBuff), 3000))
    {
        strncpy (modemDiagCommandResponse[MODEM_DIAG_BUFF_IMSI], replyBuff, sizeof (modemDiagCommandResponse[MODEM_DIAG_BUFF_IMSI]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current SIM Identification (CCID).
 *     
 *
 * @note
 *   This function uses the AT#CCID command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool app_modem_diag_getSimId(void)
{
    if (app_modem_sendATGetReply("AT#CCID", "#CCID:", replyBuff, sizeof(replyBuff), 3000))
    {
        strncpy (modemDiagCommandResponse[MODEM_DIAG_BUFF_SIM_ID], replyBuff, sizeof (modemDiagCommandResponse[MODEM_DIAG_BUFF_SIM_ID]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Gets the current modem (currently a Telit LE-910) SIM status.
 *     
 *
 * @note
 *   This function uses the AT+CPIN? command.
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the command is successful
 ******************************************************************************/
bool app_modem_diag_getPinStatus(void)
{
    if (app_modem_sendATGetReply("AT+CPIN?", "+CPIN:", replyBuff, sizeof(replyBuff), 3000))
    {
        strncpy (modemDiagCommandResponse[MODEM_DIAG_BUFF_PIN_STATUS], replyBuff, sizeof (modemDiagCommandResponse[MODEM_DIAG_BUFF_PIN_STATUS]));
        return true;
    }
    
    return false;
}

/***************************************************************************//**
 * @brief
 *   Saves the start time for the specified modem task.
 *     
 *
 * @note
 *   Currently, this routine saves the start time in msec even though it is in
 *   resolution of seconds, since we do not have a msec interrupt running.
 *       
 *
 * @param[in] modemTaskType - indicates which modem task
 *
 * @return none
 ******************************************************************************/
void app_modem_diag_saveModemTaskTimeStart(E_MODEM_DIAG_TIME_TASKS_T modemTaskType)
{
    // save the time when this modem task is started.
    modemDiagTaskTimeTable.modemTaskTime[modemTaskType].startTime = app_rtc_seconds_get() * 1000;
    
}


/***************************************************************************//**
 * @brief
 *   Saves the duration for the specified modem task.
 *     
 *
 * @note
 *   Currently, this routine computes the duration in msec even though it is in
 *   resolution of seconds, since we do not have a msec interrupt running.
 *       
 *
 * @param[in] modemTaskType - indicates which modem task
 *
 * @return none
 ******************************************************************************/
void app_modem_diag_saveModemTaskTimeDuration(E_MODEM_DIAG_TIME_TASKS_T modemTaskType)
{
    uint32_t   taskEndTime; 
    
    // get the time when this modem task completes
    taskEndTime = app_rtc_seconds_get() * 1000;
    
    // save the time when this modem task is started.
    modemDiagTaskTimeTable.modemTaskTime[modemTaskType].duration = 
            taskEndTime - modemDiagTaskTimeTable.modemTaskTime[modemTaskType].startTime;
    
}



