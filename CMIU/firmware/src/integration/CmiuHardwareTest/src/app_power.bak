/******************************************************************************
*******************************************************************************
**
**         Filename: app_power.c
**    
**           Author: Barry Huggins
**          Created: 12/23/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_power.c
*
* @brief This file contains the routines to manage the power for the 
*        CMIU module. 
* 
************************************************************************/

/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "em_device.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "efm32lg_uart.h"
#include "app_modem.h"
#include "app_power.h"
#include "app_timers.h"
#include "app_gpio.h"
#include "app_uart.h"


/***************************************************************************//**
 * @brief
 *   Function to power on the modem (currently a Telit LE-910)
 *
 * @note
 *   This function powers on the Telit modem. To do this it does the following steps:
 *       enable modem power by taking PA10 high
 *       enable the UART level shifter by taking PD8 low
 *       turn on the modem by driving PD4 high for 1 second and then low
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void app_power_modemPower_on(void)
{
    /* enable modem power (PA10) - active high */
    GPIO_PinOutSet(gpioPortA, 10);
    
    /* enable level shifter - active low */
    GPIO_PinOutClear(gpioPortD, 8);
    Timers_Loop_Delay (100);
    
    /* modem on/off - active low */
 //   GPIO->P[3].DOUT |= (1 << 4);
    
    /* modem on/off - drive high for 1 sec */
    GPIO_PinOutSet(gpioPortD, 4);
    Timers_Loop_Delay (1100);
    
    GPIO_PinOutClear(gpioPortD, 4);

    // Enable the 1 msec timer tick to allow us to time modem functions
    app_timers_enableMsecs();
}

/***************************************************************************//**
 * @brief
 *   Function to power off the modem (currently a Telit LE-910)
 *
 * @note
 *   This function powers off the Telit modem. To do this it does the following steps:
 *       enable modem power by taking PA10 high
 *       enable the UART level shifter by taking PD8 low
 *       turn on the modem by driving PD4 high for 1 second and then low
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void app_power_modemPower_off(void)
{
    /* disable level shifter (PD8) - active low */
    GPIO_PinOutSet(gpioPortD, 8);
    Timers_Loop_Delay (100);

    /* modem on/off - drive high for 2 sec */
    GPIO_PinOutSet(gpioPortD, 4);
    Timers_Loop_Delay (2100);
    
    GPIO_PinOutClear(gpioPortD, 4);

    /* disable modem power (PA10) - active high */
    GPIO_PinOutClear(gpioPortA, 10);

    // disable the 1 msec timer tick to avoid waking every msec
    app_timers_disableMsecs();    
}


/***************************************************************************//**
 * @brief
 *   Determines if the modem (currently a Telit LE-910) is currently powered on.
 *
 * @note
 *   This function determines if the Telit modem is powered on and running.
 *   It does this by looking at PD3
 *       
 *
 * @param[in] none
 *
 * @return bool indicating if the modem is powered on
 ******************************************************************************/
bool app_power_modemIsPowered(void)
{
    uint32_t     gpioInput;
    
    gpioInput = GPIO_PinInGet(gpioPortD, 3);

    if (gpioInput)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}





/***************************************************************************//**
 * @brief
 *   Function to enable 5V power supply (REG710NA-5)
 *
 * @note
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void app_power_5v_enable(void)
{
    // Enable 5V power supply
    GPIO_PinOutSet(gpioPortA, GPIO_PIN_0);
}

/***************************************************************************//**
 * @brief
 *   Function to disable 5V power supply (REG710NA-5)
 *
 * @note
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void app_power_5v_disable(void)
{
    // Disable 5V power supply
    GPIO_PinOutClear(gpioPortA, GPIO_PIN_0);
}



