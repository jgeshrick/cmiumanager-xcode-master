/******************************************************************************
*******************************************************************************
**
**         Filename: app_flash.c
**    
**           Author: Barry Huggins
**          Created: 12/23/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_flash.c
*
* @brief This file contains the main routines to access the flash memory
*        module (currently a Micron M25PX16)
* 
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "em_device.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "efm32lg_uart.h"
#include "app_flash.h"
#include "app_uart.h"
#include "app_timers.h"

/***************************************************************************//**
 * @brief
 *   Enable power to the flash memory (currently the Micron M25PX16).
 *
 * @note
 *   This function enables power to the flash memory part.
 *   It does this by taking PA9 low.
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void Flash_PowerOn(void)
{
    /* enable flash power (PA9) - active low */
    GPIO_PinOutClear(gpioPortA, 9);
    
    Timers_Delay (100);
    
}

/***************************************************************************//**
 * @brief
 *   Disable power to the flash memory (currently the Micron M25PX16).
 *
 * @note
 *   This function enables power to the flash memory part.
 *   It does this by taking PA9 high.
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void Flash_PowerOff(void)
{
    
    Timers_Delay (100);
    
    /* disable flash power (PA9) - active low */
    GPIO_PinOutSet(gpioPortA, 9);

}

/***************************************************************************//**
 * @brief
 *   Function to write the specified data to the SPI output.
 *
 * @note
 *   This function writes the given data to the SPI output. This SPI output
 *   is on USART2.
 *       
 *
 * @param[out] dataPtr 
 *               points to the data to be sent out the SPI
 * @param[out] len
 *               gives the number of bytes to be output
 *
 * @return bool indicating if successful
 ******************************************************************************/
bool  Flash_SpiWrite (uint8_t *dataPtr, uint16_t  len)
{
    int16_t     i;
    uint8_t     *tempPtr;
    
    tempPtr = dataPtr;
    
    for (i=0; i<len; i++)
    {
        USART_Tx(USART2, *tempPtr);
        tempPtr++;
    }    

    return TRUE;
}

/***************************************************************************//**
 * @brief
 *   Function to read the specified data from the SPI output.
 *
 * @note
 *   This function reads the given number of bytes from the SPI output. This SPI 
 *   is read from  USART2.
 *       
 *
 * @param[in] dataPtr 
 *               points to the buffer for the incoming data from the SPI
 * @param[out] len
 *               gives the number of bytes to be input
 *
 * @return bool indicating if successful
 ******************************************************************************/
bool  Flash_SpiRead (uint8_t *dataPtr, uint16_t  len)
{
    int16_t     i;
    uint8_t     *tempPtr;

    tempPtr = dataPtr;
    
    for (i=0; i<len; i++)
    {    
        *tempPtr = USART_Rx(USART2);
        tempPtr++;
    }
    
    return TRUE;
}

/***************************************************************************//**
 * @brief
 *   Function to read the device identification from the flash memory part.
 *
 * @note
 *   This function reads the device identification from the flash memory part
 *   using the read identification command (0x9F).
 *       
 *
 * @param[out] none 
 *
 * @return uint16_t flash identification value
 ******************************************************************************/
uint16_t  Flash_ReadDeviceIdentification()
{
    uint8_t  cmd = SPI_FLASH_INS_RDID; 
    uint8_t  identification[20]; 
    uint16_t    identificationResult;

    /* enable flash chip select (PC5) - active low */
    GPIO_PinOutClear(gpioPortC, 5);

    // Send the command
    Flash_SpiWrite (&cmd, 1);

    // Read the result
    Flash_SpiRead (identification, 20);

    /* disable flash chip select (PC5) - active low */
    GPIO_PinOutSet(gpioPortC, 5);


    // create the result ( memory type + memory capacity )
    identificationResult = identification[1];
    identificationResult <<= 8;
    identificationResult |= identification[2];

    
    return identificationResult;
}

/***************************************************************************//**
 * @brief
 *   Function to read the status register of the flash memory part.
 *
 * @note
 *   This function reads the status register of the flash memory part
 *   using the read status register command (0x05).
 *       
 *
 * @param[out] none 
 *
 * @return uint16_t flash identification value
 *
 *  rev:
 * 	Apr092015-jef: 
 *			[1] fixed bug; now grabbing correct byte
 *			[2] removed GPIO action (not necessary, for now)
 *
 ******************************************************************************/
uint8_t  Flash_ReadStatusRegister()
{
    uint8_t  cmd = SPI_FLASH_INS_RDSR; 
    uint8_t  statusRegister[2]; 

    /* flash chip select (PC5) - active low */
    // GPIO_PinOutClear(gpioPortC, 5);

    // Send the command
    Flash_SpiWrite (&cmd, 1);

    // Read the result
    Flash_SpiRead (statusRegister, 2);

    /* flash chip select (PC5) - active low */
    // GPIO_PinOutSet(gpioPortC, 5);

    
    return statusRegister[1];
}

bool  Flash_WriteEnable( void )
{
    uint8_t  cmd = SPI_FLASH_INS_WREN;; 

    // Send the command
    Flash_SpiWrite (&cmd, 1);

    return TRUE;
}

bool  Flash_WriteDisable( void )
{
    uint8_t  cmd = SPI_FLASH_INS_WRDI;; 

    // Send the command
    Flash_SpiWrite (&cmd, 1);

    return TRUE;
}

bool    Flash_TestWriteRead (void)
{
    uint8_t    testBuffer[32];
    uint8_t     commandAndAddress[4];
    char    debugBuffer[64];
    
    Uart_Uart1WriteString ("Testing flash read/write \n");
    
    Flash_WriteEnable();
    memset (testBuffer, 0, sizeof(testBuffer));
    strcpy ((char *)testBuffer, "Neptune Test!!");
    commandAndAddress[0] = SPI_FLASH_INS_PP;
    commandAndAddress[1] = 0;
    commandAndAddress[2] = 0x02;
    commandAndAddress[3] = 0;
    
    Flash_SpiWrite (commandAndAddress, 4);
    
    Flash_SpiWrite (testBuffer, 16);
    
    Flash_WriteDisable();
    
    Timers_Delay (5000);
    
    memset (testBuffer, 0, sizeof(testBuffer));
    commandAndAddress[0] = SPI_FLASH_INS_READ;
    commandAndAddress[1] = 0;
    commandAndAddress[2] = 0x02;
    commandAndAddress[3] = 0;
    
    Flash_SpiWrite (commandAndAddress, 4);
    // we seem to first have to read the 4 bytes in response to our command
    Flash_SpiRead (testBuffer, 4);

    Flash_SpiRead (testBuffer, 16);
    
    sprintf (debugBuffer, "flash read: %s\n", testBuffer);
    Uart_Uart1WriteString (debugBuffer);
    
    return TRUE;
}
