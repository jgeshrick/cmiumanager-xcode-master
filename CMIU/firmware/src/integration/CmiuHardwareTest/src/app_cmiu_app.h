/******************************************************************************
*******************************************************************************
**
**         Filename: app_cmiu_app.h
**    
**           Author: Troy Harstad
**          Created: 1/16/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_CMIU_APP_H
#define __APP_CMIU_APP_H



/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None

/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
// Define values of byRfActivationMode
#define RF_ACTIVATION_MODE_PING     0x01
#define RF_ACTIVATION_MODE_COMMAND  0x02

// Define values of cmiu_app_state
#define CMIU_APP_STATE_SWIPE            0
#define CMIU_APP_STATE_NORMAL           1

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void app_cmiu_app_init(void);
extern void app_cmiu_app_tick(void);
extern void app_cmiu_app_intervalUpdate(void);
extern uint16_t app_cmiu_app_datalogInterval_retrieve(void);
extern uint8_t app_cmiu_app_rfActivationMode_retrieve(void);
extern void app_cmiu_app_rfActivationMode_command_enter(void);
extern uint16_t app_cmiu_app_arbReadAge_retrieve(void);
extern uint8_t app_cmiu_app_state_get(void);


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/



/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_CMIU_APP_H

