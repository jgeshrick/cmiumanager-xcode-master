/******************************************************************************
*******************************************************************************
**
**         Filename: app_flash.h
**    
**           Author: Barry Huggins
**          Created: 12/31/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: 
**
** Revision History:
**         xx/xx/xx: 
**
**
**
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef __APP_FLASH_H
#define __APP_FLASH_H

#include "stdtypes.h"

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/

/******** InstructionsCode ********/ 
#define SPI_FLASH_INS_DUMMY 0xAA            			/* dummy byte*/
enum
{
	//Instruction set
	SPI_FLASH_INS_WREN        = 0x06,				/* write enable*/
	SPI_FLASH_INS_WRDI        = 0x04,					/* write disable*/
	SPI_FLASH_INS_RDID         = 0x9F,					/* Read Identification 20 bytes*/
	/*SPI_FLASH_INS_RDID1         = 0x9E,				 Read Identification 3 bytes*/
	SPI_FLASH_INS_RDSR         = 0x05,				/* read status register*/
	SPI_FLASH_INS_WRSR        = 0x01,					/* write status register*/
	SPI_FLASH_INS_WRLR        = 0xE5,      				/* Write to Lock Register*/
	SPI_FLASH_INS_RDLR        = 0xE8,       				/* Read Lock Register*/
	SPI_FLASH_INS_READ        = 0x03,					/* read data bytes*/
	SPI_FLASH_INS_FAST_READ   = 0x0B,				/* read data bytes at higher speed*/
	SPI_FLASH_INS_DOFR        = 0x3B,        			/*Dual output Fast Read*/
	SPI_FLASH_INS_DIFP         = 0xA2,        			/*Dual input Fast Read*/
	SPI_FLASH_INS_ROTP        = 0x4B,        			/*Read OTP(Read 64 Bytes of OTP area)*/
	SPI_FLASH_INS_POTP        = 0x42,        			/*Program OTP(program 64 Bytes of OTP area)*/
	SPI_FLASH_INS_PP          = 0x02,					/* page program*/
	SPI_FLASH_INS_SSE          = 0x20,					/* Subsector erase*/
	SPI_FLASH_INS_SE          = 0xD8,					/* sector erase*/
       SPI_FLASH_INS_BE          = 0xC7,					/* bulk erase*/
 	#ifndef NO_DEEP_POWER_DOWN_SUPPORT
	SPI_FLASH_INS_RDP         = 0xAB,					/* release from deep power-down*/
	SPI_FLASH_INS_DP          = 0xB9,					/* deep power-down*/
	#endif
};

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
void Flash_PowerOn(void);
void Flash_PowerOff(void);
uint16_t  Flash_ReadDeviceIdentification(void);
uint8_t  Flash_ReadStatusRegister(void);
bool  Flash_WriteEnable( void );
bool  Flash_WriteDisable( void );
bool  Flash_TestWriteRead (void);
bool  Flash_SpiWrite (uint8_t *dataPtr, uint16_t  len);
bool  Flash_SpiRead (uint8_t *dataPtr, uint16_t  len);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif // __APP_FLASH_H

