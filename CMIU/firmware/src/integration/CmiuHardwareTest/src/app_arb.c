/******************************************************************************
*******************************************************************************
**
**         Filename: ARB.c
**    
**           Author: Nick Sinas
**          Created: 12/13/2012
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: This is the ARB (2/3 wire) interface implementation
**
** Revision History:
**
**	    Code Review:
**
**    Copyright 2012 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.
**
**
******************************************************************************
******************************************************************************/


/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include "typedefs.h"
#include "em_gpio.h"
#include "Typedefs.h"
#include "app_mathFunc.h"
#include "app_timers.h"
#include "app_uart.h"
#include "app_arb.h"
#include "app_bcd2bin.h"
#include "app_error.h"
#include "app_metrology.h"
#include "app_gpio.h"
#include "em_emu.h"


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
// These are the values used as the wake intervals which determines the 
// clocking frequency.  Roughly each of these is the number of RTC clocks to achieve
// the given interval value however they were adjusted after experimentation.
#define CLK_416US_INTERVAL  12
#define CLK_180US_INTERVAL  5
#define CLK_156US_INTERVAL	3
#define CLK_1500MS_INTERVAL 49152

// This was taken from R900v3, this allows for a 3.5 second time-out
// This is used with 1200Hz clocking so a single frequency period is:
// (1/1200)*.5 = 416us
// This time-out is only incremented on the high clock
// 416us*4095*2 = 3.4 seconds
#define ARB_CLOCKING_TIMEOUT    0x0FFF

// Constants in Neptune Reading formats
#define ARB_REGISTER_DATA_STX   0x82
#define ARB_REGISTER_DATA_ETB   0x17
#define ARB_REGISTER_DATA_ETX   0x03
#define ARB_REGISTER_DATA_FORMATCODE_BASIC  0xB1
#define ARB_REGISTER_DATA_FORMATCODE_PLUS   0xB2
#define ARB_REGISTER_DATA_FORMATCODE_EV2    0x33

// Constants in Sensus Reading formats
#define ARB_REGISTER_DATA_SENSUS_FIXED      0xD2
#define ARB_REGISTER_DATA_SENSUS_VARIABLE   0x56
#define ARB_REGISTER_DATA_SENSUS_FIELD_R    0xD2
#define ARB_REGISTER_DATA_SENSUS_FIELD_I    0xC9
#define ARB_REGISTER_DATA_SENSUS_SEMICOLON  0xBB
#define ARB_REGISTER_DATA_SENSUS_COMMA      0xAC
#define ARB_REGISTER_DATA_SENSUS_PERIOD     0x2E
#define ARB_REGISTER_DATA_SENSUS_CR         0x8D

// Used to differentiate the different delimiters
// Passed as parameter when getting a field length
#define ARB_REGISTER_DATA_NEPTUNEDELIMITER  0x01
#define ARB_REGISTER_DATA_SENSUSDELIMITER   0x02

// Pulse pulse stages
#define PULSING_POWERUP_STAGE_0     0
#define PULSING_1STLOW_STAGE_1      1
#define PULSING_PLUSPULSE_STAGE_2   2
#define PULSING_2NDLOW_STAGE_3      3

// Bit patterns used in ARBV readings
#define ARBV_START_1    0x43
#define ARBV_START_0    0x7F
#define ARBV_BIT_1      0x03
#define ARBV_BIT_0      0x3F
#define ARBV_FCHAR      0x0F

#define ARB_MODE_AUTODETECT     1
#define ARB_MODE_NORMAL         2
#define ARB_MODE_NORMAL_RETRY	3

#define AUTODETECT_STAGE_1  1   // F Pulse to check for EV2
#define AUTODETECT_STAGE_2  2   // E Pulse to check for EV1, ProRead, Sensus
#define AUTODETECT_STAGE_3	3	// ARBV

// Added temporarily
#define WAKE_INTERVAL_TICKS 500

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static uint8_t ARB_ClockAndRead(void);
static uint8_t ARB_ClockAndRead_CheckRegType(void);
static uint8_t ARB_ClockAndRead_CheckID(void);

// ARB States
static void ARB_State_Register_Read_Register(void);
static void ARB_State_Register_PlusPulse(void);
static void ARB_State_Register_WaitForData(void);
static void ARB_State_Register_ReadData(void);


static void ARB_State_ARBV_Read_Pass1(void);
static void ARB_State_ARBV_Read_Pass2(void);
static void ARB_State_ARBV_WaitForData(void);
static void ARB_State_ARBV_ReadData(void);


// ARB Register Functions
static void ARB_RegisterType_SetState(void);
static void ARB_Autodetect_SetState(void);
static uint8_t ARB_Autodetect_Check_Status(void);
static void ARB_Register_ToggleClock(void);
static uint8_t ARB_PlusInterval_DataLineCheck(void);

static uint8_t ARB_PlusInterval_Handler(void);

static void ARB_TimeTracking_Start(void);
static void ARB_TimeTracking_AdjustRTC(void);
static void ARB_TimeTracking_ReadTimeout(void);

static void ARB_RegisterData_ProcessData(void);
static void ARB_RegisterData_Done(void);
static uint8_t ARB_RegisterDataParse_Neptune(void);
static uint8_t ARB_RegisterDataParse_Sensus(void);
static void ARB_RegisterDataParse_Basic(void);
static void ARB_RegisterDataParse_Plus(void);
static void ARB_RegisterDataParse_SensusFixed(void);
static void ARB_RegisterDataParse_SensusVariable(void);
static uint8_t ARB_RegisterData_ParityCheck(void);
static uint8_t ARB_NeptuneFormat_DataIntegrityCheck(void);
static uint8_t ARB_RegisterData_ConvertToBinaryID(void);
static uint8_t ARB_RegisterData_ConvertToBinaryReading(void);
static void ARB_RegisterData_SetReading_ARBError_Error(void);

static void ARB_RegisterData_SetIDToError_NonNumeric(void);
static void ARB_RegisterData_SetIDToError_NoRead(void);
static void ARB_RegisterData_SetIDToError_IDNotSupported(void);



static void ARB_RegisterData_SetReading_UnsupportedReading_Error(void);
static void ARB_RegisterData_Clear(void);
static void RegisterData_ClearReadingID(void);

static void ARB_ReceivedData_Retrieve(uint8_t byReceivedDataIndex, uint8_t *pbyRegisterData, uint8_t byCount);
static uint8_t ARB_FieldLength_Retrieve(uint8_t byReceivedDataIndex, uint8_t byDelimiterType, uint8_t byMaxLength);

// ARB ARBV Functions
static void ARB_ARBV_ProcessData(void);
static void ARB_ARBV_SetState(void);
static uint8_t ARB_ARBV_BuildReading(void);
static void ARB_ARBV_OrderReadings(uint8_t byFCharIndex);
static uint8_t ARB_ARBVReading_Parse(void);
static uint8_t ARB_ARBV_ToggleClockGetData(void);
static void ARB_ARBV_Done(void);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// Flag used to track if ARB Read is done
static uint8_t byARBDataStarted;

// Received data container
static uint8_t byReceviedData;

// Bit counter
static uint8_t byBitCounter;

// Bit mask
static uint8_t byBitMask;

// Stage of the Plus Pulse
static uint8_t byPlusPulseStage;

// Buffer and index for received data
static S_REGISTER_DATA sRegisterData;
static uint8_t abyReceviedData[34];
static uint8_t byReceviedDataIndex;
static uint8_t byReceivedDataLength;

// ARBV Variables
// (8 clocks per bit)*(4 bits per character)*(16 characters per read) = 512 total bits
// (4 bits per character)*(16 characters per read) = 64 bytes
static uint8_t abyReceviedARBVData[64];
static uint8_t byStartConditionFound;
static uint8_t byARBVPassNumber;
static uint8_t byDecodedARBVDataTemp[16];
static uint8_t byDecodedARBVDataPass1[16];
static uint8_t byDecodedARBVDataPass2[16];
// Clocking delay used before using data line
static uint8_t byARBVDelay;

// ARB Module state pointer for current state
static PFN_STATE_VOID_VOID pfnARB_State;   

// Flag used to track if ARB Read is done
static uint8_t byARBReadDone;

// Clocking Counter time-out
static uint16_t wClockingTimeout;

// Time Tracking
static uint16_t w416usTicks;
static uint16_t w156usTicks;
static uint32_t dwStartTime;

// Register Type
static uint8_t byRegisterType;

// Register Type detected on each read
static uint8_t byDetectedRegisterType;

// Plus Interval (A/B/C/D)
static uint8_t byPlusInterval;

// Mode that ARB functions in: Auto-detect or Normal
static uint8_t byARBMode;

// Auto-detect stage
static uint8_t byAutodetectStage;

// Auto-detect Plus Interval
static uint8_t byAutodetectPlusInterval;

// Auto-detect error
static uint8_t byAutodetectError;

// Retry attempts
static uint8_t byRetryAttempts;




/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/


/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_InitHW
**    
**              Author: Nick Sinas
**             Created: 12/13/2012
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: Initializes the hardware associated with the 2/3 wire
**                      subsystem.
**
******************************************************************************/
void ARB_InitHW(void) 
{
    // No hardware initialization done for this module.  
}


/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_Init
**    
**              Author: Nick Sinas
**             Created: 12/13/2012
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: Initializes ARB module.
**						Force auto-detect is called here on a reset but
**						this will be called by the magnet swipe mode anyway.
**						This will not affect the plus interval, auto-detect mode
**						uses a separate plus interval in order to preserve the
**						plus interval through a reset.
**						On a POR the Plus Interval will be set to A
**
******************************************************************************/
void ARB_Init(void)
{
	// Setup for auto-detection
	ARB_ForceAutodetect();
	// Clear the structure
	ARB_RegisterData_Clear();
	// Reset the retry attempts
	byRetryAttempts = 0;
	
	// If this is a POR initialize the Plus Interval to A
//	if((Power_ResetSource_Retrieve() & RESET_SOURCE_POR))
	{
		byPlusInterval = PLUS_INTERVAL_A;
	}		 
}

/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_ReadRegister
**    
**              Author: Nick Sinas
**             Created: 12/18/2012
**    
**  Function Arguments: None
**    Function Returns: PASS or FAIL
**
**         Description: External function to read the register
**						This will return a PASS, FAIL with retry or FAIL with
**						no retry. This notifies the RV4A_App and let's the app
**						handle the retry timing.
**
******************************************************************************/
uint8_t ARB_ReadRegister(void)
{    
    static uint8_t byRegisterReadResult = 0;
	static uint8_t byARBError = 0;

	// Attempt to read once with detected meter
    byRegisterReadResult = ARB_ClockAndRead();

	// If the read was good, done, exit passed
	if(byRegisterReadResult)
	{			
		// Only output the ARB reading info if in DEBUG_MODE
// 		#ifdef DEBUG_MODE
// 		Command_ARBReadInfo_BuildAndSend();	
// 		#endif

		// Set the mode to normal, reset retry attempts
		byRetryAttempts = 0;
		byARBMode = ARB_MODE_NORMAL;			
		return REGISTER_READ_PASS;
	}

	// If time-out, return error with no retry
	if(Error_ErrorCode_Retrieve() == ERROR_ARB_TIMEOUT)
	{
        byRetryAttempts = 0;
		DEBUG_OUTPUT_TEXT("ERROR Register Read Timeout\r\n");
		return REGISTER_READ_FAIL_NORETRY;
	}  
    
	// If mismatch register ID, return error with no retry
	if(Error_ErrorCode_Retrieve() == ERROR_ARB_DATA_MISMATCH_REG_ID)
	{
        byRetryAttempts = 0;
		DEBUG_OUTPUT_TEXT("ERROR Register Mismatch ID\r\n");
		return REGISTER_READ_FAIL_NORETRY;
	}         
    
    // Increment retry attempts
	byRetryAttempts++;
    
    // Check if in Auto-detect mode
    if(byARBMode == ARB_MODE_AUTODETECT)
    {
        if(byRetryAttempts > 2)
        {
            byRetryAttempts = 0;
            DEBUG_OUTPUT_TEXT("Autodetection failed.\r\n");
            return REGISTER_READ_FAIL_NORETRY;
        }
        DEBUG_OUTPUT_TEXT("Retrying next Autodetection Stage...\r\n");
		return REGISTER_READ_FAIL_RETRY;
    }

	// Error detected

	// If error, enter retry mode
    byARBMode = ARB_MODE_NORMAL_RETRY;	

    // Get error code
	byARBError = Error_ErrorCode_ARB_Retrieve();
    
    // The mismatch register error is only allowed one retry (other errors get two)
    // Also after the first retry if there is still a mismatch then we assume
    // the register was really changed so force enter auto-detect and clear all
    // E-Coder flags and logs.
    if(byARBError == ERROR_ARB_DATA_MISMATCH_REG_TYPE)
    {
        // If there has already been one retry attempt and
        // still a mismatch register type then force reset
        if(byRetryAttempts > 1)
        {
            // Force auto-detect, clear E-Coder flags and logs
            ARB_ForceAutodetect();
            app_metrology_Clear_FlagsAndLogs(METROLOGY_RESET_KEY);
			DEBUG_OUTPUT_TEXT_AND_DATA("Retrying, Error: ", &byARBError, 1);
            return REGISTER_READ_FAIL_NORETRY;
            
        }
        else
        {
            // Stay in retry mode
            return REGISTER_READ_FAIL_RETRY;
        }
        
        
    }
    
	// If 2 retry attempts return error with no retry    
    if(byRetryAttempts > 2)
    {
        byRetryAttempts = 0;
        // Reset the mode to normal
        byARBMode = ARB_MODE_NORMAL;		 
        DEBUG_OUTPUT_TEXT_AND_DATA("Register Read Failed, Error: ", &byARBError, 1);
        return REGISTER_READ_FAIL_NORETRY;

	}
	else // If not 2 retry attempts return error with retry
	{
		// Stay in retry mode
		DEBUG_OUTPUT_TEXT_AND_DATA("Retrying, Error: ", &byARBError, 1);
		return REGISTER_READ_FAIL_RETRY;
	}  
}


/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_ClockAndRead
**    
**              Author: Nick Sinas
**             Created: 5/14/2013
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function clocks and reads the register depending on
**						the current mode and the register type. 
**						RTC Interrupts are disabled to preserve the intervals,
**						but the state machine uses RTC wake-ups to clock the device
**						The RTC is adjusted to make up for its "hijacking"						
**
******************************************************************************/
uint8_t ARB_ClockAndRead(void)
{	
    static uint8_t byIndex;
    volatile bool bIntStatus;
    
	// Clear the register error prior to the read attempt
    Error_ErrorCode_ARB_Clear();
    
    // Reset the Done flag
    byARBReadDone = FALSE;
    
    // Reset the clocking time-out
    wClockingTimeout = 0;
    
    // Set function pointer based on register type
    ARB_RegisterType_SetState();
    
    // Enable RTC Wake-ups
	//Power_LPM_Enable_Wakeup(RTC);

	// Start Time Tracking for the RTC
	ARB_TimeTracking_Start();

	// Enable 5V Supply and Data Bias
    app_gpio_arbMode_enable();
    
    // Set the sleep time short because the first thing done is sleep
    // This was changed so the status is checked immediately following the state running
    // So if an error was encountered it exits immediately
    //RTC_EnableWakeInterval(CLK_180US_INTERVAL); 
  	app_timers_leTimer_sleepTicks(SLEEP_TICKS_200_US);
    
	// Disable smaRTClock interrupt to preserve intervals
    //EIE1 &= ~(BIT1);

    // Run the ARB state until a time-out occurs or byARBReadDone is set TRUE, meaning
    // the ARB read routine is done.
    while((!byARBReadDone) && (wClockingTimeout < ARB_CLOCKING_TIMEOUT))
    {
		// Enable RTC Wake-ups
		//////Power_LPM_Enable_Wakeup(RTC);
        
        // !!! THIS IMPLEMENTATION STILL HAS THE POSSIBILITY OF GOING TO SLEEP WITH THE 
        // !!! FLAG SET CAUSING A BRICK UNTIL RESET. THIS COULD BE MITIGATED BY ENABLING
        // !!! THE INTERRUPT AND HAVING THE ISR CHECK IF IN "ARBMODE"
        // Clear the RTC Event flag
		///////RTC_Alarm0_EventFlag_Clear();
        
        // Enter Sleep Mode and sleep for WAKE_INTERVAL            
//        Power_LPM(PMU0CF_SLEEP);		
        
        // Run ARB state
        pfnARB_State();       

        // Clear int flag
        app_timer_timer0_clearIntFlag();
        bIntStatus = false;

        while (bIntStatus == false)
        {
                // Wait for next interrupt
            bIntStatus = app_timer_timer0_intFlag_get();
            __NOP();
        }
        
    }
    
    // Ensure that 5V Supply and Data Bias are disabled
    app_gpio_arbMode_disable();
    
    // Disable Timer 0 
    app_timers_timer0_disableIntInterval();

	// Reset the wakeup interval
	///////RTC_EnableWakeInterval(WAKE_INTERVAL_TICKS);
    

	// Enable smaRTClock interrupt 
//    EIE1 |= (BIT1);    
    
    // Set the detected register type for this read
    sRegisterData.byDetectedRegisterType = byDetectedRegisterType;
    
    
    // Check if time-out just occurred or if ARBV time-out already occurred
    if((wClockingTimeout >= ARB_CLOCKING_TIMEOUT) ||
       (Error_ErrorCode_Retrieve() == ERROR_ARB_TIMEOUT))
    {
        // Set the time-out error
		Error_ARB_Data_Timeout();
		ARB_RegisterData_SetReading_ARBError_Error();
        ARB_RegisterData_SetIDToError_NoRead();
        
        // Force auto-detect and clear all flags since a time-out error has occurred
        ARB_ForceAutodetect();
        app_metrology_Clear_FlagsAndLogs(METROLOGY_RESET_KEY);
		
		// If the register type is NOT ARBV then call time tracking for a time-out
		// ARBV only clocks for 5ms
		if(byRegisterType != REGISTER_ARBV)
		{
			ARB_TimeTracking_ReadTimeout();
		}
				
        return FALSE;
    }
    
    // Adjust the RTC after it was hijacked, this can only be done if
    // there wasn't a time-out error
    ARB_TimeTracking_AdjustRTC(); 
    
    // No errors at this point, check if this was an auto-detect
    if(byARBMode == ARB_MODE_AUTODETECT)
    {
        // If the register type was found, set the register type to the detected register type
        // If the auto-detect stage failed, move it to the next stage
        if(ARB_Autodetect_Check_Status())
        {
            // Set the register type, register ID, and register ID length
			byRegisterType = byDetectedRegisterType;
			sRegisterData.byRegisterType = byRegisterType;
            sRegisterData.byRegisterIDLength = sRegisterData.byDetectedRegisterIDLength;          
                      
            for(byIndex=0; byIndex<sRegisterData.byDetectedRegisterIDLength; byIndex++)
            {
                sRegisterData.abyRegisterID[byIndex] = sRegisterData.abyDetectedRegisterID[byIndex];
            }
                       
            
			// Set the mode to normal
            byARBMode = ARB_MODE_NORMAL;
            DEBUG_OUTPUT_TEXT("Autodetection successful\r\n");
            DEBUG_OUTPUT_TEXT_AND_DATA("Register Type: ", &sRegisterData.byRegisterType, 1);
			return TRUE;
        }
		else // Auto-detect failed, but no error was found
		{
			Error_ARB_Data_Unknown_Register();
			ARB_RegisterData_SetReading_ARBError_Error();
			ARB_RegisterData_SetIDToError_NoRead();
			return FALSE;
		}      
    }
    
    
    
    // Check if the register type and detected register type match
    if(!ARB_ClockAndRead_CheckRegType())
    {
        // Found unacceptable mismatch in register type vs detected register type 
        Error_ARB_Data_Mismtach_RegisterType();
        ARB_RegisterData_SetReading_ARBError_Error();
        ARB_RegisterData_SetIDToError_NoRead();
        return FALSE;        
    }
    
    
    // Check if the register ID and detected register ID match
    // Will not return if there is a mismatch
    if(!ARB_ClockAndRead_CheckID())
    {
        // Found mismatch in register ID vs. detected register ID 
        Error_ARB_Data_Mismtach_RegisterID();
        ARB_RegisterData_SetReading_ARBError_Error();
        ARB_RegisterData_SetIDToError_NoRead();
        return FALSE;               
    }
    
    
    // Check if error was found in normal mode
    if(Error_ErrorCode_ARB_Retrieve())
    {
        // Error was set
        return FALSE;
    }  

	// Get here on a normal good read

    DEBUG_OUTPUT_TEXT_AND_DATA("Plus Interval Sent: ", &byPlusInterval, 1);
    DEBUG_OUTPUT_TEXT("Register Read Successful\r\n");
    
    // No errors found
    return TRUE;
}




/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_ClockAndRead_CheckRegType
**    
**              Author: Troy Harstad
**             Created: 5/8/2014
**
**    
**  Function Arguments: None
**    Function Returns: FALSE - Register type mismatch found
**                      TRUE - No type register mismatch
**
**         Description: This function checks to make sure the expected register
**                      type is the same as the detected register type.
**						
**
******************************************************************************/
static uint8_t ARB_ClockAndRead_CheckRegType(void)
{
    // Do not check register type in auto-detect mode (only normal and retry mode)
    if(byARBMode == ARB_MODE_AUTODETECT)
    {
        return TRUE;
    }   
    
    // Check to see if the register type was different than the
    // detected register type.
    if(sRegisterData.byRegisterType != sRegisterData.byDetectedRegisterType)
    {
        // If the register type is E-Coder V2 but we detected an E-Coder V1
        // that is OK since we clock both with A-D pulses.
        if((sRegisterData.byRegisterType == REGISTER_ECODER2) &&
           (sRegisterData.byDetectedRegisterType == REGISTER_ECODER))
        {
            return TRUE;
        }
        
        // All other register type mismatches are unacceptable
        return FALSE;       
    }  
    
    // No mismatch
    return TRUE;
}


/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_ClockAndRead_CheckID
**    
**              Author: Troy Harstad
**             Created: 5/9/2014
**
**    
**  Function Arguments: None
**    Function Returns: FALSE - Register ID mismatch found
**                      TRUE - No register ID mismatch
**
**         Description: This function checks to make sure the expected register
**                      ID is the same as the detected register ID.
**						
**
******************************************************************************/
static uint8_t ARB_ClockAndRead_CheckID(void)
{
    static uint8_t byIndex;
    
    // Do not check register ID in auto-detect mode (only normal and retry mode)
    if(byARBMode == ARB_MODE_AUTODETECT)
    {
        return TRUE;
    }
    
    // Do not check register ID when connected to an ARBV
    if(sRegisterData.byDetectedRegisterType == REGISTER_ARBV)
    {
        return TRUE;
    }
    
    // Check if ID changes
    for(byIndex=0; byIndex<sRegisterData.byDetectedRegisterIDLength; byIndex++)
    {
        if(sRegisterData.abyDetectedRegisterID[byIndex] != sRegisterData.abyRegisterID[byIndex])
        {
            
            DEBUG_OUTPUT_TEXT_AND_DATA("Expected Reg ID: 0x", &sRegisterData.abyRegisterID[0],sRegisterData.byDetectedRegisterIDLength);   
            DEBUG_OUTPUT_TEXT_AND_DATA("Detected Reg ID: 0x", &sRegisterData.abyDetectedRegisterID[0],sRegisterData.byDetectedRegisterIDLength); 
            
            // New ID (unexpected so force auto-detect and clear all metrology flags)
            ARB_ForceAutodetect();
            app_metrology_Clear_FlagsAndLogs(METROLOGY_RESET_KEY);

            return FALSE;           
        }    
    }
    
    // ID matches
    return TRUE;  
}



/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_TimeTracking_Start
**						ARB_TimeTracking_AdjustRTC
**						ARB_TimeTracking_ReadTimeout
**    
**              Author: Nick Sinas
**             Created: 4/5/2013
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function adjusts the RTC to account for the time
**						it was hijacked to conduct a register read.
**						Currently this timing is +/- 10ms, this can be improved
**						based on the register it is connected to
**						
******************************************************************************/
static void ARB_TimeTracking_Start(void)
{
	w416usTicks = 0;
	w156usTicks = 0;
/////////	dwStartTime = RTC_GetCurrentTime();
}
static void ARB_TimeTracking_AdjustRTC(void)
{
	static uint32_t dwTimeAdjust;

	// 416us intervals should be filled when reading a ProRead, ECoder, or Sensus
	// Multiply by 14 because (1/32768) = 30.5us -> 416us/30.5us = 13.6
	dwTimeAdjust = (uint32_t)(w416usTicks * 14);

	// 156us intervals should be filled when reading an ARBV
	// Multiply by 5 because (1/32768) = 30.5us -> 156us/30.5us = 5 
	dwTimeAdjust += (uint32_t)(w156usTicks * 5);

    // Adjust based on register type
    
	// Add the current time the RTC was at before it was hijacked 
	dwTimeAdjust += dwStartTime;
    
    // Check for overflow, and check if close to boundary, 10 ticks was selected
    if(dwTimeAdjust >= (WAKE_INTERVAL_TICKS - 10))
    {
        // Reset current time
        dwTimeAdjust = 0;
    }    

    // Set Wakeup Interval and Current Time
///////////    RTC_Set_Timeout_And_CurrentTime(WAKE_INTERVAL_TICKS, dwTimeAdjust);	

}
static void ARB_TimeTracking_ReadTimeout(void)
{
	uint8_t byCount;
	
	// 3.5 seconds of clocking, this decrements the interval values to make up for that time
	// By calling each one 3 times to simulate 3 seconds
	for(byCount = 0; byCount < 3; byCount++)
	{
// ADD BACK FOR CMIU!!!
// 		// Decrement the interval counter values for all tasks
// 	    RV4A_App_IntervalUpdate();
// 		
// 		// Decrement the interval values for commands
// 		Command_TimeoutUpdate();
// 	    
// 	    // Increment general purpose tick timer value
// 	    RTC_TickTimer_Increment();
	}
}



/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_RegisterType_SetState
**    
**              Author: Nick Sinas
**             Created: 3/1/2013
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function sets function pointer state
**						machine based on the register type and the mode
**						
**						
******************************************************************************/
static void ARB_RegisterType_SetState(void)
{
    // Check if Auto-detect mode
    if(byARBMode == ARB_MODE_AUTODETECT)
    {
        // Let auto-detect set the state
        ARB_Autodetect_SetState();
    }
    else // Normal Mode
    {
        // Set the state based on the register type
        switch(byRegisterType)
        {
            case REGISTER_UNKNOWN:
                // Auto-detect
                ARB_ForceAutodetect();
                ARB_Autodetect_SetState();
                break;
			// Sensus, ProRead and ECoders are all clocked with the same pulse
			// It was determined that the R900v3 clocks all registers but the ARBV the same (A/B/C/D)
			case REGISTER_SENSUS_FIXED:
            case REGISTER_SENSUS_VAR:
            case REGISTER_PROREAD:
            case REGISTER_ECODER:
            case REGISTER_ECODER2:  
                pfnARB_State = ARB_State_Register_Read_Register; 
                break;
            case REGISTER_ARBV:
                pfnARB_State = ARB_State_ARBV_Read_Pass1;
                break;
            default:
                // Auto-detect
                ARB_ForceAutodetect();
                ARB_Autodetect_SetState();
                break;                 
        } 
    }           
}


/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_RegisterData_Retrieve
**    
**              Author: Nick Sinas
**             Created: 3/1/2013
**    
**  Function Arguments: None
**    Function Returns: pointer to S_REGISTER_DATA
**
**         Description: This function retrieves a pointer to the RegisterData
**                      structure.
**						This is used externally to gain access to the current 
**						reading data. 
**						
**
******************************************************************************/
S_REGISTER_DATA* ARB_RegisterData_Retrieve(void)
{
    // return a pointer to the Register Data structure
    return (&sRegisterData);
}


/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_ForceAutodetect
**    
**              Author: Nick Sinas
**             Created: 3/1/2013
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function forces an auto-detect on the next read by
**						setting up the variables that determine the mode and
**						register type
**						
**
******************************************************************************/
void ARB_ForceAutodetect(void)
{
    byARBMode = ARB_MODE_AUTODETECT;
    byRegisterType = REGISTER_UNKNOWN;
    sRegisterData.byRegisterType = REGISTER_UNKNOWN;
	byDetectedRegisterType = REGISTER_UNKNOWN;  
    sRegisterData.byDetectedRegisterType = REGISTER_UNKNOWN;
	byAutodetectStage = AUTODETECT_STAGE_1;           
}


/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_ReadRegister_OnDemand
**    
**              Author: Nick Sinas
**             Created: 3/1/2013
**    
**  Function Arguments: Plus Interval and Register Type
**    Function Returns: read status - PASS or FAIL
**
**         Description: This function is used to read a register on demand
**						No retries.	The mode and interval are preserved.
**						
**
******************************************************************************/
uint8_t ARB_ReadRegister_OnDemand(uint8_t byNewPlusInterval, uint8_t byNewRegisterType)
{
    uint8_t bySaveInterval;
    uint8_t byReadStatus;
    uint8_t bySaveRegisterType;
    uint8_t bySaveARBMode;
    
    // Force to normal mode
    bySaveARBMode = byARBMode;
    byARBMode = ARB_MODE_NORMAL;
	
    // Check if this is an auto-detect request
    if(byNewRegisterType == REGISTER_UNKNOWN)
    {
        // This will enter auto-detect mode
        ARB_ForceAutodetect();
    }
    else if(byNewRegisterType == REGISTER_USEDETECTED)
    {        
        // Save the register type 
        bySaveRegisterType = byRegisterType;
        // Do not change the register type
    }
    else
    {
        // Save the register type 
        bySaveRegisterType = byRegisterType;
        //Set the temporary register type
        byRegisterType = byNewRegisterType;
    }

    // What about using the detected register and interval??
    if(byNewPlusInterval != PLUS_INTERVAL_NEXT)
    {
        // Set the plus interval
        bySaveInterval = byPlusInterval;
        byPlusInterval = byNewPlusInterval;
    }
    
    // Read register
    byReadStatus = ARB_ClockAndRead();
    
    // Restore interval, register type and mode
    byPlusInterval = bySaveInterval; 
    byRegisterType = bySaveRegisterType;
    sRegisterData.byDetectedRegisterType = bySaveRegisterType;
    sRegisterData.byRegisterType = bySaveRegisterType;    
    byARBMode = bySaveARBMode;
    
    return byReadStatus;   
}



/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_PlusInterval_Handler
**    
**              Author: Nick Sinas
**             Created: 3/1/2013
**    
**  Function Arguments: None
**    Function Returns: Plus Interval to use to determine the timing to make
**						the high plus pulse
**
**         Description: This function will return the E or F pulse when in 
**						auto-detect mode. If in normal mode, it will determine
**						the next interval to use and return that value
**						A -> B -> C -> D -> A -> B -> C -> D -> A ...
**						
**
******************************************************************************/
static uint8_t ARB_PlusInterval_Handler(void)
{   
    // If in auto-detect mode, use the pulse interval that was already set
	if(byARBMode == ARB_MODE_AUTODETECT)
	{
		return byAutodetectPlusInterval;
	}
	
    // If in retry mode, use the E Pulse
    if(byARBMode == ARB_MODE_NORMAL_RETRY)
    {
    	return PLUS_INTERVAL_E;
    }
    
    // Set the next interval   
    if(byPlusInterval >= PLUS_INTERVAL_D)
    {
        byPlusInterval = PLUS_INTERVAL_A;
    }
    else
    {
        byPlusInterval++;
    }
    
    return byPlusInterval;
}


/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_Autodetect_SetState
**    
**              Author: Nick Sinas
**             Created: 03/04/2012
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: Auto-detection scheme:
**						Stage 1: F Pulse, this checks for an ECoderV2
**						Stage 2: (if no EV2) E Pulse (everything but ARBV)
**						Stage 3: ARBV
**
**                      This was changed to be a function that sets the state
**                      so upon a good read, this will exit quicker.
**						
**
******************************************************************************/
static void ARB_Autodetect_SetState(void)
{

	// Auto-detect sends an F Pulse to check for EV2
    // If 0x33 is found it is an ECoder V2
    // Auto-detect sends an E Pulse.
    // How each register handles the E pulse:
    // ECoders: detects the plus pulse and responds with plus data
    // ProRead: ignores plus pulse and responds to 1200Hz with basic data
    // Sensus: ignores plus pulse and responds to 1200Hz with variable or fixed format
    // ARBV: responds oddly to 1200Hz, this would set the unknown register flag
    //       this would call ARBV read routines
    // If data is still bad after ARBV routines, the RegisterType is still set to unknown
    switch(byAutodetectStage)
	{
		case AUTODETECT_STAGE_1:			
			byAutodetectError = ERROR_CODE_NO_ERROR;
			// Set F Pulse
        	byAutodetectPlusInterval = PLUS_INTERVAL_F;
			// Run ECoder state
		    pfnARB_State = ARB_State_Register_Read_Register; 
		    // Setup Timer 0 for 1200 Hz (416us interrupt intervals)
		    app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_1200HZ);		
			break;
		case AUTODETECT_STAGE_2:			    	    
        	// Set E Pulse
        	byAutodetectPlusInterval = PLUS_INTERVAL_E;
			// Run ECoder State
			pfnARB_State = ARB_State_Register_Read_Register;			
		    // Setup Timer 0 for 1200 Hz (416us interrupt intervals)
		    app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_1200HZ);		
			break;
		case AUTODETECT_STAGE_3:			
			// ARBV
			pfnARB_State = ARB_State_ARBV_Read_Pass1;
            // Setup Timer 0 for 3200 Hz (156us interrupt intervals)
            app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_3200HZ);		
			break;
		default:
            // Reset the stage
            byAutodetectStage = AUTODETECT_STAGE_1;			
			// Set F Pulse
        	byAutodetectPlusInterval = PLUS_INTERVAL_F;
			// Run ECoder state
		    pfnARB_State = ARB_State_Register_Read_Register; 
		    // Setup Timer 0 for 1200 Hz (416us interrupt intervals)
		    app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_1200HZ);	
			return;
	}   
}


/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_Autodetect_Check_Status
**    
**              Author: Nick Sinas
**             Created: 03/04/2012
**    
**  Function Arguments: None
**    Function Returns: TRUE or FALSE if expected register was detected for
**                      that stage.
**
**         Description: Used to determine if the Auto-detection was successful
**						
**
******************************************************************************/
static uint8_t ARB_Autodetect_Check_Status(void)
{
    switch(byAutodetectStage)
	{
		// ECoderV2 Check
        case AUTODETECT_STAGE_1:
            // Check if Stage 1 passed
			if(byDetectedRegisterType == REGISTER_ECODER2)
			{
				// Success 
				Error_ErrorCode_ARB_Clear();
				return TRUE;	
			}
            // Increment the stage 
			byAutodetectStage++;
            // Display the error
			byAutodetectError = Error_ErrorCode_ARB_Retrieve(); 
			DEBUG_OUTPUT_TEXT_AND_DATA("Autodetect Stage I Failed, Error: ", &byAutodetectError, 1);
			DEBUG_WAITFORUART();
			break;
        // ECoder/ProRead/Sensus Check    
        case AUTODETECT_STAGE_2:			
			// Check if the meter type was found
			if(byDetectedRegisterType != REGISTER_UNKNOWN)
			{
				// Success
				Error_ErrorCode_ARB_Clear();
				return TRUE;	
			}
            // Increment the stage 
			byAutodetectStage++;
            // Display the error
			byAutodetectError = Error_ErrorCode_ARB_Retrieve(); 
			DEBUG_OUTPUT_TEXT_AND_DATA("Autodetect Stage II Failed, Error: ", &byAutodetectError, 1);
			DEBUG_WAITFORUART();
			break;
		// ARBV Check
        case AUTODETECT_STAGE_3:
			// Check if the meter type was found
			if(byDetectedRegisterType != REGISTER_UNKNOWN)
			{
				// Success
				Error_ErrorCode_ARB_Clear();
				return TRUE;	
			}
            // Reset the stage
            byAutodetectStage = AUTODETECT_STAGE_1;            
            // If ARBV resulted in a time-out error, then use the previous data error
			// this is only done in auto-detection because the ARBV time-out is 5ms which 
			// all registers other than ARBV cannot respond within, masking a data error
			if(Error_ErrorCode_ARB_Retrieve() == ERROR_ARB_TIMEOUT)
			{
				 Error_ErrorCode_ARB_Set(byAutodetectError);
			}            
            // Display the error
			byAutodetectError = Error_ErrorCode_ARB_Retrieve(); 
			DEBUG_OUTPUT_TEXT_AND_DATA("Autodetect Stage III Failed, Error: ", &byAutodetectError, 1);
			DEBUG_WAITFORUART();
			break;
		// Unknown Stage
        default:
            // Reset the stage
            byAutodetectStage = AUTODETECT_STAGE_1;
			return FALSE;
	}

    return FALSE;
}


/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_Variables_Clear
**    
**              Author: Nick Sinas
**             Created: 05/02/2013
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function clears the global variables used in the 
**						reading routines
**						
**
******************************************************************************/
void ARB_Variables_Clear(void)
{
	byReceviedData = 0;
    byBitCounter = 0;
    byPlusPulseStage = 0;
    byReceviedDataIndex = 0;
    byBitMask = BIT0;
    byARBDataStarted = 0;
    wClockingTimeout = 0;
    byReceivedDataLength = 0;
	RegisterData_ClearReadingID();
}


/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_State_Register_Read_Register
**    
**              Author: Nick Sinas
**             Created: 12/19/2012
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: Setup state for reading a register other than ARBV
**						
**
******************************************************************************/
void ARB_State_Register_Read_Register(void)
{
    // Reset variables
    ARB_Variables_Clear();	
    
    // Setup Timer 0 for 1200 Hz (416us interrupt intervals)
    app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_1200HZ);
    
    // Set state to 1200Hz Read
    pfnARB_State = ARB_State_Register_PlusPulse;
    
    // Enable 5V Supply and Data Bias
    app_gpio_arbMode_enable(); 
}

/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_State_Register_PlusPulse
**    
**              Author: Nick Sinas
**             Created: 12/19/2012
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: Sends the power up and plus pulses 
**
**                      PLUS clocking:						
**                 __________________________     _________     ____      ____
**                |                         |____|        |____|    |____|    |...
**                         55ms              416us  PLUS       1 2 0 0 H z 
**                                                   ^
**                                                A/B/C/D
**
******************************************************************************/
void ARB_State_Register_PlusPulse(void)
{  
//     // Sleep Timing values found my experiment
//     // These are very close to CLK_416US_INTERVAL*2,3,4,5,6 but some time was added to make more accurate
//     // Values found: A[835us], B[1.260ms], C[1.660ms], D[2.055ms], E[2.505ms], F[2.91ms]
//     // The first 2 indexes are unused but have default values of 416us timing
//     static uint8_t abyPlusIntervalTime[8] = {CLK_416US_INTERVAL, CLK_416US_INTERVAL, 25, 39, 52, 65, 80, 93};

//     // Sleep Timing values found my experiment
//     // These are very close to CLK_416US_INTERVAL*2,3,4,5,6 but some time was added to make more accurate
//     // Values found: A[835us], B[1.260ms], C[1.660ms], D[2.055ms], E[2.505ms], F[2.91ms]
//     // The first 2 indexes are unused but have default values of 416us timing
     static uint8_t abyPlusIntervalTime[8] = {SLEEP_TICKS_PLUS_PULSE_A, SLEEP_TICKS_PLUS_PULSE_A, 
                                              SLEEP_TICKS_PLUS_PULSE_A, SLEEP_TICKS_PLUS_PULSE_B, 
                                              SLEEP_TICKS_PLUS_PULSE_C, SLEEP_TICKS_PLUS_PULSE_D, 
                                              SLEEP_TICKS_PLUS_PULSE_E, SLEEP_TICKS_PLUS_PULSE_F};


    switch(byPlusPulseStage)
    {
        // First 55ms 'power up' high pulse
        case PULSING_POWERUP_STAGE_0:
            // Set line high
            GPIO_PinOutSet(ARB_CLOCK_PORT,ARB_CLOCK_PIN);
        
            // RTC sleep for 55 ms
            // 152 was found experimentally
            //RTC_EnableWakeInterval(152 * CLK_416US_INTERVAL);
            app_timers_leTimer_sleepTicks(SLEEP_TICKS_55_MS);
        
			// Update Time Tracking
			w416usTicks = 152;
            // Go to next state
            byPlusPulseStage++;
            break;
        // First 416 us low pulse
        case PULSING_1STLOW_STAGE_1:
            // Set line low
            GPIO_PinOutClear(ARB_CLOCK_PORT,ARB_CLOCK_PIN);
            // Setup Timer 0 for 1200 Hz (416us interrupt intervals)
            app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_1200HZ);  
			// Update Time Tracking
			w416usTicks++;         
            // Go to next state
            byPlusPulseStage++;
            break;
        // High for Plus Pulse
        case PULSING_PLUSPULSE_STAGE_2:
            GPIO_PinOutSet(ARB_CLOCK_PORT,ARB_CLOCK_PIN);
            // Check if the data line moved during the plus pulse
            if(ARB_PlusInterval_DataLineCheck())
            {
            	// Data has started, go straight to reading data
            	pfnARB_State = ARB_State_Register_ReadData;
            	// Setup Timer 0 for 1200 Hz (416us interrupt intervals)
                app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_1200HZ);
            	// Update Time Tracking
            	w416usTicks++;
            }
            else
            {
                // RTC sleep for interval based on current pulse
               app_timers_leTimer_sleepTicks(abyPlusIntervalTime[ARB_PlusInterval_Handler()]); 
                // Update Time Tracking
                w416usTicks += byPlusInterval;
            }			
            		
            // Go to next state
            byPlusPulseStage++;
            break;
        // Second 416 us low pulse
        case PULSING_2NDLOW_STAGE_3:           
            // Set line low
            GPIO_PinOutClear(ARB_CLOCK_PORT,ARB_CLOCK_PIN);
            // Setup Timer 0 for 1200 Hz (416us interrupt intervals)
            app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_1200HZ);
			// Update Time Tracking
			w416usTicks++;
             // Set state to 1200Hz Read to get the data
            pfnARB_State = ARB_State_Register_WaitForData;            
            break;
		default:
			// Set line high
            GPIO_PinOutSet(ARB_CLOCK_PORT,ARB_CLOCK_PIN);
            // Setup Timer 0 for 1200 Hz (416us interrupt intervals)
            app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_1200HZ);
			// Update Time Tracking
			w416usTicks++;
             // Set state to 1200Hz Read to get the data
            pfnARB_State = ARB_State_Register_WaitForData;            
            break;			           
    }
}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_PlusInterval_DataLineCheck
**    
**              Author: Nick Sinas
**             Created: 7/26/2013
**    
**  Function Arguments: None
**    Function Returns: Data started ==  TRUE, if not FALSE
**
**         Description:	This function is called during the PLUS pulse to check
**						if data started
**
**                      ARB_CLK and ARB_DATA are inverted between the MCU and
**                      the 3-wire ARB interface.
**						
******************************************************************************/
static uint8_t ARB_PlusInterval_DataLineCheck(void)
{
    // Save Data line          
    if(GPIO_PinInGet(ARB_DATA_PORT,ARB_DATA_PIN) == 0)
    {
    	byARBDataStarted = 1;
    	byBitCounter++;
    	return TRUE;  
    } 
	
    return FALSE;   
}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_State_Register_WaitForData
**						ARB_Register_ToggleClock
**    
**              Author: Nick Sinas
**             Created: 12/19/2012
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description:	ToggleClock toggles the clock line
**						WaitForData is called until data starts. If no register
**						is connected, this state will be called until a time-out
**						occurs.
**
**                      ARB_CLK and ARB_DATA are inverted between the MCU and
**                      the 3-wire ARB interface.
**						
******************************************************************************/
static void ARB_State_Register_WaitForData(void)
{
    static uint8_t byDataLineSnapshot;   
    
    // Toggle CLK line            
    ARB_Register_ToggleClock();
    
    // If this is a low clock period, return, only process on high clock
    if(!GPIO_PinInGet(ARB_CLOCK_PORT,ARB_CLOCK_PIN))
    {
        return;
    }
    
    // Save Data line          
    if(GPIO_PinInGet(ARB_DATA_PORT,ARB_DATA_PIN))
    {
        byDataLineSnapshot = 1;
    }
    else
    {
        byDataLineSnapshot = 0;
    }
    
    // Check if data has started
    if(!byARBDataStarted)
    {
        // Check if line is low indicating a start bit
        if(byDataLineSnapshot == 0)
        {
            byARBDataStarted = 1;
			byBitCounter++;
			pfnARB_State = ARB_State_Register_ReadData;
			return;  
        }
        else // Line is still high, continue clocking
        {            
            return;
        }
    }

	// Sanity
    pfnARB_State = ARB_State_Register_ReadData; 
}

// This function toggles the CLK for an register read
static void ARB_Register_ToggleClock(void)
{	
    
    // Toggle CLK line            
    if(GPIO_PinInGet(ARB_CLOCK_PORT,ARB_CLOCK_PIN))
    {
        // Time-out is only incremented on high clock
        wClockingTimeout++;
        GPIO_PinOutClear(ARB_CLOCK_PORT,ARB_CLOCK_PIN);
		w416usTicks++;
    }
    else
    {
        GPIO_PinOutSet(ARB_CLOCK_PORT,ARB_CLOCK_PIN);
		w416usTicks++;
    }    
    
}

/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_State_Register_ReadData
**    
**              Author: Nick Sinas
**             Created: 12/19/2012
**
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This state works by checking the data line for data
**                      once data begins it shifts in bits based on the data line
**                      once 11 bits have been received it creates and saves a byte
**                      to the data buffer
**
**                      ARB_CLK and ARB_DATA are inverted between the MCU and
**                      the 3-wire ARB interface.
**						
******************************************************************************/
static void ARB_State_Register_ReadData(void)
{
    static uint8_t byDataLineSnapshot;   
    
	// Toggle Clock
	ARB_Register_ToggleClock();

    // If this is a low clock period, return, only process on high clock
    if(!GPIO_PinInGet(ARB_CLOCK_PORT,ARB_CLOCK_PIN))
    {
        return;
    }
    
    // Save Data line          
    if(GPIO_PinInGet(ARB_DATA_PORT,ARB_DATA_PIN))
    {
        byDataLineSnapshot = 1;
    }
    else
    {
        byDataLineSnapshot = 0;
    }
          
    
    // Start bit
    if(byBitCounter == 0)   
    {
        // Ignore the Start bit
        // Increment bit counter for next bit
        byBitCounter++;
        return;
    }
    
    // Stop bits
    if(byBitCounter > 8)
    {
        // Waiting for Start Bit
        // Check if this is still not a Stop bit
        if(byDataLineSnapshot != 0)
        {
            return;
        }
        else
        {
            // Received Start bit
            byReceviedData = 0;
            byBitCounter = 1; 
            byBitMask = BIT0;
            return;
        }        
    }  
    
    // If a 1 has been detected OR in the mask
    if(byDataLineSnapshot)
    {
        byReceviedData |= byBitMask;
    }
    
    // Shift the mask for the next bit
    byBitMask <<= 1;
    
    // Check if whole byte has been received
    if(byBitCounter == 8)
    {
        // Save byte to buffer
        abyReceviedData[byReceviedDataIndex] = byReceviedData;
        byReceviedDataIndex++;
    }
    
    // Increment bit counter for next bit
    byBitCounter++;
    
    // Check if 34 bytes have been received or if a <CR> (Sensus end format) has been received
    if((byReceviedDataIndex >= 34) || (byReceviedData == ARB_REGISTER_DATA_SENSUS_CR))
    {
        // Save length, since we stored [33] above then incremented to 34, byReceviedDataIndex
        // is the real length (no longer need to decrement as was done in previous versions)
        byReceivedDataLength = byReceviedDataIndex;

		// Disable ARB lines for power savings
        app_gpio_arbMode_disable();
        
		// Process the received data
        ARB_RegisterData_ProcessData();        
        
    }    
    
}

/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_State_Register_ProcessData
**    
**              Author: Nick Sinas
**             Created: 12/19/2012
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function processes the data received to determine if the 
**                      response was valid. Auto-detection mode is handled here.
**						
******************************************************************************/
static void ARB_RegisterData_ProcessData(void)
{    	
    // RTC will be adjusted later, this is set high in case processing takes longer than 416us
    ////RTC_EnableWakeInterval(WAKE_INTERVAL_TICKS);

    // First check the parity of the data
    if(!ARB_RegisterData_ParityCheck())
    {
        // Depending on the mode, exit or go to the next auto-detect state
        // Error, reading and ID already set
        ARB_RegisterData_Done();		
    	return;        
    }

	// Good parity at this point
    
    // Determine how to handle the data received by looking at the fist byte
    switch(abyReceviedData[0])
    {
        // Check for a Neptune format (STX)
        case ARB_REGISTER_DATA_STX:
            // Try to parse data as a Neptune format
            ARB_RegisterDataParse_Neptune();
            break;
        // Check for a Sensus formats (V or R)
        case ARB_REGISTER_DATA_SENSUS_FIXED:
        case ARB_REGISTER_DATA_SENSUS_VARIABLE:
            ARB_RegisterDataParse_Sensus();
            break;
        default:
            // Let the retries handle this
            Error_ARB_Data_Unknown_Register();
            ARB_RegisterData_SetReading_ARBError_Error();
            ARB_RegisterData_SetIDToError_NoRead();
            break;
    }
	
    // Done
	ARB_RegisterData_Done();   
}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_RegisterData_Done
**    
**              Author: Nick Sinas
**             Created: 8/31/2013
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function ensures that the ARB lines are turned off 
**                      and sets the variable to exit the state machine
**
**						
******************************************************************************/
static void ARB_RegisterData_Done(void)
{
    // Disable ARB lines for power savings
    app_gpio_arbMode_disable();
	
	// Exit
	byARBReadDone = TRUE;

}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_RegisterData_ParityCheck
**    
**              Author: Nick Sinas
**             Created: 2/21/2013
**    
**  Function Arguments: None
**    Function Returns: If all of the data in the received data buffer is even
**						parity it returns TRUE, and FALSE otherwise
**
**         Description: This function checks to see if each byte is even parity
**
**						
******************************************************************************/
static uint8_t ARB_RegisterData_ParityCheck(void)
{
    static uint8_t byIndex;
    static uint8_t byParityCheck;
    static uint8_t byParity;
    
    // Loop through each byte and confirm it is even parity
    for(byIndex = 0; byIndex < byReceivedDataLength; byIndex++)
    {
        byParity = 0;
        byParityCheck =  abyReceviedData[byIndex];
        while(byParityCheck)
        {
            byParity ^= 1;
            byParityCheck = byParityCheck & (byParityCheck - 1);
        }
        
        // Parity will be non-zero if odd parity
        if(byParity)
        {
            // Set error
	        Error_ARB_Data_Parity();
			ARB_RegisterData_SetReading_ARBError_Error();
			ARB_RegisterData_SetIDToError_NoRead();
			return FALSE;
        }   
    }
    
    return TRUE;
}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_RegisterDataParse_Neptune
**    
**              Author: Nick Sinas
**             Created: 2/20/2013
**    
**  Function Arguments: none
**    Function Returns: uint8_t TRUE or FALSE
**
**         Description: This functions determines how to decode the data in the 
**						received data buffer.
**						
******************************************************************************/
static uint8_t ARB_RegisterDataParse_Neptune(void)
{
    // Confirm number of ETB's and Checksum
    if(!ARB_NeptuneFormat_DataIntegrityCheck())
    {
        // Error was already set
        return FALSE;
    }
    // Check if Basic (B1)        
    if(abyReceviedData[1] == ARB_REGISTER_DATA_FORMATCODE_BASIC)
    {
        byDetectedRegisterType = REGISTER_PROREAD;
        ARB_RegisterDataParse_Basic();
    }
    // Check if Plus Message B2
    else if(abyReceviedData[1] == ARB_REGISTER_DATA_FORMATCODE_PLUS)
    {
        byDetectedRegisterType = REGISTER_ECODER;
        ARB_RegisterDataParse_Plus();
    }
    // Check if Plus Message 33 for F Pulse response
    else if(abyReceviedData[1] == ARB_REGISTER_DATA_FORMATCODE_EV2)
    {
		byDetectedRegisterType = REGISTER_ECODER2;
        ARB_RegisterDataParse_Plus();
    }
    else
    {
        // Error 
        Error_ARB_Data_Unknown_Register();
		ARB_RegisterData_SetReading_ARBError_Error();
		ARB_RegisterData_SetIDToError_NoRead();
        byDetectedRegisterType = REGISTER_UNKNOWN;
        return FALSE;
    }
    
    return TRUE;
}

/******************************************************************************
**
**            Filename: LCD.c
**        Routine Name: ARB_NeptuneFormat_DataIntegrityCheck
**    
**              Author: Nick Sinas
**             Created: 2/21/2013
**    
**  Function Arguments: None
**    Function Returns: uint8_t TRUE or FALSE
**
**         Description: This function checks to see if the Neptune formatted
**                      data received contains 5 ETB's and the checksum matched
**                      a calculated checksum
**						
******************************************************************************/
static uint8_t ARB_NeptuneFormat_DataIntegrityCheck(void)
{
    static uint8_t byETBCount;
    static uint8_t byCalculatedChecksum;
    static uint8_t byChecksumMSB;
    static uint8_t byChecksumLSB;
    static uint8_t byIndex;
    
    // This format counts the number of ETB's in the format.
    // There must be 5 ETB's.
    
    // It also calculates the checksum
    // The checksum is the 2 bytes after the 5th ETB
    // It needs to be converted from 2 even parity ASCII bytes to hex
    // The 1st and last index (STX and ETX) are not included in the checksum
    
    byCalculatedChecksum = 0;
    byETBCount = 0;
    for(byIndex = 1; byIndex < byReceivedDataLength; byIndex++)
    {
        byCalculatedChecksum += abyReceviedData[byIndex];
        // Check if byte is an ETB
        if(abyReceviedData[byIndex] == ARB_REGISTER_DATA_ETB)
        {
            // It is an ETB, increment ETB counter
            byETBCount++;
            // Check if this is the 5th ETB, the checksum follows this
            if(byETBCount == 5)
            {
                // Get the checksum bytes
                byChecksumMSB = abyReceviedData[byIndex + 1];
                byChecksumLSB = abyReceviedData[byIndex + 2];
                // Make these 2 even parity ASCII bytes hex
                MathFunc_AsciiToHex(&byChecksumMSB);
                MathFunc_AsciiToHex(&byChecksumLSB);
                byChecksumMSB = byChecksumMSB << 4;
                byChecksumMSB = (byChecksumMSB | byChecksumLSB);
                // Compare to the calculated checksum
                if(byChecksumMSB == byCalculatedChecksum)
                {
                    return TRUE;
                }
                else
                {
                    // Bad checksum
					Error_ARB_Data_Checksum();
					ARB_RegisterData_SetReading_ARBError_Error();
					ARB_RegisterData_SetIDToError_NoRead();
                    return FALSE;
                }
            }
        }
    }
    
    // 5 ETB's were not found 
    Error_ARB_Data_Format();
	ARB_RegisterData_SetReading_ARBError_Error();
	ARB_RegisterData_SetIDToError_NoRead();
    return FALSE;
    
}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_RegisterDataParse_Basic
**    
**              Author: Nick Sinas
**             Created: 2/20/2013
**    
**  Function Arguments: none
**    Function Returns: none
**
**         Description: This function parses the raw abyReceviedData data
**                      and extracts the Reading and ID and saves them to the
**                      RegisterData structure.
**                      This function is called when Neptune BASIC format is
**                      detected.
**						
******************************************************************************/
static void ARB_RegisterDataParse_Basic(void)
{
    static uint8_t byETBFound;
    static uint8_t byReadIndex;
    static uint8_t byReadLength;
    static uint8_t byIDIndex;
    static uint8_t byIDLength;
    static uint8_t byIndex;
    static uint8_t byIDOffset;
    
    byETBFound = FALSE;
    byIndex = 1;
    
    // Reading is located after the first ETB
    // This is not blocking as 5 ETBs have already been confirmed
    while(!byETBFound)
    {
        // Check if the first ETB has been found 
        if(abyReceviedData[byIndex] == ARB_REGISTER_DATA_ETB)
        {
            byReadIndex = byIndex + 1;
            byETBFound = TRUE;
        }
        byIndex++;      
    }
    // Get the length of the Read
    byReadLength = ARB_FieldLength_Retrieve(byReadIndex, ARB_REGISTER_DATA_NEPTUNEDELIMITER, 6);

	// Check if the length was in bounds of the 34 bytes
	if(!byReadLength)
	{
		// Error
		Error_ARB_Data_Format();
		ARB_RegisterData_SetReading_ARBError_Error();
		ARB_RegisterData_SetIDToError_NoRead();
		return;
	}

    // Place the data into the structure
    ARB_ReceivedData_Retrieve(byReadIndex, &sRegisterData.abyReading[0], byReadLength);
    
    // Add the decimal reading to the structure
    ARB_RegisterData_ConvertToBinaryReading();

    // Reset to find the ID
    byETBFound = FALSE;
    // The set reading offset
    byIndex += byReadLength;
    
    // ID is located after the second ETB
    // This is not blocking as 5 ETB have already been confirmed
    while(!byETBFound)
    {
        // Check if the first ETB has been found 
        if(abyReceviedData[byIndex] == ARB_REGISTER_DATA_ETB)
        {
            byIDIndex = byIndex + 1;
            byETBFound = TRUE;
        }
        byIndex++;      
    }
    // Get the length of the ID
    byIDLength = ARB_FieldLength_Retrieve(byIDIndex, ARB_REGISTER_DATA_NEPTUNEDELIMITER, 10);

	// Check if the length was in the bounds of the 34 bytes
	if(!byIDLength)
	{
		// No ID
		ARB_RegisterData_SetIDToError_NoRead();
		return;
	}

    // Place the data into the structure
    // The ID must be right justified when less than 10 digits
    if(byIDLength > 10)
    {
        byIDLength = 10;
    }
	// ID Offset is 10 digits - actual ID length
	// This is used to determine where to place the ID so it is zero padded and right justified
    byIDOffset = 10 - byIDLength;

    ARB_ReceivedData_Retrieve(byIDIndex, (&sRegisterData.abyDetectedRegisterID[0]) + byIDOffset, byIDLength); 

    // Store the detected register ID length
    sRegisterData.byDetectedRegisterIDLength = byIDLength;
    
    // Add the decimal ID to the structure
    ARB_RegisterData_ConvertToBinaryID();
}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_RegisterDataParse_Plus
**    
**              Author: Nick Sinas
**             Created: 2/20/2013
**    
**  Function Arguments: none
**    Function Returns: none
**
**         Description: This function parses the raw abyReceviedData data
**                      and extracts the Reading and ID and saves them to the
**                      RegisterData structure.
**                      This function is called when Neptune PLUS format is
**                      detected.
**						
******************************************************************************/
static void ARB_RegisterDataParse_Plus(void)
{
    //static uint8_t byIndex;
    
    // Get reading
    // The 7th and 8th digits are located at index 27 and 28
    ARB_ReceivedData_Retrieve(7, &sRegisterData.abyReading[0], 6);
    ARB_ReceivedData_Retrieve(27, &sRegisterData.abyReading[6], 2);
    
    // Get ID
    // The ID is located at index 14
    ARB_ReceivedData_Retrieve(14, &sRegisterData.abyDetectedRegisterID[0], 10);
    
    // Store the detected register ID length (always 10 for Plus)
    sRegisterData.byDetectedRegisterIDLength = 10;
    
    // Get Leak Flags
    sRegisterData.byLeakStatusFlag = abyReceviedData[29];
    // Shift right once to match the bits used in the RF packet
    sRegisterData.byLeakStatusFlag >>= 1;
    
    // Get the spare bit 1
    sRegisterData.bySpare1 = abyReceviedData[25];
    sRegisterData.bySpare1 &= BIT5;
    sRegisterData.bySpare1 >>= 5;
    
    // Get the spare bit 2
    sRegisterData.bySpare2 = abyReceviedData[29];
    sRegisterData.bySpare2 &= BIT0;
    
    // Add the decimal conversions to the structure
    ARB_RegisterData_ConvertToBinaryReading(); 
    ARB_RegisterData_ConvertToBinaryID();
    
}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_RegisterDataParse_Sensus
**    
**              Author: Nick Sinas
**             Created: 2/20/2013
**    
**  Function Arguments: none
**    Function Returns: uint8_t TRUE or FALSE
**
**         Description: This function determines how the data should be parsed
**						once a sensus format is detected
**                      
**						
******************************************************************************/
static uint8_t ARB_RegisterDataParse_Sensus(void)
{
    // Check if Fixed Sensus        
    if(abyReceviedData[0] == ARB_REGISTER_DATA_SENSUS_FIXED)
    {
        byDetectedRegisterType = REGISTER_SENSUS_FIXED;
        ARB_RegisterDataParse_SensusFixed();
    }
    // Check if Variable Sensus
    else if(abyReceviedData[0] == ARB_REGISTER_DATA_SENSUS_VARIABLE)
    {
        byDetectedRegisterType = REGISTER_SENSUS_VAR;
        ARB_RegisterDataParse_SensusVariable();
    }
    else
    {
        // Unknown register error
        Error_ARB_Data_Unknown_Register();
		ARB_RegisterData_SetReading_ARBError_Error();
		ARB_RegisterData_SetIDToError_NoRead();
		byDetectedRegisterType = REGISTER_UNKNOWN;
        return FALSE;
    }
    
    return TRUE;
}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_RegisterDataParse_SensusFixed
**    
**              Author: Nick Sinas
**             Created: 2/20/2013
**    
**  Function Arguments: none
**    Function Returns: none
**
**         Description: This function parses the raw abyReceviedData data
**                      and extracts the Reading and ID and saves them to the
**                      RegisterData structure.
**                      This function is called when Sensus Fixed format is
**                      detected.
**                      Sensus Fixed Format:
**                      RnnnniiiiiiiiCR
**						
******************************************************************************/
static void ARB_RegisterDataParse_SensusFixed(void)
{
    // To get here the first byte had to be a "R"
    // The following 4 bytes are the reading
    // Grab the next 4 and place them in the structure
    ARB_ReceivedData_Retrieve(1, &sRegisterData.abyReading[0], 4);   
    
    // Add the 8 digit ID
	ARB_ReceivedData_Retrieve(5, &sRegisterData.abyDetectedRegisterID[0] + 2, 8);
    
    // Store the detected register ID length (always 8 for Sensus fixed)
    sRegisterData.byDetectedRegisterIDLength = 8;

	// Add decimal conversion of reading and ID to structure
    ARB_RegisterData_ConvertToBinaryReading();
	ARB_RegisterData_ConvertToBinaryID();
    
}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_RegisterDataParse_SensusVariable
**    
**              Author: Nick Sinas
**             Created: 2/20/2013
**    
**  Function Arguments: none
**    Function Returns: none
**
**         Description: This function parses the raw abyReceviedData data
**                      and extracts the Reading and ID and saves them to the
**                      RegisterData structure.
**                      This function is called when Sensus Variable format is
**                      detected.
**                      Typical Sensus Variable Format:
**                      V;RB00000000,+55;IB1111111111;K1111111111<CR>
**						
******************************************************************************/
static void ARB_RegisterDataParse_SensusVariable(void)
{
    static uint8_t byFieldFound;
    static uint8_t byIndex;
    static uint8_t byReadIndex;
    static uint8_t byIDIndex;
    static uint8_t byIDLength;
    static uint8_t byReadLength;
    static uint8_t byIDOffset;
    
    byFieldFound = FALSE;
    byIndex = 1;
    
    // To get here the first byte had to be a "V"
    // Scan until a "R" is found indicating reading field
    // Skip the next field which should be a "B"
    // Scan until 8 digits or a delimiter is found
    while((!byFieldFound) && (byIndex < byReceivedDataLength))
    {
        if(abyReceviedData[byIndex] == ARB_REGISTER_DATA_SENSUS_FIELD_R)
        {
            // Add 2 because the next digit is the field
            // Typically: V;RB000000; where the 0's are the reading
            byReadIndex = byIndex + 2;
            byFieldFound = TRUE;
        }
        byIndex++;
    }

    // Check if the field was found
    if(!byFieldFound)
    {
    	// Error
    	Error_ARB_Data_Format();
    	ARB_RegisterData_SetReading_ARBError_Error();
    	ARB_RegisterData_SetIDToError_NoRead();
    	return;
    }
    // At this point the Reading index is known, but the length is not
	// The max is set to 12 because the spec says 8 digits of mantissa, and 4 digits of fraction
    byReadLength = ARB_FieldLength_Retrieve(byReadIndex, ARB_REGISTER_DATA_SENSUSDELIMITER, 12);

	// Check if the length was in bounds of the 34 bytes
	if(!byReadLength)
	{
		// Error
		Error_ARB_Data_Format();
		ARB_RegisterData_SetReading_ARBError_Error();
		ARB_RegisterData_SetIDToError_NoRead();
		return;
	}
    
    // Check if reading length is greater than 8 digits
    if(byReadLength > 8)
    {
        // Set the length to 8
        // This will only place the upper 8 digits of the reading, so left justified
        byReadLength = 8;        
    }	
	
	// Place the reading in the  
    ARB_ReceivedData_Retrieve(byReadIndex, &sRegisterData.abyReading[0], byReadLength);
    byIndex += byReadLength;
	
	// Convert reading now in case the ID extraction fails
	ARB_RegisterData_ConvertToBinaryReading();
    
    byFieldFound = FALSE;
    
    // Get the reading
    // It is denoted by and "I"
    while((!byFieldFound) && (byIndex < byReceivedDataLength))
    {
        if(abyReceviedData[byIndex] == ARB_REGISTER_DATA_SENSUS_FIELD_I)
        {
            // Add 2 because the next digit is the field
            // Typically: V;RB000000;IB1111111111 where the 1's are the ID
            byIDIndex = byIndex + 2;
            byFieldFound = TRUE;
        }
        byIndex++;
    }

	// Check if ID was found
    if(!byFieldFound)
    {
        // No ID
        ARB_RegisterData_SetIDToError_NoRead();
        return;
    }

    // At this point the ID index is known, but the length is not
    // The Sensus ID length can be up to 12 digits
    byIDLength = ARB_FieldLength_Retrieve(byIDIndex, ARB_REGISTER_DATA_SENSUSDELIMITER, 12); 

	// Check if the length was in the bounds of the 34 bytes
	if(!byIDLength)
	{
		// No ID
		ARB_RegisterData_SetIDToError_NoRead();
		return;
	}

    // We only use the lower 10 bytes
    if(byIDLength > 10)
    {
        // This offsets the ID index to insure the "lower" 10 bytes of the ID are used
		// This could be a 11 or 12 digit ID, so this would add 1 or 2 to the ID index
		byIDIndex += (byIDLength - 10);
		byIDLength = 10;
    }
	
    // The ID must be right justified
    byIDOffset = 10 - byIDLength; 

	// Get the ID and place into ID field of the structure   
    ARB_ReceivedData_Retrieve(byIDIndex, (&sRegisterData.abyDetectedRegisterID[0]) + byIDOffset, byIDLength);
    
    // Store the detected register ID length
    sRegisterData.byDetectedRegisterIDLength = byIDLength;
    
    // Convert the ID to binary
    ARB_RegisterData_ConvertToBinaryID();    
    
}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_RegisterData_AddDecimalID
**                      ARB_RegisterData_AddDecimalReading
**    
**              Author: Nick Sinas
**             Created: 3/20/2013
**    
**  Function Arguments: none
**    Function Returns: none
**
**         Description: This function parses the 10 bytes of ID to a 34 bit
**                      binary number. Because the bytes can only be from 0-9
**                      certain limits can be checked to handle the changing or
**                      roll-over of the upper 2 bits (bit 33 & 34). 
**						
******************************************************************************/
// This function converts the ID to hex, then converts the ID to a decimal value
static uint8_t ARB_RegisterData_ConvertToBinaryID(void)
{
    static UU32 dwID;
    static uint8_t byIndex;
    static uint8_t byUpperBits;
    
    // Convert ID to hex
    for(byIndex = 0; byIndex < 10; byIndex++)
    {
        // Convert byte to hex
        MathFunc_AsciiToHex(&sRegisterData.abyDetectedRegisterID[byIndex]);
        
        // Check if converted byte is 0-9
        if(sRegisterData.abyDetectedRegisterID[byIndex] > 9)
        {
            // Non-numeric ID
	        ARB_RegisterData_SetIDToError_NonNumeric();
			return FALSE;
        }
    }
    
    // Convert lower 9 bytes of ID into a 32 bit number (this fits because all numbers are 0-9)
    app_BCDtoBIN(&sRegisterData.abyDetectedRegisterID[0], &dwID.U32, 9);
    
    
    // The register ID is 34 bits, determine what the upper 2 bits should be
    // This is determined by roll-over
    // There are 2 bounds to check:
    // The bound that sets the 33rd bit to a 1, which occurs when the 32 bit ID is greater than 
    // 429496729 and then multiplied by 10: 4294967300 -> 0001 0000 0000 ... 0100
    // There is a second condition where the 33rd bit can be set, and that is when the ID is equal to 429496729
    // and this is multiplied by 10, this does not immediately set the 33rd bit. Once 6 or more is added, the 33rd bit is set
    // The next bounds checked is if there is a roll-over to set the 34th bit.
    // This occurs when the ID is greater than 858993459 and multiplied by 10, the upper 2 bits go to '10'
    // 8589934600 -> 0010 0000 0000 ... 1000
    // There is a second condition where the 34'th bit can be set, and that is when the ID is equal to 858993459
    // this does not immediately set the 34th bit, but once a 2 or more is added the 34th bit is set.
    byUpperBits = 0;
    // First check the lower bounds of a roll-over to '01'
    if(dwID.U32 >= 429496729)
    {
        // Check the upper bounds bounds of a roll-over to '10'
        if(dwID.U32 >= 858993459)
        {
            // Check if on upper bound edge
            if(dwID.U32 == 858993459)
            {
                // Handle exact bounds condition
                if(sRegisterData.abyDetectedRegisterID[9] > 1)
                {
                    byUpperBits = 0x02; // '10'
                }
                else
                {
                    // No roll-over to '10'
                    byUpperBits = 0x01; // '01'
                }
            }
            else
            {
                // Roll-over the upper bound
                byUpperBits = 0x02; // '10'
            }
        }
        else // ID is matching lower bound roll-over to possibly a '01'
        {
            // Check if on lower bound edge
            if(dwID.U32 == 429496729)
            {
                // Handle exact bounds
                if(sRegisterData.abyDetectedRegisterID[9] > 5)
                {
                    // Roll-over to '01'
                    byUpperBits = 0x01; 
                }
                else
                {
                    // No roll-over
                    byUpperBits = 0; 
                }                
            }
            else
            {
                // Roll-over past lower bounds to '01'
                byUpperBits = 0x01; 
            }
        }
    }
    
    // Get last digit
    dwID.U32 *= 10;
    dwID.U32 += sRegisterData.abyDetectedRegisterID[9];
    
    // Add 34 bit decimal ID to structure
    sRegisterData.abyDetectedRegisterIDDecimal[0] = byUpperBits;
    sRegisterData.abyDetectedRegisterIDDecimal[1] = dwID.U8[0];
    sRegisterData.abyDetectedRegisterIDDecimal[2] = dwID.U8[1];
    sRegisterData.abyDetectedRegisterIDDecimal[3] = dwID.U8[2];
    sRegisterData.abyDetectedRegisterIDDecimal[4] = dwID.U8[3];
        
    return TRUE;
}

// This function converts the reading to hex, then converts the reading to a decimal value
static uint8_t ARB_RegisterData_ConvertToBinaryReading(void)
{
    static uint8_t byIndex;
    
    // Convert Reading to hex
    for(byIndex = 0; byIndex < 8; byIndex++)
    {
        // Convert byte to hex
        MathFunc_AsciiToHex(&sRegisterData.abyReading[byIndex]);
        
        // Check if converted byte is 0-9
        if(sRegisterData.abyReading[byIndex] > 9)
        {
            // Non-numeric reading, set to error
            ARB_RegisterData_SetReading_UnsupportedReading_Error();
            Error_ARB_Data_Format();
			return FALSE;
        }
    }
    
    // Convert reading to binary
    app_BCDtoBIN(&sRegisterData.abyReading[0], &sRegisterData.dwReadingBinary.U32, 8);
    
    return TRUE;
}

// This function sets the reading to the Unsupported Reading error code
static void ARB_RegisterData_SetReading_UnsupportedReading_Error(void)
{
    // Set reading array to error 
	sRegisterData.abyReading[0] = 0xFF;
	sRegisterData.abyReading[1] = 0xFF;
	sRegisterData.abyReading[2] = 0xFF;
	sRegisterData.abyReading[3] = 0xFF;
	sRegisterData.abyReading[4] = 0xFF;
	sRegisterData.abyReading[5] = 0xFF;
	sRegisterData.abyReading[6] = 0xFF;
	sRegisterData.abyReading[7] = 0xFF;

	// Set binary reading to error
	sRegisterData.dwReadingBinary.U32 = READING_ERROR_DATAFORMAT;
}

// This function sets the reading to the general ARB Error error code
static void ARB_RegisterData_SetReading_ARBError_Error(void)
{
    // Set reading array to error 
	sRegisterData.abyReading[0] = 0xFF;
	sRegisterData.abyReading[1] = 0xFF;
	sRegisterData.abyReading[2] = 0xFF;
	sRegisterData.abyReading[3] = 0xFF;
	sRegisterData.abyReading[4] = 0xFF;
	sRegisterData.abyReading[5] = 0xFF;
	sRegisterData.abyReading[6] = 0xFF;
	sRegisterData.abyReading[7] = 0xFF;

	// Set binary reading to error
	sRegisterData.dwReadingBinary.U32 = READING_ERROR_GENERAL;
}


// This function sets the ID to the error code
static void ARB_RegisterData_SetIDToError_NonNumeric(void)
{
	// Fill ID array with FF
	sRegisterData.abyDetectedRegisterID[0] = 0xFF;
	sRegisterData.abyDetectedRegisterID[1] = 0xFF;
	sRegisterData.abyDetectedRegisterID[2] = 0xFF;
	sRegisterData.abyDetectedRegisterID[3] = 0xFF;
	sRegisterData.abyDetectedRegisterID[4] = 0xFF;
	sRegisterData.abyDetectedRegisterID[5] = 0xFF;
	sRegisterData.abyDetectedRegisterID[6] = 0xFF;
	sRegisterData.abyDetectedRegisterID[7] = 0xFF;
	sRegisterData.abyDetectedRegisterID[8] = 0xFF;
	sRegisterData.abyDetectedRegisterID[9] = 0xFF;
	
	// Fill Binary ID with 0x030000000F
	sRegisterData.abyDetectedRegisterIDDecimal[0] = 0x03;
	sRegisterData.abyDetectedRegisterIDDecimal[1] = 0x00;
	sRegisterData.abyDetectedRegisterIDDecimal[2] = 0x00;
	sRegisterData.abyDetectedRegisterIDDecimal[3] = 0x00;
	sRegisterData.abyDetectedRegisterIDDecimal[4] = 0x0F;
}



// This function sets the ID to the error code for a no read condition
static void ARB_RegisterData_SetIDToError_NoRead(void)
{
	// Fill ID array with FF
	sRegisterData.abyDetectedRegisterID[0] = 0xFF;
	sRegisterData.abyDetectedRegisterID[1] = 0xFF;
	sRegisterData.abyDetectedRegisterID[2] = 0xFF;
	sRegisterData.abyDetectedRegisterID[3] = 0xFF;
	sRegisterData.abyDetectedRegisterID[4] = 0xFF;
	sRegisterData.abyDetectedRegisterID[5] = 0xFF;
	sRegisterData.abyDetectedRegisterID[6] = 0xFF;
	sRegisterData.abyDetectedRegisterID[7] = 0xFF;
	sRegisterData.abyDetectedRegisterID[8] = 0xFF;
	sRegisterData.abyDetectedRegisterID[9] = 0xFF;
	
	// Fill Binary ID with 0x030000000F
	sRegisterData.abyDetectedRegisterIDDecimal[0] = 0x03;
	sRegisterData.abyDetectedRegisterIDDecimal[1] = 0x00;
	sRegisterData.abyDetectedRegisterIDDecimal[2] = 0x00;
	sRegisterData.abyDetectedRegisterIDDecimal[3] = 0x00;
	sRegisterData.abyDetectedRegisterIDDecimal[4] = 0x00;
    
    
}



// This function sets the ID to the error code for a no read condition
static void ARB_RegisterData_SetIDToError_IDNotSupported(void)
{
	// Fill ID array with FF
	sRegisterData.abyDetectedRegisterID[0] = 0xFF;
	sRegisterData.abyDetectedRegisterID[1] = 0xFF;
	sRegisterData.abyDetectedRegisterID[2] = 0xFF;
	sRegisterData.abyDetectedRegisterID[3] = 0xFF;
	sRegisterData.abyDetectedRegisterID[4] = 0xFF;
	sRegisterData.abyDetectedRegisterID[5] = 0xFF;
	sRegisterData.abyDetectedRegisterID[6] = 0xFF;
	sRegisterData.abyDetectedRegisterID[7] = 0xFF;
	sRegisterData.abyDetectedRegisterID[8] = 0xFF;
	sRegisterData.abyDetectedRegisterID[9] = 0xFF;
	
	// Fill Binary ID with 0x030000000F
	sRegisterData.abyDetectedRegisterIDDecimal[0] = 0x03;
	sRegisterData.abyDetectedRegisterIDDecimal[1] = 0x00;
	sRegisterData.abyDetectedRegisterIDDecimal[2] = 0x00;
	sRegisterData.abyDetectedRegisterIDDecimal[3] = 0x00;
	sRegisterData.abyDetectedRegisterIDDecimal[4] = 0x10;
}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_FieldLength_Retrieve
**    
**              Author: Nick Sinas
**             Created: 2/20/2013
**    
**  Function Arguments: uint8_t byReceivedDataIndex, uint8_t *pbyRegisterData, uint8_t byCount
**    Function Returns: none
**
**         Description: This function copies the data from the abyReceviedData array
**                      to a pointer location, this is typically a pointer into the
**                      RegisterData structure. This is used to move the ID and the Reading 
**						
******************************************************************************/
static void ARB_ReceivedData_Retrieve(uint8_t byReceivedDataIndex, uint8_t *pbyRegisterData, uint8_t byCount)
{
    static uint8_t byIndex;
    
    for(byIndex = 0; byIndex < byCount; byIndex++)
    {
        *pbyRegisterData = abyReceviedData[byReceivedDataIndex];
        pbyRegisterData++;
        byReceivedDataIndex++;
    }
}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_FieldLength_Retrieve
**    
**              Author: Nick Sinas
**             Created: 2/20/2013
**    
**  Function Arguments: uint8_t byReceivedDataIndex, uint8_t byDelimiterType, uint8_t byMaxLength
**    Function Returns: uint8_t - length
**
**         Description: This function takes an index into the abyReceviedData array
**                      and a delimiter type (Neptune or Sensus) and a max length
**                      and this function returns the length of the ID or Reading until
**                      a delimiter is found 
**						
******************************************************************************/
static uint8_t ARB_FieldLength_Retrieve(uint8_t byReceivedDataIndex, uint8_t byDelimiterType, uint8_t byMaxLength)
{
    static uint8_t byCount;    
    static uint8_t byDelimiterFound;
    
    byCount = 0;
    byDelimiterFound = FALSE;
    
    // Delimiter has not been found and length is not greater than the passed length
    while((!byDelimiterFound) && (byCount <= byMaxLength))
    {
        // Check if the index is still in bounds
		if(byReceivedDataIndex > byReceivedDataLength)
		{
			// This is an invalid length, indicating an error
			return 0;
		}
        // Check for a Neptune delimiter
        if(byDelimiterType == ARB_REGISTER_DATA_NEPTUNEDELIMITER)
        {
            if(abyReceviedData[byReceivedDataIndex] == ARB_REGISTER_DATA_ETB)
            {
                // Done
                return byCount;
            }
            else
            {
                byCount++;
            }
        }
        // There are multiple delimiters
        else // byDelimiterType = ARB_REGISTER_DATA_SENSUSDELIMITER
        {
            if((abyReceviedData[byReceivedDataIndex] == ARB_REGISTER_DATA_SENSUS_SEMICOLON) ||
                (abyReceviedData[byReceivedDataIndex] == ARB_REGISTER_DATA_SENSUS_COMMA) ||
                (abyReceviedData[byReceivedDataIndex] == ARB_REGISTER_DATA_SENSUS_PERIOD) ||
                (abyReceviedData[byReceivedDataIndex] == ARB_REGISTER_DATA_SENSUS_CR) )
            {
                // Done
                return byCount;
            }
            else
            {
                byCount++;
            }
        }
        
        // Increment index
        byReceivedDataIndex++;
        
    }
    
    // If we get here the length was greater than the max legnth
    return 0;    

}


/******************************************************************************
**
**            Filename: ARB.c
**        Routine Name: ARB_RegisterData_Clear
**    
**              Author: Nick Sinas
**             Created: 2/20/2013
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: This function clears out the RegisterData structure 
**                      to all 0's.
**                      THIS IS REQUIRED SO RIGHT/LEFT JUSTIFICATION AND ZERO 
**                      PADDING WORKS
**						
******************************************************************************/
static void ARB_RegisterData_Clear(void)
{
    static uint8_t byIndex;
    
    // Clear Register Data structure
    RegisterData_ClearReadingID();     
    
    // Clear Reading decimal
    sRegisterData.dwReadingBinary.U32 = 0x00;    
    
    // Clear ID decimal
    for(byIndex = 0; byIndex < sizeof(sRegisterData.abyDetectedRegisterIDDecimal); byIndex++)
    {
        sRegisterData.abyDetectedRegisterIDDecimal[byIndex] = 0x00;
    }
    
    // Clear  Leak Status Flag
    for(byIndex = 0; byIndex < sizeof(sRegisterData.byLeakStatusFlag); byIndex++)
    {
        sRegisterData.byLeakStatusFlag = 0x00;
    }
    
    sRegisterData.bySpare1 = 0;
    sRegisterData.bySpare2 = 0;
    sRegisterData.byLeakStatusFlag = 0;
}

static void RegisterData_ClearReadingID(void)
{
	static uint8_t byIndex;
    
    // Clear Reading array
    for(byIndex = 0; byIndex < sizeof(sRegisterData.abyReading); byIndex++)
    {
        sRegisterData.abyReading[byIndex] = 0x00;
    }

	// Clear ID array
    for(byIndex = 0; byIndex < sizeof(sRegisterData.abyDetectedRegisterID); byIndex++)
    {
        sRegisterData.abyDetectedRegisterID[byIndex] = 0x00;
    }
}

/******************************************************************************
*******************************************************************************
**
**            Filename: ARB.c 
**       Function Name: ARB_State_ARBV_Read_Pass1
**                      ARB_State_ARBV_Read_Pass2
**                      ARB_State_Read_ARBV
**						ARB_State_ARBV_WaitForData
**						ARB_State_ARBV_ReadData
**						ARB_State_ARBV_ProcessData
**    
**              Author: Nick Sinas
**             Created: 03/05/2013
**    
**  Function Arguments: None
**    Function Returns: None
**
**         Description: Reads an ARBV   register 
**						
**
******************************************************************************/
// This state reads the ARBV meter for Pass 1 at 50% duty cycle
void ARB_State_ARBV_Read_Pass1(void)
{
    // Reset variables
    ARB_Variables_Clear();
    
	// ARBV specific variables
	byBitMask = BIT7;
	byARBVPassNumber = 1;
    byARBVDelay = 0;
	byStartConditionFound = 0;
    
    // Enable 5V Supply and Data Bias
    app_gpio_arbMode_enable();
    
    // Setup Timer 0 for 3200 Hz (156us interrupt intervals)
    app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_3200HZ);   
    
    // Set state to ARBV Clock
    pfnARB_State = ARB_State_ARBV_WaitForData;    
}

// This state reads the ARBV meter for Pass 2 at 66% duty cycle
void ARB_State_ARBV_Read_Pass2(void)
{
    // Reset variables
    ARB_Variables_Clear();
    
	// ARBV specific variables
	byBitMask = BIT7;
    byARBVPassNumber = 2;
    byARBVDelay = 0;
	byStartConditionFound = 0;
    
    // Enable 5V Supply and Data Bias
    app_gpio_arbMode_enable();	
    
    // Setup Timer 0 for 3200 Hz (156us interrupt intervals)
    app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_3200HZ);   
    
    // Set state to ARBV Clock
    pfnARB_State = ARB_State_ARBV_WaitForData;    
}

// This function toggles the CLK line based on the ARBV Pass #
uint8_t ARB_ARBV_ToggleClockGetData(void)
{
	static uint8_t byCount;
    
    // This is the max time-out 
    // If data never starts, the time-out will be 5ms
    // If data starts erroneously, and no start condition is
    // ever found, this time-out will make sure clocking
    // will stop after 3.5s
    wClockingTimeout++;
    
    
	// Toggle CLK line            
    if(GPIO_PinInGet(ARB_CLOCK_PORT,ARB_CLOCK_PIN))
    {
        GPIO_PinOutClear(ARB_CLOCK_PORT,ARB_CLOCK_PIN);
        // 2nd Pass duty cycle should be 66%
        if(byARBVPassNumber == 2)
        {
            // Setup Timer 0 for 3200 Hz (156us interrupt intervals)
            app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_3200HZ);			
        }
        
        // Update time tracking
        w156usTicks++;
    }
    else
    {
        GPIO_PinOutSet(ARB_CLOCK_PORT,ARB_CLOCK_PIN);
        // 2nd Pass duty cycle should be 66%
        if(byARBVPassNumber == 2)
        {
            // Setup Timer 0 for 1600 Hz (312us interrupt intervals)
            app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_1600HZ);
            
			// Update time tracking
			w156usTicks += 2;
        }
		else
		{
			// Update time tracking
			w156usTicks++;	
		}
    }

	// Wait for Data line to move once CLK line has been set
    // This give about a 30us delay, this is 3x the spec
    for(byCount = 0; byCount < 120; byCount++)
    {
        __NOP();
    }    
    
    // Get data line snap shot     
    if(GPIO_PinInGet(ARB_DATA_PORT,ARB_DATA_PIN))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

// This state clocks the register until the a data response
void ARB_State_ARBV_WaitForData()
{	
	static uint8_t byDataLine; 	
	
	// Toggle the CLK line
	byDataLine = ARB_ARBV_ToggleClockGetData();    
	
	// Allow the Data Line to settle, check once 10 transitions have been sent 
    if(byARBVDelay < 10)
    {
        byARBVDelay++;
        return;
    }

	// Check for time-out
    if(!byARBDataStarted)
    {
        if(byDataLine == 0)
        {
            byARBDataStarted = TRUE;
			// Set state to ARBV Clock
    		pfnARB_State = ARB_State_ARBV_ReadData;
			return;
        }
        else // Line is still high, continue clocking, increment the time-out counter
        {
            byARBVDelay++;
            if(byARBVDelay < 32)
            {
                return;
            }
            else
            {
                // If data has not started after 5ms, time-out
                // 32 cycles = 5ms (1 cycle @ 3.2kHz = 156us, 5ms/156us = 32)
				Error_ARB_Data_Timeout();
                ARB_RegisterData_SetReading_ARBError_Error();
				ARB_RegisterData_SetIDToError_NoRead();
                ARB_ARBV_Done();                
                return;
            }
        }
    }

	// Sanity
	// Set state to ARBV Clock
    pfnARB_State = ARB_State_ARBV_ReadData;
}

// This state clocks the ARBV meter and stores the data 
// A start condition must be found before it starts storing data
// Data must start within 5ms of clocking
// Data must send its complete message within 3.5s of data starting
void ARB_State_ARBV_ReadData(void)
{
    static uint8_t byDataLine;   
                
    // Toggle the CLK line and get Data line
	byDataLine = ARB_ARBV_ToggleClockGetData();               
    
    // Shift the data line into the data buffer based on the mask
    if(byDataLine)
    {        
        byReceviedData |= byBitMask;
    }  
    
    // Shift the mask
    byBitMask >>= 1; 
    
    // Increment bit count
    byBitCounter++;    
    
    // Check if a byte has been received
    if(byBitCounter == 8)
    {        
        // Check if start condition has been found 
        // This sets the byte boundary
        if(!byStartConditionFound)
        {
            // Check if a Start 1 or Start 0 has been detected
            if((byReceviedData == ARBV_START_1) || (byReceviedData == ARBV_START_0))
            {
                // Start Condition found
                byStartConditionFound = 1;
            }
            else
            {
                // Start Condition not found
                // Shift in one more bit
                byReceviedData <<= 1;
                byBitCounter = 7;
                byBitMask = BIT0;
            }            
        }
        
        // Byte boundary known, save this to the buffer
        if(byStartConditionFound) 
        {        
            //P0 |= (BIT6);
            // Save byte to buffer and reset data variables
            abyReceviedARBVData[byReceviedDataIndex] = byReceviedData;
            byReceviedDataIndex++;
            byBitCounter = 0;
            byReceviedData = 0;
            byBitMask = BIT7;
        }
    }
        
    // Read 64 "bits" which will be 16 characters
    if(byReceviedDataIndex > 64)
    {        
        // Disable ARB lines for power savings
        app_gpio_arbMode_disable();
        
        // Process the data
        ARB_ARBV_ProcessData();
        
        // Call state handler
        ARB_ARBV_SetState();
        
    }    
    
}

// This function sets the state to next appropriate state and updates the RTC
void ARB_ARBV_SetState(void)
{
    if(byARBVPassNumber == 1)
	{
        pfnARB_State = ARB_State_ARBV_Read_Pass2;
            
        // Setup Timer 0 for 3200 Hz (156us interrupt intervals)
        app_timers_timer0_setupIntInterval(TIMER0_INT_INTVERAL_3200HZ);
    }
    else 
    {
        ARB_ARBV_Done();
    }
}

// This function processes the received ARBV data and places them in buffers, comparing if 2nd pass
// Typical ARBV packet (decoded): FEEEEE111111EEEE, where 1's are the 6 digit reading
void ARB_ARBV_ProcessData(void)
{
    static uint8_t byDecodeIndex;
    
    // Set the interval so that we do not pass the wakeup during calculations
    ////RTC_EnableWakeInterval(WAKE_INTERVAL_TICKS);

	// ARBV does not support an ID
	ARB_RegisterData_SetIDToError_IDNotSupported();
    
	// Build and order readings
	if(!ARB_ARBV_BuildReading())
	{
		// Error in reading		
        ARB_ARBV_Done();
        return;
	}   
    
    // Check if this is the second pass and the data needs to be compared
    if(byARBVPassNumber == 2)
    {		
		// Compare Pass 1 and Pass 2
        for(byDecodeIndex = 0; byDecodeIndex < 16; byDecodeIndex++)
        {
            if(byDecodedARBVDataPass1[byDecodeIndex] != byDecodedARBVDataPass2[byDecodeIndex])
            {
                // Error in the data
                Error_ARB_Data_Format();
			    ARB_ARBV_Done();
		        return;
            }
        }
        
		// Readings matched         

        // The copy reading to structure
        if(ARB_ARBVReading_Parse())
        {
            // No errors in the reading so convert to decimal
            // Add decimal conversion of reading to structure
            ARB_RegisterData_ConvertToBinaryReading();

			// Set the detected register type
        	byDetectedRegisterType = REGISTER_ARBV;
        }
		
        // If the parsing of the reading failed, the conversion to
        // decimal was already handled, as it was filled with all 1's
   
        // Done
        ARB_ARBV_Done();
    }
}

// The ARBV data must be "built" from 4 bit pattern that signify a binary 1 or 0
// There are 2 patterns for a binary 1 and 2 patterns for a binary 0.
// This function loops through the received data and matches the bit patterns to build
// bytes.
static uint8_t ARB_ARBV_BuildReading()
{
	static uint8_t byReceivedIndex;
    static uint8_t byCount;
    static uint8_t byDecodeIndex;
    static uint8_t byDecodedByte;
    static uint8_t byFCharIndex;

    // Loop through data and build characters
    byDecodeIndex = 0;
    byReceivedIndex = 0;
    while(byReceivedIndex < 64)
    {
        // 4 "bits" per binary bit
        byDecodedByte = 0;
        byBitMask = BIT0;
        for(byCount = 0; byCount < 4; byCount++)
        {
            // Check for bit '1' condition
            if((abyReceviedARBVData[byReceivedIndex] == ARBV_BIT_1) ||(abyReceviedARBVData[byReceivedIndex] == ARBV_START_1))
            {
                byDecodedByte |= byBitMask;
                byBitMask <<= 1;
            }
            // Check for bit '0' condition
            else if((abyReceviedARBVData[byReceivedIndex] == ARBV_BIT_0) ||(abyReceviedARBVData[byReceivedIndex] == ARBV_START_0))
            {   
                byBitMask <<= 1;
            }
            else // Bit pattern did not match
            {
                // ERROR 
                Error_ARB_Data_Format();
				ARB_RegisterData_SetReading_ARBError_Error();
                return FALSE;
            }
            
            byReceivedIndex++;
        }
        // Check if byte was an F Char
        if(byDecodedByte == ARBV_FCHAR)
        {
            byFCharIndex = byDecodeIndex;
        }
        // Save "byte"
        byDecodedARBVDataTemp[byDecodeIndex++] = byDecodedByte; 
              
    }

	// Now order the built readings
    ARB_ARBV_OrderReadings(byFCharIndex);

    return TRUE;

}

// The ARBV data output is a circular buffer of data
// This function finds the 'F Character' in the reading, then 
// reorders the reading to put all characters in their default 
// indexes, this allows for the referencing of the reading more easily
static void ARB_ARBV_OrderReadings(uint8_t byFCharIndex)
{
	static uint8_t byReceivedIndex;
	static uint8_t byDecodeIndex;
	static uint8_t byCount;

	// Copy to Pass# array ordered correctly 
    // The data can be recevied with the F character at any index, this finds it and orders it
    // This is possible because the data wraps around.
    // Copy at the F char through the end
    byReceivedIndex = byFCharIndex;
    for(byDecodeIndex = 0; byDecodeIndex < (16 - byFCharIndex); byDecodeIndex++)
    {
        if(byARBVPassNumber == 1)
        {
            byDecodedARBVDataPass1[byDecodeIndex] = byDecodedARBVDataTemp[byReceivedIndex++];
        }
        else
        {
            byDecodedARBVDataPass2[byDecodeIndex] = byDecodedARBVDataTemp[byReceivedIndex++];
        }
        
    }
    // Copy the rest
    for(byCount = 0; byCount < byFCharIndex; byCount++)
    {
        if(byARBVPassNumber == 1)
        {
            byDecodedARBVDataPass1[byDecodeIndex++] = byDecodedARBVDataTemp[byCount];
        }
        else
        {
            byDecodedARBVDataPass2[byDecodeIndex++] = byDecodedARBVDataTemp[byCount];
        }
    }
}


// This function parses the ARBV reading, checking for errors
static uint8_t ARB_ARBVReading_Parse()
{
    static uint8_t byIndex;
    static uint8_t byGoodReading;
    static uint8_t by4DigitRead;
    
    byGoodReading = TRUE;
    by4DigitRead = FALSE;
    
    // Check if a 4 digit reading
    // Check if digits 5 and 6 are both E's which indicates a 4 digit ARBV, so pad with 0's
    if((byDecodedARBVDataPass1[5] == 0x0E) && (byDecodedARBVDataPass1[6] == 0x0E))
    {
        sRegisterData.abyReading[4] = 0;
        sRegisterData.abyReading[5] = 0;
        byGoodReading = TRUE;
        by4DigitRead = TRUE;
    }
    else // 6 digit read
    {
        // Check for all bad conditions
        // B in 5 or 6
        // E in 5
        if((byDecodedARBVDataPass1[5] == 0x0B) || (byDecodedARBVDataPass1[6] == 0x0B) || byDecodedARBVDataPass1[5] == 0x0E)
        {
            ARB_RegisterData_SetReading_UnsupportedReading_Error();        
            return FALSE;
        }

        // Check digit 6 for 5 or E
        if(byDecodedARBVDataPass1[6] == 0x0E)
        {
            // This should be forced to 0
            byDecodedARBVDataPass1[6] = 0;
        }
        else if(byDecodedARBVDataPass1[6] != 0x05)
        {
            byGoodReading = FALSE;
        }

        // Check digit 5 to make sure its numeric (0-9)
        if(byDecodedARBVDataPass1[5] > 9)
        {
            // Error
            byGoodReading = FALSE;
        }
    }    
    

    
    // Check for anything non-numeric (0-9)
    for(byIndex = 7; byIndex < 11; byIndex++)
    {
        if(byDecodedARBVDataPass1[byIndex] > 9)
        {
            // Error
            byGoodReading = FALSE;
            break;
        }
    }  
    
    // Check if the reading is good, assign to structure
    if(byGoodReading)
    {
        // Get the reading
        sRegisterData.abyReading[0] = byDecodedARBVDataPass1[7];
        sRegisterData.abyReading[1] = byDecodedARBVDataPass1[8];
        sRegisterData.abyReading[2] = byDecodedARBVDataPass1[9];
        sRegisterData.abyReading[3] = byDecodedARBVDataPass1[10];
        // If this is not a 4 digit read then get the actual digits, else they have already been set to 0
        if(!by4DigitRead)
        {
            sRegisterData.abyReading[4] = byDecodedARBVDataPass1[5];
            sRegisterData.abyReading[5] = byDecodedARBVDataPass1[6];
        }
    }
    else // Reading contains errors, so fill with all 1's to indicate an error
    {
        ARB_RegisterData_SetReading_UnsupportedReading_Error();
        Error_ARB_Data_Format();
    }
    
    return byGoodReading;
    
}


static void ARB_ARBV_Done(void)
{   
    // Ensure that 5V Supply and Data Bias are disabled
	app_gpio_arbMode_disable();
	
    // Exit
	byARBReadDone = TRUE;
	
}
