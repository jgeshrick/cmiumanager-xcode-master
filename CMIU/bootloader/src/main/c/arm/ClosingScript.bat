:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::        Filename: ClosingScript.bat
::          Author: Brian Arnberg
::            Date: 2015.06.25
::
::         Purpose: This script handles merging the hex file and outputing a
::                  binary file.
::
::     Description: This script sets up the arguments for and calls the File
::                  Combiner program. This decouples the command line from
::                  uVision, which makes build automation easier.
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
@SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
@ECHO OFF
@SET TARGET_NAME=%1

:: We expect the target name to be non null.
:: We also expect the target name to not include filename extensions.
if [%TARGET_NAME%]==[] GOTO ERROR

@SET KEIL_OUTPUT_HEX=..\..\..\..\target\obj\CMIU_BL.hex
@SET KEIL_OUTPUT_BIN=..\..\..\..\target\obj\CMIU_BL.bin
@SET APPLICATION_OUTPUT_BIN=..\..\..\..\target\obj\CmiuApplicationImage.bin
@SET CONFIGURATION_OUTPUT_BIN=..\..\..\..\target\obj\CmiuConfigurationImage.bin
@SET ENCRYPTION_OUTPUT_BIN=..\..\..\..\target\obj\CmiuEncryptionImage.bin
@SET BLE_CONFIG_OUTPUT_BIN=..\..\..\..\target\obj\CmiuBleConfigImage.bin

@SET FILE_COMBINER=FileCombiner.exe
@SET FILE_TYPE=CMIU
@SET BOOTLOADER_HEX=%KEIL_OUTPUT_HEX%
@SET APPLICATION_HEX=".\BoardTestWithOffset.hex"
@SET KEYTABLE_HEX=".\DevelopmentKeys.hex"
@SET OUTPUT_HEX="..\..\..\..\target\obj\%TARGET_NAME%.hex"
@SET OUTPUT_BIN="..\..\..\..\target\obj\%TARGET_NAME%.bin"
@SET ARGUMENTS=%FILE_TYPE% %APPLICATION_HEX% %BOOTLOADER_HEX% %OUTPUT_HEX% %KEYTABLE_HEX%


:RUN_FILE_COMBINER
%FILE_COMBINER% %ARGUMENTS%
@SET RETURN_ERROR=%ERRORLEVEL%
@IF NOT %RETURN_ERROR%==0 GOTO ERROR

:COPY_RESULT
:: This is for debugging with uVision.
copy %OUTPUT_HEX% %KEIL_OUTPUT_HEX%

ENDLOCAL
EXIT /B

:ERROR
ECHO %FILE_COMBINER%(1): error: #1: FileCombiner failed for some reason.
ENDLOCAL
EXIT /B %RETURN_ERROR%

