#!/usr/bin/python
################################################################################
################################################################################
#       Filename: Hex2CSourceArray.py
#
#         Author: Brian Arnberg
#        Created: 2015.01.30
#
#   Last Edit By: Brian Arnberg (2015.06.18)
#      Last Edit: Updated the ARM code for the CMIU project.
#
#       Comments: This script is used to convert all or part of a hex file to
#                 an array of hex values in a C Source File. This is useful
#                 for stripping the bootloader out of a munged processor file
#                 and including it in another file as a compiled in array.
#        Purpose: This can be used to help generate a bootloader-replacing
#                 application. Ex. I need to update the bootloader on a potted
#                 unit for some reason. This makes generating the bootloader
#                 updating application much easier.
#
# vision History:
#     2015.01.29: Programming started (Brian Arnberg)
#     2015.06.18: Updated for CMIU (Brian Arnberg)
#
#     Copyright 2015 as unpublished work.
#     All rights reserved
#
#     The information contained herein is confidential
#     property of Neptune Technology Group. The use, copying, transfer
#     or disclosure of such information is prohibited except by express
#     written agreement with Neptune Technology Group.
################################################################################
################################################################################

"""Show content of hex file as hexdump."""

VERSION = '0.1'


USAGE = '''Hex2CSourceArray: convert the contents of a hex file to a C Source Array.
Usage:
    python Hex2CSourceArray.py [options] HEXFILE

Options:
    -h, --help              this help message.
    -v, --version           version info.
    -a, --ARM               Produce an ARM file (default is 8051)
    -r, --range=START:END   specify address range for dumping
                            (ascii hex value).
                            Range can be in form 'START:' or ':END'.

Arguments:
    HEXFILE     name of hex file for processing (use '-' to read
                from stdin)
  Credits: The contents of this script were adapted from 
    the example script hex2dump.py from the IntelHex project.
'''

DISCLOSURE = '''/******************************************************************************
*******************************************************************************
**
**      Filename:   FILENAME
**
**        Author:   Hex2CSourceArray.py
**       Created:   DATE
**
**    This file was generated by Hex2CSourceArray.py on the above mentioned
**    Date. The purpose of the file is to provide an array that represent
**    all or part of a specific hex file. This makes including the contents
**    of that hex file in the project easier.
**
**    Copyright YEAR as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/
'''

INCLUDES = '''
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
/*-- Required --*/
#include <Typedefs.h>
#include <compiler_defs.h>
#include <C8051F960_defs.h>

/*-- Module Specific --*/
#include <HFILENAME>

'''

INCLUDES_ARM = '''
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
/*-- Required --*/
#include <stdint.h>

/*-- Module Specific --*/
#include <HFILENAME>

'''

import sys
import StringIO



def string2Files(cSourceArrayString, arrayLength, fileName):
    from datetime import date
    today = date.today()
    today = today.strftime('%Y.%m.%d')
    thisYear = date.today().strftime('%Y')

    cFileName = fileName+'.c'
    hFileName = fileName+'.h'

    legalMatter = DISCLOSURE
    legalMatter = legalMatter.replace('DATE',today)
    legalMatter = legalMatter.replace('YEAR',thisYear)

    headerName = 'HEADER_'+fileName.upper()+'_H\n'
    headerTopMatter = '#ifndef '+headerName+'#define '+headerName

    hSourceString = legalMatter
    hSourceString = hSourceString.replace('FILENAME',hFileName)
    hSourceString += '\n\n'+headerTopMatter+'\n\n'

    hSourceString += '#define '+fileName.upper()+'_LENGTH '+str(arrayLength)+'UL\n'
    hSourceString += 'extern code uint8_t '+fileName+'['+str(arrayLength)+'];\n\n#endif\n\n'

    cSourceArrayString = legalMatter+INCLUDES+cSourceArrayString
    cSourceArrayString = cSourceArrayString.replace('FILENAME',cFileName,1)
    cSourceArrayString = cSourceArrayString.replace('HFILENAME',hFileName)


    # Turn the C Source Strings into compilable files
    cFile = open(cFileName,'w')
    hFile = open(hFileName,'w')
    cFile.write(cSourceArrayString)
    hFile.write(hSourceString)
    cFile.close()
    hFile.close()
    print("Files written!")
    return 0

def string2FilesARM(cSourceArrayString, arrayStart, arrayLength, fileName):
    from datetime import date
    today = date.today()
    today = today.strftime('%Y.%m.%d')
    thisYear = date.today().strftime('%Y')

    cFileName = fileName+'.c'
    hFileName = fileName+'.h'

    legalMatter = DISCLOSURE
    legalMatter = legalMatter.replace('DATE',today)
    legalMatter = legalMatter.replace('YEAR',thisYear)

    headerName = 'HEADER_'+fileName.upper()+'_H\n'
    headerTopMatter = '#ifndef '+headerName+'#define '+headerName

    hSourceString = legalMatter
    hSourceString = hSourceString.replace('FILENAME',hFileName)
    hSourceString += '\n\n'+headerTopMatter+'\n\n'

    hSourceString += '#define '+fileName.upper()+'_ATTRIBUTE __attribute((section("'+fileName+'"))) __attribute__((used))\n'
    hSourceString += '#define '+fileName.upper()+'_LENGTH '+str(arrayLength)+'UL\n'
    hSourceString += '#define '+fileName.upper()+'_ADDRESS '+str(arrayStart)+'UL\n'
    hSourceString += '#define '+fileName.upper()+'_END (('+fileName.upper()+'_ADDRESS)+('+fileName.upper()+'_LENGTH))\n'
    hSourceString += '#define '+fileName.upper()+'_CRC (('+fileName.upper()+'_END)-(4UL))\n'
    hSourceString += 'extern uint8_t '+fileName+'['+fileName.upper()+'_LENGTH];\n\n#endif\n\n'

    cSourceArrayString = legalMatter+INCLUDES_ARM+cSourceArrayString
    cSourceArrayString = cSourceArrayString.replace('FILENAME',cFileName,1)
    cSourceArrayString = cSourceArrayString.replace('HFILENAME',hFileName)


    # Turn the C Source Strings into compilable files
    cFile = open(cFileName,'w')
    hFile = open(hFileName,'w')
    cFile.write(cSourceArrayString)
    hFile.write(hSourceString)
    cFile.close()
    hFile.close()

    print("Files written!")
    return 0

def hex2CArray(hexfile, start=None, end=None, ARM=None, arrayName="CSourceArray"):
    import intelhex
    if hexfile == '-':
        hexfile = sys.stdin
    try:
        ih = intelhex.IntelHex(hexfile)
    except (IOError, intelhex.IntelHexError), e:
        sys.stderr.write('Error reading file: %s\n' % e)
        return 1
    if not (start is None and end is None):
        ih = ih[slice(start,end)]
        length = end - start
    else:
        length = ''
    # Do string manipulations on the hexdump
    sio = StringIO.StringIO()
    ih.dump(sio)
    hexString = sio.getvalue()
    hexString = hexString.splitlines(True)
    if ARM is None:
        cSourceArray = "code uint8_t "+arrayName+"["+str(length)+"] = \n    {\n"
    else:
        cSourceArray = "uint8_t "+arrayName+"["+arrayName.upper()+"_LENGTH] "+arrayName.upper()+"_ATTRIBUTE =\n    {\n"
    for line in hexString:
        line = line.split(None,17)
        del line[17]
        del line[0]
        cSourceArray += '    '
        for word in line:
            cSourceArray += "0x"+word+","
        cSourceArray += '\n'
    cSourceArray = cSourceArray.rstrip(',\n')
    cSourceArray += '\n    };\n\n'

    if ARM is None:
        string2Files(cSourceArray,length,arrayName)
    else:
        string2FilesARM(cSourceArray,start,length,arrayName)

    sio.close()
    return 0

def main(argv=None):
    import getopt

    if argv is None:
        argv = sys.argv[1:]

    start = None
    end = None
    ARM = None

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hvap:r:", ["help", "version", "ARM", "range="])

        for o, a in opts:
            if o in ("-h", "--help"):
                print(USAGE)
                return 0
            elif o in ("-v", "--version"):
                print(VERSION)
                return 0
            elif o in ("-a", "--ARM"):
                ARM = 1
            elif o in ("-r", "--range"):
                try:
                    l = a.split(":")
                    if l[0] != '':
                        start = int(l[0], 16)
                    if l[1] != '':
                        end   = int(l[1], 16)
                except:
                    raise getopt.GetoptError('No. Band Range.')
        if not args:
            raise getopt.GetoptError('You at lease need to specify the hex file')
        elif len(args) is 1:
            args.append("CSourceArray")
        elif len(args) > 2:
            raise getopt.GetoptError('Too many arguments!')
    except getopt.GetoptError, msg:
        txt = 'ERROR: '+str(msg)
        print(txt)
        print(USAGE)
        return 2

    try:
        hex2CArray(args[0], start, end, ARM, args[1])
    except IOError, e:
        import errno
        if e.errno not in (o,errno.EPIPE):
            raise


if __name__ == '__main__':
    import sys
    sys.exit(main())
