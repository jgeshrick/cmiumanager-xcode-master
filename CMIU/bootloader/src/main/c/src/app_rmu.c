/******************************************************************************
*******************************************************************************
**
**         Filename: app_rmu.c
**
**           Author: Troy Harstad
**          Created: 4/30/15
**
**     Last Edit By: Brian Arnberg 2015.08.14
**        Last Edit: Functions calling rmu_init can force it to report a POR.
**
**      Description:
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_rmu.c
*
* @brief This file contains the application code used to setup and use the
*        reset management unit (RMU).
*
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include "typedefs.h"
#include "MemoryMap.h"
#include "app_uart.h"
#include "app_rmu.h"
#include "em_rmu.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
static uint32_t resetCause RESET_SOURCE_VAR;


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/

/***************************************************************************//**
 * @brief
 *   Initializes rmu module
 *
 * @note

 *
 * @param[in] *ForcePor - report resetCause as POR if *ForcePor is true.
 *
 * @return None
 ******************************************************************************/
void app_rmu_init(uint32_t *ForcePor)
{
    DEBUG_INSTRUCTION(UU32 resetTemp);

    // Change reset cause to POR conditionally
    if (*ForcePor == true)
    {
        resetCause = POR;
    }
    else if (RESET_CAUSE_ERROR == resetCause)
    {
        resetCause = POR;
    }
    else
    {
        // Store the real reset cause
        resetCause = RMU_ResetCauseGet();
    }

    // Clear reset cause
    RMU_ResetCauseClear();


    DEBUG_INSTRUCTION(resetTemp.U32 = resetCause);
    DEBUG_OUTPUT_TEXT_AND_DATA("Reset Source: ", &resetTemp.U8[0], 4);


}


/***************************************************************************//**
 * @brief
 *   Getter function for reset cause
 *
 * @note

 *
 * @param[in] None
 *
 * @return uint32_t resetCause
 ******************************************************************************/
uint32_t app_rmu_resetCauseGet(void)
{
    return resetCause;
}

/***************************************************************************//**
 * @brief
 *   Setter function for reset cause
 *
 * @note

 *
 * @param[in] uint32_t resetCause
 *
 * @return none
 ******************************************************************************/
void app_rmu_resetCauseSet(uint32_t newResetCause)
{
    resetCause = newResetCause;
}

