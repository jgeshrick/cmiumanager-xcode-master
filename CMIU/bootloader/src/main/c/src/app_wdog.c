/******************************************************************************
*******************************************************************************
**
**         Filename: app_wdog.c
**    
**           Author: Troy Harstad
**          Created: 4/30/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_wdog.c
*
* @brief This file contains the application code used to setup and use the
*         watchdog.
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include "app_wdog.h"
#include "em_wdog.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/

/***************************************************************************//**
 * @brief
 *   Initializes watchdog module
 *
 * @note
 *   The CMIU is setup to use the 1kHz RC oscillator as the watchdog timer
 *   source and the 256k counter value (actually 262145) as the timeout period.
 *
 *   Timeout = (TimeoutPeriod) + 1) / f = (262145 + 1)/ 1000 = 262.146 seconds
 *  
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_wdog_init(void)
{ 
    
    WDOG_Init_TypeDef wdogInit;
    
    
    wdogInit.enable = false;                // Disasble wdog after init completed
    wdogInit.debugRun = false;              // Disable running during debug halt
    wdogInit.em2Run = true;                 // Counter shall keep running when in EM2
    wdogInit.em3Run = true;                 // Counter shall keep running when in EM3    
    wdogInit.em4Block = true;               // Block EMU from entering EM4
    wdogInit.swoscBlock = true;             // Block SW from disabling LFRCO/LFXO oscillators
    wdogInit.lock = false;                  // Do not block SW from modifying the configuration
    wdogInit.clkSel = wdogClkSelULFRCO;     // Use 1kHz internal RC oscillator 
    wdogInit.perSel = wdogPeriod_256k;      // Use 256k counter value for timeout period  
    
    
    // Init watchdog--Not enabled at this point
    WDOG_Init(&wdogInit);
      
}


/***************************************************************************//**
 * @brief
 *   Enables watchdog timer, locks watchdog settings
 *  
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_wdog_enable(void)
{
    // Feed dog before enabling
    WDOG_Feed();
    
    // Enable watchdog
    WDOG_Enable(true);
    
    // Lock watchdog
    WDOG_Lock();
    
}




