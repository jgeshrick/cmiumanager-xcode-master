/******************************************************************************
*******************************************************************************
**
**         Filename: app_uart.c
**    
**           Author: Barry Huggins
**          Created: 12/23/14
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_uart.c
*
* @brief This file contains some UART utility routines for the CMIU project.
* 
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "stdtypes.h"
#include "em_device.h"
#include "em_usart.h"
#include "em_assert.h"
#include "em_int.h"
#include "efm32lg_uart.h"
#include "autogen_usart1.h"
#include "app_uart.h"
#include "app_timers.h"



static void Uart_reverse(uint8_t s[]);
static void Uart_itoa(int n, uint8_t s[]);


/* Declare a circular buffer structure to use for Rx and Tx queues */
#define UART_BUFFERSIZE          512

volatile struct circularBuffer
{
    uint8_t  data[UART_BUFFERSIZE];  /* data buffer */
    uint32_t rdI;               /* read index */
    uint32_t wrI;               /* write index */
    uint32_t pendingBytes;      /* count of how many bytes are not yet handled */
    bool     overflow;          /* buffer overflow indicator */
} Uart_rxBuf1, Uart_txBuf1 = { {0}, 0, 0, 0, false };

void debug_printf (char *printString)
{
#ifdef SWO_DEBUG
    printf (printString);
#else
    Uart_Uart1WriteString (printString);
#endif // SWO_DEBUG    
}

/***************************************************************************//**
 * @brief
 *   UART1 setup function.
 *
 * @note
 *   This function sets up the interrupts for UART1.
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void Uart_Uart1Setup(void)
{

    /* Prepare UART Rx and Tx interrupts */
    USART_IntClear(USART1, _UART_IFC_MASK);
    USART_IntEnable(USART1, UART_IEN_RXDATAV);
    NVIC_ClearPendingIRQ(USART1_RX_IRQn); 
    NVIC_ClearPendingIRQ(USART1_TX_IRQn); 
    NVIC_EnableIRQ(USART1_RX_IRQn); 
    NVIC_EnableIRQ(USART1_TX_IRQn); 

    /* Enable UART */
    USART_Enable(USART1, usartEnable);
    
    /* Initialize Uart_rxBuf1 */
    Uart_rxBuf1.rdI = 0;
    Uart_rxBuf1.wrI = 0;
    Uart_rxBuf1.pendingBytes = 0;
    Uart_rxBuf1.overflow = false;
    
    /* Initialize Uart_txBuf1 */
    Uart_txBuf1.rdI = 0;
    Uart_txBuf1.wrI = 0;
    Uart_txBuf1.pendingBytes = 0;
    Uart_txBuf1.overflow = false;

    /* Make sure the interrup lock count starts at 0,
     * otherwise interrupts won't be enabled. */
    INT_LockCnt = 0;
}

/***************************************************************************//**
 * @brief
 *   UART0 setup function.
 *
 * @note
 *   This function sets up the interrupts for UART0.
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void Uart_Uart0Setup(void)
{

    /* Prepare UART Rx and Tx interrupts */
    USART_IntClear(USART0, _UART_IFC_MASK);
    USART_IntEnable(USART0, UART_IEN_RXDATAV);
    NVIC_ClearPendingIRQ(USART0_RX_IRQn); 
    NVIC_ClearPendingIRQ(USART0_TX_IRQn); 
    NVIC_EnableIRQ(USART0_RX_IRQn); 
    NVIC_EnableIRQ(USART0_TX_IRQn); 

    /* Enable UART */
    USART_Enable(USART0, usartEnable);
}

/***************************************************************************//**
 * @brief
 *   Checks for a input char from UART 1.
 *
 * @note
 *   This function looks for an incoming char from UART1.
 *       
 *
 * @param[in] none
 *
 * @return bool indicates if there is a char available for input from UART 1
 ******************************************************************************/
bool    Uart_Uart1IsCharReady()
{
    if (Uart_rxBuf1.pendingBytes < 1)
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}


/***************************************************************************//**
 * @brief
 *   Outputs the specified chars to UART 1.
 *
 * @note
 *   This function outputs the specified chars to UART1. If there is no space 
 *    for these chars then it will wait until space is available.
 *       
 *
 * @param[in] dataPtr
 *   characters to be output on UART 1
 * @param[in] dataLen
 *   number of chars to be output on UART 1
 *
 * @return none
 ******************************************************************************/
void Uart_Uart1PutData(char *dataPtr, uint16_t dataLen)
{
    uint32_t i = 0;

    /* Check if buffer is large enough for data */
    if (dataLen > UART_BUFFERSIZE)
    {
        /* Buffer can never fit the requested amount of data */
        return;
    }
  
    /* Check if buffer has room for new data */
    if ((Uart_txBuf1.pendingBytes + dataLen) > UART_BUFFERSIZE)
    {
        /* Wait until room */
        while ((Uart_txBuf1.pendingBytes + dataLen) > UART_BUFFERSIZE)
        {
            ;   // do nothing
        }
    }

    /* Fill dataPtr[0:dataLen-1] into txBuffer */
    INT_Disable();  
    while (i < dataLen)
    {
        Uart_txBuf1.data[Uart_txBuf1.wrI] = *(dataPtr + i);
        Uart_txBuf1.wrI             = (Uart_txBuf1.wrI + 1) % UART_BUFFERSIZE;
        i++;
    }

    /* Increment pending byte counter */
    Uart_txBuf1.pendingBytes += dataLen;
    INT_Enable(); 

    /* Enable interrupt on USART TX Buffer*/
    USART_IntEnable(USART1, UART_IEN_TXBL);
}

/***************************************************************************//**
 * @brief
 *   Outputs the specified string to UART 1.
 *
 * @note
 *   This function outputs the specified string to UART1. If there is no space 
 *    for these chars then it will wait until space is available.
 *       
 *
 * @param[in] dataPtr
 *   string to be output on UART 1
 *
 * @return none
 ******************************************************************************/
void Uart_Uart1WriteString (char *dataPtr)
{
    Uart_Uart1PutData(dataPtr, strlen(dataPtr));
}

/***************************************************************************//**
 * @brief
 *   Outputs a single char to UART 1.
 *
 * @note
 *   This function outputs a single char to UART1. If there is no space 
 *    for this char then it will wait until space is available.
 *       
 *
 * @param[in] char
 *   character to be output on UART 1
 *
 * @return none
 ******************************************************************************/
void Uart_Uart1PutChar(uint8_t ch)
{
    /* Check if Tx queue has room for new data */
    if ((Uart_txBuf1.pendingBytes + 1) > UART_BUFFERSIZE)
    {
        /* Wait until there is room in queue */
        while ((Uart_txBuf1.pendingBytes + 1) > UART_BUFFERSIZE) 
        {
            ;   // do nothing
        }
    }

    // disable interrupts as there are interrupt routines which also output to this UART.
    INT_Disable();  

    /* Copy ch into txBuffer */
    Uart_txBuf1.data[Uart_txBuf1.wrI] = ch;
    Uart_txBuf1.wrI = (Uart_txBuf1.wrI + 1) % UART_BUFFERSIZE;

    /* Increment pending byte counter */
    Uart_txBuf1.pendingBytes++;
    
    INT_Enable();  

    /* Enable interrupt on USART TX Buffer*/
    USART_IntEnable(USART1, UART_IEN_TXBL);
}



/***************************************************************************//**
 * @brief
 *   Inputs the specified number of chars from UART 1.
 *
 * @note
 *   This function inputs the specified number of chars from UART1. If these 
 *    chars are not available then it will wait for them.
 *       
 *
 * @param[in] dataPtr
 *   characters to be output on UART 1
 * @param[in] dataLen
 *   number of chars to be output on UART 1
 *
 * @return none
 ******************************************************************************/
uint32_t Uart_Uart1GetData(char *dataPtr, uint32_t dataLen)
{
    uint32_t i = 0;

    /* Wait until the requested number of bytes are available */
    if (Uart_rxBuf1.pendingBytes < dataLen)
    {
        while (Uart_rxBuf1.pendingBytes < dataLen) 
        {
            ;   // do nothing
        }
    }

    if (dataLen == 0)
    {
        dataLen = Uart_rxBuf1.pendingBytes;
    }

    /* Copy data from Rx buffer to dataPtr */
    while (i < dataLen)
    {
        *(dataPtr + i) = Uart_rxBuf1.data[Uart_rxBuf1.rdI];
        Uart_rxBuf1.rdI      = (Uart_rxBuf1.rdI + 1) % UART_BUFFERSIZE;
        i++;
    }

    /* Decrement pending byte counter */
    Uart_rxBuf1.pendingBytes -= dataLen;

    return i;
}

/***************************************************************************//**
 * @brief
 *   Gets an input char from UART 1.
 *
 * @note
 *   This function gets an incoming char from UART1. If there is no char 
 *    available then it will wait until a char is received.
 *       
 *
 * @param[in] none
 *
 * @return uint8_t provides an input char from UART 1
 ******************************************************************************/
char Uart_Uart1GetChar( )
{
    uint8_t ch;

    /* Check if there is a byte that is ready to be fetched. If no byte is ready, wait for incoming data */
    if (Uart_rxBuf1.pendingBytes < 1)
    {
        while (Uart_rxBuf1.pendingBytes < 1) 
        {
            ;   // do nothing
        }
    }

    /* Copy data from buffer */
    ch = Uart_rxBuf1.data[Uart_rxBuf1.rdI];
    Uart_rxBuf1.rdI = (Uart_rxBuf1.rdI + 1) % UART_BUFFERSIZE;

    /* Decrement pending byte counter */
    Uart_rxBuf1.pendingBytes--;

    return ch;
}

uint32_t    app_uart_uart1_bytesAvailable()
{
    return Uart_rxBuf1.pendingBytes;
}

/***************************************************************************//**
 * @brief
 *   UART1 RX IRQ Handler.
 *
 * @note
 *   This function is the input handler for UART1. 
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void USART1_RX_IRQHandler(void)
{
    /* Check for RX data valid interrupt */
    if (USART1->IF & UART_IF_RXDATAV)
    {
        /* Copy data into RX Buffer */
        uint8_t rxData = USART_Rx(USART1);
        Uart_rxBuf1.data[Uart_rxBuf1.wrI] = rxData;
        Uart_rxBuf1.wrI             = (Uart_rxBuf1.wrI + 1) % UART_BUFFERSIZE;
        Uart_rxBuf1.pendingBytes++;

        /* Flag Rx overflow */
        if (Uart_rxBuf1.pendingBytes > UART_BUFFERSIZE)
        {
            Uart_rxBuf1.overflow = true;
        }
    }
}

/***************************************************************************//**
 * @brief
 *   UART1 TX IRQ Handler.
 *
 * @note
 *   This function is the output handler for UART1. 
 *       
 *
 * @param[in] none
 *
 * @return none
 ******************************************************************************/
void USART1_TX_IRQHandler(void)
{
    /* Check TX buffer level status */
    if (USART1->IF & UART_IF_TXBL)
    {
        if (Uart_txBuf1.pendingBytes > 0)
        {
            /* Transmit pending character */
            USART_Tx(USART1, Uart_txBuf1.data[Uart_txBuf1.rdI]);
            Uart_txBuf1.rdI = (Uart_txBuf1.rdI + 1) % UART_BUFFERSIZE;
            Uart_txBuf1.pendingBytes--;
        }

        /* Disable Tx interrupt if no more bytes in queue */
        if (Uart_txBuf1.pendingBytes == 0)
        {
            USART_IntDisable(USART1, UART_IEN_TXBL);
        }
    }
}


/***************************************************************************//**
 * @brief
 *   Returns status of UART1 Tx Completion
 *
 * @note
 *       
 *
 * @param[in] none    
 *
 * @return bool 
 *    true if UART Tx is complete (0 pending bytes and TXC bit indicates
 *    Tx buffer and shift register contains no more data).
 ******************************************************************************/
bool Uart_Uart1_TxComplete(void)
{
    if((Uart_txBuf1.pendingBytes)  ||
        (!(USART1->STATUS & UART_STATUS_TXC)))
    {
        // Pending bytes remain or Tx is not complete
        return false;
    }
    
    else      
    {
        // No pending bytes and Tx is complete
        return true;
    }
}



/***************************************************************************//**
 * @brief
 *   Outputs the specified string and data to UART 1.
 *
 * @note
 *   This function outputs the specified string to UART1. If there is no space 
 *    for these chars then it will wait until space is available.
 *       
 *
 * @param[in] dataPtr
 *   string to be output on UART 1
 *
 * @return none
 ******************************************************************************/
void Uart_Uart1WriteStringAndData (char *dataPtr, uint8_t *pbyDataBuffer, uint8_t byDataLength)
{
    UU32 toBeConverted;
    uint32_t i;
    uint8_t str[10];
    
    EFM_ASSERT(byDataLength < 5);
    
    toBeConverted.U32 = 0UL;
    
    // Move bytes 
    for(i=0; i<byDataLength; i++)
    {
        toBeConverted.U8[i] = pbyDataBuffer[i];       
    }
      
    // Write text string
    Uart_Uart1PutData(dataPtr, strlen(dataPtr));    
    
    // Convert int to string
    Uart_itoa(toBeConverted.U32, str);
    
    // Write coverted string
    Uart_Uart1PutData((char *)str, strlen((char *)str));
    
    // Write new line and return.
    Uart_Uart1WriteString("\n\r");
}



/***************************************************************************//**
 * @brief
 *   Reverses string
 *
 * @note
 *       SHOULD CONSIDER MOVING THIS OUT OF UART.C!!!
 *
 * @param[in] 
 *
 * @return
 ******************************************************************************/
static void Uart_reverse(uint8_t s[])
{
    int c, i, j;

    for (i = 0, j = strlen((char *)s)-1; i<j; i++, j--) 
    {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}



/***************************************************************************//**
 * @brief
 *   Converts integer to string
 *
 * @note     
 *          SHOULD CONSIDER MOVING THIS OUT OF UART.C!!!
 * 
 * @param[in] 
 *
 * @return 
 ******************************************************************************/
/* itoa: convert n to characters in s */
static void Uart_itoa(int n, uint8_t s[])
{
    int i, sign;

    if ((sign = n) < 0) /* record sign */
    n = -n; /* make n positive */
    i = 0;
    do { /* generate digits in reverse order */
    s[i++] = n % 10 + '0'; /* get next digit */
    } while ((n /= 10) > 0); /* delete it */
    if (sign < 0)
    s[i++] = '-';
    s[i] = '\0';
    Uart_reverse(s);
}

 

