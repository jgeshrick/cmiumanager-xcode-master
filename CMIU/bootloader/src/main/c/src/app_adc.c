/******************************************************************************
*******************************************************************************
**
**         Filename: app_adc.c
**    
**           Author: Troy Harstad
**          Created: 7/6/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_adc.c
*
* @brief This file contains the application code used to setup the ADC
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include "app_adc.h"
#include "em_adc.h"
#include "typedefs.h"
#include "app_uart.h"

#ifndef CMIU_UNIT_TEST
    #include "app_cmit_interface.h"
#endif

#include "em_cmu.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static void app_adc_vddDiv3_store(uint32_t AdcVal);
static void app_adc_intTemp_store(int8_t AdcVal);


/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// Base the ADC configuration on the default setup 
static ADC_Init_TypeDef adcInit;// = ADC_INIT_DEFAULT;
static ADC_InitSingle_TypeDef adcSingleInit;// = ADC_INITSINGLE_DEFAULT;
static ADC_TypeDef *pAdc;
static uint32_t vddDiv3Adc;
static uint32_t vddAdcLastTx;
static int8_t internalTempInC;
static int8_t tempLastTx;


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/


/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/

/***************************************************************************//**
 * @brief
 *   Initializes ADC module
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_adc_init(void)
{ 
    // Set to default values
    adcInit.ovsRateSel = adcOvsRateSel2;
    adcInit.lpfMode = adcLPFilterBypass;
    adcInit.warmUpMode = adcWarmupNormal;
    adcInit.timebase = _ADC_CTRL_TIMEBASE_DEFAULT;
    adcInit.prescale = _ADC_CTRL_PRESC_DEFAULT;
    adcInit.tailgate = false;       
   
       
    // Set to default values
    adcSingleInit.prsSel = adcPRSSELCh0; 
    adcSingleInit.acqTime =  adcAcqTime16;
    adcSingleInit.reference = adcRef1V25;
    adcSingleInit.resolution = adcRes12Bit;
    adcSingleInit.input = adcSingleInpCh0;
    adcSingleInit.diff = false;
    adcSingleInit.prsEnable = false;
    adcSingleInit.leftAdjust = false;
    adcSingleInit.rep = false; 

    
    // Enable ADC0 clock
    CMU_ClockEnable(cmuClock_ADC0, true); 
        
    // Initialize timebases
    adcInit.timebase = ADC_TimebaseCalc(0);
    adcInit.prescale = ADC_PrescaleCalc(400000, 0);
    ADC_Init(ADC0, &adcInit);

    // Init pointer to ADC0
    pAdc = ADC0;
    
    // Init VDD/3 ADC value to 0 since a sample hasn't been taken yet
    vddDiv3Adc = 0x00;
    
    app_adc_intTemp_setupAndMeasure();

}


/***************************************************************************//**
 * @brief
 *   Setup and measurement of VDD/3
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_adc_vddDiv3_setupAndMeasure(void)
{   
    UU32 adcValue;
    
    // Set reference to 2.5 V
    adcSingleInit.reference = adcRef2V5;
    
    // Set input to VDD/3
    adcSingleInit.input = adcSingleInpVDDDiv3;
    
    // 12-bit ADC resolution
    adcSingleInit.resolution = adcRes12Bit;
    
    // Init ADC0 with above settings
    ADC_InitSingle(ADC0, &adcSingleInit);
    
    // Start ADC0 conversion
    ADC_Start(ADC0,adcStartSingle);
    
    // Wait until conversion has completed
    while(pAdc->STATUS & _ADC_STATUS_SINGLEACT_MASK)
    {
        // Wait for conversion to complete
    }
    
    while(!(pAdc->STATUS & _ADC_STATUS_SINGLEDV_MASK))
    {
        // Wait for valid data
    }
    
    // Retrieve ADC result
    adcValue.U32 = ADC_DataSingleGet(ADC0);
    
    // Display value
    DEBUG_OUTPUT_TEXT_AND_DATA("Battery Voltage: ", (uint8_t *)&adcValue.U8[0],4);  
    
    // Store value for later retrieval
    app_adc_vddDiv3_store(adcValue.U32);
}




/***************************************************************************//**
 * @brief
 *   Setup and measurement of internal temperature
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_adc_intTemp_setupAndMeasure(void)
{   
    UU32 adcValue;
    UU32 tempDisp;
    float  temp;
    uint8_t prod_rev;
    uint32_t temp_offset;
    
    /* Factory calibration temperature from device information page. */
    float cal_temp_0 = (float)((DEVINFO->CAL & _DEVINFO_CAL_TEMP_MASK)
                             >> _DEVINFO_CAL_TEMP_SHIFT);

    float cal_value_0 = (float)((DEVINFO->ADC0CAL2
                               & _DEVINFO_ADC0CAL2_TEMP1V25_MASK)
                              >> _DEVINFO_ADC0CAL2_TEMP1V25_SHIFT);

    /* Temperature gradient (from datasheet) */
    float t_grad = -6.3;
    
    // Set reference to 1.25 V
    adcSingleInit.reference = adcRef1V25;
    
    // Set input to temp sensor
    adcSingleInit.input = adcSingleInpTemp;
    
    // 12-bit ADC resolution
    adcSingleInit.resolution = adcRes12Bit;
    
    // Init ADC0 with above settings
    ADC_InitSingle(ADC0, &adcSingleInit);
    
    // Start ADC0 conversion
    ADC_Start(ADC0,adcStartSingle);
    
    // Wait until conversion has completed
    while(pAdc->STATUS & _ADC_STATUS_SINGLEACT_MASK)
    {
        // Wait for conversion to complete
    }
    
    while(!(pAdc->STATUS & _ADC_STATUS_SINGLEDV_MASK))
    {
        // Wait for valid data
    }
    
    // Retrieve ADC result
    adcValue.U32 = ADC_DataSingleGet(ADC0);
     
    
    /* This is a work around for Chip Rev.D Errata, Revision 0.6. */
    /* Check for product revision 16 and 17 and set the offset */
    /* for ADC0_TEMP_0_READ_1V25. */
    prod_rev = (DEVINFO->PART & _DEVINFO_PART_PROD_REV_MASK) >> _DEVINFO_PART_PROD_REV_SHIFT;

    if( (prod_rev == 16) || (prod_rev == 17) )
    {
        temp_offset = 112;
    }
    else
    {
        temp_offset = 0;
    }
    
    // Add correction factor
    adcValue.U32 += temp_offset;   
         
    // Calc temperature based on ADC value and calibration values
    temp = (cal_temp_0 - ((cal_value_0 - adcValue.U32)  / t_grad));
    
    // Round and convert to signed int
    if(temp >= 0)
    {
        temp += 0.5;
        tempDisp.S32 = (int32_t)temp;      
    }
    
    else
    {
        temp -= 0.5;
        tempDisp.S32 = (int32_t)temp;         
    }
    
    
    // Display value
    DEBUG_OUTPUT_TEXT_AND_DATA("Temp (C): ", (uint8_t *)&tempDisp.U8[0],4);  
    
    // Store value for later retrieval
    app_adc_intTemp_store((int8_t)temp);
}



/***************************************************************************//**
 * @brief
 *   Store the VDD/3 ADC measurement
 *
 * @note
 *
 * @param[in] AdcVal    ADC value to store
 *
 * @return None
 ******************************************************************************/
static void app_adc_vddDiv3_store(uint32_t AdcVal)
{
    vddDiv3Adc = AdcVal;   
}


/***************************************************************************//**
 * @brief
 *   Store the VDD/3 ADC measurement associated with most recent Tx attempt
 *
 * @note
 *
 * @param[in] AdcVal    ADC value to store
 *
 * @return None
 ******************************************************************************/
void app_adc_vddDiv3_mostRecentTx_store(uint32_t AdcVal)
{
    vddAdcLastTx = AdcVal;   
}


/***************************************************************************//**
 * @brief
 *   Retrieve the latest VDD/3 ADC measurement
 *
 * @note
 *
 * @param[in] None
 *
 * @return vddDiv3Adc
 ******************************************************************************/
uint32_t app_adc_vddDiv3_retrieve(void)
{
    return vddDiv3Adc;   
}

/***************************************************************************//**
 * @brief
 *   Retrieve the latest VDD/3 ADC measurement associated with latest Tx attempt
 *
 * @note
 *
 * @param[in] None
 *
 * @return vddDiv3Adc
 ******************************************************************************/
uint32_t app_adc_vddDiv3_mostRecentTx_retrieve(void)
{
    return vddAdcLastTx;   
}


/***************************************************************************//**
 * @brief
 *   Store the internal temp ADC measurement
 *
 * @note
 *
 * @param[in] AdcVal    ADC temperature value to store
 *
 * @return None
 ******************************************************************************/
static void app_adc_intTemp_store(int8_t AdcVal)
{
    internalTempInC = AdcVal;   
}



/***************************************************************************//**
 * @brief
 *   Store the internal temp value
 *
 * @note
 *
 * @param[in] temp    Temp value to store associated with last Tx attempt
 *
 * @return None
 ******************************************************************************/
void app_adc_intTemp_mostRecentTx_store(int8_t temp)
{
    tempLastTx = temp;   
}



/***************************************************************************//**
 * @brief
 *   Retrieve the latest internal temp ADC measurement
 *
 * @note
 *
 * @param[in] None
 *
 * @return internalTempInC
 ******************************************************************************/
int8_t app_adc_intTemp_retrieve(void)
{
    return internalTempInC;
}


/***************************************************************************//**
 * @brief
 *   Retrieve the temperature after latest transmission attempt
 *
 * @note
 *
 * @param[in] None
 *
 * @return tempLastTx
 ******************************************************************************/
int8_t app_adc_intTemp_mostRecentTx_retrieve(void)
{
    return tempLastTx;
}




