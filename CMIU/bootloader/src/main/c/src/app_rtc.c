/******************************************************************************
*******************************************************************************
**
**         Filename: app_rtc.c
**    
**           Author: Troy Harstad
**          Created: 1/16/15
**
**     Last Edit By: 
**        Last Edit: 
**    
**      Description: 
**
**
**
**
**
** Revision History:
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune 
**    Technology Group. The user, copying, transfer or disclosure of such 
**	  information is prohibited except by express written agreement with 
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file app_rtc.c
*
* @brief This file contains the application code used to setup/use the RTC
* 
************************************************************************/




/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdint.h>
#include "app_rtc.h"
#include "em_cmu.h"
#include "em_rtc.h"
#include "em_gpio.h"
#include "typedefs.h"
#include "app_uart.h"


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/


/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static void app_rtc_clock_inc(void);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
// Variable that gets incremented once per second that can be used
// by other modules for multi-second timeouts.
static volatile uint32_t secondsCount = 0;

// Default to Feb. 1st, 2015 12:00 AM GMT.
static volatile U_NEPTUNE_TIME cmiuTime = {.S64 = 1422748800};

// Time difference last time was updated
static U_NEPTUNE_TIME diffTime;

/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/



/*=========================================================================*/
/*  C O D E                                                                */
/*=========================================================================*/



/***************************************************************************//**
 * @brief
 *   Initializes RTC module
 *
 * @note
 *   Enables LFACLK and selects LFXO as clock source for RTC.  Sets up the RTC 
 *   to generate an interrupt every second.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void app_rtc_init(void)
{
    RTC_Init_TypeDef rtcInit = RTC_INIT_DEFAULT;

    /* Initialize global variables explicitly. */
    diffTime.S64 = 0;
    secondsCount = 0;
    cmiuTime.S64 = 1422748800;

    /* Enable LE domain registers */
    CMU_ClockEnable(cmuClock_CORELE, true);

    /* Enable LFXO as LFACLK in CMU. This will also start LFXO */
    CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);

    /* Set a clock divisor of 32 to reduce power conumption. */
    CMU_ClockDivSet(cmuClock_RTC, cmuClkDiv_32);

    /* Enable RTC clock */
    CMU_ClockEnable(cmuClock_RTC, true);

    /* Initialize RTC */
    rtcInit.enable   = false;  /* Do not start RTC after initialization is complete. */
    rtcInit.debugRun = false;  /* Halt RTC when debugging. */
    rtcInit.comp0Top = true;   /* Wrap around on COMP0 match. */
    RTC_Init(&rtcInit);

    /* Interrupt every second */
    RTC_CompareSet(0, ((RTC_FREQ / 32)) - 1 );

    /* Enable interrupt */
    NVIC_EnableIRQ(RTC_IRQn);
    RTC_IntEnable(RTC_IEN_COMP0);

    /* Start Counter */
    RTC_Enable(true);
}





/***************************************************************************//**
 * @brief
 *   Set the CMIU time via pointer
 *
 * @param[in] Pointer to NEPTUNE_TIME_T that the time gets set from
 *
 * @return None
 ******************************************************************************/
void app_rtc_time_set(U_NEPTUNE_TIME * time)
{   
    static U_NEPTUNE_TIME prevTime;
    
    // Store current time for later use
    prevTime.S64 = cmiuTime.S64;
    
    // Calculate time change, positive indicates new time is after prev time
    diffTime.S64 = time->S64 - prevTime.S64;
    
    DEBUG_OUTPUT_TEXT_AND_DATA("Time Change (Unix): ", (uint8_t*)&diffTime.S8[0], 4);
    
    // Check if new time is different to current time
    if(diffTime.S64 != 0)
    {
        // Reschedule tasks
//        app_cmiu_app_scheduler_timeChange(&diffTime);                 
    }
    
    
    // New time is now CMIU time
    cmiuTime.S64 = time->S64;  
}



/***************************************************************************//**
 * @brief
 *   Get the CMIU time via pointer
 *
 * @param[in] Pointer to NEPTUNE_TIME_T that time gets loaded in to
 *
 * @return None
 ******************************************************************************/
void app_rtc_time_get(U_NEPTUNE_TIME * time)
{
    time->S64 = cmiuTime.S64;  
}



/***************************************************************************//**
 * @brief
 *   Increment the CMIU time by one second
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void app_rtc_clock_inc(void)
{
    cmiuTime.S64++;

    DEBUG_OUTPUT_TEXT_AND_DATA("CMIU Time (Unix): ", (uint8_t*)&cmiuTime.S8[0], 4);
}


/***************************************************************************//**
 * @brief
 *   Interrupt Service Routine for RTC
 *
 * @note
 *   Should be reached once per second
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
void RTC_IRQHandler(void)
{
    // Clear RTC Comp 0 interrrupt
    RTC_IntClear(RTC_IFC_COMP0);    
    
    // Increment clock by one second
    app_rtc_clock_inc();
    
    // Update application interval task handlers 
//    app_cmiu_app_intervalUpdate();    

    // Increment seconds counter
    secondsCount++;
    
}

/***************************************************************************//**
 * @brief
 *   Get the CMIU time in seconds.
 *
 * @param[in] none
 *
 * @return number of seconds that the CMIU has been running.
 ******************************************************************************/
uint32_t app_rtc_seconds_get()
{
    return secondsCount;  
}


/***************************************************************************//**
 * @brief
 *   Get the last time difference in seconds.
 *
 * @param[in] none
 *
 * @return number of seconds the time was adjusted on the last call to 
 *     app_rtc_time_set().
 ******************************************************************************/
U_NEPTUNE_TIME app_rtc_timeDiff_full_get(void)
{
    return diffTime;  
}



/***************************************************************************//**
 * @brief
 *   Get the last time difference in seconds.
 *
 * @param[in] none
 *
 * @return number of seconds the time was adjusted on the last call to 
 *     app_rtc_time_set().
 ******************************************************************************/
int8_t app_rtc_timeDiff_partial_get(void)
{
    U_NEPTUNE_TIME tempDiffTime;
    
    tempDiffTime.S64 =  diffTime.S64;
    
    // If time difference is positive and > 127 seconds, then use 127
    if(tempDiffTime.S64 > 127)       
    {
        return 127;
    }

    // If time difference is 0 up to 127 then use actual value of
    // tempDiffTime.S8[7] (LSB)
    else if ((tempDiffTime.S64 >= 0) && (tempDiffTime.S64 <= 127))
    {
        return tempDiffTime.S8[7];
    }
    
      
    // If time difference is negative and < -128, then use -128
    else if (tempDiffTime.S64 <= -128)
    {
        return -128;
    }
    
    // If time difference is -1 down to -128 then use actual value of
    // tempDiffTime.S64[7] (LSB), set negative bit 
    else  //((tempDiffTime.S64 < 0x00) && (tempDiffTime.S64 > -128))
    {
        return (tempDiffTime.S8[7] |= 0x80);
    }
    
    
}




