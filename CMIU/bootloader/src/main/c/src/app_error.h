/******************************************************************************
*******************************************************************************
**
**         Filename: Error.h
**    
**           Author: Troy Harstad
**          Created: 10/08/2009
**
**     Last Edit By: 
**        Last Edit: 
**    
**         Comments: This is the header file for Error.c source file.  Only
**                   externally accessible functions are prototyped here.
**
** Revision History:
**          10/08/09: First created (Troy Harstad)
**
**
**
**
**    Copyright 2009 as unpublished work.
**    All rights reserved
**
**    The information The information contained herein is confidential 
**    property of Neptune Technology Group. The user, copying, transfer 
**    or disclosure of such information is prohibited except by express 
**    written agreement with Neptune Technology Group.**
**
******************************************************************************
******************************************************************************/


// Next two lines and the ending #endif guard against multiple inclusion
// thus making the header file idempotent.
#ifndef HEADER_ERROR_H
#define HEADER_ERROR_H


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h


/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None


/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
// No Error
#define ERROR_CODE_NO_ERROR			    0x00
//
// Error Codes
//
// ARB Errors
#define ERROR_ARB_TIMEOUT                   0x01
#define ERROR_ARB_DATA_PARITY               0x02
#define ERROR_ARB_DATA_CHECKSUM             0x03
#define ERROR_ARB_DATA_FORMAT               0x04
#define ERROR_ARB_DATA_UNKNOWN_REG          0x05
#define ERROR_ARB_DATA_MISMATCH_REG_TYPE    0x06
#define ERROR_ARB_DATA_MISMATCH_REG_ID      0x07

// Other Errors
#define ERROR_IMGCRC_CONFIGURATION      0x10

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
extern void Error_Init(void);
extern void Error_InitHW(void);

// Error Code helper functions
extern uint8_t Error_ErrorCode_Retrieve(void);
extern uint8_t Error_ErrorCode_ARB_Retrieve(void);
extern void Error_ErrorCode_Clear(void);
extern void Error_ErrorCode_ARB_Clear(void);
// Error Code setting functions
extern void Error_ErrorCode_ARB_Set(uint8_t byCode);
extern void Error_ARB_Data_Timeout(void);
extern void Error_ARB_Data_Parity(void);
extern void Error_ARB_Data_Checksum(void);
extern void Error_ARB_Data_Format(void);
extern void Error_ARB_Data_Unknown_Register(void);
extern void Error_ARB_Data_Mismtach_RegisterType(void);
extern void Error_ARB_Data_Mismtach_RegisterID(void);
extern void Error_ImageCRC_Configuration(void);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
         

/*=========================================================================*/
/*  M I S C                                                                */
/*=========================================================================*/
// None


#endif
