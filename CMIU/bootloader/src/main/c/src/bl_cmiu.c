/******************************************************************************
*******************************************************************************
**
**         Filename: bl_cmiu.c
**
**           Author: Troy Harstad
**          Created: 1/26/2015
**
**     Last Edit By: Brian Arnberg
**        Last Edit: 2015.06.08
**
**      Description:
**
**
**
** Revision History:
**        1/26/2015: First created (by Troy Harstad, as app_cmiu.bl)
**        6/08/2015: Moved to CMIU bootloader project
**
**    Copyright 2015 as unpublished work.
**    All rights reserved
**
**    The information contained herein is confidential property of Neptune
**    Technology Group. The use, copying, transfer or disclosure of such
**    information is prohibited except by express written agreement with
**    Neptune Technology Group.
**
******************************************************************************
******************************************************************************/

/*!***********************************************************************
* @file bl_cmiu.c
*
* @brief Initializes CMIU for bootloader operations, conditionally updates
*        application images, conditionally launches applications or enters
*        No Apps Mode.
*
************************************************************************/
/*=========================================================================*/
/*  I N C L U D E    F I L E S                                             */
/*=========================================================================*/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include "typedefs.h"
#include "MemoryMap.h"
#include "efm32lg332f256.h"
#include "stdtypes.h"
#include "autogen_init.h"
#include "cmiuFlash.h"
#include "cmiuSpi.h"
#include "app_uart.h"
#include "app_timers.h"
#include "app_power.h"
#include "app_rtc.h"
#include "app_gpio.h"
#include "app_cmu.h"
#include "app_error.h"
#include "app_wdog.h"
#include "app_rmu.h"
#include "CmiuBootloaderConfiguration.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "em_timer.h"
#include "em_msc.h"

#include "bl_ivu.h"
#include "cmiuImages.h"



/*=========================================================================*/
/*  D E F I N I T I O N S                                                  */
/*=========================================================================*/
/**
 ** @brief Bootloader States
 **
 ** @note What should the bootloader do next?
 **/
typedef enum BL_STATE
{
    /** The images still needs to be validated */
    PleaseValidate,
    /** All Internal images are valid; There are no new images to program */
    LaunchApplication,
    /** There is a new image pending, and it is a valid image */
    LoadNewImage,
    /** Current app is bad!!! Load backup image */
    LoadBackup,
    /** Store Current images to Backup Locations */
    StoreBackup,
    /** Reset the CMIU */
    ResetSelf,
    /** Run NoApps Mode */
    NoAppsMode
} BL_STATE;

/*=========================================================================*/
/*  F U N C T I O N   P R O T O T Y P E S                                  */
/*=========================================================================*/
static BL_STATE bl_cmiu_init(void);
static BL_STATE bl_cmiu_check_images(void);
static BL_STATE bl_cmiu_programNewImage(void);
static BL_STATE bl_cmiu_restoreFromBackup(void);
static BL_STATE bl_cmiu_storeBackupImage(void);
static BL_STATE bl_cmiu_launch_application(void);
static void bl_cmiu_NoAppsMode(void);
static void bl_cmiu_Callback(uint32_t address);
static void bl_cmiu_reset(void);

static void bl_cmiu_ClearJustProgrammed(void);

/*=========================================================================*/
/*  V A R I A B L E S                                                      */
/*=========================================================================*/
/** This is a pointer to an array of ImageInformation_t type blocks.
 * Each item in the array contains information about the location of
 * the associated image, its image type, and its size. The pointer
 * is initially undefined, but a routine in CmiuBootloaderConfiguration.c
 * initializes the variable so that it points to a valid array (either
 * within the bootloader itself or within the Configuration image.
 */
ImageInformation_t* ImageLists;

/** This const will be updated by the initialization routine only after a
 * factory reprogramming event.
 */
static const volatile uint32_t JustProgrammed BOOTLOADER_SPECIAL_ATTRIBUTE = (uint8_t) true;

/** How many more times can we try to backup the image? */
static uint32_t TryBackupAgain;


/*=========================================================================*/
/*  T Y P E   D E C L A R A T I O N S                                      */
/*=========================================================================*/
// See Typedefs.h
// Let the compiler check the typedefs.  For instance if int8_t ever becomes
// something other than 1 byte the [sizeof(int8_t) == 1] will become false,
// which means the union member int8_err will have an array size of zero.
// The compiler will give an error if the array size is set to zero.
// PC-lint will give several errors for this since it is an incomplete array
// and the mebers are not referenced so it is removed from the lint check using
// the #ifndef _lint directive.
#ifndef _lint
union {
    char int8_err   [sizeof(int8_t)    == 1];
    char uint8_err  [sizeof(uint8_t)   == 1];
    char int16_err  [sizeof(int16_t)   == 2];
    char uint16_err [sizeof(uint16_t)  == 2];
    char int32_err  [sizeof(int32_t)   == 4];
    char uint32_err [sizeof(uint32_t)  == 4];
    char uint64_err [sizeof(uint64_t)  == 8];
    char int64_err  [sizeof(int64_t)   == 8];
    char ptr_err    [sizeof(uintptr_t) == 4];
} forceErrorSometimes_t ; //Anonymous
#endif

/*=========================================================================*/
/*  M A C R O S                                                            */
/*=========================================================================*/
// None



/***************************************************************************//**
 * @brief
 *   Main Function
 *
 * @note
 *   Initializes bootloader, checks image CRC's, checks image versions (internal
 *   and external memory), conditionally updates/reprograms images. If the
 *   application image and configuration image are both good, this starts the
 *   application. Otherwise, it goes into No Apps Mode.
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
int main(void)
{
    /** Image Status Information */
    static volatile BL_STATE NextState;
    const S_MIU_IMG_INFO *version;

    /**  Initialize CMIU BootLoader. */
    NextState = bl_cmiu_init();
//#define FORCE_BULK_ERASE_FIRST
    #ifdef FORCE_BULK_ERASE_FIRST
        /** Initialize communications with the external flash */
        Flash_init();

        /** Killit. */
        if(Flash_BulkErase())
        {
            DEBUG_OUTPUT_TEXT("BULK ERASE SUCCESS\r\n");
        }
        else
        {
            DEBUG_OUTPUT_TEXT("BULK ERASE FAILED\r\n");
        }

        /** Deinitialize communications with the external flash */
        Flash_Deinit();
    #endif /* FORCE_BULK_ERASE_FIRST */
//#define FORCE_NO_APPS
    #ifdef FORCE_NO_APPS
        NextState = NoAppsMode;
    #endif /* FORCE_NO_APPS */

    /** Log application version to UART1 */
    version = (S_MIU_IMG_INFO *) BOOTLOADER_INFO;
    char versionToPrint[40];
    sprintf(versionToPrint,
        "\n\rCmiuBootloader v%x.%x.%x.%x\n\r\n\r",
        version->versionMajor,
        version->versionMinor,
        version->versionYearMonthDay,
        version->versionBuild);
    DEBUG_OUTPUT_TEXT(versionToPrint);

    /** Bootloader image validation state machine */
    for(;;)
    {
        /** Use NextState to determine whether to launch the program,
         ** reprogam an image, or go to NoAppsMode. */
        switch (NextState)
        {
            /** Validate the various images. Decide whether to launch
                the application or load an image. */
            case PleaseValidate:
            {
                NextState = bl_cmiu_check_images();
                break;
            }

            /** All necessary internal images are valid. There are no
                pending changes. */
            case LaunchApplication:
            {
                /* Start Application and don't return! */
                NextState = bl_cmiu_launch_application();
                /** From Joe: If you get to the point where the
                 * Application Launch fails, the processor will
                 * continue to RESET and end up in this mode.
                 * You may consider going to No_Apps_Mode if
                 * you continue in this loop. */
                break;
            }

            /** The new image in external memory is OK. Replace the
                internal image with the new image. */
            case LoadNewImage:
            {
                NextState = bl_cmiu_programNewImage();
                break;
            }

            /** The internal image needs to be reverted to whatever
                image is in backup. */
            case LoadBackup:
            {
                NextState = bl_cmiu_restoreFromBackup();
                break;
            }

            /** At least one internal image needs to be stored to
                its backup location in external memory. */
            case StoreBackup:
            {
                NextState = bl_cmiu_storeBackupImage();
                break;
            }

            /** Issue a software reset. */
            case ResetSelf:
            {
                bl_cmiu_reset(); // This does not return
                break;
            }

            /** Go to NoAppsMode if certain internal images are bad
                and there's nothing else to do. */
            case NoAppsMode:    /* FALLTHROUGH */
            default:
            {
                /* Go to NoAppsMode and don't come back! */
                bl_cmiu_NoAppsMode();
                break;
            }
        }
    }

    /* If we are here, we need to reset the unit. */
    bl_cmiu_reset();

}


/***************************************************************************//**
 * @brief
 *   Performs overall CMIU initialization - besides what is being done in
 *   autogen modules.
 *
 * @note
 *
 *
 * @param[in] None
 *
 * @return NextState - Based on the reset source, decide the next BL state.
 ******************************************************************************/
static BL_STATE bl_cmiu_init(void)
{
    uint32_t  startDelay;
    BL_STATE NextState = PleaseValidate;

    /* Initialize chip */
    eADesigner_Init();
    gpio_Init();
    Uart_Uart1Setup();

    //USART2_setup();
    USART_Enable(USART2, usartEnable);//USART2_setup performs the same functions that this does.

    /* Self Identify */
    DEBUG_OUTPUT_TEXT("\r\nBootLoader \r\n");

    /* Do real initialization and overwrite some autogeneration stuff */
    // CMU should be initialized first since if a module clock is disabled, the
    // registers of that module are not accessible and reading from such
    // registers may return undefined values. Writing to registers of clock
    // disabled modules have no effect. One should normally avoid accessing
    // module registers of a module with a disabled clock.
    app_cmu_init();

    // Init RMU module
    app_rmu_init((uint32_t *)&JustProgrammed);

    // Update the image lists
    ImageLists = getImageLists();

    /* Check RESET source */
    #ifdef CHECK_RESET
    /* If RESET SOURCE was mag-swipe, skip to application execution */
    if ((app_rmu_resetCauseGet() & MAG_SWIPE) == MAG_SWIPE)
    {
        NextState = LaunchApplication;
        DEBUG_OUTPUT_TEXT("Skipping to application...\n\r");
    }
    else
    {
        NextState = PleaseValidate;
    }
    #endif /* CHECK_RESET */

    /** If we're going to launch the application, we can skip the rest
        of the initialization */
    if (NextState != LaunchApplication)
    {
        // Initialize GPIO module
        app_gpio_init();

        // Set the flash module to a known state 
        Flash_startup();

        // Initialize adc module
        app_adc_init();

        // Initialize Timers module
        app_timers_init();

        // Initialize the bluetooth module (make sure it's off).
        // We would call something like app_ble_init(), but the bootloader
        // doesn't have the app_ble module, so we can't call it. Fortunately,
        // we really just want to ensure the bluetooth is powered off, which we
        // do in app_gpio_init()

        // Init modem module (make sure it's off).
        // app_modem_init();
        app_power_modemPower_off();

        // Init wdog module
        //app_wdog_init(); /* This will interfere with the application if we let it run */

        // Init the image validation unit
        bl_ivu_init();

        // Init SPI controller
        cmiuSpi_init();


        // Delay before possible sleep so that debugger can attach to target
        for(startDelay = 0x1FFFFF; startDelay>0; startDelay--)
        {
            __NOP();
        }


        DEBUG_OUTPUT_TEXT("Bootloader Initialization is complete\n\r");


        // Moving this to the end since it causes the unit to hang upon reset if
        // ringbuffer isn't initialized first.  Still need enable interrupt only when
        // needed!!!
        //Uart_Uart0Setup(); //Enabling this causes the BL to hang if we go to NoAppsMode

        // Enable watchdog timer
        //app_wdog_enable(); /* Do we really want this? */

        // We will only try to backup images thrice
        TryBackupAgain = 3;
    }


    return NextState;

}

/***************************************************************************//**
 * @brief
 *   Performs image validation for each CMIU image.
 *
 * @note
 *   A valid image has a good CRC and proper version information.
 *
 *
 * @param[in] None
 *
 * @return NextState - Based on the validation, the next state the bootloader
 *                     should execute.
 ******************************************************************************/
static BL_STATE bl_cmiu_check_images(void)
{
    BL_STATE NextState;
    IMAGE_VALIDATION_INFORMATION ValidationStatus = SafeToLaunch;

    DEBUG_OUTPUT_TEXT("Begin image validation...\n\r");

    /** Use bl_ivu_CheckImages to validate all known images. */
    ValidationStatus = bl_ivu_CheckImages(ImageLists,START_OF_CURRENT,NUMBER_OF_IMAGES);

    if (ValidationStatus == SafeToLaunch)
    {
        NextState = LaunchApplication;
        DEBUG_OUTPUT_TEXT("\n\r...preparing to launch application.\n\r");
    }
    else if (ValidationStatus == CorruptBackup)
    {
        NextState = StoreBackup;
        DEBUG_OUTPUT_TEXT("\n\r...backup images corrupt.\n\r");
        if(TryBackupAgain == 0)
        {
            DEBUG_OUTPUT_TEXT("...preparing to launch application anyways.\n\r");
            NextState = LaunchApplication;
        }

    }
    else if (ValidationStatus == NewImageValid)
    {
        NextState = LoadNewImage;
        DEBUG_OUTPUT_TEXT("\n\r...preparing to load new image.\n\r");
    }
    else if (ValidationStatus == CorruptInternals)
    {
        NextState = LoadBackup;
        DEBUG_OUTPUT_TEXT("\n\r...preparing to restore from backup image.\n\r");
    }
    else
    {
        NextState = NoAppsMode;
        DEBUG_OUTPUT_TEXT("\n\r...preparing to enter NoAppsMode.\n\r");
    }

    return NextState;

}

/***************************************************************************//**
 * @brief
 *   Overwrites the internal application with the new application in the
 *   external memory.
 *
 * @note
 *
 * @param[in] None
 *
 * @return NextState: Based on the result of the programming attempt; what the
 *          bootloader should do next.
 ******************************************************************************/
static BL_STATE bl_cmiu_programNewImage(void)
{
    static BL_STATE NextState = PleaseValidate;
    uint32_t sumImageStatus;
    bool updateAttempted;

    sumImageStatus = bl_ivu_GetSumImageStatus();
    updateAttempted = false;

    // 1. Save the old image?
    // 2. Erase the old image.
    // 3. Copy new image from external memory to the proper location in internal
    //    memory.
    // 4. Mark the new image in external memory as moved?
    // 5. Schedule image validation.

     if ((sumImageStatus & (NEW_APPLICATION)) == (NEW_APPLICATION))
    {
        /** First, backup the current application. */
        if ((sumImageStatus & (CMIU_APPLICATION)) == (CMIU_APPLICATION))
        {
            DEBUG_OUTPUT_TEXT("...Backup the current application\r\n");
            cmiuImages_putExternal(&ImageLists[CmiuApplication], &ImageLists[BackupApplication]);
        }

        /** Then, run the update. */
        DEBUG_OUTPUT_TEXT("...Update the application\r\n");
        cmiuImages_getExternal(&ImageLists[NewApplication], &ImageLists[CmiuApplication]);
        updateAttempted = true;
    }

    if ((sumImageStatus & (NEW_CONFIGURATION)) == (NEW_CONFIGURATION))
    {
        /** First, backup the current configuration. */
        if ((sumImageStatus & (CMIU_CONFIGURATION)) == (CMIU_CONFIGURATION))
        {
            DEBUG_OUTPUT_TEXT("...Backup the current configuration\r\n");
            cmiuImages_putExternal(&ImageLists[CmiuConfiguration], &ImageLists[BackupConfiguration]);
        }

        /** Then, run the update. */
        DEBUG_OUTPUT_TEXT("...Update the configuration\r\n");
        cmiuImages_getExternal(&ImageLists[NewConfiguration], &ImageLists[CmiuConfiguration]);
        updateAttempted = true;
    }

    if ((sumImageStatus & (NEW_ENCRYPTION)) == (NEW_ENCRYPTION))
    {
        /** First, backup the current application. */
        if ((sumImageStatus & (CMIU_ENCRYPTION)) == (CMIU_ENCRYPTION))
        {
            DEBUG_OUTPUT_TEXT("...Backup the current encryption\r\n");
            cmiuImages_putExternal(&ImageLists[CmiuEncryption], &ImageLists[BackupEncryption]);
        }

        /** Then, run the update. */
        DEBUG_OUTPUT_TEXT("...Update the encryption\r\n");
        cmiuImages_getExternal(&ImageLists[NewEncryption], &ImageLists[CmiuEncryption]);
        updateAttempted = true;
    }

    if ((sumImageStatus & (NEW_BLE_CONFIGURATION)) == (NEW_BLE_CONFIGURATION))
    {
        /** First, backup the current application. */
        if ((sumImageStatus & (CMIU_BLE_CONFIGURATION)) == (CMIU_BLE_CONFIGURATION))
        {
            DEBUG_OUTPUT_TEXT("...Backup the current BLE configuration\r\n");
            cmiuImages_putExternal(&ImageLists[CmiuBleConfiguration], &ImageLists[BackupBleConfiguration]);
        }

        /** Then, run the update. */
        DEBUG_OUTPUT_TEXT("...Update the BLE configuration\r\n");
        cmiuImages_getExternal(&ImageLists[NewBleConfiguration], &ImageLists[CmiuBleConfiguration]);
        updateAttempted = true;
    }

    if (updateAttempted)
    {
        DEBUG_OUTPUT_TEXT("...CMIU Image Update is complete\n\r");
        NextState = ResetSelf;

        /* Report a POR to the application */
        app_rmu_resetCauseSet(RESET_CAUSE_ERROR);
    }
    else
    {
        DEBUG_OUTPUT_TEXT("...No Image Updates Attempted.\n\r");
        if ((sumImageStatus & SAFE_TO_LAUNCH) == SAFE_TO_LAUNCH)
        {
            /** The update failed, but we still have enough to launch. */
            NextState = LaunchApplication;
        }
        else
        {
            /** The update failed and our current images are insufficient,
             ** so let's try to load from backup images. */
            NextState = LoadBackup;
        }
    }

    return NextState;
}

/***************************************************************************//**
 * @brief
 *   Overwrites the internal application with the backup application in the
 *   external memory.
 *
 * @note
 *
 * @param[in] None
 *
 * @return NextState: Based on the result of the programming attempt; what the
 *          bootloader should do next.
 ******************************************************************************/
static BL_STATE bl_cmiu_restoreFromBackup(void)
{
    static BL_STATE NextState = NoAppsMode;
    uint32_t sumImageStatus;
    bool restoreAttempted;

    sumImageStatus = bl_ivu_GetSumImageStatus();
    restoreAttempted = false;

    // 1. Copy backup image from external memory to the proper location in internal
    //    memory.
    // 2. Schedule processor reset.

    if ((sumImageStatus & (BACKUP_APPLICATION | CMIU_APPLICATION)) == (BACKUP_APPLICATION))
    {
        DEBUG_OUTPUT_TEXT("...Restore the application\r\n");
        cmiuImages_getExternal(&ImageLists[BackupApplication], &ImageLists[CmiuApplication]);
        restoreAttempted = true;
    }

    if ((sumImageStatus & (BACKUP_CONFIGURATION | CMIU_CONFIGURATION)) == (BACKUP_CONFIGURATION))
    {
        DEBUG_OUTPUT_TEXT("...Restore the configuration\r\n");
        cmiuImages_getExternal(&ImageLists[BackupConfiguration], &ImageLists[CmiuConfiguration]);
        restoreAttempted = true;
    }

    if ((sumImageStatus & (BACKUP_ENCRYPTION | CMIU_ENCRYPTION)) == (BACKUP_ENCRYPTION))
    {
        DEBUG_OUTPUT_TEXT("...Restore the encryption\r\n");
        cmiuImages_getExternal(&ImageLists[BackupEncryption], &ImageLists[CmiuEncryption]);
        restoreAttempted = true;
    }

    if ((sumImageStatus & (BACKUP_BLE_CONFIGURATION | CMIU_BLE_CONFIGURATION)) == (BACKUP_BLE_CONFIGURATION))
    {
        DEBUG_OUTPUT_TEXT("...Restore the ble configuration\r\n");
        cmiuImages_getExternal(&ImageLists[BackupBleConfiguration], &ImageLists[CmiuBleConfiguration]);
        restoreAttempted = true;
    }

    if (restoreAttempted)
    {
        DEBUG_OUTPUT_TEXT("...Restore is complete\n\r");
        NextState = ResetSelf;

        /* Report a POR to the application */
        app_rmu_resetCauseSet(RESET_CAUSE_ERROR);
    }
    else
    {
        DEBUG_OUTPUT_TEXT("...No Images were restored from backup.\r\n");
        if ((sumImageStatus & SAFE_TO_LAUNCH) == SAFE_TO_LAUNCH)
        {
            /** The restore failed, but we still have enough to launch. */
            NextState = LaunchApplication;
        }
        else
        {
            /** There's nothing else to do but go to NoAppsMode. */
            NextState = NoAppsMode;
        }
    }

    return NextState;
}

/***************************************************************************//**
 * @brief
 *   Overwrites the backup image with whatever is in internal flash memory.
 *
 * @note
 *   This will only overwrite corrupt backup images.
 *
 * @param[in] None
 *
 * @return NextState: Based on the result of the programming attempt; what the
 *         bootloader should do next.
 ******************************************************************************/
static BL_STATE bl_cmiu_storeBackupImage(void)
{
    static BL_STATE NextState = PleaseValidate;
    uint32_t sumImageStatus;
    bool storeAttempted;

    sumImageStatus = bl_ivu_GetSumImageStatus();
    storeAttempted = false;

    // 1. Copy current image from internal memory to the proper location in
    //    external memory.
    // 2. Schedule image validation.

    if ((sumImageStatus & (BACKUP_APPLICATION | CMIU_APPLICATION)) == (CMIU_APPLICATION))
    {
        DEBUG_OUTPUT_TEXT("...Backup the application\r\n");
        cmiuImages_putExternal(&ImageLists[CmiuApplication], &ImageLists[BackupApplication]);
        storeAttempted = true;
    }

    if ((sumImageStatus & (BACKUP_CONFIGURATION | CMIU_CONFIGURATION)) == (CMIU_CONFIGURATION))
    {
        DEBUG_OUTPUT_TEXT("...Backup the configuration\r\n");
        cmiuImages_putExternal(&ImageLists[CmiuConfiguration], &ImageLists[BackupConfiguration]);
        storeAttempted = true;
    }

    #if 0
    #warning this is so bad don't ship with this in place it's really bad
    if ((sumImageStatus & (NEW_CONFIGURATION | CMIU_CONFIGURATION)) == (CMIU_CONFIGURATION))
    {
        DEBUG_OUTPUT_TEXT("...put the configuration\r\n");
        cmiuImages_putExternal(&ImageLists[CmiuConfiguration], &ImageLists[NewConfiguration]);
        storeAttempted = true;
    }
    #endif

    if ((sumImageStatus & (BACKUP_ENCRYPTION | CMIU_ENCRYPTION)) == (CMIU_ENCRYPTION))
    {
        DEBUG_OUTPUT_TEXT("...Backup the encryption\r\n");
        cmiuImages_putExternal(&ImageLists[CmiuEncryption], &ImageLists[BackupEncryption]);
        storeAttempted = true;
    }

    if ((sumImageStatus & (BACKUP_BLE_CONFIGURATION | CMIU_BLE_CONFIGURATION)) == (CMIU_BLE_CONFIGURATION))
    {
        DEBUG_OUTPUT_TEXT("...Backup the ble configuration\r\n");
        cmiuImages_putExternal(&ImageLists[CmiuBleConfiguration], &ImageLists[BackupBleConfiguration]);
        storeAttempted = true;
    }

    if (storeAttempted)
    {
        NextState = PleaseValidate;
        DEBUG_OUTPUT_TEXT("...Backup complete\n\r");

        if (TryBackupAgain > 0)
        {
            --TryBackupAgain;
        }
    }
    else
    {
        DEBUG_OUTPUT_TEXT("...No images were stored to their backup locations.\n\r");
        if ((sumImageStatus & SAFE_TO_LAUNCH) == SAFE_TO_LAUNCH)
        {
            /** The store failed, but we can launch safely. */
            NextState = LaunchApplication;
        }
        else
        {
            /** This is a last-ditched effort, but let's try to load from Backup. */
            NextState = LoadBackup;
        }
    }

    return NextState;
}

/***************************************************************************//**
 * @brief
 *   Bootloader mode that is executed when the application image or the
 *   configuration image is bad.
 *
 * @note
 *   NoAppsMode must initialize the RTC and watchdog timer before it begins
 *   running. These cannot be started in the global initialization function
 *   because they will interfere with other operations.
 *
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void bl_cmiu_NoAppsMode(void)
{
    uint8_t i;
    DEBUG_OUTPUT_TEXT("...No Apps Mode running\n\r");

    // Initialize RTC module
    app_rtc_init();
    // Init wdog module
    app_wdog_init();
    // Enable the pesky watchdog
    app_wdog_enable();

    //Start countdown from 60
    i = 60;

    // Infinite Loop
    for(;;)
    {
        /* TODO: Actually fleshout the No Apps Mode. */
        /* Bothersome #warning so I don't forget... */
        #warning NoApps mode needs to be defined.

        // Set PA.4 upon wakeup
        DEBUG_INSTRUCTION(GPIO_PinOutSet(gpioPortA, 4));

        // Toggle PA.3 on every wakeup (1 second)
        DEBUG_INSTRUCTION(GPIO_PinOutToggle(gpioPortA, 3));

        // Feed watchdog timer
        WDOG_Feed();

        if (0 == i)
        {
            /* Display "NoApps Mode" once a minute. */
            DEBUG_OUTPUT_TEXT("NoApps Mode. \r\n");
            /* Reset countdown iterator. */
            i = 60;
        }
        /* Always decrement the countdown timer. */
        --i;

        // Wait for UART to complete before entering sleep mode
        DEBUG_WAITFORUART();

        // Clear PA.4 before going to sleep
        DEBUG_INSTRUCTION(GPIO_PinOutClear(gpioPortA, 4));

        // Enter EM2 and wake up up to one second later
        EMU_EnterEM2(true);
    }
}
////////////////////////////////////////////////
//////// H A R D F A U L T   H A N D L E R /////
////////////////////////////////////////////////
void HardFault_Handler(void)
{
  GPIO_PinOutSet(gpioPortA, 4);
  GPIO_PinOutSet(gpioPortA, 3);
  NVIC_SystemReset();
}


/***************************************************************************//**
 * @brief
 *   Launches the internal application.
 *
 * @note
 *   This function should not return. However, if it does, we will tell the
 *   state machine to go to NoAppsMode.
 *
 * @param[in] None
 *
 * @return
 *  BL_STATE.NoAppsMode - if the function returns, we should go to NoAppsMode.
 ******************************************************************************/
static BL_STATE bl_cmiu_launch_application(void)
{

    DEBUG_OUTPUT_TEXT("...launching application\n\r\n\r");

    /** Conditionally clear JustProgrammed before we try to
     * launch the application */
    if (JustProgrammed)
    {
        bl_cmiu_ClearJustProgrammed();
    }

    /* Wait for UART to complete before jumping to application */
    DEBUG_WAITFORUART();

    /* Jump to application address */
    bl_cmiu_Callback((uint32_t)(ImageLists[CmiuApplication].Address));

    /* If we get here, the application launch failed, so we should
     * go to NoAppsMode to conserve power and keep from continuously
     * reissuing the Callback.
     */
    return NoAppsMode;
}

/***************************************************************************//**
 * @brief
 *   Jumps to required application
 *
 * @note
 *   Resets the IVT address and stack pointer, and then jumps to the application
 *   reset vector.
 *
 * @param[in]
 *   address [in] The address to jump to
 *
 * @return None
 ******************************************************************************/
static void bl_cmiu_Callback(uint32_t address)
{
    /*lint -e923 intentional casts as the address is being cast into a pointer */
    /* The reset vector lives at the second entry to the vector table, so + 4 */
    uint32_t* pResetVector = (uint32_t *)(address + 4u);

    /* Set the interrupt vector table */
    SCB->VTOR = address;

    /* Set stack pointer to that of the application */
    __set_MSP(*(uint32_t *)address);

    /* Jump to the application reset vector.
       The reset vector is stored as the second element in
       the vector table. */
    ((void (*)())(*pResetVector))(); /*lint !e746 Call to a function without a prototype as it is a jump to an address */
    /* lint -restore Restore from above */
}

/***************************************************************************//**
 * @brief
 *   Reset the device.
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void bl_cmiu_reset(void)
{
    /* Reset device */
    DEBUG_OUTPUT_TEXT("Executing Device Reset...\n\r\n\r");

    /* Wait for UART to complete before entering sleep mode */
    DEBUG_WAITFORUART();

    /* Use CMSIS standard library function to issue the softare reset */
    NVIC_SystemReset();
}

/***************************************************************************//**
 * @brief
 *   Clear the JustProgrammed variable (which is in internal flash)
 *
 * @note
 *
 * @param[in] None
 *
 * @return None
 ******************************************************************************/
static void bl_cmiu_ClearJustProgrammed(void)
{
    uint8_t buffer[4];
    uint32_t i;
    msc_Return_TypeDef mscRetval;

    /** Prep the buffer to be all zeros */
    buffer[0] = 0x00;
    buffer[1] = 0x00;
    buffer[2] = 0x00;
    buffer[3] = 0x00;

    /** We'll try to force the clear only 4 times */
    i = 4;

    /** Wait until i is cleared */
    while(i>0)
    {
        /** Init the memory controller */
        MSC_Init();

        /** Write the buffer to the location of JustProgrammed */
        mscRetval = MSC_WriteWord((uint32_t *)&JustProgrammed, buffer, 4);

        /** Deinit the memory controller */
        MSC_Deinit();

        /** Prepare to exit if the write was successful */
        if (mscRetval == 0)
        {
            i = 0;
        }
        else
        {
            --i;
        }
    }

}
