/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */
package com.neptunetg.mdce.lora.service;

import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;

/**
 * Saves packets to DynamoDB
 */
public interface MiuPacketSaver
{
    void insertAsync(MiuPacketReceivedDynamoItem miuPacket);
}
