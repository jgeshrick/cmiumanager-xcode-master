/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.lora.debug;

import com.neptunetg.common.lora.parser.LoraPacketParser;
import com.neptunetg.common.lora.pdu.L900Packet;
import com.neptunetg.common.util.HexUtils;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This controller stands in for Senet's and Actilities downlink URL.  Any messages sent to this URL are logged
 * for debug purposes.
 */
@Controller
public class DummyDownlinkController
{
    private static final Logger log = LoggerFactory.getLogger(DummyDownlinkController.class);

    @Value("#{envProperties['lora.actility.aslrckey']}")
    String asLrcKey;

    private final LoraPacketParser packetParser = new LoraPacketParser();

    private final LinkedList receivedCommandsSenet = new LinkedList();

    private final LinkedList receivedCommandsActility = new LinkedList();

    @RequestMapping(value = "/debug/senet/dummy-downlink", method = RequestMethod.POST)
    public ResponseEntity receiveSenetDownlink(
             @RequestParam("eui") String eui,
             @RequestParam("user") String user,
             @RequestParam("pass") String password,
             @RequestParam("port") String port,
             @RequestParam("value") String value,
             @RequestHeader HttpHeaders headers)
    {
        final StringBuilder logMessage = new StringBuilder("Received HTTP POST at /debug/senet/dummy-downlink.  EUI=");
        logMessage.append(eui).append("; credentials: ").append(user).append('/').append(password).append("; ");
        logMessage.append("port=").append(port).append(System.lineSeparator()).append("HTTP headers: ");

        for (Map.Entry<String, List<String>> header : headers.entrySet())
        {
            for (String headerValue : header.getValue())
            {
                logMessage.append(header.getKey()).append('=').append(headerValue).append("; ");
            }
        }
        final long contentLength = headers.getContentLength(); //-1L if Content-Length not present
        if (contentLength == -1L)
        {
            return new ResponseEntity(HttpStatus.LENGTH_REQUIRED); //return 411 - this is what Senet does
        }

        logMessage.append(value).append(System.lineSeparator());

        try
        {
            final byte[] pduBytes = HexUtils.parseHexToByteArray(value);
            final L900Packet pdu = packetParser.parsePacket(pduBytes);
            logMessage.append(pdu);
        }
        catch (Exception e)
        {
            logMessage.append("An error occurred trying to parse the PDU: ").append(e);
        }

        log.info(logMessage.toString());

        receivedCommandsSenet.add(eui + "," + value);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/debug/actility/dummy-downlink", method = RequestMethod.POST)
    public ResponseEntity receiveActilityDownlink(
            @RequestParam("DevEUI") String eui,
            @RequestParam("FPort") String fPort,
            @RequestParam("Payload") String payload,
            @RequestParam("AS_ID") String asId,
            @RequestParam("Time") String time,
            @RequestParam("Token") String token,
            @RequestHeader HttpHeaders headers
    )
    {
        final StringBuilder logMessage = new StringBuilder("Received HTTP POST at /debug/actility/dummy-downlink.  EUI=");
        logMessage.append(eui).append(";");
        logMessage.append("port=").append(fPort).append(System.lineSeparator()).append("HTTP headers: ");

        for (Map.Entry<String, List<String>> header : headers.entrySet())
        {
            for (String headerValue : header.getValue())
            {
                logMessage.append(header.getKey()).append('=').append(headerValue).append("; ");
            }
        }

        final long contentLength = headers.getContentLength(); //-1L if Content-Length not present

        if (contentLength == -1L)
        {
            //return 411 - this is what Senet does, I don't know if Actility does this too
            return new ResponseEntity(HttpStatus.LENGTH_REQUIRED);
        }

        logMessage.append(payload).append(System.lineSeparator());

        try
        {
            final byte[] pduBytes = HexUtils.parseHexToByteArray(payload);
            final L900Packet pdu = packetParser.parsePacket(pduBytes);
            logMessage.append(pdu);
        }
        catch (Exception e)
        {
            logMessage.append("An error occurred trying to parse the PDU: ").append(e);
        }

        if(authenticateActilityToken(eui, fPort, payload, asId, time, asLrcKey, token))
        {
            logMessage.append("Token authenticated");
            log.info(logMessage.toString());
            receivedCommandsActility.add(eui + "," + payload);
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        else
        {
            logMessage.append("Token failed authentication");
            log.info(logMessage.toString());
            return new ResponseEntity(HttpStatus.UNAUTHORIZED); //Should actually be a 350 error
        }
    }

    private boolean authenticateActilityToken(String eui, String fPort, String payload, String asId, String time, String asLrcKey, String token)
    {
            String concatenatedDetails =
                            "DevEUI=" + eui +
                            "&FPort=" + fPort +
                            "&Payload=" + payload +
                            "&AS_ID=" + asId +
                            "&Time=" + time +
                            asLrcKey;

        try
        {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(concatenatedDetails.getBytes(StandardCharsets.UTF_8));
            return token.equals(Hex.encodeHexString(hash));
        }
        catch (NoSuchAlgorithmException e)
        {
            log.error("No such algorithm exception", e);
        }

        return false;
    }

    @RequestMapping(value = "/debug/senet/dummy-downlink", method = RequestMethod.GET)
    @ResponseBody
    public String getReceivedCommandSenet()
    {
        if(!receivedCommandsSenet.isEmpty())
        {
            return (String) receivedCommandsSenet.pop();
        }

        return "";
    }

    @RequestMapping(value = "/debug/actility/dummy-downlink", method = RequestMethod.GET)
    @ResponseBody
    public String getReceivedCommandActility()
    {
        if(!receivedCommandsActility.isEmpty())
        {
            return (String) receivedCommandsActility.pop();
        }

        return "";
    }
}

