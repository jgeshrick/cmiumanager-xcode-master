/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.lora.data;

import com.neptunetg.mdce.common.config.DynamoDynamicConfigService;
import org.springframework.stereotype.Service;

/**
 * Dynamic config is not available as this application cannot access the database.
 */
@Service
public class NoDynamicConfigService implements DynamoDynamicConfigService
{
    @Override
    public boolean isDynamicConfigAvailable()
    {
        return false;
    }

    @Override
    public long getOldTableDeletionMinAgeMonths()
    {
        throw new UnsupportedOperationException("No dynamic config in this application");
    }
}
