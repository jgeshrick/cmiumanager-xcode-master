/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */
package com.neptunetg.mdce.lora.service;

import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.mdce.common.data.PacketRepository;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
@EnableScheduling
public class DynamoDbMiuPacketSaver implements MiuPacketSaver, DisposableBean
{
    private static final Logger log = LoggerFactory.getLogger(DynamoDbMiuPacketSaver.class);

    private static final int MAX_MESSAGE_BATCH = 10000; //to avoid insanely large batches

    @Autowired
    private PacketRepository packetRepo;

    @Autowired
    private MdceIpcPublishService awsQueueService;

    private final BlockingQueue<MiuPacketReceivedDynamoItem> miuPacketsToSave = new LinkedBlockingQueue<>();

    private final AtomicBoolean shutdownFlag = new AtomicBoolean(false);


    /**
     * A periodically running task to update miu details in mysql table and add received packet to dynamo
     */
    @Scheduled(fixedDelay = 100)
    public void updateMiuDetails()
    {
        if (!shutdownFlag.get())
        {
            try
            {
                processMiuPackets();
            }
            catch (Exception e)
            {
                log.error("Error while processing queues.  Continuing", e);
            }
        }
    }

    private boolean processMiuPackets()
    {
        final List<MiuPacketReceivedDynamoItem> packets = new ArrayList<>();
        miuPacketsToSave.drainTo(packets, MAX_MESSAGE_BATCH);

        if (!packets.isEmpty())
        {
            try
            {
                this.packetRepo.insertMiuPacketsReceived(packets);
                log.debug("inserted {} packets into Dynamo", packets.size());
            }
            catch (Exception ex)
            {
                final AlertSource alertSource = AlertSource.AWS_EC2_DB_FAILED_ADD;
                awsQueueService.sendMessage(MdceIpcPublishService.ALERTS,
                        "{\"Source\":\"" + alertSource.getAlertId() + "\"," +
                                "\"Level\":\"Error\"," +
                                "\"Message\":\"Cannot save packet to dynamoDb. " + ex.getMessage() + "\"}");

                log.error("Cannot save packet to dynamo", ex);
            }
        }
        return !miuPacketsToSave.isEmpty();
    }

    @Override
    public void insertAsync(MiuPacketReceivedDynamoItem miuPacket)
    {
        if (miuPacket != null)
        {
            this.miuPacketsToSave.add(miuPacket);
        }
    }

    public String getCountsDisplay()
    {
        return MessageFormat.format("packets {0}",
                this.miuPacketsToSave.size());
    }


    @Override
    public void destroy() throws Exception
    {
        if (!this.shutdownFlag.get())
        {
            boolean queuesNotEmpty;

            this.shutdownFlag.set(true);
            do
            {
                log.info("Shutting down... queue size: {}", getCountsDisplay());

                try
                {
                    queuesNotEmpty = processMiuPackets();
                } catch (Exception e)
                {
                    log.error("Error occurred during shutdownFlag.  Unprocessed messages may be lost: " + getCountsDisplay(), e);
                    break;
                }
            } while (queuesNotEmpty);
        }
    }
}
