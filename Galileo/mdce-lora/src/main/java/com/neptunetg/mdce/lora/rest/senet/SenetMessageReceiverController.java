/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */
package com.neptunetg.mdce.lora.rest.senet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.data.miu.Eui;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.lora.json.SenetForwardPacketJson;
import com.neptunetg.common.lora.parser.LoraPacketParser;
import com.neptunetg.common.lora.pdu.PacketId;
import com.neptunetg.common.lora.pdu.TimeAndDatePacket;
import com.neptunetg.common.lora.pdu.TimeOffsetCommandPacket;
import com.neptunetg.mdce.common.data.model.DynamoItemPacketType;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import com.neptunetg.mdce.lora.rest.BadRequestException;
import com.neptunetg.mdce.lora.service.MiuPacketSaver;
import com.neptunetg.mdce.lora.service.SenetDownlinkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;

/**
 * Receives LoRa messages forwarded by Senet.  See "HTTP/HTTPS" in
 * "Senet Packet Forwarding v1.3.pdf" for more information.
 */
@Controller
public class SenetMessageReceiverController
{
    private static final Logger log = LoggerFactory.getLogger(SenetMessageReceiverController.class);

    private final LoraPacketParser loraPacketParser = new LoraPacketParser();

    private final ObjectMapper jackson = new ObjectMapper();

    @Autowired
    private MdceIpcPublishService sqsPublisher;

    @Autowired
    private MiuPacketSaver miuPacketSaver;

    @Autowired
    private SenetDownlinkService downlinkService;

    @RequestMapping(value="/lora/senet", method=RequestMethod.POST)
    public ResponseEntity processSenetMessage(@RequestBody String requestBody) throws Exception
    {
        final SenetForwardPacketJson l900SenetPacket;
        final Eui eui;
        final PacketId packetId;
        final Instant builtTime;

        try
        {
            l900SenetPacket = loraPacketParser.parseSenetForwardPacketJson(requestBody);
            eui = Eui.valueOf(l900SenetPacket.getEui());
            packetId = l900SenetPacket.getPacketIdFromPdu();
            builtTime = l900SenetPacket.getTxTimeInstant();

        }
        catch (IOException e)
        {
            log.error("Failed to parse JSON received at Senet listener!\nJSON received:\n\n" + requestBody, e);
            throw new BadRequestException("Failed to parse JSON received", e); // 400 bad request
        }

        log.debug("\nReceived message from L900 with ID: " + eui.extractMiuId().toString() + "\nJSON received:\n\n" + requestBody);

        try
        {
            buildDynamoItemAndSave(eui, packetId, builtTime,
                    requestBody.getBytes(StandardCharsets.UTF_8));
        }
        catch (Exception e)
        {
            log.error("Failed to save JSON received at Senet listener for EUI " + eui + "!\nJSON received:\n\n" + requestBody, e);
            throw e; // 500 internal server error
        }

        try
        {
            respondIfTimeDatePacket(eui, packetId, builtTime, l900SenetPacket);
        }
        catch (Exception e)
        {
            log.error("Failed to transmit time offset command JSON to Senet for EUI " + eui, e);
            // OK response
        }

        try
        {
            updateLastHeardTime("Senet", requestBody);
        }
        catch (Exception e)
        {
            log.error("Failed to enqueue last heard time update for JSON received at Senet listener for EUI " + eui + "!\nJSON received:\n\n" + requestBody, e);
            // OK response
        }

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


    private void buildDynamoItemAndSave(Eui eui, PacketId packetId, Instant builtTime,
                                        byte[] l900SenetPacketRawBytes)
    {
        final MiuId l900Id = eui.extractMiuId();

        final Instant currentTime = Instant.now();

        long builtTimeEpoch = 0;

        if(builtTime != null)
        {
            builtTimeEpoch = builtTime.toEpochMilli();
        }

        final String packetType = DynamoItemPacketType.fromSystemAndId(packetId.getId(), "L900").toString();

        final MiuPacketReceivedDynamoItem miuPacket = new MiuPacketReceivedDynamoItem();
        miuPacket.setMiuId(l900Id.numericValue());
        miuPacket.setPacketData(l900SenetPacketRawBytes);
        miuPacket.setPacketType(packetType);
        miuPacket.setDateBuilt(builtTimeEpoch);
        miuPacket.setInsertDate(currentTime.toEpochMilli());
        miuPacket.setDateReceived(currentTime.toEpochMilli());
        miuPacket.setSourceDeviceType("L900");
        miuPacket.setTargetDeviceType("MDCE");

        miuPacketSaver.insertAsync(miuPacket);
    }

    private void respondIfTimeDatePacket(Eui eui, PacketId packetId, Instant builtTime,
                                         SenetForwardPacketJson l900SenetPacket) throws IOException
    {
        if(PacketId.TIME_AND_DATE.equals(packetId))
        {
            log.debug("Creating time & date command packet in response to L900 with ID: " +
                    eui.extractMiuId().toString());

            final TimeAndDatePacket timeAndDatePacket = loraPacketParser.parseTimeAndDatePacket(l900SenetPacket.getPduBytes());

            final Duration offset = Duration.between(timeAndDatePacket.getTimeAndDate(), builtTime);

            if(offset.abs().getSeconds() > 5)
            {
                final TimeOffsetCommandPacket timeOffsetCommandPacket =
                        new TimeOffsetCommandPacket(offset, timeAndDatePacket.getUpdateIdentifier());

                final byte[] timeOffsetCommandPdu = timeOffsetCommandPacket.getBytes();

                downlinkService.sendMessageToSenetDevice(eui, timeOffsetCommandPdu);
            }
        }
    }

    private void updateLastHeardTime(String provider, String requestBody)
    {

        try
        {
            final StringBuilder forwardedMessage = new StringBuilder("LoRa-Provider: ").append(provider).append('\n').append('\n');
            forwardedMessage.append(requestBody);
            sqsPublisher.sendMessage(MdceIpcPublishService.LORA_PACKETS_RECEIVED, forwardedMessage.toString());
        }
        catch (Exception e)
        {
            log.error("Failed to enqueue last heard time update for JSON received at Senet listener! " + requestBody, e);
        }

    }


}
