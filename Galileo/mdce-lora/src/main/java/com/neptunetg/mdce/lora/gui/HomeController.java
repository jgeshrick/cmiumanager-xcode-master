/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.lora.gui;

import com.neptunetg.mdce.lora.service.ApplicationVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

/**
 * Just a empty handler for root url. Does nothing functional.
 */
@Controller
public class HomeController
{
    @Autowired
    private ApplicationVersion applicationVersion;

    @Value("#{envProperties['env.name']}")
    private String envName;

    @Value("#{envProperties['server.name']}")
    private String serverName;

    /**
     * Landing page for mdce-lora
     */
    @RequestMapping(value="/pages/debug", method= {RequestMethod.GET, RequestMethod.HEAD})
    public String getIndexPage(Model model)
    {
        model.addAttribute("environmentName", envName);
        model.addAttribute("serverName", serverName);
        model.addAttribute("currentServerTime", new Date());
        model.addAttribute("applicationVersion", applicationVersion.getApplicationVersion());
        return "index";
    }
}
