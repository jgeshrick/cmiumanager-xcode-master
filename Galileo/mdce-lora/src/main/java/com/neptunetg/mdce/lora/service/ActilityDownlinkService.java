/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.lora.service;

import com.neptunetg.common.data.miu.Eui;
import com.neptunetg.common.util.HexUtils;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;

/**
 * Class that can send messages to Actility
 */
@Service
public class ActilityDownlinkService
{
    private static final Logger log = LoggerFactory.getLogger(ActilityDownlinkService.class);

    private final static String FPort = "1";

    private final String urlPattern;
    private static String asLrcKey;
    private static String asId;

    @Autowired
    public ActilityDownlinkService(
            @Value("#{envProperties['lora.actility.downlink.url']}") String urlPattern,
            @Value("#{envProperties['lora.actility.asid']}") String asId,
            @Value("#{envProperties['lora.actility.aslrckey'] }") String asToLrcKey
    )
    {
        this.urlPattern = urlPattern;
        this.asLrcKey = asToLrcKey;
        this.asId = asId;
    }

    public void sendMessageToActilityDevice(Eui eui, byte[] pduBytes) throws IOException
    {
        log.debug("Sending PDU to L900 with ID: " + eui.extractMiuId().toString() +
                " PDU: " + HexUtils.byteArrayToHex(pduBytes));

        final String time = Instant.now().toString();
        final String payload = HexUtils.byteArrayToHex(pduBytes);
        final String token = generateToken(eui, payload, time);

        String urlParameters = ("?DevEUI=" + eui.toString() +
                                "&FPort=" + FPort +
                                "&Payload=" + payload +
                                "&AS_ID=" + asId +
                                "&Time=" + time +
                                "&Token=" + token);

        try
        {
            final URL actilityUrl = new URL(this.urlPattern + urlParameters);
            final HttpURLConnection con = (HttpURLConnection) actilityUrl.openConnection();

            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("charset", "UTF-8");
            con.setRequestProperty("Content-Length", "0");
            con.setFixedLengthStreamingMode(0L); //empty POST.  Sets Content-Length header.

            int responseCode = con.getResponseCode();

            con.disconnect();

            if (responseCode >= 200 && responseCode < 300)
            {
                return; //all done, empty response
            }
            else
            {
                throw new IOException("Failure response code from Actility (" + responseCode + ") when sending message for EUI " + eui + "; URL was " + actilityUrl);
            }
        }
        catch (Exception e)
        {
            throw new IOException("Error while attempting to send message for EUI " + eui + "; URL was " + this.urlPattern, e);
        }
    }

    private String generateToken(Eui eui, String payload, String time)
    {
        String concatenatedDetails =
                        "DevEUI=" + eui.toString() +
                        "&FPort=" + FPort +
                        "&Payload=" + payload +
                        "&AS_ID=" + asId +
                        "&Time=" + time +
                        asLrcKey;

        try
        {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(concatenatedDetails.getBytes(StandardCharsets.UTF_8));
            return Hex.encodeHexString(hash);
        }
        catch (NoSuchAlgorithmException ex)
        {
            log.error("SHA-256 - No such algorithm exception thrown", ex);
            return ""; //Return an empty string if failed as we can't generate the token
        }
    }
}
