/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */
package com.neptunetg.mdce.lora.rest.actility;

import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.data.miu.Eui;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.lora.json.ActilityUplinkJson;
import com.neptunetg.common.lora.parser.LoraPacketParser;
import com.neptunetg.common.lora.pdu.PacketId;
import com.neptunetg.common.lora.pdu.TimeAndDatePacket;
import com.neptunetg.common.lora.pdu.TimeOffsetCommandPacket;
import com.neptunetg.mdce.common.data.model.DynamoItemPacketType;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import com.neptunetg.mdce.lora.rest.BadRequestException;
import com.neptunetg.mdce.lora.service.ActilityDownlinkService;
import com.neptunetg.mdce.lora.service.MiuPacketSaver;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.Instant;

/**
 * Receives LoRa messages forwarded by Actility.
 */
@Controller
public class ActilityMessageReceiverController
{
    private static final Logger log = LoggerFactory.getLogger(ActilityMessageReceiverController.class);
    private static final int actilityPacketIdOffset = 50;

    @Value("#{envProperties['lora.actility.aslrckey']}")
    private String asLrcKey;

    private final LoraPacketParser loraPacketParser = new LoraPacketParser();

    @Autowired
    private MdceIpcPublishService sqsPublisher;

    @Autowired
    private MiuPacketSaver miuPacketSaver;

    @Autowired
    private ActilityDownlinkService downlinkService;

    @RequestMapping(value="/lora/actility", method=RequestMethod.POST)
    public ResponseEntity processActilityMessage(@RequestBody String requestBody,
                                                 @RequestParam(value = "AS_ID") String asId,
                                                 @RequestParam(value = "LrnDevEui") String lrnDevEui,
                                                 @RequestParam(value = "LrnFPort") String lrnFPort,
                                                 @RequestParam(value = "LrnInfos") String lrnInfos,
                                                 @RequestParam(value = "Time") String httpGenTime,
                                                 @RequestParam(value = "Token") String token) throws Exception
    {
        final ActilityUplinkJson l900ActilityPacket;
        final Eui eui;
        final PacketId packetId;
        final Instant builtTime;

        try
        {
            l900ActilityPacket = loraPacketParser.parseActilityForwardPacketJson(requestBody);
            eui = Eui.valueOf(l900ActilityPacket.getEui());
            packetId = l900ActilityPacket.getPacketIdFromPdu();
            builtTime = l900ActilityPacket.getTxTimeInstant();
        }
        catch (IOException e)
        {
            log.error("Failed to parse JSON received at Actility listener!\nJSON received:\n\n" + requestBody, e);
            throw new BadRequestException("Failed to parse JSON received", e); // 400 bad request
        }

        log.debug("\nReceived message from L900 with ID: " + eui.extractMiuId().toString() + "\nJSON received:\n\n" + requestBody);

        if(authenticateActilityToken(lrnDevEui, lrnFPort, lrnInfos, asId, httpGenTime,
                l900ActilityPacket, asLrcKey, token))
        {
            try
            {
                buildDynamoItemAndSave(eui, packetId, builtTime,
                        requestBody.getBytes(StandardCharsets.UTF_8));
            }
            catch (Exception e)
            {
                log.error("Failed to save JSON received at Actility listener for EUI " + eui + "!\nJSON received:\n\n" + requestBody, e);
                throw e; // 500 internal server error
            }

            try
            {
                respondIfTimeDatePacket(eui, packetId, builtTime, l900ActilityPacket);
            }
            catch (Exception e)
            {
                log.error("Failed to transmit time offset command JSON to Actility for EUI " + eui, e);
                // OK response
            }

            try
            {
                updateLastHeardTime("Actility", requestBody);
            }
            catch (Exception e)
            {
                log.error("Failed to enqueue last heard time update for JSON received at Actility listener for EUI " + eui + "!\nJSON received:\n\n" + requestBody, e);
                // OK response
            }
        }

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


    private void buildDynamoItemAndSave(Eui eui, PacketId packetId, Instant builtTime,
                                        byte[] l900ActilityPacketRawBytes)
    {
        final MiuId l900Id = eui.extractMiuId();

        final Instant currentTime = Instant.now();

        long builtTimeEpoch = 0;

        if(builtTime != null)
        {
            builtTimeEpoch = builtTime.toEpochMilli();
        }

        final String packetType = DynamoItemPacketType.fromSystemAndId(
                packetId.getId() + actilityPacketIdOffset, "L900").toString();

        final MiuPacketReceivedDynamoItem miuPacket = new MiuPacketReceivedDynamoItem();
        miuPacket.setMiuId(l900Id.numericValue());
        miuPacket.setPacketData(l900ActilityPacketRawBytes);
        miuPacket.setPacketType(packetType);
        miuPacket.setDateBuilt(builtTimeEpoch);
        miuPacket.setInsertDate(currentTime.toEpochMilli());
        miuPacket.setDateReceived(currentTime.toEpochMilli());
        miuPacket.setSourceDeviceType("L900");
        miuPacket.setTargetDeviceType("MDCE");

        miuPacketSaver.insertAsync(miuPacket);
    }

    private void respondIfTimeDatePacket(Eui eui, PacketId packetId, Instant builtTime,
                                         ActilityUplinkJson l900ActilityPacket) throws IOException
    {
        if(PacketId.TIME_AND_DATE.equals(packetId))
        {
            log.debug("Creating time & date command packet in response to L900 with ID: " +
                    eui.extractMiuId().toString());

            final TimeAndDatePacket timeAndDatePacket = loraPacketParser.parseTimeAndDatePacket(l900ActilityPacket.getPduBytes());

            final Duration offset = Duration.between(timeAndDatePacket.getTimeAndDate(), builtTime);

            if(offset.abs().getSeconds() > 5)
            {
                final TimeOffsetCommandPacket timeOffsetCommandPacket =
                        new TimeOffsetCommandPacket(offset, timeAndDatePacket.getUpdateIdentifier());

                final byte[] timeOffsetCommandPdu = timeOffsetCommandPacket.getBytes();

                downlinkService.sendMessageToActilityDevice(eui, timeOffsetCommandPdu);
            }
        }
    }

    private void updateLastHeardTime(String provider, String requestBody)
    {

        try
        {
            final StringBuilder forwardedMessage = new StringBuilder("LoRa-Provider: ").append(provider).append('\n').append('\n');
            forwardedMessage.append(requestBody);
            sqsPublisher.sendMessage(MdceIpcPublishService.LORA_PACKETS_RECEIVED, forwardedMessage.toString());
        }
        catch (Exception e)
        {
            log.error("Failed to enqueue last heard time update for JSON received at Actility listener! " + requestBody, e);
        }

    }

    private boolean authenticateActilityToken(String LrnDevEui, String LrnFPort, String LrnInfos, String asId, String time,
                                              ActilityUplinkJson actilityForwardPacketJson, String LrcAsKey, String token) throws NoSuchAlgorithmException
    {
        String concatenatedDetails =
                actilityForwardPacketJson.getCustomerId() +
                actilityForwardPacketJson.getEui() +
                actilityForwardPacketJson.getPort() +
                actilityForwardPacketJson.getSeqNo() +
                actilityForwardPacketJson.getPdu() +
                "LrnDevEui=" + LrnDevEui +
                "&LrnFPort=" + LrnFPort +
                "&LrnInfos=" + LrnInfos +
                "&AS_ID=" + asId +
                "&Time=" + time +
                LrcAsKey;

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(concatenatedDetails.getBytes(StandardCharsets.UTF_8));

        return token.equals(Hex.encodeHexString(hash));
    }
}
