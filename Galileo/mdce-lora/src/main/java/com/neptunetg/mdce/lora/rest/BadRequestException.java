/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserve
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.lora.rest;

/**
 * Global REST exception handler for HTTP 400 Bad Request error
 */
public class BadRequestException extends Exception
{
    private static final long serialVersionUID = 1L;

    public BadRequestException()
    {
    }

    public BadRequestException(String message)
    {
        super(message);
    }

    public BadRequestException(Throwable cause)
    {
        super(cause);
    }

    public BadRequestException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public BadRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
