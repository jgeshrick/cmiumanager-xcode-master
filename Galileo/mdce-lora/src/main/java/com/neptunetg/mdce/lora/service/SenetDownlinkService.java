/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.lora.service;

import com.neptunetg.common.data.miu.Eui;
import com.neptunetg.common.util.HexUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Class that can send messages to Senet
 */
@Service
public class SenetDownlinkService
{
    private static final Logger log = LoggerFactory.getLogger(SenetDownlinkService.class);

    private static final String DOWNLINK_URL_PATTERN_EUI_PLACEHOLDER = "{eui}";
    private static final String DOWNLINK_URL_PATTERN_PDU_PLACEHOLDER = "{pdu}";


    private final String downlinkUrlPattern;
    private final String apikeyHeaderKey;
    private final String apikeyHeaderValue;

    @Autowired
    public SenetDownlinkService(
            @Value("#{envProperties['lora.senet.downlink.url.pattern']}") String downlinkUrlPattern,
            @Value("#{envProperties['lora.senet.downlink.header.apikey'] }") String apikeyHeader
    )
    {
        this.downlinkUrlPattern = downlinkUrlPattern;

        if (StringUtils.hasText(apikeyHeader))
        {
            final int apikeyHeaderEqualsPos = apikeyHeader.indexOf('=');

            if (apikeyHeaderEqualsPos < 0)
            {
                this.apikeyHeaderKey = apikeyHeader;
                this.apikeyHeaderValue = "";
            }
            else
            {
                this.apikeyHeaderKey = apikeyHeader.substring(0, apikeyHeaderEqualsPos);
                this.apikeyHeaderValue = apikeyHeader.substring(apikeyHeaderEqualsPos + 1);
            }
        }
        else
        {
            this.apikeyHeaderKey = null;
            this.apikeyHeaderValue = null;
        }
    }

    public void sendMessageToSenetDevice(Eui eui, byte[] pduBytes) throws IOException
    {
        log.debug("Sending PDU to L900 with ID: " + eui.extractMiuId().toString() +
                " PDU: " + HexUtils.byteArrayToHex(pduBytes));

        final URL senetUrl = createDownlinkUrl(eui, pduBytes);
        final HttpURLConnection con = (HttpURLConnection) senetUrl.openConnection();

        try
        {
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setRequestProperty("User-Agent", "Neptune-MDCE");
            con.setFixedLengthStreamingMode(0L); //empty POST.  Sets Content-Length header.

            if (apikeyHeaderKey != null)
            {
                con.setRequestProperty(apikeyHeaderKey, apikeyHeaderValue);
            }
            int responseCode = con.getResponseCode();

            if (responseCode >= 200 && responseCode < 300)
            {
                return; //all done, empty response
            }
            else
            {
                throw new IOException("Failure response code from Senet (" + responseCode + ") when sending message for EUI " + eui + "; URL was " + senetUrl);
            }
        }
        catch (Exception e)
        {
            throw new IOException("Error while attempting to send message for EUI " + eui + "; URL was " + senetUrl, e);
        }
        finally
        {
            con.disconnect();
        }
    }


    private URL createDownlinkUrl(Eui eui, byte[] pduBytes) throws MalformedURLException
    {
        final String urlWithEui = checkPopulateUrl(this.downlinkUrlPattern, DOWNLINK_URL_PATTERN_EUI_PLACEHOLDER, eui);
        final String urlWithEuiAndPdu = checkPopulateUrl(urlWithEui, DOWNLINK_URL_PATTERN_PDU_PLACEHOLDER, HexUtils.byteArrayToHex(pduBytes));

        final URL ret = new URL(urlWithEuiAndPdu);
        return ret;
    }

    private String checkPopulateUrl(String initialUrl, String placeholder, Object value)
    {
        final String ret = initialUrl.replace(placeholder, value.toString());

        if (ret.equals(initialUrl))
        {
            throw new Error("Bad configuration: configured Senet downlink URL pattern \"" + this.downlinkUrlPattern + "\" should contain placeholder " + placeholder);
        }
        return ret;
    }
}
