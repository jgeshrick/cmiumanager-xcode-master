-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mdce
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mdce` ;

-- -----------------------------------------------------
-- Schema mdce
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mdce` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `mdce` ;

-- -----------------------------------------------------
-- Table `site_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `site_list` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `site_list` (
  `site_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(100) NULL,
  `last_data_request` DATETIME NULL,
  PRIMARY KEY (`site_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `miu_details`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `miu_details` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `miu_details` (
  `miu_id` INT UNSIGNED NOT NULL,
  `site_id` INT UNSIGNED NULL,
  `miu_type` INT NULL,
  `last_insert_date` DATETIME NULL,
  `first_insert_date` DATETIME NULL,
  `last_heard_time` DATETIME NULL,
  `next_sync_time` DATETIME NULL,
  `next_sync_time_warning` DATETIME NULL,
  `next_sync_time_error` DATETIME NULL,
  `meter_active` VARCHAR(1) NULL DEFAULT 'N',
  PRIMARY KEY (`miu_id`),
  INDEX `fk_miu_details_utilities1_idx` (`site_id` ASC),
  INDEX `i_miu_details_next_sync` (`next_sync_time` ASC),
  CONSTRAINT `fk_miu_details_utilities1`
    FOREIGN KEY (`site_id`)
    REFERENCES `site_list` (`site_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `tag_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tag_list` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tag_list` (
  `tag_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL,
  `public` TINYINT(1) NULL,
  PRIMARY KEY (`tag_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `miu_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `miu_tags` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `miu_tags` (
  `miu_id` INT UNSIGNED NOT NULL,
  `tag_id` INT UNSIGNED NOT NULL,
  INDEX `fk_miu_tags_miu_details_idx` (`miu_id` ASC),
  INDEX `fk_miu_tags_tag_list_idx` (`tag_id` ASC),
  PRIMARY KEY (`tag_id`, `miu_id`),
  CONSTRAINT `fk_miu_tags_miu_details`
    FOREIGN KEY (`miu_id`)
    REFERENCES `miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_miu_tags_tag_list`
    FOREIGN KEY (`tag_id`)
    REFERENCES `tag_list` (`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `site_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `site_tags` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `site_tags` (
  `site_id` INT UNSIGNED NOT NULL,
  `tag_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`site_id`, `tag_id`),
  INDEX `fk_utility_tags_tag_list1_idx` (`tag_id` ASC),
  CONSTRAINT `fk_utility_tags_tag_list1`
    FOREIGN KEY (`tag_id`)
    REFERENCES `tag_list` (`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_utility_tags_utilities1`
    FOREIGN KEY (`site_id`)
    REFERENCES `site_list` (`site_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `user_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_list` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `user_list` (
  `user_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(45) NULL,
  `user_email` VARCHAR(45) NULL,
  `user_password` BLOB NULL,
  `user_salt` BLOB NULL,
  `user_pbkdf2_inter` INT NULL,
  `user_level` ENUM('SiteOperator', 'MsocOperator', 'MsocAdmin', 'Distributor') NULL,
  `user_alert_system_warning` TINYINT(1) NULL,
  `user_alert_system_error` TINYINT(1) NULL,
  `user_alert_cmiu_warning` TINYINT(1) NULL,
  `user_alert_cmiu_error` TINYINT(1) NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `user_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_tags` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `user_tags` (
  `user_id` INT UNSIGNED NOT NULL,
  `tag_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `tag_id`),
  INDEX `fk_user_tags_tag_list_idx` (`tag_id` ASC),
  CONSTRAINT `fk_user_tags_user_list`
    FOREIGN KEY (`user_id`)
    REFERENCES `user_list` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_tags_tag_list`
    FOREIGN KEY (`tag_id`)
    REFERENCES `tag_list` (`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `kpi_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kpi_list` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `kpi_list` (
  `kpi_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(45) NULL,
  `params` MEDIUMTEXT NULL,
  `config` MEDIUMTEXT NULL,
  `kpi_sql` MEDIUMTEXT NULL,
  `grid_config` MEDIUMTEXT NULL,
  `grid_sql` VARCHAR(45) NULL,
  `details_grid_config` MEDIUMTEXT NULL,
  `details_grid_sql` MEDIUMTEXT NULL,
  `last_update_sql` MEDIUMTEXT NULL,
  PRIMARY KEY (`kpi_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `user_dashboards`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_dashboards` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `user_dashboards` (
  `user_dashboard_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `view_name` VARCHAR(45) NULL,
  `public` TINYINT(1) NULL,
  PRIMARY KEY (`user_dashboard_id`, `user_id`),
  INDEX `fk_user_dashboards_user_list_idx` (`user_id` ASC),
  CONSTRAINT `fk_user_dashboards_user_list0`
    FOREIGN KEY (`user_id`)
    REFERENCES `user_list` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `user_dashboards_kpis`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_dashboards_kpis` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `user_dashboards_kpis` (
  `user_dashboards_kpis_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_dashboard_id` INT UNSIGNED NOT NULL,
  `kpi_id` INT UNSIGNED NOT NULL,
  `grid_x` INT NULL,
  `grid_y` INT NULL,
  `colspan` INT NULL,
  `rowspan` INT NULL,
  `params_override` MEDIUMTEXT NULL,
  PRIMARY KEY (`user_dashboards_kpis_id`),
  INDEX `fk_user_dashboards_dashboard_list_idx` (`kpi_id` ASC),
  INDEX `fk_user_views_dashboards_user_views1_idx` (`user_dashboard_id` ASC),
  CONSTRAINT `fk_user_dashboards_dashboard_list`
    FOREIGN KEY (`kpi_id`)
    REFERENCES `kpi_list` (`kpi_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_views_dashboards_user_views1`
    FOREIGN KEY (`user_dashboard_id`)
    REFERENCES `user_dashboards` (`user_dashboard_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `user_dashboards_kpis_default_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_dashboards_kpis_default_tags` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `user_dashboards_kpis_default_tags` (
  `user_dashboards_kpis_id` INT UNSIGNED NOT NULL,
  `tag_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`user_dashboards_kpis_id`, `tag_id`),
  INDEX `fk_user_dashboards_default_tags_tag_list_idx` (`tag_id` ASC),
  CONSTRAINT `fk_user_dashboards_default_tags_user_views_dashboards`
    FOREIGN KEY (`user_dashboards_kpis_id`)
    REFERENCES `user_dashboards_kpis` (`user_dashboards_kpis_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_views_dashboards_default_tags_tag_list`
    FOREIGN KEY (`tag_id`)
    REFERENCES `tag_list` (`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `event_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `event_list` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `event_list` (
  `event_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(45) NULL,
  `start_date_time` DATETIME NULL,
  `interval_type` VARCHAR(1) NULL,
  `interval_value` INT NULL,
  `event_sql` MEDIUMTEXT NULL,
  `last_run_time` DATETIME NULL,
  `next_run_time` DATETIME NULL,
  PRIMARY KEY (`event_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `event_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `event_data` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `event_data` (
  `event_data_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `event_id` INT UNSIGNED NOT NULL,
  `n1` DECIMAL(10,0) NULL,
  `n2` DECIMAL(10,0) NULL,
  `d1` DATE NULL,
  `d2` DATE NULL,
  `dt1` DATETIME NULL,
  `dt2` DATETIME NULL,
  `s1` VARCHAR(100) NULL,
  `s2` VARCHAR(100) NULL,
  `i1` INT NULL,
  `i2` INT NULL,
  `u1` INT UNSIGNED NULL,
  `u2` INT NULL,
  INDEX `fk_event_data_event_list1_idx` (`event_id` ASC),
  PRIMARY KEY (`event_data_id`),
  CONSTRAINT `fk_event_data_event_list1`
    FOREIGN KEY (`event_id`)
    REFERENCES `event_list` (`event_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `miu_data_usage`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `miu_data_usage` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `miu_data_usage` (
  `miu_id` INT UNSIGNED NOT NULL,
  `data_usage_date` DATE NOT NULL,
  `bytes_used_today` INT NULL,
  `bytes_used_month_to_date` INT NULL,
  PRIMARY KEY (`miu_id`, `data_usage_date`),
  CONSTRAINT `fk_miu_data_usage_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `miu_commands`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `miu_commands` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `miu_commands` (
  `miu_command_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `miu_id` INT UNSIGNED NOT NULL,
  `command_type` INT NULL,
  `command_params` BLOB NULL,
  `command_entered_time` DATETIME NULL COMMENT 'Time when command has been send to broker',
  `command_fulfilled_time` DATETIME NULL COMMENT 'Time when command has been acked by CMIU',
  `status` ENUM('created', 'queued', 'committed', 'recalled', 'cancelled','rejected') NULL COMMENT 'Status of this command item:\nCREATED --> QUEUED --> COMMITTED\n(QUEUED) -> RECALLED -> CANCELLED',
  `source_device_type` VARCHAR(45) NULL,
  `target_device_type` VARCHAR(45) NULL,
  PRIMARY KEY (`miu_command_id`, `miu_id`),
  INDEX `fk_miu_commands_miu_details1_idx` (`miu_id` ASC),
  CONSTRAINT `fk_miu_commands_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `audit_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `audit_list` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `audit_list` (
  `audit_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `audit_type` INT NULL,
  `audit_params` INT NULL,
  `audit_user_name` VARCHAR(45) NULL,
  `audit_time` DATETIME NULL,
  `miu_id` INT UNSIGNED NULL,
  `old_values` VARCHAR(1024) NULL,
  `new_values` VARCHAR(1024) NULL,
  PRIMARY KEY (`audit_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `miu_heard_packets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `miu_heard_packets` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `miu_heard_packets` (
  `miu_heard_packets_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `miu_id` INT UNSIGNED NOT NULL,
  `heard_time` DATETIME NOT NULL,
  `rssi` DECIMAL(6,2) NULL,
  `ber` DECIMAL(3,1) NULL,
  `processor_reset_counter` INT NULL,
  `register_time` INT NULL,
  `register_time_to_activate_context` INT NULL,
  `register_time_connected` INT NULL,
  `register_time_to_transfer_packet` INT NULL,
  `disconnect_time` INT NULL,
  `mag_swipe_counter` INT NULL,
  `battery_capacity` DECIMAL(6,2) NULL,
  PRIMARY KEY (`miu_heard_packets_id`, `miu_id`),
  INDEX `fk_miu_heard_packets_miu_details1_idx` (`miu_id` ASC),
  INDEX `i_miu_heard_packets_time` (`heard_time` ASC),
  CONSTRAINT `fk_miu_heard_packets_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `user_public_dashboards`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_public_dashboards` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `user_public_dashboards` (
  `user_id` INT UNSIGNED NOT NULL,
  `user_dashboard_id` INT UNSIGNED NOT NULL,
  `dashboard_order` INT NULL,
  PRIMARY KEY (`user_id`, `user_dashboard_id`),
  INDEX `fk_user_public_dashboards_user_dashboards_idx` (`user_dashboard_id` ASC),
  CONSTRAINT `fk_user_public_dashboards_user_list`
    FOREIGN KEY (`user_id`)
    REFERENCES `user_list` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_public_dashboards_user_dashboards`
    FOREIGN KEY (`user_dashboard_id`)
    REFERENCES `user_dashboards` (`user_dashboard_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cmiu_config_set`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cmiu_config_set` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cmiu_config_set` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  `billing_plan_id` INT NOT NULL,
  `reporting_start_mins` INT NULL,
  `reporting_interval_mins` INT NULL,
  `reporting_number_of_retries` INT NULL,
  `reporting_transmit_windows_mins` INT NULL,
  `reporting_quiet_start_mins` INT NULL,
  `reporting_quiet_end_mins` INT NULL,
  `reporting_sync_warning_mins` INT NULL,
  `reporting_sync_error_mins` INT NULL,
  `recording_start_time_mins` INT NULL,
  `recording_interval_mins` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cmiu_config_mgmt`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cmiu_config_mgmt` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cmiu_config_mgmt` (
  `miu_id` INT UNSIGNED NOT NULL,
  `current_config` INT UNSIGNED NOT NULL,
  `current_config_apply_date` DATETIME NULL,
  `default_config` INT UNSIGNED NOT NULL,
  `planned_config` INT UNSIGNED NULL,
  `planned_config_apply_date` DATETIME NULL,
  `planned_config_revert_date` DATETIME NULL,
  `reported_config` INT UNSIGNED NOT NULL,
  `reported_config_date` DATETIME NULL,
  INDEX `fk_cmiu_configuration_management_miu_details1_idx` (`miu_id` ASC),
  INDEX `fk_cmiu_configuration_management_cmiu_configuration_set1_idx` (`current_config` ASC),
  INDEX `fk_cmiu_configuration_management_cmiu_configuration_set2_idx` (`default_config` ASC),
  INDEX `fk_cmiu_configuration_management_cmiu_configuration_set3_idx` (`planned_config` ASC),
  INDEX `fk_cmiu_configuration_management_cmiu_configuration_set4_idx` (`reported_config` ASC),
  CONSTRAINT `fk_cmiu_configuration_management_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cmiu_configuration_management_cmiu_configuration_set1`
    FOREIGN KEY (`current_config`)
    REFERENCES `cmiu_config_set` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cmiu_configuration_management_cmiu_configuration_set2`
    FOREIGN KEY (`default_config`)
    REFERENCES `cmiu_config_set` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cmiu_configuration_management_cmiu_configuration_set3`
    FOREIGN KEY (`planned_config`)
    REFERENCES `cmiu_config_set` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cmiu_configuration_management_cmiu_configuration_set4`
    FOREIGN KEY (`reported_config`)
    REFERENCES `cmiu_config_set` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cmiu_config_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cmiu_config_history` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cmiu_config_history` (
  `miu_id` INT UNSIGNED NOT NULL,
  `cmiu_config` INT UNSIGNED NOT NULL,
  `timestamp` DATETIME NOT NULL,
  `received_from_cmiu` TINYINT(1) NULL COMMENT 'This config has been implemented by the CMIU. Marked TRUE if the CMIU config received over MQTT matches.',
  `change_note` VARCHAR(200) NULL COMMENT 'Capture user input notes from mdce-web CMIU config',
  INDEX `fk_cmiu_config_history_cmiu_config_set1_idx` (`cmiu_config` ASC),
  INDEX `fk_cmiu_config_history_miu_details1_idx` (`miu_id` ASC),
  CONSTRAINT `fk_cmiu_config_history_cmiu_config_set1`
    FOREIGN KEY (`cmiu_config`)
    REFERENCES `cmiu_config_set` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cmiu_config_history_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cns_credentials`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cns_credentials` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cns_credentials` (
  `cns_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `environment` VARCHAR(45) NOT NULL,
  `is_active` TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'set to 0 if the password has been locked and pending action from VzW support',
  `is_primary` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'The primary password the environment should use',
  `last_password_changed_time` DATETIME NULL COMMENT 'The last time when the password is change. This is used to keep track of password rotation.',
  `is_callback_registered` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Check whether callback has been registered to this account.',
  PRIMARY KEY (`cns_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cellular_device_details`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cellular_device_details` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cellular_device_details` (
  `cellular_device_details_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `miu_id` INT UNSIGNED NOT NULL,
  `iccid` VARCHAR(20) NULL,
  `imei` VARCHAR(16) NULL,
  `msisdn` VARCHAR(20) NULL COMMENT 'MSISDN seems to be the only identifier field when callback is sent on changing device provisioning state',
  `imsi` VARCHAR(16) NULL,
  `provisioning_state` TINYINT(2) NULL COMMENT 'provisioning state of the cellular device:\n- unregistered (not detected in VzW network UWS account yet)\n- pre active\n- activated\n- suspended\n- deactivated\n- pending-activate\n- pending-suspend\n- pending-restore\n- pending-deactivate',
  `last_update_time` DATETIME NULL COMMENT 'Datetime when this record has been updated',
  `modem_config_vendor` VARCHAR(45) NULL,
  `modem_config_firmware` VARCHAR(45) NULL,
  PRIMARY KEY (`cellular_device_details_id`),
  INDEX `fk_cellular_device_details_miu_details1_idx` (`miu_id` ASC),
  CONSTRAINT `fk_cellular_device_details_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `miu_monthly_stats`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `miu_monthly_stats` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `miu_monthly_stats` (
  `miu_id` INT UNSIGNED NOT NULL,
  `year` INT NOT NULL,
  `month` INT NOT NULL,
  `num_expected` INT NULL,
  `num_heard` INT NULL,
  `avg_rssi` DECIMAL(6,2) NULL,
  `min_rssi` INT NULL,
  `max_rssi` INT NULL,
  `avg_ber` DECIMAL(3,3) NULL,
  `min_ber` DECIMAL(3,1) NULL,
  `max_ber` DECIMAL(3,1) NULL,
  `avg_processor_reset_counter` DECIMAL(6,2) NULL,
  `min_processor_reset_counter` INT NULL,
  `max_processor_reset_counter` INT NULL,
  `avg_register_time` DECIMAL(6,2) NULL,
  `min_register_time` INT NULL,
  `max_register_time` INT NULL,
  `avg_register_time_to_activate_context` DECIMAL(6,2) NULL,
  `min_register_time_to_activate_context` INT NULL,
  `max_register_time_to_activate_context` INT NULL,
  `avg_register_time_connected` DECIMAL(6,2) NULL,
  `min_register_time_connected` INT NULL,
  `max_register_time_connected` INT NULL,
  `avg_register_time_to_transfer_packet` DECIMAL(6,2) NULL,
  `min_register_time_to_transfer_packet` INT NULL,
  `max_register_time_to_transfer_packet` INT NULL,
  `avg_disconnect_time` DECIMAL(6,2) NULL,
  `min_disconnect_time` INT NULL,
  `max_disconnect_time` INT NULL,
  `avg_mag_swipe_counter` DECIMAL(6,2) NULL,
  `min_mag_swipe_counter` INT NULL,
  `max_mag_swipe_counter` INT NULL,
  `avg_battery_capacity` DECIMAL(6,2) NULL,
  `min_battery_capacity` DECIMAL(6,2) NULL,
  `max_battery_capacity` DECIMAL(6,2) NULL,
  PRIMARY KEY (`miu_id`, `year`, `month`),
  INDEX `i_miu_monthly_stats_date` (`year` ASC, `month` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `miu_daily_stats`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `miu_daily_stats` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `miu_daily_stats` (
  `miu_id` INT UNSIGNED NOT NULL,
  `heard_date` DATE NOT NULL,
  `num_expected` INT NULL,
  `num_heard` INT NULL,
  `avg_rssi` DECIMAL(6,3) NULL,
  `min_rssi` DECIMAL(6,2) NULL,
  `max_rssi` DECIMAL(6,2) NULL,
  `avg_ber` DECIMAL(3,3) NULL,
  `min_ber` DECIMAL(3,1) NULL,
  `max_ber` DECIMAL(3,1) NULL,
  `avg_processor_reset_counter` DECIMAL(6,2) NULL,
  `min_processor_reset_counter` INT NULL,
  `max_processor_reset_counter` INT NULL,
  `avg_register_time` DECIMAL(6,2) NULL,
  `min_register_time` INT NULL,
  `max_register_time` INT NULL,
  `avg_register_time_to_activate_context` DECIMAL(6,2) NULL,
  `min_register_time_to_activate_context` INT NULL,
  `max_register_time_to_activate_context` INT NULL,
  `avg_register_time_connected` DECIMAL(6,2) NULL,
  `min_register_time_connected` INT NULL,
  `max_register_time_connected` INT NULL,
  `avg_register_time_to_transfer_packet` DECIMAL(6,2) NULL,
  `min_register_time_to_transfer_packet` INT NULL,
  `max_register_time_to_transfer_packet` INT NULL,
  `avg_disconnect_time` DECIMAL(6,2) NULL,
  `min_disconnect_time` INT NULL,
  `max_disconnect_time` INT NULL,
  `avg_mag_swipe_counter` DECIMAL(6,2) NULL,
  `min_mag_swipe_counter` INT NULL,
  `max_mag_swipe_counter` INT NULL,
  `avg_battery_capacity` DECIMAL(6,2) NULL,
  `min_battery_capacity` DECIMAL(6,2) NULL,
  `max_battery_capacity` DECIMAL(6,2) NULL,
  PRIMARY KEY (`miu_id`, `heard_date`),
  INDEX `i_miu_daily_stats` (`heard_date` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cmiu_registered_images`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cmiu_registered_images` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cmiu_registered_images` (
  `cmiu_image_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `s3_path` VARCHAR(255) NULL COMMENT 's3 source path/URL for image, excluding the https://s3..../bucketname/',
  `register_date` DATETIME NULL,
  `type` INT NULL,
  `image_size` INT NULL,
  `start_address` INT NULL,
  PRIMARY KEY (`cmiu_image_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mdce_api_tokens`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce_api_tokens` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mdce_api_tokens` (
  `token` BINARY(8) NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `partner_key` BINARY(16) NOT NULL,
  `originating_ip` VARCHAR(45) NOT NULL,
  `expiry` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `site_id` INT NULL,
  PRIMARY KEY (`token`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gateway_details`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gateway_details` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gateway_details` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `gateway_id` VARCHAR(45) NOT NULL,
  `site_id` INT UNSIGNED NULL,
  `last_insert_date` DATETIME NULL,
  `first_insert_date` DATETIME NULL,
  `last_heard_time` DATETIME NULL,
  `next_sync_time` DATETIME NULL,
  `next_sync_time_warning` DATETIME NULL,
  `next_sync_time_error` DATETIME NULL,
  `gateway_active` CHAR NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  INDEX `fk_gateway_details_site_list1_idx` (`site_id` ASC),
  CONSTRAINT `fk_gateway_details_site_list1`
    FOREIGN KEY (`site_id`)
    REFERENCES `site_list` (`site_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `ref_data_info_contact`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ref_data_info_contact` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ref_data_info_contact` (
  `ref_data_info_contact_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `contact_name` VARCHAR(45) NULL,
  `work_phone` VARCHAR(20) NULL,
  `title` VARCHAR(80) NULL,
  PRIMARY KEY (`ref_data_info_contact_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `ref_data_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ref_data_info` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ref_data_info` (
  `ref_data_info_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` VARCHAR(20) NULL,
  `system_id` VARCHAR(20) NULL,
  `account_name` VARCHAR(100) NULL,
  `address1` VARCHAR(45) NULL,
  `address2` VARCHAR(45) NULL,
  `address3` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `state` VARCHAR(45) NULL,
  `postal_code` VARCHAR(45) NULL,
  `country` VARCHAR(45) NULL,
  `main_phone` VARCHAR(45) NULL,
  `fax` VARCHAR(45) NULL,
  `sales_territory_owner` VARCHAR(45) NULL,
  `account_manager` VARCHAR(45) NULL,
  `regional_manager` VARCHAR(45) NULL,
  `type` VARCHAR(45) NULL,
  `sub_type` VARCHAR(45) NULL,
  `status` VARCHAR(45) NULL,
  `primary_contact_id` INT UNSIGNED NULL,
  `parent_customer_number` VARCHAR(45) NULL,
  `customer_number` VARCHAR(45) NULL,
  PRIMARY KEY (`ref_data_info_id`),
  INDEX `ref_data_info_contact_ fk_idx` (`primary_contact_id` ASC),
  CONSTRAINT `ref_data_info_contact_fk`
    FOREIGN KEY (`primary_contact_id`)
    REFERENCES `ref_data_info_contact` (`ref_data_info_contact_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `ref_data_utilities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ref_data_utilities` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ref_data_utilities` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_id` INT UNSIGNED NOT NULL,
  `info_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `ref_data_utilties_info_id_fk_idx` (`info_id` ASC),
  CONSTRAINT `ref_data_utilties_info_id_fk`
    FOREIGN KEY (`info_id`)
    REFERENCES `ref_data_info` (`ref_data_info_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `ref_data_secondary_contact_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ref_data_secondary_contact_info` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ref_data_secondary_contact_info` (
  `info_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `contact_id` INT UNSIGNED NOT NULL,
  INDEX `ref_data_info_pk_fk_idx` (`info_id` ASC),
  INDEX `ref_data_info_contact_pk_fk_idx` (`contact_id` ASC),
  CONSTRAINT `ref_data_info_pk_fk`
    FOREIGN KEY (`info_id`)
    REFERENCES `ref_data_info` (`ref_data_info_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ref_data_info_contact_pk_fk`
    FOREIGN KEY (`contact_id`)
    REFERENCES `ref_data_info_contact` (`ref_data_info_contact_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `ref_data_distributer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ref_data_distributer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ref_data_distributer` (
  `ref_data_distributer_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_number` VARCHAR(45) NULL,
  `info_id` INT UNSIGNED NULL,
  PRIMARY KEY (`ref_data_distributer_id`),
  INDEX `ref_data_info_id_idx` (`info_id` ASC),
  CONSTRAINT `ref_data_info_id`
    FOREIGN KEY (`info_id`)
    REFERENCES `ref_data_info` (`ref_data_info_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = big5;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `ref_data_customer_number`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ref_data_customer_number` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ref_data_customer_number` (
  `customer_number_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `info_id` INT UNSIGNED NOT NULL,
  `customer_number` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`customer_number_id`),
  INDEX `customer_number_info_fk_idx` (`info_id` ASC),
  CONSTRAINT `customer_number_info_fk`
    FOREIGN KEY (`info_id`)
    REFERENCES `ref_data_info` (`ref_data_info_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `site_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `site_user` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `site_user` (
  `site_list_site_id` INT UNSIGNED NOT NULL,
  `user_list_user_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`site_list_site_id`, `user_list_user_id`),
  INDEX `fk_site_list_has_user_list_user_list1_idx` (`user_list_user_id` ASC),
  INDEX `fk_site_list_has_user_list_site_list1_idx` (`site_list_site_id` ASC),
  CONSTRAINT `fk_site_list_has_user_list_site_list1`
    FOREIGN KEY (`site_list_site_id`)
    REFERENCES `site_list` (`site_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_site_list_has_user_list_user_list1`
    FOREIGN KEY (`user_list_user_id`)
    REFERENCES `user_list` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `alert_alerts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `alert_alerts` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `alert_alerts` (
  `alert_alerts_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `alert_source` VARCHAR(250) NOT NULL,
  `alert_creation_date` DATETIME NOT NULL,
  `alert_state` ENUM('new', 'handled', 'stale', 'cleared') NOT NULL,
  `alert_level` ENUM('warning', 'error') NOT NULL,
  `alert_ticket_id` VARCHAR(45) NULL,
  `alert_latest_message` VARCHAR(1000) NULL,
  `alert_last_update_date` DATETIME NULL,
  PRIMARY KEY (`alert_alerts_id`),
  UNIQUE INDEX `alert_source_UNIQUE` (`alert_source` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `alert_log`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `alert_log` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `alert_log` (
  `alert_log_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `alert_log_date` DATETIME NOT NULL,
  `alert_log_message` VARCHAR(1000) NOT NULL,
  `alert_alerts_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`alert_log_id`),
  INDEX `fk_alert_log_alert_alerts1_idx` (`alert_alerts_id` ASC),
  CONSTRAINT `fk_alert_log_alert_alerts1`
    FOREIGN KEY (`alert_alerts_id`)
    REFERENCES `alert_alerts` (`alert_alerts_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gateway_config`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gateway_config` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gateway_config` (
  `id` INT NOT NULL,
  `gateway_db_id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `site_id` INT NULL,
  `MinimumStorageTime` INT NULL,
  `TransferInterval` INT NULL,
  `TransferOffsetTime` INT NULL,
  `FirstReportingStartTime` INT NULL,
  `FirstReportingEndTime` INT NULL,
  `FirstReportingTimeInterval` INT NULL,
  `SecondReportingStartTime` INT NULL,
  `SecondReportingEndTime` INT NULL,
  `SecondReportingTimeInterval` INT NULL,
  `ThirdReportingStartTime` INT NULL,
  `ThirdReportingEndTime` INT NULL,
  `ThirdReportingTimeInterval` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `gateway_db_id_UNIQUE` (`gateway_db_id` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gateway_heard_packets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gateway_heard_packets` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gateway_heard_packets` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `gateway_db_id` INT NOT NULL,
  `heard_time` DATETIME NOT NULL,
  `mius_heard` INT NOT NULL,
  `ook_packets_heard` INT NOT NULL,
  `fsk_packets_heard` INT NOT NULL,
  `config_packets_heard` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gateway_monthly_stats`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gateway_monthly_stats` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gateway_monthly_stats` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `gateway_db_id` INT NOT NULL,
  `num_expected` INT NOT NULL,
  `num_heard` INT NOT NULL,
  `avg_mius_heard` DECIMAL(6,2) NULL,
  `min_mius_heard` INT NOT NULL,
  `max_mius_heard` INT NULL,
  `avg_ook_packets_heard` DECIMAL(6,2) NULL,
  `min_ook_packets_heard` INT NOT NULL,
  `max_ook_packets_heard` INT NULL,
  `avg_fsk_packets_heard` DECIMAL(6,2) NULL,
  `min_fsk_packets_heard` INT NOT NULL,
  `max_fsk_packets_heard` INT NULL,
  `avg_config_packets_heard` DECIMAL(6,2) NULL,
  `min_config_packets_heard` INT NOT NULL,
  `max_config_packets_heard` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;
USE `mdce` ;

-- -----------------------------------------------------
-- procedure split_list
-- -----------------------------------------------------

USE `mdce`;
DROP procedure IF EXISTS `split_list`;
SHOW WARNINGS;

DELIMITER $$
USE `mdce`$$
create procedure split_list (list_string long varchar, string_delimiter varchar(255))
begin
    DECLARE iRow INT;
    DECLARE iPos INT;
    DECLARE iPos2 INT;

    DROP TEMPORARY TABLE IF EXISTS temp_split_list;
    CREATE TEMPORARY TABLE temp_split_list
                          (row_number INT,
                           row_value VARCHAR(4000)
                          ) ENGINE=Memory;
                          
	SET iRow  = 0;
    SET iPos  = 1;
    SET iPos2 = Locate(string_delimiter, list_string, iPos);
    
    WHILE iPos2 > 0 DO
    
        SET iRow = iRow + 1;
        INSERT INTO temp_split_list (row_number,
                                     row_value)
					         VALUES (iRow,
                                     substring(list_string, iPos, iPos2 - iPos));
                                 
		SET iPos = iPos2 + length(string_delimiter);
        SET iPos2 = Locate(string_delimiter, list_string, iPos);
    END WHILE;
    
    /*
     * add any text left over
     */
	IF iPos <= length(list_string) THEN
        SET iRow = iRow + 1;
        INSERT INTO temp_split_list (row_number,
                                     row_value)
					         VALUES (iRow,
                                     substr(substring(list_string, iPos), 1, 4000));
	END IF;
    
end;$$

DELIMITER ;
SHOW WARNINGS;

-- -----------------------------------------------------
-- procedure build_tag_filter
-- -----------------------------------------------------

USE `mdce`;
DROP procedure IF EXISTS `build_tag_filter`;
SHOW WARNINGS;

DELIMITER $$
USE `mdce`$$
create procedure build_tag_filter (tag_list long varchar)
begin

    /*
     * build table to store results
     */
	DROP TEMPORARY TABLE IF EXISTS temp_tag_mius;
    CREATE TEMPORARY TABLE temp_tag_mius
                          (miu_id INT UNSIGNED,
                           PRIMARY KEY (miu_id)
                          ) ENGINE=Memory;
                          
	/*
     * if we have a tag list, get the distinct MIU's in it
     */
	IF length(tag_list) > 0 THEN
		/*
		 * split the list
		 */
		CALL split_list(tag_list, ',');
		
		/*
		 * now insert any miu's for an utilities that have this tag
		 */
		INSERT INTO temp_tag_mius
				   (miu_id)
				   (SELECT DISTINCT m.miu_id
					  FROM miu_details m,
						   utility_tags t,
                           tag_list tl,
						   temp_split_list l
					 WHERE m.utility_id = t.utility_id AND
						   t.tag_id = tl.tag_id AND
                           tl.name = l.row_value AND
                           m.meter_active = 'Y');
   
		/*
		 * now insert any miu's that have this tag for themselves
		 */
		INSERT IGNORE INTO temp_tag_mius
						  (miu_id)
						  (SELECT DISTINCT m.miu_id
							 FROM miu_details m,
								  miu_tags t,
                                  tag_list tl,
								  temp_split_list l
							WHERE m.miu_id = t.miu_id AND
						          t.tag_id = tl.tag_id AND
                                  tl.name = l.row_value AND
                                   m.meter_active = 'Y');
   ELSE
      /*
       * no tag list, so add all MIU's
       */
		INSERT INTO temp_tag_mius
				   (miu_id)
				   (SELECT miu_id
					  FROM miu_details m 
				     WHERE m.meter_active = 'Y');
	END IF;						
									
end;$$

DELIMITER ;
SHOW WARNINGS;

-- -----------------------------------------------------
-- procedure split_list_int
-- -----------------------------------------------------

USE `mdce`;
DROP procedure IF EXISTS `split_list_int`;
SHOW WARNINGS;

DELIMITER $$
USE `mdce`$$
create procedure split_list_int (list_string long varchar, string_delimiter varchar(255))
begin
    DECLARE iRow INT;
    DECLARE iPos INT;
    DECLARE iPos2 INT;

    DROP TEMPORARY TABLE IF EXISTS temp_split_list;
    CREATE TEMPORARY TABLE temp_split_list
                          (row_number INT,
                           row_value INT
                          ) ENGINE=Memory;
                          
	SET iRow  = 0;
    SET iPos  = 1;
    SET iPos2 = Locate(string_delimiter, list_string, iPos);
    
    WHILE iPos2 > 0 DO
    
        SET iRow = iRow + 1;
        INSERT INTO temp_split_list (row_number,
                                     row_value)
					         VALUES (iRow,
                                     substring(list_string, iPos, iPos2 - iPos));
                                 
		SET iPos = iPos2 + length(string_delimiter);
        SET iPos2 = Locate(string_delimiter, list_string, iPos);
    END WHILE;
    
    /*
     * add any text left over
     */
	IF iPos <= length(list_string) THEN
        SET iRow = iRow + 1;
        INSERT INTO temp_split_list (row_number,
                                     row_value)
					         VALUES (iRow,
                                     substr(substring(list_string, iPos), 1, 4000));
	END IF;
    
end;$$

DELIMITER ;
SHOW WARNINGS;

-- -----------------------------------------------------
-- procedure split_list_uint
-- -----------------------------------------------------

USE `mdce`;
DROP procedure IF EXISTS `split_list_uint`;
SHOW WARNINGS;

DELIMITER $$
USE `mdce`$$
create procedure split_list_uint (list_string long varchar, string_delimiter varchar(255))
begin
    DECLARE iRow INT;
    DECLARE iPos INT;
    DECLARE iPos2 INT;

    DROP TEMPORARY TABLE IF EXISTS temp_split_list;
    CREATE TEMPORARY TABLE temp_split_list
                          (row_number INT UNSIGNED,
                           row_value INT
                          ) ENGINE=Memory;
                          
	SET iRow  = 0;
    SET iPos  = 1;
    SET iPos2 = Locate(string_delimiter, list_string, iPos);
    
    WHILE iPos2 > 0 DO
    
        SET iRow = iRow + 1;
        INSERT INTO temp_split_list (row_number,
                                     row_value)
					         VALUES (iRow,
                                     substring(list_string, iPos, iPos2 - iPos));
                                 
		SET iPos = iPos2 + length(string_delimiter);
        SET iPos2 = Locate(string_delimiter, list_string, iPos);
    END WHILE;
    
    /*
     * add any text left over
     */
	IF iPos <= length(list_string) THEN
        SET iRow = iRow + 1;
        INSERT INTO temp_split_list (row_number,
                                     row_value)
					         VALUES (iRow,
                                     substr(substring(list_string, iPos), 1, 4000));
	END IF;
    
end;
$$

DELIMITER ;
SHOW WARNINGS;

-- -----------------------------------------------------
-- function get_prev_bill_date
-- -----------------------------------------------------

USE `mdce`;
DROP function IF EXISTS `get_prev_bill_date`;
SHOW WARNINGS;

DELIMITER $$
USE `mdce`$$
CREATE FUNCTION get_prev_bill_date (dateCheck DATE, billingDay int)
   RETURNS date
   DETERMINISTIC
BEGIN
   DECLARE billDate DATE;
   
   SET billDate = adddate(adddate(dateCheck, -1 * dayofmonth(dateCheck)), billingDay);
   IF billDate <= dateCheck THEN
      RETURN billDate;
   END IF;
   
   RETURN billDate - interval 1 month;

END;
$$

DELIMITER ;
SHOW WARNINGS;

-- -----------------------------------------------------
-- function get_next_bill_date
-- -----------------------------------------------------

USE `mdce`;
DROP function IF EXISTS `get_next_bill_date`;
SHOW WARNINGS;

DELIMITER $$
USE `mdce`$$
CREATE FUNCTION get_next_bill_date (dateCheck DATE, billingDay int)
   RETURNS date
   DETERMINISTIC
BEGIN
   DECLARE billDate DATE;
   
   SET billDate = adddate(adddate(dateCheck, -1 * dayofmonth(dateCheck)), billingDay);
   IF billDate > dateCheck THEN
      RETURN billDate;
   END IF;
   
   RETURN billDate + interval 1 month;

END;
$$

DELIMITER ;
SHOW WARNINGS;

-- -----------------------------------------------------
-- procedure update_miu_stats
-- -----------------------------------------------------

USE `mdce`;
DROP procedure IF EXISTS `update_miu_stats`;
SHOW WARNINGS;

DELIMITER $$
USE `mdce`$$
CREATE PROCEDURE update_miu_stats(ats_date timestamp)
begin
   DECLARE ldate_month_start DATE;
   DECLARE ldate_month_end DATE;
   
   IF ats_date IS NULL THEN
      SET ats_date = current_date;
   END IF;
   
   /*
    * convert to chicago
    */
   SET ats_date = convert_tz(ats_date, 'UTC', 'America/Chicago');
   
   SET ldate_month_start = ats_date - interval day(ats_date) - 1 day;
   SET ldate_month_end   = ldate_month_start + interval 1 month;
   
   /*
    * if 1st hour of the day, update last 3 days
    */
   IF hour(ats_date) = 0  THEN
      CALL update_miu_daily_stats(ats_date - interval 1 day);
      CALL update_miu_daily_stats(ats_date - interval 2 day);
      CALL update_miu_daily_stats(ats_date - interval 3 day);

      /*
       * if 1st 3 days of the month, calculate previous month as well
       */
      IF day(ats_date) < 4 THEN
         CALL update_miu_monthly_stats(ldate_month_start - interval 1 month, ldate_month_start);

      END IF;                                   
   END IF;                            

   /*
    * update current day
    */
   CALL update_miu_daily_stats(ats_date);
   
   /*
    * now update current month
    */
   CALL update_miu_monthly_stats(ldate_month_start, ldate_month_end);    
   
end 
$$

DELIMITER ;
SHOW WARNINGS;

-- -----------------------------------------------------
-- procedure update_miu_daily_stats
-- -----------------------------------------------------

USE `mdce`;
DROP procedure IF EXISTS `update_miu_daily_stats`;
SHOW WARNINGS;

DELIMITER $$
USE `mdce`$$
CREATE PROCEDURE update_miu_daily_stats(adate_stats DATE)
begin
   DECLARE ldate_utc DATE;
   DECLARE li_mins INT;
   DECLARE li_mins_utc INT;
   DECLARE lbool_prev_date BOOLEAN;
   
   IF adate_stats IS NULL THEN
      SET adate_stats = convert_tz(current_date, 'UTC', 'America/Chicago');
   END IF;
   
   /*
    * convert to UTC
    */
   SET ldate_utc = convert_tz(adate_stats, 'America/Chicago', 'UTC');
   
   /*
    * calculate the mins different for UTC
    */
   SET li_mins_utc = timestampdiff(minute, current_timestamp, convert_tz(current_timestamp, 'UTC', 'America/Chicago'));
   
   /*
    * calculate minutes after midnight
    */
   SET li_mins = timestampdiff(minute, 
                               date(convert_tz(current_timestamp, 'UTC', 'America/Chicago')), 
                               convert_tz(current_timestamp, 'UTC', 'America/Chicago'));
                               
   IF adate_stats < current_date THEN                               
      SET lbool_prev_date = true;
   ELSE
      SET lbool_prev_date = false;
   END IF;

   INSERT INTO miu_daily_stats (miu_id, heard_date, 
        num_expected, num_heard, 
		avg_rssi, min_rssi, max_rssi, 
        avg_ber, min_ber, max_ber, 
        avg_processor_reset_counter, min_processor_reset_counter, max_processor_reset_counter,
        avg_register_time, min_register_time, max_register_time, 
        avg_register_time_to_activate_context, min_register_time_to_activate_context, max_register_time_to_activate_context, 
        avg_register_time_connected, min_register_time_connected, max_register_time_connected, 
        avg_register_time_to_transfer_packet, min_register_time_to_transfer_packet, max_register_time_to_transfer_packet, 
        avg_disconnect_time, min_disconnect_time, max_disconnect_time, 
        avg_mag_swipe_counter, min_mag_swipe_counter, max_mag_swipe_counter, 
        avg_battery_capacity, min_battery_capacity, max_battery_capacity) 
              (SELECT m.miu_id, adate_stats,
                      coalesce((
							 select 60*24 / reporting_interval_mins 
							 from cmiu_config_set ccs 
							 inner join cmiu_config_mgmt ccm 
							 on ccm.current_config = ccs.id
							 where ccm.miu_id = miu_id
						 ), 1),
                      count(p.miu_id),
                      avg(rssi), min(rssi), max(rssi),
                      avg(ber), min(ber), max(ber),
                      avg(processor_reset_counter), min(processor_reset_counter), max(processor_reset_counter),
                      avg(register_time), min(register_time), max(register_time),
                      avg(register_time_to_activate_context), min(register_time_to_activate_context), max(register_time_to_activate_context),
                      avg(register_time_connected), min(register_time_connected), max(register_time_connected),
                      avg(register_time_to_transfer_packet), min(register_time_to_transfer_packet), max(register_time_to_transfer_packet),
                      avg(disconnect_time), min(disconnect_time), max(disconnect_time),
                      avg(mag_swipe_counter), min(mag_swipe_counter), max(mag_swipe_counter),
                      avg(battery_capacity), min(battery_capacity), max(battery_capacity)       
                 FROM miu_details m
					     LEFT OUTER JOIN miu_heard_packets p ON m.miu_id = p.miu_id AND
                                                           heard_time >= adate_stats - INTERVAL li_mins_utc MINUTE AND 
                                                           heard_time < adate_stats - INTERVAL li_mins_utc MINUTE + INTERVAL 1 day
                WHERE m.meter_active = 'Y'                                           
                GROUP BY miu_id, adate_stats)
     ON DUPLICATE KEY UPDATE    
		num_expected = VALUES(num_expected), num_heard = VALUES(num_heard),
		avg_rssi = VALUES(avg_rssi), min_rssi = VALUES(min_rssi), max_rssi = VALUES(max_rssi),
		avg_ber = VALUES(avg_ber), min_ber = VALUES(min_ber), max_ber = VALUES(max_ber), 
		avg_processor_reset_counter = VALUES(avg_processor_reset_counter), min_processor_reset_counter = VALUES(min_processor_reset_counter), max_processor_reset_counter = VALUES(max_processor_reset_counter),
		avg_register_time = VALUES(avg_register_time), min_register_time = VALUES(min_register_time), max_register_time = VALUES(max_register_time), 
		avg_register_time_to_activate_context = VALUES(avg_register_time_to_activate_context), min_register_time_to_activate_context = VALUES(min_register_time_to_activate_context), max_register_time_to_activate_context = VALUES(max_register_time_to_activate_context), 
		avg_register_time_connected = VALUES(avg_register_time_connected), min_register_time_connected = VALUES(min_register_time_connected), max_register_time_connected = VALUES(max_register_time_connected), 
		avg_register_time_to_transfer_packet = VALUES(avg_register_time_to_transfer_packet), min_register_time_to_transfer_packet = VALUES(min_register_time_to_transfer_packet), max_register_time_to_transfer_packet = VALUES(max_register_time_to_transfer_packet), 
		avg_disconnect_time = VALUES(avg_disconnect_time), min_disconnect_time = VALUES(min_disconnect_time), max_disconnect_time = VALUES(max_disconnect_time), 
		avg_mag_swipe_counter = VALUES(avg_mag_swipe_counter), min_mag_swipe_counter = VALUES(min_mag_swipe_counter), max_mag_swipe_counter = VALUES(max_mag_swipe_counter), 
		avg_battery_capacity = VALUES(avg_battery_capacity), min_battery_capacity = VALUES(min_battery_capacity), max_battery_capacity = VALUES(max_battery_capacity);                            
        
                
end$$

DELIMITER ;
SHOW WARNINGS;

-- -----------------------------------------------------
-- procedure update_miu_monthly_stats
-- -----------------------------------------------------

USE `mdce`;
DROP procedure IF EXISTS `update_miu_monthly_stats`;
SHOW WARNINGS;

DELIMITER $$
USE `mdce`$$
CREATE PROCEDURE update_miu_monthly_stats(start_date DATE, end_date DATE)
begin
   
	INSERT INTO miu_monthly_stats
	   (miu_id, year, month,
	    num_expected, num_heard,
	    avg_rssi, min_rssi, max_rssi,
        avg_ber, min_ber, max_ber, 
        avg_processor_reset_counter, min_processor_reset_counter, max_processor_reset_counter,
        avg_register_time, min_register_time, max_register_time, 
        avg_register_time_to_activate_context, min_register_time_to_activate_context, max_register_time_to_activate_context, 
        avg_register_time_connected, min_register_time_connected, max_register_time_connected, 
        avg_register_time_to_transfer_packet, min_register_time_to_transfer_packet, max_register_time_to_transfer_packet, 
        avg_disconnect_time, min_disconnect_time, max_disconnect_time, 
        avg_mag_swipe_counter, min_mag_swipe_counter, max_mag_swipe_counter, 
        avg_battery_capacity, min_battery_capacity, max_battery_capacity) 
	(SELECT miu_id, year(start_date), month(start_date),
			sum(num_expected), sum(num_heard),
			sum(avg_rssi * num_heard) / greatest(1, sum(num_heard)), min(min_rssi), max(max_rssi),
            sum(avg_ber * num_heard) / greatest(1, sum(num_heard)), min(min_ber), max(max_ber),
            sum(avg_processor_reset_counter * num_heard) / greatest(1, sum(num_heard)), min(min_processor_reset_counter), max(max_processor_reset_counter),
            sum(avg_register_time * num_heard) / greatest(1, sum(num_heard)), min(min_register_time), max(max_register_time),
            sum(avg_register_time_to_activate_context * num_heard) / greatest(1, sum(num_heard)), min(min_register_time_to_activate_context), max(max_register_time_to_activate_context),
            sum(avg_register_time_connected * num_heard) / greatest(1, sum(num_heard)), min(min_register_time_connected), max(max_register_time_connected),
            sum(avg_register_time_to_transfer_packet * num_heard) / greatest(1, sum(num_heard)), min(min_register_time_to_transfer_packet), max(max_register_time_to_transfer_packet),
            sum(avg_disconnect_time * num_heard) / greatest(1, sum(num_heard)), min(min_disconnect_time), max(max_disconnect_time),
            sum(avg_mag_swipe_counter * num_heard) / greatest(1, sum(num_heard)), min(min_mag_swipe_counter), max(max_mag_swipe_counter),
            sum(avg_battery_capacity * num_heard) / greatest(1, sum(num_heard)), min(min_battery_capacity), max(max_battery_capacity)
	   FROM miu_daily_stats
	  WHERE heard_date >= start_date AND
			heard_date < end_date
	  GROUP BY miu_id)
	ON DUPLICATE KEY UPDATE 
			num_expected = VALUES(num_expected), num_heard = VALUES(num_heard),
			avg_rssi = VALUES(avg_rssi), min_rssi = VALUES(min_rssi), max_rssi = VALUES(max_rssi),
			avg_ber = VALUES(avg_ber), min_ber = VALUES(min_ber), max_ber = VALUES(max_ber), 
			avg_processor_reset_counter = VALUES(avg_processor_reset_counter), min_processor_reset_counter = VALUES(min_processor_reset_counter), max_processor_reset_counter = VALUES(max_processor_reset_counter),
			avg_register_time = VALUES(avg_register_time), min_register_time = VALUES(min_register_time), max_register_time = VALUES(max_register_time), 
			avg_register_time_to_activate_context = VALUES(avg_register_time_to_activate_context), min_register_time_to_activate_context = VALUES(min_register_time_to_activate_context), max_register_time_to_activate_context = VALUES(max_register_time_to_activate_context), 
			avg_register_time_connected = VALUES(avg_register_time_connected), min_register_time_connected = VALUES(min_register_time_connected), max_register_time_connected = VALUES(max_register_time_connected), 
			avg_register_time_to_transfer_packet = VALUES(avg_register_time_to_transfer_packet), min_register_time_to_transfer_packet = VALUES(min_register_time_to_transfer_packet), max_register_time_to_transfer_packet = VALUES(max_register_time_to_transfer_packet), 
			avg_disconnect_time = VALUES(avg_disconnect_time), min_disconnect_time = VALUES(min_disconnect_time), max_disconnect_time = VALUES(max_disconnect_time), 
			avg_mag_swipe_counter = VALUES(avg_mag_swipe_counter), min_mag_swipe_counter = VALUES(min_mag_swipe_counter), max_mag_swipe_counter = VALUES(max_mag_swipe_counter), 
			avg_battery_capacity = VALUES(avg_battery_capacity), min_battery_capacity = VALUES(min_battery_capacity), max_battery_capacity = VALUES(max_battery_capacity);                            
            

end$$

DELIMITER ;
SHOW WARNINGS;
SET SQL_MODE = '';
GRANT USAGE ON *.* TO mdceintegration;
 DROP USER mdceintegration;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
SHOW WARNINGS;
CREATE USER 'mdceintegration' IDENTIFIED BY 'mdceintegration';

GRANT SELECT, INSERT, TRIGGER ON TABLE * TO 'mdceintegration';
GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE * TO 'mdceintegration';
SHOW WARNINGS;
SET SQL_MODE = '';
GRANT USAGE ON *.* TO mdcemqtt;
 DROP USER mdcemqtt;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
SHOW WARNINGS;
CREATE USER 'mdcemqtt' IDENTIFIED BY 'mdcemqtt';

GRANT SELECT, INSERT, TRIGGER ON TABLE * TO 'mdcemqtt';
GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE * TO 'mdcemqtt';
SHOW WARNINGS;
SET SQL_MODE = '';
GRANT USAGE ON *.* TO mdceweb;
 DROP USER mdceweb;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
SHOW WARNINGS;
CREATE USER 'mdceweb' IDENTIFIED BY 'mdceweb';

GRANT SELECT, INSERT, TRIGGER ON TABLE * TO 'mdceweb';
GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE * TO 'mdceweb';
SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
