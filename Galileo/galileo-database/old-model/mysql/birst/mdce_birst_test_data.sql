delimiter |
drop event if exists jgg_update_dates|
create event jgg_update_dates
   ON SCHEDULE EVERY 1 HOUR STARTS CURRENT_DATE + INTERVAL 15 MINUTE
   do
   begin
      declare li_hour int;
      DECLARE lts_hour TIMESTAMP;
      
      SET li_hour = timestampdiff(hour, current_date, current_timestamp);
      SET lts_hour = adddate(current_date, interval li_hour hour);
      
      /*
       * add the miu_heard_packets, make some harder to hear than others
       */
      INSERT INTO miu_heard_packets
                 (miu_id,
                  heard_time,
                  rssi)
                 (SELECT miu_id,
                         lts_hour,
                         floor(rand() * 8) - 90 - floor(Mod(miu_id, 100) / 3)                   
                    FROM miu_details
                   WHERE (mod(miu_id, 100) < 90 AND rand() > .04) OR
                         (mod(miu_id, 100) > 89 AND mod(miu_id, 1000) <> 999 AND rand() < .5) OR 
                         (mod(miu_id, 1000) = 999 AND rand() < .08 ))                   
        ON DUPLICATE KEY UPDATE heard_time = VALUES(heard_time),
                                rssi = VALUES(rssi);       
      
      INSERT INTO miu_details
                 (miu_id,
                  last_heard_time,
                  next_sync_time)
                 (SELECT miu_id,
                         heard_time,
                         heard_time + interval 1 hour
                    FROM miu_heard_packets
                   WHERE heard_time = lts_hour)
        ON DUPLICATE KEY UPDATE last_heard_time = VALUES(last_heard_time),
                                next_sync_time = VALUES(next_sync_time);       
      
      update miu_details set next_sync_time_error = adddate(next_sync_time, interval 185 minute);
      update miu_details set next_sync_time_warning = adddate(next_sync_time, interval 125 minute);

   end |								
   
drop event if exists update_miu_stats|
create event update_miu_stats
   ON SCHEDULE EVERY 15 MINUTE STARTS CURRENT_DATE + INTERVAL 20 MINUTE
   do
   begin
      CALL update_miu_stats(null);
   end |								

DROP PROCEDURE IF EXISTS update_miu_stats|
CREATE PROCEDURE update_miu_stats(ats_date timestamp)
begin
   DECLARE ldate_month_start DATE;
   DECLARE ldate_month_end DATE;
   
   IF ats_date IS NULL THEN
      SET ats_date = current_timestamp;
   END IF;
   
   /*
    * convert to chicago
    */
   SET ats_date = convert_tz(ats_date, 'UTC', 'America/Chicago');
   
   SET ldate_month_start = ats_date - interval day(ats_date) - 1 day;
   SET ldate_month_end   = ldate_month_start + interval 1 month;
   
   /*
    * if 1st hour of the day, update last 3 days
    */
   IF hour(ats_date) = 0  THEN
      CALL update_miu_daily_stats(ats_date - interval 1 day);
      CALL update_miu_daily_stats(ats_date - interval 2 day);
      CALL update_miu_daily_stats(ats_date - interval 3 day);

      /*
       * if 1st 3 days of the month, calculate previous month as well
       */
      IF day(ats_date) < 4 THEN
         INSERT INTO miu_monthly_stats
                    (miu_id,
                     year,
                     month,
                     avg_rssi,
                     min_rssi,
                     max_rssi,
                     num_expected,
                     num_heard)
                    (SELECT miu_id,
                            year(ldate_month_start - interval 1 month),
                            month(ldate_month_start - interval 1 month),
                            avg(avg_rssi),
                            min(min_rssi),
                            max(max_rssi),
                            sum(num_expected),
                            sum(num_heard)
                       FROM miu_daily_stats
                      WHERE heard_date >= ldate_month_start - interval 1 month AND
                            heard_date < ldate_month_start
                      GROUP BY miu_id)
           ON DUPLICATE KEY UPDATE avg_rssi     = VALUES(avg_rssi),
                                   min_rssi     = VALUES(min_rssi),
                                   max_rssi     = VALUES(max_rssi),
                                   num_expected = VALUES(num_expected),
                                   num_heard    = VALUES(num_heard);
      END IF;                                   
   END IF;                            

   /*
    * update current day
    */
   CALL update_miu_daily_stats(ats_date);
   
   /*
    * now update current month
    */
   INSERT INTO miu_monthly_stats
              (miu_id,
               year,
               month,
               avg_rssi,
               min_rssi,
               max_rssi,
               num_expected,
               num_heard)
              (SELECT miu_id,
                      year(ldate_month_start),
                      month(ldate_month_start),
                      avg(avg_rssi),
                      min(min_rssi),
                      max(max_rssi),
                      sum(num_expected),
                      sum(num_heard)
                 FROM miu_daily_stats
                WHERE heard_date >= ldate_month_start AND
                      heard_date < ldate_month_end
                GROUP BY miu_id)
     ON DUPLICATE KEY UPDATE avg_rssi     = VALUES(avg_rssi),
                             min_rssi     = VALUES(min_rssi),
                             max_rssi     = VALUES(max_rssi),
                             num_expected = VALUES(num_expected),
                             num_heard    = VALUES(num_heard);
end |								

DROP PROCEDURE IF EXISTS update_miu_daily_stats|
CREATE PROCEDURE update_miu_daily_stats(adate_stats DATE)
begin
   DECLARE ldate_utc DATE;
   DECLARE li_mins INT;
   DECLARE li_mins_utc INT;
   DECLARE lbool_prev_date BOOLEAN;
   
   IF adate_stats IS NULL THEN
      SET adate_stats = convert_tz(current_date, 'UTC', 'America/Chicago');
   END IF;
   
   /*
    * convert to UTC
    */
   SET ldate_utc = convert_tz(adate_stats, 'America/Chicago', 'UTC');
   
   /*
    * calculate the mins different for UTC
    */
   SET li_mins_utc = timestampdiff(minute, current_timestamp, convert_tz(current_timestamp, 'UTC', 'America/Chicago'));
   
   /*
    * calculate minutes after midnight
    */
   SET li_mins = timestampdiff(minute, 
                               date(convert_tz(current_timestamp, 'UTC', 'America/Chicago')), 
                               convert_tz(current_timestamp, 'UTC', 'America/Chicago'));
                               
   IF adate_stats < current_date THEN                               
      SET lbool_prev_date = true;
   ELSE
      SET lbool_prev_date = false;
   END IF;

   INSERT INTO miu_daily_stats
              (miu_id,
               heard_date,
               avg_rssi,
               min_rssi,
               max_rssi,
               num_expected,
               num_heard)
              (SELECT m.miu_id,
                      adate_stats,
                      avg(rssi),
                      min(rssi),
                      max(rssi),
                      coalesce((SELECT if(lbool_prev_date, 24, floor(li_mins / 60 * 60 / interval_mins + if(li_mins < start_mins, 0, 1)))                            
								  FROM cmiu_config_history h
									 LEFT OUTER JOIN cmiu_config_set s ON s.id = h.cmiu_config
									 LEFT OUTER JOIN reporting_plans p ON p.reporting_plan_id = s.reporting_plan_id
								 WHERE h.miu_id = p.miu_id AND
									   h.timestamp < ldate_utc + interval 1 day
								 ORDER BY timestamp DESC
								 LIMIT 1), 1),
                      count(p.miu_id)
                 FROM miu_details m
					     LEFT OUTER JOIN miu_heard_packets p ON m.miu_id = p.miu_id AND
                                                           heard_time >= adate_stats - INTERVAL li_mins_utc MINUTE AND 
                                                           heard_time < adate_stats - INTERVAL li_mins_utc MINUTE + INTERVAL 1 day
                WHERE m.meter_active = 'Y'                                           
                GROUP BY miu_id, adate_stats)
     ON DUPLICATE KEY UPDATE avg_rssi     = VALUES(avg_rssi),
                             min_rssi     = VALUES(min_rssi),
                             max_rssi     = VALUES(max_rssi),
                             num_expected = VALUES(num_expected),
                             num_heard    = VALUES(num_heard);
                
end |								


drop procedure if exists build_data|
create procedure build_data (ai_num_util int, ai_num_mius int)
begin
    DECLARE li_Util INT;
    DECLARE li_MIU INT;
    DECLARE li_tag INT;
    DECLARE li_hour INT;
    DECLARE li_reporting_plan_id INT;
    DECLARE li_config_set_id INT;
    DECLARE li_data_collection_id INT;
    
    /*
     * clear out existing data
     */
    DELETE FROM cmiu_config_history;
    DELETE FROM miu_heard_packets;
    DELETE FROM miu_monthly_stats;
    DELETE FROM miu_daily_stats;
    DELETE FROM miu_tags;
    DELETE FROM utility_tags;
    DELETE FROM miu_details;
    DELETE FROM utility_list;
    DELETE FROM tag_list;

    /*
     * build default tags
     */
    INSERT INTO tag_list
               (tag_id,
                name,
                public)
        VALUES (1,
                'All',
                true);
    INSERT INTO tag_list
               (tag_id,
                name,
                public)
        VALUES (2,
                'Region: E',
                true);
    INSERT INTO tag_list
               (tag_id,
                name,
                public)
        VALUES (3,
                'Region: W',
                true);
    INSERT INTO tag_list
               (tag_id,
                name,
                public)
        VALUES (4,
                'Region: SE',
                true);
                
   /*
    * build default recording, reportiing, config_set
    */
   SELECT min(reporting_plan_id)
     INTO li_reporting_plan_id
     FROM reporting_plans
    WHERE interval_mins = 60;
   IF li_reporting_plan_id IS NULL THEN
      SELECT coalesce(max(reporting_plan_id), 1)
        INTO li_reporting_plan_id
        FROM reporting_plans;
      INSERT INTO reporting_plans
                 (reporting_plan_id,
                  name, 
                  start_mins,
                  interval_mins)
          VALUES (li_reporting_plan_id,
                  'Test Reporting Plan 60',
                  0,
                  60);
   END IF;                  
       
   SELECT min(data_collection_id)
     INTO li_data_collection_id
     FROM data_collection_plans
    WHERE interval_mins = 60;
   IF li_data_collection_id IS NULL THEN
      SELECT coalesce(max(data_collection_id), 1)
        INTO li_data_collection_id
        FROM data_collection_plans;
      INSERT INTO data_collection_plans
                 (data_collection_id,
                  name, 
                  start_time_mins,
                  interval_mins)
          VALUES (li_data_collection_id,
                  'Test data collection Plan 60',
                  0,
                  60);
   END IF;                  
   
   SELECT min(id)
     INTO li_config_set_id
     FROM cmiu_config_set
    WHERE reporting_plan_id = li_reporting_plan_id AND
          data_collection_plans_data_collection_id = li_data_collection_id;
  IF li_config_set_id IS NULL THEN
      SELECT coalesce(max(id), 1)
        INTO li_config_set_id
        FROM cmiu_config_set;
      INSERT INTO cmiu_config_set
                 (id,
                  name, 
                  reporting_plan_id,
                  data_collection_plans_data_collection_id)
          VALUES (li_config_set_id,
                  'Test data collection Plan 60',
                  li_reporting_plan_id,
                  li_data_collection_id);
   END IF;                  
   
    
    /*
     * put out found meter utility
     */
    INSERT INTO utility_list(utility_id,
                             name)
                     VALUES (0,
                             'FOUND METERS'); 
    INSERT INTO utility_tags(utility_id,
                             tag_id)
                     VALUES (0,
                             1);                             
                             
   SET li_Util = 1;
   
   WHILE li_Util <= ai_num_util DO

       INSERT INTO utility_list(utility_id,
                                name)
                        VALUES (li_Util,
                                concat('Utility ', li_Util)); 
       INSERT INTO utility_tags(utility_id,
                                tag_id)
                        VALUES (li_Util,
                                1);                             
       INSERT INTO utility_tags(utility_id,
                                tag_id)
                        VALUES (li_Util,
                                Mod(li_Util,3)+2);                             
       INSERT INTO tag_list
                  (tag_id,
                   name,
                   public)
           VALUES (li_Util+4,
                   concat('Utility: Utility ', li_Util),
                   true);
       INSERT INTO utility_tags(utility_id,
                                tag_id)
                        VALUES (li_Util,
                                li_Util+4);                             
                                
      SET li_Util = li_Util + 1;                                
      
   END WHILE;                              
   
   COMMIT;
   
   /*
    * now add the miu's
    */
   SET li_MIU  = 1000000000;
   SET li_hour = hour(current_timestamp);
   
   WHILE li_MIU < 1000000000 + ai_num_mius DO
      INSERT INTO miu_details
                 (miu_id,
                  utility_id,
                  last_heard_time,
                  next_sync_time,
                  next_sync_time_warning,
                  next_sync_time_error)
          VALUES (li_MIU,
                  mod(li_MIU, ai_num_util) + 1,
                  adddate(current_date, interval li_hour hour),
                  adddate(current_date, interval li_hour + 1 hour),
                  adddate(current_date, interval (li_hour + 3) * 60 + 5 minute),
                  adddate(current_date, interval (li_hour + 4) * 60 + 5 minute));
                  
      INSERT INTO miu_heard_packets
                 (miu_id,
                  heard_time,
                  rssi)
          VALUES (li_MIU,
                  adddate(current_date, interval li_hour hour),
                  floor(rand() * 8) - 90 - Mod(li_MIU, 30));                   
                  
      INSERT INTO cmiu_config_history                  
                 (miu_id,
                  cmiu_config,
                  timestamp,
                  received_from_cmiu)
          VALUES (li_MIU,
                  li_config_set_id,
                  current_timestamp,
                  false);
                  
      SET li_MIU = li_MIU + 1;                  
   
   END WHILE;
   
   COMMIT;

   
end|

delimiter ;   


