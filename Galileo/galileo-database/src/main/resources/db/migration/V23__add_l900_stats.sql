-- MSPD-1858
-- Adding columns for SNR, DataRate, TXPower, Freq

ALTER TABLE miu_heard_packets 
  ADD COLUMN `snr` DECIMAL(6,2) NULL COMMENT 'Units = dB',
  ADD COLUMN `datarate` INT(11) NULL COMMENT 'Units = bits per second',
  ADD COLUMN `txpower` DECIMAL(6,2) NULL COMMENT 'Units = dBm',
  ADD COLUMN `freq` DECIMAL(6,2) NULL COMMENT 'Units = MHz';


ALTER TABLE miu_daily_stats 
  ADD COLUMN `avg_snr` DECIMAL(6,3) NULL COMMENT 'Units = dB',
  ADD COLUMN `min_snr` DECIMAL(6,2) NULL COMMENT 'Units = dB',
  ADD COLUMN `max_snr` DECIMAL(6,2) NULL COMMENT 'Units = dB',

  ADD COLUMN `avg_datarate` DECIMAL(12,1) NULL COMMENT 'Units = bits per second',
  ADD COLUMN `min_datarate` INT(11) NULL COMMENT 'Units = bits per second',
  ADD COLUMN `max_datarate` INT(11) NULL COMMENT 'Units = bits per second',

  ADD COLUMN `avg_txpower` DECIMAL(6,3) NULL COMMENT 'Units = dBm',
  ADD COLUMN `min_txpower` DECIMAL(6,2) NULL COMMENT 'Units = dBm',
  ADD COLUMN `max_txpower` DECIMAL(6,2) NULL COMMENT 'Units = dBm',

  ADD COLUMN `avg_freq` DECIMAL(6,3) NULL COMMENT 'Units = MHz',
  ADD COLUMN `min_freq` DECIMAL(6,2) NULL COMMENT 'Units = MHz',
  ADD COLUMN `max_freq` DECIMAL(6,2) NULL COMMENT 'Units = MHz';


ALTER TABLE miu_monthly_stats
  ADD COLUMN `avg_snr` DECIMAL(6,3) NULL COMMENT 'Units = dB',
  ADD COLUMN `min_snr` DECIMAL(6,2) NULL COMMENT 'Units = dB',
  ADD COLUMN `max_snr` DECIMAL(6,2) NULL COMMENT 'Units = dB',

  ADD COLUMN `avg_datarate` DECIMAL(12,1) NULL COMMENT 'Units = bits per second',
  ADD COLUMN `min_datarate` INT(11) NULL COMMENT 'Units = bits per second',
  ADD COLUMN `max_datarate` INT(11) NULL COMMENT 'Units = bits per second',

  ADD COLUMN `avg_txpower` DECIMAL(6,3) NULL COMMENT 'Units = dBm',
  ADD COLUMN `min_txpower` DECIMAL(6,2) NULL COMMENT 'Units = dBm',
  ADD COLUMN `max_txpower` DECIMAL(6,2) NULL COMMENT 'Units = dBm',

  ADD COLUMN `avg_freq` DECIMAL(6,3) NULL COMMENT 'Units = MHz',
  ADD COLUMN `min_freq` DECIMAL(6,2) NULL COMMENT 'Units = MHz',
  ADD COLUMN `max_freq` DECIMAL(6,2) NULL COMMENT 'Units = MHz';

-- -----------------------------------------------------
-- procedure update_miu_daily_stats
-- -----------------------------------------------------

DROP PROCEDURE IF EXISTS update_miu_daily_stats;

DELIMITER $$


CREATE PROCEDURE update_miu_daily_stats(adate_stats DATE)
  begin
    DECLARE ldate_utc DATE;
    DECLARE li_mins INT;
    DECLARE li_mins_utc INT;
    DECLARE lbool_prev_date BOOLEAN;

    IF adate_stats IS NULL THEN
      SET adate_stats = convert_tz(current_date, 'UTC', 'America/Chicago');
    END IF;

    /*
     * convert to UTC
     */
    SET ldate_utc = convert_tz(adate_stats, 'America/Chicago', 'UTC');

    /*
     * calculate the mins different for UTC
     */
    SET li_mins_utc = timestampdiff(minute, current_timestamp, convert_tz(current_timestamp, 'UTC', 'America/Chicago'));

    /*
     * calculate minutes after midnight
     */
    SET li_mins = timestampdiff(minute,
                                date(convert_tz(current_timestamp, 'UTC', 'America/Chicago')),
                                convert_tz(current_timestamp, 'UTC', 'America/Chicago'));

    IF adate_stats < current_date THEN
      SET lbool_prev_date = true;
    ELSE
      SET lbool_prev_date = false;
    END IF;

    DROP TABLE IF EXISTS tmp_miu_daily_stats;


    CREATE TEMPORARY TABLE tmp_miu_daily_stats(
      miu_id INT PRIMARY KEY,
      num_expected INT,
      meter_active CHAR(1)
    )
      AS SELECT m.miu_id AS miu_id, coalesce(60*24 / reporting_interval_mins, 1) AS num_expected, m.meter_active as meter_active
         FROM miu_details m
           LEFT OUTER JOIN cmiu_config_mgmt ccm ON m.miu_id = ccm.miu_id
           LEFT OUTER JOIN cmiu_config_set ccs ON ccm.current_config = ccs.id;

    INSERT INTO miu_daily_stats (miu_id, heard_date,
                                 num_expected, num_heard,
                                 avg_rssi, min_rssi, max_rssi,
                                 avg_ber, min_ber, max_ber,
                                 avg_rsrq, min_rsrq, max_rsrq,
                                 avg_processor_reset_counter, min_processor_reset_counter, max_processor_reset_counter,
                                 avg_register_time, min_register_time, max_register_time,
                                 avg_register_time_to_activate_context, min_register_time_to_activate_context, max_register_time_to_activate_context,
                                 avg_register_time_connected, min_register_time_connected, max_register_time_connected,
                                 avg_register_time_to_transfer_packet, min_register_time_to_transfer_packet, max_register_time_to_transfer_packet,
                                 avg_disconnect_time, min_disconnect_time, max_disconnect_time,
                                 avg_mag_swipe_counter, min_mag_swipe_counter, max_mag_swipe_counter,
                                 avg_battery_capacity, min_battery_capacity, max_battery_capacity,
                                 avg_battery_voltage, min_battery_voltage, max_battery_voltage,
                                 avg_temperature, min_temperature, max_temperature,
                                 avg_snr, min_snr, max_snr,
                                 avg_datarate, min_datarate, max_datarate,
                                 avg_txpower, min_txpower, max_txpower,
                                 avg_freq, min_freq, max_freq
    )
      (SELECT p.miu_id, adate_stats,
         tmp.num_expected,
         count(p.miu_id),
         sum(avg_rssi * miu_packet_count) / greatest(sum(miu_packet_count), 1), min(min_rssi), max(max_rssi),
         avg(ber), min(ber), max(ber),
         avg(rsrq), min(rsrq), max(rsrq),
         avg(processor_reset_counter), min(processor_reset_counter), max(processor_reset_counter),
         avg(register_time), min(register_time), max(register_time),
         avg(register_time_to_activate_context), min(register_time_to_activate_context), max(register_time_to_activate_context),
         avg(register_time_connected), min(register_time_connected), max(register_time_connected),
         avg(register_time_to_transfer_packet), min(register_time_to_transfer_packet), max(register_time_to_transfer_packet),
         avg(disconnect_time), min(disconnect_time), max(disconnect_time),
         avg(mag_swipe_counter), min(mag_swipe_counter), max(mag_swipe_counter),
         avg(battery_capacity), min(battery_capacity), max(battery_capacity),
         avg(battery_voltage), min(battery_voltage), max(battery_voltage),
         avg(temperature), min(temperature), max(temperature),
         avg(snr), min(snr), max(snr),
         avg(datarate), min(datarate), max(datarate),
         avg(txpower), min(txpower), max(txpower),
         avg(freq), min(freq), max(freq)
       FROM miu_heard_packets p
         INNER JOIN tmp_miu_daily_stats tmp ON tmp.miu_id = p.miu_id
       WHERE
         heard_time >= adate_stats - INTERVAL li_mins_utc MINUTE AND
         heard_time < adate_stats - INTERVAL li_mins_utc MINUTE + INTERVAL 1 day
       GROUP BY miu_id)
    ON DUPLICATE KEY UPDATE
      num_expected = VALUES(num_expected), num_heard = VALUES(num_heard),
      avg_rssi = VALUES(avg_rssi), min_rssi = VALUES(min_rssi), max_rssi = VALUES(max_rssi),
      avg_ber = VALUES(avg_ber), min_ber = VALUES(min_ber), max_ber = VALUES(max_ber),
      avg_rsrq = VALUES(avg_rsrq), min_rsrq = VALUES(min_rsrq), max_rsrq = VALUES(max_rsrq),
      avg_processor_reset_counter = VALUES(avg_processor_reset_counter), min_processor_reset_counter = VALUES(min_processor_reset_counter), max_processor_reset_counter = VALUES(max_processor_reset_counter),
      avg_register_time = VALUES(avg_register_time), min_register_time = VALUES(min_register_time), max_register_time = VALUES(max_register_time),
      avg_register_time_to_activate_context = VALUES(avg_register_time_to_activate_context), min_register_time_to_activate_context = VALUES(min_register_time_to_activate_context), max_register_time_to_activate_context = VALUES(max_register_time_to_activate_context),
      avg_register_time_connected = VALUES(avg_register_time_connected), min_register_time_connected = VALUES(min_register_time_connected), max_register_time_connected = VALUES(max_register_time_connected),
      avg_register_time_to_transfer_packet = VALUES(avg_register_time_to_transfer_packet), min_register_time_to_transfer_packet = VALUES(min_register_time_to_transfer_packet), max_register_time_to_transfer_packet = VALUES(max_register_time_to_transfer_packet),
      avg_disconnect_time = VALUES(avg_disconnect_time), min_disconnect_time = VALUES(min_disconnect_time), max_disconnect_time = VALUES(max_disconnect_time),
      avg_mag_swipe_counter = VALUES(avg_mag_swipe_counter), min_mag_swipe_counter = VALUES(min_mag_swipe_counter), max_mag_swipe_counter = VALUES(max_mag_swipe_counter),
      avg_battery_capacity = VALUES(avg_battery_capacity), min_battery_capacity = VALUES(min_battery_capacity), max_battery_capacity = VALUES(max_battery_capacity),
      avg_battery_voltage = VALUES(avg_battery_voltage), min_battery_voltage = VALUES(min_battery_voltage), max_battery_voltage = VALUES(max_battery_voltage),
      avg_temperature = VALUES(avg_temperature), min_temperature = VALUES(min_temperature), max_temperature = VALUES(max_temperature),
      avg_snr = VALUES(avg_snr), min_snr = VALUES(min_snr), max_snr = VALUES(max_snr),
      avg_datarate = VALUES(avg_datarate), min_datarate = VALUES(min_datarate), max_datarate = VALUES(max_datarate),
      avg_txpower = VALUES(avg_txpower), min_txpower = VALUES(min_txpower), max_txpower = VALUES(max_txpower),
      avg_freq = VALUES(avg_freq), min_freq = VALUES(min_freq), max_freq = VALUES(max_freq)
    ;

    DROP TABLE tmp_miu_daily_stats;
  end
$$


DELIMITER ;


-- -----------------------------------------------------
-- procedure update_miu_monthly_stats
-- -----------------------------------------------------

DROP procedure IF EXISTS `update_miu_monthly_stats`;
SHOW WARNINGS;

DELIMITER $$

CREATE PROCEDURE update_miu_monthly_stats(start_date DATE, end_date DATE)
  begin

    INSERT INTO miu_monthly_stats
    (miu_id, year, month,
     num_expected, num_heard,
     avg_rssi, min_rssi, max_rssi,
     avg_ber, min_ber, max_ber,
     avg_rsrq, min_rsrq, max_rsrq,
     avg_processor_reset_counter, min_processor_reset_counter, max_processor_reset_counter,
     avg_register_time, min_register_time, max_register_time,
     avg_register_time_to_activate_context, min_register_time_to_activate_context, max_register_time_to_activate_context,
     avg_register_time_connected, min_register_time_connected, max_register_time_connected,
     avg_register_time_to_transfer_packet, min_register_time_to_transfer_packet, max_register_time_to_transfer_packet,
     avg_disconnect_time, min_disconnect_time, max_disconnect_time,
     avg_mag_swipe_counter, min_mag_swipe_counter, max_mag_swipe_counter,
     avg_battery_capacity, min_battery_capacity, max_battery_capacity,
     avg_battery_voltage, min_battery_voltage, max_battery_voltage,
     avg_temperature, min_temperature, max_temperature,
     avg_snr, min_snr, max_snr,
     avg_datarate, min_datarate, max_datarate,
     avg_txpower, min_txpower, max_txpower,
     avg_freq, min_freq, max_freq
    )
      (SELECT miu_id, year(start_date), month(start_date),
         sum(num_expected), sum(num_heard),
         sum(avg_rssi * num_heard) / greatest(1, sum(num_heard)), min(min_rssi), max(max_rssi),
         sum(avg_ber * num_heard) / greatest(1, sum(num_heard)), min(min_ber), max(max_ber),
         sum(avg_rsrq * num_heard) / greatest(1, sum(num_heard)), min(min_rsrq), max(max_rsrq),
         sum(avg_processor_reset_counter * num_heard) / greatest(1, sum(num_heard)), min(min_processor_reset_counter), max(max_processor_reset_counter),
         sum(avg_register_time * num_heard) / greatest(1, sum(num_heard)), min(min_register_time), max(max_register_time),
         sum(avg_register_time_to_activate_context * num_heard) / greatest(1, sum(num_heard)), min(min_register_time_to_activate_context), max(max_register_time_to_activate_context),
         sum(avg_register_time_connected * num_heard) / greatest(1, sum(num_heard)), min(min_register_time_connected), max(max_register_time_connected),
         sum(avg_register_time_to_transfer_packet * num_heard) / greatest(1, sum(num_heard)), min(min_register_time_to_transfer_packet), max(max_register_time_to_transfer_packet),
         sum(avg_disconnect_time * num_heard) / greatest(1, sum(num_heard)), min(min_disconnect_time), max(max_disconnect_time),
         sum(avg_mag_swipe_counter * num_heard) / greatest(1, sum(num_heard)), min(min_mag_swipe_counter), max(max_mag_swipe_counter),
         sum(avg_battery_capacity * num_heard) / greatest(1, sum(num_heard)), min(min_battery_capacity), max(max_battery_capacity),
         sum(avg_battery_voltage * num_heard) / greatest(1, sum(num_heard)), min(min_battery_voltage), max(max_battery_voltage),
         sum(avg_temperature * num_heard) / greatest(1, sum(num_heard)), min(min_temperature), max(max_temperature),
         sum(avg_snr * num_heard) / greatest(1, sum(num_heard)), min(min_snr), max(max_snr),
         sum(avg_datarate * num_heard) / greatest(1, sum(num_heard)), min(min_datarate), max(max_datarate),
         sum(avg_txpower * num_heard) / greatest(1, sum(num_heard)), min(min_txpower), max(max_txpower),
         sum(avg_freq * num_heard) / greatest(1, sum(num_heard)), min(min_freq), max(max_freq)
       FROM miu_daily_stats
       WHERE heard_date >= start_date AND
             heard_date < end_date
       GROUP BY miu_id)
    ON DUPLICATE KEY UPDATE
      num_expected = VALUES(num_expected), num_heard = VALUES(num_heard),
      avg_rssi = VALUES(avg_rssi), min_rssi = VALUES(min_rssi), max_rssi = VALUES(max_rssi),
      avg_ber = VALUES(avg_ber), min_ber = VALUES(min_ber), max_ber = VALUES(max_ber),
      avg_rsrq = VALUES(avg_rsrq), min_rsrq = VALUES(min_rsrq), max_rsrq = VALUES(max_rsrq),
      avg_processor_reset_counter = VALUES(avg_processor_reset_counter), min_processor_reset_counter = VALUES(min_processor_reset_counter), max_processor_reset_counter = VALUES(max_processor_reset_counter),
      avg_register_time = VALUES(avg_register_time), min_register_time = VALUES(min_register_time), max_register_time = VALUES(max_register_time),
      avg_register_time_to_activate_context = VALUES(avg_register_time_to_activate_context), min_register_time_to_activate_context = VALUES(min_register_time_to_activate_context), max_register_time_to_activate_context = VALUES(max_register_time_to_activate_context),
      avg_register_time_connected = VALUES(avg_register_time_connected), min_register_time_connected = VALUES(min_register_time_connected), max_register_time_connected = VALUES(max_register_time_connected),
      avg_register_time_to_transfer_packet = VALUES(avg_register_time_to_transfer_packet), min_register_time_to_transfer_packet = VALUES(min_register_time_to_transfer_packet), max_register_time_to_transfer_packet = VALUES(max_register_time_to_transfer_packet),
      avg_disconnect_time = VALUES(avg_disconnect_time), min_disconnect_time = VALUES(min_disconnect_time), max_disconnect_time = VALUES(max_disconnect_time),
      avg_mag_swipe_counter = VALUES(avg_mag_swipe_counter), min_mag_swipe_counter = VALUES(min_mag_swipe_counter), max_mag_swipe_counter = VALUES(max_mag_swipe_counter),
      avg_battery_capacity = VALUES(avg_battery_capacity), min_battery_capacity = VALUES(min_battery_capacity), max_battery_capacity = VALUES(max_battery_capacity),
      avg_battery_voltage = VALUES(avg_battery_voltage), min_battery_voltage = VALUES(min_battery_voltage), max_battery_voltage = VALUES(max_battery_voltage),
      avg_temperature = VALUES(avg_temperature), min_temperature = VALUES(min_temperature), max_temperature = VALUES(max_temperature),
      avg_snr = VALUES(avg_snr), min_snr = VALUES(min_snr), max_snr = VALUES(max_snr),
      avg_datarate = VALUES(avg_datarate), min_datarate = VALUES(min_datarate), max_datarate = VALUES(max_datarate),
      avg_txpower = VALUES(avg_txpower), min_txpower = VALUES(min_txpower), max_txpower = VALUES(max_txpower),
      avg_freq = VALUES(avg_freq), min_freq = VALUES(min_freq), max_freq = VALUES(max_freq)
    ;

  end$$

DELIMITER ;
SHOW WARNINGS;

