-- MSPD 1663
-- Make miu_details.site_id NOT NULL

UPDATE miu_details SET site_id = 0 WHERE site_id IS NULL;
ALTER TABLE miu_details MODIFY site_id INT(10) UNSIGNED NOT NULL DEFAULT 0 ;
