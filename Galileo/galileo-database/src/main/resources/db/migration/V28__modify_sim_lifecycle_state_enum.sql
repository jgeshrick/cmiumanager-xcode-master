-- MSPD-1975
-- Include 'Aging' in the enum values for SIM lifecycle state.

ALTER TABLE sim_details MODIFY COLUMN state ENUM('Unassigned', 'Assigned', 'Aging', 'Terminated', 'Rogue');

ALTER TABLE sim_lifecycle_state MODIFY COLUMN state ENUM('Unassigned', 'Assigned', 'Aging', 'Terminated', 'Rogue');
