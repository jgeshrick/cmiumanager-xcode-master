-- MSPD-2136
-- Convert database to utf8

ALTER DATABASE mdce CHARACTER SET utf8 COLLATE utf8_general_ci;

SET collation_connection = 'utf8_general_ci';
SET collation_server = 'utf8_general_ci';

SET character_set_client = 'utf8';
SET character_set_connection = 'utf8';
SET character_set_results = 'utf8';
SET character_set_server = 'utf8';

ALTER TABLE `alert_alerts` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `alert_log` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `apn_lookup` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `audit_list` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `cellular_device_details` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `cmiu_config_history` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `cmiu_config_mgmt` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `cmiu_config_set` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `cmiu_registered_images` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `cmiu_revision_history` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `cns_credentials` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `event_data` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `event_list` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `gateway_config` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `gateway_details` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `gateway_heard_packets` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `gateway_monthly_stats` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `kpi_list` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `lora_device_details` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `mdce_api_tokens` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `mdce_settings` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `miu_commands` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `miu_daily_stats` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `miu_data_usage` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `miu_details` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `miu_heard_packets` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `miu_lifecycle_state` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `miu_manufacture_info` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `miu_manufacture_info_test_station` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `miu_manufacture_info_tests` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `miu_manufacture_info_tests_log` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `miu_monthly_stats` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `miu_tags` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `password_reset_request` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `ref_data_customer_number` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `ref_data_distributer` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `ref_data_info` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `ref_data_info_contact` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `ref_data_secondary_contact_info` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `ref_data_utilities` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `sim_details` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `sim_lifecycle_state` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `site_list` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `site_tags` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `site_user` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `tag_list` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `tz_test` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `user_dashboards` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `user_dashboards_kpis` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `user_dashboards_kpis_default_tags` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `user_list` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `user_public_dashboards` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `user_tags` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;