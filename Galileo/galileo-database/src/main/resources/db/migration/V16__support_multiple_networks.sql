-- MSPD-1581
-- Modify MDCE to support both CNS interfaces simultaneously
-- Split out sim_details into separate table as they have an independent lifecycle

CREATE TABLE IF NOT EXISTS sim_details (
  `sim_details_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `miu_id` INT UNSIGNED NULL,
  `iccid` VARCHAR(20) NOT NULL,
  `msisdn` VARCHAR(20) NULL COMMENT 'MSISDN seems to be the only identifier field when callback is sent on changing device provisioning state',
  `imsi` VARCHAR(16) NULL,
  `sim_cellular_network` VARCHAR(45) NULL,
  `last_state_change_timestamp` DATETIME,
  `state` ENUM('Unassigned', 'Assigned', 'Terminated', 'Rogue'),
  PRIMARY KEY (`sim_details_id`),
  INDEX `fk_csim_details_miu_details1_idx` (`miu_id` ASC),
  UNIQUE INDEX `miu_id_UNIQUE` (`miu_id` ASC),
  UNIQUE INDEX `iccid_UNIQUE` (`iccid` ASC),
  CONSTRAINT `fk_sim_details1`
  FOREIGN KEY (`miu_id`)
  REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS sim_lifecycle_state (
  sim_lifecycle_state_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  sim_details_id INT UNSIGNED NOT NULL,
  timestamp DATETIME,
  `state` ENUM('Unassigned', 'Assigned', 'Terminated', 'Rogue'),
    PRIMARY KEY (sim_lifecycle_state_id),
  CONSTRAINT `fk_sim_lifecycle_state1`
    FOREIGN KEY (`sim_details_id`)
    REFERENCES sim_details (`sim_details_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

INSERT INTO sim_details (`miu_id`, `iccid`, `msisdn`, `imsi`, `sim_cellular_network`, state, last_state_change_timestamp)
  (SELECT miu_id, iccid, msisdn, imsi, 'VZW', 'Assigned', NOW() from cellular_device_details where iccid is not null);

ALTER TABLE cellular_device_details
  CHANGE modem_config_vendor modem_vendor VARCHAR(45),
  CHANGE imei imei NUMERIC(16),
  ADD COLUMN modem_cellular_network VARCHAR(45),
  DROP COLUMN iccid,
  DROP COLUMN msisdn,
  DROP COLUMN imsi;


UPDATE cellular_device_details SET modem_cellular_network='VZW';

