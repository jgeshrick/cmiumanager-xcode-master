-- MSPD-1301
-- Adding the APN column to Cellular Device Details to support updated set/get cellular config interface

ALTER TABLE cellular_device_details
  ADD COLUMN apn VARCHAR(100);
