-- Script to create 3 users for mdce applications
-- This script should already been created if forward engineering on mwb is manually run.
-- However, user and privilege is not generated when forward engineering mwb using forward-engineer.bat

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DELIMITER ;
SHOW WARNINGS;
SET SQL_MODE = '';
GRANT USAGE ON *.* TO mdceintegration;
 DROP USER mdceintegration;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
SHOW WARNINGS;
CREATE USER 'mdceintegration' IDENTIFIED BY 'mdceintegration';

GRANT SELECT, INSERT, TRIGGER ON TABLE * TO 'mdceintegration';
GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE * TO 'mdceintegration';
SHOW WARNINGS;
SET SQL_MODE = '';
GRANT USAGE ON *.* TO mdcemqtt;
 DROP USER mdcemqtt;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
SHOW WARNINGS;
CREATE USER 'mdcemqtt' IDENTIFIED BY 'mdcemqtt';

GRANT SELECT, INSERT, TRIGGER ON TABLE * TO 'mdcemqtt';
GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE * TO 'mdcemqtt';
SHOW WARNINGS;
SET SQL_MODE = '';
GRANT USAGE ON *.* TO mdceweb;
 DROP USER mdceweb;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
SHOW WARNINGS;
CREATE USER 'mdceweb' IDENTIFIED BY 'mdceweb';

GRANT SELECT, INSERT, TRIGGER ON TABLE * TO 'mdceweb';
GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE * TO 'mdceweb';

SHOW WARNINGS;
SET SQL_MODE = '';
GRANT USAGE ON *.* TO readonly;
 DROP USER readonly;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
SHOW WARNINGS;
CREATE USER 'readonly' IDENTIFIED BY 'Neptune$';

GRANT SELECT ON TABLE * TO 'readonly';
SHOW WARNINGS;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS; 

---------- MDCE-DDL-FE.SQL
-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: mdce
-- Source Schemata: mdce
-- Workbench Version: 6.3.4
-- ----------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------------------------------------------------------
-- Schema mdce
-- ----------------------------------------------------------------------------
-- Note: this breaks Flyway
-- DROP SCHEMA IF EXISTS `mdce` ;
-- CREATE SCHEMA IF NOT EXISTS `mdce` DEFAULT CHARACTER SET utf8 ;

-- ----------------------------------------------------------------------------
-- Table mdce.miu_details
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`miu_details` (
  `miu_id` INT UNSIGNED NOT NULL COMMENT '',
  `site_id` INT UNSIGNED NULL COMMENT '',
  `miu_type` INT NULL COMMENT '',
  `last_insert_date` DATETIME NULL COMMENT '',
  `first_insert_date` DATETIME NULL COMMENT '',
  `last_heard_time` DATETIME NULL COMMENT '',
  `next_sync_time` DATETIME NULL COMMENT '',
  `next_sync_time_warning` DATETIME NULL COMMENT '',
  `next_sync_time_error` DATETIME NULL COMMENT '',
  `meter_active` VARCHAR(1) NULL DEFAULT 'N' COMMENT '',
  PRIMARY KEY (`miu_id`)  COMMENT '',
  INDEX `fk_miu_details_utilities1_idx` (`site_id` ASC)  COMMENT '',
  INDEX `i_miu_details_next_sync` (`next_sync_time` ASC)  COMMENT '',
  CONSTRAINT `fk_miu_details_utilities1`
    FOREIGN KEY (`site_id`)
    REFERENCES `mdce`.`site_list` (`site_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.site_list
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`site_list` (
  `site_id` INT UNSIGNED NOT NULL COMMENT '',
  `name` VARCHAR(100) NULL COMMENT '',
  `last_data_request` DATETIME NULL COMMENT '',
  PRIMARY KEY (`site_id`)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.tag_list
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`tag_list` (
  `tag_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `name` VARCHAR(50) NULL COMMENT '',
  `public` TINYINT(1) NULL COMMENT '',
  PRIMARY KEY (`tag_id`)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.miu_tags
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`miu_tags` (
  `miu_id` INT UNSIGNED NOT NULL COMMENT '',
  `tag_id` INT UNSIGNED NOT NULL COMMENT '',
  INDEX `fk_miu_tags_miu_details_idx` (`miu_id` ASC)  COMMENT '',
  INDEX `fk_miu_tags_tag_list_idx` (`tag_id` ASC)  COMMENT '',
  PRIMARY KEY (`tag_id`, `miu_id`)  COMMENT '',
  CONSTRAINT `fk_miu_tags_miu_details`
    FOREIGN KEY (`miu_id`)
    REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_miu_tags_tag_list`
    FOREIGN KEY (`tag_id`)
    REFERENCES `mdce`.`tag_list` (`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.site_tags
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`site_tags` (
  `site_id` INT UNSIGNED NOT NULL COMMENT '',
  `tag_id` INT UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`site_id`, `tag_id`)  COMMENT '',
  INDEX `fk_utility_tags_tag_list1_idx` (`tag_id` ASC)  COMMENT '',
  CONSTRAINT `fk_utility_tags_tag_list1`
    FOREIGN KEY (`tag_id`)
    REFERENCES `mdce`.`tag_list` (`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_utility_tags_utilities1`
    FOREIGN KEY (`site_id`)
    REFERENCES `mdce`.`site_list` (`site_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.user_list
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`user_list` (
  `user_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `user_name` VARCHAR(45) NULL COMMENT '',
  `user_email` VARCHAR(45) NULL COMMENT '',
  `user_password` BLOB NULL COMMENT '',
  `user_salt` BLOB NULL COMMENT '',
  `user_pbkdf2_inter` INT NULL COMMENT '',
  `user_level` ENUM('Distributor', 'SiteOperator', 'MsocOperator', 'MsocAdmin') NULL COMMENT '',
  `user_alert_system_warning` TINYINT(1) NULL COMMENT '',
  `user_alert_system_error` TINYINT(1) NULL COMMENT '',
  `user_alert_cmiu_warning` TINYINT(1) NULL COMMENT '',
  `user_alert_cmiu_error` TINYINT(1) NULL COMMENT '',
  PRIMARY KEY (`user_id`)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.user_tags
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`user_tags` (
  `user_id` INT UNSIGNED NOT NULL COMMENT '',
  `tag_id` INT UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`user_id`, `tag_id`)  COMMENT '',
  INDEX `fk_user_tags_tag_list_idx` (`tag_id` ASC)  COMMENT '',
  CONSTRAINT `fk_user_tags_user_list`
    FOREIGN KEY (`user_id`)
    REFERENCES `mdce`.`user_list` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_tags_tag_list`
    FOREIGN KEY (`tag_id`)
    REFERENCES `mdce`.`tag_list` (`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.user_dashboards_kpis
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`user_dashboards_kpis` (
  `user_dashboards_kpis_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `user_dashboard_id` INT UNSIGNED NOT NULL COMMENT '',
  `kpi_id` INT UNSIGNED NOT NULL COMMENT '',
  `grid_x` INT NULL COMMENT '',
  `grid_y` INT NULL COMMENT '',
  `colspan` INT NULL COMMENT '',
  `rowspan` INT NULL COMMENT '',
  `params_override` MEDIUMTEXT NULL COMMENT '',
  PRIMARY KEY (`user_dashboards_kpis_id`)  COMMENT '',
  INDEX `fk_user_dashboards_dashboard_list_idx` (`kpi_id` ASC)  COMMENT '',
  INDEX `fk_user_views_dashboards_user_views1_idx` (`user_dashboard_id` ASC)  COMMENT '',
  CONSTRAINT `fk_user_dashboards_dashboard_list`
    FOREIGN KEY (`kpi_id`)
    REFERENCES `mdce`.`kpi_list` (`kpi_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_views_dashboards_user_views1`
    FOREIGN KEY (`user_dashboard_id`)
    REFERENCES `mdce`.`user_dashboards` (`user_dashboard_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.kpi_list
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`kpi_list` (
  `kpi_id` INT UNSIGNED NOT NULL COMMENT '',
  `name` VARCHAR(45) NULL COMMENT '',
  `params` MEDIUMTEXT NULL COMMENT '',
  `config` MEDIUMTEXT NULL COMMENT '',
  `kpi_sql` MEDIUMTEXT NULL COMMENT '',
  `grid_config` MEDIUMTEXT NULL COMMENT '',
  `grid_sql` VARCHAR(45) NULL COMMENT '',
  `details_grid_config` MEDIUMTEXT NULL COMMENT '',
  `details_grid_sql` MEDIUMTEXT NULL COMMENT '',
  `last_update_sql` MEDIUMTEXT NULL COMMENT '',
  PRIMARY KEY (`kpi_id`)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.user_dashboards_kpis_default_tags
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`user_dashboards_kpis_default_tags` (
  `user_dashboards_kpis_id` INT UNSIGNED NOT NULL COMMENT '',
  `tag_id` INT UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`user_dashboards_kpis_id`, `tag_id`)  COMMENT '',
  INDEX `fk_user_dashboards_default_tags_tag_list_idx` (`tag_id` ASC)  COMMENT '',
  CONSTRAINT `fk_user_dashboards_default_tags_user_views_dashboards`
    FOREIGN KEY (`user_dashboards_kpis_id`)
    REFERENCES `mdce`.`user_dashboards_kpis` (`user_dashboards_kpis_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_views_dashboards_default_tags_tag_list`
    FOREIGN KEY (`tag_id`)
    REFERENCES `mdce`.`tag_list` (`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.event_list
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`event_list` (
  `event_id` INT UNSIGNED NOT NULL COMMENT '',
  `name` VARCHAR(45) NULL COMMENT '',
  `start_date_time` DATETIME NULL COMMENT '',
  `interval_type` VARCHAR(1) NULL COMMENT '',
  `interval_value` INT NULL COMMENT '',
  `event_sql` MEDIUMTEXT NULL COMMENT '',
  `last_run_time` DATETIME NULL COMMENT '',
  `next_run_time` DATETIME NULL COMMENT '',
  PRIMARY KEY (`event_id`)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.event_data
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`event_data` (
  `event_data_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `event_id` INT UNSIGNED NOT NULL COMMENT '',
  `n1` DECIMAL(10,0) NULL COMMENT '',
  `n2` DECIMAL(10,0) NULL COMMENT '',
  `d1` DATE NULL COMMENT '',
  `d2` DATE NULL COMMENT '',
  `dt1` DATETIME NULL COMMENT '',
  `dt2` DATETIME NULL COMMENT '',
  `s1` VARCHAR(100) NULL COMMENT '',
  `s2` VARCHAR(100) NULL COMMENT '',
  `i1` INT NULL COMMENT '',
  `i2` INT NULL COMMENT '',
  `u1` INT UNSIGNED NULL COMMENT '',
  `u2` INT NULL COMMENT '',
  INDEX `fk_event_data_event_list1_idx` (`event_id` ASC)  COMMENT '',
  PRIMARY KEY (`event_data_id`)  COMMENT '',
  CONSTRAINT `fk_event_data_event_list1`
    FOREIGN KEY (`event_id`)
    REFERENCES `mdce`.`event_list` (`event_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.user_dashboards
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`user_dashboards` (
  `user_dashboard_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `user_id` INT UNSIGNED NOT NULL COMMENT '',
  `view_name` VARCHAR(45) NULL COMMENT '',
  `public` TINYINT(1) NULL COMMENT '',
  PRIMARY KEY (`user_dashboard_id`, `user_id`)  COMMENT '',
  INDEX `fk_user_dashboards_user_list_idx` (`user_id` ASC)  COMMENT '',
  CONSTRAINT `fk_user_dashboards_user_list0`
    FOREIGN KEY (`user_id`)
    REFERENCES `mdce`.`user_list` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.miu_data_usage
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`miu_data_usage` (
  `miu_id` INT UNSIGNED NOT NULL COMMENT '',
  `data_usage_date` DATE NOT NULL COMMENT '',
  `bytes_used_today` INT NULL COMMENT '',
  `bytes_used_month_to_date` INT NULL COMMENT '',
  PRIMARY KEY (`miu_id`, `data_usage_date`)  COMMENT '',
  CONSTRAINT `fk_miu_data_usage_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.miu_commands
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`miu_commands` (
  `miu_command_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `miu_id` INT UNSIGNED NOT NULL COMMENT '',
  `command_type` INT NULL COMMENT '',
  `command_params` BLOB NULL COMMENT '',
  `command_entered_time` DATETIME NULL COMMENT 'Time when command has been send to broker',
  `command_fulfilled_time` DATETIME NULL COMMENT 'Time when command has been acked by CMIU',
  `status` ENUM('created', 'queued', 'committed', 'recalled', 'cancelled','rejected') NULL COMMENT 'Status of this command item:\nCREATED --> QUEUED --> COMMITTED\n(QUEUED) -> RECALLED -> CANCELLED',
  `source_device_type` VARCHAR(45) NULL COMMENT '',
  `target_device_type` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`miu_command_id`, `miu_id`)  COMMENT '',
  INDEX `fk_miu_commands_miu_details1_idx` (`miu_id` ASC)  COMMENT '',
  CONSTRAINT `fk_miu_commands_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.audit_list
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`audit_list` (
  `audit_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `audit_type` INT NULL COMMENT '',
  `audit_params` INT NULL COMMENT '',
  `audit_user_name` VARCHAR(45) NULL COMMENT '',
  `audit_time` DATETIME NULL COMMENT '',
  `miu_id` INT UNSIGNED NULL COMMENT '',
  `old_values` VARCHAR(1024) NULL COMMENT '',
  `new_values` VARCHAR(1024) NULL COMMENT '',
  PRIMARY KEY (`audit_id`)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.miu_heard_packets
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`miu_heard_packets` (
  `miu_id` INT UNSIGNED NOT NULL COMMENT '',
  `heard_time` DATETIME NOT NULL COMMENT '',
  `miu_packet_count` INT NULL COMMENT '',
  `avg_rssi` DECIMAL(6,3) NULL COMMENT 'Units = dBm',
  `min_rssi` DECIMAL(6,2) NULL COMMENT 'Units = dBm',
  `max_rssi` DECIMAL(6,2) NULL COMMENT 'Units = dBm',
  `ber` DECIMAL(3,1) NULL COMMENT 'Units = %',
  `processor_reset_counter` INT NULL COMMENT '',
  `register_time` INT NULL COMMENT 'Units = millis',
  `register_time_to_activate_context` INT NULL COMMENT 'Units = millis',
  `register_time_connected` INT NULL COMMENT 'Units = millis',
  `register_time_to_transfer_packet` INT NULL COMMENT 'Units = millis',
  `disconnect_time` INT NULL COMMENT 'Units = millis',
  `mag_swipe_counter` INT NULL COMMENT '',
  `battery_capacity` DECIMAL(6,2) NULL COMMENT 'Units = mV',
  `temperature` DECIMAL(3,1) NULL COMMENT 'Units = degrees Celsius',
  INDEX `fk_miu_heard_packets_miu_details1_idx` (`miu_id` ASC)  COMMENT '',
  INDEX `i_miu_heard_packets_time` (`heard_time` ASC)  COMMENT '',
  CONSTRAINT `fk_miu_heard_packets_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.user_public_dashboards
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`user_public_dashboards` (
  `user_id` INT UNSIGNED NOT NULL COMMENT '',
  `user_dashboard_id` INT UNSIGNED NOT NULL COMMENT '',
  `dashboard_order` INT NULL COMMENT '',
  PRIMARY KEY (`user_id`, `user_dashboard_id`)  COMMENT '',
  INDEX `fk_user_public_dashboards_user_dashboards_idx` (`user_dashboard_id` ASC)  COMMENT '',
  CONSTRAINT `fk_user_public_dashboards_user_list`
    FOREIGN KEY (`user_id`)
    REFERENCES `mdce`.`user_list` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_public_dashboards_user_dashboards`
    FOREIGN KEY (`user_dashboard_id`)
    REFERENCES `mdce`.`user_dashboards` (`user_dashboard_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.cmiu_config_set
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`cmiu_config_set` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `name` VARCHAR(128) NOT NULL COMMENT '',
  `billing_plan_id` INT NOT NULL COMMENT '',
  `reporting_start_mins` INT NULL COMMENT '',
  `reporting_interval_mins` INT NULL COMMENT '',
  `reporting_number_of_retries` INT NULL COMMENT '',
  `reporting_transmit_windows_mins` INT NULL COMMENT '',
  `reporting_quiet_start_mins` INT NULL COMMENT '',
  `reporting_quiet_end_mins` INT NULL COMMENT '',
  `reporting_sync_warning_mins` INT NULL COMMENT '',
  `reporting_sync_error_mins` INT NULL COMMENT '',
  `recording_start_time_mins` INT NULL COMMENT '',
  `recording_interval_mins` INT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.cmiu_config_mgmt
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`cmiu_config_mgmt` (
  `miu_id` INT UNSIGNED NOT NULL COMMENT '',
  `current_config` INT UNSIGNED NOT NULL COMMENT '',
  `current_config_apply_date` DATETIME NULL COMMENT '',
  `default_config` INT UNSIGNED NOT NULL COMMENT '',
  `planned_config` INT UNSIGNED NULL COMMENT '',
  `planned_config_apply_date` DATETIME NULL COMMENT '',
  `planned_config_revert_date` DATETIME NULL COMMENT '',
  `reported_config` INT UNSIGNED NOT NULL COMMENT '',
  `reported_config_date` DATETIME NULL COMMENT '',
  INDEX `fk_cmiu_configuration_management_miu_details1_idx` (`miu_id` ASC)  COMMENT '',
  INDEX `fk_cmiu_configuration_management_cmiu_configuration_set1_idx` (`current_config` ASC)  COMMENT '',
  INDEX `fk_cmiu_configuration_management_cmiu_configuration_set2_idx` (`default_config` ASC)  COMMENT '',
  INDEX `fk_cmiu_configuration_management_cmiu_configuration_set3_idx` (`planned_config` ASC)  COMMENT '',
  INDEX `fk_cmiu_configuration_management_cmiu_configuration_set4_idx` (`reported_config` ASC)  COMMENT '',
  CONSTRAINT `fk_cmiu_configuration_management_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cmiu_configuration_management_cmiu_configuration_set1`
    FOREIGN KEY (`current_config`)
    REFERENCES `mdce`.`cmiu_config_set` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cmiu_configuration_management_cmiu_configuration_set2`
    FOREIGN KEY (`default_config`)
    REFERENCES `mdce`.`cmiu_config_set` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cmiu_configuration_management_cmiu_configuration_set3`
    FOREIGN KEY (`planned_config`)
    REFERENCES `mdce`.`cmiu_config_set` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cmiu_configuration_management_cmiu_configuration_set4`
    FOREIGN KEY (`reported_config`)
    REFERENCES `mdce`.`cmiu_config_set` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.cmiu_config_history
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`cmiu_config_history` (
  `miu_id` INT UNSIGNED NOT NULL COMMENT '',
  `cmiu_config` INT UNSIGNED NOT NULL COMMENT '',
  `timestamp` DATETIME NOT NULL COMMENT '',
  `received_from_cmiu` TINYINT(1) NULL COMMENT 'This config has been implemented by the CMIU. Marked TRUE if the CMIU config received over MQTT matches.',
  `change_note` VARCHAR(200) NULL COMMENT 'Capture user input notes from mdce-web CMIU config',
  INDEX `fk_cmiu_config_history_cmiu_config_set1_idx` (`cmiu_config` ASC)  COMMENT '',
  INDEX `fk_cmiu_config_history_miu_details1_idx` (`miu_id` ASC)  COMMENT '',
  CONSTRAINT `fk_cmiu_config_history_cmiu_config_set1`
    FOREIGN KEY (`cmiu_config`)
    REFERENCES `mdce`.`cmiu_config_set` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cmiu_config_history_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.cns_credentials
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`cns_credentials` (
  `cns_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `user` VARCHAR(45) NOT NULL COMMENT '',
  `password` VARCHAR(45) NOT NULL COMMENT '',
  `environment` VARCHAR(45) NOT NULL COMMENT '',
  `is_active` TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'set to 0 if the password has been locked and pending action from VzW support',
  `is_primary` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'The primary password the environment should use',
  `last_password_changed_time` DATETIME NULL COMMENT 'The last time when the password is change. This is used to keep track of password rotation.',
  `is_callback_registered` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Check whether callback has been registered to this account.',
  PRIMARY KEY (`cns_id`)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.cellular_device_details
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`cellular_device_details` (
  `cellular_device_details_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `miu_id` INT UNSIGNED NOT NULL COMMENT '',
  `iccid` VARCHAR(20) NULL COMMENT '',
  `imei` VARCHAR(16) NULL COMMENT '',
  `msisdn` VARCHAR(20) NULL COMMENT 'MSISDN seems to be the only identifier field when callback is sent on changing device provisioning state',
  `imsi` VARCHAR(16) NULL COMMENT '',
  `provisioning_state` TINYINT(2) NULL COMMENT 'provisioning state of the cellular device:\n- unregistered (not detected in VzW network UWS account yet)\n- pre active\n- activated\n- suspended\n- deactivated\n- pending-activate\n- pending-suspend\n- pending-restore\n- pending-deactivate',
  `last_update_time` DATETIME NULL COMMENT 'Datetime when this record has been updated',
  `modem_config_vendor` VARCHAR(45) NULL COMMENT '',
  `modem_config_firmware` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`cellular_device_details_id`)  COMMENT '',
  INDEX `fk_cellular_device_details_miu_details1_idx` (`miu_id` ASC)  COMMENT '',
  CONSTRAINT `fk_cellular_device_details_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.miu_monthly_stats
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`miu_monthly_stats` (
  `miu_id` INT UNSIGNED NOT NULL COMMENT '',
  `year` INT NOT NULL COMMENT '',
  `month` INT NOT NULL COMMENT '',
  `num_expected` INT NULL COMMENT '',
  `num_heard` INT NULL COMMENT '',
  `avg_rssi` DECIMAL(6,2) NULL COMMENT '',
  `min_rssi` INT NULL COMMENT '',
  `max_rssi` INT NULL COMMENT '',
  `avg_ber` DECIMAL(3,3) NULL COMMENT '',
  `min_ber` DECIMAL(3,1) NULL COMMENT '',
  `max_ber` DECIMAL(3,1) NULL COMMENT '',
  `avg_processor_reset_counter` DECIMAL(6,2) NULL COMMENT '',
  `min_processor_reset_counter` INT NULL COMMENT '',
  `max_processor_reset_counter` INT NULL COMMENT '',
  `avg_register_time` DECIMAL(6,2) NULL COMMENT '',
  `min_register_time` INT NULL COMMENT '',
  `max_register_time` INT NULL COMMENT '',
  `avg_register_time_to_activate_context` DECIMAL(6,2) NULL COMMENT '',
  `min_register_time_to_activate_context` INT NULL COMMENT '',
  `max_register_time_to_activate_context` INT NULL COMMENT '',
  `avg_register_time_connected` DECIMAL(6,2) NULL COMMENT '',
  `min_register_time_connected` INT NULL COMMENT '',
  `max_register_time_connected` INT NULL COMMENT '',
  `avg_register_time_to_transfer_packet` DECIMAL(6,2) NULL COMMENT '',
  `min_register_time_to_transfer_packet` INT NULL COMMENT '',
  `max_register_time_to_transfer_packet` INT NULL COMMENT '',
  `avg_disconnect_time` DECIMAL(6,2) NULL COMMENT '',
  `min_disconnect_time` INT NULL COMMENT '',
  `max_disconnect_time` INT NULL COMMENT '',
  `avg_mag_swipe_counter` DECIMAL(6,2) NULL COMMENT '',
  `min_mag_swipe_counter` INT NULL COMMENT '',
  `max_mag_swipe_counter` INT NULL COMMENT '',
  `avg_battery_capacity` DECIMAL(6,2) NULL COMMENT '',
  `min_battery_capacity` DECIMAL(6,2) NULL COMMENT '',
  `max_battery_capacity` DECIMAL(6,2) NULL COMMENT '',
  `avg_temperature` DECIMAL(3,3) NULL COMMENT '',
  `min_temperature` DECIMAL(3,1) NULL COMMENT '',
  `max_temperature` DECIMAL(3,1) NULL COMMENT '',
  PRIMARY KEY (`miu_id`, `year`, `month`)  COMMENT '',
  INDEX `i_miu_monthly_stats_date` (`year` ASC, `month` ASC)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.miu_daily_stats
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`miu_daily_stats` (
  `miu_id` INT UNSIGNED NOT NULL COMMENT '',
  `heard_date` DATE NOT NULL COMMENT '',
  `num_expected` INT NULL COMMENT '',
  `num_heard` INT NULL COMMENT '',
  `avg_rssi` DECIMAL(6,3) NULL COMMENT '',
  `min_rssi` DECIMAL(6,2) NULL COMMENT '',
  `max_rssi` DECIMAL(6,2) NULL COMMENT '',
  `avg_ber` DECIMAL(3,3) NULL COMMENT '',
  `min_ber` DECIMAL(3,1) NULL COMMENT '',
  `max_ber` DECIMAL(3,1) NULL COMMENT '',
  `avg_processor_reset_counter` DECIMAL(6,2) NULL COMMENT '',
  `min_processor_reset_counter` INT NULL COMMENT '',
  `max_processor_reset_counter` INT NULL COMMENT '',
  `avg_register_time` DECIMAL(6,2) NULL COMMENT '',
  `min_register_time` INT NULL COMMENT '',
  `max_register_time` INT NULL COMMENT '',
  `avg_register_time_to_activate_context` DECIMAL(6,2) NULL COMMENT '',
  `min_register_time_to_activate_context` INT NULL COMMENT '',
  `max_register_time_to_activate_context` INT NULL COMMENT '',
  `avg_register_time_connected` DECIMAL(6,2) NULL COMMENT '',
  `min_register_time_connected` INT NULL COMMENT '',
  `max_register_time_connected` INT NULL COMMENT '',
  `avg_register_time_to_transfer_packet` DECIMAL(6,2) NULL COMMENT '',
  `min_register_time_to_transfer_packet` INT NULL COMMENT '',
  `max_register_time_to_transfer_packet` INT NULL COMMENT '',
  `avg_disconnect_time` DECIMAL(6,2) NULL COMMENT '',
  `min_disconnect_time` INT NULL COMMENT '',
  `max_disconnect_time` INT NULL COMMENT '',
  `avg_mag_swipe_counter` DECIMAL(6,2) NULL COMMENT '',
  `min_mag_swipe_counter` INT NULL COMMENT '',
  `max_mag_swipe_counter` INT NULL COMMENT '',
  `avg_battery_capacity` DECIMAL(6,2) NULL COMMENT '',
  `min_battery_capacity` DECIMAL(6,2) NULL COMMENT '',
  `max_battery_capacity` DECIMAL(6,2) NULL COMMENT '',
  `avg_temperature` DECIMAL(3,3) NULL COMMENT '',
  `min_temperature` DECIMAL(3,1) NULL COMMENT '',
  `max_temperature` DECIMAL(3,1) NULL COMMENT '',
  PRIMARY KEY (`miu_id`, `heard_date`)  COMMENT '',
  INDEX `i_miu_daily_stats` (`heard_date` ASC)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.cmiu_registered_images
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`cmiu_registered_images` (
  `cmiu_image_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `name` VARCHAR(45) NULL COMMENT '',
  `s3_path` VARCHAR(255) NULL COMMENT 's3 source path/URL for image, excluding the https://s3..../bucketname/',
  `register_date` DATETIME NULL COMMENT '',
  `type` INT NULL COMMENT '',
  `image_size` INT NULL COMMENT '',
  `start_address` INT NULL COMMENT '',
  PRIMARY KEY (`cmiu_image_id`)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.mdce_api_tokens
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`mdce_api_tokens` (
  `token` BINARY(8) NOT NULL COMMENT '',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `partner_key` BINARY(16) NOT NULL COMMENT '',
  `originating_ip` VARCHAR(45) NOT NULL COMMENT '',
  `expiry` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `site_id` INT NULL COMMENT '',
  PRIMARY KEY (`token`)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.gateway_details
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`gateway_details` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `gateway_id` VARCHAR(45) NOT NULL COMMENT '',
  `site_id` INT UNSIGNED NULL COMMENT '',
  `last_insert_date` DATETIME NULL COMMENT '',
  `first_insert_date` DATETIME NULL COMMENT '',
  `last_heard_time` DATETIME NULL COMMENT '',
  `next_sync_time` DATETIME NULL COMMENT '',
  `next_sync_time_warning` DATETIME NULL COMMENT '',
  `next_sync_time_error` DATETIME NULL COMMENT '',
  `gateway_active` CHAR NOT NULL DEFAULT 'Y' COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_gateway_details_site_list1_idx` (`site_id` ASC)  COMMENT '',
  CONSTRAINT `fk_gateway_details_site_list1`
    FOREIGN KEY (`site_id`)
    REFERENCES `mdce`.`site_list` (`site_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.ref_data_utilities
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`ref_data_utilities` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `site_id` INT UNSIGNED NOT NULL COMMENT '',
  `info_id` INT UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `ref_data_utilties_info_id_fk_idx` (`info_id` ASC)  COMMENT '',
  CONSTRAINT `ref_data_utilties_info_id_fk`
    FOREIGN KEY (`info_id`)
    REFERENCES `mdce`.`ref_data_info` (`ref_data_info_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.ref_data_info
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`ref_data_info` (
  `ref_data_info_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `account_id` VARCHAR(20) NULL COMMENT '',
  `system_id` VARCHAR(20) NULL COMMENT '',
  `account_name` VARCHAR(100) NULL COMMENT '',
  `address1` VARCHAR(45) NULL COMMENT '',
  `address2` VARCHAR(45) NULL COMMENT '',
  `address3` VARCHAR(45) NULL COMMENT '',
  `city` VARCHAR(45) NULL COMMENT '',
  `state` VARCHAR(45) NULL COMMENT '',
  `postal_code` VARCHAR(45) NULL COMMENT '',
  `country` VARCHAR(45) NULL COMMENT '',
  `main_phone` VARCHAR(45) NULL COMMENT '',
  `fax` VARCHAR(45) NULL COMMENT '',
  `sales_territory_owner` VARCHAR(45) NULL COMMENT '',
  `account_manager` VARCHAR(45) NULL COMMENT '',
  `regional_manager` VARCHAR(45) NULL COMMENT '',
  `type` VARCHAR(45) NULL COMMENT '',
  `sub_type` VARCHAR(45) NULL COMMENT '',
  `status` VARCHAR(45) NULL COMMENT '',
  `primary_contact_id` INT UNSIGNED NULL COMMENT '',
  `parent_customer_number` VARCHAR(45) NULL COMMENT '',
  `customer_number` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`ref_data_info_id`)  COMMENT '',
  INDEX `ref_data_info_contact_ fk_idx` (`primary_contact_id` ASC)  COMMENT '',
  CONSTRAINT `ref_data_info_contact_fk`
    FOREIGN KEY (`primary_contact_id`)
    REFERENCES `mdce`.`ref_data_info_contact` (`ref_data_info_contact_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.ref_data_info_contact
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`ref_data_info_contact` (
  `ref_data_info_contact_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `contact_name` VARCHAR(45) NULL COMMENT '',
  `work_phone` VARCHAR(20) NULL COMMENT '',
  `title` VARCHAR(80) NULL COMMENT '',
  PRIMARY KEY (`ref_data_info_contact_id`)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.ref_data_secondary_contact_info
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`ref_data_secondary_contact_info` (
  `info_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `contact_id` INT UNSIGNED NOT NULL COMMENT '',
  INDEX `ref_data_info_pk_fk_idx` (`info_id` ASC)  COMMENT '',
  INDEX `ref_data_info_contact_pk_fk_idx` (`contact_id` ASC)  COMMENT '',
  CONSTRAINT `ref_data_info_pk_fk`
    FOREIGN KEY (`info_id`)
    REFERENCES `mdce`.`ref_data_info` (`ref_data_info_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ref_data_info_contact_pk_fk`
    FOREIGN KEY (`contact_id`)
    REFERENCES `mdce`.`ref_data_info_contact` (`ref_data_info_contact_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.ref_data_distributer
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`ref_data_distributer` (
  `ref_data_distributer_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `customer_number` VARCHAR(45) NULL COMMENT '',
  `info_id` INT UNSIGNED NULL COMMENT '',
  PRIMARY KEY (`ref_data_distributer_id`)  COMMENT '',
  INDEX `ref_data_info_id_idx` (`info_id` ASC)  COMMENT '',
  CONSTRAINT `ref_data_info_id`
    FOREIGN KEY (`info_id`)
    REFERENCES `mdce`.`ref_data_info` (`ref_data_info_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = big5;

-- ----------------------------------------------------------------------------
-- Table mdce.ref_data_customer_number
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`ref_data_customer_number` (
  `customer_number_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `info_id` INT UNSIGNED NOT NULL COMMENT '',
  `customer_number` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`customer_number_id`)  COMMENT '',
  INDEX `customer_number_info_fk_idx` (`info_id` ASC)  COMMENT '',
  CONSTRAINT `customer_number_info_fk`
    FOREIGN KEY (`info_id`)
    REFERENCES `mdce`.`ref_data_info` (`ref_data_info_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.site_user
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`site_user` (
  `site_list_site_id` INT UNSIGNED NOT NULL COMMENT '',
  `user_list_user_id` INT UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`site_list_site_id`, `user_list_user_id`)  COMMENT '',
  INDEX `fk_site_list_has_user_list_user_list1_idx` (`user_list_user_id` ASC)  COMMENT '',
  INDEX `fk_site_list_has_user_list_site_list1_idx` (`site_list_site_id` ASC)  COMMENT '',
  CONSTRAINT `fk_site_list_has_user_list_site_list1`
    FOREIGN KEY (`site_list_site_id`)
    REFERENCES `mdce`.`site_list` (`site_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_site_list_has_user_list_user_list1`
    FOREIGN KEY (`user_list_user_id`)
    REFERENCES `mdce`.`user_list` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.alert_alerts
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`alert_alerts` (
  `alert_alerts_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `alert_source` VARCHAR(250) NOT NULL COMMENT '',
  `alert_creation_date` DATETIME NOT NULL COMMENT '',
  `alert_state` ENUM('new', 'handled', 'stale', 'cleared') NOT NULL COMMENT '',
  `alert_level` ENUM('warning', 'error') NOT NULL COMMENT '',
  `alert_ticket_id` VARCHAR(45) NULL COMMENT '',
  `alert_latest_message` VARCHAR(1000) NULL COMMENT '',
  `alert_last_update_date` DATETIME NULL COMMENT '',
  PRIMARY KEY (`alert_alerts_id`)  COMMENT '',
  UNIQUE INDEX `alert_source_UNIQUE` (`alert_source` ASC)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.alert_log
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`alert_log` (
  `alert_log_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `alert_log_date` DATETIME NOT NULL COMMENT '',
  `alert_log_message` VARCHAR(1000) NOT NULL COMMENT '',
  `alert_alerts_id` INT UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`alert_log_id`)  COMMENT '',
  INDEX `fk_alert_log_alert_alerts1_idx` (`alert_alerts_id` ASC)  COMMENT '',
  CONSTRAINT `fk_alert_log_alert_alerts1`
    FOREIGN KEY (`alert_alerts_id`)
    REFERENCES `mdce`.`alert_alerts` (`alert_alerts_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.gateway_config
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`gateway_config` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `gateway_db_id` INT NOT NULL COMMENT '',
  `from_date` TIMESTAMP NULL COMMENT '',
  `name` VARCHAR(45) NULL COMMENT '',
  `site_id` INT NULL COMMENT '',
  `MinimumStorageTime` INT NULL COMMENT '',
  `TransferInterval` INT NULL COMMENT '',
  `TransferOffsetTime` INT NULL COMMENT '',
  `FirstReportingStartTime` INT NULL COMMENT '',
  `FirstReportingEndTime` INT NULL COMMENT '',
  `FirstReportingTimeInterval` INT NULL COMMENT '',
  `SecondReportingStartTime` INT NULL COMMENT '',
  `SecondReportingEndTime` INT NULL COMMENT '',
  `SecondReportingTimeInterval` INT NULL COMMENT '',
  `ThirdReportingStartTime` INT NULL COMMENT '',
  `ThirdReportingEndTime` INT NULL COMMENT '',
  `ThirdReportingTimeInterval` INT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.gateway_heard_packets
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`gateway_heard_packets` (
  `gateway_id` VARCHAR(45) NOT NULL COMMENT '',
  `heard_time` DATETIME NOT NULL COMMENT '',
  `mius_heard` INT NOT NULL COMMENT '',
  `ook_packets_heard` INT NOT NULL COMMENT '',
  `fsk_packets_heard` INT NOT NULL COMMENT '',
  `config_packets_heard` INT NOT NULL COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.gateway_monthly_stats
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`gateway_monthly_stats` (
  `gateway_id` VARCHAR(45) NOT NULL COMMENT '',
  `year` INT NOT NULL COMMENT '',
  `month` INT NOT NULL COMMENT '',
  `num_expected` INT NOT NULL COMMENT '',
  `num_heard` INT NOT NULL COMMENT '',
  `avg_mius_heard` DECIMAL(6,2) NULL COMMENT '',
  `min_mius_heard` INT NULL COMMENT '',
  `max_mius_heard` INT NULL COMMENT '',
  `avg_ook_packets_heard` DECIMAL(6,2) NULL COMMENT '',
  `min_ook_packets_heard` INT NULL COMMENT '',
  `max_ook_packets_heard` INT NULL COMMENT '',
  `avg_fsk_packets_heard` DECIMAL(6,2) NULL COMMENT '',
  `min_fsk_packets_heard` INT NULL COMMENT '',
  `max_fsk_packets_heard` INT NULL COMMENT '',
  `avg_config_packets_heard` DECIMAL(6,2) NULL COMMENT '',
  `min_config_packets_heard` INT NULL COMMENT '',
  `max_config_packets_heard` INT NULL COMMENT '',
  PRIMARY KEY (`gateway_id`, `year`, `month`)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Table mdce.mdce_settings
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`mdce_settings` (
  `key` VARCHAR(100) NOT NULL COMMENT '',
  `int_value` INT NULL COMMENT '',
  `string_value` VARCHAR(1024) NULL COMMENT '',
  PRIMARY KEY (`key`)  COMMENT '')
ENGINE = InnoDB;

-- ----------------------------------------------------------------------------
-- Routine mdce.split_list
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `mdce`$$
create procedure split_list (list_string long varchar, string_delimiter varchar(255))
begin
    DECLARE iRow INT;
    DECLARE iPos INT;
    DECLARE iPos2 INT;

    DROP TEMPORARY TABLE IF EXISTS temp_split_list;
    CREATE TEMPORARY TABLE temp_split_list
                          (row_number INT,
                           row_value VARCHAR(4000)
                          ) ENGINE=Memory;
                          
	SET iRow  = 0;
    SET iPos  = 1;
    SET iPos2 = Locate(string_delimiter, list_string, iPos);
    
    WHILE iPos2 > 0 DO
    
        SET iRow = iRow + 1;
        INSERT INTO temp_split_list (row_number,
                                     row_value)
					         VALUES (iRow,
                                     substring(list_string, iPos, iPos2 - iPos));
                                 
		SET iPos = iPos2 + length(string_delimiter);
        SET iPos2 = Locate(string_delimiter, list_string, iPos);
    END WHILE;
    
    /*
     * add any text left over
     */
	IF iPos <= length(list_string) THEN
        SET iRow = iRow + 1;
        INSERT INTO temp_split_list (row_number,
                                     row_value)
					         VALUES (iRow,
                                     substr(substring(list_string, iPos), 1, 4000));
	END IF;
    
end;$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine mdce.build_tag_filter
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `mdce`$$
create procedure build_tag_filter (tag_list long varchar)
begin

    /*
     * build table to store results
     */
	DROP TEMPORARY TABLE IF EXISTS temp_tag_mius;
    CREATE TEMPORARY TABLE temp_tag_mius
                          (miu_id INT UNSIGNED,
                           PRIMARY KEY (miu_id)
                          ) ENGINE=Memory;
                          
	/*
     * if we have a tag list, get the distinct MIU's in it
     */
	IF length(tag_list) > 0 THEN
		/*
		 * split the list
		 */
		CALL split_list(tag_list, ',');
		
		/*
		 * now insert any miu's for an utilities that have this tag
		 */
		INSERT INTO temp_tag_mius
				   (miu_id)
				   (SELECT DISTINCT m.miu_id
					  FROM miu_details m,
						   utility_tags t,
                           tag_list tl,
						   temp_split_list l
					 WHERE m.utility_id = t.utility_id AND
						   t.tag_id = tl.tag_id AND
                           tl.name = l.row_value AND
                           m.meter_active = 'Y');
   
		/*
		 * now insert any miu's that have this tag for themselves
		 */
		INSERT IGNORE INTO temp_tag_mius
						  (miu_id)
						  (SELECT DISTINCT m.miu_id
							 FROM miu_details m,
								  miu_tags t,
                                  tag_list tl,
								  temp_split_list l
							WHERE m.miu_id = t.miu_id AND
						          t.tag_id = tl.tag_id AND
                                  tl.name = l.row_value AND
                                   m.meter_active = 'Y');
   ELSE
      /*
       * no tag list, so add all MIU's
       */
		INSERT INTO temp_tag_mius
				   (miu_id)
				   (SELECT miu_id
					  FROM miu_details m 
				     WHERE m.meter_active = 'Y');
	END IF;						
									
end;$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine mdce.split_list_int
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `mdce`$$
create procedure split_list_int (list_string long varchar, string_delimiter varchar(255))
begin
    DECLARE iRow INT;
    DECLARE iPos INT;
    DECLARE iPos2 INT;

    DROP TEMPORARY TABLE IF EXISTS temp_split_list;
    CREATE TEMPORARY TABLE temp_split_list
                          (row_number INT,
                           row_value INT
                          ) ENGINE=Memory;
                          
	SET iRow  = 0;
    SET iPos  = 1;
    SET iPos2 = Locate(string_delimiter, list_string, iPos);
    
    WHILE iPos2 > 0 DO
    
        SET iRow = iRow + 1;
        INSERT INTO temp_split_list (row_number,
                                     row_value)
					         VALUES (iRow,
                                     substring(list_string, iPos, iPos2 - iPos));
                                 
		SET iPos = iPos2 + length(string_delimiter);
        SET iPos2 = Locate(string_delimiter, list_string, iPos);
    END WHILE;
    
    /*
     * add any text left over
     */
	IF iPos <= length(list_string) THEN
        SET iRow = iRow + 1;
        INSERT INTO temp_split_list (row_number,
                                     row_value)
					         VALUES (iRow,
                                     substr(substring(list_string, iPos), 1, 4000));
	END IF;
    
end;$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine mdce.split_list_uint
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `mdce`$$
create procedure split_list_uint (list_string long varchar, string_delimiter varchar(255))
begin
    DECLARE iRow INT;
    DECLARE iPos INT;
    DECLARE iPos2 INT;

    DROP TEMPORARY TABLE IF EXISTS temp_split_list;
    CREATE TEMPORARY TABLE temp_split_list
                          (row_number INT UNSIGNED,
                           row_value INT
                          ) ENGINE=Memory;
                          
	SET iRow  = 0;
    SET iPos  = 1;
    SET iPos2 = Locate(string_delimiter, list_string, iPos);
    
    WHILE iPos2 > 0 DO
    
        SET iRow = iRow + 1;
        INSERT INTO temp_split_list (row_number,
                                     row_value)
					         VALUES (iRow,
                                     substring(list_string, iPos, iPos2 - iPos));
                                 
		SET iPos = iPos2 + length(string_delimiter);
        SET iPos2 = Locate(string_delimiter, list_string, iPos);
    END WHILE;
    
    /*
     * add any text left over
     */
	IF iPos <= length(list_string) THEN
        SET iRow = iRow + 1;
        INSERT INTO temp_split_list (row_number,
                                     row_value)
					         VALUES (iRow,
                                     substr(substring(list_string, iPos), 1, 4000));
	END IF;
    
end;
$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine mdce.get_prev_bill_date
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `mdce`$$
CREATE FUNCTION get_prev_bill_date (dateCheck DATE, billingDay int)
   RETURNS date
   DETERMINISTIC
BEGIN
   DECLARE billDate DATE;
   
   SET billDate = adddate(adddate(dateCheck, -1 * dayofmonth(dateCheck)), billingDay);
   IF billDate <= dateCheck THEN
      RETURN billDate;
   END IF;
   
   RETURN billDate - interval 1 month;

END;
$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine mdce.get_next_bill_date
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `mdce`$$
CREATE FUNCTION get_next_bill_date (dateCheck DATE, billingDay int)
   RETURNS date
   DETERMINISTIC
BEGIN
   DECLARE billDate DATE;
   
   SET billDate = adddate(adddate(dateCheck, -1 * dayofmonth(dateCheck)), billingDay);
   IF billDate > dateCheck THEN
      RETURN billDate;
   END IF;
   
   RETURN billDate + interval 1 month;

END;
$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine mdce.update_miu_stats
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `mdce`$$
CREATE PROCEDURE update_miu_stats(ats_date timestamp)
begin
   DECLARE ldate_month_start DATE;
   DECLARE ldate_month_end DATE;
   
   IF ats_date IS NULL THEN
      SET ats_date = current_date;
   END IF;
   
   /*
    * convert to chicago
    */
   SET ats_date = convert_tz(ats_date, 'UTC', 'America/Chicago');
   
   SET ldate_month_start = ats_date - interval day(ats_date) - 1 day;
   SET ldate_month_end   = ldate_month_start + interval 1 month;
   
   /*
    * if 1st hour of the day, update last 3 days
    */
   IF hour(ats_date) = 0  THEN
      CALL update_miu_daily_stats(ats_date - interval 1 day);
      CALL update_miu_daily_stats(ats_date - interval 2 day);
      CALL update_miu_daily_stats(ats_date - interval 3 day);

      /*
       * if 1st 3 days of the month, calculate previous month as well
       */
      IF day(ats_date) < 4 THEN
         CALL update_miu_monthly_stats(ldate_month_start - interval 1 month, ldate_month_start);

      END IF;                                   
   END IF;                            

   /*
    * update current day
    */
   CALL update_miu_daily_stats(ats_date);
   
   /*
    * now update current month
    */
   CALL update_miu_monthly_stats(ldate_month_start, ldate_month_end);    
   
end 
$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine mdce.update_miu_daily_stats
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `mdce`$$
CREATE PROCEDURE update_miu_daily_stats(adate_stats DATE)
begin
   DECLARE ldate_utc DATE;
   DECLARE li_mins INT;
   DECLARE li_mins_utc INT;
   DECLARE lbool_prev_date BOOLEAN;
   
   IF adate_stats IS NULL THEN
      SET adate_stats = convert_tz(current_date, 'UTC', 'America/Chicago');
   END IF;
   
   /*
    * convert to UTC
    */
   SET ldate_utc = convert_tz(adate_stats, 'America/Chicago', 'UTC');
   
   /*
    * calculate the mins different for UTC
    */
   SET li_mins_utc = timestampdiff(minute, current_timestamp, convert_tz(current_timestamp, 'UTC', 'America/Chicago'));
   
   /*
    * calculate minutes after midnight
    */
   SET li_mins = timestampdiff(minute, 
                               date(convert_tz(current_timestamp, 'UTC', 'America/Chicago')), 
                               convert_tz(current_timestamp, 'UTC', 'America/Chicago'));
                               
   IF adate_stats < current_date THEN                               
      SET lbool_prev_date = true;
   ELSE
      SET lbool_prev_date = false;
   END IF;

   INSERT INTO miu_daily_stats (miu_id, heard_date, 
        num_expected, num_heard, 
		avg_rssi, min_rssi, max_rssi, 
        avg_ber, min_ber, max_ber, 
        avg_processor_reset_counter, min_processor_reset_counter, max_processor_reset_counter,
        avg_register_time, min_register_time, max_register_time, 
        avg_register_time_to_activate_context, min_register_time_to_activate_context, max_register_time_to_activate_context, 
        avg_register_time_connected, min_register_time_connected, max_register_time_connected, 
        avg_register_time_to_transfer_packet, min_register_time_to_transfer_packet, max_register_time_to_transfer_packet, 
        avg_disconnect_time, min_disconnect_time, max_disconnect_time, 
        avg_mag_swipe_counter, min_mag_swipe_counter, max_mag_swipe_counter, 
        avg_battery_capacity, min_battery_capacity, max_battery_capacity,
        avg_temperature, min_temperature, max_temperature
        ) 
              (SELECT m.miu_id, adate_stats,
                      coalesce((
							 select 60*24 / reporting_interval_mins 
							 from cmiu_config_set ccs 
							 inner join cmiu_config_mgmt ccm 
							 on ccm.current_config = ccs.id
							 where ccm.miu_id = miu_id
						 ), 1),
                      count(p.miu_id),
                      sum(avg_rssi * miu_packet_count) / greatest(sum(miu_packet_count), 1), min(min_rssi), max(max_rssi),
                      avg(ber), min(ber), max(ber),
                      avg(processor_reset_counter), min(processor_reset_counter), max(processor_reset_counter),
                      avg(register_time), min(register_time), max(register_time),
                      avg(register_time_to_activate_context), min(register_time_to_activate_context), max(register_time_to_activate_context),
                      avg(register_time_connected), min(register_time_connected), max(register_time_connected),
                      avg(register_time_to_transfer_packet), min(register_time_to_transfer_packet), max(register_time_to_transfer_packet),
                      avg(disconnect_time), min(disconnect_time), max(disconnect_time),
                      avg(mag_swipe_counter), min(mag_swipe_counter), max(mag_swipe_counter),
                      avg(battery_capacity), min(battery_capacity), max(battery_capacity),       
                      avg(temperature), min(temperature), max(temperature)       
                 FROM miu_details m
					     LEFT OUTER JOIN miu_heard_packets p ON m.miu_id = p.miu_id AND
                                                           heard_time >= adate_stats - INTERVAL li_mins_utc MINUTE AND 
                                                           heard_time < adate_stats - INTERVAL li_mins_utc MINUTE + INTERVAL 1 day
                WHERE m.meter_active = 'Y'                                           
                GROUP BY miu_id, adate_stats)
     ON DUPLICATE KEY UPDATE    
		num_expected = VALUES(num_expected), num_heard = VALUES(num_heard),
		avg_rssi = VALUES(avg_rssi), min_rssi = VALUES(min_rssi), max_rssi = VALUES(max_rssi),
		avg_ber = VALUES(avg_ber), min_ber = VALUES(min_ber), max_ber = VALUES(max_ber), 
		avg_processor_reset_counter = VALUES(avg_processor_reset_counter), min_processor_reset_counter = VALUES(min_processor_reset_counter), max_processor_reset_counter = VALUES(max_processor_reset_counter),
		avg_register_time = VALUES(avg_register_time), min_register_time = VALUES(min_register_time), max_register_time = VALUES(max_register_time), 
		avg_register_time_to_activate_context = VALUES(avg_register_time_to_activate_context), min_register_time_to_activate_context = VALUES(min_register_time_to_activate_context), max_register_time_to_activate_context = VALUES(max_register_time_to_activate_context), 
		avg_register_time_connected = VALUES(avg_register_time_connected), min_register_time_connected = VALUES(min_register_time_connected), max_register_time_connected = VALUES(max_register_time_connected), 
		avg_register_time_to_transfer_packet = VALUES(avg_register_time_to_transfer_packet), min_register_time_to_transfer_packet = VALUES(min_register_time_to_transfer_packet), max_register_time_to_transfer_packet = VALUES(max_register_time_to_transfer_packet), 
		avg_disconnect_time = VALUES(avg_disconnect_time), min_disconnect_time = VALUES(min_disconnect_time), max_disconnect_time = VALUES(max_disconnect_time), 
		avg_mag_swipe_counter = VALUES(avg_mag_swipe_counter), min_mag_swipe_counter = VALUES(min_mag_swipe_counter), max_mag_swipe_counter = VALUES(max_mag_swipe_counter), 
		avg_battery_capacity = VALUES(avg_battery_capacity), min_battery_capacity = VALUES(min_battery_capacity), max_battery_capacity = VALUES(max_battery_capacity),
		avg_temperature = VALUES(avg_temperature), min_temperature = VALUES(min_temperature), max_temperature = VALUES(max_temperature);
                
end$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine mdce.update_miu_monthly_stats
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `mdce`$$
CREATE PROCEDURE update_miu_monthly_stats(start_date DATE, end_date DATE)
begin
   
	INSERT INTO miu_monthly_stats
	   (miu_id, year, month,
	    num_expected, num_heard,
	    avg_rssi, min_rssi, max_rssi,
        avg_ber, min_ber, max_ber, 
        avg_processor_reset_counter, min_processor_reset_counter, max_processor_reset_counter,
        avg_register_time, min_register_time, max_register_time, 
        avg_register_time_to_activate_context, min_register_time_to_activate_context, max_register_time_to_activate_context, 
        avg_register_time_connected, min_register_time_connected, max_register_time_connected, 
        avg_register_time_to_transfer_packet, min_register_time_to_transfer_packet, max_register_time_to_transfer_packet, 
        avg_disconnect_time, min_disconnect_time, max_disconnect_time, 
        avg_mag_swipe_counter, min_mag_swipe_counter, max_mag_swipe_counter, 
        avg_battery_capacity, min_battery_capacity, max_battery_capacity,
        avg_temperature, min_temperature, max_temperature
        ) 
	(SELECT miu_id, year(start_date), month(start_date),
			sum(num_expected), sum(num_heard),
			sum(avg_rssi * num_heard) / greatest(1, sum(num_heard)), min(min_rssi), max(max_rssi),
            sum(avg_ber * num_heard) / greatest(1, sum(num_heard)), min(min_ber), max(max_ber),
            sum(avg_processor_reset_counter * num_heard) / greatest(1, sum(num_heard)), min(min_processor_reset_counter), max(max_processor_reset_counter),
            sum(avg_register_time * num_heard) / greatest(1, sum(num_heard)), min(min_register_time), max(max_register_time),
            sum(avg_register_time_to_activate_context * num_heard) / greatest(1, sum(num_heard)), min(min_register_time_to_activate_context), max(max_register_time_to_activate_context),
            sum(avg_register_time_connected * num_heard) / greatest(1, sum(num_heard)), min(min_register_time_connected), max(max_register_time_connected),
            sum(avg_register_time_to_transfer_packet * num_heard) / greatest(1, sum(num_heard)), min(min_register_time_to_transfer_packet), max(max_register_time_to_transfer_packet),
            sum(avg_disconnect_time * num_heard) / greatest(1, sum(num_heard)), min(min_disconnect_time), max(max_disconnect_time),
            sum(avg_mag_swipe_counter * num_heard) / greatest(1, sum(num_heard)), min(min_mag_swipe_counter), max(max_mag_swipe_counter),
            sum(avg_battery_capacity * num_heard) / greatest(1, sum(num_heard)), min(min_battery_capacity), max(max_battery_capacity),
            sum(avg_temperature * num_heard) / greatest(1, sum(num_heard)), min(min_temperature), max(max_temperature)
	   FROM miu_daily_stats
	  WHERE heard_date >= start_date AND
			heard_date < end_date
	  GROUP BY miu_id)
	ON DUPLICATE KEY UPDATE 
			num_expected = VALUES(num_expected), num_heard = VALUES(num_heard),
			avg_rssi = VALUES(avg_rssi), min_rssi = VALUES(min_rssi), max_rssi = VALUES(max_rssi),
			avg_ber = VALUES(avg_ber), min_ber = VALUES(min_ber), max_ber = VALUES(max_ber), 
			avg_processor_reset_counter = VALUES(avg_processor_reset_counter), min_processor_reset_counter = VALUES(min_processor_reset_counter), max_processor_reset_counter = VALUES(max_processor_reset_counter),
			avg_register_time = VALUES(avg_register_time), min_register_time = VALUES(min_register_time), max_register_time = VALUES(max_register_time), 
			avg_register_time_to_activate_context = VALUES(avg_register_time_to_activate_context), min_register_time_to_activate_context = VALUES(min_register_time_to_activate_context), max_register_time_to_activate_context = VALUES(max_register_time_to_activate_context), 
			avg_register_time_connected = VALUES(avg_register_time_connected), min_register_time_connected = VALUES(min_register_time_connected), max_register_time_connected = VALUES(max_register_time_connected), 
			avg_register_time_to_transfer_packet = VALUES(avg_register_time_to_transfer_packet), min_register_time_to_transfer_packet = VALUES(min_register_time_to_transfer_packet), max_register_time_to_transfer_packet = VALUES(max_register_time_to_transfer_packet), 
			avg_disconnect_time = VALUES(avg_disconnect_time), min_disconnect_time = VALUES(min_disconnect_time), max_disconnect_time = VALUES(max_disconnect_time), 
			avg_mag_swipe_counter = VALUES(avg_mag_swipe_counter), min_mag_swipe_counter = VALUES(min_mag_swipe_counter), max_mag_swipe_counter = VALUES(max_mag_swipe_counter), 
			avg_battery_capacity = VALUES(avg_battery_capacity), min_battery_capacity = VALUES(min_battery_capacity), max_battery_capacity = VALUES(max_battery_capacity),
			avg_temperature = VALUES(avg_temperature), min_temperature = VALUES(min_temperature), max_temperature = VALUES(max_temperature);
            

end$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine mdce.update_gw_stats
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `mdce`$$
CREATE PROCEDURE update_gw_stats(ats_date timestamp)
begin
   DECLARE ldate_month_start DATE;
   DECLARE ldate_month_end DATE;
   
   IF ats_date IS NULL THEN
      SET ats_date = current_date;
   END IF;
   
   /*
    * convert to chicago
    */
   SET ats_date = convert_tz(ats_date, 'UTC', 'America/Chicago');
   
   SET ldate_month_start = ats_date - interval day(ats_date) - 1 day;
   SET ldate_month_end   = ldate_month_start + interval 1 month;

  /*
   * if 1st 3 days of the month, calculate previous month as well
   */
  IF day(ats_date) < 4 THEN
	 CALL update_gw_monthly_stats(ldate_month_start - interval 1 month, ldate_month_start);

  END IF;                                   

   /*
    * now update current month
    */
   CALL update_gw_monthly_stats(ldate_month_start, ldate_month_end);    
   
end 
$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine mdce.update_gw_monthly_stats
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `mdce`$$
CREATE PROCEDURE update_gw_monthly_stats(start_date DATE, end_date DATE)
begin
   
	INSERT INTO mdce.gateway_monthly_stats (gateway_id, year, month,
		num_expected, num_heard, 
		avg_mius_heard, min_mius_heard, max_mius_heard, 
		avg_ook_packets_heard, min_ook_packets_heard, max_ook_packets_heard, 
		avg_fsk_packets_heard, min_fsk_packets_heard, max_fsk_packets_heard, 
		avg_config_packets_heard, min_config_packets_heard, max_config_packets_heard) 
	(SELECT gateway_id, year(start_date), month(start_date),
			(
				select 
					sum(timestampdiff(minute, gc1.from_date, ifnull((select min(gc2.from_date) from gateway_config gc2 where gc2.gateway_db_id = gc1.gateway_db_id and gc2.from_date > gc1.from_date), end_date)) / gc1.TransferInterval) 
                    from gateway_config gc1 
                    where gc1.gateway_db_id = (select id from gateway_details gd where gd.gateway_id=ghp.gateway_id)
			)
			, count(1),
			avg(mius_heard), min(mius_heard), max(mius_heard), 
			avg(ook_packets_heard), min(ook_packets_heard), max(ook_packets_heard), 
			avg(fsk_packets_heard), min(fsk_packets_heard), max(fsk_packets_heard), 
			avg(config_packets_heard), min(config_packets_heard), max(config_packets_heard) 
	   FROM gateway_heard_packets ghp
	  WHERE ghp.heard_time >= start_date AND
			ghp.heard_time < end_date
	  GROUP BY ghp.gateway_id)
	ON DUPLICATE KEY UPDATE 
			num_expected = VALUES(num_expected), num_heard = VALUES(num_heard),
			avg_mius_heard = VALUES(avg_mius_heard), min_mius_heard = VALUES(min_mius_heard), max_mius_heard = VALUES(max_mius_heard),
			avg_ook_packets_heard = VALUES(avg_ook_packets_heard), min_ook_packets_heard = VALUES(min_ook_packets_heard), max_ook_packets_heard = VALUES(max_ook_packets_heard), 
			avg_fsk_packets_heard = VALUES(avg_fsk_packets_heard), min_fsk_packets_heard = VALUES(min_fsk_packets_heard), max_fsk_packets_heard = VALUES(max_fsk_packets_heard), 
			avg_config_packets_heard = VALUES(avg_config_packets_heard), min_config_packets_heard = VALUES(min_config_packets_heard), max_config_packets_heard = VALUES(max_config_packets_heard);
            

end$$

DELIMITER ;
SET FOREIGN_KEY_CHECKS = 1;
