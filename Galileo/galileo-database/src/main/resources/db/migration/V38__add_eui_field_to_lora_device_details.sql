-- MSPD-2077
-- Add EUI column to Lora_device_details table

ALTER TABLE mdce.lora_device_details ADD COLUMN eui VARCHAR(33) NOT NULL UNIQUE
