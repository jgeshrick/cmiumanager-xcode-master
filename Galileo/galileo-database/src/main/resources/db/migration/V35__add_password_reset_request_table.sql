-- MSPD-982
-- Add table to record and track password reset requests.

-- ----------------------------------------------------------------------------
-- Table mdce.password_reset_request
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`password_reset_request` (
  `password_reset_request_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(32) BINARY NOT NULL,
  `user_id` INT(10) UNSIGNED NULL,
  `matched_user_name` VARCHAR(45) NOT NULL,
  `entered_email_address` VARCHAR(45) NOT NULL,
  `created_timestamp` DATETIME NOT NULL,
  `valid_until_timestamp` DATETIME NOT NULL,
  `requestor_ip_address` VARCHAR(45) NOT NULL,
  `claimed` TINYINT(1) NOT NULL DEFAULT 0,
  `claimed_timestamp` DATETIME NULL,
  `claimant_ip_address` VARCHAR(45) NULL,

  PRIMARY KEY (`password_reset_request_id`),
  UNIQUE (`token`),

  INDEX `i_password_reset_request_token` (`token`),
  INDEX `i_password_reset_request_matched_user_name` (`matched_user_name`),
  INDEX `i_password_reset_request_entered_email_address` (`entered_email_address`),
  INDEX `i_password_reset_request_valid_until_timestamp` (`valid_until_timestamp`),
  INDEX `fk_password_reset_request_user_list_idx` (`user_id`),

  CONSTRAINT `fk_password_reset_request_user_list`
    FOREIGN KEY (`user_id`)
    REFERENCES `mdce`.`user_list` (`user_id`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
