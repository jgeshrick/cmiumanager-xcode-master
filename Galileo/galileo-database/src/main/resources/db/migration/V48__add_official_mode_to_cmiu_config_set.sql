-- MDCE-2260
-- Adds official_mode column to cmiu_config_set

ALTER TABLE cmiu_config_set ADD official_mode boolean NOT NULL DEFAULT false;