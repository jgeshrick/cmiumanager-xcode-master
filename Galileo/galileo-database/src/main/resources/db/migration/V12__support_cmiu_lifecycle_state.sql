-- MSPD-1310
-- Add support for recording the CMIUs lifecycle state

CREATE TABLE IF NOT EXISTS `mdce`.`cmiu_lifecycle_state` (
  `miu_id` INT UNSIGNED NOT NULL,
  `cmiu_lifecycle_state_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `timestamp` DATETIME,
  `state` ENUM('newborn', 'pre_activating', 'pre_activated', 
    'activated', 'heard', 'pre_pot', 'confirmed', 'development', 'scanned', 
    'shipped', 'unclaimed', 'claimed', 'rma_ed', 'dead', 'rogue'),
  PRIMARY KEY (`cmiu_lifecycle_state_id`),
  CONSTRAINT `fk_cmiu_lifecycle_state_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

ALTER TABLE `mdce`.`miu_details`
ADD `last_state_change_timestamp` DATETIME,
ADD `state` ENUM('newborn', 'pre_activating', 'pre_activated', 
    'activated', 'heard', 'pre_pot', 'confirmed', 'development', 'scanned', 
    'shipped', 'unclaimed', 'claimed', 'rma_ed', 'dead', 'rogue');
