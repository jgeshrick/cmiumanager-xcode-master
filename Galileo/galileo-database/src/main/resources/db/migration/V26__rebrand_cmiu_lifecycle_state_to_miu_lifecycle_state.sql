-- MSPD-1836
-- Rebrand cmiu_lifecycle_state to miu_lifecycle_state

RENAME TABLE cmiu_lifecycle_state TO miu_lifecycle_state;
ALTER TABLE miu_lifecycle_state CHANGE cmiu_lifecycle_state_id miu_lifecycle_state_id INT(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE miu_lifecycle_state MODIFY COLUMN state ENUM('Newborn', 'PreActivating', 'PreActivated', 'TestJoined', 'Activated', 'Heard', 'PrePrePot', 'PrePot', 'PostPot', 'Confirmed', 'Development', 'Scanned', 'Shipped', 'Unclaimed', 'Claimed', 'RmaEd', 'Dead', 'Rogue', 'Aging', 'Unspecified');

ALTER TABLE miu_details MODIFY COLUMN state ENUM('Newborn', 'PreActivating', 'PreActivated', 'TestJoined', 'Activated', 'Heard', 'PrePrePot', 'PrePot', 'PostPot', 'Confirmed', 'Development', 'Scanned', 'Shipped', 'Unclaimed', 'Claimed', 'RmaEd', 'Dead', 'Rogue', 'Aging', 'Unspecified');
