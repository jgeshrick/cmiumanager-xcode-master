-- MSPD-1861
-- Add table to hold network details for LoRa devices

-- ----------------------------------------------------------------------------
-- Table mdce.lora_device_details
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `mdce`.`lora_device_details` (
  `lora_device_details_id` INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE,
  `miu_id` INT(10) UNSIGNED NOT NULL UNIQUE,
  `lora_network` ENUM("Senet", "Actility") NOT NULL,
  `last_update_time` DATETIME NULL,

  PRIMARY KEY (`lora_device_details_id`),

  CONSTRAINT `fk_miu_id_miu_details_miu_id`
    FOREIGN KEY (`miu_id`)
    REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
