-- MSPD-2162
-- Add an auto-incremented ID column to cmiu_config_history for sequence tracking.

ALTER TABLE `mdce`.`cmiu_config_history`
ADD COLUMN cmiu_config_history_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY;
