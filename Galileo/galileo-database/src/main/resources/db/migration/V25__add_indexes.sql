-- MSPD-1911
-- Add indexes to columns searchable by JdbcMiuSearchRepository.

CREATE INDEX `idx_ref_data_utilities_site_id`  ON `mdce`.`ref_data_utilities` (site_id) COMMENT '' ALGORITHM DEFAULT LOCK DEFAULT;
CREATE INDEX `idx_ref_data_info_state`  ON `mdce`.`ref_data_info` (state) COMMENT '' ALGORITHM DEFAULT LOCK DEFAULT;
CREATE INDEX `idx_ref_data_info_regional_manager`  ON `mdce`.`ref_data_info` (regional_manager) COMMENT '' ALGORITHM DEFAULT LOCK DEFAULT;
CREATE INDEX `idx_miu_details_miu_type`  ON `mdce`.`miu_details` (miu_type) COMMENT '' ALGORITHM DEFAULT LOCK DEFAULT;
CREATE INDEX `idx_cellular_device_details_imei`  ON `mdce`.`cellular_device_details` (imei) COMMENT '' ALGORITHM DEFAULT LOCK DEFAULT;
