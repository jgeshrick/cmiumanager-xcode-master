-- MSPD-2103
-- Add antenna type to lora_device_details

ALTER TABLE mdce.lora_device_details
ADD COLUMN antenna_type ENUM('internal', 'external');
