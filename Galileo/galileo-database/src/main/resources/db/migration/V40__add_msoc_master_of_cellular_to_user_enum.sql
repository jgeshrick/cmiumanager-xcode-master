-- MSPD-1669
-- Add MSOC Master Of Cellular to the user type enum

ALTER TABLE mdce.user_list
MODIFY COLUMN user_level ENUM('Distributor', 'SiteOperator', 'MsocOperator', 'MsocAdmin', 'MsocMasterOfCellular');
 

