-- MSPD-1172
-- Improve stats tables and procedures

ALTER TABLE miu_heard_packets MODIFY avg_rssi decimal(7,3);
ALTER TABLE miu_heard_packets MODIFY ber decimal(4,1);

ALTER TABLE miu_daily_stats MODIFY avg_rssi decimal(7,3);
ALTER TABLE miu_daily_stats MODIFY min_ber decimal(4,1);
ALTER TABLE miu_daily_stats MODIFY max_ber decimal(4,1);
ALTER TABLE miu_daily_stats MODIFY avg_ber decimal(6,3);
ALTER TABLE miu_daily_stats MODIFY avg_processor_reset_counter decimal(12,1);
ALTER TABLE miu_daily_stats MODIFY avg_register_time decimal(12,1);
ALTER TABLE miu_daily_stats MODIFY avg_register_time_to_activate_context decimal(12,1);
ALTER TABLE miu_daily_stats MODIFY avg_register_time_connected decimal(12,1);
ALTER TABLE miu_daily_stats MODIFY avg_register_time_to_transfer_packet decimal(12,1);
ALTER TABLE miu_daily_stats MODIFY avg_disconnect_time decimal(12,1);
ALTER TABLE miu_daily_stats MODIFY avg_mag_swipe_counter decimal(12,1);
ALTER TABLE miu_daily_stats MODIFY avg_battery_capacity decimal(7,3);
ALTER TABLE miu_daily_stats MODIFY avg_temperature decimal(5,3);
ALTER TABLE miu_daily_stats MODIFY avg_rsrq decimal(7,3);
ALTER TABLE miu_daily_stats MODIFY avg_battery_voltage decimal(7,3);

ALTER TABLE miu_monthly_stats MODIFY avg_rssi decimal(7,3);
ALTER TABLE miu_monthly_stats MODIFY min_ber decimal(4,1);
ALTER TABLE miu_monthly_stats MODIFY max_ber decimal(4,1);
ALTER TABLE miu_monthly_stats MODIFY avg_ber decimal(6,3);
ALTER TABLE miu_monthly_stats MODIFY avg_processor_reset_counter decimal(12,1);
ALTER TABLE miu_monthly_stats MODIFY avg_register_time decimal(12,1);
ALTER TABLE miu_monthly_stats MODIFY avg_register_time_to_activate_context decimal(12,1);
ALTER TABLE miu_monthly_stats MODIFY avg_register_time_connected decimal(12,1);
ALTER TABLE miu_monthly_stats MODIFY avg_register_time_to_transfer_packet decimal(12,1);
ALTER TABLE miu_monthly_stats MODIFY avg_disconnect_time decimal(12,1);
ALTER TABLE miu_monthly_stats MODIFY avg_mag_swipe_counter decimal(12,1);
ALTER TABLE miu_monthly_stats MODIFY avg_battery_capacity decimal(7,3);
ALTER TABLE miu_monthly_stats MODIFY avg_temperature decimal(5,3);
ALTER TABLE miu_monthly_stats MODIFY avg_rsrq decimal(7,3);
ALTER TABLE miu_monthly_stats MODIFY avg_battery_voltage decimal(7,3);


DROP PROCEDURE IF EXISTS update_miu_daily_stats;

DELIMITER $$


CREATE PROCEDURE update_miu_daily_stats(adate_stats DATE)
  begin
    DECLARE ldate_utc DATE;
    DECLARE li_mins INT;
    DECLARE li_mins_utc INT;
    DECLARE lbool_prev_date BOOLEAN;

    IF adate_stats IS NULL THEN
      SET adate_stats = convert_tz(current_date, 'UTC', 'America/Chicago');
    END IF;

    /*
     * convert to UTC
     */
    SET ldate_utc = convert_tz(adate_stats, 'America/Chicago', 'UTC');

    /*
     * calculate the mins different for UTC
     */
    SET li_mins_utc = timestampdiff(minute, current_timestamp, convert_tz(current_timestamp, 'UTC', 'America/Chicago'));

    /*
     * calculate minutes after midnight
     */
    SET li_mins = timestampdiff(minute,
                                date(convert_tz(current_timestamp, 'UTC', 'America/Chicago')),
                                convert_tz(current_timestamp, 'UTC', 'America/Chicago'));

    IF adate_stats < current_date THEN
      SET lbool_prev_date = true;
    ELSE
      SET lbool_prev_date = false;
    END IF;

    DROP TABLE IF EXISTS tmp_miu_daily_stats;


    CREATE TEMPORARY TABLE tmp_miu_daily_stats(
      miu_id INT PRIMARY KEY,
      num_expected INT,
      meter_active CHAR(1)
    )
      AS SELECT m.miu_id AS miu_id, coalesce(60*24 / reporting_interval_mins, 1) AS num_expected, m.meter_active as meter_active
         FROM miu_details m
           LEFT OUTER JOIN cmiu_config_mgmt ccm ON m.miu_id = ccm.miu_id
           LEFT OUTER JOIN cmiu_config_set ccs ON ccm.current_config = ccs.id;

    INSERT INTO miu_daily_stats (miu_id, heard_date,
                                 num_expected, num_heard,
                                 avg_rssi, min_rssi, max_rssi,
                                 avg_ber, min_ber, max_ber,
                                 avg_rsrq, min_rsrq, max_rsrq,
                                 avg_processor_reset_counter, min_processor_reset_counter, max_processor_reset_counter,
                                 avg_register_time, min_register_time, max_register_time,
                                 avg_register_time_to_activate_context, min_register_time_to_activate_context, max_register_time_to_activate_context,
                                 avg_register_time_connected, min_register_time_connected, max_register_time_connected,
                                 avg_register_time_to_transfer_packet, min_register_time_to_transfer_packet, max_register_time_to_transfer_packet,
                                 avg_disconnect_time, min_disconnect_time, max_disconnect_time,
                                 avg_mag_swipe_counter, min_mag_swipe_counter, max_mag_swipe_counter,
                                 avg_battery_capacity, min_battery_capacity, max_battery_capacity,
                                 avg_battery_voltage, min_battery_voltage, max_battery_voltage,
                                 avg_temperature, min_temperature, max_temperature
    )
      (SELECT p.miu_id, adate_stats,
         tmp.num_expected,
         count(p.miu_id),
         sum(avg_rssi * miu_packet_count) / greatest(sum(miu_packet_count), 1), min(min_rssi), max(max_rssi),
         avg(ber), min(ber), max(ber),
         avg(rsrq), min(rsrq), max(rsrq),
         avg(processor_reset_counter), min(processor_reset_counter), max(processor_reset_counter),
         avg(register_time), min(register_time), max(register_time),
         avg(register_time_to_activate_context), min(register_time_to_activate_context), max(register_time_to_activate_context),
         avg(register_time_connected), min(register_time_connected), max(register_time_connected),
         avg(register_time_to_transfer_packet), min(register_time_to_transfer_packet), max(register_time_to_transfer_packet),
         avg(disconnect_time), min(disconnect_time), max(disconnect_time),
         avg(mag_swipe_counter), min(mag_swipe_counter), max(mag_swipe_counter),
         avg(battery_capacity), min(battery_capacity), max(battery_capacity),
         avg(battery_voltage), min(battery_voltage), max(battery_voltage),
         avg(temperature), min(temperature), max(temperature)
       FROM miu_heard_packets p
         INNER JOIN tmp_miu_daily_stats tmp ON tmp.miu_id = p.miu_id
       WHERE
         heard_time >= adate_stats - INTERVAL li_mins_utc MINUTE AND
         heard_time < adate_stats - INTERVAL li_mins_utc MINUTE + INTERVAL 1 day
       GROUP BY miu_id)
    ON DUPLICATE KEY UPDATE
      num_expected = VALUES(num_expected), num_heard = VALUES(num_heard),
      avg_rssi = VALUES(avg_rssi), min_rssi = VALUES(min_rssi), max_rssi = VALUES(max_rssi),
      avg_ber = VALUES(avg_ber), min_ber = VALUES(min_ber), max_ber = VALUES(max_ber),
      avg_rsrq = VALUES(avg_rsrq), min_rsrq = VALUES(min_rsrq), max_rsrq = VALUES(max_rsrq),
      avg_processor_reset_counter = VALUES(avg_processor_reset_counter), min_processor_reset_counter = VALUES(min_processor_reset_counter), max_processor_reset_counter = VALUES(max_processor_reset_counter),
      avg_register_time = VALUES(avg_register_time), min_register_time = VALUES(min_register_time), max_register_time = VALUES(max_register_time),
      avg_register_time_to_activate_context = VALUES(avg_register_time_to_activate_context), min_register_time_to_activate_context = VALUES(min_register_time_to_activate_context), max_register_time_to_activate_context = VALUES(max_register_time_to_activate_context),
      avg_register_time_connected = VALUES(avg_register_time_connected), min_register_time_connected = VALUES(min_register_time_connected), max_register_time_connected = VALUES(max_register_time_connected),
      avg_register_time_to_transfer_packet = VALUES(avg_register_time_to_transfer_packet), min_register_time_to_transfer_packet = VALUES(min_register_time_to_transfer_packet), max_register_time_to_transfer_packet = VALUES(max_register_time_to_transfer_packet),
      avg_disconnect_time = VALUES(avg_disconnect_time), min_disconnect_time = VALUES(min_disconnect_time), max_disconnect_time = VALUES(max_disconnect_time),
      avg_mag_swipe_counter = VALUES(avg_mag_swipe_counter), min_mag_swipe_counter = VALUES(min_mag_swipe_counter), max_mag_swipe_counter = VALUES(max_mag_swipe_counter),
      avg_battery_capacity = VALUES(avg_battery_capacity), min_battery_capacity = VALUES(min_battery_capacity), max_battery_capacity = VALUES(max_battery_capacity),
      avg_battery_voltage = VALUES(avg_battery_voltage), min_battery_voltage = VALUES(min_battery_voltage), max_battery_voltage = VALUES(max_battery_voltage),
      avg_temperature = VALUES(avg_temperature), min_temperature = VALUES(min_temperature), max_temperature = VALUES(max_temperature);

    DROP TABLE tmp_miu_daily_stats;
  end
$$


DELIMITER ;

delimiter |
drop event if exists update_gw_stats|
create event update_gw_stats
  ON SCHEDULE EVERY 1 DAY STARTS CURRENT_DATE + INTERVAL 30 HOUR
do
  begin
    CALL update_gw_stats(null);
  end |
delimiter ;

delimiter |
drop event if exists update_miu_stats|
create event update_miu_stats
  ON SCHEDULE EVERY 15 MINUTE STARTS CURRENT_TIMESTAMP + INTERVAL 20 MINUTE
do
  begin
    CALL update_miu_stats(null);
  end |
delimiter ;

delimiter |
drop event if exists clean_up_stats |
create event clean_up_stats
  /*
   * start at midnight CST
   */
  ON SCHEDULE EVERY 1 DAY STARTS CURRENT_DATE + INTERVAL 30 HOUR
do
  begin
    /*
     * clear out old records - miu_heard_packets after 5 days, miu_daily_stats after 100 days, gateway_heard_packets after 100 days
     */
    delete from miu_heard_packets where heard_time < current_date - interval 5 day;

    delete from miu_daily_stats where heard_date < current_date - interval 100 day;

    delete from gateway_heard_packets where heard_time < current_date - interval 100 day;

  end |
delimiter ;
