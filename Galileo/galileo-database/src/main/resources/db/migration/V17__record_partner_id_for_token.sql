-- Record the partner id for API tokens, to support FPC authentication

ALTER TABLE mdce_api_tokens
  ADD COLUMN partner_id INT;

UPDATE mdce_api_tokens SET partner_id = 0;
