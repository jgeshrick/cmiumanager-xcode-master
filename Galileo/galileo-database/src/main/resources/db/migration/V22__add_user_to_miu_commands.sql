-- MSPD-1746
-- Add user_name column to miu_commands table to record command issuer

ALTER TABLE mdce.miu_commands ADD COLUMN user_name VARCHAR(45) NULL DEFAULT NULL
