-- MSPD-1683
-- Add index to sim_details.sim_cellular_network

CREATE INDEX `idx_sim_details_sim_cellular_network`  ON `mdce`.`sim_details` (sim_cellular_network) COMMENT '' ALGORITHM DEFAULT LOCK DEFAULT;
