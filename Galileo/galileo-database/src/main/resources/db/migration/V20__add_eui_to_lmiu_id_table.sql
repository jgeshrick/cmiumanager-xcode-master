-- MSPD 1726
-- Add eui_details table

CREATE TABLE eui_details (
eui_details_id INT UNSIGNED AUTO_INCREMENT,
eui VARCHAR(16) UNIQUE NOT NULL,
miu_id INT UNSIGNED UNIQUE NOT NULL,
PRIMARY KEY (eui_details_id));
