-- MSPD-1979 CMIU config history not being updated
-- Values stored in column reporting_interval_mins were actually number of hours!

UPDATE mdce.cmiu_config_set SET reporting_interval_mins = reporting_interval_mins * 60 where (reporting_interval_mins % 60) != 0;
