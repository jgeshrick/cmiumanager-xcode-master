-- MSPD-815
-- Adding columns for RSRQ and battery voltage

ALTER TABLE miu_heard_packets ADD COLUMN `rsrq` DECIMAL(6,2) NULL COMMENT 'Units = dB';
ALTER TABLE miu_heard_packets ADD COLUMN `battery_voltage` DECIMAL(6,2) NULL COMMENT 'Units = mV';
ALTER TABLE miu_heard_packets MODIFY COLUMN `battery_capacity` DECIMAL(6,2) NULL COMMENT 'Units = mAh';

ALTER TABLE miu_daily_stats ADD COLUMN `avg_rsrq` DECIMAL(6,3) NULL;
ALTER TABLE miu_daily_stats ADD COLUMN `min_rsrq` DECIMAL(6,2) NULL;
ALTER TABLE miu_daily_stats ADD COLUMN `max_rsrq` DECIMAL(6,2) NULL;
ALTER TABLE miu_daily_stats MODIFY COLUMN `avg_battery_capacity` DECIMAL(6,3) NULL;
ALTER TABLE miu_daily_stats ADD COLUMN `avg_battery_voltage` DECIMAL(6,3) NULL;
ALTER TABLE miu_daily_stats ADD COLUMN `min_battery_voltage` DECIMAL(6,2) NULL;
ALTER TABLE miu_daily_stats ADD COLUMN `max_battery_voltage` DECIMAL(6,2) NULL;

ALTER TABLE miu_monthly_stats ADD COLUMN `avg_rsrq` DECIMAL(6,3) NULL;
ALTER TABLE miu_monthly_stats ADD COLUMN `min_rsrq` DECIMAL(6,2) NULL;
ALTER TABLE miu_monthly_stats ADD COLUMN `max_rsrq` DECIMAL(6,2) NULL;
ALTER TABLE miu_monthly_stats MODIFY COLUMN `avg_battery_capacity` DECIMAL(6,3) NULL;
ALTER TABLE miu_monthly_stats ADD COLUMN `avg_battery_voltage` DECIMAL(6,3) NULL;
ALTER TABLE miu_monthly_stats ADD COLUMN `min_battery_voltage` DECIMAL(6,2) NULL;
ALTER TABLE miu_monthly_stats ADD COLUMN `max_battery_voltage` DECIMAL(6,2) NULL;


-- -----------------------------------------------------
-- procedure update_miu_daily_stats
-- -----------------------------------------------------

USE `mdce`;
DROP procedure IF EXISTS `update_miu_daily_stats`;
SHOW WARNINGS;

DELIMITER $$
USE `mdce`$$
CREATE PROCEDURE update_miu_daily_stats(adate_stats DATE)
begin
   DECLARE ldate_utc DATE;
   DECLARE li_mins INT;
   DECLARE li_mins_utc INT;
   DECLARE lbool_prev_date BOOLEAN;

   IF adate_stats IS NULL THEN
      SET adate_stats = convert_tz(current_date, 'UTC', 'America/Chicago');
   END IF;

   /*
    * convert to UTC
    */
   SET ldate_utc = convert_tz(adate_stats, 'America/Chicago', 'UTC');

   /*
    * calculate the mins different for UTC
    */
   SET li_mins_utc = timestampdiff(minute, current_timestamp, convert_tz(current_timestamp, 'UTC', 'America/Chicago'));

   /*
    * calculate minutes after midnight
    */
   SET li_mins = timestampdiff(minute,
                               date(convert_tz(current_timestamp, 'UTC', 'America/Chicago')),
                               convert_tz(current_timestamp, 'UTC', 'America/Chicago'));

   IF adate_stats < current_date THEN
      SET lbool_prev_date = true;
   ELSE
      SET lbool_prev_date = false;
   END IF;

   INSERT INTO miu_daily_stats (miu_id, heard_date,
        num_expected, num_heard,
		    avg_rssi, min_rssi, max_rssi,
        avg_ber, min_ber, max_ber,
        avg_rsrq, min_rsrq, max_rsrq,
        avg_processor_reset_counter, min_processor_reset_counter, max_processor_reset_counter,
        avg_register_time, min_register_time, max_register_time,
        avg_register_time_to_activate_context, min_register_time_to_activate_context, max_register_time_to_activate_context,
        avg_register_time_connected, min_register_time_connected, max_register_time_connected,
        avg_register_time_to_transfer_packet, min_register_time_to_transfer_packet, max_register_time_to_transfer_packet,
        avg_disconnect_time, min_disconnect_time, max_disconnect_time,
        avg_mag_swipe_counter, min_mag_swipe_counter, max_mag_swipe_counter,
        avg_battery_capacity, min_battery_capacity, max_battery_capacity,
        avg_battery_voltage, min_battery_voltage, max_battery_voltage,
        avg_temperature, min_temperature, max_temperature
        )
              (SELECT m.miu_id, adate_stats,
                      coalesce((
							 select 60*24 / reporting_interval_mins
							 from cmiu_config_set ccs
							 inner join cmiu_config_mgmt ccm
							 on ccm.current_config = ccs.id
							 where ccm.miu_id = miu_id
						 ), 1),
                      count(p.miu_id),
                      sum(avg_rssi * miu_packet_count) / greatest(sum(miu_packet_count), 1), min(min_rssi), max(max_rssi),
                      avg(ber), min(ber), max(ber),
                      avg(rsrq), min(rsrq), max(rsrq),
                      avg(processor_reset_counter), min(processor_reset_counter), max(processor_reset_counter),
                      avg(register_time), min(register_time), max(register_time),
                      avg(register_time_to_activate_context), min(register_time_to_activate_context), max(register_time_to_activate_context),
                      avg(register_time_connected), min(register_time_connected), max(register_time_connected),
                      avg(register_time_to_transfer_packet), min(register_time_to_transfer_packet), max(register_time_to_transfer_packet),
                      avg(disconnect_time), min(disconnect_time), max(disconnect_time),
                      avg(mag_swipe_counter), min(mag_swipe_counter), max(mag_swipe_counter),
                      avg(battery_capacity), min(battery_capacity), max(battery_capacity),
                      avg(battery_voltage), min(battery_voltage), max(battery_voltage),
                      avg(temperature), min(temperature), max(temperature)
                 FROM miu_details m
					     LEFT OUTER JOIN miu_heard_packets p ON m.miu_id = p.miu_id AND
                                                           heard_time >= adate_stats - INTERVAL li_mins_utc MINUTE AND
                                                           heard_time < adate_stats - INTERVAL li_mins_utc MINUTE + INTERVAL 1 day
                WHERE m.meter_active = 'Y'
                GROUP BY miu_id, adate_stats)
     ON DUPLICATE KEY UPDATE
		num_expected = VALUES(num_expected), num_heard = VALUES(num_heard),
		avg_rssi = VALUES(avg_rssi), min_rssi = VALUES(min_rssi), max_rssi = VALUES(max_rssi),
		avg_ber = VALUES(avg_ber), min_ber = VALUES(min_ber), max_ber = VALUES(max_ber),
		avg_rsrq = VALUES(avg_rsrq), min_rsrq = VALUES(min_rsrq), max_rsrq = VALUES(max_rsrq),
		avg_processor_reset_counter = VALUES(avg_processor_reset_counter), min_processor_reset_counter = VALUES(min_processor_reset_counter), max_processor_reset_counter = VALUES(max_processor_reset_counter),
		avg_register_time = VALUES(avg_register_time), min_register_time = VALUES(min_register_time), max_register_time = VALUES(max_register_time),
		avg_register_time_to_activate_context = VALUES(avg_register_time_to_activate_context), min_register_time_to_activate_context = VALUES(min_register_time_to_activate_context), max_register_time_to_activate_context = VALUES(max_register_time_to_activate_context),
		avg_register_time_connected = VALUES(avg_register_time_connected), min_register_time_connected = VALUES(min_register_time_connected), max_register_time_connected = VALUES(max_register_time_connected),
		avg_register_time_to_transfer_packet = VALUES(avg_register_time_to_transfer_packet), min_register_time_to_transfer_packet = VALUES(min_register_time_to_transfer_packet), max_register_time_to_transfer_packet = VALUES(max_register_time_to_transfer_packet),
		avg_disconnect_time = VALUES(avg_disconnect_time), min_disconnect_time = VALUES(min_disconnect_time), max_disconnect_time = VALUES(max_disconnect_time),
		avg_mag_swipe_counter = VALUES(avg_mag_swipe_counter), min_mag_swipe_counter = VALUES(min_mag_swipe_counter), max_mag_swipe_counter = VALUES(max_mag_swipe_counter),
		avg_battery_capacity = VALUES(avg_battery_capacity), min_battery_capacity = VALUES(min_battery_capacity), max_battery_capacity = VALUES(max_battery_capacity),
		avg_battery_voltage = VALUES(avg_battery_voltage), min_battery_voltage = VALUES(min_battery_voltage), max_battery_voltage = VALUES(max_battery_voltage),
		avg_temperature = VALUES(avg_temperature), min_temperature = VALUES(min_temperature), max_temperature = VALUES(max_temperature);

end$$

DELIMITER ;
SHOW WARNINGS;

-- -----------------------------------------------------
-- procedure update_miu_monthly_stats
-- -----------------------------------------------------

USE `mdce`;
DROP procedure IF EXISTS `update_miu_monthly_stats`;
SHOW WARNINGS;

DELIMITER $$
USE `mdce`$$
CREATE PROCEDURE update_miu_monthly_stats(start_date DATE, end_date DATE)
begin

	INSERT INTO miu_monthly_stats
	   (miu_id, year, month,
	    num_expected, num_heard,
	    avg_rssi, min_rssi, max_rssi,
        avg_ber, min_ber, max_ber,
        avg_rsrq, min_rsrq, max_rsrq,
        avg_processor_reset_counter, min_processor_reset_counter, max_processor_reset_counter,
        avg_register_time, min_register_time, max_register_time,
        avg_register_time_to_activate_context, min_register_time_to_activate_context, max_register_time_to_activate_context,
        avg_register_time_connected, min_register_time_connected, max_register_time_connected,
        avg_register_time_to_transfer_packet, min_register_time_to_transfer_packet, max_register_time_to_transfer_packet,
        avg_disconnect_time, min_disconnect_time, max_disconnect_time,
        avg_mag_swipe_counter, min_mag_swipe_counter, max_mag_swipe_counter,
        avg_battery_capacity, min_battery_capacity, max_battery_capacity,
        avg_battery_voltage, min_battery_voltage, max_battery_voltage,
        avg_temperature, min_temperature, max_temperature
        )
	(SELECT miu_id, year(start_date), month(start_date),
			sum(num_expected), sum(num_heard),
			sum(avg_rssi * num_heard) / greatest(1, sum(num_heard)), min(min_rssi), max(max_rssi),
            sum(avg_ber * num_heard) / greatest(1, sum(num_heard)), min(min_ber), max(max_ber),
            sum(avg_rsrq * num_heard) / greatest(1, sum(num_heard)), min(min_rsrq), max(max_rsrq),
            sum(avg_processor_reset_counter * num_heard) / greatest(1, sum(num_heard)), min(min_processor_reset_counter), max(max_processor_reset_counter),
            sum(avg_register_time * num_heard) / greatest(1, sum(num_heard)), min(min_register_time), max(max_register_time),
            sum(avg_register_time_to_activate_context * num_heard) / greatest(1, sum(num_heard)), min(min_register_time_to_activate_context), max(max_register_time_to_activate_context),
            sum(avg_register_time_connected * num_heard) / greatest(1, sum(num_heard)), min(min_register_time_connected), max(max_register_time_connected),
            sum(avg_register_time_to_transfer_packet * num_heard) / greatest(1, sum(num_heard)), min(min_register_time_to_transfer_packet), max(max_register_time_to_transfer_packet),
            sum(avg_disconnect_time * num_heard) / greatest(1, sum(num_heard)), min(min_disconnect_time), max(max_disconnect_time),
            sum(avg_mag_swipe_counter * num_heard) / greatest(1, sum(num_heard)), min(min_mag_swipe_counter), max(max_mag_swipe_counter),
            sum(avg_battery_capacity * num_heard) / greatest(1, sum(num_heard)), min(min_battery_capacity), max(max_battery_capacity),
            sum(avg_battery_voltage * num_heard) / greatest(1, sum(num_heard)), min(min_battery_voltage), max(max_battery_voltage),
            sum(avg_temperature * num_heard) / greatest(1, sum(num_heard)), min(min_temperature), max(max_temperature)
	   FROM miu_daily_stats
	  WHERE heard_date >= start_date AND
			heard_date < end_date
	  GROUP BY miu_id)
	ON DUPLICATE KEY UPDATE
			num_expected = VALUES(num_expected), num_heard = VALUES(num_heard),
			avg_rssi = VALUES(avg_rssi), min_rssi = VALUES(min_rssi), max_rssi = VALUES(max_rssi),
			avg_ber = VALUES(avg_ber), min_ber = VALUES(min_ber), max_ber = VALUES(max_ber),
			avg_rsrq = VALUES(avg_rsrq), min_rsrq = VALUES(min_rsrq), max_rsrq = VALUES(max_rsrq),
			avg_processor_reset_counter = VALUES(avg_processor_reset_counter), min_processor_reset_counter = VALUES(min_processor_reset_counter), max_processor_reset_counter = VALUES(max_processor_reset_counter),
			avg_register_time = VALUES(avg_register_time), min_register_time = VALUES(min_register_time), max_register_time = VALUES(max_register_time),
			avg_register_time_to_activate_context = VALUES(avg_register_time_to_activate_context), min_register_time_to_activate_context = VALUES(min_register_time_to_activate_context), max_register_time_to_activate_context = VALUES(max_register_time_to_activate_context),
			avg_register_time_connected = VALUES(avg_register_time_connected), min_register_time_connected = VALUES(min_register_time_connected), max_register_time_connected = VALUES(max_register_time_connected),
			avg_register_time_to_transfer_packet = VALUES(avg_register_time_to_transfer_packet), min_register_time_to_transfer_packet = VALUES(min_register_time_to_transfer_packet), max_register_time_to_transfer_packet = VALUES(max_register_time_to_transfer_packet),
			avg_disconnect_time = VALUES(avg_disconnect_time), min_disconnect_time = VALUES(min_disconnect_time), max_disconnect_time = VALUES(max_disconnect_time),
			avg_mag_swipe_counter = VALUES(avg_mag_swipe_counter), min_mag_swipe_counter = VALUES(min_mag_swipe_counter), max_mag_swipe_counter = VALUES(max_mag_swipe_counter),
			avg_battery_capacity = VALUES(avg_battery_capacity), min_battery_capacity = VALUES(min_battery_capacity), max_battery_capacity = VALUES(max_battery_capacity),
			avg_battery_voltage = VALUES(avg_battery_voltage), min_battery_voltage = VALUES(min_battery_voltage), max_battery_voltage = VALUES(max_battery_voltage),
			avg_temperature = VALUES(avg_temperature), min_temperature = VALUES(min_temperature), max_temperature = VALUES(max_temperature);


end$$

DELIMITER ;
SHOW WARNINGS;

