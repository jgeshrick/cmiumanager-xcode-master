-- MSPD-1396
-- Add support for allowing/disallowing concurrent MDCE sessions for specific users.

ALTER TABLE `user_list` ADD COLUMN `allow_concurrent_sessions` TINYINT(1) DEFAULT 0;
