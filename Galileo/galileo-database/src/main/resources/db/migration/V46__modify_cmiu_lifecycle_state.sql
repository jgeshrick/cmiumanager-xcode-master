-- MDCE-2 || 3
-- Modifying states

ALTER TABLE miu_lifecycle_state MODIFY COLUMN state ENUM('Newborn', 'PreActivating', 'PreActivated', 'Activated', 'Heard', 'PrePrePot', 'PrePot', 'PostPot', 'Confirmed', 'Development', 'Scanned', 'Shipped', 'Unclaimed', 'Claimed', 'RmaEd', 'Dead', 'Rogue', 'Aging', 'Unspecified', 'Suspended', 'PreSuspended', 'DeathBed');
ALTER TABLE miu_details MODIFY COLUMN state ENUM('Newborn', 'PreActivating', 'PreActivated', 'Activated', 'Heard', 'PrePrePot', 'PrePot', 'PostPot', 'Confirmed', 'Development', 'Scanned', 'Shipped', 'Unclaimed', 'Claimed', 'RmaEd', 'Dead', 'Rogue', 'Aging', 'Unspecified', 'Suspended', 'PreSuspended', 'DeathBed');