-- MSPD-2111
-- Make app_eui non unique

ALTER TABLE mdce.lora_device_details 
DROP INDEX app_eui;
