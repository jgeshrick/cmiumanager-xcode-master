-- MSPD-1310
-- Modify enum for miu lifecycle state in miu_details table

ALTER TABLE miu_details MODIFY COLUMN state ENUM('Newborn', 'PreActivating', 'PreActivated', 'Activated', 'Heard', 'PrePrePot', 'PrePot', 'PostPot', 'Confirmed', 'Development', 'Scanned', 'Shipped', 'Unclaimed', 'Claimed', 'RmaEd', 'Dead', 'Rogue', 'Aging', 'Unspecified');
