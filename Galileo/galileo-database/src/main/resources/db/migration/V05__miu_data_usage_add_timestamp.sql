-- MSPD-1017
-- Add Timestamp

ALTER TABLE miu_data_usage ADD data_usage_datetime TIMESTAMP;
