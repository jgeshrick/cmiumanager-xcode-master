-- MSPD-1616 Review time and date usage in all java applications
-- This test table allows timezone handling to be checked

CREATE TABLE tz_test (dt_col DATETIME NULL,
                      ts_col TIMESTAMP NULL);
