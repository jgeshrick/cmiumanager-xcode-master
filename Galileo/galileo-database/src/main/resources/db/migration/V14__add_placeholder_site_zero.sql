-- MSPD-1537
-- Allow a MIU to be assigned to site ID 0, so that it can be unclaimed

INSERT IGNORE INTO `site_list` (`site_id`, `name`) VALUES (0,  CONCAT('Site 0 placeholder (', CURDATE(), ')')); 
