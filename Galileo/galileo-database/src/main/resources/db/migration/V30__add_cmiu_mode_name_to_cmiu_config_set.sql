-- MSPD-1968 Add a CMIU mode name for config sets.

ALTER TABLE mdce.cmiu_config_set ADD COLUMN cmiu_mode_name VARCHAR(50) NULL DEFAULT NULL;

UPDATE mdce.cmiu_config_set SET cmiu_mode_name = 'Basic' WHERE reporting_interval_mins = 1440 AND recording_interval_mins = 60;
UPDATE mdce.cmiu_config_set SET cmiu_mode_name = 'Advanced' WHERE reporting_interval_mins = 240 AND recording_interval_mins = 60;
UPDATE mdce.cmiu_config_set SET cmiu_mode_name = 'Pro' WHERE reporting_interval_mins = 60 AND recording_interval_mins = 15;
UPDATE mdce.cmiu_config_set SET cmiu_mode_name = 'Unknown' WHERE cmiu_mode_name IS NULL;
