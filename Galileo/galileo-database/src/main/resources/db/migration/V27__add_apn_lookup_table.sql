-- MSPD-1694
-- Add APN Lookup Table

CREATE TABLE apn_lookup (
apn_id INT UNSIGNED AUTO_INCREMENT,
mobile_network_operator ENUM('ATT', 'VZW'),
apn VARCHAR(100),
PRIMARY KEY (apn_id));
