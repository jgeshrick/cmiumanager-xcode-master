-- MDCE-2201
-- Adds a site_id for all MIU types.

CREATE TABLE mdce.miu_owner_history
(
  `miu_owner_history_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `miu_id` INT(10) UNSIGNED NOT NULL,
  `site_id` INT(10) UNSIGNED NOT NULL,
  `timestamp` DATETIME NOT NULL,
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`miu_owner_history_id`),
  CONSTRAINT `fk_miu_owner_history_miu_details` FOREIGN KEY (`miu_id`) REFERENCES `miu_details` (`miu_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- Populate the table from any available data
INSERT INTO mdce.miu_owner_history (`miu_id`, `site_id`, `timestamp`, `created`)
SELECT DISTINCT miu_id, site_id, `timestamp`, NOW()
FROM
(
  SELECT cch.miu_id, cch.site_id, cch.`timestamp`
  FROM mdce.cmiu_config_history AS cch
  WHERE cch.site_id <>
  (
    SELECT lag.site_id
    FROM mdce.cmiu_config_history AS lag
    WHERE cch.miu_id = lag.miu_id AND cch.`timestamp` > lag.`timestamp`
    ORDER BY lag.`timestamp` DESC
    LIMIT 1
  )

  UNION ALL

  SELECT cch.miu_id, cch.site_id, cch.`timestamp`
  FROM mdce.cmiu_config_history AS cch
  WHERE cch.cmiu_config_history_id IN
  (
    SELECT MIN(cmiu_config_history_id)
    FROM mdce.cmiu_config_history
    GROUP BY miu_id
  )
) AS hist
ORDER BY miu_id, `timestamp`;
