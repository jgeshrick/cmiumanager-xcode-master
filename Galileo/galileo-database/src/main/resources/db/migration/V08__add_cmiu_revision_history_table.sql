-- MSPD-1006
-- Add cmiu_revision_history table

CREATE TABLE cmiu_revision_history (
cmiu_revision_id INT UNSIGNED AUTO_INCREMENT,
miu_id INT UNSIGNED,
revision_timestamp TIMESTAMP,
firmware_revision VARCHAR(24),
config_revision VARCHAR(24),
arb_revision VARCHAR(24),
ble_revision VARCHAR(24),
encryption_revision VARCHAR(24),
telit_firmware_revision VARCHAR(100),
PRIMARY KEY (cmiu_revision_id),
FOREIGN KEY (miu_id) REFERENCES miu_details(miu_id));
