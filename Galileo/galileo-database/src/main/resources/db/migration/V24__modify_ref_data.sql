-- MSPD-1911
-- Increasing reference data column sizes to match SalesLogix database

ALTER TABLE ref_data_info
  CHANGE COLUMN `system_id` `system_id` varchar(32) DEFAULT NULL,
  CHANGE COLUMN `account_name` `account_name` varchar(128) DEFAULT NULL,
  CHANGE COLUMN `address1` `address1` varchar(64) DEFAULT NULL,
  CHANGE COLUMN `address2` `address2` varchar(64) DEFAULT NULL,
  CHANGE COLUMN `address3` `address3` varchar(64) DEFAULT NULL,
  CHANGE COLUMN `country` `country` varchar(64) DEFAULT NULL,
  CHANGE COLUMN `account_manager` `account_manager` varchar(64) DEFAULT NULL,
  CHANGE COLUMN `regional_manager` `regional_manager` varchar(64) DEFAULT NULL,
  CHANGE COLUMN `type` `type` varchar(64) DEFAULT NULL,
  CHANGE COLUMN `sub_type` `sub_type` varchar(64) DEFAULT NULL,
  CHANGE COLUMN `status` `status` varchar(64) DEFAULT NULL
;

ALTER TABLE ref_data_info_contact
  CHANGE COLUMN `contact_name` `contact_name` varchar(65) DEFAULT NULL,
  CHANGE COLUMN `work_phone` `work_phone` varchar(32) DEFAULT NULL
;


