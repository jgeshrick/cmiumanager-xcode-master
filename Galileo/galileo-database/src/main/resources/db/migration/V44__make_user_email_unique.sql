-- MSPD-2048
-- Make user email unique

-- Copy IDs of users with duplicate emails into temp table
CREATE TABLE mdce.tmp_dup_users (dup_user_id INT);

INSERT INTO mdce.tmp_dup_users (dup_user_id)
SELECT user_id AS dup_user_id
FROM mdce.user_list
WHERE user_email IN
(
    SELECT user_email
    FROM mdce.user_list
    GROUP BY user_email
    HAVING count(*) > 1
);

SET SQL_SAFE_UPDATES = 0;

-- NULL duplicates from the temp table
UPDATE mdce.user_list u
INNER JOIN (
    SELECT dup_user_id
    FROM mdce.tmp_dup_users
) AS dups ON dups.dup_user_id = u.user_id
SET user_email = NULL;

SET SQL_SAFE_UPDATES = 1;

-- Clean up
DROP TABLE mdce.tmp_dup_users;

ALTER TABLE mdce.user_list
  ADD UNIQUE (user_email);
