-- MSPD-1404
-- Add "accepted" state for command handling

ALTER TABLE miu_commands MODIFY COLUMN status ENUM('created','queued','committed','recalled','cancelled','rejected','accepted');

