-- MSPD-2077
-- Add App EUI and App Key to lora_device_details table

ALTER TABLE mdce.lora_device_details 
ADD COLUMN app_eui VARCHAR(33) UNIQUE,
ADD COLUMN app_key VARCHAR(65),
MODIFY COLUMN lora_network ENUM('Senet', 'Actility');

