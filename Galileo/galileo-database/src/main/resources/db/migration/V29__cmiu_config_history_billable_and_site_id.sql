-- MSPD-1979 CMIU config history not being updated
-- Track historic site ID and billable flags for billing purposes

ALTER TABLE `mdce`.`cmiu_config_history`
ADD COLUMN `site_id` INT(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `change_note`,
ADD COLUMN `billable` VARCHAR(1) NOT NULL DEFAULT 'N' AFTER `site_id`;

UPDATE cmiu_config_history cch SET cch.site_id = COALESCE((SELECT site_id from miu_details where miu_id = cch.miu_id), 0),
  cch.billable = COALESCE((SELECT meter_active from miu_details where miu_id = cch.miu_id), 'N');
