-- MSPD-1310
-- Add support for recording manufacturing information

CREATE TABLE IF NOT EXISTS `mdce`.`miu_manufacture_info` (
  `miu_id` INT UNSIGNED NOT NULL UNIQUE,
  `miu_manufacture_info_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `rework_count` INT UNSIGNED,
  `device_creation_timestamp` DATETIME,
  `device_first_test_timestamp` DATETIME,
  `device_last_test_timestamp` DATETIME,
  `device_registration_timestamp` DATETIME,
  `device_activation_timestamp` DATETIME,
  `device_image_modem_library` varchar(30),
  `device_image_cmiu_application` varchar(30),
  `device_image_cmiu_bootloader` varchar(30),
  `device_image_cmiu_configuration` varchar(30),
  `device_modem_firmware_revision` varchar(30),
  `device_bluetooth_mac_address` varchar(30),
  PRIMARY KEY (`miu_manufacture_info_id`),
  CONSTRAINT `fk_miu_manufacture_info_miu_details1`
    FOREIGN KEY (`miu_id`)
    REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `mdce`.`miu_manufacture_info_test_station` (
  `miu_manufacture_info_test_station_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` ENUM('fts', 'cvs'),
  `bay` varchar(30),
  `rack_id` varchar(30),
  `fixture_id` varchar(30),
  `calibration_date` DATETIME,
  `labview_application_version` varchar(30),
  `labview_platform_version` varchar(30),
  `ncc_version` varchar(30),
  `os_description` varchar(30),
  `btle_dongle_driver_version` varchar(30),
  PRIMARY KEY (`miu_manufacture_info_test_station_id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `mdce`.`miu_manufacture_info_tests` (
  `miu_manufacture_info_id` INT UNSIGNED NOT NULL,
  `miu_manufacture_info_test_station_id` INT UNSIGNED NOT NULL,
  `miu_manufacture_info_test_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `test_timestamp` DATETIME,
  PRIMARY KEY (`miu_manufacture_info_test_id`),
  CONSTRAINT `fk_miu_manufacture_info_tests_miu_manufacture_info1`
    FOREIGN KEY (`miu_manufacture_info_id`)
    REFERENCES `mdce`.`miu_manufacture_info` (`miu_manufacture_info_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_miu_manufacture_info_tests_miu_manufacture_info_test_station1`
    FOREIGN KEY (`miu_manufacture_info_test_station_id`)
    REFERENCES `mdce`.`miu_manufacture_info_test_station` (`miu_manufacture_info_test_station_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `mdce`.`miu_manufacture_info_tests_log` (
  `miu_manufacture_info_test_id` INT UNSIGNED NOT NULL,
  `miu_manufacture_info_test_log_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `log` varchar(500),
  PRIMARY KEY (`miu_manufacture_info_test_log_id`),
  CONSTRAINT `fk_miu_manufacture_info_tests_log_miu_manufacture_info_tests`
    FOREIGN KEY (`miu_manufacture_info_test_id`)
    REFERENCES `mdce`.`miu_manufacture_info_tests` (`miu_manufacture_info_test_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
