-- MSPD-1726
-- Removing the eui to miu ID table as it's no longer needed now MIU ID is part of the EUI

DROP TABLE mdce.eui_details;
