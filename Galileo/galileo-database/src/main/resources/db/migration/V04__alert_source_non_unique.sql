-- MSPD-1008
-- make alert source non-unique

ALTER TABLE alert_alerts DROP INDEX alert_source_UNIQUE;
