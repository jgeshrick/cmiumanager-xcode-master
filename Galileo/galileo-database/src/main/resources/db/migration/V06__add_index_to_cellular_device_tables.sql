-- MSPD-1114
-- Add Timestamp

ALTER TABLE `mdce`.`cellular_device_details`
ADD UNIQUE INDEX `miu_id_UNIQUE` (`miu_id` ASC);
