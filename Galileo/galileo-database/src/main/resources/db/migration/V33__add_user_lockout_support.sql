-- MSPD-1396
-- Add account lockout behaviors

ALTER TABLE `user_list` ADD COLUMN `account_lockout_enabled` TINYINT(1) DEFAULT 1;
ALTER TABLE `user_list` ADD COLUMN `failed_login_attempts` INT(10) DEFAULT 0;
ALTER TABLE `user_list` ADD COLUMN `disabled_until` DATETIME DEFAULT NULL;
