-- MSPD-1310
-- Modify enum for cmiu lifecycle state

ALTER TABLE cmiu_lifecycle_state MODIFY COLUMN state ENUM('Newborn', 'PreActivating', 'PreActivated', 'Activated', 'Heard', 'PrePrePot', 'PrePot', 'PostPot', 'Confirmed', 'Development', 'Scanned', 'Shipped', 'Unclaimed', 'Claimed', 'RmaEd', 'Dead', 'Rogue', 'Aging', 'Unspecified');
