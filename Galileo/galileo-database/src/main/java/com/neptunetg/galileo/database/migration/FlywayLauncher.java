/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.galileo.database.migration;

import org.flywaydb.core.Flyway;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Properties;

/**
 * Simple script to launch Flyway migrations
 */
public class FlywayLauncher
{

    private static String jdbcUrl, username, password;
    private static boolean migrate = true;

    public static void main(String[] args) throws Exception
    {
        if (!parseParameters(args))
        {
            printUsage();
            System.exit(1);
        }

        final Flyway flyway = new Flyway();
        flyway.setDataSource(jdbcUrl, username, password);

        if (migrate)
        {
            flyway.migrate();
        }
        else
        {
            flyway.repair();
        }

        System.exit(0);
    }

    private static void printClassPath()
    {
        final ClassLoader cl = FlywayLauncher.class.getClassLoader();

        final URL[] urls = ((URLClassLoader)cl).getURLs();

        System.out.println("Classpath:");
        for(URL url: urls){
            System.out.println(url.getFile());
        }
    }

    private static boolean parseParameters(String[] args) throws Exception
    {
        final Iterator<String> argIterator = Arrays.asList(args).iterator();
        String host = null, databaseName = null;

        while (argIterator.hasNext())
        {
            final String arg = argIterator.next();
            if ("-h".equals(arg) && argIterator.hasNext())
            {
                host = argIterator.next();
            }
            else if ("-u".equals(arg) && argIterator.hasNext())
            {
                username = argIterator.next();
            }
            else if (arg.startsWith("-p"))
            {
                password = arg.substring(2);
            }
            else if (arg.startsWith("-c"))
            {
                final String command = arg.substring(2);
                if ("migrate".equalsIgnoreCase(command))
                {
                    migrate = true;
                }
                else if ("repair".equalsIgnoreCase(command))
                {
                    migrate = false;
                }
                else
                {
                    return false;
                }
            }
            else if (!arg.startsWith("-") && databaseName == null)
            {
                databaseName = arg;
            }
        }

        if (host != null)
        {
            if (databaseName != null)
            {
                jdbcUrl = "jdbc:mysql://" + host + "/" + databaseName;
            }
            else
            {
                jdbcUrl = "jdbc:mysql://" + host;
            }
        }

        final Properties mdceEnvProperties = new Properties();
        final URL envPropertiesFile = FlywayLauncher.class.getResource("/mdce-env.properties");
        if (envPropertiesFile != null)
        {

            mdceEnvProperties.load(envPropertiesFile.openStream());
            if (jdbcUrl == null)
            {
                jdbcUrl = mdceEnvProperties.getProperty("mysql.url");
                System.out.println("Using host " + jdbcUrl + " from " + envPropertiesFile);
            }
            if (username == null)
            {
                username = mdceEnvProperties.getProperty("mysql.username");
                System.out.println("Using username " + username + " from " + envPropertiesFile);
            }
            if (password == null)
            {
                password = mdceEnvProperties.getProperty("mysql.password");
                System.out.println("Using password from " + envPropertiesFile);
            }
        }
        else
        {
            System.out.println("Note: mdce-env.properties not found in classpath.");
            printClassPath();
        }

        return jdbcUrl != null && username != null && password != null;
    }

    private static void printUsage()
    {
        //designed in imitation of mysql client command line
        System.out.println("Usage: java -cp /etc/mdce/conf.d/;* com.neptunetg.mdce.database.migration.FlywayLauncher [-h <mysqlServerHost>] [-u <mysqlUserName>] [-p<password>] [<databaseName>] [migrate|repair]");
        System.out.println("URL and credentials will be read from /etc/mdce/conf.d/mdce-env.properties if it is present on the CLASSPATH and the values have not been provided on the command line.");
    }
}
