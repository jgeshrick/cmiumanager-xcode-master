echo off

echo Running pre-commit checks in directory %cd%
echo.

set /a bad=0

set interactive=1
echo %cmdcmdline% | find /i "%~0" >nul
if not errorlevel 1 set interactive=0


echo common-packet-utils.version
for /f %%f in ('dir /S /B pom.xml') do (
	find "<common-packet-utils.version>" %%f
)
echo.

echo mdce-common-aws-packet-storage.version
for /f %%f in ('dir /S /B pom.xml') do (
	find "<mdce-common-aws-packet-storage.version>" %%f
)
echo.

echo mdce-common-internal-api.version
for /f %%f in ('dir /S /B pom.xml') do (
	find "<mdce-common-internal-api.version>" %%f
)
echo.

echo common-external-api.version
for /f %%f in ('dir /S /B pom.xml') do (
	find "<common-external-api.version>" %%f
)
echo.

echo common-aws-ipc.version
for /f %%f in ('dir /S /B pom.xml') do (
	find "<common-aws-ipc.version>" %%f
)
echo.

echo common-cellular-network-interface.version
for /f %%f in ('dir /S /B pom.xml') do (
	find "<common-cellular-network-interface.version>" %%f
)
echo.

echo common-miu-management.version
for /f %%f in ('dir /S /B pom.xml') do (
	find "<common-miu-management.version>" %%f
)
echo.


echo checking for snapshot dependencies in pom.xml files
for /f %%f in ('dir /S /B pom.xml') do (
	
	type %%f | find ".version>" | find "SNAPSHOT" | find /V "<!--" > sn.txt
	for %%S in (sn.txt) do if %%~zS gtr 0 (
		echo ** SNAPSHOT VERSIONS FOUND IN %%f **
		type sn.txt
		set /a bad = bad + 1
	)
	del sn.txt
)
echo.

echo checking for env files not "local-dev"
for /f %%f in ('dir /S /B mdce-env.properties ^| find /V "environments" ^| find /V "target"') do (
	
	type %%f | find "env.name" | find /V "#" | find /V "local-dev" > sn.txt
	type %%f | find "aws.dynamo.endpoint" | find /V "#" | find /V "localhost" >> sn.txt
	for %%S in (sn.txt) do if %%~zS gtr 0 (
		echo ** PROBLEMS WITH %%f - env should be local-dev, dynamo should be pointing at localhost **
		type sn.txt
		set /a bad = bad + 1
	)
	
	del sn.txt
)
echo.


echo checking for AWS credentials
for /f %%f in ('dir /S /B *credentials.properties ^| find /V "target"') do (
	
	type %%f | find "Key" | find /V "dummy" > sn.txt
	for %%S in (sn.txt) do if %%~zS gtr 0 (
		echo ** PROBLEMS WITH %%f - replace key values with "dummy" before commit **
		type %%f
		set /a bad = bad + 1
	)
	
	del sn.txt
)
echo.

echo checking for Java files without copyright
for /f %%f in ('dir /S /B *.java ^| find /V "target"') do (
	
	find /C "Copyright" %%f | find ": 0" > sn.txt
	for %%S in (sn.txt) do if %%~zS gtr 0 (
		echo ** PROBLEM WITH %%f - no copyright **
		set /a bad = bad + 1
	)
	
	del sn.txt
)
echo.

echo %bad% problem files found
echo.

if _%interactive%_==_0_ pause

set ERRORLEVEL=%bad%
