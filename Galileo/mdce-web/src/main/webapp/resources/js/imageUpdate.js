/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

$(document).ready(function () {
    //register dynamic list
    $("#cmiu-dynamic-list").dynamiclist(
        {addCallbackFn: dynamicListChanged}
    );

    /**
     * Jquery extension to filter option list based on matching data attribute
     * @param imageTypeSelectBox the image type select box where all options are already populated, with data attribute
     * on each option containing the keyword to filter
     * @returns {Array|Object|string|*}
     */
    jQuery.fn.filterBySelectedImageType = function(imageTypeSelectBox) {
        return this.each(function() {
            var select = this;

            //copy the option list into memory
            var options = [];
            $(select).find('option').each(function() {
                options.push({value: $(this).val(), text: $(this).text(), imagetype: $(this).data("image-type")});
            });

            //copy the options to select data attribute
            $(select).data('options', options);

            //empty the select option list
            $(select).empty();

            //bind select box change to dynamically generate option matching imageType from global option list.
            $(imageTypeSelectBox).bind('change', function() {
                var options = $(select).empty().scrollTop(0).data('options');
                var selectedImageType = $.trim($(this).val());

                $.each(options, function(i) {
                    var option = options[i];
                    if(option.imagetype === selectedImageType) {
                        $(select).append(
                            $('<option>').text(option.text).val(option.value)
                        );
                    }
                });

            });
        });
    };

    //when register new image page, image type select box is changed, filter the artifact select list to selected image type
    $("#selectedImageSource").filterBySelectedImageType($("#imageType"));

    //suppress enter and listen to cmiu id change event
    $(".cmiu-id").keypress(function(e) {
        if (e.keyCode == 13) {

            e.stopPropagation();
            e.preventDefault();
            $(this).trigger("change");
        }
    });

    $(".cmiu-id").change(function() {
        onCmiuInputChanged($(this));
    });

    function extractVersion($dropdown) {
        var $versionNumberField = $("input#versionNumber");
        var filename = $dropdown.val();
        filename = filename.replace(/\.[A-Za-z]+$/, "");
        var verRegex = /[0-9][0-9\.\-A-Za-z]+/g;
        var extractedVersion = "";
        while ((matches = verRegex.exec(filename)) !== null) {
            extractedVersion = matches[0].substring(0, 24); //max length 24 characters
        }
        $versionNumberField.val(extractedVersion);
    }

    $(".fota-image-file").on("change", function(e) {
        extractVersion($(this));
    });
    extractVersion($(".fota-image-file"));

});

var onCmiuInputChanged = function (cmiuTableCell)
{
    try
    {
        //alert("value is: " + cmiuTableCell.val());   //display selected value
        var cmiuDescription = cmiuTableCell.val() + " NOT AVAILABLE";

        $.ajax({
            'url': "metadata?cmiuid=" + cmiuTableCell.val(),
            'success': function (data) {
                cmiuDescription = data.metaData;
                cmiuTableCell.parent("td").next("td").find("span").text(cmiuDescription);
            }
        });
    }
    catch (err)
    {
        alert(err);
    }
};

var dynamicListChanged = function (newItem)
{
    newItem.find(".cmiu-id").change(function(){
        onCmiuInputChanged($(this));
    });
};

