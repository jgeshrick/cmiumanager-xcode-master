/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */


$(document).ready(function () {
    $('#get-packet-dump-button').click(function () {
        var idType = $('#idTypePacketDump').val();
        var id = $('#idValPacketDump').val();
        if (id == '')
        {
            id = '0';
        }
        var fromDaysAgo = $('#fromDaysAgo').val();

        var url = 'packetdump?'
            + idType + "=" + id
            + "&days=" + fromDaysAgo;

        console.log(url);

        location.href = url;

    });
});

$(document).ready(function () {
    $('#get-connection-history-button').click(function(){
        var mno = $('#mno').val();
        var idType = $('#idTypeCnsDump').val();
        var id = $('#idValCnsDump').val();
        if (id == '')
        {
            id = '0';
        }
        var fromDate = encodeURIComponent($('#fromDate').val());
        var toDate = encodeURIComponent($('#toDate').val());

        var url = 'getcnsdump/' + mno + '?'
            + idType + "=" + id
            + '&from=' + fromDate + '&to=' + toDate;

        console.log(url);

        location.href = url;
    });
});

$(document).ready(function(){
    $('#get-usage-history-button').click(function(){
        var mno = $('#mno').val();
        var idType = $('#idTypeCnsDump').val();
        var id = $('#idValCnsDump').val();
        if (id == '')
        {
            id = '0';
        }
        var fromDate = encodeURIComponent($('#fromDate').val());
        var toDate = encodeURIComponent($('#toDate').val());

        var url = 'downloadcnsdump/' + mno + '?'
            + idType + "=" + id
            + '&from=' + fromDate + '&to=' + toDate;

        console.log(url);

        location.href = url;

    });
});
