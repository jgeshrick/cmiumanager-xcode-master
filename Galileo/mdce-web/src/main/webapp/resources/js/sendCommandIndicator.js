/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

$(document).ready(function() {
    $('#commandForm').submit(function() {
        $('#sendCommand').css('color', 'grey');
        $('#sendCommand').css('font-weight', 'bold');
        $('#sendCommand').attr('disabled', true);
        return true;
    });
});

