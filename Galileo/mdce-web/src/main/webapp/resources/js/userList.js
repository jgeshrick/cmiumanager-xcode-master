/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

//requires jquery!

$(function () {
    /* Format and initialise modal dialog for delete user confirmation
     * */

    $("#dialog").dialog({
        autoOpen: false,
        modal: true,
        width: 600,
        closeOnEscape: true,    //press ESC key to close this dialog box
        draggable: false,
/*        buttons: {
            "Confirm delete": function () {
                $(this);
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }*/
    });


    /**
     * setup button click handler when add to/change ticket button within a form is clicked
     */
    $(".delete-user-action").click(function () {
        var deleteUrl = $(this).attr('href');
        var userName = $(this).data('username');

        $("#user-name-to-delete").html(userName);

        $("#dialog")
        .dialog("option", "buttons",
            [{
                text: "Confirm delete",
                click: function () {

                    // transfer to the url:
                    window.location.assign(deleteUrl);

                    //close this modal dialog
                    $(this).dialog("close");
                },
            },
                {
                    text: "Cancel",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ])
        .dialog("open");

        return false;
    });
})
;



