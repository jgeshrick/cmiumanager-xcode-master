/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

//an eye on your file inputs and fire an event called fileselect when a file is chosen.
$(document).on('change', '.button-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

//subscribe to fileselect event
$(document).ready( function() {
    $('.button-file :file').on('fileselect', function(event, numFiles, label) {
        //console.log(numFiles);
        //console.log(label);

        $('#selected-file-name').val(label);
    });
});
