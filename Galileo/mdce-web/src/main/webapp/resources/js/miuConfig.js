/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

$(function() {
    $("a.column-sort").click(sortableHeaderClicked);

    $("a.column-sort").after("<span class=\"sort-direction\"></span>");

    $("div.group-table[src]").each(function(){
        var selector = "#" + $(this).attr("id");

        loadSection(selector);
    });

    // Expand-collapse
    expandCollapseInitialize();

    $("#filterMiuButton").click(filterMiuButtonClicked);

    $("#miuIdFilter").keypress(function(e) {
        // Submit when enter is pressed
        if (e.which === 13) {
            e.preventDefault();
            $("#filterMiuButton").click();
        }
    });

    $(".reject-command-button").click(function () {
        var commandLink = $(this).parent("a");

        // Set up dialog box
        $("#dialog-message").html("Are you sure?");

        $("#dialog").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Confirm": function () {
                    $(this).dialog("close");
                    commandLink.trigger("click");
                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });

        return false;
    });

});

function sortableHeaderClicked(e) {
    var selectedSortBy = $(this).attr("sortBy"); // a tag sortBy
    var contentDiv = $(this).closest("div.group-table").next("div.group-content");
    var currentSortList = $(contentDiv).attr("sortBy").split(",");
    var firstSortBy = tryPopField(currentSortList, selectedSortBy);
    var newSortExpression;

    // Change direction if this is already the selected sort-by field
    var sortOrder;
    if (firstSortBy !== null && firstSortBy.substr(1) == selectedSortBy) {
        sortOrder = firstSortBy.substr(0, 1);
        var sortColumn = firstSortBy.substr(1);

        if (selectedSortBy === sortColumn) {
            sortOrder = (sortOrder === "+") ? "-" : "+"; // Toggle sort order
        } else {
            sortOrder = "+"; // Default to ascending
        }
    } else {
        // Otherwise, insert at the front.
        sortOrder = "+";
    }

    newSortExpression = sortOrder + selectedSortBy;

    currentSortList.splice(0, 0, newSortExpression);

    $(contentDiv).attr("sortBy", currentSortList.join(","));

    var selector = "#" + $(contentDiv).attr("id");
    loadSection(selector);

    e.preventDefault();
}

function expandCollapseClicked(e) {
    var expanded = $(this).closest("div.group").find(".group-content").attr("expanded") !== "true";
    $(this).closest("div.group").find(".group-header").css("display", expanded ? "block" : "none");
    $(this).closest("div.group").find(".group-content").css("display", expanded ? "block" : "none");
    $(this).text(expanded ? "Collapse" : "Expand");
    $(this).closest("div.group").find(".group-content").attr("expanded", expanded ? "true" : "false");

    e.preventDefault();
}

function expandCollapseInitialize() {
    $("a.expand-collapse-section").each(function() {
        var expanded = $(this).closest("div.group").find(".group-content").attr("expanded") === "true";
        $(this).closest("div.group").find(".group-header").css("display", expanded ? "block" : "none");
        $(this).closest("div.group").find(".group-content").css("display", expanded ? "block" : "none");
        $(this).text(expanded ? "Collapse" : "Expand");
        $(this).click(expandCollapseClicked);
    });
}

// If the named field is in the sort list, remove it from the array. If it is also the first in the list, return it
// with its direction prefix.
function tryPopField(currentSortList, fieldName) {
    var index = currentSortList.indexOf("+" + fieldName);
    if (index !== -1) {
        currentSortList.splice(index, 1);
        if (index === 0) {
            return "+" + fieldName;
        }
    }

    index = currentSortList.indexOf("-" + fieldName);
    if (index !== -1) {
        currentSortList.splice(index, 1);
        if (index === 0) {
            return "-" + fieldName;
        }
    }

    return null;
}

// Determines whether the element el is off-screen in the up or down direction.
function isOffScreen (el, direction) {
    var rect = el.first()[0].getBoundingClientRect();
    var result = false;
    if (direction === "up") {
        result = (rect.top + rect.height < 0);
    } else {
        result = (rect.bottom > window.innerHeight);
    }
    return result;
};

// (Re-)binds element events when sections are (re-)loaded.
function bindEvents() {
    $(".flush-pending-commands").hover(flushPendingMouseOver, flushPendingMouseOut);
}

// Mouse-over to alert the user to other, off-screen commands that will be affected by this 'flush' action.
function flushPendingMouseOver() {
    var miuId = $(this).attr("miuid");
    var miuDivs = $(this).closest("div.group-table").find("span[miuid='" + miuId + "']").find("a");

    $(miuDivs).toggleClass("highlight-miu", true);

    $(miuDivs).each(function(){
        if (isOffScreen($(this), "up")) {
            $("#more-above").css("display", "block");
        }
    });

    $(miuDivs).each(function(){
        if (isOffScreen($(this), "down")) {
            $("#more-below").css("display", "block");
        }
    });
}

// Hide alerts related to 'flush' actions.
function flushPendingMouseOut() {
    var miuId = $(this).attr("miuid");
    var miuDivs = $(this).closest("div.group-table").find("span[miuid='" + miuId + "']").find("a");
    $(miuDivs).toggleClass("highlight-miu", false);
    $("#more-above").css("display", "none");
    $("#more-below").css("display", "none");
}

// Initiates the loading of the section whose content div is selected by 'elementSelector'.
function loadSection(elementSelector) {
    var baseUrl = $("body").attr("configUrl");
    var src = $(elementSelector).attr("src");
    var sortBy = $(elementSelector).attr("sortBy");
    var filterByMiu = $("#miuIdFilter").val();
    var filterByNetwork = $("#networkProvider").val();
    $(elementSelector).parent().find(".loading").css("display", "block");

    var queryParameters = "?sortBy=" + encodeURIComponent(sortBy);
    if (filterByMiu) {
        queryParameters += "&miuIdFilter=" + encodeURIComponent(filterByMiu);
    }

    if (filterByNetwork) {
        queryParameters += "&networkFilter=" + encodeURIComponent(filterByNetwork)
    }

    $.ajax({
        url: baseUrl + src + queryParameters,
        type: "get",
        contentType: "text/html",
        success: function(data, textStatus, jqXhr) {
            displayResult(elementSelector, data, textStatus, jqXhr);
        }
    });

    refreshSortDirectionIndicators(elementSelector);
}

// Displays the data for a section when it has been successfully loaded.
function displayResult(elementSelector, data, textStatus, jqXhr) {
    $(elementSelector).html(data);
    bindEvents();
    $(elementSelector).parent().find(".loading").css("display", "none");

    // Check whether the login screen was returned
    if ($(elementSelector).find("input[id=\"username\"]").length) {
        location.reload(true);
    }
}

// Set direction indicator in headers for the given section
function refreshSortDirectionIndicators(contentElementSelector) {
    var sortBy = $(contentElementSelector).attr("sortby");
    var direction = sortBy.substr(0, 1);
    var field = sortBy.split(",")[0].substr(1);

    // Clear existing indicators
    $(contentElementSelector).prevAll("div.group-header").find("span.sort-direction").text("");

    var sortIndicator = (direction === "+") ? "&nbsp;&#x25B2;" : "&nbsp;&#x25BC;";
    $(contentElementSelector).prevAll("div.group-header").find("a.column-sort[sortBy=\"" + field + "\"]").nextAll("span.sort-direction").html(sortIndicator);
}

// Applies a new MIU ID filter.
function filterMiuButtonClicked() {
    var miuIdFilter = $("#miuIdFilter").val();
    var networkFilter = $("#networkProvider").val();
    var filters = [];

    if (miuIdFilter) {
        filters.push("MIU ID: " + miuIdFilter);
    }

    if (networkFilter) {
        filters.push("Network: " + networkFilter);
    }

    if (filters.length == 0) {
        $(".h2-extra-info").text("");
    } else {
        $(".h2-extra-info").text(" (filtered by " + filters.join(", ") + ")");
    }

    $("div.group-table[src]").each(function(){
        var selector = "#" + $(this).attr("id");

        loadSection(selector);
    });
}