/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

/**
 * DateTimePicker initialisation
 * https://github.com/xdan/datetimepicker
 */

$(document).ready(function()
{
    $(".dateTimePicker").datetimepicker(
        {
            format:'Y-m-d H:i',
            step:15
        });
});