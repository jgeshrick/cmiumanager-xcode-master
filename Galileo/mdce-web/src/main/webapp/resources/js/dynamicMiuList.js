/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */


$(document).ready(function () {

    //register dynamic list
    $("#miu-dynamic-list").dynamiclist({
            addCallbackFn: function (newItem)
            {
                newItem.find(".cmiu-id").each(function() {
                    bindTriggers($(this));
                    $(this).trigger("change");
                });
            }
        });

    bindTriggers($(".cmiu-id"));

});


function bindTriggers (miuInputs)
{

    miuInputs.keypress(function(e) {
        onMiuInputKeypress(e);
    });

    miuInputs.change(function() {
        onMiuInputChanged($(this));
    });

}


function onMiuInputKeypress(e)
{
    //suppress enter
    if (e.keyCode == 13) {

        e.stopPropagation();
        e.preventDefault();
        $(this).trigger("change");
    }
}

function onMiuInputChanged(miuTableCell)
{
    var miuDescriptionCell = miuTableCell.parent("td").next("td").find("span");
    var miuDescription = "";

    if( miuTableCell.val() != "" ) {

        miuDescription = "Loading...";

        try {
            $.ajax({
                'url': "metadata?miuid=" + miuTableCell.val(),
                'success': function (data) {
                    miuDescription = data.metaData;
                    miuDescriptionCell.text(miuDescription);
                },
                'error': function () {
                    miuDescription =  "N/A";
                    miuDescriptionCell.text(miuDescription);
                }
            });
        }
        catch (err) {
            alert(err);
        }

    }

    miuDescriptionCell.text(miuDescription);

};



