/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

//requires jquery!

// Process selectable row and set/unset checkbox according to selected row
$(document).ready(function () {
    $(".selectable-alert tbody").selectable({
        cancel: "input,textarea,button,select,option,a",
        stop: function () {
            /*
             $("#new-alerts-list").find("[type=checkbox]").prop('checked', false);
             $(".ui-selected", this).each(function () {
             $(this).find("[type=checkbox]").prop('checked', true);
             });
             */
        },
        selected: function (event, ui) {
            //set checkbox within the <tr> selected element
            $(ui.selected).find("[type=checkbox]").prop('checked', true);
        },
        unselected: function (event, ui) {
            //clear checkbox within the <tr> selected element
            $(ui.unselected).find("[type=checkbox]").prop('checked', false);
        }
    });

    // checkbox check/unchecked will change the Jquery select UI too
    $(".selectable-alert input[type=checkbox]").click(function () {
        var selectableRow = $(this).parents('tr.list-item.ui-selectee').first();

        if (this.checked) {
            selectableRow.addClass('ui-selected');
        }
        else {
            selectableRow.removeClass('ui-selected');
            selectableRow.find('.ui-selected').removeClass('ui-selected');
        }
    });

    // check all button
    $(".select-all-button").click(function () {

        var selectableRows = $(this).parent().siblings("table").find(".ui-selectee input[type=checkbox]");
        var bothSelectbuttons = $(this).parent().parent().find(".select-all-button");

        if (selectableRows.length > 0) {
            if ($(this).text() == "Select All") {
                bothSelectbuttons.text("Deselect All");
                selectableRows.each(function (index) {
                    !this.checked ? this.click() : null;
                })
            }
            else {
                bothSelectbuttons.text("Select All");
                selectableRows.each(function (index) {
                    this.checked ? this.click() : null;
                })
            }
        }
    });

    /* Format and initialise modal dialog for querying ticket id
     * */
    $("#dialog").dialog({
        autoOpen: false,
        modal: true,
        width: 800,
        closeOnEscape: true,    //press ESC key to close this dialog box
        draggable: true
    });

    var defaultDialogTitle = "No alerts selected";

    /**
     * setup button click handler when add to/change ticket button within a form is clicked
     */
    $(".change-ticket-button").click(function () {
        //get the form that is owning this button.
        var tableElement = $(this).parents('section').find('table.alert-list').first();
        var formId = $(tableElement).attr("data-form-id");
        var formElement = $("#" + formId).first();
        var isAlertSelected = checkAlertsSelected(tableElement);

        var dialogTitle = $(this).text();

        if (isAlertSelected) {
            $('#dialog-input').show();
        }
        else {
            dialogTitle = defaultDialogTitle;
        }

        $("#dialog")
            .dialog("option", "title", dialogTitle)
            .dialog("option", "buttons",
            [{
                text: "Assign ticket",
                click: function () {
                    //insert ticket id to form
                    var inputTicketId = $('#dialog-input-ticket-id').val();
                    //$('#ticketId').val(inputTicketId);
                    formElement.find('input[name=ticketId]').val(inputTicketId);

                    //modify values to submit
                    formElement.find('input[name=batchClear]').val(0);
                    formElement.find('input[name=batchTicket]').val(1);
                    var selectedIds = getSelectedAlertIds(tableElement);
                    formElement.find('input[name=selectedAlertIds]').val(selectedIds);

                    //submit form
                    formElement.submit();

                    //close this modal dialog
                    $(this).dialog("close");
                },
                disabled: !isAlertSelected      //button disabled if no item selected
            },
                {
                    text: "Cancel",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ])
            .dialog("option", "position", { my: "center", at: "center", of: $(this) })
            .dialog("open");

        return false;
    });

    /**
     * setup button click handler when clear alert button within a form is clicked
     */
    $(".clear-alert-button").click(function () {

        //get the form that is owning this button.
        var tableElement = $(this).parents('section').find('table.alert-list').first();
        var formId = $(tableElement).attr("data-form-id");
        var formElement = $("#" + formId).first();
        var isAlertSelected = checkAlertsSelected(tableElement);

        var dialogTitle = (isAlertSelected) ? "Clear Selected Alerts ?" : defaultDialogTitle;

        $("#dialog")
            .dialog("option", "title", dialogTitle)
            .dialog("option", "buttons",
            [{
                text: "Clear alerts",
                click: function () {
                    //modify values to submit
                    formElement.find('input[name=batchClear]').val(1);
                    formElement.find('input[name=batchTicket]').val(0);
                    var selectedIds = getSelectedAlertIds(tableElement);
                    formElement.find('input[name=selectedAlertIds]').val(selectedIds);

                    //submit form
                    formElement.submit();

                    //close this modal dialog
                    $(this).dialog("close");
                },
                disabled: !isAlertSelected      //button disabled if no item selected

            },
                {
                    text: "Cancel",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ])
            .dialog("open");

        return false;
    });
})
;

function checkAlertsSelected(tableElement) {

    // var formId = $(formElement).attr("id");
    // var tableElement = $("table[data-form-id='" + formId + "'");
    var n = tableElement.find("input:checked").length;

    $('#dialog-input').hide();

    if (n === 0) {
        $("#dialog-message").html("<p>No alert is selected for the action. </p> <p> Click to select alert. Control click or drag to select multiple elements </p>");
        return false;
    }
    else {
        $("#dialog-message").html(n + " alert" + ((n > 1) ? "s" : "") + " selected. Confirm action?");
        return true;
    }
}

// Gets a comma-delimited list of the selected alert IDs within the supplied table element.
function getSelectedAlertIds(tableElement) {
    var selectedIds = $(tableElement)
        .find(".ui-selectee input[type=checkbox]:checked") // Find selected rows
        .map(function () {
            return $(this).attr("data-alert-id");
        }) // Project Alert IDs
        .get() // To array
        .join(); // To comma-delimited string

    return selectedIds;
}


