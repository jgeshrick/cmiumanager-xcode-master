/*
 *  **************************************************************************
 *
 *      Neptune Technology Group
 *      Copyright 2017 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 */
$(function () {

    /**
     * Load stuff
     */
    $(".population-button").click(function () {
        var cmiuConnectionThreshold = $("#cmiu-connection-threshold").val();
        var cmiuMode = $("#cmiu-mode").val();
        var cmiuMno = $("#cmiu-mno").val();
        var cmiuState = $("#cmiu-state").val();
        var cmiuSiteId = $("#cmiu-site").attr("value");

        reset();

        if (!Number.isInteger(parseInt(cmiuConnectionThreshold))) {
            $("#connection-threshold-error")
                .show()
                .attr("data-showing", true);
            return;
        }

        $.ajax({
            url: "/mdce-web/pages/analysis/result" + buildUrlParams(cmiuConnectionThreshold*1000, cmiuMode, cmiuMno, cmiuState, cmiuSiteId),
            method: "GET",
            success: function (data, textStatus, jqXhr) {
                $(".timing-analysis-table").html(data);
                if($("#time-analysis-detail-table").find("tbody tr").length){
                    $(".generate-button").css('display', 'inline-block');
                    convertTableMilliSecondsToSeconds();
                } else {
                    $("#time-analysis-detail-table").remove();
                }
            }
        })
    });

    /**
     * Converting milliseconds to seconds on table.
     */
    function convertTableMilliSecondsToSeconds() {
        var rows = $("#time-analysis-detail-table").find("tbody tr");

        rows.each(function () {
            var connectionTime                  = $(this).find("td:nth-child(2)");
            var registerTime                    = $(this).find("td:nth-child(3)");
            var registerTimeToActivateContext   = $(this).find("td:nth-child(4)");
            var registerTimeToConnect           = $(this).find("td:nth-child(5)");
            var registerTimeToTransferPacket    = $(this).find("td:nth-child(6)");
            var disconnectTime                  = $(this).find("td:nth-child(7)");

            connectionTime.text(parseInt(connectionTime.text())/1000);
            registerTime.text(parseInt(registerTime.text())/1000);
            registerTimeToActivateContext.text(parseInt(registerTimeToActivateContext.text())/1000);
            registerTimeToConnect.text(parseInt(registerTimeToConnect.text())/1000);
            registerTimeToTransferPacket.text(parseInt(registerTimeToTransferPacket.text())/1000);
            disconnectTime.text(parseInt(disconnectTime.text())/1000);
        });
    }

    /**
     * Resetting stuff like error messages.
     */
    function reset() {
        $("#connection-threshold-error").hide();
        $(".generate-button").hide();
        $("#time-analysis-detail-table").remove();
    }

    /**
     * Building ajax url.
     *
     * Connection threshold is converted to milliseconds
     *
     * @param cmiuConnectionThreshold Time in second
     * @param cmiuMode
     * @param cmiuMno
     * @param cmiuState
     * @param cmiuSiteId
     * @returns {string} url
     */
    function buildUrlParams(cmiuConnectionThreshold, cmiuMode, cmiuMno, cmiuState, cmiuSiteId) {
        var urlParams = "?";

        urlParams += "connection-threshold=" + cmiuConnectionThreshold;

        if(cmiuMode != "None"){
            urlParams += "&cmiu-mode=" + cmiuMode
        }

        if(cmiuState != "None") {
            urlParams += "&cmiu-state=" + cmiuState;
        }

        if(cmiuSiteId != undefined) {
            urlParams += "&cmiu-site-id=" + cmiuSiteId;
        }

        if(cmiuMno != "None") {
            urlParams += "&cmiu-mno=" + cmiuMno;
        }

        return urlParams;
    }

    /**
     * Build and download data to CSV file.
     */
    $(".generate-button").click(function () {
        var cmiuConnectionThreshold = $("#cmiu-connection-threshold").val();
        var cmiuMode =  $("#cmiu-mode").val();
        var cmiuMno =   $("#cmiu-mno").val();
        var cmiuState = $("#cmiu-state").val();
        var cmiuSiteId = $("#cmiu-site").val();
        var csvHeader = [["CMIU%20ID", "Total%20Connection%20Time", "Register%20Time",
                        "Register%20Time%20to%20Activate%20Context", "Register%20Time%20to%20Connect", "Register%20Time%20to%20Transfer%20Packet",
                        "Disconnect%20Time", "CMIU%20Mode", "Network", "CMIU%20Lifecycle%20State", "Site%20ID"]];

        var csvBody = [];

        var results = $("#time-analysis-detail-table").find("tbody tr");

        $.each(results, function () {
            var row = [];
            $(this).find("td").each(function () {
                row.push($(this).text());
            });
            csvBody.push(row.join(","));
        });

        var csvString            = csvHeader.concat(csvBody).join("%0A");
        var downloadLink         = document.createElement('a');
        downloadLink.href        = 'data:attachment/csv,' + csvString;
        downloadLink.target      = '_blank';
        downloadLink.download    =  nameCSV(cmiuConnectionThreshold, cmiuMode, cmiuMno, cmiuState, cmiuSiteId) + ".csv";

        document.body.appendChild(downloadLink);
        downloadLink.click();
    });

    /**
     * Create csv name with search conditions.
     *
     * @param cmiuConnectionThreshold
     * @param cmiuMode
     * @param cmiuMno
     * @param cmiuState
     * @param cmiuSiteId
     * @returns {string} name for CSV file
     */
    function nameCSV(cmiuConnectionThreshold, cmiuMode, cmiuMno, cmiuState, cmiuSiteId){

        var csvName = "";

        csvName += "threshold_" + cmiuConnectionThreshold;

        if(cmiuMode != "None"){
            csvName += "__cmiu_mode_" + cmiuMode
        }

        if(cmiuState != "None") {
            csvName += "__cmiu_state_" + cmiuState;
        }

        if(cmiuSiteId != undefined) {
            csvName += "__cmiu_site_id_" + cmiuSiteId;
        }

        if(cmiuMno != "None") {
            csvName += "__cmiu_mno_" + cmiuMno;
        }

        return csvName + "__" + formatDate(new Date()).split(" ").join("_");
    }

    /**
     * Formatting date.
     *
     * @param date
     * @returns {string}
     */
    function formatDate(date) {
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return day + ' ' + monthNames[monthIndex] + ' ' + year;
    }

    $("#cmiu-site").combobox();
});