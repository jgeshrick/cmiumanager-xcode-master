/*
 *  **************************************************************************
 *
 *      Neptune Technology Group
 *      Copyright 2016 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 */

$(document).ready(function () {

    $("#old-site-display").text($("#newSiteId").data('currentsite'));

    $("input#change-ownership-submit").click(
        function() {
            $("#new-site-display").text($("#newSiteId option:selected").text());
            if ($("#old-site-display").text() != $("#new-site-display").text()) {
                $("#dialog-message").dialog({
                    resizable: false,
                    modal: true,
                    width: 500,
                    title: "Really change ownership?",
                    buttons: {
                        'Change ownership': function () {
                            $("form#form-miu-ownership").submit();
                        },
                        Cancel: function () {
                            $(this).dialog('close');
                        }
                    }
                });
                return false;
            } else {
                return true;
            }
        }
    );
});


