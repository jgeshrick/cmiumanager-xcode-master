function ajaxPieChart(url, divId)
{
    $.get(url,
            function(data) {
                initPieChart(data, divId);
            }
    ).fail(function()
            {
                $("#" + divId).text("Pie chart error: Could not get data").css('color', 'red');

            }
        )
}

function initPieChart(data, divId)
{

    if (data.error != null)
    {
        $("#" + divId).text("Cannot display pie chart: " + data.error).css('color', 'red');
    }
    else
    {
        var chart = new AmCharts.AmPieChart();
        chart.dataProvider = data.rows;
        chart.titleField = "status";
        chart.valueField = "count";
        chart.colorField = "color";
        chart.color = "#efefef";

        chart.outlineAlpha = 1;
        chart.outlineColor = chart.color;
        chart.outlineThickness = 2;

        chart.balloonText = "[[title]]\n[[value]] - ([[percents]]%)";
        chart.labelText = "[[title]]\n[[value]] - ([[percents]]%)";
        chart.numberFormatter= {precision:0, decimalSeparator:'.', thousandsSeparator:','};

        chart.startEffect = "bounce";

        chart.write(divId);
    }
}