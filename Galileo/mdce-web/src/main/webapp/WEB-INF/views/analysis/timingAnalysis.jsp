<%--
  ~  Neptune Technology Group
  ~  Copyright 2017 as unpublished work.
  ~
  ~  All rights reserved
  ~  The information contained herein is confidential
  ~  property of Neptune Technology Group. The use, copying, transfer
  ~  or disclosure of such information is prohibited except by express
  ~  written agreement with Neptune Technology Group.
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="lib/jquery-ui/jquery-ui.min.css"/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/glyphicons.css"/>
    <mdce:css href="css/timingAnalysis.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery-ui.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery.ui.combobox.js"/>
    <mdce:js src="js/timingAnalysis.js"/>

   <title> ${mdce:t("Title.Analysis")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>


</head>
<body>

<mdce:header/>

<section id="import-cmiu-list" class="container">
    <h1> CMIU Timing and Analysis Tool</h1>

    <div class="group">
        <div id="cmiu-filters">
            <div class="connection-threshold">
                <label for="cmiu-connection-threshold" class="filter">Connection Threshold (Seconds)</label>
                <input id="cmiu-connection-threshold" type="number" min="0" placeholder="Required" required/>
                <span id="connection-threshold-error" data-showing=false>Threshold is required</span>
            </div>

            <div>
                <label for="cmiu-mode" class="filter">CMIU Mode</label>
                <select id="cmiu-mode">
                    <option>None</option>
                    <option>Unknown</option>
                    <option>Basic Mode</option>
                    <option>Advanced Mode</option>
                    <option>PRO BASIC</option>
                    <option>BASIC PRO</option>
                    <option>Professional Mode</option>
                </select>
            </div>

            <div>
                <label for="cmiu-mno" class="filter">Carrier</label>
                <select id="cmiu-mno">
                    <option>None</option>
                    <option>ATT</option>
                    <option>VZW</option>
                </select>
            </div>

            <div>
                <label for="cmiu-state" class="filter">State</label>
                <select id="cmiu-state">
                    <option>None</option>
                    <option>Active</option>
                    <option>Dead</option>
                    <option>Dead Bed</option>
                    <option>Suspended</option>
                    <option>PreSuspended</option>
                    <option>Claimed</option>
                </select>
            </div>

            <div>
                <label for="cmiu-site" class="filter">Site ID</label>
                <select id="cmiu-site">
                    <option value="None"></option>
                    <c:forEach items="${siteList}" var="site">
                        <option value="${site.siteId}" >${site.siteName} (${site.siteId})</option>
                    </c:forEach>
                </select>
            </div>
        </div>
    </div>

    <div class="population-button">
        <input type="submit" class="button" value="Populate CMIU list"/>
    </div>

    <div class="generate-button">
        <input type="submit" class="button" value="Generate CSV"/>
    </div>

</section>

<section class="timing-analysis-table"></section>

</body>
</html>