<%--
*
* Neptune Technology Group
* Copyright 2017 as unpublished work.
* All rights reserved.
*
* The information contained herein is confidential
* property of Neptune Technology Group. The use, copying, transfer
* or disclosure of such information is prohibited except by express
* written agreement with Neptune Technology Group.
*
* --%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table id="time-analysis-detail-table">
    <thead>
    <tr>
        <th> CMIU ID</th>
        <th> Connection Time</th>
        <th> Register Time</th>
        <th> Register Time to Activate Context</th>
        <th> Register Time to Connect</th>
        <th> Register Time to Transfer Packet</th>
        <th> Disconnect Time</th>
        <th> CMIU Mode</th>
        <th> Carrier</th>
        <th> State</th>
        <th> Site ID</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${CmiuTimingList}" var="cmiu">
        <tr id="${cmiu.miuId}" class="table-item">
            <td class="cmiu-id"> ${cmiu.miuId} </td>
            <td class="cmiu-total-connection-time"> ${cmiu.totalConnectionTimeMilliSeconds} </td>
            <td class="cmiu-register-time"> ${cmiu.registerTimeMilliSeconds} </td>
            <td class="cmiu-register-activation-context-time"> ${cmiu.registerActivateContextTimeMilliSeconds} </td>
            <td class="cmiu-register-connection-time"> ${cmiu.registerConnectionTimeMilliSeconds} </td>
            <td class="cmiu-register-transfer-packet-time"> ${cmiu.registerTransferPacketTimeMilliSeconds} </td>
            <td class="cmiu-disconnection-time"> ${cmiu.disconnectTimeMilliSeconds} </td>
            <td class="cmiu-mode"> ${cmiu.cmiuMode} </td>
            <td class="cmiu-mno"> ${cmiu.mno} </td>
            <td class="cmiu-state"> ${cmiu.state} </td>
            <td class="cmiu-site-id"> ${cmiu.siteId} </td>
        </tr>
    </c:forEach>
    </tbody>
</table>