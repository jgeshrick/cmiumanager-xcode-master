<%--
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/c2.css"/>
    <mdce:css href="css/glyphicons.css"/>
    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery.dynamiclist.js"/>
    <mdce:js src="js/dynamicMiuList.js"/>
    <mdce:js src="js/sendCommandIndicator.js"/>

    <title>${mdce:t("Title.C2")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>

<mdce:header/>
<h1 class="page-heading">${genericCommandForm.commandTypeCmiu.commandDescription}</h1>
<section class="container">
    <form:form id="commandForm" modelAttribute="genericCommandForm" method="post">
        <form:hidden path="commandTypeCmiu"/>

        <div>
            <form:errors path="miuInputList" class="error" element="div" />
        </div>
        <div class="group">
            <div>
                <input type="submit" class="button right" id="add-config-set" name="addConfigSet" value="Add Config Set" />

                <a href="<mdce:url value="config-set" />" class="button right">List Config Sets</a>
            </div>

            <div>
                <form:label path="configSetId">Config Set</form:label>
                <form:select path="configSetId" label="Choose the config set for the MIU/s"  items="${configSetList}" itemLabel="name" itemValue="id" />
            </div>
        </div>

        <mdce:dynamicCmiuEntry miuInputList="${genericCommandForm.miuInputList}"/>

        <div>
            <input type="submit" id="sendCommand" value="Send command" class="button right"/>
        </div>

    </form:form>
</section>

</body>
</html>

