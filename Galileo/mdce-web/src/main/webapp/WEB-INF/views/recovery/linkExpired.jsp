<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<%@ page session="false" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/user.css"/>
    <mdce:css href="css/glyphicons.css"/>
    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery.dynamiclist.js"/>

    <title>${mdce:t("Title.PasswordResetLinkExpired")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>

<mdce:header/>
<h1 class="page-heading">${mdce:t("Title.PasswordResetLinkExpired")}</h1>
<section class="container">
    <div class="group">
        <h2>Expired</h2>

        <p id="link-expired-info">
            <c:url var="resetPasswordUrl" value="/pages/recovery/password"/>
            This password reset link has either passed its expiry time or has been replaced by a newer password reset
            request. If this is not expected, please contact Neptune Technology Group. Otherwise, you can
            <a href="${resetPasswordUrl}">request a new password reset link</a>.
        </p>

        <p>
            <c:url var="loginUrl" value="/pages/user/login"/>
            <a href="${loginUrl}">MDCE login</a>
        </p>
    </div>
</section>

</body>
</html>

