<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<%@ page session="false" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/user.css"/>
    <mdce:css href="css/glyphicons.css"/>
    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery.dynamiclist.js"/>

    <title>${mdce:t("Title.ResetPassword")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

    <sec:csrfMetaTags/>
</head>
<body>

<mdce:header/>
<h1 class="page-heading">${mdce:t("Title.ResetPassword")}</h1>
<section class="container">
    <form:form modelAttribute="resetPasswordFromLinkForm" method="post">
        <div class="group">
            <h2>Reset Password</h2>

            <p>
                Please enter your username and a new password for your account. The user name you enter must be for the
                account associated with ${resetPasswordFromLinkForm.email}.
            </p>

            <p>${error}</p>
            <div>
                <form:errors path="passwordMatch" class="error" element="div" />
            </div>

            <div class="field">
                <form:label path="userName">User name</form:label>
                <form:input path="userName" type="text" id="user-name"/>
                <form:errors path="userName" cssClass="error-text" />
            </div>

            <div class="field">
                <form:label path="newPassword">New password</form:label>
                <input type="password" name="newPassword" id="new-password" />
                <form:errors path="newPassword" cssClass="error-text" />
            </div>

            <div class="field">
                <form:label path="newPasswordRepeated">New password (repeated)</form:label>
                <input type="password" name="newPasswordRepeated" id="new-password-repeated" />
            </div>

            <div class="field">
                <input type="submit" id="reset-password" name="resetPassword" value="Reset Password" class="button right"/>

                <c:url var="loginUrl" value="/pages/user/login"/>
                <a href="${loginUrl}" class="button right">Cancel</a>
            </div>
        </div>
    </form:form>
</section>

</body>
</html>

