<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<%@ page session="false" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/user.css"/>
    <mdce:css href="css/glyphicons.css"/>
    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery.dynamiclist.js"/>

    <title>${mdce:t("Title.ResetPassword")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

    <sec:csrfMetaTags/>
</head>
<body>

<mdce:header/>
<h1 class="page-heading">${mdce:t("Title.ResetPassword")}</h1>
<section class="container">
    <form:form modelAttribute="requestPasswordResetLinkForm" method="post">
        <div class="group">
            <h2>Forgotten Password</h2>

            <p>To reset a forgotten password for an MDCE account, complete the form below.</p>

            <div class="field">
                <form:label path="emailAddress"
                            title="Enter the email address registered to your MDCE account">Email address</form:label>
                <form:input id="email-address" path="emailAddress" type="text"/>
                <form:errors path="emailAddress" cssClass="error-text" />
            </div>

            <div>
                <input type="submit" id="begin-password-recovery" name="beginPasswordRecovery"
                       value="Begin Password Recovery" class="button right"/>

                <c:url var="loginUrl" value="/pages/user/login"/>
                <a href="${loginUrl}" class="button right">Cancel</a>
            </div>
        </div>
    </form:form>
</section>

</body>
</html>

