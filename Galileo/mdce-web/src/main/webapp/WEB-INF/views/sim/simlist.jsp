<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
  <%--Add CSRF token for used in postLink --%>
  <meta name="_csrf" content="${_csrf.token}"/>
  <meta name="_csrf_header" content="${_csrf.headerName}"/>

  <c:url var="resourcesUrl" value="/resources"/>
  <mdce:favicon/>

  <mdce:css href="lib/jquery-ui/jquery-ui.structure.min.css"/>
  <mdce:css href="lib/jquery-ui/jquery-ui.theme.min.css"/>
  <mdce:css href="lib/jquery-ui/jquery.ui.combobox.css" />
  <mdce:css href="lib/jquery-ui/jquery-ui.custom.css"/>

  <mdce:css href="css/glyphicons.css"/>
  <mdce:css href="css/main.css"/>
  <mdce:css href="css/simdetails.css"/>

  <mdce:js src="lib/jquery-1.11.2.min.js"/>
  <mdce:js src="lib/jquery-ui/jquery-ui.min.js"/>
  <mdce:js src="lib/jquery-ui/jquery.ui.combobox.js" />

  <script>
    $(function() {
      $(".ui-comboBox").combobox();
    });
  </script>

  <title>${mdce:t("Title.Sims")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

</head>
<body>

<mdce:header/>
<h1 class="page-heading" id="sim-list-title">Sim List</h1>
<section>

    <div id="sim-filter" class="group">
      <h2>Filter</h2>

      <form:form modelAttribute="simListFilterForm" method="get" id="simListFilterForm">
        <div class="tableMain">

          <c:choose>
            <c:when test="${not empty simListFilterForm.selectedNetwork}">
              <div class="tableRow">
                <div class="tableCell">
                  <form:label path="selectedNetwork">Network</form:label>
                  <form:hidden path="selectedNetwork" />
                </div>
                <div class="tableCell">
                    ${simListFilterForm.selectedNetwork}
                </div>
              </div>
            </c:when>

            <c:otherwise>
              <div class="tableRow" title="SIMs will only be shown for the selected network">
                <div class="tableCell">
                  <form:label path="selectedNetwork">Network</form:label>
                </div>
                <div class="tableCell">
                  <c:if test="${not empty simListFilterForm.networkList}">
                    <form:select path="selectedNetwork" cssClass="ui-comboBox">
                      <form:options items="${simListFilterForm.networkList}" cssClass="ui-menu-item" />
                    </form:select>
                  </c:if>
                </div>
                <div class="tableCell">
                  <input type="submit" id="filterByNetwork" value="Filter" class="button"/>
                </div>
              </div>
            </c:otherwise>
          </c:choose>

          <c:choose>
            <c:when test="${not empty simListFilterForm.selectedLifecycleState}">
              <div class="tableRow">
                <div class="tableCell">
                  <form:label path="selectedLifecycleState">State</form:label>
                  <form:hidden path="selectedLifecycleState" />
                </div>
                <div class="tableCell">
                    ${simListFilterForm.selectedLifecycleState}
                </div>
              </div>
            </c:when>

            <c:otherwise>
              <div class="tableRow" title="SIMs will only be shown for the selected state">
                <div class="tableCell">
                  <form:label path="selectedLifecycleState">State</form:label>
                </div>
                <div class="tableCell">
                  <c:if test="${not empty simListFilterForm.lifeCycleStateList}">
                    <form:select path="selectedLifecycleState" cssClass="ui-comboBox">
                      <form:options items="${simListFilterForm.lifeCycleStateList}" cssClass="ui-menu-item" />
                    </form:select>
                  </c:if>
                </div>
                <div class="tableCell">
                  <input type="submit" id="filterByLifecycleState" value="Filter" class="button"/>
                </div>
              </div>
            </c:otherwise>
          </c:choose>

          <c:if test="${not empty simListFilterForm.selectedNetwork or not empty simListFilterForm.selectedLifecycleState}">
            <div class="tableRow">
              <div class="tableCell">
                <c:url var="resetFilterUrl" value="/pages/sim/simlist"/>
                <a href="${resetFilterUrl}" class="button">Clear</a>
              </div>
            </div>
          </c:if>

        </div>

      </form:form>
    </div>


  <div>
    Showing: ${simList.size()} of ${totalResults} SIMs.

    <c:set var="currentPage" value="${page + 1}"/>

    <c:if test="${totalResults > simList.size()}">
      Page Number: ${currentPage}
    </c:if>

    <c:if test="${currentPage > 1}">
      <c:url var="previousPage"
             value="/pages/sim/simlist/?page=${currentPage - 1}&selectedNetwork=${simListFilterForm.selectedNetwork}&selectedLifecycleState=${simListFilterForm.selectedLifecycleState}"/>
      <a href="${previousPage}">Prev</a>
    </c:if>

    <c:if test="${currentPage * simList.size() < totalResults}">
      <c:url var="nextPage"
             value="/pages/sim/simlist/?page=${currentPage + 1}&selectedNetwork=${simListFilterForm.selectedNetwork}&selectedLifecycleState=${simListFilterForm.selectedLifecycleState}"/>
      <a href="${nextPage}">Next</a>
    </c:if>
  </div>

  <div class="group">

    <table id="table-sim-list">

      <thead>
      <tr>
        <th>ICCID</th>
        <th>State</th>
        <th>Creation Date</th>
        <th>Network</th>
        <th>CMIU ID</th>
        <th>Action</th>
      </tr>
      </thead>

      <tbody>
      <c:forEach items="${simList}" var="sim">
        <tr>
          <td class="small-values" title="The ICCID of the SIM"><a href="${debugUrl}/network/device?mno=${sim.network}&iccid=${sim.iccid}" target="_blank">${sim.iccid}</a></td>
          <c:url var="url" value="/pages/sim/simlist/?selectedLifecycleState=${mdce:esc(sim.state)}"/>
          <td class="small-values" title="SIM lifecycle state"><a href="${url}">${mdce:esc(sim.state)}</a></td>
          <td class="small-values" title="Creation date">${mdce:esc(mdce:formatInstant(sim.creationDate))}</td>
          <c:url var="url" value="/pages/sim/simlist/?selectedNetwork=${mdce:esc(sim.network)}"/>
          <td class="small-values" title="The SIM network"><a href="${url}">${mdce:esc(sim.network)}</a></td>
          <c:choose>
            <c:when test="${sim.miuId!=null}">
              <c:url var="url" value="/pages/miu/miudetail/${sim.miuId}"/>
            </c:when>
            <c:otherwise>
              <c:url var="url" value="#"/>
            </c:otherwise>
          </c:choose>
          <td class="small-values" title="MIU"><a href="${url}">${mdce:esc(sim.miuId)}</a></td>
          <td class="small-values" title="Actions">

            <a href="${debugUrl}/network/device?mno=${sim.network}&iccid=${sim.iccid}" target="_blank" title="View SIM details for ICCID ${sim.iccid}" class="post separator" >Info</a>
            <a href="${debugUrl}/network/connection-events?mno=${sim.network}&iccid=${sim.iccid}" target="_blank" title="View SIM connection events for ICCID ${sim.iccid}" class="post separator">Connection</a>
            <a href="${debugUrl}/network/usage-history?mno=${sim.network}&iccid=${sim.iccid}" target="_blank" title="View usage history for ICCID ${sim.iccid}" class="post separator">Usage</a>
            <a href="${debugUrl}/network/provision-history?mno=${sim.network}&iccid=${sim.iccid}" target="_blank" title="View provision history for ICCID ${sim.iccid}" class="post separator">Provision</a>
            <a href="${debugUrl}/network/change-activation?mno=${sim.network}&iccid=${sim.iccid}" target="_blank" title="Change activation for ICCID ${sim.iccid}" class="post separator separator-last">Activation</a>
            <c:url var="changeStateUrl" value="/pages/sim/simlist/${sim.iccid}/changestate"/>
            <a href="${changeStateUrl}?page=${currentPage}&network=${simListFilterForm.selectedNetwork}&state=${simListFilterForm.selectedLifecycleState}" title="Change the lifecycle state for ICCID ${sim.iccid}" class="post separator separator-last">Change State</a>

          </td>
        </tr>
      </c:forEach>

      </tbody>

    </table>

  </div>
</section>


</body>
</html>

