<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <%--Add CSRF token for used in postLink --%>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="lib/jquery-ui/jquery-ui.structure.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.theme.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery.ui.combobox.css" />
    <mdce:css href="lib/jquery-ui/jquery-ui.custom.css"/>

    <mdce:css href="css/glyphicons.css"/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/simdetails.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery-ui.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery.ui.combobox.js" />

    <script>
        $(function() {
            $(".ui-comboBox").combobox();
        });
    </script>

    <title>${mdce:t("Title.ChangeSimState")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

</head>
<body>

<mdce:header/>
<h1 class="page-heading">SIM ${changeSimStateForm.iccid}</h1>
<section>

    <div id="sim-filter" class="group">
        <h2>Change SIM State</h2>

        <form:form modelAttribute="changeSimStateForm" method="post" id="simListFilterForm" action="${postChangeSimState}">
            <div class="tableMain">
                <div class="tableRow">
                    <div class="tableCell">
                        <form:label path="selectedSimState">State</form:label>
                        <form:select path="selectedSimState" cssClass="ui-comboBox">
                            <form:options items="${simStateList}" itemLabel="stringValue" cssClass="ui-menu-item" />
                        </form:select>
                    </div>
                </div>
                <div class="tableRow">
                    <div class="tableCell">
                        <input type="submit" class="button" id="save-sim-status" name="saveUserDetails"
                               value="Save Changes"/>
                        <input type="button" class="button" id="cancel-changes" value="Cancel"/>
                    </div>
                </div>
            </div>

        </form:form>
    </div>

</section>

<c:url var="simListUrl" value="${changeSimStateForm.returnUrl}"/>

<script type="text/javascript">
    $("#cancel-changes").click(function() {window.location.href = "${simListUrl}"});
</script>


</body>
</html>
