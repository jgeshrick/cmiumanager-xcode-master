<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <%--Add CSRF token for used in postLink --%>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="css/glyphicons.css"/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/gatewaydetails.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>

    <title>${mdce:t("Title.Gateways")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

</head>
<body>

<mdce:header/>
<h1 class="page-heading" id="gateway-list-title">Gateway List</h1>
<section>
    <div>

        <div id="gateway-filter" class="group">
            <h2>Filter</h2>
            <form:form modelAttribute="gatewayListFilterForm" method="get">
                <c:choose>
                    <c:when test="${not empty gatewayListFilterForm.selectedManager}">
                        <p>Regional Manager: ${gatewayListFilterForm.selectedManager}</p>
                        <form:hidden path="selectedManager" />
                    </c:when>
                    <c:when test="${not empty gatewayListFilterForm.selectedLocation}">
                        <p>State: ${gatewayListFilterForm.selectedLocation}</p>
                        <form:hidden path="selectedLocation" />
                    </c:when>
                    <c:when test="${empty gatewayListFilterForm.selectedSite}">
                        <%-- None of the filter is applied--%>
                        <div class="field" title="Gateways will only be shown for the selected Manager">
                            <form:label path="selectedManager">Regional Manager</form:label>
                            <form:select path="selectedManager" items="${gatewayListFilterForm.managers}"/>
                            <input type="submit" id="filterByManager" value="Filter" class="button"/>
                        </div>
                        <div class="field" title="Gateways will only be shown for the selected location">
                            <form:label path="selectedLocation">State</form:label>
                            <form:select path="selectedLocation" items="${gatewayListFilterForm.locations}"/>

                            <input type="submit" id="filterByLocation" value="Filter" class="button"/>
                        </div>
                    </c:when>

                </c:choose>
                <c:choose>
                    <c:when test="${not empty gatewayListFilterForm.selectedSite}">
                        <p>Site: ${gatewayListFilterForm.selectedSiteName} (site ID:${gatewayListFilterForm.selectedSite})</p>
                        <form:hidden path="selectedSite" />
                        <form:hidden path="selectedSiteName" />
                    </c:when>
                    <c:when test="${not empty gatewayListFilterForm.selectedManager or not empty gatewayListFilterForm.selectedLocation}">
                        <c:if test="${not empty gatewayListFilterForm.siteIds}">
                            <div class="field">
                                <form:label path="selectedSite">Site ID</form:label>
                                <form:select path="selectedSite">
                                    <form:option value="" label="--- Filter by site id ---"/>
                                    <c:forEach items="${gatewayListFilterForm.siteIds}" var="site">
                                        <form:option
                                                value="${site.siteId}"
                                                label="${site.siteName} (site ID:${site.siteId})"/>
                                    </c:forEach>
                                </form:select>
                                <input type="submit" id="filterBySite" name="filterBySite" value="Filter" class="button"/>
                            </div>
                        </c:if>
                    </c:when> <%-- otherwise: don't show full site list, user must select regional manager or state first --%>
                </c:choose>
                <c:if test="${not empty gatewayListFilterForm.selectedManager or not empty gatewayListFilterForm.selectedLocation or not empty gatewayListFilterForm.selectedSite}">
                <div>
                    <c:url var="resetFilterUrl" value="/pages/gateway/gatewaylist"/>
                    <a href="${resetFilterUrl}" class="button">Clear</a>
                </div>
                </c:if>

            </form:form>
        </div>

    </div>

    <div>
        Showing: ${gatewayList.size()} of ${totalResults} MIUs.

        <c:set var="currentPage" value="${page + 1}"/>

        <c:if test="${totalResults > 100}">
            Page Number: ${currentPage}
        </c:if>

        <c:if test="${currentPage > 1}">
            <c:url var="previousPage"
                   value="/pages/gateway/gatewaylist/?page=${currentPage - 1}&selectedSite=${gatewayListFilterForm.selectedSite}"/>
            <a href="${previousPage}">Prev</a>
        </c:if>

        <c:if test="${gatewayList.size() >= 100}">
            <c:url var="nextPage"
                   value="/pages/gateway/gatewaylist/?page=${currentPage + 1}&selectedSite=${gatewayListFilterForm.selectedSite}"/>
            <a href="${nextPage}">Next</a>
        </c:if>
    </div>

    <div class="group">

        <table id="table-gateway-list">
            <thead>
            <tr>
                <th>Gateway ID</th>
                <th>Site</th>
                <th>Last heard time</th>
                <th>Active</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${gatewayList}" var="gateway">
                <tr>
                    <td class="small-values" title="Show the details page for this Gateway">
                        <mdce:gatewayLink gatewayId="${gateway.gatewayId}" />
                    </td>
                    <c:url var="url" value="/pages/gateway/gatewaylist?selectedSite=${gateway.siteId}"/>
                    <td title="Show the details page for this Site">
                        <a href="${url}">${gateway.siteDescription} (${gateway.siteId})</a>
                    </td>
                    <td class="small-values" title="The last time the Gateway connected to Galileo">
                        ${gateway.lastHeardTime}
                    </td>
                    <td class="small-values" title="Is the gateway active">
                        ${gateway.gateway_active}
                    </td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
    </div>
</section>


</body>
</html>

