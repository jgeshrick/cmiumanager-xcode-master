<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <%--Add CSRF token for used in postLink --%>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="css/glyphicons.css"/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/gatewaydetails.css"/>
    <mdce:css href="css/audit.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.structure.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.theme.min.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery-ui.min.js"/>
    <mdce:js src="js/gatewaydetail.js"/>

    <title>${mdce:t("Title.GatewayDetails")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

</head>
<body>

<mdce:header/>
<h1 class="page-heading">Gateway Details</h1>
<section>
    <div>
        <c:url var="url" value="/pages/gateway/gatewaylist"/>
        <h2><a href="${url}">Gateway List</a> > Gateway Details : ${auditLogFilterForm.gatewayId}</h2>

        <c:set var="gateway" value="${gatewayDetails}" />
        <div class="group">
            <h2>Basic Gateway Information</h2>
            <div class="group">
                <div id="gateway-details">
                    <dl>
                        <dt title="The ID of the gateway">Gateway ID:</dt>
                        <dd>${gateway.gatewayId}</dd>
                    </dl>
                    <dl>
                        <dt title="The ID of the site that owns the gateway">Site ID:</dt>
                        <dd>${gateway.siteDescription} (${gateway.siteId})</dd>
                    </dl>
                    <dl>
                        <dt title="The first time Galileo heard the gateway">First Heard Time:</dt>
                        <dd>${gateway.creationDate}</dd>
                    </dl>
                    <dl>
                        <dt title="The last time Galileo heard the gateway">Last Heard Time:</dt>
                        <dd>${gateway.lastHeardTime}</dd>
                    </dl>
                    <dl>
                        <dt title="Is the gateway billable?">Gateway Active:</dt>
                        <dd>${gateway.gateway_active}</dd>
                    </dl>
                </div>
            </div>
            <h2>Last File Received</h2>
            <div class="group">
                <div id="gateway-details">
                    <dl>
                        <dt title="The time this file was received">Heard Time:</dt>
                        <dd>${gateway.gatewayHeardPackets.heardTime}</dd>
                    </dl>
                    <dl>
                        <dt title="The number of MIUs included in the file">Number of Mius:</dt>
                        <dd>${gateway.gatewayHeardPackets.miusHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The number of OOK packets in the file">OOK Packets Heard:</dt>
                        <dd>${gateway.gatewayHeardPackets.ookPacketsHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The number of FSK packets in the file">FSK Packets Heard:</dt>
                        <dd>${gateway.gatewayHeardPackets.fskPacketsHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The number of config packets in the file">Config Packets Heard:</dt>
                        <dd>${gateway.gatewayHeardPackets.configPacketsHeard}</dd>
                    </dl>
                </div>
            </div>
            <h2>Statistics for the Last Month</h2>
            <div class="group">
                <div id="gateway-details">
                    <dl>
                        <dt title="The number of times the gateway was expected to call into Galileo">Expected Call Ins:</dt>
                        <dd>${gateway.gatewayMonthlyStats.numExpected}</dd>
                    </dl>
                    <dl>
                        <dt title="The number of times the gateway called into Galileo in the month">Total Call Ins:</dt>
                        <dd>${gateway.gatewayMonthlyStats.numHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The average number of MIUs heard by the gateway per call in">Avg Mius Heard:</dt>
                        <dd>${gateway.gatewayMonthlyStats.avgMiusHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The minimum number of MIUs heard by the gateway per call in">Min Mius Heard:</dt>
                        <dd>${gateway.gatewayMonthlyStats.minMiusHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The maximum number of MIUs heard by the gateway per call in">Max Mius Heard:</dt>
                        <dd>${gateway.gatewayMonthlyStats.maxMiusHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The average number of OOK packets heard by the gateway per call in">Avg OOK Packets Heard:</dt>
                        <dd>${gateway.gatewayMonthlyStats.avgOokPacketsHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The minimum number of OOK packets heard by the gateway per call in">Min OOK Packets Heard:</dt>
                        <dd>${gateway.gatewayMonthlyStats.minOokPacketsHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The maximum number of OOK packets heard by the gateway per call in">Max OOK Packets Heard:</dt>
                        <dd>${gateway.gatewayMonthlyStats.maxOokPacketsHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The average number of FSK packets heard by the gateway per call in">Avg FSK Packets Heard:</dt>
                        <dd>${gateway.gatewayMonthlyStats.avgFskPacketsHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The minimum number of FSK packets heard by the gateway per call in">Min FSK Packets Heard:</dt>
                        <dd>${gateway.gatewayMonthlyStats.minFskPacketsHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The maximum number of FSK packets heard by the gateway per call in">Max FSK Packets Heard:</dt>
                        <dd>${gateway.gatewayMonthlyStats.maxFskPacketsHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The average number of config packets heard by the gateway per call in">Avg Config Packets Heard:</dt>
                        <dd>${gateway.gatewayMonthlyStats.avgConfigPacketsHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The minimum number of config packets heard by the gateway per call in">Min Config Packets Heard:</dt>
                        <dd>${gateway.gatewayMonthlyStats.minConfigPacketsHeard}</dd>
                    </dl>
                    <dl>
                        <dt title="The maximum number of config packets heard by the gateway per call in">Max Config Packets Heard:</dt>
                        <dd>${gateway.gatewayMonthlyStats.maxConfigPacketsHeard}</dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</section>


</body>
</html>

