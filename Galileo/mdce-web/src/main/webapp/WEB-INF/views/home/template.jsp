<%--
  ~  Neptune Technology Group
  ~  Copyright 2015 as unpublished work.
  ~  All rights reserve
  ~
  ~  The information contained herein is confidential
  ~  property of Neptune Technology Group. The use, copying, transfer
  ~  or disclosure of such information is prohibited except by express
  ~  written agreement with Neptune Technology Group.
  ~
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
    <mdce:css href="css/template.css"/>
    <mdce:favicon/>
    <title>${mdce:t("Title.Home")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>
<mdce:header />
<h1 class="page-heading">${mdce:t("Title.Application")}</h1>
<h2>${mdce:t("Title.Home")}</h2>

<section>
    <h1>Heading 1 - Grid layout</h1>

    <div class="span-three">Element 3/12</div>
    <div class="span-six">Element 6/12</div>
    <div class="span-three">Element 3/12</div>

    <div class="span-three">Element 3/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>
    <div class="span-three">Element 3/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>

</section>

<section>
    <div class="span-six">Element 6/12 </div>

    <div class="span-six">
        <div class="span-three green">Inner Element 3/12 </div>
        <div class="span-three green">Inner Element 3/12 </div>
        <div class="span-three green">Inner Element 3/12 </div>
        <div class="span-three green">Inner Element 3/12 </div>
    </div>
</section>

<section>
    <div class="banner">Banner</div>
</section>

<section>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>
    <div class="span-one">1/12</div>

    <div class="push-one-span-one">span 1 push 1 </div>
    <div class="push-two-span-two">span 2 push 2 </div>
    <div class="push-three-span-three">span 3 push 3 </div>
</section>


</body>
</html>
