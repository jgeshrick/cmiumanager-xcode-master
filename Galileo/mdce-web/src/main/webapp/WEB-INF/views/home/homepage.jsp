<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/glyphicons.css"/>

    <style type="text/css">

        <c:url var="logoUrl" value="/resources/images/logo_with_text_297x70.png" />
        h1.homepage-title {
            background-image: url(${logoUrl});
            background-repeat: no-repeat;
            padding-top: 76px;
            margin-bottom: 2em;
        }

    </style>

    <mdce:favicon/>
    <title>${mdce:t("Title.Home")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>
<mdce:header/>

<section class="homepage-navigation">

    <h1 class="homepage-title" id="homepage-title">${mdce:t('Title.ApplicationTitleLong')}</h1>

    <h2>${mdce:t('Title.Alerts')}</h2>

    <c:url var="url" value="/pages/alert/list"/>
    <p title="Show alerts for MIUs and Galileo"><a href="${url}">${mdce:t('Title.Alerts')}</a></p>

    <h2>${mdce:t('Title.Endpoints')}</h2>

    <c:url var="url" value="/pages/miu/miulist"/>
    <p title="Show a list of all MIUs"><a href="${url}">${mdce:t('Title.Mius')}</a></p>

    <c:url var="url" value="/pages/sim/simlist"/>
    <p title="Show a list of all SIMs"><a href="${url}">${mdce:t('Title.Sims')}</a></p>

    <c:url var="url" value="/pages/config"/>
    <p title="View and send commands to CMIUs"><a href="${url}">${mdce:t('Title.C2')}</a></p>

    <c:url var="url" value="/pages/analysis/timing"/>
    <p title="Search CMIU Connecting Times"><a href="${url}">C-TAT</a></p>

    <h2>${mdce:t('Title.Collectors')}</h2>

    <c:url var="url" value="/pages/gateway/gatewaylist"/>
    <p title="Show a list of all Gateways"><a href="${url}">${mdce:t('Title.Gateways')}</a></p>

    <h2>${mdce:t('Title.Admin')}</h2>

    <c:url var="url" value="/pages/user/list"/>
    <p title="Manage Users"><a href="${url}">${mdce:t('Title.UserManagement')}</a></p>

    <c:url var="url" value="/pages/audit/"/>
    <p title="View the log of actions performed in Galileo"><a href="${url}">${mdce:t('Title.AuditLog')}</a></p>

    <c:url var="url" value="/pages/settings/"/>
    <p title="Change settings for Galileo"><a href="${url}">${mdce:t('Title.Settings')}</a></p>

    <c:url var="url" value="/pages/user/logout"/>
    <p title="Log Out"><a href="${url}">${mdce:t('Title.LogOut')}</a></p>

</section>

<section class="homepage-status">

    <h2>Application information</h2>
    <h3>Server time</h3>
    ${mdce:formatDateTime(zonedatetime)}

    <p>Application version: ${mdce:applicationVersion()}</p>

    <p>Environment: ${environmentName}</p>

    <p>Server: ${serverName}</p>

    <h3>${mdce:t("Title.MsocMode")}</h3>

    <p>${msocModeMessage}</p>

    <security:authorize access="isAuthenticated()">
        Currently logged in as user: <security:authentication property="principal"/>. <br/>
        Role (display string): <security:authentication property="userRoleDisplayString"/> <br/>
        Granted authorities: <security:authentication property="authorities"/> <br/>
        <c:url var="url" value="/pages/user/changePassword/"/>
        <a href="${url}">${mdce:t('Title.ChangePassword')}</a>
    </security:authorize>

    <security:authorize access="! isAuthenticated()">
        Not logged in. <br/>
        Granted authorities: <security:authentication property="authorities"/>
    </security:authorize>


    <h3>${mdce:t("Title.ConnectedBackEnds")}</h3>

    <c:set var="statuses" value="${mdce:getBackEndStatus()}"/>


    <table id="back-ends">
        <thead>
        <tr>
            <th>Name</th>
            <th>URL</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="server" items="${statuses.serverStatuses}">
            <c:choose>
                <c:when test="${server.status.error}">
                    <c:set var="spanClass" value="error"/>
                    <c:set var="message" value="${server.status.errorMessage}"/>
                    <c:set var="glyph" value="X"/>
                </c:when>
                <c:when test="${server.status.warning}">
                    <c:set var="spanClass" value="warning"/>
                    <c:set var="message" value="${server.status.errorMessage}"/>
                    <c:set var="glyph" value="!"/>
                </c:when>
                <c:otherwise>
                    <c:set var="spanClass" value="ok"/>
                    <c:set var="message" value="okay"/>
                    <c:set var="glyph" value="&#x2713;"/>
                </c:otherwise>
            </c:choose>

            <tr>
                <th>${server.serverDetails.name}</th>
                <td>${server.serverDetails.url}</td>
                <td class="back-end-status"><span class="${spanClass}">(${glyph})</span> ${message}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <h3>${mdce:t("Title.OtherSites")}</h3>
    <p><a href="http://116-bpc-vm7:54202/BPCWebService/Find" target="_blank">Find my MIU</a></p>

</section>

<section class="homepage-temp">
    <h2>Old development pages - to be removed</h2>

    <c:url var="url" value="/pages/dashboard"/>
    <p><a href="${url}">View CMIU Dashboard</a></p>

    <c:url var="url" value="/pages/config/test-image-update"/>
    <p><a href="${url}">Test Image Update (temporary)</a></p>

</section>


</body>
</html>
