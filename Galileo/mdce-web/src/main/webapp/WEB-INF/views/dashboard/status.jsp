<%--
  ~  Neptune Technology Group
  ~  Copyright 2015 as unpublished work.
  ~
  ~  All rights reserved
  ~  The information contained herein is confidential
  ~  property of Neptune Technology Group. The use, copying, transfer
  ~  or disclosure of such information is prohibited except by express
  ~  written agreement with Neptune Technology Group.
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>
    <mdce:css href="lib/jquery-ui/jquery-ui.min.css"/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/glyphicons.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery-ui.min.js"/>
    <mdce:js src="lib/amcharts/amcharts.js"/>
    <mdce:js src="lib/amcharts/pie.js"/>
    <mdce:js src="js/charts.js"/>

    <title>${mdce:t("Title.Dashboard")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>


</head>
<body>

<mdce:header/>

<div>
    <mdce:status account="ALL"/>
</div>

</body>
</html>

