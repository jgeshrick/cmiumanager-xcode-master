<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <mdce:favicon/>

    <mdce:css href="lib/jquery-ui/jquery-ui.min.css"/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/glyphicons.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/amcharts/amcharts.js"/>
    <mdce:js src="lib/amcharts/pie.js"/>
    <mdce:js src="lib/jquery-ui/jquery-ui.min.js"/>
    <mdce:js src="js/charts.js"/>

    <title>${mdce:t("Title.Dashboard")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>
<div class="content">
    <mdce:header/>

    <div class="quadrants">
        <c:url var="urlHelp" value="/pages/help"/>
        <c:url var="urlStatus" value="/pages/status"/>
        <c:url var="urlAlerts" value="/pages/alerts"/>
        <c:url var="urlMiuDetails" value="/pages/miu-details"/>

        <section class="tl">
            <div class="control-box">
                <a href="${urlHelp}"><span class="glyphicon glyphicon-question-sign"></span></a>
                <a href="${urlStatus}"><span class="glyphicon glyphicon-fullscreen"></span></a>
            </div>

            <h2>Status</h2>

            <mdce:status account="ALL"/>

        </section>

        <section class="tr">
            <div class="control-box">
                <a href="${urlHelp}"><span class="glyphicon glyphicon-question-sign"></span></a>
                <a href="${urlAlerts}"><span class="glyphicon glyphicon-fullscreen"></span></a>
            </div>

            <h2>Alert / Exceptions</h2>
            <div class="alerts">
                <mdce:alerts account="ALL"/>
            </div>
        </section>

    </div>
    <div class="quadrants">

        <section class="bl">
            <div class="control-box">
                <a href="${urlHelp}"><span class="glyphicon glyphicon-question-sign"></span></a>
                <a href="${urlMiuDetails}"><span class="glyphicon glyphicon-fullscreen"></span></a>
            </div>

            <h2>CMIU Details</h2>


            <div class="cmiu-details">
                <mdce:cmiuDetailsTable account="ALL"/>
            </div>

        </section>

        <section class="br">
            <div class="control-box">
                <a href="${urlHelp}"><span class="glyphicon glyphicon-question-sign"></span></a>
                <span class="glyphicon glyphicon-fullscreen"></span>
            </div>

            <h2>Filter</h2>

        </section>
    </div>


</div>
</body>
</html>
