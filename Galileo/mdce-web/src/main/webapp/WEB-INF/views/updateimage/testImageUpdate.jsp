<%--
  ~  Neptune Technology Group
  ~  Copyright 2015 as unpublished work.
  ~  All rights reserved.
  ~
  ~  The information contained herein is confidential
  ~  property of Neptune Technology Group. The use, copying, transfer
  ~  or disclosure of such information is prohibited except by express
  ~  written agreement with Neptune Technology Group.
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="lib/jquery-ui/jquery-ui.min.css"/>
    <mdce:css href="css/c2.css"/>
    <mdce:css href="css/glyphicons.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>

    <title>${mdce:t("Title.C2")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>


</head>
<body>

<mdce:header/>
<h1 class="page-heading">Update Image Test Page</h1>
<section class="container">
    <div>
        <form id="testImageUpdateForm" method="post">
            <div>
                <label for="miuId">CMIU Id: </label>
                <input type="text" name="miuId" id="miuId" placeholder="400000102" >
            </div>

            <div>
                <label for="imageType">Image Type</label>
                <select name="imageType" id="imageType" >
                    <option value="FirmwareImage" > Firmware image </option>
                    <option value="BootloaderImage"> Bootloader image </option>
                    <option value="ArbImage"> ARB imge </option>
                    <option value="ConfigImage" > Config image </option>
                    <option value="BleConfigImage"> BLE config image </option>
                    <option value="EncryptionImage"> Encryption image </option>
                </select>
            </div>

            <div>
                <label for="fileUrl">Image file full url: </label>
                <input type="text" name="fileUrl" id="fileUrl" value="http://10.120.0.174/NeptuneProto2.bin" placeholder="http://full_url_to_image_file">
            </div>

            <div>
                <label for="startAddress">Start Address (hex)</label>
                <span class="hex-prefix">0x</span>
                <input type="text" name="startAddress" id="startAddress" value="1234" placeholder="1234 (hex without 0x)">
            </div>

            <div>
                <label for="sizeByte">Size byte (hex)</label>
                <span class="hex-prefix">0x</span>
                <input type="text" name="sizeByte" id="sizeByte" value="1234" placeholder="size in byte (hex without 0x)">
            </div>

            <div>
                <label for="revMajor">Revision </label>
                <input type="text" name="revMajor" id="revMajor" value="0" >
                <input type="text" name="revMinor" id="revMinor" value="0">
                <input type="text" name="revDate" id="revDate" value="0">
                <input type="text" name="revBuild" id="revBuild" value="0">
            </div>

            <input type="submit" class="button" value="Update image" />
        </form>
    </div>
</section>

</body>
</html>

