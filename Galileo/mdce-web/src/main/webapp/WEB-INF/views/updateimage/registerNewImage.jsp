<%--
  ~  Neptune Technology Group
  ~  Copyright 2015 as unpublished work.
  ~  All rights reserved.
  ~
  ~  The information contained herein is confidential
  ~  property of Neptune Technology Group. The use, copying, transfer
  ~  or disclosure of such information is prohibited except by express
  ~  written agreement with Neptune Technology Group.
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="lib/jquery-ui/jquery-ui.min.css"/>
    <mdce:css href="css/c2.css"/>
    <mdce:css href="css/glyphicons.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery.dynamiclist.js"/>
    <mdce:js src="js/imageUpdate.js"/>

    <title>${mdce:t("Title.C2")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>


</head>
<body>

<mdce:header/>
<h1 class="page-heading">Register new image </h1>
<section class="container">
    <div class="group">
        <form:form modelAttribute="registerNewImageForm" method="post">

            <h2>Upload new image to firmware server</h2>

            <div class="field">
                <form:label path="imageType">Image type </form:label>
                <%--<form:select path="imageType" items="${imageTypeList}" itemValue="displayName" />--%>

                <form:select path="imageType" >
                    <form:option value="0" label="--- Select image type ---" />
                    <c:forEach items="${imageTypeList}" var="imageType">
                        <form:option value="${imageType.name()}" label="${imageType.displayName}" />
                    </c:forEach>

                </form:select>
            </div>

            <div class="field">
                <form:label path="selectedImageSource">Artifact to import</form:label>
                <form:select path="selectedImageSource">
                    <form:option value="0" label="--- Select artifact version ---" />

                    <c:forEach items="${artifactList}" var="entry">

                        <%--<form:options items="${entry.value}" data-image-type="${entry.key}"/>--%>
                        <c:forEach items="${entry.value}" var="imageFullPath">
                            <c:set var="pathparts" value="${fn:split(imageFullPath, '/')}" />
                            <c:set var="filename" value="${pathparts[fn:length(pathparts) - 1]}" />

                            <form:option value="${imageFullPath}" label="${filename}" data-image-type="${entry.key}" />
                        </c:forEach>


                    </c:forEach>

                </form:select>
            </div>

            <div class="submits">
                <input type="submit" name="registerImage" value="Register image" class="button"/>
            </div>

        </form:form>
    </div>


</section>

</body>
</html>

