<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>
    <mdce:css href="css/c2.css"/>
    <mdce:css href="css/glyphicons.css"/>
    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery.dynamiclist.js"/>
    <mdce:js src="js/dynamicMiuList.js"/>

    <title>${mdce:t("Title.C2")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>

<mdce:header/>
<h1 class="page-heading">Update Image </h1>
<section class="container">
    <form:form modelAttribute="imageUpdateForm" method="post">
        <div>
            <form:errors path="miuInputList" class="error" element="div" />
            <form:errors path="imageSelected" class="error" element="div" />
            <%--<form:errors path="*" class="error" element="div" />--%>
        </div>
        <div class="group">
            <h2>Image Selection</h2>

            <div class="field">
                <input type="submit" class="button right" id="register-new-image" name="registerNewImage" value="Register New Image" />
            </div>

            <div class="field" title="Choose to update the CMIU firmware">
                <form:checkbox path="updateFirmware" cssClass="checkbox-custom"/>
                <form:label path="updateFirmware" cssClass="checkbox-custom-label">Update Firmware</form:label>
                <form:select path="firmwareImageVersion" title="Indicate the firmware version the CMIU/s should upgrade to">
                    <form:option value="-" label="Select a version" />
                    <c:forEach items="${imageVersions['FirmwareImage']}" var="element">
                        <form:option value="${element.fileName}"
                                     label="${element.fileName}"
                                     data-start-address="${element.startAddress}"
                                     data-size="${element.sizeBytes}"
                                     data-image-filename="${element.fileName}" />
                    </c:forEach>
                </form:select>
                <form:errors path="updateFirmware" cssClass="error-text"/>
            </div>
            <div class="field" title="Choose to update the CMIU config">
                <form:checkbox path="updateConfig"/>
                <form:label path="updateConfig">Update Config</form:label>
                <form:select path="configImageVersion" title="Indicate the config version the CMIU/s should upgrade to">
                    <form:option value="-" label="Select a version" />
                    <c:forEach items="${imageVersions['ConfigImage']}" var="element">
                        <form:option value="${element.fileName}"
                                     label="${element.fileName}"
                                     data-start-address="${element.startAddress}"
                                     data-size="${element.sizeBytes}"
                                     data-image-filename="${element.fileName}" />
                    </c:forEach>
                </form:select>
                <form:errors path="updateConfig" cssClass="error-text"/>
            </div>
            <div class="field" title="Choose to update the CMIU ARB config">
                <form:checkbox path="updateARBConfig"/>
                <form:label path="updateARBConfig">Update ARB</form:label>
                <form:select path="ArbImageVersion" title="Indicate the ARB config version the CMIU/s should upgrade to">
                    <form:option value="-" label="Select a version" />
                    <c:forEach items="${imageVersions['ArbConfigImage']}" var="element">
                        <form:option value="${element.fileName}"
                                     label="${element.fileName}"
                                     data-start-address="${element.startAddress}"
                                     data-size="${element.sizeBytes}"
                                     data-image-filename="${element.fileName}" />
                    </c:forEach>
                </form:select>
                <form:errors path="updateARBConfig" cssClass="error-text"/>
            </div>
            <div class="field" title="Choose to update the CMIU BLE config">
                <form:checkbox path="updateBleConfig"/>
                <form:label path="updateBleConfig">Update BLE Configuration</form:label>
                <form:select path="bleConfigImageVersion" title="Indicate the BLE config version the CMIU/s should upgrade to">
                    <form:option value="-" label="Select a version" />
                    <c:forEach items="${imageVersions['BleConfigImage']}" var="element">
                        <form:option value="${element.fileName}"
                                     label="${element.fileName}"
                                     data-start-address="${element.startAddress}"
                                     data-size="${element.sizeBytes}"
                                     data-image-filename="${element.fileName}" />
                    </c:forEach>
                </form:select>
                <form:errors path="updateBleConfig" cssClass="error-text"/>
            </div>
            <div class="field" title="Choose to update the CMIU encryption config">
                <form:checkbox path="updateEncryption"/>
                <form:label path="updateEncryption">Update Encryption</form:label>
                <form:select path="encryptionImageVersion" title="Indicate the encryption config version the CMIU/s should upgrade to">
                    <form:option value="-" label="Select a version" />
                    <c:forEach items="${imageVersions['EncryptionImage']}" var="element">
                        <form:option value="${element.fileName}"
                                     label="${element.fileName}"
                                     data-start-address="${element.startAddress}"
                                     data-size="${element.sizeBytes}"
                                     data-image-filename="${element.fileName}" />
                    </c:forEach>
                </form:select>
                <form:errors path="updateEncryption" cssClass="error-text"/>
            </div>
        </div>

        <mdce:dynamicCmiuEntry miuInputList="${imageUpdateForm.miuInputList}" />

        <div class="field">
            <input type="submit" id="update-cmiu-image" name="updateCmiuImage" value="Update CMIU image" class="button right"/>
        </div>


    </form:form>
</section>

</body>
</html>

