<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <mdce:css href="css/main.css"/>

    <mdce:favicon/>
    <title>${mdce:t("Title.Settings")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>
<mdce:header/>

<h1 class="page-heading" id="mdce-settings-view">MDCE Settings</h1>
<section>

    <div class="group">
        <h2>DynamoDB data retention</h2>

        <p>Retain packet data tables in DynamoDB for
            <strong title="The number of months before DynamoDB packet date tables should be archived">
            ${settingsForm.minAgeToDeleteMonths}
        </strong>  months</p>
    </div>

    <div class="group">
        <h2>User Management</h2>

        <p>
            Disable user accounts after <strong>${settingsForm.maxLoginAttempts}</strong> consecutive failed login
            attempts.
        </p>

        <p>
            User accounts are disabled for <strong>${settingsForm.lockedAccountTimeoutMinutes}</strong> minutes when
            the maximum number of failed login attempts is reached.
        </p>
    </div>

    <c:url var="url" value="/pages/settings/modify" />

    <p><a href="${url}">Modify settings</a></p>

    <c:url var="url" value="/pages/home" />

    <p><a href="${url}">Done</a></p>

</section>


</body>
</html>
