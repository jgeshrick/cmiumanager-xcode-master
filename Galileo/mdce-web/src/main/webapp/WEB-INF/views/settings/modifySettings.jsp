<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <mdce:css href="css/main.css"/>

    <mdce:favicon/>
    <title>${mdce:t("Title.Settings")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>
<mdce:header/>

<h1 class="page-heading" id="mdce-settings-modify">MDCE Settings</h1>
<section>

    <form:form modelAttribute="settingsForm" method="post">

        <div class="group">
            <h2>DynamoDB data retention</h2>

            <p>
                Retain packet data tables in DynamoDB for <form:input path="minAgeToDeleteMonths" cssClass="inline" />
                months.
                <form:errors path="minAgeToDeleteMonths" class="error-text"></form:errors>
            </p>
        </div>

        <div class="group">
            <h2>User Management</h2>

            <p>
                Disable user accounts after <form:input path="maxLoginAttempts" cssClass="inline" /> consecutive
                failed login attempts.
                <form:errors path="maxLoginAttempts" class="error-text"></form:errors>
            </p>

            <p>
                User accounts are disabled for <form:input path="lockedAccountTimeoutMinutes" cssClass="inline" />
                minutes when the maximum number of failed login attempts is reached.
                <form:errors path="lockedAccountTimeoutMinutes" class="error-text"></form:errors>
            </p>
        </div>

        <div>
            <input type="submit" value="Save" class="button"/>
            <c:url var="url" value="/pages/settings" />

            <p><a href="${url}" class="button">Cancel</a></p>
        </div>

        <div class="group top-margin">
            <h2>Manage MNO callbacks</h2>
            <p>
                Must be "Master of Cellular" user, and on a whitelisted IP to use these links
            </p>
            <c:url var="url" value="/pages/settings/callback/VZW/register" />
            <li><a href="${url}" class="post">Register verizon callback</a></li>
            <c:url var="url" value="/pages/settings/callback/VZW/unregister" />
            <li><a href="${url}" class="post">Unregister verizon callback</a></li>
        </div>

    </form:form>

</section>


</body>
</html>
