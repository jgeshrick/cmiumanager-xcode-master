<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>

    <%--Add CSRF token for used in postLink --%>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="css/main.css"/>
    <mdce:css href="css/alert.css"/>
    <mdce:css href="css/glyphicons.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery-ui.min.js"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.structure.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.theme.min.css"/>
    <%--<mdce:js src="lib/jquery.dynamiclist.js"/>--%>
    <mdce:js src="js/alert.js"/>

    <%--<meta http-equiv="refresh" content="15">--%>

    <title>${mdce:t("Title.Alerts")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

</head>
<body>

<mdce:header/>

<c:set var="filterString" value="${filterStringForm.filterString}"/>

<h1 class="page-heading" id="alert-list-title">
    Alerts <c:if test="${not empty filterString}">(filtered by: ${filterString})</c:if>
</h1>

<c:url var="alertList" value="/pages/alert/list"/>
<c:url var="newAlertBatch" value="/pages/alert/newbatchaction"/>
<c:url var="handledAlertBatch" value="/pages/alert/handledbatchaction"/>
<c:url var="staleAlertBatch" value="/pages/alert/stalebatchaction"/>

<section class="container">
    <form:form modelAttribute="filterStringForm" method="get" action="${alertList}">
        <div title="Matches the start of the alert source">
            <form:label path="filterString">Filter Alerts by Alert Source:</form:label>
            <form:input path="filterString" type="text" placeholder="filter alert source" autofocus="autofocus" onFocus="this.select()"/>

            <input type="submit" id="filter-alert" name="filterAlertString" value="Filter Alerts" class="button"/>

            <c:if test="${not empty filterStringForm.filterString}">
                <c:url var="alertUrl" value="/pages/alert/list"/>
                <a href="${alertUrl}" class="button">Clear Filter</a>
            </c:if>
        </div>
    </form:form>
</section>

<section class="container">
    <mdce:alertList alertsForm="${newAlertForm}"
                    formModelAttributeName="newAlertForm"
                    name="New Alerts"
                    selectAllButton="Select All"
                    changeTicketButtonText = "Add Ticket to Selected Alerts"
                    clearAlertButtonText = "Move Selected Alerts to Cleared"
                    postUrl = "${newAlertBatch}"
                    filterString = "${filterString}"
                    isSelectable="true"
            />
</section>

<section class="container">
    <mdce:alertList alertsForm="${handledAlertForm}"
                    formModelAttributeName="handledAlertForm"
                    name="Handled Alerts"
                    selectAllButton="Select All"
                    changeTicketButtonText = "Change ticket ID on Selected Alerts"
                    clearAlertButtonText = "Move Selected Alerts to Cleared"
                    postUrl = "${handledAlertBatch}"
                    filterString = "${filterString}"
                    isSelectable="true"
            />
</section>


<section class="container">
    <mdce:alertList alertsForm="${staleAlertForm}"
                    formModelAttributeName="staleAlertForm"
                    name="Stale Alerts"
                    selectAllButton="Select All"
                    changeTicketButtonText = "Change ticket ID on Selected Alerts"
                    clearAlertButtonText = "Move Selected Alerts to Cleared"
                    postUrl = "${staleAlertBatch}"
                    filterString = "${filterString}"
                    isSelectable="true"
            />
</section>


<section class="container">
    <mdce:alertList alertsForm="${clearedAlertForm}"
                    formModelAttributeName="clearedAlertForm"
                    name="Cleared Alerts"
                    postUrl = ""
                    filterString = "${filterString}"
                    isSelectable="false"
            />
</section>


<%--Display handled alerts--%>


<%--Dialog box--%>
<section id="dialog" title="Enter Ticket ID" class="container">
    <p id="dialog-message"></p>

    <div id="dialog-input">
        <label for="dialog-input-ticket-id">Enter ticket id</label>
        <input id="dialog-input-ticket-id" type="text" placeholder="ticket ID"/>
    </div>
</section>

</body>
</html>

