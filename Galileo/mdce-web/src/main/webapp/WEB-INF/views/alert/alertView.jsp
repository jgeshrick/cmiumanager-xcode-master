<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2017 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<html>
<head>
    <%--Add CSRF token for used in postLink --%>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="css/main.css"/>
    <mdce:css href="css/alert.css"/>
    <mdce:css href="css/glyphicons.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery.dynamiclist.js"/>
    <mdce:js src="js/postlink.js"/>

    <title>${mdce:t("Title.Alerts")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>

<mdce:header/>
<h1 class="page-heading">Alert From: ${alertDetails.alertSource}</h1>
<c:url var="alertListPage" value="/pages/alert/list"/>
<c:url var="setAlertCleared" value="/pages/alert/setascleared/"/>

<section class="container">
    <a href="${alertListPage}" class="button">Back to Alerts List</a>
    <c:if test="${alertDetails.alertState != 'CLEARED'}">
        <a href="${setAlertCleared}${alertDetails.alertId}" class="button right">Clear Alert</a>
    </c:if>

    <form id="alert-info">
            <label for="alertSource">Alert Source:</label>
            <input id="alertSource" readonly value="${alertDetails.alertSource}"/>
            <div class="clear-fix"></div>
            <label for="alertId">Alert ID:</label>
            <input id="alertId" readonly value="${alertDetails.alertId}"/>
        </div>
        <div>
            <label for="alertCreationDate">Initial Alert Date:</label>
            <input id="alertCreationDate" readonly value="${mdce:formatDateTime(alertDetails.alertCreationDate)}"/>
            <div class="clear-fix"></div>
            <label for="alertState">Alert State:</label>
            <input id="alertState" readonly value="${alertDetails.alertState}"/>
            <div class="clear-fix"></div>
            <label for="alertLevel">Alert Level:</label>
            <input id="alertLevel" readonly value="${alertDetails.alertLevel}"/>
            <div class="clear-fix"></div>

        <c:if test="${alertDetails.alertTicketId != null}">
            <div>
                <label for="alertTicketId">Ticket Id:</label>
                <input id="alertTicketId" readonly value="${alertDetails.alertTicketId}"/>
            </div>
        </c:if>
    </form>

    <form:form modelAttribute="alertDetails" method="post">
        <div>
            <c:choose>
                <c:when test="${alertDetails.alertState=='NEW'}">
                    <form:label path="alertTicketId">Set Ticket ID:</form:label>
                    <form:input path="alertTicketId" type="text"/>
                    <input type="submit" id="add-ticket" name="addTicket" value="Add Ticket ID + Set As Handled"
                           class="button right"/>
                </c:when>
                <c:when test="${alertDetails.alertState!='CLEARED'}">
                    <form:label path="alertTicketId">Set Ticket ID:</form:label>
                    <form:input path="alertTicketId" type="text"/>
                    <input type="submit" id="add-ticket" name="addTicket" value="Change Ticket ID"
                           class="button right"/>
                </c:when>
            </c:choose>
        </div>
    </form:form>

    <div class="group" id="alert-log-dynamic-list">
        <h2>Log Messages</h2>
        <table>
            <thead>
            <tr>
                <th>Date</th>
                <th>Message</th>
            </tr>
            </thead>
            <c:forEach items="${alertLogs}" var="log">
                <tr class="list-item">
                    <td class="alert-log-list-date">
                            ${mdce:formatInstant(log.logDate)}
                    </td>
                    <td class="alert-log-list-message">${log.logMessage}</td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <div>
        <p>A maximum of 10 logs per alert are retained.</p>
    </div>
</section>

</body>
</html>

