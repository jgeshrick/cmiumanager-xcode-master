<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>

    <%--Add CSRF token for used in postLink --%>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="css/glyphicons.css"/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/audit.css"/>
    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <title>${mdce:t("Title.AuditLog")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>

<mdce:header/>
<h1 class="page-heading" id="audit-log-title">Audit Log</h1>
<section>
    <div>
        <h2>Audit list</h2>

        <div id="audit-filter" class="group">
            <h3>Filter</h3>
            <form:form modelAttribute="auditLogFilterForm" method="get">
                <div title="Show audit logs for only this MIU">
                    <mdce:labelInputElement path="miuId" label="MIU"/>
                </div>
                <div title="Show audit logs that only originated from this user">
                    <mdce:labelInputElement path="user" label="Originator"/>
                </div>
                <div title="Show audit logs from up to this many days ago">
                    <mdce:labelInputElement path="daysAgo" label="Show logs from last day"/>
                </div>
                <form:hidden path="page"/>
                <div>
                    <input type="submit" value="Filter Logs" class="button right" title="Apply Filters">
                    <c:url var="previousPage" value="./"/>
                    <a href="${previousPage}" class="button right">Reset Filter</a>
                </div>
            </form:form>
        </div>

        <div class="table-action-buttons">
            <c:url var="downloadAuditLog" value="/pages/audit/download"/>
            <a href="${downloadAuditLog}?${downloadRequestParams}" class="button right" title="Download audit log as CSV">
                Download audit log
            </a>

                <p>Showing: ${numResults} of ${totalResults} entries.

                <%--auditLogFilterForm.page page number is index 0--%>
                <c:set var="currentPage" value="${auditLogFilterForm.page + 1}" />

                <c:if test="${totalResults > 100}">
                    Page Number: ${currentPage}
                </c:if>

                <c:if test="${currentPage > 1}">
                    <c:url var="previousPage"
                           value="/pages/audit/?page=${currentPage - 1}&daysAgo=${auditLogFilterForm.daysAgo}&user=${auditLogFilterForm.user}"/>
                    <a href="${previousPage}">Prev</a>
                </c:if>

                <c:if test="${numResults >= 100}">
                    <c:url var="nextPage"
                           value="/pages/audit/?page=${currentPage + 1}&daysAgo=${auditLogFilterForm.daysAgo}&user=${auditLogFilterForm.user}"/>
                    <a href="${nextPage}">Next</a>
                </c:if>
            </p>
        </div>

        <div class="group">

            <table id="table-audit-list">
                <colgroup>
                    <col style="width: 24ex;" />
                    <col style="width: 14ex;" />
                    <col style="width: 12ex;" />
                    <col style="width: 25ex;" />
                    <col style="width: 25ex;" />
                    <col style="width: 40ex;" />
                </colgroup>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Originator</th>
                        <th>MIU ID</th>
                        <th>Event Type</th>
                        <th>Old Value</th>
                        <th>New Value</th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach items="${auditLogList}" var="auditLog">
                    <tr>
                        <td title="The date the audit log entry was made">
                                ${mdce:formatInstant(auditLog.dateTime)}
                        </td>

                        <c:url var="url"
                               value="/pages/audit/?daysAgo=${auditLogFilterForm.daysAgo}&user=${auditLog.userName}&miuId=${auditLogFilterForm.miuId}"/>
                        <td title="The user the audit log originated from">
                            <a href="${url}">${auditLog.userName}</a>
                        </td>

                        <td title="Show the details page for this MIU">
                            <mdce:miuLink muId="${auditLog.miuId}"/>
                        </td>

                        <td title="The type of event">${auditLog.auditType}</td>
                        <td title="The initial value">${auditLog.oldValue}</td>
                        <td title="The value that the user changed to">${auditLog.newValue}</td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>

        <div class="table-action-buttons"> <%-- TODO: factor the pagination + download button out into a tag --%>
            <c:url var="downloadAuditLog" value="/pages/audit/download"/>
            <a href="${downloadAuditLog}?${downloadRequestParams}" class="button right" title="Download audit log as CSV">
                Download audit log
            </a>

            <p>Showing: ${numResults} of ${totalResults} entries.

                <%--auditLogFilterForm.page page number is index 0--%>
                <c:set var="currentPage" value="${auditLogFilterForm.page + 1}" />

                <c:if test="${totalResults > 100}">
                    Page Number: ${currentPage}
                </c:if>

                <c:if test="${currentPage > 1}">
                    <c:url var="previousPage"
                           value="/pages/audit/?page=${currentPage - 1}&daysAgo=${auditLogFilterForm.daysAgo}&user=${auditLogFilterForm.user}"/>
                    <a href="${previousPage}">Prev</a>
                </c:if>

                <c:if test="${numResults >= 100}">
                    <c:url var="nextPage"
                           value="/pages/audit/?page=${currentPage + 1}&daysAgo=${auditLogFilterForm.daysAgo}&user=${auditLogFilterForm.user}"/>
                    <a href="${nextPage}">Next</a>
                </c:if>
            </p>
        </div>

    </div>
</section>

<div id="audit-log-end">
    <%--temp place holder for detecting end of page--%>
</div>

</body>
</html>

