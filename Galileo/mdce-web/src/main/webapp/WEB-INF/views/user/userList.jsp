<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="css/main.css"/>
    <mdce:css href="css/user.css"/>
    <mdce:css href="css/glyphicons.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery-ui.min.js"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.structure.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.theme.min.css"/>

    <mdce:js src="js/postlink.js"/>
    <mdce:js src="js/userList.js"/>

    <title>${mdce:t("Title.UserManagement")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>

<mdce:header/>
<h1 class="page-heading" id="user-list-title">User List</h1>

<section class="container">
    <div class="group" id="user-dynamic-list">
        <h2>Users</h2>

        <div class="table-action-buttons">
            <c:url var="url" value="/pages/user/add"/>
            <a href="${url}" class="button right" id="user-dynamic-list-add-new" >Add New User</a>
        </div>
        <table>
            <thead>
            <tr>
                <th>Username</th>
                <th>User Level</th>
                <th>Email</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>

            <tbody>

            <c:forEach items="${userlist}" var="user">
            <tr class="list-item">
                <td class="user-list-username">${user.userName}</td>
                <td class="user-list-userlevel">${user.userLevel}</td>
                <td class="user-list-email">${user.email}</td>
                <td class="user-list-status">
                    <c:choose>
                        <c:when test="${user.isCurrentlyDisabled()}">
                            Disabled until ${mdce:formatInstant(user.disabledUntil)}
                        </c:when>
                        <c:when test="${user.failedLoginAttempts != 0}">
                            <span title="An incorrect password has been entered ${user.failedLoginAttempts} time(s) consecutively for this user.">
                                OK (incorrect password entries: ${user.failedLoginAttempts})
                            </span>
                        </c:when>
                        <c:otherwise>
                            OK
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <a href="<c:url value="details/${user.userId}/${user.userName}"/>" title="View user details" >Details</a> |
                    <a href="<c:url value="resetpassword/${user.userId}/${user.userName}"/>" title="Reset Password" >Reset password</a> |
                    <a href="delete/${user.userId}" title="Delete User" class="delete-user-action" data-username="${user.userName}">Delete account</a>
                </td>
            </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="table-action-buttons">
            <c:url var="url" value="/pages/user/add"/>
            <a href="${url}" class="button right" id="user-dynamic-list-add-new" >Add New User</a>
        </div>

    </div>
</section>


<%--Dialog box--%>
<section id="dialog" title="Delete user confirmation" class="container">
    <p id="dialog-message">
        User <span id="user-name-to-delete"></span> will be deleted from the account.
        <br />
        Click <strong>Confirm Delete</strong> button to proceed.
    </p>
</section>

</body>
</html>

