<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="css/main.css"/>
    <mdce:css href="css/user.css"/>
    <mdce:css href="css/alert.css"/>
    <mdce:css href="css/glyphicons.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery.dynamiclist.js"/>
    <mdce:js src="js/postlink.js"/>

    <title>${mdce:t("Title.UserManagement")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

</head>
<body>

<c:url var="saveAlertSettings" value="/pages/user/savealerts"/>
<c:url var="changeUserDetails" value="/pages/user/changeuserdetails"/>
<c:url var="userDetails" value="/pages/user/details/${userid}/${username}"/>

<mdce:header/>
<h1 class="page-heading">User Details</h1>

<section class="container">
    <div class="group" id="user-details">
        <h2>User Details</h2>
        <form:form modelAttribute="changeUserDetailsForm" method="post" action="${changeUserDetails}">
            <form:hidden path="userId" value="${userId}"/>
            <form:hidden path="previousUserName" value="${previousUserName}"/>

            <div class="field">
                <form:label path="userName">User Name</form:label>
                <form:input path="userName" type="text"/>
                <form:errors path="userName" cssClass="error-text" />
            </div>

            <div class="field">
                <form:label path="userLevel">User Level</form:label>
                <form:select path="userLevel">
                    <form:options items="${userLevels}" itemLabel="roleDescription"/>
                </form:select>
            </div>

            <div class="field">
                <form:label path="email">Email</form:label>
                <form:input path="email" type="text"/>
                <form:errors path="email" cssClass="error-text" />
            </div>

            <div class="field">
                <form:label
                        title="When ticked, this account can be disabled after repeated failed login attempts as defined in the MDCE settings."
                        path="accountLockoutEnabled">Lockouts Enabled</form:label>
                <form:checkbox id="accountLockoutEnabled" path="accountLockoutEnabled"/>
                <form:errors path="accountLockoutEnabled" cssClass="error-text" />
            </div>

            <div class="field">
                <form:label
                        title="Allows a disabled user account to be re-enabled. The checkbox is only available when the user's account is currently disabled."
                        path="currentlyDisabled">Login Disabled</form:label>
                <form:checkbox id="currentlyDisabled" path="currentlyDisabled" disabled="${!changeUserDetailsForm.showDisabledCheckbox}"/>
            </div>

            <div class="field">
                <form:label
                        title="When ticked, the user is allowed to be logged in concurrently from different browsers and locations."
                        path="allowConcurrentSessions">Allow Concurrent Sessions</form:label>
                <form:checkbox id="allowConcurrentSessions" path="allowConcurrentSessions"/>
                <form:errors path="allowConcurrentSessions" cssClass="error-text" />
            </div>

            <div>
                <input type="submit" class="button" id="save-user-details"
                       name="saveUserDetails" value="Save User Details"/>
            </div>
        </form:form>
    </div>
</section>

<section class="container">
    <div class="group" id="user-alerts-settings">
        <h2>Alert Settings</h2>
        <form:form modelAttribute="alertsettingsform" method="post" action="${saveAlertSettings}">
            <form:hidden path="userId" value="${userId}"/>
            <table>
                <thead>
                <tr>
                    <th>Alert Source</th>
                    <th>Alert on warning</th>
                    <th>Alert on error</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="source">System</td>
                    <td class="checkbox"><form:checkbox path="systemWarning"/></td>
                    <td class="checkbox"><form:checkbox path="systemError"/></td>
                </tr>
                <tr>
                    <td class="source">CMIU</td>
                    <td class="checkbox"><form:checkbox path="cmiuWarning"/></td>
                    <td class="checkbox"><form:checkbox path="cmiuError"/></td>
                </tr>
                </tbody>
            </table>
            <div>
                <input type="submit" class="button" id="save-alert-settings"
                       name="saveAlertSettings" value="Save Alert Settings"/>
            </div>
        </form:form>
        <p>
            <c:url var="url" value="/pages/alert/list" />
            Note: User will not receive emails for existing on-going alert conditions. Please should visit the <a href="${url}"> Alerts </a> page to see any current alert conditions.
        </p>
    </div>
</section>

<section class="container">
    <div class="group" id="user-sites-dynamic-list">
        <h2>Sites For ${username}</h2>

        <div>
            <form:form method="post" modelAttribute="addsiteform" action="${userDetails}">
                <form:select path="siteId">
                    <form:option value="0" label="--Select Utility--"/>
                    <c:forEach items="${noaccesssitelist}" var="site">
                        <form:option value="${site.siteId}" label="${site.siteName}"/>
                    </c:forEach>
                </form:select>
                <div>
                    <input type="submit" class="button" id="add-new-site" name="addNewSite"
                           value="Add Site"/>
                </div>
            </form:form>
        </div>
        <table>
            <thead>
            <tr>
                <th>Site</th>
                <th>Delete Site</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${sitelist}" var="site">
                <tr class="list-item">
                    <td class="user-sites-site">${site.siteName}</td>
                    <td>
                        <a href="<c:url value="/pages/user/details/${userid}/${username}/delete/${site.siteId}"/>" title="Delete Site"><span
                                class="glyphicon glyphicon-trash"></span></a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</section>

</body>
</html>

