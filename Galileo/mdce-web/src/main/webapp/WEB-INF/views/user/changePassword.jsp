<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/user.css"/>
    <mdce:css href="css/glyphicons.css"/>
    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery.dynamiclist.js"/>

    <title>${mdce:t("Title.ChangePassword")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

    <sec:csrfMetaTags/>
</head>
<body>

<mdce:header/>
<h1 class="page-heading">${mdce:t("Title.ChangePassword")}</h1>
<section class="container">
    <form:form modelAttribute="changePasswordForm" method="post">
        <div class="group">
            <h2>Password Details</h2>

            <p>${error}</p>
            <div>
                <form:errors path="passwordMatch" class="error" element="div" />
            </div>

            <div class="field">
                <form:label path="currentPassword">Current password</form:label>
                <input type="password" name="currentPassword" id="current-password" />
                <form:errors path="currentPassword" cssClass="error-text" />
            </div>

            <div class="field">
                <form:label path="newPassword">New password</form:label>
                <input type="password" name="newPassword" id="new-password" />
                <form:errors path="newPassword" cssClass="error-text" />
            </div>

            <div class="field">
                <form:label path="newPasswordRepeated">New password (repeated)</form:label>
                <input type="password" name="newPasswordRepeated" id="new-password-repeated" />
            </div>
        <div>
            <form:input path="userName" type="hidden" id="username"/>
            <input type="submit" id="change-password" name="changePassword" value="Change Password" class="button right"/>
        </div>

    </form:form>
</section>

</body>
</html>

