<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/user.css"/>
    <mdce:css href="css/glyphicons.css"/>
    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery.dynamiclist.js"/>

    <title>${mdce:t("Title.UserManagement")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>

<mdce:header/>
<h1 class="page-heading">Add User</h1>
<section class="container">
    <form:form modelAttribute="addUserForm" method="post">
        <div>
            <form:errors path="passwordMatch" class="error" element="div" />
            <c:if test="${not empty errorMessage}">
                <div class="error">${errorMessage}</div>
            </c:if>
        </div>
        <div class="group">
            <h2>New User Details</h2>

            <p>${error}</p>

            <div class="field">
                <form:label path="userName">User Name</form:label>
                <form:input path="userName" type="text"/>
                <form:errors path="userName" cssClass="error-text" />
            </div>
            <div class="field">
                <form:label path="userLevel">User Level</form:label>
                <form:select path="userLevel">
                    <form:options items="${userLevels}" itemLabel="roleDescription"/>
                </form:select>
            </div>
            <div class="field">
                <form:label path="password">Password</form:label>
                <form:input path="password" type="password"/>
                <form:errors path="password" cssClass="error-text" />
            </div>
            <div class="field">
                <form:label path="passwordRepeated">Password (repeated)</form:label>
                <form:input path="passwordRepeated" type="password"/>
                <form:errors path="passwordRepeated" />
            </div>
            <div class="field">
                <form:label path="email">Email</form:label>
                <form:input path="email" type="text"/>
                <form:errors path="email" cssClass="error-text" />
            </div>
        <div>
            <input type="submit" id="add-user" name="addUser" value="Add User" class="button right"/>
        </div>

    </form:form>
</section>

</body>
</html>

