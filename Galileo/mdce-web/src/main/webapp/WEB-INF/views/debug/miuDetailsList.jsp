<%--
  ~  Neptune Technology Group
  ~  Copyright 2015 as unpublished work.
  ~
  ~  All rights reserved
  ~  The information contained herein is confidential
  ~  property of Neptune Technology Group. The use, copying, transfer
  ~  or disclosure of such information is prohibited except by express
  ~  written agreement with Neptune Technology Group.
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/grid.css"/>
    <title>${mdce:t("Title.Mius")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

</head>
<body>
<mdce:header/>

<div class="maincontent">
    <h2>MIU List</h2>

    <strong>Total MIUs known to MDCE: ${miuDetailsList.size()}</strong>

    <div>
        <c:if test="${not empty miuDetailsList}">
            <table>
                <thead>
                    <%--construct row header--%>
                    <tr>
                        <c:forEach items="${miuDetailsList.get(0)}" var="kvp">
                            <th>
                                    ${kvp.key}
                            </th>
                        </c:forEach>
                    </tr>
                </thead>
                <%--fill in map items per row--%>
                <c:forEach var="usage" items="${miuDetailsList}">
                    <tr>
                        <c:forEach items="${usage}" var="kvp">
                            <td>
                                    ${kvp.value}
                            </td>
                        </c:forEach>
                    </tr>
                </c:forEach>
            </table>

        </c:if>
    </div>
</div>
</body>
</html>
