<%--
  ~  Neptune Technology Group
  ~  Copyright 2017 as unpublished work.
  ~  All rights reserved.
  ~
  ~  The information contained herein is confidential
  ~  property of Neptune Technology Group. The use, copying, transfer
  ~  or disclosure of such information is prohibited except by express
  ~  written agreement with Neptune Technology Group.
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <%--Add CSRF token for used in postLink --%>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <c:url var="resourcesUrl" value="/resources"/>
    <c:url var="cellularUrl" value="/pages/diagnostics" />
    <mdce:favicon/>

    <mdce:css href="lib/jquery-ui/jquery-ui.structure.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.theme.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery.ui.combobox.css" />
    <mdce:css href="lib/jquery-ui/jquery-ui.custom.css"/>

    <mdce:css href="css/main.css"/>
    <mdce:css href="css/grid.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery-ui.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery.ui.combobox.js" />
    <mdce:js src="js/diagnosticTools.js" />
</head>

<body>
<mdce:header/>
<h1 class="page-heading" id="diagnostic-tools">Diagnostic Tools</h1>
<section>
    <div>
        <div id="packet-dump=filter" class="group">
            <h2>Packet dump</h2>
            <form>
                <div class="tableMain left">
                    <div class="tableRow">
                        <div class="tableCell">
                            <select id="idTypePacketDump" name="idTypePacketDump" class="span_3_of_4">
                                <option value="l900_id" selected>L900 ID</option>
                                <option value="cmiu_id" selected>CMIU ID</option>
                                <option value="iccid">CMIU ICCID</option>
                                <option value="imei">CMIU IMEI</option>
                                <option value="cmiu_site_id">All CMIUs for site ID</option>
                            </select>
                        </div>
                        <div class="tableCell">
                            <input id="idValPacketDump" name="idValPacketDump" placeholder="400000000" class="span_4_of_4"/>
                        </div>
                    </div>

                    <div class="tableRow">
                        <div class="tableCell">
                            <label for="fromDaysAgo" class="span_4_of_4">Records from the last </label>
                        </div>
                        <div class="tableCell">
                            <input id="fromDaysAgo" name="fromDate" value="" placeholder="10"/>
                            <span>days</span>
                        </div>
                    </div>

                    <div class="tableRow">
                        <div class="tableCell">
                            <input id="get-packet-dump-button" type=button class="button" value="Download Packets"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div>
        <div id="cns-dump=filter" class="group">
            <h2>Get CMIU cellular history</h2>
            <p>
                Download CMIU cellular connection event or usage history.
            </p>
            <div>
                <form>
                    <div class="tableMain left">
                        <div class="tableRow">
                            <div class="tableCell">
                                <label for="mno" class="span_3_of_4">Mobile network</label>
                            </div>
                            <div class="tableCell">
                                <select id="mno" name="mno" class="span_4_of_4">
                                    <option value="VZW" selected>Verizon</option>
                                    <option value="ATT">AT&T</option>
                                </select>
                            </div>
                        </div>

                        <div class="tableRow">
                            <div class="tableCell">
                                <select id="idTypeCnsDump" name="idTypeCnsDump" class="span_3_of_4">
                                    <option value="miu_id" selected>MIUID</option>
                                    <option value="iccid">ICCID</option>
                                    <option value="imei">IMEI</option>
                                    <option value="cmiu_site_id">All MIUs for site ID</option>
                                </select>
                            </div>
                            <div class="tableCell">
                                <input id="idValCnsDump" class="span_4_of_4" name="miu" placeholder="400000000" />
                            </div>
                        </div>

                        <div class="tableRow">
                            <div class="tableCell">
                                <label for="fromDate" class="span_3_of_4">From date </label>
                            </div>
                            <div class="tableCell">
                                <input id="fromDate" name="from" class="span_4_of_4" value="${fromDate}" placeholder="yyyy-mm-ddThh:ss-05:00"/>
                            </div>
                        </div>

                        <div class="tableRow">
                            <div class="tableCell">
                                <label for="toDate" class="span_3_of_4">To date (inclusive)</label>
                            </div>
                            <div class="tableCell">
                                <input id="toDate" name="to" class="span_4_of_4" value="${toDate}" placeholder="yyyy-mm-ddThh:ss-05:00"/>
                            </div>
                        </div>

                        <div class="tableRow">
                            <div class="tableCell">
                                <input id="get-usage-history-button" type=button class="button" value="Get Usage History"/>
                            </div>
                            <div class="tableCell">
                                <input id="get-connection-history-button" type=button class="button"  value="Get Connection History"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
</body>
</html>
