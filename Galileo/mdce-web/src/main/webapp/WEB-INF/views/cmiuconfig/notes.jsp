<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>

    <mdce:favicon/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/c2.css"/>

    <title>${mdce:t("Title.C2")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>
<mdce:header/>


<section class="container">
    <h1>CMIU Configuration History</h1>
    <div>
        Cmiu Id: ${miuId}
    </div>
    <div>
        <h2>Configuration set change note</h2>
        <em>Current change note is only associated with config change. TODO add change note field to command</em>
        <ul>
        <c:forEach items="${cmiuConfigHistoryList}" var="configHistory">
            <li>
                <h3><c:out value="${configHistory.dateTime}"/>:</h3>
                Config set: <c:out value="${configHistory.cmiuConfigSet.id}"/> <br />
                <p>
                    Note: <c:out value="${configHistory.changeNote}"/>
                </p>
            </li>
        </c:forEach>
        </ul>
    </div>
</section>


</body>
</html>