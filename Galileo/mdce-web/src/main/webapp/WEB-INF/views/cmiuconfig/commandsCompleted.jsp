<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<c:forEach items="${commandCompleteList}" var="entry">

    <ul class="group-row">
        <c:choose>
            <c:when test="${!entry.isSameMiuAsPrevious}">
                <li class="list-content-cmiu group-cell" title="The ID of the MIU">
                    <mdce:miuLink muId="${entry.miuId}"/>
                </li>
                <li class="list-content-sim-mno group-cell" title="The cellular network of the CMIU SIM">
                    ${entry.networkProvider}
                </li>
            </c:when>
        </c:choose>
        <li title="MIU Commands" miuId="${entry.miuId}">

            <ul class="group-row">
                <li class="list-content-user group-cell" title="The user who sent this command">
                        ${entry.user}
                </li>
                <li class="list-content-command group-cell" title="The command which was sent">
                        ${entry.commandType}
                </li>
                <li class="list-content-details group-cell" title="The details for the command sent">
                        ${entry.details}
                </li>
                <li class="list-content-submitted group-cell" title="The time the command was sent">
                    <span title="${entry.commandEnteredTime}"> ${mdce:formatInstant(entry.commandEnteredTime)}</span>
                </li>
                <li class="list-content-received group-cell" title="The time the command was completed">
                    <span title="${entry.commandFulfilledTime}"> ${mdce:formatInstant(entry.commandFulfilledTime)}</span>
                </li>
                <li class="list-content-note group-cell">
                    <a href="config/notes/${entry.miuId}" title="Notes for MIU ${entry.miuId}" > <span class="glyphicon glyphicon-file"></span></a>
                </li>
            </ul>

        </li>
    </ul>

</c:forEach>

<div class="commands-completed-loaded"/>
