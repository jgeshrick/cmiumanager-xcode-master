<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>

    <mdce:favicon/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/c2.css"/>
    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="js/inputFile.js"/>

    <title>${mdce:t("Title.C2")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>
<mdce:header/>

<h1 class="page-heading">Batch add CMIU list</h1>

<section id="import-cmiu-list" class="container">
    <div class="group">
        <h1>Import CMIU list</h1>

        <div>
            <p>Import CMIU list from a text file. Used for batch adding a list of CMIU to the CMIU Config tab.</p>

            <p>Format for the file TBD. Currently assumes the file contains a list of CMIU, 1 id per line.</p>
        </div>

        <%-- csrf has to be submitted as a param for multipart form!
            http://docs.spring.io/spring-security/site/docs/current/reference/html/csrf.html#csrf-include-csrf-token-in-action
        --%>
        <form:form method="post" modelAttribute="importCmiuFileForm" enctype="multipart/form-data" action="batch-add-cmiu?${_csrf.parameterName}=${_csrf.token}">
            <form:hidden path="redirectUrl" />
<%--            <div>
                <form:label path="cmiuListDefFile">Select file to import</form:label>
                <form:input path="cmiuListDefFile" type="file" cssClass="custom-file-input"/>
            </div>--%>
            <div>
                <label for="cmiu-list-file">Select file to import</label>
                <span id="cmiu-list-file" class="button-file button">Browse<input name="cmiuListDefFile" type="file"></span>
                <input id="selected-file-name" disabled="disabled" placeholder="No file selected" />
            </div>
            <div>
                <form:label path="changeNote">Change note</form:label>
                <form:textarea path="changeNote"/>
            </div>

            <div>
                <input type="submit" class="button" value="Populate CMIU list"/>
            </div>
        </form:form>
    </div>
</section>

</body>
</html>