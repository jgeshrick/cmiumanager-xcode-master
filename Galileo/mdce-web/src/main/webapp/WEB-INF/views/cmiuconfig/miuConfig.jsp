<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <%--Add CSRF token for used in postLink --%>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="css/main.css"/>
    <mdce:css href="css/c2.css"/>
    <mdce:css href="css/glyphicons.css"/>
    <mdce:css href="css/miuconfig.css"/>

    <mdce:css href="lib/jquery-ui/jquery-ui.structure.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.theme.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.custom.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery.dynamiclist.js"/>
    <mdce:js src="lib/jquery-ui/jquery-ui.min.js"/>
    <mdce:js src="js/postlink.js"/>

    <mdce:js src="js/miuConfig.js" />

    <title>${mdce:t("Title.C2")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<c:url var="configUrl" value="/pages/config/"/>

<body configUrl="${configUrl}">

<mdce:header/>
<h1 class="page-heading" id="c2-interface-title">Command &amp; Control Interface</h1>
<section class="container">

    <div class="cnc-float-left">
        <input type="text" placeholder="Filter by MIU ID" name="miuIdFilter" id="miuIdFilter"/>
        <select name="networkProvider" id="networkProvider">
            <option value="">Network: any</option>
            <option value="(none)">Network: none</option>
            <c:forEach var="network" items="${networkProviders}">
                <option value="${network}">${network}</option>
            </c:forEach>
        </select>
        <input type="button" id="filterMiuButton" class="button" value="Filter"/>
    </div>

    <div>
        <div id="c2-toolbar">
            <div id="c2-toolbar-dropdown">
                <a class="button" title="Select the page for the type of command to send">Send Commands<span class="glyphicon glyphicon-chevron-down"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="<mdce:url value="config/image-update" />">Update image</a></li>
                    <li><a href="<mdce:url value="config/set-recording-reporting-interval" />">Recording/Reporting
                        interval</a></li>
                    <li><a href="<mdce:url value="config/request-publish-packet" />">Request Publish Packet</a></li>
                    <li><a href="<mdce:url value="config/update-modem-firmware" />">CMIU Modem FOTA</a></li>
                    <li><a href="<%--<mdce:url value="config/reboot-cmiu" />--%>#" class="disabled">Reboot CMIU</a></li>
                    <li><a href="<%--<mdce:url value="config/emulate-mag-swipe" />--%>#" class="disabled">Emulate Mag Swipe</a></li>
                    <li><a href="<%--<mdce:url value="config/erase-datalog" />--%>#" class="disabled">Erase Datalog</a></li>
                    <li><a href="<mdce:url value="config/set-time-offset-l900" />">Set Time Offset L900</a></li>
                    <li><a href="<mdce:url value="config/reset-l900" />">Reset L900</a></li>
                    <li><a href="<mdce:url value="config/deactivate-l900" />">Deactivate L900</a></li>
                </ul>
            </div>

        </div>
    </div>
</section>

<section class="container">
    <div class="group commands-in-progress">
        <h2>Commands In Progress<span class="h2-extra-info"></span></h2>

        <div class="expand-collapse"><a href="#" class="expand-collapse-section"></a></div>
        <div class="progress-header-group group-header group-table">
            <div class="loading">Loading...</div>
            <ul class="group-row">
                <li class="list-content-cmiu group-cell"><a class="column-sort" href="#" sortBy="miuId">MIU</a></li>
                <li class="list-content-sim-mno group-cell"><a class="column-sort" href="#" sortBy="networkProvider">Network</a></li>
                <li class="list-content-user group-cell"><a class="column-sort" href="#" sortBy="user">User</a></li>
                <li class="list-content-command group-cell"><a class="column-sort" href="#" sortBy="command">Command</a></li>
                <li class="list-content-details group-cell"><a class="column-sort" href="#" sortBy="details">Details</a></li>
                <li class="list-content-submitted group-cell"><a class="column-sort" href="#" sortBy="submitted">Submitted</a></li>
                <li class="list-content-note group-cell">Note</li>
                <li class="list-content-action group-cell">Action</li>
            </ul>
        </div>

        <div class="progress-content-group group-content group-table" id="commands-in-progress-div"
             src="commandsInProgress" sortBy="+miuId,-submitted" expanded="true">
        </div>
    </div>

    <div class="group commands-received">
        <h2>Image Update Commands Received<span class="h2-extra-info"></span></h2>

        <div class="expand-collapse"><a href="#" class="expand-collapse-section"></a></div>
        <div class="progress-header-group group-header group-table">
            <div class="loading">Loading...</div>
            <ul class="group-row">
                <li class="list-content-cmiu group-cell"><a class="column-sort" href="#" sortBy="miuId">MIU</a></li>
                <li class="list-content-sim-mno group-cell"><a class="column-sort" href="#" sortBy="networkProvider">Network</a></li>
                <li class="list-content-user group-cell">User</li>
                <li class="list-content-command group-cell"><a class="column-sort" href="#" sortBy="command">Command</a></li>
                <li class="list-content-details group-cell"><a class="column-sort" href="#" sortBy="details">Details</a></li>
                <li class="list-content-submitted group-cell"><a class="column-sort" href="#" sortBy="submitted">Submitted</a></li>
                <li class="list-content-received group-cell"><a class="column-sort" href="#" sortBy="fulfilled">Received</a></li>
                <li class="list-content-note group-cell">Note</li>
                <li class="list-content-action group-cell">Action</li>
            </ul>
        </div>

        <div class="progress-content-group group-content group-table" id="commands-received-div"
             src="commandsReceived" sortBy="+miuId,-fulfilled" expanded="true">
        </div>
    </div>

    <div class="group commands-recently-completed">

        <h2>Recently Completed Commands<span class="h2-extra-info"></span></h2>

        <p>Completed within last 10 days</p>

        <div class="expand-collapse"><a href="#" class="expand-collapse-section"></a></div>
        <div class="progress-header-group group-header group-table">
            <div class="loading">Loading...</div>
            <ul class="group-row">
                <li class="list-content-cmiu group-cell"><a class="column-sort" href="#" sortBy="miuId">MIU</a></li>
                <li class="list-content-sim-mno group-cell"><a class="column-sort" href="#" sortBy="networkProvider">Network</a></li>
                <li class="list-content-user group-cell">User</li>
                <li class="list-content-command group-cell"><a class="column-sort" href="#" sortBy="command">Command</a></li>
                <li class="list-content-details group-cell"><a class="column-sort" href="#" sortBy="details">Details</a></li>
                <li class="list-content-submitted group-cell"><a class="column-sort" href="#" sortBy="submitted">Submitted</a></li>
                <li class="list-content-received group-cell"><a class="column-sort" href="#" sortBy="fulfilled">Completed</a></li>
                <li class="list-content-note group-cell">Note</li>
            </ul>
        </div>

        <div class="progress-content-group group-content group-table" id="commands-completed-div"
             src="commandsCompleted" sortBy="+miuId,-fulfilled" expanded="true">
        </div>
    </div>

    <div class="group commands-rejected">

        <h2>Rejected Commands<span class="h2-extra-info"></span></h2>

        <p>Rejected within last 10 days</p>

        <div class="expand-collapse"><a href="#" class="expand-collapse-section"></a></div>
        <div class="progress-header-group group-header group-table">
            <div class="loading">Loading...</div>
            <ul class="group-row">
                <li class="list-content-cmiu group-cell"><a class="column-sort" href="#" sortBy="miuId">MIU</a></li>
                <li class="list-content-sim-mno group-cell"><a class="column-sort" href="#" sortBy="networkProvider">Network</a></li>
                <li class="list-content-user group-cell">User</li>
                <li class="list-content-command group-cell"><a class="column-sort" href="#" sortBy="command">Command</a></li>
                <li class="list-content-details group-cell"><a class="column-sort" href="#" sortBy="details">Details</a></li>
                <li class="list-content-submitted group-cell"><a class="column-sort" href="#" sortBy="submitted">Submitted</a></li>
                <li class="list-content-received group-cell"><a class="column-sort" href="#" sortBy="fulfilled">Completed</a></li>
                <li class="list-content-note group-cell">Note</li>
            </ul>
        </div>

        <div class="progress-content-group group-content group-table" id="commands-rejected-div"
             src="commandsRejected" sortBy="+miuId,-fulfilled" expanded="true">
        </div>
    </div>

    <div id="more-above" class="more-in-group-above">More &uArr;</div>
    <div id="more-below" class="more-in-group-below">More &dArr;</div>

</section>

<%--Dialog box--%>
<section id="dialog" title="Confirm action" class="container">
    <p id="dialog-message"></p>
</section>

</body>
</html>

