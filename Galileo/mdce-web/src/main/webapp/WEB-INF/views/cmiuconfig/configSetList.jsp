<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015, 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="css/main.css"/>
    <mdce:css href="css/c2.css"/>

    <title>${mdce:t("Title.C2")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>
<mdce:header/>
<h1 class="page-heading" id="config-set-list-title">List CMIU config set</h1>
<section>
    <div class="group">
        <div>
            <c:url var="url" value="/pages/config-set/add"/>
            <p><a href="${url}" class="button">+ Add Config Set</a></p>
        </div>

        <div>
            <table id="cmiu-config-set-table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>CMIU mode name</th>
                    <th>Reporting Start (mins after 00:00 UTC)</th>
                    <th>Report Interval (hours)</th>
                    <th>Reporting Retries</th>
                    <th>Transmit window (mins)</th>
                    <th>Quiet start (mins after 00:00 UTC)</th>
                    <th>Quiet end (mins after 00:00 UTC)</th>
                    <th>Recording Start (mins after 00:00 UTC)</th>
                    <th>Recording Interval (mins)</th>
                    <th>Tools</th>
                </tr>
                </thead>
                <c:forEach items="${configSetList}" var="configSet">
                    <tr>
                        <td>${configSet.id}</td>
                        <td>${configSet.name}</td>
                        <td>${configSet.cmiuModeName}</td>
                        <td>${configSet.reportingStartMins}</td>

                        <fmt:formatNumber var="reportingPlanIntervalHoursDisplayValue"
                                          value="${configSet.reportingIntervalMins / 60}"
                                          maxFractionDigits="0" />
                        <td>${reportingPlanIntervalHoursDisplayValue}</td>
                        <td>${configSet.reportingNumberOfRetries}</td>
                        <td>${configSet.reportingTransmitWindowMins}</td>
                        <td>${configSet.reportingQuietStartMins}</td>
                        <td>${configSet.reportingQuietEndMins}</td>
                        <td>${configSet.recordingStartTimeMins}</td>
                        <td>${configSet.recordingIntervalMins}</td>
                        <td>
                            <c:url var="url" value="/pages/config-set/edit?id=${configSet.id}"/>
                            <a href="${url}">Edit</a>
                        </td>
                    </tr>
                </c:forEach>

            </table>
        </div>

    </div>


</section>

</body>
</html>
