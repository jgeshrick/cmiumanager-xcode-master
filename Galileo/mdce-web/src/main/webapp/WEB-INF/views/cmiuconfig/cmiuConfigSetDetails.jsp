<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>

    <mdce:favicon/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/c2.css"/>

    <title>${mdce:t("Title.C2")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>
</head>
<body>
<mdce:header/>

<h1 class="page-heading">${newCmiuConfigSetForm.displayHeader}</h1>
<c:if test="${not empty configSetId}">
    <c:url var="editConfigSetUrl" value="/pages/config-set/edit" >
        <c:param name="id" value="${configSetId}" />
    </c:url>
    <span class="error">Config Set already exists link to edit <a href="${editConfigSetUrl}">Here</a></span>
</c:if>


<section class="config-set-edit container">
    <div class="group">
        <form:form modelAttribute="newCmiuConfigSetForm" method="post">
            <div>
                <mdce:labelInputElement path="name" label="Name"/>
            </div>
            <div>
                <h3>CMIU mode</h3>

                <div>
                    <mdce:labelInputElement path="cmiuModeName" label="CMIU mode name"/>
                </div>

                <div class="field">
                    <form:label
                            title="When ticked, this mode will be flagged as an official mode and alerts will not be triggered when a unit reports this mode."
                            path="officialMode">Is Official Mode</form:label>
                    <form:checkbox id="officialMode" path="officialMode"/>
                    <form:errors path="officialMode" cssClass="error-text" />
                </div>

            </div>
            <div>
                <h3>Reporting plan</h3>

                <div>
                    <mdce:labelInputElement path="reportingPlanStartMins" label="Reporting start minutes (mins after midnight UTC)" readonly = "${!newCmiuConfigSetForm.addNewConfig}" />
                </div>

                <div>
                    <mdce:labelInputElement path="reportingPlanIntervalHours" label="Reporting interval (hours)" readonly = "${!newCmiuConfigSetForm.addNewConfig}" />
                </div>

                <div>
                    <mdce:labelInputElement path="reportingPlanNumberOfRetries" label="Reporting retries" readonly = "${!newCmiuConfigSetForm.addNewConfig}" />
                </div>

                <div>
                    <mdce:labelInputElement path="reportingPlanTransmitWindowMins" label="Transmit window (mins)" readonly = "${!newCmiuConfigSetForm.addNewConfig}" />
                </div>

                <div>
                    <mdce:labelInputElement path="reportingPlanQuietStartMins" label="Quiet start (mins after midnight UTC)" readonly = "${!newCmiuConfigSetForm.addNewConfig}" />
                </div>

                <div>
                    <mdce:labelInputElement path="reportingPlanQuietEndMins" label="Quiet end (mins after midnight UTC)" readonly = "${!newCmiuConfigSetForm.addNewConfig}" />
                </div>
            </div>
            <div>
                <h3>Recording plan</h3>

                <div>
                    <mdce:labelInputElement path="recordingPlanStartTimeMins" label="Recording start time (minutes after midnight UTC)" readonly = "${!newCmiuConfigSetForm.addNewConfig}" />
                </div>
                <div>
                    <mdce:labelInputElement path="recordingPlanIntervalMins" label="Recording interval (mins)" readonly = "${!newCmiuConfigSetForm.addNewConfig}" />
                </div>

            </div>

            <form:hidden path="addNewConfig" />

            <div class="input">
                <input type="submit" class="button right" value="Submit"/>
            </div>


        </form:form>
    </div>
</section>

</body>
</html>