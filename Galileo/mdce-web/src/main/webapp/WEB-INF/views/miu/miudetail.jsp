<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015, 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <%--Add CSRF token for used in postLink --%>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="css/glyphicons.css"/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/miudetails.css"/>
    <mdce:css href="css/audit.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.structure.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.theme.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.custom.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery-ui.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery.ui.combobox.js" />
    <mdce:js src="js/miudetail.js"/>

    <script type="text/javascript">
        $(function() {
            $(".ui-comboBox").combobox();
        });
    </script>

    <title>${mdce:t("Title.Miu")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

</head>
<body>

<mdce:header/>
<h1 class="page-heading">MIU Details</h1>
<section>
    <div>
        <c:url var="url" value="/pages/miu/miulist"/>
        <h2><a href="${url}">MIU List</a> > MIU Details : ${auditLogFilterForm.miuId}</h2>

        <c:set var="miu" value="${miuDetails}" />
        <div class="group">
            <div id="miu-details">
                <dl>
                    <dt>Type:</dt>
                    <dd>${miu.type}</dd>
                </dl>
                <c:if test="${miu.type == 'CMIU' || miu.type == 'L900'}">
                    <dl>
                        <dt>Lifecycle State:</dt>
                        <dd>${miu.lifecycleState}</dd>
                    </dl>
                </c:if>
                <dl>
                    <dt>Site:</dt>
                    <dd site-id="${miu.siteId}">${miu.siteDisplay}</dd>
                </dl>
                <dl>
                    <dt title="The last time the MIU was heard by MDCE">Last heard time:</dt>
                    <c:choose>
                        <c:when test="${not empty miu.lastHeardTimeAsInstant}">
                            <dd>${mdce:formatInstant(miu.lastHeardTimeAsInstant)}</dd>
                        </c:when>
                        <c:otherwise>
                            <dd>(Not yet heard)</dd>
                        </c:otherwise>
                    </c:choose>
                </dl>
                <dl>
                    <dt title="Is the MIU billable">Active:</dt>
                    <dd>${miu.meterActive}</dd>
                </dl>
                <%-- TODO MSPD-1006: Record image version details in MySQL --%>
                <c:if test="${miu.type == 'CMIU'}">
                    <dl>
                        <dt title="The cellular network">Cellular network:</dt>
                        <dd>Modem: ${miu.modemMno}; SIM: ${miu.networkProvider}</dd>
                    </dl>
                    <dl>
                        <dt title="The CMIU firmware revision reported by the MIU if supported">CMIU firmware revision:</dt>
                        <c:choose>
                            <c:when test="${not empty miu.cmiuFirmwareRevision}">
                                <dd>${miu.cmiuFirmwareRevision}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not yet reported)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The config revision reported by the MIU if supported">CMIU config revision:</dt>
                        <c:choose>
                            <c:when test="${not empty miu.cmiuConfigRevision}">
                                <dd>${miu.cmiuConfigRevision}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not yet reported)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The encryption revision reported by the MIU if supported">CMIU encryption revision:</dt>
                        <c:choose>
                            <c:when test="${not empty miu.cmiuEncryptionRevision}">
                                <dd>${miu.cmiuEncryptionRevision}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not yet reported)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The arb revision reported by the MIU if supported">CMIU ARB revision:</dt>
                        <c:choose>
                            <c:when test="${not empty miu.cmiuArbRevision}">
                                <dd>${miu.cmiuArbRevision}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not yet reported)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The ble revision reported by the MIU if supported">CMIU BLE revision:</dt>
                        <c:choose>
                            <c:when test="${not empty miu.cmiuBleRevision}">
                                <dd>${miu.cmiuBleRevision}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not yet reported)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The telit firmware revision reported by the MIU if supported">Telit firmware revision:</dt>
                        <c:choose>
                            <c:when test="${not empty miu.telitFirmwareRevision}">
                                <dd>${miu.telitFirmwareRevision}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not yet reported)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The IMEI number reported from verizon for CMIUs only">IMEI:</dt>
                        <dd>${miu.imei}</dd>
                    </dl>
                    <dl>
                        <dt title="The ICCID number reported from verizon for CMIUs only">ICCID:</dt>
                        <dd>${miu.iccid}</dd>
                    </dl>
                    <dl>
                        <dt title="The MSISDN number, reported from verizon for CMIUs only">MSISDN:</dt>
                        <dd>${miu.msisdn}</dd>
                    </dl>
                    <dl>
                        <dt title="The cellular data usage, reported from Verizon for CMIUs only">Cellular data usage:</dt>
                        <c:choose>
                            <c:when test="${not empty miu.miuDataUsage}">
                                <dd>
                                    <dl>
                                        <dt>Daily</dt>
                                        <dd>${miu.miuDataUsage.byteUsedToday} bytes used between
                                        ${mdce:formatDateTime(midnightUtc)} and ${mdce:formatInstant(miu.miuDataUsage.dataUsageDatetime)}</dd>
                                    </dl>
                                    <dl>
                                        <dt>Current month</dt>
                                        <dd>${miu.miuDataUsage.byteUsedMonthToDate} bytes used between
                                                ${mdce:formatDateTime(startOfMonthMidnightUtc)} and ${mdce:formatInstant(miu.miuDataUsage.dataUsageDatetime)}</dd>
                                    </dl>
                                </dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not available)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The CMIU config set name">Current CMIU config set:</dt>
                        <c:choose>
                            <c:when test="${not empty miu.cmiuConfigSetName}">
                                <dd>${miu.cmiuConfigSetName}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not available)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The CMIU mode name">Current CMIU mode:</dt>
                        <c:choose>
                            <c:when test="${not empty miu.cmiuModeName}">
                                <dd>${miu.cmiuModeName}</dd>
                            </c:when>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The CMIU reading interval in minutes">Current CMIU reading interval:</dt>
                        <c:choose>
                            <c:when test="${miu.readingIntervalMins ne 0}">
                                <dd>${miu.readingIntervalMins}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not available)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The CMIU recording interval in minutes">Current CMIU recording interval:</dt>
                        <c:choose>
                            <c:when test="${miu.recordingIntervalMins ne 0}">
                                <dd>${miu.recordingIntervalMins}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not available)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The CMIU reporting interval in minutes">Current CMIU reporting interval:</dt>
                        <c:choose>
                            <c:when test="${miu.reportingIntervalMins ne 0}">
                                <dd>${miu.reportingIntervalMins}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not available)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>

                    <dl>
                        <dt title="The CMIU config set name">Reported CMIU config set:</dt>
                        <c:choose>
                            <c:when test="${not empty miu.reportedCmiuConfigSetName}">
                                <dd>${miu.reportedCmiuConfigSetName}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not available)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The CMIU mode name">Reported CMIU mode:</dt>
                        <c:choose>
                            <c:when test="${not empty miu.reportedCmiuModeName}">
                                <dd>${miu.reportedCmiuModeName}</dd>
                            </c:when>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The CMIU reading interval in minutes">Reported CMIU reading interval:</dt>
                        <c:choose>
                            <c:when test="${miu.reportedReadingIntervalMins ne 0}">
                                <dd>${miu.reportedReadingIntervalMins}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not available)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The CMIU recording interval in minutes">Reported CMIU recording interval:</dt>
                        <c:choose>
                            <c:when test="${miu.reportedRecordingIntervalMins ne 0}">
                                <dd>${miu.reportedRecordingIntervalMins}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not available)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                    <dl>
                        <dt title="The CMIU reporting interval in minutes">Reported CMIU reporting interval:</dt>
                        <c:choose>
                            <c:when test="${miu.reportedReportingIntervalMins ne 0}">
                                <dd>${miu.reportedReportingIntervalMins}</dd>
                            </c:when>
                            <c:otherwise>
                                <dd>(Not available)</dd>
                            </c:otherwise>
                        </c:choose>
                    </dl>
                </c:if>
                <c:if test="${miu.type == 'L900'}">
                    <dl>
                        <dt title="EUI">EUI:</dt>
                        <dd>${miu.eui}</dd>
                    </dl>

                    <dl>
                        <dt title="Antenna Type">Antenna Type:</dt>
                        <dd>${miu.antennaType}</dd>
                    </dl>
                </c:if>
            </div>
        </div>

        <h2>Change MIU Ownership</h2>
        <div class="group">

            <form id="form-miu-ownership" method="post">
                <div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <select id="newSiteId" name="newSiteId" class="ui-comboBox" title="Change the site ID associated with the MIU" data-currentsite="${miu.siteDisplay}">
                        <c:forEach items="${siteListOptions}" var="siteDetail">
                            <%-- Note: current site is not actually present in siteListOptions, so no option will actually be pre-selected --%>
                            <option value="${siteDetail.siteId}" ${siteDetail.siteId == miu.siteId ? "selected":""}>${siteDetail.siteName} (Site ID ${siteDetail.siteId})</option>
                        </c:forEach>
                    </select>

                    <input id="change-ownership-submit" type="submit" value="Change Ownership" class="button"/>

                    <div id="dialog-message">Are you sure you want to change the owner from <div id="old-site-display">${miu.siteDisplay}</div> to <div id="new-site-display">...</div>?</div>
                </div>
            </form>
        </div>

        <c:if test="${miu.type == 'CMIU' || miu.type == 'L900'}">
            <h2>Change CMIU Lifecycle State</h2>
            <div class="group">

                <form id="form-cmiu-lifecycle-state" method="post">
                    <div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <select id="lifecycleState" name="lifecycleState" title="Change the Lifecycle State associated with the CMIU">
                            <c:forEach items="${lifecycleStates}" var="lifecycleState">
                                <%-- Note: current site is not actually present in siteListOptions, so no option will actually be pre-selected --%>
                                <option value="${lifecycleState}">${lifecycleState}</option>
                            </c:forEach>
                        </select>

                        <input id="change-cmiu-lifecycle-state-submit" type="submit" value="Change Lifecycle State" class="button"/>
                    </div>
                </form>
            </div>
        </c:if>


        <h2>Audit Log Entries for MIU ${auditLogFilterForm.miuId}</h2>

        <div id="audit-filter" class="group">
            <h3>Filter Audit Log Entries</h3>

            <form:form modelAttribute="auditLogFilterForm" method="get">
                <mdce:labelInputElement path="user" label="User"/>
                <mdce:labelInputElement path="daysAgo" label="Show logs from previous days"/>
                <form:hidden path="miuId"/>
                <form:hidden path="page"/>
                <div><input type="submit" value="Filter Audit Log Entries" class="button"></div>
            </form:form>
        </div>

        <c:if test="${miu.auditLog.totalResults == 0}">
            <div> No Audit Log entries found</div>
        </c:if>
        <c:if test="${miu.auditLog.totalResults > 0}">


            <div>
                Showing: ${miu.auditLog.numResults} of ${miu.auditLog.totalResults} entries.

                <%--auditLogFilterForm.page page number is index 0--%>
                <c:set var="currentPage" value="${auditLogFilterForm.page + 1}" />

                <c:if test="${miu.auditLog.totalResults > 100}">
                    Page Number: ${currentPage}
                </c:if>

                <c:if test="${currentPage > 1}">
                    <c:url var="previousPage"
                           value="/pages/miu/miudetail/${auditLogFilterForm.miuId}/?page=${currentPage - 1}&daysAgo=${auditLogFilterForm.daysAgo}&user=${auditLogFilterForm.user}"/>
                    <a href="${previousPage}">Prev</a>
                </c:if>

                <c:if test="${miu.auditLog.numResults >= 100}">
                    <c:url var="nextPage"
                           value="/pages/miu/miudetail/${auditLogFilterForm.miuId}/?page=${currentPage + 1}&daysAgo=${auditLogFilterForm.daysAgo}&user=${auditLogFilterForm.user}"/>
                    <a href="${nextPage}">Next</a>
                </c:if>
            </div>
            <div class="group">
                <table id="table-audit-list">
                    <colgroup>
                        <col style="width: 24ex;" />
                        <col style="width: 14ex;" />
                        <col style="width: 25ex;" />
                        <col style="width: 25ex;" />
                        <col style="width: 40ex;" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th>Log time</th>
                        <th>User Name</th>
                        <th>Type</th>
                        <th>Old Values</th>
                        <th>New Values</th>
                    </tr>
                    </thead>

                    <tbody>

                    <c:forEach items="${miu.auditLog.results}" var="auditLogEntry">
                        <tr>
                            <td title="The date the audit log entry was made">${mdce:formatInstant(auditLogEntry.dateTime)}</td>
                            <c:url var="url"
                                   value="/pages/miu/miudetail/${auditLogFilterForm.miuId}/?user=${auditLogEntry.userName}&daysAgo=${auditLogFilterForm.daysAgo}"/>
                            <td title="The user the audit log originated from"><a href="${url}">${auditLogEntry.userName}</a></td>
                            <td title="The type of event">${auditLogEntry.auditType}</td>
                            <td title="The initial value">${auditLogEntry.oldValue}</td>
                            <td title="The value that the user changed to">${auditLogEntry.newValue}</td>
                        </tr>
                    </c:forEach>

                    </tbody>
                </table>
            </div>

            <div>
                Showing: ${miu.auditLog.numResults} of ${miu.auditLog.totalResults} entries.

                    <%--auditLogFilterForm.page page number is index 0--%>
                <c:set var="currentPage" value="${auditLogFilterForm.page + 1}" />

                <c:if test="${miu.auditLog.totalResults > 100}">
                    Page Number: ${currentPage}
                </c:if>

                <c:if test="${currentPage > 1}">
                    <c:url var="previousPage"
                           value="/pages/miu/miudetail/${auditLogFilterForm.miuId}/?page=${currentPage - 1}&daysAgo=${auditLogFilterForm.daysAgo}&user=${auditLogFilterForm.user}"/>
                    <a href="${previousPage}">Prev</a>
                </c:if>

                <c:if test="${miu.auditLog.numResults >= 100}">
                    <c:url var="nextPage"
                           value="/pages/miu/miudetail/${auditLogFilterForm.miuId}/?page=${currentPage + 1}&daysAgo=${auditLogFilterForm.daysAgo}&user=${auditLogFilterForm.user}"/>
                    <a href="${nextPage}">Next</a>
                </c:if>
            </div>
        </c:if>
    </div>
</section>


</body>
</html>

