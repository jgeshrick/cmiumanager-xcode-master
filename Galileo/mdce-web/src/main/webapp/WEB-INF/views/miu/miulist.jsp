<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <%--Add CSRF token for used in postLink --%>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>

    <mdce:css href="lib/jquery-ui/jquery-ui.structure.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery-ui.theme.min.css"/>
    <mdce:css href="lib/jquery-ui/jquery.ui.combobox.css" />
    <mdce:css href="lib/jquery-ui/jquery-ui.custom.css"/>

    <mdce:css href="css/glyphicons.css"/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/miudetails.css"/>

    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery-ui.min.js"/>
    <mdce:js src="lib/jquery-ui/jquery.ui.combobox.js" />

    <script>
        $(function() {
            $(".ui-comboBox").combobox();

            // Automatically post back when MIU Type/CMIU Mode selection is changed.
            $("#selectMiuType").change(function() {
                $("#miuListFilterForm").submit();
            });

            $("#selectCmiuMode").change(function() {
                $("#miuListFilterForm").submit();
            });

            // Re-examine the entered ID whenever it's changed.
            $("#idInput").keyup(function () { endpointIdChanged(); });
            $("#idInput").change(function () { endpointIdChanged(); });

            // Update on load.
            endpointIdChanged();
        });

        // Keep the detected ID type up to date
        function endpointIdChanged() {

            // Change the selected MIU type based on the number of digits in the entered ID
            var idInput = $("#idInput").val();
            var selectIdType = $("#selectIdType");
            var enteredIdType = getMiuIdType(idInput);
            selectIdType.val(enteredIdType);
            var idTypeText = $("#selectIdType option:selected").text();
            $("#displayedIdType").text(idTypeText);

            // Show the input greyed-out if the number doesn't have a recognized ID format.
            if (idTypeText){
                $("#idInput").removeClass("greyed-out");
            } else {
                if (!$("#idInput").hasClass("greyed-out")) {
                    $("#idInput").addClass("greyed-out");
                }
            }
        }

        // Detect the type of ID the user has entered
        function getMiuIdType(number) {

            // Check for null string
            if (!number) {
                return "";
            }

            // Strip out slashes, dashes and spaces
            var digits = number
                    .replace(/\\/g, "")
                    .replace(/\//g, "")
                    .replace(/-/g, "") // Hyphen
                    .replace(/–/g, "") // En-dash
                    .replace(/—/g, "") // Em-dash
                    .replace(/ /g, "");

            // Check for non-numeric characters
            if (digits.match(/[^\d]/)) {
                return "";
            }

            // Check for known lengths of digits
            if (digits.length >= 9 && digits.length <= 10 && parseInt(digits) <= 2147483647) {
                return "miuId";
            } else if (digits.length >= 15 && digits.length <= 16) {
                return "imei";
            } else if (digits.length >= 19 && digits.length <= 20) {
                return "iccid";
            } else {
                return "";
            }
        }

    </script>

    <title>${mdce:t("Title.Mius")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

</head>
<body>
<span id="timestamp" class="ui-helper-hidden"><%= new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new java.util.Date()) %></span>
<mdce:header/>
<h1 class="page-heading" id="miu-list-title">MIU List</h1>
<section>
    <div>

        <div id="miu-filter" class="group">
            <h2>Filter</h2>
            <form:form modelAttribute="miuListFilterForm" method="get" id="miuListFilterForm">
                <div class="tableMain left padRight">

                    <div class="tableRow">
                        <div class="tableCell">
                            <form:label cssClass="medium" path="enteredId">ID <span id="displayedIdType"></span></form:label>
                        </div>
                        <div class="tableCell">
                            <form:input path="enteredId" id="idInput" placeholder="MIU ID, IMEI or ICCID" />
                            <form:select path="enteredIdType" id="selectIdType" style="display: none;">
                                <form:option value=""></form:option>
                                <form:option value="miuId"> (MIU ID)</form:option>
                                <form:option value="imei"> (IMEI)</form:option>
                                <form:option value="iccid"> (ICCID)</form:option>
                            </form:select>
                        </div>
                    </div>

                    <div class="tableRow">
                        <div class="tableCell">
                            <form:label cssClass="medium" path="selectedMiuType">MIU Type</form:label>
                        </div>
                        <div class="tableCell">
                            <form:select path="selectedMiuType" id="selectMiuType">
                                <c:forEach items="${miuListFilterForm.miuTypes}" var="miuType">
                                    <form:option value="${miuType}" selected="${miuType == miuListFilterForm.selectedMiuType ? 'SELECTED' : ''}" label="${miuType}"/>
                                </c:forEach>
                            </form:select>
                        </div>
                    </div>

                    <div class="tableRow">
                        <div class="tableCell">
                            <form:label cssClass="medium" path="selectedCmiuMode">CMIU Mode</form:label>
                        </div>
                        <div class="tableCell">
                            <form:select path="selectedCmiuMode" id="selectCmiuMode">
                                <c:forEach items="${miuListFilterForm.cmiuModeOptions}" var="cmiuMode">
                                    <form:option value="${cmiuMode}" selected="${cmiuMode == miuListFilterForm.selectedCmiuMode ? 'SELECTED' : ''}" label="${cmiuMode}"/>
                                </c:forEach>
                            </form:select>
                        </div>
                    </div>

                    <div class="tableRow">
                        <div class="tableCell">
                            <form:label cssClass="medium" path="selectedNetworkProvider">Network</form:label>
                        </div>
                        <div class="tableCell">
                            <form:select path="selectedNetworkProvider" id="selectedNetworkProvider">
                                <c:forEach items="${miuListFilterForm.networkProviderOptions}" var="network">
                                    <form:option value="${network}" selected="${network == miuListFilterForm.selectedNetworkProvider ? 'SELECTED' : ''}" label="${network}"/>
                                </c:forEach>
                            </form:select>
                        </div>
                    </div>
                </div>

                <div class="tableMain left">
                    <c:choose>
                        <c:when test="${not empty miuListFilterForm.selectedManager}">
                            <div class="tableRow">
                                <div class="tableCell">
                                    <form:label cssClass="medium" path="selectedManager">Regional Manager</form:label>
                                    <form:hidden path="selectedManager" />
                                </div>
                                <div class="tableCell">
                                    ${miuListFilterForm.selectedManager}
                                </div>
                            </div>
                        </c:when>
                        <c:when test="${not empty miuListFilterForm.selectedLocation}">
                            <div class="tableRow">
                                <div class="tableCell">
                                    <form:label cssClass="medium" path="selectedManager">State</form:label>
                                    <form:hidden path="selectedLocation" />
                                </div>
                                <div class="tableCell">
                                    ${miuListFilterForm.selectedLocation}
                                </div>
                            </div>
                        </c:when>
                        <c:when test="${empty miuListFilterForm.selectedSite}">
                            <%-- None of the filter is applied--%>
                            <div class="tableRow" title="MIUs will only be shown for the selected Manager">
                                <div class="tableCell">
                                    <form:label cssClass="medium" path="selectedManager">Regional Manager</form:label>
                                </div>
                                <div class="tableCell">
                                    <form:select path="selectedManager" items="${miuListFilterForm.managers}" cssClass="ui-comboBox"/>
                                </div>
                                <div class="tableCell">
                                    <input type="submit" id="filterByManager" value="Filter" class="button"/>
                                </div>
                            </div>
                            <div class="tableRow" title="MIUs will only be shown for the selected State">
                                <div class="tableCell">
                                    <form:label cssClass="medium" path="selectedLocation">State</form:label>
                                </div>
                                <div class="tableCell">
                                    <form:select path="selectedLocation" items="${miuListFilterForm.locations}" cssClass="ui-comboBox"/>
                                </div>
                                <div class="tableCell">
                                    <input type="submit" id="filterByLocation" value="Filter" class="button"/>
                                </div>
                            </div>
                        </c:when>

                    </c:choose>
                    <c:choose>
                        <c:when test="${not empty miuListFilterForm.selectedSite}">
                            <div class="tableRow">
                                <div class="tableCell">
                                    <form:label cssClass="medium" path="selectedSite">Site</form:label>
                                    <form:hidden path="selectedSite" />
                                    <form:hidden path="selectedSiteName" />
                                </div>
                                <div class="tableCell">
                                    ${miuListFilterForm.selectedSiteName} (site ID:${miuListFilterForm.selectedSite})</p>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${not empty miuListFilterForm.siteIds}">
                                <div class="tableRow">
                                    <div class="tableCell">
                                        <form:label cssClass="medium" path="selectedSite">Site ID</form:label>
                                    </div>
                                    <div class="tableCell">
                                        <div id="miuSiteSelect">
                                        <form:select path="selectedSite" cssClass="ui-comboBox">
                                            <form:option value="" label=""/>
                                            <c:forEach items="${miuListFilterForm.siteIds}" var="site">
                                                <form:option value="${site.siteId}" label="${site.siteName} (site ID:${site.siteId})"/>
                                            </c:forEach>
                                        </form:select>
                                        </div>
                                    </div>
                                    <div class="tableCell">
                                        <input type="submit" id="filterBySite" name="filterBySite" value="Filter" class="button"/>
                                    </div>
                                </div>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${not empty miuListFilterForm.selectedManager or not empty miuListFilterForm.selectedLocation or not empty miuListFilterForm.selectedSite}">
                        <div class="tableRow">
                            <div class="tableCell">
                                <c:url var="resetFilterUrl" value="/pages/miu/miulist"/>
                                <a href="${resetFilterUrl}" class="button">Clear</a>
                            </div>
                        </div>
                    </c:if>
                </div>

            </form:form>
        </div>

    </div>

    <div>
        <span>Showing: ${navigationModel.fromRowNumber}-${navigationModel.toRowNumber} of ${totalResults} MIUs. </span>
        <span>Page: </span>

        <c:set var="currentPage" value="${page + 1}"/>
        <c:set var="pageLinkRange" value="${navigationModel.pageLinkRange}"/>

        <c:choose>
            <c:when test="${currentPage > 1}">
                <c:url var="firstPage" value="${navigationModel.firstPageHref}"/>
                <c:url var="previousPage" value="${navigationModel.previousPageHref}"/>
                <span class="pageLink"><a href="${firstPage}">First</a></span>
                <span class="pageLink"><a href="${previousPage}">Prev</a></span>
            </c:when>
            <c:otherwise>
                <span class="pageLink">First</span>
                <span class="pageLink">Prev</span>
            </c:otherwise>
        </c:choose>

        <c:forEach var="pageLink" items="${navigationModel.previousPageHrefs}">
            <c:choose>
                <c:when test="${pageLink.pageNumber ne 0}">
                    <c:url var="href" value="${pageLink.href}"/>
                    <span class="pageLink"><a href="${href}">${pageLink.pageNumber}</a></span>
                </c:when>
                <c:otherwise>
                    <span class="pageLink">&nbsp;</span>
                </c:otherwise>
            </c:choose>
        </c:forEach>

        <span class="currentPage">${currentPage}</span>

        <c:forEach var="pageLink" items="${navigationModel.nextPageHrefs}">
            <c:choose>
                <c:when test="${pageLink.pageNumber ne 0}">
                    <c:url var="href" value="${pageLink.href}"/>
                    <span class="pageLink"><a href="${href}">${pageLink.pageNumber}</a></span>
                </c:when>
                <c:otherwise>
                    <span class="pageLink">&nbsp;</span>
                </c:otherwise>
            </c:choose>
        </c:forEach>

        <c:choose>
            <c:when test="${currentPage < navigationModel.numberOfPages}">
                <c:url var="nextPage" value="${navigationModel.nextPageHref}"/>
                <c:url var="lastPage" value="${navigationModel.lastPageHref}"/>
                <span class="pageLink"><a href="${nextPage}">Next</a></span>
                <span class="pageLink"><a href="${lastPage}">Last</a></span>
            </c:when>
            <c:otherwise>
                <span class="pageLink">Next</span>
                <span class="pageLink">Last</span>
            </c:otherwise>
        </c:choose>
    </div>

    <div class="group">

        <table id="table-miu-list">
            <thead>
            <tr>
                <th>MIU ID</th>
                <th>Type</th>
                <th>Site</th>
                <th>Last heard time</th>
                <th><abbr about="'Active' refers to whether this MIU is set active or inactive in N_SIGHT">Active</abbr></th>
                <th><span title="The CMIU recording interval in minutes">Recording interval (mins)</span></th>
                <th><span title="The CMIU reporting interval in hours">Reporting interval (hours)</span></th>
                <th>Reported CMIU Mode</th>
                <th>Network</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${miuList}" var="miu">
                <tr>
                    <td class="small-values" title="The ID for the MIU">
                        <mdce:miuLink muId="${miu.miuId}" />
                    </td>
                    <c:url var="url" value="/pages/miu/miulist?selectedSite=${miu.siteId}"/>
                    <td class="small-values" title="The MIU type">
                        ${miu.type}
                    </td>
                    <td title="Show the details page for this site">
                        <a href="${url}">${miu.siteDescription}</a>
                    </td>
                    <td class="small-values" title="The last time this MIU was heard by Galileo">
                        ${mdce:formatInstant(miu.lastHeardTimeAsInstant)}
                    </td>
                    <td class="small-values" title="Indicates whether this meter is billable">
                        ${miu.meterActive}
                    </td>
                    <td class="small-values">
                        <c:choose>
                            <c:when test="${miu.recordingIntervalMins ne 0}">
                                ${miu.recordingIntervalMins}
                            </c:when>
                        </c:choose>
                    </td>
                    <td class="small-values">
                        <c:choose>
                            <c:when test="${miu.reportingIntervalMins ne 0}">
                                ${String.format("%.2f", miu.reportingIntervalMins / 60)}
                            </c:when>
                        </c:choose>
                    </td>
                    <td class="small-values">
                            ${miu.cmiuModeName}
                    </td>
                    <td class="small-values">
                            ${miu.networkProvider}
                    </td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
    </div>
</section>


</body>
</html>

