<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2017 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ page session="false" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/user.css"/>
    <mdce:css href="css/glyphicons.css"/>
    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <title>${mdce:t("Title.AccessDenied")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

    <%--Need this for Spring security CSRF protection --%>
    <sec:csrfMetaTags/>
</head>
<body>

<mdce:header/>
<h1 class="page-heading">Access Denied </h1>
<section class="container">

    <div class="group">
        <div>
            <c:if test="${empty userName}">
                <%--<p>Username: <em>${userName}</em>,</p>--%>
                <p id="login-required">
                    You are required to login before you can access this page.
                </p>
            </c:if>

            <c:if test="${not empty userName}">
                <%--<p>Username: <em>${userName}</em>,</p>--%>
                <p id="access-not-allowed">
                    Access to the page is not allowed. Please ensure you have the correct permission.
                </p>
            </c:if>
        </div>

        <div>
            <c:if test="${not empty userName}">
                <p>Username: ${userName}</p>
            </c:if>
            <p>MDCE access: ${msocModeMessage}</p>
        </div>
    </div>

</section>
</body>
</html>



