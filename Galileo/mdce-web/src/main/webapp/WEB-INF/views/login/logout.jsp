<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ page session="false" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/user.css"/>
    <mdce:css href="css/glyphicons.css"/>
    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <title>${mdce:t("Title.LogOut")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

    <%--Need this for Spring security CSRF protection --%>
    <sec:csrfMetaTags/>
</head>
<body>

<mdce:header/>
<h1 class="page-heading">Logout </h1>
<section class="container">

    <%--<h2>${username }</h2>--%>

    <h2>Confirm Log Out</h2>


    <c:url var="homeUrl" value="/"/>
    <c:url var="loginPageUrl" value="/pages/user/login"/>
    <c:url var="logoutUrl" value="/pages/user/logout"/>

    <sec:authorize access="isAnonymous()">
        Hi guest, you are not <a href="${loginPageUrl}">LOG IN</a>
    </sec:authorize>

    <sec:authorize access="isAuthenticated()">

        <div>
            User: <sec:authentication property="principal"/> is logged in.

            <form action="${logoutUrl}" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <input type="submit" class="button"  value="Log out"/>
            </form>
        </div>
    </sec:authorize>


</section>
</body>
</html>



