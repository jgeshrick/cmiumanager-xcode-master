<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ page session="false" %>

<html>
<head>
    <c:url var="resourcesUrl" value="/resources"/>
    <mdce:favicon/>
    <mdce:css href="css/main.css"/>
    <mdce:css href="css/login.css"/>
    <mdce:css href="css/glyphicons.css"/>
    <mdce:js src="lib/jquery-1.11.2.min.js"/>
    <title>${mdce:t("Title.LogIn")} | ${mdce:t("Title.HtmlHeadTitleSuffix")}</title>

    <%--Need this for Spring security CSRF protection --%>
    <sec:csrfMetaTags/>
</head>
<body>

<mdce:header/>
<h1 class="page-heading">Login </h1>
<section class="container">
    <div class="group">

        <%--<h2>${username }</h2>--%>

        <h2>Log in</h2>

        <c:set var="loginUrl">
            <c:url value="/pages/user/login"/>
        </c:set>

        <form method="POST" id="login-form" class="input-form" action="${loginUrl}" autocomplete="off">

            <c:if test="${not empty errorMessage}">
                <div class="error">
                    <p>Error: ${errorMessage}</p>
                </div>
            </c:if>

            <div class="field">
                <label for="login">User name</label>
                <input type="text" id="username" name="username" placeholder="username" autofocus="autofocus"
                       value="${lastUsername}"/>
            </div>

            <div class="field">
                <label for="password">Password</label>
                <input type="password" id="extra-password-field-to-prevent-browser-saving-password" hidden="true"/>
                <input type="password" id="password" name="password" placeholder="password"/>
            </div>

            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

            <div class="submits right">
                <input type="submit" id="login" class="button" name="login" value="Login"/>
            </div>

            <div class="field">
                <c:url var="resetPasswordUrl" value="/pages/recovery/password"/>
                <a id="reset-forgotten-password" href="${resetPasswordUrl}">Reset forgotten password</a>
            </div>

        </form>
    </div>


    <c:url var="homeUrl" value="/"/>
    <c:url var="loginPageUrl" value="/pages/user/login"/>
    <c:url var="logoutUrl" value="/pages/user/logout"/>

    <sec:authorize access="isAuthenticated()">
        <div class="group">

                <%--
                    <sec:authorize access="isAnonymous()">
                        Hi guest, <a href="${loginPageUrl}">LOG IN</a>
                    </sec:authorize>
                --%>


            <div>
                <h2>Logout</h2>

                <div>
                    User: <sec:authentication property="principal"/> is already login.
                </div>

                <c:url var="logoutUrl" value="/pages/user/logout"/>
                <form action="${logoutUrl}" method="post">
                    <div class="right">
                        <input type="submit" class="button" value="Log out"/>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </div>
                </form>
            </div>
        </div>
    </sec:authorize>
</section>
</body>
</html>



