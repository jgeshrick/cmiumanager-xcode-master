<%--
/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

// Use this tag to refer to a CSS file within the resources folder.

// It adds a cache-buster parameter "?app-version=1.2.3.4" to ensure that the new CSS is retrieved by
// the browser after upgrade

--%>
<%@ attribute name="src" required="true" type="java.lang.String"%>
<%@ attribute name="defer" required="false" type="java.lang.Boolean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce"%>
<c:url var="jsPath" value="/resources/${src}">
    <c:param name="app-version" value="${mdce:applicationVersion()}" />
</c:url>
<c:choose>
    <c:when test="${defer}">
        <script src="${jsPath}" defer></script>
    </c:when>
    <c:otherwise>
        <script src="${jsPath}"></script>
    </c:otherwise>
</c:choose>
