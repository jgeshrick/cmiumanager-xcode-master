<%--
  ~  Neptune Technology Group
  ~  Copyright 2015 as unpublished work.
  ~  All rights reserve
  ~
  ~  The information contained herein is confidential
  ~  property of Neptune Technology Group. The use, copying, transfer
  ~  or disclosure of such information is prohibited except by express
  ~  written agreement with Neptune Technology Group.
  ~
  --%>
<%@ attribute name="href" required="true" type="java.lang.String"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce"%>
<c:url var="lessPath" value="/resources/${href}">
    <c:param name="app-version" value="${mdce:applicationVersion()}" />
</c:url>
<link href="${lessPath}" rel="stylesheet" media="screen"/>