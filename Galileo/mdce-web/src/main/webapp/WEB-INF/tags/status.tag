<%--
  ~  Neptune Technology Group
  ~  Copyright 2015 as unpublished work.
  ~  
  ~  All rights reserved
  ~  The information contained herein is confidential
  ~  property of Neptune Technology Group. The use, copying, transfer
  ~  or disclosure of such information is prohibited except by express
  ~  written agreement with Neptune Technology Group.
  ~
  --%>

<%@ attribute name="account" required="true" type="java.lang.String" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>

<div class="content">

    <div id="daily-msr-chart" class="status-chart">Loading...</div>
    <div id="monthly-msr-chart" class="status-chart">Loading...</div>

    <div>
        <p>Date: <input type="text" id="datepicker"></p>
    </div>
</div>

<c:url var="dailyMsrDataUrl" value="/chartdata/msr"/>
<c:url var="monthlyMsrDataUrl" value="/chartdata/msr"/>

<script type="text/javascript">

    AmCharts.ready(function () {

        ajaxPieChart("<c:out value="${dailyMsrDataUrl}" />", "daily-msr-chart");
        ajaxPieChart("<c:out value="${monthlyMsrDataUrl}" />", "monthly-msr-chart");

    });


</script>

<script>
    $(function() {
        $( "#datepicker" ).datepicker();
    });
</script>



