<%--
/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

// favicon

--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce"%>
<c:url var="faviconUrl" value="/resources/images/neptune.ico" />
<link rel="icon" href="${faviconUrl}"/>
