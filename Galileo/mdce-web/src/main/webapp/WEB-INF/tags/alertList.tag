<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ attribute name="formModelAttributeName" required="true" type="java.lang.String" %>
<%@ attribute name="alertsForm" required="true" type="com.neptunetg.mdce.web.gui.alert.AlertsForm" %>
<%@ attribute name="name" required="true" type="java.lang.String" %>
<%@ attribute name="changeTicketButtonText" required="false" type="java.lang.String" %>
<%@ attribute name="selectAllButton" required="false" type="java.lang.String" %>
<%@ attribute name="clearAlertButtonText" required="false" type="java.lang.String" %>
<%@ attribute name="postUrl" required="false" type="java.lang.String" %>
<%@ attribute name="filterString" required="true" type="java.lang.String" %>
<%@ attribute name="isSelectable" required="true" type="java.lang.Boolean" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<c:url var="alertView" value="/pages/alert/view"/>


<form:form modelAttribute="${formModelAttributeName}" method="post" action="${postUrl}">
    <form:hidden path="filterString" value="${filterString}"/>
    <form:hidden path="ticketId"/>
    <form:hidden path="selectedAlertIds"/>
    <input type="hidden" name="batchClear" value=""/>
    <input type="hidden" name="batchTicket" value=""/>
</form:form>
    <div class="group">
        <h2>${name} (${alertsForm.alertDetailsList.size()})</h2>

        <c:choose>
            <c:when test="${alertsForm.alertDetailsList.size() > 0}">
                <%--<input name="ticketId" type="hidden" value="${alertsForm.ticketId}">--%>

                <div class="table-action-buttons">
                    <c:if test="${not empty selectAllButton}">
                        <a class="button right select-all-button">${selectAllButton}</a>
                    </c:if>

                    <c:if test="${not empty clearAlertButtonText}">
                        <a class="button right clear-alert-button">${clearAlertButtonText}</a>
                    </c:if>

                    <c:if test="${not empty changeTicketButtonText}">
                        <a class="button right change-ticket-button">${changeTicketButtonText}</a>
                    </c:if>


                </div>
                <c:choose>
                    <c:when test="${isSelectable}">
                        <table class="alert-list selectable-alert" data-form-id="${formModelAttributeName}">
                    </c:when>
                    <c:otherwise>
                        <table class="alert-list" data-form-id="${formModelAttributeName}">
                    </c:otherwise>
                </c:choose>

                    <thead>
                    <tr>
                        <th>Alert Source</th>
                        <th>Ticket ID</th>
                        <th>Level</th>
                        <th>Initial Date</th>
                        <th>Latest Update</th>
                        <th>Latest Message</th>
                        <th>View</th>
                        <c:if test="${isSelectable}">
                            <th>Select</th>
                        </c:if>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${alertsForm.alertDetailsList}" var="alert" varStatus="count">
                        <tr class="list-item">
                            <td class="alert-list-source" title="Where the alert originated from">
                                ${alert.alertSource}
                            </td>
                            <td class="alert-list-ticket" title="The ticket ID used to track this issue">
                                ${alert.alertTicketId}
                            </td>
                            <c:choose>
                                <c:when test="${alert.alertLevel=='ERROR'}">
                                    <td class="alert-list-level-error" title="The severity of the alert">
                                        ${alert.alertLevel}
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td class="alert-list-level" title="The severity of the alert">
                                        ${alert.alertLevel}
                                    </td>
                                </c:otherwise>
                            </c:choose>
                            <td class="alert-list-initial-date" title="When the alert was created">
                                    ${mdce:formatDateTime(alert.alertCreationDate)}
                            </td>
                            <td class="alert-list-update" title="The last time the alert state was updated">
                                    ${mdce:formatDateTime(alert.latestAlertDate)}
                            </td>
                            <td class="alert-list-message" title="The message from the alert source">
                                ${alert.recentMessage}
                            </td>
                            <td class="list-content-note" title="View the alert details">
                                <a href="${alertView}/${alert.alertId}" title="View / Edit Alert">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </td>
                            <c:if test="${isSelectable}">
                                <td>
                                    <input type="checkbox" data-alert-id="${alert.alertId}" class="alert-selected-checkbox"/>
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

                <div class="table-action-buttons">
                    <c:if test="${not empty selectAllButton}">
                        <a class="button right select-all-button">${selectAllButton}</a>
                    </c:if>
                    <c:if test="${not empty clearAlertButtonText}">
                        <a class="button right clear-alert-button">${clearAlertButtonText}</a>
                    </c:if>
                    <c:if test="${not empty changeTicketButtonText}">
                        <a class="button right change-ticket-button">${changeTicketButtonText}</a>
                    </c:if>
                </div>

            </c:when>
            <c:otherwise>
                <div>No alert</div>
            </c:otherwise>
        </c:choose>
    </div>
