<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:url var="homeUrl" value="/pages/home"/>
<c:url var="bgUrl" value="/resources/images/bkgd_dark_stripes.jpg" />
<div class="top-bar" style="background-image: url('${bgUrl}')">
    <div class="status"> <%-- Note, due to a bug in Chrome, this float-right element must come first to avoid it moving down to a new line --%>

        <sec:authorize access="isAuthenticated()">
            <c:set var="username">
                <sec:authentication property="principal"/>
            </c:set>

            <span class="login-user">
                <span class="glyphicon glyphicon-user"></span>
                ${username}
            </span>

        </sec:authorize>

        <span class="back-end-status">
            <c:set var="statuses" value="${mdce:getBackEndStatus()}"/>
            <c:forEach var="server" items="${statuses.serverStatuses}">
                <c:choose>
                    <c:when test="${server.status.error}">
                        <c:set var="spanClass" value="error"/>
                        <c:set var="title" value="${server.serverDetails.name} : ${server.status.errorMessage}"/>
                        <c:set var="glyph" value="X"/>
                    </c:when>
                    <c:when test="${server.status.warning}">
                        <c:set var="spanClass" value="warning"/>
                        <c:set var="title" value="${server.serverDetails.name} : ${server.status.errorMessage}"/>
                        <c:set var="glyph" value="!"/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="spanClass" value="ok"/>
                        <c:set var="title" value="${server.serverDetails.name} : okay"/>
                        <c:set var="glyph" value="&#x2713;"/>
                    </c:otherwise>
                </c:choose>
                <span class="${spanClass}" title="${title}">(${glyph})</span>
            </c:forEach>
        </span>

        <span class="application-version">${mdce:applicationVersion()}</span>

    </div>

    <span class="menu-icon" title="${mdce:t('Header.Menu')}">&#9776;<div class="popup-container">
            <div class="popup">
                <ul>
                    <c:url var="url" value="/pages/home" />
                    <li><a href="${url}">${mdce:t('Title.Home')}</a></li>

                    <c:url var="url" value="/pages/miu/miulist" />
                    <li><a href="${url}">${mdce:t('Title.Mius')}</a></li>

                    <c:url var="url" value="/pages/gateway/gatewaylist" />
                    <li><a href="${url}">${mdce:t('Title.Gateways')}</a></li>

                    <c:url var="url" value="/pages/analysis/timing" />
                    <li><a href="${url}">C-TAT</a></li>

                    <c:url var="url" value="/pages/sim/simlist" />
                    <li><a href="${url}">${mdce:t('Title.Sims')}</a></li>

                    <c:url var="url" value="/pages/alert/list" />
                    <li><a href="${url}">${mdce:t('Title.Alerts')}</a></li>

                    <c:url var="url" value="/pages/config" />
                    <li><a href="${url}">${mdce:t('Title.C2')}</a></li>

                    <c:url var="url" value="/pages/audit/" />
                    <li><a href="${url}">${mdce:t('Title.AuditLog')}</a></li>

                    <c:url var="url" value="/pages/user/list" />
                    <li><a href="${url}">${mdce:t('Title.UserManagement')}</a></li>

                    <c:url var="url" value="/pages/settings" />
                    <li><a href="${url}">${mdce:t('Title.Settings')}</a></li>

                    <c:url var="url" value="/pages/diagnostics/diagnosticTools" />
                    <li><a href="${url}">${mdce:t('Title.DiagnosticTools')}</a></li>

                    <%-- login or logout? --%>
                    <sec:authentication var="user" property="principal" />
                    <c:choose>
                        <c:when test="${user eq 'anonymousUser'}" >
                            <c:url var="url" value="/pages/user/login" />
                            <li><a href="${url}">Log in</a></li>
                        </c:when>
                        <c:otherwise>
                            <c:url var="url" value="/pages/user/logout" />
                            <li><a href="${url}">Log out</a></li>
                        </c:otherwise>
                    </c:choose>

                </ul>
            </div>
        </div>
    </span>
    <c:url var="logoUrl" value="/resources/images/logo_61x59.png" />
    <span class="application-header"><a href="${homeUrl}"><img src="${logoUrl}" width="30" height="30" border="0" alt="${mdce:t("Header.ApplicationTitleShort")} &ndash; ${fn:toUpperCase(mdce:environmentName())}" />${mdce:t("Header.ApplicationTitleShort")} &ndash; ${fn:toUpperCase(mdce:environmentName())}</a></span>
</div>