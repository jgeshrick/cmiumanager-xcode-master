<%--
  ~  **************************************************************************
  ~
  ~      Neptune Technology Group
  ~      Copyright 2016 as unpublished work.
  ~      All rights reserved
  ~
  ~      The information contained herein is confidential
  ~      property of Neptune Technology Group. The use, copying, transfer
  ~      or disclosure of such information is prohibited except by express
  ~      written agreement with Neptune Technology Group.
  ~
  ~  **************************************************************************
  --%><%@ attribute name="gatewayId" required="true" type="java.lang.String"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><c:url var="gatewayDetailsUrl" value="/pages/gateway/gatewaydetail/${gatewayId}"/><a href="${gatewayDetailsUrl}">${gatewayId}</a>