<%--
  ~  Neptune Technology Group
  ~  Copyright 2015 as unpublished work.
  ~  
  ~  All rights reserved
  ~  The information contained herein is confidential
  ~  property of Neptune Technology Group. The use, copying, transfer
  ~  or disclosure of such information is prohibited except by express
  ~  written agreement with Neptune Technology Group.
  ~
  --%>

<%@ attribute name="account" required="true" type="java.lang.String" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>

<div id="alert-tabs">
    <ul>
        <li><a href="#alert-tab-1">Active</a></li>
        <li><a href="#alert-tab-2">Watching</a></li>
    </ul>
    <div id="alert-tab-1">
        <div>
            <h3>Not heard from CMIU <span class="glyphicon glyphicon-envelope"></span></h3>

            <ul>
                <li>CMIU 123456789 Acccount 12345 <span class="glyphicon glyphicon-envelope"></span>
                </li>
                <li>CMIU 123456789 Acccount 12345 <span class="glyphicon glyphicon-envelope"></span></li>
            </ul>
        </div>
        <div>
            <h3>Changed Towers <span class="glyphicon glyphicon-envelope"></span></h3>
            <ul>
                <li>CMIU 123456789 Acccount 12345 <span class="glyphicon glyphicon-envelope"></span></li>
                <li>CMIU 123456789 Acccount 12345 <span class="glyphicon glyphicon-envelope"></span></li>
            </ul>
        </div>
    </div>
    <div id="alert-tab-2">
        <div>
            <h3>Not heard from CMIU <span class="glyphicon glyphicon-envelope"></span></h3>
            <ul>
                <li>CMIU 123456789 Acccount 12345 <span class="glyphicon glyphicon-envelope"></span></li>
                <li>CMIU 123456789 Acccount 12345 <span class="glyphicon glyphicon-envelope"></span></li>
            </ul>
        </div>
    </div>

</div>

<script>
    $(function () {
        $("#alert-tabs").tabs();
    });
</script>
