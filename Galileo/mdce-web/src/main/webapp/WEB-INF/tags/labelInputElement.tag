<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ attribute name="path" required="true" type="java.lang.String" %>
<%@ attribute name="label" required="true" type="java.lang.String" %>
<%@ attribute name="inputCssClass" required="false" type="java.lang.String" %>
<%@ attribute name="readonly" required="false" type="java.lang.Boolean" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${empty readonly}">
    <c:set var="readonly" value="false"/>
</c:if>

<spring:bind path="${path}">
  <div class="${status.error ? 'field has-error' : 'field'}">
    <form:label path="${path}">${label}</form:label>
    <form:input path="${path}" cssClass="${inputCssClass}" readonly="${readonly}"/>
    <form:errors path="${path}" cssClass="error-text"/>
  </div>
</spring:bind>
