<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ attribute name="miuInputList" required="true" type="java.util.List" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="group" id="miu-dynamic-list">
    <h2>List of L900s to Update</h2>

    <div>
        <table>
            <thead>
            <tr>
                <th>L900</th>
                <th>Description</th>
                <th></th>
            </tr>
            </thead>

            <c:forEach items="${miuInputList}" varStatus="vs">
                <c:set var="hasError">
                    <form:errors path="miuInputList[${vs.index}].miuId"/>
                </c:set>

                <tr class="list-item">
                    <td class="miu-list-id ${not empty hasError?'error-invalid-miu-entry':''}">
                        <form:input path="miuInputList[${vs.index}].miuId" placeholder="Enter L900 ID" cssClass="cmiu-id" />
                    </td>
                    <td class="miu-list-description">
                        <span id="metadata${vs.index}">${miuInputList[vs.index].metaData}</span>
                    </td>
                    <td class="miu-list-row-action">
                        <a href="#" class="list-remove" title="Remove entry">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div>
        <a href="#" class="list-add"><span class="button">+ Add L900</span></a>
        <%--<input type="submit" name="appendMiuList" value="+ Batch Add MIU" class="button"/>--%>
    </div>

</div>