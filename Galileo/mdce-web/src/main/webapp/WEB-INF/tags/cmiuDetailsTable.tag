<%--
  ~  Neptune Technology Group
  ~  Copyright 2015 as unpublished work.
  ~  
  ~  All rights reserved
  ~  The information contained herein is confidential
  ~  property of Neptune Technology Group. The use, copying, transfer
  ~  or disclosure of such information is prohibited except by express
  ~  written agreement with Neptune Technology Group.
  ~
  --%>

<%@ attribute name="account" required="true" type="java.lang.String"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://mdce.neptunetg.com/taglib" prefix="mdce"%>

<label>Account: </label>
<select id="account">
    <option value="all">ALL</option>
    <option value="account-1">Account 1</option>
    <option value="account-2">Account 2</option>
    <option value="account-3">Account 3</option>
</select>

<div id="error-display" class="error"></div>

<table id="cmiu_detail_table">
    <thead>
    <tr>
        <th> CMIU ID </th>
        <th> Reading </th>
        <th> Status </th>
        <th> SIM Id </th>
        <th> Freq Band </th>
        <th> Roaming </th>
        <th> Carrier Info </th>
        <th> Registering </th>
        <th> Initializing</th>
        <th> Tower ID </th>
        <th> Network </th>
        <th> Local code </th>
        <th> BSIC </th>
        <th> BCC </th>
        <th> Rx Level </th>
        <th> Rx dBm </th>
    </tr>
    </thead>

</table>

<div id="cmiu-details-debug"> </div>

<c:url var="cmiuDetailUrl" value="/cmiu/details" />

<script type="text/javascript">


    $( document ).ready(function() {
        getCmiuDetailsFromAccount("ALL");
    });


    function populateTable(results) {

        var theader = "<thead><tr><th> CMIU ID </th><th> Reading </th><th> Status </th><th> SIM Id </th>"+
                "<th> Freq Band </th><th> Roaming </th><th> Carrier Info </th><th> Registering </th><th> Initializing</th>" +
                "<th> Tower ID </th><th> Network </th><th> Local code </th><th> BSIC </th><th> BCC </th>" +
                "<th> Rx Level </th><th> Rx dBm </th></tr></thead>";

        var trHTML = '';
        $.each(results, function (i, item) {
            trHTML += '<tr>' +
                    '<td>' + item.miuId + '</td>' +
                    '<td>' + item.lastHeardTime + '</td><td>' + item.reading + '</td>' +
                    '<td>' + item.simId + '</td><td>' + item.freqBand + '</td><td>' + item.roaming + '</td>' +
                    '<td>' + item.carrierInfo + '</td>' +
                    '<td>' + item.registering + '</td><td>' + item.initializing + '</td><td>' + item.towerId + '</td>' +
                    '<td>' + item.network + '</td><td>' + item.localCode + '</td><td>' + item.bsic + '</td>' +
                    '<td>' + item.bcc + '</td><td>' + item.rxLevel + '</td><td>' + item.rxDBm + '</td>' +
                    '</tr>';

        });

        $('#cmiu_detail_table').empty().append(theader).append(trHTML);
        $('#error-display').hide();
    }

    function displayError(errorMsg) {
        $('#cmiu_detail_table').empty();
        $('#error-display').html('<p>' + errorMsg + '</p>').show();
    }

    function getJsonPretty(jsonObj){
        return '<p>' +
                JSON.stringify(jsonObj, null, '\t')
        '</p>';
    }

    function getCmiuDetailsFromAccount(account) {
        $.ajax({
            url: '${cmiuDetailUrl}',
            type: 'GET',
            data: {
                account: account
            },
            success: function (response) {
                $('#cmiu-details-debug').append(getJsonPretty(response)).hide();    //todo for debug, to remove

                if (response.error != null ) {
                    displayError(response.error);
                }
                else {
                    populateTable(response.results);
                }
            }
        });

    }


    $("#account").change(function(){

        var str = "";
        $( "select option:selected" ).each(function() {
            str += $( this ).text() + " ";
        });

        getCmiuDetailsFromAccount(str);

    });

</script>
