/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.command;

import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest;
import com.neptunetg.mdce.web.gui.config.GenericCommandForm;
import com.neptunetg.mdce.web.gui.config.ImageUpdateForm;
import com.neptunetg.mdce.web.gui.config.MiuInput;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Form validator for validing CMIU entries for send command
 */
public class L900InputListFormValidator implements Validator
{
    @Override
    public boolean supports(Class<?> clazz)
    {
        return ImageUpdateForm.class.equals(clazz) || GenericCommandForm.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors)
    {
        List<MiuInput> l900InputList = Collections.emptyList();

        GenericCommandForm commandForm;

        if (o instanceof GenericCommandForm)
        {
            commandForm = (GenericCommandForm) o;
            l900InputList = ((GenericCommandForm) o).getMiuInputList();
        }
        else
        {
            errors.reject("genericform.invalid.format", "Invalid format of form object");
            return;
        }

        //All L900 entries regardless of whether they are in the correct format
        List<String> l900Entries = l900InputList.stream()
                .map(MiuInput::getMiuId)
                .collect(Collectors.toList());

        List<String> validL900List = l900Entries
                .stream()
                .filter(s -> StringUtils.hasText(s))
                .collect(Collectors.toList());

        if (validL900List.isEmpty())
        {
            //no cmiu entries entered
            errors.rejectValue("miuInputList", "l900.not.defined", "No valid L900 is entered.");
        }
        else
        {
            boolean isUndefinedError = false;
            boolean hasFormatError = false;
            boolean hasRangeError = false;

            for (int i = 0; i < l900InputList.size(); i++)
            {
                MiuInput l900Description = l900InputList.get(i);

                if (StringUtils.hasText(l900Description.getMiuId())) //allowed to be empty as long as at least one is provided
                {
                    if (!isNumeric(l900Description.getMiuId()))
                    {
                        errors.rejectValue("miuInputList[" + i + "].miuId", "l900.invalid.format", "L900 is not a number");
                        hasFormatError = true;
                    } else if (!isValidL900IdRange(Long.valueOf(l900Description.getMiuId())))
                    {
                        errors.rejectValue("miuInputList[" + i + "].miuId", "l900.invalid.range", "L900 value is not within the valid range");
                        hasRangeError = true;
                    }
                }
            }

            if (isUndefinedError)
            {
                errors.rejectValue("miuInputList", "l900.not.defined", "No valid L900 is entered.");
            }

            if (hasFormatError)
            {
                errors.rejectValue("miuInputList", "l900.invalid.format", "L900 is not a number");
            }

            if (hasRangeError)
            {
                errors.rejectValue("miuInputList", "l900.invalid.range", "L900 value is not within the valid range");
            }
        }

        if (CommandRequest.CommandTypeL900.COMMAND_TIME_OFFSET.equals(commandForm.getCommandTypeL900()))
        {
            if (!isNumeric(commandForm.getTimeOffset()))
            {
                errors.rejectValue("timeOffset", "timeOffset.invalid.format", "Time offset is not a valid number");
            }
        }


    }



    /**
     * Verify that the L900 ID is valid range
     *
     * @param id containing the cmiuid
     * @return true if valid
     */
    private static boolean isValidL900IdRange(Long id)
    {
        return 700000000 <= id && id <= 800000000;
    }

    private static boolean isNumeric(String id)
    {
        try
        {
            long l = Long.parseLong(id);
            return true;
        }
        catch (NumberFormatException ex)
        {
            return false;
        }
    }
}
