/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public final class FormatDateTime
{
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");
    private final static DateTimeFormatter INSTANT_TO_STRING_FORMATTER =
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z").withZone(ZoneId.of("US/Central"));

    //Prevent this class to be instantiated
    private FormatDateTime(){}

    /**
     * Static for use in taglib
     * @return Server statuses
     */
    public static String formatDateToCentralTime(ZonedDateTime datetime)
    {
        if (datetime == null)
        {
            return null;
        }
        ZonedDateTime newDateTime = datetime.withZoneSameInstant(ZoneId.of("US/Central"));
        return newDateTime.format(DATE_TIME_FORMATTER);
    }

    public static String formatInstantToCentralTime(Instant datetime)
    {
        if (datetime == null)
        {
            return null;
        }
        return INSTANT_TO_STRING_FORMATTER.format(datetime);
    }
}
