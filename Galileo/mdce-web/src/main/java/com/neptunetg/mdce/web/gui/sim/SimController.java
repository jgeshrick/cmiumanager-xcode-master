/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.sim;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.sim.model.SimDisplayDetails;
import com.neptunetg.mdce.common.internal.sim.model.SimLifecycleState;
import com.neptunetg.mdce.common.internal.sim.service.SimService;
import com.neptunetg.mdce.common.internal.sim.model.SimDisplayDetailsPagedResult;
import com.neptunetg.mdce.common.internal.site.service.RefService;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import com.neptunetg.mdce.web.gui.audit.AuditLogsFilterForm;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;

/**
 * For administrating sim. All url in this page is accessible only to login with MsocAdmin authority
 */

@Controller
public class SimController
{
    private static final Logger logger = LoggerFactory.getLogger(SimController.class);

    private static final String TOTAL_RESULTS = "totalResults";
    private static final String PAGE = "page";
    private static final String SIM_LIST = "simList";
    private static final String NETWORK = "network";
    private static final String DEBUG_URL = "debugUrl";
    private static final String SIM_LIST_URL = "/pages/sim/simlist";

    private static final String DB_NULL = "-";

    private static final String DEFAULT_SELECTION = "";

    private final BackEndManager backEndManager;

    private final String envName;

    @Autowired
    public SimController(BackEndManager backEndManager, @Qualifier("envProperties") Properties envProperties) throws InternalApiException
    {
        this.backEndManager = backEndManager;
        this.envName = envProperties.getProperty("env.name");

    }

    @ModelAttribute("auditLogFilterForm")
    private AuditLogsFilterForm getAuditLogsFilterForm()
    {
        return new AuditLogsFilterForm();
    }

    @ModelAttribute("miuListFilterForm")
    private SimListFilterForm getSimListFilterForm() throws InternalApiException
    {
        return new SimListFilterForm();
    }

    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping(value = SIM_LIST_URL, method = RequestMethod.GET)
    public String listSimDetails(
            @ModelAttribute("simListFilterForm") SimListFilterForm simListFilterForm,
            ModelMap model
    ) throws Exception
    {
        final HashMap<String, String> options = new HashMap<>();

        addKeyValueToMapIfNotNull(NETWORK, simListFilterForm.getSelectedNetwork(), options);
        addKeyValueToMapIfNotNull(SimService.SIM_LIFECYCLE_STATE, simListFilterForm.getSelectedLifecycleState(), options);
        addKeyValueToMapIfNotNull(SimService.ICCID, simListFilterForm.getSelectedIccid(), options);

        int newPage = simListFilterForm.getPageNumberForPagedResult();

        addKeyValueToMapIfNotNull(PAGE, String.valueOf(newPage), options);

        SimDisplayDetailsPagedResult simDetails = getSimService().getSimList(options);

        model.addAttribute(SIM_LIST, simDetails.getResults());
        model.addAttribute(TOTAL_RESULTS, simDetails.getTotalResults());

        model.addAttribute(DEBUG_URL, getMdceIntegrationDebugHomepagePageUrl(this.envName));

        final RefService refService = getRefDataService();
        final SimService simService = getSimService();

        simListFilterForm.setNetworkList(setupSelectionsWithFirstEntryAsUndefined((refService.getSimNetworks(options))));
        simListFilterForm.setLifeCycleStateList(setupSelectionsWithFirstEntryAsUndefined((refService.getSimLifeCycleStates(options))));
        simListFilterForm.setIccidList(setupSelectionsWithFirstEntryAsUndefined(simService.getSimIccids(options)));

        model.addAttribute("simListFilterForm", simListFilterForm);
        model.addAllAttributes(options);

        return "sim/simlist.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping(value = "/pages/sim/simlist/{iccid}/changestate", method = RequestMethod.GET)
    public String getChangeSimState(
            @PathVariable("iccid") String iccid,
            @RequestParam(value = "page", required = false) String pageFilter,
            @RequestParam(value = "network", required = false) String networkFilter,
            @RequestParam(value = "state", required = false) String stateFilter,
            ModelMap model) throws Exception
    {
        String simListUrl = SIM_LIST_URL + getSimListQueryParameters(pageFilter, networkFilter, stateFilter);

        SimDisplayDetails simDetails = getSimService().getSimDetails(new HashMap<>(), iccid);

        SimLifecycleState simLifecycleState = SimLifecycleState.fromString(simDetails.getState());
        ChangeSimStateForm changeSimStateForm = new ChangeSimStateForm(
                simDetails.getIccid(), simLifecycleState, pageFilter, networkFilter, stateFilter, simListUrl);

        model.addAttribute("changeSimStateForm", changeSimStateForm);

        return "sim/changestate.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "/pages/sim/simlist/{iccid}/changestate", method = RequestMethod.POST)
    public String postChangeSimState(
            @ModelAttribute("changeSimStateForm") ChangeSimStateForm changeSimStateForm,
            @PathVariable("iccid") String iccid,
            BindingResult result,
            ModelMap model) throws Exception
    {
        if (result.hasErrors())
        {
            return "sim/changestate.jsp";
        }
        else
        {
            Map<String, String> options = new HashMap<>();
            options.put(SimService.ICCID, iccid);
            options.put(SimService.SIM_LIFECYCLE_STATE, changeSimStateForm.getSelectedSimState().getStringValue());
            getSimService().updateSimLifecycleState(options);

            model.addAttribute("changeSimStateForm", changeSimStateForm);

            return "redirect:" + SIM_LIST_URL;
        }
    }

    private static String getSimListQueryParameters(String pageNumber, String networkFilter, String stateFilter)
    {
        final HashMap<String, String> options = new HashMap<>();

        String simListQueryData;
        addKeyValueToMapIfNotNull(PAGE, pageNumber, options);
        addKeyValueToMapIfNotNull(NETWORK, networkFilter, options);
        addKeyValueToMapIfNotNull(SimService.SIM_LIFECYCLE_STATE, stateFilter, options);

        if (options.size() == 0)
        {
            simListQueryData = "";
        }
        else
        {
            final List<BasicNameValuePair> filterOptions = options.entrySet().stream()
                    .map(entry -> new BasicNameValuePair(entry.getKey(), entry.getValue()))
                    .collect(Collectors.toList());

            simListQueryData = "?" + URLEncodedUtils.format(filterOptions, Charset.forName("UTF-8"));
        }

        return simListQueryData;
    }

    /**
     * Add a key value pair to a supplied dictionary if the value is not null.
     * @param key key
     * @param value value string
     * @param options dictionary hash map where the key value pair is added
     */
    private static void addKeyValueToMapIfNotNull(String key, String value, final HashMap<String, String> options)
    {
        if (value != null && value != "")
        {
            options.put(key, value);
        }
    }

    private List<String> setupSelectionsWithFirstEntryAsUndefined(List<String> selectionList)
    {
        List<String> list = new ArrayList<>();

        list.add(DEFAULT_SELECTION);

        selectionList.forEach( (str) -> {
            if (str != null)
            {
                list.add(str);
            }
            else
            {
                list.add(DB_NULL);
            }
        });

        return list;
    }


    private SimService getSimService() throws InternalApiException
    {
        return backEndManager.createClient(backEndManager.getAllBackEndConfigs(), SimService.class);
    }

    private RefService getRefDataService() throws InternalApiException
    {
        return backEndManager.createClient(backEndManager.getAllBackEndConfigs(), RefService.class);
    }

    /**
     * @deprecated mdce-web should not link to the internal debug page!
     */
    @Deprecated
    private static String getMdceIntegrationDebugHomepagePageUrl(final String envName) throws InternalApiException
    {
        switch (envName)
        {
            case "local-dev":
                return "http://localhost:8081/mdce-integration/pages/debug";

            case "prod":
                return "http://52.2.114.70:8081/pages/debug";

            case "preprod":
                return "http://52.27.63.177:8081/pages/debug";

            case "auto-test":
                return "http://52.25.21.32:8081/pages/debug"; //http://auto-test.mdce-nonprod.neptunensight.com:8081/pages/debug also works, unless your browser has remembered to redirect it to https

            case "int":
                return "http://52.11.212.174:8081/pages/debug"; //http://int.mdce-nonprod.neptunensight.com:8081/pages/debug also works, unless your browser has remembered to redirect it to https

            default:
                return "http://" + envName + ".mdce-nonprod.neptunensight.com:8081/pages/debug";

        }

    }



}
