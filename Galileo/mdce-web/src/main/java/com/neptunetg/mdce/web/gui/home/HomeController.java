/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.home;

import com.neptunetg.mdce.common.internal.client.InternalApiClientConfig;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import com.neptunetg.mdce.web.gui.common.DisplayStrings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Displays the home page
 */
@Controller
public class HomeController
{
    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    private final boolean msocMode;
    private final String envName;
    private final String serverName;


    @Autowired
    private BackEndManager backEndManager;

    @RequestMapping({"/", "/pages"})
    public String root(Model model)
    {
        return "redirect:/pages/home";
    }

    @Autowired
    public HomeController(@Value("${whitelistedMode}") boolean msocMode,
            @Value("#{envProperties['env.name']}") String envName,
            @Value("#{envProperties['server.name']}") String serverName)
    {
        this.msocMode = msocMode;
        this.envName = envName;
        this.serverName = serverName;
    }

    @ModelAttribute("availableBackEnds")
    public List<InternalApiClientConfig> defaultAvailableBackEnds()
    {
        return backEndManager.getAllBackEndConfigs();
    }

    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping("/pages/home")
    public String home(Model model)
    {
        model.addAttribute("msocModeMessage", DisplayStrings.t("Status.MsocMode." + this.msocMode));
        model.addAttribute("environmentName", envName);
        model.addAttribute("serverName", serverName);

        model.addAttribute("zonedatetime", ZonedDateTime.now());

        return "home/homepage.jsp";
    }

    /**
     * Generate a template for looking at css layout
     * @return
     */
    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping("/pages/template")
    @Deprecated
    public String template()
    {
        return "home/template.jsp";
    }
}
