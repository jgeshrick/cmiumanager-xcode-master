/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.login;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Manage user login/logout pages.
 */
@SessionAttributes(value = {UserController.REDIRECT_AFTER_LOGIN_SESSION_ATTRIBUTE, UserController.ERROR_MESSAGE_SESSION_ATTRIBUTE})
@Controller
public class UserController
{
    public static final String LOGIN_URL = "/pages/user/login";
    private static final String LOGIN_FAIL_URL = "/pages/user/login-fail";
    public static final String REDIRECT_AFTER_LOGIN_SESSION_ATTRIBUTE = "redirectAfterLogin";
    public static final String ERROR_MESSAGE_SESSION_ATTRIBUTE = "errorMessage";

    @RequestMapping(value = LOGIN_URL, method = RequestMethod.GET)
    public String login(ModelMap model,
                        @RequestParam(value = "lastUsername", required = false) String lastUsername,
                        @RequestParam(value = "failed", required = false) boolean failed,
                        @RequestParam(value = REDIRECT_AFTER_LOGIN_SESSION_ATTRIBUTE, required = false) String redirectAfterLogin,
                        final HttpServletRequest request)
    {
        model.addAttribute("lastUsername", lastUsername);
        model.addAttribute("failed", failed);

        //if a target url is explicitly defined, use it, else use original target url (from defaultSavedRequest), which will be handled
        //by onAuthenticationSuccess
        if (redirectAfterLogin != null)
        {
            model.addAttribute(REDIRECT_AFTER_LOGIN_SESSION_ATTRIBUTE, redirectAfterLogin);
        }

        return "login/login.jsp";
    }

    @RequestMapping(value = LOGIN_FAIL_URL, method = RequestMethod.GET)
    public String loginError(ModelMap model,
                             final RedirectAttributes redirectAttributes)
    {
        redirectAttributes.addFlashAttribute("error", "true");

        return "redirect:/pages/user/login";
    }

    @RequestMapping(value = "/pages/user/logout", method = RequestMethod.GET)
    public String logout()
    {
        return "login/logout.jsp";
    }

}
