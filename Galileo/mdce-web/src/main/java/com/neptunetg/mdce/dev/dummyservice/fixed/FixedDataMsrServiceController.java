/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.dev.dummyservice.fixed;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.dashboard.model.StatusCounts;
import com.neptunetg.mdce.common.internal.dashboard.service.MsrService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Provides MSR statistics
 */
@RestController
public class FixedDataMsrServiceController implements MsrService
{
    private static final String DUMMY_URL_MSR_BY_GROUP= "/dummyservice/fixed" + URL_MSR_BY_GROUP;


    @RequestMapping(DUMMY_URL_MSR_BY_GROUP)
    @Override
    public StatusCounts getMsrByGroup(@RequestParam(PARAM_MSR_GROUP_ID) String groupId) throws InternalApiException
    {
        StatusCounts ret = new StatusCounts();

        ret.setSuccessCount(77);
        ret.setFailCount(33);
        ret.setPendingCount(40);

        return ret;
    }



}
