/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.login;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.site.model.SiteForUser;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.common.internal.user.model.MdceUserRole;
import com.neptunetg.mdce.common.internal.user.model.ModifiedUserDetails;
import com.neptunetg.mdce.common.internal.user.model.NewUserDetails;
import com.neptunetg.mdce.common.internal.user.model.UserAlertSettings;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import com.neptunetg.mdce.common.internal.user.service.UserService;
import com.neptunetg.mdce.web.backend.BackEndManager;
import com.neptunetg.mdce.web.gui.user.AddSiteForm;
import com.neptunetg.mdce.web.gui.user.AddUserForm;
import com.neptunetg.mdce.web.gui.user.ChangeUserDetailsForm;
import com.neptunetg.mdce.web.gui.user.ResetPasswordForm;
import org.apache.commons.io.Charsets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriUtils;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.*;

import static com.neptunetg.mdce.web.gui.login.LoginUtils.getLoggedInUserName;

/**
 * For administrating user. All url in this page is accessible only to login user with MsocAdmin authority
 */

@Controller
public class UserManagementController
{
    @Autowired
    private BackEndManager backEndManager;

    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR_WHITELISTED)
    @RequestMapping(value = "/pages/user/list", method = RequestMethod.GET)
    public String listUserDetails(ModelMap model) throws InternalApiException
    {
        final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);

        List<UserDetails> userDetails = userService.listUsers();
        model.addAttribute("userlist", userDetails);

        return "user/userList.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR_WHITELISTED)
    @RequestMapping(value = "/pages/user/details/{userid}/{username}", method = RequestMethod.GET)
    public String viewUsersSettings(ModelMap model,
                                    @PathVariable("userid") int userId,
                                    @PathVariable("username") String userName) throws InternalApiException
    {
        final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);
        UserDetails userDetails = userService.getUserDetails(userName);
        ChangeUserDetailsForm changeUserDetailsForm = new ChangeUserDetailsForm(userDetails);

        model.addAttribute("sitelist", userService.getSitesForUser(userId));
        model.addAttribute("username", userName);
        model.addAttribute("addsiteform", new AddSiteForm());
        model.addAttribute("noaccesssitelist", userService.getSitesNotForUser(userId));
        model.addAttribute("alertsettingsform", userService.getUserAlertSettings(userId));
        model.addAttribute("userLevels", MdceUserRole.getCreatableRoles());
        model.addAttribute("changeUserDetailsForm", changeUserDetailsForm);

        return "user/userSettings.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "/pages/user/details/{userid}/{username}", method = RequestMethod.POST)
    public String addSiteToUser(@ModelAttribute("addsiteform") AddSiteForm addSiteForm,
                                @PathVariable("userid") int userId,
                                @PathVariable("username") String userName) throws InternalApiException, UnsupportedEncodingException
    {
        if (addSiteForm.getSiteId() != 0)
        {
            final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);
            SiteForUser siteForUser = new SiteForUser();
            siteForUser.setUserId(userId);
            siteForUser.setSiteId(addSiteForm.getSiteId());
            siteForUser.setRequester(getLoggedInUserName());

            userService.addSiteToUser(siteForUser);
        }

        return "redirect:/pages/user/details/" + Integer.toString(userId) + "/" + UriUtils.encodePath(userName, Charsets.UTF_8.toString()).toString();
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "/pages/user/savealerts", method = RequestMethod.POST)
    public String saveAlertSettings(@ModelAttribute("alertsettingsform") UserAlertSettings usersAlertsSettings) throws InternalApiException
    {
        final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);

        usersAlertsSettings.setRequester(getLoggedInUserName());
        userService.setUserAlertSettings(usersAlertsSettings);

        return "redirect:/pages/user/list";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "/pages/user/details/{userid}/{username}/delete/{siteid}", method = RequestMethod.GET)
    public String deleteSiteFromUser(ModelMap model,
                                     @PathVariable("userid") int userId,
                                     @PathVariable("siteid") int siteId,
                                     @PathVariable("username") String userName) throws InternalApiException, UnsupportedEncodingException
    {
        final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);
        SiteForUser siteForUser = new SiteForUser();
        siteForUser.setUserId(userId);
        siteForUser.setSiteId(siteId);
        siteForUser.setRequester(getLoggedInUserName());
        userService.removeSiteFromUser(siteForUser);

        return "redirect:/pages/user/details/" + Integer.toString(userId) + "/" + UriUtils.encodePath(userName, Charsets.UTF_8.toString()).toString();
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "/pages/user/add", method = RequestMethod.GET)
    public String addNewUserPage(ModelMap model) throws InternalApiException
    {
        model.addAttribute("addUserForm", new AddUserForm());
        model.addAttribute("userLevels", MdceUserRole.getCreatableRoles());

        return "user/addUser.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "/pages/user/add", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("addUserForm") @Valid AddUserForm addUserForm,
                          BindingResult result,
                          Model model) throws InternalApiException
    {
        //form validation
        if (result.hasErrors())
        {
            return "user/addUser.jsp";
        }

        //validation ok
        if (addUserForm.getPassword().equals(addUserForm.getPasswordRepeated()))
        {
            final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);
            final List<UserDetails> userByEmail = userService.getUserDetailsByEmail(addUserForm.getEmail());
            final UserDetails userDetailsByUsername = userService.getUserDetails(addUserForm.getUserName());

            NewUserDetails newUserDetails = new NewUserDetails(
                    addUserForm.getUserName(), addUserForm.getEmail(),
                    addUserForm.getPassword(), addUserForm.getUserLevel(),
                    getLoggedInUserName());


            //If we get any user details back look for username and passowrd.
            String message = "%s already assigned to user: %s id: %d";

            // check is username in user
            if (userDetailsByUsername != null)
            {
                model.addAttribute("errorMessage", String.format(message, "Username",
                        userDetailsByUsername.getUserName(),
                        userDetailsByUsername.getUserId()));
                return "user/addUser.jsp";


            }

            // check if email in use
            if (userByEmail.size() > 0)
            {
                model.addAttribute("errorMessage", String.format(message, "Email",
                        userByEmail.get(0).getUserName(),
                        userByEmail.get(0).getUserId()));
                return "user/addUser.jsp";
            }

            boolean success = userService.addUser(newUserDetails);

            if (success)
            {
                return "redirect:/pages/user/list";
            }
        }

        model.addAttribute("error", "passwords do not match, or user already exists, please try again");
        List<String> userLevelStrings = new ArrayList<>();

        for (MdceUserRole userLevel : MdceUserRole.values())
        {
            userLevelStrings.add(userLevel.getRoleDescription());
        }

        model.addAttribute("userLevels", userLevelStrings);
        return "user/addUser.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "pages/user/resetpassword/{userid}/{username}", method = RequestMethod.GET)
    public String resetPasswordPage(@PathVariable("userid") int userId,
                                    @PathVariable("username") String userName,
                                    ModelMap model) throws InternalApiException
    {
        model.addAttribute("resetPasswordForm", new ResetPasswordForm());
        model.addAttribute("userid", userId);
        model.addAttribute("userName", userName);
        return "user/resetPassword.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "pages/user/resetpassword/{userid}/{username}", method = RequestMethod.POST)
    public String resetPassword(
            @PathVariable("userid") int userId,
            @ModelAttribute("resetPasswordForm") @Valid ResetPasswordForm resetPasswordForm,
            BindingResult result,
            Model model) throws InternalApiException
    {

        if (result.hasErrors())
        {
            return "user/resetPassword.jsp";
        }
        else
        {
            //assume form validation has already been performed.

            ModifiedUserDetails modifiedUserDetails = new ModifiedUserDetails();
            modifiedUserDetails.setUserId(userId);
            modifiedUserDetails.setPassword(resetPasswordForm.getPassword());
            modifiedUserDetails.setRequester(getLoggedInUserName());

            final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);
            userService.resetPassword(modifiedUserDetails);

            return "redirect:/pages/user/list";
        }

    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "pages/user/delete/{userId}", method = RequestMethod.GET)
    public String removeUser(HttpServletRequest request,
                             @PathVariable(value = "userId") int userId) throws InternalApiException
    {
        final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);

        ModifiedUserDetails modifiedUserDetails = new ModifiedUserDetails();
        modifiedUserDetails.setUserId(userId);
        modifiedUserDetails.setRequester(getLoggedInUserName());

        userService.removeUser(modifiedUserDetails);

        return "redirect:/pages/user/list";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "pages/user/changeuserdetails", method = RequestMethod.POST)
    public String changeUserDetails(
            @ModelAttribute("changeUserDetailsForm") @Valid ChangeUserDetailsForm changeUserDetailsForm,
            BindingResult result,
            Model model) throws InternalApiException
    {
        final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);
        final UserDetails userDetails = userService.getUserDetails(changeUserDetailsForm.getPreviousUserName());
        final boolean wasUserNameChanged = !userDetails.getUserName().equals(changeUserDetailsForm.getUserName());
        final boolean wasUserLevelChanged = !userDetails.getUserLevel().equals(changeUserDetailsForm.getUserLevel());
        final boolean wasEmailChanged = !userDetails.getEmail().equals(changeUserDetailsForm.getEmail());
        final boolean wasAccountLockoutEnabledChanged = userDetails.isAccountLockoutEnabled() != changeUserDetailsForm.isAccountLockoutEnabled();
        final boolean wasCurrentlyDisabledChanged = userDetails.isCurrentlyDisabled() != changeUserDetailsForm.isCurrentlyDisabled();
        final boolean wasAllowConcurrentSessionsChanged = userDetails.getAllowConcurrentSessions() != changeUserDetailsForm.getAllowConcurrentSessions();

        // Additional username validation
        if (wasUserNameChanged && StringUtils.hasText(changeUserDetailsForm.getUserName()))
        {
            final UserDetails existingUserDetails = userService.getUserDetails(changeUserDetailsForm.getUserName());

            if (existingUserDetails != null)
            {
                result.rejectValue("userName", "userName.alreadyInUse");
            }
        }

        if (result.hasErrors())
        {
            model.addAttribute("sitelist", userService.getSitesForUser((int)userDetails.getUserId()));
            model.addAttribute("username", userDetails.getUserName());
            model.addAttribute("userid", userDetails.getUserId());
            model.addAttribute("addsiteform", new AddSiteForm());
            model.addAttribute("noaccesssitelist", userService.getSitesNotForUser((int)userDetails.getUserId()));
            model.addAttribute("alertsettingsform", userService.getUserAlertSettings((int)userDetails.getUserId()));
            model.addAttribute("userLevels", MdceUserRole.getCreatableRoles());

            return "user/userSettings.jsp";
        }
        else
        {
            ModifiedUserDetails modifiedUserDetails = new ModifiedUserDetails();
            modifiedUserDetails.setUserId((int) changeUserDetailsForm.getUserId());
            boolean isDetailChanged = false;

            if (wasUserNameChanged)
            {
                modifiedUserDetails.setUserName(changeUserDetailsForm.getUserName());
                modifiedUserDetails.setPreviousUserName(changeUserDetailsForm.getPreviousUserName());
                isDetailChanged = true;
            }

            if (wasUserLevelChanged)
            {
                modifiedUserDetails.setUserRole(changeUserDetailsForm.getUserLevel());
                isDetailChanged = true;
            }

            if (wasEmailChanged)
            {
                modifiedUserDetails.setEmail(changeUserDetailsForm.getEmail());
                isDetailChanged = true;
            }

            if (wasAccountLockoutEnabledChanged)
            {
                modifiedUserDetails.setAccountLockoutEnabled(changeUserDetailsForm.isAccountLockoutEnabled());
                isDetailChanged = true;
            }

            if (wasCurrentlyDisabledChanged)
            {
                modifiedUserDetails.setCurrentlyDisabled(changeUserDetailsForm.isCurrentlyDisabled());
                isDetailChanged = true;
            }

            if (wasAllowConcurrentSessionsChanged)
            {
                modifiedUserDetails.setAllowConcurrentSessions(changeUserDetailsForm.getAllowConcurrentSessions());
                isDetailChanged = true;
            }

            if (isDetailChanged)
            {
                modifiedUserDetails.setRequester(getLoggedInUserName());
                userService.setUserDetails(userDetails.getUserName(), modifiedUserDetails);
            }

            return "redirect:/pages/user/list";
        }

    }

}
