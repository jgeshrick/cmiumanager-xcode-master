/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.recovery;

import javax.validation.constraints.Size;

/**
 * Contains the details of a request for a password reset email.
 */
public class RequestPasswordResetLinkForm
{
    // Valid email address unlikely to have fewer than 6 characters; 45 characters is maximum supported in database.
    @Size(min = 6, max = 45, message = "Please enter the email address registered to your MDCE account")
    private String emailAddress;

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }
}
