/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *
 *  All rights reserved
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.gui.common;

import java.util.List;

/**
 * A generic class used to encapsulate response for AJAX request from mdce-web.
 */
public class JsonDataResponse<T>
{
    /**
     * Error from backend data fetch
     */
    private String error;

    /**
     * Contain results of a backend data fetch
     */
    private List<T> results;

    public String getError()
    {
        return error;
    }

    public void setError(String error)
    {
        this.error = error;
    }

    public List<T> getResults()
    {
        return results;
    }

    public void setResults(List<T> results)
    {
        this.results = results;
    }
}
