/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.dashboard;


import com.neptunetg.mdce.common.internal.miu.model.CmiuDetails;
import com.neptunetg.mdce.common.internal.miu.service.CmiuDetailsService;
import com.neptunetg.mdce.web.backend.BackEndManager;
import com.neptunetg.mdce.web.gui.common.JsonDataResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Provide utilities for miu_details table.
 */
@Controller
@Deprecated
public class MiuDetailsController
{
    private static final Logger logger = LoggerFactory.getLogger(MiuDetailsController.class);

    @Autowired
    private BackEndManager backEndManager;

    @RequestMapping("/pages/miu-details")
    public String showMiuDetails()
    {
        return "dashboard/miuDetails.jsp";
    }

    @RequestMapping("/cmiu/details")
    @ResponseBody
    public JsonDataResponse<CmiuDetails> getCmiuDetails(@RequestParam("account") String accountId)
    {
        JsonDataResponse<CmiuDetails> response = new JsonDataResponse<CmiuDetails>();

        try
        {
            final CmiuDetailsService service = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuDetailsService.class);
            List<CmiuDetails> cmiuDetailsList = service.getCmiuByAccount(accountId);
            response.setResults(cmiuDetailsList);
        }
        catch (Exception e)
        {
            logger.error("Error getting CMIU details for account", accountId);
            response.setError(e.getMessage());
        }

        return response;

    }
}
