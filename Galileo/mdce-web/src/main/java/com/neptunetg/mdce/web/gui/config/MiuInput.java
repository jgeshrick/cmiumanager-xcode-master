/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.config;

/**
 * Miu and meta data
 */
public class MiuInput
{
    private String miuId;

    private String metaData;

    public MiuInput()
    {}

    public String getMiuId()
    {
        return miuId;
    }

    public void setMiuId(String miuId)
    {
        this.miuId = miuId;
    }

    public String getMetaData()
    {
        return metaData;
    }

    public void setMetaData(String metaData)
    {
        this.metaData = metaData;
    }
}
