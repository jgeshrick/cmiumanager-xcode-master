/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.sim;

import com.neptunetg.mdce.common.internal.sim.model.SimLifecycleState;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * Backing form for changing the lifecycle state of a SIM.
 */
public class ChangeSimStateForm
{
    private String iccid;

    private SimLifecycleState selectedSimState;

    private List<SimLifecycleState> simStateList;

    /**
     * The current page index on the SIM list.
     */
    private String pageFilter;

    /**
     * The selected 'Network' filter value on the SIM list.
     */
    private String networkFilter;

    /**
     * The selected 'State' filter value on the SIM list.
     */
    private String stateFilter;

    /**
     * The path & query data required to restore the previous view of the SIM List.
     */
    private String returnUrl;

    public ChangeSimStateForm(String iccid, SimLifecycleState selectedSimState, String pageFilter,
                              String networkFilter, String stateFilter, String returnUrl)
    {
        this();

        this.iccid = iccid;
        this.selectedSimState = selectedSimState;
        this.pageFilter = pageFilter;
        this.networkFilter = networkFilter;
        this.stateFilter = stateFilter;
        this.returnUrl = returnUrl;
    }

    public ChangeSimStateForm()
    {
        this.simStateList = new ArrayList<>(EnumSet.allOf(SimLifecycleState.class));
    }

    public String getIccid()
    {
        return iccid;
    }

    public void setIccid(String iccid)
    {
        this.iccid = iccid;
    }

    public SimLifecycleState getSelectedSimState()
    {
        return selectedSimState;
    }

    public void setSelectedSimState(SimLifecycleState selectedSimState)
    {
        this.selectedSimState = selectedSimState;
    }

    public List<SimLifecycleState> getSimStateList()
    {
        return simStateList;
    }

    public void setSimStateList(List<SimLifecycleState> simStateList)
    {
        this.simStateList = simStateList;
    }

    public String getPageFilter()
    {
        return pageFilter;
    }

    public void setPageFilter(String pageFilter)
    {
        this.pageFilter = pageFilter;
    }

    public String getNetworkFilter()
    {
        return networkFilter;
    }

    public void setNetworkFilter(String networkFilter)
    {
        this.networkFilter = networkFilter;
    }

    public String getStateFilter()
    {
        return stateFilter;
    }

    public void setStateFilter(String stateFilter)
    {
        this.stateFilter = stateFilter;
    }

    public String getReturnUrl()
    {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl)
    {
        this.returnUrl = returnUrl;
    }
}
