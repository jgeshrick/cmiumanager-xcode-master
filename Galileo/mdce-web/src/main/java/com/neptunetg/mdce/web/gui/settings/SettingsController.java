/*
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 */

package com.neptunetg.mdce.web.gui.settings;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.settings.service.MdceSettingsService;
import com.neptunetg.mdce.common.internal.settings.service.MnoCallbackService;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import com.neptunetg.mdce.web.gui.login.LoginUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.rmi.RemoteException;

/**
 * Manage MDCE settings
 */
@Controller
public class SettingsController
{
    private final BackEndManager backEndManager;

    @Autowired
    public SettingsController(BackEndManager backEndManager)
    {
        this.backEndManager = backEndManager;
    }

    @RequestMapping(value = "/pages/settings", method = RequestMethod.GET)
    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN)
    public String viewSettings(Model model) throws InternalApiException
    {
        populateModelWithSettings(model);

        return "settings/settings.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN)
    @RequestMapping(value = "/pages/settings/modify", method = RequestMethod.GET)
    public String beginChangeSettings(Model model) throws InternalApiException
    {
        populateModelWithSettings(model);

        return "settings/modifySettings.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "/pages/settings/modify", method = RequestMethod.POST)
    public String submitChangeSettings(@Valid @ModelAttribute SettingsForm form, BindingResult bindingResult) throws InternalApiException
    {
        if (bindingResult.hasErrors())
        {
            return "settings/modifySettings.jsp";
        }

        long minAgeToDeleteMonths = form.getMinAgeToDeleteMonths();
        long maxLoginAttempts = form.getMaxLoginAttempts();
        long lockedAccountTimeoutMinutes = form.getLockedAccountTimeoutMinutes();

        MdceSettingsService settingsService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MdceSettingsService.class);

        settingsService.setIntSettingValue(
                MdceSettingsService.OLD_TABLE_MIN_AGE_MONTHS_TO_DELETE_KEY,
                minAgeToDeleteMonths,
                LoginUtils.getLoggedInUserName());

        settingsService.setIntSettingValue(
                MdceSettingsService.MAX_LOGIN_ATTEMPTS_KEY,
                maxLoginAttempts,
                LoginUtils.getLoggedInUserName());

        settingsService.setIntSettingValue(
                MdceSettingsService.LOCKED_ACCOUNT_TIMEOUT_MINUTES_KEY,
                lockedAccountTimeoutMinutes,
                LoginUtils.getLoggedInUserName());

        return "redirect:/pages/settings";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_MASTER_OF_CELLULAR_WHITELISTED)
    @RequestMapping(value = "pages/settings/callback/{mno}/register")
    @ResponseBody
    public String registerCallbackWithMno(@PathVariable(value = "mno") String mno) throws InternalApiException, RemoteException
    {
        MnoCallbackService callbackService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MnoCallbackService.class);
        return callbackService.registerCallback(mno);
    }

    @PreAuthorize(LoginConstants.IS_MSOC_MASTER_OF_CELLULAR_WHITELISTED)
    @RequestMapping(value = "pages/settings/callback/{mno}/unregister")
    @ResponseBody
    public String unregisterCallbackWithMno(@PathVariable(value = "mno") String mno) throws InternalApiException, RemoteException
    {
        MnoCallbackService callbackService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MnoCallbackService.class);
        return callbackService.unregisterCallback(mno);
    }

    private void populateModelWithSettings(Model model) throws InternalApiException
    {
        MdceSettingsService settingsService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MdceSettingsService.class);

        // Get values from internal settings service.
        Long minAgeToDeleteMonths = settingsService.getIntSettingValue(
                MdceSettingsService.OLD_TABLE_MIN_AGE_MONTHS_TO_DELETE_KEY,
                MdceSettingsService.OLD_TABLE_MIN_AGE_MONTHS_TO_DELETE_DEFAULT_VALUE);

        Long maxLoginAttempts = settingsService.getIntSettingValue(
                MdceSettingsService.MAX_LOGIN_ATTEMPTS_KEY,
                MdceSettingsService.MAX_LOGIN_ATTEMPTS_DEFAULT_VALUE);

        Long lockedAccountTimeoutMinutes = settingsService.getIntSettingValue(
                MdceSettingsService.LOCKED_ACCOUNT_TIMEOUT_MINUTES_KEY,
                MdceSettingsService.LOCKED_ACCOUNT_TIMEOUT_MINUTES_DEFAULT_VALUE);

        SettingsForm form = new SettingsForm();
        form.setMinAgeToDeleteMonths(minAgeToDeleteMonths);
        form.setMaxLoginAttempts(maxLoginAttempts);
        form.setLockedAccountTimeoutMinutes(lockedAccountTimeoutMinutes);

        model.addAttribute("settingsForm", form);
    }

}