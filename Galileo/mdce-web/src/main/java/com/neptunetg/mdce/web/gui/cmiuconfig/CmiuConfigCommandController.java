/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.cmiuconfig;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CmiuDescriptionViewModel;
import com.neptunetg.mdce.common.internal.miu.service.MiuCommandControlService;
import com.neptunetg.mdce.common.internal.miu.service.CmiuConfigMgmtService;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 * Command and Control page - general handler.
 */
@Controller
@SessionAttributes({ "cmiuConfigCommandList" })
@PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
public class CmiuConfigCommandController
{
    private static final Logger logger = LoggerFactory.getLogger(CmiuConfigCommandController.class);

    @Autowired
    private BackEndManager backEndManager;

    @RequestMapping(value = "/pages/config/flush", method=RequestMethod.GET)
    public String flushCommandsFromCmiu(@RequestParam("miuId") long miuId) throws InternalApiException
    {
        final MiuCommandControlService commandControlService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MiuCommandControlService.class);

        int flushedCommands = commandControlService.flushCommand(miuId);

        return "redirect:/pages/config";
    }

    @RequestMapping(value = "/pages/config/reject", method=RequestMethod.GET)
    public String rejectCommand(@RequestParam("commandId") long commandId) throws InternalApiException
    {
        final MiuCommandControlService commandControlService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MiuCommandControlService.class);

        commandControlService.rejectCommand(commandId);

        return "redirect:/pages/config";
    }

    /**
     * Get Cmiu Config note for a given cmiu.
     * @param cmiuId id of the CMIU to retrieve the note
     * @return json
     */
    @RequestMapping(value="/pages/config/notes/{cmiu}")
    public String getCmiuConfigNotes(@PathVariable(value = "cmiu") long cmiuId, Model model) throws InternalApiException
    {
        final CmiuConfigMgmtService configMgmtService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuConfigMgmtService.class);

        model.addAttribute("cmiuId", cmiuId);
        model.addAttribute("cmiuConfigHistoryList", configMgmtService.getCmiuConfigHistory(cmiuId));
        return "cmiuconfig/notes.jsp";
    }


    /**
     * AJAX request for CMIU meta-data
     * @param cmiuId
     * @return
     * @throws InternalApiException
     */
    @RequestMapping(value="/pages/config/metadata", method = RequestMethod.GET)
    @ResponseBody
    public CmiuDescriptionViewModel getCmiuDescription(@RequestParam("cmiuid") long cmiuId) throws InternalApiException
    {
        final MiuCommandControlService commandService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MiuCommandControlService.class);
        return commandService.getCmiuDescription(cmiuId);
    }

}
