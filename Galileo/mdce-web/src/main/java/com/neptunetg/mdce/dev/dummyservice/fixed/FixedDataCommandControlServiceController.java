/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.dev.dummyservice.fixed;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CmiuDescriptionViewModel;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandSummary;
import com.neptunetg.mdce.common.internal.miu.service.MiuCommandControlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Fake service for handling CMIU configuration command and control
 */
@RestController
@RequestMapping("/dummyservice/fixed")
public class FixedDataCommandControlServiceController implements MiuCommandControlService
{
    private static final Logger logger = LoggerFactory.getLogger(FixedDataCommandControlServiceController.class);


    /**
     * Send a command to the backend for processing and publishing to MQTT broker.
     *
     * @param commandRequest containing the details of the command to be send to the backend
     */
    @Override
    @RequestMapping(value=URL_CMIU_COMMAND, method=RequestMethod.POST)
    public void sendCommand(@RequestBody CommandRequest commandRequest)
    {
        //do nothing for the moment
        logger.debug("sendCommand called");
    }

    @Override
    public void rejectCommand(@RequestParam("commandId") long l) throws InternalApiException
    {
        //do nothing
        logger.debug("rejectCommand called with commandId " + l);
    }

    /**
     * Remove all commands that queue for a CMIU. This causes the RDS to change all commands that are in CREATED or QUEUED
     * state to RECALLED.
     *
     * @param cmiuId the cmiu id
     * @return number of command affected
     */
    @Override
    public int flushCommand(@RequestParam(REQUEST_PARAM_CMIUID) long cmiuId) throws InternalApiException
    {
        return 1;
    }

    @Override
    public CmiuDescriptionViewModel getCmiuDescription(@RequestParam(REQUEST_PARAM_CMIUID) long cmiuId)
    {
        return new CmiuDescriptionViewModel(cmiuId, "Cmiu: " + cmiuId + "fake description");
    }

    @Override
    public List<CommandSummary> getCommandsInProgress() throws InternalApiException
    {
        List<CommandSummary> commandSummaries = new ArrayList<>();

        //create fake data
        CommandSummary commandSummary1 = new CommandSummary();
        commandSummary1.setCommandId(1);
        commandSummary1.setMiuId(123456);
        commandSummary1.setCommandType(CommandRequest.CommandTypeCmiu.COMMAND_UPDATE_IMAGE.getCommandDescription());
        commandSummary1.setCommandEnteredTime(Instant.now());
        commandSummary1.setDetails("image update details here, image type bla bla");
        commandSummaries.add(commandSummary1);

        CommandSummary commandSummary3 = new CommandSummary();
        commandSummary3.setCommandId(1);
        commandSummary3.setMiuId(234567);
        commandSummary3.setCommandType(CommandRequest.CommandTypeCmiu.COMMAND_UPDATE_IMAGE.getCommandDescription());
        commandSummary3.setCommandEnteredTime(Instant.now());
        commandSummary3.setDetails("image udpate details here");
        commandSummaries.add(commandSummary3);

        return commandSummaries;
    }

    @Override
    public List<CommandSummary> getCommandsReceived() throws InternalApiException
    {
        List<CommandSummary> commandSummaries = new ArrayList<>();

        //create fake data
        CommandSummary commandSummary1 = new CommandSummary();
        commandSummary1.setCommandId(1);
        commandSummary1.setMiuId(123456);
        commandSummary1.setCommandType(CommandRequest.CommandTypeCmiu.COMMAND_UPDATE_IMAGE.getCommandDescription());
        commandSummary1.setCommandEnteredTime(Instant.now());
        commandSummary1.setDetails("image update details here, image type bla bla");
        commandSummaries.add(commandSummary1);

        CommandSummary commandSummary3 = new CommandSummary();
        commandSummary3.setCommandId(1);
        commandSummary3.setMiuId(234567);
        commandSummary3.setCommandType(CommandRequest.CommandTypeCmiu.COMMAND_UPDATE_IMAGE.getCommandDescription());
        commandSummary3.setCommandEnteredTime(Instant.now());
        commandSummary3.setDetails("image udpate details here");
        commandSummaries.add(commandSummary3);

        return commandSummaries;
    }

    @Override
    public List<CommandSummary> getCommandsCompleted(@RequestParam(REQUEST_PARAM_DAYS_AGO) int daysAgo) throws InternalApiException
    {
        List<CommandSummary> commandSummaries = new ArrayList<>();

        //create fake data
        CommandSummary commandSummary1 = new CommandSummary();
        commandSummary1.setCommandId(1);
        commandSummary1.setMiuId(123456);
        commandSummary1.setCommandType(CommandRequest.CommandTypeCmiu.COMMAND_UPDATE_IMAGE.getCommandDescription());
        commandSummary1.setCommandEnteredTime(Instant.now());
        commandSummary1.setDetails("image update details here, image type bla bla");
        commandSummaries.add(commandSummary1);

        CommandSummary commandSummary3 = new CommandSummary();
        commandSummary3.setCommandId(1);
        commandSummary3.setMiuId(234567);
        commandSummary3.setCommandType(CommandRequest.CommandTypeCmiu.COMMAND_UPDATE_IMAGE.getCommandDescription());
        commandSummary3.setCommandEnteredTime(Instant.now());
        commandSummary3.setDetails("image udpate details here");
        commandSummaries.add(commandSummary3);

        return commandSummaries;
    }

    @Override
    public List<CommandSummary> getCommandsRejected(@RequestParam(REQUEST_PARAM_DAYS_AGO) int daysAgo) throws InternalApiException
    {
        List<CommandSummary> commandSummaries = new ArrayList<>();

        //create fake data
        CommandSummary commandSummary1 = new CommandSummary();
        commandSummary1.setCommandId(1);
        commandSummary1.setMiuId(123456);
        commandSummary1.setCommandType(CommandRequest.CommandTypeCmiu.COMMAND_UPDATE_IMAGE.getCommandDescription());
        commandSummary1.setCommandEnteredTime(Instant.now());
        commandSummary1.setDetails("image update details here, image type bla bla");
        commandSummaries.add(commandSummary1);

        CommandSummary commandSummary3 = new CommandSummary();
        commandSummary3.setCommandId(1);
        commandSummary3.setMiuId(234567);
        commandSummary3.setCommandType(CommandRequest.CommandTypeCmiu.COMMAND_UPDATE_IMAGE.getCommandDescription());
        commandSummary3.setCommandEnteredTime(Instant.now());
        commandSummary3.setDetails("image udpate details here");
        commandSummaries.add(commandSummary3);

        return commandSummaries;
    }

}
