/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.audit;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Backing form for filter audit log
 */
public class AuditLogsFilterForm
{
    private Long miuId;
    private String user;

    @NotNull (message = "Has to be greater than 6")
    @Min(value = 7, message = "Has to be greater than 6")
    private Integer daysAgo = Integer.valueOf(7);

    private int page;

    public Long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(Long miuId)
    {
        this.miuId = miuId;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public Integer getDaysAgo()
    {
        return daysAgo;
    }

    public void setDaysAgo(Integer daysAgo)
    {
        this.daysAgo = daysAgo;
    }

    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }

    /**
     * Get a destination page number for passing to PagedResult.
     * Page display on html starts from 1, whereas internally it is base 0
     * @return page number based 0
     */
    public int getPageNumberForPagedResult()
    {
        return page > 1?  page - 1 : 0;
    }
}
