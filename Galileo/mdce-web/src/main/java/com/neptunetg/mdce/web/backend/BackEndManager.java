/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.backend;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.client.InternalApiClientConfig;
import com.neptunetg.mdce.common.internal.client.MultiServerClient;
import com.neptunetg.mdce.common.internal.client.SingleServerClient;
import com.neptunetg.mdce.common.internal.servicestatus.model.ServiceStatus;
import com.neptunetg.mdce.common.internal.servicestatus.service.InternalApiLibraryVersionInfo;
import com.neptunetg.mdce.common.internal.servicestatus.service.ServiceStatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * Manages the available back-ends
 */
@Service
public class BackEndManager
{

    private static final Logger logger = LoggerFactory.getLogger(BackEndManager.class);

    private final List<InternalApiClientConfig> backEndConfigs = new CopyOnWriteArrayList<>();

    private final ExecutorService backEndCallThreadPool = Executors.newFixedThreadPool(30);

    private BackEndStatusViewModel serverStatuses;

    private static BackEndManager instance;

    @Autowired
    public BackEndManager(@Qualifier("envProperties") Properties envProperties) throws MalformedURLException
    {
        final int serverCount = Integer.parseInt(envProperties.getProperty("backend.server.count"));

        for (int i = 0; i < serverCount; i++)
        {
            final String name = envProperties.getProperty(String.format("backend.server.%d.name", i));
            final String url = envProperties.getProperty(String.format("backend.server.%d.url", i));
            final boolean enabled = Boolean.parseBoolean(envProperties.getProperty(String.format("backend.server.%d.enabled", i)));
            if (enabled)
            {
                this.backEndConfigs.add(new InternalApiClientConfig(name, url));
            }
            else
            {
                logger.warn("Back end " + name + " is disabled");
            }
        }
        instance = this; //to allow access from static status service
    }

    public List<InternalApiClientConfig> getAllBackEndConfigs()
    {
        final List<InternalApiClientConfig> ret = new ArrayList<>(backEndConfigs.size());
        ret.addAll(backEndConfigs);
        return ret;
    }


    public List<InternalApiClientConfig> getBackEndConfigs(List<String> names)
    {
        final Set<String> nameSet = new HashSet<>(names.size());
        nameSet.addAll(names);
        return backEndConfigs.stream().filter(c -> nameSet.contains(c.getName())).collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public <T> T createClient(List<InternalApiClientConfig> servers, Class<T> interfaceClass) throws InternalApiException
    {
        if (servers.size() == 1)
        {
            final InternalApiClientConfig config = servers.get(0);
            return (T) new SingleServerClient(config.getName(), config.getUrl());
        }
        else
        {
            return (T) new MultiServerClient(servers, this.backEndCallThreadPool, 15L);
        }
    }

    private ServiceStatus checkStatus(InternalApiClientConfig config)
    {
        ServiceStatus ret = new ServiceStatus();
        try
        {
            final ServiceStatusService statusService = new SingleServerClient(config.getName(), config.getUrl());
            ret = statusService.getServiceStatus();
            if (!ret.isError() && !ret.isWarning() && !InternalApiLibraryVersionInfo.LIBRARY_VERSION.equals(ret.getLibraryVersion()))
            {
                ret.setWarning(true);
                ret.setErrorMessage("WARNING: version mismatch in API library.  Web application is on " + InternalApiLibraryVersionInfo.LIBRARY_VERSION
                    + ", back end is on " + ret.getLibraryVersion());
            }
        }
        catch (Exception e)
        {
            ret.setError(true);
            ret.setErrorMessage("ERROR: " + e.getMessage());
        }
        return ret;
    }

    /**
     * Static for use in taglib
     * @return Server statuses
     */
    public static BackEndStatusViewModel getServerStatuses()
    {
        if (instance != null)
        {
            return instance.getOrRefreshServerStatuses();
        }
        else
        {
            return null;
        }
    }

    private synchronized BackEndStatusViewModel getOrRefreshServerStatuses()
    {
        Instant now = Instant.now();
        if (serverStatuses != null)
        {
            if (now.isBefore(serverStatuses.getFetchTime().plus(1L, ChronoUnit.MINUTES))) //less than 1 minute stale
            {
                return serverStatuses;
            }
        }

        final BackEndStatusViewModel newServerStatuses = new BackEndStatusViewModel(now);
        for (InternalApiClientConfig config : backEndConfigs)
        {
            final ServiceStatus status = checkStatus(config);
            newServerStatuses.add(config, status);
        }

        serverStatuses = newServerStatuses;
        return newServerStatuses;
    }
}
