/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.audit;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.audit.model.AuditLog;
import com.neptunetg.mdce.common.internal.audit.model.AuditLogPagedResult;
import com.neptunetg.mdce.common.internal.audit.service.AuditLogService;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Render pages for audit logs.
 */
@Controller
@RequestMapping("/pages/audit")
public class AuditController
{
    private static final String PAGE = "page";
    private static final String NEXT = "next";

    @Autowired
    private BackEndManager backEndManager;

    @ModelAttribute("auditLogFilterForm")
    private AuditLogsFilterForm getAuditLogsFilterForm()
    {
        return new AuditLogsFilterForm();
    }

    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String showAuditLog(
            @ModelAttribute("auditLogFilterForm") @Valid AuditLogsFilterForm auditLogsFilterForm,
            BindingResult bindingResult,
            Model model,
            HttpServletRequest request
    ) throws InternalApiException
    {
        //Note: binding errors will be displayed even if it is not handled.

        final AuditLogService auditLogService = getAuditLogService();

        //page display on html starts from 1, whereas internally it is base 0
        int newPage = auditLogsFilterForm.getPageNumberForPagedResult();

        AuditLogPagedResult auditLogList = auditLogService.getAuditLog(auditLogsFilterForm.getMiuId(),
                auditLogsFilterForm.getUser(), auditLogsFilterForm.getDaysAgo(), newPage);

        auditLogsFilterForm.setPage(newPage);

        model.addAttribute("auditLogFilterForm", auditLogsFilterForm);
        model.addAttribute("auditLogList", auditLogList.getResults());
        model.addAttribute("totalResults", auditLogList.getTotalResults());
        model.addAttribute("numResults", auditLogList.getNumResults());

        auditLogsFilterForm.setPage(auditLogList.getPageNum());


        String requestParam = request.getQueryString();
        model.addAttribute("downloadRequestParams", request.getQueryString());

        return "audit/auditLog.jsp";
    }

    private AuditLogService getAuditLogService() throws InternalApiException
    {
        return backEndManager.createClient(backEndManager.getAllBackEndConfigs(), AuditLogService.class);
    }

    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public void downloadAuditLog(HttpServletResponse response,
                                 @RequestParam(value = "miu-id", required = false) Long miuId,
                                 @RequestParam(value = "user", required = false) String userName,
                                 @RequestParam(value = "days-ago", required = false) Integer daysOfRecord) throws InternalApiException, IOException
    {
        response.setContentType("text/csv");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        response.setHeader("content-disposition",
                String.format("attachment; filename=\"audit_log_%s.csv\"", dateFormat.format(new Date())));

        final AuditLogService auditLogService = getAuditLogService();

        AuditLogPagedResult auditLogList = auditLogService.getAuditLog(miuId, userName, daysOfRecord, null);

        final PrintWriter writer = response.getWriter();

        //printer header
        writer.println(AuditLog.getCsvHeader());

        if (auditLogList != null && auditLogList.getResults() != null)
        {
            auditLogList.getResults().forEach(auditLog -> writer.println(auditLog.toCsv()));
        }

    }
}
