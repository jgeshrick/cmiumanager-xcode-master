/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui;

import java.time.Duration;
import java.time.Instant;

public final class CompareDateTime
{

    //Prevent this class to be instantiated
    private CompareDateTime(){}

    /**
     * Returns number of whole days since Instant given
     * @param time
     * @return Number of whole days between time and now
     */
    public static int getDaysAgo(Instant time)
    {
        if( time == null ) { throw new IllegalArgumentException("Null Instant provided"); }

        return Math.toIntExact(Duration.between(time, Instant.now()).toDays());
    }
}
