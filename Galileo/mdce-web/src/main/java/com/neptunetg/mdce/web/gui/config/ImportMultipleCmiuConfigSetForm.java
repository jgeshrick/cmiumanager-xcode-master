/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.gui.config;

import org.springframework.web.multipart.MultipartFile;

/**
 * Backing form for CmiuConfigSet
 */
public class ImportMultipleCmiuConfigSetForm
{
    private long cmiuConfigId;
    private String changeNote;
    private String redirectUrl;

    private MultipartFile cmiuListDefFile;

    public long getCmiuConfigId()
    {
        return cmiuConfigId;
    }

    public void setCmiuConfigId(long cmiuConfigId)
    {
        this.cmiuConfigId = cmiuConfigId;
    }

    public String getChangeNote()
    {
        return changeNote;
    }

    public void setChangeNote(String changeNote)
    {
        this.changeNote = changeNote;
    }

    public MultipartFile getCmiuListDefFile()
    {
        return cmiuListDefFile;
    }

    public void setCmiuListDefFile(MultipartFile cmiuListDefFile)
    {
        this.cmiuListDefFile = cmiuListDefFile;
    }

    public String getRedirectUrl()
    {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl)
    {
        this.redirectUrl = redirectUrl;
    }
}
