/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.dev.dummyservice.fixed;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.audit.model.AuditLog;
import com.neptunetg.mdce.common.internal.audit.model.AuditLogPagedResult;
import com.neptunetg.mdce.common.internal.audit.service.AuditLogService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Dummy backend implementation of audit log service
 */
@RestController
public class FixAuditLogServiceController implements AuditLogService
{
    /**
     * Create some fake audit log objects
     */
    private static AuditLog createFakeAuditLog(long miuId)
    {
        AuditLog auditLog = new AuditLog();

        auditLog.setDateTime(Instant.now());
        auditLog.setUserName("dummy user name");
        auditLog.setMiuId(miuId);
        auditLog.setAuditType("miu config");
        auditLog.setOldValue("old config value " + miuId);
        auditLog.setNewValue("new config value " + miuId);

        return auditLog;
    }

    @Override
    @RequestMapping(value = URL_AUDIT_LOG, method = RequestMethod.GET)
    public AuditLogPagedResult getAuditLog(@RequestParam(value = REQUEST_PARAM_AUDIT_FILTER_MIU, required = false) Long miuId,
                                           @RequestParam(value = REQUEST_PARAM_AUDIT_FILTER_USERNAME, required = false) String userName,
                                           @RequestParam(value = REQUEST_PARAM_AUDIT_LOG_DAYS_AGO, required = false) Integer daysAgo,
                                           @RequestParam(value = REQUEST_PARAM_AUDIT_LOG_PAGE, required = false) Integer page) throws InternalApiException
    {
        List<AuditLog> auditLogs = new ArrayList<>();

        for (int i = 0; i < 10; i++)
        {
            auditLogs.add(createFakeAuditLog(i + 400000000));
        }

        return new AuditLogPagedResult(auditLogs.size(), auditLogs.size(), 0, auditLogs);
    }

    @Override
    @RequestMapping(value = URL_AUDIT_LOG_ACCESS_DENIED, method = RequestMethod.GET)
    public void logAccessDenied(@RequestParam(value = REQUEST_PARAM_AUDIT_USERNAME) String userName,
                         @RequestParam(value = REQUEST_PARAM_AUDIT_DESCRIPTION) String description
    ) throws InternalApiException
    {}
}
