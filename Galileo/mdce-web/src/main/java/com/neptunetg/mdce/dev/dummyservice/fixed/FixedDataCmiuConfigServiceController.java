/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.dev.dummyservice.fixed;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigChange;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigHistory;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigMgmt;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;
import com.neptunetg.mdce.common.internal.miu.service.CmiuConfigMgmtService;
import com.neptunetg.mdce.common.internal.miu.service.CmiuConfigSetService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Fake service for handling CMIU configuration command and control
 */
@RestController
@RequestMapping("/dummyservice/fixed")
public class FixedDataCmiuConfigServiceController implements CmiuConfigMgmtService, CmiuConfigSetService
{
    //store cmiu config mgmt entries, key is the CMIU id
    private final Map<Integer, CmiuConfigMgmt> cmiuConfigMgmtMap = new HashMap<>();

    //store cmiu config set
    private final List<CmiuConfigSet> cmiuConfigSetList = new ArrayList<>();

    private final List<CmiuConfigHistory> cmiuConfigHistory = new ArrayList<>();

    public FixedDataCmiuConfigServiceController()
    {
        //create some fake config sets
        this.createFakeDataSet();
    }

    private void createFakeDataSet()
    {
        //create some fake Cmiu config set
        CmiuConfigSet configSet1 = new CmiuConfigSet("Config set 1");
        configSet1.setId(1);

        configSet1.setRecordingStartTimeMins(1);
        configSet1.setRecordingIntervalMins(3600);

        configSet1.setReportingIntervalMins(300);
        configSet1.setReportingNumberOfRetries(3);
        configSet1.setReportingQuietStartMins(1);
        configSet1.setReportingQuietEndMins(123);

        this.cmiuConfigSetList.add(configSet1);

        CmiuConfigSet configSet2 = new CmiuConfigSet("Config set 2");
        configSet2.setId(2);

        configSet2.setRecordingStartTimeMins(1);
        configSet2.setRecordingIntervalMins(7200);

        configSet2.setReportingIntervalMins(600);
        configSet2.setReportingNumberOfRetries(6);
        configSet2.setReportingQuietStartMins(5);
        configSet2.setReportingQuietEndMins(321);

        this.cmiuConfigSetList.add(configSet2);

        //create some fake cmiu config mgmt
        CmiuConfigMgmt configMgmt1 = new CmiuConfigMgmt(1);
        configMgmt1.setCurrentConfig(configSet1);
        configMgmt1.setDefaultConfig(configSet1);
        configMgmt1.setReportedConfig(configSet1);
        configMgmt1.setReportedConfigDate(Instant.now());

        CmiuConfigMgmt configMgmt2 = new CmiuConfigMgmt(2);
        configMgmt2.setCurrentConfig(configSet2);
        configMgmt2.setDefaultConfig(configSet1);
        configMgmt2.setReportedConfig(configSet1);
        configMgmt2.setReportedConfigDate(Instant.now());

        this.cmiuConfigMgmtMap.put(1, configMgmt1);
        this.cmiuConfigMgmtMap.put(2, configMgmt2);

    }

    /**
     * Get the CMIU config
     *
     * @param cmiuId the cmiu id
     * @return Status counts
     * @throws InternalApiException If something goes wrong in the request
     */
    @Override
    @RequestMapping(URL_CMIU_CONFIG)
    public CmiuConfigMgmt getCmiuConfigMgmt(@RequestParam(PARAM_CMIU_ID) long cmiuId) throws InternalApiException
    {
        return this.cmiuConfigMgmtMap.getOrDefault(cmiuId, null);
    }

    /**
     * Change the CMIU configuration.
     *
     * @param newCmiuConfigChangeList list of CMIU containing new CMIU configurations to be persisted to database
     * @return
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(value = URL_CMIU_CONFIG, method = RequestMethod.POST)
    public CmiuConfigMgmt addCmiuConfig(@RequestParam(value = CmiuConfigMgmtService.PARAM_USER) String userName, @RequestBody List<CmiuConfigChange> newCmiuConfigChangeList) throws InternalApiException
    {
        for (CmiuConfigChange config : newCmiuConfigChangeList)
        {
            //check whether current CmiuConfigMgmt entries is present for the CmiuId
            CmiuConfigMgmt configMgmt = this.cmiuConfigMgmtMap.getOrDefault(config.getCmiuId(), null);
            CmiuConfigSet selectedConfigSet = this.cmiuConfigSetList.stream()
                    .filter(c -> c.getId() == config.getConfigSetId()).findFirst().get();

            if (configMgmt == null)
            {
                //create a new configuration management for the CMIU
                configMgmt = new CmiuConfigMgmt(config.getCmiuId());
                this.cmiuConfigMgmtMap.put(config.getCmiuId(), configMgmt);
                configMgmt.setReportedConfig(this.cmiuConfigSetList.get(0));    //set the first config set as report config
                configMgmt.setReportedConfigDate(Instant.now());
            }

            configMgmt.setCurrentConfig(selectedConfigSet);
            configMgmt.setPlannedConfigApplyDate(Instant.now());

//            configMgmt.setPlannedConfig(selectedConfigSet);
//            configMgmt.setPlannedConfigApplyDate(Instant.now());

            //add an entry in config history
            CmiuConfigHistory configHistory = new CmiuConfigHistory();
            configHistory.setMiuId(config.getCmiuId());
            configHistory.setDateTime(Instant.now());
            configHistory.setChangeNote(config.getChangeNote());

            this.cmiuConfigHistory.add(configHistory);

        }

        return null;
    }

    /**
     * Get a list of cmiu config mgmt entries representing CMIU config commands that are being schedule for sending to CMIU.
     * This is done by comparing the configSet value of current and reported config set regardless of the config set id. If they are different,
     * then a config change has been made and is awaiting confirmation from the CMIU that it has been implemented in the hardware.
     * @return list of CMIU which are awaiting confirmation of changes to their config.
     */
    @Override
    @RequestMapping(value = URL_CMIU_CONFIG_CHANGE_IN_PROCESS)
    public List<CmiuConfigMgmt> getCmiuConfigChangeInProcessList()
    {
        return this.cmiuConfigMgmtMap.entrySet().stream()
                .filter(item -> !item.getValue().getCurrentConfig().isEqual(item.getValue().getReportedConfig()))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }



    /**
     * Get a list of cmiu config mgmt entries representing CMIU config commands that has been changed and acknowledged (reported) by the CMIU
     * for the last X days.
     * This filters the Cmiu config mgmt table to find reported config which has been changed in the last X days and matches current config.
     *
     * @param changedDaysAgo the number of days ago the changes has been made to the reported config
     * @return filtered list of cmiu mgmt table
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(value = URL_CMIU_CONFIG_CHANGE_COMPLETED)
    public List<CmiuConfigMgmt> getCmiuConfigChangeCompletedList(@RequestParam(PARAM_CONFIG_SET_LAST_CHANGED_DAYS_AGO) int changedDaysAgo) throws InternalApiException
    {
        Instant filterMinDate = Instant.now().minus(changedDaysAgo, ChronoUnit.DAYS);
        List<CmiuConfigMgmt> result = this.cmiuConfigMgmtMap.entrySet().stream()
                .filter(item -> item.getValue().getCurrentConfig().isEqual(item.getValue().getReportedConfig()))    //report config is same as current config
                .filter(item -> Instant.ofEpochMilli(item.getValue().getReportedConfigDate().toEpochMilli()).isAfter(filterMinDate))  //reported config is less than changeDaysAgo days old
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());

        // Simulate CMIU has committed the changes to current config, set report config to be same as  current config.
        this.cmiuConfigMgmtMap.forEach((k, v) ->
        {
            if (v.getCurrentConfig().getId() != v.getReportedConfig().getId())
            {
                v.setReportedConfig(v.getCurrentConfig());
                v.setReportedConfigDate(Instant.now());
            }
        });

        return result;
    }

    @Override
    @RequestMapping(value = URL_CMIU_CONFIG_HISTORY)
    @ResponseBody
    public List<CmiuConfigHistory> getCmiuConfigHistory(@RequestParam(PARAM_CMIU_ID) long cmiuId)
    {
        return this.cmiuConfigHistory.stream().filter(c -> c.getMiuId() == cmiuId).collect(Collectors.toList());
    }

    /**
     * Get a list of config sets
     *
     * @return
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(URL_CMIU_CONFIG_SET_LIST)
    public List<CmiuConfigSet> getCmiuConfigSetList() throws InternalApiException
    {
        return this.cmiuConfigSetList;
    }

    /**
     * Get a config set from id
     *
     * @param configSetId the configset id
     * @return
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(URL_CMIU_CONFIG_SET)
    public CmiuConfigSet getCmiuConfigSet(@RequestParam(value = PARAM_CONFIG_SET_ID) Long configSetId) throws InternalApiException
    {
        return this.cmiuConfigSetList.stream().filter(e -> e.getId() == configSetId).findFirst().get();
    }

    /**
     * Add a new CMIU config set to database
     * @param cmiuConfigSet new config set definition
     * @return
     * @throws InternalApiException
     * @throws JsonProcessingException
     */
    @Override
    @RequestMapping(value = URL_CMIU_CONFIG_SET, method = RequestMethod.POST)
    public void addCmiuConfigSet(@RequestParam(value = CmiuConfigSetService.PARAM_USER) String userName, @RequestBody CmiuConfigSet cmiuConfigSet) throws InternalApiException, JsonProcessingException
    {
        //add to list
        cmiuConfigSet.setId(Instant.now().getEpochSecond());  //temporarily assign an id
        this.cmiuConfigSetList.add(cmiuConfigSet);
    }

    /**
     * Edit an existing CMIU config set
     *
     * @param modifiedConfigSet new config set definition
     * @return
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(value = URL_CMIU_CONFIG_SET_EDIT, method = RequestMethod.POST)
    public void editCmiuConfigSet(@RequestParam(value = CmiuConfigSetService.PARAM_USER) String userName, @RequestBody CmiuConfigSet modifiedConfigSet) throws InternalApiException, JsonProcessingException
    {
        //add to list
        CmiuConfigSet configSet = this.cmiuConfigSetList.stream()
                .filter(c -> c.getId() == modifiedConfigSet.getId())
                .findFirst()
                .get();

        if (configSet == null)
        {
            throw new InternalApiException("Cmiu Config Set id not found id=" + modifiedConfigSet.getId());
        }

        configSet.setName(modifiedConfigSet.getName());

        configSet.setReportingIntervalMins(modifiedConfigSet.getReportingIntervalMins());
        configSet.setReportingQuietStartMins(modifiedConfigSet.getReportingQuietStartMins());
        configSet.setReportingQuietEndMins(modifiedConfigSet.getReportingQuietEndMins());
        configSet.setReportingStartMins(modifiedConfigSet.getReportingStartMins());
        configSet.setReportingNumberOfRetries(modifiedConfigSet.getReportingNumberOfRetries());

        configSet.setRecordingIntervalMins(modifiedConfigSet.getRecordingIntervalMins());
        configSet.setRecordingStartTimeMins(modifiedConfigSet.getRecordingStartTimeMins());
    }

}
