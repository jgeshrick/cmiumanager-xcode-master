/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.dev.dummyservice.fixed;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.common.internal.site.model.SiteForUser;
import com.neptunetg.mdce.common.internal.user.model.LoginCredentials;
import com.neptunetg.mdce.common.internal.user.model.LoginResult;
import com.neptunetg.mdce.common.internal.user.model.MdceUserRole;
import com.neptunetg.mdce.common.internal.user.model.ModifiedUserDetails;
import com.neptunetg.mdce.common.internal.user.model.NewUserDetails;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequest;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequestClaim;
import com.neptunetg.mdce.common.internal.user.model.UserAlertSettings;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import com.neptunetg.mdce.common.internal.user.service.UserService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Fake implementation of user service
 */
@RestController
@RequestMapping("/dummyservice/fixed")
public class FixedUserService implements UserService
{
    private final Map<Long, UserDetails> userDetailsList = new HashMap<>();
    private AtomicInteger id = new AtomicInteger(1);

    public FixedUserService()
    {
        createAndAddUser("admin", MdceUserRole.MsocAdmin);
        createAndAddUser("msoc-operator", MdceUserRole.MsocOperator);
        createAndAddUser("site-operator", MdceUserRole.SiteOperator);
        createAndAddUser("distributor", MdceUserRole.Distributor);
        createAndAddUser("master-of-cellular", MdceUserRole.MsocMasterOfCellular);
    }

    @Override
    public boolean addUser(@RequestBody NewUserDetails newUserDetails) throws InternalApiException
    {
        createAndAddUser(newUserDetails.getUserName(), newUserDetails.getUserLevel());
        return true;
    }

    @Override
    public void removeUser(@RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException
    {
        this.userDetailsList.remove((long) modifiedUserDetails.getUserId());
    }

    @Override
    @RequestMapping(value=URL_USER_LIST, method=RequestMethod.GET)
    public List<UserDetails> listUsers() throws InternalApiException
    {
        return this.userDetailsList.entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    @Override
    public void resetPassword(@RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException
    {
        //do nothing
    }

    @Override
    public List<SiteDetails> getSitesForUser(@RequestBody int userId) throws InternalApiException
    {
        List<SiteDetails> siteDetailsList = new ArrayList<>();
        siteDetailsList.add(new SiteDetails(1, "site-1", "Joe Blow", "AL"));
        siteDetailsList.add(new SiteDetails(2, "site-2", "Joe Blow", "AL"));

        return siteDetailsList;
    }

    @Override
    public List<SiteDetails> getSitesNotForUser(@RequestBody int userId) throws InternalApiException
    {
        List<SiteDetails> siteDetailsList = new ArrayList<>();
        siteDetailsList.add(new SiteDetails(3, "site-3", "Joe Blow", "AL"));
        siteDetailsList.add(new SiteDetails(4, "site-4", "Joe Blow", "AL"));

        return siteDetailsList;
    }

    @Override
    public boolean addSiteToUser(@RequestBody SiteForUser siteForUser) throws InternalApiException
    {
        return false;
    }

    @Override
    public boolean removeSiteFromUser(@RequestBody SiteForUser siteForUser) throws InternalApiException
    {
        return false;
    }

    @Override
    @RequestMapping(value=URL_USER_VERIFY, method= RequestMethod.POST)
    public LoginResult verifyUser(@RequestBody LoginCredentials loginCredentials) throws InternalApiException
    {
        boolean isUsernameMatched = this.userDetailsList.entrySet()
                .stream()
                .anyMatch(longUserDetailsEntry -> longUserDetailsEntry.getValue().getUserName().equals(loginCredentials.getUserName()));

        return isUsernameMatched ? LoginResult.OK : LoginResult.LOGIN_FAILED;
    }

    @Override
    public UserDetails getUserDetails(@PathVariable("username") String userName) throws InternalApiException
    {
        Optional<UserDetails> ret = this.userDetailsList.entrySet()
                .stream()
                .filter(longUserDetailsEntry -> longUserDetailsEntry.getValue().getUserName().equals(userName))
                .map(Map.Entry::getValue)
                .findFirst();

        return ret.isPresent()? ret.get():null;
    }

    @Override
    public List<UserDetails> findUsers(@RequestBody Map<String, Object> options) throws InternalApiException
    {
        return null;
    }

    @Override
    public boolean setUserDetails(@PathVariable("username") String s, @RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException
    {
        return false;
    }

    @Override
    public UserAlertSettings getUserAlertSettings(@PathVariable("userid") int userId) throws InternalApiException
    {
        UserAlertSettings alertSettings = new UserAlertSettings();
        alertSettings.setUserId(userId);
        alertSettings.setCmiuError(true);
        alertSettings.setCmiuWarning(true);
        alertSettings.setSystemError(true);
        alertSettings.setSystemWarning(true);

        return alertSettings;
    }

    @Override
    public boolean setUserAlertSettings(@RequestBody UserAlertSettings userAlertSettings) throws InternalApiException
    {
        return true;
    }

    @Override
    public boolean addPasswordResetRequest(@RequestBody PasswordResetRequest passwordResetRequest) throws InternalApiException
    {
        return false;
    }

    @Override
    public boolean claimPasswordResetRequest(@PathVariable("token") String token, @RequestBody PasswordResetRequestClaim passwordResetRequestClaim) throws InternalApiException
    {
        return false;
    }

    @Override
    public int expirePasswordResetRequestsByUser(@RequestParam(USER_ID_PARAM) long userId) throws InternalApiException
    {
        return 0;
    }

    @Override
    public PasswordResetRequest getPasswordResetRequestByToken(String token) throws InternalApiException
    {
        return null;
    }

    @Override
    public List<UserDetails> getUserDetailsByEmail(@RequestParam(name = "email") String s) throws InternalApiException
    {
        return null;
    }

    private long createAndAddUser(String user, MdceUserRole role)
    {
        long id = this.id.incrementAndGet();

        UserDetails newUser = new UserDetails();
        newUser.setUserId(id);
        newUser.setUserName(user);
        newUser.setUserLevel(role);
        newUser.setEmail(user + "@email.com");

        this.userDetailsList.put(id, newUser);

        return id;

    }
}
