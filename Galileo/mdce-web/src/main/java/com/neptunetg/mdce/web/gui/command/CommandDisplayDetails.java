/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.gui.command;

import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandSummary;

import java.time.Instant;

/**
 * A specialized command summary model that can indicate whether the previous row is for a command to the same MIU.
 */
public class CommandDisplayDetails
{
    private long commandId;
    private long miuId;
    private String user;
    private String commandType;
    private String details;
    private Instant commandEnteredTime;
    private Instant commandFulfilledTime;
    private boolean isSameMiuAsPrevious;
    private long commandCount;
    private String networkProvider;

    public static CommandDisplayDetails fromCommandSummary(CommandSummary commandSummary, CommandSummary previousCommandSummary)
    {
        CommandDisplayDetails commandDisplayDetails = new CommandDisplayDetails();
        commandDisplayDetails.setMiuId(commandSummary.getMiuId());
        commandDisplayDetails.setCommandId(commandSummary.getCommandId());
        commandDisplayDetails.setCommandId(commandSummary.getCommandId());
        commandDisplayDetails.setCommandType(commandSummary.getCommandType());
        commandDisplayDetails.setCommandEnteredTime(commandSummary.getCommandEnteredTime());
        commandDisplayDetails.setCommandFulfilledTime(commandSummary.getCommandFulfilledTime());
        commandDisplayDetails.setDetails(commandSummary.getDetails());
        commandDisplayDetails.setUser(commandSummary.getUser());
        commandDisplayDetails.setNetworkProvider(commandSummary.getNetworkProvider());

        boolean isSameMiuAsPrevious = previousCommandSummary != null &&
                commandSummary.getMiuId() == previousCommandSummary.getMiuId();

        commandDisplayDetails.setIsSameMiuAsPrevious(isSameMiuAsPrevious);
        commandDisplayDetails.setCommandCount(0);

        return commandDisplayDetails;
    }

    public long getCommandId()
    {
        return commandId;
    }

    public void setCommandId(long commandId)
    {
        this.commandId = commandId;
    }

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getCommandType()
    {
        return commandType;
    }

    public void setCommandType(String commandType)
    {
        this.commandType = commandType;
    }

    public String getDetails()
    {
        return details;
    }

    public void setDetails(String details)
    {
        this.details = details;
    }

    public Instant getCommandEnteredTime() { return commandEnteredTime; }

    public void setCommandEnteredTime(Instant commandEnteredTime) { this.commandEnteredTime = commandEnteredTime; }

    public Instant getCommandFulfilledTime() { return commandFulfilledTime; }

    public void setCommandFulfilledTime(Instant commandFulfilledTime) { this.commandFulfilledTime = commandFulfilledTime; }

    public boolean getIsSameMiuAsPrevious()
    {
        return isSameMiuAsPrevious;
    }

    public void setIsSameMiuAsPrevious(boolean sameMiuAsPrevious)
    {
        isSameMiuAsPrevious = sameMiuAsPrevious;
    }

    public long getCommandCount()
    {
        return commandCount;
    }

    public void setCommandCount(long commandCount)
    {
        this.commandCount = commandCount;
    }

    public String getNetworkProvider()
    {
        return networkProvider;
    }

    public void setNetworkProvider(String networkProvider)
    {
        this.networkProvider = networkProvider;
    }
}
