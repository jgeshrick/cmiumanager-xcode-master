/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.dev.dummyservice.fixed;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.CmiuDetails;
import com.neptunetg.mdce.common.internal.miu.service.CmiuDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Stubbed implementation of CmiuDetailsService
 */
@RestController
public class FixedDataCmiuDetailsServiceController implements CmiuDetailsService
{
    private static final String DUMMY_URL_CMIU_BY_ACCOUNT = "/dummyservice/fixed" + URL_CMIU_BY_ACCOUNT;


    /**
     * Get the MSR for a group
     *
     * @param accountId ID uniquely identifying group of endpoints
     * @return Status counts
     * @throws InternalApiException If something goes wrong in the request
     */
    @RequestMapping(DUMMY_URL_CMIU_BY_ACCOUNT)
    @Override
    public List<CmiuDetails> getCmiuByAccount(@RequestParam(PARAM_CMIU_ACCOUNT_ID) String accountId) throws InternalApiException
    {
        List<CmiuDetails> cmiuDetailsList = new ArrayList<>();

        //temporary create fake CmiuDetails and add to list
        for(int i=1; i<11;i++)
        {
            CmiuDetails cmiuDetails = new CmiuDetails((long) i);
            cmiuDetails.setTowerId(accountId);    //todo now temporary use account name as towerid, to show table changing, to be fixed

            cmiuDetailsList.add(cmiuDetails);
        }

        return cmiuDetailsList;
    }
}
