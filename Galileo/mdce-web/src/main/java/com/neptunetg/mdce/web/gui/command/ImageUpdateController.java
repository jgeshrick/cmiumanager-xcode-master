/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.command;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import com.neptunetg.mdce.common.internal.miu.service.MiuCommandControlService;
import com.neptunetg.mdce.common.internal.miu.service.CmiuImageUpdateService;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import com.neptunetg.mdce.web.gui.config.MiuInput;
import com.neptunetg.mdce.web.gui.config.ImageUpdateForm;
import com.neptunetg.mdce.web.gui.config.ImportMultipleCmiuConfigSetForm;
import com.neptunetg.mdce.web.gui.config.RegisterNewImageForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.neptunetg.mdce.web.gui.login.LoginUtils.getLoggedInUserName;

/**
 * GUI frontend for image update workflow
 */
@Controller
@RequestMapping("/pages/config/image-update")
@SessionAttributes({"savedImageUpdateForm"})
public class ImageUpdateController
{

    private static final Logger logger = LoggerFactory.getLogger(ImageUpdateController.class);

    @Autowired
    private BackEndManager backEndManager;

    /**
     * Construct a list of image type for populating html select element
     *
     * @return
     */
    @ModelAttribute("imageVersions")
    public Map<String, List<ImageDescription>> getImageVersions() throws InternalApiException
    {
        final CmiuImageUpdateService service = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuImageUpdateService.class);
        final List<ImageDescription> imageList = service.getAvailableImageList();

        //Sort the artifact, latest version at the top.
        final Comparator<ImageDescription> byArtifactVersion = ImageUpdateController::compareVersionNumbers;

        final Map<String, List<ImageDescription>> imageVersions = imageList.stream()
                .sorted(byArtifactVersion)
                .collect(Collectors.groupingBy(id -> id.getImageType().name()));


        for (Map.Entry<String, List<ImageDescription>> entry : imageVersions.entrySet())
        {
            try
            {
                String previousVersion = null;
                for (Iterator<ImageDescription> i = entry.getValue().iterator(); i.hasNext(); )
                {
                    final String currentVersion = i.next().getVersion();
                    if (ObjectUtils.nullSafeEquals(currentVersion, previousVersion))
                    {
                        i.remove();
                    }
                    previousVersion = currentVersion;
                }
            }
            catch (Exception e)
            {
                logger.error("Failed to dedup images for " + entry.getKey(), e);
            }
        }
        return imageVersions;
    }

    /**
     * Returns -1 if v1 > v2
     * @param v1
     * @param v2
     * @return
     */
    private static int compareVersionNumbers(ImageDescription v1, ImageDescription v2)
    {
        try
        {
            int ret = Integer.compare(v2.getRevisionMajor(), v1.getRevisionMajor());
            if (ret != 0)
            {
                return ret;
            }
            ret = Integer.compare(v2.getRevisionMinor(), v1.getRevisionMinor());
            if (ret != 0)
            {
                return ret;
            }
            ret = Integer.compare(v2.getRevisionDate(), v1.getRevisionDate());
            if (ret != 0)
            {
                return ret;
            }
            ret = Integer.compare(v2.getRevisionBuildNum(), v1.getRevisionBuildNum());
            if (ret != 0)
            {
                return ret;
            }
        }
        catch (Exception e)
        {
            logger.error("Error during version number comparison of " + v1 + " and " + v2, e);
        }
        return v2.getVersion().compareTo(v1.getVersion());
    }

    @ModelAttribute("savedImageUpdateForm")
    public ImageUpdateForm getImageUpdateForm()
    {
        return new ImageUpdateForm();
    }


    /**
     * Load image update specification page
     */
    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String loadImageUpdateView(@ModelAttribute("savedImageUpdateForm") ImageUpdateForm savedImageUpdateForm,
                                      Model model)
    {
        ImageUpdateForm imageUpdateForm = this.getImageUpdateForm();
        imageUpdateForm.setMiuInputList(savedImageUpdateForm.getMiuInputList());

        model.addAttribute("imageUpdateForm", imageUpdateForm);

        return "updateimage/imageUpdate.jsp";
    }

    /**
     * Submit a image update request to the backend
     *
     * @param imageUpdateForm backing form contain list of cmiu and image type, version
     * @param bindResult      used for validation
     * @param sessionStatus   session status to be cleared when the request is successfully processed
     * @param ra              redirectAttributes added to prevent parameters to be passed to redirect (default) http://stackoverflow.com/questions/2543797/spring-redirect-after-post-even-with-validation-errors
     * @return redirect to pending change page
     */
    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "", method = RequestMethod.POST, params = "updateCmiuImage")
    public String updateCmiuImage(
            @ModelAttribute("imageUpdateForm") ImageUpdateForm imageUpdateForm,
            BindingResult bindResult,
            SessionStatus sessionStatus,
            RedirectAttributes ra) throws InternalApiException
    {
        //validate CMIU entries
        CmiuInputListFormValidator formValidator = new CmiuInputListFormValidator();
        formValidator.validate(imageUpdateForm, bindResult);

        //validate image entry
        validateImageSelection(imageUpdateForm, bindResult);

        if (bindResult.hasErrors())
        {
            return "updateimage/imageUpdate.jsp";
        }

        //validation ok, prepare to send command
        final CmiuImageUpdateService imageUpdateService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuImageUpdateService.class);
        List<ImageDescription> availableImageList = imageUpdateService.getAvailableImageList();

        final MiuCommandControlService commandService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MiuCommandControlService.class);

        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(getLoggedInUserName());
        commandRequest.setCommandCmiu(CommandRequest.CommandTypeCmiu.COMMAND_UPDATE_IMAGE);

        long executionTime = Instant.now().getEpochSecond();
        commandRequest.setExecutionTimeEpochSecond(executionTime);  //fixme, change to user configurable time

        List<ImageDescription> imageDescriptionList = new ArrayList<>();

        if (imageUpdateForm.isUpdateFirmware())
        {
            ImageDescription image = availableImageList.stream()
                    .filter(i -> i.getImageType() == ImageDescription.ImageType.FirmwareImage
                            && i.getFileName().equals(imageUpdateForm.getFirmwareImageVersion()))
                    .findFirst()
                    .get();

            imageDescriptionList.add(image);
        }

        if (imageUpdateForm.isUpdateBootloader())
        {
            ImageDescription image = availableImageList.stream()
                    .filter(i -> i.getImageType() == ImageDescription.ImageType.BootloaderImage
                            && i.getFileName().equals(imageUpdateForm.getBootloaderImageVersion()))
                    .findFirst()
                    .get();

            imageDescriptionList.add(image);
        }

        if (imageUpdateForm.isUpdateARBConfig())
        {
            ImageDescription image = availableImageList.stream()
                    .filter(i -> i.getImageType() == ImageDescription.ImageType.ArbConfigImage
                            && i.getFileName().equals(imageUpdateForm.getArbImageVersion()))
                    .findFirst()
                    .get();

            imageDescriptionList.add(image);
        }

        if (imageUpdateForm.isUpdateBleConfig())
        {
            ImageDescription image = availableImageList.stream()
                    .filter(i -> i.getImageType() == ImageDescription.ImageType.BleConfigImage
                            && i.getFileName().equals(imageUpdateForm.getBleConfigImageVersion()))
                    .findFirst()
                    .get();

            imageDescriptionList.add(image);
        }

        if (imageUpdateForm.isUpdateConfig())
        {
            ImageDescription image = availableImageList.stream()
                    .filter(i -> i.getImageType() == ImageDescription.ImageType.ConfigImage
                            && i.getFileName().equals(imageUpdateForm.getConfigImageVersion()))
                    .findFirst()
                    .get();

            imageDescriptionList.add(image);
        }

        if (imageUpdateForm.isUpdateEncryption())
        {
            ImageDescription image = availableImageList.stream()
                    .filter(i -> i.getImageType() == ImageDescription.ImageType.EncryptionImage
                            && i.getFileName().equals(imageUpdateForm.getEncryptionImageVersion()))
                    .findFirst()
                    .get();

            imageDescriptionList.add(image);
        }

        commandRequest.setImageDescription(imageDescriptionList);

        //add list of CMIU
        commandRequest.setMiuList(imageUpdateForm.getMiuInputList().stream()
                .map(MiuInput::getMiuId)
                .filter(cmiuId -> StringUtils.hasText(cmiuId))
                .map(Long::valueOf)
                .collect(Collectors.toList()));

        commandService.sendCommand(commandRequest);

        sessionStatus.setComplete();
        return "redirect:/pages/config";
    }

    /**
     * Register a new image, persisting the value in the backing form (CMIU list, image selection and version).
     *
     * @param imageUpdateForm The backing form submitted in the post request
     * @return
     */
    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "", method = RequestMethod.POST, params = "registerNewImage")
    public String requestRegisterNewImage(
            @ModelAttribute("imageUpdateForm") ImageUpdateForm imageUpdateForm, //pass in to retain input values within the session
            BindingResult bindResult,
            SessionStatus sessionStatus,
            RedirectAttributes ra)
    {
        return "redirect:/pages/config/image-update/register-new";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "", method = RequestMethod.POST, params = "appendCmiuList")
    public String batchAddCmiuRequest(@ModelAttribute("imageUpdateForm") ImageUpdateForm imageUpdateForm,
                                      BindingResult bindResult,
                                      SessionStatus sessionStatus,
                                      RedirectAttributes ra,
                                      Model model)
    {
        //save image update form
        ImageUpdateForm saveImageUpdateForm = this.getImageUpdateForm();
        saveImageUpdateForm.setMiuInputList(imageUpdateForm.getMiuInputList());

        model.addAttribute("savedImageUpdateForm", saveImageUpdateForm);

        return "redirect:/pages/config/image-update/batch-add-cmiu";
    }

    /**
     * View model for generating a list of CMIU id based on a CSV? file and mass config change
     * @return
     * @throws InternalApiException
     */
    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping(value="batch-add-cmiu", method = RequestMethod.GET)
    public String prepareImportCmiuConfigFile(Model model, HttpServletRequest request) throws InternalApiException
    {
        ImportMultipleCmiuConfigSetForm form = new ImportMultipleCmiuConfigSetForm();
        form.setRedirectUrl(request.getHeader("Referer"));

        model.addAttribute("importCmiuFileForm", form);

        return "cmiuconfig/batchAddCmiu.jsp";
    }

    /**
     * Accept a file containing a list of CMIU, parse and add to existing CMIU config list.
     * @param uploadForm Form backing object containing the CMIU file, selected config set and change note
     * @return redirect to Cmiu Config page
     * @throws IOException
     */
    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value="batch-add-cmiu", method = RequestMethod.POST)
    public String importCmiuListFile(@ModelAttribute("importCmiuFileForm") ImportMultipleCmiuConfigSetForm uploadForm,
                                       @ModelAttribute("savedImageUpdateForm") ImageUpdateForm savedImageUpdateForm) throws IOException, InternalApiException
    {
        final MiuCommandControlService commandService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MiuCommandControlService.class);

        MultipartFile file = uploadForm.getCmiuListDefFile();
        if (!file.isEmpty())
        {
            //currently assuming cmiu file contains list of CMIU, each CMIU in one line
            InputStream inputStream = file.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            //read each line of CMIU id and add to current cmiuConfigCommandList
            while ((line = bufferedReader.readLine()) != null)
            {
                MiuInput description = new MiuInput();
                description.setMiuId(line.trim());

                String cmiuMetaData = commandService.getCmiuDescription(Long.valueOf(description.getMiuId())).getMetaData();
                description.setMetaData(cmiuMetaData);

                savedImageUpdateForm.getMiuInputList().add(description);
            }
        }

        //remove first entry if it is a null entry. One entry is required for dynamic list to work
        if (savedImageUpdateForm.getMiuInputList().size() > 1 &&
                savedImageUpdateForm.getMiuInputList().get(0).getMiuId() == null)
        {
            savedImageUpdateForm.getMiuInputList().remove(0);
        }

        return "redirect:" + uploadForm.getRedirectUrl();
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "register-new", method = RequestMethod.GET)
    public String showRegisterNewImageView(Model model) throws InternalApiException
    {
        final CmiuImageUpdateService imageService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuImageUpdateService.class);
        Map<ImageDescription.ImageType, List<String>> artifactList = imageService.getBuildArtifactList(ImageDescription.ImageType.BootloaderImage);

        //exclude bootloader
        artifactList.remove(ImageDescription.ImageType.BootloaderImage);

        RegisterNewImageForm registerNewImageForm = new RegisterNewImageForm();

        List<ImageDescription.ImageType> imageTypes = ImageDescription.ImageType.getList();
        imageTypes.remove(ImageDescription.ImageType.BootloaderImage);

        model.addAttribute("artifactList", artifactList);
        model.addAttribute("imageTypeList", imageTypes);
        model.addAttribute("registerNewImageForm", registerNewImageForm);

        return "updateimage/registerNewImage.jsp";
    }

    /**
     * Register an image from s3
     */
    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "register-new", method = RequestMethod.POST)
    public String registerNewImage(@ModelAttribute("registerNewImageForm") RegisterNewImageForm registerNewImageForm) throws InternalApiException
    {
        final CmiuImageUpdateService imageService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuImageUpdateService.class);
        imageService.registerBuildArtifact(registerNewImageForm.getImageType(), registerNewImageForm.getSelectedImageSource());
        return "redirect:/pages/config/image-update";
    }


    private static void validateImageSelection(ImageUpdateForm imageUpdateForm, BindingResult bindingResult)
    {
        //ensure at least one image has been selected
        if (!imageUpdateForm.isUpdateFirmware() &&
                !imageUpdateForm.isUpdateConfig() &&
                !imageUpdateForm.isUpdateARBConfig() &&
                !imageUpdateForm.isUpdateBleConfig() &&
                !imageUpdateForm.isUpdateEncryption())
        {
            bindingResult.rejectValue("imageSelected", "no.image.selected", "No image is selected. Please select at least one valid image");
        }

        // ensure that if the image is checked, there is a valid image version
        validateSelectedImageHasCorrectVersion("updateFirmware", imageUpdateForm.isUpdateFirmware(), imageUpdateForm.getFirmwareImageVersion(), bindingResult);
        validateSelectedImageHasCorrectVersion("updateConfig", imageUpdateForm.isUpdateConfig(), imageUpdateForm.getConfigImageVersion(), bindingResult);
        validateSelectedImageHasCorrectVersion("updateARBConfig", imageUpdateForm.isUpdateARBConfig(), imageUpdateForm.getArbImageVersion(), bindingResult);
        validateSelectedImageHasCorrectVersion("updateBleConfig", imageUpdateForm.isUpdateBleConfig(), imageUpdateForm.getBleConfigImageVersion(), bindingResult);
        validateSelectedImageHasCorrectVersion("updateEncryption", imageUpdateForm.isUpdateEncryption(), imageUpdateForm.getEncryptionImageVersion(), bindingResult);

    }

    private static boolean validateSelectedImageHasCorrectVersion( String fieldName, boolean imageChecked, String selectedValue, BindingResult bindingResult)
    {
        if (imageChecked && selectedValue.equals("-"))
        {
            bindingResult.rejectValue(fieldName, "no.image.selected", "Please select a valid image version.");
            return false;
        }

        return true;
    }
}
