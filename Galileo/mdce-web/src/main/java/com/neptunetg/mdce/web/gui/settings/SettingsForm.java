/*
 *  **************************************************************************
 *
 *      Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 */

package com.neptunetg.mdce.web.gui.settings;

import com.neptunetg.mdce.common.internal.settings.service.MdceSettingsService;

import javax.validation.constraints.Min;

/**
 * Settings change
 */
public class SettingsForm
{
    @Min(MdceSettingsService.OLD_TABLE_MIN_AGE_MONTHS_TO_DELETE_MIN_VALUE)
    private long minAgeToDeleteMonths;

    @Min(MdceSettingsService.MAX_LOGIN_ATTEMPTS_MIN_VALUE)
    private long maxLoginAttempts;

    @Min(MdceSettingsService.LOCKED_ACCOUNT_TIMEOUT_MINUTES_MIN_VALUE)
    private long lockedAccountTimeoutMinutes;

    public long getMinAgeToDeleteMonths()
    {
        return minAgeToDeleteMonths;
    }

    public void setMinAgeToDeleteMonths(long minAgeToDeleteMonths)
    {
        this.minAgeToDeleteMonths = minAgeToDeleteMonths;
    }

    public long getMaxLoginAttempts()
    {
        return maxLoginAttempts;
    }

    public void setMaxLoginAttempts(long maxLoginAttempts)
    {
        this.maxLoginAttempts = maxLoginAttempts;
    }

    public long getLockedAccountTimeoutMinutes()
    {
        return lockedAccountTimeoutMinutes;
    }

    public void setLockedAccountTimeoutMinutes(long lockedAccountTimeoutMinutes)
    {
        this.lockedAccountTimeoutMinutes = lockedAccountTimeoutMinutes;
    }
}
