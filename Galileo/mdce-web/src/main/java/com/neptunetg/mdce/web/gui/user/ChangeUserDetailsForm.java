/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.user;

import com.neptunetg.mdce.common.internal.user.model.MdceUserRole;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Backing form for the User Details editor.
 */
public class ChangeUserDetailsForm
{
    private long userId;

    @Size(min = 3, max = 30, message = "User name must have at least 3 characters")
    private String userName;

    private String previousUserName;

    private MdceUserRole userLevel;

    @NotEmpty(message = "Please enter your email address")
    @Email(message = "Invalid email format")
    private String email;

    private boolean isCurrentlyDisabled;

    private boolean showDisabledCheckbox;

    private boolean isAccountLockoutEnabled;

    private boolean allowConcurrentSessions;

    public ChangeUserDetailsForm()
    {
    }

    public ChangeUserDetailsForm(UserDetails userDetails)
    {
        this.userId = userDetails.getUserId();
        this.setPreviousUserName(userDetails.getUserName());
        this.userName = userDetails.getUserName();
        this.userLevel = userDetails.getUserLevel();
        this.email = userDetails.getEmail();
        this.isCurrentlyDisabled = userDetails.isCurrentlyDisabled();
        this.showDisabledCheckbox = userDetails.isCurrentlyDisabled();
        this.isAccountLockoutEnabled = userDetails.isAccountLockoutEnabled();
        this.allowConcurrentSessions = userDetails.getAllowConcurrentSessions();
    }

    public long getUserId()
    {
        return userId;
    }

    public void setUserId(long userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPreviousUserName()
    {
        return previousUserName;
    }

    public void setPreviousUserName(String previousUserName)
    {
        this.previousUserName = previousUserName;
    }

    public MdceUserRole getUserLevel()
    {
        return userLevel;
    }

    public void setUserLevel(MdceUserRole userLevel)
    {
        this.userLevel = userLevel;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public boolean isCurrentlyDisabled()
    {
        return isCurrentlyDisabled;
    }

    public void setCurrentlyDisabled(boolean currentlyDisabled)
    {
        isCurrentlyDisabled = currentlyDisabled;
    }

    public boolean isAccountLockoutEnabled()
    {
        return isAccountLockoutEnabled;
    }

    public void setAccountLockoutEnabled(boolean accountLockoutEnabled)
    {
        isAccountLockoutEnabled = accountLockoutEnabled;
    }

    public boolean isShowDisabledCheckbox()
    {
        return showDisabledCheckbox;
    }

    public void setShowDisabledCheckbox(boolean showDisabledCheckbox)
    {
        this.showDisabledCheckbox = showDisabledCheckbox;
    }

    public boolean getAllowConcurrentSessions()
    {
        return allowConcurrentSessions;
    }

    public void setAllowConcurrentSessions(boolean allowConcurrentSessions)
    {
        this.allowConcurrentSessions = allowConcurrentSessions;
    }
}
