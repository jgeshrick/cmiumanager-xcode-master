/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.alert;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLog;
import com.neptunetg.mdce.common.internal.alert.service.AlertRestService;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Displays the alerts page.
 *
 * Note that the alert ID selection is passed from the client as a comma-delimited list rather than model binding to a
 * list of checkboxes. This is to avoid hitting Tomcat's default limit of 10000 parameters per form POST, a scenario
 * that has been encountered when the alert list has grown to thousands of items. Given that the limit on the parameter
 * count is intended to mitigate the possibility of a DoS, the comma-delimited list approach was taken so that no
 * increase to the default limit was required. For details, see http://www.ocert.org/advisories/ocert-2011-003.html
 * and http://tomcat.apache.org/tomcat-6.0-doc/config/http.html.
 */
@Controller
public class AlertController
{
    private static final Logger logger = LoggerFactory.getLogger(AlertController.class);

    @Autowired
    private BackEndManager backEndManager;

    @ModelAttribute("filterStringForm")
    public FilterStringForm getFilterStringForm()
    {
        return new FilterStringForm();
    }

    /**
     * Method to increase the data binder limit, so that more than the default number of
     * 256 alerts can be dealt with on the alerts page
     */
    @InitBinder
    public void initBinder(WebDataBinder binder)
    {
        //Set auto grow limit very high
        binder.setAutoGrowCollectionLimit(100000);
    }

    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping(value="/pages/alert/list", method = RequestMethod.GET)
    public String alertList(Model model,
                            @ModelAttribute("filterStringForm") FilterStringForm filterStringForm) throws InternalApiException
    {
        final AlertRestService alertRestService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), AlertRestService.class);
        String alertSourceFilter = filterStringForm.getFilterString();

        if(alertSourceFilter == null)
        {
            alertSourceFilter = "%";
        }
        else
        {
            alertSourceFilter = "%" + trimAndEscapeFilterString(alertSourceFilter) + "%";
        }

        AlertsForm newAlertDetailsForm = new AlertsForm();
        newAlertDetailsForm.setAlertDetailsList(alertRestService.getNewAlertList(alertSourceFilter));
        model.addAttribute("newAlertForm", newAlertDetailsForm);

        AlertsForm handledAlertDetailsForm = new AlertsForm();
        handledAlertDetailsForm.setAlertDetailsList(alertRestService.getHandledAlertList(alertSourceFilter));
        model.addAttribute("handledAlertForm", handledAlertDetailsForm);

        AlertsForm staleAlertDetailsForm = new AlertsForm();
        staleAlertDetailsForm.setAlertDetailsList(alertRestService.getStaleAlertList(alertSourceFilter));
        model.addAttribute("staleAlertForm", staleAlertDetailsForm);

        AlertsForm clearedAlertDetailsForm = new AlertsForm();
        clearedAlertDetailsForm.setAlertDetailsList(alertRestService.getClearedAlertList(alertSourceFilter));
        model.addAttribute("clearedAlertForm", clearedAlertDetailsForm);

        return "alert/alertLists.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR_WHITELISTED)
    @RequestMapping(value="/pages/alert/newbatchaction", method = RequestMethod.POST, params={ "batchTicket=1" })
    public String commitNewTicketForNewAlertList(@ModelAttribute("newAlertForm") AlertsForm newAlertsForm) throws InternalApiException
    {
        final AlertRestService alertRestService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), AlertRestService.class);

        for (Long alertId : newAlertsForm.getSelectedAlertIdList())
        {
            AlertDetails alert = getAlertDetailsForTicketIdUpdate(alertId, newAlertsForm.getTicketId());
            alertRestService.setAlertStateToHandled(alert.getAlertId());
            alertRestService.setAlertTicketId(alert);
        }

        if(newAlertsForm.getFilterString().length() > 0)
        {
            return "redirect:/pages/alert/list?filterString=" + newAlertsForm.getFilterString();
        }

        return "redirect:/pages/alert/list";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR_WHITELISTED)
    @RequestMapping(value="/pages/alert/newbatchaction", method = RequestMethod.POST, params={ "batchClear=1" })
    public String newAlertListBatchClear(@ModelAttribute("newAlertForm") AlertsForm newAlertsForm) throws InternalApiException
    {
        final AlertRestService alertRestService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), AlertRestService.class);

        for (Long alertId : newAlertsForm.getSelectedAlertIdList())
        {
            alertRestService.setAlertStateToCleared(alertId);
        }
        if(newAlertsForm.getFilterString().length() > 0)
        {
            return "redirect:/pages/alert/list?filterString=" + trimAndEscapeFilterString(newAlertsForm.getFilterString());
        }

        return "redirect:/pages/alert/list";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR_WHITELISTED)
    @RequestMapping(value="/pages/alert/handledbatchaction", method = RequestMethod.POST, params={ "batchTicket=1" })
    public String handledAlertListBatchTicket(@ModelAttribute("handledAlertForm") AlertsForm handledAlertsForm) throws InternalApiException
    {
        final AlertRestService alertRestService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), AlertRestService.class);

        for (Long alertId : handledAlertsForm.getSelectedAlertIdList())
        {
            AlertDetails alert = getAlertDetailsForTicketIdUpdate(alertId, handledAlertsForm.getTicketId());
            alertRestService.setAlertStateToHandled(alert.getAlertId());
            alertRestService.setAlertTicketId(alert);
        }

        if(handledAlertsForm.getFilterString().length() > 0)
        {
            return "redirect:/pages/alert/list?filterString=" + trimAndEscapeFilterString(handledAlertsForm.getFilterString());
        }

        return "redirect:/pages/alert/list";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR_WHITELISTED)
    @RequestMapping(value="/pages/alert/handledbatchaction", method = RequestMethod.POST, params={ "batchClear=1" })
    public String handledAlertListBatchClear(@ModelAttribute("handledAlertForm") AlertsForm handledAlertsForm) throws InternalApiException
    {
        final AlertRestService alertRestService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), AlertRestService.class);

        for (Long alertId : handledAlertsForm.getSelectedAlertIdList())
        {
            alertRestService.setAlertStateToCleared(alertId);
        }

        if(handledAlertsForm.getFilterString().length() > 0)
        {
            return "redirect:/pages/alert/list?filterString=" + trimAndEscapeFilterString(handledAlertsForm.getFilterString());
        }

        return "redirect:/pages/alert/list";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR_WHITELISTED)
    @RequestMapping(value="/pages/alert/stalebatchaction", method = RequestMethod.POST, params={ "batchTicket=1" })
    public String staleAlertListBatchTicket(@ModelAttribute("staleAlertForm") AlertsForm staleAlertsForm) throws InternalApiException
    {

        final AlertRestService alertRestService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), AlertRestService.class);

        for (Long alertId : staleAlertsForm.getSelectedAlertIdList())
        {
            AlertDetails alert = getAlertDetailsForTicketIdUpdate(alertId, staleAlertsForm.getTicketId());
            alertRestService.setAlertStateToHandled(alert.getAlertId());
            alertRestService.setAlertTicketId(alert);
        }

        if(staleAlertsForm.getFilterString().length() > 0)
        {
            return "redirect:/pages/alert/list?filterString=" + trimAndEscapeFilterString(staleAlertsForm.getFilterString());
        }

        return "redirect:/pages/alert/list";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR_WHITELISTED)
    @RequestMapping(value="/pages/alert/stalebatchaction", method = RequestMethod.POST, params={ "batchClear=1" })
    public String staleAlertListBatchClear(@ModelAttribute("staleAlertForm") AlertsForm staleAlertsForm) throws InternalApiException
    {
        final AlertRestService alertRestService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), AlertRestService.class);

        for (Long alertId : staleAlertsForm.getSelectedAlertIdList())
        {
            alertRestService.setAlertStateToCleared(alertId);
        }
        if(staleAlertsForm.getFilterString().length() > 0)
        {
            return "redirect:/pages/alert/list?filterString=" + trimAndEscapeFilterString(staleAlertsForm.getFilterString());
        }

        return "redirect:/pages/alert/list";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping(value="pages/alert/view/{alertId}", method= RequestMethod.GET)
    public String viewAlertDetail(Model model,
                            @PathVariable("alertId") Long alertId) throws InternalApiException
    {
        final AlertRestService alertRestService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), AlertRestService.class);

        List<AlertLog> alertLogs = alertRestService.getAlertLog(alertId);
        model.addAttribute("alertLogs", alertLogs);

        AlertDetails alertDetails = alertRestService.getAlert(alertId);
        model.addAttribute("alertDetails", alertDetails);

        return "alert/alertView.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR_WHITELISTED)
    @RequestMapping(value="pages/alert/view/{alertId}", method = RequestMethod.POST)
    public String alertView(@PathVariable("alertId") long alertId,
                            @ModelAttribute("alertDetails") AlertDetails alertDetails,
                            Model model) throws InternalApiException
    {
        final AlertRestService alertRestService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), AlertRestService.class);

        alertRestService.setAlertStateToHandled(alertId);

        alertDetails.setAlertId(alertId);
        alertRestService.setAlertTicketId(alertDetails);

        return "redirect:/pages/alert/view/" + Long.toString(alertId);
    }

    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR_WHITELISTED)
    @RequestMapping(value="pages/alert/setascleared/{alertId}", method = RequestMethod.GET)
    public String setAlertAsCleared(@PathVariable("alertId") Long alertId) throws InternalApiException
    {
        final AlertRestService alertRestService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), AlertRestService.class);

        alertRestService.setAlertStateToCleared(alertId);

        return "redirect:/pages/alert/list/";
    }

    private String trimAndEscapeFilterString(String filterString)
    {
        filterString = filterString.trim();
        filterString = filterString.replace("\\", "\\\\");
        filterString = filterString.replace("%", "\\%");
        filterString = filterString.replace("_", "\\_");
        filterString = filterString.replace("[", "\\[");
        return filterString;
    }

    /**
     * Builds a temporary instance of AlertDetails for use with AlertRestService.setAlertTicketId(AlertDetails).
     * @param alertId The ID of the alert being updated.
     * @param ticketId The ticket ID to asssign to the alert.
     * @return An instance of AlertDetails initialized with the supplied parameter values.
     */
    private AlertDetails getAlertDetailsForTicketIdUpdate(Long alertId, String ticketId)
    {
        AlertDetails alertDetails = new AlertDetails();
        alertDetails.setAlertId(alertId.longValue());
        alertDetails.setAlertTicketId(ticketId);

        return alertDetails;
    }
}
