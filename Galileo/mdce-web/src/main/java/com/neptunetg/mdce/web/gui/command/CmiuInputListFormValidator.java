/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.command;

import com.neptunetg.mdce.web.gui.config.MiuInput;
import com.neptunetg.mdce.web.gui.config.GenericCommandForm;
import com.neptunetg.mdce.web.gui.config.ImageUpdateForm;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Form validator for validing CMIU entries for send command
 */
public class CmiuInputListFormValidator implements Validator
{
    @Override
    public boolean supports(Class<?> clazz)
    {
        return ImageUpdateForm.class.equals(clazz) || GenericCommandForm.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors)
    {
        List<MiuInput> miuInputList = Collections.emptyList();

        if (o instanceof ImageUpdateForm)
        {
            miuInputList = ((ImageUpdateForm) o).getMiuInputList();
        }
        else if (o instanceof GenericCommandForm)
        {
            miuInputList = ((GenericCommandForm) o).getMiuInputList();
        }

        //All cmiu entries regardless of whether they are in the correct format
        List<String> cmiuEntries = miuInputList.stream()
                .map(MiuInput::getMiuId)
                .collect(Collectors.toList());

        List<String> validCmiuList = cmiuEntries
                .stream()
                .filter(s -> StringUtils.hasText(s))
                .collect(Collectors.toList());

        if (validCmiuList.isEmpty())
        {
            //no cmiu entries entered
            errors.rejectValue("miuInputList", "cmiu.not.defined", "No valid CMIU is entered.");
        }
        else
        {
            boolean isUndefinedError = false;
            boolean hasFormatError = false;
            boolean hasRangeError = false;

            for (int i = 0; i < miuInputList.size(); i++)
            {
                MiuInput cmiuDescription = miuInputList.get(i);

                if (StringUtils.hasText(cmiuDescription.getMiuId())) //allowed to be empty as long as at least one is provided
                {
                    if (!isNumeric(cmiuDescription.getMiuId()))
                    {
                        errors.rejectValue("miuInputList[" + i + "].miuId", "cmiu.invalid.format", "CMIU is not a number");
                        hasFormatError = true;
                    } else if (!isValidCmiuIdRange(Long.valueOf(cmiuDescription.getMiuId())))
                    {
                        errors.rejectValue("miuInputList[" + i + "].miuId", "cmiu.invalid.range", "CMIU value is not within the valid range");
                        hasRangeError = true;
                    }
                }
            }

            if (isUndefinedError)
            {
                errors.rejectValue("miuInputList", "cmiu.not.defined", "No valid CMIU is entered.");
            }

            if (hasFormatError)
            {
                errors.rejectValue("miuInputList", "cmiu.invalid.format", "CMIU is not a number");
            }

            if (hasRangeError)
            {
                errors.rejectValue("miuInputList", "cmiu.invalid.range", "CMIU value is not within the valid range");
            }
        }


    }



    /**
     * Verify that the CMIU ID is valid range
     *
     * @param id containing the cmiuid
     * @return true if valid
     */
    private static boolean isValidCmiuIdRange(Long id)
    {
        // Supported range for a CMIU ID is 400,000,000 <= n < 600,000,000.
        return 400000000 <= id && id < 600000000;
    }

    private static boolean isNumeric(String id)
    {
        try
        {
            long l = Long.parseLong(id);
            return true;
        }
        catch (NumberFormatException ex)
        {
            return false;
        }
    }
}
