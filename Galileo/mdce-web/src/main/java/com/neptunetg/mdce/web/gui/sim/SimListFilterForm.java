/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.sim;


import com.neptunetg.mdce.common.internal.site.model.SiteDetails;

import java.util.List;

/**
 * Backing form for Sim list filtering
 */
public class SimListFilterForm
{

    private String selectedNetwork;
    private String selectedLifecycleState;
    private String selectedIccid;

    private List<String> networkList;
    private List<String> lifeCycleStateList;
    private List<String> iccidList;

    private int page;


    public String getSelectedNetwork() { return selectedNetwork; }

    public void setSelectedNetwork(String selectedNetwork) { this.selectedNetwork = selectedNetwork; }

    public String getSelectedLifecycleState() { return selectedLifecycleState; }

    public void setSelectedLifecycleState(String selectedLifecycleState) { this.selectedLifecycleState = selectedLifecycleState; }

    public String getSelectedIccid() { return selectedIccid; }

    public void setSelectedIccid(String selectedIccid) { this.selectedIccid = selectedIccid; }

    public List<String> getNetworkList() { return networkList; }

    public void setNetworkList(List<String> networkList) { this.networkList = networkList; }

    public List<String> getLifeCycleStateList() { return lifeCycleStateList; }

    public void setLifeCycleStateList(List<String> lifeCycleStateList) { this.lifeCycleStateList = lifeCycleStateList; }

    public List<String> getIccidList() { return iccidList; }

    public void setIccidList(List<String> iccidList) { this.iccidList = iccidList; }

    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }


    /**
     * Get a destination page number for passing to PagedResult.
     * Page display on html starts from 1, whereas internally it is base 0
     * @return page number based 0
     */
    public int getPageNumberForPagedResult()
    {
        return page > 1?  page - 1 : 0;
    }
}
