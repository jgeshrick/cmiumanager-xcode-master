/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */
package com.neptunetg.mdce.web.gui.analysis;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.anaylsis.service.CmiuAnalysisService;
import com.neptunetg.mdce.common.internal.site.service.SiteService;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * CMIU Timing Analysis.
 */
@Controller
public class CmiuAnalysisController
{
    /**
     * Logger.
     */
    private static final Logger logger = LoggerFactory.getLogger(CmiuAnalysisController.class);

    /**
     * Pulls in services from common project.
     */
    private BackEndManager backEndManager;

    /**
     * Landing page.
     *
     * @return page
     */
    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR)
    @RequestMapping("/pages/analysis/timing")
    public String cmiuTimingAnalysis(Model model) throws InternalApiException
    {
        SiteService siteService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), SiteService.class);

        model.addAttribute("siteList",siteService.getSiteList());
        return "analysis/timingAnalysis.jsp";
    }

    /**
     * Getting list of MIUs with total connection time of more than threshold
     * and that meet the other conditions passed in.
     *
     * @param model to send data back to page
     * @param connectionThresholdMilliSeconds max connection condition
     * @param cmiuMode cmiu mode
     * @param mno network provider
     * @param state cmiu state
     * @param siteId site scmiu assigned to.
     * @return page
     *
     * @throws InternalApiException
     */
    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR)
    @RequestMapping(value = "/pages/analysis/result", method = RequestMethod.GET)
    public String cmiuTimingData(Model model,
                                 @RequestParam(value = "connection-threshold", required = false) Long connectionThresholdMilliSeconds,
                                 @RequestParam(value = "cmiu-mode", required = false) String cmiuMode,
                                 @RequestParam(value = "cmiu-mno", required = false) String mno,
                                 @RequestParam(value = "cmiu-state", required = false) String state,
                                 @RequestParam(value = "cmiu-site-id",  required = false) String siteId) throws InternalApiException {

        CmiuAnalysisService cmiuAnalysisService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuAnalysisService.class);

        model.addAttribute("CmiuTimingList", cmiuAnalysisService.getCmiuTimingInfo(connectionThresholdMilliSeconds, cmiuMode, mno, state, siteId));
        return "analysis/timingAnalysisTable.jsp";
    }

    /**
     * Injection setter.
     *
     * @param backEndManager tool to pull in services
     */
    @Autowired
    public void setBackEndManager(BackEndManager backEndManager) {
        this.backEndManager = backEndManager;
    }
}

