/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.user;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.user.model.*;
import com.neptunetg.mdce.common.internal.user.service.UserService;
import com.neptunetg.mdce.web.backend.BackEndManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

import java.util.List;

import static com.neptunetg.mdce.web.gui.login.LoginUtils.getLoggedInUserName;

/**
 * For viewing & editing a user's own details (initially for changing passwords only).
 */
@Controller
public class UserDetailsController
{
    private final BackEndManager backEndManager;
    private SessionRegistry sessionRegistry;

    @Autowired
    public UserDetailsController(BackEndManager backEndManager, SessionRegistry sessionRegistry)
    {
        this.backEndManager = backEndManager;
        this.sessionRegistry = sessionRegistry;
    }

    /**
     * GET method for the Change Password user form.
     */
    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR_WHITELISTED)
    @RequestMapping(path = "/pages/user/changePassword", method = RequestMethod.GET)
    public String getChangePassword(ModelMap model) throws InternalApiException
    {
        final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);

        final String userName = getLoggedInUserName();
        UserDetails userDetails = userService.getUserDetails(userName);

        ChangePasswordForm changePasswordForm = new ChangePasswordForm();
        changePasswordForm.setUserName(userDetails.getUserName());

        model.addAttribute("changePasswordForm", changePasswordForm);

        return "user/changePassword.jsp";
    }

    /**
     * POST method for submitting a Change Password request.
     */
    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(path = "/pages/user/changePassword", method = RequestMethod.POST)
    public String postChangePassword(@ModelAttribute("changePasswordForm") @Valid ChangePasswordForm changePasswordForm,
                                     BindingResult result,
                                     ModelMap model) throws InternalApiException
    {
        final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);

        final String userName = getLoggedInUserName();
        UserDetails userDetails = userService.getUserDetails(userName);

        // Check that the user submitting the request is the same user who originally loaded the form.
        if (!changePasswordForm.getUserName().equals(userName))
        {
            result.rejectValue("userName", "error.userName", "This form is no longer valid. Please re-load the page and try again.");
        }

        // Verify the supplied current password
        LoginCredentials loginCredentials = new LoginCredentials(userDetails.getUserName(), changePasswordForm.getCurrentPassword());
        LoginResult verifyPasswordResult = userService.verifyUser(loginCredentials);

        if (verifyPasswordResult == LoginResult.ACCOUNT_DISABLED)
        {
            // Expire any active sessions if the account is disabled.
            List<SessionInformation> allSessions = sessionRegistry.getAllSessions(userDetails.getUserName(), false);
            allSessions.forEach(SessionInformation::expireNow);

            return "redirect:/pages/user/login";
        }
        else if (verifyPasswordResult != LoginResult.OK)
        {
            result.rejectValue("currentPassword", "error.currentPassword", "The password is incorrect. Please re-enter your current password.");
        }

        if (result.hasErrors())
        {
            changePasswordForm.setCurrentPassword("");
            changePasswordForm.setNewPassword("");
            changePasswordForm.setNewPasswordRepeated("");

            return "user/changePassword.jsp";
        }
        else
        {
            // Apply changes
            ModifiedUserDetails modifiedUserDetails = new ModifiedUserDetails();
            modifiedUserDetails.setUserId((int)userDetails.getUserId());
            modifiedUserDetails.setPassword(changePasswordForm.getNewPassword());
            modifiedUserDetails.setRequester(userDetails.getUserName());

            userService.resetPassword(modifiedUserDetails);

            return "redirect:/pages/home";
        }
    }
}
