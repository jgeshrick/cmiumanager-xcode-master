/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.miu;

/**
 * Created by MC5 on 11/11/2016.
 * Represents a page number and a page link.
 */
public class PageReference
{
    private int pageNumber;
    private String href;

    public PageReference(int pageNumber, String href)
    {
        this.pageNumber = pageNumber;
        this.href = href;
    }

    public int getPageNumber()
    {
        return pageNumber;
    }

    public String getHref()
    {
        return href;
    }
}
