/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.authentication;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.user.model.LoginCredentials;
import com.neptunetg.mdce.common.internal.user.model.LoginResult;
import com.neptunetg.mdce.common.internal.user.model.MdceUserRole;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import com.neptunetg.mdce.common.internal.user.service.UserService;
import com.neptunetg.mdce.web.backend.BackEndManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;

import java.util.ArrayList;
import java.util.List;

/**
 * Used by Spring security framework for authenticating user and password
 */

public class MdceUserAuthenticator implements UserAuthenticator
{
    private static final Logger logger = LoggerFactory.getLogger(MdceUserAuthenticator.class);

    private final BackEndManager backEndManager;
    private SessionRegistry sessionRegistry;

    @Autowired
    public MdceUserAuthenticator(BackEndManager backEndManager, SessionRegistry sessionRegistry)
    {
        this.backEndManager = backEndManager;
        this.sessionRegistry = sessionRegistry;
    }

    /**
     * Grant authority based on the user role.
     *
     * @param role the role assigned to the user
     * @return a list of granted authority.
     */
    private static List<GrantedAuthority> grantAuthority(MdceUserRole role)
    {
        List<GrantedAuthority> grantedAuths = new ArrayList<>();

        if (role.getAuthLevel() >= MdceUserRole.SiteOperator.getAuthLevel())
        {
            // add a ROLE prefix for Spring Security 4, see http://stackoverflow.com/questions/19525380/difference-between-role-and-grantedauthority-in-spring-security
            grantedAuths.add(new SimpleGrantedAuthority(MdceUserRole.SiteOperator.getRoleName()));
        }

        if (role.getAuthLevel() >= MdceUserRole.MsocViewer.getAuthLevel())
        {
            grantedAuths.add(new SimpleGrantedAuthority(MdceUserRole.MsocViewer.getRoleName()));
        }

        if (role.getAuthLevel() >= MdceUserRole.MsocOperator.getAuthLevel())
        {
            grantedAuths.add(new SimpleGrantedAuthority(MdceUserRole.MsocOperator.getRoleName()));
        }

        if (role.getAuthLevel() >= MdceUserRole.MsocAdmin.getAuthLevel())
        {
            grantedAuths.add(new SimpleGrantedAuthority(MdceUserRole.MsocAdmin.getRoleName()));
        }

        if(role.getAuthLevel() >= MdceUserRole.MsocMasterOfCellular.getAuthLevel())
        {
            grantedAuths.add(new SimpleGrantedAuthority(MdceUserRole.MsocMasterOfCellular.getRoleName()));
        }

        return grantedAuths;
    }

    /**
     * Called by Spring Security Framework to authenticate user.
     * @param username the given user name
     * @param password the supplied password
     * @return User info object (authentication token) is returned if the user/password pair has been successfully verified.
     * @throws SecurityException
     * @throws InternalApiException
     */
    @Override
    public UserInfo auth(String username, String password)
            throws SecurityException, InternalApiException
    {
        final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);
        LoginCredentials loginCredentials = new LoginCredentials(username, password);

        LoginResult loginResult = userService.verifyUser(loginCredentials);

        if (loginResult == LoginResult.OK)
        {
            UserDetails userDetails = userService.getUserDetails(username);
            MdceUserRole role = userDetails.getUserLevel();
            boolean allowConcurrentSessions = userDetails.getAllowConcurrentSessions();

            return new UserInfo(username, password, role, grantAuthority(role), allowConcurrentSessions);
        }
        else
        {
            if (loginResult == LoginResult.ACCOUNT_DISABLED)
            {
                // Expire any active sessions if the account is disabled.
                List<SessionInformation> allSessions = sessionRegistry.getAllSessions(username, false);
                allSessions.forEach(SessionInformation::expireNow);
            }

            logger.debug("Authentication failed");

            throw new SecurityException("The user name or password may be incorrect, or the account may be disabled.");
        }

    }

}
