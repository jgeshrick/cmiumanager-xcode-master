/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.gui.config;

import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;

/**
 * Backing form for image update.
 */
public class RegisterNewImageForm
{
    private ImageDescription.ImageType imageType;
    private String imageMetaData;
    private String selectedImageSource;

    public ImageDescription.ImageType getImageType()
    {
        return imageType;
    }

    public void setImageType(ImageDescription.ImageType imageType)
    {
        this.imageType = imageType;
    }

    public String getImageMetaData()
    {
        return imageMetaData;
    }

    public void setImageMetaData(String imageMetaData)
    {
        this.imageMetaData = imageMetaData;
    }

    public String getSelectedImageSource()
    {
        return selectedImageSource;
    }

    public void setSelectedImageSource(String selectedImageSource)
    {
        this.selectedImageSource = selectedImageSource;
    }
}
