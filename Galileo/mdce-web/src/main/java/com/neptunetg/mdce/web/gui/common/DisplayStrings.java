/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.common;


import com.neptunetg.mdce.web.gui.dashboard.DashboardController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Utility class for dealing with internationalized display strings
 */
public class DisplayStrings
{

    private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);

    /**
     * The build of the applications
     */
    private static final String APPLICATION_VERSION = initApplicationVersion();

    /**
     * The build of the applications
     */
    private static final String ENVIRONMENT_NAME = initEnvironmentName();

    /**
     * Internationalized resource bundle
     */
    private static final ResourceBundle STRINGS = ResourceBundle.getBundle("mdce-messages");

    /**
     * Get translation of a string
     * @param key The key to look up
     * @return The translation of the string
     */
    public static String t(String key)
    {
        if (STRINGS.containsKey(key))
        {
            return STRINGS.getString(key);
        }
        else
        {
            if (logger.isWarnEnabled())
            {
                logger.warn("Key not found in messages bundle: " + key);
            }
            return key;
        }
    }

    /**
     * Initialize the application version
     * @return application version string read from Maven-filtered file
     */
    private static String initApplicationVersion()
    {
        Properties versionDetails = new Properties();
        try
        {
            versionDetails.load(DisplayStrings.class.getResourceAsStream("/applicationVersion.properties"));
        }
        catch (IOException e)
        {
            throw new RuntimeException("Couldn't read application version!", e);
        }
        return versionDetails.getProperty("applicationVersion");
    }

    /**
     * Initialize the environment name
     * @return environment name string read from Maven-filtered file
     */
    private static String initEnvironmentName()
    {
        Properties mdceEnvironmentDetails = new Properties();

        try
        {
            mdceEnvironmentDetails.load(DisplayStrings.class.getResourceAsStream("/mdce-env.properties"));
        }
        catch (IOException e)
        {
            throw new RuntimeException("Couldn't read MDCE environment name!", e);
        }

        return mdceEnvironmentDetails.getProperty("env.name");
    }

    /**
     * Get the application version string
     * @return application version
     */
    public static String getApplicationVersion()
    {
        return APPLICATION_VERSION;
    }

    /**
     * Get the MDCE environment name
     * @return the MDCE environment name
     */
    public static String getEnvironmentName()
    {
        return ENVIRONMENT_NAME;
    }
}
