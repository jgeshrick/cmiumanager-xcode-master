/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.gateway;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.gateway.model.GatewayDisplayDetails;
import com.neptunetg.mdce.common.internal.gateway.model.GatewayDisplayDetailsPagedResult;
import com.neptunetg.mdce.common.internal.gateway.service.GatewayService;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataUtility;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.common.internal.site.service.RefService;
import com.neptunetg.mdce.common.internal.site.service.SiteService;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import com.neptunetg.mdce.web.gui.audit.AuditLogsFilterForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * For administrating gateway. All url in this page is accessible only to login gateway with MsocAdmin authority
 */

@Controller
public class GatewayDetailController
{
    private static final Logger logger = LoggerFactory.getLogger(GatewayDetailController.class);

    private static final String SITE_LIST = "siteList";
    private static final String TOTAL_RESULTS = "totalResults";
    private static final String USER = "user";
    private static final String DAYS_AGO = "daysAgo";
    private static final String PAGE = "page";
    private static final String GATEWAY_LIST = "gatewayList";
    private static final String GATEWAY_DETAILS = "gatewayDetails";
    private static final String SITE_DETAILS = "siteRefData";
    private static final String GATEWAY_ID = "gatewayId";
    private static final String SITE_ID = "siteId";
    private static final String SITE_NAME = "siteName";
    private static final String REGIONAL_MANAGER = "regionalManager";
    private static final String LOCATION = "location";

    private static final String DEFAULT_SELECTION = "";

    private BackEndManager backEndManager;

    @Autowired
    public GatewayDetailController(BackEndManager backEndManager)
    {
        this.backEndManager = backEndManager;
    }

    @ModelAttribute("gatewayListFilterForm")
    private GatewayListFilterForm getMiuListFilterForm() throws InternalApiException
    {
        return new GatewayListFilterForm();
    }

    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping(value = "/pages/gateway/gatewaylist", method = RequestMethod.GET)
    public String listGatewayDetails(
            @ModelAttribute("gatewayListFilterForm") GatewayListFilterForm gatewayListFilterForm,
            ModelMap model
    ) throws Exception
    {
        final HashMap<String, String> options = new HashMap<>();

        addKeyValueToMapIfNotNull(REGIONAL_MANAGER, gatewayListFilterForm.getSelectedManager(), options);
        addKeyValueToMapIfNotNull(LOCATION, gatewayListFilterForm.getSelectedLocation(), options);
        addKeyValueToMapIfNotNull(SITE_ID, gatewayListFilterForm.getSelectedSite(), options);
        addKeyValueToMapIfNotNull(SITE_NAME, gatewayListFilterForm.getSelectedSiteName(), options);

        int newPage = gatewayListFilterForm.getPageNumberForPagedResult();

        addKeyValueToMapIfNotNull(PAGE, String.valueOf(newPage), options);

        GatewayDisplayDetailsPagedResult gatewayDetails = getGatewayService().getGatewayList(options);

        model.addAttribute(GATEWAY_LIST, gatewayDetails.getResults());
        model.addAttribute(TOTAL_RESULTS, gatewayDetails.getTotalResults());

        final RefService refService = getRefDataService();

        gatewayListFilterForm.setManagers(setupSelectionsWithFirstEntryAsUndefined((refService.getRegionalManagers())));
        gatewayListFilterForm.setLocations(setupSelectionsWithFirstEntryAsUndefined((refService.getStates())));

        final List<SiteDetails> siteDetailsArrayList;

        if (hasSelection(gatewayListFilterForm.getSelectedManager()))
        {
            siteDetailsArrayList = refService.getSitesForRegionalManager(gatewayListFilterForm.getSelectedManager());
            gatewayListFilterForm.setSiteIds(siteDetailsArrayList);
        }
        else if (hasSelection(gatewayListFilterForm.getSelectedLocation()))
        {
            siteDetailsArrayList = refService.getSitesForState(gatewayListFilterForm.getSelectedLocation());
            gatewayListFilterForm.setSiteIds(siteDetailsArrayList);
        }
        else if (hasSelection(gatewayListFilterForm.getSelectedSite()))
        {
            final int siteId = parseSiteId(gatewayListFilterForm.getSelectedSite());
            siteDetailsArrayList = Arrays.asList(getSiteService().getSiteDetails(siteId));
        }
        else
        {
            siteDetailsArrayList = getSiteService().getSiteList();
        }

        augmentFormWithSiteName(gatewayListFilterForm, siteDetailsArrayList);
        augmentGatewayListWithSiteNames(gatewayDetails.getResults(), siteDetailsArrayList);

        model.addAttribute("gatewayListFilterForm", gatewayListFilterForm);
        model.addAllAttributes(options);

        return "gateway/gatewaylist.jsp";
    }

    private int parseSiteId(String selectedSite)
    {
        try
        {
            return Integer.parseInt(selectedSite);
        }
        catch (NumberFormatException e)
        {
            logger.error("Bad site ID from form: " + selectedSite, e);
            return -1;
        }
    }

    /**
     * Add a key value pair to a supplied dictionary if the value is not null.
     * @param key key
     * @param value value string
     * @param options dictionary hash map where the key value pair is added
     */
    private static void addKeyValueToMapIfNotNull(String key, String value, final HashMap<String, String> options)
    {
        if (value != null)
        {
            options.put(key, value);
        }
    }

    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping(value = "/pages/gateway/gatewaydetail/{gatewayId}", method = RequestMethod.GET)
    public String getGatewayDetails(ModelMap model,
                                @PathVariable("gatewayId") String gatewayId
    ) throws Exception
    {
        final GatewayService gatewayService = getGatewayService();

        //page display on html starts from 1, whereas internally it is base 0

        final HashMap<String, String> options = new HashMap<>();

        addKeyValueToMapIfNotNull(GATEWAY_ID, String.valueOf(gatewayId), options);

        final GatewayDisplayDetailsPagedResult gatewayDetailsList = gatewayService.getGatewayList(options);

        if (gatewayDetailsList.getNumResults() == null || gatewayDetailsList.getNumResults().intValue() < 1)
        {
            //TODO: return 404
            throw new RuntimeException("404 - Gateway with ID " + gatewayId + " not found");
        }

        final GatewayDisplayDetails gatewayDetails = gatewayDetailsList.getResults().get(0);

        final RefService refDataService = getRefDataService();
        final MdceReferenceDataUtility siteRefData = refDataService.getSiteReferenceData((int)gatewayDetails.getSiteId());
        final List<SiteDetails> allSites = getSiteService().getSiteList();
        augmentGatewayDetailsWithSiteName(gatewayDetails, allSites);

        final ZonedDateTime midnightUtc = Instant.now().atZone(ZoneOffset.UTC).withHour(0).withMinute(0).withSecond(0).withNano(0);
        final ZonedDateTime startOfMonthMidnightUtc = midnightUtc.withDayOfMonth(1);

        model.addAttribute(GATEWAY_DETAILS, gatewayDetails);
        model.addAttribute(SITE_DETAILS, siteRefData);
        model.addAttribute(SITE_LIST, allSites);
        model.addAttribute("midnightUtc", midnightUtc);
        model.addAttribute("startOfMonthMidnightUtc", startOfMonthMidnightUtc);

        model.addAllAttributes(options);

        return "gateway/gatewaydetail.jsp";
    }

    private static void augmentGatewayListWithSiteNames(Collection<GatewayDisplayDetails> gateways, Collection<SiteDetails> sites)
    {
        final Map<Integer, SiteDetails> siteDetailsLookup = new HashMap<>();

        for(SiteDetails site : sites)
        {
            siteDetailsLookup.put(site.getSiteId(), site);
        }

        for (GatewayDisplayDetails gatewayDisplayDetails : gateways)
        {
            final SiteDetails site = siteDetailsLookup.get(gatewayDisplayDetails.getSiteId());
            if (site != null)
            {
                gatewayDisplayDetails.setSiteDescription(site.getSiteName());
            }
        }
    }

    private static void augmentFormWithSiteName(GatewayListFilterForm form, Collection<SiteDetails> sites)
    {

        final String siteIdString = form.getSelectedSite();
        if (StringUtils.hasText(siteIdString))
        {
            try
            {
                int siteId = Integer.parseInt(siteIdString);
                sites
                        .stream()
                        .filter(s -> s.getSiteId() == siteId)
                        .findFirst()
                        .ifPresent(s -> form.setSelectedSiteName(s.getSiteName()));
            }
            catch (NumberFormatException e)
            {
                logger.error("Bad site ID from form: " + siteIdString, e);
            }
        }

    }

    private static void augmentGatewayDetailsWithSiteName(GatewayDisplayDetails gateway, Collection<SiteDetails> sites)
    {

        sites
                .stream()
                .filter(s -> s.getSiteId() == gateway.getSiteId())
                .findFirst()
                .ifPresent(s -> gateway.setSiteDescription(s.getSiteName()));

    }

    private GatewayService getGatewayService() throws InternalApiException
    {
        return backEndManager.createClient(backEndManager.getAllBackEndConfigs(), GatewayService.class);
    }

    private SiteService getSiteService() throws InternalApiException
    {
        return backEndManager.createClient(backEndManager.getAllBackEndConfigs(), SiteService.class);
    }

    private RefService getRefDataService() throws InternalApiException
    {
        return backEndManager.createClient(backEndManager.getAllBackEndConfigs(), RefService.class);
    }

    private List<String> setupSelectionsWithFirstEntryAsUndefined(List<String> selectionList)
    {
        List<String> list = new ArrayList<>();
        list.add(DEFAULT_SELECTION);
        list.addAll(selectionList);

        return list;
    }

    /**
     * Check if a value received from a html:select tag has value defined and it is not the first "default" option.
     */
    private boolean hasSelection(String selectValue)
    {
        return selectValue != null && !selectValue.equals(DEFAULT_SELECTION);
    }
}
