/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.command;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.lora.service.LoraService;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandSummary;
import com.neptunetg.mdce.common.internal.miu.service.MiuCommandControlService;
import com.neptunetg.mdce.common.internal.sim.service.SimService;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Landing page for C&C.
 *
 * Where a 'sortBy' parameter is supported, the client supplies a comma-separated list of fields. Each field is
 * prefixed by '+' to indicate ascending order or '-' to indicate descending order. The fields in the 'sortBy'
 * list are supplied in order or preference, with the highest preference first in the list.
 *
 * Sorting & filtering are done in the controller as it keeps the internal service calls simple and the expectation at
 * the time of writing is that the volume of data returned will be relatively low.
 */
@Controller
public class ConfigCommandLandingPageController
{
    private static final Logger logger = LoggerFactory.getLogger(ConfigCommandLandingPageController.class);

    private static final String MIU_ID_FIELD = "miuId";
    private static final Comparator<CommandSummary> MIU_ID_ASC_COMPARATOR = (t, p) -> Long.compare(t.getMiuId(), p.getMiuId());
    private static final Comparator<CommandSummary> MIU_ID_DESC_COMPARATOR = (t, p) -> -Long.compare(t.getMiuId(), p.getMiuId());

    private static final String SUBMITTED_FIELD = "submitted";
    private static final Comparator<CommandSummary> SUBMITTED_ASC_COMPARATOR = (t, p) -> compareInstants(t.getCommandEnteredTime(), p.getCommandEnteredTime());
    private static final Comparator<CommandSummary> SUBMITTED_DESC_COMPARATOR = (t, p) -> -compareInstants(t.getCommandEnteredTime(), p.getCommandEnteredTime());

    private static final String USER_FIELD = "user";
    private static final Comparator<CommandSummary> USER_ASC_COMPARATOR = (t, p) -> compareStrings(t.getUser(), p.getUser());
    private static final Comparator<CommandSummary> USER_DESC_COMPARATOR = (t, p) -> -compareStrings(t.getUser(), p.getUser());

    private static final String COMMAND_FIELD = "command";
    private static final Comparator<CommandSummary> COMMAND_ASC_COMPARATOR = (t, p) -> compareStrings(t.getCommandType(), p.getCommandType());
    private static final Comparator<CommandSummary> COMMAND_DESC_COMPARATOR = (t, p) -> -compareStrings(t.getCommandType(), p.getCommandType());

    private static final String DETAILS_FIELD = "details";
    private static final Comparator<CommandSummary> DETAILS_ASC_COMPARATOR = (t, p) -> compareStrings(t.getDetails(), p.getDetails());
    private static final Comparator<CommandSummary> DETAILS_DESC_COMPARATOR = (t, p) -> -compareStrings(t.getDetails(), p.getDetails());

    private static final String FULFULLED_FIELD = "fulfilled";
    private static final Comparator<CommandSummary> FULFULLED_ASC_COMPARATOR = (t, p) -> compareInstants(t.getCommandFulfilledTime(), p.getCommandFulfilledTime());
    private static final Comparator<CommandSummary> FULFULLED_DESC_COMPARATOR = (t, p) -> -compareInstants(t.getCommandFulfilledTime(), p.getCommandFulfilledTime());

    private static final String NETWORK_PROVIDER_FIELD = "networkProvider";
    private static final Comparator<CommandSummary> NETWORK_PROVIDER_ASC_COMPARATOR = (t, p) -> compareStrings(t.getNetworkProvider(), p.getNetworkProvider());
    private static final Comparator<CommandSummary> NETWORK_PROVIDER_DESC_COMPARATOR = (t, p) -> -compareStrings(t.getNetworkProvider(), p.getNetworkProvider());

    private static final String NETWORK_NONE = "(none)";

    @Autowired
    private BackEndManager backEndManager;

    /**
     * Landing page for Cmiu Command and Control interface. Show in progress and completed command view.
     * @return
     * @throws InternalApiException
     */
    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping("/pages/config")
    public String showCommandControlProgressStatus(Model model)
            throws InternalApiException
    {
        SimService simService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), SimService.class);
        LoraService loraService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), LoraService.class);

        List<String> networkProviders = new ArrayList<>();
        networkProviders.addAll(simService.getSimMnoList());
        networkProviders.addAll(loraService.getLoraNetworkList());

        model.addAttribute("networkProviders", networkProviders);

        return "cmiuconfig/miuConfig.jsp";
    }

    /**
     * Null-safe String comparison function, in which null is considered less than non-null.
     * @param a The left hand side of the comparison.
     * @param b The right hand side of the comparison.
     * @return The result of the comparison, taking nulls into account.
     */
    public static int compareStrings(String a, String b)
    {
        if (a == null ^ b == null)
        {
            return (a == null) ? -1 : 1;
        }

        if (a == null && b == null)
        {
            return 0;
        }

        return a.compareTo(b);
    }

    /**
     * Null-safe Instant comparison function, in which null is considered less than non-null.
     * @param a The left hand side of the comparison.
     * @param b The right hand side of the comparison.
     * @return The result of the comparison, taking nulls into account.
     */
    public static int compareInstants(Instant a, Instant b)
    {
        if (a == null ^ b == null)
        {
            return (a == null) ? -1 : 1;
        }

        if (a == null && b == null)
        {
            return 0;
        }

        return a.compareTo(b);
    }

    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping("/pages/config/commandsInProgress")
    public String getCommandsInProgress(
            @RequestParam(value = "sortBy", required = false) String sortBy,
            @RequestParam(value = "miuIdFilter", required = false) String miuIdFilter,
            @RequestParam(value = "networkFilter", required = false) String networkFilter,
            Model model)
            throws InternalApiException
    {
        final MiuCommandControlService commandControlService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MiuCommandControlService.class);

        List<CommandDisplayDetails> commandInProgressList = getSortedAndFilteredDisplayDetails(
                commandControlService.getCommandsInProgress(), sortBy, miuIdFilter, networkFilter);

        // Get a count of commands per MIU ID
        commandInProgressList
                .stream()
                .forEach(c -> {
                    c.setCommandCount(commandInProgressList
                            .stream()
                            .filter(inner -> inner.getMiuId() == c.getMiuId())
                            .count());
                });

        model.addAttribute("commandInProgressList", commandInProgressList);

        return "cmiuconfig/commandsInProgress.jsp";
    }


    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping("/pages/config/commandsReceived")
    public String getCommandsReceived(
            @RequestParam("sortBy") String sortBy,
            @RequestParam(value = "miuIdFilter", required = false) String miuIdFilter,
            @RequestParam(value = "networkFilter", required = false) String networkFilter,
            Model model)
            throws InternalApiException
    {
        final MiuCommandControlService commandControlService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MiuCommandControlService.class);

        List<CommandDisplayDetails> commandReceivedList = getSortedAndFilteredDisplayDetails(
                commandControlService.getCommandsReceived(), sortBy, miuIdFilter, networkFilter);

        model.addAttribute("commandReceivedList", commandReceivedList);

        return "cmiuconfig/commandsReceived.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping("/pages/config/commandsCompleted")
    public String getCommandsCompleted(
            @RequestParam("sortBy") String sortBy,
            @RequestParam(value = "miuIdFilter", required = false) String miuIdFilter,
            @RequestParam(value = "networkFilter", required = false) String networkFilter,
            Model model)
            throws InternalApiException
    {
        final MiuCommandControlService commandControlService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MiuCommandControlService.class);

        List<CommandDisplayDetails> commandCompleteList = getSortedAndFilteredDisplayDetails(
                commandControlService.getCommandsCompleted(10), sortBy, miuIdFilter, networkFilter);

        model.addAttribute("commandCompleteList", commandCompleteList);

        return "cmiuconfig/commandsCompleted.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping("/pages/config/commandsRejected")
    public String getCommandsRejected(
            @RequestParam("sortBy") String sortBy,
            @RequestParam(value = "miuIdFilter", required = false) String miuIdFilter,
            @RequestParam(value = "networkFilter", required = false) String networkFilter,
            Model model)
            throws InternalApiException
    {
        final MiuCommandControlService commandControlService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MiuCommandControlService.class);

        List<CommandDisplayDetails> commandRejectedList = getSortedAndFilteredDisplayDetails(
                commandControlService.getCommandsRejected(10), sortBy, miuIdFilter, networkFilter);

        model.addAttribute("commandRejectedList", commandRejectedList);

        return "cmiuconfig/commandsRejected.jsp";
    }

    private List<CommandDisplayDetails> getSortedAndFilteredDisplayDetails(
            List<CommandSummary> commandSummaryList, String sortBy, String miuIdFilter, String networkFilter)
    {
        Comparator<CommandSummary> comparator = getComparator(sortBy);
        CommandSummary[] previous = new CommandSummary[1];

        return commandSummaryList
                .stream()
                .filter(c -> filterByMiuIdAndNetwork(c, miuIdFilter, networkFilter))
                .sorted(comparator::compare)
                .map((commandSummary) -> {
                    // Convert to the display model
                    CommandDisplayDetails commandDisplayDetails = CommandDisplayDetails.fromCommandSummary(commandSummary, previous[0]);
                    previous[0] = commandSummary;

                    return commandDisplayDetails;
                })
                .collect(Collectors.toList());
    }

    /**
     * Checks whether the candidate CommandSummary matches the supplied search criteria.
     * @param commandSummary The CommandSummary whose MIU ID to search (as text).
     * @param findMiuId The MIU ID fragment to find.
     * @param findNetwork The SIM MNO to match exactly.
     * @return True if the search criteria are met, otherwise false.
     */
    private boolean filterByMiuIdAndNetwork(CommandSummary commandSummary, String findMiuId, String findNetwork)
    {
        // Match by partial MIU ID
        if (StringUtils.hasText(findMiuId))
        {
            findMiuId = findMiuId.trim();

            if (!Long.toString(commandSummary.getMiuId()).contains(findMiuId))
            {
                return false;
            }
        }

        // Match by Network
        if (StringUtils.hasText(findNetwork))
        {
            findNetwork = findNetwork.trim();

            if (findNetwork.equalsIgnoreCase(NETWORK_NONE))
            {
                // No network
                if (StringUtils.hasText(commandSummary.getNetworkProvider()))
                {
                    return false;
                }
            }

            else if (!findNetwork.equalsIgnoreCase(commandSummary.getNetworkProvider()))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Gets a Comparator to apply sorting to a list of CommandSummary objects based on the supplied string.
     * @param sortBy A comma-separated list of zero or more field names, prefixed by + or - to determine ASC or DESC.
     * @return A Comparator function to apply the requested sort.
     */
    private Comparator<CommandSummary> getComparator(String sortBy)
    {
        // Start with an "everything-equal" comparator to which n "thenComparing" comparators can be appended.
        Comparator<CommandSummary> comparator = (t, p) -> 0;

        if (StringUtils.hasText(sortBy) && sortBy.length() > 1)
        {
            // Iterate over sort fields
            String[] sortByItems = sortBy.split(",");

            for (String sortByItem : sortByItems)
            {
                // Add comparator for the field
                final boolean sortAscending = sortByItem.startsWith("+");
                final String sortField = sortByItem.substring(1);

                Comparator<CommandSummary> c = getCommandSummaryComparator(sortAscending, sortField);
                if (c != null)
                {
                    comparator = comparator.thenComparing(c);
                }
            }
        }

        return comparator;
    }

    private Comparator<CommandSummary> getCommandSummaryComparator(boolean sortAscending, String sortField)
    {
        // Gets the comparator for the sortable field
        Comparator<CommandSummary> comparator = null;
        if (MIU_ID_FIELD.equals(sortField))
        {
            comparator = sortAscending ? MIU_ID_ASC_COMPARATOR : MIU_ID_DESC_COMPARATOR;
        }
        else if (SUBMITTED_FIELD.equals(sortField))
        {
            comparator = sortAscending ? SUBMITTED_ASC_COMPARATOR : SUBMITTED_DESC_COMPARATOR;
        }
        else if (USER_FIELD.equals(sortField))
        {
            comparator = sortAscending ? USER_ASC_COMPARATOR : USER_DESC_COMPARATOR;
        }
        else if (COMMAND_FIELD.equals(sortField))
        {
            comparator = sortAscending ? COMMAND_ASC_COMPARATOR : COMMAND_DESC_COMPARATOR;
        }
        else if (DETAILS_FIELD.equals(sortField))
        {
            comparator = sortAscending ? DETAILS_ASC_COMPARATOR : DETAILS_DESC_COMPARATOR;
        }
        else if (FULFULLED_FIELD.equals(sortField))
        {
            comparator = sortAscending ? FULFULLED_ASC_COMPARATOR : FULFULLED_DESC_COMPARATOR;
        }
        else if (NETWORK_PROVIDER_FIELD.equals(sortField))
        {
            comparator = sortAscending ? NETWORK_PROVIDER_ASC_COMPARATOR : NETWORK_PROVIDER_DESC_COMPARATOR;
        }

        return comparator;
    }
}
