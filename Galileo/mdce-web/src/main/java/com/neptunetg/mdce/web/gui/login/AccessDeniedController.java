/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.login;

import com.neptunetg.mdce.web.gui.common.DisplayStrings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * Handle 403 Access denied.
 */
@Controller
public class AccessDeniedController
{
    @Value("${whitelistedMode}")
    private boolean msocMode;

    public static final String ACCESS_DENIED_URL = "/pages/access_denied";


    @RequestMapping(value = ACCESS_DENIED_URL, method = RequestMethod.GET)
    public String viewAccessDenied(Principal user, Model model)
    {

        if (user != null)
        {
            model.addAttribute("userName", user.getName());
        }

        model.addAttribute("msocModeMessage", DisplayStrings.t("Status.MsocMode." + this.msocMode));

        return "/login/accessDenied.jsp";

    }


}
