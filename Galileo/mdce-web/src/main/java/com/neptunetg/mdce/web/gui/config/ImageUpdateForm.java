/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.config;

import java.util.ArrayList;
import java.util.List;

/**
 * Backing form for image update.
 */
public class ImageUpdateForm
{

    private boolean updateFirmware;
    private boolean updateBootloader;
    private boolean updateConfig;
    private boolean updateARBConfig;
    private boolean updateBleConfig;
    private boolean updateEncryption;

    private String firmwareImageVersion;
    private String bootloaderImageVersion;
    private String configImageVersion;
    private String ArbImageVersion;
    private String bleConfigImageVersion;
    private String encryptionImageVersion;

    private boolean registerNewImage;

    private boolean imageSelected;

    private List<MiuInput> miuInputList;

    public ImageUpdateForm()
    {
        miuInputList = new ArrayList<>();
        miuInputList.add(new MiuInput()); //add a blank CmiuDescription for the view to render the first row
    }

    public String getFirmwareImageVersion()
    {
        return firmwareImageVersion;
    }

    public void setFirmwareImageVersion(String firmwareImageVersion)
    {
        this.firmwareImageVersion = firmwareImageVersion;
    }

    public List<MiuInput> getMiuInputList()
    {
        return miuInputList;
    }

    public void setMiuInputList(List<MiuInput> miuInputList)
    {
        this.miuInputList = miuInputList;
    }

    public boolean isUpdateFirmware()
    {
        return updateFirmware;
    }

    public void setUpdateFirmware(boolean updateFirmware)
    {
        this.updateFirmware = updateFirmware;
    }

    public boolean isUpdateBootloader()
    {
        return updateBootloader;
    }

    public void setUpdateBootloader(boolean updateBootloader)
    {
        this.updateBootloader = updateBootloader;
    }

    public boolean isUpdateConfig()
    {
        return updateConfig;
    }

    public void setUpdateConfig(boolean updateConfig)
    {
        this.updateConfig = updateConfig;
    }

    public boolean isUpdateARBConfig()
    {
        return updateARBConfig;
    }

    public void setUpdateARBConfig(boolean updateARBConfig)
    {
        this.updateARBConfig = updateARBConfig;
    }

    public boolean isUpdateBleConfig()
    {
        return updateBleConfig;
    }

    public void setUpdateBleConfig(boolean updateBleConfig)
    {
        this.updateBleConfig = updateBleConfig;
    }

    public String getBootloaderImageVersion()
    {
        return bootloaderImageVersion;
    }

    public void setBootloaderImageVersion(String bootloaderImageVersion)
    {
        this.bootloaderImageVersion = bootloaderImageVersion;
    }

    public String getConfigImageVersion()
    {
        return configImageVersion;
    }

    public void setConfigImageVersion(String configImageVersion)
    {
        this.configImageVersion = configImageVersion;
    }

    public String getArbImageVersion()
    {
        return ArbImageVersion;
    }

    public void setArbImageVersion(String arbImageVersion)
    {
        ArbImageVersion = arbImageVersion;
    }

    public String getBleConfigImageVersion()
    {
        return bleConfigImageVersion;
    }

    public void setBleConfigImageVersion(String bleConfigImageVersion)
    {
        this.bleConfigImageVersion = bleConfigImageVersion;
    }

    public boolean isRegisterNewImage()
    {
        return registerNewImage;
    }

    public void setRegisterNewImage(boolean registerNewImage)
    {
        this.registerNewImage = registerNewImage;
    }

    public boolean isUpdateEncryption()
    {
        return updateEncryption;
    }

    public void setUpdateEncryption(boolean updateEncryption)
    {
        this.updateEncryption = updateEncryption;
    }

    public String getEncryptionImageVersion()
    {
        return encryptionImageVersion;
    }

    public void setEncryptionImageVersion(String encryptionImageVersion)
    {
        this.encryptionImageVersion = encryptionImageVersion;
    }

    public boolean isImageSelected()
    {
        return imageSelected;
    }

    public void setImageSelected(boolean imageSelected)
    {
        this.imageSelected = imageSelected;
    }
}
