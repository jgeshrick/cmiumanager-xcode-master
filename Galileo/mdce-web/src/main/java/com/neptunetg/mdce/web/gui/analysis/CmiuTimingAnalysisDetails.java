/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */
package com.neptunetg.mdce.web.gui.analysis;

/**
 * CMIU Timing Model.
 */
public class CmiuTimingAnalysisDetails
{
    private Long miuId;
    private Integer totalConnectionTimeMilliSeconds;
    private Integer registerTimeMilliSeconds;
    private Integer registerActivateContextTimeMilliSeconds;
    private Integer registerConnectionTimeMilliSeconds;
    private Integer registerTransferPacketTimeMilliSeconds;
    private Integer disconnectTimeMilliSeconds;
    private String cmiuMode;
    private String mno;
    private String state;
    private Long siteId;

    public CmiuTimingAnalysisDetails(){}

    public CmiuTimingAnalysisDetails(Long miuId, Integer totalConnectionTimeMilliSeconds, Integer registerTimeMilliSeconds,
                                     Integer registerActivateContextTimeMilliSeconds, Integer registerConnectionTimeMilliSeconds,
                                     Integer registerTransferPacketTimeMilliSeconds, Integer disconnectTimeMilliSeconds, String cmiuMode,
                                     String mno, String state, Long siteId) {
        this.miuId = miuId;
        this.totalConnectionTimeMilliSeconds = totalConnectionTimeMilliSeconds;
        this.registerTimeMilliSeconds = registerTimeMilliSeconds;
        this.registerActivateContextTimeMilliSeconds = registerActivateContextTimeMilliSeconds;
        this.registerConnectionTimeMilliSeconds = registerConnectionTimeMilliSeconds;
        this.registerTransferPacketTimeMilliSeconds = registerTransferPacketTimeMilliSeconds;
        this.disconnectTimeMilliSeconds = disconnectTimeMilliSeconds;
        this.cmiuMode = cmiuMode;
        this.mno = mno;
        this.state = state;
        this.siteId = siteId;
    }

    public Long getMiuId() {
        return miuId;
    }

    public void setMiuId(Long miuId) {
        this.miuId = miuId;
    }

    public Integer getTotalConnectionTimeMilliSeconds() {
        return totalConnectionTimeMilliSeconds;
    }

    public void setTotalConnectionTimeMilliSeconds(Integer totalConnectionTimeMilliSeconds) {
        this.totalConnectionTimeMilliSeconds = totalConnectionTimeMilliSeconds;
    }

    public Integer getRegisterTimeMilliSeconds() {
        return registerTimeMilliSeconds;
    }

    public void setRegisterTimeMilliSeconds(Integer registerTimeMilliSeconds) {
        this.registerTimeMilliSeconds = registerTimeMilliSeconds;
    }

    public Integer getRegisterActivateContextTimeMilliSeconds() {
        return registerActivateContextTimeMilliSeconds;
    }

    public void setRegisterActivateContextTimeMilliSeconds(Integer registerActivateContextTimeMilliSeconds) {
        this.registerActivateContextTimeMilliSeconds = registerActivateContextTimeMilliSeconds;
    }

    public Integer getRegisterConnectionTimeMilliSeconds() {
        return registerConnectionTimeMilliSeconds;
    }

    public void setRegisterConnectionTimeMilliSeconds(Integer registerConnectionTimeMilliSeconds) {
        this.registerConnectionTimeMilliSeconds = registerConnectionTimeMilliSeconds;
    }

    public Integer getRegisterTransferPacketTimeMilliSeconds() {
        return registerTransferPacketTimeMilliSeconds;
    }

    public void setRegisterTransferPacketTimeMilliSeconds(Integer registerTransferPacketTimeMilliSeconds) {
        this.registerTransferPacketTimeMilliSeconds = registerTransferPacketTimeMilliSeconds;
    }

    public Integer getDisconnectTimeMilliSeconds() {
        return disconnectTimeMilliSeconds;
    }

    public void setDisconnectTimeMilliSeconds(Integer disconnectTimeMilliSeconds) {
        this.disconnectTimeMilliSeconds = disconnectTimeMilliSeconds;
    }

    public String getCmiuMode() {
        return cmiuMode;
    }

    public void setCmiuMode(String cmiuMode) {
        this.cmiuMode = cmiuMode;
    }

    public String getMno() {
        return mno;
    }

    public void setMno(String mno) {
        this.mno = mno;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }
}
