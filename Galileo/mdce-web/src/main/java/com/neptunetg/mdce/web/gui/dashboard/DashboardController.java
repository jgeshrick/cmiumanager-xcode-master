/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.dashboard;


import com.neptunetg.mdce.common.internal.dashboard.model.StatusCounts;
import com.neptunetg.mdce.common.internal.dashboard.service.MsrService;
import com.neptunetg.mdce.web.backend.BackEndManager;
import com.neptunetg.mdce.web.gui.dashboard.amcharts.ChartDataResponse;
import com.neptunetg.mdce.web.gui.dashboard.amcharts.StatusCountItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;

/**
 * Displays the dashboard page
 */
@Controller
@Deprecated
public class DashboardController
{

    private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);

    @Autowired
    private BackEndManager backEndManager;

    @RequestMapping("/pages/dashboard")
    public String showDashboard()
    {
        return "dashboard/dashboard.jsp";
    }

    @RequestMapping("/pages/status")
    public String showStatus()
    {
        return "dashboard/status.jsp";
    }

    @RequestMapping("/pages/help")
    public String showHelp()
    {
        return "dashboard/help.jsp";
    }

/*
        var chartData = [{
            "status": "Missed",
            "count": 23
        }, {
            "status": "Received",
            "count": 77
        }];
 */

    /**
     * Handle AJAX request
     * @return JSON
     */
    @RequestMapping("/chartdata/msr")
    @ResponseBody
    public ChartDataResponse<StatusCountItem> getMsrData()
    {
        final ChartDataResponse<StatusCountItem> ret = new ChartDataResponse<>();

        final StatusCountItem successCount = new StatusCountItem("Received", "#548235");
        final StatusCountItem failCount = new StatusCountItem("Missed", "#ff3300");
        final StatusCountItem pendingCount = new StatusCountItem("Pending", "#a0a0a0");

        try
        {
            final MsrService service = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MsrService.class);

            final StatusCounts counts = service.getMsrByGroup("0");

            successCount.add(counts.getSuccessCount());
            failCount.add(counts.getFailCount());
            pendingCount.add(counts.getPendingCount());

            ret.setRows(Arrays.asList(successCount, failCount, pendingCount));
        }
        catch (Exception e)
        {
            logger.error("Error getting MSR data", e);
            ret.setError("Error getting data from back end service: " + e.getMessage());
        }

        return ret;
    }

}
