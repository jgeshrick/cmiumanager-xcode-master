/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.config;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * Backing form for CmiuConfigSet
 */
public class CmiuConfigSetForm
{
    private long id;

    @NotEmpty(message = "Please enter a name for the config set")
    private String name;

    @Min(0) @Max(65535)
    private int reportingPlanStartMins;

    @Min(0) @Max(255)
    private int reportingPlanIntervalHours;

    @Min(0) @Max(65535)
    private int reportingPlanTransmitWindowMins;

    @Min(0) @Max(65535)
    private int reportingPlanQuietStartMins;

    @Min(0) @Max(65535)
    private int reportingPlanQuietEndMins;

    @Min(0) @Max(255)
    private int reportingPlanNumberOfRetries;

    @Min(0) @Max(65535)
    private int recordingPlanStartTimeMins;

    @Min(0) @Max(255)
    private int recordingPlanIntervalMins;

    @NotEmpty(message = "Please enter a CMIU mode name for the config set")
    private String cmiuModeName;

    private boolean officialMode;


    private boolean addNewConfig = false;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getReportingPlanStartMins()
    {
        return reportingPlanStartMins;
    }

    public void setReportingPlanStartMins(int reportingPlanStartMins)
    {
        this.reportingPlanStartMins = reportingPlanStartMins;
    }

    public int getReportingPlanIntervalHours()
    {
        return reportingPlanIntervalHours;
    }

    public void setReportingPlanIntervalHours(int reportingPlanIntervalHours)
    {
        this.reportingPlanIntervalHours = reportingPlanIntervalHours;
    }

    public int getReportingPlanTransmitWindowMins()
    {
        return reportingPlanTransmitWindowMins;
    }

    public void setReportingPlanTransmitWindowMins(int reportingPlanTransmitWindowMins)
    {
        this.reportingPlanTransmitWindowMins = reportingPlanTransmitWindowMins;
    }

    public int getReportingPlanQuietStartMins()
    {
        return reportingPlanQuietStartMins;
    }

    public void setReportingPlanQuietStartMins(int reportingPlanQuietStartMins)
    {
        this.reportingPlanQuietStartMins = reportingPlanQuietStartMins;
    }

    public int getReportingPlanQuietEndMins()
    {
        return reportingPlanQuietEndMins;
    }

    public void setReportingPlanQuietEndMins(int reportingPlanQuietEndMins)
    {
        this.reportingPlanQuietEndMins = reportingPlanQuietEndMins;
    }

    public int getRecordingPlanStartTimeMins()
    {
        return recordingPlanStartTimeMins;
    }

    public void setRecordingPlanStartTimeMins(int recordingPlanStartTimeMins)
    {
        this.recordingPlanStartTimeMins = recordingPlanStartTimeMins;
    }

    public int getRecordingPlanIntervalMins()
    {
        return recordingPlanIntervalMins;
    }

    public void setRecordingPlanIntervalMins(int recordingPlanIntervalMins)
    {
        this.recordingPlanIntervalMins = recordingPlanIntervalMins;
    }

    public int getReportingPlanNumberOfRetries()
    {
        return reportingPlanNumberOfRetries;
    }

    public void setReportingPlanNumberOfRetries(int reportingPlanNumberOfRetries)
    {
        this.reportingPlanNumberOfRetries = reportingPlanNumberOfRetries;
    }

    /** Whether this form is for adding a new config or editing an existing config set
     *
     * @return
     */
    public String getDisplayHeader()
    {
        return this.isAddNewConfig()? "Define new config set":"Edit config set";
    }

    public boolean isAddNewConfig()
    {
        return addNewConfig;
    }

    public void setAddNewConfig(boolean addNewConfig)
    {
        this.addNewConfig = addNewConfig;
    }

    public String getCmiuModeName()
    {
        return cmiuModeName;
    }

    public void setCmiuModeName(String cmiuModeName)
    {
        this.cmiuModeName = cmiuModeName;
    }

    public boolean getOfficialMode()
    {
        return officialMode;
    }

    public void setOfficialMode(boolean officialMode)
    {
        this.officialMode = officialMode;
    }
}
