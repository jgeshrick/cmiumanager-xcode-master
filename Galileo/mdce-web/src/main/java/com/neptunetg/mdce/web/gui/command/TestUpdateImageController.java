/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.command;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import com.neptunetg.mdce.common.internal.miu.service.MiuCommandControlService;
import com.neptunetg.mdce.common.internal.miu.service.CmiuImageUpdateService;
import com.neptunetg.mdce.web.backend.BackEndManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.neptunetg.mdce.web.gui.login.LoginUtils.getLoggedInUserName;

/**
 * Image update
 */
@Controller
@Deprecated
public class TestUpdateImageController
{
    private static final Logger logger = LoggerFactory.getLogger(TestUpdateImageController.class);

    @Autowired
    private BackEndManager backEndManager;

    /**
     * A test webpage for single button image update for the purpose of Proof of concept.
     */
    @RequestMapping(value = "/pages/config/test-image-update", method = RequestMethod.GET)
    public String getTestImageUpdateView()
    {
        return "updateimage/testImageUpdate.jsp";
    }

    @RequestMapping(value = "/pages/config/test-image-update", method = RequestMethod.POST)
    public String submitTestImageUpdate(
            @RequestParam ("cmiuId") long cmiuId,
            @RequestParam("imageType") String imageType,
            @RequestParam("startAddress") String startAddress,
            @RequestParam("sizeByte") String sizeByte,
            @RequestParam("revMajor") String revMajor,
            @RequestParam("revMinor") String revMinor,
            @RequestParam("revDate") String revDate,
            @RequestParam("revBuild") String revBuild,
            @RequestParam ("fileUrl") String fileUrl) throws InternalApiException
    {
        //submit to backend
        final CmiuImageUpdateService service = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuImageUpdateService.class);

        //we are going to create a hardcoded image description here
        ImageDescription imageDescription = new ImageDescription();
        imageDescription.setFileName(fileUrl);

        switch(imageType)
        {
            case "FirmwareImage":
                imageDescription.setImageType(ImageDescription.ImageType.FirmwareImage);
                break;

            case "BootloaderImage":
                imageDescription.setImageType(ImageDescription.ImageType.BootloaderImage);
                break;

            case "ArbImage":
                imageDescription.setImageType(ImageDescription.ImageType.ArbConfigImage);
                break;

            case "ConfigImage":
                imageDescription.setImageType(ImageDescription.ImageType.ConfigImage);
                break;

            case "BleConfigImage":
                imageDescription.setImageType(ImageDescription.ImageType.BleConfigImage);
                break;

            case "EncryptionImage":
                imageDescription.setImageType(ImageDescription.ImageType.EncryptionImage);
                break;
        }

        imageDescription.setStartAddress(Integer.parseInt(startAddress, 16));
        imageDescription.setSizeBytes(Integer.parseInt(sizeByte, 16));
        imageDescription.setRevision(
                Integer.parseInt(revMajor),
                Integer.parseInt(revMinor),
                Integer.parseInt(revDate),
                Integer.parseInt(revBuild));


//        service.updateCmiuImage(cmiuId, imageDescription);

        final MiuCommandControlService commandService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MiuCommandControlService.class);

        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(getLoggedInUserName());
        commandRequest.setCommandCmiu(CommandRequest.CommandTypeCmiu.COMMAND_UPDATE_IMAGE);
        long executionTime = Instant.now().getEpochSecond();
        commandRequest.setExecutionTimeEpochSecond(executionTime);  //fixme, change to user configurable time

        List<ImageDescription> imageDescriptionList = new ArrayList<>();
        imageDescriptionList.add(imageDescription);

        commandRequest.setImageDescription(imageDescriptionList);

        //add list of CMIU
        commandRequest.setMiuList(Arrays.asList(cmiuId));

        commandService.sendCommand(commandRequest);

        return "redirect:/";
    }




}
