/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.miu;

import org.springframework.util.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by MC5 on 10/11/2016.
 * Contains navigation data for the MIU List page.
 */
public class MiuListNavigationModel
{
    /**
     * How many extra pages to link in each direction.
     */
    private static final int PAGE_LINK_RANGE = 3;

    private static final String BASE_PATH = "/pages/miu/miulist";

    private static final String PAGE_PARAM = "page";
    private static final String ENTERED_ID_PARAM = "enteredId";
    private static final String ENTERED_ID_TYPE_PARAM = "enteredIdType";
    private static final String SELECTED_MIU_TYPE_PARAM = "selectedMiuType";
    private static final String SELECTED_MANAGER_PARAM = "selectedManager";
    private static final String SELECTED_LOCATION_PARAM = "selectedLocation";
    private static final String SELECTED_SITE_PARAM = "selectedSite";
    private static final String SELECTED_CMIU_MODE_PARAM = "selectedCmiuMode";
    private static final String SELECTED_NETWORK_PROVIDER_PARAM = "selectedNetworkProvider";

    private final int toRowNumber;
    private final int fromRowNumber;
    private final int resultsPerPage;
    private final boolean hasMultiplePages;
    private final String firstPageHref;
    private final String previousPageHref;
    private final List<PageReference> previousPageHrefs;
    private final List<PageReference> nextPageHrefs;
    private final String nextPageHref;
    private final String lastPageHref;
    private final int numberOfRows;
    private final int page;
    private final int numberOfPages;

    /**
     * Creates a new instance of the page navigation model
     * @param resultsPerPage
     * @param page
     * @param numberOfRows
     * @param miuListFilterForm
     */
    public MiuListNavigationModel(int resultsPerPage, int page, int numberOfRows, MiuListFilterForm miuListFilterForm)
    {
        this.resultsPerPage = resultsPerPage;
        this.page = page;
        this.numberOfRows = numberOfRows;

        previousPageHrefs = new ArrayList<>();
        nextPageHrefs = new ArrayList<>();

        int startIndex = (page - 1) * resultsPerPage;
        numberOfPages = ((numberOfRows - 1) / resultsPerPage) + 1;
        fromRowNumber = startIndex + 1;
        toRowNumber = smallerOf(startIndex + resultsPerPage, numberOfRows);
        hasMultiplePages = numberOfPages > 1;

        // Build first & prev links
        if (page > 1)
        {
            firstPageHref = BASE_PATH + getQueryString(1, miuListFilterForm);
            previousPageHref = BASE_PATH + getQueryString(page - 1, miuListFilterForm);
        }
        else
        {
            firstPageHref = null;
            previousPageHref = null;
        }

        // Build next & last links
        if (page < numberOfPages)
        {
            nextPageHref = BASE_PATH + getQueryString(page + 1, miuListFilterForm);
            lastPageHref = BASE_PATH + getQueryString(numberOfPages, miuListFilterForm);
        }
        else
        {
            nextPageHref = null;
            lastPageHref = null;
        }

        // Build prev & next page link maps
        for (int i = 0; i < PAGE_LINK_RANGE; i++)
        {
            int prevPage = page - PAGE_LINK_RANGE + i;

            if (prevPage >= 1)
            {
                previousPageHrefs.add(new PageReference(prevPage, BASE_PATH + getQueryString(prevPage, miuListFilterForm)));
            }
            else
            {
                previousPageHrefs.add(new PageReference(0, null));
            }

            int nextPage = page + 1 + i;

            if (nextPage <= numberOfPages)
            {
                nextPageHrefs.add(new PageReference(nextPage, BASE_PATH + getQueryString(nextPage, miuListFilterForm)));
            }
            else
            {
                nextPageHrefs.add(new PageReference(0, null));
            }
        }
    }

    public int getResultsPerPage()
    {
        return resultsPerPage;
    }

    public boolean isHasMultiplePages()
    {
        return hasMultiplePages;
    }

    public String getNextPageHref()
    {
        return nextPageHref;
    }

    public String getPreviousPageHref()
    {
        return previousPageHref;
    }

    public int getNumberOfRows()
    {
        return numberOfRows;
    }

    public int getPage()
    {
        return page;
    }

    public int getFromRowNumber()
    {
        return fromRowNumber;
    }

    public int getToRowNumber()
    {
        return toRowNumber;
    }

    public String getFirstPageHref()
    {
        return firstPageHref;
    }

    public String getLastPageHref()
    {
        return lastPageHref;
    }

    public List<PageReference> getPreviousPageHrefs()
    {
        return previousPageHrefs;
    }

    public List<PageReference> getNextPageHrefs()
    {
        return nextPageHrefs;
    }

    public int getPageLinkRange()
    {
        return PAGE_LINK_RANGE;
    }

    public int getNumberOfPages()
    {
        return numberOfPages;
    }

    private int smallerOf(int a, int b)
    {
        return a > b ? b : a;
    }

    private String getQueryString(int page, MiuListFilterForm miuListFilterForm)
    {
        List<String> parameters = new ArrayList<>();

        // Add any parameters to the URL query data
        if (page != 1)
        {
            parameters.add(PAGE_PARAM + "=" + page);
        }

        if (StringUtils.hasText(miuListFilterForm.getEnteredId()))
        {
            parameters.add(ENTERED_ID_PARAM + "=" + urlEncode(miuListFilterForm.getEnteredId()));
        }

        if (StringUtils.hasText(miuListFilterForm.getEnteredIdType()))
        {
            parameters.add(ENTERED_ID_TYPE_PARAM + "=" + urlEncode(miuListFilterForm.getEnteredIdType()));
        }

        if (StringUtils.hasText(miuListFilterForm.getSelectedMiuType()))
        {
            parameters.add(SELECTED_MIU_TYPE_PARAM + "=" + urlEncode(miuListFilterForm.getSelectedMiuType()));
        }

        if (StringUtils.hasText(miuListFilterForm.getSelectedManager()))
        {
            parameters.add(SELECTED_MANAGER_PARAM + "=" + urlEncode(miuListFilterForm.getSelectedManager()));
        }

        if (StringUtils.hasText(miuListFilterForm.getSelectedLocation()))
        {
            parameters.add(SELECTED_LOCATION_PARAM + "=" + urlEncode(miuListFilterForm.getSelectedLocation()));
        }

        if (StringUtils.hasText(miuListFilterForm.getSelectedSite()))
        {
            parameters.add(SELECTED_SITE_PARAM + "=" + urlEncode(miuListFilterForm.getSelectedSite()));
        }

        if (StringUtils.hasText(miuListFilterForm.getSelectedCmiuMode()))
        {
            parameters.add(SELECTED_CMIU_MODE_PARAM + "=" + urlEncode(miuListFilterForm.getSelectedCmiuMode()));
        }

        if (StringUtils.hasText(miuListFilterForm.getSelectedNetworkProvider()))
        {
            parameters.add(SELECTED_NETWORK_PROVIDER_PARAM + "=" + urlEncode(miuListFilterForm.getSelectedNetworkProvider()));
        }

        String queryString = String.join("&", parameters);

        // Prepend '?' delimiter if there are any query parameters
        if (StringUtils.hasText(queryString))
        {
            queryString = "?" + queryString;
        }

        return queryString;
    }

    private String urlEncode(String text)
    {
        try
        {
            return URLEncoder.encode(text, "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            return "";
        }
    }
}
