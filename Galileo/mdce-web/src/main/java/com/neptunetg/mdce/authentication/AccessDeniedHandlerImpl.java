/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.authentication;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.audit.service.AuditLogService;
import com.neptunetg.mdce.web.backend.BackEndManager;
import com.neptunetg.mdce.web.gui.login.AccessDeniedController;
import com.neptunetg.mdce.web.gui.login.UserController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * AccessDeniedHandler invoked by spring security on csrf token expiry or access denied.
 */
@Service
public class AccessDeniedHandlerImpl implements AccessDeniedHandler
{
    private static final Logger logger = LoggerFactory.getLogger(AccessDeniedHandlerImpl.class);

    private final BackEndManager backEndManager;

    @Autowired
    public AccessDeniedHandlerImpl(BackEndManager backEndManager)
    {
        this.backEndManager = backEndManager;
    }

    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException
    {
        logger.debug("{}", httpServletRequest);
        logger.debug("exception: ", e);

        final String baseUrl = httpServletRequest.getContextPath();

        if (e instanceof InvalidCsrfTokenException)
        {
            //csrf token has expired.. redirect to login with redirect url set.
            httpServletRequest.getSession().setAttribute("errorMessage", "Session has expired. Please login again");
            httpServletResponse.sendRedirect(baseUrl + UserController.LOGIN_URL);
        }
        else if (e instanceof AuthorizationServiceException)
        {
            //we should not end here because onAuthenticationFailure should have already redirected user.
            httpServletRequest.getSession().setAttribute("errorMessage", "Fail to verify user credential. Please check user name and password.");
            httpServletRequest.getSession().setAttribute("redirectAfterLogin", httpServletRequest.getRequestURI());
            httpServletResponse.sendRedirect(baseUrl + UserController.LOGIN_URL);
        }
        else
        {
            this.logDenial(httpServletRequest.getRemoteUser(), "Access Denied To URL: " + httpServletRequest.getRequestURI());
            //redirect to access denied page
            httpServletResponse.sendRedirect(baseUrl + AccessDeniedController.ACCESS_DENIED_URL);

        }

    }

    private void logDenial(String userName, String description)
    {
        try
        {
            AuditLogService auditLogService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), AuditLogService.class);
            auditLogService.logAccessDenied(userName, description);
        }
        catch (InternalApiException e)
        {
            logger.error("Exception while logging denial of access for " + userName + " (" + description + ")", e);
        }
    }
}
