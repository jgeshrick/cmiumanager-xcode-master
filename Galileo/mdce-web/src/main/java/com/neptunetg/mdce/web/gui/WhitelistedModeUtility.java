/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

/**
 * A convenient utility to evaluate the context parameter whitelistedMode, defined in web.xml (and NOT available in root context).
 * This is used to check whether MSOC mode is set, to restrict access to certain @Controller methods (webpage) on top of
 * Spring security check.
 */
@Controller
public class WhitelistedModeUtility
{
    private final boolean whitelistedMode;

    @Autowired
    public WhitelistedModeUtility(@Value("${whitelistedMode}") boolean whitelistedMode)
    {
        this.whitelistedMode = whitelistedMode;
    }

    /**
     * Use by PreAuthorise Spring EL to check, on top of user authentication, whether the whitelistedMode which is set in
     * servlet context is true. This is to restrict access to admin page if this mdce-web instance does not have whitelistedMode enabled.
     */
    public boolean whitelistedMode()
    {
        return whitelistedMode;
    }
}
