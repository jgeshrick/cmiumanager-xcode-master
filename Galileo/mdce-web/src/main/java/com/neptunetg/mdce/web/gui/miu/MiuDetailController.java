/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.miu;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.lora.service.LoraService;
import com.neptunetg.mdce.common.internal.miu.model.MiuDisplayDetails;
import com.neptunetg.mdce.common.internal.miu.model.MiuDisplayDetailsPagedResult;
import com.neptunetg.mdce.common.internal.miu.service.MiuService;
import com.neptunetg.mdce.common.internal.sim.service.SimService;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataUtility;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.common.internal.site.service.RefService;
import com.neptunetg.mdce.common.internal.site.service.SiteService;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import com.neptunetg.mdce.web.gui.audit.AuditLogsFilterForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.neptunetg.mdce.web.gui.login.LoginUtils.getLoggedInUserName;

/**
 * For administrating miu. All url in this page is accessible only to login miu with MsocAdmin authority
 */

@Controller
public class MiuDetailController
{
    private static final Logger logger = LoggerFactory.getLogger(MiuDetailController.class);

    /**
     * If this value is changed, it also needs to be changed in (or passed to and handled by) the MiuSearchRepository
     * implementation.
     */
    private static final int RESULTS_PER_PAGE = 100;

    private static final String SITE_LIST_OPTIONS = "siteListOptions";
    private static final String TOTAL_RESULTS = "totalResults";
    private static final String NAVIGATION_MODEL = "navigationModel";
    private static final String USER = "user";
    private static final String DAYS_AGO = "daysAgo";
    private static final String PAGE = "page";
    private static final String MIU_LIST = "miuList";
    private static final String MIU_DETAILS = "miuDetails";
    private static final String SITE_DETAILS = "siteRefData";
    private static final String SETTABLE_LIFECYCLE_STATES = "lifecycleStates";
    private static final String MIU_ID = "miuId";
    private static final String SITE_ID = "siteId";
    private static final String SITE_NAME = "siteName";
    private static final String REGIONAL_MANAGER = "regionalManager";
    private static final String LOCATION = "location";
    private static final String MIU_TYPE_OPTION = "miuType";
    private static final String CMIU_MODE_OPTION = "cmiuMode";
    private static final String NETWORK_PROVIDER_OPTION = "networkProvider";

    private static final String DEFAULT_SELECTION = "";

    private BackEndManager backEndManager;

    @Autowired
    public MiuDetailController(BackEndManager backEndManager)
    {
        this.backEndManager = backEndManager;
    }

    @ModelAttribute("auditLogFilterForm")
    private AuditLogsFilterForm getAuditLogsFilterForm()
    {
        return new AuditLogsFilterForm();
    }

    @ModelAttribute("miuListFilterForm")
    private MiuListFilterForm getMiuListFilterForm() throws InternalApiException
    {
        return new MiuListFilterForm();
    }

    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    @RequestMapping(value = "/pages/miu/miulist", method = RequestMethod.GET)
    public String listMiuDetails(
            @ModelAttribute("miuListFilterForm") MiuListFilterForm miuListFilterForm,
            ModelMap model
    ) throws Exception
    {
        final HashMap<String, String> options = new HashMap<>();

        addKeyValueToMapIfNotNull(REGIONAL_MANAGER, miuListFilterForm.getSelectedManager(), options);
        addKeyValueToMapIfNotNull(LOCATION, miuListFilterForm.getSelectedLocation(), options);
        addKeyValueToMapIfNotNull(SITE_ID, miuListFilterForm.getSelectedSite(), options);
        addKeyValueToMapIfNotNull(SITE_NAME, miuListFilterForm.getSelectedSiteName(), options);
        addKeyValueToMapIfNotNull(MIU_TYPE_OPTION, miuListFilterForm.getSelectedMiuType(), options);
        addKeyValueToMapIfNotNull(CMIU_MODE_OPTION, miuListFilterForm.getSelectedCmiuMode(), options);
        addKeyValueToMapIfNotNull(NETWORK_PROVIDER_OPTION, miuListFilterForm.getSelectedNetworkProvider(), options);

        // Add the ID query using the 'select' option value as the key
        String enteredIdType = miuListFilterForm.getEnteredIdType();
        if (StringUtils.hasText(enteredIdType))
        {
            addKeyValueToMapIfNotNull(enteredIdType, miuListFilterForm.getEnteredId(), options);
        }
        else
        {
            miuListFilterForm.setEnteredId("");
        }

        int newPage = miuListFilterForm.getPageNumberForPagedResult();

        addKeyValueToMapIfNotNull(PAGE, String.valueOf(newPage), options);

        MiuDisplayDetailsPagedResult miuDetails = getMiuService().getMiuList(options);

        // Set up navigation details for pagination
        MiuListNavigationModel navigationModel = new MiuListNavigationModel(
                RESULTS_PER_PAGE, miuDetails.getPageNum() + 1, miuDetails.getTotalResults(), miuListFilterForm);

        model.addAttribute(MIU_LIST, miuDetails.getResults());
        model.addAttribute(TOTAL_RESULTS, miuDetails.getTotalResults());
        model.addAttribute(NAVIGATION_MODEL, navigationModel);

        final RefService refService = getRefDataService();

        miuListFilterForm.setManagers(setupSelectionsWithFirstEntryAsUndefined(refService.getRegionalManagers()));
        miuListFilterForm.setLocations(setupSelectionsWithFirstEntryAsUndefined(refService.getStates()));
        miuListFilterForm.setMiuTypes(setupSelectionsWithFirstEntryAsUndefined(refService.getMiuTypes()));
        miuListFilterForm.setCmiuModeOptions(setupSelectionsWithFirstEntryAsUndefined(getMiuService().getAllCmiuModes()));

        List<String> networks = new ArrayList<>();
        networks.addAll(getSimService().getSimMnoList());
        networks.addAll(getLoraService().getLoraNetworkList());
        miuListFilterForm.setNetworkProviderOptions(setupSelectionsWithFirstEntryAsUndefined(networks));

        final List<SiteDetails> siteDetailsArrayList;


        if (hasSelection(miuListFilterForm.getSelectedManager()))
        {
            siteDetailsArrayList = refService.getSitesForRegionalManager(miuListFilterForm.getSelectedManager());
        }
        else if (hasSelection(miuListFilterForm.getSelectedLocation()))
        {
            siteDetailsArrayList = refService.getSitesForState(miuListFilterForm.getSelectedLocation());
        }
        else if (hasSelection(miuListFilterForm.getSelectedSite()))
        {
            final int siteId = parseSiteId(miuListFilterForm.getSelectedSite());
            siteDetailsArrayList = Arrays.asList(getSiteService().getSiteDetails(siteId));
        }
        else
        {
            siteDetailsArrayList = getSiteService().getSiteList();
        }

        miuListFilterForm.setSiteIds(siteDetailsArrayList);

        augmentFormWithSiteName(miuListFilterForm, siteDetailsArrayList);
        augmentMiuListWithSiteNames(miuDetails.getResults(), siteDetailsArrayList);

        model.addAttribute("miuListFilterForm", miuListFilterForm);
        model.addAllAttributes(options);

        return "miu/miulist.jsp";
    }

    private int parseSiteId(String selectedSite)
    {
        try
        {
            return Integer.parseInt(selectedSite);
        }
        catch (NumberFormatException e)
        {
            logger.error("Bad site ID from form: " + selectedSite, e);
            return -1;
        }
    }

    /**
     * Add a key value pair to a supplied dictionary if the value is not null.
     * @param key key
     * @param value value string
     * @param options dictionary hash map where the key value pair is added
     */
    private static void addKeyValueToMapIfNotNull(String key, String value, final HashMap<String, String> options)
    {
        if (value != null)
        {
            options.put(key, value);
        }
    }

    @RequestMapping(value = "/pages/miu/miudetail/{miuId}", method = RequestMethod.GET)
    @PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
    public String getMiuDetails(ModelMap model,
                                @PathVariable("miuId") int miuId,
                                @ModelAttribute("auditLogFilterForm") @Valid AuditLogsFilterForm auditLogsFilterForm,
                                BindingResult bindingResult
    ) throws Exception {
        final MiuService miuService = getMiuService();

        //page display on html starts from 1, whereas internally it is base 0
        int newPage = auditLogsFilterForm.getPageNumberForPagedResult();

        final HashMap<String, String> options = new HashMap<>();

        addKeyValueToMapIfNotNull(MIU_ID, String.valueOf(miuId), options);
        addKeyValueToMapIfNotNull(USER, auditLogsFilterForm.getUser(), options);
        addKeyValueToMapIfNotNull(DAYS_AGO, String.valueOf(auditLogsFilterForm.getDaysAgo()), options);
        addKeyValueToMapIfNotNull(PAGE, String.valueOf(newPage), options);

        auditLogsFilterForm.setPage(newPage);

        final MiuDisplayDetailsPagedResult miuDetailsList = miuService.getMiuList(options);

        if (miuDetailsList.getNumResults() == null || miuDetailsList.getNumResults() < 1) {
            return HttpStatus.NOT_FOUND.toString();
        }

        final MiuDisplayDetails miuDetails = miuDetailsList.getResults().get(0);

        final RefService refDataService = getRefDataService();
        final MdceReferenceDataUtility siteRefData = refDataService.getSiteReferenceData(miuDetails.getSiteId());
        final List<SiteDetails> allSites = getSiteService().getSiteList();
        augmentMiuDetailsWithSiteName(miuDetails, allSites);

        // Remove current site from site list
        final List<SiteDetails> sitesExcludeCurrent = new ArrayList<>(allSites);
        removeSiteFromSiteList(miuDetails.getSiteId(), sitesExcludeCurrent);

        // Get possible lifecycle States
        List<String> settableLifecycleStates = miuService.getSetableLifecycleStates(miuId, miuDetails.getType());

        final ZonedDateTime midnightUtc = Instant.now().atZone(ZoneOffset.UTC).withHour(0).withMinute(0).withSecond(0).withNano(0);
        final ZonedDateTime startOfMonthMidnightUtc = midnightUtc.withDayOfMonth(1);

        model.addAttribute("auditLogFilterForm", auditLogsFilterForm);
        model.addAttribute(MIU_DETAILS, miuDetails);
        model.addAttribute(SITE_DETAILS, siteRefData);
        model.addAttribute(SITE_LIST_OPTIONS, sitesExcludeCurrent);
        model.addAttribute(SETTABLE_LIFECYCLE_STATES, settableLifecycleStates);
        model.addAttribute("midnightUtc", midnightUtc);
        model.addAttribute("startOfMonthMidnightUtc", startOfMonthMidnightUtc);

        model.addAllAttributes(options);

        return "miu/miudetail.jsp";
    }


    private static void removeSiteFromSiteList(int siteId, Collection<SiteDetails> sites)
    {
        sites.removeIf(p -> p.getSiteId() == siteId);
    }


    private static void augmentMiuListWithSiteNames(Collection<MiuDisplayDetails> mius, Collection<SiteDetails> sites)
    {
        final Map<Integer, SiteDetails> siteDetailsLookup = new HashMap<>();

        for(SiteDetails site : sites)
        {
            siteDetailsLookup.put(site.getSiteId(), site);
        }

        for (MiuDisplayDetails miuDisplayDetails : mius)
        {
            final SiteDetails site = siteDetailsLookup.get(miuDisplayDetails.getSiteId());

            if (site != null)
            {
                miuDisplayDetails.setSiteDescription(site.getSiteName());
            }
        }
    }

    private static void augmentFormWithSiteName(MiuListFilterForm form, Collection<SiteDetails> sites)
    {

        final String siteIdString = form.getSelectedSite();
        if (StringUtils.hasText(siteIdString))
        {
            try
            {
                int siteId = Integer.parseInt(siteIdString);
                sites
                        .stream()
                        .filter(s -> s.getSiteId() == siteId)
                        .findFirst()
                        .ifPresent(s -> form.setSelectedSiteName(s.getSiteName()));
            }
            catch (NumberFormatException e)
            {
                logger.error("Bad site ID from form: " + siteIdString, e);
            }
        }

    }

    private static void augmentMiuDetailsWithSiteName(MiuDisplayDetails miu, Collection<SiteDetails> sites)
    {

        sites
                .stream()
                .filter(s -> s.getSiteId() == miu.getSiteId())
                .findFirst()
                .ifPresent(s -> miu.setSiteDescription(s.getSiteName()));

    }

    @RequestMapping(value = "/pages/miu/miudetail/{miuId}", method = RequestMethod.POST)
    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    public String changeMiuOwnershipOrLifecycleState(
            @PathVariable(MIU_ID) int miuId,
            @RequestParam(value = "newSiteId", required = false) Integer newSiteId,
            @RequestParam(value = "lifecycleState", required = false) String newLifecycleState) throws InternalApiException
    {
        final MiuService miuService = getMiuService();

        if(newSiteId != null)
        {
            miuService.claimMiu(miuId, newSiteId, getLoggedInUserName());
        }
        else
        {
            miuService.setLifecycleState(miuId, newLifecycleState);
        }

        return "redirect:/pages/miu/miudetail/" + miuId;
    }

    private MiuService getMiuService() throws InternalApiException
    {
        return backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MiuService.class);
    }

    private SimService getSimService() throws InternalApiException
    {
        return backEndManager.createClient(backEndManager.getAllBackEndConfigs(), SimService.class);
    }

    private LoraService getLoraService() throws InternalApiException
    {
        return backEndManager.createClient(backEndManager.getAllBackEndConfigs(), LoraService.class);
    }

    private SiteService getSiteService() throws InternalApiException
    {
        return backEndManager.createClient(backEndManager.getAllBackEndConfigs(), SiteService.class);
    }

    private RefService getRefDataService() throws InternalApiException
    {
        return backEndManager.createClient(backEndManager.getAllBackEndConfigs(), RefService.class);
    }

    private List<String> setupSelectionsWithFirstEntryAsUndefined(List<String> selectionList)
    {
        List<String> list = new ArrayList<>();
        list.add(DEFAULT_SELECTION);
        list.addAll(selectionList);

        return list;
    }

    /**
     * Check if a value received from a html:select tag has value defined and it is not the first "default" option.
     */
    private boolean hasSelection(String selectValue)
    {
        return selectValue != null && !selectValue.equals(DEFAULT_SELECTION);
    }
}
