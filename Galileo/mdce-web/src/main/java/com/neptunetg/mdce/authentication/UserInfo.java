/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.authentication;


import com.neptunetg.mdce.common.internal.user.model.MdceUserRole;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

public class UserInfo extends UsernamePasswordAuthenticationToken
{

    private static final long serialVersionUID = 1L;

    private final MdceUserRole mdceUserRole;
    private boolean allowConcurrentSessions;

    UserInfo(String username, String password, MdceUserRole userRole, List<GrantedAuthority> grantedAuths,
             boolean allowConcurrentSessions)
    {
        super(username, password, grantedAuths);
        this.mdceUserRole = userRole;
        this.allowConcurrentSessions = allowConcurrentSessions;
    }

    public boolean isAuthenticated()
    {
        return mdceUserRole.getAuthLevel() > MdceUserRole.NotAuthorized.getAuthLevel();
    }

    public String getUserRoleDisplayString()
    {
        return this.mdceUserRole.getRoleName();
    }

    public boolean getAllowConcurrentSessions()
    {
        return allowConcurrentSessions;
    }

    public void setAllowConcurrentSessions(boolean allowConcurrentSessions)
    {
        this.allowConcurrentSessions = allowConcurrentSessions;
    }
}
