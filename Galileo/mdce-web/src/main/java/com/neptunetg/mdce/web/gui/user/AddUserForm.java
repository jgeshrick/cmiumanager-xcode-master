/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.user;

import com.neptunetg.mdce.common.internal.user.model.MdceUserRole;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Size;

/**
 * Created by WJD1 on 13/08/2015.
 * A form to hold information for creating a new user
 */
public class AddUserForm
{
    @Size(min = 3, max = 30, message = "User name must have at least 3 characters")
    private String userName;

    private MdceUserRole userLevel;

    @NotEmpty(message = "Please enter your email address")
    @Email(message = "Invalid email format")
    private String email;

    @NotEmpty(message = "Please enter your password.")
    @Size(min = 6, max = 50, message = "Your password must between 6 and 50 characters")
    private String password;
    private String passwordRepeated;

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public MdceUserRole getUserLevel()
    {
        return userLevel;
    }

    public void setUserLevel(MdceUserRole userLevel)
    {
        this.userLevel = userLevel;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email.trim();
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password.trim();
    }

    public String getPasswordRepeated()
    {
        return passwordRepeated;
    }

    public void setPasswordRepeated(String passwordRepeated)
    {
        this.passwordRepeated = passwordRepeated;
    }

    @AssertTrue(message = "Passwords do not match")
    public boolean isPasswordMatch()
    {
        return !(this.password == null || this.passwordRepeated == null) &&
                this.password.equals(this.passwordRepeated);

    }
}
