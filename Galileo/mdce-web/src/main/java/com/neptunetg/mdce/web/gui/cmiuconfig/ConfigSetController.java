/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.cmiuconfig;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigChange;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigMgmt;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;
import com.neptunetg.mdce.common.internal.miu.service.CmiuConfigMgmtService;
import com.neptunetg.mdce.common.internal.miu.service.CmiuConfigSetService;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import com.neptunetg.mdce.web.gui.config.CmiuConfigSetForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletContext;
import javax.validation.Valid;

import java.util.List;

import static com.neptunetg.mdce.web.gui.login.LoginUtils.getLoggedInUserName;

/**
 * Web for managing Config Set CRUD.
 */
@Controller
public class ConfigSetController
{
    private static final Logger logger = LoggerFactory.getLogger(ConfigSetController.class);

    @Autowired
    private BackEndManager backEndManager;

    /**
     * Display a list of CMIU config set
     */
    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR)
    @RequestMapping("/pages/config-set")
    public String listCmiuConfigSet(Model model) throws InternalApiException
    {
        final CmiuConfigSetService service = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuConfigSetService.class);
        model.addAttribute("configSetList", service.getCmiuConfigSetList());
        return "cmiuconfig/configSetList.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping("/pages/config-set/add")
    public String prepareAddConfigSet(Model model)
    {
        CmiuConfigSetForm addConfigSetForm = new CmiuConfigSetForm();
        addConfigSetForm.setAddNewConfig(true);

        model.addAttribute("newCmiuConfigSetForm", addConfigSetForm);

        return "cmiuconfig/cmiuConfigSetDetails.jsp";
    }

    /**
     * Add a new Config Set to the config set list
     * @param inputForm backing form containing the new config set definition
     * @param bindResult for validation
     * @throws InternalApiException
     */
    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "/pages/config-set/add", method = RequestMethod.POST)
    public String addConfigSet(Model model,
                               @ModelAttribute("newCmiuConfigSetForm") @Valid CmiuConfigSetForm inputForm,
                               BindingResult bindResult) throws InternalApiException, JsonProcessingException
    {

        final CmiuConfigSetService service = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuConfigSetService.class);

        final List<CmiuConfigSet> configSetList = service.getCmiuConfigSetList();

        if (bindResult.hasErrors())
        {
            return "cmiuconfig/cmiuConfigSetDetails.jsp";
        }

        //create a new config set
        CmiuConfigSet newConfigSet = new CmiuConfigSet();
        newConfigSet.setName(inputForm.getName());

        newConfigSet.setCmiuModeName(inputForm.getCmiuModeName().trim());

        //reporting
        newConfigSet.setReportingStartMins(inputForm.getReportingPlanStartMins());
        newConfigSet.setReportingIntervalMins(inputForm.getReportingPlanIntervalHours() * 60);
        newConfigSet.setReportingNumberOfRetries(inputForm.getReportingPlanNumberOfRetries());
        newConfigSet.setReportingQuietStartMins(inputForm.getReportingPlanQuietStartMins());
        newConfigSet.setReportingQuietEndMins(inputForm.getReportingPlanQuietEndMins());
        newConfigSet.setReportingTransmitWindowMins(inputForm.getReportingPlanTransmitWindowMins());

        //data collection
        newConfigSet.setRecordingStartTimeMins(inputForm.getRecordingPlanStartTimeMins());
        newConfigSet.setRecordingIntervalMins(inputForm.getRecordingPlanIntervalMins());


        // checking for duplicate config sets
        for(CmiuConfigSet configSet: configSetList)
        {
            if (configSet.isEqual(newConfigSet))
            {
                model.addAttribute("configSetId",  configSet.getId());
                return "cmiuconfig/cmiuConfigSetDetails.jsp";
            }
        }

        service.addCmiuConfigSet(getLoggedInUserName(), newConfigSet);

        return "redirect:/pages/config-set";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping("/pages/config-set/edit")
    public String prepareEditConfigSet(Model model, @RequestParam("id") long configSetId) throws InternalApiException
    {
        CmiuConfigChange cmiuConfigChange = new CmiuConfigChange();
        final CmiuConfigSetService service = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuConfigSetService.class);
        CmiuConfigSet configSet = service.getCmiuConfigSet(configSetId);

        CmiuConfigSetForm form = new CmiuConfigSetForm();
        form.setId(configSet.getId());
        form.setName(configSet.getName());

        form.setCmiuModeName(configSet.getCmiuModeName());
        form.setOfficialMode(configSet.getOfficialMode());

        form.setReportingPlanIntervalHours((configSet.getReportingIntervalMins() + 59)/60);
        form.setReportingPlanQuietEndMins(configSet.getReportingQuietEndMins());
        form.setReportingPlanQuietStartMins(configSet.getReportingQuietStartMins());
        form.setReportingPlanStartMins(configSet.getReportingStartMins());
        form.setReportingPlanTransmitWindowMins(configSet.getReportingTransmitWindowMins());
        form.setReportingPlanNumberOfRetries(configSet.getReportingNumberOfRetries());

        form.setRecordingPlanIntervalMins(configSet.getRecordingIntervalMins());
        form.setRecordingPlanStartTimeMins(configSet.getRecordingStartTimeMins());

        model.addAttribute("newCmiuConfigSetForm", form);

        return "cmiuconfig/cmiuConfigSetDetails.jsp";
    }

    /**
     * Add a new Config Set
     * @param inputForm backing form containing the new config set definition
     * @param bindResult for validation
     * @throws InternalApiException
     */
    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "/pages/config-set/edit", method = RequestMethod.POST)
    public String editConfigSet(@ModelAttribute("newCmiuConfigSetForm") @Valid CmiuConfigSetForm inputForm, BindingResult bindResult) throws InternalApiException, JsonProcessingException
    {
        if (bindResult.hasErrors())
        {
            return "cmiuconfig/cmiuConfigSetDetails.jsp";
        }

        final CmiuConfigSetService service = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuConfigSetService.class);

        CmiuConfigSet configSet = service.getCmiuConfigSet(inputForm.getId());
        configSet.setName(inputForm.getName());

        configSet.setCmiuModeName(inputForm.getCmiuModeName().trim());

        configSet.setReportingStartMins(inputForm.getReportingPlanStartMins());
        configSet.setReportingIntervalMins(inputForm.getReportingPlanIntervalHours() * 60);
        configSet.setReportingNumberOfRetries(inputForm.getReportingPlanNumberOfRetries()); //fixme
        configSet.setReportingQuietStartMins(inputForm.getReportingPlanQuietStartMins());
        configSet.setReportingQuietEndMins(inputForm.getReportingPlanQuietEndMins());
        configSet.setReportingTransmitWindowMins(inputForm.getReportingPlanTransmitWindowMins());

        configSet.setRecordingStartTimeMins(inputForm.getRecordingPlanStartTimeMins());
        configSet.setRecordingIntervalMins(inputForm.getRecordingPlanIntervalMins());
        configSet.setOfficialMode(inputForm.getOfficialMode());

        service.editCmiuConfigSet(getLoggedInUserName(), configSet);    //todo, change to edit

        return "redirect:/pages/config-set";
    }


    /**
     * Get current Cmiu config mgmt.
     * @param cmiuId id of the CMIU to retrieve the config
     * @return json
     */
    @PreAuthorize(LoginConstants.IS_MSOC_OPERATOR)
    @RequestMapping(value="/pages/config/{cmiu}")
    @ResponseBody
    public CmiuConfigMgmt getCmiuConfig(@PathVariable(value = "cmiu") long cmiuId) throws InternalApiException
    {
        final CmiuConfigMgmtService service = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuConfigMgmtService.class);
        return service.getCmiuConfigMgmt(cmiuId);
    }

}
