/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.web.backend;

import com.neptunetg.mdce.common.internal.client.InternalApiClientConfig;
import com.neptunetg.mdce.common.internal.servicestatus.model.ServiceStatus;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Holds the status of the back end servers for display
 */
public class BackEndStatusViewModel
{
    private final Instant fetchTime;
    private final List<IndividualServerStatus> serverStatuses = new CopyOnWriteArrayList<>();

    public BackEndStatusViewModel(Instant fetchTime)
    {
        this.fetchTime = fetchTime;
    }

    public Instant getFetchTime()
    {
        return fetchTime;
    }

    public List<IndividualServerStatus> getServerStatuses()
    {
        return new ArrayList<>(serverStatuses);
    }

    public void add(InternalApiClientConfig serverDetails, ServiceStatus status)
    {
        serverStatuses.add(new IndividualServerStatus(serverDetails, status));
    }

    public static class IndividualServerStatus
    {
        private final InternalApiClientConfig serverDetails;

        private final ServiceStatus status;

        public IndividualServerStatus(InternalApiClientConfig serverDetails, ServiceStatus status)
        {
            this.serverDetails = serverDetails;
            this.status = status;
        }

        public InternalApiClientConfig getServerDetails()
        {
            return serverDetails;
        }

        public ServiceStatus getStatus()
        {
            return status;
        }
    }
}
