/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.alert;

import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A Class for holding details of an alert
 */
public class AlertsForm
{
    private List<AlertDetails> alertDetailsList;
    private String ticketId;
    private String filterString;
    private String selectedAlertIds;

    public List<AlertDetails> getAlertDetailsList()
    {
        return alertDetailsList;
    }

    public void setAlertDetailsList(List<AlertDetails> alertDetailsList)
    {
        this.alertDetailsList = alertDetailsList;
    }

    public String getTicketId()
    {
        return ticketId;
    }

    public void setTicketId(String ticketId)
    {
        this.ticketId = ticketId;
    }

    public String getFilterString()
    {
        return filterString;
    }

    public void setFilterString(String filterString)
    {
        this.filterString = filterString.trim();
    }

    public String getSelectedAlertIds()
    {
        return this.selectedAlertIds;
    }

    /**
     * Sets a comma-delimited list of alert IDs selected by the user.
     * @param selectedAlertIds A comma-delimited list of alert IDs.
     */
    public void setSelectedAlertIds(String selectedAlertIds)
    {
        this.selectedAlertIds = selectedAlertIds;
    }

    /**
     * Gets a list of alert IDs based on the current value of selectedAlertIds.
     * @return A list of all selected alert IDs.
     */
    public List<Long> getSelectedAlertIdList()
    {
        if (this.selectedAlertIds == null)
        {
            return new ArrayList<>();
        }
        else
        {
            return Arrays.stream(this.selectedAlertIds.split(","))
                    .map(v -> tryParseLong(v.trim()))
                    .filter(v -> v != null)
                    .collect(Collectors.toList());
        }
    }

    private Long tryParseLong(Object obj)
    {
        Long retVal;

        try
        {
            retVal = Long.parseLong((String) obj);
        }
        catch (NumberFormatException nfe)
        {
            retVal = null;
        }

        return retVal;
    }
}
