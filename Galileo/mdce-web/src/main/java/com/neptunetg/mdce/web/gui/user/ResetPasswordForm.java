/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.user;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Size;

/**
 * Created by WJD1 on 13/08/2015.
 * A form to hold information for creating a new user
 */
public class ResetPasswordForm
{
    @NotEmpty(message = "Password field cannot be empty.")
    @Size(min = 6, max = 50, message = "Your password must between 6 and 50 characters")
    private String password;

    @NotEmpty(message = "Password field cannot be empty.")
    private String passwordRepeated;

    private boolean passwordMatch;

    public String getPasswordRepeated()
    {
        return passwordRepeated;
    }

    public void setPasswordRepeated(String passwordRepeated)
    {
        this.passwordRepeated = passwordRepeated;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * Invoked by the validator automatically
     *
     * @return true if validation is ok
     */
    @AssertTrue(message = "Passwords do not match")
    public boolean isPasswordMatch()
    {
        return !(this.password == null || this.passwordRepeated == null) &&
                this.password.equals(this.passwordRepeated);
    }
}
