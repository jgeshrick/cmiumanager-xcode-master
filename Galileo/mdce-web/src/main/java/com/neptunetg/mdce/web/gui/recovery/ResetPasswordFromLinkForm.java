/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.recovery;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Size;

/**
 * Contains details of a password reset claim.
 */
public class ResetPasswordFromLinkForm
{
    @NotEmpty(message = "Please enter your MDCE user name.")
    private String userName;

    @NotEmpty(message = "New password field cannot be empty.")
    @Size(min = 6, max = 50, message = "The new password must be between 6 and 50 characters.")
    private String newPassword;

    @NotEmpty(message = "Password field cannot be empty.")
    private String newPasswordRepeated;

    private String email;

    private boolean passwordMatch;

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getNewPassword()
    {
        return newPassword;
    }

    public void setNewPassword(String newPassword)
    {
        this.newPassword = newPassword;
    }

    public String getNewPasswordRepeated()
    {
        return newPasswordRepeated;
    }

    public void setNewPasswordRepeated(String newPasswordRepeated)
    {
        this.newPasswordRepeated = newPasswordRepeated;
    }

    /**
     * Invoked by the validator automatically
     *
     * @return true if validation is ok
     */
    @AssertTrue(message = "The new password fields do not match")
    public boolean isPasswordMatch()
    {
        return !(this.newPassword == null || this.newPasswordRepeated == null) &&
                this.newPassword.equals(this.newPasswordRepeated);
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}
