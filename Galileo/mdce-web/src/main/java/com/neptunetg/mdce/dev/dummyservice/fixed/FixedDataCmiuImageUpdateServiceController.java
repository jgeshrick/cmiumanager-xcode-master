/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.dev.dummyservice.fixed;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import com.neptunetg.mdce.common.internal.miu.service.CmiuImageUpdateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Fake service for handling CMIU configuration command and control
 */
@RestController
@RequestMapping("/dummyservice/fixed")
public class FixedDataCmiuImageUpdateServiceController implements CmiuImageUpdateService
{
    private static final Logger logger = LoggerFactory.getLogger(FixedDataCmiuImageUpdateServiceController.class);

    /**
     * Get a list of available image of the specified image type from the RDS
     */
    @Override
    @RequestMapping(value = URL_CMIU_IMAGE, params = REQUEST_PARAM_IMAGE_TYPE)
    public List<ImageDescription> getAvailableImageList(@RequestParam(REQUEST_PARAM_IMAGE_TYPE)ImageDescription.ImageType imageType) throws InternalApiException
    {
        List<ImageDescription> imageDescriptionList = new ArrayList<>();
        ImageDescription imageDescription = new ImageDescription();
        imageDescription.setImageType(imageType);
        imageDescription.setSizeBytes(2048);
        imageDescription.setStartAddress(100);

        imageDescriptionList.add(imageDescription);

        return imageDescriptionList;
    }

    /**
     * Get list of all available images form the RDS
     * @return
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(URL_CMIU_IMAGE)
    public List<ImageDescription> getAvailableImageList() throws InternalApiException
    {
        List<ImageDescription> imageDescriptionList = new ArrayList<>();

        for (ImageDescription.ImageType imageType : ImageDescription.ImageType.values())
        {
            ImageDescription imageDescription = new ImageDescription();
            imageDescription.setFileName(imageType + "-" + imageDescription.getVersion());
            imageDescription.setImageType(imageType);
            imageDescription.setSizeBytes(2048);
            imageDescription.setStartAddress(100);
            imageDescription.setRevision(1, 2, 3, 4);
            imageDescriptionList.add(imageDescription);

            ImageDescription imageDescription2 = new ImageDescription();
            imageDescription2.setFileName(imageType + "-" + imageDescription.getVersion());
            imageDescription2.setImageType(imageType);
            imageDescription2.setSizeBytes(1024);
            imageDescription2.setStartAddress(0);
            imageDescription2.setRevision(5, 6, 7, 8);
            imageDescriptionList.add(imageDescription2);

        }

        return imageDescriptionList;
    }

    /**
     * Get a list of available build artifact (from S3)for purpose of selection/importing into the RDS
     *
     * @param imageType the type of image to retrieve, if null, all imageTypes are returned
     * @return a list of available artifacts grouped by imageType
     */
    @Override
    public Map<ImageDescription.ImageType, List<String>> getBuildArtifactList(ImageDescription.ImageType imageType)
    {
        //return a fake list
        Map<ImageDescription.ImageType, List<String>> artifactMap = new HashMap<>();
        if (imageType != null)
        {
            artifactMap.put(imageType, generateFakeArtifactList(imageType));
        }
        else
        {
            artifactMap.put(ImageDescription.ImageType.FirmwareImage, generateFakeArtifactList(ImageDescription.ImageType.FirmwareImage));
            artifactMap.put(ImageDescription.ImageType.BootloaderImage, generateFakeArtifactList(ImageDescription.ImageType.BootloaderImage));
            artifactMap.put(ImageDescription.ImageType.BleConfigImage, generateFakeArtifactList(ImageDescription.ImageType.BleConfigImage));
            artifactMap.put(ImageDescription.ImageType.ArbConfigImage, generateFakeArtifactList(ImageDescription.ImageType.ArbConfigImage));
            artifactMap.put(ImageDescription.ImageType.ConfigImage, generateFakeArtifactList(ImageDescription.ImageType.ConfigImage));
            artifactMap.put(ImageDescription.ImageType.EncryptionImage, generateFakeArtifactList(ImageDescription.ImageType.EncryptionImage));
            artifactMap.put(ImageDescription.ImageType.TelitModuleImage, generateFakeArtifactList(ImageDescription.ImageType.TelitModuleImage));
        }

        return artifactMap;
    }

    private static List<String> generateFakeArtifactList(ImageDescription.ImageType imageType)
    {
        List<String> list = new ArrayList<>();
        for(int i=0;i<5; i++)
        {
            list.add(imageType + "-" + i  + "-" + LocalDate.now() + ".bin");
        }

        return list;
    }

    /**
     * Register a build architact to be used in future image
     *
     * @param imageType
     * @param filePath
     */
    @Override
    public void registerBuildArtifact(@RequestParam(REQUEST_PARAM_IMAGE_TYPE) ImageDescription.ImageType imageType, @RequestParam(REQUEST_PARAM_NAME) String filePath)
    {
        logger.debug("register build artifact: {}, {}", imageType, filePath);

    }
}
