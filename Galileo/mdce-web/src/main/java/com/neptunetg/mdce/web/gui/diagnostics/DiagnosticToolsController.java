/*
 *  Neptune Technology Group
 *  Copyright 2017 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.gui.diagnostics;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.diagnostics.service.CnsDumpService;
import com.neptunetg.mdce.common.internal.diagnostics.service.PacketDumpService;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.print.DocFlavor;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;


/**
 Extract packets from Dynamo and format as CSV.
 */
@Controller
@PreAuthorize(LoginConstants.IS_MSOC_VIEWER)
@RequestMapping(value = "/pages/diagnostics/")
public class DiagnosticToolsController
{
    private static final Logger log = LoggerFactory.getLogger(DiagnosticToolsController.class);

    private static final ZoneId CENTRAL_TIME = ZoneId.of("US/Central");

    private final BackEndManager backEndManager;

    @Autowired
    public DiagnosticToolsController(BackEndManager backEndManager)
    {
        this.backEndManager = backEndManager;
    }

    @RequestMapping(value= "packetdump", method= RequestMethod.GET)
    public void getPacketDump(
            @RequestParam(value = PacketDumpService.L900_ID, required = false, defaultValue = "") String l900IdParam,
            @RequestParam(value = PacketDumpService.CMIU_ID, required = false, defaultValue = "") String cmiuIdParam,
            @RequestParam(value = PacketDumpService.ICCID, required = false, defaultValue = "") String iccid,
            @RequestParam(value = PacketDumpService.IMEI, required = false, defaultValue = "") String imei,
            @RequestParam(value = PacketDumpService.CMIU_SITE_ID, required = false, defaultValue = "") Integer siteId,
            @RequestParam(value = PacketDumpService.DAYS, required = false, defaultValue = "10") Integer daysOfRecord,
            HttpServletResponse response) throws InternalApiException, IOException
    {
        final PacketDumpService packetDumpService =
                backEndManager.createClient(backEndManager.getAllBackEndConfigs(), PacketDumpService.class);

        InputStream inputStream = packetDumpService.getPacketDump(l900IdParam, cmiuIdParam, iccid, imei, siteId, daysOfRecord);

        final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        final Instant now = Instant.now();
        final ZonedDateTime nowCdt = now.atZone(ZoneId.of("US/Central"));
        response.setHeader("content-disposition",
                String.format("attachment; filename=\"packet_dump_%s.csv\"",
                        dateFormat.format(nowCdt)));
        response.setContentType("text/csv");

        IOUtils.copy(inputStream, response.getOutputStream());
    }

    /**
     * Get CMIU connection history from two date range. Assumes time start at 0:00 hour.
     * This request is recommended for getting long histories as it split record retrievals into multiple requests of
     * records on per day basis, and streaming the results out while retrieving the next day of records.
     *
     * @param response handler to response where CSV will be stream out
     * @param fromDate date of start of record in the format yyyy-mm-dd eg 2015-05-30
     * @param toDate   date (inclusive) of end of record in the format yyyy-mm-dd eg 2015-05-30
     * @throws IOException
     */
    @RequestMapping(value = "getcnsdump/{mno}", method = RequestMethod.GET)
    public void getCellularConnectionHistoryWithDateRange(@PathVariable("mno") String mno,
                                                          Model model,
                                                          @RequestParam(value = "miu_id", required = false, defaultValue = "") Integer miuIdParam,
                                                          @RequestParam(value = "iccid", required = false, defaultValue = "") String iccidParam,
                                                          @RequestParam(value = "imei", required = false, defaultValue = "") String imeiParam,
                                                          @RequestParam(value = "site_id", required = false, defaultValue = "") Integer siteIdParam,
                                                          HttpServletResponse response,
                                                          @RequestParam(value = "from") String fromDate,
                                                          @RequestParam(value = "to") String toDate) throws InternalApiException, IOException
    {
        final CnsDumpService cnsDumpService =
                backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CnsDumpService.class);

        try
        {
            InputStream inputStream = cnsDumpService.getCellularConnectionHistoryWithDateRange(mno, miuIdParam, iccidParam, imeiParam, siteIdParam, fromDate, toDate);

            final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            response.setHeader("content-disposition",
                    String.format("attachment; filename=\"connection_history_Date_%s.csv\"",
                            ZonedDateTime.now().format(dateFormat)));
            response.setContentType("text/csv");

            IOUtils.copy(inputStream, response.getOutputStream());
        }
        catch (InternalApiException e)
        {
            response.sendRedirect("forward:/diagnosticTools");
        }
    }


    @RequestMapping(value = "downloadcnsdump/{mno}", method = RequestMethod.GET)
    public void downloadCellularUsageHistoryByDateRange(@PathVariable("mno") String mno,
                                                        Model model,
                                                        @RequestParam(value = "miu_id", required = false, defaultValue = "") Integer miuIdParam,
                                                        @RequestParam(value = "iccid", required = false, defaultValue = "") String iccidParam,
                                                        @RequestParam(value = "imei", required = false, defaultValue = "") String imeiParam,
                                                        @RequestParam(value = "site_id", required = false, defaultValue = "") Integer siteIdParam,
                                                        HttpServletResponse response,
                                                        @RequestParam(value = "from") String fromDate,
                                                        @RequestParam(value = "to") String toDate) throws InternalApiException, IOException
    {
        final CnsDumpService cnsDumpService =
                backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CnsDumpService.class);

        try
        {
            InputStream inputStream = cnsDumpService.downloadCellularUsageHistoryByDateRange(mno, miuIdParam, iccidParam, imeiParam, siteIdParam, fromDate, toDate);

            final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            response.setHeader("content-disposition",
                    String.format("attachment; filename=\"usage_history_Date_%s.csv\"",
                            ZonedDateTime.now().format(dateFormat)));
            response.setContentType("text/csv");

            IOUtils.copy(inputStream, response.getOutputStream());
        }
        catch (InternalApiException e)
        {
            response.sendRedirect("forward:/diagnosticTools");
        }
    }



    @RequestMapping(value = "diagnosticTools", method = RequestMethod.GET)
    public String getDiagnosticToolsView(Model model)
    {
        final ZonedDateTime now = ZonedDateTime.now(CENTRAL_TIME);
        model.addAttribute("toDate", now.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
        model.addAttribute("fromDate", now.minus(1L, ChronoUnit.WEEKS).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));

        return "diagnostics/diagnosticTools.jsp";
    }

}
