/*
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 */

package com.neptunetg.mdce.web.gui.command;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigChange;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest;
import com.neptunetg.mdce.common.internal.miu.service.MiuCommandControlService;
import com.neptunetg.mdce.common.internal.miu.service.CmiuConfigMgmtService;
import com.neptunetg.mdce.common.internal.miu.service.CmiuConfigSetService;
import com.neptunetg.mdce.common.internal.miu.service.CmiuModemFirmwareService;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.web.backend.BackEndManager;
import com.neptunetg.mdce.web.gui.config.MiuInput;
import com.neptunetg.mdce.web.gui.config.GenericCommandForm;
import com.neptunetg.mdce.web.gui.config.ImportMultipleCmiuConfigSetForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.neptunetg.mdce.web.gui.login.LoginUtils.getLoggedInUserName;

/**
 * GUI frontend for constructing generic commands - those that does not requires additional command parameters to be defined.
 *
 */
@Controller
@RequestMapping("/pages/config")
@SessionAttributes({"savedGenericCommandForm"})
public class GenericMiuCommandController
{
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    private static final Logger logger = LoggerFactory.getLogger(GenericMiuCommandController.class);

    @Autowired
    private BackEndManager backEndManager;

    @ModelAttribute("savedGenericCommandForm")
    public GenericCommandForm getGenericCommandForms()
    {
        return new GenericCommandForm(CommandRequest.CommandTypeCmiu.COMMAND_REBOOT_CMIU);
    }

    @ModelAttribute("configSetList")
    public List<CmiuConfigSet> getCmiuConfigSetList() throws InternalApiException
    {
        final CmiuConfigSetService service = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuConfigSetService.class);
        return service.getCmiuConfigSetList();
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "reboot-cmiu", method = RequestMethod.GET)
    public String loadRebootCmiuView(@ModelAttribute("savedGenericCommandForm") GenericCommandForm savedGenericCommandForm,
                                     Model model)
    {
        this.prepareViewModel(model, CommandRequest.CommandTypeCmiu.COMMAND_REBOOT_CMIU, savedGenericCommandForm.getMiuInputList());
        return "commands/genericCommand.jsp";
    }

    /**
     * Prepare the view model for send command entries
     * @param model
     * @param commandTypeCmiu
     * @param savedCmiuList
     */
    private void prepareViewModel(Model model, CommandRequest.CommandTypeCmiu commandTypeCmiu, List<MiuInput> savedCmiuList)
    {
        GenericCommandForm genericCommandForm = this.getGenericCommandForms();
        genericCommandForm.setCommandTypeCmiu(commandTypeCmiu);
        genericCommandForm.setMiuInputList(savedCmiuList);

        model.addAttribute("genericCommandForm", genericCommandForm);
    }

    /**
     * Prepare the view model for send command entries
     * @param model
     * @param commandTypeL900
     * @param savedL900List
     */
    private void prepareViewModel(Model model, CommandRequest.CommandTypeL900 commandTypeL900, List<MiuInput> savedL900List)
    {
        GenericCommandForm genericCommandForm = this.getGenericCommandForms();
        genericCommandForm.setCommandTypeL900(commandTypeL900);
        genericCommandForm.setMiuInputList(savedL900List);

        model.addAttribute("genericCommandForm", genericCommandForm);
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "emulate-mag-swipe", method = RequestMethod.GET)
    public String loadEmulateMagSwipeView(@ModelAttribute("savedGenericCommandForm") GenericCommandForm savedGenericCommandForm,
                                          Model model)
    {
        this.prepareViewModel(model, CommandRequest.CommandTypeCmiu.COMMAND_MAG_SWIPE_EMULATION, savedGenericCommandForm.getMiuInputList());
        return "commands/genericCommand.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "erase-datalog", method = RequestMethod.GET)
    public String loadEraseDatalogView(@ModelAttribute("savedGenericCommandForm") GenericCommandForm savedGenericCommandForm,
                                          Model model)
    {
        this.prepareViewModel(model, CommandRequest.CommandTypeCmiu.COMMAND_ERASE_DATALOG, savedGenericCommandForm.getMiuInputList());
        return "commands/genericCommand.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "set-recording-reporting-interval", method = RequestMethod.GET)
    public String loadSetRecordingReportingIntervalView(@ModelAttribute("savedGenericCommandForm") GenericCommandForm savedGenericCommandForm,
                                              Model model) throws InternalApiException
    {
        final CmiuConfigSetService service = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuConfigSetService.class);
        model.addAttribute("configSetList", service.getCmiuConfigSetList());

        this.prepareViewModel(model, CommandRequest.CommandTypeCmiu.COMMAND_SET_RECORDING_REPORTING_INTERVALS, savedGenericCommandForm.getMiuInputList());

        return "commands/recordingReportingInterval.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "update-modem-firmware", method = RequestMethod.GET)
    public String loadUpdateModemFirmwareIntervalView(@ModelAttribute("savedGenericCommandForm") GenericCommandForm savedGenericCommandForm,
                                              Model model) throws InternalApiException
    {
        final CmiuModemFirmwareService service = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuModemFirmwareService.class);
        model.addAttribute("modemFirmwareList", service.getAvailableCmiuModemFirmwareImages());

        this.prepareViewModel(model, CommandRequest.CommandTypeCmiu.COMMAND_MODEM_FOTA, savedGenericCommandForm.getMiuInputList());

        return "commands/modemFirmwareUpdateCommand.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "request-publish-packet", method = RequestMethod.GET)
    public String loadRequestPublishPacketView(@ModelAttribute("savedGenericCommandForm") GenericCommandForm savedGenericCommandForm,
                                               Model model) throws InternalApiException
    {
        model.addAttribute("requestPacketList", EnumSet.allOf(CommandRequest.PacketType.class));

        this.prepareViewModel(model, CommandRequest.CommandTypeCmiu.COMMAND_PUBLISH_REQUESTED_PACKET, savedGenericCommandForm.getMiuInputList());

        return "commands/requestPublishPacketCommand.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "set-time-offset-l900", method = RequestMethod.GET)
    public String loadRequestSetTimeOffsetL900View(@ModelAttribute("savedGenericCommandForm") GenericCommandForm savedGenericCommandForm,
                                               Model model) throws InternalApiException
    {
        this.prepareViewModel(model, CommandRequest.CommandTypeL900.COMMAND_TIME_OFFSET, savedGenericCommandForm.getMiuInputList());

        return "commands/setTimeOffsetCommandL900.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_MASTER_OF_CELLULAR_WHITELISTED)
    @RequestMapping(value = "reset-l900", method = RequestMethod.GET)
    public String loadRequestResetL900View(@ModelAttribute("savedGenericCommandForm") GenericCommandForm savedGenericCommandForm,
                                                   Model model) throws InternalApiException
    {
        this.prepareViewModel(model, CommandRequest.CommandTypeL900.COMMAND_RESET, savedGenericCommandForm.getMiuInputList());

        return "commands/genericL900Command.jsp";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_MASTER_OF_CELLULAR_WHITELISTED)
    @RequestMapping(value = "deactivate-l900", method = RequestMethod.GET)
    public String loadRequestDeactivateL900View(@ModelAttribute("savedGenericCommandForm") GenericCommandForm savedGenericCommandForm,
                                           Model model) throws InternalApiException
    {
        this.prepareViewModel(model, CommandRequest.CommandTypeL900.COMMAND_DEACTIVATE, savedGenericCommandForm.getMiuInputList());

        return "commands/genericL900Command.jsp";
    }

    /**
     * Submit a image update request to the backend
     *
     * @param genericCommandForm backing form contain list of cmiu and image type, version
     * @param bindResult      used for validation
     * @param sessionStatus   session status to be cleared when the request is successfully processed
     * @param ra              redirectAttributes added to prevent parameters to be passed to redirect (default) http://stackoverflow.com/questions/2543797/spring-redirect-after-post-even-with-validation-errors
     * @return redirect to pending change page
     */
    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = {"reboot-cmiu", "emulate-mag-swipe", "enter-sleep-state", "enter-operation-mode",
            "erase-datalog", "publish-basic-config", "publish-detail-config"},
            method = RequestMethod.POST)
    public String sendGenericCommand(
            @ModelAttribute("genericCommandForm") GenericCommandForm genericCommandForm,
            HttpServletRequest request,
            BindingResult bindResult,
            SessionStatus sessionStatus,
            RedirectAttributes ra) throws InternalApiException
    {
        CmiuInputListFormValidator formValidator = new CmiuInputListFormValidator();
        formValidator.validate(genericCommandForm, bindResult);

        genericCommandForm.setMiuInputList(removeEmptyMiuRows(genericCommandForm.getMiuInputList()));

        if (bindResult.hasErrors())
        {
            return "commands/genericCommand.jsp";
        }

        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(getLoggedInUserName());
        commandRequest.setCommandCmiu(genericCommandForm.getCommandTypeCmiu());
        commandRequest.setExecutionTimeEpochSecond(Instant.now().getEpochSecond());  //fixme, change to user configurable time

        addMiuListAndSendCommand(commandRequest, genericCommandForm.getMiuInputList());

        sessionStatus.setComplete();
        return "redirect:/pages/config";
    }

    /**
     * Submit a L900 command request to the backend
     *
     * @param genericCommandForm backing form contain list of L900 and command type
     * @param bindResult      used for validation
     * @param sessionStatus   session status to be cleared when the request is successfully processed
     * @param ra              redirectAttributes added to prevent parameters to be passed to redirect (default) http://stackoverflow.com/questions/2543797/spring-redirect-after-post-even-with-validation-errors
     * @return redirect to pending change page
     */
    @PreAuthorize(LoginConstants.IS_MSOC_MASTER_OF_CELLULAR_WHITELISTED)
    @RequestMapping(value = {"reset-l900", "deactivate-l900"}, method = RequestMethod.POST)
    public String sendGenericL900Command(
            @ModelAttribute("genericCommandForm") GenericCommandForm genericCommandForm,
            HttpServletRequest request,
            BindingResult bindResult,
            SessionStatus sessionStatus,
            RedirectAttributes ra) throws InternalApiException
    {
        L900InputListFormValidator formValidator = new L900InputListFormValidator();
        formValidator.validate(genericCommandForm, bindResult);

        genericCommandForm.setMiuInputList(removeEmptyMiuRows(genericCommandForm.getMiuInputList()));

        if (bindResult.hasErrors())
        {
            return "commands/genericL900Command.jsp";
        }

        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(getLoggedInUserName());
        commandRequest.setCommandL900(genericCommandForm.getCommandTypeL900());
        commandRequest.setExecutionTimeEpochSecond(Instant.now().getEpochSecond());

        addMiuListAndSendCommand(commandRequest, genericCommandForm.getMiuInputList());

        sessionStatus.setComplete();
        return "redirect:/pages/config";
    }

    /**
     * Request to add a batch of CMIU defined in a text file
     * @param genericCommandForm
     * @param bindResult
     * @param sessionStatus
     * @param ra
     * @return
     */
    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = {"reboot-cmiu", "emulate-mag-swipe", "enter-sleep-state", "enter-operation-mode",
            "erase-datalog", "publish-basic-config", "publish-detail-config",
            "set-current-time", "set-recording-reporting-interval",
            "publish-interval-data", "update-modem-firmware"}, method = RequestMethod.POST, params = "appendCmiuList")
    public String batchAddCmiuRequest(@ModelAttribute("genericCommandForm") GenericCommandForm genericCommandForm,
                                      BindingResult bindResult,
                                      SessionStatus sessionStatus,
                                      RedirectAttributes ra,
                                      Model model)
    {
        //save CMIU list
        GenericCommandForm saveCmiuUpdateForm = this.getGenericCommandForms();
        saveCmiuUpdateForm.setMiuInputList(genericCommandForm.getMiuInputList());

        model.addAttribute("savedGenericCommandForm", saveCmiuUpdateForm);

        return "redirect:/pages/config/batch-add-cmiu";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "update-modem-firmware", method = RequestMethod.POST)
    public String updateModemFirmware(
            @ModelAttribute("genericCommandForm") GenericCommandForm genericCommandForm,
            BindingResult bindResult,
            SessionStatus sessionStatus,
            RedirectAttributes ra) throws InternalApiException, JsonProcessingException
    {
        CmiuInputListFormValidator formValidator = new CmiuInputListFormValidator();
        formValidator.validate(genericCommandForm, bindResult);

        genericCommandForm.setMiuInputList(removeEmptyMiuRows(genericCommandForm.getMiuInputList()));

        if (bindResult.hasErrors())
        {
            return "commands/modemFirmwareUpdateCommand.jsp";
        }

        final String requester = getLoggedInUserName();

        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(requester);
        commandRequest.setCommandCmiu(CommandRequest.CommandTypeCmiu.COMMAND_MODEM_FOTA);
        commandRequest.setExecutionTimeEpochSecond(Instant.now().getEpochSecond());
        commandRequest.setFotaFilename(genericCommandForm.getImageName());


        final String versionNumber = genericCommandForm.getVersionNumber();
        if(StringUtils.hasText(versionNumber))
        {
            commandRequest.setFotaFileImageDescription(versionNumber.trim());
            addMiuListAndSendCommand(commandRequest, genericCommandForm.getMiuInputList());
            sessionStatus.setComplete();

            return "redirect:/pages/config";
        }
        else
        {
            logger.debug("Invalid version number used in FOTA command");
            return "redirect:/pages/config/update-modem-firmware";
        }
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "set-recording-reporting-interval", method = RequestMethod.POST)
    public String setRecordingReportingInterval(
            @ModelAttribute("genericCommandForm") GenericCommandForm genericCommandForm,
            BindingResult bindResult,
            SessionStatus sessionStatus,
            Model model) throws InternalApiException, JsonProcessingException
    {
        CmiuInputListFormValidator formValidator = new CmiuInputListFormValidator();
        formValidator.validate(genericCommandForm, bindResult);

        genericCommandForm.setMiuInputList(removeEmptyMiuRows(genericCommandForm.getMiuInputList()));

        if (bindResult.hasErrors())
        {
            return "commands/recordingReportingInterval.jsp";
        }

        final String requester = getLoggedInUserName();
        final CmiuConfigSetService configSetService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuConfigSetService.class);
        CmiuConfigSet configSetList = configSetService.getCmiuConfigSet(genericCommandForm.getConfigSetId());

        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(requester);
        commandRequest.setCommandCmiu(genericCommandForm.getCommandTypeCmiu());
        commandRequest.setExecutionTimeEpochSecond(Instant.now().getEpochSecond());  //fixme, change to user configurable time

        commandRequest.setRecordingStartTime(configSetList.getRecordingStartTimeMins());
        commandRequest.setRecordingInterval(configSetList.getRecordingIntervalMins());
        commandRequest.setReportingStartMins(configSetList.getReportingStartMins());
        commandRequest.setReportingIntervalHours(configSetList.getReportingIntervalMins()/60);
        commandRequest.setReportingRetries(configSetList.getReportingNumberOfRetries());
        commandRequest.setReportingTransmitWindows(configSetList.getReportingTransmitWindowMins());
        commandRequest.setReportingQuietStart(configSetList.getReportingQuietStartMins());
        commandRequest.setReportingQuietEnd(configSetList.getReportingQuietEndMins());

        addMiuListAndSendCommand(commandRequest, genericCommandForm.getMiuInputList());

        final CmiuConfigMgmtService configMgmtService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuConfigMgmtService.class);

        //transform to CmiuConfigChange list for submission to CmiuConfigMgmtService
        List<CmiuConfigChange> configChangeList = genericCommandForm.getMiuInputList()
                .stream()
                .map(cmiuDescription -> new CmiuConfigChange(Integer.valueOf(cmiuDescription.getMiuId()), genericCommandForm.getConfigSetId()))
                .collect(Collectors.toList());

        //submit config change to configMgmtService to be recorded in RDS
        configMgmtService.addCmiuConfig(requester, configChangeList);

        sessionStatus.setComplete();
        return "redirect:/pages/config";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "request-publish-packet", method = RequestMethod.POST)
    public String requestPacketToPublish(
            @ModelAttribute("genericCommandForm") GenericCommandForm genericCommandForm,
            BindingResult bindResult,
            SessionStatus sessionStatus,
            RedirectAttributes ra) throws InternalApiException, JsonProcessingException
    {
        CmiuInputListFormValidator formValidator = new CmiuInputListFormValidator();
        formValidator.validate(genericCommandForm, bindResult);

        genericCommandForm.setMiuInputList(removeEmptyMiuRows(genericCommandForm.getMiuInputList()));

        if (bindResult.hasErrors())
        {
            return "commands/requestPublishPacketCommand.jsp";
        }

        final String requester = getLoggedInUserName();

        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(requester);
        commandRequest.setCommandCmiu(genericCommandForm.getCommandTypeCmiu());
        commandRequest.setExecutionTimeEpochSecond(Instant.now().getEpochSecond());  //fixme, change to user configurable time

        commandRequest.setPacketType(CommandRequest.PacketType.fromPacketNumber(genericCommandForm.getRequestPacketSetId()));

        addMiuListAndSendCommand(commandRequest, genericCommandForm.getMiuInputList());

        sessionStatus.setComplete();
        return "redirect:/pages/config";
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "set-time-offset-l900", method = RequestMethod.POST)
    public String setTimeOffsetL900(
            @ModelAttribute("genericCommandForm") GenericCommandForm genericCommandForm,
            BindingResult bindResult,
            SessionStatus sessionStatus,
            RedirectAttributes ra) throws InternalApiException, JsonProcessingException
    {
        L900InputListFormValidator formValidator = new L900InputListFormValidator();
        formValidator.validate(genericCommandForm, bindResult);

        genericCommandForm.setMiuInputList(removeEmptyMiuRows(genericCommandForm.getMiuInputList()));

        if (bindResult.hasErrors())
        {
            return "commands/setTimeOffsetCommandL900.jsp";
        }

        final String requester = getLoggedInUserName();

        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(requester);
        commandRequest.setCommandL900(CommandRequest.CommandTypeL900.COMMAND_TIME_OFFSET);
        commandRequest.setExecutionTimeEpochSecond(Instant.now().getEpochSecond());

        final String timeOffsetStr = genericCommandForm.getTimeOffset();

        try
        {
            commandRequest.setTimeOffset(Long.parseLong(timeOffsetStr));
            addMiuListAndSendCommand(commandRequest, genericCommandForm.getMiuInputList());
            sessionStatus.setComplete();

            return "redirect:/pages/config";
        }
        catch (NumberFormatException e)
        {
            logger.debug("Time offset entered is not a valid number:", e);
            return "redirect:/pages/config/set-time-offset-l900";
        }
    }

    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value = "set-recording-reporting-interval", method = RequestMethod.POST, params = "addConfigSet")
    public String addConfigSetRequest(
            @ModelAttribute("genericCommandForm") GenericCommandForm genericCommandForm,
            BindingResult bindResult,
            SessionStatus sessionStatus,
            RedirectAttributes ra) throws InternalApiException
    {
        return "redirect:/pages/config-set/add";
    }

    private void addMiuListAndSendCommand(CommandRequest commandRequest, List<MiuInput> miuList) throws InternalApiException
    {
        //add list of MIUs
        commandRequest.setMiuList(miuList.stream()
                .filter(miuInput -> StringUtils.hasText(miuInput.getMiuId()))
                .map(miuInput -> Long.valueOf(miuInput.getMiuId()))
                .collect(Collectors.toList()));

        final MiuCommandControlService commandService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MiuCommandControlService.class);
        commandService.sendCommand(commandRequest);
    }

    /**
     * View model for generating a list of CMIU id based on a CSV? file and mass config change
     * @throws InternalApiException
     */
    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value="batch-add-cmiu", method = RequestMethod.GET)
    public String prepareImportCmiuConfigFile(Model model, HttpServletRequest request) throws InternalApiException
    {
        ImportMultipleCmiuConfigSetForm form = new ImportMultipleCmiuConfigSetForm();
        form.setRedirectUrl(request.getHeader("Referer"));

        model.addAttribute("importCmiuFileForm", form);

        return "cmiuconfig/batchAddCmiu.jsp";
    }

    /**
     * Accept a file containing a list of CMIU, parse and add to existing CMIU config list.
     * The function will search and accept any 8 digit word.
     * @param uploadForm Form backing object containing the CMIU file, selected config set and change note
     * @return redirect to Cmiu Config page
     * @throws IOException
     */
    @PreAuthorize(LoginConstants.IS_MSOC_ADMIN_WHITELISTED)
    @RequestMapping(value="batch-add-cmiu", method = RequestMethod.POST)
    public String importCmiuListFile(@ModelAttribute("importCmiuFileForm") ImportMultipleCmiuConfigSetForm uploadForm,
                                       @ModelAttribute("savedGenericCommandForm") GenericCommandForm savedGenericCommandForm) throws IOException, InternalApiException
    {
        MultipartFile file = uploadForm.getCmiuListDefFile();

        List<MiuInput> importedCmiuList = importCmiuIdFromTextFile(file);

        savedGenericCommandForm.getMiuInputList().addAll(importedCmiuList);

        //remove first entry if it is a null entry. One entry is required for dynamic list to work
        if (savedGenericCommandForm.getMiuInputList().size() > 1 &&
                savedGenericCommandForm.getMiuInputList().get(0).getMiuId() == null)
        {
            savedGenericCommandForm.getMiuInputList().remove(0);
        }


        return "redirect:" + uploadForm.getRedirectUrl();
    }

    private List<MiuInput> importCmiuIdFromTextFile(MultipartFile file) throws IOException, InternalApiException
    {
        final MiuCommandControlService commandService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), MiuCommandControlService.class);

        List<MiuInput> cmiuInputList = new ArrayList<>();

        if (!file.isEmpty())
        {
            InputStream inputStream = file.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line;

            final Pattern pattern = Pattern.compile("\\b([0-9]{9})\\b");    //matches a string of 8 digits within a word boundary

            //read each line of CMIU id and add to current cmiuConfigCommandList
            while ((line = bufferedReader.readLine()) != null)
            {
                Matcher matcher = pattern.matcher(line);

                while(matcher.find())
                {
                    String foundCmiuId = matcher.group(0);
                    MiuInput cmiuInput = new MiuInput();
                    cmiuInput.setMiuId(foundCmiuId);

                    String cmiuMetaData = commandService.getCmiuDescription(Long.valueOf(cmiuInput.getMiuId())).getMetaData();
                    cmiuInput.setMetaData(cmiuMetaData);

                    //add to list
                    cmiuInputList.add(cmiuInput);
                }
            }

        }

        return cmiuInputList;
    }

    /**
     * Remove rows from MIU Input Lists which are empty
     * @param miuRows rows of MIUs
     * @return A list of MIUs
     */
    private List<MiuInput> removeEmptyMiuRows(List<MiuInput> miuRows)
    {
        List<MiuInput> filteredList = new ArrayList<>();

        for(MiuInput miu : miuRows)
        {
            if(miu.getMiuId().trim() != "")
            {
                filteredList.add(miu);
            }
        }

        return filteredList;
    }

    /**
     * Validate an input date/time string and update the binding result to reject the value if there is an error
     * @param bindResult
     * @param inputTime
     */
    private boolean validateTime(BindingResult bindResult, String inputField, String inputTime)
    {
        try
        {
            LocalDateTime localStartTime = LocalDateTime.parse(inputTime, dtf);
            return true;
        }
        catch (DateTimeParseException ex)
        {
            bindResult.rejectValue(inputField, "command.invalid.format.datetime", "Invalid datetime format");
        }

        return false;
    }

}
