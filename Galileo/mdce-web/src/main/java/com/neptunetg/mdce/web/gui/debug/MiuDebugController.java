/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *
 *  All rights reserved
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.gui.debug;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.CmiuDetails;
import com.neptunetg.mdce.common.internal.miu.service.CmiuDetailsService;
import com.neptunetg.mdce.web.backend.BackEndManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * To display Miu Details
 */
@Controller
public class MiuDebugController
{
    @Autowired
    private BackEndManager backEndManager;

    @RequestMapping("/debug/miu-details")
    public String getMiuDetails(@RequestParam(value = "account", defaultValue = "ALL") String accountId, Model model) throws InternalApiException
    {
        final CmiuDetailsService service = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), CmiuDetailsService.class);
        List<CmiuDetails> cmiuDetailsList = service.getCmiuByAccount(accountId);

        model.addAttribute("miuDetailsList", cmiuDetailsList);
        return "debug/miuDetailsList.jsp";
    }
}
