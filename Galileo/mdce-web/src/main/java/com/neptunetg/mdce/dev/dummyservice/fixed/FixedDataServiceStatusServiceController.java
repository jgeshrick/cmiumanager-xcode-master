/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.dev.dummyservice.fixed;

import com.neptunetg.mdce.common.internal.servicestatus.model.ServiceStatus;
import com.neptunetg.mdce.common.internal.servicestatus.service.InternalApiLibraryVersionInfo;
import com.neptunetg.mdce.common.internal.servicestatus.service.ServiceStatusService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Provides MSR statistics
 */
@RestController
public class FixedDataServiceStatusServiceController implements ServiceStatusService
{
    private static final String DUMMY_URL_SERVICE_STATUS = "/dummyservice/fixed" + URL_SERVICE_STATUS;


    @RequestMapping(DUMMY_URL_SERVICE_STATUS)
    @Override
    public ServiceStatus getServiceStatus()
    {
        ServiceStatus ret = new ServiceStatus();

        ret.setLibraryVersion(InternalApiLibraryVersionInfo.LIBRARY_VERSION);

        ret.setEnvironmentName("Dummy fixed data");

        return ret;
    }



}
