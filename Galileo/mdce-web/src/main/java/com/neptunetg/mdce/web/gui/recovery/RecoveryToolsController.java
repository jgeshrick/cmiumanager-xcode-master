/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.recovery;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.email.model.EmailMessage;
import com.neptunetg.mdce.common.internal.email.service.EmailService;
import com.neptunetg.mdce.common.internal.user.model.ModifiedUserDetails;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequest;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequestClaim;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import com.neptunetg.mdce.common.internal.user.service.UserService;
import com.neptunetg.mdce.web.backend.BackEndManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Provides users with the ability to reset their passwords using a link sent via email.
 */
@Controller
public class RecoveryToolsController
{
    private static final Logger log = LoggerFactory.getLogger(RecoveryToolsController.class);

    private static final int RESET_LINK_VALID_MINUTES = 20;
    private final BackEndManager backEndManager;

    @Autowired
    public RecoveryToolsController(BackEndManager backEndManager)
    {
        this.backEndManager = backEndManager;
    }

    /**
     * GET method for the Request Password Reset form.
     */
    @RequestMapping(path = "/pages/recovery/password", method = RequestMethod.GET)
    public String getRequestPasswordReset(ModelMap model)
    {
        RequestPasswordResetLinkForm requestPasswordResetLinkForm = new RequestPasswordResetLinkForm();

        model.addAttribute("requestPasswordResetLinkForm", requestPasswordResetLinkForm);

        return "recovery/requestPasswordResetLink.jsp";
    }

    /**
     * POST method to submit a request to the Request Password Reset form.
     */
    @RequestMapping(path = "/pages/recovery/password", method = RequestMethod.POST)
    public String postRequestPasswordReset(
            @ModelAttribute("requestPasswordResetLinkForm") @Valid RequestPasswordResetLinkForm requestPasswordResetLinkForm,
            BindingResult bindingResult,
            ModelMap model,
            HttpServletRequest request) throws InternalApiException
    {
        if (bindingResult.hasErrors())
        {
            return "recovery/requestPasswordResetLink.jsp";
        }
        else
        {
            final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);

            // Find the user by the supplied email address.
            Map<String, Object> searchOptions = new HashMap<>();
            searchOptions.put("email", requestPasswordResetLinkForm.getEmailAddress());
            List<UserDetails> userDetailsList = userService.findUsers(searchOptions);

            if (userDetailsList.size() == 0)
            {
                log.info("Password reset request submitted for unregistered email address: " +
                        requestPasswordResetLinkForm.getEmailAddress());
            }
            else if (userDetailsList.size() != 1)
            {
                log.info("Password reset request submitted for an email address registered to multiple accounts. " +
                "A password reset token has not been issued for this request. Email address: " +
                        requestPasswordResetLinkForm.getEmailAddress());
            }
            else if (userDetailsList.size() == 1)
            {
                UserDetails userDetails = userDetailsList.get(0);

                // Expire existing requests
                userService.expirePasswordResetRequestsByUser(userDetails.getUserId());

                // Create new request
                PasswordResetRequest passwordResetRequest = new PasswordResetRequest();
                passwordResetRequest.setToken(PasswordResetRequest.generateToken());
                passwordResetRequest.setUserId((int)userDetails.getUserId());
                passwordResetRequest.setMatchedUserName(userDetails.getUserName());
                passwordResetRequest.setEnteredEmailAddress(requestPasswordResetLinkForm.getEmailAddress());
                passwordResetRequest.setValidUntilTimestamp(Instant.now().plus(RESET_LINK_VALID_MINUTES, ChronoUnit.MINUTES));
                passwordResetRequest.setRequestorIpAddress(request.getRemoteAddr());

                userService.addPasswordResetRequest(passwordResetRequest);

                // Notify user by email
                final EmailService emailService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), EmailService.class);

                final String content = generatePasswordResetLinkEmail(passwordResetRequest);
                EmailMessage emailMessage = new EmailMessage(
                        new String[]{passwordResetRequest.getEnteredEmailAddress()},
                        "Neptune MDCE Password Reset Request",
                        content);

                emailService.sendEmail(emailMessage);

                log.debug("Password reset request created for email address: " +
                        requestPasswordResetLinkForm.getEmailAddress());
            }

            return "recovery/passwordResetLinkRequestReceived.jsp";
        }
    }

    /**
     * GET method for claiming a password reset request.
     */
    @RequestMapping(path = "/pages/recovery/password/{token}", method = RequestMethod.GET)
    public String getResetPasswordFromLink(@PathVariable("token") String token, ModelMap model) throws InternalApiException
    {
        final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);

        PasswordResetRequest passwordResetRequest = userService.getPasswordResetRequestByToken(token);

        if (passwordResetRequest == null)
        {
            return HttpStatus.NOT_FOUND.toString();
        }
        else if (passwordResetRequest.isClaimed())
        {
            return "recovery/linkAlreadyUsed.jsp";
        }
        else if (Instant.now().compareTo(passwordResetRequest.getValidUntilTimestamp()) > 0)
        {
            return "recovery/linkExpired.jsp";
        }
        else
        {
            ResetPasswordFromLinkForm resetPasswordFromLinkForm = new ResetPasswordFromLinkForm();
            resetPasswordFromLinkForm.setEmail(passwordResetRequest.getEnteredEmailAddress());

            model.addAttribute("resetPasswordFromLinkForm", resetPasswordFromLinkForm);

            return "recovery/resetPasswordFromLink.jsp";
        }
    }

    /**
     * POST method for claiming a password reset request.
     */
    @RequestMapping(path = "/pages/recovery/password/{token}", method = RequestMethod.POST)
    public String postResetPasswordFromLink(@PathVariable("token") String token,
                                            @ModelAttribute("resetPasswordFromLinkForm") @Valid
                                                    ResetPasswordFromLinkForm resetPasswordFromLinkForm,
                                            BindingResult bindingResult,
                                            ModelMap model,
                                            HttpServletRequest request) throws InternalApiException
    {
        final UserService userService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), UserService.class);

        PasswordResetRequest passwordResetRequest = userService.getPasswordResetRequestByToken(token);
        resetPasswordFromLinkForm.setEmail(passwordResetRequest.getEnteredEmailAddress());
        UserDetails userDetails = userService.getUserDetails(passwordResetRequest.getMatchedUserName());

        // Add a model error if the entered username doesn't match the one associated with the token.
        if (StringUtils.hasText(resetPasswordFromLinkForm.getUserName()) &&
                !passwordResetRequest.getMatchedUserName().equalsIgnoreCase(resetPasswordFromLinkForm.getUserName()))
        {
            bindingResult.rejectValue("userName", "errors.userName", "This user name doesn't match the one associated with this password reset link");
        }

        if (passwordResetRequest == null)
        {
            return HttpStatus.NOT_FOUND.toString();
        }
        else if (passwordResetRequest.isClaimed())
        {
            return "recovery/linkAlreadyUsed.jsp";
        }
        else if (Instant.now().compareTo(passwordResetRequest.getValidUntilTimestamp()) > 0)
        {
            return "recovery/linkExpired.jsp";
        }
        else if (bindingResult.hasErrors())
        {
            return "recovery/resetPasswordFromLink.jsp";
        }
        else
        {
            PasswordResetRequestClaim passwordResetRequestClaim = new PasswordResetRequestClaim();
            passwordResetRequestClaim.setClaimantIpAddress(request.getRemoteAddr());
            passwordResetRequestClaim.setPasswordResetRequestId(passwordResetRequest.getPasswordResetRequestId());

            // Make sure the userId still matches with the associated user name.
            if (userDetails == null || userDetails.getUserId() != passwordResetRequest.getUserId())
            {
                log.warn("Password reset request contains mismatched identity information. PasswordResetRequestId: " +
                         passwordResetRequest.getPasswordResetRequestId());

                throw new InternalApiException("The password reset request contains invalid user information.");
            }

            // An extra check that the entered user name matches the one associated with the token.
            if (!passwordResetRequest.getMatchedUserName().equalsIgnoreCase(resetPasswordFromLinkForm.getUserName()))
            {
                throw new InternalApiException("The entered username doesn't match the one associated with the password reset request.");
            }

            // Claim the token.
            boolean claimResult = userService.claimPasswordResetRequest(token, passwordResetRequestClaim);
            if (!claimResult)
            {
                log.warn("An attempt to claim a password reset request failed internally. PasswordResetRequestId: " +
                        passwordResetRequest.getPasswordResetRequestId());

                throw new InternalApiException("The password reset request could not be claimed.");
            }

            // Change the password
            ModifiedUserDetails modifiedUserDetails = new ModifiedUserDetails();
            modifiedUserDetails.setUserId((int)userDetails.getUserId());
            modifiedUserDetails.setPassword(resetPasswordFromLinkForm.getNewPassword());
            modifiedUserDetails.setRequester(userDetails.getUserName());

            userService.resetPassword(modifiedUserDetails);

            log.debug("A password reset request was successfully used. PasswordResetRequestId: " +
                    passwordResetRequest.getPasswordResetRequestId());

            // Notify user by email
            final EmailService emailService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), EmailService.class);

            final String content = generatePasswordResetConfirmationEmail(passwordResetRequest);
            EmailMessage emailMessage = new EmailMessage(
                    new String[]{userDetails.getEmail()},
                    "Neptune MDCE Password Reset Confirmation",
                    content);

            emailService.sendEmail(emailMessage);

            log.debug("Password reset confirmation email sent to " + userDetails.getEmail() +
                      " for PasswordResetRequestId: " + passwordResetRequest.getPasswordResetRequestId());

            return "recovery/passwordChanged.jsp";
        }
    }

    private String generatePasswordResetLinkEmail(PasswordResetRequest passwordResetRequest)
    {
        final String link = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/pages/recovery/password/" + passwordResetRequest.getToken())
                .build()
                .toUriString();

        final String content =
                "This message has been generated by the Neptune MDCE portal.\n\n" +
                "A password reset link has been requested for the MDCE account registered to " +
                passwordResetRequest.getEnteredEmailAddress() + ". The request originated from IP address " +
                passwordResetRequest.getRequestorIpAddress() + ".\n\n" +
                "If you do not recognize this request, please notify Neptune Technology Group. Otherwise, use " +
                "the link below to reset your password.\n\n" +
                link + "\n\n" +
                "The link wil be active for " + RESET_LINK_VALID_MINUTES + " minutes from the time it was requested. " +
                "Any previous password reset links for this account are no longer valid.";

        return content;
    }

    private String generatePasswordResetConfirmationEmail(PasswordResetRequest passwordResetRequest)
    {
        final String link = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/pages/user/login")
                .build()
                .toUriString();

        final String content =
                "This message has been generated by the Neptune MDCE portal.\n\n" +
                "The password for the Neptune MDCE portal account registered to " +
                passwordResetRequest.getEnteredEmailAddress() + " has been reset. The request originated from IP " +
                "address " + passwordResetRequest.getRequestorIpAddress() + ".\n\n" +
                "If you did not request a password reset for this account, please notify Neptune Technology Group. " +
                "Otherwise, you can log in to MDCE now using your new password.\n\n" +
                link + "\n";

        return content;
    }

}
