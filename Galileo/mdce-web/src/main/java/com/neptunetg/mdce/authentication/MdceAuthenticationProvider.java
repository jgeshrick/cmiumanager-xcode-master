/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.authentication;

import com.neptunetg.mdce.web.gui.login.UserController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * For implementing Spring Security Framework user authentication.
 */
public class MdceAuthenticationProvider
        implements AuthenticationProvider, AuthenticationFailureHandler, AuthenticationSuccessHandler
{
    private final SessionRegistry sessionRegistry;

    private final UserAuthenticator validateUserService;

    @Autowired
    public MdceAuthenticationProvider(UserAuthenticator validateUserService, SessionRegistry sessionRegistry)
    {
        super();
        this.validateUserService = validateUserService;
        this.sessionRegistry = sessionRegistry;
    }


    @Override
    public Authentication authenticate(Authentication authentication)
    {
        String username = authentication.getName();

        String password = authentication.getCredentials().toString();

        if (username.isEmpty() || password.isEmpty())
        {
            throw new AuthenticationServiceException("User name and password need to be completed");
        }

        try
        {

            UserInfo user = validateUserService.auth(username, password);

            if (user.isAuthenticated())
            {
                if (!user.getAllowConcurrentSessions())
                {
                    expireOtherSessionsForUser(user);
                }

                return user;
            }
        }
        catch (Exception e)
        {
            throw new AuthenticationServiceException("Error authenticating user. " + e.getMessage(), e);
        }

        return null;
    }


    @Override
    public boolean supports(Class<?> authentication)
    {

        return authentication.equals(UsernamePasswordAuthenticationToken.class);

    }


    @Override
    public void onAuthenticationFailure(HttpServletRequest req,
                                        HttpServletResponse response, AuthenticationException ex)
            throws IOException, ServletException
    {
        HttpSession session = req.getSession();

        //save error message to session attribute for redirected Login page to pick up and display
        session.setAttribute(UserController.ERROR_MESSAGE_SESSION_ATTRIBUTE, ex.getMessage());

        response.sendRedirect(req.getContextPath() + UserController.LOGIN_URL);
    }


    @Override
    public void onAuthenticationSuccess(HttpServletRequest req,
                                        HttpServletResponse response, Authentication auth) throws IOException,
            ServletException
    {

        DefaultSavedRequest defaultSavedRequest = (DefaultSavedRequest) req.getSession().getAttribute("SPRING_SECURITY_SAVED_REQUEST");

        HttpSession session = req.getSession();
        Object redirectPathVal = session.getAttribute(UserController.REDIRECT_AFTER_LOGIN_SESSION_ATTRIBUTE);

        //clear session attribute related to login to prevent stale error message and redirect url used if user
        //navigate login page again.
        session.removeAttribute(UserController.REDIRECT_AFTER_LOGIN_SESSION_ATTRIBUTE);
        session.removeAttribute(UserController.ERROR_MESSAGE_SESSION_ATTRIBUTE);

        if (redirectPathVal != null)
        {
            response.sendRedirect(redirectPathVal.toString());
        }
        else if (defaultSavedRequest != null)
        {
            response.sendRedirect(defaultSavedRequest.getRequestURI());
        }
        else
        {
            response.sendRedirect(req.getContextPath() + "/");
        }
    }

    /**
     * Expires any other active sessions for the supplied user.
     * @param user The user whose other active sessions are to be expired.
     */
    private void expireOtherSessionsForUser(UserInfo user)
    {
        List<SessionInformation> allSessions = sessionRegistry.getAllSessions(user.getName(), false);
        allSessions.forEach(SessionInformation::expireNow);
    }

}
