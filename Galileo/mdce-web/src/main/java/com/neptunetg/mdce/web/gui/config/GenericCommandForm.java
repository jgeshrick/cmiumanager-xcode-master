/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.config;

import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Backing form for generic command and control page, excluding image update.
 */
public class GenericCommandForm
{
    //Command type for CMIU
    private CommandRequest.CommandTypeCmiu commandTypeCmiu;

    //Command type for L900
    private CommandRequest.CommandTypeL900 commandTypeL900;

    private List<MiuInput> miuInputList;

    //to store mqtt address for set mqtt host or set mqtt fallback host
    private String address;

    //to store any date time related input string
    private String time1;

    //to store any date time related input string
    private String time2;

    //for recording and reporting
    private long configSetId;

    //for modem firmware update
    private String imageName;

    //for requesting to a packet
    private int requestPacketSetId;

    // e.g. fota firmware version
    private String versionNumber;

    //Time offset
    private String timeOffset;

    public GenericCommandForm()
    {}

    public GenericCommandForm(CommandRequest.CommandTypeCmiu commandTypeCmiu)
    {
        this.commandTypeCmiu = commandTypeCmiu;

        miuInputList = new ArrayList<>();
        miuInputList.add(new MiuInput()); //add a blank CmiuDescription for the view to render the first row
    }

    public CommandRequest.CommandTypeCmiu getCommandTypeCmiu()
    {
        return commandTypeCmiu;
    }

    public void setCommandTypeCmiu(CommandRequest.CommandTypeCmiu commandTypeCmiu)
    {
        this.commandTypeCmiu = commandTypeCmiu;
    }

    public CommandRequest.CommandTypeL900 getCommandTypeL900()
    {
        return commandTypeL900;
    }

    public void setCommandTypeL900(CommandRequest.CommandTypeL900 commandTypeL900)
    {
        this.commandTypeL900 = commandTypeL900;
    }

    public List<MiuInput> getMiuInputList()
    {
        return miuInputList;
    }

    public void setMiuInputList(List<MiuInput> miuInputList)
    {
        this.miuInputList = miuInputList;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getTime1()
    {
        return time1;
    }

    public void setTime1(String time1)
    {
        this.time1 = time1;
    }

    public String getTime2()
    {
        return time2;
    }

    public void setTime2(String time2)
    {
        this.time2 = time2;
    }

    public long getConfigSetId()
    {
        return configSetId;
    }

    public void setConfigSetId(long configSetId)
    {
        this.configSetId = configSetId;
    }

    public int getRequestPacketSetId()
    {
        return requestPacketSetId;
    }

    public void setRequestPacketSetId(int requestPacketSetId)
    {
        this.requestPacketSetId = requestPacketSetId;
    }

    public String getImageName()
    {
        return imageName;
    }

    public void setImageName(String imageName)
    {
        this.imageName = imageName;
    }

    public String getVersionNumber()
    {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber)
    {
        this.versionNumber = versionNumber;
    }

    public String getTimeOffset()
    {
        return timeOffset;
    }

    public void setTimeOffset(String timeOffset)
    {
        this.timeOffset = timeOffset;
    }

}
