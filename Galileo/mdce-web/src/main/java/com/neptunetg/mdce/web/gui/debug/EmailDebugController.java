/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *
 *  All rights reserved
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.gui.debug;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.email.model.EmailMessage;
import com.neptunetg.mdce.common.internal.email.service.EmailService;
import com.neptunetg.mdce.web.backend.BackEndManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Properties;

/**
 * To display the most recently generated email when running in test mode.
 */
@Controller
public class EmailDebugController
{
    private final BackEndManager backEndManager;

    private final Properties envProperties;

    @Autowired
    public EmailDebugController(BackEndManager backEndManager, @Qualifier("envProperties") Properties envProperties)
    {
        this.backEndManager = backEndManager;
        this.envProperties = envProperties;
    }

    @RequestMapping("/pages/debug/last-email")
    @ResponseBody
    public EmailMessage getLastEmail() throws InternalApiException
    {
        final boolean enabled = Boolean.parseBoolean(envProperties.getProperty("debug.last-email.enabled"));

        if (!enabled)
        {
            throw new NotFoundException("");
        }

        final EmailService emailService = backEndManager.createClient(backEndManager.getAllBackEndConfigs(), EmailService.class);
        EmailMessage emailMessage = emailService.getDebugModeEmail();

        return emailMessage;
    }
}
