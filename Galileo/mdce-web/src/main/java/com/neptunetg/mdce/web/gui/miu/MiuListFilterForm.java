/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.miu;


import com.neptunetg.mdce.common.internal.site.model.SiteDetails;

import java.util.List;

/**
 * Backing form for Miu list filtering
 */
public class MiuListFilterForm
{
    private String selectedManager;
    private String selectedLocation;
    private String selectedSite;
    private String selectedMiuType;
    private String selectedSiteName;
    private String enteredId;
    private String enteredIdType;
    private String selectedCmiuMode;
    private String selectedNetworkProvider;

    private List<String> managers;
    private List<String> locations;
    private List<SiteDetails> siteIds;
    private List<String> miuTypes;
    private List<String> cmiuModeOptions;
    private List<String> networkProviderOptions;

    private int page;

    public List<String> getManagers()
    {
        return managers;
    }

    public void setManagers(List<String> managers)
    {
        this.managers = managers;
    }

    public String getSelectedManager()
    {
        return selectedManager;
    }

    public void setSelectedManager(String selectedManager)
    {
        this.selectedManager = selectedManager;
    }

    public List<String> getLocations()
    {
        return locations;
    }

    public void setLocations(List<String> locations)
    {
        this.locations = locations;
    }

    public String getSelectedLocation()
    {
        return selectedLocation;
    }

    public void setSelectedLocation(String selectedLocation)
    {
        this.selectedLocation = selectedLocation;
    }

    public List<SiteDetails> getSiteIds()
    {
        return siteIds;
    }

    public void setSiteIds(List<SiteDetails> siteIds)
    {
        this.siteIds = siteIds;
    }

    public String getSelectedSite()
    {
        return selectedSite;
    }

    public void setSelectedSite(String selectedSite)
    {
        this.selectedSite = selectedSite;
    }

    public String getSelectedSiteName()
    {
        return selectedSiteName;
    }

    public void setSelectedSiteName(String selectedSiteName)
    {
        this.selectedSiteName = selectedSiteName;
    }

    public List<String> getMiuTypes()
    {
        return miuTypes;
    }

    public void setMiuTypes(List<String> miuTypes)
    {
        this.miuTypes = miuTypes;
    }

    public String getSelectedMiuType()
    {
        return selectedMiuType;
    }

    public void setSelectedMiuType(String selectedMiuType)
    {
        this.selectedMiuType = selectedMiuType;
    }

    public String getEnteredId()
    {
        return enteredId;
    }

    public void setEnteredId(String enteredId)
    {
        this.enteredId = enteredId;
    }

    public String getEnteredIdType()
    {
        return enteredIdType;
    }

    public void setEnteredIdType(String enteredIdType)
    {
        this.enteredIdType = enteredIdType;
    }

    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }


    /**
     * Get a destination page number for passing to PagedResult.
     * Page display on html starts from 1, whereas internally it is base 0
     * @return page number based 0
     */
    public int getPageNumberForPagedResult()
    {
        return page > 1?  page - 1 : 0;
    }

    public String getSelectedCmiuMode()
    {
        return selectedCmiuMode;
    }

    public void setSelectedCmiuMode(String selectedCmiuMode)
    {
        this.selectedCmiuMode = selectedCmiuMode;
    }

    public List<String> getCmiuModeOptions()
    {
        return cmiuModeOptions;
    }

    public void setCmiuModeOptions(List<String> cmiuModeOptions)
    {
        this.cmiuModeOptions = cmiuModeOptions;
    }

    public String getSelectedNetworkProvider()
    {
        return selectedNetworkProvider;
    }

    public void setSelectedNetworkProvider(String selectedNetworkProvider)
    {
        this.selectedNetworkProvider = selectedNetworkProvider;
    }

    public List<String> getNetworkProviderOptions()
    {
        return networkProviderOptions;
    }

    public void setNetworkProviderOptions(List<String> networkProviderOptions)
    {
        this.networkProviderOptions = networkProviderOptions;
    }
}
