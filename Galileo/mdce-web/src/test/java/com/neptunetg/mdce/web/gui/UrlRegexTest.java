/*
 *  **************************************************************************
 *
 *      Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 *
 *  **************************************************************************
 */

package com.neptunetg.mdce.web.gui;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.regex.Pattern;

/**
 * Check regexes for use in rewrite.conf
 */
public class UrlRegexTest
{
    @Test
    public void testUrlRewriteRegex()
    {
        Pattern pattern = Pattern.compile("^[\\/](?!mdce\\-web$|mdce\\-web\\/).*$|^[^\\/].*$|^$"); //   ^[\/](?!mdce\-web$|mdce\-web\/).*$|^[^\/].*$|^$
        assertFalse(pattern.matcher("/mdce-web").matches());
        assertFalse(pattern.matcher("/mdce-web/").matches());
        assertFalse(pattern.matcher("/mdce-web/path").matches());
        assertTrue(pattern.matcher("/mdce-webs").matches());
        assertTrue(pattern.matcher("").matches());
        assertTrue(pattern.matcher("/something/mdce-webs").matches());


    }

}
