/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui;

import org.junit.Test;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static com.neptunetg.mdce.web.gui.FormatDateTime.formatDateToCentralTime;
import static org.junit.Assert.assertEquals;

public final class FormatDateTimeTest
{
    @Test
    public void testFormatDate()
    {
        ZonedDateTime zdt = ZonedDateTime.parse("2015-01-30T12:34:56Z", DateTimeFormatter.ISO_DATE_TIME);

        String formattedDate = formatDateToCentralTime(zdt);

        assertEquals("2015-01-30 06:34:56 CST", formattedDate);

        formattedDate = formatDateToCentralTime(ZonedDateTime.parse("2015-12-01T23:59:00-05:00", DateTimeFormatter.ISO_DATE_TIME));
        assertEquals("2015-12-01 22:59:00 CST", formattedDate);

        formattedDate = formatDateToCentralTime(ZonedDateTime.parse("2015-12-01T05:00:00-00:00", DateTimeFormatter.ISO_DATE_TIME));
        assertEquals("2015-11-30 23:00:00 CST", formattedDate);

    }


}