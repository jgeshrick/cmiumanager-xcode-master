/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.gui.miu;

import org.junit.Test;
import org.springframework.util.StringUtils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests that the MIU list page links model generates links correctly.
 */
public class MiuListNavigationModelTest
{
    private static final int ROWS_PER_PAGE = 100;
    private static final int TOTAL_RESULTS = 500;
    private static final int EXPECTED_NUMBER_OF_PAGES = 5;

    @Test
    public void test_LinkToPage1_IsNotGenerated_WhenOnPage1()
    {
        MiuListFilterForm form = new MiuListFilterForm();

        MiuListNavigationModel miuListNavigationModel = new MiuListNavigationModel(ROWS_PER_PAGE, 1, TOTAL_RESULTS, form);

        assertFalse(StringUtils.hasText(miuListNavigationModel.getFirstPageHref()));
    }

    @Test
    public void test_LinkToPage1_IsGenerated_WhenOnLastPage()
    {
        MiuListFilterForm form = new MiuListFilterForm();

        MiuListNavigationModel miuListNavigationModel = new MiuListNavigationModel(ROWS_PER_PAGE, EXPECTED_NUMBER_OF_PAGES, TOTAL_RESULTS, form);

        assertTrue(StringUtils.hasText(miuListNavigationModel.getFirstPageHref()));
    }

    @Test
    public void test_LinkToLastPage_IsGenerated_WhenOnPage1()
    {
        MiuListFilterForm form = new MiuListFilterForm();

        MiuListNavigationModel miuListNavigationModel = new MiuListNavigationModel(ROWS_PER_PAGE, 1, TOTAL_RESULTS, form);

        assertTrue(StringUtils.hasText(miuListNavigationModel.getLastPageHref()));
    }

    @Test
    public void test_LinkToLastPage_IsNotGenerated_WhenOnLastPage()
    {
        MiuListFilterForm form = new MiuListFilterForm();

        MiuListNavigationModel miuListNavigationModel = new MiuListNavigationModel(ROWS_PER_PAGE, EXPECTED_NUMBER_OF_PAGES, TOTAL_RESULTS, form);

        assertFalse(StringUtils.hasText(miuListNavigationModel.getLastPageHref()));
    }

    /**
     * Verify that any search options are retained in the generated page links.
     */
    @Test
    public void test_LinkToLastPage_HasExpectedQueryParameters_WhenFormOptionsAreProvided()
    {
        MiuListFilterForm form = new MiuListFilterForm();
        form.setEnteredId("400000100");
        form.setEnteredIdType("miuId");
        form.setSelectedMiuType("MiuType");
        form.setSelectedManager("Manager");
        form.setSelectedLocation("Location");
        form.setSelectedSite("Site");
        form.setSelectedCmiuMode("CmiuMode");
        form.setSelectedNetworkProvider("NetworkProvider");

        MiuListNavigationModel miuListNavigationModel = new MiuListNavigationModel(ROWS_PER_PAGE, EXPECTED_NUMBER_OF_PAGES, TOTAL_RESULTS, form);

        String href = miuListNavigationModel.getFirstPageHref();
        assertTrue(href.contains("enteredId=400000100"));
        assertTrue(href.contains("enteredIdType=miuId"));
        assertTrue(href.contains("selectedMiuType=MiuType"));
        assertTrue(href.contains("selectedManager=Manager"));
        assertTrue(href.contains("selectedLocation=Location"));
        assertTrue(href.contains("selectedSite=Site"));
        assertTrue(href.contains("selectedCmiuMode=CmiuMode"));
        assertTrue(href.contains("selectedNetworkProvider=NetworkProvider"));
    }
}
