/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.scheduler.service;

/**
 * A scheduler service
 */
public interface NotificationService
{

    String SCHEDULING_TIME_ZONE = "America/Chicago";

    /**
     * Send a notification to update packet repo
     */
    void notifyUpdatePacketRepo();

    /**
     * Send a notification to fetch CMIU cellular data usage history from CNS and check for alerts.
     */
    void notifyGetCellularUsage();

    /**
     * Send a notification to fetch check the lifecycle state of each CMIU
     */
    void notifyCheckCmiuLifecycleState();

    /**
     * Send a notification to check the state of each unactivated ICCID with a CMIU attached
     */
    void notifyCheckPendingCellularStates();

    /**
     * Send a notification to update SIM data and check for new SIMs
     */
    void notifyCheckSimList();

    /**
     * Send a notification to check if site has communicated.
     */
    void notifyCheckSiteCommunication();
}
