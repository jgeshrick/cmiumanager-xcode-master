/*
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 */

package com.neptunetg.mdce.scheduler.service;

import com.neptunetg.common.aws.MdceIpcPublishService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Implementation of Scheduler service
 */
@Service
@EnableScheduling
public class NotificationServiceImpl implements NotificationService
{
    private static final Logger log = LoggerFactory.getLogger(NotificationServiceImpl.class);

    private final MdceIpcPublishService awsQueueService;

    @Autowired
    public NotificationServiceImpl(MdceIpcPublishService awsQueueService)
    {
        this.awsQueueService = awsQueueService;
    }

    /**
     * Send a notification to update packet repo.
     */
    @Override
    public void notifyUpdatePacketRepo()
    {
        this.awsQueueService.sendMessage(MdceIpcPublishService.MANAGE_NOSQL_QUEUE, LocalDateTime.now().toString());
    }

    /**
     * Send a notification to fetch CMIU cellular data usage history from CNS and check for alerts.
     */
    @Override
    public void notifyGetCellularUsage()
    {
        this.awsQueueService.sendMessage(MdceIpcPublishService.CHECK_CELLULAR_BILLING, LocalDateTime.now().toString());
    }

    /**
     * Send a notification to check the lifecycle state of each CMIU
     */
    @Override
    public void notifyCheckCmiuLifecycleState()
    {
        this.awsQueueService.sendMessage(MdceIpcPublishService.CHECK_CMIU_LIFECYCLE_STATE, LocalDateTime.now().toString());
    }

    /**
     * Send a notification to check the state of each preactivated ICCID with a CMIU attached
     */
    @Override
    public void notifyCheckPendingCellularStates()
    {
        this.awsQueueService.sendMessage(MdceIpcPublishService.CHECK_PENDING_CELLULAR_STATE, LocalDateTime.now().toString());
    }


    /*
     * Send a notification to poll the list of SIMs for new data
     */
    @Override
    public void notifyCheckSimList()
    {
        this.awsQueueService.sendMessage(MdceIpcPublishService.CHECK_SIM_LIST, LocalDateTime.now().toString());
    }

    /**
     * Send a notification to check if site has communicated.
     */
    @Override
    public void notifyCheckSiteCommunication()
    {
        this.awsQueueService.sendMessage(MdceIpcPublishService.CHECK_SITE_COMMUNICATION, LocalDateTime.now().toString());
    }

    /**
     * Runs every hour to prod the packet repo to perform maintenence tasks
     * Seconds=0, Minutes=0, Hours=*, Day of month=*, Month=*, Day of week=*
     * Timezone = US Central, not that this is relevant
     */
    @Scheduled(cron = "0 0 * * * *", zone = SCHEDULING_TIME_ZONE)
    private void maintainPacketRepositoryHourly()
    {
        log.info("maintainPacketRepositoryHourly - {}", LocalDateTime.now());
        this.notifyUpdatePacketRepo();
    }

    /**
     * Runs every hour to get near real time data usage for the current day.
     * Seconds=0, Minutes=0, Hours=*, Day of month=*, Month=*, Day of week=*
     * Timezone = US Central, not that this is relevant
     */
    @Scheduled(cron = "0 0 * * * *", zone = SCHEDULING_TIME_ZONE)
    private void getCellularDeviceDataUsage()
    {
        log.info("check cellular data usage - {}", LocalDateTime.now());
        this.notifyGetCellularUsage();
    }

    /**
     * Runs every six hours
     */
    @Scheduled(fixedDelay = 6*60*60*1000)
    private void notifyAlertServiceToRemoveOldAlerts()
    {
        this.awsQueueService.sendMessage(MdceIpcPublishService.MANAGE_OLD_ALERT, LocalDateTime.now().toString());
    }

    /**
     * Runs every hour
     */
    @Scheduled(fixedDelay = 60*60*1000)
    private void notifyCheckCmiuLifecycleStateHourly()
    {
        log.info("check CMIU lifecycle state - {}", LocalDateTime.now());
        this.notifyCheckCmiuLifecycleState();
    }

    /**
     * Runs every minute
     * Check preactivated, suspended and death bed states
     */
    @Scheduled(fixedDelay = 60*1000)
    private void notifyCheckPendingCellularStatesEveryMinute()
    {
        log.info("check unactivated cellular state - {}", LocalDateTime.now());
        this.notifyCheckPendingCellularStates();
    }

    /**
     * Runs every hour
     */
    @Scheduled(fixedDelay = 60*60*1000)
    private void notifyCheckSimListHourly()
    {
        log.info("check sim list");
        this.notifyCheckSimList();
    }

    /**
     * Send a notification to check if site has communicated.
     */
    @Scheduled(fixedDelay = 6*60*60*1000)
    private void notifyCheckSiteCommunicationEverySixHours()
    {
        log.info("check site communication");
        System.out.println("check site communication");
        this.notifyCheckSiteCommunication();
    }
}
