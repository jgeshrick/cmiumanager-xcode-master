/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.scheduler.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

/**
 * Return the application version in the applicationVersion.properties of the file
 */
@Service
public class ApplicationInfoService
{
    @Value("#{appVersion['applicationVersion']}")
    private String applicationVersion;

    @Value("#{envProperties['env.name']}")
    private String environmentName;

    /**
     * Get the application version from applicationVersion.properties
     * @return application version.
     */
    public String getApplicationVersion()
    {
        return applicationVersion;
    }

    /**
     * Get the environment name
     * @return name of environment
     */
    public String getEnvironmentName()
    {
        return environmentName;
    }



}
