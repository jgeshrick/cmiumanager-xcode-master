/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.scheduler;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.FileAppender;
import com.neptunetg.mdce.scheduler.domain.ApplicationInfoService;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.ZonedDateTime;
import java.util.Iterator;

/**
 * main() loop for scheduler
 */
public class RunScheduler
{
    private static final Logger log = (Logger)LoggerFactory.getLogger(RunScheduler.class);

    private static final long ONE_HOUR_MILLIS = 1000L * 60L * 60L;

    public static void main(String... args)
    {
        final PrintStream sysout = System.out;
        sysout.println("MDCE Scheduler");
        sysout.println("Started at: " + ZonedDateTime.now());

        log.info("Starting up");

        String hostname;
        try
        {
            hostname = InetAddress.getLocalHost().getHostName();
        }
        catch (UnknownHostException e)
        {
            hostname = "unknown (" + e.getMessage() + ")";
        }
        sysout.println("Host: " + hostname);
        log.info("Host: " + hostname);

        final ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext("/spring/root-context.xml");

        final ApplicationInfoService version = appContext.getBean(ApplicationInfoService.class);

        sysout.println("Scheduler Code Version: " + version.getApplicationVersion());
        sysout.println("Environment: " + version.getEnvironmentName());
        log.info("Scheduler Version: " + version.getApplicationVersion());
        log.info("Environment: " + version.getEnvironmentName());
        sysout.println("See log file:");

        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        for (Logger logger : context.getLoggerList()) {
            for (Iterator<Appender<ILoggingEvent>> index = logger.iteratorForAppenders(); index.hasNext();) {
                Appender<ILoggingEvent> appender = index.next();
                if (appender instanceof FileAppender)
                {
                    FileAppender<?> fa = (FileAppender<?>)appender;
                    sysout.println(fa.getFile());
                }
            }
        }

        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                log.info("Received signal to shut down");
                sysout.println("Received signal to shut down at " + ZonedDateTime.now());
                appContext.close();
            }
        });


        //now wait until killed
        for(;;)
        {
            try
            {
                Thread.sleep(ONE_HOUR_MILLIS);
            }
            catch (InterruptedException e)
            {
                log.info("Run loop interrupted");
                sysout.println("Interrupted at " + ZonedDateTime.now());
                appContext.close();
                break;
            }
        }
    }
}
