/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.scheduler.service;

import com.neptunetg.common.aws.MdceIpcPublishService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)  //cause mocks to be created
public class NotificationServiceImplTest
{

    @InjectMocks
    NotificationServiceImpl notificationService;    //SUT

    @Mock
    private MdceIpcPublishService awsQueueService;  //injected into SUT

    @Test
    public void testNotifyUpdatePacketRepo() throws Exception
    {
        notificationService.notifyUpdatePacketRepo();

        verify(awsQueueService).sendMessage(eq(MdceIpcPublishService.MANAGE_NOSQL_QUEUE), anyString());

    }

}