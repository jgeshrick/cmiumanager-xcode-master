<%--
  ~ /* ***************************************************************************
  ~ *
  ~ *    Neptune Technology Group
  ~ *    Copyright 2015 as unpublished work.
  ~ *    All rights reserved
  ~ *
  ~ *    The information contained herein is confidential
  ~ *    property of Neptune Technology Group. The use, copying, transfer
  ~ *    or disclosure of such information is prohibited except by express
  ~ *    written agreement with Neptune Technology Group.
  ~ *
  ~ *****************************************************************************/
  --%>

<%--
  Created by IntelliJ IDEA.
  User: SML1
  Date: 13/03/2015
  Time: 18:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
    <title>MDCE MQTT - Packet dump</title>
    <c:url var="resourcesUrl" value="/resources"/>
    <link rel="stylesheet" href="${resourcesUrl}/css/mdce.css" media="all" />
    <link rel="stylesheet" href="${resourcesUrl}/css/grid.css" media="all" />
    <link rel="icon" href="${resourcesUrl}/images/neptune.ico"/>

</head>
<body>
<div id="titlebar">
    <img src="${resourcesUrl }/images/neptune_header_logo.png"/>

    <h1>Managed Data Collection Engine: DynamoDb received miu packet dump</h1>
</div>
<div class="maincontent">
    <div>Number of packets: ${packetViewModels.size()} for miu id: ${miuId}</div>

    <c:if test="${not empty packetViewModels}">
        <table>
            <thead>
                <tr>
                    <th>Built date (epoch millis)</th>
                    <th>Built date (human readable)</th>
                    <th>Insert date (epoch millis)</th>
                    <th>Insert date (human readable)</th>
                    <th>Packet data (hex)</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="packet" items="${packetViewModels}">
                    <tr>
                        <td>${packet.dynamoItem.dateBuilt}</td>
                        <td><fmt:formatDate value="${packet.dateBuilt}" type="BOTH" dateStyle="LONG" timeStyle="LONG" /></td>
                        <td>${packet.dynamoItem.insertDate}</td>
                        <td><fmt:formatDate value="${packet.insertDate}" type="BOTH" dateStyle="LONG" timeStyle="LONG" /></td>
                        <td><div style="overflow: scroll; max-width: 90ex;">${packet.packetHex}</div></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

    </c:if>
</div>
</body>
</html>
