<%--
  ~ /* ***************************************************************************
  ~ *
  ~ *    Neptune Technology Group
  ~ *    Copyright 2015 as unpublished work.
  ~ *    All rights reserved
  ~ *
  ~ *    The information contained herein is confidential
  ~ *    property of Neptune Technology Group. The use, copying, transfer
  ~ *    or disclosure of such information is prohibited except by express
  ~ *    written agreement with Neptune Technology Group.
  ~ *
  ~ *****************************************************************************/
  --%>

<%--
  Created by IntelliJ IDEA.
  User: SML1
  Date: 02/03/2015
  Time: 10:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<html>
<head>
    <title>MDCE MQTT - MQTT Broker status</title>
    <c:url var="resourcesUrl" value="/resources"/>
    <link rel="stylesheet" href="${resourcesUrl}/css/mdce.css" media="all"/>
    <link rel="stylesheet" href="${resourcesUrl}/css/grid.css" media="all"/>
    <link rel="icon" href="${resourcesUrl}/images/neptune.ico"/>
</head>
<body>
<div id="titlebar">
    <img src="${resourcesUrl }/images/neptune_header_logo.png"/>

    <h1>Managed Data Collection Engine: Mosquitto Broker Status</h1>
</div>
<div class="maincontent">
    <div>
        <h2>Primary broker detail:</h2>
        <ul>
            <li>Broker: ${mqttUrl}</li>
            <li>Client id: ${mqttClientId}</li>
            <li>Local client dir: ${mqttDir}</li>
            <li>Connected: ${isConnectedToMqttBroker}</li>
            <li>Last status updated: ${lastUpdateTime}</li>
        </ul>
        <form method="POST">
            <input type="hidden" name="reconnect" value="true">
            <input type="submit" value="Reconnect">
        </form>
    </div>
    <div>
        <h2>Full broker list:</h2>
        <ul>
            <c:forEach var="broker" items="${allBrokers}">
                <li>${broker}</li>
            </c:forEach>
        </ul>
    </div>

    <div>
        <h2>Primary broker status (topic: $SYS/#)</h2>
        <table>
            <thead>
            <tr>
                <th>Topic</th>
                <th>Data</th>
            </tr>
            </thead>
            <tbody>

            <div>Number of <a href="http://mosquitto.org/man/mosquitto-8.html">broker status</a> items
                (rows): ${brokerStatus.size()}</div>
            <p>Description of Mosquitto broker statuses is <a href="http://mosquitto.org/man/mosquitto-8.html">available
                here</a></p>
            <c:forEach items="${brokerStatus}" var="kvp">
                <tr>
                    <td>${kvp.key}</td>
                    <td>${kvp.value}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>
