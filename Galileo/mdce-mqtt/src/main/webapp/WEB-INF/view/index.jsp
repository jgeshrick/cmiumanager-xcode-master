<%--
  ~ /* ***************************************************************************
  ~ *
  ~ *    Neptune Technology Group
  ~ *    Copyright 2015 as unpublished work.
  ~ *    All rights reserved
  ~ *
  ~ *    The information contained herein is confidential
  ~ *    property of Neptune Technology Group. The use, copying, transfer
  ~ *    or disclosure of such information is prohibited except by express
  ~ *    written agreement with Neptune Technology Group.
  ~ *
  ~ *****************************************************************************/
  --%>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>MDCE MQTT</title>
    <c:url var="resourcesUrl" value="/resources" />
    <link rel="stylesheet" href="${resourcesUrl}/css/mdce.css" media="all" />
    <link rel="stylesheet" href="${resourcesUrl}/css/grid.css" media="all" />
    <link rel="icon" href="${resourcesUrl}/images/neptune.ico" />
</head>
<body>
    <div id="titlebar">
        <img src="${resourcesUrl }/images/neptune_header_logo.png"/>

        <h1>Managed Data Collection Engine: MDCE MQTT</h1>
    </div>
    <div class="maincontent">
        <div class="section">
            <p>Application version: ${applicationVersion}</p>
            <p>Environment: ${environmentName}</p>
            <p>Server: ${serverName}</p>
        </div>
        <div>
            <ul>
                <c:url var="url" value="/pages/debug/broker/" />
                <li><a href="${url}" > Broker status</a></li>
                <c:url var="url" value="/pages/debug/packet/1" />
                <li><a href="${url}" > Dynamo packet viewer miuid = 1 </a></li>
            </ul>
        </div>
    </div>


</body>
</html>
