/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.common;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Create valid alert notification JSON in format:
 *
 "{\"Source\":\"CMIU/" + lastReportedConfig.getMiuId() + "/Config/Mismatch\"," +
 "\"Level\":\"Error\"," +
 "\"Message\":\" Mismatch config. Expected: " + configMgmt.getCurrentConfig().getConfigParametersDescription() +
 ". Reported: " + lastReportedConfig.getConfigParametersDescription() +
 "\"}");
 */
public class JsonAlertMessage
{
    @JsonProperty("Source")
    public String source;

    @JsonProperty("Level")
    public String level;

    @JsonProperty("Message")
    public String message;

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public String getLevel()
    {
        return level;
    }

    public void setLevel(String level)
    {
        this.level = level;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
