/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.mqtt.common;

import com.neptunetg.common.data.miu.MiuId;

/**
 * Cmiu command update
 */
public class CmiuCommandResponse
{
    private final MiuId miuId;
    private final long commandType;
    private final long errorId;

    public CmiuCommandResponse(MiuId miuId, long commandType, long errorId)
    {
        this.miuId = miuId;
        this.commandType = commandType;
        this.errorId = errorId;
    }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public long getCommandType()
    {
        return commandType;
    }

    public long getErrorId()
    {
        return errorId;
    }

}
