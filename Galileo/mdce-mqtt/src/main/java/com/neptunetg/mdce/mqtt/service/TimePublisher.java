/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.service;


import com.neptunetg.common.packet.model.builder.PacketBuilder;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.tags.CurrentTimeAndDateData;
import com.neptunetg.common.packet.model.taggeddata.tags.MqttPacketHeaderData;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Every second, update the retained time message on the primary broker
 */
@Service
@EnableScheduling
public class TimePublisher
{

    private static final Logger logger = LoggerFactory.getLogger(TimePublisher.class);

    private final BrokerConnectAsMdceService topicSub;

    @Autowired
    public TimePublisher(BrokerConnectAsMdceService topicSub)
    {
        this.topicSub = topicSub;
    }

    private final AtomicBoolean enablePublisher = new AtomicBoolean(false);

    private static final String topicName = "Time";

    public void Start()
    {
        enablePublisher.set(true);
    }

    public void stop()
    {
        enablePublisher.set(false);
    }

    /**
     * Every second, send time packet to primary broker
     */
    @Scheduled(cron = "* * * * * *")
    public void publishTimeTask() throws MqttException
    {
        if (this.isPublishing())
        {
            this.publishTime();
        }
    }


    /**
     * Publish the time to MQTT broker
     * @throws MqttException
     */
    private void publishTime() throws MqttException
    {
        MqttPacketHeaderData mqttPacketHeaderData = new MqttPacketHeaderData.Builder()
                .sequenceNumber(0)
                .keyInfo(0)
                .encryptionMethod(0)
                .token(0)
                .build();

        CurrentTimeAndDateData currentTimeAndDateData = CurrentTimeAndDateData.Builder.build();

        PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.TimeOfDay)
                .appendTagData(mqttPacketHeaderData)
                .appendTagData(currentTimeAndDateData);

        byte[] rawTimeAndDatePacket = packetBuilder.buildTaggedPacket().toByteSequence();

        final int timeTopicQos = 0;

        //publish a retained message so that mqtt client will receive the message when it subscribes
        topicSub.publishToPrimaryBroker(topicName, rawTimeAndDatePacket, timeTopicQos, true);
    }

    public String getTopicName()
    {
        return topicName;
    }

    public boolean isPublishing()
    {
        return this.enablePublisher.get();
    }

}
