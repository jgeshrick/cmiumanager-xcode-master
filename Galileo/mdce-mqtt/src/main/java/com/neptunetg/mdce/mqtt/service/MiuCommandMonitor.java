/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.command.CommandLifeCycle;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.common.data.miu.command.MiuCommandDetail;
import com.neptunetg.common.data.miu.command.MiuCommandRepository;
import com.neptunetg.common.util.HexUtils;
import com.neptunetg.mdce.common.data.PacketRepository;
import com.neptunetg.mdce.common.data.model.DynamoItemPacketType;
import com.neptunetg.mdce.common.data.model.MiuPacketSentDynamoItem;
import com.neptunetg.mdce.mqtt.common.MdceMqttInternalException;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * For publishing and retrieving CMIU commands to/from the broker.
 * The class constantly polls the RDS database to check whether commands need to be published.
 */
@Service
public class MiuCommandMonitor implements AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(MiuCommandMonitor.class);
    public static final String cmiuTopicName = "CMIU/Command";

    private final MiuCommandRepository miuCommandRepository;

    private final BrokerConnectAsMdceService brokers;

    private final BrokerConnectAsMiuService brokersConnectedAsCmiu;

    private final PacketRepository packetRepo;

    private final MdceIpcSubscribeService awsSubscribeService;

    private MdceIpcPublishService awsQueueService;


    @Autowired
    public MiuCommandMonitor(
            MdceIpcSubscribeService awsSubscribeService,
            MdceIpcPublishService awsQueueService,
            MiuCommandRepository miuCommandRepo,
            BrokerConnectAsMdceService pubSubService,
            BrokerConnectAsMiuService allBrokers,
            PacketRepository packetRepo)
    {
        this.miuCommandRepository = miuCommandRepo;
        this.brokers = pubSubService;
        this.brokersConnectedAsCmiu = allBrokers;
        this.packetRepo = packetRepo;
        this.awsSubscribeService = awsSubscribeService;
        this.awsQueueService = awsQueueService;

        //create a send command observer to listen to AWS SQS for send command message sent from mdce-integration
        awsSubscribeService.register(this, MdceIpcSubscribeService.SEND_COMMAND_QUEUE, Duration.ofSeconds(1L), 1);
    }

    /**
     * Publish a MIU configuration message to the brokers
     * @throws MqttException
     */
    public void publishCommand(MiuCommand command) throws MqttException, JsonProcessingException
    {
        final int commandTopicQos = 1;

        final String topic = cmiuTopicName + "/" + command.getMiuId();
        final byte[] dataToSend = command.getCommandParams();

        if (topic != null && dataToSend != null)
        {
            if (log.isInfoEnabled())
            {
                log.info("Publishing to topic: {}, message: {}", topic, HexUtils.byteArrayToHex(dataToSend));
            }

            try
            {
                brokers.publishToAllBrokers(topic, dataToSend, commandTopicQos, false);
            }
            catch (MqttException e)
            {
                log.error("Failed to send command over MQTT: ", e);

                final AlertSource alertSource = AlertSource.MQTT_FAILED_CONNECTION;

                awsQueueService.sendMessage(MdceIpcPublishService.ALERTS,
                        "{\"Source\":\"" + alertSource.getAlertId() + "\"," +
                                "\"Level\":\"Error\"," +
                                "\"Message\":\"Error when attempting to send command over MQTT, " +
                                "suspect state file or directory is missing\"}");
            }

            //record to dynamoDb
            try
            {
                final Instant now = Instant.now();

                final MiuPacketSentDynamoItem miuPacket = new MiuPacketSentDynamoItem();
                miuPacket.setMiuId(command.getMiuId().numericValue());
                miuPacket.setPacketData(command.getCommandParams());
                miuPacket.setDateBuilt(now.toEpochMilli());
                miuPacket.setInsertDate(now.toEpochMilli());
                miuPacket.setSourceDeviceType("MDCE");

                MiuType targetDeviceType = command.getTargetDeviceType();
                if (targetDeviceType == null)
                {
                    targetDeviceType = MiuType.CMIU;
                }
                miuPacket.setTargetDeviceType(targetDeviceType.toString());

                miuPacket.setPacketType(DynamoItemPacketType.CMIU_COMMAND_CONFIGURATION.toString());

                packetRepo.insertMiuPacketSent(miuPacket);
            }
            catch (Exception e)
            {
                log.error("Cannot insert packet received for " + command.getMiuId(), e);
            }
        }
    }

    /**
     * Check whether there is any new commands to need to be published to mqtt broker.
     */
    public boolean checkAndPublishNewCommandTask(AtomicBoolean cancellationToken)
    {
        log.trace("checkAndPublishNewCommandTask...");

        //get config commands that are awaiting deletion from mqtt broker
        List<MiuCommandDetail> deleteCommandList = miuCommandRepository.getMiuCommandsByState(CommandLifeCycle.RECALLED, MiuType.CMIU);

        Map<MiuId, List<MiuCommand>> deleteCommandListGroupByMiuId = deleteCommandList.stream()
                .collect(Collectors.groupingBy(c -> c.getMiuId()));

        for (Map.Entry<MiuId, List<MiuCommand>> me : deleteCommandListGroupByMiuId.entrySet())
        {
            if (cancellationToken.get())
            {
                return false;
            }
            final MiuId miuId = me.getKey();
            final List<MiuCommand> commandList = me.getValue();
            try
            {
                this.recallQueuedMiuCommand(miuId, commandList.get(0).getTargetDeviceType());

                miuCommandRepository.updateMiuCommandsStateByMiuId(miuId,
                        CommandLifeCycle.RECALLED,
                        CommandLifeCycle.CANCELLED);

                //commands that are queued at MQTT broker will be removed too, because we do not know whether they have
                //been received by the MIU.
                miuCommandRepository.updateMiuCommandsStateByMiuId(miuId,
                        CommandLifeCycle.QUEUED,
                        CommandLifeCycle.CANCELLED);
            }
            catch (MdceMqttInternalException e)
            {
                log.error("Error occurred while recalling command for MIU ID " + miuId, e);
            }
        }

        // get miu commands that are created and waiting to be published to mqtt broker
        final List<MiuCommandDetail> publishCommandList = miuCommandRepository.getMiuCommandsByState(CommandLifeCycle.CREATED, MiuType.CMIU);

       for (MiuCommand c : publishCommandList)
       {
           if (cancellationToken.get())
           {
               return false;
           }
           //publish
           try
           {
                List<MiuCommandDetail> pendingCommands =
                        miuCommandRepository.getPendingCommands(c.getMiuId());

                //If no commands pending for MIU yet, then re-subscribe, to reduce state-fullness
                if(pendingCommands.size() == 0)
                {
                    brokersConnectedAsCmiu.subscribeMiuCommandTopicOnAllBrokers(c.getMiuId().toString(),
                            c.getTargetDeviceType());
                }

                this.publishCommand(c);

                //if publish is successful, update state of the command from CREATED to QUEUED
                miuCommandRepository.updateMiuCommandsState(c.getCommandId(), CommandLifeCycle.QUEUED);
           }
           catch (MqttException e)
           {
                log.error("Error occurred while publishing command for MIU ID " + c.getMiuId(), e);
           }
           catch (Exception e)
           {
                log.error("Error occured while trying to publish command for MIU ID" + c.getMiuId(), e);
           }
       }

       return true; //all done
    }

    /**
     * Recall any outstanding messages published in the MQTT broker that has yet to be published to the target CMIU.
     * @param miuId the MIU id of the MIU for creating the mqtt clientId. The MIU MQTT client ID is MIU/{miuId}.
     * @return the number of messages retrieved from the broker.
     */
    public int recallQueuedMiuCommand(MiuId miuId, MiuType miuType) throws MdceMqttInternalException
    {
        int messageRecalled = 0;

        try
        {
            brokersConnectedAsCmiu.clearAllOutgoingMessages(miuId, miuType, Instant.now());

            //set all miu commands of this miu of state Recalled to Cancelled
            miuCommandRepository.updateMiuCommandsStateByMiuId(miuId,
                    CommandLifeCycle.RECALLED, CommandLifeCycle.CANCELLED);
        }
        catch (Exception e)
        {
            log.error("Error in recalling queued command" + e);
            throw new MdceMqttInternalException(e.getMessage());
        }

        return messageRecalled;
    }

    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        log.trace("SQS message received: {} - {}", messageId, message);
        return this.checkAndPublishNewCommandTask(cancellationToken);
    }
}
