/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.mqtt.data.settings;

import com.neptunetg.mdce.common.config.DynamoDynamicConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Stores settings for MDCE in mdce_settings table
 */
@Repository
public class JdbcMdceSettingsRepository implements MdceSettingsRepository, DynamoDynamicConfigService
{

    private static final Logger log = LoggerFactory.getLogger(JdbcMdceSettingsRepository.class);

    private final JdbcTemplate db;

    @Autowired
    public JdbcMdceSettingsRepository(JdbcTemplate db)
    {
        this.db = db;
    }


    @Override
    public long getIntSetting(String key, long defaultValue)
    {
        String sql = "SELECT int_value FROM mdce_settings WHERE `key`=?";
        try
        {
            return db.queryForObject(sql, Long.class, key);
        }
        catch (EmptyResultDataAccessException e)
        {
            sql = "INSERT INTO `mdce`.`mdce_settings` (`key`, `int_value`) VALUES (?, ?)";
            db.update(sql, key, defaultValue);
            return defaultValue;
        }
    }


    @Override
    public String getTextSetting(String key, String defaultValue)
    {
        String sql = "SELECT string_value FROM mdce_settings WHERE `key`=?";
        try
        {
            return db.queryForObject(sql, String.class, key);
        }
        catch (EmptyResultDataAccessException e)
        {
            sql = "INSERT INTO `mdce`.`mdce_settings` (`key`, `string_value`) VALUES (?, ?)";
            db.update(sql, key, defaultValue);
            return defaultValue;
        }
    }

    @Override
    public boolean isDynamicConfigAvailable()
    {
        return true;
    }

    @Override
    public long getOldTableDeletionMinAgeMonths()
    {
        return getIntSetting(DELETE_TABLE_MIN_AGE_MONTHS_SETTING, 3L);
    }
}
