/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.service;

import com.neptunetg.mdce.mqtt.service.subscription.BrokerStatusSubscription;
import com.neptunetg.mdce.mqtt.service.subscription.CmiuReportSubscription;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Management the connection/disconnection to MQTT broker during application startup and shutdown respectively,
 * setup subscriptions to listen to, and start time topic publication service.
 */
@Service
@EnableScheduling
public class MqttSubscriptionManager implements ApplicationListener<ApplicationEvent>
{
    public static final String MQTT_RESTARTED_FILENAME = ".mqtt-restarted";

    private static final Logger log = LoggerFactory.getLogger(MqttSubscriptionManager.class);

    @Autowired
    private TimePublisher timePublisher;

    @Autowired
    private BrokerConnectAsMdceService pubSubService;

    @Autowired
    private BrokerStatusSubscription brokerStatusSub;

    @Autowired
    private CmiuReportSubscription cmiuReportSub;

    @Value("#{envProperties['mqtt.tmpDir']}")
    private String tmpFolder;

    private AtomicBoolean isMdceMqttInitialized;

    public MqttSubscriptionManager()
    {
        this.isMdceMqttInitialized = new AtomicBoolean();
        this.isMdceMqttInitialized.set(false);
    }

    /**
     * @throws MqttException
     * Starts the mqtt lifecycle by connecting to the broker, subscribing to topics and start timer publishing.
     */
    public void startMdceMqtt() throws MqttException
    {
        //register callback handler before calling connect() so that it will process any queued message, if any,
        // immediately after client has connected to the broker.
        pubSubService.registerCallback(brokerStatusSub.getTopic(), brokerStatusSub);
        pubSubService.registerCallback(cmiuReportSub.getTopic(), cmiuReportSub);

        pubSubService.connect();

        pubSubService.subscribe(brokerStatusSub.getTopic(), 0);   //qos 0
        pubSubService.subscribe(cmiuReportSub.getTopic(), 1);    //qos 1

        timePublisher.Start();

        isMdceMqttInitialized.set(true);
    }

    /**
     * Automatically disconnect from the broker when the bean is destroyed.
     */
    public void stopMdceMqtt()
    {
        isMdceMqttInitialized.set(false);

        log.info("Disconnecting MDCE-Mqtt client");
        timePublisher.stop();
        pubSubService.disconnect();
        cmiuReportSub.shutdown();
        brokerStatusSub.shutdown();
    }


    /**
     * Listen on application event to connect/disconnect to/from MQTT broker.
     * ServletRequestEvent is also triggered in this function which are ignored.
     * @param applicationEvent event raised on application start, refresh, close etc raised both by root context
     *                         and servlet.
     *
     */
    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent)
    {

        if (applicationEvent instanceof ContextRefreshedEvent)
        {
            log.info("ContextRefreshedEvent received: " + applicationEvent);
            //context refresh event is used to handle root context startup
            if (((ContextRefreshedEvent) applicationEvent).getApplicationContext().getDisplayName().contains("Root"))
            {
                try
                {
                    this.startMdceMqtt();
                }
                catch (MqttException e)
                {
                    log.error("Error starting mdce mqtt service: ", e);
                }
            }

        }
        else if (applicationEvent instanceof ContextClosedEvent)
        {
            //Context closed event is used to handle root context shutdown
            log.info("ContextClosedEvent " + ((ContextClosedEvent) applicationEvent).getApplicationContext().getDisplayName());

            if (((ContextClosedEvent) applicationEvent).getApplicationContext().getDisplayName().contains("Root"))
            {
                this.stopMdceMqtt();
            }


        }
    }

    /**
     * Periodically check for connection to mqtt broker and reconnect if it has been disconnected.
     * The interval for checking is set to 1 second for the moment.
     */
    @Scheduled(fixedDelay = 1000)
    public void checkIsConnectedToMqttBroker()
    {
        if (!pubSubService.isConnected())
        {
            log.info("Scheduler is reconnecting to MQTT broker.");
            try
            {
                this.startMdceMqtt();
            }
            catch (MqttException e)
            {
                log.error("Error attempting to reconnect to MQTT broker", e);
            }
        }
    }

    /**
     * Using a similar check to MqttStatusCheck.checkMqttStatusDirectoryExists, checks whether the MQTT directory is
     * present and, if not, restarts the MQTT client.
     */
    @Scheduled(fixedDelay = 1000*60) // Run every 1 minute
    private void checkMqttStatusDirectoryExists()
    {
        if (!isMdceMqttInitialized.get())
        {
            log.debug("MQTT temp directory not checked as service is not currently initialized.");
            return;
        }

        File folder = new File(tmpFolder);
        File[] listOfFiles = folder.listFiles();

        boolean containsMqttFolder = false;

        if (listOfFiles != null)
        {
            for (File file : listOfFiles)
            {
                if (file.isDirectory() && file.getName().contains("mqtt"))
                {
                    containsMqttFolder = true;
                }
            }
        }

        if(!containsMqttFolder)
        {
            stopMdceMqtt();
            try
            {
                startMdceMqtt();
            }
            catch (MqttException e)
            {
                log.error("Error attempting to restart MQTT broker", e);
            }

            try
            {
                // Leave a breadcrumb for MqttStatusCheck so that an alert can be created.
                String path = tmpFolder + (tmpFolder.endsWith(File.separator) ? "" : File.separator) + MQTT_RESTARTED_FILENAME;
                new FileOutputStream(path, false).close();
            }
            catch (IOException e)
            {
                log.error("Error creating breadcrumb at path \"" + tmpFolder + "\" for MQTT restart.", e);
            }
        }
    }}
