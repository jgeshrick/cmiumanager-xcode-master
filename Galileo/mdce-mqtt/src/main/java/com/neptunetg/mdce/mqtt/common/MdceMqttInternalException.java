/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.mqtt.common;

/**
 * Created by SML1 on 19/06/2015.
 */

public class MdceMqttInternalException extends Exception
{
    /**
     * Exception with message only
     * @param message The message
     */
    public MdceMqttInternalException(String message)
    {
        super(message);
    }

    /**
     * Exception with message and cause
     * @param message Message
     * @param cause Cause
     */
    public MdceMqttInternalException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
