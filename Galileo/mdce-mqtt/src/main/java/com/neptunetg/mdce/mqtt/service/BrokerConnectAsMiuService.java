/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.service;

import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import javafx.util.Pair;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.util.*;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Service which connects to the MQTT brokers as a MIU.  Used to subscribe MIUs on all the brokers behind the
 * load balancer, and to wipe outgoing commands from all brokers
 */
@Service
public class BrokerConnectAsMiuService implements MqttCallback, ApplicationListener<ContextClosedEvent>
{
    private static final Logger log = LoggerFactory.getLogger(BrokerConnectAsMiuService.class);

    public static final TemporalAmount CLEAR_ALL_OUTGOING_MESSAGES_DELAY = Duration.of(5L, ChronoUnit.MINUTES);
    private static final long MILLIS_TO_WAIT_FOR_COMMANDS_TO_DRAIN = 5000L;

    private final String[] brokerUrls;
    private final String mqttclientTmpDir;

    private final ScheduledThreadPoolExecutor executor;
    private final Set<Pair<MiuId, MiuType>> miuIdsToClearMessages = new HashSet<Pair<MiuId, MiuType>>();

    private final MdceIpcPublishService ipc;


    @Autowired
    public BrokerConnectAsMiuService(@Qualifier("envProperties") Properties envProperties,
                                     MdceIpcPublishService ipc)
    {
        this.ipc = ipc;

        this.executor = new ScheduledThreadPoolExecutor(1);
        this.executor.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);

        this.mqttclientTmpDir = envProperties.getProperty("mqtt.tmpDir", "");

        final int mqttBrokerCount;
        try
        {
            mqttBrokerCount = Integer.parseInt(envProperties.getProperty("mqtt.broker.count"));
        }
        catch (NumberFormatException e)
        {
            throw new RuntimeException("Could not parse value of mqtt.broker.count=" + envProperties.getProperty("mqtt.broker.count") + "from mdce-env.properties", e);
        }

        this.brokerUrls = new String[mqttBrokerCount];

        for (int brokerId = 0; brokerId < mqttBrokerCount; brokerId++)
        {
            brokerUrls[brokerId] = envProperties.getProperty("mqtt.broker." + brokerId + ".url");
        }

        log.info("Initialized with " + mqttBrokerCount + " MQTT brokers");
    }

    public void subscribeMiuCommandTopicOnAllBrokers(String miuId, MiuType miuType) throws MqttException
    {
        if (log.isDebugEnabled())
        {
            log.debug("Subscribing MIU ID {} on all brokers", miuId);
        }

        for (String brokerUrl : brokerUrls)
        {
            MqttClient client = null;

            try
            {
                client = (this.mqttclientTmpDir.isEmpty()) ?
                        new MqttClient(brokerUrl, getMiuClientId(miuId, miuType)) :
                        new MqttClient(brokerUrl, getMiuClientId(miuId, miuType), new MqttDefaultFilePersistence(this.mqttclientTmpDir));

                MqttConnectOptions connectOptions = new MqttConnectOptions();
                // both the client and server will maintain state across restarts of the client, , the server and the connection
                connectOptions.setCleanSession(false);
                client.connect(connectOptions);
                client.subscribe(getMiuCommandTopic(miuId, miuType));
            }
            catch (MqttException e)
            {
                log.error("Error subscribing MIU " + miuId + " to broker " + brokerUrl, e);
                throw e;
            }
            finally
            {
                if(client != null)
                {
                    log.debug("Disconnecting and closing client {} on broker", miuId, brokerUrl);
                    client.disconnect();
                    client.close();
                }
            }
        }
    }

    /**
     * Clear all outgoing messages after a while
     * @param miuId MIU ID for which to clear messages
     * @param notBefore Do not clear before this time
     */
    public void clearAllOutgoingMessages(final MiuId miuId, MiuType miuType, Instant notBefore)
    {
        log.info("Scheduling to clear all outgoing messages for miuId {} at {}", miuId, notBefore);
        miuIdsToClearMessages.add(new Pair<>(miuId, miuType));
        final Runnable task = () -> doClearOutgoingMessages(miuId, miuType);
        executor.schedule(task, Duration.between(Instant.now(), notBefore).toMillis(), TimeUnit.MILLISECONDS);
    }

    private void doClearOutgoingMessages(MiuId miuId, MiuType miuType)
    {
        log.debug("Clearing all outgoing message for miuid {} from {} brokers.", miuId, brokerUrls.length);
        for (String brokerUrl : brokerUrls)
        {
            MqttClient clientToClose = null;
            try
            {
                log.debug("Create fake miu [clientId: {}] for connection to broker: {}", miuId, brokerUrl);
                final MqttClient client = (this.mqttclientTmpDir.isEmpty()) ?
                        new MqttClient(brokerUrl, getMiuClientId(String.valueOf(miuId), miuType)) :
                        new MqttClient(brokerUrl, getMiuClientId(String.valueOf(miuId), miuType),
                                new MqttDefaultFilePersistence(this.mqttclientTmpDir));

                clientToClose = client;
                client.setCallback(this);
                MqttConnectOptions connectOptions = new MqttConnectOptions();
                // both the client and server will maintain state across restarts of the client, , the server and the connection
                connectOptions.setCleanSession(false);
                client.connect(connectOptions);
                client.subscribe(getMiuCommandTopic(String.valueOf(miuId), miuType));

                final Runnable task = () -> stopListening(miuId, miuType, client);
                executor.schedule(task, MILLIS_TO_WAIT_FOR_COMMANDS_TO_DRAIN, TimeUnit.MILLISECONDS);
            }
            catch (MqttException e)
            {
                log.error("Error draining MIU " + miuId + " commands from broker " + brokerUrl, e);

                attemptClearMessagesOnSomeOtherInstance(miuId, miuType);

                if (clientToClose != null)
                {
                    try
                    {
                        clientToClose.disconnect();
                        clientToClose.close();
                    }
                    catch (MqttException e2)
                    {
                        log.warn("Error closing client after error", e2);
                    }
                }
            }
        }
    }

    private void stopListening(MiuId miuId, MiuType miuType, MqttClient client)
    {
        try
        {
            client.disconnect();
            client.close();
            log.info("Finished flushing commands - disconnecting fake miu clientId: {} from broker {}", miuId, client.getServerURI());
            miuIdsToClearMessages.remove(new Pair<>(miuId, miuType));
        }
        catch (MqttException e)
        {
            log.error("Error disconnecting client MIU ID: " + miuId + " from broker " + client.getServerURI() + ".", e);
        }
    }

    private void attemptClearMessagesOnSomeOtherInstance(MiuId miuId, MiuType miuType)
    {
        ipc.sendMessage(MdceIpcPublishService.CLEAR_MIU_MESSAGES, miuId.toString() + " , " + miuType.toString());
    }


    /**
     * Generate mqtt client id for the MIU id for connection as a fake Cmiu
     * @param miuId id of the cmiu
     * @return mqtt client id for the cmiu
     */
    private static String getMiuClientId(String miuId, MiuType miuType)
    {
        return "CMIU/" + miuId;
    }

    /**
     * Generate topic for Miu command
     * @param miuId miu id
     * @return mqtt topic CMIU/Command/{cmiuId} for CMIU, LMIU/Senet/Send/EUI for LMIU
     */
    private static String getMiuCommandTopic(String miuId, MiuType miuType)
    {
        return "CMIU/Command/" + miuId;
    }


    /**
     * Handler for connection lost.  Just logs.
     */
    @Override
    public void connectionLost(Throwable e)
    {
        log.error("Connection lost!", e);
        //TODO: fail current operation
    }

    /**
     * Implements MqttCallback interface. Handler called by MQTT broker when message delivery has completed.
     * Do nothing now. Just log to file.
     */
    @Override
    public void deliveryComplete(IMqttDeliveryToken arg0)
    {
        log.debug("deliveryComplete for message with ID " + arg0.getMessageId());
    }

    /**
     * Implements MqttCallback interface. Handler called by MQTT broker when any MQTT message arrived.
     * Parses message and puts it into the repository.
     */
    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception
    {
        log.debug("discarding message on topic: " + topic);
    }


    /**
     * Get the URLs of all the brokers
     * @return List of URLs
     */
    public List<String> getUrls()
    {
        return Arrays.asList(brokerUrls);
    }


    @Override
    public void onApplicationEvent(ContextClosedEvent contextClosedEvent)
    {
        log.info("Shutting down thread pool");
        try
        {
            executor.shutdown();
            executor.awaitTermination(1L, TimeUnit.SECONDS);

            for (Pair<MiuId, MiuType> miu : this.miuIdsToClearMessages)
            {
                log.info("Posting to SQS to remove outgoing messages for MIU " + miu.getKey() +
                        ", as this was pending at the time of shutdown");
                attemptClearMessagesOnSomeOtherInstance(miu.getKey(), miu.getValue());
            }

            this.miuIdsToClearMessages.clear();
        }
        catch (Exception e)
        {
            log.warn("Exception during cleanup", e);
        }
    }
}
