/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.service;


import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.common.data.miu.MiuType;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Listens for SQS message that a MIU has been created, and subscribes its client ID to its
 * command topics on both brokers
 */
@Service
public class MiuCreationMonitor implements AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(MiuCreationMonitor.class);

    private final BrokerConnectAsMiuService allBrokers;

    private final MdceIpcSubscribeService awsQueueService;

    @Autowired
    public MiuCreationMonitor(BrokerConnectAsMiuService allBrokers,
                              MdceIpcSubscribeService awsQueueService)
    {
        this.allBrokers = allBrokers;
        this.awsQueueService = awsQueueService;

        //create a send command observer to listen to AWS SQS for send command message sent from mdce-integration
        awsQueueService.register(this, MdceIpcSubscribeService.SUBSCRIBE_MIU_ALL_BROKERS, Duration.ofMinutes(1L), 100);
    }


    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        log.trace("SQS message received: {} - {}", messageId, message);
        String[] splitStrings = message.split(",");

        final String cmiuId = splitStrings[0].trim();
        final String miuTypeStr = splitStrings[1].trim();

        try
        {
            allBrokers.subscribeMiuCommandTopicOnAllBrokers(cmiuId, MiuType.fromString(miuTypeStr));
            return true;
        }
        catch (MqttException e)
        {
            log.error("Failed to subscribe MIU with ID " + cmiuId + " on all brokers");
            return false;
        }
    }
}
