/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.cellular.*;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.common.data.miu.command.MiuCommandDetail;
import com.neptunetg.common.data.miu.command.MiuCommandRepository;
import com.neptunetg.common.data.miu.heard.MiuUpdateRepository;
import com.neptunetg.common.packet.model.CmdId;
import com.neptunetg.common.packet.model.ErrorId;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.SecureBlockArrayData;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.CharArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.ImageVersionInfoData;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.mdce.common.data.MdceDataException;
import com.neptunetg.mdce.common.data.PacketRepository;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import com.neptunetg.mdce.mqtt.common.CmiuCommandResponse;
import com.neptunetg.mdce.mqtt.common.CmiuModemFirmwareVersion;
import com.neptunetg.mdce.mqtt.common.JsonAlertMessage;
import com.neptunetg.mdce.mqtt.data.miu.MiuPacketDataAndStats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Scheduler and updater for performing periodic updates to miu_details table.
 */
@Service
@EnableScheduling
public class MiuDetailsUpdater
{
    private static final Logger log = LoggerFactory.getLogger(MiuDetailsUpdater.class);

    private static final int MAX_MESSAGE_BATCH = 10000; //to avoid insanely large batches

    private final MiuCommandRepository miuCommandRepository;

    private final MiuUpdateRepository miuRepository;

    private final CmiuConfigRepository configRepository;

    private final CmiuRepository cmiuRepository;

    private final PacketRepository packetRepo;

    private final MdceIpcPublishService awsQueueService;

    private final BlockingQueue<MiuPacketDataAndStats> miuReportUpdateQueue = new LinkedBlockingQueue<>();
    private final BlockingQueue<CmiuConfigSetAssociation> miuConfigurationUpdateQueue = new LinkedBlockingQueue<>();
    private final BlockingQueue<CmiuModemFirmwareVersion> miuModemFirmwareVersionUpdateQueue = new LinkedBlockingQueue<>();
    private final BlockingQueue<CmiuCommandResponse> miuCommandResponseUpdateQueue = new LinkedBlockingQueue<>();
    private final BlockingQueue<CmiuRevisionSet> miuRevisionUpdateQueue = new LinkedBlockingQueue<>();

    private final AtomicBoolean shutdownFlag = new AtomicBoolean(false);

    @Autowired
    public MiuDetailsUpdater(MiuCommandRepository miuCommandRepository, MiuUpdateRepository miuRepository, CmiuConfigRepository configRepository, CmiuRepository cmiuRepository, PacketRepository packetRepo, MdceIpcPublishService awsQueueService)
    {
        this.miuCommandRepository = miuCommandRepository;
        this.miuRepository = miuRepository;
        this.configRepository = configRepository;
        this.cmiuRepository = cmiuRepository;
        this.packetRepo = packetRepo;
        this.awsQueueService = awsQueueService;
    }

    /**
     * A periodically running task to update miu details in mysql table and add received packet to dynamo
     */
    @Scheduled(fixedDelay = 100)
    public void updateMiuDetails()
    {
        if (!shutdownFlag.get())
        {
            try
            {
                processAllQueues();
            }
            catch (Exception e)
            {
                log.error("Error while processing queues.  Continuing", e);
            }
        }
    }

    /**
     * Process all the queues.  Returns false if all queues are empty
     * @return false if all queues are empty, otherwise true
     * @throws MdceDataException If something goes wrong in Dynamo
     */
    private boolean processAllQueues() throws MdceDataException
    {
        log.debug("Checking all queues for MIU updates");

        boolean notEmpty = processMiuPacketsReceived();

        notEmpty |= processCmiuConfigUpdates();

        notEmpty |= processCmiuCommandResponseUpdates();

        notEmpty |= processModemFirmwareUpdates();

        notEmpty |= processRevisionUpdate();

        log.debug("Finished checking all queues for MIU updates; queues are " + (notEmpty ? "still not all empty":"all empty"));

        return notEmpty;
    }

    private boolean processMiuPacketsReceived() throws MdceDataException
    {
        final List<MiuPacketDataAndStats> miuUpdates = new ArrayList<>();
        miuReportUpdateQueue.drainTo(miuUpdates, MAX_MESSAGE_BATCH);

        if (!miuUpdates.isEmpty())
        {
            final List<MiuPacketReceivedDynamoItem> packets = new ArrayList<>();

            for (MiuPacketDataAndStats queuedMiuDetail : miuUpdates)
            {
                miuRepository.updateMiuHeardAllStats(queuedMiuDetail.getMiuId(),
                        MiuType.valueOf(queuedMiuDetail.getReceivedDynamoItem().getSourceDeviceType()),
                        queuedMiuDetail.getLastHeardTime(), Instant.now(), queuedMiuDetail);
                packets.add(queuedMiuDetail.getReceivedDynamoItem());
            }
            //add to dynamoDB
            try
            {
                this.packetRepo.insertMiuPacketsReceived(packets);
                log.debug("inserted {} packets into Dynamo", packets.size());
            }
            catch (Exception ex)
            {
                final AlertSource alertSource = AlertSource.AWS_EC2_DB_FAILED_ADD;

                awsQueueService.sendMessage(MdceIpcPublishService.ALERTS,
                        "{\"Source\":\"" + alertSource.getAlertId() + "\"," +
                                "\"Level\":\"Error\"," +
                                "\"Message\":\"Cannot save packet to dynamoDb. " + ex.getMessage() + "\"}"); //TODO: make an alternative to AlertDetails somewhere else to avoid manual JSON construction here

                log.error("Cannot save packet to dynamo", ex);
            }
        }

        return !this.miuReportUpdateQueue.isEmpty();
    }

    private boolean processCmiuConfigUpdates()
    {
        final List<CmiuConfigSetAssociation> miuConfigurationUpdates = new ArrayList<>();
        this.miuConfigurationUpdateQueue.drainTo(miuConfigurationUpdates, MAX_MESSAGE_BATCH);

        //validate cmiu configuration from RDS with posted cmiu configuration
        for (CmiuConfigSetAssociation lastReportedConfig : miuConfigurationUpdates)
        {
            try
            {
                processCmiuReportedConfig(lastReportedConfig);
            }
            catch (Exception e)
            {
                log.error("Exception occurred while checking configuration update for CMIU " + lastReportedConfig.getMiuId(), e);
            }


        }
        return !this.miuConfigurationUpdateQueue.isEmpty();

    }

    /**
     * mdce-mqtt has received a report from a CMIU of its configuration.  Record the configuration
     * in the database, creating a new config set if necessary.
     * @param lastReportedConfig
     */
    private void processCmiuReportedConfig(CmiuConfigSetAssociation lastReportedConfig)
    {

        final MiuId miuId = lastReportedConfig.getMiuId();

        //may be null
        final CmiuConfigSetManagement initialConfigSetManagement = configRepository.getCmiuConfigMgmt(miuId);

        final ConfigSet oldConfigSet;
        if (initialConfigSetManagement == null)
        {
            oldConfigSet = null;
        }
        else
        {
            oldConfigSet = initialConfigSetManagement.getCurrentConfig();
        }

        log.debug("Checking whether CMIU " + miuId + " 2Rs config has changed");
        // creates config management row if missing
        this.configRepository.updateCmiuConfig(miuId, lastReportedConfig.getConfigSet(), oldConfigSet,
                true, "Reported by CMIU " + miuId + " over MQTT");

        // will not be null
        final CmiuConfigSetManagement newConfigSetManagement = configRepository.getCmiuConfigMgmt(miuId);

        if (newConfigSetManagement.isReportedConfigMismatchToCurrentConfig())
        {
            final String configMismatchMessage = buildConfigMismatchMessageJson(miuId,
                    newConfigSetManagement.getReportedConfig(), newConfigSetManagement.getCurrentConfig());
            awsQueueService.sendMessage(MdceIpcPublishService.ALERTS,
                    configMismatchMessage);
        }

    }

    /*
    To build:
     "{\"Source\":\"CMIU/" + lastReportedConfig.getMiuId() + "/Config/Mismatch\"," +
 "\"Level\":\"Error\"," +
 "\"Message\":\" Mismatch config. Expected: " + configMgmt.getCurrentConfig().getConfigParametersDescription() +
 ". Reported: " + lastReportedConfig.getConfigParametersDescription() +
 "\"}");
     */
    private String buildConfigMismatchMessageJson(MiuId miuId, ConfigSet lastReportedConfig, ConfigSet currentConfig)
    {
        final ObjectMapper jackson = new ObjectMapper();
        final JsonAlertMessage message = new JsonAlertMessage();
        final AlertSource alertSource = AlertSource.cmiuConfigMismatch(miuId.numericValue());
        message.setSource(alertSource.getAlertId());
        message.setLevel("Error");
        message.setMessage(currentConfig.getDifferencesSummary(lastReportedConfig, "assigned", "reported"));

        try
        {
            return jackson.writeValueAsString(message);
        }
        catch (JsonProcessingException e)
        {
            throw new RuntimeException("Unexpected JSON serialization error", e);
        }
    }

    /**
     * Function to process firmware revisions reported by a CMIU
     * It updates the database if any new firmware revisions are reported
     * It also checks for image update commands, and compares the new expected
     * image version to that reported by the CMIU. If all the reported images
     * match the expected the command is moved to completed. If some but not all
     * match then the command is moved to rejected (something has gone wrong)
     * @return true if the revision queue isn't empty
     */
    private boolean processRevisionUpdate()
    {
        final List<CmiuRevisionSet> cmiuRevisionUpdates = new ArrayList<>();
        miuRevisionUpdateQueue.drainTo(cmiuRevisionUpdates, MAX_MESSAGE_BATCH);

        for( CmiuRevisionSet reportedRevisions : cmiuRevisionUpdates)
        {
            CmiuRevisionSet previousRevisionSet = cmiuRepository.getCurrentRevisionSet(reportedRevisions.getMiuId());

            if(previousRevisionSet == null ||
                    areRevisionsNotNullAndNotEqual(reportedRevisions.getFirmwareRevision(), previousRevisionSet.getFirmwareRevision()) ||
                    areRevisionsNotNullAndNotEqual(reportedRevisions.getConfigRevision(), previousRevisionSet.getConfigRevision()) ||
                    areRevisionsNotNullAndNotEqual(reportedRevisions.getArbRevision(), previousRevisionSet.getArbRevision()) ||
                    areRevisionsNotNullAndNotEqual(reportedRevisions.getBleRevision(), previousRevisionSet.getBleRevision()) ||
                    areRevisionsNotNullAndNotEqual(reportedRevisions.getEncryptionRevision(), previousRevisionSet.getEncryptionRevision()) ||
                    areRevisionsNotNullAndNotEqual(reportedRevisions.getTelitFirmwareRevision(), previousRevisionSet.getTelitFirmwareRevision()))
            {
                replaceNullRevisionsWithPrevious(reportedRevisions, previousRevisionSet);
                cmiuRepository.addRevisionSet(reportedRevisions);
            }

            List<MiuCommandDetail> pendingCommands = miuCommandRepository.getPendingCommands(reportedRevisions.getMiuId());

            pendingCommands.stream()
                    .filter(miuCommand -> miuCommand.getCommandType() == CmdId.UpdateImage.getId())
                    .forEach(miuCommand -> {
                        //check whether the firmware version is the same
                        TaggedPacket tp = PacketParser.parseTaggedPacket(miuCommand.getCommandParams());

                        //get encrypted data
                        Map<Integer, TaggedData> tpMap = PacketParser.parseSecureTag(tp.findTag(SecureBlockArrayData.class));

                        //Check for each image type in the command, and compare to the reported image type
                        int imagesInCommandCount = 0;
                        int imagesMatchingCommandCount = 0;

                        ImageVersionInfoData commandedFirmwareRevision = (ImageVersionInfoData) tpMap.getOrDefault(
                                TagId.FirmwareRevision.getId(), null);

                        if(commandedFirmwareRevision != null)
                        {
                            imagesInCommandCount++;

                            if(reportedRevisions.getFirmwareRevision() != null &&
                                    reportedRevisions.getFirmwareRevision().
                                            equals(commandedFirmwareRevision.getVersion()))
                            {
                                imagesMatchingCommandCount++;
                            }
                        }


                        ImageVersionInfoData commandedConfigRevision = (ImageVersionInfoData) tpMap.getOrDefault(
                                TagId.ConfigRevision.getId(), null);

                        if(commandedConfigRevision != null)
                        {
                            imagesInCommandCount++;

                            if(reportedRevisions.getConfigRevision() != null &&
                                    reportedRevisions.getConfigRevision().
                                            equals(commandedConfigRevision.getVersion()))
                            {
                                imagesMatchingCommandCount++;
                            }
                        }


                        ImageVersionInfoData commandedArbRevision = (ImageVersionInfoData) tpMap.getOrDefault(
                                TagId.ArbConfigRevision.getId(), null);

                        if(commandedArbRevision != null)
                        {
                            imagesInCommandCount++;

                            if(reportedRevisions.getArbRevision() != null &&
                                    reportedRevisions.getArbRevision().
                                            equals(commandedArbRevision.getVersion()))
                            {
                                imagesMatchingCommandCount++;
                            }
                        }


                        ImageVersionInfoData commandedBleRevision = (ImageVersionInfoData) tpMap.getOrDefault(
                                TagId.BleConfigRevision.getId(), null);

                        if(commandedBleRevision != null)
                        {
                            imagesInCommandCount++;

                            if(reportedRevisions.getBleRevision() != null &&
                                    reportedRevisions.getBleRevision().
                                            equals(commandedBleRevision.getVersion()))
                            {
                                imagesMatchingCommandCount++;
                            }
                        }


                        ImageVersionInfoData commandedEncryptionRevision = (ImageVersionInfoData) tpMap.getOrDefault(
                                TagId.EncryptionConfigRevision.getId(), null);

                        if(commandedEncryptionRevision != null)
                        {
                            imagesInCommandCount++;

                            if(reportedRevisions.getEncryptionRevision() != null &&
                                    reportedRevisions.getEncryptionRevision().
                                            equals(commandedEncryptionRevision.getVersion()))
                            {
                                imagesMatchingCommandCount++;
                            }
                        }


                        if(imagesInCommandCount > 0)
                        {
                            //If all commanded images have been updated move command to completed
                            if(imagesInCommandCount == imagesMatchingCommandCount)
                            {
                                miuCommandRepository.updateMiuCommandsStateToCommit(miuCommand.getMiuId(),
                                        miuCommand.getCommandType());
                            }
                            else if(imagesMatchingCommandCount != 0) //If some commands have succeeded then move to rejected
                            {
                               miuCommandRepository.updateMiuCommandsStateToRejected(miuCommand.getMiuId(),
                                       miuCommand.getCommandType());
                            }
                        }
                    });
        }

        return !this.miuRevisionUpdateQueue.isEmpty();
    }

    /**
     * Validate current reported modem firmware version
     * @return false if not all items in the blocked queued has been processed
     */
    private boolean processModemFirmwareUpdates()
    {
        final List<CmiuModemFirmwareVersion> firmwareVersionUpdates = new ArrayList<>();
        miuModemFirmwareVersionUpdateQueue.drainTo(firmwareVersionUpdates, MAX_MESSAGE_BATCH);

        for(CmiuModemFirmwareVersion reportedFirmwareVersion : firmwareVersionUpdates)
        {
            //look at Pending commands and assumes Update Firmware commands with matching firmware version has been successful,
            //move the commands to completed bucket.

            List<MiuCommandDetail> pendingCommands = miuCommandRepository.getPendingCommands(reportedFirmwareVersion.getMiuId());

            pendingCommands.stream()
                .filter(miuCommand -> miuCommand.getCommandType() == CmdId.ModemFota.getId()) //filter to update image command only
                .forEach(miuCommand -> {
                    //check whether the firmware version is the same
                    TaggedPacket tp = PacketParser.parseTaggedPacket(miuCommand.getCommandParams());

                    //get encrypted data
                    Map<Integer, TaggedData> tpMap =
                            PacketParser.parseSecureTag(tp.findTag(SecureBlockArrayData.class));
                    CharArrayData queuedFirmwareVersion =
                            (CharArrayData) tpMap.getOrDefault(TagId.NewImageVersionInformation.getId(), null);

                    if (queuedFirmwareVersion != null &&
                            queuedFirmwareVersion.getAsString().equals(reportedFirmwareVersion.getImageVersion()))
                    {
                        int recordUpdated = miuCommandRepository.updateMiuCommandsStateToCommit(reportedFirmwareVersion.getMiuId(), CmdId.ModemFota.getId());
                        log.debug("Update firmware Command status for miu: {} changed to commit based on detailedConfig reports. Reported firmware version is {}, number of records updated: {}",
                                reportedFirmwareVersion.getMiuId().numericWrapperValue(), reportedFirmwareVersion.getImageVersion(), recordUpdated);
                    }
                });
        }

        return !this.miuModemFirmwareVersionUpdateQueue.isEmpty();
    }

    private void replaceNullRevisionsWithPrevious(CmiuRevisionSet reportedRev, CmiuRevisionSet previousRev)
    {
        if(previousRev != null)
        {
            if (reportedRev.getTelitFirmwareRevision() == null)
            {
                reportedRev.setTelitFirmwareRevision(previousRev.getTelitFirmwareRevision());
            }

            if (reportedRev.getEncryptionRevision() == null)
            {
                reportedRev.setEncryptionRevision(previousRev.getEncryptionRevision());
            }

            if (reportedRev.getArbRevision() == null)
            {
                reportedRev.setArbRevision(previousRev.getArbRevision());
            }

            if (reportedRev.getBleRevision() == null)
            {
                reportedRev.setBleRevision(previousRev.getBleRevision());
            }

            if (reportedRev.getConfigRevision() == null)
            {
                reportedRev.setConfigRevision(previousRev.getConfigRevision());
            }

            if (reportedRev.getFirmwareRevision() == null)
            {
                reportedRev.setFirmwareRevision(previousRev.getFirmwareRevision());
            }
        }
    }

    private boolean areRevisionsNotNullAndNotEqual(final String reportedRev, final String previousRev)
    {
        if(reportedRev == null)
        {
            return false;
        }
        else if(previousRev == null)
        {
            return true;
        }

        return !reportedRev.equals(previousRev);
    }

    private boolean processCmiuCommandResponseUpdates()
    {
        final List<CmiuCommandResponse> cmiuCommandResponseUpdates = new ArrayList<>();
        this.miuCommandResponseUpdateQueue.drainTo(cmiuCommandResponseUpdates, MAX_MESSAGE_BATCH);

        //validate cmiu command response from RDS with posted cmiu configuration
        for (CmiuCommandResponse response : cmiuCommandResponseUpdates)
        {
            if (response.getErrorId() == ErrorId.NoError.getId())
            {
                int recordUpdated;
                long commandType = response.getCommandType();

                if(commandType == CmdId.ModemFota.getId() ||
                        commandType == CmdId.UpdateImage.getId())
                {
                    recordUpdated = miuCommandRepository.updateMiuCommandsStateToAccepted(response.getMiuId(),
                            (int) commandType);
                }
                else
                {
                    recordUpdated = miuCommandRepository.updateMiuCommandsStateToCommit(response.getMiuId(),
                            (int) commandType);
                }

                if (recordUpdated < 1)
                {
                    //TODO: should we do something?
                    log.debug("No records updated");
                }
            }
            else
            {
                //Todo add some extra error handling, such as recording the error code returned
                log.debug("NACK Command response received");
                int recordUpdated = miuCommandRepository.updateMiuCommandsStateToRejected(response.getMiuId(),
                        (int) response.getCommandType());

                if (recordUpdated < 1)
                {
                    //TODO: should we do something?
                    log.debug("No records updated");
                }
            }
        }
        return !this.miuCommandResponseUpdateQueue.isEmpty();
    }


    public void scheduleForUpdate(MiuPacketDataAndStats queuedMiuDetail)
    {
        miuReportUpdateQueue.add(queuedMiuDetail);
    }

    /**
     * Add a new CMIU configuration data update to the queue for validation
     * @param cmiuConfigSet cmiuConfiguration data posted by a CMIU
     */
    public void scheduleForCmiuConfigurationValidation(CmiuConfigSetAssociation cmiuConfigSet)
    {
        miuConfigurationUpdateQueue.add(cmiuConfigSet);
    }

    /**
     * Add a new modem firmware version update to the queue for validation
     * @param cmiuModemFirmwareVersion the image version data posted by a CMIU
     */
    public void scheduleForModemFirmwareVersionValidation(CmiuModemFirmwareVersion cmiuModemFirmwareVersion)
    {
        miuModemFirmwareVersionUpdateQueue.add(cmiuModemFirmwareVersion);
    }

    /**
     * Add the CMIU firmware / config revisions to a queue for processing
     * @param cmiuRevisionSet
     */
    public void scheduleForRevisionVersionHistoryUpdate(CmiuRevisionSet cmiuRevisionSet)
    {
        miuRevisionUpdateQueue.add(cmiuRevisionSet);
    }

    /**
     * Add a new CMIU configuration data update to the queue for validation
     * @param commandResponse cmiuConfiguration data posted by a CMIU
     */
    public void scheduleForCmiuCommandResponseValidation(CmiuCommandResponse commandResponse)
    {
        miuCommandResponseUpdateQueue.add(commandResponse);
    }


    public void shutdown()
    {
        if (!this.shutdownFlag.get())
        {
            boolean queuesNotEmpty;

            this.shutdownFlag.set(true);
            do
            {
                log.info("Shutting down... queue sizes: {}", getCountsDisplay());

                try
                {
                    queuesNotEmpty = processAllQueues();
                } catch (Exception e)
                {
                    log.error("Error occurred during shutdownFlag.  Unprocessed messages may be lost: " + getCountsDisplay(), e);
                    break;
                }
            } while (queuesNotEmpty);
        }
    }

    public String getCountsDisplay()
    {
        return MessageFormat.format("reports {0}, config updates {1}, command response updates {2}, firmware version updates {3}",
                miuReportUpdateQueue.size(),
                miuConfigurationUpdateQueue.size(),
                miuCommandResponseUpdateQueue.size());
    }

}
