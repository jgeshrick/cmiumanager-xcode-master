/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.common;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.ImageVersionInfoData;

/**
 * Used to temporary store the miu and firmware version info from a detailConfig MQTT report update for validation.
 */
public class CmiuImageVersion
{
    private final MiuId miuId;
    private final ImageVersionInfoData imageVersion;
    private final TagId imageType;

    public CmiuImageVersion(MiuId miuId, ImageVersionInfoData imageVersion, TagId imageType)
    {
        this.miuId = miuId;
        this.imageVersion = imageVersion;
        this.imageType = imageType;
    }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public ImageVersionInfoData getImageVersion()
    {
        return imageVersion;
    }

    public TagId getImageType()
    {
        return imageType;
    }
}
