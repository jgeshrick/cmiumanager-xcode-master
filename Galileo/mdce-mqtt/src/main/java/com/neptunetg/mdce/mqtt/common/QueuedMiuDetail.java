/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.mqtt.common;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.tags.BatteryVoltage;
import com.neptunetg.common.packet.model.taggeddata.tags.CmiuDiagnosticsData;
import com.neptunetg.common.packet.model.taggeddata.tags.CmiuInformationData;
import com.neptunetg.common.packet.model.taggeddata.tags.CmiuPacketHeaderData;
import com.neptunetg.common.packet.model.taggeddata.tags.ConnectionTimingLogData;
import com.neptunetg.common.packet.model.taggeddata.tags.NetworkPerformanceData;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.tags.Temperature;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;

import java.time.Instant;

/**
 * Queuing of miu details to be updated to relational database.
 */
public class QueuedMiuDetail
{
    protected final MiuId miuId;
    protected final Instant lastHeardTime;
    protected final MiuPacketReceivedDynamoItem receivedDynamoItem;

    protected final boolean signalInfoAvailable;
    protected final double rssi;
    protected final double ber;

    protected final boolean rsrqAvailable;
    protected final double rsrq;

    protected final boolean processorResetCounterAvailable;
    protected final int processorResetCounter;

    protected final boolean systemInfoAvailable;
    protected final int magSwipeCounter;
    protected final int batteryCapacityMah; //battery voltage in mAh

    protected final boolean batteryVoltageAvailable;
    protected final double batteryVoltageMv;

    protected final boolean temperatureAvailable;
    protected final int temperatureDegC; //temperature in degC

    protected final boolean cellularTimingsAvailable;
    protected final int registerTimeMillis;
    protected final int registerTimeToActivateContextMillis;
    protected final int registerTimeConnectedMillis;
    protected final int registerTimeToTransferPacketMillis;
    protected final int disconnectTimeMillis;

    public QueuedMiuDetail(MiuId miuId, Instant currentTimestamp, MiuPacketReceivedDynamoItem receivedDynamoItem,
                           CmiuPacketHeaderData headerTag, TagSequence secureTags)
    {
        this.miuId = miuId;
        this.lastHeardTime = currentTimestamp;
        this.receivedDynamoItem = receivedDynamoItem;

        boolean signalInfoAvailable = false;
        double rssi = Double.NaN;
        double ber = Double.NaN;

        boolean rsrqAvailable = false;
        double rsrq = Double.NaN;

        boolean processorResetCounterAvailable = false;
        int processorResetCounter = Integer.MIN_VALUE;

        boolean deviceInfoAvailable = false;
        int magSwipeCounter = Integer.MIN_VALUE;
        int batteryCapacityMah = Integer.MIN_VALUE;

        boolean batteryVoltageAvailable = false;
        double batteryVoltageMv = Double.NaN;

        boolean temperatureAvailable = false;
        int temperatureDegC = Integer.MIN_VALUE;

        boolean cellularTimingsAvailable = false;
        int registerTimeMillis = Integer.MIN_VALUE;
        int registerTimeToActivateContextMillis = Integer.MIN_VALUE;
        int registerTimeConnectedMillis = Integer.MIN_VALUE;
        int registerTimeToTransferPacketMillis = Integer.MIN_VALUE;
        int disconnectTimeMillis = Integer.MIN_VALUE;

        if (headerTag != null)
        {
            final Integer rssiValue = headerTag.getRssiDbm();
            final Double berValue = headerTag.getBerPercentageMax();
            if (rssiValue != null && berValue != null)
            {
                signalInfoAvailable = true;
                rssi = rssiValue.doubleValue();
                ber = berValue.doubleValue();

                if (secureTags != null)
                {
                    final CmiuDiagnosticsData diagnostics = secureTags.findTag(TagId.CmiuHwDiagnostics, CmiuDiagnosticsData.class);
                    if (diagnostics != null)
                    {
                        processorResetCounterAvailable = true;
                        processorResetCounter = diagnostics.getProcessorResetCount();
                    }

                    final CmiuInformationData information = secureTags.findTag(TagId.CmiuInformation, CmiuInformationData.class);
                    if (information != null)
                    {
                        deviceInfoAvailable = true;
                        magSwipeCounter = information.getMagSwipeCounter();
                        batteryCapacityMah = information.getBatteryCapacityMah();
                    }

                    final ConnectionTimingLogData timings = secureTags.findTag(TagId.ConnectionTimingLog, ConnectionTimingLogData.class);
                    if (timings != null)
                    {
                        cellularTimingsAvailable = true;
                        registerTimeMillis = timings.getTimeFromPowerOnToNetworkRegistration();
                        registerTimeToActivateContextMillis = timings.getTimeFromNetworkRegistrationToContextActivation();
                        registerTimeConnectedMillis = timings.getTimeFromContextActivationToServerConnection();
                        registerTimeToTransferPacketMillis = timings.getTimeToTransferMessageToServer();
                        disconnectTimeMillis = timings.getTimeToDisconnectAndShutdown();
                    }

                    final BatteryVoltage batteryVoltage = secureTags.findTag(TagId.BatteryVoltage, BatteryVoltage.class);
                    if (batteryVoltage != null)
                    {
                        batteryVoltageAvailable = true;
                        batteryVoltageMv = batteryVoltage.getMillivolts();
                    }

                    final Temperature temperature = secureTags.findTag(TagId.Temperature, Temperature.class);

                    if (temperature != null)
                    {
                        temperatureAvailable = true;
                        temperatureDegC = temperature.getDegreesC();
                    }

                    final NetworkPerformanceData networkPerformanceData = secureTags.findTag(TagId.NetworkPerformance, NetworkPerformanceData.class);

                    if (networkPerformanceData != null)
                    {
                        rsrqAvailable = true;
                        rsrq = networkPerformanceData.getRsrq();
                    }
                }


            }
        }

        this.signalInfoAvailable = signalInfoAvailable;
        this.rssi = rssi;
        this.ber = ber;

        this.rsrqAvailable = rsrqAvailable;
        this.rsrq = rsrq;

        this.processorResetCounterAvailable = processorResetCounterAvailable;
        this.processorResetCounter = processorResetCounter;

        this.systemInfoAvailable = deviceInfoAvailable && processorResetCounterAvailable;
        this.magSwipeCounter = magSwipeCounter;
        this.batteryCapacityMah = batteryCapacityMah;

        this.batteryVoltageAvailable = batteryVoltageAvailable;
        this.batteryVoltageMv = batteryVoltageMv;

        this.temperatureAvailable = temperatureAvailable;
        this.temperatureDegC = temperatureDegC;

        this.cellularTimingsAvailable = cellularTimingsAvailable;
        this.registerTimeMillis = registerTimeMillis;
        this.registerTimeToActivateContextMillis = registerTimeToActivateContextMillis;
        this.registerTimeConnectedMillis = registerTimeConnectedMillis;
        this.registerTimeToTransferPacketMillis = registerTimeToTransferPacketMillis;
        this.disconnectTimeMillis = disconnectTimeMillis;
    }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public Instant getLastHeardTime()
    {
        return lastHeardTime;
    }

    public MiuPacketReceivedDynamoItem getReceivedDynamoItem()
    {
        return receivedDynamoItem;
    }


}
