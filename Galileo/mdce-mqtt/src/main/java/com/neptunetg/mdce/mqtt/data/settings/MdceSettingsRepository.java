/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.mqtt.data.settings;

/**
 * Stores modifiable MDCE settings that go in the database
 */
public interface MdceSettingsRepository
{
    String DELETE_TABLE_MIN_AGE_MONTHS_SETTING = "aws.dynamo.deletetable.min.age.months";

    long getIntSetting(String key, long defaultValue);

    String getTextSetting(String key, String defaultValue);


}
