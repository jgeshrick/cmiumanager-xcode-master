/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.mqtt.gui;

import com.neptunetg.mdce.mqtt.service.BrokerConnectAsMiuService;
import com.neptunetg.mdce.mqtt.service.BrokerConnectAsMdceService;
import com.neptunetg.mdce.mqtt.service.MqttSubscriptionManager;
import com.neptunetg.mdce.mqtt.service.subscription.BrokerStatusSubscription;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;
import java.util.TreeMap;

/**
 * Web page to display Mosquitto MQTT broker status.
 */
@Controller
@RequestMapping(value = "/pages/debug/broker")
public class BrokerStatusController
{
    @Autowired
    private BrokerStatusSubscription brokerStatusService;

    @Autowired
    private BrokerConnectAsMdceService primaryBroker;

    @Autowired
    private BrokerConnectAsMiuService allBrokers;

    @Autowired
    private MqttSubscriptionManager subscriptionManager;

    @RequestMapping(value="", method= {RequestMethod.GET})
    public String getBrokerStatus(Model model)
    {
        //sort topic
        Map<String, String> sortedBrokerStatus = new TreeMap<>(brokerStatusService.getBrokerStatus());
        model.addAttribute("isConnectedToMqttBroker", primaryBroker.isConnected());
        model.addAttribute("mqttClientId", primaryBroker.getClientId());
        model.addAttribute("mqttUrl", primaryBroker.getUrl());
        model.addAttribute("mqttDir", primaryBroker.getWorkingDir());

        model.addAttribute("lastUpdateTime", brokerStatusService.getLastUpdateTime());
        model.addAttribute("brokerStatus", sortedBrokerStatus);

        model.addAttribute("allBrokers", allBrokers.getUrls());
        return "brokerStatus";
    }

    @RequestMapping(value="", method= {RequestMethod.POST})
    public String reconnectMqttBroker(@RequestParam(value="reconnect", required = true) boolean reconnect) throws MqttException
    {
        if (reconnect)
        {
            subscriptionManager.startMdceMqtt();
        }
        return "redirect:" + "../";
    }


}
