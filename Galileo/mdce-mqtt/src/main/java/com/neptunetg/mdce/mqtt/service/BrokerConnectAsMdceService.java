/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.service;

import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.mdce.mqtt.service.subscription.MqttMessageHandler;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Service which connects to the brokers as the mdce-mqtt application.  This is used
 * to receive incoming MQTT messages from the primary broker and to send outgoing commands
 * to all brokers.
 */
@Service
public class BrokerConnectAsMdceService implements MqttCallback, ApplicationListener<ContextClosedEvent>
{
    private static final Logger log = LoggerFactory.getLogger(BrokerConnectAsMdceService.class);
    private final HashMap<String, MqttMessageHandler> subscriptionList = new HashMap<>();

    private final String primaryBrokerUrl;

    private final String[] nonPrimaryBrokerUrls;

    private final String clientId;
    private final String tmpDir;

    private IMqttClient primaryBrokerMqttClient;
    private IMqttClient[] nonPrimaryBrokerMqttClient;

    @Autowired
    private MdceIpcPublishService awsQueueService;

    @Autowired
    public BrokerConnectAsMdceService(@Qualifier("envProperties") Properties envProperties)
    {
        this.primaryBrokerUrl = envProperties.getProperty("mqtt.broker.primary.url");
        this.clientId = envProperties.getProperty("mqtt.clientId");
        this.tmpDir = envProperties.getProperty("mqtt.tmpDir", "");

        final int mqttBrokerCount;
        try
        {
            mqttBrokerCount = Integer.parseInt(envProperties.getProperty("mqtt.broker.count"));
        }
        catch (NumberFormatException e)
        {
            throw new RuntimeException("Could not parse value of mqtt.broker.count=" + envProperties.getProperty("mqtt.broker.count") + "from mdce-env.properties", e);
        }

        this.nonPrimaryBrokerUrls = new String[mqttBrokerCount - 1];
        this.nonPrimaryBrokerMqttClient = new IMqttClient[this.nonPrimaryBrokerUrls.length];

        int brokerIndex = 0;

        for (int brokerId = 0; brokerId < mqttBrokerCount; brokerId++)
        {
            final String brokerUrl = envProperties.getProperty("mqtt.broker." + brokerId + ".url");

            if (!this.primaryBrokerUrl.equals(brokerUrl))
            {
                if (brokerIndex >= nonPrimaryBrokerUrls.length)
                {
                    throw new RuntimeException("Primary broker URL " + primaryBrokerUrl + " not found in list of broker URLs!");
                }

                this.nonPrimaryBrokerUrls[brokerIndex++] = brokerUrl;
            }
        }
        log.info("Initialized with " + mqttBrokerCount + " MQTT brokers");
    }

    /**
     * Connect to an MQTT topic on a server
     */
    public synchronized void connect()
    {
        final AlertSource alertSource = AlertSource.MQTT_FAILED_CONNECTION;
        try
        {
            disconnect();
            log.debug("Connecting to primary MQTT broker: {} with client id {}", this.primaryBrokerUrl, this.clientId);

            final MqttClient primaryBrokerMqttClient = (this.tmpDir.isEmpty()) ?
                    new MqttClient(this.primaryBrokerUrl, this.clientId) :
                    new MqttClient(this.primaryBrokerUrl, this.clientId, new MqttDefaultFilePersistence(this.tmpDir));

            primaryBrokerMqttClient.setCallback(this);
            primaryBrokerMqttClient.setTimeToWait(5000);

            MqttConnectOptions connectOptions = new MqttConnectOptions();
            // both the client and server will maintain state across restarts of the client, the server and the connection
            connectOptions.setCleanSession(false);

            primaryBrokerMqttClient.connect(connectOptions);
            this.primaryBrokerMqttClient = primaryBrokerMqttClient;

            for (int nonPrimaryBrokerIndex = 0; nonPrimaryBrokerIndex < this.nonPrimaryBrokerUrls.length; nonPrimaryBrokerIndex++)
            {
                final String brokerUrl = this.nonPrimaryBrokerUrls[nonPrimaryBrokerIndex];
                log.debug("Connecting to non-primary MQTT broker: {} with client id {}", brokerUrl, this.clientId);

                final MqttClient nonPrimaryClient = (this.tmpDir.isEmpty()) ?
                        new MqttClient(brokerUrl, this.clientId) :
                        new MqttClient(brokerUrl, this.clientId, new MqttDefaultFilePersistence(this.tmpDir));


                nonPrimaryClient.connect(connectOptions);
                this.nonPrimaryBrokerMqttClient[nonPrimaryBrokerIndex] = nonPrimaryClient;
            }

            awsQueueService.sendMessage(MdceIpcPublishService.ALERTS,
                            "{\"Source\":\"" + alertSource.getAlertId() + "\"," +
                            "\"Level\":\"Okay\"," +
                            "\"Message\":\"MDCE-MQTT successfully connected to the broker\"}");

        }
        catch (MqttException e)
        {
            disconnect();
            log.error("Error connecting to mqtt broker", e);

            awsQueueService.sendMessage(MdceIpcPublishService.ALERTS,
                            "{\"Source\":\"" + alertSource.getAlertId() + "\"," +
                            "\"Level\":\"Error\"," +
                            "\"Message\":\"MDCE-MQTT failed to connect to broker, MQTT Exception\"}");
        }
        catch (Exception e)
        {
            disconnect();
            log.error("Error connecting to mqtt broker", e);

            awsQueueService.sendMessage(MdceIpcPublishService.ALERTS,
                            "{\"Source\":\"" + alertSource.getAlertId() + "\"," +
                            "\"Level\":\"Error\"," +
                            "\"Message\":\"MDCE-MQTT failed to connect to broker, Exception\"}");
        }
    }

    /**
     * Subscribe to a new topic on the primary broker.
     *
     * @param newTopic                     The new topic to subscribed to, can include + and * wildcard.
     */
    public void subscribe(String newTopic, int qos)
    {
        if (this.primaryBrokerMqttClient != null && this.isConnected()) //need to connect to broker before we can subscribe
        {
            try
            {
                log.info("Subscribing to topic: " + newTopic);
                this.primaryBrokerMqttClient.subscribe(newTopic, qos);
            }
            catch (MqttException e)
            {
                log.error("Exception while subscribing", e);
            }
        }
    }

    /**
     * Register the callback handler to a topic. Note this function needs to be called before calling the connect service,
     * else it is likely any messages that are already queued in mqtt broker will not be handled after this client has connect()
     * to the broker.
     *
     * @param topic                        the topic to subscribe
     * @param messageArriveCallbackHandler the message handler for the topic
     */
    public void registerCallback(String topic, MqttMessageHandler messageArriveCallbackHandler)
    {
        if (!this.subscriptionList.containsKey(topic))
        {
            this.subscriptionList.put(topic, messageArriveCallbackHandler);
        }
    }




    /**
     * Get the URL we are connected to
     *
     * @return URL we are connected to, or null if not connected
     */
    public String getUrl()
    {
        return this.primaryBrokerUrl;
    }

    /**
     * Get this application's client ID
     *
     * @return The client ID, or null if not connected
     */
    public String getClientId()
    {
        return clientId;
    }

    /**
     * Get the working directory used for Paho Mqtt client.
     *
     * @return working directory for Mqtt client
     */
    public String getWorkingDir()
    {
        return tmpDir;
    }

    /**
     * Are we connected?
     *
     * @return true if connected, false otherwise
     */
    public boolean isConnected()
    {
        return this.primaryBrokerMqttClient != null && this.primaryBrokerMqttClient.isConnected();
    }

    /**
     * disconnect MQTT
     */
    public synchronized void disconnect()
    {
        try
        {
            if (this.primaryBrokerMqttClient != null)
            {
                log.debug("Disconnecting from " + this.primaryBrokerMqttClient);
                this.primaryBrokerMqttClient.disconnect();
            }
        }
        catch (MqttException e)
        {
            log.error("Cannot disconnect from broker", e);
        }
        finally
        {
            this.primaryBrokerMqttClient = null;
        }
        for (int nonPrimaryBrokerIndex = 0; nonPrimaryBrokerIndex < this.nonPrimaryBrokerUrls.length; nonPrimaryBrokerIndex++)
        {
            try
            {
                if (this.nonPrimaryBrokerMqttClient[nonPrimaryBrokerIndex] != null)
                {
                    log.debug("Disconnecting from " + this.nonPrimaryBrokerMqttClient[nonPrimaryBrokerIndex]);
                    this.nonPrimaryBrokerMqttClient[nonPrimaryBrokerIndex].disconnect();
                }
            }
            catch (MqttException e)
            {
                log.error("Cannot disconnect from broker", e);
            }
            finally
            {
                this.nonPrimaryBrokerMqttClient[nonPrimaryBrokerIndex] = null;
            }
        }
    }

    /**
     * Publish a data to a topic over MQTT with the specified qos
     *
     * @param topic    The topic to publish
     * @param message  The message to publish as the data payload
     * @param qos      Quality of service for the MQTT, 0, 1, or 2.
     * @param retained Retained message?  This causes the broker to store the last message and the corresponding QoS for that topic. Each client that
     *                 subscribes to a topic pattern, which matches the topic of the retained message, will receive the message
     *                 immediately after subscribing.
     */
    public void publishToPrimaryBroker(String topic, byte[] message, int qos, boolean retained) throws MqttException
    {
        if (this.primaryBrokerMqttClient != null && this.primaryBrokerMqttClient.isConnected())
        {
            MqttMessage newMessage = new MqttMessage(message);
            newMessage.setQos(qos);
            newMessage.setRetained(retained);
            this.primaryBrokerMqttClient.publish(topic, newMessage);
        }
    }

    /**
     * Publish a data to a topic over to all brokers with the specified qos
     *
     * @param topic    The topic to publish
     * @param message  The message to publish as the data payload
     * @param qos      Quality of service for the MQTT, 0, 1, or 2.
     * @param retained Retained message?  This causes the broker to store the last message and the corresponding QoS for that topic. Each client that
     *                 subscribes to a topic pattern, which matches the topic of the retained message, will receive the message
     *                 immediately after subscribing.
     */
    public void publishToAllBrokers(String topic, byte[] message, int qos, boolean retained) throws MqttException
    {
        publishToPrimaryBroker(topic, message, qos, retained);
        for (int nonPrimaryBrokerIndex = 0; nonPrimaryBrokerIndex < this.nonPrimaryBrokerMqttClient.length; nonPrimaryBrokerIndex++)
        {
            final IMqttClient mqttClient = this.nonPrimaryBrokerMqttClient[nonPrimaryBrokerIndex];
            if (mqttClient.isConnected())
            {
                log.debug("Publishing to non primary broker [{}]", nonPrimaryBrokerIndex);
                MqttMessage newMessage = new MqttMessage(message);
                newMessage.setQos(qos);
                newMessage.setRetained(retained);
                mqttClient.publish(topic, newMessage);
            }

        }
    }

    /**
     * Process all message arrivals - examines the message topic and call the relevant subscription service message handler
     * which has matching topic.
     * Although we could subscribe to a wildcard topic, every topic arrival has the full topic pathname.
     * This function matches the full pathname of incoming topic to that of the subscriber (could be wildcard) in order
     * to find the correct subscriber to handler the incoming message.
     *
     * @param topic   The topic for the arrival message
     * @param message The message payload
     */
    private void processMessage(String topic, MqttMessage message) throws IOException, MqttException
    {
        for (Map.Entry<String, MqttMessageHandler> entry : this.subscriptionList.entrySet())
        {
            String k = entry.getKey();

            if (matchesSubscription(k, topic))
            {
                MqttMessageHandler handler = entry.getValue();
                handler.processMessage(topic, message);
            }
        }
    }

    /**
     * Matches incoming topics to subscribed topics containing  wildcards
     *
     * @param subscriptionKey the subscribed topic, including wildcards
     * @param topic           the topic that has been received
     * @return true if matches
     */
    public static boolean matchesSubscription(String subscriptionKey, String topic)
    {
        String regex = subscriptionKey.replace("$", "\\$")  //escape topic containing $
                .replaceAll("\\+", "[a-zA-Z0-9_]\\+")       //include single hierarchy wildcard "+"
                .replaceAll("#", "[a-zA-Z0-9_\\/]\\+");     //include multiple hierarchy wildcard "#"
        return topic.matches(regex);
    }


    /**
     * Handler for connection lost.  Just logs.
     */
    @Override
    public void connectionLost(Throwable e)
    {
        log.error("Connection lost!", e);

        final AlertSource alertSource = AlertSource.MQTT_FAILED_CONNECTION;

        awsQueueService.sendMessage(MdceIpcPublishService.ALERTS,
                            "{\"Source\":\"" + alertSource.getAlertId() + "\"," +
                            "\"Level\":\"Warning\"," +
                            "\"Message\":\"Connection to MQTT broker lost\"}");
        //TODO: We might need to clean up and reconnect to broker!
    }


    /**
     * Implements MqttCallback interface. Handler called by MQTT broker when message delivery has completed.
     * Do nothing now. Just log to file.
     */
    @Override
    public void deliveryComplete(IMqttDeliveryToken arg0)
    {
        if (log.isTraceEnabled())
        {
            final StringBuilder message = new StringBuilder("deliveryComplete for message with ID ");
            message.append(arg0.getMessageId());
            final String[] topics = arg0.getTopics();
            if (topics.length == 1)
            {
                message.append(" on topic ").append(topics[0]);
            }
            else
            {
                message.append(" on topics ").append(topics[0]);
                for (int i = 1; i < topics.length; i++)
                {
                    message.append(", ").append(topics[i]);
                }
            }
            log.trace(message.toString());
        }
    }

    /**
     * Implements MqttCallback interface. Handler called by MQTT broker when any MQTT message arrived.
     * Parses message and puts it into the repository.
     */
    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception
    {
        try
        {
            byte[] payload = message.getPayload();

            if (log.isTraceEnabled())
            {
                log.trace("message arrived on topic: " + topic +
                        ", Qos: " + message.getQos() +
                        ", Message length (bytes): " + payload.length);

            }

            processMessage(topic, message);
        }
        catch (Exception e)
        {
            log.error("Exception occurred in mqttCallbackImpl-messageArrive:", e);
        }
        catch (Throwable t)
        {
            log.error("Throwable mqttCallbackImpl Exception:", t);
        }

    }


    @Override
    public void onApplicationEvent(ContextClosedEvent contextClosedEvent)
    {
        disconnect(); //Paho starts non-daemon background threads.  Important to disconnect on close.
    }
}
