/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.data.miu;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.util.JdbcDateUtil;
import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.tags.CmiuPacketHeaderData;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import com.neptunetg.mdce.mqtt.common.QueuedMiuDetail;
import org.springframework.jdbc.core.PreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.time.Instant;

/**
 * Enables data from MIU packets to be inserted into DB
 */
public class MiuPacketDataAndStats extends QueuedMiuDetail implements PreparedStatementSetter
{

    public MiuPacketDataAndStats(MiuId miuId, Instant currentTimestamp, MiuPacketReceivedDynamoItem receivedDynamoItem, CmiuPacketHeaderData headerTag, TagSequence secureTags)
    {
        super(miuId, currentTimestamp, receivedDynamoItem, headerTag, secureTags);
    }

    @Override
    public void setValues(PreparedStatement s) throws SQLException
    {
        s.setLong(1, miuId.numericValue());
        s.setObject(2, JdbcDateUtil.bindDate(lastHeardTime == null ? Instant.now() : lastHeardTime)); //shouldn't be null

        s.setInt(3, 1); //1 packet

        if (signalInfoAvailable)
        {
            s.setDouble(4, rssi);
            s.setDouble(5, rssi);
            s.setDouble(6, rssi);
            s.setDouble(7, ber);
        }
        else
        {
            s.setNull(4, Types.DOUBLE);
            s.setNull(5, Types.DOUBLE);
            s.setNull(6, Types.DOUBLE);
            s.setNull(7, Types.DOUBLE);
        }

        if (rsrqAvailable)
        {
            s.setDouble(8, rsrq);
        }
        else
        {
            s.setNull(8, Types.DOUBLE);
        }

        if (processorResetCounterAvailable)
        {
            s.setInt(9, processorResetCounter);
        }
        else
        {
            s.setNull(9, Types.INTEGER);
        }

        if (cellularTimingsAvailable)
        {
            s.setInt(10, registerTimeMillis);
            s.setInt(11, registerTimeToActivateContextMillis);
            s.setInt(12, registerTimeConnectedMillis);
            s.setInt(13, registerTimeToTransferPacketMillis);
            s.setInt(14, disconnectTimeMillis);
        }
        else
        {
            s.setNull(10, Types.INTEGER);
            s.setNull(11, Types.INTEGER);
            s.setNull(12, Types.INTEGER);
            s.setNull(13, Types.INTEGER);
            s.setNull(14, Types.INTEGER);
        }

        if (systemInfoAvailable)
        {
            s.setInt(15, magSwipeCounter);
            s.setDouble(16, batteryCapacityMah);
        }
        else
        {
            s.setNull(15, Types.INTEGER);
            s.setNull(16, Types.DOUBLE);
        }

        if (batteryVoltageAvailable)
        {
            s.setDouble(17, batteryVoltageMv);
        }
        else
        {
            s.setNull(17, Types.DOUBLE);
        }

        if (temperatureAvailable)
        {
            s.setDouble(18, temperatureDegC);
        }
        else
        {
            s.setNull(18, Types.DOUBLE);
        }

        s.setNull(19, Types.DOUBLE);
        s.setNull(20, Types.INTEGER);
        s.setNull(21, Types.DOUBLE);
        s.setNull(22, Types.DOUBLE);
    }
}
