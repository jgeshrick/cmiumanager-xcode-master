/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.service;


import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Listens for SQS message that outgoing messages for a MIU need to be cleared down.  These are
 * sent for cancelled scheduled tasks when another mdce-mqtt is shutting down
 */
@Service
public class MiuClearCommandsMonitor implements AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(MiuClearCommandsMonitor.class);

    private final BrokerConnectAsMiuService allBrokers;

    @Autowired
    public MiuClearCommandsMonitor(MdceIpcSubscribeService awsQueueService, BrokerConnectAsMiuService allBrokers)
    {
        this.allBrokers = allBrokers;
        awsQueueService.register(this, MdceIpcSubscribeService.CLEAR_MIU_MESSAGES, Duration.ofSeconds(1L), 1);
    }

    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        log.trace("SQS message received: {} - {}", messageId, message);
        String[] splitStrings = message.split(",");

        final String miuIdStr = splitStrings[0].trim();
        final String miuTypeStr = splitStrings[1].trim();

        allBrokers.clearAllOutgoingMessages(MiuId.fromString(miuIdStr),
                MiuType.fromString(miuTypeStr),
                Instant.now().plus(BrokerConnectAsMiuService.CLEAR_ALL_OUTGOING_MESSAGES_DELAY));
        return true;
    }
}
