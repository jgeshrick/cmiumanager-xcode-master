/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.mqtt.service.subscription;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Handle Mosquitto Broker status publication and retains status data.
 */
@Service
public class BrokerStatusSubscription implements MqttMessageHandler
{
    private static final String TOPIC = "$SYS/#";

    private static final Logger log = LoggerFactory.getLogger(BrokerStatusSubscription.class);
    private final Map<String, String> brokerStatus = new ConcurrentHashMap<>();
    private volatile Instant lastUpdateTime;

    private final AtomicBoolean shutdownFlag = new AtomicBoolean(false);

    public Map<String, String> getBrokerStatus()
    {
        return brokerStatus;
    }


    @Override
    public void processMessage(String topic, MqttMessage message)
    {
        if (!this.shutdownFlag.get())
        {
            lastUpdateTime = Instant.now();
            brokerStatus.put(topic, message.toString());

            if (log.isTraceEnabled())
            {
                log.trace("TOPIC: " + topic + ". Message: " + message);
            }
        }
    }

    public String getTopic()
    {
        return TOPIC;
    }

    /**
     * Getter for last time when a message is received
     * @return
     */
    public Instant getLastUpdateTime()
    {
        return lastUpdateTime;
    }

    @Override
    public void shutdown()
    {
        if (!this.shutdownFlag.get())
        {
            this.shutdownFlag.set(true);
        }
    }
}
