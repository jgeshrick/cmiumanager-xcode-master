/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.common;

import com.neptunetg.common.data.miu.MiuId;

/**
 * Used to temporary store the miu and modem firmware version info from a detailConfig MQTT report update for validation.
 */
public class CmiuModemFirmwareVersion
{
    private final MiuId miuId;
    private final String imageVersion;

    public CmiuModemFirmwareVersion(MiuId miuId, String imageVersion)
    {
        this.miuId = miuId;
        this.imageVersion = imageVersion;
    }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public String getImageVersion()
    {
        return imageVersion;
    }

}
