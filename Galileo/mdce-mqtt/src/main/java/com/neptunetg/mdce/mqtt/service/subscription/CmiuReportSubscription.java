/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.service.subscription;

import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.cellular.CmiuConfigSetAssociation;
import com.neptunetg.common.data.miu.cellular.CmiuRevisionSet;
import com.neptunetg.common.data.miu.cellular.ConfigSet;
import com.neptunetg.common.data.miu.heard.MiuUpdateRepository;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.packet.model.PacketInstigatorId;
import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.*;
import com.neptunetg.common.packet.model.taggeddata.types.CharArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.ImageVersionInfoData;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerData;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.common.packet.parser.TagSequenceParser;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleService;
import com.neptunetg.mdce.common.data.model.DynamoItemPacketType;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import com.neptunetg.mdce.mqtt.common.CmiuCommandResponse;
import com.neptunetg.mdce.mqtt.common.CmiuModemFirmwareVersion;
import com.neptunetg.mdce.mqtt.data.miu.MiuPacketDataAndStats;
import com.neptunetg.mdce.mqtt.service.BrokerConnectAsMiuService;
import com.neptunetg.mdce.mqtt.service.MiuDetailsUpdater;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.time.Instant;

/**
 * Subscription to CMIU report topic to receive report publication from CMIU.
 */
@Service
public class CmiuReportSubscription implements MqttMessageHandler
{
    private static final String TOPIC = "CMIU/Report/#";    //subscribe to all CMIUs report

    private static final Logger log = LoggerFactory.getLogger(CmiuReportSubscription.class);

    private final BrokerConnectAsMiuService allBrokers;

    private final MiuDetailsUpdater miuDetailsUpdater;

    private final TagSequenceParser tagSequenceParser;

    private final MiuLifecycleService miuLifecycleService;

    private final MdceIpcPublishService awsQueueService;

    @Autowired
    public CmiuReportSubscription(BrokerConnectAsMiuService allBrokers,
                                  MiuDetailsUpdater miuDetailsUpdater,
                                  MiuLifecycleService cmiuLifecycleService,
                                  MdceIpcPublishService awsQueueService)
    {
        this.allBrokers = allBrokers;
        this.miuDetailsUpdater = miuDetailsUpdater;
        this.tagSequenceParser = new TagSequenceParser();
        this.miuLifecycleService = cmiuLifecycleService;
        this.awsQueueService = awsQueueService;
    }

    /**
     * Handle Meter incoming report subscription
     * @param topic The topic received.
     * @param message message contained in this topic.
     */
    @Override
    public void processMessage(String topic, MqttMessage message)
    {
        log.debug("TOPIC: " + topic + ". Message: " + DatatypeConverter.printHexBinary(message.getPayload()));

        final int lastSlashPos = topic.lastIndexOf('/');
        int cmiuId = -1;
        if (lastSlashPos > 0)
        {
            try
            {
                cmiuId = Integer.parseInt(topic.substring(lastSlashPos + 1));
            }
            catch (Exception e)
            {
                log.warn("Can't guess CMIU ID from topic " + topic, e);
            }
        }

        processCmiuMessage(cmiuId, message.getPayload());
    }

    private void processCmiuMessage(final int cmiuIdFromTopic, final byte[] cmiuMessagePayload)
    {
        final Instant currentTime = Instant.now();
        MiuId cmiuId = MiuId.valueOf(cmiuIdFromTopic);
        int packetTypeId = -1;

        if (cmiuMessagePayload.length > 0)
        {
            packetTypeId = cmiuMessagePayload[0] & 0xff;
        }

        long dateBuiltEpochMillis = 0L;

        TaggedPacket packet = null;
        TagSequence secureTags = null;
        CmiuPacketHeaderData packetHeader = null;

        try //try parse
        {
            packet = PacketParser.parseTaggedPacket(cmiuMessagePayload);

            final TaggedDataPacketType incomingPacketType = packet.getPacketType();

            //get header
            packetHeader = packet.findTag(CmiuPacketHeaderData.class);

            //get encrypted data
            final SecureBlockArrayData secBlock = packet.findTag(SecureBlockArrayData.class);

            if (secBlock != null)
            {
                secureTags = tagSequenceParser.parseTagSequence(secBlock.getEncryptedData());
            }

            //find packet type
            packetTypeId = packet.getPacketId();

            if (packetHeader != null)
            {
                final int cmiuIdFromPacketHeader = packetHeader.getCmiuId();

                if (cmiuIdFromPacketHeader != cmiuId.numericValue())
                {
                    cmiuId = MiuId.valueOf(cmiuIdFromPacketHeader);
                    log.warn("Received packet with header indicating CMIU ID " + cmiuIdFromPacketHeader + " on topic for CMIU " + cmiuIdFromTopic); //TODO: alert
                }

                dateBuiltEpochMillis = packetHeader.getCurrentTimeAndDate().toEpochMilli();
            }
        }
        catch (Exception e)
        {
            log.error("Exception while parsing packet from CMIU " + cmiuIdFromTopic + ".  Packet has been scheduled for insertion but not otherwise processed.", e); //TODO: alert
        }

        final DynamoItemPacketType packetType = DynamoItemPacketType.fromSystemAndId(packetTypeId, DynamoItemPacketType.SYSTEM_CMIU);

        //schedule for updating last heard time and add packet to dynamo
        final MiuPacketReceivedDynamoItem miuPacket = new MiuPacketReceivedDynamoItem();
        miuPacket.setMiuId(cmiuId.numericValue());
        miuPacket.setPacketData(cmiuMessagePayload);
        miuPacket.setPacketType(packetType.toString());
        miuPacket.setDateBuilt(dateBuiltEpochMillis);
        miuPacket.setInsertDate(currentTime.toEpochMilli());
        miuPacket.setSourceDeviceType("CMIU");
        miuPacket.setTargetDeviceType("MDCE");

        miuDetailsUpdater.scheduleForUpdate(new MiuPacketDataAndStats(
                cmiuId, currentTime, miuPacket, packetHeader, secureTags
        ));

        if (secureTags != null) //parsed successfully
        {
            if (packetTypeId == TaggedDataPacketType.CommandConfigurationResponse.getId())
            {
                processCommandConfigurationResponsePacket(cmiuId, secureTags, currentTime);

            }
            else if (packetTypeId == TaggedDataPacketType.BasicConfigurationStatus.getId())
            {
                processCmiuConfigurationStatusPacket(cmiuId, packetTypeId, secureTags);
            }
            else if (packetTypeId == TaggedDataPacketType.DetailedConfigurationStatus.getId())
            {
                processCmiuConfigurationStatusPacket(cmiuId, packetTypeId, secureTags);
                processCmiuReportedNetworkInfo(cmiuId, secureTags);
            }


            checkForPacketInstigator(cmiuId, secureTags);
        }
    }

    /**
     * A function to check for the packet instigator tag, and change a CMIUs lifecycle state if appropriate
     * @param cmiuId
     * @param secureTags secure tags from a packet
     */
    private void checkForPacketInstigator(MiuId cmiuId, TagSequence secureTags)
    {
        IntegerData packetInstigatorTag = secureTags.findTag(TagId.PacketInstigator, IntegerData.class);

        if(packetInstigatorTag != null)
        {
            if(packetInstigatorTag.getValue() == PacketInstigatorId.CVS.getId())
            {
                miuLifecycleService.setMiuLifecycleState(cmiuId, MiuLifecycleStateEnum.HEARD);
            }
            else if(packetInstigatorTag.getValue() == PacketInstigatorId.FFTS.getId())
            {
                miuLifecycleService.setMiuLifecycleState(cmiuId, MiuLifecycleStateEnum.CONFIRMED);
            }
        }
    }


    /**
     If it is a command response packet, check the DB to see whether this is a response to an outgoing command,
     and close the command by updating the command lifecycle status.
     parse incoming packets to close the command response loop
     * @param cmiuId
     * @param secureTags secure tags from a CommandConfigurationResponsePacket
     * @param currentTime Current time for processing
     */
    private void processCommandConfigurationResponsePacket(MiuId cmiuId, TagSequence secureTags, Instant currentTime)
    {
        final IntegerData commandDataTag = secureTags.findTag(TagId.Command, IntegerData.class);
        final IntegerData errorTag = secureTags.findTag(TagId.Error, IntegerData.class);

        miuDetailsUpdater.scheduleForCmiuCommandResponseValidation(
                new CmiuCommandResponse(cmiuId,
                        commandDataTag.getValue(),
                        errorTag.getValue())
        );

        //clear MQTT topic from ALL brokers, assuming CMIU that connects to a specific broker would have already
        //receive all the queued messages
        allBrokers.clearAllOutgoingMessages(cmiuId, MiuType.CMIU,
                currentTime.plus(BrokerConnectAsMiuService.CLEAR_ALL_OUTGOING_MESSAGES_DELAY));
    }


    /**
     //If it is a configuration tag, check if there is a pending config change, update the config table as well as the command table.
     //CmiuConfigurationData AND IntervalRecordingConfigurationData should be present in either DetailConfigPacket or BasicConfigPacket
     //use these two tags to validate report Cmiu config with current cmiu config
     * @param cmiuId
     * @param secureTags
     */
    private void processCmiuConfigurationStatusPacket(MiuId cmiuId, int packetTypeId, TagSequence secureTags)
    {
        final RecordingAndReportingIntervalData r2Config = secureTags.findTag(TagId.RecordingandReportingInterval, RecordingAndReportingIntervalData.class);
        if (r2Config != null)
        {
            final ConfigSet configSet = new ConfigSet();
            configSet.setReportingNumberOfRetries(r2Config.getReportingNumberOfRetries());
            configSet.setReportingIntervalMins(r2Config.getReportingIntervalHours() * 60);
            configSet.setReportingTransmitWindowMins(r2Config.getReportingTransmitWindowsMins());
            configSet.setReportingStartMins(r2Config.getRecordingStartTimeMins());
            configSet.setReportingQuietStartMins(r2Config.getReportingQuietStartMins());
            configSet.setReportingQuietEndMins(r2Config.getReportingQuietEndMins());

            configSet.setRecordingStartTimeMins(r2Config.getRecordingStartTimeMins());
            configSet.setRecordingIntervalMins(r2Config.getRecordingIntervalMins());

            final CmiuConfigSetAssociation association = new CmiuConfigSetAssociation(cmiuId, 0, null, configSet); //site id and billable ignored
            miuDetailsUpdater.scheduleForCmiuConfigurationValidation(association);
        }
        else
        {
            final CmiuBasicConfigurationData cmiuConfig = secureTags.findTag(TagId.CmiuConfiguration, CmiuBasicConfigurationData.class);

            final IntervalRecordingConfigurationData recordingConfig = secureTags.findTag(TagId.IntervalRecording, IntervalRecordingConfigurationData.class);

            if (cmiuConfig != null && recordingConfig != null)
            {
                final ConfigSet configSet = new ConfigSet();
                configSet.setReportingNumberOfRetries(cmiuConfig.getReportingWindowNumRetries());
                configSet.setReportingIntervalMins(cmiuConfig.getReportingIntervalHours() * 60);
                configSet.setReportingTransmitWindowMins(cmiuConfig.getReportingWindow());
                configSet.setReportingStartMins(cmiuConfig.getReportingStartTime());
                configSet.setReportingQuietStartMins(cmiuConfig.getQuietTimeStartMinutes());
                configSet.setReportingQuietEndMins(cmiuConfig.getQuietTimeEndMinutes());

                configSet.setRecordingStartTimeMins(recordingConfig.getStartTimeOfRecordingInterval());
                configSet.setRecordingIntervalMins(recordingConfig.getDeviceRecordingInterval());

                final CmiuConfigSetAssociation association = new CmiuConfigSetAssociation(cmiuId, 0, null, configSet); //site id and billable ignored

                awsQueueService.sendMessage(MdceIpcPublishService.CHECK_CMIU_MODE,
                        cmiuId.toString() + "," + configSet.getRecordingIntervalMins() + "," + configSet.getReportingIntervalMins());

                miuDetailsUpdater.scheduleForCmiuConfigurationValidation(association);
            }
            else
            {
                log.error("Cannot identify config set, as an expected tag is missing: require " + TagId.RecordingandReportingInterval + " or both " +
                        TagId.CmiuConfiguration + " (" + (cmiuConfig == null ? "absent" : "present") +
                        ") and " + TagId.IntervalRecording + " (" + (recordingConfig == null ? "absent" : "present") +
                        ").  Affects packet from CMIU " + cmiuId + ", packet type " + packetTypeId);
            }
        }
        //Check firmware version info if it is a detail config packet
        if(packetTypeId == TaggedDataPacketType.DetailedConfigurationStatus.getId())
        {
            CmiuRevisionSet cmiuRevisionSet = new CmiuRevisionSet();
            cmiuRevisionSet.setMiuId(cmiuId);
            cmiuRevisionSet.setTimestamp(Instant.now());

            final ImageVersionInfoData firmwareRevision = secureTags.findTag(TagId.FirmwareRevision, ImageVersionInfoData.class);

            if(firmwareRevision != null)
            {
                cmiuRevisionSet.setFirmwareRevision(firmwareRevision.toString());
            }
            else
            {
                log.error("Cannot retrieve firmware version info from  basic configuration tag and interval recording configuration tag from BasicConfiguration packet or DetailedConfiguration packet");
            }

            final ImageVersionInfoData configRevision = secureTags.findTag(TagId.ConfigRevision, ImageVersionInfoData.class);

            if(configRevision != null)
            {
                cmiuRevisionSet.setConfigRevision(configRevision.toString());
            }

            final ImageVersionInfoData arbRevision = secureTags.findTag(TagId.ArbConfigRevision, ImageVersionInfoData.class);

            if(arbRevision != null)
            {
                cmiuRevisionSet.setArbRevision(arbRevision.toString());
            }

            final ImageVersionInfoData bleRevision = secureTags.findTag(TagId.BleConfigRevision, ImageVersionInfoData.class);

            if(bleRevision != null)
            {
                cmiuRevisionSet.setBleRevision(bleRevision.toString());
            }

            final ImageVersionInfoData encryptionRevision = secureTags.findTag(TagId.EncryptionConfigRevision, ImageVersionInfoData.class);

            if(encryptionRevision != null)
            {
                cmiuRevisionSet.setEncryptionRevision(encryptionRevision.toString());
            }

            final CharArrayData telitFirmwareRevision = secureTags.findTag(TagId.ModemSoftwareRevisionNumber, CharArrayData.class);

            if(telitFirmwareRevision != null)
            {
                cmiuRevisionSet.setTelitFirmwareRevision(telitFirmwareRevision.toString());
                miuDetailsUpdater.scheduleForModemFirmwareVersionValidation(
                        new CmiuModemFirmwareVersion(cmiuId, telitFirmwareRevision.getAsString()));
            }

            miuDetailsUpdater.scheduleForRevisionVersionHistoryUpdate(cmiuRevisionSet);
        }
    }

    public void processCmiuReportedNetworkInfo(MiuId cmiuId, TagSequence secureTags)
    {
        final SimImsiData imsi = secureTags.findTag(TagId.SimImsi, SimImsiData.class);
        final SimCardIdData simId = secureTags.findTag(TagId.SimCardId, SimCardIdData.class);
        final ModemSerialNumberData imei = secureTags.findTag(TagId.ModemSerialNumber, ModemSerialNumberData.class);

        if(imsi != null && simId != null && imei != null)
        {
            awsQueueService.sendMessage(MdceIpcPublishService.CMIU_CHECK_REPORTED_SIM_AND_MODEM_DETAILS,
                    cmiuId.toString() + "," + imsi.getAsString() + "," + simId.getAsString() + "," + imei.getAsString());
        }
    }


    @Override
    public String getTopic()
    {
        return TOPIC;
    }


    @Override
    public void shutdown()
    {
        miuDetailsUpdater.shutdown();
    }
}
