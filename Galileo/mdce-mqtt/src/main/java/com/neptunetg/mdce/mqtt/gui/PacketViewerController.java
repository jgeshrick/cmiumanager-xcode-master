/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.mqtt.gui;

import com.amazonaws.services.dynamodbv2.document.Table;
import com.neptunetg.mdce.common.data.PacketRepository;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.xml.bind.DatatypeConverter;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Provide utility to view received packet stored in dynamoDb.
 */
@Controller
@RequestMapping(value="/pages/debug/packet")
public class PacketViewerController
{
    @Autowired
    private PacketRepository packetRepository;

    @RequestMapping(value="{miuId}", method= {RequestMethod.GET})
    public String getReceivedPacket(Model model, @PathVariable("miuId") int miuId)
    {
        final Instant now = Instant.now();
        final Instant thirtyOneDaysAgo = now.minus(31L, ChronoUnit.DAYS);

        final List<Table> tables = packetRepository.getRelevantTablesForMiuPacketQuery(thirtyOneDaysAgo, now);

        final Stream<MiuPacketReceivedDynamoItem> receivedPackets =
                packetRepository.streamMiuPacketsInsertedInDateRange(miuId, thirtyOneDaysAgo, now, tables);

        final List<MiuPacketViewModel> packetViewModels = receivedPackets.map(p -> new MiuPacketViewModel(p)).collect(Collectors.toList());

        model.addAttribute("miuId", miuId);
        model.addAttribute("packetViewModels", packetViewModels);
        return "packetList";
    }

    public static class MiuPacketViewModel
    {
        private final MiuPacketReceivedDynamoItem dynamoItem;

        public MiuPacketViewModel(MiuPacketReceivedDynamoItem dynamoItem)
        {
            this.dynamoItem = dynamoItem;
        }

        public MiuPacketReceivedDynamoItem getDynamoItem()
        {
            return dynamoItem;
        }

        public Date getDateBuilt()
        {
            return new Date(dynamoItem.getDateBuilt());
        }


        public Date getInsertDate()
        {
            return new Date(dynamoItem.getInsertDate());
        }

        public String getPacketHex()
        {
            return DatatypeConverter.printHexBinary(dynamoItem.getPacketData().array());
        }
    }
}
