/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.mqtt.service.subscription;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.IOException;

/**
 * Provide callback handler for handling of arrival of subscribed topics.
 */
public interface MqttMessageHandler
{
    /**
     * Handler to process incoming MQTT publication associated to the subscribed topic.
     * @param topic The fully qualified topic name of the incoming message
     * @param message The message content
     */
    public void processMessage(String topic, MqttMessage message) throws IOException, MqttException;

    /** The topic that this message handler will handle
     * @return the topic that this handler is interested in
     */
    public String getTopic();

    /**
     * This method is called after MQTT unsubscribe.  It allows the message handler to ensure that all messages
     * are processed.
     */
    public void shutdown();
}
