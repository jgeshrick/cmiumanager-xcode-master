/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.service;

import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CmiuConfigRepository;
import com.neptunetg.common.data.miu.cellular.CmiuConfigSetAssociation;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.common.data.miu.cellular.ConfigSet;
import com.neptunetg.common.data.miu.command.MiuCommandRepository;
import com.neptunetg.common.data.miu.heard.MiuUpdateRepository;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.mdce.common.data.PacketRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class MiuDetailsUpdaterTest
{

    @Mock
    private MiuCommandRepository miuCommandRepository;

    @Mock
    private MiuUpdateRepository miuRepository;

    @Mock
    private CmiuConfigRepository configRepository;

    @Mock
    private CmiuRepository cmiuRepository;

    @Mock
    private PacketRepository packetRepo;

    @Mock
    private MdceIpcPublishService awsQueueService;


    @InjectMocks
    private MiuDetailsUpdater updater;

    @Before
    public void setUp()
    {
        //Mockito initialise annotated mocks
        initMocks(this);

    }

    @Test
    public void testMissingConfigManagementRow()
    {
        final ConfigSet configSet = new ConfigSet();

        configSet.setRecordingStartTimeMins(0);
        configSet.setRecordingIntervalMins(60);
        configSet.setReportingStartMins(0);
        configSet.setReportingIntervalMins(24 * 60);
        configSet.setReportingNumberOfRetries(2);
        configSet.setReportingTransmitWindowMins(120);
        configSet.setReportingQuietStartMins(0);
        configSet.setReportingQuietEndMins(0);

        final MiuId miuId = MiuId.valueOf(4);
        final int siteId = 25;

        final CmiuConfigSetAssociation configSetAssociation = new CmiuConfigSetAssociation(miuId, siteId, null, configSet);
        this.updater.scheduleForCmiuConfigurationValidation(configSetAssociation);

        // configRepository
        this.updater.updateMiuDetails();

        //this would have returned null
        verify(this.configRepository, atLeastOnce()).getCmiuConfigMgmt(miuId);

        //config should have been updated despite null.  This will create the missing config management row
        verify(this.configRepository, times(1)).updateCmiuConfig(eq(miuId), eq(configSet), eq(null),
                eq(true), contains(miuId.toString()));

    }
}