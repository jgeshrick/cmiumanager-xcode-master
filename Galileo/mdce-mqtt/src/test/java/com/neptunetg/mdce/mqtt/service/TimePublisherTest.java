/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.mqtt.service;

import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class TimePublisherTest
{
    //Mockito injects mockTopicSubService into the instance annotated with @InjectMocks annotation
    @Mock
    private BrokerConnectAsMdceService mockTopicSubService;

    @Mock
    private MdceIpcSubscribeService ipcSubService;

    //the class under test. InjectMocks annotation marked for Mockito to perform injection to this class.
    //Mockito will consider all fields having @Mock or @Spy annotation as potential candidates to be
    // injected into the instance annotated with @InjectMocks annotation.
    @InjectMocks
    private TimePublisher timePublisher;



    @Before
    public void setUp()
    {
        //Mockito initialise annotated mocks
        initMocks(this);

    }


    @Test
    public void testDefaultTimerDoesNotPublish()
    {
        assertFalse(timePublisher.isPublishing());
    }

    @Test
    public void testStart() throws Exception
    {
        timePublisher.Start();
        assertTrue(timePublisher.isPublishing());
    }

    @Test
    public void testStop() throws Exception
    {
        timePublisher.Start();
        assertTrue(timePublisher.isPublishing());

        timePublisher.stop();
        assertFalse(timePublisher.isPublishing());
    }

    @Test
    public void testPublishTimeTask() throws Exception
    {
        timePublisher.publishTimeTask();
        final ArgumentCaptor<byte[]> captureMessageData = ArgumentCaptor.forClass(byte[].class);
        verify(this.mockTopicSubService, never())
                .publishToPrimaryBroker(
                        eq(timePublisher.getTopicName()), captureMessageData.capture(), anyInt(), anyBoolean()
                );

        timePublisher.Start();
        assertTrue(timePublisher.isPublishing());
        Thread.sleep(1100L);
        timePublisher.publishTimeTask();

        verify(this.mockTopicSubService,atLeastOnce()).publishToPrimaryBroker(
                eq(timePublisher.getTopicName()), captureMessageData.capture(), anyInt(), anyBoolean());

        final byte[] packetData = captureMessageData.getValue();
        final TaggedPacket packet = PacketParser.parseTaggedPacket(packetData);
        assertEquals(TaggedDataPacketType.TimeOfDay, packet.getPacketType());

    }
}