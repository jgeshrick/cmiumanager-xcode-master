/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.mqtt.service;

import com.neptunetg.mdce.mqtt.service.subscription.BrokerStatusSubscription;
import com.neptunetg.mdce.mqtt.service.subscription.CmiuReportSubscription;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)  //cause mocks to be created
public class MqttSubscriptionManagerTest
{
    //class under test
    @InjectMocks
    MqttSubscriptionManager subscriptionManager;

    @Mock
    private TimePublisher timePublisher;

    @Mock
    private BrokerConnectAsMdceService pubSubService;

    @Mock
    private BrokerStatusSubscription brokerStatusSub;

    @Mock
    private CmiuReportSubscription cmiuDiagnosticSub;

    @Test
    public void testStartMdceMqtt() throws Exception
    {
        subscriptionManager.startMdceMqtt();

        //verify it attempts to connect to mqtt broker
        verify(pubSubService).connect();

        //verify starts timer
        verify(timePublisher).Start();
    }

    @Test
    public void testStopMdceMqtt() throws Exception
    {
        subscriptionManager.stopMdceMqtt();

        verify(timePublisher).stop();

        verify(pubSubService).disconnect();
    }
}