/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.mqtt.service.subscription;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleService;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import com.neptunetg.mdce.mqtt.common.QueuedMiuDetail;
import com.neptunetg.mdce.mqtt.data.miu.MiuPacketDataAndStats;
import com.neptunetg.mdce.mqtt.service.BrokerConnectAsMiuService;
import com.neptunetg.mdce.mqtt.service.MiuDetailsUpdater;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.junit.*;
import org.mockito.ArgumentCaptor;

/**
 * Test the CmiuReportSubscription test
 */
public class CmiuReportSubscriptionTest
{
    /**
     * Check that if the packet-utils throws an Exception, the packet is still inserted
     */
    @Test
    public void testUnparseablePacketHandling()
    {
        final BrokerConnectAsMiuService allBrokers = mock(BrokerConnectAsMiuService.class);
        final MiuDetailsUpdater updater = mock(MiuDetailsUpdater.class);
        final MiuLifecycleService lifecycleService = mock(MiuLifecycleService.class);
        final MdceIpcPublishService awsSqsService = mock(MdceIpcPublishService.class);

        final CmiuReportSubscription crs = new CmiuReportSubscription(allBrokers, updater,
                lifecycleService, awsSqsService);

        final MqttMessage message = new MqttMessage();
        message.setPayload(new byte[] {3, 1, 5}); //truncated

        crs.processMessage("CMIU/Report/400000002", message);

        final ArgumentCaptor<MiuPacketDataAndStats> qmdCaptor = ArgumentCaptor.forClass(MiuPacketDataAndStats.class);
        verify(updater, times(1)).scheduleForUpdate(qmdCaptor.capture()); //should still be updated despite error

        final QueuedMiuDetail qmd = qmdCaptor.getValue();
        assertEquals(MiuId.valueOf(400000002), qmd.getMiuId());
        final MiuPacketReceivedDynamoItem di = qmd.getReceivedDynamoItem();
        assertEquals(400000002, di.getMiuId());
        assertEquals("CMIU:3", di.getPacketType());
    }
}
