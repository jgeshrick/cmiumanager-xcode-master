/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.mqtt.service;

import org.junit.Test;

import java.util.Properties;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MqttTopicSubscriptionServiceTest
{
    private BrokerConnectAsMdceService mqttTopicSubscriptionService =  new BrokerConnectAsMdceService(getBrokerProperties());


    private static final Properties getBrokerProperties()
    {
        final Properties envProperties = new Properties();

        envProperties.setProperty("mqtt.broker.primary.url", "tcp://unknownHost:1883");
        envProperties.setProperty("mqtt.clientId", "testClientId");
        envProperties.setProperty("mqtt.tmpDir", "");

        envProperties.setProperty("mqtt.broker.count", "1");
        envProperties.setProperty("mqtt.broker.0.url", "tcp://unknownHost:1883");

        return envProperties;
    }

    @Test
    public void testMatchesSubscriptionCorrectlyDetectsSingleHierarchyWildcard() throws Exception
    {
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("a", "a"));
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("a/b/c/d", "a/b/c/d"));
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("a/b/c/+", "a/b/c/d"));
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("a/b/+/+", "a/b/c/d"));
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("a/+/+/+", "a/b/c/d"));
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("+/+/+/+", "a/b/c/d"));

        assertFalse(BrokerConnectAsMdceService.matchesSubscription("a+", "a/b/c/d"));
        assertFalse(BrokerConnectAsMdceService.matchesSubscription("a+/+/+/+", "a/b/c/d"));
        assertFalse(BrokerConnectAsMdceService.matchesSubscription("a/+/+", "a/b/c/d"));
        assertFalse(BrokerConnectAsMdceService.matchesSubscription("a/b/+", "a/b/c/d"));
        assertFalse(BrokerConnectAsMdceService.matchesSubscription("+/+/+", "a/b/c/d"));
        assertFalse(BrokerConnectAsMdceService.matchesSubscription("a/b/c", "a/b/c/d"));
        assertFalse(BrokerConnectAsMdceService.matchesSubscription("b/+/c/d", "a/b/c/d"));

        assertFalse(BrokerConnectAsMdceService.matchesSubscription("a/b/c/d", "a/b/c/d/e"));
        assertFalse(BrokerConnectAsMdceService.matchesSubscription("a/b/c/d", "a/b/c/"));

    }

    @Test
    public void testMatchesSubscriptionMatchesHashWildcard() throws Exception
    {
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("a/b/c/#", "a/b/c/d"));
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("a/b/#", "a/b/c/d"));
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("a/#", "a/b/c/d"));
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("#", "a/b/c/d"));

        assertFalse(BrokerConnectAsMdceService.matchesSubscription("b/#", "a/b/c/d"));
    }

    @Test
    public void testMatchesSubscriptionMatchesCombinationCombination() throws Exception
    {
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("+/b/c/#", "a/b/c/d"));
    }

    @Test
    public void testMatchesSubscriptionMatchesBrokerStatusTopic() throws Exception
    {
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("$SYS", "$SYS"));
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("$SYS/#", "$SYS/broker"));
        assertTrue(BrokerConnectAsMdceService.matchesSubscription("$SYS/#", "$SYS/broker/test"));
    }


}