# -*- coding: utf-8 -*-
"""

Definitions of production environment
As per 10065-1_DO04_0002 MDCE Software Architecture Specification

"""

#Credentials for the S3 buckets shared between prod and nonprod
s3SharedBucketCredentials = {
    # credentials for neptune-binary-distribution-manager
    "aws_access_key_id": "AKIAIIW23OGZYWLBPLFQ",
    "aws_secret_access_key": "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"
}


################# WHITELISTS ###################

# Note: all CIDR lists should have a trailing comma to ensure correct interpretation by Salt

#IP addresses of NTG and its contractors.  These IPs are allowed access to the
#system, e.g. SSH, MSOC web etc.
trustedCidrs = (
    "66.222.22.2/32",       #Troy Harstad, April 1, 2016
    "24.227.104.32/27",     #Tallassee
    "71.170.201.99/32",     #Plano
    "50.180.95.157/32",     #Charles Cottle, April 20, 2017
    "217.33.180.66/32",     #Sagentia
    "54.174.27.31/32",      #Jon Greisz testing (AWS N_Sight instance)
    "70.63.161.250/32",     #Barry Huggins
    "52.6.234.227/32",      #NAT Gateway in vpn-prod-public-a-subnet (i.e. IP address for outgoing traffic from the salt master - for the mdceMonitor external ping test)
)

#IP addresses of LoRa providers
loraCidrs = (
    "207.20.39.117/32",     #Senet production server
    "198.46.49.118/32",     #Senet production server (App EUI added Sept 2016)
    "217.69.25.69/32",      #Actility production server
    "217.69.25.85/32",      #Actility production server
    "34.197.99.138/32",     #Senet
    "52.88.64.191/32",      #Senet
)

#IP addresses of NTG factories
factoryCidrs = (
    "207.201.209.96/27",    #MC Assembly – Melbourne, FL
    "24.227.104.32/27",     #Tallassee Factory
    "201.174.16.162/32",    #RoperMex - Juarez, MX
)

################# WHITELIST SUMMARY UPLOAD ###################

whitelistSummaryConfig = {
    'account': 'aws-mdce-prod',
    's3': {
        'bucketName': 'neptune-mdce-deployment',
        'bucketRegion': 'us-east-1',
        'whitelistHtmlFolder': '/whitelist_summary',
        'whitelistHtmlFileName': 'whitelist_summary_prod.html',
    }
}

################# VPC ELEMENT NAMING ###################

MNO_PREFIX = 'vpn-'

################# VPCS ###################


#Adding a VPC spec to this list will not cause it to be created.  You need to do that manually (MNO VPCs)
#or run python CreateEnvVpc.py <envName> to create the MDCE VPC for that environment if missing
vpcs = {
    MNO_PREFIX + "prod-vpc": {
        "vpcName": MNO_PREFIX + "prod-vpc",
        "igName": MNO_PREFIX + "prod-internet-gateway",
        "vpcCidr":  "10.123.0.0/16",
        "mnoRouting": [ #TODO: AT&T missing?
                {"type": "gateway", "id": "vgw-905eb2f9", "propagate": True, "cidrList": None}, #AT&T vgw?
        ],
        "region": "us-east-1",
        "accountLevelSubnets": [
            # first is created with NAT gateway and will have Bastion Host created
            {
                "name": MNO_PREFIX + "prod-public-a-subnet", "az": "a", "cidr": "10.123.0.0/26", "public": True,
                "nat-gateway": {
                    "elasticIp": "52.6.234.227"
                },
                "bastion-host": {
                    "name": "aw-mdce-prod-bst01",
                    "elasticIp": "34.194.23.165"
                }
            },
            # second is created for Bastion Host only
            {
                "name": MNO_PREFIX + "prod-public-c-subnet", "az": "c",  "cidr": "10.123.0.64/26", "public": True,
                "nat-gateway": None, 
                "bastion-host": {
                    "name": "aw-mdce-prod-bst02",
                    "elasticIp": "34.202.113.169"
                }
            },
        ],
        "publicSubnetRoutingTable": {
            "name": MNO_PREFIX + "prod-public-routing"
        }
            
    },
    "mdce-prod-vpc": {
        "vpcName": "mdce-prod-vpc",
        "igName": "mdce-prod-internet-gateway",
        "vpcCidr":  "10.124.0.0/16",
        "peeringConnectionToMnoVpc": MNO_PREFIX + "mdce-prod-peering",
        "region": "us-east-1"
    }
}


################# TEMPLATES ###################


__prodTemplate = {
    "mnoVpcSubnets": (
        {"name": "private-a",  "az": "a", "cidrSuffix": ".0/26",   "public": False},
        {"name": "private-c",  "az": "c", "cidrSuffix": ".64/26",  "public": False}
    ),
    "mdceVpcSubnets": (
        {"name": "public-a",  "az": "a", "cidrSuffix": ".0.0/24", "public": True,
            "nat-gateway": {
                "elasticIp": "52.2.114.70"
            },
            "bastion-host": None
        },
        {"name": "public-c",  "az": "c", "cidrSuffix": ".1.0/24", "public": True,
            "nat-gateway": {
                "elasticIp": "52.1.197.195"
            },
            "bastion-host": None
        },
        {"name": "private-a", "az": "a", "cidrSuffix": ".2.0/24", "public": False},
        {"name": "private-c", "az": "c", "cidrSuffix": ".3.0/24", "public": False}
    ),
    "multiAZ": True,
    "brokerInstances": 2,
    "appInstances": 2,
    "efs": True,
    "elb": True
}

################# ENVIRONMENTS ###################

#To create an environment, add its spec to this list and then run python CreateEnvVpc.py <envName>
envs = {
    "prod": {
        "mnoVpc": MNO_PREFIX + "prod-vpc",
        "mnoVpcCidr": "10.123.99.0/24",
        "mdceVpc": "mdce-prod-vpc",
        "mdceVpcCidr": "10.124.0.0/16",
        "rdsMasterUserPassword": "ar349780jfs--.rwge0",
        "rdsInstanceType": "db.r3.large",
        "rdsEndpointNameSuffix": "-rds-aurora-cluster",
        "template": __prodTemplate,
        "httpsCertArn": "arn:aws:iam::347209793840:server-certificate/mdce_neptunensight_com",
        "elasticIps": {
        },
        "internalIps": { #we specify these because they are configured in the VZW network DHCP options.  If the servers are recreated, we want them to keep these internal IPs
            "aw-mdce-prod-brk01": "10.123.99.25",
            "aw-mdce-prod-brk02": "10.123.99.99"
        }

    }
}
