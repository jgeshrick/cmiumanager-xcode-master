#!yaml_jinja
{% from "aws-info.map.jinja" import awsInfo %}
{% from "mdce-env-spec.map.jinja" import trustedCidrs %}
{% from "mdce-env-spec.map.jinja" import vpcs %}
{% from "mdce-env-spec.map.jinja" import envs %}
{% from "mdce-env-spec.map.jinja" import MNO_PREFIX %}

# AWS state file for prod environment ec2 instances.
# See https://docs.saltstack.com/en/latest/ref/states/all/salt.states.cloud.html

{% set ENV_NAME = 'prod' %}                {#- PROD-SPECIFIC #}
{% set ACCOUNT_ID = '347209793840' %}      {#- PROD-SPECIFIC #}

{% set CENTOS_AMI = 'ami-96a818fe' %} {#- CentOS 7 x86_64 (2014_09_29) EBS HVM-b7ee8a69-ee97-4a49-9e68-afaee216db2e-ami-d2a117ba.2 (ami-96a818fe) in us-east-1 PROD-SPECIFIC #}

{% set CLOUD_PROVIDER = 'aws-prod-ec2' %}  {#- PROD-SPECIFIC #}

{% set mnoVpcInfo = vpcs[envs[ENV_NAME]["mnoVpc"]] %}
{% set REGION = mnoVpcInfo["region"] %}
{% set MNO_VPC_ID = awsInfo[REGION]["vpcId"][mnoVpcInfo["vpcName"]] %}

{% set mdceVpcInfo = vpcs[envs[ENV_NAME]["mdceVpc"]] %}
{% set MDCE_VPC_ID = awsInfo[REGION]["vpcId"][mdceVpcInfo["vpcName"]] %}

{% set MNO_VPC_IP_24BIT = envs[ENV_NAME]["mnoVpcCidr"] | replace(".0/24", "") %}
{% set MDCE_VPC_IP_16BIT = envs[ENV_NAME]["mdceVpcCidr"] | replace(".0.0/16", "") %}

{% set BROKER_INTERNAL_SG_ID = awsInfo[REGION]["sgId"][MNO_VPC_ID][MNO_PREFIX~ENV_NAME~"-broker-internal-security-group"] %}
{% set APPS_INTERNAL_SG_ID = awsInfo[REGION]["sgId"][MDCE_VPC_ID]["mdce-"~ENV_NAME~"-applications-internal-security-group"] %}
{% set BROKER_MANAGEMENT_SG_ID = awsInfo[REGION]["sgId"][MNO_VPC_ID][MNO_PREFIX~"prod-management-internal-security-group"] %} {#- PROD-SPECIFIC #}
{% set APPS_MANAGEMENT_INTERNAL_SG_ID = awsInfo[REGION]["sgId"][MDCE_VPC_ID]["mdce-prod-management-internal-security-group"] %} {#- PROD-SPECIFIC #}

{% if awsInfo[REGION]["sgId"] is defined
and BROKER_INTERNAL_SG_ID is defined
and awsInfo[REGION]["sgId"][MNO_VPC_ID][MNO_PREFIX~ENV_NAME~"-broker-external-security-group"] is defined
and APPS_INTERNAL_SG_ID is defined 
and awsInfo[REGION]["sgId"][MDCE_VPC_ID]["mdce-"~ENV_NAME~"-applications-external-security-group"] is defined 
and BROKER_MANAGEMENT_SG_ID is defined
%}

{% set CLOUDWATCH_SNS_ARN = "arn:aws:sns:"+REGION+":"+ACCOUNT_ID+":"+ENV_NAME+"-cloudwatch-sns-topic" %}
{% set EMAIL_ALERT_SNS_ARN = "arn:aws:sns:"+REGION+":"+ACCOUNT_ID+":aws-prod-alarm-recipients" %} {#- PROD-SPECIFIC #}

{% set SERVER_NAME = "aw-mdce-" + ENV_NAME + "-brk01" %}
{% set EIP = envs[ENV_NAME]["elasticIps"][SERVER_NAME] %}
{% set IIP = envs[ENV_NAME]["internalIps"][SERVER_NAME] %}
{{ SERVER_NAME }}:
    cloud.present:
        - grains:
            env: {{ ENV_NAME }}
            defaultUser: centos
        - cloud_provider: {{ CLOUD_PROVIDER }}
        - image: {{ CENTOS_AMI }}
        - size: 'm4.xlarge'
        - ssh_username: centos
        - location: {{ REGION }}
        - delvol_on_destroy: True
        - iam_profile: 'arn:aws:iam::{{ ACCOUNT_ID }}:instance-profile/mdce-aws-service-access-role'
        - network_interfaces:
            - DeviceIndex: 0
              PrivateIpAddresses:
                - Primary: True
{%- if IIP is defined %}                
                  PrivateIpAddress: {{ IIP }}
{%- endif %}
{%- if EIP is defined %}
              associate_eip: {{ awsInfo[REGION]["elasticIpAllocationId"][EIP] }}
{%- else %}
              AssociatePublicIpAddress: False
{%- endif %}
              SubnetId: {{ awsInfo[REGION]["subnetId"][MNO_VPC_ID][MNO_PREFIX~"prod-private-a-subnet"] }}  {#- PROD-SPECIFIC #}
              SecurityGroupId:
                - {{ BROKER_MANAGEMENT_SG_ID }}
                - {{ BROKER_INTERNAL_SG_ID }} 
        - tag:
            'Env': '{{ ENV_NAME }}'

{% if awsInfo[REGION]["ec2Id"] is defined and awsInfo[REGION]["ec2Id"][SERVER_NAME] is defined %}

{% set ALARM_NAME = SERVER_NAME + " ping test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Ping
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: ping status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server: 
            - {{ SERVER_NAME }}
          Environment: 
            - {{ ENV_NAME }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " CPU high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: CPUUtilization
        namespace: AWS/EC2
        statistic: Average
        comparison: ">="
        threshold: 95.0
        period: 300
        evaluation_periods: 2
        description: CPU at over 95% on AWS server instance {{ SERVER_NAME }} for 10 minutes
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions:
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " status check failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: StatusCheckFailed
        namespace: AWS/EC2
        statistic: Maximum
        comparison: ">"
        threshold: 0.0
        period: 300
        evaluation_periods: 1
        description: AWS status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " disk usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: DiskSpaceUtilization
        namespace: System/Linux
        statistic: Average
        comparison: ">="
        threshold: 90.0
        period: 300
        evaluation_periods: 1
        description: Disk is 90% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          Filesystem: 
            - /dev/xvda1
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
          MountPath: 
            - /
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions:
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " memory usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: MemoryUtilization
        namespace: System/Linux
        statistic: Average
        comparison: ">="
        threshold: 80.0
        period: 300
        evaluation_periods: 2
        description: RAM is 80% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}


{% endif %} # EC2 ID available


{% set SERVER_NAME = "aw-mdce-" + ENV_NAME + "-brk02" %}
{% set EIP = envs[ENV_NAME]["elasticIps"][SERVER_NAME] %}
{% set IIP = envs[ENV_NAME]["internalIps"][SERVER_NAME] %}
{{ SERVER_NAME }}:
    cloud.present:
        - grains:
            env: {{ ENV_NAME }}
            defaultUser: centos
        - cloud_provider: {{ CLOUD_PROVIDER }}
        - image: {{ CENTOS_AMI }}
        - size: 'm4.xlarge'
        - ssh_username: centos
        - location: {{ REGION }}
        - delvol_on_destroy: True
        - iam_profile: 'arn:aws:iam::{{ ACCOUNT_ID }}:instance-profile/mdce-aws-service-access-role'
        - network_interfaces:
            - DeviceIndex: 0
              PrivateIpAddresses:
                - Primary: True
{%- if IIP is defined %}                
                  PrivateIpAddress: {{ IIP }}
{%- endif %}
{%- if EIP is defined %}
              associate_eip: {{ awsInfo[REGION]["elasticIpAllocationId"][EIP] }}
{%- else %}
              AssociatePublicIpAddress: False
{%- endif %}
              SubnetId: {{ awsInfo[REGION]["subnetId"][MNO_VPC_ID][MNO_PREFIX~"prod-private-c-subnet"] }}  {#- PROD-SPECIFIC #}
              SecurityGroupId:
                - {{ BROKER_MANAGEMENT_SG_ID }}
                - {{ BROKER_INTERNAL_SG_ID }} 
        - tag:
            'Env': '{{ ENV_NAME }}'

{% if awsInfo[REGION]["ec2Id"] is defined and awsInfo[REGION]["ec2Id"][SERVER_NAME] is defined %}

{% set ALARM_NAME = SERVER_NAME + " ping test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Ping
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: ping status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server: 
            - {{ SERVER_NAME }}
          Environment: 
            - {{ ENV_NAME }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " CPU high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: CPUUtilization
        namespace: AWS/EC2
        statistic: Average
        comparison: ">="
        threshold: 95.0
        period: 300
        evaluation_periods: 2
        description: CPU at over 95% on AWS server instance {{ SERVER_NAME }} for 10 minutes
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " status check failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: StatusCheckFailed
        namespace: AWS/EC2
        statistic: Maximum
        comparison: ">"
        threshold: 0.0
        period: 300
        evaluation_periods: 1
        description: AWS status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " disk usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: DiskSpaceUtilization
        namespace: System/Linux
        statistic: Average
        comparison: ">="
        threshold: 90.0
        period: 300
        evaluation_periods: 1
        description: Disk is 90% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          Filesystem: 
            - /dev/xvda1
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
          MountPath: 
            - /
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " memory usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: MemoryUtilization
        namespace: System/Linux
        statistic: Average
        comparison: ">="
        threshold: 80.0
        period: 300
        evaluation_periods: 2
        description: RAM is 80% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% endif %} # EC2 ID available

{% set SERVER_NAME = "aw-mdce-" + ENV_NAME + "-app01" %}
{% set IIP = envs[ENV_NAME]["internalIps"][SERVER_NAME] %}
{{ SERVER_NAME }}:
    cloud.present:
        - grains:
            env: {{ ENV_NAME }}
            defaultUser: centos
        - cloud_provider: {{ CLOUD_PROVIDER }}
        - image: {{ CENTOS_AMI }}
        - size: 'm4.xlarge'
        - ssh_username: centos
        - location: {{ REGION }}
        - delvol_on_destroy: True
        - iam_profile: 'arn:aws:iam::{{ ACCOUNT_ID }}:instance-profile/mdce-aws-service-access-role'
        - network_interfaces:
            - DeviceIndex: 0
              PrivateIpAddresses:
                - Primary: True
{%- if IIP is defined %}                
                  PrivateIpAddress: {{ IIP }}
{%- endif %}
              AssociatePublicIpAddress: False
              SubnetId: {{ awsInfo[REGION]["subnetId"][MDCE_VPC_ID]["mdce-prod-private-a-subnet"] }}  {#- PROD-SPECIFIC #}
              SecurityGroupId:
                - {{ APPS_INTERNAL_SG_ID }} 
                - {{ APPS_MANAGEMENT_INTERNAL_SG_ID }}
        - tag:
            'Env': '{{ ENV_NAME }}'


{% if awsInfo[REGION]["ec2Id"] is defined and awsInfo[REGION]["ec2Id"][SERVER_NAME] is defined %}

{% set ALARM_NAME = SERVER_NAME + " ping test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Ping
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: ping status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server:
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " mdce-lora app test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Heartbeat
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: mdce-lora status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server:
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application:
            - mdce-lora
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " mdce-mqtt app test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Heartbeat
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: mqtt status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server:
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application:
            - mdce-mqtt
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " fpc app test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Heartbeat
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: fpc status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server:
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application:
            - fpc
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " mdce-integration app test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Heartbeat
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: integration status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server:
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application:
            - mdce-integration
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " whitelisted web access test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: WhitelistedWebAccess
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: whitelisted web access status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server:
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application:
            - mdce-web
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " CPU high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: CPUUtilization
        namespace: AWS/EC2
        statistic: Average
        comparison: ">="
        threshold: 95.0
        period: 300
        evaluation_periods: 2
        description: CPU at over 95% on AWS server instance {{ SERVER_NAME }} for 10 minutes
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " status check failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: StatusCheckFailed
        namespace: AWS/EC2
        statistic: Maximum
        comparison: ">"
        threshold: 0.0
        period: 300
        evaluation_periods: 1
        description: AWS status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " disk usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: DiskSpaceUtilization
        namespace: System/Linux
        statistic: Average
        comparison: ">="
        threshold: 90.0
        period: 300
        evaluation_periods: 1
        description: Disk is 90% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          Filesystem: 
            - /dev/xvda1
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
          MountPath: 
            - /
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " memory usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: MemoryUtilization
        namespace: System/Linux
        statistic: Average
        comparison: ">="
        threshold: 80.0
        period: 300
        evaluation_periods: 2
        description: RAM is 80% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions:
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " mdce-tomcat Java heap usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: OldSpaceRetainedUtilization
        namespace: System/Java
        statistic: Average
        comparison: ">="
        threshold: 80.0
        period: 300
        evaluation_periods: 2
        description: mdce-tomcat Java heap is 80% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server: 
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application: 
            - mdce-tomcat
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " mdce-lora-tomcat Java heap usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: OldSpaceRetainedUtilization
        namespace: System/Java
        statistic: Average
        comparison: ">="
        threshold: 80.0
        period: 300
        evaluation_periods: 2
        description: mdce-lora-tomcat Java heap is 80% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server: 
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application: 
            - mdce-lora-tomcat
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% endif %} # endif EC2 ID available



{% set SERVER_NAME = "aw-mdce-" + ENV_NAME + "-app02" %}
{% set IIP = envs[ENV_NAME]["internalIps"][SERVER_NAME] %}
{{ SERVER_NAME }}:
    cloud.present:
        - grains:
            env: {{ ENV_NAME }}
            defaultUser: centos
        - cloud_provider: {{ CLOUD_PROVIDER }}
        - image: {{ CENTOS_AMI }}
        - size: 'm4.xlarge'
        - ssh_username: centos
        - location: {{ REGION }}
        - delvol_on_destroy: True
        - iam_profile: 'arn:aws:iam::{{ ACCOUNT_ID }}:instance-profile/mdce-aws-service-access-role'
        - network_interfaces:
            - DeviceIndex: 0
              PrivateIpAddresses:
                - Primary: True
{%- if IIP is defined %}                
                  PrivateIpAddress: {{ IIP }}
{%- endif %}
              AssociatePublicIpAddress: False
              SubnetId: {{ awsInfo[REGION]["subnetId"][MDCE_VPC_ID]["mdce-prod-private-c-subnet"] }}  {#- PROD-SPECIFIC #}
              SecurityGroupId:
                - {{ APPS_INTERNAL_SG_ID }} 
                - {{ APPS_MANAGEMENT_INTERNAL_SG_ID }}
        - tag:
            'Env': '{{ ENV_NAME }}'


{% if awsInfo[REGION]["ec2Id"] is defined and awsInfo[REGION]["ec2Id"][SERVER_NAME] is defined %}

{% set ALARM_NAME = SERVER_NAME + " ping test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Ping
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: ping status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server:
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " CPU high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: CPUUtilization
        namespace: AWS/EC2
        statistic: Average
        comparison: ">="
        threshold: 95.0
        period: 300
        evaluation_periods: 2
        description: CPU at over 95% on AWS server instance {{ SERVER_NAME }} for 10 minutes
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " status check failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: StatusCheckFailed
        namespace: AWS/EC2
        statistic: Maximum
        comparison: ">"
        threshold: 0.0
        period: 300
        evaluation_periods: 1
        description: AWS status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " disk usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: DiskSpaceUtilization
        namespace: System/Linux
        statistic: Average
        comparison: ">="
        threshold: 90.0
        period: 300
        evaluation_periods: 1
        description: Disk is 90% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          Filesystem: 
            - /dev/xvda1
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
          MountPath: 
            - /
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " memory usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: MemoryUtilization
        namespace: System/Linux
        statistic: Average
        comparison: ">="
        threshold: 80.0
        period: 300
        evaluation_periods: 2
        description: RAM is 80% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " mdce-tomcat Java heap usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: OldSpaceRetainedUtilization
        namespace: System/Java
        statistic: Average
        comparison: ">="
        threshold: 80.0
        period: 300
        evaluation_periods: 2
        description: mdce-tomcat Java heap is 80% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server: 
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application: 
            - mdce-tomcat
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " mdce-lora-tomcat Java heap usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: OldSpaceRetainedUtilization
        namespace: System/Java
        statistic: Average
        comparison: ">="
        threshold: 80.0
        period: 300
        evaluation_periods: 2
        description: mdce-lora-tomcat Java heap is 80% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server: 
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application: 
            - mdce-lora-tomcat
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% endif %} # endif EC2 ID available

{% endif %} # endif security group IDs available
