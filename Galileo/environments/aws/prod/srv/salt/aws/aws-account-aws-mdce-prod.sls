#!yaml_jinja
{% from "aws-info.map.jinja" import awsInfo %}
{% from "mdce-env-spec.map.jinja" import vpcs %}
{% from "mdce-env-spec.map.jinja" import trustedCidrs %}
{% from "mdce-env-spec.map.jinja" import MNO_PREFIX %}

{% set CENTOS_AMI = 'ami-96a818fe' %} {#- CentOS 7 x86_64 (2014_09_29) EBS HVM-b7ee8a69-ee97-4a49-9e68-afaee216db2e-ami-d2a117ba.2 (ami-96a818fe) in us-east-1 PROD-SPECIFIC #}
{% set CLOUD_PROVIDER = 'aws-prod-ec2' %}  {#- PROD-SPECIFIC #}

{% set mnoVpcInfo = vpcs[MNO_PREFIX ~ "prod-vpc"] %} {#- PROD-SPECIFIC #}
{% set REGION = mnoVpcInfo["region"] %}
{% set MNO_VPC_ID = awsInfo[REGION]["vpcId"][mnoVpcInfo["vpcName"]] %}

{% set MNO_EXTERNAL_MANAGEMENT_SG_NAME = MNO_PREFIX ~ 'prod-management-external-security-group' %} {#- PROD-SPECIFIC #}
{% set MNO_EXTERNAL_MANAGEMENT_SG_ID = awsInfo[REGION]["sgId"][MNO_VPC_ID][MNO_EXTERNAL_MANAGEMENT_SG_NAME] %}


# AWS state file for aws-mdce-prod account
# See https://docs.saltstack.com/en/latest/ref/states/all/salt.states.boto_secgroup.html

# Note - salt.state.boto_secgroup doesn't seem able to apply tags at the moment.  We'll need to tag the security groups etc. afterwards

{{ MNO_PREFIX ~ "prod-salt-master-security-group"}}:
    boto_secgroup.present:
        - description: Salt minions contact Salt master via these TCP ports
        - vpc_id: {{ awsInfo["us-east-1"]["vpcId"][MNO_PREFIX~"prod-vpc"] }} {#- PROD-SPECIFIC #}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 4505
            to_port: 4506
            cidr_ip:
                - 10.123.0.0/16 {#- PROD-SPECIFIC #}
                - 10.124.0.0/16 {#- PROD-SPECIFIC #}
        - region: {{ REGION }}
        - profile: salt-boto-aws-management


{{ MNO_EXTERNAL_MANAGEMENT_SG_NAME }}:
    boto_secgroup.present:
        - description: Provides SSH (port 22) access to Bastion Host
        - vpc_id: {{ awsInfo["us-east-1"]["vpcId"][MNO_PREFIX~"prod-vpc"] }} {#- PROD-SPECIFIC #}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 22
            to_port: 22
            cidr_ip:
{% for cidr in trustedCidrs %}
                - {{ cidr }}
{% endfor %}
                - 10.123.0.0/16  {#- PROD-SPECIFIC #}
          - ip_protocol: icmp
            from_port: -1
            to_port: -1
            cidr_ip:
{% for cidr in trustedCidrs %}
                - {{ cidr }}
{% endfor %}
                - 10.123.0.0/16  {#- PROD-SPECIFIC #}
        - region: {{ REGION }}
        - profile: salt-boto-aws-management

{{ MNO_PREFIX ~ 'prod-management-internal-security-group' }}:
    boto_secgroup.present:
        - description: Provides SSH (port 22) access to EC2 instances from Bastion host
        - vpc_id: {{ awsInfo["us-east-1"]["vpcId"][MNO_PREFIX ~ "prod-vpc"] }} {#- PROD-SPECIFIC #}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 22
            to_port: 22
            cidr_ip:
                - 10.123.0.0/16  {#- PROD-SPECIFIC #}
          - ip_protocol: icmp
            from_port: -1
            to_port: -1
            cidr_ip:
                - 10.123.0.0/16  {#- PROD-SPECIFIC #}
        - region: {{ REGION }}
        - profile: salt-boto-aws-management
        
# Note - boto 2.38.0 does not support managed policies - see MSPD-561.  So we recreate them as inline policies
mdce-aws-service-access-role:
    boto_iam_role.present:
        - name: mdce-aws-service-access-role
        - policies:
            temp-inline-AmazonS3FullAccess:
                Statement: 
                    - Action:
                          - s3:*
                      Effect: Allow
                      Resource: "*"
            temp-inline-AmazonDynamoDBFullAccesswithDataPipeline:
                Statement: 
                    - Action:
                          - cloudwatch:DeleteAlarms
                          - cloudwatch:DescribeAlarmHistory
                          - cloudwatch:DescribeAlarms
                          - cloudwatch:DescribeAlarmsForMetric
                          - cloudwatch:GetMetricStatistics
                          - cloudwatch:ListMetrics
                          - cloudwatch:PutMetricAlarm
                          - dynamodb:*
                          - sns:CreateTopic
                          - sns:DeleteTopic
                          - sns:ListSubscriptions
                          - sns:ListSubscriptionsByTopic
                          - sns:ListTopics
                          - sns:Subscribe
                          - sns:Unsubscribe
                      Effect: Allow
                      Resource: "*"
                      Sid: DDBConsole
                    - Action:
                          - datapipeline:*
                          - iam:ListRoles
                      Effect: Allow
                      Resource: "*"
                      Sid: DDBConsoleImportExport
                    - Action:
                          - iam:GetRolePolicy
                          - iam:PassRole
                      Effect: Allow
                      Resource: "*"
                      Sid: IAMEDPRoles
            policy-allow-sns-get-list-publish:
                Statement: 
                    - Action:
                          - sns:GetEndpointAttributes
                          - sns:GetPlatformApplicationAttributes
                          - sns:GetSubscriptionAttributes
                          - sns:GetTopicAttributes
                          - sns:ListEndpointsByPlatformApplication
                          - sns:ListPlatformApplications
                          - sns:ListSubscriptions
                          - sns:ListSubscriptionsByTopic
                          - sns:ListTopics
                          - sns:Publish
                      Effect: Allow
                      Resource: "*"
            policy-allow-sqs:
                Statement: 
                    - Action:
                          - sqs:*
                      Effect: Allow
                      Resource: "*"
            policy-allow-cloudwatch-put-custom-metrics:
                Statement: 
                    - Action:
                          - cloudwatch:PutMetricData
                      Effect: Allow
                      Resource: "*"
            policy-allow-datapipeline-full-access:
                Statement: 
                    - Action:
                          - datapipeline:*
                      Effect: Allow
                      Resource: "*"
        - region: {{ REGION }}
        - profile: salt-boto-aws-management

## Bastion hosts

{% if MNO_EXTERNAL_MANAGEMENT_SG_ID is defined %}
{% for subnet in vpcs[MNO_PREFIX ~ "prod-vpc"]["accountLevelSubnets"] %} {#- PROD-SPECIFIC #}
{% if subnet["bastion-host"] is defined %}
{% set SERVER_NAME = subnet["bastion-host"]["name"] %}
{% set EIP = subnet["bastion-host"]["elasticIp"] %}
{{ SERVER_NAME }}:
    cloud.present:
        - grains:
            defaultUser: centos
        - cloud_provider: {{ CLOUD_PROVIDER }}
        - image: {{ CENTOS_AMI }}
        - size: 't2.micro'
        - private_key: /etc/salt/ssh-keys/mdce-production-bastion-ssh-key.pem {#- PROD-SPECIFIC #}
        - keyname: mdce-production-bastion-ssh-key {#- PROD-SPECIFIC #}
        - ssh_username: centos
        - location: {{ REGION }}
        - delvol_on_destroy: True
        - network_interfaces:
            - DeviceIndex: 0
              PrivateIpAddresses:
                - Primary: True
{%- if IIP is defined %}                
                  PrivateIpAddress: {{ IIP }}
{%- endif %}
{%- if EIP is defined %}
              associate_eip: {{ awsInfo[REGION]["elasticIpAllocationId"][EIP] }}
{%- else %}
              AssociatePublicIpAddress: True     #Note - actually doesn't work in salt 2015.5.3 (Lithium) (current version as of Aug 3, 2015).  The auto-assign setting on the subnet is used instead.  See https://github.com/saltstack/salt/issues/19288
{%- endif %}
              SubnetId: {{ awsInfo[REGION]["subnetId"][MNO_VPC_ID][subnet["name"]] }}
              SecurityGroupId:
                - {{ MNO_EXTERNAL_MANAGEMENT_SG_ID }}
{% endif %}
{% endfor %}
{% endif %}
