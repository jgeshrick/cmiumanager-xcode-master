#!yaml_jinja

# Top file for MDCE production environment management.
# See http://docs.saltstack.com/en/latest/ref/states/top.html

base:
  '*':
    - network
    - timezone
    - hostname

  'aw-mdce-*-bst*':
    - hosts.not-salt-master
    - bastion-hardening

  'aw-mdce-prod-sal01':  #salt-master
    - aws.aws-account-aws-mdce-prod
    - aws.aws-prod.security-groups
    - aws.aws-prod.ec2
    - aws.aws-prod.elb
    - monitoring
    - hosts.salt-master

  'aw-mdce-prod-brk*':
    - nfs.broker
    - bind
    - vsftpd
    - vsftpd.conf-no-ip-override.conf
    - hosts.not-salt-master
    
  'aw-mdce-*-brk*':
    - apache
    - aws-scripts-mon
    - mosquitto

  'aw-mdce-*-app*':
    - java
    - galileo
    - mdce
    - fpc
    - tomcat
    - aws-scripts-mon
    - aws-scripts-mon.mdce-metrics
    - vsftpd.test-files-for-application-server
    - hosts.not-salt-master

  'aw-mdce-prod-app*':
    - nfs.apps
    - tomcat.conf-behind-elb
    - tomcat.systemd.large

  'aw-mdce-*-app01':
    - mysql
    - galileo.galileo-db-migrate
    - galileo.apn-data
    - mdce.mdce-scheduler

