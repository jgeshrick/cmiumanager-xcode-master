#!/bin/bash

ret=0

awk '/^  .aw-mdce-.*/ {split($0, f, "-"); inPreprodStuff=0; env=f[3]; if (env == "*" || env == "preprod") {inPreprodStuff=1; ln = $0; sub(/preprod/, "(ENV)", ln); print ""; print ln;} next;} /[a-zA-Z0-9]/ {if (inPreprodStuff) {print $0;}}' preprod-cross-check/srv/salt/top.sls > preprod-cross-check/preprod_top_sls.txt
awk '/^  .aw-mdce-.*/ {split($0, f, "-"); inProdStuff=0; env=f[3]; if (match($0, "salt\-master")) {inProdStuff=0} else if (env == "*" || env == "prod") {inProdStuff=1; ln = $0; sub(/prod/, "(ENV)", ln); print ""; print ln;} next;} /[a-zA-Z0-9]/ {if (inProdStuff) {print $0;}}' salt-master/srv/salt/top.sls > preprod-cross-check/prod_top_sls.txt

echo comparing top.sls files
diff -w preprod-cross-check/preprod_top_sls.txt preprod-cross-check/prod_top_sls.txt

return_value=$?
if [ $return_value != 0 ]; then
	echo unexpected difference between files!
	ret=$return_value
else
	echo files are consistent
fi

grep -v 'PREPROD\-SPECIFIC\|^#.*\|^[[:space:]]*$' preprod-cross-check/srv/salt/galileo/preprod/mdce-env.properties > preprod-cross-check/preprod_mdce_env_properties.txt
grep -v 'PROD\-SPECIFIC\|^#.*\|^[[:space:]]*$' salt-master/srv/salt/galileo/prod/mdce-env.properties > preprod-cross-check/prod_mdce_env_properties.txt

echo comparing mdce-env.properties files
diff -w preprod-cross-check/preprod_mdce_env_properties.txt preprod-cross-check/prod_mdce_env_properties.txt

return_value=$?
if [ $return_value != 0 ]; then
	echo unexpected difference between files!
	ret=$return_value
else
	echo files are consistent
fi

cd preprod-cross-check/srv/salt/aws/aws-preprod/
grep -v 'PREPROD\-SPECIFIC\|^#.*\|^[[:space:]]*$' *.sls > ../../../../preprod_aws.txt
cd  ../../../../../
cd salt-master/srv/salt/aws/aws-prod/
grep -v 'PROD\-SPECIFIC\|^#.*\|^[[:space:]]*$' *.sls > ../../../../../preprod-cross-check/prod_aws.txt
cd  ../../../../../

echo comparing aws state files
diff -w preprod-cross-check/preprod_aws.txt preprod-cross-check/prod_aws.txt

return_value=$?
if [ $return_value != 0 ]; then
	echo unexpected difference between files!
	ret=$return_value
else
	echo files are consistent
fi

exit $ret
