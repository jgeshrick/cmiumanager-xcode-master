#!yaml_jinja

# Top file for Salt Pillars
# See http://docs.saltstack.com/en/latest/topics/tutorials/pillar.html

base:
  'aw-mdce-int-brk01':
    - salt-boto-aws-management
  '*':
    - neptune-binary-distribution-manager
