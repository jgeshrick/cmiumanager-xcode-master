# -*- coding: utf-8 -*-
"""

Definitions of non-production environments
As per 10065-1_DO04_0002 MDCE Software Architecture Specification

"""

#Credentials for the S3 buckets shared between prod and nonprod
s3SharedBucketCredentials = {
    # credentials for neptune-binary-distribution-manager
    "aws_access_key_id": "AKIAIIW23OGZYWLBPLFQ",
    "aws_secret_access_key": "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"
}

################# WHITELISTS ###################

#Note: all CIDR lists should have a trailing comma to ensure correct interpretation by Salt

#IP addresses of NTG and its contractors.  These IPs are allowed access to the
#system, e.g. SSH, MSOC web etc.
#If this list gets too long, security group management through Salt will start to fail.
trustedCidrs = (
    "66.222.22.2/32",       #Troy Harstad, March 23, 2016
    "24.227.104.32/27",     #Tallassee
    "71.170.201.99/32",     #Plano
    "217.33.180.66/32",     #Sagentia
    "54.174.27.31/32",      #Jon Greisz testing (AWS N_Sight instance)
    "50.180.95.157/32",     #Charles Cottle, April 20, 2017
    "70.63.161.250/32",     #Barry Huggins
    "50.202.1.113/32",      #Nick Sinas, June 27, 2016
    "54.187.239.195/32",    #Int Broker 1 (the salt master - for the mdceMonitor external ping test)
)

#IP addresses that are allowed to access mdce-lora
loraCidrs = (
    "54.191.105.115/32",    #BIRST Connect test machine for Greisz
    "207.20.39.117/32",     #Senet production server
    "198.46.49.118/32",     #Senet production server
    "217.69.25.69/32",      #Actility production server
    "217.69.25.85/32",      #Actility production server
    "34.197.99.138/32",     #Senet
    "52.88.64.191/32",      #Senet
)

#IP addresses of NTG factories
factoryCidrs = (
    "207.201.209.96/27",    #MC Assembly – Melbourne, FL
)

################# WHITELIST SUMMARY UPLOAD ###################

whitelistSummaryConfig = {
    'account': 'jonsaws (nonprod)',
    's3': {
        'bucketName': 'neptune-mdce-deployment',
        'bucketRegion': 'us-east-1',
        'whitelistHtmlFolder': '/whitelist_summary',
        'whitelistHtmlFileName': 'whitelist_summary_nonprod.html',
    }
}

################# VPC ELEMENT NAMING ###################

MNO_PREFIX = 'vpn-' #TODO: update to 'mno-' pre-migration

################# VPCS ###################

#Adding a VPC spec to this list will not cause it to be created.  You need to do that manually (MNO VPCs)
#or run python CreateEnvVpc.py <envName> to create the MDCE VPC for that environment if missing
vpcs = {
    "mno-nonprod-vpc": {  #TODO: replace "mno-" with MNO_PREFIX + once MNO_PREFIX has been updated
        "vpcName": "mno-nonprod-vpc", #TODO: replace "mno-" with MNO_PREFIX + once MNO_PREFIX has been updated
        "igName": MNO_PREFIX + "nonprod-internet-gateway",
        "vpcCidr":  "10.120.0.0/16",
        "mnoRouting": [
            {"type": "interface", "id": "eni-dd514e84", "propagate": False, "cidrList": ["10.128.0.0/16",]},  #Elastic network interface on i-8463d38a (EC2_VZ_Demo-CSR1000V)
            {"type": "gateway", "id": "vgw-a9e53bb7", "propagate": True, "cidrList": None},   #AT&T Virtual Private Gateway
        ],
        "region": "us-west-2",
        "accountLevelSubnets": [
            # first is created with NAT gateway and will have Bastion Host created
            {
                "name": MNO_PREFIX + "nonprod-public-a-subnet", "az": "a", "cidr": "10.120.3.0/26", "public": True,
                "nat-gateway": {
                    "elasticIp": "34.208.138.216"
                },
                "bastion-host": {
                    "name": "aw-mdce-nonprod-bst01",
                    "elasticIp": "52.89.241.250"
                }
            },
            # second is created for Bastion Host only
            {
                "name": MNO_PREFIX + "nonprod-public-b-subnet", "az": "b",  "cidr": "10.120.3.64/26", "public": True,
                "nat-gateway": None, 
                "bastion-host": {
                    "name": "aw-mdce-nonprod-bst02",
                    "elasticIp": "52.26.23.234"
                }
            },
        ],
        "publicSubnetRoutingTable": {
            "name": MNO_PREFIX + "nonprod-public-routing"
        }
    },
    "mdce-dev-vpc": {
        "vpcName": "mdce-dev-vpc",
        "igName": "mdce-dev-internet-gateway",
        "vpcCidr":  "10.121.0.0/16",
        "peeringConnectionToMnoVpc": MNO_PREFIX + "mdce-dev-peering",
        "region": "us-west-2"
    },
    "mdce-preprod-vpc": {
        "vpcName": "mdce-preprod-vpc",
        "igName": "mdce-preprod-internet-gateway",
        "vpcCidr":  "10.122.0.0/16",
        "peeringConnectionToMnoVpc": MNO_PREFIX + "mdce-preprod-peering",
        "region": "us-west-2"
    }
}


################# TEMPLATES ###################

__devTemplate = {
    # TODO: NAT
    "mnoVpcSubnets": [
        {"name": "public",    "az": None, "cidrSuffix": ".0/26",   "public": True, "nat-gateway": None},
    ],
    "mdceVpcSubnets": [
        {"name": "public",    "az": None, "cidrSuffix": ".0/26",   "public": True, "nat-gateway": None},
        {"name": "private-a", "az": "a",  "cidrSuffix": ".128/26", "public": False},
        {"name": "private-b", "az": "b",  "cidrSuffix": ".192/26", "public": False},
    ],
    "multiAZ": False,
    "brokerInstances": 1,
    "appInstances": 1,
    "efs": False,
    "elb": False
}

#int is special because it hosts some shared services: NAT gateway and Salt Master
__intTemplate = {
    "mnoVpcSubnets": [
        {"name": "public",    "az": None, "cidrSuffix": ".0/26",   "public": True, "nat-gateway": None, "bastion-host": None},
    ],
    "mdceVpcSubnets": [
        {"name": "public",    "az": None, "cidrSuffix": ".0/26",   "public": True, 
        "nat-gateway": 
            {
                "elasticIp": "TODO" #TODO: elastic IP for dev NAT
            },
            "bastion-host": None
        },
        {"name": "private-a", "az": "a",  "cidrSuffix": ".128/26", "public": False},
        {"name": "private-b", "az": "b",  "cidrSuffix": ".192/26", "public": False},
    ],
    "multiAZ": False,
    "brokerInstances": 1,
    "appInstances": 1,
    "efs": False,
    "elb": False
}

__prodTemplate = {
    "mnoVpcSubnets": [
        {"name": "private-a",  "az": "a", "cidrSuffix": ".0/26",   "public": False},
        {"name": "private-b",  "az": "b", "cidrSuffix": ".64/26",  "public": False},
    ],
    "mdceVpcSubnets": [
        {"name": "public-a", "az": "a", "cidrSuffix": ".0.0/24", "public": True, 
            "nat-gateway": {
                "elasticIp": "52.11.15.234"
            },
            "bastion-host": None
        },
        {"name": "public-b", "az": "b", "cidrSuffix": ".1.0/24", "public": True, 
            "nat-gateway": {
                "elasticIp": "52.27.63.177"
            },
            "bastion-host": None
        },
        {"name": "private-a", "az": "a", "cidrSuffix": ".2.0/24", "public": False},
        {"name": "private-b", "az": "b", "cidrSuffix": ".3.0/24", "public": False},
    ],
    "multiAZ": True,
    "brokerInstances": 2,
    "appInstances": 2,
    "efs": True,
    "elb": True
}

################# ENVIRONMENTS ###################

#To create an environment, add its spec to this list and then run python CreateEnvVpc.py <envName>
envs = {
    "int": {
        "mnoVpc": "mno-nonprod-vpc", #TODO: replace "mno-" with MNO_PREFIX + once MNO_PREFIX has been updated
        "mnoVpcCidr": "10.120.97.0/24",
        "mdceVpc": "mdce-dev-vpc",
        "mdceVpcCidr": "10.121.97.0/24",
        "rdsMasterUserPassword": "fsag3Q321::",
        "rdsInstanceType": "db.t2.micro",
        "rdsEndpointNameSuffix": "-rds",
        "template": __intTemplate,
        "useFtpTestSecurityGroup": False,
        "elasticIps": {
            "aw-mdce-int-brk01": "54.187.239.195",
            "aw-mdce-int-app01": "52.11.212.174"
        },
        "internalIps": {
        }
        
    },
    "auto-test": {
        "mnoVpc": "mno-nonprod-vpc", #TODO: replace "mno-" with MNO_PREFIX + once MNO_PREFIX has been updated
        "mnoVpcCidr": "10.120.98.0/24",
        "mdceVpc": "mdce-dev-vpc",
        "mdceVpcCidr": "10.121.98.0/24",
        "rdsMasterUserPassword": "f3463ysw!",
        "rdsInstanceType": "db.t2.micro",
        "rdsEndpointNameSuffix": "-rds",
        "template": __devTemplate,
        "useFtpTestSecurityGroup": True,
        "elasticIps": {
            "aw-mdce-auto-test-app01": "52.25.21.32",
            "aw-mdce-auto-test-brk01": "54.191.137.243"
        },
        "internalIps": {
        }
    },
    "preprod": {
        "mnoVpc": "mno-nonprod-vpc", #TODO: replace "mno-" with MNO_PREFIX + once MNO_PREFIX has been updated
        "mnoVpcCidr": "10.120.99.0/24",
        "mdceVpc": "mdce-preprod-vpc",	#Note - different from the others
        "mdceVpcCidr": "10.122.0.0/16",	#Note - different from the others
        "rdsMasterUserPassword": "fsgsdghergerge89g76s9g-f0a9s6",
        "rdsInstanceType": "db.r3.large",
        "rdsEndpointNameSuffix": "-rds-aurora-cluster",
        "template": __prodTemplate,  #Note - different
        "useFtpTestSecurityGroup": False,
        "httpsCertArn": "arn:aws:iam::934427565871:server-certificate/star_mdce-nonprod_neptunensight_com",
        "elasticIps": {
        },
        "internalIps": { #we specify these because they are configured in the VZW private network DHCP options.  If the servers are recreated, we want them to keep these internal IPs
            "aw-mdce-preprod-brk01": "10.120.99.22",
            "aw-mdce-preprod-brk02": "10.120.99.91"
        },
    },
    "dev1": {
        "mnoVpc": "mno-nonprod-vpc", #TODO: replace "mno-" with MNO_PREFIX + once MNO_PREFIX has been updated
        "mnoVpcCidr": "10.120.101.0/24",
        "mdceVpc": "mdce-dev-vpc",
        "mdceVpcCidr": "10.121.101.0/24",
        "rdsMasterUserPassword": "fsgsgerg!",
        "rdsInstanceType": "db.t2.micro",
        "rdsEndpointNameSuffix": "-rds",
        "template": __devTemplate,
        "useFtpTestSecurityGroup": False,
        "elasticIps": {
            "aw-mdce-dev1-app01": "52.26.146.127"
        },
        "internalIps": {
        }
    },
    "sqa": {
        "mnoVpc": "mno-nonprod-vpc", #TODO: replace "mno-" with MNO_PREFIX + once MNO_PREFIX has been updated
        "mnoVpcCidr": "10.120.104.0/24",
        "mdceVpc": "mdce-dev-vpc",
        "mdceVpcCidr": "10.121.104.0/24",
        "rdsMasterUserPassword": "gew87iuj!9",
        "rdsInstanceType": "db.t2.micro",
        "rdsEndpointNameSuffix": "-rds",
        "template": __devTemplate,
        "useFtpTestSecurityGroup": False,
        "elasticIps": {
            "aw-mdce-sqa-app01": "52.89.166.190"
        },
        "internalIps": {
        }
    },
    "dvtest": {
        "mnoVpc": "mno-nonprod-vpc", #TODO: replace "mno-" with MNO_PREFIX + once MNO_PREFIX has been updated
        "mnoVpcCidr": "10.120.105.0/24",
        "mdceVpc": "mdce-dev-vpc",
        "mdceVpcCidr": "10.121.105.0/24",
        "rdsMasterUserPassword": "Jwgh34Lq23",
        "rdsInstanceType": "db.t2.micro",
        "rdsEndpointNameSuffix": "-rds",
        "template": __devTemplate,
        "useFtpTestSecurityGroup": False,
        "elasticIps": {
            "aw-mdce-dvtest-app01": "52.33.178.112"
        },
        "internalIps": {
        },
        "brokerDnsAliases": (
            "prod",  #DVTest broker will also appear as broker.prod.mdce to accommodate CMIUs that have been newly transferred across from the production billing account.
        )
    }
}
