#!yaml_jinja

# MDCE HTTPS certificates for non-ELB environments


/etc/mdce/https-certs/star_mdce-nonprod_neptunensight_com.key:
  file.managed:
    - source: salt://galileo/https-certs/star_mdce-nonprod_neptunensight_com.key
    - user: mdce
    - group: mdce
    - mode: 444
    - makedirs: True
    - require:
      - user: mdce-user
    - require_in:
      - service: mdce-tomcat
    - watch_in:
      - service: mdce-tomcat

/etc/mdce/https-certs/star_mdce-nonprod_neptunensight_com.crt:
  file.managed:
    - source: salt://galileo/https-certs/star_mdce-nonprod_neptunensight_com.crt
    - user: mdce
    - group: mdce
    - mode: 444
    - makedirs: True
    - require:
      - user: mdce-user
    - require_in:
      - service: mdce-tomcat
    - watch_in:
      - service: mdce-tomcat

/etc/mdce/https-certs/DigiCertCA.crt:
  file.managed:
    - source: salt://galileo/https-certs/DigiCertCA.crt
    - user: mdce
    - group: mdce
    - mode: 444
    - makedirs: True
    - require:
      - user: mdce-user
    - require_in:
      - service: mdce-tomcat
    - watch_in:
      - service: mdce-tomcat
