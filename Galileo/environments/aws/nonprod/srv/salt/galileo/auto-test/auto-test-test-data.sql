-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: mdce
-- ------------------------------------------------------
-- Server version	5.6.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `audit_list`
--

LOCK TABLES `audit_list` WRITE;
/*!40000 ALTER TABLE `audit_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `cellular_device_details`
--

LOCK TABLES `cellular_device_details` WRITE;
/*!40000 ALTER TABLE `cellular_device_details` DISABLE KEYS */;
INSERT INTO `cellular_device_details`(cellular_device_details_id,
`miu_id`,
`imei`,
`provisioning_state`,
`last_update_time`,
`modem_vendor`,
`modem_cellular_network`,
`modem_config_firmware`) VALUES 
(1,400000102,'353238060024824', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(2,400000104,'990000560449458',1,'2015-06-15 16:30:50', 'Telit','VZW', '0.0.0.0'),
(3,400000210,'353238060161527', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(4,400000212,'353238060161022', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(5,400000214,'353238060162780', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(6,400000216,'353238060161337', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(7,400000218,'353238060160628', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(8,400000220,'353238060161568', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(9,400000222,'353238060161352', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(10,400000224,'353238060160693', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(11,400000226,'353238060161501', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(12,400000228,'353238060161485', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(13,400000230,'353238060162715', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(14,400000232,'353238060162665', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(15,400000234,'353238060162251', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(16,400000236,'353238060163135', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(17,400000238,'353238060161824', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(18,400000240,'353238060163283', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(19,400000242,'353238060162160', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(20,400000244,'353238060161790', NULL,NULL, 'Telit','VZW', '0.0.0.0'),
(21,400000246,'999999999999999', NULL,NULL, 'Telit','ATT', '0.0.0.0'),
(22,400000248,'353238060161792', NULL,NULL, 'Telit','ATT', '0.0.0.0')
ON DUPLICATE KEY UPDATE
`miu_id` = VALUES(miu_id),
`imei` = VALUES(imei),
`provisioning_state` = VALUES(provisioning_state),
`last_update_time` = VALUES(last_update_time),
`modem_vendor` = VALUES(modem_vendor),
`modem_cellular_network` = VALUES(modem_cellular_network),
`modem_config_firmware` = VALUES(modem_config_firmware);




/*!40000 ALTER TABLE `cellular_device_details` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `sim_details` WRITE;
/*!40000 ALTER TABLE `sim_details` DISABLE KEYS */;

UPDATE `sim_details` set miu_id = NULL
WHERE `miu_id` = 400000102 OR
`miu_id` = 400000104 OR
`miu_id` = 400000210 OR
`miu_id` = 400000212 OR
`miu_id` = 400000214 OR
`miu_id` = 400000216 OR
`miu_id` = 400000218 OR
`miu_id` = 400000220 OR
`miu_id` = 400000222 OR
`miu_id` = 400000224 OR
`miu_id` = 400000226 OR
`miu_id` = 400000228 OR
`miu_id` = 400000230 OR
`miu_id` = 400000232 OR
`miu_id` = 400000234 OR
`miu_id` = 400000236 OR
`miu_id` = 400000238 OR
`miu_id` = 400000240 OR
`miu_id` = 400000242 OR
`miu_id` = 400000244 OR
`miu_id` = 400000246 OR
`miu_id` = 400000248;

-- clear out ICCIDs that match our standard ones
UPDATE `sim_details` set iccid = 10000000000000000000 + sim_details_id
WHERE `iccid` in 
('89148000001471859842',
'89148000001471859586',
'89148000002036030226',
'89148000001471859602',
'89148000001471859776',
'89148000001471859628',
'89148000001471859891',
'89148000001471859693',
'89148000001471859966',
'89148000001471859735',
'89148000001471859875',
'89148000001471859883',
'89148000001471860006',
'89148000001471859982',
'89148000001471859933',
'89148000001471865369',
'89148000001471859925',
'89148000001471859917',
'89148000001471859958',
'89148000001471859974',
'89148000001471859976',
'99999999999999999999');

INSERT INTO `sim_details`(sim_details_id,
`miu_id`,
`iccid`,
`msisdn`,
`imsi`,
`sim_cellular_network`) VALUES 
(1,400000102,'89148000001471859842','3344159614', '111','VZW'),
(2,400000104,'89148000001471859586','3344159626','111','VZW'),
(3,400000210,'89148000002036030226','12057251234','111','VZW'),
(4,400000212,'89148000001471859602','3344159786','111','VZW'),
(5,400000214,'89148000001471859776','3344159799','111','VZW'),
(6,400000216,'89148000001471859628','3344159839','111','VZW'),
(7,400000218,'89148000001471859891','3344159849','111','VZW'),
(8,400000220,'89148000001471859693','3344159852','111','VZW'),
(9,400000222,'89148000001471859966','3344159390','111','VZW'),
(10,400000224,'89148000001471859735','3344151054','111','VZW'),
(11,400000226,'89148000001471859875','3344151105','111','VZW'),
(12,400000228,'89148000001471859883','3344151198','111','VZW'),
(13,400000230,'89148000001471860006','3344151267','111','VZW'),
(14,400000232,'89148000001471859982','3344151361','111','VZW'),
(15,400000234,'89148000001471859933','3344151387','111','VZW'),
(16,400000236,'89148000001471865369','3344151413','111','VZW'),
(17,400000238,'89148000001471859925','3344151509','111','VZW'),
(18,400000240,'89148000001471859917','3344151618','111','VZW'),
(19,400000242,'89148000001471859958','3344151651','111','VZW'),
(20,400000244,'89148000001471859974','3344151697','111','VZW'),
(21,400000246,'99999999999999999999','9999999999','111','ATT'),
(22,400000248,'89148000001471859976','3344151699','111','VZW')
ON DUPLICATE KEY UPDATE
`miu_id` = VALUES(miu_id),
`iccid` = VALUES(iccid),
`msisdn` = VALUES(msisdn),
`imsi` = VALUES(imsi),
`sim_cellular_network` = VALUES(sim_cellular_network);




/*!40000 ALTER TABLE `sim_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `cmiu_config_mgmt`
--

LOCK TABLES `cmiu_config_mgmt` WRITE;
/*!40000 ALTER TABLE `cmiu_config_mgmt` DISABLE KEYS */;

INSERT INTO `cmiu_config_mgmt`
(`miu_id`,
`current_config`,
`current_config_apply_date`,
`default_config`,
`planned_config`,
`planned_config_apply_date`,
`planned_config_revert_date`,
`reported_config`,
`reported_config_date`)
VALUES (400000102,1,NULL,1,1,NULL,NULL,1,NULL)
ON DUPLICATE KEY UPDATE
`current_config` = VALUES(current_config),
`current_config_apply_date` = VALUES(current_config_apply_date),
`default_config` = VALUES(default_config),
`planned_config` = VALUES(planned_config),
`planned_config_apply_date` = VALUES(planned_config_apply_date),
`planned_config_revert_date` = VALUES(planned_config_revert_date),
`reported_config` = VALUES(reported_config),
`reported_config_date`  = VALUES(reported_config_date);


/*!40000 ALTER TABLE `cmiu_config_mgmt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `cmiu_config_set`
--

LOCK TABLES `cmiu_config_set` WRITE;
/*!40000 ALTER TABLE `cmiu_config_set` DISABLE KEYS */;

INSERT INTO `mdce`.`cmiu_config_set`
(`id`,
`name`,
`billing_plan_id`,
`reporting_start_mins`,
`reporting_interval_mins`,
`reporting_number_of_retries`,
`reporting_transmit_windows_mins`,
`reporting_quiet_start_mins`,
`reporting_quiet_end_mins`,
`reporting_sync_warning_mins`,
`reporting_sync_error_mins`,
`recording_start_time_mins`,
`recording_interval_mins`)
VALUES (1,'Config Set 1',1,0,60,0,15,0,0,0,0,0,15),(2,'Config Set 2',1,0,1440,3,5,1080,1320,0,0,360,60),(21,'Custom configSet from cmiu 1',1,30,240,3,56,12,34,NULL,NULL,10,20)
ON DUPLICATE KEY UPDATE
`name` = VALUES(name),
`billing_plan_id` = VALUES(billing_plan_id),
`reporting_start_mins` = VALUES(reporting_start_mins),
`reporting_interval_mins` = VALUES(reporting_interval_mins),
`reporting_number_of_retries` = VALUES(reporting_number_of_retries),
`reporting_transmit_windows_mins` = VALUES(reporting_transmit_windows_mins),
`reporting_quiet_start_mins` = VALUES(reporting_quiet_start_mins),
`reporting_quiet_end_mins`  = VALUES(reporting_quiet_end_mins),
`reporting_sync_warning_mins` = VALUES(reporting_sync_warning_mins),
`reporting_sync_error_mins` = VALUES(reporting_sync_error_mins),
`recording_start_time_mins` = VALUES(recording_start_time_mins),
`recording_interval_mins` = VALUES(recording_interval_mins);


/*!40000 ALTER TABLE `cmiu_config_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `miu_details`
--

LOCK TABLES `miu_details` WRITE;
/*!40000 ALTER TABLE `miu_details` DISABLE KEYS */;
INSERT INTO `miu_details`
(`miu_id`,
`site_id`,
`miu_type`,
`last_insert_date`,
`first_insert_date`,
`last_heard_time`,
`next_sync_time`,
`next_sync_time_warning`,
`next_sync_time_error`,
`meter_active`,
`last_state_change_timestamp`,
`state`)
VALUES (400000102,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000104,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000202,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000210,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000212,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000214,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000216,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000218,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000220,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000222,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000224,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000226,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000228,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(500000000,59111,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(500000002,59111,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(500000004,59111,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(500000006,59111,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(500000008,59111,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(500000010,59111,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(200,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(202,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(204,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(206,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(208,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(210,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000230,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000232,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000234,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000236,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000238,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000240,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000242,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000244,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000246,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated'),
(400000248,2370,1,NULL,NULL,NULL,NULL,NULL,NULL,'Y', NULL, 'Activated')
ON DUPLICATE KEY UPDATE
`site_id` = VALUES(site_id),
`miu_type` = VALUES(miu_type),
`last_insert_date` = VALUES(last_insert_date),
`first_insert_date` = VALUES(first_insert_date),
`last_heard_time` = VALUES(last_heard_time),
`next_sync_time` = VALUES(next_sync_time),
`next_sync_time_warning` = VALUES(next_sync_time_warning),
`next_sync_time_error` = VALUES(next_sync_time_error),
`meter_active` = VALUES(meter_active);


--
-- Dumping data for table `site_list`
--

LOCK TABLES `site_list` WRITE;
/*!40000 ALTER TABLE `site_list` DISABLE KEYS */;
INSERT INTO `site_list` (`site_id`,
`name`,
`last_data_request`)
VALUES (1,'MDCE Test Site',NULL),
(59111,'JG Test Site',NULL)
ON DUPLICATE KEY UPDATE
name = VALUES(name),
last_data_request = VALUES(last_data_request);

/*!40000 ALTER TABLE `site_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user_list`
--

LOCK TABLES `user_list` WRITE;
/*!40000 ALTER TABLE `user_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_list` ENABLE KEYS */;
INSERT INTO mdce.user_list (user_id, user_name, user_email, user_password, user_salt, user_pbkdf2_inter, user_level, account_lockout_enabled, allow_concurrent_sessions) 
VALUES (1, 'admin', 'nobody@email.com', 0x05A84937B5F1FEA2EFA802A3B7969A569CE13DD1AE2A07EFE82A3B01B549DAF4, 0xE4B6DDC5F704B455157817664CD86D7E3BC2244ED18345690EC6BC62309FD535, 1000, 'MsocAdmin', 0, 1)
ON DUPLICATE KEY UPDATE
user_name = VALUES(user_name),
user_email = VALUES(user_email), 
user_password = VALUES(user_password), 
user_salt = VALUES(user_salt), 
user_pbkdf2_inter = VALUES(user_pbkdf2_inter),
user_level = VALUES(user_level),
account_lockout_enabled = VALUES(account_lockout_enabled),
allow_concurrent_sessions = VALUES(allow_concurrent_sessions)
;

UNLOCK TABLES;

--
-- Data for L900 tests
--

LOCK TABLES `miu_details` WRITE;
INSERT INTO mdce.miu_details (miu_id, miu_type, site_id) VALUES
(700000200, 2, 1),
(700000202, 2, 1),
(700000204, 2, 1),
(700000206, 2, 1),
(700000208, 2, 1),
(700000210, 2, 1),
(700000212, 2, 1)
ON DUPLICATE KEY UPDATE
miu_type = VALUES(miu_type),
site_id = VALUES(site_id);
UNLOCK TABLES;

