#!yaml_jinja

# applies the DDL to the DB when it changes

{% from "aws-info.map.jinja" import awsInfo %}
{% from "mdce-env-spec.map.jinja" import envs %}

{% set REGION = 'us-west-2' %}
{% set ENV_NAME = grains.env %}

{% set rdsEndpoint = awsInfo[REGION]['rdsEndpoint']["mdce-" + ENV_NAME + "-rds"] %}
{% set rdsMasterPassword = envs[ENV_NAME]['rdsMasterUserPassword'] %}
{% set testDataSqlFile = "/etc/mdce/"~ENV_NAME~"-test-data.sql" %}


{{ testDataSqlFile }}:
  file.managed:
    - source: salt://galileo/{{ ENV_NAME }}/{{ ENV_NAME }}-test-data.sql
    - user: mdce
    - group: mdce
    - mode: 644
    - makedirs: True

mysql -h {{ rdsEndpoint }} -u mdcerdsmaster -p{{ rdsMasterPassword }} mdce < {{ testDataSqlFile }}:
  cmd.wait:
    - watch:
      - cmd: mdce-execute-database-migration
      - file: {{ testDataSqlFile }}
    - require:
      - user: mdce-user
      - pkg: mariadb
