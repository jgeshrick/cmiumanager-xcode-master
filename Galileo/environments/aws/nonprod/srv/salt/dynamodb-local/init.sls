#!yaml_jinja

# DynamoDB local
# see http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Tools.DynamoDBLocal.html

# if you do a GET from the following address, you can find the latest version number in the returned filename:
# http://dynamodb-local.s3-website-us-west-2.amazonaws.com/dynamodb_local_latest.tar.gz

{% set DYNAMODB_LOCAL_VERSION = '2015-07-16_1.0' %}


dynamodb-group:
  group.present:
    - name: dynamodb

dynamodb-user:
  user.present:
    - name: dynamodb
    - groups: 
      - dynamodb
    - home: /home/dynamodb
    - shell: /bin/bash
    - require:
      - group: dynamodb-group


/opt/dynamodb-local-{{ DYNAMODB_LOCAL_VERSION }}:
  file.directory:
    - user: dynamodb
    - group: dynamodb
    - mode: 755
    - makedirs: True
    - require:
      - file: /opt
      - user: dynamodb-user


/opt/dynamodb-local:
  file.symlink:
    - target: /opt/dynamodb-local-{{ DYNAMODB_LOCAL_VERSION }}
    - require:
      - file: /opt/dynamodb-local-{{ DYNAMODB_LOCAL_VERSION }}
      

salt://dynamodb-local/download_dynamodb_local.sh:
  cmd.script:
    - env:
      - DYNAMODB_LOCAL_URL: http://dynamodb-local.s3-website-us-west-2.amazonaws.com/dynamodb_local_{{ DYNAMODB_LOCAL_VERSION }}.tar.gz
      - DYNAMODB_LOCAL_VERSION: {{ DYNAMODB_LOCAL_VERSION }}
    - require:
      - file: /opt/dynamodb-local-{{ DYNAMODB_LOCAL_VERSION }}
    - unless:
      - ls /opt/dynamodb-local-{{ DYNAMODB_LOCAL_VERSION }}/DynamoDBLocal.jar


/lib/systemd/system/dynamodb-local.service:
  file.managed:
    - source: salt://dynamodb-local/systemd/dynamodb-local.service
    - user: root
    - group: root
    - mode: 555

/etc/sysconfig/dynamodb-local.environment:
  file.managed:
    - source: salt://dynamodb-local/systemd/dynamodb-local.environment
    - user: root
    - group: root
    - mode: 555


mkfs -t ext4 /dev/xvdb:
  cmd.run:
    - unless:
      - blkid /dev/xvdb | grep "ext4"

/var/dynamodb-local-data/:
  mount.mounted:
    - device: /dev/xvdb
    - fstype: ext4
    - mkmnt: True
    - persist: True
    - opts:
      - defaults
    - watch:
      - cmd: mkfs -t ext4 /dev/xvdb

/var/dynamodb-local-data/db:
  file.directory:
    - user: dynamodb
    - group: dynamodb
    - mode: 755
    - require:
      - mount: /var/dynamodb-local-data/

dynamodb-local:
  service.running:
    - enable: True
    - require:
      - file: /lib/systemd/system/dynamodb-local.service
      - file: /etc/sysconfig/dynamodb-local.environment
      - file: /var/dynamodb-local-data/db
    - onlyif:
      - ls /opt/dynamodb-local/DynamoDBLocal.jar
    - watch:
      - file: /opt/dynamodb-local
