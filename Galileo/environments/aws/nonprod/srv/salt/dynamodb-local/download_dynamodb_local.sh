#Script to download dynamodb-local. 
#Ensure this file has Unix line ends

curl ${DYNAMODB_LOCAL_URL} | tar xz -C /opt/dynamodb-local-${DYNAMODB_LOCAL_VERSION}
chown -R dynamodb:dynamodb /opt/dynamodb-local-${DYNAMODB_LOCAL_VERSION}
cat /opt/dynamodb-local-${DYNAMODB_LOCAL_VERSION}/README.txt
