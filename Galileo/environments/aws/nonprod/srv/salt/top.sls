#!yaml_jinja

# Top file for MDCE nonproduction environment management.
# See http://docs.saltstack.com/en/latest/ref/states/top.html

base:
  '*':
    - network
    - timezone
    - hostname

  'aw-mdce-*-bst*':
    - hosts.not-salt-master
    - bastion-hardening

  'aw-mdce-int-brk01':  #salt-master
    - aws.aws-account-jonsaws
    - aws.aws-auto-test
    - aws.aws-dev1
    - aws.aws-int
    - aws.aws-preprod.security-groups
    - aws.aws-preprod.ec2
    - aws.aws-preprod.elb
    - aws.aws-sqa
    - aws.aws-dvtest
    - monitoring
    - hosts.salt-master

  'aw-mdce-auto-test-brk*':
    - nfs.fake
    - vsftpd
    - vsftpd.conf-public-ip-override.conf
    - hosts.not-salt-master
  'aw-mdce-int-brk*':
    - nfs.fake
    - tcpdump
    - vsftpd
    - vsftpd.conf-no-ip-override.conf
  'aw-mdce-sqa-brk*':
    - nfs.fake
    - tcpdump
    - vsftpd
    - vsftpd.conf-no-ip-override.conf
    - hosts.not-salt-master
  'aw-mdce-dvtest-brk*':
    - nfs.fake
    - tcpdump
    - vsftpd
    - vsftpd.conf-no-ip-override.conf
    - hosts.not-salt-master
  'aw-mdce-dev*-brk*':
    - nfs.fake
    - tcpdump
    - vsftpd
    - vsftpd.conf-no-ip-override.conf
    - hosts.not-salt-master
  'aw-mdce-preprod-brk*':
    - nfs.broker
    - bind
    - vsftpd
    - vsftpd.conf-no-ip-override.conf
    - hosts.not-salt-master

  'aw-mdce-*-brk*':
    - apache
    - aws-scripts-mon
    - mosquitto

  'aw-mdce-*-app*':
    - java
    - galileo
    - mdce
    - fpc
    - tomcat
    - aws-scripts-mon
    - aws-scripts-mon.mdce-metrics
    - vsftpd.test-files-for-application-server
    - hosts.not-salt-master

  'aw-mdce-auto-test-app*':   #Note: we don't specify tomcat.systemd.small for auto-test because the process size can conflict with dynamodb-local
    - nfs.fake
    - galileo.https-certs
    - tomcat.native-apr          
    - tomcat.conf-no-elb    
    - tomcat.systemd.tiny
  'aw-mdce-int-app*':     
    - nfs.fake
    - galileo.https-certs
    - tomcat.native-apr          
    - tomcat.conf-no-elb
    - tomcat.systemd.small
  'aw-mdce-sqa-app*':     
    - nfs.fake
    - galileo.https-certs
    - tomcat.native-apr          
    - tomcat.conf-no-elb
    - tomcat.systemd.small
  'aw-mdce-dvtest-app*':     
    - nfs.fake
    - galileo.https-certs
    - tomcat.native-apr          
    - tomcat.conf-no-elb
    - tomcat.systemd.small
  'aw-mdce-dev*-app*':
    - nfs.fake
    - galileo.https-certs
    - tomcat.native-apr          
    - tomcat.conf-no-elb  
    - tomcat.systemd.small
  'aw-mdce-preprod-app*':     
    - nfs.apps
    - tomcat.conf-behind-elb
    - tomcat.systemd.large

  'aw-mdce-*-app01':
    - mysql
    - galileo.galileo-db-migrate
    - galileo.apn-data
    - mdce.mdce-scheduler
    
  'aw-mdce-auto-test-app01':
    - dynamodb-local
    - mosquitto                 #isolated Mosquitto broker that no mdce-mqtt instance is listening to.  Useful for testing.
    - galileo.auto-test.test-data
