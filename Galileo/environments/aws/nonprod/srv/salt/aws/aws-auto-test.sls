#!yaml_jinja
{% from "aws-info.map.jinja" import awsInfo %}
{% from "mdce-env-spec.map.jinja" import trustedCidrs %}
{% from "mdce-env-spec.map.jinja" import vpcs %}
{% from "mdce-env-spec.map.jinja" import envs %}
{% from "mdce-env-spec.map.jinja" import MNO_PREFIX %}

# AWS state file for auto-test environment
# See https://docs.saltstack.com/en/latest/ref/states/all/salt.states.boto_secgroup.html

# Note - salt.state.boto_secgroup doesn't seem able to apply tags at the moment (June 2015).  We'll need to tag the security groups etc. afterwards

{% set ENV_NAME = 'auto-test' %}
{% set ACCOUNT_ID = '934427565871' %}
{% set CENTOS_AMI = 'ami-c7d092f7' %} {# CentOS 7 x86_64 (2014_09_29) EBS HVM-b7ee8a69-ee97-4a49-9e68-afaee216db2e-ami-d2a117ba.2 in us-west-2 #}

{% set CLOUD_PROVIDER = 'aws-nonprod-ec2' %}

{% set mnoVpcInfo = vpcs[envs[ENV_NAME]["mnoVpc"]] %}
{% set REGION = mnoVpcInfo["region"] %}
{% set MNO_VPC_ID = awsInfo[REGION]["vpcId"][mnoVpcInfo["vpcName"]] %}

{% set mdceVpcInfo = vpcs[envs[ENV_NAME]["mdceVpc"]] %}
{% set MDCE_VPC_ID = awsInfo[REGION]["vpcId"][mdceVpcInfo["vpcName"]] %}

{% set MNO_VPC_IP_24BIT = envs[ENV_NAME]["mnoVpcCidr"] | replace(".0/24", "") %}
{% set MDCE_VPC_IP_24BIT = envs[ENV_NAME]["mdceVpcCidr"] | replace(".0/24", "") %}
{% set RDS_ENDPOINT_NAME_SUFFIX = envs[ENV_NAME]["rdsEndpointNameSuffix"] %}
{% set BROKER_MANAGEMENT_SG_ID = awsInfo[REGION]["sgId"][MNO_VPC_ID][MNO_PREFIX~"nonprod-management-security-group"] %}
{% set APPS_MANAGEMENT_SG_ID = awsInfo[REGION]["sgId"][MDCE_VPC_ID]["mdce-dev-management-security-group"] %}

{% set CLOUDWATCH_SNS_ARN = "arn:aws:sns:"+REGION+":"+ACCOUNT_ID+":"+ENV_NAME+"-cloudwatch-sns-topic" %}
{% set CLOUDWATCH_SQS_ARN = "arn:aws:sqs:"+REGION+":"+ACCOUNT_ID+":"+ENV_NAME+"-cloudwatch" %}
{% set EMAIL_ALERT_SNS_ARN = "arn:aws:sns:"+REGION+":"+ACCOUNT_ID+":aws-nonprod-alarm-recipients" %}

{{ ENV_NAME }}-cloudwatch:
  boto_sqs.present:
    - region: {{ REGION }}
    - attributes:
        Policy: '{"Version": "2012-10-17", "Statement": [{"Effect": "Allow", "Principal": {"AWS": "*"},"Action": "SQS:SendMessage","Resource": "{{ CLOUDWATCH_SQS_ARN }}","Condition": {"ArnEquals": {"aws:SourceArn": "{{ CLOUDWATCH_SNS_ARN }}"}}}]}'

{{ ENV_NAME }}-cloudwatch-sns-topic:
  boto_sns.present:
    - region: {{ REGION }}
    - subscriptions:
      - protocol: sqs
        endpoint: {{ CLOUDWATCH_SQS_ARN }}

{{ MNO_PREFIX~ENV_NAME }}-broker-security-group:
    boto_secgroup.present:
        - description: Permit MQTT access to Mosquitto and FTP access
        - vpc_id: {{ MNO_VPC_ID }}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 1883
            to_port: 1883
            cidr_ip:
                - 10.0.0.0/8
{%- for cidr in trustedCidrs %}
                - {{ cidr }}
{%- endfor %}
          - ip_protocol: tcp
            from_port: 80
            to_port: 80
            cidr_ip:
                - 10.0.0.0/8
{%- for cidr in trustedCidrs %}
                - {{ cidr }}
{%- endfor %}
          - ip_protocol: udp
            from_port: 53
            to_port: 53
            cidr_ip:
                - 10.0.0.0/8
{%- for cidr in trustedCidrs %}
                - {{ cidr }}
{%- endfor %}
          - ip_protocol: tcp
            from_port: 53
            to_port: 53
            cidr_ip:
                - 10.0.0.0/8
{%- for cidr in trustedCidrs %}
                - {{ cidr }}
{%- endfor %}
        - region: {{ REGION }}
        - profile: salt-boto-aws-management

{{ MNO_PREFIX~ENV_NAME }}-ftptest-security-group:
    boto_secgroup.present:
        - name: '{{ MNO_PREFIX~ENV_NAME }}-ftptest-security-group'
        - description: Access to ftp server tests on {{ ENV_NAME }}
        - vpc_id: {{ MNO_VPC_ID }}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 21
            to_port: 21
            cidr_ip:
                - 10.0.0.0/8
{%- for cidr in trustedCidrs %}
                - {{ cidr }}
{%- endfor %}
          - ip_protocol: tcp
            from_port: 65000
            to_port: 65534
            cidr_ip:
                - 10.0.0.0/8
{%- for cidr in trustedCidrs %}
                - {{ cidr }}
{%- endfor %}
        - region: {{ REGION }}
        - profile: salt-boto-aws-management


mdce-{{ ENV_NAME }}-applications-security-group:
    boto_secgroup.present:
        - name: 'mdce-{{ ENV_NAME }}-applications-security-group'
        - description: Access to mdce-integration, mdce-mqtt and mdce-web for {{ ENV_NAME }}
        - vpc_id: {{ MDCE_VPC_ID }}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 1883
            to_port: 1883       #Isolated MQTT instance that no mdce-mqtt is listening to.  Called from integration test (Sagentia IP, which is one of the trustedIps)
            cidr_ip:
{%- for cidr in trustedCidrs %}
                - {{ cidr }}
{%- endfor %}
                - 10.0.0.0/8
          - ip_protocol: tcp
            from_port: 8080     #mdce-mqtt
            to_port: 8083       #mdce-lora
            cidr_ip:
              - 10.0.0.0/8      #access from anywhere in LAN
          - ip_protocol: tcp
            from_port: 9080     #mdce-web external
            to_port: 9081       #mdce-web MSOC
            cidr_ip:
              - 10.0.0.0/8      #access from anywhere in LAN
          - ip_protocol: tcp
            from_port: 8444     #mdce-integration HTTPS
            to_port: 8444       
            cidr_ip:
              - 0.0.0.0/0
          - ip_protocol: tcp
            from_port: 9001     #mdce-integration HTTPS for VZW callback
            to_port: 9001
            cidr_ip:
              - 0.0.0.0/0
          - ip_protocol: tcp
            from_port: 443      #mdce-web external HTTPS
            to_port: 443
            cidr_ip:
              - 0.0.0.0/0
          - ip_protocol: tcp
            from_port: 8443     #mdce-mqtt HTTPS
            to_port: 8446       #mdce-lora HTTPS
            cidr_ip:
{%- for cidr in trustedCidrs %}
                - {{ cidr }}
{%- endfor %}
          - ip_protocol: tcp
            from_port: 8000     #DynamoDB-local
            to_port: 9999       #Trusted CIDRs can access 8000-9999 in auto-test
            cidr_ip:
{% for cidr in trustedCidrs %}
                - {{ cidr }}
{% endfor %}
        - region: {{ REGION }}
        - profile: salt-boto-aws-management

mdce-{{ ENV_NAME }}-rds-security-group:
    boto_secgroup.present:
        - name: 'mdce-{{ ENV_NAME }}-rds-security-group'
        - description: Permit access to MySQL RDS for {{ ENV_NAME }}
        - vpc_id: {{ MDCE_VPC_ID }}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 3306
            to_port: 3306
            cidr_ip:
              - {{MDCE_VPC_IP_24BIT}}.0/26      #mdce public subnet
        - region: {{ REGION }}
        - profile: salt-boto-aws-management


{% if awsInfo[REGION]["sgId"][MNO_VPC_ID][MNO_PREFIX~ENV_NAME~"-broker-security-group"] is defined
and awsInfo[REGION]["sgId"][MDCE_VPC_ID][MNO_PREFIX~ENV_NAME~"-ftptest-security-group"] is defined
and awsInfo[REGION]["sgId"][MDCE_VPC_ID]["mdce-"~ENV_NAME~"-applications-security-group"] is defined %}

{% set SERVER_NAME = "aw-mdce-" + ENV_NAME + "-brk01" %}

{% set EIP = envs[ENV_NAME]["elasticIps"][SERVER_NAME] %}
{% set IIP = envs[ENV_NAME]["internalIps"][SERVER_NAME] %}
{{ SERVER_NAME }}:
    cloud.present:
        - grains:
            env: {{ ENV_NAME }}
            defaultUser: centos
        - cloud_provider: {{ CLOUD_PROVIDER }}
        - image: {{ CENTOS_AMI }}
        - size: 't2.micro'
        - ssh_username: centos
        - location: {{ REGION }}
        - delvol_on_destroy: True
        - iam_profile: 'arn:aws:iam::{{ ACCOUNT_ID }}:instance-profile/mdce-aws-service-access-role'
        - network_interfaces:
            - DeviceIndex: 0
              PrivateIpAddresses:
                - Primary: True
{%- if IIP is defined %}                
                  PrivateIpAddress: {{ IIP }}
{%- endif %}
{%- if EIP is defined %}
              associate_eip: {{ awsInfo[REGION]["elasticIpAllocationId"][EIP] }}
{%- else %}
              AssociatePublicIpAddress: True     #Note - actually doesn't work in salt 2015.5.3 (Lithium) (current version as of Aug 3, 2015).  The auto-assign setting on the subnet is used instead.  See https://github.com/saltstack/salt/issues/19288
{%- endif %}
              SubnetId: {{ awsInfo[REGION]["subnetId"][MNO_VPC_ID][MNO_PREFIX~ENV_NAME~"-public-subnet"] }}
              SecurityGroupId:
                - {{ BROKER_MANAGEMENT_SG_ID }}
                - {{ awsInfo[REGION]["sgId"][MNO_VPC_ID][MNO_PREFIX~ENV_NAME~"-broker-security-group"] }} 
                - {{ awsInfo[REGION]["sgId"][MNO_VPC_ID][MNO_PREFIX~ENV_NAME~"-ftptest-security-group"] }} 
        - tag:
            'Env': '{{ ENV_NAME }}'


{% if awsInfo[REGION]["ec2Id"][SERVER_NAME] is defined %}

{% set ALARM_NAME = SERVER_NAME + " ping test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Ping
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: ping status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server: 
            - {{ SERVER_NAME }}
          Environment: 
            - {{ ENV_NAME }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = "aw-mdce-" + ENV_NAME + "-rds storage space low" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: FreeStorageSpace
        namespace: AWS/RDS
        statistic: Average
        comparison: "<"
        threshold: 1000000000
        period: 300
        valuation_periods: 1
        description: RDS capacity low - risk of environment failure
        dimensions:
          DBInstanceIdentifier:
            - mdce-{{ ENV_NAME }}{{ RDS_ENDPOINT_NAME_SUFFIX }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions:
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " status check failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: StatusCheckFailed
        namespace: AWS/EC2
        statistic: Maximum
        comparison: ">"
        threshold: 0.0
        period: 300
        evaluation_periods: 1
        description: AWS status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " disk usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: DiskSpaceUtilization
        namespace: System/Linux
        statistic: Average
        comparison: ">="
        threshold: 90.0
        period: 300
        evaluation_periods: 1
        description: Disk is 90% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          Filesystem: 
            - /dev/xvda1
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
          MountPath: 
            - /
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions:
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " memory usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: MemoryUtilization
        namespace: System/Linux
        statistic: Average
        comparison: ">="
        threshold: 80.0
        period: 300
        evaluation_periods: 2
        description: RAM is 80% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% endif %} # endif EC2 ID available

{% set SERVER_NAME = "aw-mdce-" + ENV_NAME + "-app01" %}
{% set EIP = envs[ENV_NAME]["elasticIps"][SERVER_NAME] %}
{% set IIP = envs[ENV_NAME]["internalIps"][SERVER_NAME] %}
{{ SERVER_NAME }}:
    cloud.present:
        - grains:
            env: {{ ENV_NAME }}
            defaultUser: centos
        - cloud_provider: {{ CLOUD_PROVIDER }}
        - image: {{ CENTOS_AMI }}
        - size: 't2.large'
        - ssh_username: centos
        - location: {{ REGION }}
        - delvol_on_destroy: True
        - iam_profile: 'arn:aws:iam::{{ ACCOUNT_ID }}:instance-profile/mdce-aws-service-access-role'
        - network_interfaces:
            - DeviceIndex: 0
              PrivateIpAddresses:
                - Primary: True
{%- if IIP is defined %}                
                  PrivateIpAddress: {{ IIP }}
{%- endif %}
{%- if EIP is defined %}
              associate_eip: {{ awsInfo[REGION]["elasticIpAllocationId"][EIP] }}
{%- else %}
              AssociatePublicIpAddress: True     #Note - actually doesn't work in salt 2015.5.3 (Lithium) (current version as of Aug 3, 2015).  The auto-assign setting on the subnet is used instead.  See https://github.com/saltstack/salt/issues/19288
{%- endif %}
              SubnetId: {{ awsInfo[REGION]["subnetId"][MDCE_VPC_ID]["mdce-"~ENV_NAME~"-public-subnet"] }}
              SecurityGroupId:
                - {{ APPS_MANAGEMENT_SG_ID }}
                - {{ awsInfo[REGION]["sgId"][MDCE_VPC_ID]["mdce-"~ENV_NAME~"-applications-security-group"] }} 
        - block_device_mappings:
            - DeviceName: /dev/sdb
              Ebs.VolumeSize: 10
              Ebs.VolumeType: 'gp2'
        - tag:
            'Env': '{{ ENV_NAME }}'


{% if awsInfo[REGION]["ec2Id"][SERVER_NAME] is defined %}

{% set ALARM_NAME = SERVER_NAME + " ping test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Ping
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: ping status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server:
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " mdce-lora app test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Heartbeat
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: mdce-lora status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server:
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application:
            - mdce-lora
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " mdce-mqtt app test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Heartbeat
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: mqtt status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server:
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application:
            - mdce-mqtt
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " fpc app test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Heartbeat
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: fpc status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server:
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application:
            - fpc
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " mdce-integration app test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: Heartbeat
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: integration status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server:
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application:
            - mdce-integration
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " whitelisted web access test failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: WhitelistedWebAccess
        namespace: System/Server/Status
        statistic: Average
        comparison: "<"
        threshold: 0.7
        period: 300
        evaluation_periods: 2
        description: whitelisted web access status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          Server:
            - {{ SERVER_NAME }}
          Environment:
            - {{ ENV_NAME }}
          Application:
            - mdce-web
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " status check failed" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: StatusCheckFailed
        namespace: AWS/EC2
        statistic: Maximum
        comparison: ">"
        threshold: 0.0
        period: 300
        evaluation_periods: 1
        description: AWS status check failed for AWS server instance {{ SERVER_NAME }}
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " disk usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: DiskSpaceUtilization
        namespace: System/Linux
        statistic: Average
        comparison: ">="
        threshold: 90.0
        period: 300
        evaluation_periods: 1
        description: Disk is 90% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          Filesystem: 
            - /dev/xvda1
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
          MountPath: 
            - /
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions: 
          - {{ CLOUDWATCH_SNS_ARN }}

{% set ALARM_NAME = SERVER_NAME + " memory usage high" %}
{{ ALARM_NAME }}:
  boto_cloudwatch_alarm.present:
    - region: {{ REGION }}
    - attributes:
        metric: MemoryUtilization
        namespace: System/Linux
        statistic: Average
        comparison: ">="
        threshold: 90.0
        period: 300
        evaluation_periods: 2
        description: RAM is 90% full on AWS server instance {{ SERVER_NAME }}
        dimensions:
          InstanceId: 
            - {{ awsInfo[REGION]["ec2Id"][SERVER_NAME] }}
        alarm_actions:
          - {{ EMAIL_ALERT_SNS_ARN }}
          - {{ CLOUDWATCH_SNS_ARN }}
        insufficient_data_actions: [ ]
        ok_actions:
          - {{ CLOUDWATCH_SNS_ARN }}

{% endif %} # endif EC2 ID available

{% endif %} # endif security group IDs available
