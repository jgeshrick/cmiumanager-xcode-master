#!yaml_jinja
{% from "aws-info.map.jinja" import awsInfo %}
{% from "mdce-env-spec.map.jinja" import trustedCidrs %}
{% from "mdce-env-spec.map.jinja" import factoryCidrs %}
{% from "mdce-env-spec.map.jinja" import loraCidrs %}
{% from "mdce-env-spec.map.jinja" import vpcs %}
{% from "mdce-env-spec.map.jinja" import envs %}
{% from "mdce-env-spec.map.jinja" import MNO_PREFIX %}

# AWS state file for preprod environment - security groups, SQS and SNS
# See https://docs.saltstack.com/en/latest/ref/states/all/salt.states.boto_secgroup.html

# Note - salt.state.boto_secgroup doesn't seem able to apply tags at the moment (June 2015).  We'll need to tag the security groups etc. afterwards

{% set ENV_NAME = 'preprod' %}         {#- PREPROD-SPECIFIC #}
{% set ACCOUNT_ID = '934427565871' %}  {#- PREPROD-SPECIFIC #}

{% set mnoVpcInfo = vpcs[envs[ENV_NAME]["mnoVpc"]] %}
{% set REGION = mnoVpcInfo["region"] %}
{% set MNO_VPC_ID = awsInfo[REGION]["vpcId"][mnoVpcInfo["vpcName"]] %}

{% set mdceVpcInfo = vpcs[envs[ENV_NAME]["mdceVpc"]] %}
{% set MDCE_VPC_ID = awsInfo[REGION]["vpcId"][mdceVpcInfo["vpcName"]] %}

{% set MNO_VPC_IP_24BIT = envs[ENV_NAME]["mnoVpcCidr"] | replace(".0/24", "") %}
{% set MDCE_VPC_IP_16BIT = envs[ENV_NAME]["mdceVpcCidr"] | replace(".0.0/16", "") %}

{% set CLOUDWATCH_SNS_ARN = "arn:aws:sns:"+REGION+":"+ACCOUNT_ID+":"+ENV_NAME+"-cloudwatch-sns-topic" %}
{% set CLOUDWATCH_SQS_ARN = "arn:aws:sqs:"+REGION+":"+ACCOUNT_ID+":"+ENV_NAME+"-cloudwatch" %}
{% set DATAPIPELINE_SNS_ARN = "arn:aws:sns:"+REGION+":"+ACCOUNT_ID+":"+ENV_NAME+"-datapipeline-sns-topic" %}
{% set DATAPIPELINE_SQS_ARN = "arn:aws:sqs:"+REGION+":"+ACCOUNT_ID+":"+ENV_NAME+"-datapipeline" %}


{{ ENV_NAME }}-cloudwatch:
  boto_sqs.present:
    - region: {{ REGION }}
    - attributes:
        Policy: '{"Version": "2012-10-17", "Statement": [{"Effect": "Allow", "Principal": {"AWS": "*"},"Action": "SQS:SendMessage","Resource": "{{ CLOUDWATCH_SQS_ARN }}","Condition": {"ArnEquals": {"aws:SourceArn": "{{ CLOUDWATCH_SNS_ARN }}"}}}]}'

{{ ENV_NAME }}-cloudwatch-sns-topic:
  boto_sns.present:
    - region: {{ REGION }}
    - subscriptions:
      - protocol: sqs
        endpoint: {{ CLOUDWATCH_SQS_ARN }}

{{ ENV_NAME }}-datapipeline:
  boto_sqs.present:
    - region: {{ REGION }}
    - attributes:
        Policy: '{"Version": "2012-10-17", "Statement": [{"Effect": "Allow", "Principal": {"AWS": "*"},"Action": "SQS:SendMessage","Resource": "{{ DATAPIPELINE_SQS_ARN }}","Condition": {"ArnEquals": {"aws:SourceArn": "{{ DATAPIPELINE_SNS_ARN }}"}}}]}'

{{ ENV_NAME }}-datapipeline-sns-topic:
  boto_sns.present:
    - region: {{ REGION }}
    - subscriptions:
      - protocol: sqs
        endpoint: {{ DATAPIPELINE_SQS_ARN }}

mdce-{{ ENV_NAME }}-management-internal-security-group:
    boto_secgroup.present:
        - description: Provides SSH (port 22) access from the Bastion Host
        - vpc_id: {{ MDCE_VPC_ID }}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 22
            to_port: 22
            cidr_ip:
                - 10.120.0.0/16  {#- PREPROD-SPECIFIC #}
          - ip_protocol: icmp
            from_port: -1
            to_port: -1
            cidr_ip:
                - 10.120.0.0/16  {#- PREPROD-SPECIFIC #}
        - region: {{ REGION }}
        - profile: salt-boto-aws-management

{{ MNO_PREFIX~ENV_NAME }}-broker-internal-security-group:
    boto_secgroup.present:
        - description: Permit ELB access to Mosquitto and Apache, and CMIU access to FTP
        - vpc_id: {{ MNO_VPC_ID }}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 1883
            to_port: 1883
            cidr_ip:
                - {{ envs[ENV_NAME]["mnoVpcCidr"] }}
                - {{MDCE_VPC_IP_16BIT}}.2.0/24      #MNO private-a subnet
                - {{MDCE_VPC_IP_16BIT}}.3.0/24      #MNO private-b subnet {#- PREPROD-SPECIFIC #}
          - ip_protocol: tcp
            from_port: 80
            to_port: 80
            cidr_ip:
                - {{ envs[ENV_NAME]["mnoVpcCidr"] }}
          - ip_protocol: udp
            from_port: 53
            to_port: 53
            cidr_ip:
                - 10.0.0.0/8
          - ip_protocol: tcp
            from_port: 53
            to_port: 53
            cidr_ip:
                - 10.0.0.0/8
          - ip_protocol: tcp
            from_port: 65000 
            to_port: 65534
            cidr_ip:
                - 10.0.0.0/8
          - ip_protocol: tcp
            from_port: 21 
            to_port: 21 
            cidr_ip:
                - 10.0.0.0/8
          - ip_protocol: icmp
            from_port: -1
            to_port: -1
            cidr_ip:
                - 10.0.0.0/8
        - region: {{ REGION }}
        - profile: salt-boto-aws-management

{{ MNO_PREFIX~ENV_NAME }}-broker-external-security-group:
    boto_secgroup.present:
        - description: Permit external access to broker ELB
        - vpc_id: {{ MNO_VPC_ID }}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 1883
            to_port: 1883
            cidr_ip:
                - 10.0.0.0/8
{%- for cidr in trustedCidrs %}
                - {{ cidr }}
{%- endfor %}
          - ip_protocol: tcp
            from_port: 80
            to_port: 80
            cidr_ip:
                - 10.0.0.0/8
{%- for cidr in trustedCidrs %}
                - {{ cidr }}
{%- endfor %}
        - region: {{ REGION }}
        - profile: salt-boto-aws-management

{{ MNO_PREFIX~ENV_NAME }}-nfs-security-group:
    boto_secgroup.present:
        - description: Permit brokers access to MNO VPC EFS
        - vpc_id: {{ MNO_VPC_ID }}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 2049
            to_port: 2049
            cidr_ip:
                - {{ envs[ENV_NAME]["mnoVpcCidr"] }}
        - region: {{ REGION }}
        - profile: salt-boto-aws-management


mdce-{{ ENV_NAME }}-applications-internal-security-group:
    boto_secgroup.present:
        - description: Internal access to mdce-integration, mdce-mqtt, mdce-web and fpc for {{ ENV_NAME }}
        - vpc_id: {{ MDCE_VPC_ID }}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 8080     #mdce-mqtt
            to_port: 8083       #mdce-lora
            cidr_ip:
              - 10.0.0.0/8      #access from anywhere in LAN
          - ip_protocol: tcp
            from_port: 9080     #mdce-web external
            to_port: 9081       #mdce-web internal
            cidr_ip:
              - 10.0.0.0/8      #access from anywhere in LAN
        - region: {{ REGION }}
        - profile: salt-boto-aws-management

mdce-{{ ENV_NAME }}-applications-external-security-group:
    boto_secgroup.present:
        - description: External access to mdce-integration, mdce-mqtt and mdce-web for {{ ENV_NAME }}
        - vpc_id: {{ MDCE_VPC_ID }}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 8444     #mdce-integration HTTPS
            to_port: 8444
            cidr_ip:
              - 0.0.0.0/0       #whole internet
          - ip_protocol: tcp
            from_port: 9001     #mdce-integration HTTPS for VZW callback
            to_port: 9001
            cidr_ip:
              - 0.0.0.0/0       #whole internet
{#- EXTERNAL MDCE-WEB ACCESS DISABLED
          - ip_protocol: tcp
            from_port: 443      #mdce-web external HTTPS
            to_port: 443
            cidr_ip:
              - 0.0.0.0/0       #whole internet
#}              
          - ip_protocol: tcp
            from_port: 9444     #mdce-web MSOC
            to_port: 9444
            cidr_ip:
{%- for cidr in trustedCidrs %}
                - {{ cidr }}
{%- endfor %}
          - ip_protocol: tcp
            from_port: 8443     #mdce-mqtt HTTPS
            to_port: 8446       #mdce-lora HTTPS
            cidr_ip:
{%- for cidr in trustedCidrs %}
                - {{ cidr }}
{%- endfor %}
{%- if factoryCidrs %}
          - ip_protocol: tcp
            from_port: 8445     #fpc HTTPS
            to_port: 8445
            cidr_ip:
{%- for cidr in factoryCidrs %}
                - {{ cidr }}
{%- endfor %}
{%- endif %}
{%- if loraCidrs %}
          - ip_protocol: tcp
            from_port: 8446     #mdce-lora HTTPS
            to_port: 8446
            cidr_ip:
{%- for cidr in loraCidrs %}
                - {{ cidr }}
{%- endfor %}
{%- endif %}
        - region: {{ REGION }}
        - profile: salt-boto-aws-management

mdce-{{ ENV_NAME }}-rds-security-group:
    boto_secgroup.present:
        - name: 'mdce-{{ ENV_NAME }}-rds-security-group'
        - description: Permit access to MySQL RDS for {{ ENV_NAME }}
        - vpc_id: {{ MDCE_VPC_ID }}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 3306
            to_port: 3306
            cidr_ip:
              - {{MDCE_VPC_IP_16BIT}}.2.0/24      #mdce private-a subnet
              - {{MDCE_VPC_IP_16BIT}}.3.0/24      #mdce private-b subnet {#- PREPROD-SPECIFIC #}
        - region: {{ REGION }}
        - profile: salt-boto-aws-management

mdce-{{ ENV_NAME }}-nfs-security-group:
    boto_secgroup.present:
        - description: Permit brokers access to MNO VPC EFS
        - vpc_id: {{ MDCE_VPC_ID }}
        - rules_egress:
          - ip_protocol: all
            from_port: -1
            to_port: -1
            cidr_ip:
              - 0.0.0.0/0
        - rules:
          - ip_protocol: tcp
            from_port: 2049
            to_port: 2049
            cidr_ip:
              - {{MDCE_VPC_IP_16BIT}}.2.0/24      #mdce private-a subnet
              - {{MDCE_VPC_IP_16BIT}}.3.0/24      #mdce private-b subnet {#- PREPROD-SPECIFIC #}
        - region: {{ REGION }}
        - profile: salt-boto-aws-management

