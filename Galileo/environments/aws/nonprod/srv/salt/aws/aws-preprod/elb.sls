#!yaml_jinja
{% from "aws-info.map.jinja" import awsInfo %}
{% from "mdce-env-spec.map.jinja" import trustedCidrs %}
{% from "mdce-env-spec.map.jinja" import vpcs %}
{% from "mdce-env-spec.map.jinja" import envs %}
{% from "mdce-env-spec.map.jinja" import MNO_PREFIX %}

# AWS state file for auto-test environment
# See https://docs.saltstack.com/en/latest/ref/states/all/salt.states.boto_elb.html

{% set ENV_NAME = 'preprod' %}  {#- PREPROD-SPECIFIC #}
{% set CERT = 'arn:aws:iam::934427565871:server-certificate/star_mdce-nonprod_neptunensight_com' %}  {#- PREPROD-SPECIFIC #}

{% set mnoVpcInfo = vpcs[envs[ENV_NAME]["mnoVpc"]] %}
{% set REGION = mnoVpcInfo["region"] %}
{% set MNO_VPC_ID = awsInfo[REGION]["vpcId"][mnoVpcInfo["vpcName"]] %}

{% set mdceVpcInfo = vpcs[envs[ENV_NAME]["mdceVpc"]] %}
{% set MDCE_VPC_ID = awsInfo[REGION]["vpcId"][mdceVpcInfo["vpcName"]] %}

{% set MNO_VPC_IP_24BIT = envs[ENV_NAME]["mnoVpcCidr"] | replace(".0/24", "") %}
{% set MDCE_VPC_IP_16BIT = envs[ENV_NAME]["mdceVpcCidr"] | replace(".0.0/16", "") %}

{% if awsInfo[REGION]["sgId"] is defined
and awsInfo[REGION]["sgId"][MNO_VPC_ID][MNO_PREFIX~ENV_NAME~"-broker-internal-security-group"] is defined
and awsInfo[REGION]["sgId"][MNO_VPC_ID][MNO_PREFIX~ENV_NAME~"-broker-external-security-group"] is defined
and awsInfo[REGION]["sgId"][MDCE_VPC_ID]["mdce-"~ENV_NAME~"-applications-internal-security-group"] is defined
and awsInfo[REGION]["sgId"][MDCE_VPC_ID]["mdce-"~ENV_NAME~"-applications-external-security-group"] is defined
%}

{{ MNO_PREFIX~ENV_NAME }}-elb:
  boto_elb.present:
    - region: {{ REGION }}
    - subnets:
      - {{ awsInfo[REGION]["subnetId"][MNO_VPC_ID][MNO_PREFIX~ENV_NAME~"-private-a-subnet"] }}
      - {{ awsInfo[REGION]["subnetId"][MNO_VPC_ID][MNO_PREFIX~ENV_NAME~"-private-b-subnet"] }}  {#- PREPROD-SPECIFIC #}
    - security_groups:
      - {{ awsInfo[REGION]["sgId"][MNO_VPC_ID][MNO_PREFIX~ENV_NAME~"-broker-external-security-group"] }}
    - scheme: internal
    - attributes:
        cross_zone_load_balancing:
          enabled: true
    - listeners:
      - elb_port: 1883
        instance_port: 1883
        elb_protocol: TCP
      - elb_port: 80
        instance_port: 80
        elb_protocol: HTTP
    - tags: {Env: '{{ ENV_NAME }}', Name: '{{ MNO_PREFIX~ENV_NAME }}-elb'}

mdce-{{ ENV_NAME }}-internal-elb:
  boto_elb.present:
    - region: {{ REGION }}
    - subnets:
      - {{ awsInfo[REGION]["subnetId"][MDCE_VPC_ID]["mdce-"~ENV_NAME~"-private-a-subnet"] }}
      - {{ awsInfo[REGION]["subnetId"][MDCE_VPC_ID]["mdce-"~ENV_NAME~"-private-b-subnet"] }}  {#- PREPROD-SPECIFIC #}
    - security_groups:
      - {{ awsInfo[REGION]["sgId"][MDCE_VPC_ID]["mdce-"~ENV_NAME~"-applications-internal-security-group"] }}
    - scheme: internal
    - attributes:
        cross_zone_load_balancing:
          enabled: true
    - listeners:
      - elb_port: 8080
        instance_port: 8080
        elb_protocol: HTTP
      - elb_port: 8081
        instance_port: 8081
        elb_protocol: HTTP
      - elb_port: 9080
        instance_port: 9080
        elb_protocol: HTTP
      - elb_port: 9081
        instance_port: 9081
        elb_protocol: HTTP
    - tags: {Env: '{{ ENV_NAME }}', Name: 'mdce-{{ ENV_NAME }}-internal-elb'}

# Note: mdce-{{ ENV_NAME }}-external-elb is created by Boto script CreateEnvEc2.py.
# It was removed from here as it is now an Application ELB ("elb v2"), which Salt cannot create as of 2016.

{% endif %} # endif security group IDs available
