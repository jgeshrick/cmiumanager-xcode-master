#!/usr/bin/python
# -*- coding: utf-8 -*-
"""

This script produces a report that summarises the whitelists.
It doesn't do anything that affects the system.

Requires: pip install Jinja2

To run locally, first set BOTO_CONFIG=C:\code\neptune\mspd\trunk\Galileo\environments\aws\nonprod\etc\boto.cfg (adjust path as required)

"""


import config.EnvSpecificConstants as config

import lib.BotoVpcWrapper as boto_vpc
import lib.BotoEc2Wrapper as boto_ec2
import lib.BotoS3Wrapper as boto_s3
from datetime import datetime
from jinja2 import Template


def summarizeSecGroup(secGroupMap, newSecGroup):
	if newSecGroup:
		secGroupMap[newSecGroup.name] = newSecGroup.rules

def getVpcId(cache, vpcName, vpcRegion):
	if vpcName in cache:
		return cache[vpcName]
	vpc = boto_vpc.find_vpc(vpc_name=vpcName, region=vpcRegion)
	cache[vpcName] = vpc.id
	return vpc.id


# start script

print("Fetching security groups from AWS")

accountSecGroupNames = [config.MNO_PREFIX+'nonprod-management-security-group', 'mdce-dev-management-security-group', 'mdce-prod-management-security-group',]
mnoEnvSecGroupSuffixes = ['broker-security-group',]
mdceEnvSecGroupSuffixes = ['applications-security-group', 'applications-external-security-group',]

vpcIdCache = {}
secGroupSummary = {}

for vpcName in config.vpcs:
	region=config.vpcs[vpcName]['region']
	vpcId = getVpcId(vpcIdCache, vpcName, region)
	for sg in accountSecGroupNames:
		summarizeSecGroup(secGroupSummary, boto_ec2.find_security_group(sg, vpcId, region))

for envName in config.envs:
	vpcName = config.envs[envName]['mnoVpc']
	region=config.vpcs[vpcName]['region']
	vpcId = getVpcId(vpcIdCache, vpcName, region)
	for suffix in mnoEnvSecGroupSuffixes:
		sg = config.MNO_PREFIX+envName+'-'+suffix
		summarizeSecGroup(secGroupSummary, boto_ec2.find_security_group(sg, vpcId, region))
	
	vpcName = config.envs[envName]['mdceVpc']
	region=config.vpcs[vpcName]['region']
	vpcId = getVpcId(vpcIdCache, vpcName, region)
	for suffix in mdceEnvSecGroupSuffixes:
		sg = 'mdce-'+envName+'-'+suffix
		summarizeSecGroup(secGroupSummary, boto_ec2.find_security_group(sg, vpcId, region))
	
secGroupTimestamp = datetime.now()


s3Settings = config.whitelistSummaryConfig['s3']

print('Summarizing whitelists to ' + s3Settings['whitelistHtmlFileName'])

template = Template("""

<html>
	<head>
		<title>MDCE whitelists for account {{ config.account }}</title>
	</head>
	<body>
		<h1>MDCE whitelists</h1>
		<h2>Whitelists as defined in EnvSpecificConstants.py</h2>
		<p>These whitelists are defined in Subversion.  When salt runs (against the salt master server's salt minion), it updates the whitelists in AWS to match these.</p>
		<h3>Trusted CIDRs (trustedCidrs)</h3>
		<p>These CIDRs have full access to MDCE, e.g. Neptune's IP address</p>
		<p>The list contains {{ config.trustedCidrs|length }} CIDRs.</p>
		<ol>
			{%- for cidr in config.trustedCidrs %}
				<li>{{ cidr }}</li>
			{%- endfor %}
		</ol>
		<h3>LoRa CIDRs (loraCidrs)</h3>
		<p>These CIDRs have access to the mdce-lora HTTP endpoint.  They should be the originating IP addresses of the LoRa network uplink requests.</p>
		<p>The list contains {{ config.loraCidrs|length }} CIDRs.</p>
		<ol>
			{%- for cidr in config.loraCidrs %}
				<li>{{ cidr }}</li>
			{%- endfor %}
		</ol>
		<h3>Factory CIDRs (factoryCidrs)</h3>
		<p>These CIDRs have access to the fpc API.  They should be the IP addresses of the MIU factories.</p>
		<p>The list contains {{ config.factoryCidrs|length }} CIDRs.</p>
		<ol>
			{%- for cidr in config.factoryCidrs %}
				<li>{{ cidr }}</li>
			{%- endfor %}
		</ol>
		
		<h2>AWS security groups</h2>
		<p>Snapshot at: {{ secGroupTimestamp }}</p>
		<ul>
			{%- for sg in secGroupSummary %}
				<li>{{ sg }}
					<ul>
						{%- for rule in secGroupSummary[sg] %}
							<li>{%- if rule.to_port | int() > rule.from_port | int() %}Ports {{ rule.from_port }}-{{ rule.to_port }}{%- elif rule.from_port | int() == -1 %}All ports{%- else %}Port {{ rule.from_port }}{%- endif %}
								<ul>
									{%- for grant in rule.grants %}
										<li>{{ grant.cidr_ip }}</li>
									{%- endfor %}
								</ul>
							</li>
						{%- endfor %}
					</ul>
				</li>
			{%- endfor %}
		</ul>
	</body>
</html>

	""")

htmlOutput = template.render(config=config, 
	secGroupTimestamp=secGroupTimestamp,
	secGroupSummary=secGroupSummary)

with open(s3Settings['whitelistHtmlFileName'], 'w') as f:
	f.write(htmlOutput)


s3Path = s3Settings['whitelistHtmlFolder'] + '/' +  s3Settings['whitelistHtmlFileName']
print('Uploading whitelist summary to s3://' + s3Settings['bucketName'] + s3Path)

boto_s3.upload_html(s3Settings['bucketName'], s3Settings['bucketRegion'], s3Path, htmlOutput, config.s3SharedBucketCredentials)

print('Done')
