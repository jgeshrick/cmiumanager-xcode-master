#!/usr/bin/python
# -*- coding: utf-8 -*-
"""

Create the environment VPC and S3

"""

import sys

import config.EnvSpecificConstants as config
import lib.AwsCacheFileManager as awsCache
import lib.BotoVpcWrapper as boto_vpc
import lib.BotoS3Wrapper as boto_s3
import lib.BotoEc2Wrapper as boto_ec2



def calculateSubnet(initial, newSuffix):
    dotsToReplace = newSuffix.count('.')
    initialParts = initial.split('.')
    return '.'.join(initialParts[:-dotsToReplace]) + newSuffix
    
    

awsCache.load()

#constants

if len(sys.argv) < 2:
    sys.exit("Usage: CreateEnvVpc <envName>")

ENV_NAME=sys.argv[1]

print "Setting up VPC for environment " + ENV_NAME

envConfig = config.envs[ENV_NAME]
mnoVpcConfig = config.vpcs[envConfig["mnoVpc"]]
mdceVpcConfig = config.vpcs[envConfig["mdceVpc"]]
MNO_PREFIX = config.MNO_PREFIX

REGION = mnoVpcConfig["region"]

MNO_VPC_NAME = mnoVpcConfig["vpcName"]
MNO_IG_NAME = mnoVpcConfig["igName"]
MDCE_VPC_NAME = mdceVpcConfig["vpcName"]
MDCE_IG_NAME = mdceVpcConfig["igName"]

MNO_MDCE_VPC_PEERING_CONNECTION_NAME = mdceVpcConfig["peeringConnectionToMnoVpc"]

print "MNO VPC:"
print mnoVpcConfig

print "MDCE VPC:"
print mdceVpcConfig

print "environment config:"
print envConfig


#state - VPC

mnoVpc = None
mnoVpcCidr = mnoVpcConfig["vpcCidr"]

mnoVpc = boto_vpc.find_vpc(vpc_name=MNO_VPC_NAME, region=REGION)
if mnoVpc is None:
    #this VPC will be created manually
    sys.exit("MNO VPC " + MNO_VPC_NAME + " not found!  Aborting")


mnoVpcId = mnoVpc.id
awsCache.setVpcId(REGION, MNO_VPC_NAME, mnoVpcId)

sys.stdout.write("MNO VPC " + MNO_VPC_NAME + ", ID=" + mnoVpcId + "\n")


#state - VPC

mdceVpc = None
mdceVpcCidr =mdceVpcConfig["vpcCidr"]

mdceVpc = boto_vpc.find_vpc(vpc_name=MDCE_VPC_NAME, region=REGION)
if mdceVpc is None:
    mdceVpc = boto_vpc.create_vpc(
        cidr_block = mdceVpcCidr,
        vpc_name = MDCE_VPC_NAME,
        region = REGION,
    )
if mdceVpc is None:
    sys.exit("MDCE VPC " + MNO_VPC_NAME + " not found!  Aborting")


mdceVpcId = mdceVpc.id
awsCache.setVpcId(REGION, MDCE_VPC_NAME, mdceVpcId)

sys.stdout.write("MDCE VPC " + MDCE_VPC_NAME + ", ID=" + mdceVpcId + "\n")

# state - VPC peering

pcName = MNO_MDCE_VPC_PEERING_CONNECTION_NAME
peeringConnection = boto_vpc.find_vpc_peering_connection(name=pcName, region=REGION)
if peeringConnection is None:
    peeringConnection = boto_vpc.create_vpc_peering_connection(
        name = pcName,
        vpc_id = mnoVpcId,
        peer_vpc_id = mdceVpcId,
        region = REGION
    )

awsCache.setVpcPeeringConnectionId(REGION, MNO_MDCE_VPC_PEERING_CONNECTION_NAME, peeringConnection.id)


#state - internet gateway

igName = MNO_IG_NAME

vpnIg = boto_vpc.find_internet_gateway(name=igName, region=REGION)
if vpnIg is None:
    vpnIg = boto_vpc.create_internet_gateway(
        vpc_id = mnoVpcId,
        internet_gateway_name = igName,
        region = REGION
    )
awsCache.setInternetGatewayId(REGION, igName, vpnIg.id)

igName = MDCE_IG_NAME

mdceIg = boto_vpc.find_internet_gateway(name=igName, region=REGION)
if mdceIg is None:
    mdceIg = boto_vpc.create_internet_gateway(
        vpc_id = mdceVpcId,
        internet_gateway_name = igName,
        region = REGION
    )
awsCache.setInternetGatewayId(REGION, igName, mdceIg.id)


#state - routing table - account level

publicSubnetRoutingTableSpecification = mnoVpcConfig["publicSubnetRoutingTable"]
rtName = publicSubnetRoutingTableSpecification["name"]
rt = boto_vpc.find_route_table(name=rtName, region=REGION)
if rt is None:
    rt = boto_vpc.create_route_table(
        vpc_id = mnoVpcId,
        route_table_name = rtName,
        tags = {},
        region = REGION
    )
    for mdceVpcSpec in config.vpcs:
        if mdceVpcSpec["peeringConnectionToMnoVpc"]:
            boto_vpc.create_route(
                route_table_id = rt.id,
                destination_cidr_block = mdceVpcSpec["vpcCidr"],
                gateway_id = mdceVpcSpec["peeringConnectionToMnoVpc"],
                region = REGION
            )
    boto_vpc.create_route(
        route_table_id = rt.id,
        destination_cidr_block = "0.0.0.0/0",
        gateway_id = vpnIg.id,
        region = REGION        
    )

awsCache.setRouteTableId(REGION, rtName, rt.id)

mnoPublicRoutingTable = rt

#state - subnets and NAT gateways of MNO VPC - account level

mnoVpcNatGatewayId = None
subnetConfigs = mnoVpcConfig["accountLevelSubnets"]
for subnetConfig in subnetConfigs:
    
    subnetName = subnetConfig["name"]
    az = None if subnetConfig["az"] is None else REGION + subnetConfig["az"]
    cidr = subnetConfig["cidr"]
    assignPublicIp = subnetConfig["public"]
    rtId = mnoPublicRoutingTable.id if subnetConfig["public"] else None
    natGatewaySpec = subnetConfig.get("nat-gateway", None)
    
    subnet = boto_vpc.find_subnet(vpc_id=mnoVpcId, region=REGION, name=subnetName)
    if subnet is None:
        subnet = boto_vpc.create_subnet(
            vpc_id = mnoVpcId,
            cidr_block = cidr,
            availability_zone = az,
            subnet_name = subnetName,
            route_table_id = rtId,
            auto_assign_public_ip = assignPublicIp,
            tags = {"Env": ENV_NAME},
            region = REGION
        )
    awsCache.setSubnetId(REGION, mnoVpcId, subnetName, subnet.id)

    if natGatewaySpec:
        natGateway = boto_ec2.find_nat_gateway(vpc_id=mnoVpcId, region=REGION, subnetId=subnet.id)
        elasticIp = boto_ec2.find_elastic_ip(eip=natGatewaySpec["elasticIp"], region=REGION)
        if natGateway is None or natGateway["State"] != "available":
            natGateway = boto_ec2.create_nat_gateway(
                vpc_id=mnoVpcId, 
                subnetId=subnet.id, 
                eipAllocationId = elasticIp.allocation_id,
                region=REGION
            )
        awsCache.setElasticIpAllocationId(REGION, natGatewaySpec["elasticIp"], elasticIp.allocation_id)
        mnoVpcNatGatewayId = natGateway["NatGatewayId"]
        awsCache.setNatGatewayId(REGION, mnoVpcId, subnetName, mnoVpcNatGatewayId)

#state - routing table - environment level

rtName = MNO_PREFIX + ENV_NAME + "-private-routing"
rt = boto_vpc.find_route_table(name=rtName, region=REGION)
if rt is None:
    rt = boto_vpc.create_route_table(
        vpc_id = mnoVpcId,
        route_table_name = rtName,
        tags = {"Env": ENV_NAME},
        region = REGION        
    )
    boto_vpc.create_route(
        route_table_id = rt.id,
        destination_cidr_block = envConfig["mdceVpcCidr"],  #route to MDCE subnets for the environment
        gateway_id = peeringConnection.id,
        region = REGION        
    )
    boto_vpc.create_route(
        route_table_id = rt.id,
        destination_cidr_block = "0.0.0.0/0",
        gateway_id = mnoVpcNatGatewayId,
        region = REGION        
    )
    
    for routingSpec in mnoVpcConfig["mnoRouting"]:
        routeType = routingSpec["type"]
        destination = routingSpec["id"]
        propagate = routingSpec["propagate"]
        for cidr in routingSpec["cidrList"] or []:
            if routeType == "interface":
                boto_vpc.create_route(
                    route_table_id = rt.id,
                    destination_cidr_block = cidr,
                    interface_id = destination,
                    region = REGION
                )
            else:
                boto_vpc.create_route(
                    route_table_id = rt.id,
                    destination_cidr_block = cidr,
                    gateway_id = destination,
                    region = REGION
                )
        if propagate:
            boto_vpc.enable_vgw_route_propagation(
                route_table_id = rt.id, 
                gateway_id = destination,
                region = REGION
            )

mnoPrivateRoutingTable = rt
awsCache.setRouteTableId(REGION, rtName, rt.id)

rtName = "mdce-" + ENV_NAME + "-public-routing"

rt = boto_vpc.find_route_table(name=rtName, region=REGION)
if rt is None:
    rt = boto_vpc.create_route_table(
        vpc_id = mdceVpcId,
        route_table_name = rtName,
        tags = {"Env": ENV_NAME},
        region = REGION        
    )
    boto_vpc.create_route(
        route_table_id = rt.id,
        destination_cidr_block = mnoVpcCidr,  #route to the whole MNO VPC, including the salt master
        gateway_id = peeringConnection.id,
        region = REGION        
    )
    boto_vpc.create_route(
        route_table_id = rt.id,
        destination_cidr_block = "0.0.0.0/0",
        gateway_id = mdceIg.id,
        region = REGION
    )

mdcePublicRoutingTable = rt
awsCache.setRouteTableId(REGION, rtName, rt.id)



#state - subnets of MNO VPC - environment level

subnetConfigs = envConfig["template"]["mnoVpcSubnets"]
for subnetConfig in subnetConfigs:
    
    subnetName = MNO_PREFIX + ENV_NAME + "-" + subnetConfig["name"] + "-subnet"
    az = None if subnetConfig["az"] is None else REGION + subnetConfig["az"]
    cidr = calculateSubnet(envConfig["mnoVpcCidr"], subnetConfig["cidrSuffix"])
    assignPublicIp = subnetConfig["public"]
    rtId = mnoPrivateRoutingTable.id if subnetConfig["public"] else None
    
    subnet = boto_vpc.find_subnet(vpc_id=mnoVpcId, region=REGION, name=subnetName)
    if subnet is None:
        subnet = boto_vpc.create_subnet(
            vpc_id = mnoVpcId,
            cidr_block = cidr,
            availability_zone = az,
            subnet_name = subnetName,
            route_table_id = mnoPrivateRoutingTable.id,
            auto_assign_public_ip = assignPublicIp,
            tags = {"Env": ENV_NAME},
            region = REGION        
        )
    awsCache.setSubnetId(REGION, mnoVpcId, subnetName, subnet.id)


#state - subnets and routing tables of MDCE VPC

subnetConfigs = envConfig["template"]["mdceVpcSubnets"]
privateRoutingTableIdPerAz = {}
natGatewayIdPerAz = {}

for subnetConfig in subnetConfigs:
    
    if subnetConfig["public"]:
        rt = mdcePublicRoutingTable
    else:
        rtName = "mdce-" + ENV_NAME + "-" + subnetConfig["name"] + "-routing"

        rt = boto_vpc.find_route_table(name=rtName, region=REGION)
        if rt is None:
            rt = boto_vpc.create_route_table(
                vpc_id = mdceVpcId,
                route_table_name = rtName,
                tags = {"Env": ENV_NAME},
                region = REGION        
            )
            boto_vpc.create_route(
                route_table_id = rt.id,
                destination_cidr_block = mnoVpcCidr,
                gateway_id = peeringConnection.id,
                region = REGION        
            )
            boto_vpc.create_route(
                route_table_id = rt.id,
                destination_cidr_block = "0.0.0.0/0",
                gateway_id = natGatewayIdPerAz[subnetConfig["az"]],
                region = REGION        
            )

    subnetName = "mdce-" + ENV_NAME + "-" + subnetConfig["name"] + "-subnet"
    az = None if subnetConfig["az"] is None else REGION + subnetConfig["az"]
    cidr = calculateSubnet(envConfig["mdceVpcCidr"], subnetConfig["cidrSuffix"])
    assignPublicIp = subnetConfig["public"]
    rtId = rt.id
    natGatewaySpec = subnetConfig.get("nat-gateway", None)

    subnet = boto_vpc.find_subnet(vpc_id=mdceVpcId, region=REGION, name=subnetName)
    if subnet is None:
        subnet = boto_vpc.create_subnet(
            vpc_id = mdceVpcId,
            cidr_block = cidr,
            availability_zone = az,
            subnet_name = subnetName,
            route_table_id = rtId,
            auto_assign_public_ip = assignPublicIp,
            tags = {"Env": ENV_NAME},
            region = REGION        
        )
    awsCache.setSubnetId(REGION, mdceVpcId, subnetName, subnet.id)

    if natGatewaySpec:
        natGateway = boto_ec2.find_nat_gateway(vpc_id=mdceVpcId, region=REGION, subnetId=subnet.id)
        elasticIp = boto_ec2.find_elastic_ip(eip=natGatewaySpec["elasticIp"], region=REGION)
        if natGateway is None or natGateway["State"] != "available":
            natGateway = boto_ec2.create_nat_gateway(
                vpc_id=mdceVpcId,
                subnetId=subnet.id, 
                eipAllocationId = elasticIp.allocation_id,
                region=REGION
            )
        awsCache.setElasticIpAllocationId(REGION, natGatewaySpec["elasticIp"], elasticIp.allocation_id)
        natGatewayIdPerAz[subnetConfig["az"]] = natGateway["NatGatewayId"]
        awsCache.setNatGatewayId(REGION, mnoVpcId, subnetName, natGateway["NatGatewayId"])


#state - S3

bucketName = "neptune-mdce-" + ENV_NAME + "-large-packets"

if not boto_s3.find_bucket(name=bucketName, region=REGION):
    boto_s3.create_bucket(
        name = bucketName,
        tags = {"Env": ENV_NAME},
        region = REGION
    )


bucketName = "neptune-mdce-" + ENV_NAME + "-old-data"

if not boto_s3.find_bucket(name=bucketName, region=REGION):
    boto_s3.create_bucket(
        name = bucketName,
        tags = {"Env": ENV_NAME},
        region = REGION
    )


# write IDs to cache file
awsCache.save()

sys.stdout.write("Successfully completed first step of creation of environment " + ENV_NAME + "\n")


