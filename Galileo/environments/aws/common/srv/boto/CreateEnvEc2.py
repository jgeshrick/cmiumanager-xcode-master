#!/usr/bin/python
# -*- coding: utf-8 -*-
"""

Tag the environment EBS and ENI after creation.  Register instances with ELB

"""

import sys
import config.EnvSpecificConstants as config
import lib.AwsCacheFileManager as awsCache
import lib.BotoVpcWrapper as boto_vpc
import lib.BotoEc2Wrapper as boto_ec2
import lib.BotoEfsWrapper as boto_efs
import lib.BotoElbWrapper as boto_elb
import lib.BotoElbV2Wrapper as boto_elb2

awsCache.load()

# constants

if len(sys.argv) < 2:
    sys.exit("Usage: CreateEnvRds <envName>")

ENV_NAME = sys.argv[1]

print "Setting up EBS and ENI tags for environment " + ENV_NAME

envConfig = config.envs[ENV_NAME]
mnoVpcConfig = config.vpcs[envConfig["mnoVpc"]]
mdceVpcConfig = config.vpcs[envConfig["mdceVpc"]]
MNO_PREFIX = config.MNO_PREFIX

REGION = mnoVpcConfig["region"]
MNO_VPC_NAME = mnoVpcConfig["vpcName"]
MDCE_VPC_NAME = mdceVpcConfig["vpcName"]

CERT=envConfig["httpsCertArn"] if envConfig["template"]["elb"] else None

mnoVpc = boto_vpc.find_vpc(vpc_name=MNO_VPC_NAME, region=REGION)
if mnoVpc is None:
    # this VPC will be created manually
    sys.exit("MNO VPC " + MNO_VPC_NAME + " not found!  Aborting")


mnoVpcId = mnoVpc.id
awsCache.setVpcId(REGION, MNO_VPC_NAME, mnoVpcId)

sys.stdout.write("MNO VPC " + MNO_VPC_NAME + ", ID=" + mnoVpcId + "\n")


# state - VPC

mdceVpc = None
mdceVpcCidr = mdceVpcConfig["vpcCidr"]

mdceVpc = boto_vpc.find_vpc(vpc_name=MDCE_VPC_NAME, region=REGION)
if mdceVpc is None:
    mdceVpc = boto_vpc.create_vpc(
        cidr_block=mdceVpcCidr,
        vpc_name=MDCE_VPC_NAME,
        region=REGION,
    )
if mdceVpc is None:
    sys.exit("MDCE VPC " + MNO_VPC_NAME + " not found!  Aborting")


mdceVpcId = mdceVpc.id


# state - EBS and ENI tags

appInstanceIds = []
brokerInstanceIds = []

serversToTag = [
        {
            "ec2Name": "aw-mdce-" + ENV_NAME + "-app" + str(instanceNumber).rjust(2, '0'),
            "env": ENV_NAME,
            "addToList": "app"
        } 
        for instanceNumber in range(1, envConfig["template"]["appInstances"] + 1)
    ] + [
        {
            "ec2Name": "aw-mdce-" + ENV_NAME + "-brk" + str(instanceNumber).rjust(2, '0'),
            "env": ENV_NAME,
            "addToList": "brk"    
        } 
        for instanceNumber in range(1, envConfig["template"]["brokerInstances"] + 1)
    ] + [
        {
            "ec2Name": subnetSpec["bastion-host"]["name"],
            "env": None,
            "addToList": None    
        } 
        for subnetSpec in mnoVpcConfig["accountLevelSubnets"] if subnetSpec["bastion-host"]
    ]
        

for serverInfo in serversToTag:

    ec2Name = serverInfo["ec2Name"]

    instance = boto_ec2.find_instance(ec2Name, REGION)

    awsCache.setEc2Id(REGION, ec2Name, instance.id)
    if serverInfo["addToList"] == "app":
        appInstanceIds.append(instance.id)
    elif serverInfo["addToList"] == "brk":
        brokerInstanceIds.append(instance.id)

    volumeName = ec2Name + "-ebs"
    volume = boto_ec2.get_attached_volume(instance.id, "/dev/sda1", REGION)
    boto_ec2.ensure_volume_tags(
        volume=volume,
        tags={"Env": serverInfo["env"], "Name": volumeName} if serverInfo["env"] else {"Name": volumeName},
        region=REGION
    )
    awsCache.setEbsId(REGION, volumeName, volume.id)

    volumeName = ec2Name + "-dynamodb-local-data-ebs"
    volume = boto_ec2.get_attached_volume(instance.id, "/dev/sdb", REGION)
    if volume:
        boto_ec2.ensure_volume_tags(
            volume=volume,
            tags={"Env": serverInfo["env"], "Name": volumeName} if serverInfo["env"] else {"Name": volumeName},
            region=REGION
        )
        awsCache.setEbsId(REGION, volumeName, volume.id)

    eniName = ec2Name + "-eni"
    eni = boto_ec2.get_attached_network_interface(instance.id, 0, REGION)
    boto_ec2.ensure_eni_tags(
        eni=eni,
        tags={"Env": ENV_NAME, "Name": eniName} if serverInfo["env"] else {"Name": eniName},
        region=REGION
    )
    awsCache.setEniId(REGION, eniName, eni.id)

# state - ELB

if envConfig["template"]["elb"]:

    elbName = MNO_PREFIX + ENV_NAME + "-elb"
    elb = boto_elb.find_elb(elbName, REGION)
    if elb is None:
        sys.exit("ELB " + elbName + " not found!  Aborting")

    boto_elb.ensure_elb_tags(
        elbName=elbName,
        tags={"Env": ENV_NAME, "Name": elbName},
        region=REGION
    )
    awsCache.setElbDnsName(REGION, elbName, elb["DNSName"])

    boto_elb.ensure_registered_instances(elb, brokerInstanceIds, REGION)

    enis = boto_ec2.get_network_interfaces_by_description(
        description="ELB " + elbName,
        region=REGION
    )
    for eni in enis:
        boto_ec2.ensure_eni_tags(
            eni=eni,
            tags={"Env": ENV_NAME, "Name": elbName + "-" + eni.availability_zone[-1:] + "-eni"},
            region=REGION
        )

    elbName = "mdce-" + ENV_NAME + "-internal-elb"
    elb = boto_elb.find_elb(elbName, REGION)
    if elb is None:
        sys.exit("ELB " + elbName + " not found!  Aborting")

    boto_elb.ensure_elb_tags(
        elbName=elbName,
        tags={"Env": ENV_NAME, "Name": elbName},
        region=REGION
    )
    awsCache.setElbDnsName(REGION, elbName, elb["DNSName"])

    boto_elb.ensure_registered_instances(elb, appInstanceIds, REGION)

    enis = boto_ec2.get_network_interfaces_by_description(
        description="ELB " + elbName,
        region=REGION
    )
    for eni in enis:
        boto_ec2.ensure_eni_tags(
            eni=eni,
            tags={"Env": ENV_NAME, "Name": elbName + "-" + eni.availability_zone[-1:] + "-eni"},
            region=REGION
        )

# Application ELB (external)

    elbName = "mdce-" + ENV_NAME + "-external-elb"
    elb = boto_elb2.find_application_elb(elbName, REGION)
    
    if elb is None:
        
        subnetConfigs = envConfig["template"]["mdceVpcSubnets"]
        subnetNames = ["mdce-" + ENV_NAME + "-" + subnetConfig["name"] + "-subnet" for subnetConfig in subnetConfigs if subnetConfig["public"]]
        subnetIds = [boto_vpc.find_subnet(vpc_id=mdceVpcId, region=REGION, name=subnetName).id for subnetName in subnetNames]
        secGroupId =  boto_ec2.find_security_group("mdce-" + ENV_NAME + "-applications-external-security-group", mdceVpcId, REGION).id
        
        elb = boto_elb2.create_external_application_elb(
            name=elbName,
            subnetIdList=subnetIds,
            securityGroupIdList=[secGroupId],
            tags={"Env": ENV_NAME, "Name": elbName},
            region=REGION
        )
        

    awsCache.setElbDnsName(REGION, elbName, elb["DNSName"])
    
    for targetGroupInfo in [
        {"service": "mqtt", "extport": 8443, "intport": 8080, "healthCheckPath": "/mdce-mqtt/debug/application/status", "sticky": False},
        {"service": "intg8444", "extport": 8444, "intport": 8081, "healthCheckPath": "/debug/application/status", "sticky": False},
        {"service": "intg9001", "extport": 9001, "intport": 8081, "healthCheckPath": "/debug/application/status", "sticky": False},
        {"service": "fpc", "extport": 8445, "intport": 8082, "healthCheckPath": "/fpc/debug/application/status", "sticky": False},
        {"service": "lora", "extport": 8446, "intport": 8083, "healthCheckPath": "/mdce-lora/debug/application/status", "sticky": False},
        {"service": "extweb", "extport": 443, "intport": 9080, "healthCheckPath": "/mdce-web/pages/user/login", "sticky": True},
        {"service": "intweb", "extport": 9444, "intport": 9081, "healthCheckPath": "/mdce-web/pages/user/login", "sticky": True},
    ]:
        tgName = "mdce-" + ENV_NAME + "-" + targetGroupInfo["service"] + "-elb-tg"
        tg = boto_elb2.find_elb_target_group(tgName, REGION)
        if tg is None:
            boto_elb2.create_target_group(
                name=tgName, 
                port=targetGroupInfo["intport"],
                vpcId=mdceVpcId,
                healthCheckPath=targetGroupInfo["healthCheckPath"], 
                ec2IdList=appInstanceIds, 
                sticky=targetGroupInfo["sticky"],
                region=REGION
            )
            boto_elb2.create_listeners(
                elbName=elbName,
                targetGroupName=tgName,
                externalPort=targetGroupInfo["extport"],
                certArn=CERT, 
                region=REGION
            )
        boto_elb2.ensure_registered_instances(tg, appInstanceIds, REGION)
            

    enis = boto_ec2.get_network_interfaces_by_description(
        description="ELB app/" + elbName,
        region=REGION
    )
    for eni in enis:
        boto_ec2.ensure_eni_tags(
            eni=eni,
            tags={"Env": ENV_NAME, "Name": elbName + "-" + eni.availability_zone[-1:] + "-eni"},
            region=REGION
        )


# state - EFS ID

if envConfig["template"]["efs"]:
    efsName = MNO_PREFIX + ENV_NAME + "-efs"
    efs = boto_efs.find_efs(efsName, REGION)
    if efs is None:
        sys.exit("EFS " + efsName + " not found!  Aborting")

    awsCache.setEfsId(REGION, efsName, efs["FileSystemId"])

    efsName = "mdce-" + ENV_NAME + "-efs"
    efs = boto_efs.find_efs(efsName, REGION)
    if efs is None:
        sys.exit("EFS " + efsName + " not found!  Aborting")

    awsCache.setEfsId(REGION, efsName, efs["FileSystemId"])


# state - RDS ENI tags

sg = boto_ec2.find_security_group("mdce-" + ENV_NAME + "-rds-security-group", mdceVpcId, REGION)
enis = boto_ec2.get_network_interfaces_in_security_group(sg.id, REGION)
for eni in enis:
    subnetId = eni.subnet_id
    subnet = boto_vpc.get_subnet_by_id(subnetId, REGION)
    eniName = "mdce-" + ENV_NAME + "-rds-" + subnet.availability_zone[-1:] + "-eni"
    boto_ec2.ensure_eni_tags(
        eni=eni,
        tags={"Env": ENV_NAME, "Name": eniName},
        region=REGION
    )
    awsCache.setEniId(REGION, eniName, eni.id)

# state - NFS ENI tags

sg = boto_ec2.find_security_group(MNO_PREFIX + ENV_NAME + "-nfs-security-group", mnoVpcId, REGION)
if sg is not None:
    enis = boto_ec2.get_network_interfaces_in_security_group(sg.id, REGION)
    for eni in enis:
        subnetId = eni.subnet_id
        subnet = boto_vpc.get_subnet_by_id(subnetId, REGION)
        eniName = MNO_PREFIX + ENV_NAME + "-nfs-" + subnet.availability_zone[-1:] + "-eni"
        boto_ec2.ensure_eni_tags(
            eni=eni,
            tags={"Env": ENV_NAME, "Name": eniName},
            region=REGION
        )
        awsCache.setEniId(REGION, eniName, eni.id)


sg = boto_ec2.find_security_group("mdce-" + ENV_NAME + "-nfs-security-group", mdceVpcId, REGION)
if sg is not None:
    enis = boto_ec2.get_network_interfaces_in_security_group(sg.id, REGION)
    for eni in enis:
        subnetId = eni.subnet_id
        subnet = boto_vpc.get_subnet_by_id(subnetId, REGION)
        eniName = "mdce-" + ENV_NAME + "-nfs-" + subnet.availability_zone[-1:] + "-eni"
        boto_ec2.ensure_eni_tags(
            eni=eni,
            tags={"Env": ENV_NAME, "Name": eniName},
            region=REGION
        )
        awsCache.setEniId(REGION, eniName, eni.id)

# write IDs to cache file
awsCache.save()

sys.stdout.write("Successfully completed EC2 tagging step of creation of environment " + ENV_NAME + "\n")

import PrepareSaltForAws  # run the Prepare Salt For Aws script
