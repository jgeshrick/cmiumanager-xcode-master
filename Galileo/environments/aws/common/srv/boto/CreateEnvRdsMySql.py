#!/usr/bin/python
# -*- coding: utf-8 -*-
"""

Create the environment - RDS MySQL.
Also tags security groups since SaltStack can't do this yet.

"""

import sys
import re
import config.EnvSpecificConstants as config
import lib.AwsCacheFileManager as awsCache
import lib.BotoVpcWrapper as boto_vpc
import lib.BotoRdsWrapper as boto3_rds
import lib.BotoEc2Wrapper as boto_ec2

awsCache.load()

# constants

if len(sys.argv) < 2:
    sys.exit("Usage: CreateEnvRdsMySql.py <envName>")

ENV_NAME = sys.argv[1]

print "Setting up RDS MySQL for environment " + ENV_NAME

envConfig = config.envs[ENV_NAME]
mnoVpcConfig = config.vpcs[envConfig["mnoVpc"]]
mdceVpcConfig = config.vpcs[envConfig["mdceVpc"]]
MNO_PREFIX = config.MNO_PREFIX

REGION = mnoVpcConfig["region"]

MNO_VPC_NAME = mnoVpcConfig["vpcName"]
MNO_VPC_NAME_MIDDLE = re.compile('[a-z]+\-(.*)\-vpc').match(MNO_VPC_NAME).group(1)
MDCE_VPC_NAME = mdceVpcConfig["vpcName"]


RDS_MASTER_USER_PASSWORD = envConfig["rdsMasterUserPassword"]


# state - VPC
mnoVpc = None

mnoVpc = boto_vpc.find_vpc(vpc_name=MNO_VPC_NAME, region=REGION)
if mnoVpc is None:
    sys.exit("Cannot find ID for VPC " + MNO_VPC_NAME + ".  VPC must exist before this script can be run")


mnoVpcId = mnoVpc.id
awsCache.setVpcId(REGION, MNO_VPC_NAME, mnoVpcId)

sys.stdout.write("VPC " + MNO_VPC_NAME + ", ID=" + mnoVpcId + "\n")


mdceVpc = None

mdceVpc = boto_vpc.find_vpc(vpc_name=MDCE_VPC_NAME, region=REGION)
if mdceVpc is None:
    sys.exit("Cannot find ID for VPC " + MDCE_VPC_NAME + ".  VPC must exist before this script can be run")


mdceVpcId = mdceVpc.id
awsCache.setVpcId(REGION, MDCE_VPC_NAME, mdceVpcId)

sys.stdout.write("VPC " + MDCE_VPC_NAME + ", ID=" + mdceVpcId + "\n")


# state - tags on security groups.  Needed since salt.states.boto_secgroup can't do this yet


secgroupName = MNO_PREFIX + MNO_VPC_NAME_MIDDLE + '-salt-master-security-group'
secGroup = boto_ec2.find_security_group(secgroupName, vpc_id=mnoVpcId, region=REGION)
if secGroup is None:
    sys.exit("Cannot find security group " + secgroupName + ".  Security group must exist before this script can be run")
boto_ec2.ensure_security_group_tags(
    secGroup,
    REGION,
    {}
)
awsCache.setSecurityGroupId(REGION, mnoVpcId, secgroupName, secGroup.id)

secgroupName = MNO_PREFIX + MNO_VPC_NAME_MIDDLE + '-management-security-group'
secGroup = boto_ec2.find_security_group(secgroupName, vpc_id=mnoVpcId, region=REGION)
if secGroup is None:
    sys.exit("Cannot find security group " + secgroupName + ".  Security group must exist before this script can be run")
boto_ec2.ensure_security_group_tags(
    secGroup,
    REGION,
    {}
)
awsCache.setSecurityGroupId(REGION, mnoVpcId, secgroupName, secGroup.id)


if envConfig["mdceVpc"] == "mdce-dev-vpc":
    secgroupName = "mdce-dev-management-security-group"
    tags = {}
else:
    secgroupName = "mdce-" + ENV_NAME + "-management-internal-security-group"
    tags = {"Env": ENV_NAME}

secGroup = boto_ec2.find_security_group(secgroupName, vpc_id=mdceVpcId, region=REGION)
if secGroup is None:
    sys.exit("Cannot find security group " + secgroupName + " in VPC " + mdceVpcId + ".  Security group must exist before this script can be run")
boto_ec2.ensure_security_group_tags(
    secGroup,
    REGION,
    tags
)
awsCache.setSecurityGroupId(REGION, mdceVpcId, secgroupName, secGroup.id)


if envConfig["mdceVpc"] == "mdce-dev-vpc":
    secgroupNames = [MNO_PREFIX + ENV_NAME + "-broker-security-group"]
else:
    secgroupNames = [MNO_PREFIX + ENV_NAME + "-broker-internal-security-group",
                     MNO_PREFIX + ENV_NAME + "-broker-external-security-group",
                     MNO_PREFIX + ENV_NAME + "-nfs-security-group"]

for secgroupName in secgroupNames:
    secGroup = boto_ec2.find_security_group(secgroupName, vpc_id=mnoVpcId, region=REGION)
    if secGroup is None:
        sys.exit("Cannot find security group " + secgroupName + " in VPC " + mnoVpcId + ".  Security group must exist before this script can be run")
    boto_ec2.ensure_security_group_tags(
        secGroup,
        REGION,
        {"Env": ENV_NAME}
    )
    awsCache.setSecurityGroupId(REGION, mnoVpcId, secgroupName, secGroup.id)


if envConfig["useFtpTestSecurityGroup"] is True:
    secgroupName = [MNO_PREFIX + ENV_NAME + "-ftptest-security-group"]

for secgroupName in secgroupNames:
    secGroup = boto_ec2.find_security_group(secgroupName, vpc_id=mnoVpcId, region=REGION)
    if secGroup is None:
        sys.exit("Cannot find security group " + secgroupName + " in VPC " + mnoVpcId + ".  Security group must exist before this script can be run")
    boto_ec2.ensure_security_group_tags(
        secGroup,
        REGION,
        {"Env": ENV_NAME}
    )
    awsCache.setSecurityGroupId(REGION, mnoVpcId, secgroupName, secGroup.id)


if envConfig["mdceVpc"] == "mdce-dev-vpc":
    secgroupNames = ["mdce-" + ENV_NAME + "-applications-security-group"]
else:
    secgroupNames = ["mdce-" + ENV_NAME + "-applications-internal-security-group",
                     "mdce-" + ENV_NAME + "-applications-external-security-group",
                     "mdce-" + ENV_NAME + "-nfs-security-group"]

for secgroupName in secgroupNames:
    secGroup = boto_ec2.find_security_group(secgroupName, vpc_id=mdceVpcId, region=REGION)
    if secGroup is None:
        sys.exit("Cannot find security group " + secgroupName + " in VPC " + mdceVpcId + ".  Security group must exist before this script can be run")
    boto_ec2.ensure_security_group_tags(
        secGroup,
        REGION,
        tags
    )
    awsCache.setSecurityGroupId(REGION, mdceVpcId, secgroupName, secGroup.id)


secgroupName = "mdce-" + ENV_NAME + "-rds-security-group"
secGroup = boto_ec2.find_security_group(secgroupName, vpc_id=mdceVpcId, region=REGION)
if secGroup is None:
    sys.exit("Cannot find security group " + secgroupName + ".  Security group must exist before this script can be run")
boto_ec2.ensure_security_group_tags(
    secGroup,
    REGION,
    {"Env": ENV_NAME}
)
awsCache.setSecurityGroupId(REGION, mdceVpcId, secgroupName, secGroup.id)
rdsSecGroupId = secGroup.id

# state - RDS

# not env-dependent
pgName = "mdce-rds-mysql-parameter-group"

if not boto3_rds.db_parameter_group_exists(db_parameter_group_name=pgName, region=REGION):
    boto3_rds.create_db_parameter_group(
        db_parameter_group_name=pgName,
        db_parameter_group_family="mysql5.6",
        description="Parameters for MDCE RDS e.g. allow CREATE FUNCTION",
        tags=None,
        region=REGION,
        parameters=[
            "log_bin_trust_function_creators", 1, "apply-immediate",
            "event_scheduler", "ON", "apply-immediate",
        ]
    )

sngName = "mdce-" + ENV_NAME + "-rds-subnet-group"

if not boto3_rds.db_subnet_group_exists(db_subnet_group_name=sngName, region=REGION):

    subnetIds = []
    for subnetInfo in envConfig["template"]["mdceVpcSubnets"]:
        if not subnetInfo["public"]:
            subnetName = "mdce-" + ENV_NAME + "-" + subnetInfo["name"] + "-subnet"
            subnet = boto_vpc.find_subnet(vpc_id=mdceVpcId, region=REGION, name=subnetName)
            subnetIds.append(subnet.id)

    boto3_rds.create_db_subnet_group(
        db_subnet_group_name=sngName,
        db_subnet_group_description=ENV_NAME + " RDS",
        subnet_ids=subnetIds,
        tags={"Env": ENV_NAME},
        region=REGION
    )


rdsName = "mdce-" + ENV_NAME + envConfig["rdsEndpointNameSuffix"]
masterUsername = "mdcerdsmaster"
multiAz = envConfig["template"]["multiAZ"]
instanceClass = envConfig["rdsInstanceType"]

rdsInstance = boto3_rds.find_db_instance(db_instance_identifier=rdsName, region=REGION)
if rdsInstance is None:
    rdsInstance = boto3_rds.create_db_instance(
        db_instance_identifier=rdsName,
        allocated_storage=5,
        storage_type="gp2",   # general purpose (SSD)
        db_instance_class=instanceClass,
        engine="MySQL",
        engine_version="5.6.34",
        master_username=masterUsername,
        master_user_password=RDS_MASTER_USER_PASSWORD,
        db_name="mdce",
        db_security_groups=None,
        vpc_security_group_ids=[rdsSecGroupId],
        availability_zone=None,
        db_subnet_group_name=sngName,
        preferred_maintenance_window=None,
        db_parameter_group_name=pgName,
        backup_retention_period=5,
        preferred_backup_window='17:00-17:30',
        port=None,
        multi_az=multiAz,
        auto_minor_version_upgrade=True,
        license_model=None,
        iops=None,
        option_group_name="default:mysql-5-6",
        character_set_name=None,
        publicly_accessible=False,
        region=REGION,
        tags={"Env": ENV_NAME}
    )
    print "RDS instance " + rdsName + " is initializing.  Visit https://" + REGION + ".console.aws.amazon.com/rds/home?region=" + REGION + "#dbinstances:id=" + rdsName + ";sf=all to get the endpoint when available.  This needs to go into mdce-env.properties."

endpoint = boto3_rds.get_instance_endpoint(rdsInstance)

awsCache.setRdsEndpoint(REGION, rdsName, endpoint)


# write IDs to cache file
awsCache.save()

sys.stdout.write("Successfully completed RDS MySQL step of creation of environment " + ENV_NAME + "\n")
