# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 17:49:32 2015

@author: abh1
"""

import boto.ec2
import boto3
import sys

def find_security_group(secgroupName, vpc_id, region):
    boto_ec2 = boto.ec2.connect_to_region(region)

    if vpc_id is None:
        sgs = boto_ec2.get_all_security_groups()
    else:
        sgs = boto_ec2.get_all_security_groups(filters = {"vpc-id": vpc_id})
        
    for sg in sgs:
        if sg.name == secgroupName:
            return sg
    return None
    
    

def ensure_security_group_tags(secGroup, region, tags):
    newTags = {}    
    
    if secGroup.tags.get("Name", None) != secGroup.name:
        newTags["Name"] = secGroup.name

    for tagName in tags:
        if secGroup.tags.get(tagName, None) != tags[tagName]:
            newTags[tagName] = tags[tagName]
    
    if bool(newTags):
        sys.stdout.write("Updating tags for security group " + secGroup.name + "\n")
        boto_ec2 = boto.ec2.connect_to_region(region)
        boto_ec2.create_tags(secGroup.id, newTags)

def find_instance(instanceName, region):
    boto_ec2 = boto.ec2.connect_to_region(region)
    instances = boto_ec2.get_only_instances()
    for instance in instances:
        if instance.tags.get("Name", None) == instanceName:
            return instance
    return None



def ensure_instance_tags(instance, region, tags):
    newTags = {}    

    for tagName in tags:
        if instance.tags.get(tagName, None) != tags[tagName]:
            newTags[tagName] = tags[tagName]
    
    if bool(newTags):
        sys.stdout.write("Updating tags for instance " + instance.id + "\n")
        boto_ec2 = boto.ec2.connect_to_region(region)
        boto_ec2.create_tags(instance.id, newTags)


def find_volume(volumeName, region):
    boto_ec2 = boto.ec2.connect_to_region(region)
    volumes = boto_ec2.get_all_volumes()
    for volume in volumes:
        if volume.tags.get("Name", None) == volumeName:
            return volume
    return None


def get_attached_volume(instanceId, device, region):
    boto_ec2 = boto.ec2.connect_to_region(region)
    volumes = boto_ec2.get_all_volumes()

    for volume in volumes:
        if volume.attach_data.instance_id == instanceId and volume.attach_data.device == device:
            return volume
    return None

def ensure_volume_tags(volume, region, tags):
    newTags = {}    

    for tagName in tags:
        if volume.tags.get(tagName, None) != tags[tagName]:
            newTags[tagName] = tags[tagName]
    
    if bool(newTags):
        sys.stdout.write("Updating tags for EBS volume " + volume.id + "\n")
        boto_ec2 = boto.ec2.connect_to_region(region)
        boto_ec2.create_tags(volume.id, newTags)

def find_elastic_ip(eip, region):
    boto_ec2 = boto.ec2.connect_to_region(region)
    addresses = boto_ec2.get_all_addresses()
    for addr in addresses:
        if addr.public_ip == eip:
            return addr
    return None
    
    
    
def get_attached_network_interface(instanceId, deviceIndex, region):
    boto_ec2 = boto.ec2.connect_to_region(region)
    enis = boto_ec2.get_all_network_interfaces()

    for eni in enis:
        if eni.attachment and eni.attachment.instance_id == instanceId and eni.attachment.device_index == deviceIndex:
            return eni
    return None


def get_network_interfaces_in_security_group(sgId, region):
    boto_ec2 = boto.ec2.connect_to_region(region)
    enis = boto_ec2.get_all_network_interfaces()

    ret = []
    for eni in enis:
        for sg in eni.groups:
            if sg.id == sgId:
                ret.append(eni)
                break
    return ret


def find_network_interface(name, region):
    boto_ec2 = boto.ec2.connect_to_region(region)
    enis = boto_ec2.get_all_network_interfaces()

    for eni in enis:
        if eni.tags.get("Name", None) == name:
            return eni
    return None


#e.g. 'description': u'ELB mdce-preprod-external-elb', 
def get_network_interfaces_by_description(description, region):
    boto_ec2 = boto.ec2.connect_to_region(region)
    enis = boto_ec2.get_all_network_interfaces()

    ret = [eni for eni in enis if getattr(eni, "description", "").startswith(description)]

    return ret



def ensure_eni_tags(eni, region, tags):
    newTags = {}    

    for tagName in tags:
        if eni.tags.get(tagName, None) != tags[tagName]:
            newTags[tagName] = tags[tagName]
    
    if bool(newTags):
        sys.stdout.write("Updating tags for Elastic Network Interface " + eni.id + "\n")
        boto_ec2 = boto.ec2.connect_to_region(region)
        boto_ec2.create_tags(eni.id, newTags)

# returns the last available NAT gateway, or the first non-available one
def find_nat_gateway(vpc_id, region, subnetId):
    boto3_ec2 = boto3.client('ec2', region_name=region)
    result = boto3_ec2.describe_nat_gateways(Filters=[
        {'Name': 'subnet-id', 'Values': [subnetId]},
        {'Name': 'vpc-id', 'Values': [vpc_id]},
        ])
    if result["NatGateways"]:
        ret = result["NatGateways"][0]
        for gatewayFound in result["NatGateways"]:
            if ret["State"] != "available" and gatewayFound == "available":
                ret = gatewayFound
        return ret
    else:
        return None

def create_nat_gateway(vpc_id, subnetId, eipAllocationId, region):
    sys.stdout.write("creating NAT gateway in subnet " + subnetId + "\n")

    boto3_ec2 = boto3.client('ec2', region_name=region)
    result = boto3_ec2.create_nat_gateway(
        SubnetId=subnetId,
        AllocationId=eipAllocationId
    )
    try:
        ret = result["NatGateway"]
        sys.stdout.write("successfully created NAT gateway with ID " + ret["NatGatewayId"] + "\n")
        return ret
    except:
        sys.stdout.write("Couldn't parse response: " + repr(result))
        return None
    
