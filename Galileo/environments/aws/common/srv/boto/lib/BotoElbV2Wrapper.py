# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 16:37:26 2015

Wrapper for boto3 elbv2 to enable management of AWS 
Application Load Balancers 

See http://boto3.readthedocs.io/en/latest/reference/services/elbv2.html

@author: ABH1
"""


import boto3
import botocore.exceptions

def connect(region):
    return boto3.client('elbv2', region_name=region)

def convertInstanceIdListForBoto(idList):
    return [{'InstanceId': i} for i in idList]

def find_application_elb(name, region):
    boto3_elbv2 = connect(region)
    try:
        resp = boto3_elbv2.describe_load_balancers(Names=[name])
        lbd = resp["LoadBalancers"]
        if lbd:
            return lbd[0]
        else:
            return None
    except botocore.exceptions.ClientError:
        return None

def find_elb_target_group(name, region):
    boto3_elbv2 = connect(region)
    try:
        resp = boto3_elbv2.describe_target_groups(Names=[name])
        tgd = resp["TargetGroups"]
        if tgd:
            return tgd[0]
        else:
            return None
    except botocore.exceptions.ClientError:
        return None

def create_external_application_elb(name, subnetIdList, securityGroupIdList, tags, region):
    boto3_elbv2 = connect(region)
    response = boto3_elbv2.create_load_balancer(
        Name=name,
        Subnets=subnetIdList,
        SecurityGroups=securityGroupIdList,
        Scheme='internet-facing',
        Tags=[{"Key": tagName, "Value": tags[tagName]} for tagName in tags]
    )
    lbd = response["LoadBalancers"]
    if lbd:
        return lbd[0]
    else:
        return None
    

def ensure_application_elb_tags(elbName, tags, region):
    
    boto3_elbv2 = connect(region)
    
    lbd = find_application_elb(elbName, region)
    arn = lbd["LoadBalancerArn"]
    
    tagInfo = boto3_elbv2.describe_tags(ResourceArns=[arn])
    
    newTags = set(tags.keys())
    
    for existingTag in tagInfo["TagDescriptions"][0]["Tags"]:
        if existingTag["Value"] == tags.get(existingTag["Key"], None):
            newTags.remove(existingTag["Key"])
    
    if bool(newTags):
        print("Updating tags " + repr(newTags) + " for ELB " + elbName)
        
        tagsToAdd = [{"Key": tagName, "Value": tags[tagName]} for tagName in newTags]
        
        boto3_elbv2.add_tags(ResourceArns=[arn],
                           Tags=tagsToAdd)

def create_target_group(name, port, vpcId, healthCheckPath, ec2IdList, sticky, region):
    
    boto3_elbv2 = connect(region)
    
    response = boto3_elbv2.create_target_group(
        Name=name,
        Protocol='HTTP',  #always HTTP behind the ELB
        Port=port,
        VpcId=vpcId,
        HealthCheckProtocol='HTTP',
        HealthCheckPath=healthCheckPath,
        HealthCheckIntervalSeconds=30,
        HealthCheckTimeoutSeconds=5,
        HealthyThresholdCount=5,
        UnhealthyThresholdCount=2
    )
    
    tgd = response["TargetGroups"]
    if not tgd:
        return None
    tgArn = tgd[0]["TargetGroupArn"]
    
    boto3_elbv2.register_targets(
        TargetGroupArn=tgArn,
        Targets=[
            {'Id': id} for id in ec2IdList
        ]
    )
    
    boto3_elbv2.modify_target_group_attributes(
        TargetGroupArn=tgArn,
        Attributes=[
            {
                'Key': 'stickiness.enabled',
                'Value': ('true' if sticky else 'false')
            },
            {
                'Key': 'stickiness.type',
                'Value': 'lb_cookie'
            }
        ]
    )
    
    return tgd[0]


def create_listeners(elbName, targetGroupName, externalPort, certArn, region):
    
    boto3_elbv2 = connect(region)
    
    lbd = find_application_elb(elbName, region)
    elbArn = lbd["LoadBalancerArn"]
    
    targetGroupInfo = boto3_elbv2.describe_target_groups(Names=[targetGroupName]) 
    
    targetGroupArn = targetGroupInfo["TargetGroups"][0]["TargetGroupArn"]
    
    response = boto3_elbv2.create_listener(
        LoadBalancerArn=elbArn,
        Protocol='HTTPS',
        Port=externalPort,
        Certificates=[
            {
                'CertificateArn': certArn
            },
        ],
        DefaultActions=[
            {
                'Type': 'forward',
                'TargetGroupArn': targetGroupArn
            },
        ]
    )
    
    return response


def ensure_registered_instances(tgInfo, desiredInstances, region):

    boto3_elbv2 = connect(region)
    tgArn = tgInfo["TargetGroupArn"]
    currentTgStatusResponse = boto3_elbv2.describe_target_health(TargetGroupArn=tgArn)

    currentTgInstances = [targetHealthDescription["Target"]["Id"] for targetHealthDescription in currentTgStatusResponse["TargetHealthDescriptions"]]

    instancesToRegister = set(desiredInstances) - set(currentTgInstances)
    instancesToDeregister = set(currentTgInstances) - set(desiredInstances)
    if instancesToDeregister:
        print("Deregistering EC2 instances " + repr(instancesToDeregister) + " from ELB TG " + tgInfo["TargetGroupName"])
        boto3_elbv2.deregister_targets(TargetGroupArn=tgArn, Targets=[{"Id": id} for id in instancesToDeregister])
    if instancesToRegister:
        print("Registering EC2 instances " + repr(instancesToRegister) + " with ELB TG " + tgInfo["TargetGroupName"])
        boto3_elbv2.register_targets(TargetGroupArn=tgArn, Targets=[{"Id": id} for id in instancesToRegister])
