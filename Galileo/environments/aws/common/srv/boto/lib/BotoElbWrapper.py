# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 16:37:26 2015

Wrapper for boto3 elb to enable management of AWS 
Classic Load Balancers 

See http://boto3.readthedocs.io/en/latest/reference/services/elb.html

@author: ABH1
"""


import boto3
import botocore.exceptions

def connect(region):
    return boto3.client('elb', region_name=region)

def convertInstanceIdListForBoto(idList):
    ret = []
    for i in idList:
        ret.append({'InstanceId': i});
    return ret


def find_elb(name, region):
    boto3_elb = connect(region)
    try:
        resp = boto3_elb.describe_load_balancers(LoadBalancerNames=[name]);
        lbd = resp["LoadBalancerDescriptions"]
        if lbd:
            return lbd[0]
        else:
            return None
    except botocore.exceptions.ClientError:
        return None

def register_instances_with_load_balancer(elbName, instancesToRegister, region):
    boto3_elb = connect(region)
    print("Registering EC2 instances " + repr(instancesToRegister) + " with ELB " + elbName)
    boto3_elb.register_instances_with_load_balancer(
        LoadBalancerName=elbName,
        Instances=convertInstanceIdListForBoto(instancesToRegister)
    )
    

def deregister_instances_from_load_balancer(elbName, instancesToDeregister, region):
    boto3_elb = connect(region)
    print("Deregistering EC2 instances " + repr(instancesToDeregister) + " from ELB " + elbName)
    boto3_elb.deregister_instances_from_load_balancer(
        LoadBalancerName=elbName,
        Instances=convertInstanceIdListForBoto(instancesToDeregister)
    )    

def ensure_registered_instances(elbInfo, desiredInstances, region):
    currentElbInstances = [currentInstanceInfo["InstanceId"] for currentInstanceInfo in elbInfo["Instances"]]

    instancesToRegister = set(desiredInstances) - set(currentElbInstances)
    instancesToDeregister = set(currentElbInstances) - set(desiredInstances)
    if instancesToRegister:
        register_instances_with_load_balancer(elbInfo["LoadBalancerName"],
                                                       instancesToRegister, 
                                                       region)
    if instancesToDeregister:
        deregister_instances_from_load_balancer(elbInfo["LoadBalancerName"],
                                                         instancesToDeregister, 
                                                         region)


def ensure_elb_tags(elbName, region, tags):
    
    boto3_elb = connect(region)
    
    tagInfo = boto3_elb.describe_tags(LoadBalancerNames=[elbName])
    
    newTags = set(tags.keys())
    
    for existingTag in tagInfo["TagDescriptions"][0]["Tags"]:
        if existingTag["Value"] == tags.get(existingTag["Key"], None):
            newTags.remove(existingTag["Key"])
    
    if bool(newTags):
        print("Updating tags " + repr(newTags) + " for ELB " + elbName)
        
        tagsToAdd = []
        for tagName in newTags:
            tagsToAdd.append({"Key": tagName, "Value": tags[tagName]})
        
        boto3_elb.add_tags(LoadBalancerNames=[elbName],
                           Tags=tagsToAdd)

def ensure_sticky_sessions(elbInfo, incomingPort, policyName, cookieName, region):
    boto3_elb = connect(region)
    existingAppCookieStickinessPolicies = elbInfo["Policies"]["AppCookieStickinessPolicies"]
    existingAppCookieStickinessPolicyNames = set()
    found = False
    for ep in existingAppCookieStickinessPolicies:
        existingAppCookieStickinessPolicyNames.add(ep["PolicyName"])
        if ep["PolicyName"] == policyName:
            found = True

    if not found:
        print("Adding app cookie " + cookieName + " stickiness policy " + policyName + " to ELB " + elbInfo["LoadBalancerName"])
        boto3_elb.create_app_cookie_stickiness_policy(
            LoadBalancerName=elbInfo["LoadBalancerName"],
            PolicyName=policyName,
            CookieName=cookieName
        )
    
    for listener in elbInfo["ListenerDescriptions"]:
        if listener["Listener"]["LoadBalancerPort"] == incomingPort:
            listenerPolicies = listener["PolicyNames"]
            if policyName not in listenerPolicies:
                newListenerPolicies = list(set(listenerPolicies) - set(existingAppCookieStickinessPolicyNames))
                newListenerPolicies.append(policyName)
                print("Updating " + elbInfo["LoadBalancerName"] + " listener policies for port " + str(incomingPort) + " from " + repr(listenerPolicies)  + " to " + repr(newListenerPolicies))
                boto3_elb.set_load_balancer_policies_of_listener(
                    LoadBalancerName=elbInfo["LoadBalancerName"],
                    LoadBalancerPort=incomingPort,
                    PolicyNames=newListenerPolicies         
                )
    
    
    
    