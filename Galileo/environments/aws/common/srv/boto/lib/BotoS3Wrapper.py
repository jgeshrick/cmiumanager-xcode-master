# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 17:49:32 2015

@author: abh1
"""

import json
import boto.s3
import sys

from boto.s3.connection import Location 

def connect(region, credentials = None):
    if credentials is None:
        return boto.s3.connect_to_region(region) #use system-wide credentials
    else:
        return boto.s3.connect_to_region(region, 
                                         aws_access_key_id = credentials["aws_access_key_id"], 
                                         aws_secret_access_key = credentials["aws_secret_access_key"])

def find_bucket(name, region, credentials = None):
    boto_s3 = connect(region, credentials)

    return boto_s3.lookup(name)


def create_bucket(name, tags, region, policy = None):
    sys.stdout.write("Creating S3 bucket " + name + "\n")
    boto_s3 = connect(region, None)

    location = {
        'ap-northeast-1': Location.APNortheast,
        'ap-southeast-1': Location.APSoutheast,
        'ap-southeast-2': Location.APSoutheast2,
        'eu-central-1': Location.EU,
        'eu-west-1': Location.EU,
        'sa-east-1': Location.SAEast,
        'us-east-1': Location.DEFAULT,
        'us-west-1': Location.USWest,
        'us-west-2': Location.USWest2
    }[region]

    bucket = boto_s3.create_bucket(name, location = location, policy = policy) #policy (boto.s3.acl.CannedACLStrings) – A canned ACL policy that will be applied to the new key in S3.
    
    tagset = boto.s3.tagging.TagSet()
    for tagName in tags:
        tagset.add_tag(tagName, tags[tagName])
    tagsToAdd = boto.s3.tagging.Tags()
    tagsToAdd.add_tag_set(tagset)
    bucket.set_tags(tagsToAdd)    
    
#TODO: delete.  No longer needed since MSPD-627 was resolved
def update_bucket_policy_permit_ips(bucketName, region, ipList, credentials):
    bucket = find_bucket(bucketName, region, credentials)
    if bucket is not None:
        print("Checking bucket policy on " + bucketName)
        policyString = bucket.get_policy()
        policyJson = json.loads(policyString)
        statements = [statement for statement in policyJson["Statement"] if statement["Effect"] == "Allow" and statement["Principal"] == "*" and statement["Action"] == "s3:GetObject"]
        if statements:
            for statement in statements:
                existingIps = statement["Condition"]["IpAddress"]["aws:SourceIp"]
                newIps = set(ipList).difference(set(existingIps))
                if newIps:
                    print "  Updating bucket policy on " + bucketName + " which currently allows IPs " + str(existingIps) + " to allow additional IPs " + str(newIps)
                    existingIps.extend(newIps)
                    policyString = json.dumps(policyJson)
                    bucket.set_policy(policyString)
                else:
                    print("  Bucket policy okay (allows all IPs of managed EC2 instances)")
        else:
            print "  !!! Error: no IP address whitelist found in bucket policy!"

def upload_html(bucketName, region, path, text, credentials = None):
    bucket = find_bucket(bucketName, region, credentials)
    key = boto.s3.key.Key(bucket)
    key.name = path
    key.content_type = 'text/html'
    key.set_contents_from_string(text)
