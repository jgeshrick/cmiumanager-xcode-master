# -*- coding: utf-8 -*-
"""
Functions to check for existence and create missing VPCs, subnets etc.

Basically a replacement for salt.states.boto_vpc until it exists

@author: abh1
"""


import boto.vpc
import sys
import time


def ask(question):
    sys.stdout.write(question)
    while True:
        choice = raw_input().lower()
        sys.stdout.write("\n")
        if choice == "y":
            return True
        elif choice == "n":
            return False
        else:
            sys.stdout.write("Please press Y or N:")


def find_vpc(vpc_name, region):
    boto_vpc = boto.vpc.connect_to_region(region)

    allVpcs = boto_vpc.get_all_vpcs()
    for vpc in allVpcs:
        if vpc.tags.get("Name", "") == vpc_name:
            return vpc
    return None


def create_vpc(cidr_block, instance_tenancy=None, vpc_name=None, tags=None, region=None, key=None, keyid=None, profile=None):
    if ask("About to create VPC " + vpc_name + " with CIDR block " + cidr_block + ". Proceed? [Y/N]"):
        boto_vpc = boto.vpc.connect_to_region(region)
        vpc = boto_vpc.create_vpc(cidr_block, instance_tenancy)
        vpc.add_tag("Name", vpc_name)
        if tags:
            vpc.add_tags(tags)
        boto_vpc.modify_vpc_attribute(vpc.id, enable_dns_support=True)
        boto_vpc.modify_vpc_attribute(vpc.id, enable_dns_hostnames=True)
        return vpc
    return None


def find_subnet(name, region, vpc_id=None):
    boto_vpc = boto.vpc.connect_to_region(region)

    if vpc_id:
        subnets = boto_vpc.get_all_subnets(filters = {"vpcId": vpc_id})
    else:
        subnets = boto_vpc.get_all_subnets()
    for subnet in subnets:
        if subnet.tags.get("Name", "") == name:
            return subnet
    return None

def get_subnet_by_id(subnetId, region):
    boto_vpc = boto.vpc.connect_to_region(region)
    
    sns = boto_vpc.get_all_subnets(subnet_ids=[subnetId])
    if len(sns):
        return sns[0]
    else:
        return None

def create_subnet(vpc_id, cidr_block, availability_zone=None, subnet_name=None, route_table_id=None, auto_assign_public_ip = False, tags=None, region=None, key=None, keyid=None, profile=None):
    sys.stdout.write("creating subnet " + subnet_name + " with CIDR block " + cidr_block + "\n")
    boto_vpc = boto.vpc.connect_to_region(region)
    subnet = boto_vpc.create_subnet(vpc_id, cidr_block, availability_zone)
    time.sleep(5)
    if auto_assign_public_ip:
        #workaround for another boto gap - see http://stackoverflow.com/questions/25977048/how-to-modify-auto-assign-public-ip-on-subnet-with-boto
        saveApiVersion = boto_vpc.APIVersion
        boto_vpc.APIVersion = '2014-06-15'
        boto_vpc.get_status('ModifySubnetAttribute', {'SubnetId': subnet.id, 'MapPublicIpOnLaunch.Value': 'true'}, verb='POST')
        boto_vpc.APIVersion = saveApiVersion
    subnet.add_tag("Name", subnet_name)
    subnet.add_tags(tags)
    if route_table_id:
        boto_vpc.associate_route_table(route_table_id, subnet.id)
    return subnet
    

def find_internet_gateway(name, region):
    boto_vpc = boto.vpc.connect_to_region(region)

    igs = boto_vpc.get_all_internet_gateways()
    for ig in igs:
        if ig.tags.get("Name", "") == name:
            return ig
    return None


def create_internet_gateway(internet_gateway_name, vpc_id, tags=None, region=None, key=None, keyid=None, profile=None):
    sys.stdout.write("Creating internet gateway " + internet_gateway_name + "\n")
    boto_vpc = boto.vpc.connect_to_region(region)
    ig = boto_vpc.create_internet_gateway()
    ig.add_tag("Name", internet_gateway_name)
    if tags:
        ig.add_tags(tags)
    boto_vpc.attach_internet_gateway(ig.id, vpc_id)
    return ig

def find_vpc_peering_connection(name, region=None):
    boto_vpc = boto.vpc.connect_to_region(region)
    pcs = boto_vpc.get_all_vpc_peering_connections();
    for pc in pcs:
        if pc.tags.get("Name", "") == name:
            return pc
    return None
    
def create_vpc_peering_connection(name, vpc_id, peer_vpc_id, tags=None, region=None, peer_owner_id=None):
    sys.stdout.write("Creating VPC peering connection " + name + " between VPCs " + vpc_id + "," + peer_vpc_id + "\n")
    boto_vpc = boto.vpc.connect_to_region(region)
    pc = boto_vpc.create_vpc_peering_connection(vpc_id, peer_vpc_id, peer_owner_id)
    pc.add_tag("Name", name)
    if tags:
        pc.add_tags(tags)
    boto_vpc.accept_vpc_peering_connection(pc.id)
    return pc
    
def find_route_table(name, region=None):
    boto_vpc = boto.vpc.connect_to_region(region)

    rts = boto_vpc.get_all_route_tables();
    for rt in rts:
        if rt.tags.get("Name", "") == name:
            return rt
    return None

def create_route_table(vpc_id, route_table_name=None, tags=None, region=None, key=None, keyid=None, profile=None):
    sys.stdout.write("Creating route table " + route_table_name + "\n")
    boto_vpc = boto.vpc.connect_to_region(region)
    rt = boto_vpc.create_route_table(vpc_id)
    rt.add_tag("Name", route_table_name)
    if tags:
        rt.add_tags(tags)
    return rt

def create_route(route_table_id, destination_cidr_block, gateway_id=None, instance_id=None, interface_id=None, region=None, key=None, keyid=None, profile=None):
    boto_vpc = boto.vpc.connect_to_region(region)
    route = boto_vpc.create_route(route_table_id, destination_cidr_block, gateway_id, instance_id, interface_id)
    return route

def enable_vgw_route_propagation(route_table_id, gateway_id, region):
    boto_vpc = boto.vpc.connect_to_region(region)
    route = boto_vpc.enable_vgw_route_propagation(route_table_id, gateway_id)
