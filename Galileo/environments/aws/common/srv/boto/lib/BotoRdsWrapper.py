# -*- coding: utf-8 -*-
"""
Functions to check for existence and create missing RDS

@author: abh1
"""


import boto3
import sys

from botocore.exceptions import ClientError


#In boto_rds2, Tags must be passed as tuples in the form [(‘key1’, ‘valueForKey1’), (‘key2’, ‘valueForKey2’)]
def convert_tag_dict_to_aws_format(tags):
    ret = []
    for key in tags:
        ret.append({"Key": key, "Value": tags[key]})
    return ret

def connect(region):
    return boto3.client('rds', region_name=region)

def db_parameter_group_exists(db_parameter_group_name, region):
    try:
      boto3_rds = connect(region)
      ret = boto3_rds.describe_db_parameter_groups(DBParameterGroupName=db_parameter_group_name)
    except ClientError:
        ret = None
    return ret
    
def create_db_parameter_group(db_parameter_group_name, db_parameter_group_family, description, tags, region, parameters):
    boto3_rds = connect(region)
    sys.stdout.write("Creating DB parameter group " + db_parameter_group_name + "\nNote: wait 5 minutes before using this, apparently")
    ret = boto3_rds.create_db_parameter_group(
        DBParameterGroupName=db_parameter_group_name, 
        DBParameterGroupFamily=db_parameter_group_family, 
        Description=description, 
        Tags=convert_tag_dict_to_aws_format(tags)
    )
    boto3_rds.modify_db_parameter_group(
        DBParameterGroupName=db_parameter_group_name, 
        Parameters=parameters
    )
    return ret


def db_subnet_group_exists(db_subnet_group_name, region):
    try:
        boto3_rds = connect(region)
        ret = boto3_rds.describe_db_subnet_groups(DBSubnetGroupName=db_subnet_group_name)
    except ClientError:
        ret = None
    return ret

def create_db_subnet_group(db_subnet_group_name, db_subnet_group_description, subnet_ids, tags, region):
    boto3_rds = connect(region)
    sys.stdout.write("Creating DB subnet group " + db_subnet_group_name + "\n")
    ret = boto3_rds.create_db_subnet_group(
        DBSubnetGroupName=db_subnet_group_name, 
        DBSubnetGroupDescription=db_subnet_group_description, 
        SubnetIds=subnet_ids,
        Tags=convert_tag_dict_to_aws_format(tags)
    )
    return ret
    
    
def find_db_instance(db_instance_identifier, region):
    boto3_rds = connect(region)
    try:
        ret = boto3_rds.describe_db_instances(DBInstanceIdentifier=db_instance_identifier)
        return ret["DBInstances"][0]
    except ClientError:
        return None

def find_db_cluster(db_cluster_identifier, region):
    boto3_rds = connect(region)
    try:
        ret = boto3_rds.describe_db_clusters(DBClusterIdentifier=db_cluster_identifier)
        return ret["DBClusters"][0]
    except ClientError:
        return None

def get_instance_endpoint(db_instance_info): #db_instance_info = returned from find_db_instance
    if db_instance_info is None:
        return None
    endpointInfo = db_instance_info.get("Endpoint", None)
    if endpointInfo is None:
        return None
    return endpointInfo.get("Address", None)

def get_cluster_endpoint(db_cluster_info): #db_cluster_info = returned from find_db_cluster
    if db_cluster_info is None:
        return None
    return db_cluster_info.get("Endpoint", None)


def create_db_cluster(region, tags,
                        DatabaseName, 
                        DBClusterIdentifier,
                        DBClusterParameterGroupName,
                        VpcSecurityGroupIds,
                        DBSubnetGroupName,
                        Engine,
                        EngineVersion,
                        BackupRetentionPeriod,
                        PreferredBackupWindow,
                        MasterUsername,
                        MasterUserPassword):
    boto3_rds = connect(region)
    print "Creating RDS cluster " + DBClusterIdentifier
    
    awsTags = convert_tag_dict_to_aws_format(tags)  
    
    boto3_rds.create_db_cluster(
        DatabaseName = DatabaseName, 
        DBClusterIdentifier = DBClusterIdentifier,
        DBClusterParameterGroupName = DBClusterParameterGroupName,
        VpcSecurityGroupIds = VpcSecurityGroupIds,
        DBSubnetGroupName = DBSubnetGroupName,
        Engine = Engine,
        EngineVersion = EngineVersion,
        BackupRetentionPeriod = BackupRetentionPeriod,
        PreferredBackupWindow = PreferredBackupWindow,
        MasterUsername = MasterUsername,
        MasterUserPassword = MasterUserPassword,
        Tags = awsTags
    )
               


def create_db_instance(db_instance_identifier, db_instance_class, region, 
                       engine, master_username=None, master_user_password=None, allocated_storage = None, storage_type=None, db_cluster_name=None, engine_version=None, db_name=None, db_security_groups=None, vpc_security_group_ids=None, availability_zone=None, 
                       db_subnet_group_name=None, preferred_maintenance_window=None, db_parameter_group_name=None, backup_retention_period=None, 
                       preferred_backup_window=None, port=None, multi_az=None, auto_minor_version_upgrade=None, license_model=None, iops=None, 
                       option_group_name=None, character_set_name=None, publicly_accessible=None, tags=None):
    boto3_rds = connect(region)
    print "Creating RDS instance " + db_instance_identifier

    kwargs = {
        "DBInstanceIdentifier": db_instance_identifier,
        "DBInstanceClass": db_instance_class,
        "Engine": engine
    }
    if master_username is not None:
        kwargs["MasterUsername"]=master_username
    if master_user_password is not None:
        kwargs["MasterUserPassword"]=master_user_password
    if allocated_storage is not None:
        kwargs["AllocatedStorage"]=allocated_storage
    if storage_type is not None:
        kwargs["StorageType"]=storage_type
    if db_cluster_name is not None:
        kwargs["DBClusterIdentifier"]=db_cluster_name
    if engine_version is not None:
        kwargs["EngineVersion"]=engine_version
    if db_name is not None:
        kwargs["DBName"]=db_name
    if db_security_groups is not None:
        kwargs["DBSecurityGroups"]=db_security_groups
    if vpc_security_group_ids is not None:
        kwargs["VpcSecurityGroupIds"]=vpc_security_group_ids
    if availability_zone is not None:
        kwargs["AvailabilityZone"]=availability_zone
    if db_subnet_group_name is not None:
        kwargs["DBSubnetGroupName"]=db_subnet_group_name
    if preferred_maintenance_window is not None:
        kwargs["PreferredMaintenanceWindow"]=preferred_maintenance_window
    if db_parameter_group_name is not None:
        kwargs["DBParameterGroupName"]=db_parameter_group_name
    if backup_retention_period is not None:
        kwargs["BackupRetentionPeriod"]=backup_retention_period
    if preferred_backup_window is not None:
        kwargs["PreferredBackupWindow"]=preferred_backup_window
    if port is not None:
        kwargs["Port"]=port
    if multi_az is not None:
        kwargs["MultiAZ"]=multi_az
    if auto_minor_version_upgrade is not None:
        kwargs["AutoMinorVersionUpgrade"]=auto_minor_version_upgrade
    if license_model is not None:
        kwargs["LicenseModel"]=license_model
    if iops is not None:
        kwargs["Iops"]=iops
    if option_group_name is not None:
        kwargs["OptionGroupName"]=option_group_name
    if character_set_name is not None:
        kwargs["CharacterSetName"]=character_set_name
    if publicly_accessible is not None:
        kwargs["PubliclyAccessible"]=publicly_accessible
    if tags is not None:
        kwargs["Tags"]=convert_tag_dict_to_aws_format(tags)
    #others not covered: TdeCredentialArn, TdeCredentialPassword, KmsKeyId

    ret = boto3_rds.create_db_instance(**kwargs)
    return ret
    