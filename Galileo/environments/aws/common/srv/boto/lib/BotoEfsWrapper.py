# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 16:37:26 2015

@author: ABH1
"""


import boto3


def connect(region):
    return boto3.client('efs', region_name=region)

def find_efs(name, region):
    boto3_efs = connect(region)
    efss = boto3_efs.describe_file_systems()
    for efs in efss["FileSystems"]:
        if efs["Name"] == name:
            return efs
            #note: id property is efs["FileSystemId"]
    return None
