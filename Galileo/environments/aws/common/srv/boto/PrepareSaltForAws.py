#!/usr/bin/python
# -*- coding: utf-8 -*-
"""

This script checks all the AWS IDs in the cache file aws_info_cache.json are still correct
and creates ../salt/aws-info.map.jinja and ../salt/mdce-env-spec.map.jinja
These files are required before running Salt.

"""


import copy

import config.EnvSpecificConstants as config

import lib.AwsCacheFileManager as awsCache
import lib.BotoVpcWrapper as boto_vpc
import lib.BotoEc2Wrapper as boto_ec2
import lib.BotoS3Wrapper as boto_s3
import lib.BotoRdsWrapper as boto_rds2
import lib.BotoEfsWrapper as boto_efs
import lib.BotoElbWrapper as boto_elb

def findAllConfiguredEipsInRegion(region):
    ret = set()
    for vpcName in config.vpcs:
        vpc = config.vpcs[vpcName]
        if vpc["region"] == region:
            for subnet in vpc.get("accountLevelSubnets", []):
                for subnetMemberName in subnet:
                    subnetMember = subnet[subnetMemberName]
                    if isinstance(subnetMember, dict) and "elasticIp" in subnetMember:
                        ret.add(subnetMember["elasticIp"])
    for envName in config.envs:
        env = config.envs[envName]
        if config.vpcs[env["mnoVpc"]]["region"] == region:
            for subnet in (env["template"].get("mnoVpcSubnets", []) + env["template"].get("mdceVpcSubnets", [])):
                for subnetMemberName in subnet:
                    subnetMember = subnet[subnetMemberName]
                    if isinstance(subnetMember, dict) and "elasticIp" in subnetMember:
                        ret.add(subnetMember["elasticIp"])
            for serverWithIp in env.get("elasticIps", []):
                ret.add(env["elasticIps"][serverWithIp])
    return ret

def checkRegion(region):

    originalCategories = copy.copy(awsCache.awsInfoCache[region])

    allEipsInRegion = findAllConfiguredEipsInRegion(region)

    for category in originalCategories:
        catMap = copy.copy(awsCache.awsInfoCache[region][category])
        print "checking " + category + " (" + str(len(catMap.keys())) + " cached items)"
        if category == "vpcId":
            for vpcName in catMap:
                vpc = boto_vpc.find_vpc(vpc_name=vpcName, region=region)
                awsCache.setVpcId(region, vpcName, getattr(vpc, "id", ""))
        elif category == "vpcPeeringConnectionId":
            for vpcpcName in catMap:
                vpcpc = boto_vpc.find_vpc_peering_connection(name=vpcpcName, region=region)
                awsCache.setVpcPeeringConnectionId(region, vpcpcName, getattr(vpcpc, "id", ""))
        elif category == "igId":
            for igName in catMap:
                ig = boto_vpc.find_internet_gateway(name=igName, region=region)
                awsCache.setInternetGatewayId(region, igName, getattr(ig, "id", ""))
        elif category == "rtId":
            for rtName in catMap:
                rt = boto_vpc.find_route_table(name=rtName, region=region)
                awsCache.setRouteTableId(region, rtName, getattr(rt, "id", ""))
        elif category == "subnetId":
            for vpcId in catMap:
                for snName in catMap[vpcId]:
                    subnet = boto_vpc.find_subnet(name=snName, region=region, vpc_id=vpcId)
                    awsCache.setSubnetId(region, vpcId, snName, getattr(subnet, "id", ""))
        elif category == "sgId":
            for vpcId in catMap:
                for sgName in catMap[vpcId]:
                    sg = boto_ec2.find_security_group(secgroupName=sgName, region=region, vpc_id=vpcId)
                    awsCache.setSecurityGroupId(region, vpcId, sgName, getattr(sg, "id", ""))
        elif category == "natGatewayId":
            for vpcId in catMap:
                for ngSubnetName in catMap[vpcId]:
                    subnet = boto_vpc.find_subnet(name=ngSubnetName, region=region, vpc_id=vpcId)
                    if subnet:
                        ng = boto_ec2.find_nat_gateway(subnetId=subnet.id, region=region, vpc_id=vpcId)
                        awsCache.setNatGatewayId(region, vpcId, ngSubnetName, ng["NatGatewayId"])
        elif category == "ec2Id":
            for ec2Name in catMap:
                instance = boto_ec2.find_instance(ec2Name, region)
                awsCache.setEc2Id(region, ec2Name, getattr(instance, "id", ""))
                awsCache.setEc2IpAddresses(region, ec2Name, getattr(instance, "ip_address", ""), getattr(instance, "private_ip_address", ""))
                if instance is not None:
                    subnet = boto_vpc.get_subnet_by_id(instance.subnet_id, region=region)
                    if subnet is not None:
                        az = subnet.availability_zone
                        awsCache.setEc2Az(region, ec2Name, az)
                    else:
                        awsCache.setEc2Az(region, ec2Name, "")
                else:
                    awsCache.setEc2Az(region, ec2Name, "")
        elif category == "ebsId":
            for ebsName in catMap:
                volume = boto_ec2.find_volume(ebsName, region)
                awsCache.setEbsId(region, ebsName, getattr(volume, "id", ""))
        elif category == "rdsEndpoint":
            for rdsInstanceOrClusterName in catMap:
                rdsCluster = boto_rds2.find_db_cluster(db_cluster_identifier=rdsInstanceOrClusterName, region=region)
                if rdsCluster is not None:
                    awsCache.setRdsEndpoint(region, rdsInstanceOrClusterName, boto_rds2.get_cluster_endpoint(rdsCluster))
                else:
                    rdsInstance = boto_rds2.find_db_instance(db_instance_identifier=rdsInstanceOrClusterName, region=region)
                    if rdsInstance is not None:
                        awsCache.setRdsEndpoint(region, rdsInstanceOrClusterName, boto_rds2.get_instance_endpoint(rdsInstance))
                    else:
                        awsCache.setRdsEndpoint(region, rdsInstanceOrClusterName, "")
        elif category == "elasticIpAllocationId":
            for eip in set(catMap).union(allEipsInRegion):
                eipDetails = boto_ec2.find_elastic_ip(eip=eip, region=region)
                awsCache.setElasticIpAllocationId(region, eip, getattr(eipDetails, "allocation_id", ""))
        elif category == "eniId":
            for eniName in catMap:
                eniDetails = boto_ec2.find_network_interface(name=eniName, region=region)
                awsCache.setEniId(region, eniName, getattr(eniDetails, "id", ""))
        elif category == "efsId":
            for efsName in catMap:
                efsDetails = boto_efs.find_efs(name=efsName, region=region)
                fsId = efsDetails["FileSystemId"] if efsDetails is not None else ""
                awsCache.setEfsId(region, efsName, fsId)
        elif category == "elbDns":
            for elbName in catMap:
                elbDetails = boto_elb.find_elb(name=elbName, region=region)
                dnsName = elbDetails["DNSName"] if elbDetails is not None else ""
                awsCache.setElbDnsName(region, elbName, dnsName)
        elif category != "publicIp" and category != "privateIp" and category != "ec2Az":
            raise Exception('Unrecognised category ' + category)


# start script

awsCache.load()


print "Preparing AWS files for Salt"

for region in awsCache.awsInfoCache:
    print "Region " + region
    checkRegion(region)

# write IDs to cache file
awsCache.save()

# Read in apns.csv and write apns.sql
fileDir = "./../salt/galileo/"
with open(fileDir + "apns.csv", "r") as fRead, open(fileDir + "apns.sql", "w") as fWrite:
    fWrite.write("DELETE FROM mdce.apn_lookup;\n")
    for line in fRead:
        mnoAndApn = tuple(val.strip() for val in line.split(",", 1))
        fWrite.write("INSERT INTO apn_lookup (mobile_network_operator, apn) VALUES (\"{0}\", \"{1}\");\n".
                     format(*mnoAndApn))
