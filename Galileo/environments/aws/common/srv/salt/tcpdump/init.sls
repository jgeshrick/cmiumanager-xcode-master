#!yaml_jinja

# Install and run the tcpdump service
#

tcpdump:
  pkg.installed

chcon -t bin_t /usr/sbin/tcpdump:
  cmd.run

/var/packetdump:
  file.directory:
    - user: root 
    - group: root 
    - mode: 777
    - makedirs: True

/lib/systemd/system/mdce-tcpdump.service:
  file.managed:
    - source: salt://tcpdump/systemd/mdce-tcpdump.service
    - user: root
    - group: root
    - mode: 555

mdce-tcpdump:
  service.running:
    - enable: True 
    - require:
      - file: /lib/systemd/system/mdce-tcpdump.service

