#!yaml_jinja

# Amazon CloudWatch Monitoring Scripts for Linux v1.2.1
# See http://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/mon-scripts-perl.html

{% set MON_SCRIPTS_VERSION = '1.2.1' %}

unzip:
  pkg:
    - installed

perl:
  pkg:
    - installed

perl-DateTime:
  pkg:
    - installed
    - require:
      - pkg: perl

perl-Sys-Syslog:
  pkg:
    - installed
    - require:
      - pkg: perl

perl-LWP-Protocol-https:
  pkg:
    - installed
    - require:
      - pkg: perl

perl-Digest-SHA:
  pkg:
    - installed
    - require:
      - pkg: perl

perl-Regexp-Common:
  pkg:
    - installed
    - require:
      - pkg: perl

aws-scripts-mon:
  archive.extracted:
    - name: /opt/
    - source: http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-{{ MON_SCRIPTS_VERSION }}.zip
    - source_hash: sha1=98fb930930b147d0a9cbe0e9bee4eb9c3aee3259
    - archive_format: zip
    - if_missing: /opt/aws-scripts-mon
    - require:
      - pkg: unzip
      - file: /opt

#cron to send Linux metrics every 5 mins.  Metrics appear in CloudWatch under "Linux System"
perl /opt/aws-scripts-mon/mon-put-instance-data.pl --mem-util --disk-space-util --disk-path=/ --from-cron:
  cron.present:
    - user: root
    - minute: '*/5'
    - require:
      - archive: aws-scripts-mon
      - pkg: perl-DateTime
      - pkg: perl-Sys-Syslog
      - pkg: perl-LWP-Protocol-https
      - pkg: perl-Digest-SHA    
