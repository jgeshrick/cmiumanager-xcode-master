#!/usr/bin/perl -w

# Send MDCE custom metrics to AWS CloudWatch
# This consists of System/Java namespace, Application, mdce-tomcat, metric OldSpaceUtilization (%)

use strict;
use warnings;

use Regexp::Common;
use FindBin;                 # locate this script
use lib "$FindBin::Bin/..";  # use the parent directory
use CloudWatchClient;
use Date::Parse;

my $envName;
my $serverName;

&get_server_info();

my @metrics = ();

# OldSpaceUtilization for mdce-tomcat

&get_old_space_utilization(\@metrics, 'mdce-tomcat');

&get_old_space_utilization(\@metrics, 'mdce-lora-tomcat');

# Application log errors

&get_app_log_errors(\@metrics, 900);

# Web log errors

&get_web_log_errors(\@metrics, 'mdce-tomcat', 900);
&get_web_log_errors(\@metrics, 'mdce-lora-tomcat', 900);

# now send

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
if ($min % 5 == 0)
{
	my %params = ();
	$params{'Input'} = {};
	my $input_ref = $params{'Input'}; 
	$input_ref->{'Namespace'} = "System/Java";
	$input_ref->{'MetricData'} = \@metrics;


	my %opts = ();
	$opts{'retries'} = 2;
	$opts{'verbose'} = 0;
	$opts{'verify'} = 0;
	$opts{'user-agent'} = "mdce-put-metrics";
	$opts{'enable_compression'} = 1;

	my $response = CloudWatchClient::call_json('PutMetricData', \%params, \%opts);
	my $code = $response->code;
	my $message = $response->message;

	if ($code == 200) {
		my $request_id = $response->headers->{'x-amzn-requestid'};
		print "\nSuccessfully reported metrics to CloudWatch. Reference Id: $request_id\n\n";
	}
	elsif ($code < 100) {
		exit_with_error($message);
	}
	elsif ($code != 200) {
		exit_with_error("Failed to call CloudWatch: HTTP $code. Message: $message");
	}
	
}


sub get_old_space_utilization (\@)
{
	my $metrics = shift;
	my $app_name = shift;
	my $pid_file = "/opt/tomcat/bin/$app_name-catalina.pid";
	my $stats_tracking_file = "$app_name-stats-tracking.dat";

	my $previous_old_space_utilization = 100;
	my $previous_fgc = -1;
	my $old_space_utilization_after_fgc = -1;
	
	if (open(FILE, $stats_tracking_file))
	{
		chomp(my @stats_tracking = <FILE>);
		close FILE;
		if (@stats_tracking > 0 && $RE{num}{int}->matches($stats_tracking[0]))
		{
			$previous_old_space_utilization = $stats_tracking[0];
		}
		if (@stats_tracking > 1 && $RE{num}{int}->matches($stats_tracking[1]))
		{
			$old_space_utilization_after_fgc = $stats_tracking[1];
		}
		if (@stats_tracking > 2 && $RE{num}{int}->matches($stats_tracking[2]))
		{
			$previous_fgc = $stats_tracking[2];
		}
	}

	if (open(FILE, $pid_file))
	{
		my $pid = <FILE>;
		close FILE;

		my $jstat_info=`/usr/local/java/jdk/bin/jstat -gcutil $pid`;
		my @jstat_fields = split(' ', $jstat_info);

		my $old_space_utilization = $jstat_fields[14];
		my $fgc = $jstat_fields[19];
		
		if (defined($previous_old_space_utilization) && $old_space_utilization < $previous_old_space_utilization && $fgc != $previous_fgc)
		{
			# a drop in old space utilization and change in FGC count indicates a FGC has occurred
			$old_space_utilization_after_fgc = $old_space_utilization
		}


		my $timestamp = CloudWatchClient::get_offset_time(0);
		my $instance_id = CloudWatchClient::get_instance_id();

		my $value = $old_space_utilization;
		
		my $dimensions = [
			{"Name" => "Server", "Value" => $serverName}, 
			{"Name" => "Environment", "Value" => $envName}, 
			{"Name" => "Application", "Value" => $app_name}
		] ;

		my $metric = {};
		$metric->{"MetricName"} = "OldSpaceUtilization";
		$metric->{"Timestamp"} = $timestamp;
		$metric->{"RawValue"} = $value;
		$metric->{"Unit"} = "Percent";
		$metric->{"Dimensions"} = $dimensions; 
		
		push @$metrics, ($metric);
		
		if ($old_space_utilization_after_fgc != -1)
		{
			my $metric = {};

			$metric->{"MetricName"} = "OldSpaceRetainedUtilization";
			$metric->{"Timestamp"} = $timestamp;
			$metric->{"RawValue"} = $old_space_utilization_after_fgc;
			$metric->{"Unit"} = "Percent";
			$metric->{"Dimensions"} = $dimensions; 

			push @$metrics, ($metric);

		}
		
		my $stats_tracking_file_handle;
		if (open($stats_tracking_file_handle, ">", $stats_tracking_file))
		{
			print $stats_tracking_file_handle "$old_space_utilization\n";
			print $stats_tracking_file_handle "$old_space_utilization_after_fgc\n";
			print $stats_tracking_file_handle "$fgc\n";
			close $stats_tracking_file_handle;
		}
	}
	else
	{
		return undef;
	}
}

sub get_app_log_errors (\@)
{
	my $metrics = shift;
	my $max_seconds_ago = shift;
	
	my %app_name_to_err_count_map;
	
	# find files modified in last hour
	my $path = '/var/share/currentserver/log/mdce';
	opendir DIR, $path;
	my @dir = readdir(DIR);
	close DIR;
	foreach (@dir) {
		if (($max_seconds_ago/86400.0) > -M "$path/$_") {
			if (/^([a-z\-]+)(?:\-[0-9\-]+)?\.log$/) {
				my $app_name = $1;
				$app_name_to_err_count_map{$app_name} = ($app_name_to_err_count_map{$app_name} || 0) + count_errors_in_app_log("$path/$_", $max_seconds_ago);
			}
		}
	}
	for my $app_name (keys %app_name_to_err_count_map) {

		my $timestamp = CloudWatchClient::get_offset_time(0);
		my $instance_id = CloudWatchClient::get_instance_id();

		my $value = $app_name_to_err_count_map{$app_name};
		
		my $dimensions = [
			{"Name" => "Server", "Value" => $serverName}, 
			{"Name" => "Environment", "Value" => $envName}, 
			{"Name" => "Application", "Value" => $app_name}
		] ;

		my $metric = {};
		$metric->{"MetricName"} = "AppLogErrors";
		$metric->{"Timestamp"} = $timestamp;
		$metric->{"RawValue"} = $value;
		$metric->{"Unit"} = "Count";
		$metric->{"Dimensions"} = $dimensions; 
		
		push @$metrics, ($metric);				
	}
	
}

sub web_log_name_to_app_name
{
	$_ = shift;
	if (/^localhost_80_443_access\..*\.log$/) {
		return 'mdce-web';
	} elsif (/^localhost_9081_9444_access\..*\.log$/) {
		return 'mdce-web';
	} elsif (/^localhost_8080_8443_access\..*\.log$/) {
		return 'mdce-mqtt';
	} elsif (/^localhost_8081_8444_access\..*\.log$/) {
		return 'mdce-integration';
	} elsif (/^localhost_8082_8445_access\..*\.log$/) {
		return 'fpc';
	} elsif (/^localhost_8083_8446_access\..*\.log$/) {
		return 'mdce-lora';
	} else {
		return 'unknown';
	}
	
}

sub get_web_log_errors (\@)
{
	my $metrics = shift;
	my $tomcat_instance_name = shift;
	my $max_seconds_ago = shift;
	
	my %app_name_to_err_count_map;
	
	# find files modified in last hour
	my $path = "/var/share/currentserver/log/$tomcat_instance_name";
	opendir DIR, $path;
	my @dir = readdir(DIR);
	close DIR;
	foreach (@dir) {
		if (($max_seconds_ago/86400.0) > -M "$path/$_") {
			if (/^.*_access.*\.log$/) {
				my $app_name = web_log_name_to_app_name($_);
				$app_name_to_err_count_map{$app_name} = ($app_name_to_err_count_map{$app_name} || 0) + count_errors_in_web_log("$path/$_", $max_seconds_ago);
			}
		}
	}
	for my $app_name (keys %app_name_to_err_count_map) {

		my $timestamp = CloudWatchClient::get_offset_time(0);
		my $instance_id = CloudWatchClient::get_instance_id();

		my $value = $app_name_to_err_count_map{$app_name};
		
		my $dimensions = [
			{"Name" => "Server", "Value" => $serverName}, 
			{"Name" => "Environment", "Value" => $envName}, 
			{"Name" => "Application", "Value" => $app_name}
		] ;

		my $metric = {};
		$metric->{"MetricName"} = "WebLogErrors";
		$metric->{"Timestamp"} = $timestamp;
		$metric->{"RawValue"} = $value;
		$metric->{"Unit"} = "Count";
		$metric->{"Dimensions"} = $dimensions; 
		
		push @$metrics, ($metric);				
	}
	
}

sub count_errors_in_app_log {
	my $file_path = shift;
	my $max_seconds_ago = shift;

	my $ret = 0;

	open my $fh, '<', $file_path or return 0;
	while (my $line = <$fh>) {
		if ($line =~ /^([0-9\-]+ [0-9\:\.]+ [[+\-]?[0-9]+) .* ERROR .*$/) {
			my $dateString = $1;
			my $timestamp = str2time($dateString);
			my $currentTime = time();
			my $seconds_ago = $currentTime - $timestamp;
			if ($seconds_ago < $max_seconds_ago) {
				$ret = $ret + 1;
			}
		}
	}
	return $ret;
}

sub count_errors_in_web_log {
	my $file_path = shift;
	my $max_seconds_ago = shift;

	my $ret = 0;

	open my $fh, '<', $file_path or return 0;
	while (my $line = <$fh>) {
		if ($line =~ /^.*\[(.*)\].* 500 [0-9]+$/) {
			my $dateString = $1;
			my $timestamp = str2time($dateString);
			my $currentTime = time();
			my $seconds_ago = $currentTime - $timestamp;
			if ($seconds_ago < $max_seconds_ago) {
				$ret = $ret + 1;
			}
		}
	}
	
	return $ret;
}

sub get_server_info {
	open my $fh, '<', '/etc/mdce/conf.d/mdce-env.properties' or die;
	while (my $line = <$fh>) {
		if ($line =~ /^env\.name=(.+)$/) {
			$envName = $1;
		} elsif ($line =~ /^server\.name=(.+)$/) {
			$serverName = $1;
		}
	}
}


# Prints out or logs an error and then exits.
sub exit_with_error {
	my $message = shift;
	print STDERR $message;

	exit 1;
}



