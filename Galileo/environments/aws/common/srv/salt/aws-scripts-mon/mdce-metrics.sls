#!yaml_jinja

#Custom perl script to get a custom monitoring metric for the MDCE Java process

/opt/aws-scripts-mon/custom:
  file.directory:
    - user: mdce
    - group: mdce
    - mode: 755
    - require:
      - archive: aws-scripts-mon

/opt/aws-scripts-mon/custom/mdce-put-metrics.pl:
  file.managed:
    - source: salt://aws-scripts-mon/mdce-put-metrics.pl
    - user: mdce
    - group: mdce
    - mode: 755
    - require:
      - file: /opt/aws-scripts-mon/custom

#allow mdce user to write to the aws-mon cache
/var/mdce/aws-mon:
  file.directory:
    - mode: 777
    - makedirs: True
    - recurse:
      - mode
    - require:
      - archive: aws-scripts-mon

#cron to check Java Tomcat metrics every minute.  The metrics are sent every 5 minutes.
# Metrics appear in CloudWatch under "System/Java"
perl /opt/aws-scripts-mon/custom/mdce-put-metrics.pl:
  cron.present:
    - user: mdce    #must run as MDCE user to be able to connect to mdce-tomcat application without the use of jstatd
    - minute: '*/1'
    - require:
      - file: /opt/aws-scripts-mon/custom/mdce-put-metrics.pl
      - file: /var/mdce/aws-mon
      - pkg: perl-DateTime
      - pkg: perl-Sys-Syslog
      - pkg: perl-LWP-Protocol-https
      - pkg: perl-Digest-SHA
      - pkg: perl-Regexp-Common
