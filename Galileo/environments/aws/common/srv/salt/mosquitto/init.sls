#!yaml_jinja

# Mosquitto MQTT broker for CentOS

# URLs from http://mosquitto.org/download/
# sha512 hashes generated at http://hash.online-convert.com/sha512-generator
{% if grains.os == "CentOS" %}
{%   set repoUrl = "http://download.opensuse.org/repositories/home:/oojah:/mqtt/CentOS_CentOS-7/home:oojah:mqtt.repo" %}
{%   set repoHash = "sha512=ebd8b108de4e4908df22a28edeca6421fea1a7e5b85b513d42b3646d7efe69d9f1984e023ecdc94084fbbc110ef7bb277883cd175005445701342191d143aa60" %} 
{% elif grains.os == "RedHat" %}
{%   set repoUrl = "http://download.opensuse.org/repositories/home:/oojah:/mqtt/RedHat_RHEL-7/home:oojah:mqtt.repo"  %}
{%   set repoHash = "sha512=9c1130c418d7eb0bb00ae01af8aab684b35d5be21701549254467e77b3cba20793f26252e576b05603a8b8d31a5903f2920fd3de883e1e71c543c3b7d076a55b" %}
{% endif %}

'/etc/yum.repos.d/home:oojah:mqtt.repo':
  file.managed:
    - user: {{ grains.defaultUser }}
    - group: {{ grains.defaultUser }}
    - mode: 664
    - source: {{ repoUrl }}
    - source_hash: {{ repoHash }}

/var/share/currentserver/log/mosquitto:
  file.directory:
    - user: {{ grains.defaultUser }}
    - group: {{ grains.defaultUser }}
    - mode: 777
    - makedirs: True
    - require:
      - file: /var/share/currentserver  #NFS
   
/etc/mosquitto/mosquitto.conf:
  file.managed:
    - source: salt://mosquitto/mosquitto.conf
    - user: mosquitto
    - group: mosquitto
    - mode: 755
    - require:
      - pkg: mosquitto
   

/etc/mosquitto/conf.d/log.conf:
  file.managed:
    - source: salt://mosquitto/log.conf
    - user: mosquitto
    - group: mosquitto
    - mode: 755
    - makedirs: True
    - require:
      - pkg: mosquitto

/etc/mosquitto/conf.d/queue.conf:
  file.managed:
    - source: salt://mosquitto/queue.conf
    - user: mosquitto
    - group: mosquitto
    - mode: 755
    - makedirs: True
    - require:
      - pkg: mosquitto

/var/share/currentserver/data:
  file.directory:
    - user: mosquitto
    - group: mosquitto
    - mode: 755
    - makedirs: False
    - require:
      - file: /var/share/currentserver

/var/share/currentserver/data/mosquitto:
  file.directory:
    - user: mosquitto
    - group: mosquitto
    - mode: 755
    - makedirs: False
    - require:
      - file: /var/share/currentserver/data
      
mosquitto:
  pkg.installed:
    - require:
      - file: '/etc/yum.repos.d/home:oojah:mqtt.repo'
  service.running:
    - enable: True
    - require:
      - pkg: mosquitto
      - file: /var/share/currentserver/log/mosquitto
      - file: /var/share/currentserver/data/mosquitto
      - file: /etc/mosquitto/mosquitto.conf
      - file: /etc/mosquitto/conf.d/log.conf
      - file: /etc/mosquitto/conf.d/queue.conf
