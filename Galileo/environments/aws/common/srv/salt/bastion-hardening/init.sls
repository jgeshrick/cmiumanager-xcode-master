# Install unzip - needed for Salt config distribution.  See MSPD-2227
unzip:
  pkg:
    - installed

# Remove Postfix service

postfix:
  pkg:
    - removed

# Remove rpcbind

rpcbind:
  pkg:
    - removed

# Run yum updates

pkg.upgrade:
  module.run:
    - refresh: True
