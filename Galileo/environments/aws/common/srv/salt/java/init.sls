#!yaml_jinja

# Oracle JDK8

{% set JDK_VERSION = '1.8.0_45' %}
{% set JDK_MARKETING_VERSION = '8u45' %}
{% set JDK_FOLDER = JDK_MARKETING_VERSION + '-b14' %}

/usr/local/java:
  file.directory:
    - user: {{ grains.defaultUser }}
    - group: {{ grains.defaultUser }}
    - mode: 755
    - makedirs: True

salt://java/download_java.sh:
  cmd.script:
    - env:
      - JDK_URL: http://download.oracle.com/otn-pub/java/jdk/{{ JDK_FOLDER }}/jdk-{{ JDK_MARKETING_VERSION }}-linux-x64.tar.gz
    - require:
      - file: /usr/local/java
    - unless:
      - ls /usr/local/java/jdk{{ JDK_VERSION }}/bin/java
    

/usr/local/java/jdk:
  file.symlink:
    - target: /usr/local/java/jdk{{ JDK_VERSION }}


/etc/profile.d/java.sh:
  file.managed:
    - source: salt://java/java.sh
    - user: root
    - group: root
    - mode: 555
