#Script to download JDK. 
#Ensure this file has Unix line ends

curl -v -j -k -L -H "Cookie: oraclelicense=accept-securebackup-cookie" \
	${JDK_URL} \
	| tar xz -C /usr/local/java/
