#!yaml_jinja

# FPC deployment
# Note: S3 credentials are in /srv/pillar/neptune-binary-distribution-manager.sls


{% set ENV_NAME = grains['env'] %}
{% from "galileo/"~ENV_NAME~"/mdce-app-versions.map.jinja" import mdceAppVersions %}
{% set DISTRO_ROOT="s3://neptune-mdce-deployment" %}

/opt/tomcat/webapps-8082-8445/fpc.war:
  file.managed:
    - source: {{ DISTRO_ROOT }}/fpc/{{ mdceAppVersions["fpc"] }}/fpc.war
    - source_hash: {{ DISTRO_ROOT }}/fpc/{{ mdceAppVersions["fpc"] }}/fpc.war.sha1
    - user: mdce
    - group: mdce
    - mode: 644
    - makedirs: True
    - require:
      - user: mdce-user
      - file: /opt/tomcat
    - watch_in:
      - service: mdce-tomcat
    - prereq_in:
      - cmd: shutdown-mdce-tomcat   #shut down before deploy

rm -rf /opt/tomcat/webapps-8082-8445/fpc: #remove exploded WAR to force Tomcat to re-explode it
  cmd.wait:
    - watch:
      - file: /opt/tomcat/webapps-8082-8445/fpc.war
    - require_in:
      - service: mdce-tomcat
