/etc/hosts:
  file.managed:
    - source: salt://hosts/salt-master/hosts
    - user: root
    - group: root
    - mode: 644
    - makedirs: False
    - template: jinja
