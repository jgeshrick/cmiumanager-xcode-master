#!yaml_jinja

# Fake NFS - just a local folder

/var/fake_nfs:
  file.directory:
    - user: root
    - group: root
    - mode: 777
    - makedirs: True

/var/fake_nfs/{{ grains.id }}:
  file.directory:
    - user: root
    - group: root
    - mode: 777
    - makedirs: False
    - require: 
      - file: /var/fake_nfs

/var/fake_nfs/htdocs:
  file.directory:
    - user: root
    - group: root
    - mode: 777
    - makedirs: False
    - require: 
      - file: /var/fake_nfs


/var/share:
  file.directory:
    - user: root
    - group: root
    - mode: 777
    - makedirs: True

/var/share/allservers:
  file.symlink:
    - target: /var/fake_nfs
    - require:
      - file: /var/fake_nfs
      
/var/share/currentserver:
  file.symlink:
    - target: /var/fake_nfs/{{ grains.id }}
    - require:
      - file: /var/fake_nfs/{{ grains.id }}
      - file: /var/share

/var/share/htdocs:
  file.symlink:
    - target: /var/fake_nfs/htdocs
    - require:
      - file: /var/fake_nfs/htdocs
      - file: /var/share