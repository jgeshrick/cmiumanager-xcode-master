#!yaml_jinja

# Mount NFS for Java applications

{% from "aws-info.map.jinja" import awsInfo %}
{% from "mdce-env-spec.map.jinja" import vpcs %}
{% from "mdce-env-spec.map.jinja" import envs %}

{% set ENV_NAME = grains.env %}

{% set mnoVpcInfo = vpcs[envs[ENV_NAME]["mnoVpc"]] %}
{% set REGION = mnoVpcInfo["region"] %}
{% set MNO_VPC_ID = awsInfo[REGION]["vpcId"][mnoVpcInfo["vpcName"]] %}

{% set az = awsInfo[REGION]["ec2Az"][grains.id] %}
{% set efsId = awsInfo[REGION]["efsId"]["mdce-" ~ ENV_NAME ~ "-efs"] %}
{% set remoteShare = az ~ "." ~ efsId ~ ".efs." ~ REGION ~ ".amazonaws.com:/" %}

nfs-utils:
  pkg:
    - installed

/mnt/nfs:
  mount.mounted:
    - fstype: nfs4
    - device: {{ remoteShare }}
    - mkmnt: True
    - persist: True
    - opts:
      - defaults

/mnt/nfs/{{ grains.id }}:
  file.directory:
    - user: root
    - group: root
    - mode: 777
    - makedirs: False
    - require: 
      - mount: /mnt/nfs

/mnt/nfs/htdocs:
  file.directory:
    - user: root
    - group: root
    - mode: 777
    - makedirs: False
    - require: 
      - mount: /mnt/nfs
      
/var/share:
  file.directory:
    - user: root
    - group: root
    - mode: 777
    - makedirs: True

/var/share/currentserver:
  file.symlink:
    - target: /mnt/nfs/{{ grains.id }}
    - require:
      - file: /mnt/nfs/{{ grains.id }}
      - file: /var/share

/var/share/allservers:
  file.symlink:
    - target: /mnt/nfs
    - require:
      - mount: /mnt/nfs
      - file: /var/share

/var/share/htdocs:
  file.symlink:
    - target: /mnt/nfs/htdocs
    - require:
      - file: /mnt/nfs/htdocs
      - file: /var/share