#!yaml_jinja

# Mount NFS for Java applications

{% from "aws-info.map.jinja" import awsInfo %}
{% from "mdce-env-spec.map.jinja" import vpcs %}
{% from "mdce-env-spec.map.jinja" import envs %}
{% from "mdce-env-spec.map.jinja" import MNO_PREFIX %}

{% set ENV_NAME = grains.env %}

{% set mnoVpcInfo = vpcs[envs[ENV_NAME]["mnoVpc"]] %}
{% set REGION = mnoVpcInfo["region"] %}
{% set MNO_VPC_ID = awsInfo[REGION]["vpcId"][mnoVpcInfo["vpcName"]] %}

{% set az = awsInfo[REGION]["ec2Az"][grains.id] %}
{% set efsId = awsInfo[REGION]["efsId"][MNO_PREFIX ~ ENV_NAME ~ "-efs"] %}
{% set remoteShare = az ~ "." ~ efsId ~ ".efs." ~ REGION ~ ".amazonaws.com:/" %}

nfs-utils:
  pkg:
    - installed

/mnt/nfs:
  mount.mounted:
    - fstype: nfs4
    - device: {{ remoteShare }}
    - mkmnt: True
    - persist: True
    - opts:
      - defaults

/mnt/nfs/{{ grains.id }}:
  file.directory:
    - user: {{ grains.defaultUser }}
    - group: {{ grains.defaultUser }}
    - mode: 777
    - makedirs: True
    - require: 
      - mount: /mnt/nfs
    
/var/share:
  file.directory:
    - user: {{ grains.defaultUser }}
    - group: {{ grains.defaultUser }}
    - mode: 777
    - makedirs: True

/var/share/allservers:
  file.symlink:
    - target: /mnt/nfs
    - require:
      - mount: /mnt/nfs
      - file: /var/share

/var/share/currentserver:
  file.symlink:
    - target: /mnt/nfs/{{ grains.id }}
    - require:
      - file: /mnt/nfs/{{ grains.id }}
      - file: /var/share
