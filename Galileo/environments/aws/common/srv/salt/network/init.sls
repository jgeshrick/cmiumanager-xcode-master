#!yaml_jinja

# Linux in AWS defaults to jumbo packets, but to allow large block
# sizes for image download and also to enable Mosquitto RPM
# to be downloaded, it needs to be reduced to 1400.

# also create /opt folder if not exists

set-mtu:
  cmd.run:
    - name: ip link set dev eth0 mtu 1400
    - unless: ip link show eth0 | grep "mtu 1400"

/etc/sysconfig/network-scripts/ifcfg-eth0:
  file.append:
    - text: MTU=1400

service network restart eth0:
  cmd.wait:
    - watch:
      - file: /etc/sysconfig/network-scripts/ifcfg-eth0

/opt:
  file.directory:
    - user: {{ grains.defaultUser }}
    - group: {{ grains.defaultUser }}
    - mode: 755
    - makedirs: True
