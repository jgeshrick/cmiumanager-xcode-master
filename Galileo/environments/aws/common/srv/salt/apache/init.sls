#!yaml_jinja

# Apache web server

httpd:          #Fedora / CentOS name of apache2 package
  pkg:
    - installed
  service.running:
    - enable: True
    - require:
      - pkg: httpd
      - pkg: mod_ssl
      - file: /etc/httpd/conf.d/MDCE_s3_reverse_proxy.conf
      
mod_ssl:
  pkg:
    - installed
    
/etc/httpd/conf.d/MDCE_s3_reverse_proxy.conf:
  file.managed:
    - source: salt://apache/MDCE_s3_reverse_proxy.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: httpd
    
