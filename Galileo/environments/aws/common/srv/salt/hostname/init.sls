#!yaml_jinja

# Set hostname

/etc/cloud/cloud.cfg.d/01_hostname.cfg:
  file.managed:
    - source: salt://hostname/01_hostname.cfg
    - user: root
    - group: root
    - mode: 664

/etc/hostname:
  file.managed:
    - contents_grains: id

hostnamectl set-hostname {{ grains.id }}:
  cmd.wait:
    - runas: root
    - watch:
      - file: /etc/cloud/cloud.cfg.d/01_hostname.cfg
    - require:
      - file: /etc/cloud/cloud.cfg.d/01_hostname.cfg
