#!yaml_jinja

# Set timezone to central

/etc/localtime:
  file.symlink:
    - target: /usr/share/zoneinfo/US/Central
