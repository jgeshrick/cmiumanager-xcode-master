#!yaml_jinja

# "Bind" DNS server ("named") 

bind:          #Fedora / CentOS name of bind9 named package
  pkg:
    - installed

bind-utils:
  pkg:
    - installed

/etc/named.conf:
  file.managed:
    - source: salt://bind/named.conf
    - user: root
    - group: named
    - mode: 640
    - template: jinja    
    - require:
      - pkg: bind

/var/named/mdce.zone:
  file.managed:
    - source: salt://bind/mdce.zone
    - user: root
    - group: named
    - mode: 640
    - template: jinja    
    - require:
      - pkg: bind

named:
  service.running:
    - enable: True
    - require:
      - pkg: bind
      - pkg: bind-utils
      - file: /etc/named.conf
      - file: /var/named/mdce.zone
    - watch:
      - file: /etc/named.conf
      - file: /var/named/mdce.zone
