#!yaml_jinja

# MDCE code deployment
#Apply database migrations to target environment

{% set ENV_NAME = grains.env %}
{% from "mdce-env-spec.map.jinja" import envs %}
{% from "galileo/"~ENV_NAME~"/mdce-app-versions.map.jinja" import mdceAppVersions %}

{% set RDS_NAME = "mdce-" + ENV_NAME + envs[ENV_NAME]["rdsEndpointNameSuffix"] %}
{% set RDS_MASTER_PASSWORD = envs[ENV_NAME]['rdsMasterUserPassword'] %}
{% set DISTRO_ROOT="s3://neptune-mdce-deployment" %}
{% set GALILEO_DATABASE_ZIP= DISTRO_ROOT ~ "/galileo/galileo-database/" ~ mdceAppVersions["galileo-database"] ~ "/galileo-database-" ~ mdceAppVersions["galileo-database"] ~".zip" %}

/opt/mdce/mdce-database:
  file.directory:
    - user: mdce
    - group: mdce
    - mode: 755
    - makedirs: False

/opt/mdce/mdce-database/galileo-database.zip:
  file.managed:
    - source: {{ GALILEO_DATABASE_ZIP }}
    - source_hash: {{ GALILEO_DATABASE_ZIP }}.sha1
    - archive_format: zip
    - user: mdce
    - group: mdce
    - require:
      - file: /opt/mdce/mdce-database
    - watch_in:
      - service: mdce-tomcat
    - prereq_in:
      - cmd: shutdown-mdce-tomcat   #shut down before deploy

mdce-execute-database-migration:
  cmd.run:
    - name: "rm *.jar; unzip -o galileo-database.zip; /usr/local/java/jdk/bin/java -cp /etc/mdce/conf.d:* com.neptunetg.galileo.database.migration.FlywayLauncher -u mdcerdsmaster -p{{ RDS_MASTER_PASSWORD }} -cmigrate"
    - cwd: /opt/mdce/mdce-database
    - runas: mdce
    - require:
      - pkg: unzip
      - file: /opt/mdce/mdce-database/galileo-database.zip
    - require_in:
      - service: mdce-tomcat
