#!yaml_jinja

# applies the DDL to the DB when it changes

{% from "aws-info.map.jinja" import awsInfo %}
{% from "mdce-env-spec.map.jinja" import vpcs %}
{% from "mdce-env-spec.map.jinja" import envs %}

{% set ENV_NAME = grains.env %}

{% set mnoVpcInfo = vpcs[envs[ENV_NAME]["mnoVpc"]] %}
{% set REGION = mnoVpcInfo["region"] %}

{% set rdsEndpoint = awsInfo[REGION]["rdsEndpoint"]["mdce-" + ENV_NAME + envs[ENV_NAME]["rdsEndpointNameSuffix"]] %}
{% set rdsMasterPassword = envs[ENV_NAME]["rdsMasterUserPassword"] %}
{% set apnSqlFile = "/etc/mdce/apns.sql" %}


{{ apnSqlFile }}:
  file.managed:
    - source: salt://galileo/apns.sql
    - user: mdce
    - group: mdce
    - mode: 644
    - makedirs: True

apn-sql:
  cmd.wait:
    - name: | 
        mysql -h {{ rdsEndpoint }} -u mdcerdsmaster -p{{ rdsMasterPassword }} mdce < {{ apnSqlFile }}
    - watch:
      - cmd: mdce-execute-database-migration
      - file: {{ apnSqlFile }}
    - require:
      - user: mdce-user
      - pkg: mariadb
