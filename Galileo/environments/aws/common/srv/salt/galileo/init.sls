#!yaml_jinja

# MDCE setup

{% set ENV_NAME = grains['env'] %}

mdce-group:
  group.present:
    - name: mdce

mdce-user:
  user.present:
    - name: mdce
    - groups: 
      - mdce
    - home: /home/mdce
    - shell: /bin/bash
    - require:
      - group: mdce-group

/var/share/currentserver/log/mdce: 
  file.directory:
    - user: mdce
    - group: mdce
    - mode: 777
    - makedirs: False
    - require:
      - file: /var/share/currentserver/log  #NFS

/var/share/htdocs/gateway:
  file.directory:
    - user: root
    - group: root
    - mode: 777
    - makedirs: False
    - require:
      - file: /var/share/htdocs  #NFS

/var/share/htdocs/gateway/received:
  file.directory:
    - user: root
    - group: root
    - mode: 777
    - makedirs: False
    - require:
      - file: /var/share/htdocs/gateway

/var/mdce:
  file.directory:
    - user: mdce
    - group: mdce
    - mode: 755
    - makedirs: False


/etc/mdce/conf.d/mdce-env.properties:
  file.managed:
    - source: salt://galileo/{{ ENV_NAME }}/mdce-env.properties
    - user: mdce
    - group: mdce
    - mode: 644
    - makedirs: True
    - template: jinja
    - require:
      - user: mdce-user
      - file: /var/mdce

/etc/mdce/conf.d/api/api_partner_key.jce:
  file.managed:
    - source: salt://galileo/api_partner_key.jce
    - user: mdce
    - group: mdce
    - mode: 644
    - makedirs: True
    - require:
      - user: mdce-user

/opt/mdce:
  file.directory:
    - user: mdce
    - group: mdce
    - mode: 755
    - makedirs: True
