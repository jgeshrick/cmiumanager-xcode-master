#!yaml_jinja

# MDCE code deployment
# Note: S3 credentials are in /srv/pillar/neptune-binary-distribution-manager.sls

{% set ENV_NAME = grains.env %}
{% from "galileo/"~ENV_NAME~"/mdce-app-versions.map.jinja" import mdceAppVersions %}
{% set DISTRO_ROOT="s3://neptune-mdce-deployment/mdce" %}

/opt/mdce/mdce-scheduler.jar:
  file.managed:
    - source: {{ DISTRO_ROOT }}/mdce-scheduler/{{ mdceAppVersions["mdce-scheduler"] }}/mdce-scheduler.jar
    - source_hash: {{ DISTRO_ROOT }}/mdce-scheduler/{{ mdceAppVersions["mdce-scheduler"] }}/mdce-scheduler.jar.sha1
    - user: mdce
    - group: mdce
    - mode: 644
    - makedirs: True
    - require:
      - user: mdce-user
      - file: /opt/mdce

/opt/tomcat/webapps/mdce-mqtt.war:
  file.managed:
    - source: {{ DISTRO_ROOT }}/mdce-mqtt/{{ mdceAppVersions["mdce-mqtt"] }}/mdce-mqtt.war
    - source_hash: {{ DISTRO_ROOT }}/mdce-mqtt/{{ mdceAppVersions["mdce-mqtt"] }}/mdce-mqtt.war.sha1
    - user: mdce
    - group: mdce
    - mode: 644
    - makedirs: True
    - require:
      - user: mdce-user
      - file: /opt/tomcat
    - watch_in:
      - service: mdce-tomcat
    - prereq_in:
      - cmd: shutdown-mdce-tomcat   #shut down before deploy

rm -rf /opt/tomcat/webapps/mdce-mqtt: #remove exploded WAR to force Tomcat to re-explode it
  cmd.wait:
    - watch:
      - file: /opt/tomcat/webapps/mdce-mqtt.war
    - require_in:
      - service: mdce-tomcat

/opt/tomcat/webapps-8081-8444/ROOT.war:
  file.managed:
    - source: {{ DISTRO_ROOT }}/mdce-integration/{{ mdceAppVersions["mdce-integration"] }}/mdce-integration.war
    - source_hash: {{ DISTRO_ROOT }}/mdce-integration/{{ mdceAppVersions["mdce-integration"] }}/mdce-integration.war.sha1
    - user: mdce
    - group: mdce
    - mode: 644
    - makedirs: True
    - require:
      - user: mdce-user
      - file: /opt/tomcat
    - watch_in:
      - service: mdce-tomcat
    - prereq_in:
      - cmd: shutdown-mdce-tomcat   #shut down before deploy


rm -rf /opt/tomcat/webapps/mdce-lora: #remove exploded WAR to force Tomcat to re-explode it
  cmd.wait:
    - watch:
      - file: /opt/tomcat/webapps-8083-8446/mdce-lora.war
    - require_in:
      - service: mdce-lora-tomcat

/opt/tomcat/webapps-8083-8446/mdce-lora.war:
  file.managed:
    - source: {{ DISTRO_ROOT }}/mdce-lora/{{ mdceAppVersions["mdce-lora"] }}/mdce-lora.war
    - source_hash: {{ DISTRO_ROOT }}/mdce-lora/{{ mdceAppVersions["mdce-lora"] }}/mdce-lora.war.sha1
    - user: mdce
    - group: mdce
    - mode: 644
    - makedirs: True
    - require:
      - user: mdce-user
      - file: /opt/tomcat
    - watch_in:
      - service: mdce-lora-tomcat
    - prereq_in:
      - cmd: shutdown-mdce-lora-tomcat   #shut down before deploy

rm -rf /opt/tomcat/webapps-8081-8444/mdce-integration: #remove exploded WAR to force Tomcat to re-explode it
  cmd.wait:
    - watch:
      - file: /opt/tomcat/webapps-8081-8444/ROOT.war
    - require_in:
      - service: mdce-tomcat

rm -f /opt/tomcat/webapps-8081-8444/mdce-integration.war: #only ROOT.war should be present
  cmd.wait:
    - watch:
      - file: /opt/tomcat/webapps-8081-8444/ROOT.war
    - require_in:
      - service: mdce-tomcat

rm -rf /opt/tomcat/webapps-8081-8444/ROOT: #remove exploded WAR to force Tomcat to re-explode it
  cmd.wait:
    - watch:
      - file: /opt/tomcat/webapps-8081-8444/ROOT.war
    - require_in:
      - service: mdce-tomcat

/opt/tomcat/webapps-80-443/mdce-web.war:
  file.managed:
    - source: {{ DISTRO_ROOT }}/mdce-web/{{ mdceAppVersions["mdce-web"] }}/mdce-web.war
    - source_hash: {{ DISTRO_ROOT }}/mdce-web/{{ mdceAppVersions["mdce-web"] }}/mdce-web.war.sha1
    - user: mdce
    - group: mdce
    - mode: 644
    - makedirs: True
    - require:
      - user: mdce-user
      - file: /opt/tomcat
    - watch_in:
      - service: mdce-tomcat
    - prereq_in:
      - cmd: shutdown-mdce-tomcat   #shut down before deploy

rm -rf /opt/tomcat/webapps-80-443/mdce-web: #remove exploded WAR to force Tomcat to re-explode it
  cmd.wait:
    - watch:
      - file: /opt/tomcat/webapps-80-443/mdce-web.war
    - require_in:
      - service: mdce-tomcat

/opt/tomcat/webapps-80-443/ROOT: #this needs to exist for redirects from outside /mdce-web to work
  file.directory:
    - user: mdce
    - group: mdce
    - mode: 777
    - makedirs: True
    - require:
      - user: mdce-user
      - file: /opt/tomcat

/opt/tomcat/webapps-9081-9444/mdce-web.war:
  file.managed:
    - source: {{ DISTRO_ROOT }}/mdce-web/{{ mdceAppVersions["mdce-web"] }}/mdce-web.war
    - source_hash: {{ DISTRO_ROOT }}/mdce-web/{{ mdceAppVersions["mdce-web"] }}/mdce-web.war.sha1
    - user: mdce
    - group: mdce
    - mode: 644
    - makedirs: True
    - require:
      - user: mdce-user
      - file: /opt/tomcat
    - watch_in:
      - service: mdce-tomcat
    - prereq_in:
      - cmd: shutdown-mdce-tomcat   #shut down before deploy

rm -rf /opt/tomcat/webapps-9081-9444/mdce-web: #remove exploded WAR to force Tomcat to re-explode it
  cmd.wait:
    - watch:
      - file: /opt/tomcat/webapps-9081-9444/mdce-web.war
    - require_in:
      - service: mdce-tomcat

/opt/tomcat/webapps-9081-9444/ROOT: #this needs to exist for redirects from outside /mdce-web to work
  file.directory:
    - user: mdce
    - group: mdce
    - mode: 777
    - makedirs: True
    - require:
      - user: mdce-user
      - file: /opt/tomcat

