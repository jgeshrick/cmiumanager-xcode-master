#!yaml_jinja

# MDCE Scheduler Service


/lib/systemd/system/mdce-scheduler.service:
  file.managed:
    - source: salt://mdce/systemd/mdce-scheduler.service
    - user: root
    - group: root
    - mode: 555

/etc/sysconfig/mdce-scheduler.environment:
  file.managed:
    - source: salt://mdce/systemd/mdce-scheduler.environment
    - user: root
    - group: root
    - mode: 555

mdce-scheduler:
  service.running:
    - enable: True
    - require:
      - file: /opt/mdce/mdce-scheduler.jar
      - file: /etc/mdce/conf.d/mdce-env.properties
      - file: /etc/profile.d/java.sh
      - file: /lib/systemd/system/mdce-scheduler.service
      - file: /etc/sysconfig/mdce-scheduler.environment
    - watch:
      - file: /opt/mdce/mdce-scheduler.jar
      - file: /etc/mdce/conf.d/mdce-env.properties
      