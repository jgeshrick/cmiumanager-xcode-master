#!yaml_jinja

#
# Copy over and run the MDCE monitoring service
#

/lib/systemd/system/mdce-monitoring.service:
  file.managed:
    - source: salt://monitoring/systemd/mdce-monitoring.service
    - user: root
    - group: root
    - mode: 555
    - makedirs: True

/home/centos/mdceMonitoring/mdceMonitor.py:
  file.managed:
    - source: salt://monitoring/mdceMonitor.py
    - user: root
    - group: root
    - mode: 555
    - makedirs: True

/home/centos/mdceMonitoring/EnvSpecificConstants.py:
  file.managed:
    - source: /srv/boto/config/EnvSpecificConstants.py #Note: this only works because the mdceMonitoring happens to run on the salt-master which also gets the /srv/boto/config/EnvSpecificConstants.py file pushed onto it
    - user: root
    - group: root
    - mode: 555
    - makedirs: True

mdce-monitoring:
  service.running:
    - enable: True 
    - require:
      - file: /lib/systemd/system/mdce-monitoring.service
    - watch:
      - file: /home/centos/mdceMonitoring/mdceMonitor.py
