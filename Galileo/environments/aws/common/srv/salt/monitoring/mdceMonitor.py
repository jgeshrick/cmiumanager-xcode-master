# This is the script for monitoring MDCE
import time
import datetime
import os
import re
import json
import urllib2
import boto3
from EnvSpecificConstants import envs
from EnvSpecificConstants import vpcs

CACHE_FILE = "/srv/boto/aws_info_cache.json"

awsInfoCache = {}

if os.path.exists(CACHE_FILE):
    print "Loading AWS cache from " + CACHE_FILE
    with open(CACHE_FILE, "r") as f:
        awsInfoCache = json.load(f)

appPattern = re.compile("app\d{2}")

ipAccessableApps = (("mdce-mqtt", ":8080/mdce-mqtt/debug/application/status"),
                    ("mdce-integration", ":8081/debug/application/status"),
                    ("fpc", ":8082/fpc/debug/application/status"),
                    ("mdce-lora", ":8083/mdce-lora/debug/application/status"))

previousResults = None


def sendCloudWatchMetric(metricName, appName, serverName, envName, region, success):
    """Sends a metric to AWS CloudWatch"""
    now = datetime.datetime.utcnow()

    client = boto3.client('cloudwatch', region_name=region)
    
    dimensions = [
        {
            'Name': 'Server',
            'Value': serverName
        },
        {
            'Name': 'Environment',
            'Value': envName
        },
    ]
    if appName is not None:
        dimensions.append({
            'Name': 'Application',
            'Value': appName
        })


    response = put_metric_data(
        client,
        'System/Server/Status',
        [
            {
                'MetricName': metricName,
                'Dimensions': dimensions,
                'Timestamp': now,
                'Value': 1 if success else 0,
            },
        ]
    )

    return response

def put_metric_data(client, namespace, metricData):
    return client.put_metric_data(Namespace=namespace, MetricData=metricData)
    # print (metricData)

def checkWebpage(url, env):
    """Checks to see if a webpage is accessable, and then adds the result along
       with the environment name to a resultsList"""
    try:
        page = urllib2.urlopen(url, timeout=1)
        return page.getcode() == 200
    except IOError:
        return False

def getServerList(env):
    ret = []
    instanceCount = envs[env]["template"]["appInstances"]
    for instanceNumber in range(1, instanceCount + 1):
        ret.append("aw-mdce-" + env + "-app" + str(instanceNumber).rjust(2, '0'))
        
    instanceCount = envs[env]["template"]["brokerInstances"]
    for instanceNumber in range(1, instanceCount + 1):
        ret.append("aw-mdce-" + env + "-brk" + str(instanceNumber).rjust(2, '0'))
    
    return ret

def getRegion(env):
    return vpcs[envs[env]['mdceVpc']]['region']

while True:
    # Get all the IP addresses for the servers
    
    serverList = [
        {
            'env': env,
            'serverName': serverName,
            'privateIpAddress': awsInfoCache[getRegion(env)]['privateIp'][serverName],
            'region': getRegion(env)
        }
        for env in envs
        for serverName in getServerList(env)
        if serverName in awsInfoCache[getRegion(env)]['privateIp']
    ]

    # Ping every IP address and record the result
    for server in serverList:
        pingScore = 0.0
        if os.system("ping -c 1 " + server['privateIpAddress']) == 0:
            pingScore = pingScore + 1.0
        sendCloudWatchMetric("Ping", None, server['serverName'], server['env'], server['region'], pingScore)

    # Get the app servers
    appExt = [s for s in serverList if appPattern.search(s['serverName'])]

    # TODO need to check that the scheduler app is up
    # Get the status page on all the IP accessable apps
    for server in appExt:
        for app in ipAccessableApps:
            success = False
            try:
                page = urllib2.urlopen("http://" + server['privateIpAddress'] + app[1])
                if page.getcode() == 200 and '"status":"up"' in page.read():
                    success = True
            except IOError:
                pass  # Not a problem, just means we can't reach the webpage
            sendCloudWatchMetric("Heartbeat", app[0], server['serverName'], server['env'], server['region'], success)

    # Check the external and whitelisted access to web
    for env in envs:
        for server in (s for s in appExt if env == s['env']):
            nonProdSection, env = (".mdce-nonprod", env) if env != "prod" else ("", "mdce")

            if server is not None:
                sendCloudWatchMetric("WhitelistedWebAccess", 'mdce-web', server['serverName'], server['env'], server['region'],
                                     checkWebpage("https://" + env + nonProdSection +
                                                  ".neptunensight.com:9444/mdce-web/pages/user/login", env))
                # external web access via port 443 is switched off (part of MSPD-2175 change) so don't check this
                #sendCloudWatchMetric("ExternalWebAccess", 'mdce-web', server['serverName'], server['env'],
                #                     checkWebpage("https://" + env + nonProdSection +
                #                                  ".neptunensight.com/mdce-web/pages/user/login", env))

    time.sleep(60)
