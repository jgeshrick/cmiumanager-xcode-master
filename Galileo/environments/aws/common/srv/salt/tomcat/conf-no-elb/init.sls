#!yaml_jinja

# Tomcat 8 configuration without ELB

/opt/tomcat/conf/catalina.properties:
  file.managed:
    - source: salt://tomcat/conf-no-elb/catalina.properties
    - makedirs: True
    - require:
      - file: /opt/tomcat
    - require_in:
      - service: mdce-tomcat
    - watch_in:
      - service: mdce-tomcat

/opt/tomcat/conf/mdce-tomcat/logging.properties:
  file.managed:
    - source: salt://tomcat/conf-no-elb/mdce-tomcat/logging.properties
    - makedirs: True
    - require:
      - file: /opt/tomcat
    - require_in:
      - service: mdce-tomcat
    - watch_in:
      - service: mdce-tomcat

/opt/tomcat/conf/mdce-tomcat/server.xml:
  file.managed:
    - source: salt://tomcat/conf-no-elb/mdce-tomcat/server.xml
    - makedirs: True
    - require:
      - file: /opt/tomcat
    - require_in:
      - service: mdce-tomcat
    - watch_in:
      - service: mdce-tomcat

/opt/tomcat/conf/mdce-lora-tomcat/logging.properties:
  file.managed:
    - source: salt://tomcat/conf-no-elb/mdce-lora-tomcat/logging.properties
    - makedirs: True
    - require:
      - file: /opt/tomcat
    - require_in:
      - service: mdce-lora-tomcat
    - watch_in:
      - service: mdce-lora-tomcat

/opt/tomcat/conf/mdce-lora-tomcat/server.xml:
  file.managed:
    - source: salt://tomcat/conf-no-elb/mdce-lora-tomcat/server.xml
    - makedirs: True
    - require:
      - file: /opt/tomcat
    - require_in:
      - service: mdce-lora-tomcat
    - watch_in:
      - service: mdce-lora-tomcat
      
/opt/tomcat/conf/Catalina-mdce-web-9081-9444/localhost/rewrite.config:
  file.managed:
    - source: salt://tomcat/conf-no-elb/rewrite.config
    - mode: 755
    - user: mdce
    - group: mdce
    - makedirs: True
    - require:
      - file: /opt/tomcat
    - require_in:
      - service: mdce-tomcat
    - watch_in:
      - service: mdce-tomcat
      
/opt/tomcat/conf/Catalina-mdce-web-80-443/localhost/rewrite.config:
  file.managed:
    - source: salt://tomcat/conf-no-elb/rewrite.config
    - mode: 755
    - user: mdce
    - group: mdce
    - makedirs: True
    - require:
      - file: /opt/tomcat
    - require_in:
      - service: mdce-tomcat
    - watch_in:
      - service: mdce-tomcat
      