#!yaml_jinja

# Tomcat 8

{% set TOMCAT_VERSION = '8.0.26' -%}
{% set TOMCAT_DOWNLOAD_URL = 'http://archive.apache.org/dist/tomcat/tomcat-8/v'~TOMCAT_VERSION~'/bin/apache-tomcat-'~TOMCAT_VERSION~'.tar.gz' -%}


wipe-tomcat-temp:
  cmd.wait:
    - name: rm -rf /opt/tomcat/temp/*
    - watch: 
      - cmd: shutdown-mdce-tomcat  #wipe the temp folder when shut down Tomcat.  Workaround for https://issues.apache.org/jira/browse/AXIS2-3919?focusedCommentId=13687774&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-13687774

shutdown-mdce-tomcat:  # add this a prereq_in to other states to ensure Tomcat is shut down before they are run
  cmd.run:
    - name: systemctl stop mdce-tomcat
    - require:
      - file: /lib/systemd/system/mdce-tomcat.service

shutdown-mdce-lora-tomcat:  # add this a prereq_in to other states to ensure Tomcat is shut down before they are run
  cmd.run:
    - name: systemctl stop mdce-lora-tomcat
    - require:
      - file: /lib/systemd/system/mdce-lora-tomcat.service

/var/share/currentserver/log:
  file.directory:
    - user: mdce
    - group: mdce
    - mode: 777
    - makedirs: False
    - require:
      - file: /var/share/currentserver  #NFS

/var/share/currentserver/log/mdce-tomcat:
  file.directory:
    - user: mdce
    - group: mdce
    - mode: 777
    - makedirs: False
    - require:
      - file: /var/share/currentserver/log

/var/share/currentserver/log/mdce-lora-tomcat:
  file.directory:
    - user: mdce
    - group: mdce
    - mode: 777
    - makedirs: False
    - require:
      - file: /var/share/currentserver/log

/opt/apache-tomcat-{{ TOMCAT_VERSION }}:  #TODO: once salt has Tomcat8 available, switch to using yum package
  archive.extracted:
    - name: /opt/
    - source: {{ TOMCAT_DOWNLOAD_URL }}
    - source_hash: {{ TOMCAT_DOWNLOAD_URL }}.sha1
    - archive_format: tar
    - options: zv
    - user: mdce
    - if_missing: /opt/apache-tomcat-{{ TOMCAT_VERSION }}
    - require:
      - file: /opt
      - user: mdce-user    
      
chown -R mdce:mdce /opt/apache-tomcat-{{ TOMCAT_VERSION }}:
  cmd.wait:
    - watch:
      - archive: /opt/apache-tomcat-{{ TOMCAT_VERSION }}

/opt/tomcat:
  file.symlink:
    - target: /opt/apache-tomcat-{{ TOMCAT_VERSION }}
    - require:
      - archive: /opt/apache-tomcat-{{ TOMCAT_VERSION }}

/opt/tomcat/bin/setenv.sh:
  file.managed:
    - source: salt://tomcat/setenv.sh
    - require:
      - file: /opt/tomcat

/lib/systemd/system/mdce-tomcat.service:
  file.managed:
    - source: salt://tomcat/systemd/mdce-tomcat.service
    - user: {{ grains.defaultUser }}
    - group: {{ grains.defaultUser }}
    - mode: 555

/lib/systemd/system/mdce-lora-tomcat.service:
  file.managed:
    - source: salt://tomcat/systemd/mdce-lora-tomcat.service
    - user: {{ grains.defaultUser }}
    - group: {{ grains.defaultUser }}
    - mode: 555

/etc/sysconfig/mdce-lora-tomcat.environment:
  file.managed:
    - source: salt://tomcat/systemd/mdce-lora-tomcat.environment
    - user: {{ grains.defaultUser }}
    - group: {{ grains.defaultUser }}
    - mode: 555

mdce-tomcat:
  service.running:
    - enable: True
    - require:
      - file: /var/share/currentserver/log/mdce
      - file: /var/share/currentserver/log/mdce-tomcat
      - file: /lib/systemd/system/mdce-tomcat.service
      - file: /etc/sysconfig/mdce-tomcat.environment
      - file: /etc/mdce/conf.d/mdce-env.properties
    - onlyif:
      - ls /opt/tomcat/bin/startup.sh
    - watch:
      - file: /lib/systemd/system/mdce-tomcat.service
      - file: /etc/sysconfig/mdce-tomcat.environment
      - file: /opt/tomcat
      - file: /etc/mdce/conf.d/mdce-env.properties

mdce-lora-tomcat:
  service.running:
    - enable: True
    - require:
      - file: /var/share/currentserver/log/mdce
      - file: /var/share/currentserver/log/mdce-lora-tomcat
      - file: /lib/systemd/system/mdce-lora-tomcat.service
      - file: /etc/sysconfig/mdce-lora-tomcat.environment
      - file: /etc/mdce/conf.d/mdce-env.properties
    - onlyif:
      - ls /opt/tomcat/bin/startup.sh
    - watch:
      - file: /lib/systemd/system/mdce-lora-tomcat.service
      - file: /etc/sysconfig/mdce-lora-tomcat.environment
      - file: /opt/tomcat
      - file: /etc/mdce/conf.d/mdce-env.properties
