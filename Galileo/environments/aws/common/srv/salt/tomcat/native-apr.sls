#!yaml_jinja

# Tomcat 8 Native APR 1.1.33

{% set TOMCAT_NATIVE_VERSION_MAJOR = '1' %}             #native version inside tomcat tarball bin/tomcat-native.tar.gz
{% set TOMCAT_NATIVE_VERSION_MINOR = '1.33' %}          #native version inside tomcat tarball bin/tomcat-native.tar.gz

{% set TOMCAT_NATIVE_VERSION = TOMCAT_NATIVE_VERSION_MAJOR + '.' + TOMCAT_NATIVE_VERSION_MINOR %} 
{% set TOMCAT_NATIVE_LIB = '/usr/local/apr/lib/libtcnative-' + TOMCAT_NATIVE_VERSION_MAJOR + '.so.0.' + TOMCAT_NATIVE_VERSION_MINOR %} 

apr-devel:
  pkg:
    - installed    

openssl-devel:
  pkg:
    - installed    

gcc:
  pkg:
    - installed    

salt://tomcat/install_tomcat_native_apr.sh:             #TODO: once CentOS has tomcat-native 1.1.33 available, switch to using yum package
  cmd.script:
    - env:
      - TOMCAT_NATIVE_VERSION: {{ TOMCAT_NATIVE_VERSION }}
    - require:
      - file: /opt/tomcat
      - user: mdce-user
      - pkg: apr-devel
      - pkg: openssl-devel
      - pkg: gcc
      - file: /etc/profile.d/java.sh
    - onlyif:
      - ls /usr/local/java/jdk/bin/java
    - unless:
      - ls {{ TOMCAT_NATIVE_LIB }}

/etc/profile.d/tomcat-native.sh:
  file.managed:
    - source: salt://tomcat/tomcat-native.sh
    - user: root
    - group: root
    - mode: 555
    - require_in:
      - pkg: mdce-tomcat
    - onlyif:
      - ls {{ TOMCAT_NATIVE_LIB }}
    - watch:
      - cmd: salt://tomcat/install_tomcat_native_apr.sh

port-80-prerouting:
  iptables.append:
    - table: nat
    - chain: PREROUTING
    - proto: tcp
    - dport: 80
    - jump: REDIRECT
    - to-port: 9080
    - require_in:
      - pkg: mdce-tomcat

port-443-prerouting:
  iptables.append:
    - table: nat
    - chain: PREROUTING
    - proto: tcp
    - dport: 443
    - jump: REDIRECT
    - to-port: 9443
    - require_in:
      - pkg: mdce-tomcat

port-80-output:
  iptables.append:
    - table: nat
    - chain: OUTPUT
    - proto: tcp
    - destination: 127.0.0.1
    - dport: 80
    - jump: REDIRECT
    - to-ports: 9080
    - require_in:
      - pkg: mdce-tomcat

port-443-output:
  iptables.append:
    - table: nat
    - chain: OUTPUT
    - proto: tcp
    - destination: 127.0.0.1
    - dport: 443
    - jump: REDIRECT
    - to-ports: 9443
    - require_in:
      - pkg: mdce-tomcat

