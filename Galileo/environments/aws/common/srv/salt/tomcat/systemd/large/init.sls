#!yaml_jinja

# Tomcat 8

/etc/sysconfig/mdce-tomcat.environment:
  file.managed:
    - source: salt://tomcat/systemd/large/mdce-tomcat.environment
    - user: {{ grains.defaultUser }}
    - group: {{ grains.defaultUser }}
    - mode: 555
