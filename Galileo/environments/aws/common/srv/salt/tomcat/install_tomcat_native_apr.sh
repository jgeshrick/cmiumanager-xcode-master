source /etc/profile.d/java.sh
cd /home/centos/
tar xzvf /opt/tomcat/bin/tomcat-native.tar.gz
cd tomcat-native-${TOMCAT_NATIVE_VERSION}-src/jni/native
./configure --with-apr=/usr/bin/apr-1-config --with-ssl=/usr/lib64/openssl
make
make install
