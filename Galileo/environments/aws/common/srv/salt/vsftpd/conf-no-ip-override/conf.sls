/etc/vsftpd/vsftpd.conf:
  file.managed:
    - source: salt://vsftpd/conf-no-ip-override/vsftpd.conf
    - user: root
    - group: root
    - mode: 570 
    - require:
      - pkg: vsftpd

