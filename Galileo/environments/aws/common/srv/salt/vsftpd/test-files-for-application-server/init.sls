#!yaml_jinja

# test files for FOTA

/var/share/ftp/deltafota_LE910VZN_V_17.01.571_to_V_17.01.571-T001.upd:
  file.managed:
    - source: salt://vsftpd/deltafota_LE910VZN_V_17.01.571_to_V_17.01.571-T001.upd
    - user: centos
    - group: centos
    - mode: 440
    - makedirs: true

/var/share/ftp/deltafota_LE910VZN_V_17.01.571-T001_to_V_17.01.571.upd:
  file.managed:
    - source: salt://vsftpd/deltafota_LE910VZN_V_17.01.571-T001_to_V_17.01.571.upd
    - user: centos
    - group: centos
    - mode: 440
    - makedirs: true

/var/share/ftp/deltafota_LE910NAA_V_17.01.500-B064_to_V_17.01.502.upd:
  file.managed:
    - source: salt://vsftpd/deltafota_LE910NAA_V_17.01.500-B064_to_V_17.01.502.upd
    - user: centos 
    - group: centos 
    - mode: 440
    - makedirs: true

/var/share/ftp/deltafota_LE910NAA_V_17.01.502_to_V_17.01.500-B064.upd:
  file.managed:
    - source: salt://vsftpd/deltafota_LE910NAA_V_17.01.502_to_V_17.01.500-B064.upd
    - user: centos 
    - group: centos
    - mode: 440
    - makedirs: true

/var/share/ftp:
  file.directory:
    - user: centos 
    - group: centos
    - mode: 775 
    - makedirs: true
