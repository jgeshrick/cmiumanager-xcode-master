#!yaml_jinja

# vsFTPd FTP server and test files

cmiuftp-group:
  group.present: 
    - name: cmiuftp

cmiuftp-user:
  user.present: 
    - name: cmiuftp
    - groups:
      - cmiuftp
    - password: $6$ZRXlDlUH$DTo4ANisIoLhKXtFLv6O.yxK4bOWl0Cy.svCyPJ4Kmkb.zUYVIaetga08my4yKquqORnYzQjQ0BREyEs35.iN/
    - shell: /bin/bash
    - home: /var/share/ftp
    - require:
      - group: cmiuftp-group
      - file: /var/share

# following folder should have already been created by cmiuftp-user state above.  Set the permissions.
/var/share/ftp:
  file.directory:
    - user: cmiuftp
    - group: cmiuftp
    - mode: 550
    - makedirs: False
    - require:
      - user: cmiuftp-user
      - file: /var/share

vsftpd:
  pkg:
    - installed
  service.running:
    - enable: True
    - require:
      - pkg: vsftpd
      - file: /etc/vsftpd/vsftpd.conf
      - file: /etc/vsftpd/user_list
      - file: /etc/vsftpd/chroot_list

/etc/vsftpd/user_list:
  file.managed:
    - source: salt://vsftpd/user_list
    - user: root
    - group: root
    - mode: 570
    - require:
      - pkg: vsftpd

/etc/vsftpd/chroot_list:
  file.managed:
    - source: salt://vsftpd/chroot_list
    - user: root
    - group: root
    - mode: 570
    - require:
      - pkg: vsftpd

/var/share/ftp/testfile.txt:
  file.managed:
    - source: salt://vsftpd/testfile.txt
    - user: cmiuftp
    - group: cmiuftp
    - mode: 440
    - makedirs: True
    - require:
      - user: cmiuftp-user
      - file: /var/share/ftp
    
/var/share/ftp/deltafota_LE910VZN_V_17.01.571_to_V_17.01.571-T001.upd:
  file.managed:
    - source: salt://vsftpd/deltafota_LE910VZN_V_17.01.571_to_V_17.01.571-T001.upd
    - user: cmiuftp 
    - group: cmiuftp 
    - mode: 440
    - makedirs: true
    - require:
      - user: cmiuftp-user
      - file: /var/share/ftp

/var/share/ftp/deltafota_LE910VZN_V_17.01.571-T001_to_V_17.01.571.upd:
  file.managed:
    - source: salt://vsftpd/deltafota_LE910VZN_V_17.01.571-T001_to_V_17.01.571.upd
    - user: cmiuftp 
    - group: cmiuftp
    - mode: 440
    - makedirs: true
    - require:
      - user: cmiuftp-user
      - file: /var/share/ftp

/var/share/ftp/deltafota_LE910NAA_V_17.01.500-B064_to_V_17.01.502.upd:
  file.managed:
    - source: salt://vsftpd/deltafota_LE910NAA_V_17.01.500-B064_to_V_17.01.502.upd
    - user: cmiuftp 
    - group: cmiuftp 
    - mode: 440
    - makedirs: true
    - require:
      - user: cmiuftp-user
      - file: /var/share/ftp

/var/share/ftp/deltafota_LE910NAA_V_17.01.502_to_V_17.01.500-B064.upd:
  file.managed:
    - source: salt://vsftpd/deltafota_LE910NAA_V_17.01.502_to_V_17.01.500-B064.upd
    - user: cmiuftp 
    - group: cmiuftp
    - mode: 440
    - makedirs: true
    - require:
      - user: cmiuftp-user
      - file: /var/share/ftp

ftp_home_dir:
  selinux.boolean:
    - value: True
    - persist: True
    - require:
      - user: cmiuftp-user
      - pkg: vsftpd
    - onlyif: #ftp_home_dir may be removed by an OS update: see https://serverfault.com/questions/825407/boolean-ftp-home-dir-is-not-defined-rhel7-3
      - sudo semanage boolean -l | grep '^ftp_home_dir'

ftpd_full_access:
  selinux.boolean:
    - value: True
    - persist: True
    - require:
      - user: cmiuftp-user
      - pkg: vsftpd
