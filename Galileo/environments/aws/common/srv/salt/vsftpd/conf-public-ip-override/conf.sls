/etc/vsftpd/vsftpd.conf:
  file.managed:
    - source: salt://vsftpd/conf-public-ip-override/vsftpd.conf
    - user: root
    - group: root
    - mode: 570 
    - template: jinja
    - require:
      - pkg: vsftpd

