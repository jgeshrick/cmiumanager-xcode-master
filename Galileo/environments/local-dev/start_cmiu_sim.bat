cd cmiu-simulator

IF NOT EXIST CmiuSimManager.py (
	@echo file CmiuSimManager.py does not exist
	@echo please extract the CMIU simulator MQTT client into folder cmiu-simulator and try again
	exit 100
)

@echo Python version (must be 32-bit to run the simulator)
python -V

@echo Starting simulator
@echo Publishing to localhost, at 0.25 min interval, starting CMIU ID 500000

rem Packet Types Available: intervalDataPacket, basicConfigPacket, commandResponsePacket, testPacket, justPacketHeader, detailedConfigPacket, justConfiguration


REM This command will send a basic Configuration packet for 1 CMIU every 15 seconds to a local broker
REM python CmiuSimManager.py localhost -s 500000 -n 1 -u 0.25 -p detailedConfigPacket -v --dumpfile hex.log

REM This command will send a detail Configuration packet for 1 CMIU every 15 seconds to a local broker
REM python CmiuSimManager.py localhost -s 500000 -n 1 -u 0.25 -p basicConfigPacket -v --dumpfile hex.log

REM This command will send Reading packets for 5 CMIU every 15 seconds to our test system broker
python CmiuSimManager.py 54.187.239.195 -s 500000 -n 3 -u 0.25 -p intervalDataPacket -v --dumpfile hex.log
