@echo starting mysql
net start MySQL56

@echo starting mosquitto
rem net start mosquitto
start cmd.exe /K "C:\Program Files (x86)\mosquitto\mosquitto.exe"

@echo starting dynamo
pushd dynamodb_local_2015-07-16_1.0
start java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb
popd

@echo Services have been started.  To stop services, run stop_services.bat and also close the DynamoDB-local window.
pause

