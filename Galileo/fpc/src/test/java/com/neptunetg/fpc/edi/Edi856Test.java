/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi;

import com.neptunetg.fpc.CommonUtil;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by sml1 on 03/02/2016.
 */
public class Edi856Test
{
    /**
     * Test generate of a EDI document for pairing of CMIU
     * @throws Exception
     */
    @Test
    public void testGenerate() throws Exception
    {
        final EdiAs2Constants ediAs2Constants = CommonUtil.getEdiAs2ConstantsForDevelopment();

        Edi856 edi856= new Edi856("NEPTUNE", "NEPTUNE", ediAs2Constants);
        String ediDoc = edi856
                .withPairingInformation(400000999L, "89148000001471859842", "353238060024824")
                .generate();

        assertNotNull(ediDoc);

        System.out.println(ediDoc);

    }
}