/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.as2;

import com.helger.as2lib.client.AS2ClientResponse;
import com.helger.as2lib.crypto.ECryptoAlgorithmSign;
import com.helger.as2lib.message.IMessageMDN;
import com.helger.as2lib.util.AS2Helper;
import com.helger.commons.base64.Base64;
import com.helger.commons.io.stream.NullOutputStream;
import com.helger.commons.mime.CMimeType;
import com.neptunetg.fpc.CommonUtil;
import com.neptunetg.fpc.edi.Edi856;
import com.neptunetg.fpc.edi.EdiAs2Constants;
import com.neptunetg.fpc.edi.EdiException;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeUtility;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

import static org.junit.Assert.*;

/**
 * Test connect to AS2 server and send a EDI document
 */
public class NeptuneAS2ClientTest
{
    private EdiAs2Constants ediAs2Constants;

    @Before
    public void createEdiConstants()
    {
        this.ediAs2Constants = CommonUtil.getEdiAs2ConstantsForDevelopment();
    }

    @Test
    @Ignore("Suppress sending of a hardcoded EDI file to Verizon production server as the ISA15 is <P>roduction")
    public void testSendEdiDocument() throws IOException, CertificateException
    {
        NeptuneAS2Client neptuneAs2Client = new NeptuneAS2Client(ediAs2Constants);
        String ediMessage = IOUtils.toString(getClass().getResourceAsStream("/verizon.x12"), "UTF-8");

        AS2ClientResponse response = neptuneAs2Client.sendMessage("NEPTUNE SUBJECT", ediMessage);

        debugPrint("Response", response.getAsString());

        assertFalse(response.hasException());   //this seems to be the way to check if the request has any problem

        assertNull(response.getException());
        assertNotNull(response.getMDN());
    }

    @Test
    @Ignore("Suppress sending of a hardcoded EDI file to Verizon production server as the ISA15 is <P>roduction")
    public void testConnectToVerizonAs2Server() throws CertificateException, FileNotFoundException
    {
        NeptuneAS2Client neptuneAs2Client = new NeptuneAS2Client(ediAs2Constants);
        AS2ClientResponse response = neptuneAs2Client.sendFile("NEPTUNE SUBJECT", "/verizon.x12");

        assertFalse(response.hasException());   //this seems to be the way to check if the request has any problem

        assertNull(response.getException());
        assertNotNull(response.getMDN());

        debugPrintResponse(response);
    }

    /**
     * Generate a dynamic EDI document and send to Verizon AS2 server.
     * @throws EdiException
     * @throws CertificateException
     * @throws FileNotFoundException
     */
    @Test
    @Ignore("VZW AS2 dev server certificate has expired, therefore throwing CertificateExpiredException")
    public void testGenerateEdi856AndSendToVZWTestServer() throws EdiException, CertificateException, FileNotFoundException
    {
        Edi856 edi856 = new Edi856("NEPTUNE", "NEPTUNE", ediAs2Constants);
        String ediDoc = edi856
                .withPairingInformation(400000999L, "89148000001471859842", "353238060024824")
                .generate();

        saveStringToFile("c:\\temp\\temp_edi.txt", ediDoc);

        NeptuneAS2Client neptuneAs2Client = new NeptuneAS2Client(ediAs2Constants);

        AS2ClientResponse response = neptuneAs2Client.sendMessage("Neptune Test Paring information", ediDoc);


        assertFalse(response.hasException());   //this seems to be the way to check if the request has any problem
        assertNull(response.getException());
        assertNotNull(response.getMDN());

        debugPrintResponse(response);
    }

    /**
     * Test pairing of real CMIU.
     * @throws EdiException
     * @throws CertificateException
     * @throws FileNotFoundException
     */
    @Test
    @Ignore("Only run this if we are pair real device. Disabled to prevent duplicate pairing of same iccid/imei")
    public void testPairing() throws EdiException, CertificateException, FileNotFoundException
    {
        this.ediAs2Constants = CommonUtil.getEdiAs2ConstantsForProduction(false);

        Edi856 edi856 = new Edi856("NEPTUNE", "NEPTUNE", ediAs2Constants);
        String ediDoc = edi856
                .withPairingInformation(400002570L, "89148000002036032271", "353238063156219")
                .generate();

        saveStringToFile("c:\\temp\\temp_edi_400002570.txt", ediDoc);

        NeptuneAS2Client neptuneAs2Client = new NeptuneAS2Client(ediAs2Constants);

        AS2ClientResponse response = neptuneAs2Client.sendMessage("Neptune Pairing information", ediDoc);

        assertFalse(response.hasException());   //this seems to be the way to check if the request has any problem
        assertNull(response.getException());
        assertNotNull(response.getMDN());

        debugPrintResponse(response);
    }

    private void saveStringToFile(final String fullFilePath, final String data) throws FileNotFoundException
    {
        System.out.println("EDI Document: " + fullFilePath + " =====================================\n");
        System.out.println();
        System.out.print(data);
        System.out.println();
        System.out.println("===================================================\n");

        PrintWriter out = new PrintWriter(fullFilePath);
        out.println(data);
        out.close();
    }

    @Test
    @Ignore("Suppress sending of a hardcoded EDI file to Verizon production server as the ISA15 is <P>roduction")
    public void testGenerateEdiFileAndSendToVerizon() throws EdiException, FileNotFoundException, CertificateException
    {
        /*Edi856 edi856= new Edi856("NEPTUNE", "NEPTUNE");
        String ediDoc = edi856
                .withPairingInformation(400000999L, "89148000001471859842", "353238060024824")
                .generate();

        PrintWriter out = new PrintWriter("c:/temp/temp_edi.txt");
        out.println(ediDoc);
        out.close();*/

        NeptuneAS2Client neptuneAs2Client = new NeptuneAS2Client(ediAs2Constants);
        AS2ClientResponse response = neptuneAs2Client.sendFile("NEPTUNE GENERATED EDI", "/generated_edi.txt");

        assertFalse(response.hasException());   //this seems to be the way to check if the request has any problem

        assertNull(response.getException());
        assertNotNull(response.getMDN());

        debugPrintResponse(response);
    }

    /**
     * Send UNIX format (LF, no CR) file for checking of signature
     */
    @Test
    @Ignore("Verizon AS2 returns integrity-check-failed error while verify the signature")
    public void testEdiFileUnix() throws CertificateException, FileNotFoundException
    {
        NeptuneAS2Client neptuneAs2Client = new NeptuneAS2Client(ediAs2Constants);
        AS2ClientResponse response = neptuneAs2Client.sendFile("NEPTUNE GENERATED EDI", "/generated_edi_unix.txt");

        assertFalse(response.hasException());   //this seems to be the way to check if the request has any problem

        assertNull(response.getException());
        assertNotNull(response.getMDN());

        debugPrintResponse(response);
    }

    @Test
    public void testMessageDigest() throws Exception
    {
        byte[] data1 = IOUtils.toByteArray(getClass().getResourceAsStream("/generated_edi_unix.txt"));
        byte[] data2 = IOUtils.toByteArray(getClass().getResourceAsStream("/generated_edi.txt"));

        final MimeBodyPart mimeBodyPart1 = new MimeBodyPart();
        mimeBodyPart1.setContent(data1, CMimeType.APPLICATION_XML.getAsString());
        final String mic1 = AS2Helper.getCryptoHelper().calculateMIC(mimeBodyPart1, ECryptoAlgorithmSign.DIGEST_SHA1, false);

        final MimeBodyPart mimeBodyPart2 = new MimeBodyPart();
        mimeBodyPart2.setContent(data2, CMimeType.APPLICATION_XML.getAsString());
        final String mic2 = AS2Helper.getCryptoHelper().calculateMIC(mimeBodyPart2, ECryptoAlgorithmSign.DIGEST_SHA1, false);

        final String mic11 = calculateMIC(mimeBodyPart1, ECryptoAlgorithmSign.DIGEST_SHA1);
        final String mic12 = calculateMIC(mimeBodyPart2, ECryptoAlgorithmSign.DIGEST_SHA1);

        final String mic31 = calculateMIC2(data1);
        final String mic32 = calculateMIC2(data2);

        assertEquals(mic1, mic11);
        assertEquals(mic2, mic12);

        assertNotEquals(mic1, mic2);
        assertNotEquals(mic11, mic12);
        assertNotEquals(mic31, mic32);

    }

    public String calculateMIC2(byte[] data) throws NoSuchAlgorithmException
    {
        final MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");

        messageDigest.update(data);

        // Build result digest array
        final byte[] mic = messageDigest.digest();

        // Perform Base64 encoding and append algorithm ID
        final String ret = Base64.encodeBytes(mic) + ", " + messageDigest.getAlgorithm();

        return ret;
    }

    private String calculateMIC(final MimeBodyPart aPart, final ECryptoAlgorithmSign eDigestAlgorithm) throws MessagingException, IOException, NoSuchProviderException, NoSuchAlgorithmException
    {
        final ASN1ObjectIdentifier aMICAlg = eDigestAlgorithm.getOID();
        final MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");

        // No need to canonicalize here - see issue #12
        final DigestOutputStream aDOS = new DigestOutputStream(new NullOutputStream(), messageDigest);
        final OutputStream aOS = MimeUtility.encode(aDOS, aPart.getEncoding());
        aPart.getDataHandler().writeTo(aOS);
        aOS.close();
        aDOS.close();


        // Build result digest array
        final byte[] aMIC = messageDigest.digest();

        // Perform Base64 encoding and append algorithm ID
        final String ret = Base64.encodeBytes(aMIC) + ", " + eDigestAlgorithm.getID();

        return ret;
    }

    private static void debugPrintResponse(AS2ClientResponse response)
    {
        System.out.println("AS2 RESPONSE OUTPUT ============================== ");
        debugPrint("Original message id", response.getOriginalMessageID());
        debugPrint("MDN Message Id", response.getMDNMessageID());
        debugPrint("MDN Disposition", response.getMDNDisposition());
        debugPrint("MDN Text", response.getMDNText());

        final IMessageMDN mdn = response.getMDN();
        debugPrint("MDN as string", mdn.getAsString());

        debugPrint("Response", response.getAsString());

        System.out.println("<<< ============================== ");
    }


    private static void debugPrint(final String key, final String value)
    {
        System.out.println(String.format("[%s]:", key));
        System.out.println("\t" + value);
        System.out.println("\n");
    }

}