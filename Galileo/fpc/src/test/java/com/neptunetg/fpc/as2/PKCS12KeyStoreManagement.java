/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.as2;

import com.helger.as2lib.cert.PKCS12CertificateFactory;
import com.helger.as2lib.exception.OpenAS2Exception;
import com.helger.as2lib.session.AS2Session;
import com.helger.as2lib.util.StringMap;
import com.helger.commons.io.resource.ClassPathResource;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

/**
 * Function to try managing PKCS12 keystore
 */
public class PKCS12KeyStoreManagement
{
    private static final String PKCS12_CERTSTORE_PATH = "/keystore/certificates.p12";
    private static final String PKCS12_CERTSTORE_PASSWORD = "test";

    /**
     * Just a utility function to load a keystore in the resource file and list the public/private keys. This can be use
     * to check whether the public/private cert/keys which are required for the AS2 transaction are present in the keystore.
     * @throws OpenAS2Exception
     * @throws IOException
     */
    @Test
    public void test() throws OpenAS2Exception, IOException
    {
        AS2Session session = new AS2Session();
        PKCS12CertificateFactory certificateFactory = initCertificateFactory(session);

        Map<String, Certificate> certMap = certificateFactory.getCertificates();

        assertNotNull(certMap);

        System.out.println("Listing " + certMap.size() + " certificates by alias");
        certMap.forEach((s, certificate) -> System.out.println( "\t" + s));

        certMap.forEach((alias, certificate) -> {
            try
            {
                System.out.println("For alias " + alias + " ====================================================");

                X509Certificate x509Certificate = certificateFactory.getCertificate(alias);

                PrivateKey privateKey = certificateFactory.getPrivateKey(x509Certificate);
                System.out.println("\tPrivate key: present, format: " + privateKey.getFormat());
            }
            catch (OpenAS2Exception e)
            {
                System.out.println("\tError retrieving private key");
            }
        });
    }

    public static PKCS12CertificateFactory initCertificateFactory(final AS2Session as2Session) throws OpenAS2Exception, IOException
    {

        // Dynamically add certificate factory
        final StringMap params = new StringMap();

        File keystore = ClassPathResource.getAsFile(PKCS12_CERTSTORE_PATH);

        if (keystore != null)
        {
            params.setAttribute(PKCS12CertificateFactory.ATTR_FILENAME, keystore.getAbsolutePath());
        }
        else
        {
            throw new IOException("Cannot load keystore file " + PKCS12_CERTSTORE_PATH);
        }

        params.setAttribute(PKCS12CertificateFactory.ATTR_PASSWORD, PKCS12_CERTSTORE_PASSWORD);
        params.setAttribute(PKCS12CertificateFactory.ATTR_SAVE_CHANGES_TO_FILE, false);

        final PKCS12CertificateFactory certificateFactory = new PKCS12CertificateFactory();
        certificateFactory.initDynamicComponent(as2Session, params);

        //we can dynamically add other certificates here by calling aCertFactory.addCertificate

        as2Session.setCertificateFactory(certificateFactory);

        return certificateFactory;
    }

}
