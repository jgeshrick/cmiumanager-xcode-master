/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import com.neptunetg.fpc.edi.EdiException;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 *
 */
public class ElementTest
{
    @Test
    public void testCreateStringElement() throws EdiException
    {
        Element<String> element = new Element<String>("BSN01", 2, 2);
        element.setValue("00");
        assertEquals("00", element.toString());
    }

    @Test
    public void testCreateNumberElement() throws EdiException
    {
        Element<Integer> element = new Element<Integer>("BSN01", 2, 2, 0);
        assertEquals("00", element.toString());
    }

    @Test
    public void testCreateNumberElementLong() throws EdiException
    {
        Element<Integer> element = new Element<Integer>("BSN01", 2, 6, 123456);
        assertEquals("123456", element.toString());
    }

    @Test(expected = EdiException.class)
    public void testCreateNumberElementThrowsException() throws EdiException
    {
        Element<Integer> element = new Element<Integer>("BSN01", 2, 2, 123456);
        assertEquals("123456", element.toString());
    }

    public void testCreatePaddedNumberElement() throws EdiException
    {
        Element<Integer> element = new Element<Integer>("BSN01", 4, 6, 12);
        assertEquals("0012", element.toString());
    }
}