/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import com.neptunetg.fpc.edi.EdiException;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by sml1 on 18/01/2016.
 */
public class SegmentTest
{
    @Test
    public void createTransactionSetHeader() throws EdiException
    {
        Segment transactionSetHeader = new Segment("ST")
                .appendElement(new Element<Integer>("ST01", 3, 3, 856))
                .appendElement(new Element<Integer>("ST02", 4, 9, 12))
                ;

        assertEquals("ST*856*0012~", transactionSetHeader.toString());
    }

    @Test
    public void createTransactionSetTrailer() throws EdiException
    {
        Segment transactionSetHeader = new Segment("SE")
                .appendElement(new Element<Integer>("SE01", 1, 10, 24))
                .appendElement(new Element<String>("SE02", 4, 9, "0234"))   //deliberately injecting a string instead of a number
                ;

        assertEquals("SE*24*0234~", transactionSetHeader.toString());
    }
}