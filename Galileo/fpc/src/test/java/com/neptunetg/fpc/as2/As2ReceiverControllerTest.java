/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.as2;

import com.helger.as2lib.cert.ICertificateFactory;
import com.helger.as2lib.session.AS2Session;
import junit.framework.TestCase;

/**
 * Created by sml1 on 02/02/2016.
 */
public class As2ReceiverControllerTest extends TestCase
{

    public void testInitCertificateFactory() throws Exception
    {
        final AS2Session as2Session = new AS2Session();

        As2ReceiverController.initCertificateFactory(as2Session);

        ICertificateFactory certFactory = as2Session.getCertificateFactory();

        assertNotNull(certFactory);
    }


}