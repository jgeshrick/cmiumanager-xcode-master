/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.model;

import org.junit.Test;

import static com.helger.commons.mock.CommonsAssert.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test parsing of EDI 997 X12 docs
 */
public class FunctionalAcknowledgmentTest
{

    @Test
    public void testParsingPositiveAck() throws Exception
    {
        final String edi997 = "ISA*00*          *00*          *ZZ*RECEIVER       *ZZ*NEPTUNE        *20160204*0913*U*00401*11*0*P*>~\n" +
                "GS*SH*RECEIVER*NEPTUNE*20160204*0913*1*X*004010~\n" +
                "ST*997*1~\n" +
                "AK1*SH*123~\n" +
                "AK2*856*0001*~\n" +
                "AK5*A~\n" +
                "AK9*A*1*1*0~\n" +
                "SE*6*0001~\n" +
                "GE*1*123~\n" +
                "IEA*1*11~";

        FunctionalAcknowledgment functionalAck = FunctionalAcknowledgment.fromString(edi997);


        assertTrue(functionalAck.isAccepted());
        assertFalse(functionalAck.isHasErrors());
        assertEquals(10, functionalAck.getSegmentsMap().size());
    }

    @Test
    public void testParsingAckWithError() throws Exception
    {
        final String edi997 = "ISA*00*          *00*          *RR*RECEIVER       *ZZ*NEPTUNE        *20160204*0906*U*00401*11*0*P*>~\n" +
                "GS*SH*RECEIVER*NEPTUNE*20160204*0906*1*X*004010~\n" +
                "ST*997*1~\n" +
                "AK1*SH*123~\n" +
                "AK2*856*0001*~\n" +
                "AK3*LIN*16~\n" +
                "AK5*R*5~\n" +
                "AK9*R*1*1*1~\n" +
                "SE*7*0001~\n" +
                "GE*1*123~\n" +
                "IEA*1*11~";

        FunctionalAcknowledgment functionalAck = FunctionalAcknowledgment.fromString(edi997);


        assertFalse(functionalAck.isAccepted());
        assertTrue(functionalAck.isHasErrors());
        assertEquals(11, functionalAck.getSegmentsMap().size());
    }

}