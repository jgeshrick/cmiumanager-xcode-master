/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.junit.Assert.assertEquals;

/**
 * Created by sml1 on 18/01/2016.
 */
public class OrderLevelTest
{

    @Test
    public void testGetData() throws Exception
    {
        OrderLevel orderLevel = new OrderLevel("PO12345");
        //orderLevel.setPrf01PurchaseOrder("PO12345");
        orderLevel.setPrf04PODate(LocalDateTime.of(2009, 12, 30, 0, 0).toInstant(ZoneOffset.UTC));

        orderLevel.setVendorOrderNumber(11111);
        orderLevel.setDepartmentNumber(22222);
        orderLevel.setInternalVendorNumber(33333);
        orderLevel.setCustomerReferenceNumber(44444);

        final String edi = orderLevel.getData();
        System.out.println(edi);

        final String expected = "HL*2*1*O~\r\n" +
                "PRF*PO12345***20091230~\r\n" +
                "REF*VN*11111~\r\n" +
                "REF*DP*22222~\r\n" +
                "REF*IA*33333~\r\n" +
                "REF*CR*44444~";
        assertEquals(expected, edi);

    }
}