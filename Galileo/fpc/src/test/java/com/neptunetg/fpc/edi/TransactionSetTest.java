/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi;

import com.neptunetg.fpc.edi.segment.TransactionSet;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by sml1 on 18/01/2016.
 */
public class TransactionSetTest
{

    @Test
    public void testTransactionSet() throws Exception
    {
        TransactionSet transactionSet = new TransactionSet(234);

        transactionSet.setSe01NumberOfSegments(24);

        System.out.println(transactionSet.getHeader());
        System.out.println(transactionSet.getTrailer());

        assertEquals("ST*856*0234~", transactionSet.getHeader());
        assertEquals("CTT*5~\r\nSE*24*0234~", transactionSet.getTrailer());

    }

    @Test
    public void testTransactionSetMaxControlNumber() throws Exception
    {
        TransactionSet transactionSet = new TransactionSet(999999999);

        transactionSet.setSe01NumberOfSegments(123456789);

        System.out.println(transactionSet.getHeader());
        System.out.println(transactionSet.getTrailer());

        assertEquals("ST*856*999999999~", transactionSet.getHeader());
        assertEquals("CTT*5~\r\nSE*123456789*999999999~", transactionSet.getTrailer());
    }

}