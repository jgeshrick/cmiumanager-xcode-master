/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi;

import com.neptunetg.fpc.edi.segment.InterchangeControl;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.junit.Assert.assertEquals;

/**
 * Create ISA and IEA tag
 */
public class InterchangeControlTest
{
    private static final String ISA = "ISA*00*          *00*          *SS*SENDER         *RR*RECEIVER       *091231*1259*U*00401*000000123*0*P*>~";


    @Test
    public void testGetInterchangeControlHeader() throws Exception
    {
        InterchangeControl ic = new InterchangeControl();
        ic.setIsa05SenderIdQualifier("SS");
        ic.setIsa06SenderId("SENDER");
        ic.setIsa07ReceiverIdQualifier("RR");
        ic.setIsa08ReceiverId("RECEIVER");
        ic.setIsa13ControlNumber(123);
        ic.setIsa15UsageIndicator("P");
        ic.setIsaDateTimeOfExchange(LocalDateTime.of(2009, 12, 31, 12, 59).toInstant(ZoneOffset.UTC));

        System.out.println(ic.getHeader());
        assertEquals(ISA, ic.getHeader());

        System.out.println(ic.getTrailer());
        assertEquals("IEA*1*" + "000000123" +"~", ic.getTrailer());
    }

}