/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import org.junit.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;

/**
 * Created by sml1 on 18/01/2016.
 */
public class BeginningSegmentTest
{
    final static public DateTimeFormatter dateFormatterYYYYMMDD = DateTimeFormatter.ofPattern("yyyyMMdd").withZone(ZoneId.of("UTC"));
    final static public DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HHmm").withZone(ZoneId.of("UTC"));

    @Test
    public void testGetData() throws Exception
    {
        BeginningSegment segment = new BeginningSegment();
        segment.setDtm02ShippedDate(LocalDateTime.of(2009, 12, 31, 12, 59).toInstant(ZoneOffset.UTC));

        final String output = segment.getData();

        System.out.println(output);

        Instant transactionDateTime = Instant.now();

        final String expectedOutput = "BSN*00*1234567890*" + dateFormatterYYYYMMDD.format(transactionDateTime)  + "*"
                + timeFormatter.format(transactionDateTime)
                + "*0002~\r\n" +
                "DTM*011*20091231~\r\n" +
                "DTM*068*20091231~";

        assertEquals(expectedOutput, output);



    }
}