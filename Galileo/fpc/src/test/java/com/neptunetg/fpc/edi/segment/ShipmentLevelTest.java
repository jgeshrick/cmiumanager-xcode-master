/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import com.neptunetg.fpc.edi.EdiException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by sml1 on 18/01/2016.
 */
public class ShipmentLevelTest
{
    @Test
    public void testShipmentLevel() throws EdiException
    {
        ShipmentLevel shipment = new ShipmentLevel();

        final String edi = shipment.getData();
        System.out.println(edi);

        final String expected = "HL*1**S~\r\n" + "PO4*****G*10*LB~";
        assertEquals(expected, edi);
    }

}