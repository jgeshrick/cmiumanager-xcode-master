/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.rest;

import com.neptunetg.mdce.common.api.data.TokenInfo;
import com.neptunetg.mdce.common.api.domain.RestClientId;
import com.neptunetg.mdce.common.api.domain.auth.TokenManagerService;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GenerateTokenControllerTest
{
    @InjectMocks
    private GenerateTokenController tokenController;

    @Mock
    private TokenManagerService tokenManagerService;

    @Mock
    private HttpServletRequest request;

    @Before
    public void InitializeTest()
    {
        when(request.getHeader(anyString())).thenReturn(null);
        when(request.getRemoteAddr()).thenReturn("localhost");
    }

    @Test
    public void getToken_WhenSuppliedStringIsNotHex_DoesNotCallTokenManagerService() throws NotAuthorizedException, BadRequestException
    {
        // Test that the TokenManagerService isn't called if the supplied token is invalid.
        final String badParterString = "eb750ee73b5cf9dX4ec78a0b23616e9b";
        TokenInfo badToken = new TokenInfo(new byte[0], new RestClientId(0, 0), Instant.now(), "", Instant.now(), "");
        when(tokenManagerService.generateToken(anyString(), anyInt(), anyString())).thenReturn(badToken);

        try
        {
            tokenController.getToken(badParterString, request);
        }
        catch (Exception e)
        {
        }

        verify(tokenManagerService, never()).generateToken(anyString(), anyInt(), anyString());
    }

    @Test(expected = BadRequestException.class)
    public void getToken_WhenSuppliedStringIsNotHex_ThrowsBadRequest() throws NotAuthorizedException, BadRequestException
    {
        // Verify that a BadRequestException is thrown if the partner string is invalid.
        final String badParterString = "eb750ee73b5cf9dX4ec78a0b23616e9b";
        TokenInfo badToken = new TokenInfo(new byte[0], new RestClientId(0, 0), Instant.now(), "", Instant.now(), "");
        when(tokenManagerService.generateToken(anyString(), anyInt(), anyString())).thenReturn(badToken);

        tokenController.getToken(badParterString, request);
    }
}
