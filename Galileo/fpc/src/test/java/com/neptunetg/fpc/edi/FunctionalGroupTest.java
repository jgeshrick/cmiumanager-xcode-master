/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi;

import com.neptunetg.fpc.edi.segment.FunctionalGroup;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.junit.Assert.assertEquals;

/**
 * Create GS and GE tag
 */
public class FunctionalGroupTest
{

    @Test
    public void testFunctionGroup() throws Exception
    {
        FunctionalGroup groupHeader = new FunctionalGroup("SENDER", "RECEIVER");
        groupHeader.setDataInterchangeDateTime(LocalDateTime.of(2009, 12, 31, 12, 59).toInstant(ZoneOffset.UTC));
        groupHeader.setGs06DataInterchangeControlNumber(123);

        final String functionGroupHeader = groupHeader.getHeader();
        System.out.println(functionGroupHeader);
        assertEquals("GS*SH*SENDER*RECEIVER*20091231*1259*123*X*004010~", functionGroupHeader);

        assertEquals("GE*1*123~", groupHeader.getTrailer());


    }
}