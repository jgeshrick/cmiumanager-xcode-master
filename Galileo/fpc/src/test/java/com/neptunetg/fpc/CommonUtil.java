/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc;

import com.neptunetg.fpc.edi.EdiAs2Constants;

import java.util.Properties;

/**
 * Created by sml1 on 25/02/2016.
 */
public class CommonUtil
{
    /**
     * Create properties for Verizon-AS2
     * @return partially created mdce-env.properties
     */
    private static Properties getEdiPropertiesForDevelopment()
    {
        final Properties envProperties = new Properties();

        envProperties.setProperty("verizon.as2.neptune.sender.id", "NEPTUNE");
        envProperties.setProperty("verizon.as2.test.mode", "true");

        //Verizon AS2 development server
        envProperties.setProperty("verizon.as2.server.url", "http://vzwas2edidev.verizonwireless.com:9080/ipnet/as2");
        envProperties.setProperty("verizon.as2.key.alias", "verizon-wireless-as2-dev");
        envProperties.setProperty("neptune.as2.key.alias", "mdce-dev0");

        return envProperties;
    }

    private static Properties getEdiPropertiesForProduction(boolean testMode)
    {
        final Properties envProperties = new Properties();

        envProperties.setProperty("verizon.as2.neptune.sender.id", "NEPTUNE");
        envProperties.setProperty("verizon.as2.test.mode", testMode?"true":"false");

        //Verizon AS2 production server
        envProperties.setProperty("verizon.as2.server.url", "http://vzwas2edi.verizonwireless.com:9080/ipnet/as2");
        envProperties.setProperty("verizon.as2.key.alias", "verizon-wireless-as2-prod");
        envProperties.setProperty("neptune.as2.key.alias", "neptune-as2-prod");

        return envProperties;
    }

    /**
     * Create a fake edi constant object for testing purpose
     * @return edi constant
     */
    public static EdiAs2Constants getEdiAs2ConstantsForDevelopment()
    {
        return new EdiAs2Constants(getEdiPropertiesForDevelopment());
    }

    public static EdiAs2Constants getEdiAs2ConstantsForProduction(boolean testMode)
    {
        return new EdiAs2Constants(getEdiPropertiesForProduction(testMode));
    }
}
