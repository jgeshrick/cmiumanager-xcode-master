/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.data.audit;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.mdce.common.internal.audit.AuditEventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Handle audit logs for JDBC
 */
@Repository
public class JdbcAuditRepository implements AuditRepository
{
    private static final Logger log = LoggerFactory.getLogger(JdbcAuditRepository.class);
    private final int MAX_STRING_LENGTH = 1024; //this is the size of the varchar(1024) in audit_list.old_values and audit_list.new_values
    private final JdbcTemplate db;

    @Autowired
    public JdbcAuditRepository(JdbcTemplate db)
    {
        this.db = db;
    }

    @Override
    public void logCmiuCanChange(MiuId miuId, CellularDeviceDetail cellularDeviceDetail)
    {
        String oldValue = "";

        String newValue = "CMIU CAN settings changed: ";

        if(cellularDeviceDetail.getApn() != null)
        {
            newValue += "APN= " + cellularDeviceDetail.getApn() + "; ";
        }

        if(cellularDeviceDetail.getIccid() != null)
        {
            newValue += "ICCID= " + cellularDeviceDetail.getIccid() + "; ";
        }

        if(cellularDeviceDetail.getModemConfig().getMno() != null)
        {
            newValue += "Mno= " + cellularDeviceDetail.getModemConfig().getMno() + "; ";
        }

        if(cellularDeviceDetail.getModemConfig().getVendor() != null)
        {
            newValue += "Vendor= " + cellularDeviceDetail.getModemConfig().getVendor() + "; ";
        }

        if(cellularDeviceDetail.getModemConfig().getImei() != null)
        {
            newValue += "IMEI= " + cellularDeviceDetail.getModemConfig().getImei() + "; ";
        }

        addAuditEntry(AuditEventType.AUDIT_EVENT_TYPE_FPC_ACTIVATE_DEVICE, miuId.numericValue(), oldValue, newValue);
    }

    private static String truncate(String value, int length)
    {
        if (value == null)
        {
            return null;
        }
        else if (value.length() > length)
        {
            return value.substring(0, length);
        }
        else
        {
            return value;
        }
    }

    private void addAuditEntry(AuditEventType eventType, Integer miuId, String oldValue, String newValue)
    {
        final String insertAuditSql = "INSERT INTO mdce.audit_list " +
                "(audit_time, audit_type, miu_id, old_values, new_values) VALUES (NOW(),?,?,?,?)";

        this.db.update(insertAuditSql, eventType.getId(), miuId,
                truncate(oldValue, MAX_STRING_LENGTH), truncate(newValue, MAX_STRING_LENGTH));
    }
}
