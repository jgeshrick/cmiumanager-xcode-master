/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.data.miu.manufacturinginfo;

import com.neptunetg.common.data.miu.MiuId;

import java.time.Instant;
import java.util.ArrayList;

/**
 * Created by WJD1 on 04/04/2016.
 * Class to hold manufacturing information for a MIU
 */
public class MiuManufactureInfo
{
    private MiuId miuId;
    private int miuManufactureInfoId;
    private ArrayList<MiuManufactureInfoTest> miuManufactureInfoTests;
    private int reworkCount;
    private Instant deviceCreationTimestamp;
    private Instant deviceFirstTestTimestamp;
    private Instant deviceLastTestTimestamp;
    private Instant deviceRegistrationTimestamp;
    private Instant deviceActivationTimestamp;
    private String deviceImageModemLibrary;
    private String deviceImageCmiuApplication;
    private String deviceImageCmiuBootloader;
    private String deviceImageCmiuConfiguration;
    private String deviceModemFirmwareRevision;
    private String deviceBluetoothMacAddress;

    public MiuId getMiuId()
    {
        return miuId;
    }

    public void setMiuId(MiuId miuId)
    {
        this.miuId = miuId;
    }

    public int getMiuManufactureInfoId()
    {
        return miuManufactureInfoId;
    }

    public void setMiuManufactureInfoId(int miuManufactureInfoId)
    {
        this.miuManufactureInfoId = miuManufactureInfoId;
    }

    public ArrayList<MiuManufactureInfoTest> getMiuManufactureInfoTests()
    {
        return miuManufactureInfoTests;
    }

    public void setMiuManufactureInfoTests(ArrayList<MiuManufactureInfoTest> miuManufactureInfoTests)
    {
        this.miuManufactureInfoTests = miuManufactureInfoTests;
    }

    public int getReworkCount()
    {
        return reworkCount;
    }

    public void setReworkCount(int reworkCount)
    {
        this.reworkCount = reworkCount;
    }

    public Instant getDeviceCreationTimestamp()
    {
        return deviceCreationTimestamp;
    }

    public void setDeviceCreationTimestamp(Instant deviceCreationTimestamp)
    {
        this.deviceCreationTimestamp = deviceCreationTimestamp;
    }

    public Instant getDeviceFirstTestTimestamp()
    {
        return deviceFirstTestTimestamp;
    }

    public void setDeviceFirstTestTimestamp(Instant deviceFirstTestTimestamp)
    {
        this.deviceFirstTestTimestamp = deviceFirstTestTimestamp;
    }

    public Instant getDeviceLastTestTimestamp()
    {
        return deviceLastTestTimestamp;
    }

    public void setDeviceLastTestTimestamp(Instant deviceLastTestTimestamp)
    {
        this.deviceLastTestTimestamp = deviceLastTestTimestamp;
    }

    public Instant getDeviceRegistrationTimestamp()
    {
        return deviceRegistrationTimestamp;
    }

    public void setDeviceRegistrationTimestamp(Instant deviceRegistrationTimestamp)
    {
        this.deviceRegistrationTimestamp = deviceRegistrationTimestamp;
    }

    public Instant getDeviceActivationTimestamp()
    {
        return deviceActivationTimestamp;
    }

    public void setDeviceActivationTimestamp(Instant deviceActivationTimestamp)
    {
        this.deviceActivationTimestamp = deviceActivationTimestamp;
    }

    public String getDeviceImageModemLibrary()
    {
        return deviceImageModemLibrary;
    }

    public void setDeviceImageModemLibrary(String deviceImageModemLibrary)
    {
        this.deviceImageModemLibrary = deviceImageModemLibrary;
    }

    public String getDeviceImageCmiuApplication()
    {
        return deviceImageCmiuApplication;
    }

    public void setDeviceImageCmiuApplication(String deviceImageCmiuApplication)
    {
        this.deviceImageCmiuApplication = deviceImageCmiuApplication;
    }

    public String getDeviceImageCmiuBootloader()
    {
        return deviceImageCmiuBootloader;
    }

    public void setDeviceImageCmiuBootloader(String deviceImageCmiuBootloader)
    {
        this.deviceImageCmiuBootloader = deviceImageCmiuBootloader;
    }

    public String getDeviceImageCmiuConfiguration()
    {
        return deviceImageCmiuConfiguration;
    }

    public void setDeviceImageCmiuConfiguration(String deviceImageCmiuConfiguration)
    {
        this.deviceImageCmiuConfiguration = deviceImageCmiuConfiguration;
    }

    public String getDeviceModemFirmwareRevision()
    {
        return deviceModemFirmwareRevision;
    }

    public void setDeviceModemFirmwareRevision(String deviceModemFirmwareRevision)
    {
        this.deviceModemFirmwareRevision = deviceModemFirmwareRevision;
    }

    public String getDeviceBluetoothMacAddress()
    {
        return deviceBluetoothMacAddress;
    }

    public void setDeviceBluetoothMacAddress(String deviceBluetoothMacAddress)
    {
        this.deviceBluetoothMacAddress = deviceBluetoothMacAddress;
    }
}
