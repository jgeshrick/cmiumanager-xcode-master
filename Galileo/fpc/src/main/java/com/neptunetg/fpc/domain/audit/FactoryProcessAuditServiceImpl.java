/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.domain.audit;

import com.neptunetg.common.data.miu.cellular.ConfigSet;
import com.neptunetg.fpc.data.audit.FactoryProcessAuditRepository;
import com.neptunetg.fpc.json.MiuCellularConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Auditing service for factory process
 */
@Service
public class FactoryProcessAuditServiceImpl implements FactoryProcessAuditService
{
    @Autowired
    private FactoryProcessAuditRepository fpAuditRepo;

    @Override
    public void logDeviceRegistration(MiuCellularConfig miuCellularConfig, boolean requestSubmittedToCNS)
    {
        fpAuditRepo.logDeviceRegistration(miuCellularConfig, requestSubmittedToCNS);
    }

}
