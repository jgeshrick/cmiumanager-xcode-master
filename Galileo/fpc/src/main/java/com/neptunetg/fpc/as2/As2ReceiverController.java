/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.as2;

import com.helger.as2lib.cert.PKCS12CertificateFactory;
import com.helger.as2lib.exception.OpenAS2Exception;
import com.helger.as2lib.message.AS2Message;
import com.helger.as2lib.message.IMessage;
import com.helger.as2lib.partner.SelfFillingPartnershipFactory;
import com.helger.as2lib.processor.CNetAttribute;
import com.helger.as2lib.processor.DefaultMessageProcessor;
import com.helger.as2lib.processor.receiver.AS2ReceiverModule;
import com.helger.as2lib.processor.receiver.AbstractActiveNetModule;
import com.helger.as2lib.processor.sender.AsynchMDNSenderModule;
import com.helger.as2lib.session.AS2Session;
import com.helger.as2lib.util.CAS2Header;
import com.helger.as2lib.util.StringMap;
import com.helger.as2lib.util.http.HTTPHelper;
import com.helger.commons.io.resource.ClassPathResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

/**
 * Listener for AS2 request from Verizon EDI for EDI 995
 */
@RestController
public class As2ReceiverController /* extends AS2ReceiverHandler*/
{
    /**
     * The file path to the PKCS12 key store
     */
    private static final String PKCS12_CERTSTORE_PATH = "/keystore/certificates.p12";

    /**
     * The password to open the PKCS12 key store
     */
    private static final String PKCS12_CERTSTORE_PASSWORD = "test";

    private static final Logger logger = LoggerFactory.getLogger(As2ReceiverController.class);
    private final As2ReceiverCustomHandler as2ReceiverHandler;

    public As2ReceiverController() throws OpenAS2Exception, IOException
    {
        final AS2Session as2Session = new AS2Session();
        final AS2ReceiverModule as2ReceiverModule = new AS2ReceiverModule();
        final AsynchMDNSenderModule asynchMDNSenderModule = new AsynchMDNSenderModule();

        initCertificateFactory(as2Session);

        final SelfFillingPartnershipFactory aPartnershipFactory = new SelfFillingPartnershipFactory();
        as2Session.setPartnershipFactory(aPartnershipFactory);

//        // Use a self-filling in-memory partnership factory
        final DefaultMessageProcessor aMessageProcessor = new DefaultMessageProcessor();
        as2Session.setMessageProcessor(aMessageProcessor);
        as2Session.getMessageProcessor().addModule(asynchMDNSenderModule);

        final StringMap aParams = new StringMap();
        aParams.setAttribute(AbstractActiveNetModule.ATTR_PORT, 8080);  //set to any number although we do not use it
        as2ReceiverModule.initDynamicComponent(as2Session, aParams);

        asynchMDNSenderModule.initDynamicComponent(as2Session, null);

        as2ReceiverHandler = new As2ReceiverCustomHandler(as2ReceiverModule);

        //start modules
        as2Session.getMessageProcessor().getAllModules().forEach(entries -> logger.info(entries.getName()));

        as2Session.getMessageProcessor().startActiveModules();

    }

    /**
     * Use Spring MVC to receive http request to this url, and attempt to extract the content and treat it as an incoming
     * AS2 request. Extract, decrypt and verify the AS2 content and send an MDN if Sync MDN is requested, else
     * return with HTTP-OK and send an Async MDN.
     * <p>
     * <p>
     * Adapted from https://github.com/phax/as2-lib/blob/master/as2-servlet/src/main/java/com/helger/as2servlet/AS2ReceiveServlet.java
     * <p>
     * This is used to process automated functional acknowledgement HTTP POST from Verizon VZW server - After an EDI 856
     * document has been send to VZW AS2 server. When the document is processed, Verizon will send back
     * an automated functional acknowledgement (997) asynchronously. This document is used to inform us whether our previously
     * send EDI document has been correctly processed.
     */
    @RequestMapping(value = "fpc/as2")
    @ResponseBody
    public ResponseEntity processFunctionalAcknowledgement(
            HttpServletRequest request)
    {
        // Create empty message
        final AS2Message as2Message = new AS2Message();
        as2Message.setAttribute(CNetAttribute.MA_SOURCE_IP, request.getRemoteAddr());
        as2Message.setAttribute(CNetAttribute.MA_SOURCE_PORT, Integer.toString(request.getRemotePort()));
        as2Message.setAttribute(CNetAttribute.MA_DESTINATION_IP, request.getLocalAddr());
        as2Message.setAttribute(CNetAttribute.MA_DESTINATION_PORT, Integer.toString(request.getLocalPort()));

        // Request type (e.g. "POST")
        as2Message.setAttribute(HTTPHelper.MA_HTTP_REQ_TYPE, request.getMethod());

        // Request URL (e.g. "/as2")
        as2Message.setAttribute(HTTPHelper.MA_HTTP_REQ_URL, request.getRequestURI());

        // Add all request headers to the AS2 message
        logger.debug("Receive request from:" + request.getRemoteHost());

        logger.debug("Request header:");
        final Enumeration<?> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements())
        {
            final String headerName = (String) headerNames.nextElement();
            final Enumeration<?> headerValues = request.getHeaders(headerName);

            while (headerValues.hasMoreElements())
            {
                final String value = (String) headerValues.nextElement();
                as2Message.addHeader(headerName, value);
                logger.debug("{}: \t{}", headerName, value);
            }
        }

        try
        {
            // Read the S/MIME content
            final byte[] messageData = readHttpPayload(request.getInputStream(), as2Message);

            // Call main handling method

            // Handle the incoming message, and return the MDN if necessary
            final String clientInfo = request.getRemoteAddr() + ":" + request.getRemotePort();

            return as2ReceiverHandler.handleIncomingMessage(clientInfo, messageData, as2Message);
        }
        catch (IOException e)
        {
            logger.error("Error in extracting http request payload. Check the request has been formatted correctly", e);
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Copied from HTTPHelper.readHttpPayload
     * Extract request content from the incoming AS2 request
     *
     * @param is      the input stream from the http request
     * @param message as2 message which is populated from the request header.
     * @return byte array extract from the request payload
     * @throws IOException
     */
    private static byte[] readHttpPayload(final InputStream is, final IMessage message) throws IOException
    {
        if (is == null || message == null)
        {
            throw new IOException("Null input parameters");
        }

        final DataInputStream dataInputStream = new DataInputStream(is);

        // Retrieve the message content
        byte[] data = null;
        final String contentLength = message.getHeader(CAS2Header.HEADER_CONTENT_LENGTH);
        if (contentLength == null)
        {
            // No "Content-Length" header present
            final String transferEncoding = message.getHeader(CAS2Header.HEADER_TRANSFER_ENCODING);
            if (transferEncoding != null)
            {
                // Remove all whitespaces in the value
                if (transferEncoding.replaceAll("\\s+", "").equalsIgnoreCase("chunked"))
                {
                    // chunked encoding
                    int length = 0;
                    for (; ; )
                    {
                        // First get hex chunk length; followed by CRLF
                        int blockLen = 0;
                        for (; ; )
                        {
                            int ch = dataInputStream.readByte();

                            if (ch == '\n') break;
                            if (ch >= 'a' && ch <= 'f') ch -= ('a' - 10);
                            else if (ch >= 'A' && ch <= 'F') ch -= ('A' - 10);
                            else if (ch >= '0' && ch <= '9') ch -= '0';
                            else continue;

                            blockLen = (blockLen * 16) + ch;
                        }
                        // Zero length is end of chunks
                        if (blockLen == 0) break;

                        // Ok, now read new chunk
                        final int newLength = length + blockLen;
                        final byte[] aNewData = new byte[newLength];
                        if (length > 0)
                        {
                            System.arraycopy(data, 0, aNewData, 0, length);
                        }

                        dataInputStream.readFully(aNewData, length, blockLen);
                        data = aNewData;
                        length = newLength;

                        // And now the CRLF after the chunk;
                        while (dataInputStream.readByte() != '\n')
                        { //deliberate empty body
                        }
                    }
                    message.setHeader(CAS2Header.HEADER_CONTENT_LENGTH, Integer.toString(length));
                }
                else
                {
                    // No "Content-Length" and unsupported "Transfer-Encoding"
                    logger.error("No \"Content-Length\" and unsupported \"Transfer-Encoding");

                    throw new IOException("Transfer-Encoding unimplemented: " + transferEncoding);
                }
            }
            else
            {
                // No "Content-Length" and no "Transfer-Encoding"
                logger.error("No \"Content-Length\" and no \"Transfer-Encoding");
                throw new IOException("Content-Length missing");
            }
        }
        else
        {
            // "Content-Length" is present
            // Receive the transmission's data
            // XX if a value > 2GB comes in, this will fail!!
            final int contentSize = Integer.parseInt(contentLength);
            data = new byte[contentSize];
            dataInputStream.readFully(data);
        }

        return data;
    }

    /**
     * Initialise certificate from keystore and add to the session supplied in the parameter. The session is passed around
     * to AS2 receiver handler, and the certificate factory is then retrieved from the session.
     *
     * @param as2Session the session to add the cert factory to
     * @throws OpenAS2Exception
     */
    public static void initCertificateFactory(final AS2Session as2Session) throws OpenAS2Exception, IOException
    {
        // Dynamically add certificate factory
        final StringMap params = new StringMap();

        File keystore = ClassPathResource.getAsFile(PKCS12_CERTSTORE_PATH);

        if (keystore != null)
        {
            params.setAttribute(PKCS12CertificateFactory.ATTR_FILENAME, keystore.getAbsolutePath());
        }
        else
        {
            throw new IOException("Cannot load keystore file " + PKCS12_CERTSTORE_PATH);
        }

        params.setAttribute(PKCS12CertificateFactory.ATTR_PASSWORD, PKCS12_CERTSTORE_PASSWORD);
        params.setAttribute(PKCS12CertificateFactory.ATTR_SAVE_CHANGES_TO_FILE, false);

        final PKCS12CertificateFactory certificateFactory = new PKCS12CertificateFactory();
        certificateFactory.initDynamicComponent(as2Session, params);

        //we can dynamically add other certificates here by calling aCertFactory.addCertificate

        as2Session.setCertificateFactory(certificateFactory);
    }
}
