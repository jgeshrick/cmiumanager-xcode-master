/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * An interceptor to validate token before passing to the relevant Rest controller methods.
 */
public class ApiRequestInterceptor extends BaseTokenAuthenticatingRestController implements HandlerInterceptor
{
    private static final Logger log = LoggerFactory.getLogger(ApiRequestInterceptor.class);

    private static final String TOKEN = "token";

    /**
     * Intercept MDCE JSON API request to validate token first.
     * @return true if token is validated
     * @throws Exception if token validation failed.
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        //validate token
        final String token = request.getParameter(TOKEN);
        validateToken(token, request);

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
    {
        //do nothing
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse httpServletResponse, Object handler, Exception e) throws Exception
    {
        if (e != null)
        {
            log.error("Error while executing API request " + request.getRequestURI(), e);
        }
        else
        {
            log.debug("Successfully completed API request " + request.getRequestURI());
        }
    }
}
