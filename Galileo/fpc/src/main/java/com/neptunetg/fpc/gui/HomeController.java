/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.gui;

import com.neptunetg.fpc.domain.ApplicationVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.Instant;
import java.util.Date;

/**
 * Home controller,
 */
@Controller
public class HomeController
{
    @Autowired
    private ApplicationVersion applicationVersion;

    @Value("#{envProperties['env.name']}")
    private String envName;


    @RequestMapping(value = "/pages/debug", method= {RequestMethod.GET, RequestMethod.HEAD})
    public String getHomePage(Model model)
    {
        model.addAttribute("environmentName", envName);
        model.addAttribute("currentServerTime", Date.from(Instant.now()));
        model.addAttribute("applicationVersion", applicationVersion.getApplicationVersion());

        return "index.jsp";
    }
}
