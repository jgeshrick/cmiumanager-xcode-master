/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 *
 */

package com.neptunetg.fpc.cns;

import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Provides access to CNS instances for each supported network
 */
@Service
public class CellularNetworkServiceManager
{
    public static final String ATT = "ATT";
    public static final String VZW = "VZW";

    @Autowired
    @Qualifier("vzwCns")
    private CellularNetworkService vzwCns;

    @Autowired
    @Qualifier("attCns")
    private CellularNetworkService attCns;

    public CellularNetworkService getCns(String networkIdentifier)
    {
        if (ATT.equalsIgnoreCase(networkIdentifier))
        {
            return attCns;
        }
        else if (VZW.equalsIgnoreCase(networkIdentifier))
        {
            return vzwCns;
        }
        else
        {
            throw new IllegalArgumentException("Unknown network operator: " + networkIdentifier);
        }
    }


    public CellularNetworkService getCns(CellularDeviceDetail cellularDeviceInfo) throws MiuDataException
    {
        if (cellularDeviceInfo == null)
        {
            throw new IllegalArgumentException("Attempting to get CNS interface for null cellularDeviceInfo");
        }
        String mno = null;
        try
        {
            mno = cellularDeviceInfo.getMno();
            return getCns(mno);
        }
        catch (Exception e)
        {
            throw new MiuDataException("Exception getting CNS interface for MIU " + cellularDeviceInfo.getMiuId() + ", MNO=" + mno, e);
        }
    }
}
