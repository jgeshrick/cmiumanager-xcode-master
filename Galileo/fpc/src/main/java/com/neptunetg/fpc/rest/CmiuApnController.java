/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.rest;

import com.neptunetg.fpc.data.apn.MobileApnRepo;
import com.neptunetg.fpc.data.apn.model.CellularProviderEnum;
import com.neptunetg.fpc.rest.json.cmiuapn.JsonCmiuApn;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import javafx.scene.control.Cell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * IF45
 */
@RestController
public class CmiuApnController extends BaseTokenAuthenticatingRestController
{
    private static final Logger log = LoggerFactory.getLogger(CmiuApnController.class);

    @Autowired
    MobileApnRepo mobRepo;

    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "/apn-info", method = RequestMethod.GET)
    @ResponseBody
    public JsonCmiuApn getApnForMno(@RequestParam(value = "mno") String mno) throws BadRequestException
    {
        CellularProviderEnum provider = CellularProviderEnum.fromStringValue(mno.toUpperCase());

        if(provider == null)
        {
            throw new BadRequestException("Invalid Mobile Operator, should be VZW or ATT");
        }

        String apn = mobRepo.getApnForCellularProvider(provider);

        JsonCmiuApn jsonCmiuApn = new JsonCmiuApn();
        jsonCmiuApn.setMno(mno);
        jsonCmiuApn.setApn(apn);

        return jsonCmiuApn;
    }
}
