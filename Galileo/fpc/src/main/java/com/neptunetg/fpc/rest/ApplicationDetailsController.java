/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.rest;

import com.neptunetg.fpc.domain.ApplicationVersion;
import com.neptunetg.fpc.json.ApplicationInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

/**
 * Utility for getting application versions
 */
@RestController
public class ApplicationDetailsController
{
    @Autowired
    private ApplicationVersion applicationVersion;

    @Value("#{envProperties['env.name']}")
    private String envName;

    @Value("#{envProperties['server.name']}")
    private String serverName;

    /**
     * Return FPC server application information to client.
     * @return
     */
    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "/info", method = {RequestMethod.GET})
    public ApplicationInformation getApplicationVersion()
    {
        final ApplicationInformation appInfo = new ApplicationInformation();
        appInfo.setEnvironment(envName);
        appInfo.setServer(serverName);
        appInfo.setVersion(applicationVersion.getApplicationVersion());
        appInfo.setCurrentTimeUtc(Instant.now().toString());

        return appInfo;
    }
}
