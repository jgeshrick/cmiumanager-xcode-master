/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.json.manufacturinginfo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by WJD1 on 05/04/2016.
 * Class to represent device details in JSON
 */
public class DeviceDetailsJson
{
    @JsonProperty("creation_timestamp")
    private String deviceCreationTimestamp;

    @JsonProperty("first_test_timestamp")
    private String deviceFirstTestTimestamp;

    @JsonProperty("last_test_timestamp")
    private String deviceLastTestTimestamp;

    @JsonProperty("registration_timestamp")
    private String deviceRegistrationTimestamp;

    @JsonProperty("activation_timestamp")
    private String deviceActivationTimestamp;

    @JsonProperty("images")
    private ImageJson images;

    @JsonProperty("modem")
    private ModemJson modem;

    @JsonProperty("bluetooth")
    private BluetoothJson bluetooth;

    public String getDeviceCreationTimestamp()
    {
        return deviceCreationTimestamp;
    }

    public void setDeviceCreationTimestamp(String deviceCreationTimestamp)
    {
        this.deviceCreationTimestamp = deviceCreationTimestamp;
    }

    public String getDeviceFirstTestTimestamp()
    {
        return deviceFirstTestTimestamp;
    }

    public void setDeviceFirstTestTimestamp(String deviceFirstTestTimestamp)
    {
        this.deviceFirstTestTimestamp = deviceFirstTestTimestamp;
    }

    public String getDeviceLastTestTimestamp()
    {
        return deviceLastTestTimestamp;
    }

    public void setDeviceLastTestTimestamp(String deviceLastTestTimestamp)
    {
        this.deviceLastTestTimestamp = deviceLastTestTimestamp;
    }

    public String getDeviceRegistrationTimestamp()
    {
        return deviceRegistrationTimestamp;
    }

    public void setDeviceRegistrationTimestamp(String deviceRegistrationTimestamp)
    {
        this.deviceRegistrationTimestamp = deviceRegistrationTimestamp;
    }

    public String getDeviceActivationTimestamp()
    {
        return deviceActivationTimestamp;
    }

    public void setDeviceActivationTimestamp(String deviceActivationTimestamp)
    {
        this.deviceActivationTimestamp = deviceActivationTimestamp;
    }

    public ImageJson getImages()
    {
        return images;
    }

    public void setImages(ImageJson images)
    {
        this.images = images;
    }

    public ModemJson getModem()
    {
        return modem;
    }

    public void setModem(ModemJson modem)
    {
        this.modem = modem;
    }

    public BluetoothJson getBluetooth()
    {
        return bluetooth;
    }

    public void setBluetooth(BluetoothJson bluetooth)
    {
        this.bluetooth = bluetooth;
    }
}
