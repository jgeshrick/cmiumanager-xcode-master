/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.data.miu.manufacturinginfo;

import java.time.Instant;
import java.util.ArrayList;

/**
 * Created by WJD1 on 04/04/2016.
 */
public class MiuManufactureInfoTest
{
    private Instant testTimestamp;
    private int testId;
    private int testStationId;
    private MiuManufactureInfoTestStation miuManufactureInfoTestStation;
    private ArrayList<MiuManufactureInfoTestLog> miuManufactureInfoTestLogs;

    public Instant getTestTimestamp()
    {
        return testTimestamp;
    }

    public void setTestTimestamp(Instant testTimestamp)
    {
        this.testTimestamp = testTimestamp;
    }

    public int getTestId()
    {
        return testId;
    }

    public void setTestId(int testId)
    {
        this.testId = testId;
    }

    public int getTestStationId()
    {
        return testStationId;
    }

    public void setTestStationId(int testStationId)
    {
        this.testStationId = testStationId;
    }

    public MiuManufactureInfoTestStation getMiuManufactureInfoTestStation()
    {
        return miuManufactureInfoTestStation;
    }

    public void setMiuManufactureInfoTestStation(MiuManufactureInfoTestStation miuManufactureInfoTestStation)
    {
        this.miuManufactureInfoTestStation = miuManufactureInfoTestStation;
    }

    public ArrayList<MiuManufactureInfoTestLog> getMiuManufactureInfoTestLogs()
    {
        return miuManufactureInfoTestLogs;
    }

    public void setMiuManufactureInfoTestLogs(ArrayList<MiuManufactureInfoTestLog> miuManufactureInfoTestLogs)
    {
        this.miuManufactureInfoTestLogs = miuManufactureInfoTestLogs;
    }
}
