/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.json.manufacturinginfo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by WJD1 on 05/04/2016.
 * Class to represent images in JSON
 */
public class ImageJson
{
    @JsonProperty("modem_library")
    private String deviceImageModemLibrary;

    @JsonProperty("cmiu_application")
    private String deviceImageCmiuApplication;

    @JsonProperty("cmiu_bootloader")
    private String deviceImageCmiuBootloader;

    @JsonProperty("cmiu_configuration")
    private String deviceImageCmiuConfiguration;

    public String getDeviceImageModemLibrary()
    {
        return deviceImageModemLibrary;
    }

    public void setDeviceImageModemLibrary(String deviceImageModemLibrary)
    {
        this.deviceImageModemLibrary = deviceImageModemLibrary;
    }

    public String getDeviceImageCmiuApplication()
    {
        return deviceImageCmiuApplication;
    }

    public void setDeviceImageCmiuApplication(String deviceImageCmiuApplication)
    {
        this.deviceImageCmiuApplication = deviceImageCmiuApplication;
    }

    public String getDeviceImageCmiuBootloader()
    {
        return deviceImageCmiuBootloader;
    }

    public void setDeviceImageCmiuBootloader(String deviceImageCmiuBootloader)
    {
        this.deviceImageCmiuBootloader = deviceImageCmiuBootloader;
    }

    public String getDeviceImageCmiuConfiguration()
    {
        return deviceImageCmiuConfiguration;
    }

    public void setDeviceImageCmiuConfiguration(String deviceImageCmiuConfiguration)
    {
        this.deviceImageCmiuConfiguration = deviceImageCmiuConfiguration;
    }
}
