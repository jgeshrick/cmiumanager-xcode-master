/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.fpc.rest.json.miuloraconfig;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * JSON for setting LoRa config
 */
public class JsonMiuLoraConfig
{
    @JsonProperty("miu_id")
    private long miuId;

    @JsonProperty("dev_eui")
    private String devEui;

    @JsonProperty("app_eui")
    private String appEui;


    public long getMiuId()
    {
        return this.miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public String getDevEui()
    {
        return devEui;
    }

    public void setDevEui(String devEui)
    {
        this.devEui = devEui;
    }

    public String getAppEui()
    {
        return appEui;
    }

    public void setAppEui(String appEui)
    {
        this.appEui = appEui;
    }
}

