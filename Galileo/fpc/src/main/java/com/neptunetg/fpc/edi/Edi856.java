/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi;

import com.neptunetg.fpc.edi.segment.BeginningSegment;
import com.neptunetg.fpc.edi.segment.FunctionalGroup;
import com.neptunetg.fpc.edi.segment.InterchangeControl;
import com.neptunetg.fpc.edi.segment.ItemLevel;
import com.neptunetg.fpc.edi.segment.KitLevel;
import com.neptunetg.fpc.edi.segment.OrderLevel;
import com.neptunetg.fpc.edi.segment.Segment;
import com.neptunetg.fpc.edi.segment.ShipmentLevel;
import com.neptunetg.fpc.edi.segment.SubpackLevel;
import com.neptunetg.fpc.edi.segment.TransactionSet;
import com.neptunetg.fpc.edi.segment.X12;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Generate EDI 856 document according to Verizon EDI specs
 */
public class Edi856
{
    private final String networkId;
    private final String partnerId;
    private String iccid;
    private String imei;
    private long cmiuId;
    private final EdiAs2Constants ediAs2Constants;

    public Edi856(final String networkId, final String partnerId, EdiAs2Constants ediAs2Constants)
    {
        this.networkId = networkId;
        this.partnerId = partnerId;
        this.ediAs2Constants = ediAs2Constants;
    }

    public Edi856 withPairingInformation(final long cmiuId, final String iccid, final String imei)
    {
        this.cmiuId = cmiuId;
        this.iccid = iccid;
        this.imei = imei;
        return this;
    }

    /**
     * Todo we need to fill in lots of fields especially in the HL segments!!!!
     * @return
     * @throws EdiException
     */
    public String generate() throws EdiException
    {
        LocalDateTime currentDateTime = LocalDateTime.now();

        //ISA and IEA
        //note: Pay attention to the identifier, they are different in test and production
        InterchangeControl interchangeControl = new InterchangeControl();
        interchangeControl.setIsa05SenderIdQualifier("ZZ");
        interchangeControl.setIsa06SenderId(this.networkId);
        interchangeControl.setIsa07ReceiverIdQualifier(EdiAs2Constants.VERIZON_EDI_QUALIFIER);
        interchangeControl.setIsa08ReceiverId(ediAs2Constants.getIsa08EdiId());
        interchangeControl.setIsa13ControlNumber((int)cmiuId);
        interchangeControl.setIsa15UsageIndicator(ediAs2Constants.getEdiIsa15UsageIndicator());
        interchangeControl.setIsaDateTimeOfExchange(currentDateTime.toInstant(ZoneOffset.UTC));

        //GS/GE
        FunctionalGroup groupHeader = new FunctionalGroup(this.partnerId, EdiAs2Constants.VERIZON_EDI_GROUP_ID);
        groupHeader.setDataInterchangeDateTime(currentDateTime.toInstant(ZoneOffset.UTC));
        groupHeader.setGs06DataInterchangeControlNumber(123);

        //ST/SE
        TransactionSet transactionSet = new TransactionSet(1);

        //BSN, DTM, DTM, CTT
        BeginningSegment segment = new BeginningSegment();
        segment.setDtm02ShippedDate(currentDateTime.toInstant(ZoneOffset.UTC));

        ShipmentLevel hl1 = new ShipmentLevel();

        OrderLevel hl2 = new OrderLevel(String.valueOf(this.cmiuId));
        hl2.setPrf04PODate(currentDateTime.toInstant(ZoneOffset.UTC));

        ItemLevel hl3 = new ItemLevel();

        KitLevel hl4 = new KitLevel(this.imei, this.iccid);

        SubpackLevel hl5 = new SubpackLevel(this.imei, this.iccid);

        //contains all segments within the ST and SE segments
        List<Segment> transactionSetSegments = new ArrayList<>();

        transactionSetSegments.addAll(segment.getDataSegments());
        transactionSetSegments.addAll(hl1.getDataSegments());
        transactionSetSegments.addAll(hl2.getDataSegments());
        transactionSetSegments.addAll(hl3.getDataSegments());
        transactionSetSegments.addAll(hl4.getDataSegments());
        transactionSetSegments.addAll(hl5.getDataSegments());

        //need to update GE
        transactionSet.setSe01NumberOfSegments(transactionSetSegments.size() + TransactionSet.TRANSACTION_SET_SEGMENTS);

        //now generate all edi segments
        List<Segment> ediSegments = new ArrayList<>();
        ediSegments.addAll(interchangeControl.getHeaderSegments());
        ediSegments.addAll(groupHeader.getHeaderSegments());
        ediSegments.addAll(transactionSet.getHeaderSegments());

        ediSegments.addAll(transactionSetSegments);

        ediSegments.addAll(transactionSet.getTrailerSegments());
        ediSegments.addAll(groupHeader.getTrailerSegments());
        ediSegments.addAll(interchangeControl.getTrailerSegments());

        return ediSegments.stream().map(Segment::toString).collect(Collectors.joining(X12.LINE_SEPARATOR));

    }
}
