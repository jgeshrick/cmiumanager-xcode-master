/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Implements IF119 - TODO NOT YET DEFINED
 * The caller of this REST service is FPC client
 */
@RestController
public class BillingActivationController
{
    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "/activate-billing", method = {RequestMethod.GET, RequestMethod.HEAD})
    public String activateBilling(String request)
    {
        //TODO - implement billing activation

        return "OK";
    }
}
