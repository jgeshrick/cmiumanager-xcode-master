/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.json.manufacturinginfo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by WJD1 on 04/04/2016.
 * Class to hold manufacturing test station information
 */
public class MiuManufactureInfoTestStationJson
{
    @JsonProperty("type")
    private String type;

    @JsonProperty("bay")
    private String bay;

    @JsonProperty("rack_id")
    private String rackId;

    @JsonProperty("fixture_id")
    private String fixtureId;

    @JsonProperty("calibration_date")
    private String calibrationDate;

    @JsonProperty("labview_application_version")
    private String labviewApplicationVersion;

    @JsonProperty("labview_platform_version")
    private String labviewPlatformVersion;

    @JsonProperty("ncc_version")
    private String nccVersion;

    @JsonProperty("os_description")
    private String osDescription;

    @JsonProperty("btle_dongle_driver_version")
    private String btleDongleDriverVersion;

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getBay()
    {
        return bay;
    }

    public void setBay(String bay)
    {
        this.bay = bay;
    }

    public String getRackId()
    {
        return rackId;
    }

    public void setRackId(String rackId)
    {
        this.rackId = rackId;
    }

    public String getFixtureId()
    {
        return fixtureId;
    }

    public void setFixtureId(String fixtureId)
    {
        this.fixtureId = fixtureId;
    }

    public String getCalibrationDate()
    {
        return calibrationDate;
    }

    public void setCalibrationDate(String calibrationDate)
    {
        this.calibrationDate = calibrationDate;
    }

    public String getLabviewApplicationVersion()
    {
        return labviewApplicationVersion;
    }

    public void setLabviewApplicationVersion(String labviewApplicationVersion)
    {
        this.labviewApplicationVersion = labviewApplicationVersion;
    }

    public String getLabviewPlatformVersion()
    {
        return labviewPlatformVersion;
    }

    public void setLabviewPlatformVersion(String labviewPlatformVersion)
    {
        this.labviewPlatformVersion = labviewPlatformVersion;
    }

    public String getNccVersion()
    {
        return nccVersion;
    }

    public void setNccVersion(String nccVersion)
    {
        this.nccVersion = nccVersion;
    }

    public String getOsDescription()
    {
        return osDescription;
    }

    public void setOsDescription(String osDescription)
    {
        this.osDescription = osDescription;
    }

    public String getBtleDongleDriverVersion()
    {
        return btleDongleDriverVersion;
    }

    public void setBtleDongleDriverVersion(String btleDongleDriverVersion)
    {
        this.btleDongleDriverVersion = btleDongleDriverVersion;
    }
}
