/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.json.manufacturinginfo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by WJD1 on 04/04/2016.
 * Class to hold JSON manufacturing information for a MIU
 */
public class MiuManufactureInfoJson
{
    @JsonProperty("miu_id")
    private int miuId;

    @JsonProperty("tests")
    private ArrayList<MiuManufactureInfoTestJson> miuManufactureInfoTestJsons;

    @JsonProperty("rework")
    private ReworkJson rework;

    @JsonProperty("device_details")
    private DeviceDetailsJson deviceDetails;

    public int getMiuId()
    {
        return miuId;
    }

    public void setMiuId(int miuId)
    {
        this.miuId = miuId;
    }

    public ArrayList<MiuManufactureInfoTestJson> getMiuManufactureInfoTestJsons()
    {
        return miuManufactureInfoTestJsons;
    }

    public void setMiuManufactureInfoTestJsons(ArrayList<MiuManufactureInfoTestJson> miuManufactureInfoTestJsons)
    {
        this.miuManufactureInfoTestJsons = miuManufactureInfoTestJsons;
    }

    public ReworkJson getRework()
    {
        return rework;
    }

    public void setRework(ReworkJson rework)
    {
        this.rework = rework;
    }

    public DeviceDetailsJson getDeviceDetails()
    {
        return deviceDetails;
    }

    public void setDeviceDetails(DeviceDetailsJson deviceDetails)
    {
        this.deviceDetails = deviceDetails;
    }
}
