/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.domain.audit;

import com.neptunetg.fpc.json.MiuCellularConfig;

/**
 * Auditing service for factory process
 */
public interface FactoryProcessAuditService
{
    /**
     * Record device pairing request
     * @param miuCellularConfig contains miu id, iccid and imei
     */
    void logDeviceRegistration(MiuCellularConfig miuCellularConfig, boolean requestSubmittedToCNS);
}
