/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 *
 */

package com.neptunetg.fpc.domain.miu;

import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.common.domain.mqtt.MiuMqttSubscriptionManager;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.ConflictException;
import com.neptunetg.mdce.common.api.rest.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Manages MIU updates within DB transactions
 */
@Service
@Transactional
public class MiuManagementServiceImpl implements MiuManagementService
{
    private final CmiuRepository cmiuRepository;

    private final MiuMqttSubscriptionManager mqttSubscriptionManager;

    @Autowired
    public MiuManagementServiceImpl(CmiuRepository cmiuRepository, MiuMqttSubscriptionManager mqttSubscriptionManager)
    {
        this.cmiuRepository = cmiuRepository;
        this.mqttSubscriptionManager = mqttSubscriptionManager;
    }

    @Override
    public void updateOrInsertMiuCellularDetails(MiuId miuId, CellularDeviceDetail cellularDeviceDetail) throws BadRequestException, ConflictException
    {
        try
        {
            this.mqttSubscriptionManager.subscribeMiuMqttIfApplicable(miuId, MiuType.CMIU);
            this.cmiuRepository.updateOrInsertMiuCellularDetails(miuId, cellularDeviceDetail);
        }
        catch (MiuDataException e)
        {
            if (e.isConflict())
            {
                throw new ConflictException("Error when updating cellular details for CMIU " + miuId, e);
            }
            else
            {
                throw new BadRequestException("Error when updating cellular details for CMIU " + miuId, e);
            }
        }

    }

    @Override
    public CellularDeviceDetail getCellularConfig(MiuId miuId) throws MiuDataException
    {
        final CellularDeviceDetail ret = this.cmiuRepository.getCellularConfig(miuId);
        if (ret == null)
        {
            throw new NotFoundException("MIU does not exist, or no cellular details set");
        }
        else
        {
            return ret;
        }
    }
}
