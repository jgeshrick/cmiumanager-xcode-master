/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.as2;

import com.helger.as2lib.crypto.ICryptoHelper;
import com.helger.as2lib.disposition.DispositionException;
import com.helger.as2lib.disposition.DispositionType;
import com.helger.as2lib.exception.OpenAS2Exception;
import com.helger.as2lib.exception.WrappedOpenAS2Exception;
import com.helger.as2lib.message.AS2Message;
import com.helger.as2lib.message.IMessageMDN;
import com.helger.as2lib.processor.NoModuleException;
import com.helger.as2lib.processor.receiver.AS2ReceiverModule;
import com.helger.as2lib.processor.receiver.AbstractActiveNetModule;
import com.helger.as2lib.processor.receiver.net.AS2ReceiverHandler;
import com.helger.as2lib.processor.sender.IProcessorSenderModule;
import com.helger.as2lib.processor.storage.IProcessorStorageModule;
import com.helger.as2lib.session.ComponentNotFoundException;
import com.helger.as2lib.session.IAS2Session;
import com.helger.as2lib.util.AS2Helper;
import com.helger.as2lib.util.CAS2Header;
import com.helger.as2lib.util.javamail.ByteArrayDataSource;
import com.helger.commons.io.stream.NonBlockingByteArrayOutputStream;
import com.helger.commons.io.stream.StreamHelper;
import com.neptunetg.fpc.edi.EdiException;
import com.neptunetg.fpc.edi.model.FunctionalAcknowledgment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;

import javax.activation.DataHandler;
import javax.annotation.Nonnull;
import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.internet.ContentType;
import javax.mail.internet.MimeBodyPart;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

/**
 * Custom handler that extends original As2ReceiverHandler
 */
public class As2ReceiverCustomHandler extends AS2ReceiverHandler
{
    private static final Logger logger = LoggerFactory.getLogger(As2ReceiverController.class);
    private final AS2ReceiverModule receiverModule;

    public As2ReceiverCustomHandler(AS2ReceiverModule receiverModule)
    {
        super(receiverModule);
        this.receiverModule = receiverModule;
    }

    /**
     * Handle an incoming HTTP message AFTER the headers where extracted.
     *
     * @param clientInfo Client connection info
     * @param msgData    The message body
     * @param msg        The AS2 message that will be filled by this method
     */
    public ResponseEntity handleIncomingMessage(@Nonnull final String clientInfo,
                                                @Nonnull final byte[] msgData,
                                                @Nonnull final AS2Message msg)
    {
        // TODO store HTTP request, headers, and data to file in Received folder
        // -> use message-id for filename?
        try
        {
            final IAS2Session session = receiverModule.getSession();

            try
            {
                // Put received data in a MIME body part
                final ContentType receivedContentType = new ContentType(msg.getHeader(CAS2Header.HEADER_CONTENT_TYPE));
                final String sReceivedContentType = receivedContentType.toString();

                final MimeBodyPart receivedPart = new MimeBodyPart();
                receivedPart.setDataHandler(new DataHandler(new ByteArrayDataSource(msgData, sReceivedContentType, null)));

                // Header must be set AFTER the DataHandler!
                receivedPart.setHeader(CAS2Header.HEADER_CONTENT_TYPE, sReceivedContentType);
                msg.setData(receivedPart);
            }
            catch (final Exception ex)
            {
                throw new DispositionException(DispositionType.createError("unexpected-processing-error"),
                        AbstractActiveNetModule.DISP_PARSING_MIME_FAILED,
                        ex);
            }

            // Extract AS2 ID's from header, find the message's partnership and
            // update the message
            try
            {
                final String as2From = msg.getAS2From();
                msg.getPartnership().setSenderAS2ID(as2From);

                final String as2To = msg.getAS2To();
                msg.getPartnership().setReceiverAS2ID(as2To);

                // Fill all partnership attributes etc.
                session.getPartnershipFactory().updatePartnership(msg, false);

                session.getPartnershipFactory().getAllPartnerships().forEach(partnership ->
                        logger.debug(partnership.getName()));
            }
            catch (final OpenAS2Exception ex)
            {
                throw new DispositionException(DispositionType.createError("authentication-failed"),
                        AbstractActiveNetModule.DISP_PARTNERSHIP_NOT_FOUND,
                        ex);
            }

            // Per RFC5402 compression is always before encryption but can be before
            // or after signing of message but only in one place
            final ICryptoHelper cryptoHelper = AS2Helper.getCryptoHelper();
            boolean isDecompressed = false;

            // Decrypt and verify signature of the data, and attach data to the
            // message
            decrypt(msg);

            if (cryptoHelper.isCompressed(msg.getContentType()))
            {
                logger.trace("Decompressing received message before checking signature...");
                decompress(msg);
                isDecompressed = true;
            }

            verify(msg);

            if (cryptoHelper.isCompressed(msg.getContentType()))
            {
                // Per RFC5402 compression is always before encryption but can be before
                // or after signing of message but only in one place
                if (isDecompressed)
                {
                    throw new DispositionException(DispositionType.createError("decompression-failed"),
                            AbstractActiveNetModule.DISP_DECOMPRESSION_ERROR,
                            new Exception("Message has already been decompressed. Per RFC5402 it cannot occur twice."));
                }

                if (msg.containsAttribute(AS2Message.ATTRIBUTE_RECEIVED_SIGNED))
                {
                    logger.trace("Decompressing received message after verifying signature...");
                }
                else
                {
                    logger.trace("Decompressing received message after decryption...");
                }

                decompress(msg);
                isDecompressed = true;
            }

            if (logger.isTraceEnabled())
            {
                try
                {
                    logger.trace("SMIME Decrypted Content-Disposition: " +
                            msg.getContentDisposition() +
                            "\n      Content-Type received: " +
                            msg.getContentType() +
                            "\n      HEADERS after decryption: " +
                            msg.getData().getAllHeaders() +
                            "\n      Content-Disposition in MSG detData() MIMEPART after decryption: " +
                            msg.getData().getContentType());
                }
                catch (final MessagingException ex)
                {
                    logger.error("Failed to trace message: " + msg, ex);
                }
            }

            //print the content
            try
            {
                String edi997Message = getStringFromInputStream((InputStream) msg.getData().getContent());
                logger.trace(edi997Message);

                FunctionalAcknowledgment functionalAck = FunctionalAcknowledgment.fromString(edi997Message);

                if (!functionalAck.isAccepted() && functionalAck.isHasErrors())
                {
                    logger.error("Error in EDI 997 functional acknowledge document");
                }

                //TODO save the EDI document to file this should be an EDI 997 Functional Acknowledgment document
            }
            catch (IOException | MessagingException e)
            {
                logger.error("Error extracting message content", e);
            }
            catch (EdiException e)
            {
                logger.error("Error parsing function acknowledge message", e);
            }

            //Send MDN - Message Delivery Notification
            try
            {
                if (msg.isRequestingMDN())
                {
                    if (msg.isRequestingAsynchMDN())
                    {
                        //if async MDN is required, we want to send HTTP_OK response to this request and then
                        //open a *separate* connection to send MDN
                        sendAsyncMDN(clientInfo, msg, DispositionType.createSuccess(), AbstractActiveNetModule.DISP_SUCCESS);

                        ResponseEntity responseEntity = new ResponseEntity(HttpStatus.NO_CONTENT);
                        return responseEntity;

                    }
                    else
                    {
                        // Transmit a success MDN if requested
                        ResponseEntity responseEntity = getSyncMDNResponse(clientInfo, msg, DispositionType.createSuccess(), AbstractActiveNetModule.DISP_SUCCESS);
                        return responseEntity;
                    }
                }
                else
                {
                    // TODO Let Spring MVC framework return HTTP_OK response
                    // Just send a HTTP OK
                    logger.info("sent HTTP OK " + clientInfo + msg.getLoggingText());

                    return new ResponseEntity(HttpStatus.OK);
                }
            }
            catch (final Exception ex)
            {
                throw new WrappedOpenAS2Exception("Error creating and returning MDN, message was stilled processed", ex);
            }
        }
        catch (final DispositionException ex)
        {
            ResponseEntity responseEntity = getSyncMDNResponse(clientInfo, msg, ex.getDisposition(), ex.getText());

            receiverModule.handleError(msg, ex);

            return responseEntity;
        }
        catch (final OpenAS2Exception ex)
        {
            receiverModule.handleError(msg, ex);

            ResponseEntity responseEntity = new ResponseEntity(ex.getMessage(), HttpStatus.BAD_REQUEST);
            return responseEntity;
        }
    }

    /**
     * Open a connection to send a async MDN to a AS2 server.
     *
     * @param clientInfo
     * @param msg
     * @param dispositionType
     * @param text
     */
    private void sendAsyncMDN(final String clientInfo,
                              final AS2Message msg,
                              final DispositionType dispositionType,
                              final String text)
    {
        final IAS2Session session = receiverModule.getSession();

        // if asyncMDN requested, close connection and initiate separate MDN

        logger.info("Setup to send async MDN [" + dispositionType.getAsString() + "] " + clientInfo + msg.getLoggingText());

        // trigger explicit sending
        try
        {
            final IMessageMDN aMdn = AS2Helper.createMDN(session, msg, dispositionType, text);
            session.getMessageProcessor().handle(IProcessorSenderModule.DO_SENDMDN, msg, null);
        }
        catch (OpenAS2Exception e)
        {
            logger.error("Error sending Async MDN", e);
        }
        catch (Exception e)
        {
            logger.error("Error constructing MDN", e);
        }
    }

    /**
     * Compose a sync MDN to be return to the AS2 POST request.
     *
     * @param clientInfo
     * @param msg
     * @param dispositionType
     * @param text
     * @return
     */
    private ResponseEntity getSyncMDNResponse(@Nonnull final String clientInfo,
                                              @Nonnull final AS2Message msg,
                                              @Nonnull final DispositionType dispositionType,
                                              @Nonnull final String text)
    {
        if (!msg.getPartnership().isBlockErrorMDN())
        {
            try
            {
                final IAS2Session session = receiverModule.getSession();
                final IMessageMDN mdn = AS2Helper.createMDN(session, msg, dispositionType, text);

                if (msg.isRequestingAsynchMDN())
                {
                    // we are just going to return a response

                    logger.info("Setup to send asynch MDN [" + dispositionType.getAsString() + "] " + clientInfo + msg.getLoggingText());

                    // trigger explicit sending
                    sendAsyncMDN(clientInfo, msg, DispositionType.createSuccess(), AbstractActiveNetModule.DISP_SUCCESS);
                }
                else
                {
                    // otherwise, send sync MDN back on same connection
                    logger.info("Sending back sync MDN [" + dispositionType.getAsString() + "] " + clientInfo + msg.getLoggingText());

                    // Get data and therefore content length for sync MDN
                    final NonBlockingByteArrayOutputStream data = new NonBlockingByteArrayOutputStream();
                    final MimeBodyPart part = mdn.getData();
                    StreamHelper.copyInputStreamToOutputStream(part.getInputStream(), data);
                    mdn.setHeader(CAS2Header.HEADER_CONTENT_LENGTH, Integer.toString(data.getSize()));

                    // start HTTP response

                    //create headers
                    // Add response headers
                    HttpHeaders responseHeaders = new HttpHeaders();

                    final Enumeration<Header> headers = mdn.getHeaders().getAllHeaders();
                    while (headers.hasMoreElements())
                    {
                        Header header = headers.nextElement();
                        responseHeaders.add(header.getName(), header.getValue());
                    }

                    // Save sent MDN for later examination
                    try
                    {
                        //TODO not required??
                        session.getMessageProcessor().handle(IProcessorStorageModule.DO_STOREMDN, msg, null);
                    }
                    catch (final ComponentNotFoundException ex)
                    {
                        // No message processor found
                        logger.error("Error in message processor", ex);
                    }
                    catch (final NoModuleException ex)
                    {
                        // No module found in message processor
                        logger.error("No module found", ex);
                    }
                    logger.info("sent MDN [" + dispositionType.getAsString() + "] " + clientInfo + msg.getLoggingText());

                    ResponseEntity responseEntity = new ResponseEntity<>(data.toByteArray(), responseHeaders, HttpStatus.OK);
                    return responseEntity;
                }
            }
            catch (final Exception ex)
            {
                final OpenAS2Exception we = WrappedOpenAS2Exception.wrap(ex);
                we.addSource(OpenAS2Exception.SOURCE_MESSAGE, msg);
                we.terminate();
            }
        }

        //return HTTP 500 on error
        HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity<>("", responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);

    }

    // convert InputStream to String
    private static String getStringFromInputStream(InputStream is)
    {
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        try
        {
            StreamUtils.copy(is, bs);
            return bs.toString("utf-8");
        }
        catch (IOException e)
        {
            logger.error("Error reading input stream", e);
        }

        return "Error in reading input stream!";
    }


}
