/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.rest;

import com.neptunetg.fpc.json.GenerateTokenResponse;
import com.neptunetg.mdce.common.api.domain.auth.TokenManagerService;
import com.neptunetg.mdce.common.api.rest.RestUtils;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Token service for FPC
 */
@RestController
public class GenerateTokenController
{
    private static final Pattern PARTNER_STRING_PATTERN = Pattern.compile("^[0-9a-fA-F]{32}$");

    @Autowired
    private TokenManagerService tokenManagerService;

    @RequestMapping(value=Constants.FPC_REST_URL_BASE + "/token", method= RequestMethod.GET)
    @ResponseBody
    public GenerateTokenResponse getToken(@RequestParam(value = "partner") String partnerKey,
                                          HttpServletRequest request) throws NotAuthorizedException, BadRequestException
    {
        if (!isParseablePartnerString(partnerKey))
        {
            throw new BadRequestException("Partner string is missing or invalid");
        }

        final String requestIp = RestUtils.getOriginatingIp(request);

        return new GenerateTokenResponse(tokenManagerService.generateToken(partnerKey, 0, requestIp));
    }

    @ExceptionHandler(NotAuthorizedException.class)
    public ResponseEntity<GenerateTokenResponse> responseNotAuthorizedError(NotAuthorizedException ex)
    {
        return new ResponseEntity<>(new GenerateTokenResponse(null), HttpStatus.UNAUTHORIZED);
    }

    /**
     * Verifies that the supplied string has exactly 32 hexadecimal characters.
     * @param partnerString The partner string to verify.
     * @return True if the supplied string is 32 hex characters; otherwise, false.
     */
    private boolean isParseablePartnerString(String partnerString)
    {
        if (!StringUtils.hasText(partnerString))
        {
            return false;
        }

        Matcher matcher = PARTNER_STRING_PATTERN.matcher(partnerString);

        return matcher.matches();
    }
}
