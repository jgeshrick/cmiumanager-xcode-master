/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;


import com.neptunetg.fpc.edi.EdiException;

import java.util.ArrayList;
import java.util.List;

/**
 * ST and SE tag
 */
public class TransactionSet extends X12
{
    private static final String ST_FORMAT = "ST*856*%04d" + SEGMENT_SEPARATOR;
    private static final String SE_FORMAT = "SE*%d*%04d" + SEGMENT_SEPARATOR;
    public static final int TRANSACTION_SET_SEGMENTS = 3;    //Header: ST, Trailer: CTT, SE

    private final int st02TransactionSetControlNumber;
    private int se01NumberOfSegments;   //total number of segments included in a transaction set including ST and SE segments

    public TransactionSet(int transactionSetControlNumber)
    {
        st02TransactionSetControlNumber = transactionSetControlNumber;
    }

    public String getHeader() throws EdiException
    {
        return getSegmentAsString(getHeaderSegments());
    }

    public List<Segment> getHeaderSegments() throws EdiException
    {
        Segment header = new Segment("ST");

        header
                .appendElement(new Element<String>("ST01", 3, 3, "856"))
                .appendElement(new Element<Integer>("ST02", 4, 9, st02TransactionSetControlNumber));

        List<Segment> segmentList = new ArrayList<>();
        segmentList.add(header);

        return segmentList;
    }

    public String getTrailer() throws EdiException
    {
        return getSegmentAsString(getTrailerSegments());
    }

    public List<Segment> getTrailerSegments() throws EdiException
    {
        Segment ctt = new Segment("CTT");
        ctt.appendElement(new Element<Integer>("CTT01", 1, 6, 5));    //total number of line items in the transaction set

        Segment trailer = new Segment("SE");

        trailer
                .appendElement(new Element<Integer>("SE01", 1, 10, se01NumberOfSegments))   //FIXME, total number of segments included in a transaction set including ST and SE segments
                .appendElement(new Element<Integer>("SE02", 4, 9, st02TransactionSetControlNumber));

        List<Segment> segmentList = new ArrayList<>();
        segmentList.add(ctt);
        segmentList.add(trailer);

        return segmentList;
    }

    public int getSe01NumberOfSegments()
    {
        return se01NumberOfSegments;
    }

    public void setSe01NumberOfSegments(int se01NumberOfSegments)
    {
        this.se01NumberOfSegments = se01NumberOfSegments;
    }
}
