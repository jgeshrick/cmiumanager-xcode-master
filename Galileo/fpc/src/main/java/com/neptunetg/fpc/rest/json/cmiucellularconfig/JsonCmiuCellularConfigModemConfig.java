/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.fpc.rest.json.cmiucellularconfig;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * JSON for IF39 and IF40 - see ETI 48-02
 */
public class JsonCmiuCellularConfigModemConfig
{
    @JsonProperty("vendor")
    private String vendor;

    @JsonProperty("mno")
    private String mno;

    @JsonProperty("imei")
    private String imei;

    public String getVendor()
    {
        return this.vendor;
    }

    public void setVendor(String vendor)
    {
        this.vendor = vendor;
    }

    public String getMno()
    {
        return mno;
    }

    public void setMno(String mno)
    {
        this.mno = mno;
    }

    public String getImei()
    {
        return this.imei;
    }

    public void setImei(String imei)
    {
        this.imei = imei;
    }
}
