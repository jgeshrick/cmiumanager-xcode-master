/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.data.miu.manufacturinginfo;

import java.time.Instant;

/**
 * Created by WJD1 on 04/04/2016.
 * Class to hold manufacturing test station information
 */
public class MiuManufactureInfoTestStation
{
    private int miuManufactureInfoTestStationId;
    private TestStationType type;
    private String bay;
    private String rackId;
    private String fixtureId;
    private Instant calibrationDate;
    private String labviewApplicationVersion;
    private String labviewPlatformVersion;
    private String nccVersion;
    private String osDescription;
    private String btleDongleDriverVersion;

    public int getMiuManufactureInfoTestStationId()
    {
        return miuManufactureInfoTestStationId;
    }

    public void setMiuManufactureInfoTestStationId(int miuManufactureInfoTestStationId)
    {
        this.miuManufactureInfoTestStationId = miuManufactureInfoTestStationId;
    }

    public TestStationType getType()
    {
        return type;
    }

    public void setType(TestStationType type)
    {
        this.type = type;
    }

    public String getBay()
    {
        return bay;
    }

    public void setBay(String bay)
    {
        this.bay = bay;
    }

    public String getRackId()
    {
        return rackId;
    }

    public void setRackId(String rackId)
    {
        this.rackId = rackId;
    }

    public String getFixtureId()
    {
        return fixtureId;
    }

    public void setFixtureId(String fixtureId)
    {
        this.fixtureId = fixtureId;
    }

    public Instant getCalibrationDate()
    {
        return calibrationDate;
    }

    public void setCalibrationDate(Instant calibrationDate)
    {
        this.calibrationDate = calibrationDate;
    }

    public String getLabviewApplicationVersion()
    {
        return labviewApplicationVersion;
    }

    public void setLabviewApplicationVersion(String labviewApplicationVersion)
    {
        this.labviewApplicationVersion = labviewApplicationVersion;
    }

    public String getLabviewPlatformVersion()
    {
        return labviewPlatformVersion;
    }

    public void setLabviewPlatformVersion(String labviewPlatformVersion)
    {
        this.labviewPlatformVersion = labviewPlatformVersion;
    }

    public String getNccVersion()
    {
        return nccVersion;
    }

    public void setNccVersion(String nccVersion)
    {
        this.nccVersion = nccVersion;
    }

    public String getOsDescription()
    {
        return osDescription;
    }

    public void setOsDescription(String osDescription)
    {
        this.osDescription = osDescription;
    }

    public String getBtleDongleDriverVersion()
    {
        return btleDongleDriverVersion;
    }

    public void setBtleDongleDriverVersion(String btleDongleDriverVersion)
    {
        this.btleDongleDriverVersion = btleDongleDriverVersion;
    }
}
