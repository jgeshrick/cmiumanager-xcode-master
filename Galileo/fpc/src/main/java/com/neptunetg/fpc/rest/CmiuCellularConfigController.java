/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.rest;

import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.cns.CnsException;
import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleService;
import com.neptunetg.fpc.audit.AuditService;
import com.neptunetg.fpc.cns.CellularNetworkServiceManager;
import com.neptunetg.fpc.domain.miu.MiuManagementService;
import com.neptunetg.fpc.rest.json.cmiucellularconfig.JsonCmiuCellularConfig;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.ConflictException;
import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * IF39 and IF40
 */
@RestController
public class CmiuCellularConfigController extends BaseTokenAuthenticatingRestController
{
    private static final Logger log = LoggerFactory.getLogger(CmiuCellularConfigController.class);

    @Autowired
    private MiuManagementService miuService;

    @Autowired
    private AuditService auditService;

    @Autowired
    private CellularNetworkServiceManager cellularNetworkServiceManager;

    @Autowired
    private MiuLifecycleService cmiuLifecycleService;

    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "/miu/{miuId}/cellular-config", method = RequestMethod.GET)
    @ResponseBody
    public JsonCmiuCellularConfig getCmiuCellularConfig(
                               @PathVariable(value = "miuId") MiuId miuId)
    {
        try
        {
            return getCellularConfig(miuId);
        }
        catch (MiuDataException e)
        {
            throw new MdceRestException("Error getting cellular config for " + miuId, e);
        }
    }

    private JsonCmiuCellularConfig getCellularConfig(MiuId miuId) throws MiuDataException
    {
        CellularDeviceDetail cellularDeviceDetail = this.miuService.getCellularConfig(miuId);

        return JsonCmiuCellularConfig.from(cellularDeviceDetail);
    }

    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "/miu/{miuId}/cellular-config", method = RequestMethod.POST)
    public ResponseEntity setCmiuCellularConfig(
                               @PathVariable(value = "miuId") MiuId miuId,
                               @RequestParam(value = "doRegisterWithMno", defaultValue = "true") boolean doRegisterWithMno,
                               @RequestBody JsonCmiuCellularConfig cellularConfig) throws BadRequestException, ConflictException, MdceRestException
    {
        checkMiuIdMatchesOrThrowBadRequest(miuId, cellularConfig);

        updateOrCreateCellularDeviceDetail(miuId, cellularConfig);

        //Only need to activate VZW devices
        if (cellularConfig.getModemConfig() != null)
        {
            final String mno = cellularConfig.getModemConfig().getMno();
            if(doRegisterWithMno)
            {
                if ("VZW".equals(mno) &&
                    cellularConfig.getSimConfig() != null &&
                    cellularConfig.getSimConfig().getIccid() != null)
                {
                    final String actionDescription = "Request for CMIU: "
                            + miuId.numericValue()
                            + " to have its sim "
                            + cellularConfig.getSimConfig().getIccid()
                            + " activated on the Verizon network";
                    log.debug(actionDescription);

                    CellularNetworkService verizon = cellularNetworkServiceManager.getCns("VZW");
                    try
                    {
                        verizon.activateDevice(cellularConfig.getSimConfig().getIccid(),
                                cellularConfig.getModemConfig().getImei());
                    }
                    catch (Exception e)
                    {
                        throw new MdceRestException(actionDescription + " failed", e);
                    }
                }
                else
                {
                    log.debug("Request for CMIU: " + miuId.numericValue() +
                            " registration with MNO " + mno +
                            " ignored as not applicable or due to missing data");
                }
            }
        }

        //Place even AT&T devices in the preactivated state, as it's good to force a check with the MNO that the
        //device has been activated, rather than just assuming
        cmiuLifecycleService.setMiuLifecycleState(miuId, MiuLifecycleStateEnum.PREACTIVATED);

        log.debug("Request for CMIU: "
                + miuId
                + " to be placed in the PREACTIVATED state");

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    private void updateOrCreateCellularDeviceDetail(MiuId miuId, JsonCmiuCellularConfig cellularConfig) throws BadRequestException, ConflictException
    {
        CellularDeviceDetail cellularDeviceDetail = cellularConfig.toCellularDeviceDetail();

        this.miuService.updateOrInsertMiuCellularDetails(miuId, cellularDeviceDetail);

        auditService.logCmiuCanChange(miuId, cellularDeviceDetail);
    }

    private void checkMiuIdMatchesOrThrowBadRequest(MiuId miuId, JsonCmiuCellularConfig cellularConfig) throws BadRequestException
    {
        if (!miuId.matches(cellularConfig.getMiuId()))
        {
            throw new BadRequestException("Miu id in request body does not match request path");
        }
    }
}
