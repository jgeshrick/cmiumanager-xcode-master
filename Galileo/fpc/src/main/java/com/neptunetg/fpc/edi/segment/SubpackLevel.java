/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;


import com.neptunetg.fpc.edi.EdiException;

import java.util.ArrayList;
import java.util.List;

/**
 * Kit level
 * TODO: expose fields for elements
 */
public class SubpackLevel extends X12
{
    private final String imei;
    private final String iccid;

    public SubpackLevel(String imei, String iccid)
    {
        this.imei = imei;
        this.iccid = iccid;
    }

    public String getData() throws EdiException
    {
        return getSegmentAsString(getDataSegments());
    }


    public List<Segment> getDataSegments() throws EdiException
    {
        //construct beginning segment for ship notice
        Segment hl1 = new Segment("HL");

        hl1.appendElement(new Element<Integer>("HL01", 1, 12, 5))   //sequence number of HL records
                .appendElement(new Element<Integer>("HL02", 1, 12, 2))
                .appendElement(new Element<String>("HL03", 1, 2, "Q")); //subpack level

        //subline item detail
        Segment sln = new Segment("SLN")
                .appendElement(new Element<Integer>("SLN01", 1, 20, 1)) //fixme numeric value indicating which NAM this A key is associated with
                .appendElement(new Element<String>("SLN02", null, null, ""))
                .appendElement(new Element<String>("SLN03", 1, 1, "I"))
                .appendElement(new Element<Integer>("SLN04", 1, 15, 1))
                .appendElement(new Element<String>("SLN05", 2, 2, "EA"))
                .appendElement(new Element<String>("SLN06", null, null, ""))
                .appendElement(new Element<String>("SLN07", null, null, ""))
                .appendElement(new Element<String>("SLN08", null, null, ""))
                .appendElement(new Element<String>("SLN09", 2, 2, "EQ"))
                .appendElement(new Element<String>("SLN10", 1, 48, "R")) //fixme: A key type code
                .appendElement(new Element<String>("SLN11", 2, 2, "ZZ"))
                .appendElement(new Element<String>("SLN12", 1, 48, " "))     //fixme: phone device authentication key, for 3G device only?
                .appendElement(new Element<String>("SLN13", 2, 2, "RN"))
                .appendElement(new Element<String>("SLN14", 1, 48, "20091231"))  //Fixme: device warranty date
                .appendElement(new Element<String>("SLN15", 2, 2, "SG"))
                .appendElement(new Element<String>("SLN16", 1, 48, this.iccid));   //SIM serial number/iccid

        List<Segment> segmentList = new ArrayList<>();
        segmentList.add(hl1);
        segmentList.add(sln);

        return segmentList;

    }

}
