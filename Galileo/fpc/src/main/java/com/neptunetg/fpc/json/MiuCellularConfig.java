/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.json;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Used for device pairing in Functional Test Station
 */
public class MiuCellularConfig
{
    @JsonProperty("miu_id")
    private long miuId;

    @JsonProperty("modem_config")
    private ModemConfig modemConfig;

    @JsonProperty("sim_config")
    private SimConfig simConfig;

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public ModemConfig getModemConfig()
    {
        return modemConfig;
    }

    public void setModemConfig(ModemConfig modemConfig)
    {
        this.modemConfig = modemConfig;
    }

    public SimConfig getSimConfig()
    {
        return simConfig;
    }

    public void setSimConfig(SimConfig simConfig)
    {
        this.simConfig = simConfig;
    }

    @Override
    public String toString()
    {
        return "miu id: " + miuId +
                " iccid: " + simConfig.getIccid() +
                " imei: " + modemConfig.getImei();
    }
}
