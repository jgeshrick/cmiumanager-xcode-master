/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import com.neptunetg.fpc.edi.EdiException;

import java.util.ArrayList;
import java.util.List;

/**
 * Hierarchical level 01
 */
public class ShipmentLevel extends X12
{
    public String getData() throws EdiException
    {
        return getSegmentAsString(getDataSegments());
    }

    public List<Segment> getDataSegments() throws EdiException
    {
        //construct beginning segment for ship notice
        Segment hl1 = new Segment("HL");

        hl1.appendElement(new Element<Integer>("HL01", 1, 12, 1))
                .appendElement(new Element<String>("HL02", null, null, ""))
                .appendElement(new Element<String>("HL03", 1, 12, "S"));

        //FIXME Not including N1 - N4 element as it is not included in sample EDI doc

        Segment po4 = new Segment("PO4");
        po4.appendElement(new Element<String>("PO401", null, null, ""))
            .appendElement(new Element<String>("PO402", null, null, ""))
            .appendElement(new Element<String>("PO403", null, null, ""))
            .appendElement(new Element<String>("PO404", null, null, ""))
            .appendElement(new Element<String>("PO405", 1, 2, "G"))
            .appendElement(new Element<Integer>("PO406", 1, 9, 10))  //Fixme shipment weight
            .appendElement(new Element<String>("PO407", 2, 2, "LB"));

        List<Segment> segmentList = new ArrayList<>();
        segmentList.add(hl1);
        segmentList.add(po4);

        return segmentList;
    }

}
