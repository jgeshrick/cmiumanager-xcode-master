/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.json.manufacturinginfo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by WJD1 on 04/04/2016.
 * Class to represent
 */
public class MiuManufactureInfoTestJson
{
    @JsonProperty("test_timestamp")
    private String testTimestamp;

    @JsonProperty("station_info")
    private MiuManufactureInfoTestStationJson miuManufactureInfoTestStationJson;

    @JsonProperty("log")
    private ArrayList<String> logEntries;

    public String getTestTimestamp()
    {
        return testTimestamp;
    }

    public void setTestTimestamp(String testTimestamp)
    {
        this.testTimestamp = testTimestamp;
    }

    public MiuManufactureInfoTestStationJson getMiuManufactureInfoTestStationJson()
    {
        return miuManufactureInfoTestStationJson;
    }

    public void setMiuManufactureInfoTestStationJson(MiuManufactureInfoTestStationJson miuManufactureInfoTestStationJson)
    {
        this.miuManufactureInfoTestStationJson = miuManufactureInfoTestStationJson;
    }

    public ArrayList<String> getLogEntries()
    {
        return new ArrayList<String>(logEntries);
    }

    public void setLogEntries(ArrayList<String> logEntries)
    {
        this.logEntries = logEntries;
    }
}
