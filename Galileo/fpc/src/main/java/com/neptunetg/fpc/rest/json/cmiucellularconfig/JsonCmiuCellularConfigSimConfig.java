/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.fpc.rest.json.cmiucellularconfig;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * JSON for IF39 and IF40 - see ETI 48-02
 */
public class JsonCmiuCellularConfigSimConfig
{
    @JsonProperty("iccid")
    private String iccid;

    @JsonProperty("imsi")
    private String imsi;

    @JsonProperty("msidn")
    private String msidn;

    @JsonProperty("mno")
    private String mno;

    public String getIccid()
    {
        return this.iccid;
    }

    public void setIccid(String iccid)
    {
        this.iccid = iccid;
    }

    public String getImsi()
    {
        return this.imsi;
    }

    public void setImsi(String imsi)
    {
        this.imsi = imsi;
    }

    public String getMsidn()
    {
        return this.msidn;
    }

    public void setMsidn(String msidn)
    {
        this.msidn = msidn;
    }

    public String getMno()
    {
        return mno;
    }

    public void setMno(String mno)
    {
        this.mno = mno;
    }
}
