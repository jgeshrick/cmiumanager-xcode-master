/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.json;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by WJD1 on 22/03/2016.
 * Used to store modem config information
 */
public class ModemConfig
{
    @JsonProperty("mno")
    private String mno;

    @JsonProperty("vendor")
    private String vendor;

    @JsonProperty("imei")
    private String imei;

    public String getMno()
    {
        return mno;
    }

    public void setMno(String mno)
    {
        this.mno = mno;
    }

    public String getVendor()
    {
        return vendor;
    }

    public void setVendor(String vendor)
    {
        this.vendor = vendor;
    }

    public String getImei()
    {
        return imei;
    }

    public void setImei(String imei)
    {
        this.imei = imei;
    }
}
