/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.rest;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.fpc.data.miu.manufacturinginfo.MiuManufactureInfo;
import com.neptunetg.fpc.data.miu.manufacturinginfo.MiuManufactureInfoTest;
import com.neptunetg.fpc.data.miu.manufacturinginfo.MiuManufactureInfoTestLog;
import com.neptunetg.fpc.data.miu.manufacturinginfo.MiuManufactureInfoTestStation;
import com.neptunetg.fpc.data.miu.manufacturinginfo.MiuManufacturingInfoRepository;
import com.neptunetg.fpc.data.miu.manufacturinginfo.TestStationType;
import com.neptunetg.fpc.json.manufacturinginfo.BluetoothJson;
import com.neptunetg.fpc.json.manufacturinginfo.DeviceDetailsJson;
import com.neptunetg.fpc.json.manufacturinginfo.ImageJson;
import com.neptunetg.fpc.json.manufacturinginfo.MiuManufactureInfoJson;
import com.neptunetg.fpc.json.manufacturinginfo.MiuManufactureInfoTestJson;
import com.neptunetg.fpc.json.manufacturinginfo.MiuManufactureInfoTestStationJson;
import com.neptunetg.fpc.json.manufacturinginfo.ModemJson;
import com.neptunetg.fpc.json.manufacturinginfo.ReworkJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


/**
 * Created by WJD1 on 05/04/2016.
 * IF11 and also get Todo-include IF for GET method
 */
@RestController
public class ManufacuringInformationController
{
    private static final Logger log = LoggerFactory.getLogger(ManufacuringInformationController.class);

    @Autowired
    private MiuManufacturingInfoRepository miuRepo;

    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "/miu/{miuId}/manufacturing-information", method = RequestMethod.POST)
    public ResponseEntity setManufacturingInformation(
            @PathVariable(value = "miuId") MiuId miuId,
            @RequestBody MiuManufactureInfoJson miuManufactureInfoJson)
    {
        MiuManufactureInfo miuManufactureInfo = miuManufactureInfoJsonToMiuManufactureInfo(miuManufactureInfoJson);

        if(miuManufactureInfo.getMiuId().numericValue() == miuId.numericValue())
        {
            Boolean result = miuRepo.setMiuManufactureInfo(miuManufactureInfo);
        }

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "/miu/{miuId}/manufacturing-information", method = RequestMethod.GET)
    @ResponseBody
    public MiuManufactureInfoJson getManufacturingInformation(
            @PathVariable(value = "miuId") MiuId miuId)
    {
        MiuManufactureInfo miuManufactureInfo = miuRepo.getMiuManufactureInfo(miuId);

        MiuManufactureInfoJson miuManufactureInfoJson = miuManufactureInfoToMiuManufactureInfoJson(miuManufactureInfo);

        return miuManufactureInfoJson;
    }

    private MiuManufactureInfo miuManufactureInfoJsonToMiuManufactureInfo(MiuManufactureInfoJson miuManufactureInfoJson)
    {
        MiuManufactureInfo miuManufactureInfo = new MiuManufactureInfo();
        miuManufactureInfo.setMiuId(MiuId.valueOf(miuManufactureInfoJson.getMiuId()));

        ArrayList<MiuManufactureInfoTest> miuManufactureInfoTests = new ArrayList<>();

        if(miuManufactureInfoJson.getMiuManufactureInfoTestJsons() != null)
        {
            for(MiuManufactureInfoTestJson testJson : miuManufactureInfoJson.getMiuManufactureInfoTestJsons())
            {
                MiuManufactureInfoTest miuManufactureInfoTest = new MiuManufactureInfoTest();
                miuManufactureInfoTest.setTestTimestamp(parseQueryDateToInstant(testJson.getTestTimestamp()));

                if(testJson.getMiuManufactureInfoTestStationJson() != null)
                {
                    MiuManufactureInfoTestStation miuManufactureInfoTestStation = new MiuManufactureInfoTestStation();
                    miuManufactureInfoTestStation.setType(TestStationType.fromString(testJson.getMiuManufactureInfoTestStationJson().getType()));
                    miuManufactureInfoTestStation.setBay(testJson.getMiuManufactureInfoTestStationJson().getBay());
                    miuManufactureInfoTestStation.setRackId(testJson.getMiuManufactureInfoTestStationJson().getRackId());
                    miuManufactureInfoTestStation.setFixtureId(testJson.getMiuManufactureInfoTestStationJson().getFixtureId());
                    miuManufactureInfoTestStation.setCalibrationDate(
                            parseQueryDateToInstant(testJson.getMiuManufactureInfoTestStationJson().getCalibrationDate()));
                    miuManufactureInfoTestStation.setLabviewApplicationVersion(testJson.getMiuManufactureInfoTestStationJson().getLabviewApplicationVersion());
                    miuManufactureInfoTestStation.setLabviewPlatformVersion(testJson.getMiuManufactureInfoTestStationJson().getLabviewPlatformVersion());
                    miuManufactureInfoTestStation.setNccVersion(testJson.getMiuManufactureInfoTestStationJson().getNccVersion());
                    miuManufactureInfoTestStation.setOsDescription(testJson.getMiuManufactureInfoTestStationJson().getOsDescription());
                    miuManufactureInfoTestStation.setBtleDongleDriverVersion(testJson.getMiuManufactureInfoTestStationJson().getBtleDongleDriverVersion());
                    miuManufactureInfoTest.setMiuManufactureInfoTestStation(miuManufactureInfoTestStation);
                }

                ArrayList<MiuManufactureInfoTestLog> miuManufactureInfoTestLogs = new ArrayList<>();

                if(testJson.getLogEntries() != null)
                {
                    for (String testLog : testJson.getLogEntries())
                    {
                        MiuManufactureInfoTestLog miuManufactureInfoTestLog = new MiuManufactureInfoTestLog();
                        miuManufactureInfoTestLog.setLog(testLog);

                        miuManufactureInfoTestLogs.add(miuManufactureInfoTestLog);
                    }
                }

                miuManufactureInfoTest.setMiuManufactureInfoTestLogs(miuManufactureInfoTestLogs);

                miuManufactureInfoTests.add(miuManufactureInfoTest);
            }
        }

        miuManufactureInfo.setMiuManufactureInfoTests(miuManufactureInfoTests);

        if(miuManufactureInfoJson.getRework() != null)
        {
            miuManufactureInfo.setReworkCount(miuManufactureInfoJson.getRework().getCount());
        }

        if(miuManufactureInfoJson.getDeviceDetails() != null)
        {
            miuManufactureInfo.setDeviceCreationTimestamp(parseQueryDateToInstant(miuManufactureInfoJson.getDeviceDetails().getDeviceCreationTimestamp()));
            miuManufactureInfo.setDeviceFirstTestTimestamp(parseQueryDateToInstant(miuManufactureInfoJson.getDeviceDetails().getDeviceFirstTestTimestamp()));
            miuManufactureInfo.setDeviceLastTestTimestamp(parseQueryDateToInstant(miuManufactureInfoJson.getDeviceDetails().getDeviceLastTestTimestamp()));
            miuManufactureInfo.setDeviceRegistrationTimestamp(parseQueryDateToInstant(miuManufactureInfoJson.getDeviceDetails().getDeviceRegistrationTimestamp()));
            miuManufactureInfo.setDeviceActivationTimestamp(parseQueryDateToInstant(miuManufactureInfoJson.getDeviceDetails().getDeviceActivationTimestamp()));

            if(miuManufactureInfoJson.getDeviceDetails().getImages() != null)
            {
                miuManufactureInfo.setDeviceImageModemLibrary(miuManufactureInfoJson.getDeviceDetails().getImages().getDeviceImageModemLibrary());
                miuManufactureInfo.setDeviceImageCmiuApplication(miuManufactureInfoJson.getDeviceDetails().getImages().getDeviceImageCmiuApplication());
                miuManufactureInfo.setDeviceImageCmiuBootloader(miuManufactureInfoJson.getDeviceDetails().getImages().getDeviceImageCmiuBootloader());
                miuManufactureInfo.setDeviceImageCmiuConfiguration(miuManufactureInfoJson.getDeviceDetails().getImages().getDeviceImageCmiuConfiguration());
            }

            if(miuManufactureInfoJson.getDeviceDetails().getModem() != null)
            {
                miuManufactureInfo.setDeviceModemFirmwareRevision(miuManufactureInfoJson.getDeviceDetails().getModem().getDeviceModemFirmwareRevision());
            }

            if(miuManufactureInfoJson.getDeviceDetails().getBluetooth() != null)
            {
                miuManufactureInfo.setDeviceBluetoothMacAddress(miuManufactureInfoJson.getDeviceDetails().getBluetooth().getDeviceBluetoothMacAddress());
            }
        }

        return miuManufactureInfo;
    }

    private MiuManufactureInfoJson miuManufactureInfoToMiuManufactureInfoJson(MiuManufactureInfo miuManufactureInfo)
    {
        MiuManufactureInfoJson miuManufactureInfoJson = new MiuManufactureInfoJson();
        miuManufactureInfoJson.setMiuId(miuManufactureInfo.getMiuId().numericValue());

        ArrayList<MiuManufactureInfoTestJson> miuManufactureInfoTestsJson = new ArrayList<>();

        if(miuManufactureInfo.getMiuManufactureInfoTests() != null)
        {
            for (MiuManufactureInfoTest test : miuManufactureInfo.getMiuManufactureInfoTests())
            {
                MiuManufactureInfoTestJson miuManufactureInfoTestJson = new MiuManufactureInfoTestJson();
                miuManufactureInfoTestJson.setTestTimestamp(formatInstantToDate(test.getTestTimestamp()));

                if (test.getMiuManufactureInfoTestStation() != null)
                {
                    MiuManufactureInfoTestStationJson miuManufactureInfoTestStationJson = new MiuManufactureInfoTestStationJson();
                    miuManufactureInfoTestStationJson.setType(returnNullIfTypeNullOtherwiseSqlString(test.getMiuManufactureInfoTestStation().getType()));
                    miuManufactureInfoTestStationJson.setBay(test.getMiuManufactureInfoTestStation().getBay());
                    miuManufactureInfoTestStationJson.setRackId(test.getMiuManufactureInfoTestStation().getRackId());
                    miuManufactureInfoTestStationJson.setFixtureId(test.getMiuManufactureInfoTestStation().getFixtureId());
                    miuManufactureInfoTestStationJson.setCalibrationDate(formatInstantToDate(test.getMiuManufactureInfoTestStation().getCalibrationDate()));
                    miuManufactureInfoTestStationJson.setLabviewApplicationVersion(test.getMiuManufactureInfoTestStation().getLabviewApplicationVersion());
                    miuManufactureInfoTestStationJson.setLabviewPlatformVersion(test.getMiuManufactureInfoTestStation().getLabviewPlatformVersion());
                    miuManufactureInfoTestStationJson.setNccVersion(test.getMiuManufactureInfoTestStation().getNccVersion());
                    miuManufactureInfoTestStationJson.setOsDescription(test.getMiuManufactureInfoTestStation().getOsDescription());
                    miuManufactureInfoTestStationJson.setBtleDongleDriverVersion(test.getMiuManufactureInfoTestStation().getBtleDongleDriverVersion());
                    miuManufactureInfoTestJson.setMiuManufactureInfoTestStationJson(miuManufactureInfoTestStationJson);
                }

                ArrayList<String> logEntrys = new ArrayList<>();

                if(test.getMiuManufactureInfoTestLogs() != null)
                {
                    for (MiuManufactureInfoTestLog log : test.getMiuManufactureInfoTestLogs())
                    {
                        logEntrys.add(log.getLog());
                    }
                }

                miuManufactureInfoTestJson.setLogEntries(logEntrys);

                miuManufactureInfoTestsJson.add(miuManufactureInfoTestJson);
            }
        }

        miuManufactureInfoJson.setMiuManufactureInfoTestJsons(miuManufactureInfoTestsJson);

        ReworkJson reworkJson = new ReworkJson();
        reworkJson.setCount(miuManufactureInfo.getReworkCount());
        miuManufactureInfoJson.setRework(reworkJson);

        DeviceDetailsJson deviceDetailsJson = new DeviceDetailsJson();
        deviceDetailsJson.setDeviceCreationTimestamp(formatInstantToDate(miuManufactureInfo.getDeviceCreationTimestamp()));
        deviceDetailsJson.setDeviceFirstTestTimestamp(formatInstantToDate(miuManufactureInfo.getDeviceFirstTestTimestamp()));
        deviceDetailsJson.setDeviceLastTestTimestamp(formatInstantToDate(miuManufactureInfo.getDeviceLastTestTimestamp()));
        deviceDetailsJson.setDeviceRegistrationTimestamp(formatInstantToDate(miuManufactureInfo.getDeviceRegistrationTimestamp()));
        deviceDetailsJson.setDeviceActivationTimestamp(formatInstantToDate(miuManufactureInfo.getDeviceActivationTimestamp()));

        ImageJson imageJson = new ImageJson();
        imageJson.setDeviceImageModemLibrary(miuManufactureInfo.getDeviceImageModemLibrary());
        imageJson.setDeviceImageCmiuApplication(miuManufactureInfo.getDeviceImageCmiuApplication());
        imageJson.setDeviceImageCmiuBootloader(miuManufactureInfo.getDeviceImageCmiuBootloader());
        imageJson.setDeviceImageCmiuConfiguration(miuManufactureInfo.getDeviceImageCmiuConfiguration());

        ModemJson modemJson = new ModemJson();
        modemJson.setDeviceModemFirmwareRevision(miuManufactureInfo.getDeviceModemFirmwareRevision());

        BluetoothJson bluetoothJson = new BluetoothJson();
        bluetoothJson.setDeviceBluetoothMacAddress(miuManufactureInfo.getDeviceBluetoothMacAddress());

        deviceDetailsJson.setImages(imageJson);
        deviceDetailsJson.setModem(modemJson);
        deviceDetailsJson.setBluetooth(bluetoothJson);

        miuManufactureInfoJson.setDeviceDetails(deviceDetailsJson);

        return miuManufactureInfoJson;
    }

    private Instant parseQueryDateToInstant(String dt)
    {
        if(dt != null)
        {
            return ZonedDateTime.parse(dt, DateTimeFormatter.ISO_DATE_TIME).toInstant();
        }

        return null;
    }

    private String formatInstantToDate(Instant instant)
    {
        if(instant != null)
        {
            DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.systemDefault());
            return formatter.format(instant);
        }

        return null;
    }

    private String returnNullIfTypeNullOtherwiseSqlString(TestStationType testStationType)
    {
        if(testStationType == null)
        {
            return null;
        }

        return testStationType.getSqlString();
    }
}


















