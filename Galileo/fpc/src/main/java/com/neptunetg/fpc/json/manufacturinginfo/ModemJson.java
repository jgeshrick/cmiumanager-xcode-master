/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.json.manufacturinginfo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by WJD1 on 05/04/2016.
 * Class to represent modem information in JSON
 */
public class ModemJson
{
    @JsonProperty("firmware_revision")
    private String deviceModemFirmwareRevision;

    public String getDeviceModemFirmwareRevision()
    {
        return deviceModemFirmwareRevision;
    }

    public void setDeviceModemFirmwareRevision(String deviceModemFirmwareRevision)
    {
        this.deviceModemFirmwareRevision = deviceModemFirmwareRevision;
    }
}
