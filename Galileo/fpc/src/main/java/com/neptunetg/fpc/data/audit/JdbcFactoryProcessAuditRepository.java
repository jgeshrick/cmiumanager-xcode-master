/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.data.audit;

import com.neptunetg.fpc.json.MiuCellularConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * JDBC implementation of the audit log repo for FPC
 */
@Repository
public class JdbcFactoryProcessAuditRepository implements FactoryProcessAuditRepository
{
    //FPC related audit events
    private static final int AUDIT_EVENT_TYPE_FPC_REGISTER_DEVICE = 15;

    private final JdbcTemplate db;


    @Autowired
    public JdbcFactoryProcessAuditRepository(JdbcTemplate db)
    {
        this.db = db;
    }

    @Override
    public void logDeviceRegistration(MiuCellularConfig miuCellularConfig, boolean requestSubmittedToCNS)
    {
        final String insertAuditSql = "INSERT INTO mdce.audit_list " +
                "(audit_time, audit_type, miu_id, old_values, new_values) VALUES (NOW(),?,?,?,?)";

        this.db.update(insertAuditSql, AUDIT_EVENT_TYPE_FPC_REGISTER_DEVICE, miuCellularConfig.getMiuId(),
                "", miuCellularConfig.toString() + " Request submitted: " + requestSubmittedToCNS);
    }



}
