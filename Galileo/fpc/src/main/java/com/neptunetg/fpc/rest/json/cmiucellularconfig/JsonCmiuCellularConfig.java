/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.fpc.rest.json.cmiucellularconfig;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;

import java.time.Instant;

/**
 * JSON for IF39 and IF40 - see ETI 48-02
 */
public class JsonCmiuCellularConfig
{
    @JsonProperty("miu_id")
    private long miuId;

    @JsonProperty("apn")
    private String apn;

    @JsonProperty("modem_config")
    private JsonCmiuCellularConfigModemConfig modemConfig = new JsonCmiuCellularConfigModemConfig();

    @JsonProperty("sim_config")
    private JsonCmiuCellularConfigSimConfig simConfig = new JsonCmiuCellularConfigSimConfig();

    /**
     * Construct a JSON representation class from RDB model of cellular device details
     *
     * @param cellularDeviceDetail
     * @return
     */
    public static JsonCmiuCellularConfig from(CellularDeviceDetail cellularDeviceDetail)
    {
        JsonCmiuCellularConfig cellularConfig = new JsonCmiuCellularConfig();

        //todo add code that gets miu details from the database
        cellularConfig.setMiuId(cellularDeviceDetail.getMiuId().numericValue());
        cellularConfig.setApn(cellularDeviceDetail.getApn());

        cellularConfig.modemConfig.setImei(cellularDeviceDetail.getModemConfig().getImei());
        cellularConfig.modemConfig.setVendor(cellularDeviceDetail.getModemConfig().getVendor());
        cellularConfig.modemConfig.setMno(cellularDeviceDetail.getModemConfig().getMno());

        cellularConfig.simConfig.setIccid(cellularDeviceDetail.getIccid());
        cellularConfig.simConfig.setImsi(cellularDeviceDetail.getImsi());
        cellularConfig.simConfig.setMsidn(cellularDeviceDetail.getMsisdn());
        cellularConfig.simConfig.setMno(cellularDeviceDetail.getSimMno());

        return cellularConfig;
    }


    public CellularDeviceDetail toCellularDeviceDetail()
    {
        final CellularDeviceDetail cellularDeviceDetail = new CellularDeviceDetail();

        cellularDeviceDetail.setMiuId(MiuId.valueOf(getMiuId()));
        cellularDeviceDetail.setApn(getApn());

        if(simConfig != null)
        {
            cellularDeviceDetail.setIccid(simConfig.getIccid());
            cellularDeviceDetail.setImsi(simConfig.getImsi());
            cellularDeviceDetail.setMsisdn(simConfig.getMsidn());
            cellularDeviceDetail.setSimMno(simConfig.getMno());
        }

        if(modemConfig != null)
        {
            cellularDeviceDetail.getModemConfig().setVendor(modemConfig.getVendor());
            cellularDeviceDetail.getModemConfig().setFirmwareRevision(null); //FPC is not sent this information.  Previously: cellularConfig.modemConfig.getFirmwareRevision());
            cellularDeviceDetail.getModemConfig().setMno(modemConfig.getMno());
            cellularDeviceDetail.getModemConfig().setImei(modemConfig.getImei());
        }
        else
        {
            cellularDeviceDetail.getModemConfig().setVendor(null);
            cellularDeviceDetail.getModemConfig().setFirmwareRevision(null); //FPC is not sent this information.  Previously: cellularConfig.modemConfig.getFirmwareRevision());
            cellularDeviceDetail.getModemConfig().setMno(null);
            cellularDeviceDetail.getModemConfig().setImei(null);
        }

        cellularDeviceDetail.setLastUpdateTime(Instant.now());

        return cellularDeviceDetail;
    }

    public long getMiuId()
    {
        return this.miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public String getApn()
    {
        return apn;
    }

    public void setApn(String apn)
    {
        this.apn = apn;
    }

    public JsonCmiuCellularConfigModemConfig getModemConfig()
    {
        return modemConfig;
    }

    public void setModemConfig(JsonCmiuCellularConfigModemConfig modemConfig)
    {
        this.modemConfig = modemConfig;
    }

    public JsonCmiuCellularConfigSimConfig getSimConfig()
    {
        return simConfig;
    }

    public void setSimConfig(JsonCmiuCellularConfigSimConfig simConfig)
    {
        this.simConfig = simConfig;
    }
}
