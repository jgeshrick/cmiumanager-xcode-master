/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import com.neptunetg.fpc.edi.EdiException;

import java.util.ArrayList;
import java.util.List;

/**
 * ItemLevel TODO expose fields for element data
 */
public class ItemLevel extends X12
{
    private final static String VZW_PART_NUMBER = "VZW080000230002";
    private String universalProductCode = "123-12345-12345";    //fixme - probably ignored since verizon is not particular in the value of this field
    private String deviceSoftwareVersion = "1.0.0.0";           //fixme - probably ignored since verizon is not particular in the value of this field

    public String getData() throws EdiException
    {
        return getSegmentAsString(getDataSegments());
    }


    public List<Segment> getDataSegments() throws EdiException
    {
        //construct beginning segment for ship notice
        Segment hl1 = new Segment("HL");

        hl1.appendElement(new Element<Integer>("HL01", 1, 12, 3))   //sequence number of HL records
                .appendElement(new Element<Integer>("HL02", 1, 12, 2))
                .appendElement(new Element<String>("HL03", 1, 2, "I")); //item level

        //reference to Item identification
        Segment lin = new Segment("LIN")
                .appendElement(new Element<Integer>("LIN01", 1, 20, 1))   //fixme always 1??
                .appendElement(new Element<String>("LIN02", 2, 2, "CB"))
                .appendElement(new Element<String>("LIN03", 1, 48, VZW_PART_NUMBER))
                .appendElement(new Element<String>("LIN04", 2, 2, "MN"))    //model number
                .appendElement(new Element<String>("LIN05", 1, 48, VZW_PART_NUMBER))
                .appendElement(new Element<String>("LIN06", 2, 2, "UI"))
                .appendElement(new Element<String>("LIN07", 1, 48, universalProductCode))
                .appendElement(new Element<String>("LIN08", 2, 2, "L1"))
                .appendElement(new Element<String>("LIN09", 1, 48, deviceSoftwareVersion));

        //item detail
        Segment sn1 = new Segment("SN1")
                .appendElement(new Element<Integer>("SN101", 1, 20, 1))
                .appendElement(new Element<Integer>("SN102", 1, 10, 1))
                .appendElement(new Element<String>("SN103", 2, 2, "EA"));

        // reference identification, todo, might need more line for this??
        Segment ref = new Segment("REF")
                .appendElement(new Element<String>("REF01", 2, 3, "06"))
                .appendElement(new Element<String>("REF02", 1, 30, "A08"));   //fixme need to find out the air interface indicator

        List<Segment> segmentList = new ArrayList<>();
        segmentList.add(hl1);
        segmentList.add(lin);
        segmentList.add(sn1);
        segmentList.add(ref);

        return segmentList;
    }

    public String getUniversalProductCode()
    {
        return universalProductCode;
    }

    public void setUniversalProductCode(String universalProductCode)
    {
        this.universalProductCode = universalProductCode;
    }

    public String getDeviceSoftwareVersion()
    {
        return deviceSoftwareVersion;
    }

    public void setDeviceSoftwareVersion(String deviceSoftwareVersion)
    {
        this.deviceSoftwareVersion = deviceSoftwareVersion;
    }
}
