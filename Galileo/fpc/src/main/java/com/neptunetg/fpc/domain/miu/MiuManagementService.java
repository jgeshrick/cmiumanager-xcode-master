/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 *
 */

package com.neptunetg.fpc.domain.miu;


import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.ConflictException;

/**
 * Service for managing MIUs
 */
public interface MiuManagementService
{
    /**
     * Create or update a CMIU's cellular details
     * @param miuId The MIU ID
     * @param cellularDeviceDetail The cellular details
     */
    void updateOrInsertMiuCellularDetails(MiuId miuId, CellularDeviceDetail cellularDeviceDetail) throws BadRequestException, ConflictException;

    /**
     * Get miu cellular details
     * @param miuId the miu id to retrieve the result
     * @return miu cellular details
     */
    CellularDeviceDetail getCellularConfig(MiuId miuId) throws MiuDataException;

}
