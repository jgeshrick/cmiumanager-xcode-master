/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.json;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Return application information associated to this server
 */
public class ApplicationInformation
{
    @JsonProperty("version")
    private String version;

    @JsonProperty("environment")
    private String environment;

    @JsonProperty("server")
    private String server;

    @JsonProperty("current_time_utc")
    private String currentTimeUtc;

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getEnvironment()
    {
        return environment;
    }

    public void setEnvironment(String environment)
    {
        this.environment = environment;
    }

    public String getServer()
    {
        return server;
    }

    public void setServer(String server)
    {
        this.server = server;
    }

    public String getCurrentTimeUtc()
    {
        return currentTimeUtc;
    }

    public void setCurrentTimeUtc(String currentTimeUtc)
    {
        this.currentTimeUtc = currentTimeUtc;
    }
}
