/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.data.miu.manufacturinginfo;

/**
 * Created by WJD1 on 04/04/2016.
 * Enum to hold the type of test station used during manufacture
 */
public enum TestStationType
{
    FTS("fts"),
    CVS("cvs");

    private final String sqlString;

    private TestStationType(String sqlEnumString)
    {
        this.sqlString = sqlEnumString;
    }

    public static TestStationType fromString(String sqlEnumString)
    {
        if(sqlEnumString == null)
        {
            return null;
        }

        for(TestStationType type : TestStationType.values())
        {
            if(sqlEnumString.equals(type.getSqlString()))
            {
                return type;
            }
        }

        return null;
    }

    public String getSqlString()
    {
        return this.sqlString;
    }

}
