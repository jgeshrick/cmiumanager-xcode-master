/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.data.apn.model;

/**
 * Enum representing a Cellular provider
 */
public enum CellularProviderEnum
{
    ATnT("ATT"),
    VERIZON("VZW");

    private final String sqlString;

    private CellularProviderEnum(String sqlEnumString)
    {
        this.sqlString = sqlEnumString;
    }

    public static CellularProviderEnum fromStringValue(String sqlEnumString)
    {
        for(CellularProviderEnum provider : CellularProviderEnum.values())
        {
            if(sqlEnumString.equals(provider.getStringValue()))
            {
                return provider;
            }
        }

        return null;
    }

    public String getStringValue()
    {
        return this.sqlString;
    }

}
