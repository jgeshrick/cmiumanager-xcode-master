/*
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 */

package com.neptunetg.fpc.rest;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleService;
import com.neptunetg.fpc.json.lifecycle.CmiuLifecycleStateJson;
import com.neptunetg.fpc.json.lifecycle.StateJson;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.ForbiddenException;
import com.neptunetg.mdce.common.api.rest.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * IF19
 */
@RestController
public class CmiuLifecycleStateController
{
    private static final Logger log = LoggerFactory.getLogger(CmiuLifecycleStateController.class);

    @Autowired
    private MiuLifecycleService miuLifecycleService;

    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "/miu/{miuId}/lifecycle-state", method = RequestMethod.GET)
    @ResponseBody
    public CmiuLifecycleStateJson getCmiuLifecycleState(
                               @PathVariable(value = "miuId") MiuId miuId)
    {
        MiuLifecycleState cmiuLifecycleState = miuLifecycleService.getMiuLifecycleState(miuId);

        if(cmiuLifecycleState == null)
        {
            throw new NotFoundException("MIU:" + miuId.numericValue() + " not found");
        }

        return cmiuLifecycleStateToCmiuLifecycleStateJson(cmiuLifecycleState);
    }

    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "/miu/{miuId}/lifecycle-state", method = RequestMethod.POST)
    public ResponseEntity setCmiuLifecycleState(
                               @PathVariable(value = "miuId") MiuId miuId,
                               @RequestBody CmiuLifecycleStateJson cmiuLifecycleStateJson) throws BadRequestException, ForbiddenException
    {
        MiuLifecycleState cmiuLifecycleState = cmiuLifecycleStateJsonToCmiuLifecycleState(cmiuLifecycleStateJson);

        if(cmiuLifecycleState.getMiuId().equals(miuId))
        {
            if(cmiuLifecycleState.getLifecycleState() == MiuLifecycleStateEnum.PREPREPOT ||
                    cmiuLifecycleState.getLifecycleState() == MiuLifecycleStateEnum.PREPOT ||
                    cmiuLifecycleState.getLifecycleState() == MiuLifecycleStateEnum.POSTPOT)
            {
                if(!miuLifecycleService.setMiuLifecycleState(miuId, cmiuLifecycleState.getLifecycleState()))
                {
                    throw new ForbiddenException("Can't change to that state from previous state");
                }
            }
            else
            {
                throw new ForbiddenException("Forbidden state");
            }
        }
        else
        {
            throw new BadRequestException("MIU IDs do not match");
        }

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    private CmiuLifecycleStateJson cmiuLifecycleStateToCmiuLifecycleStateJson(MiuLifecycleState cmiuLifecycleState)
    {
        CmiuLifecycleStateJson cmiuLifecycleStateJson = new CmiuLifecycleStateJson();
        cmiuLifecycleStateJson.setMiuId(cmiuLifecycleState.getMiuId().numericValue());

        StateJson stateJson = new StateJson();
        stateJson.setState(cmiuLifecycleState.getLifecycleState().getStringValue());
        stateJson.setTimestamp(formatInstantToDate(cmiuLifecycleState.getTransitionInstant()));

        cmiuLifecycleStateJson.setState(stateJson);

        return cmiuLifecycleStateJson;
    }

    private MiuLifecycleState cmiuLifecycleStateJsonToCmiuLifecycleState(CmiuLifecycleStateJson cmiuLifecycleStateJson)
    {
        MiuLifecycleState cmiuLifecycleState = new MiuLifecycleState();
        cmiuLifecycleState.setMiuId(MiuId.valueOf(cmiuLifecycleStateJson.getMiuId()));
        cmiuLifecycleState.setLifecycleState(MiuLifecycleStateEnum.fromStringValue(
                cmiuLifecycleStateJson.getState().getState()));
        cmiuLifecycleState.setTransitionInstant(Instant.now());

        return cmiuLifecycleState;
    }


    private String formatInstantToDate(Instant instant)
    {
        if(instant != null)
        {
            DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.systemDefault());
            return formatter.format(instant);
        }

        return null;
    }
}
