/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import com.neptunetg.fpc.edi.EdiException;

import java.util.ArrayList;
import java.util.List;

/**
 * Kit level
 * TODO: expose fields for elements
 */
public class KitLevel extends X12
{
    private final String imei;
    private final String iccid;

    public KitLevel(String imei, String iccid)
    {
        this.imei = imei;
        this.iccid = iccid;
    }

    public String getData() throws EdiException
    {
        return getSegmentAsString(getDataSegments());
    }

    public List<Segment> getDataSegments() throws EdiException
    {
        //construct beginning segment for ship notice
        Segment hl1 = new Segment("HL");

        hl1.appendElement(new Element<Integer>("HL01", 1, 12, 4))   //sequence number of HL records
                .appendElement(new Element<Integer>("HL02", 1, 12, 2))
                .appendElement(new Element<String>("HL03", 1, 2, "K")); //kit level

        //item detail
        Segment sn1 = new Segment("SN1")
                .appendElement(new Element<Integer>("SN101", 1, 20, 1)) //fixme the line number of the order being shipped
                .appendElement(new Element<Integer>("SN102", 1, 10, 1)) //fixme - A-key quantity
                .appendElement(new Element<String>("SN103", 2, 2, "EA"));


        //subline item detail
        Segment sln = new Segment("SLN")
                .appendElement(new Element<Integer>("SLN01", 1, 20, 1)) //fixme order line number
                .appendElement(new Element<String>("SLN02", null, null, ""))
                .appendElement(new Element<String>("SLN03", 1, 1, "I"))
                .appendElement(new Element<Integer>("SLN04", 1, 15, 1))
                .appendElement(new Element<String>("SLN05", 2, 2, "EA"))
                .appendElement(new Element<String>("SLN06", 1, 17, "0.00")) //fixme price per unit
                .appendElement(new Element<String>("SLN07", null, null, ""))
                .appendElement(new Element<String>("SLN08", null, null, ""))
                .appendElement(new Element<String>("SLN09", null, null, ""))    //bar coded serial number, blank for LTE devices
                .appendElement(new Element<String>("SLN10", null, null, ""))    //hex IMEI, not for LTE, blank for LTE devices
                .appendElement(new Element<String>("SLN11", null, null, ""))
                .appendElement(new Element<String>("SLN12", null, null, ""))
                .appendElement(new Element<String>("SLN13", 2, 2, "CI"))
                .appendElement(new Element<String>("SLN14", 1, 48, "12345678901"))  //MSN or PIN number
                .appendElement(new Element<String>("SLN15", 2, 2, "IS"))
                .appendElement(new Element<String>("SLN16", 1, 48, this.imei))   //IMEI
                .appendElement(new Element<String>("SLN17", 2, 2, "TW"))
                .appendElement(new Element<String>("SLN18", 1, 48, "12345678")) //fixme: 8/16 digit unlock code for embedded SIM card
                .appendElement(new Element<String>("SLN19", 2, 2, "ZR"))
                .appendElement(new Element<String>("SLN20", 1, 48, "12345678")) //fixme: 8/16 digit unlock code for embedded SIM card
                .appendElement(new Element<String>("SLN21", 2, 2, "C3"))
                .appendElement(new Element<String>("SLN22", 1, 48, "WW")) //fixme: phone/device warranty code
                .appendElement(new Element<String>("SLN23", 2, 2, "PQ"))
                .appendElement(new Element<String>("SLN24", 1, 48, "RB")) //fixme: MEID status code
                .appendElement(new Element<String>("SLN25", 2, 2, "SQ"))
                .appendElement(new Element<String>("SLN26", 1, 48, "00")) //fixme: legacy hex ESN
                .appendElement(new Element<String>("SLN27", 2, 2, "SR"))
                .appendElement(new Element<String>("SLN28", 1, 48, "00")); //fixme: legacy decimal ESN

        List<Segment> segmentList = new ArrayList<>();
        segmentList.add(hl1);
        segmentList.add(sn1);
        segmentList.add(sln);

        return segmentList;

    }

}
