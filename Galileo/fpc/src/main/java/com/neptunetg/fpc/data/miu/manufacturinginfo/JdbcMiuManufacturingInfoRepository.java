/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015, 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.data.miu.manufacturinginfo;

import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.mdce.common.api.rest.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static com.neptunetg.common.data.util.JdbcDateUtil.getInstant;

/**
 * MIU Repository for MySql database
 */
@Repository
public class JdbcMiuManufacturingInfoRepository implements MiuManufacturingInfoRepository
{
    // String Constants
    private static final String MIU_ID_CONST = "miu_id";
    private static final String SELECT_SITE_ID_FROM_MDCE_MIU_DETAILS_WHERE_MIU_ID = "SELECT site_id FROM mdce.miu_details WHERE miu_id = ?";
    // fields
    private final JdbcTemplate db;
    private final MdceIpcPublishService ipc;

    @Autowired
    public JdbcMiuManufacturingInfoRepository(JdbcTemplate db, MdceIpcPublishService ipc)
    {
        this.db = db;
        this.ipc = ipc;
    }

    @Override
    @Transactional //TODO: move @Transactional to service layer
    public Boolean setMiuManufactureInfo(MiuManufactureInfo miuManufactureInfo)
    {
        if(!checkMiuExists(miuManufactureInfo.getMiuId()))
        {
            throw new NotFoundException("MIU does not exist");
        }

        //Insert or overwrite manufacturing information for the MIU
        int miuManufactureInfoPk = insertOrUpdateStaticManufacturingInfo(miuManufactureInfo);

        //Add tests to database
        insertTestDetails(miuManufactureInfo, miuManufactureInfoPk);

        return true;
    }


    @Override
    public MiuManufactureInfo getMiuManufactureInfo(MiuId miuId)
    {
        if(!checkMiuExists(miuId))
        {
            throw new NotFoundException("MIU does not exist");
        }

        MiuManufactureInfo miuManufactureInfo = getStaticManufacturingInfo(miuId);

        miuManufactureInfo = getTestDetails(miuManufactureInfo);

        return miuManufactureInfo;
    }

    private boolean checkMiuExists(MiuId miuId)
    {
        String sql = "SELECT COUNT(*) FROM mdce.miu_details WHERE miu_id = ?";

        if(this.db.queryForObject(sql, this::countRowMapper, miuId.numericValue()) == 1)
        {
            return true;
        }

        return false;
    }

    private int countRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        return rs.getInt("COUNT(*)");
    }

    @Transactional
    private int insertOrUpdateStaticManufacturingInfo(MiuManufactureInfo miuManufactureInfo)
    {
        String sql = "INSERT INTO mdce.miu_manufacture_info (miu_id, rework_count, device_creation_timestamp, " +
                "device_first_test_timestamp, device_last_test_timestamp, device_registration_timestamp, " +
                "device_activation_timestamp, device_image_modem_library, device_image_cmiu_application, " +
                "device_image_cmiu_bootloader, device_image_cmiu_configuration, device_modem_firmware_revision, " +
                "device_bluetooth_mac_address) VALUES (?,?,FROM_UNIXTIME(?),FROM_UNIXTIME(?),FROM_UNIXTIME(?)," +
                "FROM_UNIXTIME(?),FROM_UNIXTIME(?),?,?,?,?,?,?) " +
                "ON DUPLICATE KEY UPDATE " +
                "rework_count = IFNULL(?, rework_count), " +
                "device_creation_timestamp = IFNULL(FROM_UNIXTIME(?), device_creation_timestamp), " +
                "device_first_test_timestamp = IFNULL(FROM_UNIXTIME(?), device_first_test_timestamp), " +
                "device_last_test_timestamp = IFNULL(FROM_UNIXTIME(?), device_last_test_timestamp), " +
                "device_registration_timestamp = IFNULL(FROM_UNIXTIME(?), device_registration_timestamp), " +
                "device_activation_timestamp = IFNULL(FROM_UNIXTIME(?), device_activation_timestamp), " +
                "device_image_modem_library = IFNULL(?, device_image_modem_library), " +
                "device_image_cmiu_application = IFNULL(?, device_image_cmiu_application), " +
                "device_image_cmiu_bootloader = IFNULL(?, device_image_cmiu_bootloader), " +
                "device_image_cmiu_configuration = IFNULL(?, device_image_cmiu_configuration), " +
                "device_modem_firmware_revision = IFNULL(?, device_modem_firmware_revision), " +
                "device_bluetooth_mac_address = IFNULL(?, device_bluetooth_mac_address)";

        this.db.update(sql,
                miuManufactureInfo.getMiuId().numericValue(),
                miuManufactureInfo.getReworkCount(),
                returnNullIfInstantNullOtherwiseEpoch(miuManufactureInfo.getDeviceCreationTimestamp()),
                returnNullIfInstantNullOtherwiseEpoch(miuManufactureInfo.getDeviceFirstTestTimestamp()),
                returnNullIfInstantNullOtherwiseEpoch(miuManufactureInfo.getDeviceLastTestTimestamp()),
                returnNullIfInstantNullOtherwiseEpoch(miuManufactureInfo.getDeviceRegistrationTimestamp()),
                returnNullIfInstantNullOtherwiseEpoch(miuManufactureInfo.getDeviceActivationTimestamp()),
                miuManufactureInfo.getDeviceImageModemLibrary(),
                miuManufactureInfo.getDeviceImageCmiuApplication(),
                miuManufactureInfo.getDeviceImageCmiuBootloader(),
                miuManufactureInfo.getDeviceImageCmiuConfiguration(),
                miuManufactureInfo.getDeviceModemFirmwareRevision(),
                miuManufactureInfo.getDeviceBluetoothMacAddress(),
                miuManufactureInfo.getReworkCount(),
                returnNullIfInstantNullOtherwiseEpoch(miuManufactureInfo.getDeviceCreationTimestamp()),
                returnNullIfInstantNullOtherwiseEpoch(miuManufactureInfo.getDeviceFirstTestTimestamp()),
                returnNullIfInstantNullOtherwiseEpoch(miuManufactureInfo.getDeviceLastTestTimestamp()),
                returnNullIfInstantNullOtherwiseEpoch(miuManufactureInfo.getDeviceRegistrationTimestamp()),
                returnNullIfInstantNullOtherwiseEpoch(miuManufactureInfo.getDeviceActivationTimestamp()),
                miuManufactureInfo.getDeviceImageModemLibrary(),
                miuManufactureInfo.getDeviceImageCmiuApplication(),
                miuManufactureInfo.getDeviceImageCmiuBootloader(),
                miuManufactureInfo.getDeviceImageCmiuConfiguration(),
                miuManufactureInfo.getDeviceModemFirmwareRevision(),
                miuManufactureInfo.getDeviceBluetoothMacAddress());

        sql = "SELECT miu_manufacture_info_id FROM mdce.miu_manufacture_info WHERE miu_id=?";

        return this.db.queryForObject(sql,
                this::getManufactureInfoIdFromMiuManufactureInfoRowMapper,
                miuManufactureInfo.getMiuId().numericValue());
    }

    private Long returnNullIfInstantNullOtherwiseEpoch(Instant instant)
    {
        if(instant != null)
        {
            return instant.getEpochSecond();
        }

        return null;
    }

    private String returnNullIfTypeNullOtherwiseSqlString(TestStationType testStationType)
    {
        if(testStationType == null)
        {
            return null;
        }

        return testStationType.getSqlString();
    }

    @Transactional
    private boolean insertTestDetails(MiuManufactureInfo miuManufactureInfo, int miuManufactureInfoPk)
    {
        for(MiuManufactureInfoTest test : miuManufactureInfo.getMiuManufactureInfoTests())
        {
            int testStationPk = processTestStationAndReturnPk(test.getMiuManufactureInfoTestStation());

            String sql = "INSERT INTO mdce.miu_manufacture_info_tests (miu_manufacture_info_id, " +
                    "miu_manufacture_info_test_station_id, test_timestamp) VALUES (?, ?, FROM_UNIXTIME(?))";

            this.db.update(sql,
                    miuManufactureInfoPk,
                    testStationPk,
                    test.getTestTimestamp().getEpochSecond());

            sql = "SELECT LAST_INSERT_ID()";

            int testPk = this.db.queryForObject(sql, this::lastInsertIdRowMapper);

            for(MiuManufactureInfoTestLog log : test.getMiuManufactureInfoTestLogs())
            {
                sql = "INSERT INTO mdce.miu_manufacture_info_tests_log (miu_manufacture_info_test_id, " +
                        "log) VALUES (?, ?)";

                this.db.update(sql,
                        testPk,
                        log.getLog());
            }
        }

        return true;
    }

    private MiuManufactureInfo getStaticManufacturingInfo(MiuId miuId)
    {
        String sql = "SELECT * FROM mdce.miu_manufacture_info WHERE miu_id=?";

        return this.db.queryForObject(sql, this::miuManufactureInfoRowMapper, miuId.numericValue());
    }

    private MiuManufactureInfo getTestDetails(MiuManufactureInfo miuManufactureInfo)
    {
        String sql = "SELECT * FROM mdce.miu_manufacture_info_tests WHERE miu_manufacture_info_id = ?";

        List<MiuManufactureInfoTest> miuManufactureInfoTests = this.db.query(sql,
                this::miuManufactureInfoTestsRowMapper,
                miuManufactureInfo.getMiuManufactureInfoId());

        miuManufactureInfo.setMiuManufactureInfoTests(new ArrayList<>(miuManufactureInfoTests));

        for(MiuManufactureInfoTest test : miuManufactureInfo.getMiuManufactureInfoTests())
        {
            sql = "SELECT * FROM mdce.miu_manufacture_info_test_station WHERE miu_manufacture_info_test_station_id = ?";

            test.setMiuManufactureInfoTestStation(
                    this.db.queryForObject(sql,
                            this::miuManufactureInfoTestStationRowMapper,
                            test.getTestStationId()));

            sql = "SELECT * FROM mdce.miu_manufacture_info_tests_log WHERE miu_manufacture_info_test_id = ?";

            List<MiuManufactureInfoTestLog> miuManufactureInfoTestLogs =
                    this.db.query(sql,
                            this::miuManufactureInfoTestsLogRowMapper,
                            test.getTestId());

            test.setMiuManufactureInfoTestLogs(new ArrayList<>(miuManufactureInfoTestLogs));
        }

        return miuManufactureInfo;
    }

    @Transactional
    private Integer processTestStationAndReturnPk(MiuManufactureInfoTestStation miuManufactureInfoTestStation)
    {
        //If no test station information is provided, then return null
        if(miuManufactureInfoTestStation == null)
        {
            return null;
        }

        //Check to see if an identical test station exists in SQL, if if does, return the PK
        String sql = "SELECT * FROM mdce.miu_manufacture_info_test_station WHERE " +
                "type=? AND bay=? AND rack_id=? AND fixture_id=? AND calibration_date=FROM_UNIXTIME(?) " +
                "AND labview_application_version=? AND labview_platform_version=? " +
                "AND ncc_version=? AND os_description=? AND btle_dongle_driver_version=?";

        MiuManufactureInfoTestStation existingTestStation = null;

        try
        {
            existingTestStation = this.db.queryForObject(sql,
                    this::miuManufactureInfoTestStationRowMapper,
                    miuManufactureInfoTestStation.getType(),
                    miuManufactureInfoTestStation.getBay(),
                    miuManufactureInfoTestStation.getRackId(),
                    miuManufactureInfoTestStation.getFixtureId(),
                    returnNullIfInstantNullOtherwiseEpoch(miuManufactureInfoTestStation.getCalibrationDate()),
                    miuManufactureInfoTestStation.getLabviewApplicationVersion(),
                    miuManufactureInfoTestStation.getLabviewPlatformVersion(),
                    miuManufactureInfoTestStation.getNccVersion(),
                    miuManufactureInfoTestStation.getOsDescription(),
                    miuManufactureInfoTestStation.getBtleDongleDriverVersion()
            );
        }
        catch(EmptyResultDataAccessException e)
        {
            //Record does not exist, do nothing here
        }

        if(existingTestStation != null)
        {
            return existingTestStation.getMiuManufactureInfoTestStationId();
        }

        //Since the test station details do not exist, add them, and then return the primary key
        sql = "INSERT INTO mdce.miu_manufacture_info_test_station (type, bay, rack_id, fixture_id, calibration_date, " +
                "labview_application_version, labview_platform_version, ncc_version, os_description, " +
                "btle_dongle_driver_version) VALUES (?, ?, ?, ?, FROM_UNIXTIME(?), ?, ?, ?, ?, ?)";

        this.db.update(sql,
                returnNullIfTypeNullOtherwiseSqlString(miuManufactureInfoTestStation.getType()),
                miuManufactureInfoTestStation.getBay(),
                miuManufactureInfoTestStation.getRackId(),
                miuManufactureInfoTestStation.getFixtureId(),
                returnNullIfInstantNullOtherwiseEpoch(miuManufactureInfoTestStation.getCalibrationDate()),
                miuManufactureInfoTestStation.getLabviewApplicationVersion(),
                miuManufactureInfoTestStation.getLabviewPlatformVersion(),
                miuManufactureInfoTestStation.getNccVersion(),
                miuManufactureInfoTestStation.getOsDescription(),
                miuManufactureInfoTestStation.getBtleDongleDriverVersion());

        sql = "SELECT LAST_INSERT_ID()";

        return this.db.queryForObject(sql, this::lastInsertIdRowMapper);
    }

    private int lastInsertIdRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        return rs.getInt("LAST_INSERT_ID()");
    }

    private int getManufactureInfoIdFromMiuManufactureInfoRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        return rs.getInt("miu_manufacture_info_id");
    }

    private MiuManufactureInfoTestStation miuManufactureInfoTestStationRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        MiuManufactureInfoTestStation miuManufactureInfoTestStation = new MiuManufactureInfoTestStation();
        miuManufactureInfoTestStation.setMiuManufactureInfoTestStationId(rs.getInt("miu_manufacture_info_test_station_id"));
        miuManufactureInfoTestStation.setType(TestStationType.fromString(rs.getString("type")));
        miuManufactureInfoTestStation.setBay(rs.getString("bay"));
        miuManufactureInfoTestStation.setRackId(rs.getString("rack_id"));
        miuManufactureInfoTestStation.setFixtureId(rs.getString("fixture_id"));
        miuManufactureInfoTestStation.setCalibrationDate(getInstant(rs, "calibration_date"));
        miuManufactureInfoTestStation.setLabviewApplicationVersion(rs.getString("labview_application_version"));
        miuManufactureInfoTestStation.setLabviewPlatformVersion(rs.getString("labview_platform_version"));
        miuManufactureInfoTestStation.setNccVersion(rs.getString("ncc_version"));
        miuManufactureInfoTestStation.setOsDescription(rs.getString("os_description"));
        miuManufactureInfoTestStation.setBtleDongleDriverVersion(rs.getString("btle_dongle_driver_version"));

        return miuManufactureInfoTestStation;
    }

    private MiuManufactureInfo miuManufactureInfoRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        MiuManufactureInfo miuManufactureInfo = new MiuManufactureInfo();
        miuManufactureInfo.setMiuId(MiuId.valueOf(rs.getInt("miu_id")));
        miuManufactureInfo.setMiuManufactureInfoId(rs.getInt("miu_manufacture_info_id"));
        miuManufactureInfo.setReworkCount(rs.getInt("rework_count"));
        miuManufactureInfo.setDeviceCreationTimestamp(getInstant(rs, "device_creation_timestamp"));
        miuManufactureInfo.setDeviceFirstTestTimestamp(getInstant(rs, "device_first_test_timestamp"));
        miuManufactureInfo.setDeviceLastTestTimestamp(getInstant(rs, "device_last_test_timestamp"));
        miuManufactureInfo.setDeviceRegistrationTimestamp(getInstant(rs, "device_registration_timestamp"));
        miuManufactureInfo.setDeviceActivationTimestamp(getInstant(rs, "device_activation_timestamp"));
        miuManufactureInfo.setDeviceImageModemLibrary(rs.getString("device_image_modem_library"));
        miuManufactureInfo.setDeviceImageCmiuApplication(rs.getString("device_image_cmiu_application"));
        miuManufactureInfo.setDeviceImageCmiuBootloader(rs.getString("device_image_cmiu_bootloader"));
        miuManufactureInfo.setDeviceImageCmiuConfiguration(rs.getString("device_image_cmiu_configuration"));
        miuManufactureInfo.setDeviceModemFirmwareRevision(rs.getString("device_modem_firmware_revision"));
        miuManufactureInfo.setDeviceBluetoothMacAddress(rs.getString("device_bluetooth_mac_address"));

        return miuManufactureInfo;
    }

    private MiuManufactureInfoTest miuManufactureInfoTestsRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        MiuManufactureInfoTest miuManufactureInfoTest = new MiuManufactureInfoTest();
        miuManufactureInfoTest.setTestId(rs.getInt("miu_manufacture_info_test_id"));
        miuManufactureInfoTest.setTestStationId(rs.getInt("miu_manufacture_info_test_station_id"));
        miuManufactureInfoTest.setTestTimestamp(getInstant(rs, "test_timestamp"));

        return miuManufactureInfoTest;
    }

    private MiuManufactureInfoTestLog miuManufactureInfoTestsLogRowMapper(ResultSet rs, int rownumber) throws SQLException
    {
        MiuManufactureInfoTestLog miuManufactureInfoTestLog = new MiuManufactureInfoTestLog();
        miuManufactureInfoTestLog.setLog(rs.getString("log"));

        return miuManufactureInfoTestLog;
    }

}
