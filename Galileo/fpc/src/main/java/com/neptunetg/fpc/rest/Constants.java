/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.rest;

/**
 * Created by sml1 on 19/02/2016.
 */
public final class Constants
{
    public final static String FPC_REST_URL_BASE = "/api/v1";

    private Constants(){}
}
