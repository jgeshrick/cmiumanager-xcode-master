/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */
package com.neptunetg.fpc.data.apn;

import com.neptunetg.fpc.data.apn.model.CellularProviderEnum;

/**
 * Created by WJD1 on 08/09/2016.
 * Interface for getting APN information
 */
public interface MobileApnRepo
{
    public String getApnForCellularProvider(CellularProviderEnum provider);

    public CellularProviderEnum getCellularProviderFromApn(String apn);
}
