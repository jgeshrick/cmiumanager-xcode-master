/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.rest;

import com.neptunetg.mdce.common.api.domain.RestClientId;
import com.neptunetg.mdce.common.api.domain.auth.TokenManagerService;
import com.neptunetg.mdce.common.api.rest.RestUtils;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

public abstract class BaseTokenAuthenticatingRestController
{

    @Autowired
    private TokenManagerService tokenManagerService;

    protected final void validateToken(String token, HttpServletRequest request) throws NotAuthorizedException
    {
        final String originatingIp = RestUtils.getOriginatingIp(request);
        final RestClientId authenticatedClient = tokenManagerService.checkToken(token, originatingIp);
        if (!authenticatedClient.isFpc())
        {
            throw new NotAuthorizedException("Authenticated, but not as FPC");
        }
    }

}
