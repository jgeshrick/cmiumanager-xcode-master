/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.mdce.common.api.data.TokenInfo;

/**
 * JSON object for authenticating and generating Token for access to MDCE REST api.
 */
public class GenerateTokenResponse
{

    private final TokenInfo token;

    public GenerateTokenResponse(TokenInfo token)
    {
        this.token = token;
    }

    @JsonProperty("authenticated")
    public boolean isAuthenticated()
    {
        return token != null;
    }

    @JsonProperty("auth_token")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getAuthToken()
    {
        return token != null ? token.getTokenString() : null;
    }


    @JsonProperty("authenticated_at")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getAuthenticatedAt()
    {
        return token != null ? token.getCreated().toString() : null;
    }

    @JsonProperty("expires")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getExpiryDate()
    {
        return token != null ? token.getExpiry().toString() : null;
    }


}
