/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;


import com.neptunetg.fpc.edi.EdiException;

/**
 * Generic EDI data structure for building elements for a segment
 */
public class Element<T>
{
    public static final String ELEMENT_SEPARATOR = "*";

    private final Integer minElementLength;
    private final Integer maxElementLength;
    private final String id;
    private T value;

    public Element(String id, T value) throws EdiException
    {
        this(id, null, null, value);
    }

    public Element (String id, Integer minLength, Integer maxLength)
    {
        this.id = id;
        this.minElementLength = minLength;
        this.maxElementLength = maxLength;
    }

    public Element (String id, Integer minLength, Integer maxLength, T value) throws EdiException
    {
        this.id = id;
        this.minElementLength = minLength;
        this.maxElementLength = maxLength;

        setValue(value);
    }


    public T getValue()
    {
        return value;
    }

    public void setValue(T value) throws EdiException
    {
        assert(isValidType(value));

        this.value = value;

        int length = String.valueOf(value).length();

        if (maxElementLength != null && length > maxElementLength)
        {
            throw new EdiException("Element value " + value + " exceed maximum element length " + maxElementLength);
        }

    }

    @Override
    public String toString()
    {
        final String formatter = "%"
                + getFlag()
                + ((this.minElementLength != null)? this.minElementLength:"")   //flag & width
                + getConversion();

        return String.format(formatter, value);
    }

    private boolean isValidType(T value)
    {
        final String typeName = value.getClass().getTypeName();

        return (isString(value) || isInteger(value));
    }

    private String getConversion()
    {
        return isInteger(value)? "d":"s";
    }

    private String getFlag()
    {
        return isInteger(value)?
                "0":   //prefix number with 0
                ((this.minElementLength != null && this.minElementLength > 0)?"-":"")     //left align string if min element length is defined
                ;
    }

    private static boolean isInteger(Object obj)
    {
        return obj instanceof Integer;
    }

    private static boolean isString(Object obj)
    {
        return obj instanceof String;
    }



}
