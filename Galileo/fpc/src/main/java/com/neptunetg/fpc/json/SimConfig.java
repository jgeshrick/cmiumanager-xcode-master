/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.json;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by WJD1 on 22/03/2016.
 * Used to store SIM config information
 */
public class SimConfig
{
    @JsonProperty("mno")
    private String mno;

    @JsonProperty("iccid")
    private String iccid;

    @JsonProperty("imsi")
    private String imsi;

    public String getMno()
    {
        return mno;
    }

    public void setMno(String mno)
    {
        this.mno = mno;
    }

    public String getIccid()
    {
        return iccid;
    }

    public void setIccid(String iccid)
    {
        this.iccid = iccid;
    }

    public String getImsi()
    {
        return imsi;
    }

    public void setImsi(String imsi)
    {
        this.imsi = imsi;
    }
}
