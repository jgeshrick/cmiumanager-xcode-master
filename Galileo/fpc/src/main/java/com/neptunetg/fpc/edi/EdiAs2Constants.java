/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Properties;

/**
 * Contains EDI and AS2 constants for generating EDI 856 document. Some elements are different depending on
 * the AS2 server (development or production), EDI usage mode (Test or Production)
 */
@Service
public final class EdiAs2Constants
{
    public static final String NETWORK_ID = "ZZ";
    public static final String QUALIFIER = "ZZ";
    public static final String TRADING_PARTNER_ID = NETWORK_ID;

    /* Verizon EDI qualifier is 01.
       Verizon EDI ID is 113073472VZW (VERIZONTEST for development server) and the EDI group ID ( GS segment ) is VZW4GLTE .
    */
    public static final String VERIZON_EDI_QUALIFIER = "01";

    private static final String VERIZON_EDI_ID_PRODUCTION  ="113073472VZW";   //production server
    private static final String VERIZON_EDI_ID_TEST ="VERIZONTEST";      //test server

    public static final String VERIZON_EDI_GROUP_ID  ="VZW4GLTE";

    private static final String ISA15_USAGE_INDICATOR_TEST = "T"; //test
    private static final String ISA15_USAGE_INDICATOR_PRODUCTION = "P"; //production
    private static final String VZW_AS2_PRODUCTION_SERVER_URL = "http://vzwas2edi.verizonwireless.com:9080/ipnet/as2";

    private final String verizonAs2ServerUrl;
    private final String as2SenderId;
    private final boolean isEdiTest;
    private final String verizonAs2KeyAlias;
    private final String neptuneAs2KeyAlias;


    @Autowired
    public EdiAs2Constants(@Qualifier("envProperties") Properties envProperties)
    {
        //default to VZW AS2 production server
        this.verizonAs2ServerUrl = envProperties.getProperty("verizon.as2.server.url", VZW_AS2_PRODUCTION_SERVER_URL);

        this.as2SenderId = envProperties.getProperty("verizon.as2.neptune.sender.id", "NEPTUNE");

        this.isEdiTest = Boolean.valueOf(envProperties.getProperty("verizon.as2.test.mode", "false"));

        this.verizonAs2KeyAlias = envProperties.getProperty("verizon.as2.key.alias", "verizon-wireless-as2-prod");
        this.neptuneAs2KeyAlias = envProperties.getProperty("neptune.as2.key.alias", "neptune-as2-prod");
    }

    public String getVerizonAs2ServerUrl()
    {
        return verizonAs2ServerUrl;
    }

    public String getAs2SenderId()
    {
        return as2SenderId;
    }

    public String getEdiIsa15UsageIndicator()
    {
        return isEdiTest? ISA15_USAGE_INDICATOR_TEST:ISA15_USAGE_INDICATOR_PRODUCTION;
    }

    /**
     * For ISA 08
     * @return EDI id
     */
    public String getIsa08EdiId()
    {
        return isEdiTest? VERIZON_EDI_ID_TEST:VERIZON_EDI_ID_PRODUCTION;
    }

    public String getVerizonAs2KeyAlias()
    {
        return verizonAs2KeyAlias;
    }

    public String getNeptuneAs2KeyAlias()
    {
        return neptuneAs2KeyAlias;
    }
}
