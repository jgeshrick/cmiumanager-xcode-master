/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import com.neptunetg.fpc.edi.EdiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Functional group header for X12 EDI 856
 */
public class FunctionalGroup extends X12
{
    private static final Logger log = LoggerFactory.getLogger(FunctionalGroup.class);

    private static final String GS_FORMAT = "GS*SH*%s*%s*%s*%s*%s*X*004010" + SEGMENT_SEPARATOR;
    private static final String GE_FORMAT = "GE*%d*%d" + SEGMENT_SEPARATOR;

    private final String gs02SenderTestId;  //todo find out from verizon what is this id
    private final String gs03ReceiverTestId;    //todo find out from verizon what is this id
    private Instant dataInterchangeDateTime;
    private int gs06DataInterchangeControlNumber;    //todo: what is this number

    public FunctionalGroup(String senderTestId, String receiverTestId)
    {
        this.gs02SenderTestId = senderTestId;
        this.gs03ReceiverTestId = receiverTestId;
    }
    public String getHeader() throws EdiException
    {
        return getSegmentAsString(getHeaderSegments());
    }

    public List<Segment> getHeaderSegments() throws EdiException
    {

        Segment header = new Segment("GS");

        header
            .appendElement(new Element<String>("GS01", 2, 2, "SH"))
            .appendElement(new Element<String>("GS02", 2, 15, gs02SenderTestId))
            .appendElement(new Element<String>("GS03", 2, 15, gs03ReceiverTestId))
            .appendElement(new Element<String>("GS04", 8, 8, dateFormatterYYYYMMDD.format(dataInterchangeDateTime)))
            .appendElement(new Element<String>("GS05", 4, 8, timeFormatter.format(dataInterchangeDateTime)))
            .appendElement(new Element<Integer>("GS06", 1, 9, gs06DataInterchangeControlNumber))
            .appendElement(new Element<String>("GS07", 1, 2, "X"))
            .appendElement(new Element<String>("GS08", 1, 12, "004010"));

        List<Segment> segmentList = new ArrayList<>();
        segmentList.add(header);

        return segmentList;
    }

    public String getTrailer() throws EdiException
    {
        return getSegmentAsString(getTrailerSegments());
    }

    public List<Segment> getTrailerSegments() throws EdiException
    {
        Segment trailer = new Segment("GE");

        trailer
            .appendElement(new Element<Integer>("GE01", 1, 6, 1)) //fixme: total number of transaction sets included in the functional group or interchange group terminated by the trailer containing this data element
            .appendElement(new Element<Integer>("GE02", 1, 9, gs06DataInterchangeControlNumber));

        List<Segment> segmentList = new ArrayList<>();
        segmentList.add(trailer);

        return segmentList;
    }

    public Instant getDataInterchangeDateTime()
    {
        return dataInterchangeDateTime;
    }

    public void setDataInterchangeDateTime(Instant dataInterchangeDateTime)
    {
        this.dataInterchangeDateTime = dataInterchangeDateTime;
    }

    public int getGs06DataInterchangeControlNumber()
    {
        return gs06DataInterchangeControlNumber;
    }

    public void setGs06DataInterchangeControlNumber(int gs06DataInterchangeControlNumber)
    {
        this.gs06DataInterchangeControlNumber = gs06DataInterchangeControlNumber;
    }
}
