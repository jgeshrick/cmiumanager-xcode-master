/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.data.miu.manufacturinginfo;


import com.neptunetg.common.data.miu.MiuId;

public interface MiuManufacturingInfoRepository
{
    /**
     * Set Miu Manufacturing information
     * @param miuManufactureInfo the manufacturing information
     * @return success or failure
     */
    Boolean setMiuManufactureInfo(MiuManufactureInfo miuManufactureInfo);

    /**
     * Get Miu Manufacturing information
     * @return the manufacturing information
     */
    MiuManufactureInfo getMiuManufactureInfo(MiuId miuId);

}