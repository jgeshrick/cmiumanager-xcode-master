/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.json.cmiuLifecycleState;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by WJD1 on 06/04/2016.
 * Class to represent CMIU state in JSON
 */
public class StateJson
{
    @JsonProperty("id")
    String state;

    @JsonProperty("timestamp")
    String timestamp;

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(String timestamp)
    {
        this.timestamp = timestamp;
    }
}
