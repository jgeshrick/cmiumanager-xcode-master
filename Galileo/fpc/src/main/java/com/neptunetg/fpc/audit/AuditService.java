/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.audit;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.fpc.data.audit.AuditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Exposes services related to auditing to other beans related.
 */
@Service
public class AuditService
{
    @Autowired
    private AuditRepository auditRepository;

    public void logCmiuCanChange(MiuId miuId, CellularDeviceDetail cellularDeviceDetail)
    {
        auditRepository.logCmiuCanChange(miuId, cellularDeviceDetail);
    }
}
