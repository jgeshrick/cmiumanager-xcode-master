/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2017 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.fpc.rest.json.miuloraconfig;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * JSON for setting LoRa App Key
 */
public class JsonMiuLoraAppKey
{
    //database ID for device (L900) to pair with the app key
    @JsonProperty("lora_device_details_id")
    private long loraDeviceDetailsId;

    @JsonProperty("app_key")
    private String appKey;

    public long getLoraDeviceDetailsId()
    {
        return this.loraDeviceDetailsId;
    }

    public void setLoraDeviceDetailsId(long loraDeviceDetailsId)
    {
        this.loraDeviceDetailsId = loraDeviceDetailsId;
    }

    public String getAppKey()
    {
        return appKey;
    }

    public void setAppKey(String appKey)
    {
        this.appKey = appKey;
    }
}

