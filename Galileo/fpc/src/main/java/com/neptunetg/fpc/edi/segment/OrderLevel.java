/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import com.neptunetg.fpc.edi.EdiException;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sml1 on 18/01/2016.
 */
public class OrderLevel extends X12
{
    private String prf01PurchaseOrder;
    private Instant prf04PODate;
    private int vendorOrderNumber;
    private int departmentNumber;
    private int internalVendorNumber;
    private int customerReferenceNumber;

    public OrderLevel (String purchaseOrder)
    {
        prf01PurchaseOrder = purchaseOrder;
    }

    public String getData() throws EdiException
    {
        return getSegmentAsString(getDataSegments());
    }

    public List<Segment> getDataSegments() throws EdiException
    {
        //construct beginning segment for ship notice
        Segment hl1 = new Segment("HL");

        hl1.appendElement(new Element<Integer>("HL01", 1, 12, 2))
                .appendElement(new Element<Integer>("HL02", 1, 12, 1))
                .appendElement(new Element<String>("HL03", 1, 2, "O"));

        //FIXME Not including N1 - N4 element as it is not included in sample EDI doc

        //reference to the specific purchase order
        Segment prf = new Segment("PRF");
        prf.appendElement(new Element<String>("PRF01", 1, 22, prf01PurchaseOrder))   //customer purchase order number
                .appendElement(new Element<String>("PRF02", null, null, ""))
                .appendElement(new Element<String>("PRF03", null, null, ""))
                .appendElement(new Element<String>("PRF04", 8, 8, dateFormatterYYYYMMDD.format(prf04PODate)));

        //reference identification
        Segment ref1 = new Segment("REF")
                .appendElement(new Element<String>("REF01", 2, 3, "VN"))
                .appendElement(new Element<Integer>("REF02", 1, 30, vendorOrderNumber));

        Segment ref2 = new Segment("REF")
                .appendElement(new Element<String>("REF01", 2, 3, "DP"))
                .appendElement(new Element<Integer>("REF02", 1, 30, departmentNumber));

        Segment ref3 = new Segment("REF")
                .appendElement(new Element<String>("REF01", 2, 3, "IA"))
                .appendElement(new Element<Integer>("REF02", 1, 30, internalVendorNumber));

        Segment ref4 = new Segment("REF")
                .appendElement(new Element<String>("REF01", 2, 3, "CR"))
                .appendElement(new Element<Integer>("REF02", 1, 30, customerReferenceNumber));

        List<Segment> segmentList = new ArrayList<>();
        segmentList.add(hl1);
        segmentList.add(prf);
        segmentList.add(ref1);
        segmentList.add(ref2);
        segmentList.add(ref3);
        segmentList.add(ref4);

        return segmentList;
    }


    public String getPrf01PurchaseOrder()
    {
        return prf01PurchaseOrder;
    }

    public void setPrf01PurchaseOrder(String prf01PurchaseOrder)
    {
        this.prf01PurchaseOrder = prf01PurchaseOrder;
    }

    public Instant getPrf04PODate()
    {
        return prf04PODate;
    }

    public void setPrf04PODate(Instant prf04PODate)
    {
        this.prf04PODate = prf04PODate;
    }

    public int getVendorOrderNumber()
    {
        return vendorOrderNumber;
    }

    public void setVendorOrderNumber(int vendorOrderNumber)
    {
        this.vendorOrderNumber = vendorOrderNumber;
    }

    public int getDepartmentNumber()
    {
        return departmentNumber;
    }

    public void setDepartmentNumber(int departmentNumber)
    {
        this.departmentNumber = departmentNumber;
    }

    public int getInternalVendorNumber()
    {
        return internalVendorNumber;
    }

    public void setInternalVendorNumber(int internalVendorNumber)
    {
        this.internalVendorNumber = internalVendorNumber;
    }

    public int getCustomerReferenceNumber()
    {
        return customerReferenceNumber;
    }

    public void setCustomerReferenceNumber(int customerReferenceNumber)
    {
        this.customerReferenceNumber = customerReferenceNumber;
    }
}
