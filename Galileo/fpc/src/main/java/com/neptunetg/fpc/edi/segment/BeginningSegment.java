/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import com.neptunetg.fpc.edi.EdiException;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Beginning Segment: BSN, DTM
 */
public class BeginningSegment extends X12
{
    private Instant dtm02ShippedDate;   //the date the shipment left the manufacturer's dock

    public String getData() throws EdiException
    {
        return getSegmentAsString(getDataSegments());
    }

    public List<Segment> getDataSegments() throws EdiException
    {
        //construct beginning segment for ship notice
        Segment beginningSegment = new Segment("BSN");

        Instant transactionDateTime = Instant.now();

        beginningSegment
                .appendElement(new Element<String>("BSN01", 2, 2, "00"))
                .appendElement(new Element<String>("BSN02", 2, 30, "1234567890"))   //Shipment identification
                .appendElement(new Element<String>("BSN03", 8, 8, dateFormatterYYYYMMDD.format(transactionDateTime)))   //Shipment date
                .appendElement(new Element<String>("BSN04", 4, 8, timeFormatter.format(transactionDateTime)))   //Shipment time
                .appendElement(new Element<String>("BSN05", 4, 4, "0002"));   //FIXME is this always this value?

        //construct DTM
        Segment dtm1 = new Segment("DTM");
        dtm1.appendElement(new Element<String>("DTM01", 3, 3, "011"))   //shipped date
                .appendElement(new Element<String>("DTM02", 8, 8, dateFormatterYYYYMMDD.format(dtm02ShippedDate)));

        Segment dtm2 = new Segment("DTM");
        dtm2.appendElement(new Element<String>("DTM01", 3, 3, "068"))   //current schedule ship date
                .appendElement(new Element<String>("DTM02", 8, 8, dateFormatterYYYYMMDD.format(dtm02ShippedDate)));

        List<Segment> segmentList = new ArrayList<>();
        segmentList.add(beginningSegment);
        segmentList.add(dtm1);
        segmentList.add(dtm2);

        return segmentList;

    }

    public Instant getDtm02ShippedDate()
    {
        return dtm02ShippedDate;
    }

    public void setDtm02ShippedDate(Instant dtm02ShippedDate)
    {
        this.dtm02ShippedDate = dtm02ShippedDate;
    }
}
