/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * X12 is a messaging standard developed by the American National Standards Institute (ANSI)..
 * Defines a X12 document and provide global constants for any X12 documents.
 * An X12 EDI document contains the following mandatory segments:
 ISA Segment
 GS Segment
 ST Segment
 SE Segment
 GE Segment
 IEA Segment
 https://msdn.microsoft.com/en-us/library/cc973915(v=bts.10).aspx
 */
public abstract class X12
{
    final static public String SEGMENT_SEPARATOR = "~";
    final static public DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyMMdd").withZone(ZoneId.of("UTC"));
    final static public DateTimeFormatter dateFormatterYYYYMMDD = DateTimeFormatter.ofPattern("yyyyMMdd").withZone(ZoneId.of("UTC"));
    final static public DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HHmm").withZone(ZoneId.of("UTC"));

    final static public String LINE_SEPARATOR = "\r\n"; //CRLF

    protected static String getSegmentAsString(List<Segment> segments)
    {
        return segments.stream().map(Segment::toString).collect(Collectors.joining(LINE_SEPARATOR));
    }

}
