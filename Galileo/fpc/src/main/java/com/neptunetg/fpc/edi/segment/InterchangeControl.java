/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import com.neptunetg.fpc.edi.EdiException;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * ASC X12 standards for exchange specific data between two or more trading partners.
 * This defines the interchange control header, for release version 4010
 */
public class InterchangeControl extends X12
{
    final private static String ISA_FORMAT =
            "ISA*00*          *00*          *%-2s*%-15s*%-2s*%-15s*%s*%s*U*00401*%9s*0*%1s*>" + SEGMENT_SEPARATOR;
    final private static String IEA_FORMAT = "IEA*%d*%9s" + SEGMENT_SEPARATOR;

    private String isa05SenderIdQualifier;    //2 character
    private String isa06SenderId;   //15 character
    private String isa07ReceiverIdQualifier;    //2 character
    private String isa08ReceiverId;   //15 character
    private Instant isaDateTimeOfExchange; //6 character, YYMMDD
    private int isa13ControlNumber;  //9 character, a control number assigned by the interchange sender
    private String isa15UsageIndicator; //1 char, T: test, P: Production, I: Information


    public String getHeader() throws EdiException
    {
        return getSegmentAsString(getHeaderSegments());
    }

    public List<Segment> getHeaderSegments() throws EdiException
    {
        Segment header = new Segment("ISA");

        header
            .appendElement(new Element<String>("ISA01", 2, 2, "00"))
            .appendElement(new Element<String>("ISA02", 10, 10, "          "))
            .appendElement(new Element<String>("ISA03", 2, 2, "00"))
            .appendElement(new Element<String>("ISA04", 10, 10, "          "))
            .appendElement(new Element<String>("ISA05", 2, 2, isa05SenderIdQualifier))
            .appendElement(new Element<String>("ISA06", 15, 15, isa06SenderId))
            .appendElement(new Element<String>("ISA07", 2, 2, isa07ReceiverIdQualifier))
            .appendElement(new Element<String>("ISA08", 15, 15, isa08ReceiverId))
            .appendElement(new Element<String>("ISA09", 6, 6, dateFormatter.format(isaDateTimeOfExchange)))
                .appendElement(new Element<String>("ISA10", 4, 4, timeFormatter.format(isaDateTimeOfExchange)))
                .appendElement(new Element<String>("ISA11", 1, 1, "U"))
                .appendElement(new Element<String>("ISA12", 5, 5, "00401"))
                .appendElement(new Element<Integer>("ISA13", 9, 9, isa13ControlNumber))
                .appendElement(new Element<Integer>("ISA14", 1, 1, 0))
                .appendElement(new Element<String>("ISA15", 1, 1, isa15UsageIndicator))
                .appendElement(new Element<String>("ISA16", 1, 1, ">")); //component element separator

        List<Segment> segmentList = new ArrayList<>();
        segmentList.add(header);

        return segmentList;
    }

    public String getTrailer() throws EdiException
    {
        return getSegmentAsString(getTrailerSegments());
    }


    public List<Segment> getTrailerSegments() throws EdiException
    {
        Segment trailer = new Segment("IEA");

        trailer
                .appendElement(new Element<Integer>("IEA01", 1, 5, 1))
                .appendElement(new Element<Integer>("IEA02", 9, 9, isa13ControlNumber));

        List<Segment> segmentList = new ArrayList<>();
        segmentList.add(trailer);

        return segmentList;

    }

    public String getIsa05SenderIdQualifier()
    {
        return isa05SenderIdQualifier;
    }

    public void setIsa05SenderIdQualifier(String isa05SenderIdQualifier)
    {
        this.isa05SenderIdQualifier = isa05SenderIdQualifier;
    }

    public String getIsa06SenderId()
    {
        return isa06SenderId;
    }

    public void setIsa06SenderId(String isa06SenderId)
    {
        this.isa06SenderId = isa06SenderId;
    }

    public String getIsa07ReceiverIdQualifier()
    {
        return isa07ReceiverIdQualifier;
    }

    public void setIsa07ReceiverIdQualifier(String isa07ReceiverIdQualifier)
    {
        this.isa07ReceiverIdQualifier = isa07ReceiverIdQualifier;
    }

    public String getIsa08ReceiverId()
    {
        return isa08ReceiverId;
    }

    public void setIsa08ReceiverId(String isa08ReceiverId)
    {
        this.isa08ReceiverId = isa08ReceiverId;
    }

    public Instant getIsaDateTimeOfExchange()
    {
        return isaDateTimeOfExchange;
    }

    public void setIsaDateTimeOfExchange(Instant isaDateTimeOfExchange)
    {
        this.isaDateTimeOfExchange = isaDateTimeOfExchange;
    }

    public int getIsa13ControlNumber()
    {
        return isa13ControlNumber;
    }

    public void setIsa13ControlNumber(int isa13ControlNumber)
    {
        this.isa13ControlNumber = isa13ControlNumber;
    }

    public String getIsa15UsageIndicator()
    {
        return isa15UsageIndicator;
    }

    public void setIsa15UsageIndicator(String isa15UsageIndicator)
    {
        this.isa15UsageIndicator = isa15UsageIndicator;
    }
}
