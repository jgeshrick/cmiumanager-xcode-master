/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.fpc.rest.json.cmiuapn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by WJD1 on 14/06/2016.
 * A class to represent JSON for a mno / apn pairing
 */
public class JsonCmiuApn
{
    @JsonProperty("apn")
    String apn;

    @JsonProperty("mno")
    String mno;

    public String getApn()
    {
        return apn;
    }

    public void setApn(String apn)
    {
        this.apn = apn;
    }

    public String getMno()
    {
        return mno;
    }

    public void setMno(String mno)
    {
        this.mno = mno;
    }
}
