/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.rest;

import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.cns.CnsException;
import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.fpc.cns.CellularNetworkServiceManager;
import com.neptunetg.fpc.domain.miu.MiuManagementService;
import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Manage the device lifecycle - activation, deactivation
 */
@RestController
@RequestMapping(Constants.FPC_REST_URL_BASE)
public class DeviceLifecycleManagementController
{
    private static final Logger logger = LoggerFactory.getLogger(DeviceLifecycleManagementController.class);

    @Autowired
    private MiuManagementService miuManagementService;

    @Autowired
    private CellularNetworkServiceManager cellularNetworkServiceManager;


    /**
     * Activate a cellular device and enable it to establish data connection
     * @param miuId miu id
     * @return http 200 and OK
     * @throws MdceRestException
     */
    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "miu/{miuId}/activate", method = RequestMethod.POST)
    public String activateCellularDevice(@PathVariable(value = "miuId") int miuId) throws MdceRestException
    {
        try
        {
            final CellularDeviceDetail cellularDeviceDetail = miuManagementService.getCellularConfig(MiuId.valueOf(miuId));
            final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(cellularDeviceDetail);
            final String iccid = cellularDeviceDetail.getIccid();
            final String imei = cellularDeviceDetail.getModemConfig().getImei();

            cellularNetworkService.activateDevice(iccid, imei);
        }
        catch (Exception e)
        {
            logger.error("Error activating CMIU " + miuId, e);

            throw new MdceRestException("Exception while activating CMIU " + miuId, e);  //rethrow
        }

        return "OK";
    }

    /**
     * Deactivate a cellular device, which prevents it from establishing a data connection
     * @param miuId miu id
     * @return http 200 and OK
     * @throws MdceRestException
     */
    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "/miu/{miuId}/deactivate", method = RequestMethod.POST)
    public String deactivateCellularDevice(@PathVariable(value = "miuId") long miuId) throws MdceRestException
    {
        try
        {
            final CellularDeviceDetail cellularDeviceDetail = miuManagementService.getCellularConfig(MiuId.valueOf(miuId));
            final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(cellularDeviceDetail);
            final String iccid = cellularDeviceDetail.getIccid();
            cellularNetworkService.deactivateDevice(iccid);
        }
        catch (Exception e)
        {
            logger.error("Error deactivating CMIU " + miuId, e);

            throw new MdceRestException("Exception while deactivating CMIU " + miuId, e);  //rethrow
        }

        return "OK";
    }

}
