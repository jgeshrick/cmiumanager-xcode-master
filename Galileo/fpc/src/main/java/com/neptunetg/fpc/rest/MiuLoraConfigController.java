/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.rest;

import com.neptunetg.common.data.miu.Eui;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lora.LoraDeviceDetail;
import com.neptunetg.common.data.miu.lora.LoraRepository;
import com.neptunetg.fpc.rest.json.miuloraconfig.JsonMiuLoraAppKey;
import com.neptunetg.fpc.rest.json.miuloraconfig.JsonMiuLoraConfig;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.ConflictException;
import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;

/**
 * IF39 and IF40
 */
@RestController
public class MiuLoraConfigController extends BaseTokenAuthenticatingRestController
{
    private static final Logger log = LoggerFactory.getLogger(MiuLoraConfigController.class);

    @Autowired
    private LoraRepository loraRepository;

    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "/miu/{miuId}/lora-config", method = RequestMethod.POST)
    public ResponseEntity setMiuLoraConfig(@PathVariable(value = "miuId") MiuId miuId,
                                   @RequestBody JsonMiuLoraConfig loraConfig)
    {
        LoraDeviceDetail loraDeviceDetail = loraRepository.getLoraDeviceDetailByMiuId(miuId);

        if(miuId.numericValue() != loraConfig.getMiuId())
        {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        long rowNumber;

        if(loraDeviceDetail != null)
        {
            if(loraDeviceDetail.getEui() != null && !loraDeviceDetail.getEui().equals(loraConfig.getDevEui()) ||
               loraDeviceDetail.getAppEui() != null && !loraDeviceDetail.getAppEui().equals(loraConfig.getAppEui()))
            {
                return new ResponseEntity(HttpStatus.CONFLICT);
            }

            loraDeviceDetail.setEui(Eui.valueOf(loraConfig.getDevEui()));
            loraDeviceDetail.setAppEui(Eui.valueOf(loraConfig.getAppEui()));

            rowNumber = loraRepository.insertOrOverwriteLoraDeviceDetail(loraDeviceDetail);
        }
        else
        {
            loraDeviceDetail = new LoraDeviceDetail(miuId, null, Instant.now(), Eui.valueOf(loraConfig.getDevEui()),
                    Eui.valueOf(loraConfig.getAppEui()), null, null);

            rowNumber = loraRepository.insertOrOverwriteLoraDeviceDetail(loraDeviceDetail);
        }


        return new ResponseEntity<String>(Long.toString(rowNumber), HttpStatus.OK);
    }

    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "/miu/lora-app-key", method = RequestMethod.POST)
    public ResponseEntity setMiuLoraAppKey(@RequestBody JsonMiuLoraAppKey loraAppKey) throws BadRequestException, ConflictException, MdceRestException
    {
        LoraDeviceDetail loraDeviceDetail =
                loraRepository.getLoraDeviceDetailByDeviceDetailsId(loraAppKey.getLoraDeviceDetailsId());

        if(loraDeviceDetail != null)
        {
            if(loraDeviceDetail.getAppKey() != null && !loraDeviceDetail.getAppKey().equals(loraAppKey.getAppKey()))
            {
                return new ResponseEntity(HttpStatus.CONFLICT);
            }

            loraDeviceDetail.setAppKey(loraAppKey.getAppKey());

            loraRepository.insertOrOverwriteLoraDeviceDetail(loraDeviceDetail);
        }
        else
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
