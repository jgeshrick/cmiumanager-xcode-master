/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.json.lifecycle;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by WJD1 on 06/04/2016.
 * Class to represent a change in CMIU lifecycle state in JSON
 */
public class CmiuLifecycleStateJson
{
    @JsonProperty("miu_id")
    int miuId;

    @JsonProperty("state")
    StateJson state;

    public int getMiuId()
    {
        return miuId;
    }

    public void setMiuId(int miuId)
    {
        this.miuId = miuId;
    }

    public StateJson getState()
    {
        return state;
    }

    public void setState(StateJson state)
    {
        this.state = state;
    }
}
