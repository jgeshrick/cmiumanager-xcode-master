/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.model;

import com.neptunetg.fpc.edi.EdiException;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * EDI X12 997 Functional Acknowledgment
 */
public class FunctionalAcknowledgment
{
    private final static String DOC_IDENTIFIER = "ST*997*";
    private static final String REGEX_SEGMENT_HEADER = "^([a-zA-Z0-9]+)\\*";
    private boolean hasErrors;
    private boolean isAccepted;

    private Map<String, String> segmentsMap;


    private FunctionalAcknowledgment(Map<String, String> segmentsMap)
    {
        this.segmentsMap = segmentsMap;
    }

    public boolean isHasErrors()
    {
        return hasErrors;
    }

    public boolean isAccepted()
    {
        return isAccepted;
    }


    /**
     * Parse an EDI 997 acknowledgment string
     * @param edi997
     */
    public static FunctionalAcknowledgment fromString(String edi997) throws EdiException
    {
        Map<String, String> segmentsMap = new HashMap<>();
        boolean isEdi997Document = false;

        final Pattern pattern = Pattern.compile(REGEX_SEGMENT_HEADER);   //Regex to extract only the segment header from a line

        Scanner scanner = new Scanner(edi997);
        while (scanner.hasNextLine())
        {
            final String segment = scanner.nextLine();

            if (segment.startsWith(DOC_IDENTIFIER))
            {
                isEdi997Document = true;
            }


            final Matcher matcher = pattern.matcher(segment);
            if (matcher.find())
            {
                segmentsMap.put(matcher.group(1), segment);
            }
        }
        scanner.close();

        if (!isEdi997Document)
        {
            throw new EdiException("Not an EDI 997 Functional acknowledge document");
        }
        else
        {
            FunctionalAcknowledgment ediDoc = new FunctionalAcknowledgment(segmentsMap);

            //we probably will assume the 997 is of a correct format and would not attempt to validate it
            //starts to look for error and whether the doc is accepted

            if (segmentsMap.containsKey("AK3") ||
                    segmentsMap.containsKey("AK4"))
            {
                ediDoc.hasErrors = true;
            }

            if (segmentsMap.containsKey("AK5"))
            {
                String ak501 = extractString(segmentsMap.get("AK5"), "^AK5\\*(.)");

                ediDoc.isAccepted = (ak501 != null && ak501.equalsIgnoreCase("A"));
            }

            return ediDoc;
        }
    }

    private static String extractString(final String source, final String regex)
    {
        final Pattern pattern = Pattern.compile(regex);   //Regex to extract only the segment header from a line

        final Matcher matcher = pattern.matcher(source);
        if (matcher.find())
        {
            return matcher.group(1);
        }

        return null;
    }

    public Map<String, String> getSegmentsMap()
    {
        return segmentsMap;
    }



    /**
     * AK501 error codes. https://msdn.microsoft.com/en-us/library/bb226438.aspx
     */
    public enum AK501ErrorCodes
    {
        Accepted("A"),
        AcceptedWithErrors("E"),
        RejectedFailedMessageAuthenticationCode("M"),
        PartiallyAccepted("P"),
        Rejected("R"),
        RejectedFailedValidityTests("W"),
        RejectedCannotNotRead("X");

        private String errorCode;

        private AK501ErrorCodes(String errorCode)
        {
            this.errorCode = errorCode;
        }
    }


}
