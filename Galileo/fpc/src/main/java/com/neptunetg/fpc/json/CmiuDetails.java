/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.cns.model.DeviceCellularInformation;

/**
 * Contains CMIU CANN details and associated cellular details for the purpose of checking whether the device has
 * been entered in the MDCE database and checking the device cellular state
 */
public class CmiuDetails
{
    @JsonProperty("miu_id")
    private long miuId;

    @JsonProperty("cellular_details")
    private DeviceCellularInformation cellularDetails;

    public CmiuDetails()
    {}

    public CmiuDetails(long miuId, DeviceCellularInformation cellularDetails)
    {
        this.miuId = miuId;
        this.cellularDetails = cellularDetails;
    }

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

}
