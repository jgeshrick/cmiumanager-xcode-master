/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.data.audit;

import com.neptunetg.common.data.miu.cellular.ConfigSet;
import com.neptunetg.fpc.json.MiuCellularConfig;

/**
 * Interface to factory process audit repository
 */
public interface FactoryProcessAuditRepository
{
    void logDeviceRegistration(MiuCellularConfig miuCellularConfig, boolean requestSubmittedToCNS);

}
