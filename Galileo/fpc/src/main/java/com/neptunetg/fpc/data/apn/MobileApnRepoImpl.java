/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.fpc.data.apn;

import com.neptunetg.fpc.data.apn.model.CellularProviderEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Handle audit logs for JDBC
 */
@Repository
public class MobileApnRepoImpl implements MobileApnRepo
{
    private static final Logger log = LoggerFactory.getLogger(MobileApnRepoImpl.class);
    private final JdbcTemplate db;

    @Autowired
    public MobileApnRepoImpl(JdbcTemplate db)
    {
        this.db = db;
    }

    @Override
    public String getApnForCellularProvider(CellularProviderEnum provider)
    {
        String sql = "SELECT apn FROM mdce.apn_lookup WHERE mobile_network_operator = '" +
                provider.getStringValue() +
                "' ORDER BY RAND() LIMIT 1";

        return this.db.queryForObject(sql, String.class);
    }

    @Override
    public CellularProviderEnum getCellularProviderFromApn(String apn)
    {
        String sql = "SELECT mobile_network_operator FROM mdce.apn_lookup WHERE apn = " + apn + " LIMIT 1";

        try
        {
            String provider = db.queryForObject(sql, String.class);
            return CellularProviderEnum.fromStringValue(provider);
        }
        catch(DataAccessException e)
        {
            //Return null to indicate we couldn't find that APN
            return null;
        }
    }
}
