/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi;

/**
 * Use to throw exception for building an EDI document
 */
public class EdiException extends Exception
{
    public EdiException(String message)
    {
        super(message);
    }

    public EdiException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
