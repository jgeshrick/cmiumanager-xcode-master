/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.as2;

import com.helger.as2lib.client.AS2Client;
import com.helger.as2lib.client.AS2ClientRequest;
import com.helger.as2lib.client.AS2ClientResponse;
import com.helger.as2lib.client.AS2ClientSettings;
import com.helger.as2lib.crypto.ECryptoAlgorithmCrypt;
import com.helger.as2lib.crypto.ECryptoAlgorithmSign;
import com.helger.as2lib.disposition.DispositionOptions;
import com.helger.as2lib.message.IMessage;
import com.helger.as2lib.partner.Partnership;
import com.helger.as2lib.processor.resender.IProcessorResenderModule;
import com.helger.as2lib.processor.resender.ImmediateResenderModule;
import com.helger.as2lib.processor.sender.AS2SenderModule;
import com.helger.as2lib.processor.sender.IProcessorSenderModule;
import com.helger.as2lib.session.AS2Session;
import com.helger.commons.charset.CCharset;
import com.helger.commons.io.resource.ClassPathResource;
import com.neptunetg.fpc.edi.EdiAs2Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

/**
 * AS2 client which inherits from com.helger.as2lib.client.AS2Client.
 * Overrides the sendSychronous function because the original function has not way to inject
 * content-header: Disposition-Notification-To, which Verizon AS2 server needs in order to return a valid MDN message.
 */
public class NeptuneAS2Client extends AS2Client
{
    /**
     * The file path to the PKCS12 key store
     */
    private static final String PKCS12_CERTSTORE_PATH = "/keystore/certificates.p12";

    /**
     * The password to open the PKCS12 key store
     */
    private static final String PKCS12_CERTSTORE_PASSWORD = "test";

    /**
     * Your AS2 sender ID
     */
    private static final String SENDER_AS2_ID = "NEPTUNE";

    /**
     * Your AS2 sender email address
     */
    private static final String SENDER_EMAIL = "nobody@neptunetg.com";

    //Verizon test AS2 server
    private static final String VZW_AS2_ID = "113073472VZW";

    //Contains EDI and AS2 constants related to either production or development server
    private final EdiAs2Constants ediAs2Constants;

    //Test with local Mendelson server
/*    private static final String VZW_AS2_URL = "http://172.20.7.62:8080/as2/HttpReceiver";
    private static final String VZW_AS2_ID = "NEPTUNE";
    private static final String VZW_KEY_ALIAS = "mdce-dev0"; //Verizon key alias in PKCS12 key store
*/

    //Mendelson test AS2 server
/*    private static final String SENDER_AS2_ID = "mycompanyAS2";
    private static final String SENDER_KEY_ALIAS = "key1";
    private static final String VZW_AS2_URL = "http://testas2.mendelson-e-c.com:8080/as2/HttpReceiver";
    private static final String VZW_AS2_ID = "mendelsontestAS2";
    private static final String VZW_KEY_ALIAS = "key2"; //Verizon key alias in PKCS12 key store
*/

    public NeptuneAS2Client(EdiAs2Constants ediAs2Constants)
    {
        this.ediAs2Constants = ediAs2Constants;
    }


    private static final Logger log = LoggerFactory.getLogger(NeptuneAS2Client.class);

    public AS2ClientResponse sendFile(final String subject, final String resourceFilePath) throws CertificateException, FileNotFoundException
    {
        final AS2ClientRequest request = new AS2ClientRequest(subject);

        //where the EDi file is: TODO change to dynamic string or inputstream from a dynamically generated EDI doc
        request.setData(ClassPathResource.getAsFile(resourceFilePath));

        AS2ClientResponse response = this.sendSynchronous(buildClientSettings(), request);

        log.debug(response.getAsString());

        return response;
    }

    /**
     * Send message to Verizon AS2 server
     *
     * @param subject
     * @param ediMessage
     * @return
     * @throws CertificateException
     * @throws FileNotFoundException
     */
    public AS2ClientResponse sendMessage(final String subject, final String ediMessage) throws CertificateException, FileNotFoundException
    {
        final AS2ClientRequest request = new AS2ClientRequest(subject);

        //where the EDi file is: TODO change to dynamic string or inputstream from a dynamically generated EDI doc
        request.setData(ediMessage, CCharset.CHARSET_UTF_8_OBJ);

        AS2ClientResponse response = this.sendSynchronous(buildClientSettings(), request);

        log.debug(response.getAsString());

        return response;
    }

    private AS2ClientSettings buildClientSettings() throws CertificateException, FileNotFoundException
    {
        log.debug("AS2 client, Sender settings: AS2 ID = {}, Key alias = {}",
                SENDER_AS2_ID, ediAs2Constants.getNeptuneAs2KeyAlias());
        log.debug("AS2 client, Receiver settings: AS2 ID = {}, Key alias = {}, destination AS2 server: {}",
                VZW_AS2_ID, ediAs2Constants.getVerizonAs2KeyAlias(), ediAs2Constants.getVerizonAs2ServerUrl());

        // Start building the AS2 client settings
        final AS2ClientSettings as2ClientSettings = new AS2ClientSettings();

        // Key store
        as2ClientSettings.setKeyStore(ClassPathResource.getAsFile(PKCS12_CERTSTORE_PATH), PKCS12_CERTSTORE_PASSWORD);
        as2ClientSettings.setSaveKeyStoreChangesToFile(false);

        // Fixed sender
        as2ClientSettings.setSenderData(SENDER_AS2_ID, SENDER_EMAIL, ediAs2Constants.getNeptuneAs2KeyAlias());

        as2ClientSettings.setEncryptAndSign(ECryptoAlgorithmCrypt.CRYPT_3DES, ECryptoAlgorithmSign.DIGEST_SHA1);

        // Dynamic receiver
        as2ClientSettings.setReceiverData(VZW_AS2_ID, ediAs2Constants.getVerizonAs2KeyAlias(), ediAs2Constants.getVerizonAs2ServerUrl());
        //as2ClientSettings.setReceiverCertificate(getReceiverAs2Certificate(VZW_AS2_CERTIFICATE_PATH));


        // AS2 stuff - no need to change anything in this block
        as2ClientSettings.setPartnershipName(as2ClientSettings.getSenderAS2ID() +
                "-" +
                as2ClientSettings.getReceiverAS2ID());

        as2ClientSettings.setMDNOptions(
                new DispositionOptions()
                        .setMICAlg(ECryptoAlgorithmSign.DIGEST_SHA1)
                        .setMICAlgImportance(DispositionOptions.IMPORTANCE_OPTIONAL)
                        .setProtocol(DispositionOptions.PROTOCOL_PKCS7_SIGNATURE)
                        .setProtocolImportance(DispositionOptions.IMPORTANCE_OPTIONAL))
        ;

        as2ClientSettings.setEncryptAndSign(ECryptoAlgorithmCrypt.CRYPT_3DES, ECryptoAlgorithmSign.DIGEST_SHA1);
        return as2ClientSettings;
    }


    /**
     * Retrieve server certificate from keystore
     * @param filepath filepath from the resource directory to where the server certificate is.
     * @return certificate deserialised from the certificate file.
     * @throws CertificateException
     * @throws FileNotFoundException
     */
    private X509Certificate getReceiverAs2Certificate(final String filepath) throws CertificateException, FileNotFoundException
    {

        // Open the file.
//        FileInputStream fileinputstream = new FileInputStream(filepath);
        InputStream inputStream = getClass().getResourceAsStream(filepath);

        // Get the correct certificate factory.
        CertificateFactory certificatefactory = CertificateFactory.getInstance("X.509");

        return (X509Certificate) certificatefactory.generateCertificate(inputStream);
    }

    /**
     * Send a file or message in a synchronous AS2 transaction and get an MDN back. This overrides the original function
     * because it does not includes content-header: Disposition-Notification-To, which is required for Verizon AS2 server.
     *
     * @param as2ClientSettings
     * @param request
     * @return
     */
    @Nonnull
    @Override
    public AS2ClientResponse sendSynchronous(@Nonnull final AS2ClientSettings as2ClientSettings,
                                             @Nonnull final AS2ClientRequest request)
    {
        final AS2ClientResponse response = createResponse();
        IMessage msg = null;
        try
        {
            final Partnership partnership = buildPartnership(as2ClientSettings);

            //inject additional content header: Disposition-Notification-To
            partnership.setAS2MDNTo("http://as2.neptunetg.mdce");

            msg = createMessage(partnership, request);
            response.setOriginalMessageID(msg.getMessageID());

            log.debug("MessageID to send: " + msg.getMessageID());

            final boolean hasRetries = as2ClientSettings.getRetryCount() > 0;

            // Start a new session1
            final AS2Session as2Session = createSession();

            initCertificateFactory(as2ClientSettings, as2Session);
            initPartnershipFactory(as2Session);
            initMessageProcessor(as2Session);

            if (hasRetries)
            {
                // Use synchronous no-delay resender
                final IProcessorResenderModule resenderModule = new ImmediateResenderModule();
                resenderModule.initDynamicComponent(as2Session, null);
                as2Session.getMessageProcessor().addModule(resenderModule);
            }

            as2Session.getMessageProcessor().startActiveModules();
            try
            {
                // Invoke callback
                beforeSend(as2ClientSettings, as2Session, msg);

                // Build options map for "handle"
                final Map<String, Object> handleOptions = new HashMap<>();
                if (hasRetries)
                {
                    handleOptions.put(IProcessorResenderModule.OPTION_RETRIES, Integer.toString(as2ClientSettings.getRetryCount()));
                }

                // And create a sender module that directly sends the message
                // The message processor registration is required for the resending
                // feature
                final AS2SenderModule as2SenderModule = new AS2SenderModule();
                as2SenderModule.initDynamicComponent(as2Session, null);
                as2Session.getMessageProcessor().addModule(as2SenderModule);

                as2SenderModule.handle(IProcessorSenderModule.DO_SEND, msg, handleOptions);
            }
            finally
            {
                as2Session.getMessageProcessor().stopActiveModules();
            }
        }
        catch (final Throwable t)
        {
            log.error("Error sending AS2 message", t);
            response.setException(t);
        }
        finally
        {
            if (msg != null && msg.getMDN() != null)
            {
                // May be present, even in case of an exception
                response.setMDN(msg.getMDN());
            }
        }

        log.debug("Response retrieved: " + response.getAsString());

        return response;
    }


}
