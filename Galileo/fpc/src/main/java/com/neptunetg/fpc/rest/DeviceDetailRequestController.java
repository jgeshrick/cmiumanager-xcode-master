/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.rest;

import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.cns.DeviceIdentifierKind;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.fpc.cns.CellularNetworkServiceManager;
import com.neptunetg.fpc.domain.miu.MiuManagementService;
import com.neptunetg.fpc.json.CmiuDetails;
import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import com.neptunetg.mdce.common.api.rest.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for serving device requests
 */
@RestController
public class DeviceDetailRequestController
{

    @Autowired
    private MiuManagementService miuManagementService;

    @Autowired
    private CellularNetworkServiceManager cellularNetworkServiceManager;

    /**
     * Retrieve device cellular details. This would return contains iccid, imei, device provision state.
     * @param miuId miu id
     * @return
     */
    @RequestMapping(value = Constants.FPC_REST_URL_BASE + "/miu/{miuId}/detail", method= {RequestMethod.GET})
    public CmiuDetails getDeviceDetails(@PathVariable(value = "miuId") long miuId)
    {
        try
        {
            final CellularDeviceDetail cellularDeviceDetail = miuManagementService.getCellularConfig(MiuId.valueOf(miuId));
            final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(cellularDeviceDetail);
            final String iccid = cellularDeviceDetail.getIccid();
            final DeviceCellularInformation deviceCellularInfo = cellularNetworkService.getDeviceInformationFromIdentifier(DeviceIdentifierKind.ICCID, iccid);
            if (deviceCellularInfo == null)
            {
                throw new NotFoundException("Cannot find device with MIU ID " + miuId + ", ICCID " + iccid + " via API call to " + cellularDeviceDetail.getMno());
            }
            return new CmiuDetails(miuId, deviceCellularInfo);

        }
        catch (Exception e)
        {
            throw new MdceRestException("Exception occurred while getting device details for MIU " + miuId, e);
        }
    }

}
