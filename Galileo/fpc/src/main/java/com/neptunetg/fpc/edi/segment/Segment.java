/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.fpc.edi.segment;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Generic segment constructor for specifying a segment
 */
public class Segment extends X12
{
    private final String identifier;    //Segment identifier, a 2-3 letter code
    private List<Element> elements;

    public Segment(final String segmentIdentifier)
    {
        this.identifier = segmentIdentifier;
        this.elements = new ArrayList<>();
    }

    public Segment appendElement(Element element)
    {
        elements.add(element);
        return this;
    }

    @Override
    public String toString()
    {
        return identifier + Element.ELEMENT_SEPARATOR + //Segment name + element separator
                elements.stream()   //add all data element together
                .map(Element::toString)
                .collect(Collectors.joining(Element.ELEMENT_SEPARATOR))
                + SEGMENT_SEPARATOR;    //end with segment separator
    }
}
