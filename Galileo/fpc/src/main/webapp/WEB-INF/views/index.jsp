<%--
  ~ **************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ **************************************************************************
  --%>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Factory Process Connector</title>
    <c:url var="resourcesUrl" value="/resources" />
</head>
<body>
    <div class="maincontent">
        <div class="section">
            <h1>Factory Process Connector Server</h1>
            <p>Application version: ${applicationVersion}</p>
            <p>Environment: ${environmentName}</p>
            <p>Server Timer: ${currentServerTime}</p>
        </div>
    </div>
</body>
</html>
