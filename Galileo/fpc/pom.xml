<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ **************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ **************************************************************************
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.neptunetg.fpc</groupId>
    <artifactId>fpc</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>war</packaging>

    <properties>
        <!--Set Language level-->
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>

        <!-- Generic properties -->
        <java.version>1.8</java.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>


        <!-- Spring -->
        <spring-framework.version>4.2.2.RELEASE</spring-framework.version>

        <!-- Logging -->
        <logback.version>1.1.7</logback.version>
        <slf4j.version>1.7.5</slf4j.version>

        <!-- Test -->
        <junit.version>4.12</junit.version>

        <!-- Web -->
        <jsp.version>2.2</jsp.version>
        <jstl.version>1.2</jstl.version>
        <servlet.version>2.5</servlet.version>

        <!-- common-aws-ipc.version>1.0-SNAPSHOT</common-aws-ipc.version -->
        <common-aws-ipc.version>1.6.170612.84</common-aws-ipc.version>

        <!--<mdce-common-internal-api.version>1.0-SNAPSHOT</mdce-common-internal-api.version>-->
        <mdce-common-internal-api.version>1.6.170612.246</mdce-common-internal-api.version>

        <!--<common-external-api.version>1.0-SNAPSHOT</common-external-api.version>-->
        <common-external-api.version>1.5.170407.12</common-external-api.version>

        <!--<common-cellular-network-interface.version>1.0-SNAPSHOT</common-cellular-network-interface.version>-->
        <common-cellular-network-interface.version>1.6.170626.80</common-cellular-network-interface.version>

        <!--<common-miu-management.version>1.0-SNAPSHOT</common-miu-management.version>-->
        <common-miu-management.version>1.6.170626.149</common-miu-management.version>

        <!--<common-status-check.version>1.0-SNAPSHOT</common-status-check.version>-->
        <common-status-check.version>1.1.161021.4</common-status-check.version>

    </properties>

    <build>
        <finalName>fpc</finalName>

        <plugins>
            <plugin>
                <artifactId>maven-war-plugin</artifactId>
                <version>2.6</version>
                <configuration>
                    <!--
                      Exclude dev-only resources
                    -->
                    <packagingExcludes>
                        WEB-INF/classes/mdce-env.properties,
                        WEB-INF/classes/**/*.jce,
                        WEB-INF/classes/dev/**
                    </packagingExcludes>

                </configuration>
            </plugin>

            <plugin>
                <groupId>net.ju-n.maven.plugins</groupId>
                <artifactId>checksum-maven-plugin</artifactId>
                <version>1.2</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>artifacts</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>

        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
                <excludes>
                    <exclude>keystore/*</exclude>
                    <exclude>api/*</exclude>
                </excludes>
            </resource>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>false</filtering>
                <includes>
                    <include>keystore/*</include>
                    <include>api/*</include>
                </includes>
            </resource>
        </resources>

    </build>

    <dependencies>
        <!-- Spring and Transactions -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring-framework.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-tx</artifactId>
            <version>${spring-framework.version}</version>
        </dependency>

        <!-- Servlet -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.1.0</version>
            <scope>provided</scope>
        </dependency>

        <!-- Web -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>${spring-framework.version}</version>
        </dependency>

        <!-- Logging with SLF4J & LogBack -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback.version}</version>
            <scope>runtime</scope>
        </dependency>


        <!-- Test Artifacts -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>


        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${spring-framework.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- Other Web dependencies -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>${jstl.version}</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>${servlet.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet.jsp</groupId>
            <artifactId>jsp-api</artifactId>
            <version>${jsp.version}</version>
            <scope>provided</scope>
        </dependency>

        <!--Mocking framework-->
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>2.0.5-beta</version>
            <scope>test</scope>
        </dependency>

        <!-- Relational Database -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>${spring-framework.version}</version>
        </dependency>

        <!--Apache Common IO -->
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>1.3.2</version>
        </dependency>


        <!-- DB pooling using DBCP -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-dbcp2</artifactId>
            <version>2.0.1</version>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>org.mariadb.jdbc</groupId>
            <artifactId>mariadb-java-client</artifactId>
            <version>1.3.5</version>
            <scope>runtime</scope>
        </dependency>

        <!--For connecting to AS2 server as client and to decode incoming AS2 request-->
        <dependency>
            <groupId>com.helger</groupId>
            <artifactId>as2-lib</artifactId>
            <version>2.2.5</version>
        </dependency>

        <!-- Jackson for JSON serialization and deserialization -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.6.2</version>
        </dependency>

        <!--Add-on module to support JSR-310 (Java 8 Date & Time API) data types.-->
        <dependency>
            <groupId>com.fasterxml.jackson.datatype</groupId>
            <artifactId>jackson-datatype-jsr310</artifactId>
            <version>2.6.2</version>
        </dependency>

        <!--MDCE common aws ipc -->
        <dependency>
            <groupId>com.neptunetg.common</groupId>
            <artifactId>common-aws-ipc</artifactId>
            <version>${common-aws-ipc.version}</version>
        </dependency>

        <!--MDCE common internal api code-->
        <dependency>
            <groupId>com.neptunetg.mdce.common</groupId>
            <artifactId>mdce-common-internal-api</artifactId>
            <version>${mdce-common-internal-api.version}</version>
        </dependency>

        <!--MDCE common status check-->
        <dependency>
            <groupId>com.neptunetg.common</groupId>
            <artifactId>common-status-check</artifactId>
            <version>${common-status-check.version}</version>
        </dependency>

        <!-- common external api code-->
        <dependency>
            <groupId>com.neptunetg.mdce.common</groupId>
            <artifactId>common-external-api</artifactId>
            <version>${common-external-api.version}</version>
        </dependency>

        <!-- cellular network service -->
        <dependency>
            <groupId>com.neptunetg.common</groupId>
            <artifactId>common-cellular-network-interface</artifactId>
            <version>${common-cellular-network-interface.version}</version>
        </dependency>

        <!-- common MIU management code -->
        <dependency>
            <groupId>com.neptunetg.common</groupId>
            <artifactId>common-miu-management</artifactId>
            <version>${common-miu-management.version}</version>
        </dependency>

    </dependencies>

    <repositories>
        <repository>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
                <checksumPolicy>warn</checksumPolicy>
            </releases>
            <snapshots>
                <enabled>false</enabled>
                <updatePolicy>never</updatePolicy>
                <checksumPolicy>fail</checksumPolicy>
            </snapshots>
            <id>neptune-nexus</id>
            <name>Neptune project Nexus server on lochness:8081</name>
            <url>http://10.3.1.46:8081/nexus/content/repositories/releases/</url>
            <layout>default</layout>
        </repository>
    </repositories>

</project>