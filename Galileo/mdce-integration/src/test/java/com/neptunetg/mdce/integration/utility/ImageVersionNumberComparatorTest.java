/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.utility;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static com.neptunetg.mdce.integration.utility.CmiuImageDistributionUtility.*;
import static org.junit.Assert.*;

/**
 * Test comparator
 */
public class ImageVersionNumberComparatorTest
{
    @Test
    public void testCompareVersionNumbers() {
        final ImageVersionNumberComparator c = new ImageVersionNumberComparator();

        assertEquals(1, c.compare(
                "myFile_v1.2.3.4",
                "myFile_v1.2.3.40"
                ));

        assertEquals(-1, c.compare(
                "myFile_v1.2.3.40",
                "myFile_v1.2.3.4"
        ));

        assertEquals(0, c.compare(
                "myFile_v1.2.3.4",
                "myFile_v1.2.3.4"
        ));

        assertEquals(1, c.compare(
                "myFile_v1.2.3.4",
                "myFile_v1.2.30.4"
        ));

        assertEquals(-1, c.compare(
                "myFile_v1.2.30.4",
                "myFile_v1.2.03.4"
        ));


        assertEquals(1, c.compare(
                "myFile_v1.2.3.4",
                "myFile_v1.20.3.4"
        ));

        assertEquals(-1, c.compare(
                "myFile_v1.20.3.4",
                "myFile_v1.02.3.4"
        ));

        assertEquals(1, c.compare(
                "myFile_v1.2.3.4",
                "myFile_v10.2.3.4"
        ));

        assertEquals(-1, c.compare(
                "myFile_v1.2.3.4",
                "myFile_v01.2.3.4"
        ));


        assertTrue(c.compare(
                "myFile_v1.2.3",
                "myFile_v1.2.3.4"
        ) > 0);

        assertTrue(c.compare(
                "myFile_v1.2.3.4",
                "myFile_v1.2.3"
        ) < 0);


        assertTrue(c.compare(
                "myFile_v1.2.3.4",
                "myFule_v1.2.3.4"
        ) > 0);

        assertTrue(c.compare(
                "myFule_v1.2.3.4",
                "myFile_v1.2.3.4"
        ) < 0);


        assertTrue(c.compare(
                "myFile_",
                "myFile_v1.2.3.4"
        ) > 0);

        assertTrue(c.compare(
                "myFule_v1.2.3.4",
                "myFile_"
        ) < 0);


        assertTrue(c.compare(
                "myFile_v1.2.3.4",
                "myFile_v1.2.3.4.bin"
        ) > 0);

        assertTrue(c.compare(
                "myFule_v1.2.3.4.bin",
                "myFile_v1.2.3.4"
        ) < 0);

        assertTrue(c.compare(
                "CmiuApplication_v0.12.160226.493.bin",
                "CmiuApplication_''v0.12..416.bin"
        ) < 0);

        assertTrue(c.compare(
                "CmiuApplication_''v0.12..416.bin",
                "CmiuApplication_v0.12.160226.493.bin"
        ) > 0);

    }



    @Test
    public void testSortImageLists() throws Exception {

        final ImageVersionNumberComparator c = new ImageVersionNumberComparator();
        for (String filename : new String[]{"ls1.txt", "ls2.txt", "ls3.txt", "ls4.txt"})
        {
            List<String> imageFilenames = new ArrayList<>();
            try (final BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/images/" + filename))))
            {
                while (true)
                {
                    String l = br.readLine();
                    if (l == null) break;
                    if (l.length() > 32)
                    {
                        String fn = l.substring(31);
                        if (fn.toLowerCase().endsWith(".bin"))
                        {
                            imageFilenames.add(fn);
                        }
                    }
                }
            }
            int shouldBeBeforeFirst = 0;
            String first = imageFilenames.get(0);
            for (String s : imageFilenames)
            {
                try
                {
                    if (c.compare(s, first) < 0)
                    {
                        shouldBeBeforeFirst++;
                    }
                }
                catch (Exception e)
                {
                    throw new AssertionError("Exception occurred when comparing " + s + " and " + first + ": ", e);
                }
            }
            imageFilenames.sort(c);
            assertEquals(first, imageFilenames.get(shouldBeBeforeFirst)); //old first item moved to correct place in list

            first = imageFilenames.get(0); //new first item

            for (String s : imageFilenames)
            {
                if (!first.equals(s))
                {
                    assertTrue("Comparison of " + s + " to " + first, c.compare(s, first) > 0);
                    assertTrue("Comparison of " + first + " to " + s, c.compare(first, s) < 0);
                }
            }


        }
    }
}
