/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.scheduled.lifecycle.alert.service;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleServiceImpl;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleStateTransistion;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import static org.junit.Assert.assertEquals;

/**
 * Created by WJD1 on 23/05/2016.
 * A class for testing the lifecycle state alert service
 */

@RunWith(MockitoJUnitRunner.class)
public class CmiuLifecycleAlertServiceImplTest
{
    @Mock
    MiuLifecycleServiceImpl cmiuLifecycleService;

    @Mock
    Properties envProperties;

    CmiuLifecycleAlertServiceThresholds thresholds;

    MiuId cmiuId1 = MiuId.valueOf(400000200);
    MiuId cmiuId2 = MiuId.valueOf(400000400);
    MiuId cmiuId3 = MiuId.valueOf(400000600);

    @Before
    public void setup()
    {
        when(envProperties.getProperty("alert.lifecycle.stuck.duration.days")).thenReturn("42");
        this.thresholds = new CmiuLifecycleAlertServiceThresholds(envProperties);
    }

    @Test
    public void testGetCmiusStuckInLifecycleStateForTooLong()
    {
        CmiuLifecycleAlertServiceImpl cmiuLifecycleAlertService =
                new CmiuLifecycleAlertServiceImpl(cmiuLifecycleService, thresholds);

        List<MiuLifecycleState> testStates1 = new ArrayList<>();
        testStates1.add(new MiuLifecycleState());
        testStates1.add(new MiuLifecycleState());
        testStates1.get(0).setMiuId(cmiuId1);
        testStates1.get(1).setMiuId(cmiuId2);

        List<MiuLifecycleState> testStates2 = new ArrayList<>();
        testStates2.add(new MiuLifecycleState());
        testStates2.get(0).setMiuId(cmiuId1);

        List<MiuLifecycleState> testStates3 = new ArrayList<>();

        when(cmiuLifecycleService.getCmiusInLifecycleStateBefore(any(MiuLifecycleStateEnum.class), any(Instant.class)))
            .thenReturn(testStates1)
            .thenReturn(testStates2)
            .thenReturn(testStates3);

        List<MiuLifecycleState> stuckCmius = cmiuLifecycleAlertService.getCmiusStuckInLifecycleStateForTooLong();

        assertEquals(3, stuckCmius.size());
        assertEquals(cmiuId1, stuckCmius.get(0).getMiuId());
        assertEquals(cmiuId2, stuckCmius.get(1).getMiuId());
        assertEquals(cmiuId1, stuckCmius.get(2).getMiuId());
    }

    @Test
    public void testGetCmiusStuckInLifecycleStateForTooLongNoCmius()
    {
        CmiuLifecycleAlertServiceImpl cmiuLifecycleAlertService =
                new CmiuLifecycleAlertServiceImpl(cmiuLifecycleService, thresholds);

        List<MiuLifecycleState> testStates1 = new ArrayList<>();

        when(cmiuLifecycleService.getCmiusInLifecycleStateBefore(any(MiuLifecycleStateEnum.class), any(Instant.class)))
                .thenReturn(testStates1);

        List<MiuLifecycleState> stuckCmius = cmiuLifecycleAlertService.getCmiusStuckInLifecycleStateForTooLong();

        assertEquals(0, stuckCmius.size());
    }

    @Test
    public void testGetCmiusNoClaimedForOverXDaysSinceActivation()
    {
        CmiuLifecycleAlertServiceImpl cmiuLifecycleAlertService =
                new CmiuLifecycleAlertServiceImpl(cmiuLifecycleService, thresholds);

        List<MiuLifecycleStateTransistion> cmiuLifecycleStateComparisonList1 = new ArrayList<>();

        cmiuLifecycleStateComparisonList1.add(new MiuLifecycleStateTransistion());
        cmiuLifecycleStateComparisonList1.add(new MiuLifecycleStateTransistion());

        cmiuLifecycleStateComparisonList1.get(0).setCurrentMiuLifecycleState(new MiuLifecycleState());
        cmiuLifecycleStateComparisonList1.get(0).setPreviousMiuLifecycleState(new MiuLifecycleState());
        cmiuLifecycleStateComparisonList1.get(1).setCurrentMiuLifecycleState(new MiuLifecycleState());
        cmiuLifecycleStateComparisonList1.get(1).setPreviousMiuLifecycleState(new MiuLifecycleState());

        cmiuLifecycleStateComparisonList1.get(0).getCurrentMiuLifecycleState().setLifecycleState(
                MiuLifecycleStateEnum.UNCLAIMED);
        cmiuLifecycleStateComparisonList1.get(0).getCurrentMiuLifecycleState().setTransitionInstant(
                Instant.ofEpochSecond(1));
        cmiuLifecycleStateComparisonList1.get(0).getPreviousMiuLifecycleState().setLifecycleState(
                MiuLifecycleStateEnum.ACTIVATED);
        cmiuLifecycleStateComparisonList1.get(0).getPreviousMiuLifecycleState().setTransitionInstant(
                Instant.ofEpochSecond(2));
        cmiuLifecycleStateComparisonList1.get(1).getCurrentMiuLifecycleState().setLifecycleState(
                MiuLifecycleStateEnum.UNCLAIMED);
        cmiuLifecycleStateComparisonList1.get(1).getCurrentMiuLifecycleState().setTransitionInstant(
                Instant.ofEpochSecond(3));
        cmiuLifecycleStateComparisonList1.get(1).getPreviousMiuLifecycleState().setLifecycleState(
                MiuLifecycleStateEnum.ACTIVATED);
        cmiuLifecycleStateComparisonList1.get(1).getPreviousMiuLifecycleState().setTransitionInstant(
                Instant.ofEpochSecond(4));

        when(cmiuLifecycleService.getMiusInLifecycleStateWithPreviousStateSetBefore(any(MiuLifecycleStateEnum.class),
                any(MiuLifecycleStateEnum.class), any(Instant.class)))
                .thenReturn(cmiuLifecycleStateComparisonList1);

        List<MiuLifecycleStateTransistion> stuckCmius =
                cmiuLifecycleAlertService.getCmiusNotClaimedInGracePeriod();

        assertEquals(2, stuckCmius.size());
        assertTrue(MiuLifecycleStateEnum.UNCLAIMED.equals(stuckCmius.get(0).getCurrentMiuLifecycleState()
                .getLifecycleState()));
        assertEquals(1, stuckCmius.get(0).getCurrentMiuLifecycleState().getTransitionInstant().getEpochSecond());
        assertTrue(MiuLifecycleStateEnum.ACTIVATED.equals(stuckCmius.get(0).getPreviousMiuLifecycleState()
                .getLifecycleState()));
        assertEquals(2, stuckCmius.get(0).getPreviousMiuLifecycleState().getTransitionInstant().getEpochSecond());
        assertTrue(MiuLifecycleStateEnum.UNCLAIMED.equals(stuckCmius.get(1).getCurrentMiuLifecycleState()
                .getLifecycleState()));
        assertEquals(3, stuckCmius.get(1).getCurrentMiuLifecycleState().getTransitionInstant().getEpochSecond());
        assertTrue(MiuLifecycleStateEnum.ACTIVATED.equals(stuckCmius.get(1).getPreviousMiuLifecycleState()
                .getLifecycleState()));
        assertEquals(4, stuckCmius.get(1).getPreviousMiuLifecycleState().getTransitionInstant().getEpochSecond());
    }

    @Test
    public void testGetCmiusStuckInLifecycleStateForAlerts()
    {
        CmiuLifecycleAlertServiceImpl cmiuLifecycleAlertService =
                new CmiuLifecycleAlertServiceImpl(cmiuLifecycleService, thresholds);

        //30 days
        Instant interval1 = Instant.now().minusSeconds(2592000);
        //31 Days
        Instant interval2 = Instant.now().minusSeconds(2678400);
        //32 Days
        Instant interval3 = Instant.now().minusSeconds(2764800);

        List<MiuLifecycleState> testStates1 = new ArrayList<>();
        testStates1.add(new MiuLifecycleState());
        testStates1.add(new MiuLifecycleState());
        testStates1.add(new MiuLifecycleState());

        testStates1.get(0).setMiuId(cmiuId1);
        testStates1.get(1).setMiuId(cmiuId2);
        testStates1.get(2).setMiuId(cmiuId3);

        testStates1.get(0).setLifecycleState(MiuLifecycleStateEnum.PREACTIVATED);
        testStates1.get(1).setLifecycleState(MiuLifecycleStateEnum.ACTIVATED);
        testStates1.get(2).setLifecycleState(MiuLifecycleStateEnum.CONFIRMED);

        testStates1.get(0).setTransitionInstant(interval1);
        testStates1.get(1).setTransitionInstant(interval2);
        testStates1.get(2).setTransitionInstant(interval3);

        List<MiuLifecycleState> testStates2 = new ArrayList<>();

        when( cmiuLifecycleService.getMiusStuckInLifecycleForAlerts(any(Duration.class)) )
                .thenReturn(testStates1)
                .thenReturn(testStates2);

        List<MiuLifecycleState> stuckCmius =
                cmiuLifecycleAlertService.getCmiusStuckInLifecycleStateForAlerts();

        assertEquals(3, stuckCmius.size());
        assertEquals(cmiuId1, stuckCmius.get(0).getMiuId());
        assertEquals(cmiuId2, stuckCmius.get(1).getMiuId());
        assertEquals(cmiuId3, stuckCmius.get(2).getMiuId());
        assertEquals(MiuLifecycleStateEnum.PREACTIVATED, stuckCmius.get(0).getLifecycleState());
        assertEquals(MiuLifecycleStateEnum.ACTIVATED, stuckCmius.get(1).getLifecycleState());
        assertEquals(MiuLifecycleStateEnum.CONFIRMED, stuckCmius.get(2).getLifecycleState());
        assertEquals(interval1, stuckCmius.get(0).getTransitionInstant());
        assertEquals(interval2, stuckCmius.get(1).getTransitionInstant());
        assertEquals(interval3, stuckCmius.get(2).getTransitionInstant());

    }
}

