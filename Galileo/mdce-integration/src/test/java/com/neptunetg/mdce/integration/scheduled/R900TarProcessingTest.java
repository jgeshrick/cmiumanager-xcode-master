/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.integration.scheduled;

import com.neptunetg.mdce.integration.scheduled.r900.analysis.R900TarFileAnalyzer;
import com.neptunetg.mdce.integration.scheduled.r900.service.R900DynamoPacketSpooler;
import com.neptunetg.mdce.integration.scheduled.r900.service.R900RelationalDataSpooler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

import java.io.File;

/**
 * Created by WJD1 on 15/02/2016.
 * This unit test uses the tarFileAnalyser to process a V3 gateway Tarball
 * containing a .dat file with only one R900 reading. The test checks that
 * the MIU is read from the .dat file.
 */

@RunWith(MockitoJUnitRunner.class)  //cause mocks to be created
public class R900TarProcessingTest
{
    @Mock
    public R900RelationalDataSpooler dataSaver;

    @Mock
    public R900DynamoPacketSpooler packetSaver;

    @Captor
    ArgumentCaptor<Integer> miuId;

    @Test
    public void tarProcessingTest()
    {
        File tarFile = new File(getClass().
                getResource("/tarballs/88030_GPV300343_042402_0360_MODIFIED.tar").getFile());

        doNothing().when(dataSaver).recordMiuHeard(miuId.capture(), any(), any());

        R900TarFileAnalyzer tarFileAnalyzer = new R900TarFileAnalyzer(tarFile, packetSaver, dataSaver, true);
        tarFileAnalyzer.processFile();

        assertEquals(1580047650, (int) miuId.getValue());
    }
}
