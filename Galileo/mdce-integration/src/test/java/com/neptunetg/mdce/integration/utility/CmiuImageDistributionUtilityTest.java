/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.utility;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import org.junit.Test;

import java.io.InputStream;
import java.util.Properties;

import static com.neptunetg.mdce.integration.utility.CmiuImageDistributionUtility.*;
import static org.junit.Assert.assertEquals;

/**
 * Test CmiuImageDistributionUtility
 */
public class CmiuImageDistributionUtilityTest
{

    @Test
    public void testGetDefaultStartAddressFromImageType() throws Exception
    {
        assertEquals(0x8000, CmiuImageDistributionUtility.getDefaultStartAddressFromImageType(ImageDescription.ImageType.FirmwareImage));
        assertEquals(0x7800, CmiuImageDistributionUtility.getDefaultStartAddressFromImageType(ImageDescription.ImageType.BleConfigImage));
        assertEquals(0x7000, CmiuImageDistributionUtility.getDefaultStartAddressFromImageType(ImageDescription.ImageType.EncryptionImage));
        assertEquals(0x6800, CmiuImageDistributionUtility.getDefaultStartAddressFromImageType(ImageDescription.ImageType.ConfigImage));
        assertEquals(0, CmiuImageDistributionUtility.getDefaultStartAddressFromImageType(ImageDescription.ImageType.BootloaderImage));
    }

    @Test
    public void testGetDefaultImageSizeFromImageType() throws Exception
    {
        assertEquals(229376, CmiuImageDistributionUtility.getDefaultImageSizeFromImageType(ImageDescription.ImageType.FirmwareImage));
        assertEquals(2048, CmiuImageDistributionUtility.getDefaultImageSizeFromImageType(ImageDescription.ImageType.BleConfigImage));
        assertEquals(2048, CmiuImageDistributionUtility.getDefaultImageSizeFromImageType(ImageDescription.ImageType.EncryptionImage));
        assertEquals(2048, CmiuImageDistributionUtility.getDefaultImageSizeFromImageType(ImageDescription.ImageType.ConfigImage));
        assertEquals(26624, CmiuImageDistributionUtility.getDefaultImageSizeFromImageType(ImageDescription.ImageType.BootloaderImage));
    }

    @Test
    public void testS3ImageFolderToImageType()
    {
        assertEquals(ImageDescription.ImageType.FirmwareImage, s3ImageFolderToImageType("CmiuApplication/CmiuApplication_v0.3.150710.92.bin"));
        assertEquals(ImageDescription.ImageType.BleConfigImage, s3ImageFolderToImageType("CmiuBleConfiguration/CmiuBleConfiguration_v0.3.150715.25.bin"));
        assertEquals(ImageDescription.ImageType.ConfigImage, s3ImageFolderToImageType("CmiuConfiguration/CmiuConfiguration0.3.150715.26.bin"));
        assertEquals(ImageDescription.ImageType.EncryptionImage, s3ImageFolderToImageType("CmiuEncryption/CmiuEncryption_v0.3.150714.22.bin"));
    }

    @Test
    public void testFilenameToDownloadUrl()
    {
        assertEquals("http://google.com/test-file-name", filenameToDownloadUrl("test-file-name", "google.com"));
        assertEquals("http://google.com/test-file-name", filenameToDownloadUrl("test-file-name", "google.com/"));
        assertEquals("http://google.com/test-file-name", filenameToDownloadUrl("test-file-name", "http://google.com"));
        assertEquals("https://google.com/test-file-name", filenameToDownloadUrl("test-file-name", "https://google.com"));
    }

    @Test
    public void testGetVersionInformation()
    {
        assertEquals("0.3.150710.92", getVersionInformation("CmiuApplication/CmiuApplication_v0.3.150710.92.bin"));
    }

    @Test
    public void testGetFilename()
    {
        assertEquals("CmiuApplication", getFileName("CmiuApplication/CmiuApplication_v0.3.150710.92.bin"));
        assertEquals("CmiuBleConfiguration", getFileName("CmiuBleConfiguration-blabla/CmiuBleConfiguration_v0.3.150715.25.bin"));
    }

    @Test
    public void testGetMemoryMapFilename()
    {
        assertEquals("CmiuEncryption_v0.5.150826.204_MemoryMap.properties",
                getMemoryMapPropertyKey("CmiuEncryption_v0.5.150826.204.bin"));
        assertEquals("folderpath/CmiuApplication_v0.5.150826.204_MemoryMap.properties",
                getMemoryMapPropertyKey("folderpath/CmiuApplication_v0.5.150826.204.bin"));

        assertEquals("CmiuEncryption_v0.5.150826.204_MemoryMap.properties",
                getMemoryMapPropertyKey("CmiuEncryption_v0.5.150826.204.BIN"));
        assertEquals("folderpath/CmiuApplication_v0.5.150826.204_MemoryMap.properties",
                getMemoryMapPropertyKey("folderpath/CmiuApplication_v0.5.150826.204.BIN"));

        assertEquals("CmiuEncryption_v0.5.150826.204_MemoryMap.properties",
                getMemoryMapPropertyKey("CmiuEncryption_v0.5.150826.204.Bin"));
        assertEquals("folderpath/CmiuApplication_v0.5.150826.204_MemoryMap.properties",
                getMemoryMapPropertyKey("folderpath/CmiuApplication_v0.5.150826.204.Bin"));

        assertEquals("CmiuEncryption_v0.5.150826.204.bin.others",
                getMemoryMapPropertyKey("CmiuEncryption_v0.5.150826.204.bin.others"));
        assertEquals("CmiuEncryption_v0.5.150826.204",
                getMemoryMapPropertyKey("CmiuEncryption_v0.5.150826.204"));

    }

    @Test
    public void testGetImageSizeFromMemoryMap() throws InternalApiException
    {
        String propFileName = "MemoryMap.properties";
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

        Properties memoryMapProperties = getMemoryMap(inputStream);

        assertEquals(0x00038000, getImageSizeFromImageType(ImageDescription.ImageType.FirmwareImage, memoryMapProperties));

        inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        assertEquals(0x00006800, getImageSizeFromImageType(ImageDescription.ImageType.BootloaderImage, memoryMapProperties));

        inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        assertEquals(0x00000800, getImageSizeFromImageType(ImageDescription.ImageType.ConfigImage, memoryMapProperties));

        inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        assertEquals(0x00000800, getImageSizeFromImageType(ImageDescription.ImageType.BleConfigImage, memoryMapProperties));

        inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        assertEquals(0x00000800, getImageSizeFromImageType(ImageDescription.ImageType.EncryptionImage, memoryMapProperties));

    }

    @Test
    public void testGetStartAddressSizeFromMemoryMap() throws InternalApiException
    {
        String propFileName = "MemoryMap.properties";
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

        Properties memoryMapProperties = getMemoryMap(inputStream);

        assertEquals(0x00008000, getStartAddressFromImageType(ImageDescription.ImageType.FirmwareImage, memoryMapProperties));

        inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        assertEquals(0x00000000, getStartAddressFromImageType(ImageDescription.ImageType.BootloaderImage, memoryMapProperties));

        inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        assertEquals(0x00006800, getStartAddressFromImageType(ImageDescription.ImageType.ConfigImage, memoryMapProperties));

        inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        assertEquals(0x00007800, getStartAddressFromImageType(ImageDescription.ImageType.BleConfigImage, memoryMapProperties));

        inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        assertEquals(0x00007000, getStartAddressFromImageType(ImageDescription.ImageType.EncryptionImage, memoryMapProperties));

    }
}