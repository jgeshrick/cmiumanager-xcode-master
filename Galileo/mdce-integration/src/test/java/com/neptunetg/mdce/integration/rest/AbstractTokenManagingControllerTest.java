/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.mdce.common.api.domain.RestClientId;
import com.neptunetg.mdce.common.api.domain.auth.TokenManagerService;
import com.neptunetg.mdce.integration.data.site.SiteId;
import org.junit.Before;
import org.mockito.Mock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.when;

public abstract class AbstractTokenManagingControllerTest {
    public static final String TOKEN = "12345678901234567890123456789012";

    @Mock
    protected TokenManagerService tokenManagerService;

    @Mock
    protected HttpServletRequest request;

    @Mock
    protected HttpServletResponse response;


    @Before
    public void setUp() throws Exception {
        String ipAddr = "192.1.1.1";
        when(request.getRemoteAddr()).thenReturn(ipAddr);
        when(tokenManagerService.checkToken(TOKEN, ipAddr)).thenReturn(RestClientId.forUtility(1));
    }

}