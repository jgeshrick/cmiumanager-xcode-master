/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.json.commandhistory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.junit.Test;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;

/**
 * Test serialiser deserialiser of object into json
 */
public class JsonGetCommandHistoryPacketTest
{
    @Test
    public void testConverstionToAndFromJson() throws IOException
    {
        JsonGetCommandHistoryPacket commandHistoryPacket = new JsonGetCommandHistoryPacket();
        commandHistoryPacket.setSendDate(ZonedDateTime.of(2015, 12, 20, 12, 34, 56, 789000000, ZoneId.of("UTC")).toInstant());
        commandHistoryPacket.setInsertDate(ZonedDateTime.of(2015, 1, 2, 3, 4, 5, 678000000, ZoneId.of("UTC")).toInstant());
        commandHistoryPacket.setPacket("TEST PACKET");
        commandHistoryPacket.setPacketType(1);
        commandHistoryPacket.setSourceDeviceType("source");
        commandHistoryPacket.setTargetDeviceType("target");

        ObjectMapper jsonMapper = new ObjectMapper();
        final String jsonOutput = jsonMapper.writeValueAsString(commandHistoryPacket);

        //expect
        //{"send_date":"2015-12-20T06:34:56.000000789-06:00","insert_date":"2015-01-01T21:04:05.000000678-06:00","source_device_type":"source","target_device_type":"target","packet_type":1,"packet":"TEST PACKET"}

        assertEquals("2015-12-20T06:34:56.789-06:00", JsonPath.read(jsonOutput, "$.send_date"));
        assertEquals("2015-01-01T21:04:05.678-06:00", JsonPath.read(jsonOutput, "$.insert_date"));
        assertEquals(commandHistoryPacket.getSourceDeviceType(), JsonPath.read(jsonOutput, "$.source_device_type"));
        assertEquals(commandHistoryPacket.getTargetDeviceType(), JsonPath.read(jsonOutput, "$.target_device_type"));
        assertEquals(commandHistoryPacket.getPacketType(), (int) JsonPath.read(jsonOutput, "$.packet_type"));
        assertEquals(commandHistoryPacket.getPacket(), JsonPath.read(jsonOutput, "$.packet"));

        JsonGetCommandHistoryPacket obj = jsonMapper.readValue(jsonOutput, JsonGetCommandHistoryPacket.class);

        assertEquals(commandHistoryPacket.getPacket(), obj.getPacket());
        assertEquals(commandHistoryPacket.getPacketType(), obj.getPacketType());
        assertEquals(commandHistoryPacket.getInsertDate(), obj.getInsertDate());
        assertEquals(commandHistoryPacket.getSourceDeviceType(), obj.getSourceDeviceType());
        assertEquals(commandHistoryPacket.getTargetDeviceType(), obj.getTargetDeviceType());
        assertEquals(commandHistoryPacket.getSendDate(), obj.getSendDate());

    }




}