/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.integration.utility;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

/**
 * Created by abh1 on 29/04/2015.
 */
public class SaveToFileStreamTest
{
    @Test
    public void testWrapStream() throws Exception
    {

        File copiedFile = File.createTempFile("SaveToFileStreamTest", ".tar");
        copiedFile.deleteOnExit();
        InputStream tarDataIn = getClass().getResourceAsStream("/tarballs/59111_GPV300026_042118_0962.tar");

        try (final SaveToFileStream saveToFileStream = new SaveToFileStream(tarDataIn, copiedFile))
        {
            try (final TarArchiveInputStream untarredIn = new TarArchiveInputStream(saveToFileStream))
            {
                while (true)
                {
                    TarArchiveEntry entry = untarredIn.getNextTarEntry();

                    if (entry == null)
                    {
                        break;
                    }
                    else
                    {
                        int len = (int) entry.getSize();
                        final byte[] data = new byte[len];
                        untarredIn.read(data);
                    }
                }
            }
        }

        tarDataIn = getClass().getResourceAsStream("/tarballs/59111_GPV300026_042118_0962.tar");

        int byteIndex = 0;
        try (final InputStream savedData = new FileInputStream(copiedFile))
        {
            int td;
            do
            {
                td = tarDataIn.read();
                assertEquals("Wrong at position " + byteIndex, td, savedData.read());
                byteIndex++;
            } while (td >= 0);

        }


    }
}
