/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.packets;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigHistory;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;
import com.neptunetg.mdce.integration.rest.json.miuconfig.JsonMiuConfigHistory;
import com.neptunetg.mdce.integration.rest.json.miuconfig.JsonMiuConfigHistoryCollection;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * To verify that JSON object has been correctly populated
 */
public class JsonMiuConfigHistoryTest
{

    @Test
    public void testCreateJsonMiuConfigHistory() throws Exception
    {
        List<CmiuConfigHistory> cmiuConfigHistoryList = createCmiuHistoryList();

        Optional<String> integer = Optional.of("3");

        JsonMiuConfigHistory jsonConfigHistory = JsonMiuConfigHistory.from(cmiuConfigHistoryList, 1, integer, ZoneId.systemDefault());
        assertNotNull(jsonConfigHistory);
        assertEquals(1, jsonConfigHistory.getMiuId());
        assertEquals(10, jsonConfigHistory.getSiteId());
        assertEquals("CMIU", jsonConfigHistory.getDeviceType());

        assertNotNull(jsonConfigHistory.getConfigurationHistories());

        List<JsonMiuConfigHistory.JsonMiuConfigHistoryEntry> configHistoryList = jsonConfigHistory.getConfigurationHistories();

        JsonMiuConfigHistory.JsonMiuConfigHistoryEntry configHistory = configHistoryList.get(0);
        assertNotNull(configHistory.getMiuConfigDetail());

        assertEquals(3, jsonConfigHistory.getConfigurationHistories().size());
        assertEquals(5, jsonConfigHistory.getConfigurationHistories().get(0).getMiuConfigDetail().getReporting().getStartTimeMins().intValue());
        assertEquals(6, jsonConfigHistory.getConfigurationHistories().get(0).getMiuConfigDetail().getReporting().getIntervalMins().intValue());
        assertEquals(3, jsonConfigHistory.getConfigurationHistories().get(0).getMiuConfigDetail().getReporting().getRetries().intValue());
        assertEquals(4, jsonConfigHistory.getConfigurationHistories().get(0).getMiuConfigDetail().getReporting().getTransmitWindowMins().intValue());
        assertEquals(1, jsonConfigHistory.getConfigurationHistories().get(0).getMiuConfigDetail().getQuietTime().getQuietStartMins().intValue());
        assertEquals(2, jsonConfigHistory.getConfigurationHistories().get(0).getMiuConfigDetail().getQuietTime().getQuietEndMins().intValue());
        assertEquals(7, jsonConfigHistory.getConfigurationHistories().get(0).getMiuConfigDetail().getRecording().getStartTimeMins().intValue());
        assertEquals(8, jsonConfigHistory.getConfigurationHistories().get(0).getMiuConfigDetail().getRecording().getIntervalMins().intValue());

    }

    @Test
    public void testCreateJsonMiuConfigHistoryCollection()
    {
        List<CmiuConfigHistory> cmiuConfigHistoryList = createCmiuHistoryList();
        Optional<String> integer = Optional.of("4");

        JsonMiuConfigHistoryCollection jsonConfigHistoryCollection = JsonMiuConfigHistoryCollection.from(cmiuConfigHistoryList, integer, ZoneId.systemDefault());

        assertNotNull(jsonConfigHistoryCollection);
        List<JsonMiuConfigHistory> miuConfigHistoryList = jsonConfigHistoryCollection.getMiuConfigHistoryList();
        assertNotNull(miuConfigHistoryList);
        assertEquals(4, miuConfigHistoryList.size());

        assertEquals(1, miuConfigHistoryList.get(0).getMiuId());
        assertEquals(2, miuConfigHistoryList.get(1).getMiuId());
        assertEquals(3, miuConfigHistoryList.get(2).getMiuId());
        assertEquals(4, miuConfigHistoryList.get(3).getMiuId());

        assertEquals(3, miuConfigHistoryList.get(0).getConfigurationHistories().size());
        assertEquals(4, miuConfigHistoryList.get(1).getConfigurationHistories().size());
        assertEquals(2, miuConfigHistoryList.get(2).getConfigurationHistories().size());
        assertEquals(1, miuConfigHistoryList.get(3).getConfigurationHistories().size());
    }



    @Test
    public void testFrom() throws Exception
    {

        //arrange

        final List<CmiuConfigHistory> sortedMiuConfigHistoryList = new ArrayList<>();
        final MiuId cmiuId = MiuId.valueOf(490000990);
        final MiuId otherCmiuId = MiuId.valueOf(580000880);

        final CmiuConfigSet cs1 = new CmiuConfigSet();
        cs1.setId(1L);
        cs1.setName("Config set 1");
        cs1.setReportingIntervalMins(1);

        final CmiuConfigSet cs2 = new CmiuConfigSet();
        cs2.setId(2L);
        cs2.setName("Config set 2");
        cs2.setReportingIntervalMins(2);

        final CmiuConfigSet cs3 = new CmiuConfigSet();
        cs3.setId(3L);
        cs3.setName("Config set 3");
        cs3.setReportingIntervalMins(3);

        final CmiuConfigSet cs4 = new CmiuConfigSet();
        cs4.setId(4L);
        cs4.setName("Config set 4");
        cs4.setReportingIntervalMins(4);

        final ZoneId timezone = ZoneId.of("UTC");
        CmiuConfigHistory ch = new CmiuConfigHistory();
        ch.setMiuId(cmiuId.numericValue());
        ch.setDateTime(ZonedDateTime.of(2016, 5, 5, 15, 0, 0, 0, timezone).toInstant());
        ch.setCmiuConfigSet(cs1);
        ch.setReceivedFromCmiu(true);

        sortedMiuConfigHistoryList.add(ch);

        ch = new CmiuConfigHistory();
        ch.setMiuId(cmiuId.numericValue());
        ch.setDateTime(ZonedDateTime.of(2016, 5, 5, 12, 0, 0, 0, timezone).toInstant());
        ch.setCmiuConfigSet(cs2);
        ch.setReceivedFromCmiu(false);

        sortedMiuConfigHistoryList.add(ch);

        ch = new CmiuConfigHistory();
        ch.setMiuId(otherCmiuId.numericValue()); //other CMIU ID so should be ignored
        ch.setDateTime(ZonedDateTime.of(2016, 5, 4, 12, 0, 0, 0, timezone).toInstant());
        ch.setCmiuConfigSet(cs3);
        ch.setReceivedFromCmiu(false);

        sortedMiuConfigHistoryList.add(ch);

        ch = new CmiuConfigHistory();
        ch.setMiuId(cmiuId.numericValue());
        ch.setDateTime(ZonedDateTime.of(2016, 5, 4, 12, 0, 0, 0, timezone).toInstant());
        ch.setCmiuConfigSet(cs4);
        ch.setReceivedFromCmiu(false);

        sortedMiuConfigHistoryList.add(ch);

        //act

        JsonMiuConfigHistory result = JsonMiuConfigHistory.from(sortedMiuConfigHistoryList, cmiuId, Optional.<String>empty(), timezone);

        //assert

        assertEquals(cmiuId.numericValue(), result.getMiuId());

        final List<JsonMiuConfigHistory.JsonMiuConfigHistoryEntry> chList = result.getConfigurationHistories();
        assertEquals(2, chList.size());
    }


    private List<CmiuConfigHistory> createCmiuHistoryList()
    {
        List<CmiuConfigHistory> cmiuConfigHistoryList = new ArrayList<>();
        cmiuConfigHistoryList.add(createConfigHistory(1, 3));
        cmiuConfigHistoryList.add(createConfigHistory(1, 2));
        cmiuConfigHistoryList.add(createConfigHistory(1, 1));
        cmiuConfigHistoryList.add(createConfigHistory(2, 4));
        cmiuConfigHistoryList.add(createConfigHistory(2, 3));
        cmiuConfigHistoryList.add(createConfigHistory(2, 2));
        cmiuConfigHistoryList.add(createConfigHistory(2, 1));
        cmiuConfigHistoryList.add(createConfigHistory(3, 2));
        cmiuConfigHistoryList.add(createConfigHistory(3, 1));
        cmiuConfigHistoryList.add(createConfigHistory(4, 1));

        return cmiuConfigHistoryList;
    }

    private CmiuConfigHistory createConfigHistory(int miuId, long daysOffset)
    {
        CmiuConfigHistory history = new CmiuConfigHistory();
        history.setMiuId(miuId);
        history.setSiteId(miuId * 10);
        history.setMiuType("CMIU");
        history.setMiuTypeId(1);

        history.setDateTime(ZonedDateTime.of(2016, 5, 4, 13, 0, 0, 0, ZoneId.of("US/Central")).plusDays(daysOffset).toInstant());
        history.setChangeNote(LocalDate.now().toString());

        history.setCmiuConfigSet(createConfigSet(miuId));
        return history;
    }

    private CmiuConfigSet createConfigSet(int id)
    {
        CmiuConfigSet cs = new CmiuConfigSet();
        cs.setId(id);
        cs.setName("Config set " + id);
        cs.setReportingQuietStartMins(1);
        cs.setReportingQuietEndMins(2);
        cs.setReportingNumberOfRetries(3);
        cs.setReportingTransmitWindowMins(4);
        cs.setReportingStartMins(5);
        cs.setReportingIntervalMins(6);
        cs.setRecordingStartTimeMins(7);
        cs.setRecordingIntervalMins(8);

        return cs;
    }
}