/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.mdce.common.api.domain.RestClientId;
import com.neptunetg.mdce.integration.data.miu.JdbcMiuSearchRepository;
import com.neptunetg.mdce.integration.data.miu.MiuDetails;
import com.neptunetg.mdce.integration.data.miu.MiuSearchRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.rest.json.JsonMiuListCollection;
import com.neptunetg.mdce.integration.rest.json.miuconfig.JsonMiuConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.neptunetg.mdce.integration.rest.json.JsonMiuListCollection.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)  //cause mocks to be created and injected
public class MiuListControllerTest extends AbstractTokenManagingControllerTest
{
    private static final String SAMPLE_ICCID = "89148000001471859842";
    private static final String SAMPLE_MIU_ID = "123456789";
    private static final String SAMPLE_IMEI = "990000560449458";
    private static final String SAMPLE_EUI = "ABCDEF0123456789";

    @InjectMocks
    private MiuListController beingTested;

    @Mock
    private MiuSearchRepository miuSearchRepo;

    @Mock
    private HttpServletRequest request;

    @Captor
    private ArgumentCaptor<Map<String, String>> captor;

    @Before
    public void initialize()
    {
        when(request.getAttribute("validatedClientId")).thenReturn(RestClientId.forUtility(0)); // Universal site
    }

    @Test
    public void testGetMiuListWithNonUniversalSiteId() throws Exception
    {
        when(request.getAttribute("validatedClientId")).thenReturn(RestClientId.forUtility(1)); // Site ID 1
        List<MiuDetails> miuList = new ArrayList<>();
        MiuDetails miuDetails = new MiuDetails(new MiuId(1), new SiteId(1), Instant.now(), Instant.now(), Instant.now(), MiuType.CMIU, true);
        miuList.add(miuDetails);

        when(miuSearchRepo.getMiusForUtility(new SiteId(1))).thenReturn(miuList);

        JsonMiuListCollection returned = beingTested.getMiuList(null, null, null, null, request);
        final ZoneId zoneId = MiuListController.TIME_ZONE_FOR_RESPONSE_DATES;

        assertNotNull(returned);
        assertNotNull(returned.getMiuList());
        assertEquals(1, returned.getMiuList().size());
        JsonMiuDetail jsonMiuDetail = returned.getMiuList().get(0);
        assertEquals(miuDetails.getId().numericValue(), jsonMiuDetail.getMiuId());
        assertEquals(miuDetails.getSiteId().numericValue(), jsonMiuDetail.getSiteId());
        final String expectedFirstInsertDate =
                miuDetails.getFirstInsertDate().atZone(zoneId).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        final String expectedLastInsertDate =
                miuDetails.getLastInsertDate().atZone(zoneId).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        assertEquals(expectedFirstInsertDate, jsonMiuDetail.getFirstInsertDate());
        assertEquals(expectedLastInsertDate, jsonMiuDetail.getLastInsertDate());
    }

    @Test
    public void testMiuListControllerPassesMiuFilterValueToRepo() throws Exception
    {
        // Arrange: Set up the mock to verify that the MIU ID filter value is being passed down to the MiuSearchRepository.
        List<MiuDetails> miuList = new ArrayList<>();
        when(miuSearchRepo.getMius(Matchers.anyMapOf(String.class, String.class))).thenReturn(miuList);

        // Act
        beingTested.getMiuList(SAMPLE_MIU_ID, null, null, null, request);

        verify(miuSearchRepo).getMius(captor.capture());
        final Map<String, String> capturedParameters = captor.getValue();

        // Assert
        assertTrue(capturedParameters.containsKey(JdbcMiuSearchRepository.MIU_ID));
        assertEquals(SAMPLE_MIU_ID, capturedParameters.get(JdbcMiuSearchRepository.MIU_ID));
    }

    @Test
    public void testMiuListControllerPassesImeiFilterValueToRepo() throws Exception
    {
        // Arrange: Set up the mock to verify that the IMEI filter value is being passed down to the MiuSearchRepository.
        List<MiuDetails> miuList = new ArrayList<>();
        when(miuSearchRepo.getMius(Matchers.anyMapOf(String.class, String.class))).thenReturn(miuList);

        // Act
        beingTested.getMiuList(null, null, SAMPLE_IMEI, null, request);

        verify(miuSearchRepo).getMius(captor.capture());
        final Map<String, String> capturedParameters = captor.getValue();

        // Assert
        assertTrue(capturedParameters.containsKey(JdbcMiuSearchRepository.IMEI_OPTION));
        assertEquals(SAMPLE_IMEI, capturedParameters.get(JdbcMiuSearchRepository.IMEI_OPTION));
    }

    @Test
    public void testMiuListControllerPassesIccidFilterValueToRepo() throws Exception
    {
        // Arrange: Set up the mock to verify that the ICCID filter value is being passed down to the MiuSearchRepository.
        List<MiuDetails> miuList = new ArrayList<>();
        when(miuSearchRepo.getMius(Matchers.anyMapOf(String.class, String.class))).thenReturn(miuList);

        // Act
        beingTested.getMiuList(null, SAMPLE_ICCID, null, null, request);

        verify(miuSearchRepo).getMius(captor.capture());
        final Map<String, String> capturedParameters = captor.getValue();

        // Assert
        assertTrue(capturedParameters.containsKey(JdbcMiuSearchRepository.ICCID_OPTION));
        assertEquals(SAMPLE_ICCID, capturedParameters.get(JdbcMiuSearchRepository.ICCID_OPTION));
    }

    @Test
    public void testMiuListController_FindMius_PassesExpectedOptionsToRepo() throws Exception
    {
        // Arrange: Set up the mock to verify that the ID search value is being passed down to the MiuSearchRepository.
        List<MiuDetails> miuList = new ArrayList<>();
        when(miuSearchRepo.getMius(Matchers.anyMapOf(String.class, String.class))).thenReturn(miuList);

        // Act
        beingTested.findMius(SAMPLE_EUI, request);

        verify(miuSearchRepo).getMius(captor.capture());
        final Map<String, String> capturedParameters = captor.getValue();

        // Assert
        assertTrue(capturedParameters.containsKey(JdbcMiuSearchRepository.FIND_ID_OPTION));
        assertEquals(SAMPLE_EUI, capturedParameters.get(JdbcMiuSearchRepository.FIND_ID_OPTION));
    }

    @Test
    public void testMiuListController_ReturnIdentities_False() throws Exception
    {
        // Arrange: Set up the mock to return sample data that includes cellular and L900 identities
        List<MiuDetails> miuList = getExampleMiuDetails();
        when(miuSearchRepo.getMius(Matchers.anyMapOf(String.class, String.class))).thenReturn(miuList);
        when(miuSearchRepo.getAllMius()).thenReturn(miuList);

        // Act
        JsonMiuListCollection result = beingTested.getMiuList(null, null, null, "N", request);

        // Assert that the cellular/LoRa identities are not returned
        assertEquals(2, result.getMiuList().size());
        JsonMiuDetail cmiu = result.getMiuList().stream()
                .filter(miu -> MiuId.valueOf(400000002).matches(miu.getMiuId()))
                .findFirst()
                .get();

        JsonMiuDetail l900 = result.getMiuList().stream()
                .filter(miu -> MiuId.valueOf(700000004).matches(miu.getMiuId()))
                .findFirst()
                .get();

        assertNull(cmiu.getIccid());
        assertNull(cmiu.getImei());
        assertNull(l900.getEui());
    }

    @Test
    public void testMiuListController_ReturnIdentities_True() throws Exception
    {
        // Arrange: Set up the mock to return sample data that includes cellular and L900 identities
        List<MiuDetails> miuList = getExampleMiuDetails();
        when(miuSearchRepo.getMius(Matchers.anyMapOf(String.class, String.class))).thenReturn(miuList);
        when(miuSearchRepo.getAllMius()).thenReturn(miuList);

        // Act
        JsonMiuListCollection result = beingTested.getMiuList(null, null, null, "Y", request);

        // Assert that the cellular/LoRa identities are returned where present in the source data
        assertEquals(2, result.getMiuList().size());
        JsonMiuDetail cmiu = result.getMiuList().stream()
                .filter(miu -> MiuId.valueOf(400000002).matches(miu.getMiuId()))
                .findFirst()
                .get();

        JsonMiuDetail l900 = result.getMiuList().stream()
                .filter(miu -> MiuId.valueOf(700000004).matches(miu.getMiuId()))
                .findFirst()
                .get();

        assertEquals("89148000001471859586", cmiu.getIccid());
        assertEquals("990000560449458", cmiu.getImei());

        assertEquals("ABCDEF0123456789", l900.getEui());
    }

    @Test
    public void testMiuListController_FindMius_ReturnIdentities_True() throws Exception
    {
        // Arrange: Set up the mock to return sample data that includes cellular and L900 identities
        List<MiuDetails> miuList = getExampleMiuDetails();
        when(miuSearchRepo.getMius(Matchers.anyMapOf(String.class, String.class))).thenReturn(miuList);
        when(miuSearchRepo.getAllMius()).thenReturn(miuList);

        // Act
        JsonMiuListCollection result = beingTested.findMius(SAMPLE_EUI, request);

        // Assert that the cellular/LoRa identities are returned where present in the source data
        assertEquals(2, result.getMiuList().size());
        JsonMiuDetail cmiu = result.getMiuList().stream()
                .filter(miu -> MiuId.valueOf(400000002).matches(miu.getMiuId()))
                .findFirst()
                .get();

        JsonMiuDetail l900 = result.getMiuList().stream()
                .filter(miu -> MiuId.valueOf(700000004).matches(miu.getMiuId()))
                .findFirst()
                .get();

        assertEquals("89148000001471859586", cmiu.getIccid());
        assertEquals("990000560449458", cmiu.getImei());

        assertEquals("ABCDEF0123456789", l900.getEui());
    }

    private static List<MiuDetails> getExampleMiuDetails()
    {
        List<MiuDetails> result = new ArrayList<>();

        MiuDetails cmiu = new MiuDetails(
                MiuId.valueOf(400000002),                   // MiuId
                SiteId.valueOf(1),                          // SiteId
                Instant.parse("2017-01-13T10:00:00Z"),      // lastHeardTime
                Instant.parse("2017-01-13T11:00:00Z"),      // lastInsertDate
                Instant.parse("2017-01-13T09:00:00Z"),      // firstInsertDate
                MiuType.CMIU,                               // type
                true,                                       // meterActive
                60,                                         // recordingIntervalMinutes
                1440,                                       // reportingIntervalMinutes
                "Basic",                                    // cmiuModeName
                "Basic",                                    // reportedCmiuModeName
                "VZW",                                      // networkProvider
                "89148000001471859586",                     // iccid
                "990000560449458",                          // imei
                null,                                       // eui
                "123456789",
                MiuLifecycleStateEnum.CLAIMED);             //lifecycle state

        MiuDetails l900 = new MiuDetails(
                MiuId.valueOf(700000004),                   // MiuId
                SiteId.valueOf(2),                          // SiteId
                Instant.parse("2017-01-13T15:00:00Z"),      // lastHeardTime
                Instant.parse("2017-01-13T16:00:00Z"),      // lastInsertDate
                Instant.parse("2017-01-13T14:00:00Z"),      // firstInsertDate
                MiuType.CMIU,                               // type
                true,                                       // meterActive
                60,                                         // recordingIntervalMinutes
                1440,                                       // reportingIntervalMinutes
                "Basic",                                    // cmiuModeName
                "Basic",                                    // reportedCmiuModeName
                "VZW",                                      // networkProvider
                null,                                       // iccid
                null,                                       // imei
                "ABCDEF0123456789",                         // eui
                null,
                MiuLifecycleStateEnum.CLAIMED);             // lifecycle state

        result.add(cmiu);
        result.add(l900);

        return result;
    }

}