/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.common.packet.model.CmdId;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.MqttPacketHeaderData;
import com.neptunetg.common.packet.model.taggeddata.tags.RecordingAndReportingIntervalData;
import com.neptunetg.common.packet.model.taggeddata.tags.SecureBlockArrayData;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerData;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import com.neptunetg.mdce.integration.domain.MiuCommandService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Unit Test for Cmiu command control service controller.
 */
@RunWith(MockitoJUnitRunner.class)  //cause mocks to be created
public class MiuCommandControlServiceControllerTest
{
    private final String USERNAME = "unit-test-user";

    //class under test
    @InjectMocks
    private MiuCommandControlServiceController commandControlServiceController;    //SUT

    @Mock
    private MiuCommandService miuCommandService;

    @Mock
    private MdceIpcPublishService awsQueueService;

    private List<Long> cmiuList;

    @Before
    public void setUp()
    {
        ReflectionTestUtils.setField(commandControlServiceController, "imageDownloadUrl", "fake.url");

        this.cmiuList = createFakeCmiuList(5);
    }

    @Test
    public void testSendFirmwareImageUpdateCommand() throws Exception
    {

        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(USERNAME);
        commandRequest.setCommandCmiu(CommandRequest.CommandTypeCmiu.COMMAND_UPDATE_IMAGE);

        long executionTime = Instant.now().getEpochSecond();
        commandRequest.setExecutionTimeEpochSecond(executionTime);

        List<ImageDescription> imageDescriptionList = new ArrayList<>();
        ImageDescription imageDescription = new ImageDescription();
        imageDescription.setFileName("test filename url");
        imageDescription.setSizeBytes(0x1020);
        imageDescription.setStartAddress(0x200);
        imageDescription.setImageType(ImageDescription.ImageType.FirmwareImage);
        imageDescription.setRevisionMajor(1);
        imageDescription.setRevisionMinor(2);
        imageDescription.setRevisionDate(3);
        imageDescription.setRevisionBuildNum(4);
        imageDescriptionList.add(imageDescription);

        commandRequest.setImageDescription(imageDescriptionList);

        //add list of CMIU
        List<Long> cmiuList = createFakeCmiuList(5);

        commandRequest.setMiuList(cmiuList);

        commandControlServiceController.sendCommand(commandRequest);

        //command should have been called 5 times
        ArgumentCaptor<MiuCommand> miuCommandArg = ArgumentCaptor.forClass(MiuCommand.class);
//        verify(miuRepo, times(cmiuList.size()) ).createMiuCommand(any(MiuCommand.class));
        verify(miuCommandService, times(cmiuList.size())).createMiuCommand(miuCommandArg.capture());


        List<MiuCommand> params = miuCommandArg.getAllValues();
        assertEquals(MiuId.valueOf(1), params.get(0).getMiuId());
        assertEquals(MiuId.valueOf(2), params.get(1).getMiuId());
        assertEquals(MiuId.valueOf(3), params.get(2).getMiuId());
        assertEquals(MiuId.valueOf(4), params.get(3).getMiuId());
        assertEquals(MiuId.valueOf(5), params.get(4).getMiuId());

        byte[] rawPacket = params.get(0).getCommandParams();
        TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(rawPacket);

        assertNotNull(parsedTaggedPacket.findTag(MqttPacketHeaderData.class));

        Map<Integer, TaggedData> secureDataMap = PacketParser.parseSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));

        assertEquals(5, secureDataMap.size());
        assertNotNull(secureDataMap.get(32));   //Command or configuration tag
        assertNotNull(secureDataMap.get(50));   //Current Time & Date tag
        assertNotNull(secureDataMap.get(39));   //Firmware tag
        assertNotNull(secureDataMap.get(46));   //Image
        assertNotNull(secureDataMap.get(47));   //Image Meta data
    }

    @Test
    public void testSendMultipleImageUpdateCommand() throws Exception
    {
        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(USERNAME);
        commandRequest.setCommandCmiu(CommandRequest.CommandTypeCmiu.COMMAND_UPDATE_IMAGE);

        long executionTime = Instant.now().getEpochSecond();
        commandRequest.setExecutionTimeEpochSecond(executionTime);

        List<ImageDescription> imageDescriptionList = new ArrayList<>();
        commandRequest.setImageDescription(imageDescriptionList);

        ImageDescription firmwareImageDescription = new ImageDescription();
        firmwareImageDescription.setFileName("test filename url");
        firmwareImageDescription.setSizeBytes(0x1020);
        firmwareImageDescription.setStartAddress(0x200);
        firmwareImageDescription.setImageType(ImageDescription.ImageType.FirmwareImage);
        firmwareImageDescription.setRevisionMajor(1);
        firmwareImageDescription.setRevisionMinor(2);
        firmwareImageDescription.setRevisionDate(3);
        firmwareImageDescription.setRevisionBuildNum(4);
        imageDescriptionList.add(firmwareImageDescription);

        ImageDescription bootloaderImageDescription = new ImageDescription();
        bootloaderImageDescription.setFileName("bootloader filename url");
        bootloaderImageDescription.setSizeBytes(1);
        bootloaderImageDescription.setStartAddress(2);
        bootloaderImageDescription.setImageType(ImageDescription.ImageType.BootloaderImage);
        bootloaderImageDescription.setRevisionMajor(3);
        bootloaderImageDescription.setRevisionMinor(4);
        bootloaderImageDescription.setRevisionDate(5);
        bootloaderImageDescription.setRevisionBuildNum(6);
        imageDescriptionList.add(bootloaderImageDescription);


        //add list of CMIU
        List<Long> cmiuList = createFakeCmiuList(5);
        commandRequest.setMiuList(cmiuList);

        commandControlServiceController.sendCommand(commandRequest);

        //command should have been called 5 times
        ArgumentCaptor<MiuCommand> miuCommandArg = ArgumentCaptor.forClass(MiuCommand.class);
//        verify(miuRepo, times(cmiuList.size()) ).createMiuCommand(any(MiuCommand.class));
        verify(miuCommandService, times(cmiuList.size())).createMiuCommand(miuCommandArg.capture());


        List<MiuCommand> params = miuCommandArg.getAllValues();
        assertEquals(MiuId.valueOf(1), params.get(0).getMiuId());
        assertEquals(MiuId.valueOf(2), params.get(1).getMiuId());
        assertEquals(MiuId.valueOf(3), params.get(2).getMiuId());
        assertEquals(MiuId.valueOf(4), params.get(3).getMiuId());
        assertEquals(MiuId.valueOf(5), params.get(4).getMiuId());

        //for each param, verify that we have 2 image tags, 2 imagemeta tags, 1 firmware revision, 1 bootloader tag. total tags are 8;
        params.stream().forEach(command -> {
            byte[] rawPacket = command.getCommandParams();
            TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(rawPacket);

            MqttPacketHeaderData packetHeader = parsedTaggedPacket.findTag(MqttPacketHeaderData.class);
            assertNotNull(packetHeader);
            assertEquals(TaggedDataPacketType.CommandConfiguration, parsedTaggedPacket.getPacketType());

            List<TaggedData> secureDataTagList = PacketParser.parseNonUniqueSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));

            assertCommandTag(secureDataTagList, CmdId.UpdateImage);

            assertEquals(8, secureDataTagList.size());
            assertEquals(2, secureDataTagList.stream().filter(t -> t.getTagId() == TagId.Image).count());
            assertEquals(2, secureDataTagList.stream().filter(t -> t.getTagId() == TagId.ImageMetaData).count());
            assertEquals(1, secureDataTagList.stream().filter(t -> t.getTagId() == TagId.FirmwareRevision).count());
            assertEquals(1, secureDataTagList.stream().filter(t -> t.getTagId() == TagId.BootloaderVersion).count());

        });
    }

    /**
     * Utility function to create a list of CMIU where the command update will be applied to
     *
     * @param size size of cmiu list
     * @return list of cmiu
     */
    private static List<Long> createFakeCmiuList(int size)
    {
        List<Long> cmiuList = new ArrayList<>(size);
        for (int i = 0; i < size; i++)
        {
            cmiuList.add((long) i + 1);
        }

        return cmiuList;
    }

    @Test
    public void testSendRebootCmiuCommand() throws Exception
    {
        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(USERNAME);
        commandRequest.setCommandCmiu(CommandRequest.CommandTypeCmiu.COMMAND_REBOOT_CMIU);
        commandRequest.setMiuList(this.cmiuList);
        commandRequest.setExecutionTimeEpochSecond(Instant.now().getEpochSecond());

        List<MiuCommand> params = sendCommand(commandRequest);

        params.stream()
                .forEach(command -> {
                    List<TaggedData> tagList = getSecureDataTagList(command.getCommandParams());
                    assertEquals(1, tagList.size());
                    assertCommandTag(tagList, CmdId.RebootCmiu);
                });
    }

    @Test
    public void testMagSwipeEmulationCommand() throws Exception
    {
        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(USERNAME);
        commandRequest.setCommandCmiu(CommandRequest.CommandTypeCmiu.COMMAND_MAG_SWIPE_EMULATION);
        commandRequest.setMiuList(this.cmiuList);
        commandRequest.setExecutionTimeEpochSecond(Instant.now().getEpochSecond());

        List<MiuCommand> params = sendCommand(commandRequest);

        params.stream()
                .forEach(command -> {
                    List<TaggedData> tagList = getSecureDataTagList(command.getCommandParams());
                    assertEquals(1, tagList.size());
                    assertCommandTag(tagList, CmdId.MapSwipeEmulation);
                });
    }

    @Test
    public void testEraseDataLogCommand() throws Exception
    {
        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(USERNAME);
        commandRequest.setCommandCmiu(CommandRequest.CommandTypeCmiu.COMMAND_ERASE_DATALOG);
        commandRequest.setMiuList(this.cmiuList);
        commandRequest.setExecutionTimeEpochSecond(Instant.now().getEpochSecond());

        List<MiuCommand> params = sendCommand(commandRequest);

        params.stream()
                .forEach(command -> {
                    List<TaggedData> tagList = getSecureDataTagList(command.getCommandParams());
                    assertEquals(1, tagList.size());
                    assertCommandTag(tagList, CmdId.EraseDatalog);
                });
    }

    @Test
    public void testReportingRecordingIntervalCommand() throws Exception
    {
        CommandRequest commandRequest = new CommandRequest();
        commandRequest.setUser(USERNAME);
        commandRequest.setCommandCmiu(CommandRequest.CommandTypeCmiu.COMMAND_SET_RECORDING_REPORTING_INTERVALS);
        commandRequest.setMiuList(this.cmiuList);
        commandRequest.setRecordingStartTime(1);
        commandRequest.setRecordingInterval(2);
        commandRequest.setReportingStartMins(3);
        commandRequest.setReportingIntervalHours(4);
        commandRequest.setReportingRetries(5);
        commandRequest.setReportingTransmitWindows(6);
        commandRequest.setReportingQuietStart(7);
        commandRequest.setReportingQuietEnd(8);

        List<MiuCommand> params = sendCommand(commandRequest);

        params.stream()
                .forEach(command -> {
                    List<TaggedData> tagList = getSecureDataTagList(command.getCommandParams());
                    assertEquals(2, tagList.size());
                    assertCommandTag(tagList, CmdId.SetRecordingReportingInterval);
                    assertTagDataListHasTag(tagList, TagId.RecordingandReportingInterval, 1);

                    RecordingAndReportingIntervalData tag =
                            (RecordingAndReportingIntervalData) tagList
                                    .stream()
                                    .filter(t -> t.getTagId() == TagId.RecordingandReportingInterval).findFirst().get();

                    assertEquals(commandRequest.getRecordingInterval(), tag.getRecordingIntervalMins());
                    assertEquals(commandRequest.getRecordingStartTime(), tag.getRecordingStartTimeMins());

                    assertEquals(commandRequest.getReportingStartMins(), tag.getReportingStartMins());
                    assertEquals(commandRequest.getReportingIntervalHours(), tag.getReportingIntervalHours());
                    assertEquals(commandRequest.getReportingRetries(), tag.getReportingNumberOfRetries());
                    assertEquals(commandRequest.getReportingTransmitWindows(), tag.getReportingTransmitWindowsMins());
                    assertEquals(commandRequest.getReportingQuietStart(), tag.getReportingQuietStartMins());
                    assertEquals(commandRequest.getReportingQuietEnd(), tag.getReportingQuietEndMins());


                });
    }

    private static void assertCommandTag(final List<TaggedData> taggedDataList, CmdId commandId)
    {
        Optional<TaggedData> commandTag = taggedDataList.stream().filter(t -> t.getTagId() == TagId.Command).findAny();
        assertTrue(commandTag.isPresent());
        assertEquals(commandId.getId(), ((IntegerData) commandTag.get()).getValue());
    }

    private void assertTagDataListHasTag(final List<TaggedData> taggedDataList, TagId expectedTagId, int expectedCount )
    {
        assertEquals(expectedCount, taggedDataList.stream().filter(t -> t.getTagId() == expectedTagId).count());
    }

    /**
     * Call mocked sendCommand and retrieved parameters passed to miuRepository
     * @param commandRequest the command request to be send to the service controller (SUT)
     * @return the parameters received at the miuRepo interface
     */
    private List<MiuCommand> sendCommand(CommandRequest commandRequest)
    {
        commandControlServiceController.sendCommand(commandRequest);
        ArgumentCaptor<MiuCommand> miuCommandArg = ArgumentCaptor.forClass(MiuCommand.class);
        verify(miuCommandService, times(this.cmiuList.size()) ).createMiuCommand(miuCommandArg.capture());
        verify(awsQueueService).sendMessage(eq(MdceIpcPublishService.SEND_COMMAND_QUEUE), anyString());

        List<MiuCommand> params = miuCommandArg.getAllValues();
        assertEquals(MiuId.valueOf(1), params.get(0).getMiuId());
        assertEquals(MiuId.valueOf(2), params.get(1).getMiuId());
        assertEquals(MiuId.valueOf(3), params.get(2).getMiuId());
        assertEquals(MiuId.valueOf(4), params.get(3).getMiuId());
        assertEquals(MiuId.valueOf(5), params.get(4).getMiuId());

        return params;
    }

    private static List<TaggedData> getSecureDataTagList(byte[] rawPacket)
    {
        TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(rawPacket);
        assertEquals(TaggedDataPacketType.CommandConfiguration, parsedTaggedPacket.getPacketType());

        MqttPacketHeaderData packetHeader = parsedTaggedPacket.findTag(MqttPacketHeaderData.class);
        assertNotNull(packetHeader);

        return PacketParser.parseNonUniqueSecureTag(parsedTaggedPacket.findTag(SecureBlockArrayData.class));
    }
}