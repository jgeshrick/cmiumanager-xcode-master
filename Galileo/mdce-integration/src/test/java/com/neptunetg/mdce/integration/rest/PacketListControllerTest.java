/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.amazonaws.services.dynamodbv2.document.Table;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.data.miu.heard.MiuUpdateRepository;
import com.neptunetg.mdce.common.data.PacketRepository;
import com.neptunetg.mdce.common.data.model.GatewayPacketReceivedDynamoItem;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import com.neptunetg.mdce.integration.data.gateway.GatewayDetails;
import com.neptunetg.mdce.integration.data.gateway.GatewayId;
import com.neptunetg.mdce.integration.data.gateway.GatewayRepository;
import com.neptunetg.mdce.integration.data.miu.MiuDetails;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.mdce.integration.data.miu.MiuSearchRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Ignore
@RunWith(MockitoJUnitRunner.class)  //cause mocks to be created and injected
public class PacketListControllerTest extends AbstractTokenManagingControllerTest {

    @InjectMocks
    private PacketListController beingTested;

    @Mock
    private MiuUpdateRepository miuUpdateRepository;

    @Mock
    private MiuSearchRepository miuSearchRepository;

    @Mock
    private GatewayRepository gatewayRepository;


    @Mock
    private PacketRepository packetRepository;

    @Test
    public void testGetPacketsForMiu() throws Exception {


        ZonedDateTime now = ZonedDateTime.now();
        String minInsertDate = now.toString();

        List<MiuPacketReceivedDynamoItem> itemList = new ArrayList<>();
        MiuPacketReceivedDynamoItem element = new MiuPacketReceivedDynamoItem();
        element.setMiuId(1);
        byte[] byteArray = {0x0, 0x1};
        element.setPacketData(byteArray);
        element.setPacketType("CMIU:1");
        itemList.add(0, element);

        List<Table> tables = Arrays.asList();

        MiuDetails miu = new MiuDetails(MiuId.valueOf(1), SiteId.valueOf(1), Instant.now(), Instant.now(), Instant.now(), MiuType.CMIU, null);

        when(miuSearchRepository.getMiusForUtility(SiteId.valueOf(1))).thenReturn(Arrays.asList(miu));

        when(packetRepository.getRelevantTablesForMiuPacketQuery(now.toInstant(), now.toInstant())).thenReturn(tables);

        when(packetRepository.streamMiuPacketsInsertedInDateRange(1, now.toInstant(), now.toInstant(), tables)).thenReturn(itemList.stream());

        List<GatewayPacketReceivedDynamoItem> gatewayItems = new ArrayList<>();
        GatewayPacketReceivedDynamoItem gatewayPacketReceivedDynamoItem = mock(GatewayPacketReceivedDynamoItem.class);
        when(gatewayPacketReceivedDynamoItem.getPacketType()).thenReturn("CMIU:1");
        when(gatewayPacketReceivedDynamoItem.getCollectorId()).thenReturn("CMIU:1");
        //gatewayPacketReceivedDynamoItem.setSiteId(1);
        ByteBuffer mockByteBuffer = mock(ByteBuffer.class);
        when(mockByteBuffer.array()).thenReturn(byteArray);
        gatewayPacketReceivedDynamoItem.setPacketData(mockByteBuffer);


        gatewayItems.add(0, gatewayPacketReceivedDynamoItem);

        GatewayId gwId = GatewayId.valueOf("1_GW_21431");

        GatewayDetails gw = new GatewayDetails(gwId, SiteId.valueOf(1), "1", "2", "3", "4", "5", true);

        when(gatewayRepository.getGatewaysForUtility(SiteId.valueOf(1))).thenReturn(Arrays.asList(gw));

        when(packetRepository.getRelevantTablesForGatewayPacketQuery(now.toInstant(), now.toInstant())).thenReturn(tables);

        when(packetRepository.streamGatewayPacketsInsertedInDateRange(gwId.toString(), now.toInstant(), now.toInstant(), tables)).thenReturn(gatewayItems.stream());

        ByteArrayOutputStream dataOut = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(dataOut);

        when (response.getWriter()).thenReturn(writer);

        beingTested.getPacketsForMiu(minInsertDate, minInsertDate, null, false, request, response);

        ObjectMapper jsonResponseMapper = new ObjectMapper();

        Map<String, Object> result = jsonResponseMapper.readValue(dataOut.toByteArray(), Map.class);

        //TODO: check server date
        List<Object> mius = (List<Object>) result.get("mius");
        assertNotNull(mius);
        assertEquals(1, mius.size());

        Map<String, Object> actual = (Map<String, Object>) mius.get(0);
        assertEquals(1, actual.get("miu_id"));
        List<Object> packets = (List<Object>) actual.get("packets");
        assertEquals(1, packets.size());
        Map<String, Object> data = (Map<String, Object>) packets.get(0);
        assertEquals(1, data.get("packet_type"));


        List<Object> gateways = (List<Object>) result.get("gateways");
        assertNotNull(gateways);
        assertEquals(1, gateways.size());


    }
}