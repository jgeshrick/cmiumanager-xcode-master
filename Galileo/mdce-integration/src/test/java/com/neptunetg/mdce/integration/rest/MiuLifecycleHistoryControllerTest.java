/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.AddMiuLifecycleStateResult;
import com.neptunetg.common.data.miu.lifecycle.model.HistoricMiuLifecycleState;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleService;
import com.neptunetg.mdce.common.api.domain.RestClientId;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import com.neptunetg.mdce.integration.data.miu.MiuSearchRepository;
import com.neptunetg.mdce.integration.rest.json.miulifecycle.JsonHistoricMiuLifecycleState;
import com.neptunetg.mdce.integration.rest.json.miulifecycle.JsonSetMiuLifecycleHistoryRequest;
import com.neptunetg.mdce.integration.rest.json.miulifecycle.JsonSetMiuLifecycleHistoryResult;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)  //cause mocks to be created and injected
public class MiuLifecycleHistoryControllerTest
{
    @InjectMocks
    private MiuLifecycleHistoryController controller;

    @Mock
    private MiuLifecycleService miuLifecycleService;

    @Mock
    private HttpServletRequest request;

    @Captor
    private ArgumentCaptor<Map<String, String>> captor;

    @Before
    public void initialize()
    {
        when(request.getAttribute("validatedClientId")).thenReturn(RestClientId.forUtility(0)); // Universal site
    }

    @Test(expected = NotAuthorizedException.class)
    public void testAddMiuLifecycleHistory_WithNonUniversalSiteId_RejectsRequest() throws Exception
    {
        when(request.getAttribute("validatedClientId")).thenReturn(RestClientId.forUtility(1)); // Site ID 1

        JsonSetMiuLifecycleHistoryRequest jsonRequest = new JsonSetMiuLifecycleHistoryRequest();
        jsonRequest.setLifecycleStates(new ArrayList<>());

        controller.addMiuLifecycleHistory(jsonRequest, request);
    }

    @Test
    public void testAddMiuLifecycleHistory_WithInvalidLifecycleState_ReturnsErrorDetails() throws Exception
    {
        List<JsonHistoricMiuLifecycleState> statesToAdd = new ArrayList<>();
        statesToAdd.add(new JsonHistoricMiuLifecycleState(1, MiuId.valueOf(400000000), "INVALID STATE", Instant.now()));
        JsonSetMiuLifecycleHistoryRequest jsonRequest = new JsonSetMiuLifecycleHistoryRequest();
        jsonRequest.setLifecycleStates(statesToAdd);

        when(miuLifecycleService.addMiuLifecycleStates(any())).thenReturn(new ArrayList<>());

        final JsonSetMiuLifecycleHistoryResult result = controller.addMiuLifecycleHistory(jsonRequest, request);

        assertEquals(0, result.getSucceededCount());
        assertEquals(1, result.getErrorCount());
        assertEquals(1, result.getErrorDetails().get(0).getReferenceId());
    }

}
