/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.mdce.common.api.data.TokenInfo;
import com.neptunetg.mdce.common.api.domain.RestClientId;
import com.neptunetg.mdce.common.api.domain.auth.TokenManagerService;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import com.neptunetg.mdce.integration.rest.json.GenerateTokenResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)  //cause mocks to be created
public class GenerateTokenControllerTest {

    @InjectMocks
    private GenerateTokenController beingTested;

    @Mock
    private TokenManagerService tokenManagerService;

    @Mock
    private HttpServletRequest request;

    @Before
    public void InitializeTest()
    {
        when(request.getHeader(anyString())).thenReturn(null);
        when(request.getRemoteAddr()).thenReturn("localhost");
    }

    @Test
    public void testGetTokenSuccess() throws Exception {
        final SiteId siteId = SiteId.valueOf(12452);
        final RestClientId restClientId = RestClientId.forUtility(siteId.numericValue());
        final String requestIp = "192.155.1.54";
        final String partnerKey = "ABCDEF0123456789ABCDEF0123456789";

        final TokenInfo token = new TokenInfo(new byte[]{0x12, 0x34, 0x56, 0x78, 0x12, 0x34, 0x56, 0x78},
                restClientId, Instant.EPOCH, partnerKey, Instant.now().plus(1, ChronoUnit.HOURS), requestIp);

        when(request.getRemoteAddr()).thenReturn(requestIp);
        when(tokenManagerService.generateToken(partnerKey, siteId.numericValue(), requestIp)).thenReturn(token);

        GenerateTokenResponse resp = beingTested.getToken(partnerKey, siteId, request);
        assertTrue(resp.isAuthenticated());
        assertEquals(token.getTokenString(), resp.getAuthToken());
        assertEquals(token.getCreated().toString(), resp.getAuthenticatedAt());
        assertEquals(token.getExpiry().toString(), resp.getExpiryDate());

    }

    @Test
    public void testGetTokenUnsuccessful() throws Exception {
        SiteId siteId = SiteId.UNIVERSAL;
        String requestIp = "192.155.1.54";
        String partnerKey = "12345678901234567890123456789012";

        when(request.getRemoteAddr()).thenReturn(requestIp);

        when(tokenManagerService.generateToken(partnerKey, siteId.numericValue(), requestIp)).thenThrow(new NotAuthorizedException("Testing..."));
        NotAuthorizedException capturedException = null;

        try {
            beingTested.getToken(partnerKey, siteId, request);
            fail();
        } catch (NotAuthorizedException e) {
            capturedException = e;
        }

        assertNotNull(capturedException);
        assertEquals("Testing...", capturedException.getMessage());

    }


    @Test
    public void testResponseNotAuthorizedError() throws Exception {

        ResponseEntity<GenerateTokenResponse> generateTokenResponseResponseEntity =
                beingTested.responseNotAuthorizedError(new NotAuthorizedException(HttpStatus.UNAUTHORIZED.getReasonPhrase()));

        assertNotNull(generateTokenResponseResponseEntity);
        assertEquals(generateTokenResponseResponseEntity.getStatusCode(), HttpStatus.UNAUTHORIZED);
        assertFalse(generateTokenResponseResponseEntity.getBody().isAuthenticated());

    }

    @Test
    public void getToken_WhenSuppliedStringIsNotHex_DoesNotCallTokenManagerService() throws NotAuthorizedException, BadRequestException
    {
        // Test that the TokenManagerService isn't called if the supplied token is invalid.
        SiteId siteId = SiteId.UNIVERSAL;
        final String badParterString = "eb750ee73b5cf9dX4ec78a0b23616e9b";
        TokenInfo badToken = new TokenInfo(new byte[0], new RestClientId(0, 0), Instant.now(), "", Instant.now(), "");
        when(tokenManagerService.generateToken(anyString(), anyInt(), anyString())).thenReturn(badToken);

        try
        {
            beingTested.getToken(badParterString, siteId, request);
        }
        catch (Exception e)
        {
        }

        verify(tokenManagerService, never()).generateToken(anyString(), anyInt(), anyString());
    }

    @Test(expected = BadRequestException.class)
    public void getToken_WhenSuppliedStringIsNotHex_ThrowsBadRequest() throws NotAuthorizedException, BadRequestException
    {
        // Verify that a BadRequestException is thrown if the partner string is invalid.
        SiteId siteId = SiteId.UNIVERSAL;
        final String badParterString = "eb750ee73b5cf9dX4ec78a0b23616e9b";
        TokenInfo badToken = new TokenInfo(new byte[0], new RestClientId(0, 0), Instant.now(), "", Instant.now(), "");
        when(tokenManagerService.generateToken(anyString(), anyInt(), anyString())).thenReturn(badToken);

        beingTested.getToken(badParterString, siteId, request);
    }
}