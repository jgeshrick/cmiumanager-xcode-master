/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.aws.sqs;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test parsing of SQS message originated from SNS. This is also capture a typical SNS message format for future reference.
 */
@RunWith(MockitoJUnitRunner.class)  //cause mocks to be created
public class DataPipelineAlertManagerTest
{
    private final static String testSnsMessage = "{\n" +
            "  \"Type\" : \"Notification\",\n" +
            "  \"MessageId\" : \"b4a7886d-cd11-5a73-8784-c6c4b10cac3e\",\n" +
            "  \"TopicArn\" : \"arn:aws:sns:us-west-2:934427565871:dev1-datapipeline-sns-topic\",\n" +
            "  \"Subject\" : \"subject\",\n" +
            "  \"Message\" : \"message asdf\",\n" +
            "  \"Timestamp\" : \"2015-10-27T13:19:25.568Z\",\n" +
            "  \"SignatureVersion\" : \"1\",\n" +
            "  \"Signature\" : \"DTwQU7DuEscooRo/QaLbrEWZRfL8wog9RgbrB/RL9Glo+LPTGJ5Vj4LKVbBDaXsrHprmJMSdhhgdA5zhYY7RJK31XXkj1Y1jJnF3CMUa2k1aa4FVYIVk6ex1jZoNxYVwNcRwRBJbwm8GfornYoyHZ1uJtUR7WIKEkFoqq/Eze9qWcgBWsD7rMXOta7CPURbEVJykWIxeDQpjHhb+NVwEhku5tYhcHlFRLLv7KZptxBl3d3fg1oBkDNJEsn2kDmyRcfeh2woopSKkOu2jdBBcM09iT8+ciS5jjC8f33bKF0VecmKhEyr9Iq45W3R2fmrabT9ID/crA7ixsQyV2mIKHw==\",\n" +
            "  \"SigningCertURL\" : \"https://sns.us-west-2.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem\",\n" +
            "  \"UnsubscribeURL\" : \"https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:934427565871:dev1-datapipeline-sns-topic:81ba4b17-98b4-47d4-af74-89ad20624bed\",\n" +
            "  \"MessageAttributes\" : {\n" +
            "    \"AWS.SNS.MOBILE.MPNS.Type\" : {\"Type\":\"String\",\"Value\":\"token\"},\n" +
            "    \"AWS.SNS.MOBILE.MPNS.NotificationClass\" : {\"Type\":\"String\",\"Value\":\"realtime\"},\n" +
            "    \"AWS.SNS.MOBILE.WNS.Type\" : {\"Type\":\"String\",\"Value\":\"wns/badge\"}\n" +
            "  }\n" +
            "}";

    @Mock
    private MdceIpcSubscribeService ipcSubscribeService;

    @Mock
    private AlertService alertService;

    @Test
    public void testParseJsonNode() throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readValue(testSnsMessage, JsonNode.class);

        assertEquals("message asdf", jsonNode.get("Message").textValue());
        assertEquals("subject", jsonNode.get("Subject").textValue());
    }

    @Test
    public void testCanReceiveSnsMessage()
    {
        DataPipelineAlertManager dataPipelineAlertManager = new DataPipelineAlertManager(ipcSubscribeService, alertService);

        assertTrue(dataPipelineAlertManager.processReceivedMessage(testSnsMessage, "123456", "abcdef", new AtomicBoolean(false)));

    }


}