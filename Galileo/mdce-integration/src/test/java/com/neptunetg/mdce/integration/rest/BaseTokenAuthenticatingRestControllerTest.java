/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import static org.junit.Assert.*;

/**
 * Test various methods of this base class
 */
public class BaseTokenAuthenticatingRestControllerTest extends BaseTokenAuthenticatingRestController
{

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testDateParsing() throws Exception
    {

        assertEquals(ZonedDateTime.of(2016, 1, 1, 13, 41, 21, 14145, ZoneOffset.ofHours(-6)),
                parseMinimumQueryDateMandatory("2016-01-01T13:41:21.000014145-06:00"));

        assertEquals(ZonedDateTime.of(2016, 1, 1, 13, 41, 21, 14145, ZoneOffset.ofHours(-6)),
                parseMaximumQueryDateMandatory("2016-01-01T13:41:21.000014145-06:00"));

        assertEquals(ZonedDateTime.of(2016, 1, 1, 13, 41, 21, 14145, ZoneOffset.ofHours(-6)),
                parseMinimumQueryDateOptional("2016-01-01T13:41:21.000014145-06:00"));

        assertEquals(ZonedDateTime.of(2016, 1, 1, 13, 41, 21, 14145, ZoneOffset.ofHours(-6)),
                parseMaximumQueryDateOptional("2016-01-01T13:41:21.000014145-06:00"));


        ZonedDateTime zdt = ZonedDateTime.of(2016, 1, 1, 13, 41, 21, 14145, ZoneId.of("US/Central"));

        assertEquals(zdt,
                parseMaximumQueryDateOptional(null, zdt));


        assertEquals(ZonedDateTime.of(2016, 1, 1, 0, 0, 0, 0, ZoneId.of("US/Central")),
                parseMinimumQueryDateMandatory("2016-01-01"));

        assertEquals(ZonedDateTime.of(2016, 1, 2, 0, 0, 0, 0, ZoneId.of("US/Central")),
                parseMaximumQueryDateMandatory("2016-01-01"));

    }

    @Test
    public void testMandatoryMinimumDate() throws Exception
    {
        thrown.expect(BadRequestException.class);
        parseMinimumQueryDateMandatory(null);
    }
}