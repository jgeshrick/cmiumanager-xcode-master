-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mdce
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mdce` ;

-- -----------------------------------------------------
-- Schema mdce
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mdce` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `mdce` ;

-- -----------------------------------------------------
-- Table `mdce`.`reporting_plans`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`reporting_plans` ;

CREATE TABLE IF NOT EXISTS `mdce`.`reporting_plans` (
  `reporting_plan_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(45) NULL,
  `start_mins` INT NULL,
  `interval_mins` INT NULL,
  `transmit_window_mins` INT NULL,
  `quiet_start_mins` INT NULL,
  `quiet_end_mins` INT NULL,
  PRIMARY KEY (`reporting_plan_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`data_collection_plans`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`data_collection_plans` ;

CREATE TABLE IF NOT EXISTS `mdce`.`data_collection_plans` (
  `data_collection_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(45) NULL,
  `start_time_secs` INT NULL,
  `interval_secs` INT NULL,
  PRIMARY KEY (`data_collection_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`utility_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`utility_list` ;

CREATE TABLE IF NOT EXISTS `mdce`.`utility_list` (
  `utility_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(50) NULL,
  `last_data_request` DATETIME NULL,
  PRIMARY KEY (`utility_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`billing_plans`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`billing_plans` ;

CREATE TABLE IF NOT EXISTS `mdce`.`billing_plans` (
  `billing_plan_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(45) NULL,
  `start_day` INT NULL,
  `monthly_bytes` INT NULL,
  PRIMARY KEY (`billing_plan_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`miu_details`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`miu_details` ;

CREATE TABLE IF NOT EXISTS `mdce`.`miu_details` (
  `miu_id` INT UNSIGNED NOT NULL,
  `utility_id` INT UNSIGNED NOT NULL,
  `last_heard_time` DATETIME NULL,
  `next_sync_time` DATETIME NULL,
  `miu_type` INT NULL,
  `billing_plan_id` INT UNSIGNED NOT NULL,
  `reporting_plan_id` INT UNSIGNED NOT NULL,
  `temp_reporting_plan_id` INT UNSIGNED NOT NULL,
  `temp_reporting_plan_start_time` DATETIME NULL,
  `temp_reporting_plan_end_time` DATETIME NULL,
  `data_collection_id` INT UNSIGNED NOT NULL,
  `temp_data_collection_id` INT UNSIGNED NOT NULL,
  `temp_data_collection_start_time` DATETIME NULL,
  `temp_data_collection_end_time` DATETIME NULL,
  PRIMARY KEY (`miu_id`, `utility_id`, `billing_plan_id`, `reporting_plan_id`, `temp_reporting_plan_id`, `data_collection_id`, `temp_data_collection_id`),
  INDEX `fk_miu_details_reporting_plans1_idx` (`reporting_plan_id` ASC),
  INDEX `fk_miu_details_reporting_plans2_idx` (`temp_reporting_plan_id` ASC),
  INDEX `fk_miu_details_data_collection_plans1_idx` (`data_collection_id` ASC),
  INDEX `fk_miu_details_data_collection_plans2_idx` (`temp_data_collection_id` ASC),
  INDEX `fk_miu_details_utilities1_idx` (`utility_id` ASC),
  INDEX `fk_miu_details_billing_plans1_idx` (`billing_plan_id` ASC),
  CONSTRAINT `fk_miu_details_reporting_plans1`
  FOREIGN KEY (`reporting_plan_id`)
  REFERENCES `mdce`.`reporting_plans` (`reporting_plan_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_miu_details_reporting_plans2`
  FOREIGN KEY (`temp_reporting_plan_id`)
  REFERENCES `mdce`.`reporting_plans` (`reporting_plan_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_miu_details_data_collection_plans1`
  FOREIGN KEY (`data_collection_id`)
  REFERENCES `mdce`.`data_collection_plans` (`data_collection_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_miu_details_data_collection_plans2`
  FOREIGN KEY (`temp_data_collection_id`)
  REFERENCES `mdce`.`data_collection_plans` (`data_collection_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_miu_details_utilities1`
  FOREIGN KEY (`utility_id`)
  REFERENCES `mdce`.`utility_list` (`utility_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_miu_details_billing_plans1`
  FOREIGN KEY (`billing_plan_id`)
  REFERENCES `mdce`.`billing_plans` (`billing_plan_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`tag_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`tag_list` ;

CREATE TABLE IF NOT EXISTS `mdce`.`tag_list` (
  `tag_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL,
  `public` TINYINT(1) NULL,
  PRIMARY KEY (`tag_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`miu_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`miu_tags` ;

CREATE TABLE IF NOT EXISTS `mdce`.`miu_tags` (
  `miu_id` INT UNSIGNED NOT NULL,
  `tag_id` INT UNSIGNED NOT NULL,
  INDEX `fk_miu_tags_miu_details_idx` (`miu_id` ASC),
  INDEX `fk_miu_tags_tag_list_idx` (`tag_id` ASC),
  PRIMARY KEY (`tag_id`, `miu_id`),
  CONSTRAINT `fk_miu_tags_miu_details`
  FOREIGN KEY (`miu_id`)
  REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_miu_tags_tag_list`
  FOREIGN KEY (`tag_id`)
  REFERENCES `mdce`.`tag_list` (`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`utility_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`utility_tags` ;

CREATE TABLE IF NOT EXISTS `mdce`.`utility_tags` (
  `utility_id` INT UNSIGNED NOT NULL,
  `tag_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`utility_id`, `tag_id`),
  INDEX `fk_utility_tags_tag_list1_idx` (`tag_id` ASC),
  CONSTRAINT `fk_utility_tags_tag_list1`
  FOREIGN KEY (`tag_id`)
  REFERENCES `mdce`.`tag_list` (`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_utility_tags_utilities1`
  FOREIGN KEY (`utility_id`)
  REFERENCES `mdce`.`utility_list` (`utility_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`user_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`user_list` ;

CREATE TABLE IF NOT EXISTS `mdce`.`user_list` (
  `user_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(45) NULL,
  `user_email` VARCHAR(45) NULL,
  PRIMARY KEY (`user_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`user_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`user_tags` ;

CREATE TABLE IF NOT EXISTS `mdce`.`user_tags` (
  `user_id` INT UNSIGNED NOT NULL,
  `tag_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `tag_id`),
  INDEX `fk_user_tags_tag_list_idx` (`tag_id` ASC),
  CONSTRAINT `fk_user_tags_user_list`
  FOREIGN KEY (`user_id`)
  REFERENCES `mdce`.`user_list` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_tags_tag_list`
  FOREIGN KEY (`tag_id`)
  REFERENCES `mdce`.`tag_list` (`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`kpi_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`kpi_list` ;

CREATE TABLE IF NOT EXISTS `mdce`.`kpi_list` (
  `kpi_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(45) NULL,
  `params` MEDIUMTEXT NULL,
  `config` MEDIUMTEXT NULL,
  `kpi_sql` MEDIUMTEXT NULL,
  `grid_config` MEDIUMTEXT NULL,
  `grid_sql` VARCHAR(45) NULL,
  `details_grid_config` MEDIUMTEXT NULL,
  `details_grid_sql` MEDIUMTEXT NULL,
  `last_update_sql` MEDIUMTEXT NULL,
  PRIMARY KEY (`kpi_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`user_dashboards`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`user_dashboards` ;

CREATE TABLE IF NOT EXISTS `mdce`.`user_dashboards` (
  `user_dashboard_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `view_name` VARCHAR(45) NULL,
  `public` TINYINT(1) NULL,
  PRIMARY KEY (`user_dashboard_id`, `user_id`),
  INDEX `fk_user_dashboards_user_list_idx` (`user_id` ASC),
  CONSTRAINT `fk_user_dashboards_user_list0`
  FOREIGN KEY (`user_id`)
  REFERENCES `mdce`.`user_list` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`user_dashboards_kpis`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`user_dashboards_kpis` ;

CREATE TABLE IF NOT EXISTS `mdce`.`user_dashboards_kpis` (
  `user_dashboards_kpis_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_dashboard_id` INT UNSIGNED NOT NULL,
  `kpi_id` INT UNSIGNED NOT NULL,
  `grid_x` INT NULL,
  `grid_y` INT NULL,
  `colspan` INT NULL,
  `rowspan` INT NULL,
  `params_override` MEDIUMTEXT NULL,
  PRIMARY KEY (`user_dashboards_kpis_id`),
  INDEX `fk_user_dashboards_dashboard_list_idx` (`kpi_id` ASC),
  INDEX `fk_user_views_dashboards_user_views1_idx` (`user_dashboard_id` ASC),
  CONSTRAINT `fk_user_dashboards_dashboard_list`
  FOREIGN KEY (`kpi_id`)
  REFERENCES `mdce`.`kpi_list` (`kpi_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_views_dashboards_user_views1`
  FOREIGN KEY (`user_dashboard_id`)
  REFERENCES `mdce`.`user_dashboards` (`user_dashboard_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`user_dashboards_kpis_default_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`user_dashboards_kpis_default_tags` ;

CREATE TABLE IF NOT EXISTS `mdce`.`user_dashboards_kpis_default_tags` (
  `user_dashboards_kpis_id` INT UNSIGNED NOT NULL,
  `tag_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`user_dashboards_kpis_id`, `tag_id`),
  INDEX `fk_user_dashboards_default_tags_tag_list_idx` (`tag_id` ASC),
  CONSTRAINT `fk_user_dashboards_default_tags_user_views_dashboards`
  FOREIGN KEY (`user_dashboards_kpis_id`)
  REFERENCES `mdce`.`user_dashboards_kpis` (`user_dashboards_kpis_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_views_dashboards_default_tags_tag_list`
  FOREIGN KEY (`tag_id`)
  REFERENCES `mdce`.`tag_list` (`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`event_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`event_list` ;

CREATE TABLE IF NOT EXISTS `mdce`.`event_list` (
  `event_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(45) NULL,
  `start_date_time` DATETIME NULL,
  `interval_type` VARCHAR(1) NULL,
  `interval_value` INT NULL,
  `event_sql` MEDIUMTEXT NULL,
  `last_run_time` DATETIME NULL,
  `next_run_time` DATETIME NULL,
  PRIMARY KEY (`event_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`event_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`event_data` ;

CREATE TABLE IF NOT EXISTS `mdce`.`event_data` (
  `event_data_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `event_id` INT UNSIGNED NOT NULL,
  `n1` DECIMAL(10,0) NULL,
  `n2` DECIMAL(10,0) NULL,
  `d1` DATE NULL,
  `d2` DATE NULL,
  `dt1` DATETIME NULL,
  `dt2` DATETIME NULL,
  `s1` VARCHAR(100) NULL,
  `s2` VARCHAR(100) NULL,
  `i1` INT NULL,
  `i2` INT NULL,
  `u1` INT UNSIGNED NULL,
  `u2` INT NULL,
  INDEX `fk_event_data_event_list1_idx` (`event_id` ASC),
  PRIMARY KEY (`event_data_id`),
  CONSTRAINT `fk_event_data_event_list1`
  FOREIGN KEY (`event_id`)
  REFERENCES `mdce`.`event_list` (`event_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`miu_data_usage`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`miu_data_usage` ;

CREATE TABLE IF NOT EXISTS `mdce`.`miu_data_usage` (
  `miu_id` INT UNSIGNED NOT NULL,
  `data_usage_date` DATE NOT NULL,
  `bytes_used_today` INT NULL,
  `bytes_used_month_to_date` INT NULL,
  PRIMARY KEY (`miu_id`, `data_usage_date`),
  CONSTRAINT `fk_miu_data_usage_miu_details1`
  FOREIGN KEY (`miu_id`)
  REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`miu_commands`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`miu_commands` ;

CREATE TABLE IF NOT EXISTS `mdce`.`miu_commands` (
  `miu_command_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `miu_id` INT UNSIGNED NOT NULL,
  `command_type` INT NULL,
  `command_params` BLOB NULL,
  `command_entered_time` DATETIME NULL,
  `command_fulfilled_time` DATETIME NULL,
  PRIMARY KEY (`miu_command_id`, `miu_id`),
  INDEX `fk_miu_commands_miu_details1_idx` (`miu_id` ASC),
  CONSTRAINT `fk_miu_commands_miu_details1`
  FOREIGN KEY (`miu_id`)
  REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`audit_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`audit_list` ;

CREATE TABLE IF NOT EXISTS `mdce`.`audit_list` (
  `audit_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `audit_type` INT NULL,
  `audit_params` INT NULL,
  `audit_user_id` INT UNSIGNED NULL,
  `audit_time` DATETIME NULL,
  `miu_id` INT UNSIGNED NULL,
  PRIMARY KEY (`audit_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`miu_heard_packets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`miu_heard_packets` ;

CREATE TABLE IF NOT EXISTS `mdce`.`miu_heard_packets` (
  `miu_heard_packets_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `miu_id` INT UNSIGNED NOT NULL,
  `heard_time` DATETIME NULL,
  `rssi` INT NULL,
  PRIMARY KEY (`miu_heard_packets_id`, `miu_id`),
  INDEX `fk_miu_heard_packets_miu_details1_idx` (`miu_id` ASC),
  CONSTRAINT `fk_miu_heard_packets_miu_details1`
  FOREIGN KEY (`miu_id`)
  REFERENCES `mdce`.`miu_details` (`miu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mdce`.`user_public_dashboards`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdce`.`user_public_dashboards` ;

CREATE TABLE IF NOT EXISTS `mdce`.`user_public_dashboards` (
  `user_id` INT UNSIGNED NOT NULL,
  `user_dashboard_id` INT UNSIGNED NOT NULL,
  `dashboard_order` INT NULL,
  PRIMARY KEY (`user_id`, `user_dashboard_id`),
  INDEX `fk_user_public_dashboards_user_dashboards_idx` (`user_dashboard_id` ASC),
  CONSTRAINT `fk_user_public_dashboards_user_list`
  FOREIGN KEY (`user_id`)
  REFERENCES `mdce`.`user_list` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_public_dashboards_user_dashboards`
  FOREIGN KEY (`user_dashboard_id`)
  REFERENCES `mdce`.`user_dashboards` (`user_dashboard_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

USE `mdce` ;

-- -----------------------------------------------------
-- procedure split_list
-- -----------------------------------------------------

USE `mdce`;
DROP procedure IF EXISTS `mdce`.`split_list`;

DELIMITER $$
USE `mdce`$$
create procedure split_list (list_string long varchar, string_delimiter varchar(255))
begin
DECLARE iRow INT;
DECLARE iPos INT;
DECLARE iPos2 INT;

DROP TEMPORARY TABLE IF EXISTS temp_split_list;
CREATE TEMPORARY TABLE temp_split_list
(row_number INT,
 row_value VARCHAR(4000)
) ENGINE=Memory;

SET iRow  = 0;
SET iPos  = 1;
SET iPos2 = Locate(string_delimiter, list_string, iPos);

WHILE iPos2 > 0 DO

SET iRow = iRow + 1;
INSERT INTO temp_split_list (row_number,
                             row_value)
VALUES (iRow,
        substring(list_string, iPos, iPos2 - iPos));

SET iPos = iPos2 + length(string_delimiter);
SET iPos2 = Locate(string_delimiter, list_string, iPos);
END WHILE;

/*
 * add any text left over
 */
IF iPos <= length(list_string) THEN
SET iRow = iRow + 1;
INSERT INTO temp_split_list (row_number,
                             row_value)
VALUES (iRow,
        substr(substring(list_string, iPos), 1, 4000));
END IF;

end;$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure build_tag_filter
-- -----------------------------------------------------

USE `mdce`;
DROP procedure IF EXISTS `mdce`.`build_tag_filter`;

DELIMITER $$
USE `mdce`$$
create procedure build_tag_filter (tag_list long varchar)
begin

/*
 * build table to store results
 */
DROP TEMPORARY TABLE IF EXISTS temp_tag_mius;
CREATE TEMPORARY TABLE temp_tag_mius
(miu_id INT UNSIGNED,
  PRIMARY KEY (miu_id)
) ENGINE=Memory;

/*
   * split the list
   */
CALL split_list_uint(tag_list, ',');

/*
 * now insert any packets's for an utilities that have this tag
 */
INSERT INTO temp_tag_mius
(miu_id)
  (SELECT DISTINCT miu_id
   FROM miu_details m,
     utility_tags t,
     temp_split_list l
   WHERE m.utility_id = t.utility_id AND
         t.tag_id = l.row_value);

/*
 * now insert any packets's that have this tag for themselves
 */
INSERT IGNORE INTO temp_tag_mius
(miu_id)
  (SELECT DISTINCT miu_id
   FROM miu_details m,
     miu_tags t,
     temp_split_list l
   WHERE m.utility_id = t.utility_id AND
         t.tag_id = l.row_value);
end;$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure split_list_int
-- -----------------------------------------------------

USE `mdce`;
DROP procedure IF EXISTS `mdce`.`split_list_int`;

DELIMITER $$
USE `mdce`$$
create procedure split_list_int (list_string long varchar, string_delimiter varchar(255))
begin
DECLARE iRow INT;
DECLARE iPos INT;
DECLARE iPos2 INT;

DROP TEMPORARY TABLE IF EXISTS temp_split_list;
CREATE TEMPORARY TABLE temp_split_list
(row_number INT,
 row_value INT
) ENGINE=Memory;

SET iRow  = 0;
SET iPos  = 1;
SET iPos2 = Locate(string_delimiter, list_string, iPos);

WHILE iPos2 > 0 DO

SET iRow = iRow + 1;
INSERT INTO temp_split_list (row_number,
                             row_value)
VALUES (iRow,
        substring(list_string, iPos, iPos2 - iPos));

SET iPos = iPos2 + length(string_delimiter);
SET iPos2 = Locate(string_delimiter, list_string, iPos);
END WHILE;

/*
 * add any text left over
 */
IF iPos <= length(list_string) THEN
SET iRow = iRow + 1;
INSERT INTO temp_split_list (row_number,
                             row_value)
VALUES (iRow,
        substr(substring(list_string, iPos), 1, 4000));
END IF;

end;$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure split_list_uint
-- -----------------------------------------------------

USE `mdce`;
DROP procedure IF EXISTS `mdce`.`split_list_uint`;

DELIMITER $$
USE `mdce`$$
create procedure split_list_uint (list_string long varchar, string_delimiter varchar(255))
begin
DECLARE iRow INT;
DECLARE iPos INT;
DECLARE iPos2 INT;

DROP TEMPORARY TABLE IF EXISTS temp_split_list;
CREATE TEMPORARY TABLE temp_split_list
(row_number INT UNSIGNED,
 row_value INT
) ENGINE=Memory;

SET iRow  = 0;
SET iPos  = 1;
SET iPos2 = Locate(string_delimiter, list_string, iPos);

WHILE iPos2 > 0 DO

SET iRow = iRow + 1;
INSERT INTO temp_split_list (row_number,
                             row_value)
VALUES (iRow,
        substring(list_string, iPos, iPos2 - iPos));

SET iPos = iPos2 + length(string_delimiter);
SET iPos2 = Locate(string_delimiter, list_string, iPos);
END WHILE;

/*
 * add any text left over
 */
IF iPos <= length(list_string) THEN
SET iRow = iRow + 1;
INSERT INTO temp_split_list (row_number,
                             row_value)
VALUES (iRow,
        substr(substring(list_string, iPos), 1, 4000));
END IF;

end;
$$

DELIMITER ;

-- -----------------------------------------------------
-- function get_prev_bill_date
-- -----------------------------------------------------

USE `mdce`;
DROP function IF EXISTS `mdce`.`get_prev_bill_date`;

DELIMITER $$
USE `mdce`$$
CREATE FUNCTION get_prev_bill_date (dateCheck DATE, billingDay int)
  RETURNS date
DETERMINISTIC
  BEGIN
    DECLARE billDate DATE;

    SET billDate = adddate(adddate(dateCheck, -1 * dayofmonth(dateCheck)), billingDay);
    IF billDate <= dateCheck THEN
      RETURN billDate;
    END IF;

    RETURN billDate - interval 1 month;

  END;
$$

DELIMITER ;

-- -----------------------------------------------------
-- function get_next_bill_date
-- -----------------------------------------------------

USE `mdce`;
DROP function IF EXISTS `mdce`.`get_next_bill_date`;

DELIMITER $$
USE `mdce`$$
CREATE FUNCTION get_next_bill_date (dateCheck DATE, billingDay int)
  RETURNS date
DETERMINISTIC
  BEGIN
    DECLARE billDate DATE;

    SET billDate = adddate(adddate(dateCheck, -1 * dayofmonth(dateCheck)), billingDay);
    IF billDate > dateCheck THEN
      RETURN billDate;
    END IF;

    RETURN billDate + interval 1 month;

  END;
$$

DELIMITER ;
SET SQL_MODE = '';
GRANT USAGE ON *.* TO 'mdceintegration';
DROP USER 'mdceintegration';
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'mdceintegration' IDENTIFIED BY 'mdceintegration';

GRANT SELECT, INSERT, TRIGGER, UPDATE ON TABLE `mdce`.* TO 'mdceintegration';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO 'mdcemqtt';
DROP USER 'mdcemqtt';
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'mdcemqtt' IDENTIFIED BY 'mdcemqtt';

GRANT SELECT, INSERT, TRIGGER,UPDATE ON TABLE `mdce`.* TO 'mdcemqtt';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO 'mdceweb';
DROP USER 'mdceweb';
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'mdceweb' IDENTIFIED BY 'mdceweb';

GRANT SELECT, INSERT, TRIGGER ON TABLE `mdce`.* TO 'mdceweb';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
