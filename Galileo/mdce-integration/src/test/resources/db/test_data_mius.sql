INSERT INTO `mdce`.`reporting_plans` (`reporting_plan_id`, `name`, `start_mins`, `interval_mins`, `transmit_window_mins`, `quiet_start_mins`, `quiet_end_mins`) 
VALUES (5, 'every now and then', 60, 60*24, 60, 60, 60);

INSERT INTO `mdce`.`data_collection_plans` (`data_collection_id`, `name`, `start_time_secs`, `interval_secs`) VALUES (5, 'every so often', 15*60, 15*60);

INSERT INTO `mdce`.`utility_list` (`utility_id`, `name`, `last_data_request`) VALUES (5, 'Corpse Ditch Water Company', NULL);
INSERT INTO `mdce`.`utility_list` (`utility_id`, `name`, `last_data_request`) VALUES (105, 'Latrine Gulch Water Company', NULL);

INSERT INTO `mdce`.`billing_plans` (`billing_plan_id`, `name`, `start_day`, `monthly_bytes`) VALUES (5, 'gig a month', 1, 1024*1024*1024);

INSERT INTO `mdce`.`miu_details` (`miu_id`, `utility_id`, `last_insert_date`, `next_sync_time`, `miu_type`, `billing_plan_id`, `reporting_plan_id`, `temp_reporting_plan_id`, `temp_reporting_plan_start_time`, `temp_reporting_plan_end_time`, `data_collection_id`, `temp_data_collection_id`, `temp_data_collection_start_time`, `temp_data_collection_end_time`) 
VALUES (1234567, 100, NULL, NULL, 1, 1, 1, 1, NULL, NULL, 1, 1, NULL, NULL);

INSERT INTO `mdce`.`miu_details` (`miu_id`, `utility_id`, `last_insert_date`, `next_sync_time`, `miu_type`, `billing_plan_id`, `reporting_plan_id`, `temp_reporting_plan_id`, `temp_reporting_plan_start_time`, `temp_reporting_plan_end_time`, `data_collection_id`, `temp_data_collection_id`, `temp_data_collection_start_time`, `temp_data_collection_end_time`) 
VALUES (1234568, 100, NULL, NULL, 1, 1, 1, 1, NULL, NULL, 1, 1, NULL, NULL);