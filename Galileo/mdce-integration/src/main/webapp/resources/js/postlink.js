// Adapted from https://code.google.com/p/postlink/source/browse/trunk/jquery.postlink.js

(function($) {
    $.fn.postlink = function() {
        return this.each(function() {
            var $this = $(this);
            if ($this[0].tagName != "A") {
                return;
            }
            $this.click(function(clickEvent) {
                clickEvent.preventDefault();
                var linkDetails = parseLink(this);
                var linkForm = createPostForm(linkDetails);
                $('body').append(linkForm);
                linkForm.submit();
            });
        });
        function createPostForm(hrefObj) {
            linkForm = $("<form>");
            if (hrefObj.url) {
                linkForm.attr("action", hrefObj.url);
            }
            linkForm.attr("method", "post");
            $.each(hrefObj.params, function(i, param) {
                var input = $("<input>");
                input.attr("type", "hidden");
                input.attr("name", param.key);
                input.attr("value", param.value);
                linkForm.append(input);
            });
            return linkForm;
        }

        function parseLink(element) {
            var url = element.href.split("?")[0].split('#')[0];
            var params = [];

            var parts = element.search.substr(1).split('&');
            for (var i = 0; i < parts.length; i++) {
                if (parts[i].length > 0) {
                    kvp = decodeURIComponent(parts[i]).split('=');
                    params.push({
                        key: kvp[0].trim(),
                        value: kvp[1].trim()
                    });
                }

            }

            return {
                url : url,
                params : params
            };
        }
    };
})(jQuery);

// register A tag decorated with class{postlink} to send parameters as POST
$(document).ready(function() {
    //post link
    $('a.post').postlink();
});
