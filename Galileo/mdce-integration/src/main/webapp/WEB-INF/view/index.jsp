<%--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015, 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Env: ${envProperties.envName} MDCE Integration</title>
    <c:url var="resourcesUrl" value="/resources"/>
    <link rel="stylesheet" href="${resourcesUrl}/css/mdce.css" media="all"/>
    <link rel="stylesheet" href="${resourcesUrl}/css/grid.css" media="all"/>
    <link rel="icon" href="${resourcesUrl}/images/neptune.ico"/>
    <script src="${resourcesUrl}/js/jquery-2.1.3.min.js"></script>
    <script src="${resourcesUrl}/js/postlink.js"></script>
</head>
<body>
<div id="titlebar">
    <img src="${resourcesUrl }/images/neptune_header_logo.png"/>

    <h1>Managed Data Collection Engine: MDCE Integration</h1>

    <div class="mdce-env-container">
        <div class="mdce-env">
            <h4>Environment: ${envProperties.envName}</h4>
        </div>
    </div>

</div>
<div class="maincontent">
    <div class="section">
        <p>Application version: ${applicationVersion}</p>
        <p>Environment: ${envProperties.envName}</p>
        <p>Server: ${serverName}</p>
        <p>Gateway data dump folder: ${gatewayDataDumpFolder}</p>
        <p>Current server time: ${currentServerTime}</p>
        <p>Local SQS: ${localSqs}</p>
    </div>
    <div>
        <section>
            <h2>This page is being removed</h2>
            <p>Features are being moved to the mdce-web application or the <a href="http://gontg/ntgprojects/10065-1/Project%20Wiki/MDCE%20links.aspx">MDCE Links page on Sharepoint</a></p>
            <h2>Setup</h2>
            <ul>
                <c:url var="url" value="http://gontg/ntgprojects/10065-1/Project%20Wiki/MDCE%20links.aspx" />
                <li><a href="${url}">integration CNS Verizon callback endpoint wsdl [link now on MDCE Links page on Sharepoint]</a></li>
            </ul>

            <h2>Rest API</h2>
            <p>To obtain partner keys, ask Ant H</p>
            <p>For URLs, see <a href="http://gontg/ntgprojects/10065-1/Project%20Wiki/MDCE%20links.aspx">MDCE Links page on Sharepoint</a></p>
        </section>

    </div>


</div>


</body>
</html>
