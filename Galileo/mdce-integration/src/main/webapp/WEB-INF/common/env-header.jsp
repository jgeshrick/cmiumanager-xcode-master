<%--
  ~ /* ***************************************************************************
  ~ *
  ~ *    Neptune Technology Group
  ~ *    Copyright 2015 as unpublished work.
  ~ *    All rights reserved
  ~ *
  ~ *    The information contained herein is confidential
  ~ *    property of Neptune Technology Group. The use, copying, transfer
  ~ *    or disclosure of such information is prohibited except by express
  ~ *    written agreement with Neptune Technology Group.
  ~ *
  ~ *****************************************************************************/
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="mdce-env-container">
    <div class="mdce-env">
        <c:if test="${envProperties.envName != null}"><h4>Environment: ${envProperties.envName}</h4></c:if>
        <c:if test="${envProperties.cmuId != null}"><h4>CMUID: ${envProperties.cmuId}</h4></c:if>
        <c:if test="${envProperties.iccId != null}"><h4>ICCID: ${envProperties.iccId}</h4></c:if>
    </div>
</div>