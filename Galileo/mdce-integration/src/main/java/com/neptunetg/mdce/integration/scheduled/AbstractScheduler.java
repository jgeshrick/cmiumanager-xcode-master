/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractScheduler
{
    private static final Logger logger = LoggerFactory.getLogger(AbstractScheduler.class);

    private ScheduledProcessor processor;

    protected AbstractScheduler(ScheduledProcessor processor)
    {
        this.processor = processor;
    }

    public void runScheduledProcess()
    {
        try
        {
            processor.process();
        } catch (Exception e)
        {
            final Throwable rootCause = getRootCause(e);
            //can't use Rest ExceptionHandler for asynchronous tasks so log here
            logger.error("Scheduled task throw Exception, " + e.getClass().getSimpleName() + " caused by " + rootCause.getClass() + " " + rootCause.getMessage(), rootCause);
        }
    }

    public abstract void process();

    private Throwable getRootCause(Throwable throwable)
    {
        Throwable rootCause = throwable;
        while (rootCause.getCause() != null)
        {
            rootCause = rootCause.getCause();
        }
        return rootCause;
    }
}