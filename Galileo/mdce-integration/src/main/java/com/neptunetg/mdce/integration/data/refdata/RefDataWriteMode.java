/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.refdata;

/**
 * The operating mode when adding reference data.
 */
public enum RefDataWriteMode
{
    /**
     * Remove all existing ref data before inserting new records.
     */
    ReplaceAll,
    /**
     * Append the new records to the existing data.
     */
    Append
}
