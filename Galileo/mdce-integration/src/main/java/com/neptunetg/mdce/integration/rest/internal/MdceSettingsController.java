/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.settings.service.MdceSettingsService;
import com.neptunetg.mdce.integration.audit.AuditService;
import com.neptunetg.mdce.integration.data.settings.MdceSettingsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/service")
public class MdceSettingsController implements MdceSettingsService
{

    private static final Logger logger = LoggerFactory.getLogger(MdceSettingsController.class);

    @Autowired
    private MdceSettingsRepository settingsRepository;

    @Autowired
    private AuditService auditService;

    @Override
    @ResponseBody
    @RequestMapping(value = URL_MDCE_SETTINGS_TEXT_SETTING, method = RequestMethod.GET)
    public String getTextSettingValue(@RequestParam(KEY) String s) throws InternalApiException
    {
        return settingsRepository.getTextSetting(s, null);
    }

    @Override
    @ResponseBody
    @RequestMapping(value = URL_MDCE_SETTINGS_INT_SETTING, method = RequestMethod.GET)
    public Long getIntSettingValue(@RequestParam(KEY) String s,
                                   @RequestParam(name = DEFAULT_VALUE, required = false) Long defaultValue) throws InternalApiException
    {
        return settingsRepository.getIntSetting(s, defaultValue == null ? -1L : defaultValue.longValue());
    }

    @Override
    @RequestMapping(value = URL_MDCE_SETTINGS_TEXT_SETTING, method = RequestMethod.POST)
    public void setTextSettingValue(@RequestParam(KEY) String s, @RequestBody String newVal, @RequestParam(REQUESTER) String requester) throws InternalApiException
    {
        final String oldVal = settingsRepository.getTextSetting(s, null);
        settingsRepository.setTextSetting(s, newVal);
        auditService.logSettingChanged(s, oldVal, newVal, requester);
    }

    @Override
    @RequestMapping(value = URL_MDCE_SETTINGS_INT_SETTING, method = RequestMethod.POST)
    public void setIntSettingValue(@RequestParam(KEY) String s, @RequestBody Long newVal, @RequestParam(REQUESTER) String requester) throws InternalApiException
    {
        final Long oldValNumeric = settingsRepository.getIntSetting(s, Long.MIN_VALUE);
        final String oldVal = oldValNumeric.longValue() == Long.MIN_VALUE ? null : oldValNumeric.toString();
        settingsRepository.setIntSetting(s, newVal);
        auditService.logSettingChanged(s, oldVal, String.valueOf(newVal), requester);
    }

}
