/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.domain.mqtt.MiuMqttSubscriptionManager;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.integration.audit.AuditService;
import com.neptunetg.mdce.integration.data.miu.MiuDetails;
import com.neptunetg.mdce.integration.data.miu.MiuOwnershipRepository;
import com.neptunetg.mdce.integration.data.miu.MiuSearchRepository;
import com.neptunetg.mdce.integration.data.refdata.RefDataRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.data.site.SiteRepository;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import com.neptunetg.mdce.integration.rest.json.JsonMiuActiveFlag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Provides Miu Service for controllers and other services to access mysql database.
 * TODO: break this down
 */
@Service
public class MiuServiceImpl implements MiuService
{
    private static final Logger logger = LoggerFactory.getLogger(MiuServiceImpl.class);

    private static final String ATTEMPT_TO_CLAIM_ALREADY_CLAIMED_MIU = "Attempt to claim already claimed Miu ";
    private static final String FOR_SITE = " for site ";
    private static final String FROM = " from ";
    private static final String UNABLE_TO_RAISE_ALERT = "Unable to raise alert! ";

    private static final int MAX_BATCH_SIZE = 200; //insert / update 200 rows at once


    @Autowired
    private CmiuRepository cmiuRepo;

    @Autowired
    private MiuMqttSubscriptionManager mqttSubscriptionManager;

    @Autowired
    private MiuOwnershipRepository miuOwnerRepo;

    @Autowired
    private MiuSearchRepository miuSearchRepo;

    @Autowired
    private SiteRepository siteRepository;

    @Autowired
    private AuditService auditService;

    @Autowired
    private RefDataRepository refDataRepository;

    @Autowired
    private AlertService alertService;

    @Override
    @Transactional
    public void updateMiuOwnerDetailsBatch(List<JsonMiuActiveFlag> miuJsonList, SiteId siteId)
    {
        checkMiuIsClaimableBatch(siteId, miuJsonList);

        for(JsonMiuActiveFlag miuJson : miuJsonList)
        {
            if(miuJson.getIsClaimable() == null)
            {
                updateJsonToDidntNeedToClaim(miuJson);
            }
            else if(miuJson.getIsClaimable() == true)
            {
                updateJsonToClaimed(miuJson);
            }
            else
            {
                updateJsonUnclaimed(miuJson);
            }
        }

        claimMiuOwnerDetailsBatch(miuJsonList, siteId);
    }

    private void claimMiuOwnerDetailsBatch(List<JsonMiuActiveFlag> miuJsonList, SiteId newSiteId)
    {
        updateOrInsertMiuOwnerDetailsBatch(miuJsonList, newSiteId);

        for(JsonMiuActiveFlag miuJson : miuJsonList)
        {
            if(miuJson.getIsClaimable() != null && miuJson.getIsClaimable() == true)
            {
                SiteId oldSiteId = null;

                if(miuJson.getOldSiteId() != null)
                {
                    oldSiteId = new SiteId(miuJson.getOldSiteId());
                }

                this.auditService.logMiuOwnershipChanged(new MiuId(miuJson.getMiuId()),
                        oldSiteId,
                        newSiteId,
                        this.auditService.getRequesterFromSiteId(newSiteId));
            }
        }
    }

    private void checkMiuIsClaimableBatch(SiteId siteId, List<JsonMiuActiveFlag> miuJsonList)
    {
        if (siteId.isUniversalSite())
        {
            for(JsonMiuActiveFlag miu : miuJsonList)
            {
                miu.setIsClaimable(true);
            }
        }
        else
        {
            this.miuOwnerRepo.populateExistingMiuOwnerBatch(miuJsonList);

            for (JsonMiuActiveFlag miuJson : miuJsonList)
            {
                if (miuJson.getOldSiteId() == null || (new SiteId(miuJson.getOldSiteId())).isUniversalSite())
                {
                    miuJson.setIsClaimable(Boolean.TRUE);
                }
                else if (siteId.numericValue() == miuJson.getOldSiteId())
                {
                    miuJson.setIsClaimable(null);
                }
                else
                {
                    miuJson.setIsClaimable(Boolean.FALSE);
                    MiuId miuId = MiuId.valueOf(miuJson.getMiuId());
                    SiteId oldSiteId = SiteId.valueOf(miuJson.getOldSiteId());

                    this.auditService.logRefusedMiuOwnershipUpdate(miuId,
                            siteId,
                            oldSiteId);

                    raiseAlert(new MiuId(miuJson.getMiuId()), siteId, oldSiteId);
                }
            }
        }
    }

    private void updateJsonToDidntNeedToClaim(JsonMiuActiveFlag miuJson)
    {
        miuJson.setOwned(Boolean.TRUE.toString());
        miuJson.setSetOwnershipResult(JsonMiuActiveFlag.ALREADY_OWNED);
    }

    private void updateJsonToClaimed(JsonMiuActiveFlag miuJson)
    {
        miuJson.setOwned(Boolean.TRUE.toString());
        miuJson.setSetOwnershipResult(JsonMiuActiveFlag.SUCCESSFULLY_CLAIMED);
    }

    private void updateJsonUnclaimed(JsonMiuActiveFlag miuJson)
    {
        miuJson.setOwned(Boolean.FALSE.toString());
        miuJson.setSetOwnershipResult(JsonMiuActiveFlag.CONFLICT);
    }

    private void updateOrInsertMiuOwnerDetailsBatch(List<JsonMiuActiveFlag> miuJsonList, SiteId newSiteId)
    {
        siteRepository.ensureSiteExists(newSiteId);

        final UpdateMiuSiteIdBpss updateMiuSiteIdBpss = new UpdateMiuSiteIdBpss(newSiteId, MAX_BATCH_SIZE);

        int batchIndex = 0;
        int[] miuJsonListIndex = new int[MAX_BATCH_SIZE];
        int miuJsonIndex = 0;

        for(JsonMiuActiveFlag miuJson : miuJsonList)
        {
            if(miuJson.getIsClaimable() == null && miuJson.getActive() != null ||
                   Boolean.TRUE.equals(miuJson.getIsClaimable()))
            {
                updateMiuSiteIdBpss.addMiu(miuJson);
                miuJsonListIndex[batchIndex] = miuJsonIndex;
                batchIndex++;

                if(updateMiuSiteIdBpss.getBatchSize() == MAX_BATCH_SIZE)
                {
                    updateMiuOwnerSiteIdBatch(updateMiuSiteIdBpss, miuJsonList, miuJsonListIndex);
                    batchIndex = 0;
                }
            }

            miuJsonIndex++;
        }

        if(updateMiuSiteIdBpss.getBatchSize() > 0)
        {
            updateMiuOwnerSiteIdBatch(updateMiuSiteIdBpss, miuJsonList, miuJsonListIndex);
        }

        InsertMiuDetailsBpss insertMiuDetailsBpss = new InsertMiuDetailsBpss(newSiteId, MAX_BATCH_SIZE);

        batchIndex = 0;
        miuJsonIndex = 0;

        for(JsonMiuActiveFlag miuJson : miuJsonList)
        {
            if(Boolean.FALSE.equals(miuJson.getAlreadyExists()))
            {
                insertMiuDetailsBpss.addMiu(miuJson);
                miuJsonListIndex[batchIndex] = miuJsonIndex;
                batchIndex++;

                if(insertMiuDetailsBpss.getBatchSize() == MAX_BATCH_SIZE)
                {
                    insertMiuSetOwnerBatch(insertMiuDetailsBpss, miuJsonList, miuJsonListIndex);
                    batchIndex = 0;
                }
            }

            miuJsonIndex++;
        }

        if(insertMiuDetailsBpss.getBatchSize() > 0)
        {
            insertMiuSetOwnerBatch(insertMiuDetailsBpss, miuJsonList, miuJsonListIndex);
        }
    }

    private void insertMiuSetOwnerBatch(InsertMiuDetailsBpss insertMiuDetailsBpss,
                                       List<JsonMiuActiveFlag> miuJsonList,
                                       int[] miuJsonListIndex)
    {
        this.miuOwnerRepo.insertMiuSetOwnerBatch(insertMiuDetailsBpss);

        for(int i=0; i<insertMiuDetailsBpss.getBatchSize(); i++)
        {
            MiuType miuType;

            if(miuJsonList.get(miuJsonListIndex[i]).getType() != null)
            {
                miuType = MiuType.valueOf(miuJsonList.get(miuJsonListIndex[i]).getType().trim());
            }
            else
            {
                miuType = inferMiuTypeFromMiuId(new MiuId(miuJsonList.get(miuJsonListIndex[i]).getMiuId()));
            }

            this.mqttSubscriptionManager.subscribeMiuMqttIfApplicable(new MiuId(miuJsonList.get(miuJsonListIndex[i]).getMiuId()),
                    miuType);

        }

        insertMiuDetailsBpss.clearBatch();
    }

    private void updateMiuOwnerSiteIdBatch(UpdateMiuSiteIdBpss updateMiuSiteIdBpss,
                                           List<JsonMiuActiveFlag> miuJsonList,
                                           int[] miuJsonListIndex)
    {
        int[] rowsUpdated = this.miuOwnerRepo.updateMiuOwnerBatch(updateMiuSiteIdBpss);

        for(int i=0; i<updateMiuSiteIdBpss.getBatchSize(); i++)
        {
            if(rowsUpdated[i] == 1)
            {
                miuJsonList.get(miuJsonListIndex[i]).setAlreadyExists(true);
            }
            else
            {
                miuJsonList.get(miuJsonListIndex[i]).setAlreadyExists(false);
            }
        }

        updateMiuSiteIdBpss.clearBatch();
    }

    private static MiuType inferMiuTypeFromMiuId(MiuId miuId)
    {
        if (miuId.isInCmiuIdRange())
        {
            return MiuType.CMIU;
        }
        return MiuType.R900;
    }

    private void raiseAlert(MiuId miuId, SiteId siteId, SiteId currentMiuOwner)
    {
        final AlertSource alertSource = AlertSource.miuClaimConflict(miuId.numericValue(), siteId.numericValue(), currentMiuOwner.numericValue());
        final AlertDetails alertDetails = new AlertDetails();

        alertDetails.setAlertCreationDate(ZonedDateTime.now());
        alertDetails.setAlertLevel(AlertLevel.WARNING);
        alertDetails.setAlertSource(alertSource.toString());
        alertDetails.setAlertState(AlertState.NEW);
        alertDetails.setRecentMessage(ATTEMPT_TO_CLAIM_ALREADY_CLAIMED_MIU + miuId + FOR_SITE + siteId + FROM + currentMiuOwner);

        try
        {
            this.alertService.addAlertAndSendEmail(alertDetails);
        } catch (InternalApiException e)
        {
            logger.error(UNABLE_TO_RAISE_ALERT + alertDetails.toString() + " " + e.getClass().getSimpleName() + e.getMessage(), e);
        }
    }


}
