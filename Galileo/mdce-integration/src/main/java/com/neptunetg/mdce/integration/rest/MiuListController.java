/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.mdce.integration.data.miu.JdbcMiuSearchRepository;
import com.neptunetg.mdce.integration.data.miu.MiuDetails;
import com.neptunetg.mdce.integration.data.miu.MiuSearchRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import com.neptunetg.mdce.integration.rest.json.JsonMiuListCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
public class MiuListController extends BaseTokenAuthenticatingRestController
{

    private static final String MDCE_API_V1_MIU_LIST = "/mdce/api/v1/miu_list";
    private static final String MDCE_API_V1_MIU_LIST_FIND = "/mdce/api/v1/miu_list/find";
    public static final ZoneId TIME_ZONE_FOR_RESPONSE_DATES = ZoneId.of("America/Chicago");

    @Autowired
    private MiuSearchRepository miuSearchRepo;

    /**
     * This web service will be used by external systems to a list of MIU’s owned by the site id that the token represents.
     * @return JSON containing list of packets id, first and last insert date.
     */
    @RequestMapping(value = MDCE_API_V1_MIU_LIST, method = RequestMethod.GET)
    @ResponseBody
    public JsonMiuListCollection getMiuList(
            @RequestParam(value = "miu_id", required = false) String miuId,
            @RequestParam(value = "iccid", required = false) String iccid,
            @RequestParam(value = "imei", required = false) String imei,
            @RequestParam(value = "return_identities", required = false) String returnIdentitiesParameter,
            HttpServletRequest request) throws NotAuthorizedException, BadRequestException
    {
        final SiteId siteId = getClientSiteId(request);
        final JsonMiuListCollection jsonMiuList;
        final boolean returnIdentities = "Y".equalsIgnoreCase(returnIdentitiesParameter);

        if (siteId.isUniversalSite())
        {
            final Map<String, String> searchParameters = new HashMap<>();

            // Add search parameters if supplied
            addSearchOptionIfValueSupplied(JdbcMiuSearchRepository.MIU_ID, miuId, searchParameters);
            addSearchOptionIfValueSupplied(JdbcMiuSearchRepository.IMEI_OPTION, imei, searchParameters);
            addSearchOptionIfValueSupplied(JdbcMiuSearchRepository.ICCID_OPTION, iccid, searchParameters);

            if (searchParameters.isEmpty())
            {
                jsonMiuList = JsonMiuListCollection.from(miuSearchRepo.getAllMius(), TIME_ZONE_FOR_RESPONSE_DATES, returnIdentities);
            }
            else
            {
                jsonMiuList = JsonMiuListCollection.from(miuSearchRepo.getMius(searchParameters), TIME_ZONE_FOR_RESPONSE_DATES, returnIdentities);
            }
        }
        else
        {
            jsonMiuList = JsonMiuListCollection.from(miuSearchRepo.getMiusForUtility(siteId), TIME_ZONE_FOR_RESPONSE_DATES, returnIdentities);
        }

        return jsonMiuList;
    }

    /**
     * Allows search by MIU ID, ICCID, IMEI and EUI.
     * Defaults to returning an empty set if no parameters are supplied.
     * @return JSON containing list of matching MIUs.
     */
    @RequestMapping(value = MDCE_API_V1_MIU_LIST_FIND, method = RequestMethod.GET)
    @ResponseBody
    public JsonMiuListCollection findMius(
            @RequestParam(value = "find_id", required = false) String findId,
            HttpServletRequest request) throws NotAuthorizedException, BadRequestException
    {
        final SiteId siteId = getClientSiteId(request);
        final JsonMiuListCollection jsonMiuList;
        final Map<String, String> searchParameters = new HashMap<>();

        // Add search parameters if supplied
        addSearchOptionIfValueSupplied(JdbcMiuSearchRepository.FIND_ID_OPTION, findId, searchParameters);

        if (searchParameters.isEmpty())
        {
            // Return an empty set if no search parameters were supplied.
            jsonMiuList = JsonMiuListCollection.from(new ArrayList<>(), TIME_ZONE_FOR_RESPONSE_DATES, true);
        }
        else
        {

            if (!siteId.isUniversalSite())
            {
                addSearchOptionIfValueSupplied(JdbcMiuSearchRepository.SITE_ID, siteId.toString(), searchParameters);
            }

            jsonMiuList = JsonMiuListCollection.from(miuSearchRepo.getMius(searchParameters), TIME_ZONE_FOR_RESPONSE_DATES, true);
        }

        return jsonMiuList;
    }

    private void addSearchOptionIfValueSupplied(String parameterKey, String value, Map<String, String> parameters)
    {
        if (StringUtils.hasText(value))
        {
            parameters.put(parameterKey, value);
        }
    }
}
