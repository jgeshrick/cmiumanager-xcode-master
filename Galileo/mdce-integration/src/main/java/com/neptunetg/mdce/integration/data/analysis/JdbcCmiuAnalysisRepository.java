/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.analysis;

import com.neptunetg.mdce.common.internal.anaylsis.model.CmiuTimingAnalysisDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


/**
 * Time Analysis Database.
 */
@Repository
public class JdbcCmiuAnalysisRepository implements CmiuAnalysisRepository {
    private final static String HAVING_TOTAL_CONNECTION_TIME = "HAVING total_connection_time";
    private final static String WHERE_SITE_ID = "md.site_id";
    private final static String WHERE_STATE = "md.state";
    private final static String WHERE_MNO = "sd.sim_cellular_network";
    private final static String WHERE_MODE = "ccs.cmiu_mode_name";

    private JdbcTemplate db;

    @Autowired
    public JdbcCmiuAnalysisRepository(JdbcTemplate db)
    {
        this.db = db;
    }

    @Override
    public List<CmiuTimingAnalysisDetails> getTimingInfo(Long connectionThreshold, String cmiuMode, String mno, String state, String siteId)
    {

        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        parameters.add(HAVING_TOTAL_CONNECTION_TIME, connectionThreshold);
        if (!cmiuMode.isEmpty())
        {
            parameters.add(WHERE_MODE, cmiuMode);
        }
        if(!mno.isEmpty())
        {
            parameters.add(WHERE_MNO, mno);
        }
        if(!state.isEmpty())
        {
            parameters.add(WHERE_STATE, state);
        }
        if (!siteId.isEmpty())
        {
            parameters.add(WHERE_SITE_ID, siteId);
        }

        return this.db.query(buildCmiuTimingAnalysisQuery(parameters), JdbcCmiuAnalysisRepository::mapCmiuTimingAnalysisDetails);
    }

    private static CmiuTimingAnalysisDetails mapCmiuTimingAnalysisDetails(ResultSet rs, int rowNum) throws SQLException
    {
        CmiuTimingAnalysisDetails cmiuTimingAnalysisDetails = new CmiuTimingAnalysisDetails();
        cmiuTimingAnalysisDetails.setMiuId(rs.getLong("miu_id"));
        cmiuTimingAnalysisDetails.getTotalConnectionTimeMilliSeconds(rs.getLong("total_connection_time"));
        cmiuTimingAnalysisDetails.setRegisterTimeMilliSeconds(rs.getInt("register_time"));
        cmiuTimingAnalysisDetails.setRegisterConnectionTime(rs.getInt("register_time_connected"));
        cmiuTimingAnalysisDetails.setRegisterActivateContextTimeMilliSeconds(rs.getInt("register_time_to_activate_context"));
        cmiuTimingAnalysisDetails.setDisconnectTimeMilliSeconds(rs.getInt("disconnect_time"));
        cmiuTimingAnalysisDetails.setRegisterTransferPacketTimeMilliSeconds(rs.getInt("register_time_to_transfer_packet"));
        cmiuTimingAnalysisDetails.setSiteId(rs.getInt("site_id"));
        cmiuTimingAnalysisDetails.setState(rs.getString("state"));
        cmiuTimingAnalysisDetails.setMno(rs.getString("mno"));
        cmiuTimingAnalysisDetails.setCmiuMode(rs.getString("cmiu_mode"));

        return cmiuTimingAnalysisDetails;
    }

    private String buildCmiuTimingAnalysisQuery(MultiValueMap<String, Object> parameters)
    {
        String sql = "SELECT" +
                "  mms.miu_id," +
                "  FLOOR(AVG(mms.avg_register_time + mms.avg_register_time_connected + mms.avg_register_time_to_activate_context + " +
                "      mms.avg_register_time_to_transfer_packet + mms.avg_disconnect_time)) total_connection_time, " +
                "  FLOOR(AVG(mms.avg_register_time)) register_time, " +
                "  FLOOR(AVG(mms.avg_register_time_connected)) register_time_connected, " +
                "  FLOOR(AVG(mms.avg_register_time_to_activate_context)) register_time_to_activate_context, " +
                "  FLOOR(AVG(mms.avg_disconnect_time)) disconnect_time," +
                "  FLOOR(AVG(mms.avg_register_time_to_transfer_packet)) register_time_to_transfer_packet, " +
                "  md.site_id site_id, " +
                "  md.state state, " +
                "  sd.sim_cellular_network mno, " +
                "  ccs.cmiu_mode_name cmiu_mode " +
                "FROM miu_monthly_stats mms" +
                "  JOIN " +
                "  miu_details md ON mms.miu_id = md.miu_id " +
                "  JOIN " +
                "  sim_details sd ON mms.miu_id = sd.miu_id " +
                "  JOIN " +
                "  cmiu_config_history cch ON mms.miu_id = cch.miu_id " +
                "  JOIN " +
                "  cmiu_config_set ccs ON cch.cmiu_config = ccs.id ";

        boolean isFirstClause = true;

        for (Map.Entry<String, List<Object>> entryList : parameters.entrySet()) {
            if (!entryList.getKey().equals(HAVING_TOTAL_CONNECTION_TIME)) {
                sql += isFirstClause ? " WHERE " : " AND ";
                isFirstClause = false;
                sql += entryList.getKey() + "='" + entryList.getValue().get(0).toString() + "' ";
            }
        }

        sql += "GROUP BY mms.miu_id ";
        sql += HAVING_TOTAL_CONNECTION_TIME + " > " + parameters.get(HAVING_TOTAL_CONNECTION_TIME).get(0).toString();

        return sql;
    }
}
