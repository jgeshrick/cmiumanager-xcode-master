/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.cns.CellularStatus;
import com.neptunetg.common.cns.CnsException;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleService;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.integration.cns.CellularNetworkServiceManager;
import com.neptunetg.mdce.integration.data.miu.MiuDetails;
import com.neptunetg.mdce.integration.data.miu.MiuSearchRepository;
import com.neptunetg.mdce.integration.rest.json.cmiudeactivation.JsonDeactivationDetails;
import com.neptunetg.mdce.integration.rest.json.cmiudeactivation.JsonDeactivationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/*
* IF16, IF38
*/
@RestController
public class CmiuDeactivationDetailsController extends BaseTokenAuthenticatingRestController
{
    private static final Logger log = LoggerFactory.getLogger(CmiuDeactivationDetailsController.class);

    @Autowired
    private MiuSearchRepository miuSearchRepo;

    @Autowired
    private CmiuRepository cmiuRepository;

    @Autowired
    private MiuLifecycleService miuLifecycleService;

    @Autowired
    private CellularNetworkServiceManager cellularNetworkServiceManager;

    /**
     * Retrieve data needed to fill in deactivation page.
     * @param token token id
     * @param miuId cmiu id
     * @return JsonDeactivation object
     * @throws NotAuthorizedException
     */
    @RequestMapping(value = "/mdce/api/v1/miu/config/deactivation", method = RequestMethod.GET)
    @ResponseBody
    public JsonDeactivationDetails getMiuConfigListForSite(@RequestParam("token") String token,
                                                           @RequestParam("cmiuid") MiuId miuId,
                                                           HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException
    {
        JsonDeactivationDetails results = new JsonDeactivationDetails();

        MiuDetails details = miuSearchRepo.getMiu(miuId);

        results.setCarrier(details.getNetworkProvider());
        results.setCurrentLifecycleState(details.getLifecycleState().toString());
        results.setIccid(details.getIccid());
        results.setImei(details.getImei());
        results.setMiuId(details.getId().numericWrapperValue());
        results.setLastTimeHeard(details.getLastHeardTime().toString());

        final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(details.getNetworkProvider());

        try
        {
            DeviceCellularInformation cellularDeviceInformation = cellularNetworkService.getDeviceInformation(details.getIccid());
            results.setLastCommunicationTime(cellularDeviceInformation.getLastConnectionDate().toInstant().toString());
        }
        catch (CnsException e)
        {
            log.error("Error contacting CNS for device information: ", e);
        }


        return results;

    }

    /**
     *
     * @param token token
     * @param miuId cmiu id
     * @param request request
     * @return JsonDeactivationResult object
     * @throws NotAuthorizedException
     * @throws BadRequestException
     * @throws InternalApiException
     */
    @RequestMapping(value = "/mdce/api/v1/miu/config/deactivation", method = RequestMethod.PUT)
    @ResponseBody
    public JsonDeactivationResult deactivateCmiu(@RequestParam("token") String token,
                                                           @RequestParam("cmiuid") MiuId miuId,
                                                           HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException, InternalApiException {

        JsonDeactivationResult result = new JsonDeactivationResult();

        try
        {
            result.setMiuId(miuId.numericWrapperValue());
            final CellularDeviceDetail miuCellularDetails = cmiuRepository.getCellularConfig(miuId);
            if(miuCellularDetails != null)
            {
                final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(miuCellularDetails.getMno());
                final String iccid = miuCellularDetails.getIccid();

                MiuDetails details = miuSearchRepo.getMiu(miuId);

                result.setOldState(details.getLifecycleState().toString());

                String cnsResult = "";

                switch (details.getLifecycleState()) {
                    case DEAD:
                    case DEATHBED:
                    case PRESUSPENDED:
                        result.setResult("No Action");
                        break;

                    case SUSPENDED:
                        if(cellularNetworkService.getAllowedLifecycleState(CellularStatus.DE_ACTIVE))
                        {
                            cnsResult = cellularNetworkService.deactivateDevice(iccid);
                            miuLifecycleService.setMiuLifecycleState(miuId, MiuLifecycleStateEnum.DEATHBED);
                            result.setNewState(MiuLifecycleStateEnum.DEATHBED.toString());
                            result.setResult("Success. Message Id: " + cnsResult);
                        }
                        else
                        {
                            result.setResult("Error: MIU cannot be set to Deactivated.");
                        }

                        break;

                    default:
                        cnsResult = cellularNetworkService.suspendDevice(iccid);
                        miuLifecycleService.setMiuLifecycleState(miuId, MiuLifecycleStateEnum.PRESUSPENDED);
                        result.setNewState(MiuLifecycleStateEnum.PRESUSPENDED.toString());
                        result.setResult("Success. Message Id: " + cnsResult);
                }
            }
            else
            {
                result.setResult("Error: MIU not found.");
            }
        }
        catch (Exception e)
        {
            result.setResult("Error: " + e.toString());
            log.error("Error: ", e);
        }
        return result;
    }

}
