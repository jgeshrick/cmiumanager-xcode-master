/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.MiuConfigHistoryQueryOptions;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.ForbiddenException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigHistory;
import com.neptunetg.mdce.common.internal.miu.model.SequentialCmiuConfig;
import com.neptunetg.mdce.integration.data.refdata.RefDataRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.domain.CmiuConfigService;
import com.neptunetg.mdce.integration.rest.json.miuconfig.JsonMiuConfigHistory;
import com.neptunetg.mdce.integration.rest.json.miuconfig.JsonMiuConfigHistoryCollection;
import com.neptunetg.mdce.integration.rest.json.miuconfig.JsonSequentialMiuConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * IF18 Used by external systems to retrieve configuration history about an MIU or list of MIUs.
 */
@RestController
public class CmiuConfigHistoryController extends BaseTokenAuthenticatingRestController
{

    @Autowired
    private CmiuConfigService configService;

    @Autowired
    private RefDataRepository refDataRepository;

    /**
     * This will return MIU history for all MIUs for the token’s site id.
     * If the token's site_id is 0, then all MIUs in the MDCE will be returned.
     * This is the v1 implementation, in which the config history is abbreviated to a maximum of one entry per day.
     * @param startDateStr
     * @param endDateStr
     * @param request
     * @return JsonMiuConfigHistoryCollection
     * @throws NotAuthorizedException
     * @throws BadRequestException
     */
    @RequestMapping(value = "/mdce/api/v1/miu/config_history", method = RequestMethod.GET)
    @ResponseBody
    public JsonMiuConfigHistoryCollection getAbbreviatedMiuConfigHistoryBySite(
            @RequestParam(value = "start_date", required = false) String startDateStr,
            @RequestParam(value = "end_date", required = false) String endDateStr,
            @RequestParam(value = "billable", required = false) String billable,
            @RequestParam(value = "from_miu_id", required = false) MiuId fromMiuId,
            @RequestParam(value = "to_miu_id", required = false) MiuId toMiuId,
            HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException
    {
        final SiteId siteId = getClientSiteId(request);

        //get config history from all MIUs
        final ZonedDateTime startDate = parseMaximumQueryDateOptional(startDateStr);
        final ZonedDateTime endDate = parseMaximumQueryDateOptional(endDateStr);

        MiuConfigHistoryQueryOptions queryOptions = new MiuConfigHistoryQueryOptions();
        queryOptions.setSiteId(siteId.numericWrapperValue());
        queryOptions.setStartDate(startDate != null ? Instant.from(startDate) : null);
        queryOptions.setEndDate(endDate != null ? Instant.from(endDate) : null);
        queryOptions.setMeterActive(StringUtils.hasText(billable) ? "Y".equalsIgnoreCase(billable) : null);
        queryOptions.setFromMiuId(fromMiuId);
        queryOptions.setToMiuId(toMiuId);

        final List<CmiuConfigHistory> result = this.configService.getConfigHistory(queryOptions);

        final Optional<String> distributerId = this.refDataRepository.lookupDistributerIdFromSiteId(siteId);

        final ZoneId timezone = startDate != null ? startDate.getZone() : ZoneId.of("UTC");

        return JsonMiuConfigHistoryCollection.from(result, distributerId, timezone);

    }

    @RequestMapping(value = "/mdce/api/v1/miu/{miuId}/config_history", method = RequestMethod.GET)
    @ResponseBody
    public JsonMiuConfigHistory getAbbreviatedMiuConfigHistoryByMiu(
            @PathVariable("miuId") MiuId miuId,
            @RequestParam(value = "start_date") String startDateStr,
            @RequestParam(value = "end_date") String endDateStr,
            HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException, ForbiddenException
    {
        final SiteId siteId = getClientSiteId(request);

        checkSiteIdHasAccessToCmiuOrThrowForbidden(miuId, siteId);

        //get config history from all MIUs
        final ZonedDateTime startDate = parseMaximumQueryDateMandatory(startDateStr);
        final ZonedDateTime endDate = parseMaximumQueryDateOptional(endDateStr);

        MiuConfigHistoryQueryOptions queryOptions = new MiuConfigHistoryQueryOptions();
        queryOptions.setSiteId(siteId.numericWrapperValue());
        queryOptions.setMiuId(miuId);
        queryOptions.setStartDate(Instant.from(startDate));
        queryOptions.setEndDate(Instant.from(endDate));

        //Get CMIU config history
        final List<CmiuConfigHistory> cmiuConfigHistoryList = this.configService.getConfigHistory(queryOptions);

        final Optional<String> distributerId = this.refDataRepository.lookupDistributerIdFromSiteId(siteId);

        final ZoneId timezone = startDate.getZone();

        return JsonMiuConfigHistory.from(cmiuConfigHistoryList, miuId, distributerId, timezone);
    }

    @RequestMapping(value = "/mdce/api/v2/miu/config_history", method = RequestMethod.GET)
    @ResponseBody
    public List<JsonSequentialMiuConfig> getSequentialMiuConfigHistory(
            @RequestParam(value = "last_sequence", required = false) Integer lastSequenceId,
            HttpServletRequest request)
            throws NotAuthorizedException
    {
        final SiteId siteId = getClientSiteId(request);
        if (siteId.doesNotMatch(0))
        {
            throw new NotAuthorizedException("This API version is not available to the current site ID.");
        }

        return configService.getSequentialConfigHistory(lastSequenceId)
                .stream()
                .map(JsonSequentialMiuConfig::from)
                .collect(Collectors.toList());
    }
}
