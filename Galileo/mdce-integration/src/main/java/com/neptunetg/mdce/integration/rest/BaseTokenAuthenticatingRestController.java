/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.mdce.common.api.domain.RestClientId;
import com.neptunetg.mdce.common.api.domain.auth.TokenManagerService;
import com.neptunetg.mdce.common.api.rest.RestUtils;import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.ForbiddenException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import com.neptunetg.mdce.integration.data.miu.MiuOwnershipRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.lang.String;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

public abstract class BaseTokenAuthenticatingRestController
{

    private static final ZoneId DEFAULT_TIMEZONE = ZoneId.of("US/Central");

    @Autowired
    private TokenManagerService tokenManagerService;

    @Autowired
    private MiuOwnershipRepository miuOwnerRepo;


    protected final SiteId getClientSiteId(HttpServletRequest request)
    {
        final RestClientId clientId = RestUtils.getClientIdAttribute(request);
        return SiteId.valueOf(clientId.getSiteId());
    }

    protected void validateIsBusinessProcessConnector(String token, HttpServletRequest request) throws NotAuthorizedException
    {
        final String originatingIp = RestUtils.getOriginatingIp(request);
        final RestClientId requestClient = tokenManagerService.checkToken(token, originatingIp);
        if (!requestClient.isBpc())
        {
            throw new NotAuthorizedException("Not authenticated as BPC");
        }
    }

    protected void checkSiteIdHasAccessToCmiuOrThrowForbidden(MiuId miuId, SiteId siteId) throws ForbiddenException
    {
        if (!siteId.isUniversalSite() && !siteId.equals(this.miuOwnerRepo.getMiuOwner(miuId)))
        {
            //CMIU ID is not associated to this site id!!
            throw new ForbiddenException("CMIU id is not associated to this site id");
        }
    }

    protected void checkRequestSiteIdMatchesJsonSiteIdOrThrowForbidden(SiteId requestSiteId, long jsonSiteId) throws ForbiddenException
    {
        if (requestSiteId.doesNotMatch(jsonSiteId))
        {
            throw new ForbiddenException("site id in request body does not match token");
        }
    }

    protected void checkRequestMiuIdMatchesJsonObjectOrThrowBadRequest(MiuId requestMiuId, long jsonMiuId) throws BadRequestException
    {
        if (!requestMiuId.matches(jsonMiuId))
        {
            throw new BadRequestException("Mismatch miu id in request url and content");
        }
    }

    /**
     * Attempt to parse a date from a query string.  This is expected to be an ISO date/time
     * including timezone, but a shorter ISO date is also allowed, in which case the
     * time will be midnight central on that day.
     * @param dt Date string to parse
     * @return ZonedDateTime Parsed date
     * @throws BadRequestException If bad date string or null
     */
    protected static ZonedDateTime parseMinimumQueryDateMandatory(String dt) throws BadRequestException
    {
        return parseQueryDateInternal(dt, true, null, false);
    }

    /**
     * Attempt to parse a date from a query string.  This is expected to be an ISO date/time
     * including timezone, but a shorter ISO date is also allowed, in which case the
     * time will be midnight central on that day
     * @param dt Date string to parse
     * @return ZonedDateTime Parsed date, or null if no date supplied
     * @throws BadRequestException If bad date string
     */
    protected static ZonedDateTime parseMinimumQueryDateOptional(String dt) throws BadRequestException
    {
        return parseQueryDateInternal(dt, false, null, false);
    }


    /**
     * Attempt to parse a date from a query string.  This is expected to be an ISO date/time
     * including timezone, but a shorter ISO date is also allowed, in which case the
     * time will be midnight central on the next day.
     * @param dt Date string to parse
     * @return ZonedDateTime Parsed date
     * @throws BadRequestException If bad date string or null
     */
    protected static ZonedDateTime parseMaximumQueryDateMandatory(String dt) throws BadRequestException
    {
        return parseQueryDateInternal(dt, true, null, true);
    }

    /**
     * Attempt to parse a date from a query string.  This is expected to be an ISO date/time
     * including timezone, but a shorter ISO date is also allowed, in which case the
     * time will be midnight central on the next day
     * @param dt Date string to parse
     * @return ZonedDateTime Parsed date, or null if no date supplied
     * @throws BadRequestException If bad date string
     */
    protected static ZonedDateTime parseMaximumQueryDateOptional(String dt) throws BadRequestException
    {
        return parseMaximumQueryDateOptional(dt, null);
    }

    /**
     * Attempt to parse a date from a query string.  This is expected to be an ISO date/time
     * including timezone, but a shorter ISO date is also allowed, in which case the
     * time will be midnight central on the next day
     * @param dt Date string to parse
     * @return ZonedDateTime Parsed date, or null if no date supplied
     * @throws BadRequestException If bad date string
     */
    protected static ZonedDateTime parseMaximumQueryDateOptional(String dt, ZonedDateTime defaultValue) throws BadRequestException
    {
        return parseQueryDateInternal(dt, false, defaultValue, true);
    }

    private static ZonedDateTime parseQueryDateInternal(String dt, boolean mandatory, ZonedDateTime defaultValue, boolean nextDayIfDateOnly) throws BadRequestException
    {

        if (!StringUtils.hasText(dt))
        {
            if (mandatory)
            {
                throw new BadRequestException("Missing date!");
            }
            else
            {
                return defaultValue;
            }
        }
        try
        {
            return ZonedDateTime.parse(dt, DateTimeFormatter.ISO_DATE_TIME);
        }
        catch (DateTimeParseException e)
        {
            try
            {
                final LocalDate localDate = LocalDate.parse(dt, DateTimeFormatter.ISO_LOCAL_DATE);
                if (nextDayIfDateOnly)
                {
                    return ZonedDateTime.of(localDate.plusDays(1L), LocalTime.MIDNIGHT, DEFAULT_TIMEZONE);
                }
                else
                {
                    return ZonedDateTime.of(localDate, LocalTime.MIDNIGHT, DEFAULT_TIMEZONE);
                }
            }
            catch (DateTimeParseException e2)
            {
                throw new BadRequestException("Badly formatted datetime", e);
            }
        }
    }


}
