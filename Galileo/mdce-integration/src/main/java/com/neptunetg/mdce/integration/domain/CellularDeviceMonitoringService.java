/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;

import java.time.ZonedDateTime;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Service to monitor SQS for notification to check cellular device. Get near real time usage data and verify it
 * is within limit.
 */
public interface CellularDeviceMonitoringService
{
    boolean getAndCheckUsageData(AtomicBoolean cancellationToken) throws MiuDataException;

    void checkMonthlyUseAllowanceAndRaiseAlertIfExceeded(ZonedDateTime timeNow, CellularDeviceDetail cmiu, long byteUsedThisMonth);
}
