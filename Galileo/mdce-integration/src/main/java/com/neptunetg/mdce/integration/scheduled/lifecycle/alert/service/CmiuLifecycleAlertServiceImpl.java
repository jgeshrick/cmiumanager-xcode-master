/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.scheduled.lifecycle.alert.service;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.service.miu.lifecycle.CheckCmiuLifecycleStateTransitions;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleService;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleStateTransistion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

/**
 * Created by WJD1 on 23/05/2016.
 * A class to check if an alert needs to be raised for a CMIU given its lifecycle state
 */
@Service
public class CmiuLifecycleAlertServiceImpl implements CmiuLifecycleAlertService
{
    private final MiuLifecycleService miuLifecycleService;
    private final CmiuLifecycleAlertServiceThresholds alertThresholds;

    @Autowired
    public CmiuLifecycleAlertServiceImpl(MiuLifecycleService miuLifecycleService,
                                         CmiuLifecycleAlertServiceThresholds thresholds)
    {
        this.miuLifecycleService = miuLifecycleService;
        this.alertThresholds = thresholds;
    }

    @Override
    public List<MiuLifecycleState> getCmiusStuckInLifecycleStateForTooLong()
    {
        List<MiuLifecycleState> cmiuLifecycleStatesList = new ArrayList<>();

        Instant now = Instant.now();

        for(EnumMap.Entry<MiuLifecycleStateEnum, Duration>
                maximumTimeInState : alertThresholds.getMaximumTimeInState().entrySet())
        {
            cmiuLifecycleStatesList.addAll(
                    miuLifecycleService.getCmiusInLifecycleStateBefore(maximumTimeInState.getKey(),
                    now.minus(maximumTimeInState.getValue())));
        }

        return cmiuLifecycleStatesList;
    }

    @Override
    public List<MiuLifecycleStateTransistion> getCmiusNotClaimedInGracePeriod()
    {
        List<MiuLifecycleStateTransistion> cmiuLifecycleStateComparisonList = new ArrayList<>();

        Instant now = Instant.now();

        for(EnumMap.Entry<MiuLifecycleStateEnum, Duration>
                maximumTimeSinceActivation : alertThresholds.getMaximumTimeSinceActivatedInState().entrySet())
        {
            cmiuLifecycleStateComparisonList.addAll(
                    miuLifecycleService.getMiusInLifecycleStateWithPreviousStateSetBefore(
                            MiuLifecycleStateEnum.ACTIVATED,
                            maximumTimeSinceActivation.getKey(),
                            now.minus(maximumTimeSinceActivation.getValue())));
        }

        return cmiuLifecycleStateComparisonList;
    }

    @Override
    public List<MiuLifecycleState> getCmiusStuckInLifecycleStateForAlerts()
    {
        List<MiuLifecycleState> cmiuLifecycleStatesList = new ArrayList<>();

        for(EnumMap.Entry<MiuLifecycleStateEnum, Duration>
                maximumTimeInState : alertThresholds.getMaximumTimeInState().entrySet())
        {
            cmiuLifecycleStatesList.addAll(
                    miuLifecycleService.getMiusStuckInLifecycleForAlerts(maximumTimeInState.getValue()) );
        }

        return cmiuLifecycleStatesList;
    }

    @Override
    public MiuLifecycleState getMiuCurrentLifecycleState(MiuId miuId)
    {
        return miuLifecycleService.getMiuLifecycleState(miuId);
    }
}

