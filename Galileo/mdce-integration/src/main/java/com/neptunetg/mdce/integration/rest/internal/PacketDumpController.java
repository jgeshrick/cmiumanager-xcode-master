/*
 *  Neptune Technology Group
 *  Copyright 2017 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.amazonaws.services.dynamodbv2.document.Table;
import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.common.data.miu.cellular.CellularDeviceId;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.common.data.miu.lora.LoraRepository;
import com.neptunetg.mdce.common.data.PacketRepository;
import com.neptunetg.mdce.common.internal.diagnostics.service.PacketDumpService;
import com.neptunetg.mdce.integration.data.miu.MiuDetails;
import com.neptunetg.mdce.integration.data.miu.MiuSearchRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.domain.CmiuPacketDumper;
import com.neptunetg.mdce.integration.domain.L900PacketDumper;
import com.neptunetg.mdce.integration.domain.MiuDumper;
import com.neptunetg.mdce.integration.gui.common.EnvProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 Extract packets from Dynamo and format as CSV.
 */
@Controller
@RequestMapping(value="/service")
public class PacketDumpController
{
    private static final Logger log = LoggerFactory.getLogger(PacketDumpController.class);

    @Autowired
    private PacketRepository packetRepo;

    @Autowired
    private CmiuRepository cmiuRepository;

    @Autowired
    private LoraRepository loraRepository;

    @Autowired
    private MiuSearchRepository miuSearchRepository;

    @Autowired
    private EnvProperties envProperties;


    /**
     * Return a CSV file containing extracted packets from Dynamo
     * @param daysOfRecord    number of days of record to retrieve, if not specified, the last 10 days are retrieved
     */
    @ResponseBody
    @RequestMapping(value=PacketDumpService.URL_PACKET_DUMP, method= RequestMethod.GET)
    public void getPacketDump(
            @RequestParam(value = PacketDumpService.L900_ID, required = false, defaultValue = "") String l900IdParam,
            @RequestParam(value = PacketDumpService.CMIU_ID, required = false, defaultValue = "") String cmiuIdParam,
            @RequestParam(value = PacketDumpService.ICCID, required = false, defaultValue = "") String iccid,
            @RequestParam(value = PacketDumpService.IMEI, required = false, defaultValue = "") String imei,
            @RequestParam(value = PacketDumpService.CMIU_SITE_ID, required = false, defaultValue = "") Integer cmiuSiteId,
            @RequestParam(value = PacketDumpService.DAYS, required = false, defaultValue = "10") Integer daysOfRecord,
            HttpServletResponse response
            ) throws IOException
    {
        List<MiuId> mius = getMius(l900IdParam, cmiuIdParam, iccid, imei, cmiuSiteId);
        final MiuType miuType = StringUtils.hasText(l900IdParam) ? MiuType.L900 : MiuType.CMIU;

        response.setContentType("text/csv");

        getMiuPacketsAndWriteTo(mius, daysOfRecord, miuType, response.getOutputStream());
    }

    private void getMiuPacketsAndWriteTo(List<MiuId> mius, Integer daysOfRecord, MiuType miuType, OutputStream outputStream)
    {
        try (final MiuDumper packetDumper = MiuType.CMIU.equals(miuType) ?
                new CmiuPacketDumper(outputStream) : new L900PacketDumper(outputStream, loraRepository))
        {
            final Instant now = Instant.now();
            final Instant startOfRange = now.minus(daysOfRecord.longValue(), ChronoUnit.DAYS);
            final List<Table> tables = packetRepo.getRelevantTablesForMiuPacketQuery(startOfRange, now);

            if (mius != null)
            {
                for (MiuId miuId : mius)
                {
                    Map<String, String> options = new HashMap<>();
                    options.put("miuId", miuId.toString());
                    List<MiuDetails> miuDetailsList = miuSearchRepository.getMius(options);

                    if (!miuDetailsList.isEmpty() && miuDetailsList.get(0).getType().equals(miuType))
                    {
                        packetRepo.streamMiuPacketsInsertedInDateRange(miuId.numericValue(), startOfRange, now, tables)
                                .forEach(packetDumper::writePacketToCsv);
                    }
                }
            }
        }
        catch (Exception e)
        {
            log.error("Error when writting packets to CSV", e);
        }
    }

    /**
     * Utility to get a list of MIUs which match one of the parameters
     * @param l900Id
     * @param cmiuId
     * @param iccid
     * @param imei
     * @param cmiuSiteId
     * @return A list of MIU IDs, or null if we can't find the MIU
     * @throws MiuDataException
     */
    private List<MiuId> getMius(String l900Id, String cmiuId, String iccid, String imei, Integer cmiuSiteId)
    {
        List<MiuId> mius = new ArrayList<>();

        try
        {
            if (StringUtils.hasText(cmiuId) || StringUtils.hasText(l900Id))
            {
                String miuIds = StringUtils.hasText(cmiuId) ? cmiuId : l900Id;

                for (String miuId : miuIds.split(","))
                {
                    mius.add(MiuId.fromString(miuId.trim()));
                }
            } else if (StringUtils.hasText(iccid))
            {
                CellularDeviceDetail cellularDeviceDetail =
                        cmiuRepository.getCellularConfigFromCellularDeviceIdentifier(CellularDeviceId.iccid(iccid));

                if (cellularDeviceDetail == null)
                {
                    return null;
                }

                mius.add(cellularDeviceDetail.getMiuId());
            } else if (StringUtils.hasText(imei))
            {
                CellularDeviceDetail cellularDeviceDetail =
                        cmiuRepository.getCellularConfigFromCellularDeviceIdentifier(CellularDeviceId.imei(imei));

                if (cellularDeviceDetail == null)
                {
                    return null;
                }

                mius.add(cellularDeviceDetail.getMiuId());
            } else if (cmiuSiteId != null)
            {
                miuSearchRepository.getMiusForUtility(SiteId.valueOf(cmiuSiteId)).forEach(md -> mius.add(md.getId()));
            }

            return mius;
        }
        catch (MiuDataException e)
        {
            log.error("Error accessing MIU details from identifier", e);
        }

        return null;
    }

    /**
     * Attaches environment properties bean to model
     */
    @ModelAttribute("envProperties")
    public EnvProperties envProperties() {

        return envProperties;

    }
}
