/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.cellular.CmiuConfigSetAssociation;
import com.neptunetg.common.data.miu.cellular.ConfigSet;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.common.packet.model.builder.CommandPacketBuilder;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest;
import com.neptunetg.mdce.integration.audit.AuditService;
import com.neptunetg.mdce.integration.data.refdata.RefDataRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.domain.CmiuConfigService;
import com.neptunetg.mdce.integration.domain.MiuCommandService;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.ForbiddenException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import com.neptunetg.mdce.integration.rest.json.miuconfig.JsonMiuConfig;
import com.neptunetg.mdce.integration.rest.json.miuconfig.JsonMiuConfigCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/*
* IF16, IF38
*/
@RestController
public class CmiuConfigController extends BaseTokenAuthenticatingRestController
{
	
	@Autowired
    private MiuCommandService miuCommandService;

    @Autowired
    private CmiuConfigService configService;

	@Autowired
	private RefDataRepository refDataRepository;

    @Autowired
    private AuditService auditService;

    @Autowired
    private MdceIpcPublishService awsQueueService;

    /**
     * Retrieve configuration info off all MIU matching the token (siteId).
     * If the token's site_id is 0, all MIUs for the MDCE will be returned.
     * This web service will be used by external systems to retrieve configuration info about an MIU.
     * @param token token id
     * @return one miu config or a collection of miu configs
     * @throws NotAuthorizedException
     */
    @RequestMapping(value = "/mdce/api/v1/miu/config", method = RequestMethod.GET)
    @ResponseBody
    public JsonMiuConfigCollection getMiuConfigListForSite(@RequestParam("token") String token,
                               HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException
    {
        final SiteId siteId = getClientSiteId(request);

        if (siteId.isUniversalSite())
        {
            return getAllCmiuConfigs();
        }

        return getCmiuConfigsForSiteId(siteId);
    }

    private JsonMiuConfigCollection getAllCmiuConfigs()
    {
        List<CmiuConfigSetAssociation> result = this.configService.getCurrentCmiuConfigCollection();
        return JsonMiuConfigCollection.from(result);
    }

    private JsonMiuConfigCollection getCmiuConfigsForSiteId(SiteId siteId)
    {
        List<CmiuConfigSetAssociation> result = this.configService.getCurrentCmiuConfigCollection(siteId);
        return JsonMiuConfigCollection.from(result);
    }

    @RequestMapping(value = "/mdce/api/v1/miu/{miuId}/config", method = RequestMethod.GET)
    @ResponseBody
    public JsonMiuConfig getSingleMiuConfig(
                               @PathVariable(value = "miuId") MiuId miuId,
                               HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException, ForbiddenException
    {
        final SiteId siteId = getClientSiteId(request);

        checkSiteIdHasAccessToCmiuOrThrowForbidden(miuId, siteId);

        Optional<String> distributerId = refDataRepository.lookupDistributerIdFromSiteId(siteId);

        try
        {
            return JsonMiuConfig.from(this.configService.getCurrentCmiuConfig(miuId), distributerId);
        }
        catch (MiuDataException e)
        {
            throw new MdceRestException("Error getting MIU config for MIU " + miuId, e);
        }

    }

    /**
     * IF38 This web service will be used by external systems to change configuration info for an MIU.
     * @param miuConfig
     * @return
     * @throws NotAuthorizedException
     */
    @RequestMapping(value = "/mdce/api/v1/miu/{miuId}/config", method = RequestMethod.PUT)
    public ResponseEntity setMiuConfig(@PathVariable(value = "miuId") MiuId miuId,
                               @RequestBody JsonMiuConfig miuConfig,
                               HttpServletRequest request) throws NotAuthorizedException, BadRequestException, ForbiddenException
    {

        final SiteId siteId = getClientSiteId(request);
        final String requester = this.auditService.getRequesterFromSiteId(siteId);

        //Check that the new config has a reporting interval that is a multiple of 60
        if(miuConfig.getMiuConfigDetail().getReporting().getIntervalMins() % 60 != 0)
        {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        checkSiteIdHasAccessToCmiuOrThrowForbidden(miuId, siteId);

        checkRequestMiuIdMatchesJsonObjectOrThrowBadRequest(miuId, miuConfig.getMiuId());

        ConfigSet newConfigSet = configService.setCurrentCmiuConfig(
                miuId, miuConfig.toDBConfigSet(), requester, "Applied by " + requester);

        queueChangeMiuConfigCommand(newConfigSet, requester, miuId);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }



    private void queueChangeMiuConfigCommand(ConfigSet miuConfig, String userName, MiuId miuId)
    {
        final TaggedPacket packet = new CommandPacketBuilder()
                .reportingInterval(
                        miuConfig.getReportingStartMins(),
                        miuConfig.getReportingIntervalMins()/60, //Reporting interval is set as hours in CMIU packet
                        miuConfig.getReportingNumberOfRetries(),
                        miuConfig.getReportingTransmitWindowMins(),
                        miuConfig.getReportingQuietStartMins(),
                        miuConfig.getReportingQuietEndMins())
                .recordingInterval(
                        miuConfig.getRecordingStartTimeMins(),
                        miuConfig.getRecordingIntervalMins())
                .buildSetRecordingReportingIntervalsCommandPacket(0);

        final MiuCommand command = createNewMiuCommandObject(packet, userName, miuId);
        miuCommandService.createMiuCommand(command);

        //send SQS to inform mdce-mqtt application to check message for pending commands to be processed
        awsQueueService.sendMessage(MdceIpcPublishService.SEND_COMMAND_QUEUE,
                String.format("%s: %s",
                        LocalDateTime.now(), "Set config from Set CMIU Config API")
        );
    }

    private MiuCommand createNewMiuCommandObject(TaggedPacket packet, String userName, MiuId miuId)
    {
        MiuCommand command = new MiuCommand();
        command.setMiuId(MiuId.valueOf(miuId.numericValue()));
        command.setCommandEnteredTime(Instant.now());
        command.setCommandParams(packet.toByteSequence());
        command.setCommandType(CommandRequest.CommandTypeCmiu.COMMAND_SET_RECORDING_REPORTING_INTERVALS.getCommandNumber());
        command.setTargetDeviceType(MiuType.CMIU.toString());
        command.setUserName(userName);

        return command;
    }

}
