/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/
package com.neptunetg.mdce.integration.scheduled.r900;

import java.io.File;
import java.util.concurrent.Future;

public interface AsyncFileTask
{

    Future<File> process(File fileToProcess);

}
