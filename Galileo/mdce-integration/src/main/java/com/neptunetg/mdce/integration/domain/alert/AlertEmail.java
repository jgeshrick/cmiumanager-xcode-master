/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.domain.alert;

import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import com.neptunetg.mdce.integration.domain.user.UserManagerService;
import com.neptunetg.mdce.integration.utility.SendEmailService;

import java.util.ArrayList;
import java.util.List;

/**
 * Collects multiple related alerts into a single email
 */
public class AlertEmail
{
    private static final String ALERT_FROM_MDCE_WARNING = "ALERT FROM MDCE: warning";
    private static final String ALERT_FROM_MDCE_ERROR = "ALERT FROM MDCE: error";
    private final String domainUrl;

    /**
     * If not warning then error
     */
    private boolean warning;

    private List<AlertDetails> alerts = null;

    protected AlertEmail(String domainUrl)
    {
        this.domainUrl = domainUrl;
    }

    public void addAlert(AlertDetails alert, boolean isWarning)
    {
        if (alert != null)
        {
            if (alerts == null)
            {
                this.warning = isWarning;
                this.alerts = new ArrayList<>();
                this.alerts.add(alert);
            }
            else
            {
                if (!isWarning && this.warning)
                {
                    //now an error
                    this.warning = false;
                }
                this.alerts.add(alert);
            }
        }
    }

    public boolean isAnyContent()
    {
        return this.alerts != null && !this.alerts.isEmpty();
    }

    public void send(UserManagerService userManagerService, SendEmailService emailUtility)
    {

        if (!isAnyContent())
        {
            return;
        }

        final List<UserDetails> userDetailsList = userManagerService.getUserList();
        final List<String> recipientsList = new ArrayList<>();
        final StringBuilder emailBody = new StringBuilder("You have been sent this email because you are signed up to MDCE alerts");

        boolean anySystemAlert = false;
        boolean anyNonSystemAlert = false;

        if (this.alerts.size() > 1)
        {
            emailBody.append("\n\nThis email contains ").append(this.alerts.size()).append(" alerts.");
        }

        for (AlertDetails alertDetails : this.alerts)
        {
            final String source = alertDetails.getAlertSource().split("/", 2)[0];
            if(source.contains("SYS"))
            {
                anySystemAlert = true;
            }
            else
            {
                anyNonSystemAlert = true;
            }

            emailBody
                    .append("\n\nAlert Source: ")
                    .append(alertDetails.getAlertSource())
                    .append("\n\n")
                    .append("Alert Message: \n")
                    .append(alertDetails.getRecentMessage())
                    .append("\n\n")
                    .append("Alert Details page: \n")
                    .append(domainUrl + ":9444/mdce-web/pages/alert/view/" + alertDetails.getAlertId());

        }

        for(UserDetails userDetails : userDetailsList)
        {
            boolean emailThisUser = false;
            if (anySystemAlert)
            {
                if(warning)
                {
                    if(userDetails.getUserAlertSettings().getSystemWarning())
                    {
                        emailThisUser = true;
                    }
                }
                else
                {
                    if(userDetails.getUserAlertSettings().getSystemError())
                    {
                        emailThisUser = true;
                    }
                }
            }

            if (anyNonSystemAlert)
            {
                if(warning)
                {
                    if(userDetails.getUserAlertSettings().getCmiuWarning())
                    {
                        emailThisUser = true;
                    }
                }
                else
                {
                    if(userDetails.getUserAlertSettings().getCmiuError())
                    {
                        emailThisUser = true;
                    }
                }
            }

            if (emailThisUser)
            {
                recipientsList.add(userDetails.getEmail());
            }
        }

        if (!recipientsList.isEmpty())
        {
            final String[] recipients = recipientsList.toArray(new String[recipientsList.size()]);

            emailUtility.sendEmail(recipients,
                    this.warning ? ALERT_FROM_MDCE_WARNING : ALERT_FROM_MDCE_ERROR,
                    emailBody.toString());
        }
    }

}
