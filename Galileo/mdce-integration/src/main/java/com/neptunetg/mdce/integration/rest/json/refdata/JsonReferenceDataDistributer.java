/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.rest.json.refdata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataDistributer;

import java.util.ArrayList;
import java.util.List;

/**
 * MdceReferenceDataDistributer
 *
 * "distributers": [{
 *   "customer_number": "08225300",
 *   "info": {
 *      ...
 *   }
 * }]
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonReferenceDataDistributer
{

    @JsonProperty("customer_number")
    private String customerNumber;

    @JsonProperty("info")
    private JsonReferenceDataInfo info;

    public JsonReferenceDataInfo getInfo()
    {
        return info;
    }

    public void setInfo(JsonReferenceDataInfo info)
    {
        this.info = info;
    }

    public String getCustomerNumber()
    {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber)
    {
        this.customerNumber = customerNumber;
    }


    public static List<JsonReferenceDataDistributer> from(List<MdceReferenceDataDistributer> distributers)
    {
        List<JsonReferenceDataDistributer> jsonReferenceDataDistributerList = new ArrayList<>();
        for (MdceReferenceDataDistributer distributer : distributers)
        {
            jsonReferenceDataDistributerList.add(from(distributer));
        }
        return jsonReferenceDataDistributerList;
    }

    private static JsonReferenceDataDistributer from(MdceReferenceDataDistributer distributer)
    {
        JsonReferenceDataDistributer jsonDistributer = new JsonReferenceDataDistributer();

        jsonDistributer.setCustomerNumber(distributer.getCustomerNumber());
        jsonDistributer.setInfo(JsonReferenceDataInfo.from(distributer.getInfo()));

        return jsonDistributer;
    }
}
