/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.integration.data.sim.SimDetails;
import com.neptunetg.mdce.integration.data.sim.SimAndModemDetails;
import com.neptunetg.mdce.integration.data.sim.SimSearchRepository;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Cellular service monitoring.
 */
@Service
public class CmiuReportedSimDetailsChecker implements AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(CmiuReportedSimDetailsChecker.class);
    private final MdceIpcSubscribeService awsQueueService;

    @Autowired
    private AlertService alertService;

    @Autowired
    private SimSearchRepository simRepo;

    @Autowired
    public CmiuReportedSimDetailsChecker(MdceIpcSubscribeService awsQueueService)
    {
        this.awsQueueService = awsQueueService;
        this.awsQueueService.register(this, MdceIpcSubscribeService.CMIU_CHECK_REPORTED_SIM_AND_MODEM_DETAILS,
                Duration.ofMinutes(1L), 1000);
    }


    private void checkReportedSimAndModemDetails(MiuId cmiuId, String imei, String iccid)
    {
        //MSPD-2239 Stripping whitespace that seems to be there on some packets
        imei = StringUtils.trimAllWhitespace(imei);
        iccid = StringUtils.trimAllWhitespace(iccid);

        if(!imei.equalsIgnoreCase("Not Available") && !iccid.equalsIgnoreCase("Not Available"))
        {
            List<SimAndModemDetails> miuMatches = simRepo.getCanByMiuid(cmiuId);
            List<SimAndModemDetails> iccidMatches = simRepo.getCanByIccid(iccid);
            List<SimAndModemDetails> imeiMatches = simRepo.getCanByImei(imei);

            if (miuMatches.size() > 1)
            {
                checkReportedSimAndModemDetailsForConflict(cmiuId, miuMatches, "CMIU", cmiuId.toString());
            }
            //Currently cannot happen due to DB restraints on the iccid column, but just in case that changes in the future...
            else if (iccidMatches.size() > 1)
            {
                checkReportedSimAndModemDetailsForConflict(cmiuId, iccidMatches, "ICCID", iccid);
            }
            else if (imeiMatches.size() > 1)
            {
                checkReportedSimAndModemDetailsForConflict(cmiuId, imeiMatches, "IMEI", imei);
            }
            else
            {

                final AlertSource alertSource = AlertSource.cmiuCellularConfigMismatch(cmiuId.numericValue());

                AlertDetails alertDetails = new AlertDetails();
                alertDetails.setAlertSource(alertSource.getAlertId());
                alertDetails.setAlertLevel(AlertLevel.ERROR);
                alertDetails.setAlertCreationDate(ZonedDateTime.now());
                alertDetails.setAlertState(AlertState.NEW);

                //These should never be empty due to the way this alert is triggered and the data that is returned into these variables.
                // This is a "just in case" so we can throw a log error and not blow up
                if (!miuMatches.isEmpty() || !iccidMatches.isEmpty() || !imeiMatches.isEmpty())
                {
                    //Uses 0 as the previous if statements ruled out there being more than one item in any of the lists
                    if (!miuMatches.get(0).getIccid().equals(iccid) || !miuMatches.get(0).getImei().equals(imei))
                    {
                        alertDetails.setRecentMessage("Reported ICCID: " + iccid + " or reported IMEI: " + imei +
                                " does not match with stored ICCID: " + miuMatches.get(0).getIccid() + " or stored IMEI: " + miuMatches.get(0).getImei() +
                                " for CMIU_ID: " + cmiuId + ".");
                    }
                    else if (!iccidMatches.get(0).getMiuId().equals(cmiuId) || !iccidMatches.get(0).getImei().equals(imei))
                    {
                        alertDetails.setRecentMessage("Reported CMIU_ID: " + cmiuId + " or reported IMEI: " + imei +
                                " does not match with stored CMIU_ID: " + iccidMatches.get(0).getMiuId().numericValue() + " or stored IMEI: " + iccidMatches.get(0).getImei() +
                                " for ICCID: " + iccid + ".");
                    }
                    else if (!imeiMatches.get(0).getMiuId().equals(cmiuId) || !imeiMatches.get(0).getIccid().equals(iccid))
                    {
                        alertDetails.setRecentMessage("Reported CMIU_ID: " + cmiuId + " or reported ICCID: " + iccid +
                                " does not match with stored CMIU_ID: " + imeiMatches.get(0).getMiuId().numericValue() + " or stored ICCID: " + imeiMatches.get(0).getIccid() +
                                " for IMEI: " + imei + ".");
                    }

                    if (StringUtils.hasText(alertDetails.getRecentMessage()))
                    {
                        try
                        {
                            alertService.addAlertAndSendEmail(alertDetails);
                        }
                        catch (InternalApiException e)
                        {
                            log.error("Error when adding alert for mismatch in reported SIM details: ", e);
                        }
                    }
                }
                else
                {
                    log.error("CMIU/ICCID/IMEI was empty and not found.");
                }
            }
        }
    }

    private void checkReportedSimAndModemDetailsForConflict( MiuId cmiuId, List<SimAndModemDetails> matches, String type, String passed)
    {
        /*************************************************************************************
         * if the second cmiu in the list is equal to the cmiu that was passed in
         * then we use the first cmiu in the list. if not we use the second cmiu.
         *
         * We do this because the data returned isn't always in the same order.
         * So we cannot guarantee that the first cmiu in the list is the same one that was passed in.
         *************************************************************************************/
        int cmiu2 = ( matches.get(1).getMiuId().numericValue() == cmiuId.numericValue() )
                ? matches.get(0).getMiuId().numericValue():matches.get(1).getMiuId().numericValue();

        final AlertSource alertSource = AlertSource.cmiuCellularConfigConflict(cmiuId.numericValue(), cmiu2);

        AlertDetails alertDetails = new AlertDetails();
        alertDetails.setAlertSource(alertSource.getAlertId());
        alertDetails.setAlertLevel(AlertLevel.ERROR);
        alertDetails.setAlertCreationDate(ZonedDateTime.now());
        alertDetails.setAlertState(AlertState.NEW);

        alertDetails.setRecentMessage("Multiple CAN pairings found for " + type + ": " + passed + ".");

        if (StringUtils.hasText(alertDetails.getRecentMessage()))
        {
            try
            {
                alertService.addAlertAndSendEmail(alertDetails);
            }
            catch (InternalApiException e)
            {
                log.error("Error when adding alert for conflict in reported SIM details: ", e);
            }
        }
    }

    private void checkReportedSimDetails(MiuId cmiuId, String imsi, String simId)
    {
        Map<String, String> options = new HashMap<>();
        options.put("iccid", simId);

        List<SimDetails> matchingSims = simRepo.getSims(options);

        final AlertSource alertSource = AlertSource.cmiuCellularConfigMismatch(cmiuId.numericValue());

        AlertDetails alertDetails = new AlertDetails();
        alertDetails.setAlertSource(alertSource.getAlertId());
        alertDetails.setAlertLevel(AlertLevel.WARNING);
        alertDetails.setAlertCreationDate(ZonedDateTime.now());
        alertDetails.setAlertState(AlertState.NEW);

        if(matchingSims.size() == 0)
        {
            alertDetails.setRecentMessage("Reported ICCID: " + simId + " is not associated with CMIU: " + cmiuId.toString());
        }
        else if(!matchingSims.get(0).getMiuId().equals(cmiuId))
        {
            alertDetails.setRecentMessage("Reported ICCID: " + simId +
                    " is not associated with the CMIU that reported it: " + cmiuId);
        }
        else if(!matchingSims.get(0).getImsi().equals(imsi))
        {
            alertDetails.setRecentMessage("Reported IMSI: " + imsi + " does not match the stored IMSI: " +
                    matchingSims.get(0).getImsi().toString() + " for CMIU: " + cmiuId.toString());
        }

        if(alertDetails.getRecentMessage() != null)
        {
            try
            {
                alertService.addAlertAndSendEmail(alertDetails);
            }
            catch (InternalApiException e)
            {
                log.debug("Error when adding alert for mismatch in reported SIM details: ", e);
            }
        }
    }


    /**
     * Process incoming request for checking reported CMIU sim details
     */
    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt,
                                          AtomicBoolean cancellationToken)
    {
        try
        {
            String[] receivedValues = message.split(",");

            // 0 = cmiu, 1 = IMSI, 2 = iccid, 3 = imei
            checkReportedSimAndModemDetails(MiuId.fromString(receivedValues[0]),
                    receivedValues[3], receivedValues[2]);

            return true;
        }
        catch (Exception e)
        {
            log.debug("Failed to check the reported SIM details for a CMIU:", e);
            return false;
        }
    }
}
