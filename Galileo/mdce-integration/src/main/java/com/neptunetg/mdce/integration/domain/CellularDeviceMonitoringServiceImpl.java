/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.cns.model.UsageHistory;
import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.common.data.miu.cellular.CmiuDataUsage;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.integration.cns.CellularNetworkServiceManager;
import com.neptunetg.mdce.integration.domain.alert.AlertEmail;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Cellular service monitoring.
 */
@Service
public class CellularDeviceMonitoringServiceImpl implements CellularDeviceMonitoringService, AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(CellularDeviceMonitoringServiceImpl.class);
    private static final long MAX_MONTHLY_DATA_USAGE_BYTES = 2L * 1024L * 1024L;   //2 MB per month per device limit
    private static final Duration MAX_DELAY_TILL_NO_DATA_IS_ERROR = Duration.of(24, ChronoUnit.HOURS);
    private static final int USAGE_WARNING_THRESHOLD_PERCENT = 100;
    private final MdceIpcSubscribeService awsQueueService;

    @Autowired
    private CellularNetworkServiceManager cellularNetworkServiceManager;

    @Autowired
    private AlertService alertService;

    @Autowired
    private CmiuRepository cmiuRepo;

    @Autowired
    public CellularDeviceMonitoringServiceImpl(MdceIpcSubscribeService awsQueueService)
    {
        this.awsQueueService = awsQueueService;
        this.awsQueueService.register(this, MdceIpcSubscribeService.CHECK_CELLULAR_BILLING, Duration.ofMinutes(1L), 1);
    }

    /**
     * Get and check all CMIU usage data (obtained from VZW) against threshold.
     * @param cancellationToken Allows the check to be aborted
     */
    @Override
    public boolean getAndCheckUsageData(AtomicBoolean cancellationToken) throws MiuDataException
    {
        //get a list of all miu which has cellular device registered

        List<CellularDeviceDetail> cellularDeviceList = cmiuRepo.getCellularDeviceList()
                .stream()
                .filter(cellularDeviceDetail -> (cellularDeviceDetail.getIccid() != null) && (!cellularDeviceDetail.getIccid().isEmpty()))
                .collect(Collectors.toList());

        final ZonedDateTime timeNow = ZonedDateTime.now(ZoneId.of("UTC"));
        final ZonedDateTime todayStartOfMonthUtc = timeNow.withDayOfMonth(1);  //get start of calendar month at 0:00 UTC
        final ZonedDateTime todayEndOfDayUtc = timeNow.plusDays(1).minus(1, ChronoUnit.MILLIS);

        final AlertEmail alertEmail = alertService.initAlertEmail();

        //for each cmiu, get usage data for this calendar month from VZW WNS API
        //we set a date range of 23:59:59.999 h, however, the data usage date for the current day is accounted to 0:00h, so we are still getting
        //the near real time data for today.
        for (CellularDeviceDetail cmiu : cellularDeviceList)
        {

            if (cancellationToken.get())
            {
                return false;
            }

            boolean checkStaleDataUsage = false;
            String staleUsageMessage = null;

            try
            {
                final String networkId = cmiu.getMno();
                if (StringUtils.hasText(networkId))
                {
                    final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(networkId);

                    List<UsageHistory> usageHistoryList = cellularNetworkService.getDeviceUsageHistory(cmiu.getIccid(),
                            todayStartOfMonthUtc,
                            todayEndOfDayUtc);

                    if (usageHistoryList != null && !usageHistoryList.isEmpty())
                    {
                        final long byteUsedThisMonth = usageHistoryList.stream()
                                .mapToLong(UsageHistory::getBytesUsed)
                                .sum();

                        Optional<UsageHistory> usageHistoryToday = usageHistoryList.stream()
                                .filter(usageHistory -> usageHistory.getTimeStamp()
                                        .toInstant().atZone(ZoneId.of("UTC")).toLocalDate()
                                        .equals(timeNow.toLocalDate()))
                                .findFirst();

                        final long byteUsedToday = usageHistoryToday.isPresent() ? usageHistoryToday.get().getBytesUsed() : 0;

                        log.debug("Cellular usage data for cmiu: {}, iccid: {} is daily {}, monthly {} ",
                                cmiu.getMiuId(), cmiu.getIccid(), byteUsedToday, byteUsedThisMonth);

                        checkMonthlyUseAllowanceAndRaiseAlertIfExceeded(timeNow, cmiu, byteUsedThisMonth, alertEmail);

                        //update usage data to RDS
                        try
                        {
                            cmiuRepo.saveMiuDataUsage(cmiu.getIccid(),
                                    timeNow.toLocalDate(),
                                    timeNow.toInstant(),
                                    (int) byteUsedToday, byteUsedThisMonth
                                    );
                        } catch (MiuDataException e)
                        {
                            log.error("Error updating miu data usage for CMIU " + cmiu, e);
                        }
                    } else
                    {

                        checkStaleDataUsage = true;
                        staleUsageMessage = "No usage history can be found for miu:" + cmiu.getMiuId() +
                                ", iccid:" + cmiu.getIccid() + " for last 12 hours";
                    }
                }
                else
                {
                    //empty MNO
                    log.warn("CMIU " + cmiu.getMiuId() + " has no MNO assigned, can't check network data");
                }
            }
            catch (Exception ex)
            {
                log.error("Error retrieving usage data for CMIU " + cmiu, ex);

                checkStaleDataUsage = true;
                staleUsageMessage =  "Error retrieving usage data from CNS for miu: " + cmiu.getMiuId() +
                        ", iccid: " + cmiu.getIccid() + " : " + ex.getMessage();
            }

            if (checkStaleDataUsage)
            {
                final CmiuDataUsage lastDataUsage = cmiuRepo.getLatestMiuDataUsage(cmiu.getMiuId());
                if (lastDataUsage != null)
                {
                    final Instant dataUsageTimestamp = lastDataUsage.getDataUsageDatetime();

                    if (dataUsageTimestamp != null && dataUsageTimestamp.plus(MAX_DELAY_TILL_NO_DATA_IS_ERROR).isBefore(timeNow.toInstant()))
                    {
                        log.warn(staleUsageMessage);
                        //MSPD-2302 Temporarily disabling this alert. Will be turned back on later
                        //raiseErrorRetrievingUsageAlert(cmiu.getMiuId(), timeNow, staleUsageMessage, alertEmail);
                    }
                }
            }

        }

        alertService.sendEmail(alertEmail);
        return true; //all CMIUs were checked
    }


    @Override
    public void checkMonthlyUseAllowanceAndRaiseAlertIfExceeded(ZonedDateTime timeNow, CellularDeviceDetail cmiu, long byteUsedThisMonth)
    {
        final AlertEmail alertEmail = alertService.initAlertEmail();
        checkMonthlyUseAllowanceAndRaiseAlertIfExceeded(timeNow, cmiu, byteUsedThisMonth, alertEmail);
        this.alertService.sendEmail(alertEmail);
    }

    /**
     * Check given byte use month to date against threshold and raise warning or alerts if it approaches or exceeds the limits
     * @param timeNow time where the alert if raised
     * @param cmiu cmiu cellular device
     * @param byteUsedThisMonth byteUsed month to date
     * @param alertEmail email to be populated with alerts
     */
    private void checkMonthlyUseAllowanceAndRaiseAlertIfExceeded(final ZonedDateTime timeNow, final CellularDeviceDetail cmiu, long byteUsedThisMonth,
                                                                AlertEmail alertEmail)
    {
        final long warningUsageLimitBytes = MAX_MONTHLY_DATA_USAGE_BYTES * USAGE_WARNING_THRESHOLD_PERCENT / 100L;

        if (byteUsedThisMonth > warningUsageLimitBytes)
        {
            log.debug("Cellular data over warning threshold, cmiu: {}, used: {}, allowed {}",
                    cmiu.getMiuId(), byteUsedThisMonth, MAX_MONTHLY_DATA_USAGE_BYTES);

            //alert warning
            raiseDataUsageExceedLimitAlert(cmiu.getMiuId(), timeNow, warningUsageLimitBytes, byteUsedThisMonth, false, alertEmail);
        }
        else
        {
            suppressDataUsageAlert(cmiu.getMiuId());
        }
    }

    private void raiseDataUsageExceedLimitAlert(MiuId miuId,
                                                ZonedDateTime timeNow, long allowableUsageBytes, long byteUsedThisMonth, boolean isError,
                                                AlertEmail alertEmail)
    {
        final AlertSource alertSource = AlertSource.cellularHighUsage(miuId.numericValue());
        final AlertDetails alertDetails = new AlertDetails();
        alertDetails.setAlertLevel(isError ? AlertLevel.ERROR : AlertLevel.WARNING);
        alertDetails.setAlertSource(alertSource.getAlertId());
        alertDetails.setAlertState(AlertState.NEW);

        if(isError)
        {
            alertDetails.setRecentMessage(
                    String.format(Locale.US, "Cellular usage exceeded error threshold. Used this month: %,d Bytes, Error Threshold: %,d Bytes", byteUsedThisMonth, allowableUsageBytes)
            );
        }
        else
        {
            alertDetails.setRecentMessage(
                    String.format(Locale.US, "Cellular usage exceeded warning threshold. Used this month: %,d Bytes, Warning Threshold: %,d Bytes", byteUsedThisMonth, allowableUsageBytes)
            );
        }

        alertDetails.setAlertCreationDate(timeNow);

        try
        {
            alertService.addAlert(alertDetails, alertEmail);
        }
        catch (InternalApiException e)
        {
            log.error("Cannot add alert", e);
        }
    }

    private void raiseErrorRetrievingUsageAlert(MiuId miuId, ZonedDateTime timeNow, String errorDetail, AlertEmail alertEmail)
    {
        final AlertSource alertSource = AlertSource.cellularService(miuId.numericValue());
        final AlertDetails alertDetails = new AlertDetails();
        alertDetails.setAlertLevel(AlertLevel.ERROR);
        alertDetails.setAlertSource(alertSource.getAlertId());
        alertDetails.setAlertState(AlertState.NEW);
        alertDetails.setRecentMessage("Cannot retrieve cellular usage data from CNS. " + errorDetail);
        alertDetails.setAlertCreationDate(timeNow);

        try
        {
            alertService.addAlert(alertDetails, alertEmail);
        }
        catch (InternalApiException e)
        {
            log.error("Cannot add alert", e);
        }
    }

    private void suppressDataUsageAlert(MiuId miuId)
    {
        final AlertSource alertSource = AlertSource.cellularHighUsage(miuId.numericValue());
        AlertDetails alertDetails = alertService.getAlertNotCleared(alertSource.getAlertId());

        //An alert is present
        if (alertDetails != null)
        {
            alertService.setAlertStateToStale(alertDetails.getAlertId());
        }
    }


    /**
     * Process incoming request for retrieving CNS usage data and checking for alerts.
     */
    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        try
        {
            return getAndCheckUsageData(cancellationToken);
        }
        catch (MiuDataException e)
        {
            return false;
        }
    }


}
