/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;


import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.CmiuModemFirmwareDetails;
import com.neptunetg.mdce.common.internal.miu.service.CmiuModemFirmwareService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides image update services and image registration services
 */
@RestController
@RequestMapping("/service")
public class CmiuModemFirmwareServiceController implements CmiuModemFirmwareService
{
    private static final Logger log = LoggerFactory.getLogger(CmiuModemFirmwareServiceController.class);

    @Value("#{envProperties['cmiu.modem.firmware.image.folder']}")
    String modemFirmwareImageFolder;

    @Override
    @RequestMapping(value = URL_CMIU_MODEM_FIRMWARE_LIST)
    public List<CmiuModemFirmwareDetails> getAvailableCmiuModemFirmwareImages() throws InternalApiException
    {
        File imageFolder = new File(modemFirmwareImageFolder);
        File[] listOfFiles = imageFolder.listFiles();

        List<CmiuModemFirmwareDetails> cmiuModemFirmwareDetailsList = new ArrayList<>();
        int i = 0;

        if(listOfFiles != null)
        {
            for (File file : listOfFiles)
            {
                CmiuModemFirmwareDetails cmiuModemFirmwareDetails = new CmiuModemFirmwareDetails();
                cmiuModemFirmwareDetails.setImageName(file.getName());
                cmiuModemFirmwareDetails.setId(i);
                cmiuModemFirmwareDetailsList.add(cmiuModemFirmwareDetails);
                i++;
            }
        }

        return cmiuModemFirmwareDetailsList;
    }
}
