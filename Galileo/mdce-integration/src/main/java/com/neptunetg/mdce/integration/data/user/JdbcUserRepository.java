/*
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 */

package com.neptunetg.mdce.integration.data.user;


import com.neptunetg.common.data.util.JdbcDateUtil;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.common.internal.user.model.MdceUserRole;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequest;
import com.neptunetg.mdce.common.internal.user.model.UserAlertSettings;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by WJD1 on 31/07/2015.
 * Iteractions with DB for user details
 */
@Repository
public class JdbcUserRepository implements UserRepository
{
    public static final String FIND_OPTION_EMAIL = "email";
    private JdbcTemplate db;

    @Autowired
    public JdbcUserRepository(JdbcTemplate db)
    {
        this.db = db;
    }

    /**
     * Add User to system
     * @param userName
     * @param email
     * @param userLevel
     * @param hash The hash of the users password + salt
     * @param salt An array of bytes added to the end of the password before it's hashed to increase it's randomness
     * @param iterations The number of iterations the PBKDF2 hashing algorithm used, and should use when hashing password attempts
     * @return
     */
    @Override
    public boolean addUser(String userName, String email, MdceUserRole userLevel, byte[] hash, byte[] salt, int iterations)
    {
        final String insertSql = "INSERT INTO mdce.user_list " +
        "(user_name, user_email, user_password, user_salt, user_pbkdf2_inter, user_level) " +
        "VALUES (?, ?, ?, ?, ?, ?)";

        int numRows = this.db.update(insertSql,
                userName,
                email,
                hash,
                salt,
                iterations,
                userLevel.name());

        if(numRows > 0)
        {
            return true;
        }

        return false;
    }

    /**
     * Remove user from system
     * @param userId
     * @return
     */
    @Override
    public boolean removeUser(long userId)
    {
        final String removeUsersSitesSql = "DELETE FROM mdce.site_user WHERE " +
                                           "user_list_user_id=" + Long.toString(userId);

        this.db.update(removeUsersSitesSql);

        final String removeUserSql = "DELETE FROM mdce.user_list WHERE " +
                                    "user_id=" + Long.toString(userId);

        int numRows = this.db.update(removeUserSql);

        if(numRows > 0)
        {
            return true;
        }

        return false;
    }

    /**
     * Set users password
     * @param userId
     * @param hash
     * @param salt
     * @param iterations
     * @return
     */
    @Override
    public boolean setUserPassword(long userId, byte[] hash, byte[] salt, int iterations)
    {
         final String setPasswordSql = "UPDATE mdce.user_list SET " +
                 "user_password=?, user_salt=?, user_pbkdf2_inter=? " +
                 "WHERE user_id=?";

        int numRows = this.db.update(setPasswordSql,
                hash,
                salt,
                iterations,
                userId);

        if(numRows > 0)
        {
            return true;
        }

        return false;
    }

    /**
     * Set a users level
     * @param userId
     * @param userLevel
     * @return
     */
    @Override
    public boolean setUserLevel(long userId, MdceUserRole userLevel)
    {
        final String setLevelSql = "UPDATE mdce.user_list SET " +
                "user_level=? WHERE user_id=?";

        int numRows = this.db.update(setLevelSql,
                userLevel.name(),
                userId);

        if(numRows > 0)
        {
            return true;
        }

        return false;
    }

    /**
     * Set a users email address
     * @param userId
     * @param email
     * @return
     */
    @Override
    public boolean setUserEmail(long userId, String email)
    {
        final String setEmailSql = "UPDATE mdce.user_list SET " +
                "user_email=? WHERE user_id=?";

        int numRows = this.db.update(setEmailSql,
                email,
                userId);

        if(numRows > 0)
        {
            return true;
        }

        return false;
    }

    /**
     * Set a user's username
     * @param userId
     * @param userName The new username, which must not already be in use
     * @return
     */
    @Override
    public boolean setUserName(long userId, String userName)
    {
        final String setUserNameSql = "UPDATE mdce.user_list " +
                "SET user_name=? " +
                "WHERE user_id=? " +
                // Prevent duplicate usernames
                "AND NOT EXISTS (SELECT user_id FROM (SELECT user_id from mdce.user_list WHERE user_name = ?) AS t1)";

        int numRows = this.db.update(setUserNameSql,
                userName,
                userId,
                userName);

        if(numRows > 0)
        {
            return true;
        }

        return false;
    }

    /**
     * Same as listUsers but referenced by username not userID
     * @param userName
     * @return null if user doesn't exist
     */
    @Override
    public UserDetails getUserDetailsByUserName(String userName)
    {
        String sql = "SELECT * FROM mdce.user_list WHERE user_name=?";
        List<UserDetails> userDetails = (this.db.query(sql, this::userDetailsMapper, userName));

        if(userDetails.size() > 0)
        {
            return userDetails.get(0);
        }

        return null;
    }

    /**
     * Find user details by email.
     *
     * @param userEmail user email
     * @return null if user doesn't exist
     */
    @Override
    public List<UserDetails> getUserDetailsByUserEmail(String userEmail)
    {
        String sql = "SELECT * FROM mdce.user_list WHERE user_email=?";
        List<UserDetails> userDetails = this.db.query(sql, this::userDetailsMapper, userEmail);

        if (!userDetails.isEmpty())
        {
            return userDetails;
        }

        return null;
    }

    /**
     * Retrieve a user row based on user id
     * @param userId id of user
     * @return
     */
    @Override
    public UserDetails getUserDetailsById(long userId)
    {
        String sql = "SELECT * FROM mdce.user_list WHERE user_id = ?";
        List<UserDetails> userDetails = (this.db.query(sql, this::userDetailsMapper, userId));

        if(userDetails.size() > 0)
        {
            return userDetails.get(0);
        }

        return null;
    }

    /**
     * Get a list of all the users in the system
     * @return
     */
    @Override
    public List<UserDetails> getUserList()
    {
        String sql = "SELECT * FROM mdce.user_list " +
                "ORDER BY user_name ASC";
        return (this.db.query(sql, this::userDetailsMapper));
    }

    /**
     * Search for a user, currently by exact email match only.
     */
    @Override
    public List<UserDetails> findUsers(Map<String, Object> options)
    {
        Map<String, Object> whereClauses = new HashMap<>();

        if (options.containsKey(FIND_OPTION_EMAIL))
        {
            whereClauses.put("user_email = ?", options.get(FIND_OPTION_EMAIL));
        }

        String sql =
                "SELECT * FROM mdce.user_list " +
                (whereClauses.size() == 0 ? "" : "WHERE ") +
                whereClauses.entrySet().stream().map(Map.Entry::getKey).collect(Collectors.joining(" AND "));

        Object[] parameters =
                whereClauses.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList()).toArray();

        return this.db.query(sql, parameters, this::userDetailsMapper);
    }

    /**
     * Get a list of site ID's associated to a user
     * @param userId
     * @return
     */
    @Override
    public List<SiteDetails> getSiteListForUser(long userId)
    {
        String sql = "SELECT site_list.site_id, site_list.name " +
                "FROM mdce.site_list INNER JOIN mdce.site_user ON " +
                "site_list.site_id=site_user.site_list_site_id " +
                "WHERE user_list_user_id=?";

        return (this.db.query(sql, this::siteDetailsMapper, userId));
    }

    /**
     * Get a list of site ID's a user doesn't have access to
     * @param userId
     * @return
     */
    @Override
    public List<SiteDetails> getSiteListNotForUser(long userId)
    {
        String sql = "SELECT site_list.site_id, site_list.name " +
                     "FROM mdce.site_list WHERE site_id NOT IN " +
                     "(SELECT site_list_site_id FROM mdce.site_user " +
                     "WHERE user_list_user_id=?)";

        return (this.db.query(sql, this::siteDetailsMapper, userId));
    }

    /**
     * Add a site ID to a user
     * @param userId
     * @param siteId
     * @return
     */
    @Override
    public boolean addSiteToUser(long userId, long siteId)
    {
        final String insertSql = "INSERT INTO mdce.site_user " +
        "(user_list_user_id, site_list_site_id) " +
        "VALUES (?, ?)";

        this.db.update(insertSql,
                userId,
                siteId);

        return true;
    }

    /**
     * Remove site ID from a user
     * @param userId
     * @param siteId
     * @return
     */
    @Override
    public boolean removeSiteFromUser(long userId, long siteId)
    {
        final String removeSiteFromUserSql = "DELETE FROM mdce.site_user WHERE " +
                                    "user_list_user_id=" + Long.toString(userId) +
                                    " AND site_list_site_id=" + Long.toString(siteId);

        this.db.update(removeSiteFromUserSql);

        return true;
    }

    @Override
    public UserAlertSettings getUserAlertSettings(long userId)
    {
        String sql = "SELECT * FROM mdce.user_list WHERE " +
                    "user_id = ?";

        List<UserAlertSettings> userAlertSettingsList = (this.db.query(sql, this::userAlertSettingsMapper, userId));

        return userAlertSettingsList.get(0);
    }

    @Override
    public boolean setUserAlertSettings(UserAlertSettings userAlertSettings)
    {
        final String sql = "UPDATE mdce.user_list " +
        "SET user_alert_system_warning = ?, user_alert_system_error = ?, " +
        "user_alert_cmiu_warning = ?, user_alert_cmiu_error = ? " +
        "WHERE user_id = ?";

        this.db.update(sql,
                userAlertSettings.getSystemWarning(),
                userAlertSettings.getSystemError(),
                userAlertSettings.getCmiuWarning(),
                userAlertSettings.getCmiuError(),
                userAlertSettings.getUserId());

        return true;
    }

    @Override
    public boolean setUserAccountLockoutEnabled(long userId, boolean value)
    {
        final String sql = "UPDATE mdce.user_list SET account_lockout_enabled = ? WHERE user_id = ?";

        this.db.update(sql, value, userId);

        return true;
    }

    @Override
    public boolean setUserFailedLoginAttempts(long userId, long failedLoginAttempts)
    {
        final String sql = "UPDATE mdce.user_list SET failed_login_attempts = ? WHERE user_id = ?";

        this.db.update(sql, failedLoginAttempts, userId);

        return true;
    }

    @Override
    public boolean setUserDisabledUntil(long userId, Instant disabledUntil)
    {
        final String sql = "UPDATE mdce.user_list SET disabled_until = ? WHERE user_id = ?";

        this.db.update(sql, JdbcDateUtil.bindDate(disabledUntil), userId);

        return true;
    }

    @Override
    public boolean setAllowConcurrentSessions(long userId, boolean allowConcurrentSessions)
    {
        final String sql = "UPDATE mdce.user_list SET allow_concurrent_sessions = ? WHERE user_id = ?";

        this.db.update(sql, allowConcurrentSessions, userId);

        return true;
    }

    @Override
    public boolean addPasswordResetRequest(String token, long userId, String matchedUserName,
                                           String enteredEmailAddress, Instant validUntilTimestamp,
                                           String requestorIpAddress)
    {
        final String insertSql =
                "INSERT INTO mdce.password_reset_request ("+
                    "`token`, `user_id`, `matched_user_name`, `entered_email_address`, `created_timestamp`," +
                    "`valid_until_timestamp`, `requestor_ip_address`) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";

        int numRows = this.db.update(insertSql,
                token, userId, matchedUserName, enteredEmailAddress, JdbcDateUtil.bindDate(Instant.now()),
                JdbcDateUtil.bindDate(validUntilTimestamp), requestorIpAddress);

        if(numRows > 0)
        {
            return true;
        }

        return false;
    }

    @Override
    public boolean claimPasswordResetRequest(long passwordResetRequestId, String token, String claimantIpAddress)
    {
        final String updateSql =
                "UPDATE mdce.password_reset_request "+
                "SET " +
                    "claimed = 1, " +
                    "claimed_timestamp = ?, " +
                    "claimant_ip_address = ? " +
                "WHERE password_reset_request_id = ? " +
                "AND token = ?";

        Timestamp claimedDbTimestamp = Timestamp.from(Instant.now());

        int numRows = this.db.update(updateSql,
                claimedDbTimestamp, claimantIpAddress, passwordResetRequestId, token);

        if(numRows > 0)
        {
            return true;
        }

        return false;
    }


    @Override
    public int expirePasswordResetRequestsByUser(long userId)
    {
        final String updateSql =
                "UPDATE mdce.password_reset_request "+
                "SET valid_until_timestamp = ? " +
                "WHERE user_id = ? " +
                "AND claimed = 0 " +
                "AND valid_until_timestamp > ?";

        Timestamp nowDbTimestamp = Timestamp.from(Instant.now());

        int updatedCount = this.db.update(updateSql,
                nowDbTimestamp, userId, nowDbTimestamp);

        return updatedCount;
    }

    @Override
    public PasswordResetRequest getPasswordResetRequestByToken(String token)
    {
        String sql = "SELECT * FROM mdce.password_reset_request WHERE token = ?";
        List<PasswordResetRequest> passwordResetRequests = this.db.query(sql, this::passwordResetRequestMapper, token);

        if(passwordResetRequests.size() > 0)
        {
            return passwordResetRequests.get(0);
        }

        return null;
    }

    /**
     * Row mapper for mapping SQL site list query to POJO
     * @param rs
     * @param rownumber
     * @return
     * @throws SQLException
     */
    private SiteDetails siteDetailsMapper(ResultSet rs, int rownumber) throws SQLException
    {
        SiteDetails siteDetails = new SiteDetails();
        siteDetails.setSiteId(rs.getInt("site_id"));
        siteDetails.setSiteName(rs.getString("name"));

        return siteDetails;
    }

    /**
     * Row mapper for mapping SQL query object to POJO
     * @throws SQLException
     */
    private UserDetails userDetailsMapper(ResultSet rs, int rownumber) throws SQLException
    {
        UserDetails userDetails = new UserDetails();
        userDetails.setUserId(rs.getInt("user_id"));
        userDetails.setUserName(rs.getString("user_name"));
        userDetails.setEmail(rs.getString("user_email"));
        userDetails.setHash(rs.getBytes("user_password"));
        userDetails.setSalt(rs.getBytes("user_salt"));
        userDetails.setSaltIterations(rs.getInt("user_pbkdf2_inter"));
        userDetails.setUserLevel(MdceUserRole.fromSqlAccessLevel(rs.getString("user_level")));

        userDetails.setAccountLockoutEnabled(rs.getBoolean("account_lockout_enabled"));
        userDetails.setFailedLoginAttempts(rs.getLong("failed_login_attempts"));
        Timestamp disabledUntilTimestamp = rs.getTimestamp("disabled_until");

        if (disabledUntilTimestamp != null)
        {
            userDetails.setDisabledUntil(disabledUntilTimestamp.toInstant());
        }

        // Interpreted as false if NULL.
        userDetails.setAllowConcurrentSessions(rs.getBoolean("allow_concurrent_sessions"));

        UserAlertSettings userAlertSettings = new UserAlertSettings();
        userAlertSettings.setSystemWarning(rs.getBoolean("user_alert_system_warning"));
        userAlertSettings.setSystemError(rs.getBoolean("user_alert_system_error"));
        userAlertSettings.setCmiuWarning(rs.getBoolean("user_alert_cmiu_warning"));
        userAlertSettings.setCmiuError(rs.getBoolean("user_alert_cmiu_error"));

        userDetails.setUserAlertSettings(userAlertSettings);

        return userDetails;
    }

    /**
     * Row mapper for mapping SQL query object to POJO
     * @param rs
     * @param rownumber
     * @return
     * @throws SQLException
     */
    private UserAlertSettings userAlertSettingsMapper(ResultSet rs, int rownumber) throws SQLException
    {
        UserAlertSettings userAlertSettings = new UserAlertSettings();
        userAlertSettings.setUserId(rs.getInt("user_id"));
        userAlertSettings.setSystemWarning(rs.getBoolean("user_alert_system_warning"));
        userAlertSettings.setSystemError(rs.getBoolean("user_alert_system_error"));
        userAlertSettings.setCmiuWarning(rs.getBoolean("user_alert_cmiu_warning"));
        userAlertSettings.setCmiuError(rs.getBoolean("user_alert_cmiu_error"));

        return userAlertSettings;
    }

    private PasswordResetRequest passwordResetRequestMapper(ResultSet rs, int rownumber) throws SQLException
    {
        PasswordResetRequest passwordResetRequest = new PasswordResetRequest();

        Long userId = (Long)rs.getObject("user_id");
        passwordResetRequest.setPasswordResetRequestId(rs.getLong("password_reset_request_id"));
        passwordResetRequest.setToken(rs.getString("token"));
        passwordResetRequest.setUserId(userId == null ? null : userId.intValue());
        passwordResetRequest.setMatchedUserName(rs.getString("matched_user_name"));
        passwordResetRequest.setEnteredEmailAddress(rs.getString("entered_email_address"));
        passwordResetRequest.setCreatedTimestamp(rs.getTimestamp("created_timestamp").toInstant());
        passwordResetRequest.setValidUntilTimestamp(rs.getTimestamp("valid_until_timestamp").toInstant());
        passwordResetRequest.setRequestorIpAddress(rs.getString("requestor_ip_address"));
        passwordResetRequest.setClaimed(rs.getBoolean("claimed"));

        Timestamp claimedDbTimestamp = rs.getTimestamp("claimed_timestamp");

        if (claimedDbTimestamp != null)
        {
            passwordResetRequest.setClaimedTimestamp(claimedDbTimestamp.toInstant());
        }

        passwordResetRequest.setClaimantIpAddress(rs.getString("claimant_ip_address"));

        return passwordResetRequest;
    }

    /**
     * Define the user levels
     */
    public enum UserLevels
    {
        LIMITED_READ ("limited_read"),
        LIMITED_READ_WRITE ("limited_read_write"),
        CMIU_CREATOR ("cmiu_creator"),
        READ_ONLY ("read_only"),
        SUPER_USER ("super_user");

        private final String sqlString;

        private UserLevels(String sqlEnumString)
        {
            this.sqlString = sqlEnumString;
        }

        public String getSqlString()
        {
            return this.sqlString;
        }
    }
}
