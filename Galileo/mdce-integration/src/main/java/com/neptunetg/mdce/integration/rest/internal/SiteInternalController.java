/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.common.internal.site.service.SiteService;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.data.site.SiteRepository;
import com.neptunetg.mdce.integration.rest.BaseTokenAuthenticatingRestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/service")
public class SiteInternalController extends BaseTokenAuthenticatingRestController implements SiteService
{

    private static final Logger logger = LoggerFactory.getLogger(SiteInternalController.class);

    @Autowired
    private SiteRepository siteRepo;

    @Override
    @ResponseBody
    @RequestMapping(value = SiteService.URL_SITE_LIST, method = RequestMethod.GET)
    public List<SiteDetails> getSiteList() throws InternalApiException
    {
        return siteRepo.getSiteList();
    }

    @Override
    @ResponseBody
    @RequestMapping(value = SiteService.URL_SITE_DETAILS, method = RequestMethod.GET)
    public SiteDetails getSiteDetails(int siteId) throws InternalApiException
    {
        return siteRepo.getSiteDetails(SiteId.valueOf(siteId));
    }

    /**
     * Finds sites that have not contacted mdce between a configurable amount of time.
     *
     * @param siteConnectionCheckHours max amount of hours an site should be talking
     *                                 to mdce anything after that is an issue.
     * @param siteConnectionIgnoreAfterHours max amount of hours a site can be inactive
     *                                       before we declare inactive and ignore.
     * @return list of sites
     */
    @Override
    public List<SiteDetails> getSitesNotRequestingDataWithinXHours(int siteConnectionCheckHours, int siteConnectionIgnoreAfterHours) throws InternalApiException
    {
        return siteRepo.getSitesNotRequestingDataWithinXHours(siteConnectionCheckHours, siteConnectionIgnoreAfterHours);
    }
}
