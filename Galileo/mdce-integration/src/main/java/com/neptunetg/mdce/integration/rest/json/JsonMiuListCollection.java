/*
 *  Neptune Technology Group
 *  Copyright 2017 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.mdce.integration.data.miu.MiuDetails;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

/**
    JSON object for return multiple MIUs
*/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonMiuListCollection
{

    private final ZoneId timeZone;

    private JsonMiuListCollection(ZoneId timeZone)
    {
        this.timeZone = timeZone;
    }

    @JsonProperty("mius")
    private List<JsonMiuDetail> miuList;

    public List<JsonMiuDetail> getMiuList()
    {
        return miuList;
    }

    public void setMiuList(List<JsonMiuDetail> miuList)
    {
        this.miuList = miuList;
    }

    /**
     * Constructs a new JsonMiuListCollection from the supplied MiuDetails list.
     * @param miuDetailList The source data for the new instance.
     * @param timeZoneForDates The ZoneId in which to display dates and times.
     * @param returnIdentities Sets a value indicating whether to include ICCID, IMEI and EUI identity details. When
     *                         false, the ICCID, IMEI and EUI are all set to null; otherwise, the values are included
     *                         where present in the source data.
     * @return The JSON-convertible representation of the supplied MIU details.
     */
    public static JsonMiuListCollection from(List<MiuDetails> miuDetailList, ZoneId timeZoneForDates, boolean returnIdentities)
    {
        JsonMiuListCollection miuList = new JsonMiuListCollection(timeZoneForDates);

        List<JsonMiuDetail> result;

        result = miuDetailList.stream()
                .map((miuDetail) -> miuList.addMiu(miuDetail, returnIdentities))
                .collect(Collectors.toList());
        miuList.setMiuList(result);

        return miuList;
    }

    private JsonMiuDetail addMiu(MiuDetails miuDetail, boolean returnIdentities)
    {
        return new JsonMiuDetail(miuDetail, timeZone, returnIdentities);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static final class JsonMiuDetail
    {
        @JsonProperty("miu_id")
        private int miuId;

        @JsonProperty("site_id")
        private int siteId;

        @JsonProperty("iccid")
        private String iccid;

        @JsonProperty("msisdn")
        private String msisdn;

        @JsonProperty("network_provider")
        private String networkProvider;

        @JsonProperty("miu_type")
        private String miuType;

        @JsonProperty("imei")
        private String imei;

        @JsonProperty("eui")
        private String eui;

        @JsonProperty("first_insert_date")
        private String firstInsertDate;

        @JsonProperty("last_insert_date")
        private String lastInsertDate;

        public JsonMiuDetail(MiuDetails miuDetail, ZoneId timeZone, boolean includeSecondaryIdentities)
        {
            this.miuId = miuDetail.getId().numericValue();
            this.siteId = miuDetail.getSiteId().numericValue();

            if (miuDetail.getFirstInsertDate() != null)
            {
                this.firstInsertDate = miuDetail.getFirstInsertDate().atZone(timeZone).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
            }

            if (miuDetail.getLastInsertDate() != null)
            {
                this.lastInsertDate = miuDetail.getLastInsertDate().atZone(timeZone).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
            }

            if (includeSecondaryIdentities)
            {
                this.iccid = miuDetail.getIccid();
                this.imei = miuDetail.getImei();
                this.eui = miuDetail.getEui();
                this.networkProvider = miuDetail.getNetworkProvider();
                this.msisdn = miuDetail.getMsisdn();
                this.miuType = miuDetail.getType() == null ? null : miuDetail.getType().toString();
            }
        }

        public long getMiuId()
        {
            return miuId;
        }


        public int getSiteId()
        {
            return siteId;
        }


        public String getFirstInsertDate()
        {
            return firstInsertDate;
        }


        public String getLastInsertDate()
        {
            return lastInsertDate;
        }


        public String getIccid()
        {
            return iccid;
        }

        public void setIccid(String iccid)
        {
            this.iccid = iccid;
        }

        public String getImei()
        {
            return imei;
        }

        public void setImei(String imei)
        {
            this.imei = imei;
        }

        public String getEui()
        {
            return eui;
        }

        public void setEui(String eui)
        {
            this.eui = eui;
        }
    }

}
