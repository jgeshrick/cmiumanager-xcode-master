/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain.alert;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLog;

import java.util.List;

/**
* Service interface for alerts
 */
public interface AlertService
{
    List<AlertDetails> getNewAlertList(String alertSource);

    List<AlertDetails> getHandledAlertList( String alertSource);

    List<AlertDetails> getStaleAlertList(String alertSource);

    List<AlertDetails> getClearedAlertList(String alertSource);

    AlertDetails getAlert(long alertId);

    AlertDetails getAlertNotCleared(String alertSource);

    boolean addAlertAndSendEmail(AlertDetails alertDetails) throws InternalApiException;

    boolean addAlert(AlertDetails alertDetails, AlertEmail email) throws InternalApiException;

    void sendEmail(AlertEmail email);

    boolean setAlertStateToNew(long alertId);

    boolean setAlertStateToHandled(long alertId);

    boolean setAlertStateToStale(long alertId);

    boolean setAlertStateToCleared(long alertId);

    boolean setAlertLevelToWarning(long alertId);

    boolean setAlertLevelToError(long alertId);

    boolean setAlertTicketId(AlertDetails alertDetails);

    boolean addAlertLogMessage(AlertDetails alertDetails);

    List<AlertLog> getAlertLog(long alertId);

    AlertEmail initAlertEmail();

    Long lastInsertId();

}
