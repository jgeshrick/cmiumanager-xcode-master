/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.integration.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Return the application version in the applicationVersion.properties of the file
 */
@Configuration
public class ApplicationVersion
{

    @Value("#{appVersion['applicationVersion']}")
    private String applicationVersion;

    /**
     * Get the application version from applicationVersion.properties
     * @return application version.
     */
    public String getApplicationVersion()
    {
        return applicationVersion;
    }
}
