/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.cns.callback.verizon;

import com.neptunetg.common.cns.MnoCallbackException;
import com.neptunetg.common.cns.verizonwns.VerizonCallbackSoapControllerBase;
import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.mdce.integration.cns.CnsUtils;
import com.verizon.callback.CallbackRequest;
import com.verizon.callback.CallbackResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;

/**
 * SOAP endpoint for Verizon WNS callback service
 */
@Endpoint
public class VerizonCallbackServiceSoapEndpoint extends VerizonCallbackSoapControllerBase
{

    @Autowired
    private CmiuRepository cmiuRepo;

    private static final Logger log = LoggerFactory.getLogger(VerizonCallbackServiceSoapEndpoint.class);

    public VerizonCallbackServiceSoapEndpoint()
    {
        log.trace("CallbackServiceSoapEndpoint ctor");

        //setup callback handler
        setDeviceStateChangeHandler((deviceIdentifierKind, msisdn, state) ->
        {
            //value: "Restore", "Suspend", from callback results. Other values, according to doc, is "Activate" and "Deactivate"
            try
            {
                cmiuRepo.changeCellularDeviceState(CnsUtils.cellularDeviceId(deviceIdentifierKind, msisdn), state);
                return true;
            }
            catch (MiuDataException ex)
            {
                log.error("Error saving device state change to repo", ex);
                return false;
            }
        });

        setDeviceUsageNotificationHandler((iccid, useBytes) ->
        {
            //insert data usage to repo
            try
            {
                cmiuRepo.saveMiuDataUsage(iccid,
                        LocalDate.now(),   //FIXME date should be usageDate
                        Instant.now(),
                        useBytes, 0
                        );

                return true;
            }
            catch (MiuDataException e1)
            {
                throw new RuntimeException(e1);
            }
        });

    }

    @Override
    @PayloadRoot(localPart = "CallbackRequest", namespace = "http://nphase.com/unifiedwebservice/v2")
    @ResponsePayload
    public CallbackResponse processCallbackRequest(@RequestPayload CallbackRequest request) throws MnoCallbackException
    {
        log.debug("VZW callback: Data={}, Device={}, RequestId={}, Username={}", request.getData(), request.getDevice(), request.getRequestId(), request.getUsername());

        return super.processCallbackRequest(request);
    }

}
