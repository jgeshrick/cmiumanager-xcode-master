/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.data.sim;

import com.neptunetg.common.data.miu.MiuId;

import java.io.Serializable;
import java.time.Instant;

public class SimDetails implements Serializable {

    private final long simId;
    private final String iccid;
    private final MiuId miuId;
    private final String lifeCycleState;
    private final Instant creationDate;
    private final String network;
    private final String msisdn;
    private final String imsi;

    public SimDetails(long simId, String iccid, MiuId miuId, String lifeCycleState, Instant creationDate, String network, String msisdn, String imsi)
    {
        super();
        this.iccid = iccid;
        this.lifeCycleState = lifeCycleState;
        this.miuId = miuId;
        this.creationDate = creationDate;
        this.network = network;
        this.simId = simId;
        this.msisdn = msisdn;
        this.imsi = imsi;
    }

    public long getSimId() { return simId; }

    public String getIccid() {
        return iccid;
    }

    public String getLifeCycleState() { return lifeCycleState; }

    public MiuId getMiuId() { return miuId; }

    public Instant getCreationDate() { return creationDate; }

    public String getNetwork() { return network; }

    public String getMsisdn() { return msisdn; }

    public String getImsi() { return imsi; }

}