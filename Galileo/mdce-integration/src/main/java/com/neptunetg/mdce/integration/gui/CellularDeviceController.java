/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.gui;

import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.cns.CnsException;
import com.neptunetg.common.cns.model.ConnectionEventDetail;
import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import com.neptunetg.mdce.common.internal.settings.service.MnoCallbackService;
import com.neptunetg.mdce.integration.cns.CellularNetworkServiceManager;
import com.neptunetg.mdce.integration.gui.common.EnvProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Display information about network usage
 */
@Controller
@RequestMapping(value="/service")
public class CellularDeviceController implements MnoCallbackService
{
    @Autowired
    private CellularNetworkServiceManager cellularNetworkServiceManager;

    @Autowired
    private EnvProperties envProperties;

    /**
     * Register callback with a url provided
     * @throws RemoteException
     */
    @RequestMapping(value = URL_REGISTER_VERIZON_CALLBACK, method = RequestMethod.GET)
    @ResponseBody
    public String registerCallback(@PathVariable(value = MNO) String mno) throws RemoteException
    {
        final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(mno);
        boolean result = cellularNetworkService.registerCallback();
        return mno + " callback registration success =" + result;
    }

    /**
     * unRegister callback with a url provided
     * @throws RemoteException
     */
    @RequestMapping(value = URL_UNREGISTER_VERIZON_CALLBACK, method = RequestMethod.GET)
    @ResponseBody
    public String unregisterCallback(@PathVariable(value = MNO) String mno) throws RemoteException
    {
        final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(mno);
        boolean result = cellularNetworkService.unregisterCallback();
        return mno + " platform callback unregistration success = " + result;
    }

    /**
     * Attaches environment properties bean to model
     */
    @ModelAttribute("envProperties")
    public EnvProperties envProperties()
    {
        return envProperties;
    }
}
