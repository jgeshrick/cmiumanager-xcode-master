/*
*  ***************************************************************************
*
*     Neptune Technology Group
*     Copyright 2015 as unpublished work.
*     All rights reserved
*
*     The information contained herein is confidential
*     property of Neptune Technology Group. The use, copying, transfer
*     or disclosure of such information is prohibited except by express
*     written agreement with Neptune Technology Group.
*
* ***************************************************************************
*/

package com.neptunetg.mdce.integration.data.gateway;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.neptunetg.mdce.integration.data.site.SiteId;

/**
 * Details of an R900 gateway
 */
public class GatewayDetails
{
    private final GatewayId gatewayId;

    private final SiteId siteId;

    private String firstHeardTime;
    private String lastHeardTime;
    private String nextSyncTime;
    private String nextSyncTimeWarning;
    private String nextSyncTimeError;
    private boolean gateway_active;

    public GatewayDetails(GatewayId gatewayId,
                          SiteId siteId,
                          String firstHeardTime,
                          String lastHeardTime,
                          String nextSyncTime,
                          String nextSyncTimeWarning,
                          String nextSyncTimeError,
                          boolean gateway_active)
    {
        this.gatewayId = gatewayId;
        this.siteId = siteId;
        this.firstHeardTime = firstHeardTime;
        this.lastHeardTime = lastHeardTime;
        this.nextSyncTime = nextSyncTime;
        this.nextSyncTimeWarning = nextSyncTimeWarning;
        this.nextSyncTimeError = nextSyncTimeError;
        this.gateway_active = gateway_active;
    }

    @JsonSerialize(converter=GatewayId.ToStringConverter.class)
    public GatewayId getGatewayId()
    {
        return gatewayId;
    }

    @JsonSerialize(converter=SiteId.ToIntConverter.class)
    public SiteId getSiteId()
    {
        return siteId;
    }

    public String getFirstHeardTime()
    {
        return firstHeardTime;
    }

    public void setFirstHeardTime(String firstHeardTime)
    {
        this.firstHeardTime = firstHeardTime;
    }

    public String getLastHeardTime()
    {
        return lastHeardTime;
    }

    public void setLastHeardTime(String lastHeardTime)
    {
        this.lastHeardTime = lastHeardTime;
    }

    public String getNextSyncTime()
    {
        return nextSyncTime;
    }

    public void setNextSyncTime(String nextSyncTime)
    {
        this.nextSyncTime = nextSyncTime;
    }

    public String getNextSyncTimeWarning()
    {
        return nextSyncTimeWarning;
    }

    public void setNextSyncTimeWarning(String nextSyncTimeWarning)
    {
        this.nextSyncTimeWarning = nextSyncTimeWarning;
    }

    public String getNextSyncTimeError()
    {
        return nextSyncTimeError;
    }

    public void setNextSyncTimeError(String nextSyncTimeError)
    {
        this.nextSyncTimeError = nextSyncTimeError;
    }

    public boolean isGateway_active()
    {
        return gateway_active;
    }

    public void setGateway_active(boolean gateway_active)
    {
        this.gateway_active = gateway_active;
    }
}
