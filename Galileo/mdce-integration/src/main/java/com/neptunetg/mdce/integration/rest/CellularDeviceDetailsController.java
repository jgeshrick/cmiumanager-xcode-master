/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.cns.CnsException;
import com.neptunetg.common.cns.DeviceIdentifierKind;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.mdce.integration.cns.CellularNetworkServiceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO: what API is this for?!?!?
 */
@RestController
public class CellularDeviceDetailsController
{
    @Autowired
    private CellularNetworkServiceManager cellularNetworkServiceManager;

    @RequestMapping(value = "/mdce/api/v1/miu/cellular-details", method = RequestMethod.GET)
    public DeviceCellularInformation getCmiuCellularDetailsFromDeviceIdentifier(
            @RequestParam("mno") String mno,
            @RequestParam("identifier") String deviceIdentifierKind,
            @RequestParam("value") String deviceIdentifierValue) throws CnsException
    {
        final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(mno);
        return cellularNetworkService.getDeviceInformationFromIdentifier(DeviceIdentifierKind.from(deviceIdentifierKind), deviceIdentifierValue);
    }
}
