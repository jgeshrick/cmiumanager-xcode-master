/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.data.alert;




import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLog;

import java.util.List;

/**
 * Created by WJD1 on 31/07/2015.
 * An interface which defines the functions the alert repository implementation should implement
 */
public interface AlertRepository
{
    List<AlertDetails> getNewAlertList(String alertSource);

    List<AlertDetails> getHandledAlertList( String alertSource);

    List<AlertDetails> getStaleAlertList(String alertSource);

    List<AlertDetails> getClearedAlertList(String alertSource);

    boolean addAlert(AlertDetails alertDetails);

    AlertDetails getAlert(long alertId);

    boolean setAlertStateToNew(long alertId);

    boolean setAlertStateToHandled(long alertId);

    boolean setAlertStateToStale(long alertId);

    boolean setAlertStateToCleared(long alertId);

    boolean setAlertLevelToWarning(long alertId);

    boolean setAlertLevelToError(long alertId);

    boolean setAlertTicketId(AlertDetails alertDetails);

    boolean addAlertLogMessage(AlertDetails alertDetails);

    boolean removeAlertsLogs(long alertId);

    boolean removeAlert(long alertId);

    List<AlertLog> getAlertLog(long alertId);

    /**
     * Get alert matching exactly the alert source
     * Cleared alerts are not included as there could be several
     * @param alertSource alert source path
     * @return alert details for the latest alert
     */
    AlertDetails getAlertNotCleared(String alertSource);

    /**
     * Get the last insert id.
     * @return the last insert id.
     */
    Long lastInsertId();

    /**
     * Get alerts in which the number of logs is higher than the log "window" size.
     * @return a list of matching alerts.
     */
    List<AlertDetails> getAlertsExceedingLogWindowSize();

    /**
     * Trims the logs in an alert to the window size by discarding the oldest logs.
     * @param alertId The alertId of the alert to trim.
     * @return The number of logs removed by the operation.
     */
    int trimAlertLogs(long alertId);
}
