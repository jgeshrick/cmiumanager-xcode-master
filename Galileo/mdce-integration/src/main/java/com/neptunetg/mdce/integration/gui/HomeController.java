/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.gui;

import com.neptunetg.mdce.integration.domain.ApplicationVersion;
import com.neptunetg.mdce.integration.gui.common.EnvProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.Instant;
import java.util.Date;

/**
 * Just to handle root http request url
 */
@Controller
public class HomeController
{
    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private ApplicationVersion applicationVersion;

    @Autowired
    private EnvProperties envProperties;

    @Value("#{envProperties['server.name']}")
    private String serverName;

    @Value("#{envProperties['gateway.data.dump.folder']}")
    String gatewayDataDumpFolder;

    @Value("#{envProperties['mdce.scheduler.localMode']}")
    Boolean localSqs;

    /**
     * Landing page for mdce-integration
     *
     */
    @RequestMapping(value="/pages/debug", method={RequestMethod.GET, RequestMethod.HEAD})
    public String getRoot(Model model)
    {
        logger.debug("loading home page");

        model.addAttribute("serverName", serverName);
        model.addAttribute("gatewayDataDumpFolder", gatewayDataDumpFolder);
        model.addAttribute("currentServerTime", Date.from(Instant.now()));
        model.addAttribute("applicationVersion", applicationVersion.getApplicationVersion());
        model.addAttribute("localSqs", localSqs);

        return "index";
    }


    /**
     * Attaches environment properties bean to model
     */
    @ModelAttribute("envProperties")
    public EnvProperties envProperties() {

        return envProperties;

    }

}
