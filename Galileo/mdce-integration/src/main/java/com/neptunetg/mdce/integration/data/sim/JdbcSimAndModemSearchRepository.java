/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.sim;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.mdce.integration.utility.SqlBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.neptunetg.common.data.util.JdbcDateUtil.getInstant;

/**
 * MIU search repository for MySql database
 */
@Repository
public class JdbcSimAndModemSearchRepository implements SimSearchRepository
{
    private static final String ICCID = "iccid";
    private static final String NETWORK = "network";
    private static final String LIFE_CYCLE_STATE = "lifeCycleState";

    // String Constants
    public static final String SIMID_CONST = "sim_details_id";
    public static final String MIUID_CONST = "miu_id";
    public static final String ICCID_CONST = "iccid";
    public static final String IMEI_CONST = "imei";
    private static final String CREATION_DATE_CONST = "creation_date";
    public static final String NETWORK_CONST = "sim_cellular_network";
    public static final String LIFE_CYCLE_STATE_CONST = "state";
    public static final String MSISDN_CONST = "msisdn";
    public static final String IMSI_CONST = "imsi";

    private static final String SELECT_FROM_MDCE_SIM_DETAILS = "SELECT sim_details.sim_details_id, miu_id, iccid, msisdn, imsi, sim_cellular_network, state, creation_date FROM mdce.sim_details LEFT JOIN " +
            "(SELECT MIN(timestamp) AS creation_date, sim_details_id FROM mdce.sim_lifecycle_state GROUP BY sim_details_id) " +
            "AS stamp_table ON stamp_table.sim_details_id = mdce.sim_details.sim_details_id ";

    private static final String SELECT_DISTINCT_SIM_CELLULAR_NETWORKS =
            "SELECT DISTINCT sim_cellular_network FROM sim_details ORDER BY sim_cellular_network ASC";

    private static final String SELECT_COUNT_FROM_MDCE_SIM_DETAILS = "SELECT COUNT(*) FROM mdce.sim_details";

    private static final String ORDER_BY_ICCID = " ORDER BY iccid ASC";

    private static final int maxResults = 100;
    private static final String LIMIT = " LIMIT ?," + maxResults;

    private static final String DB_NULL = "-";

    // fields
    private final JdbcTemplate db;

    @Autowired
    public JdbcSimAndModemSearchRepository(JdbcTemplate db)
    {
        this.db = db;
    }

    @Override
    public List<SimAndModemDetails> getCanByMiuid(MiuId miu_id)
    {

        String sql = "SELECT sd.miu_id, iccid, imei FROM mdce.sim_details sd " +
                "LEFT JOIN mdce.cellular_device_details cdc ON cdc.miu_id=sd.miu_id WHERE sd.miu_id=?";

        return this.db.query(sql, getSimAndModemDetails(), miu_id.numericValue());
    }

    @Override
    public List<SimAndModemDetails> getCanByIccid(String iccid)
    {

        String sql = "SELECT sd.miu_id, iccid, imei FROM mdce.sim_details sd " +
                "LEFT JOIN mdce.cellular_device_details cdc ON cdc.miu_id=sd.miu_id WHERE sd.iccid=?";

        return this.db.query(sql, getSimAndModemDetails(), iccid);
    }

    @Override
    public List<SimAndModemDetails> getCanByImei(String imei)
    {

        String sql = "SELECT sd.miu_id, iccid, imei FROM mdce.cellular_device_details cdc " +
                "LEFT JOIN mdce.sim_details sd ON cdc.miu_id=sd.miu_id WHERE cdc.imei=?";

        return this.db.query(sql, getSimAndModemDetails(), imei);
    }

    @Override
    public List<SimDetails> getSims(Map<String, String> options)
    {

        Map<String,String> criteria = new HashMap<>();

        criteria.put(ICCID, ICCID_CONST);
        criteria.put(NETWORK, NETWORK_CONST);
        criteria.put(LIFE_CYCLE_STATE, LIFE_CYCLE_STATE_CONST);

        final int page = Integer.parseInt(getOptionOrDefault(options, "page", "0"));

        SqlBuilder sql = new SqlBuilder(SELECT_FROM_MDCE_SIM_DETAILS);

        sql.addCriteriaIfExists(SqlBuilder.decodeNullValues(options), criteria);

        sql.append(ORDER_BY_ICCID);

        sql.append(LIMIT, page * maxResults);

        return this.db.query(sql.getSQL(), sql.getArguments(), getSimDetailsRowMapper());
    }


    @Override
    public int getSimCount(Map<String, String> options)
    {
        Map<String,String> criteria = new HashMap<>();

        criteria.put(ICCID, ICCID_CONST);
        criteria.put(NETWORK, NETWORK_CONST);
        criteria.put(LIFE_CYCLE_STATE, LIFE_CYCLE_STATE_CONST);

        SqlBuilder sql = new SqlBuilder(SELECT_COUNT_FROM_MDCE_SIM_DETAILS);

        sql.addCriteriaIfExists(SqlBuilder.decodeNullValues(options), criteria);

        return this.db.queryForObject(sql.getSQL(), sql.getArguments(), Integer.class);

    }


    @Override
    public List<String> getSimIccids(Map<String,String> options)
    {
        SqlBuilder sql = new SqlBuilder("SELECT DISTINCT iccid FROM mdce.sim_details");

        Map<String,String> criteria = new HashMap<>();

        criteria.put(NETWORK, NETWORK_CONST);
        criteria.put(LIFE_CYCLE_STATE, LIFE_CYCLE_STATE_CONST);

        sql.addCriteriaIfExists(SqlBuilder.decodeNullValues(options), criteria);

        sql.append(" ORDER BY iccid ASC");

        return this.db.query(sql.getSQL(), (ResultSet rs, int rowNumber) -> {
            return rs.getString("iccid");
        },
                (Object[]) sql.getArguments());
    }

    @Override
    public List<String> getSimMnos()
    {
        // Get all distinct networks
        List<String> networks = this.db.query(
                SELECT_DISTINCT_SIM_CELLULAR_NETWORKS,
                (ResultSet rs, int rowNumber) -> rs.getString("sim_cellular_network"));

        // Remove null/empty
        networks = networks
                .stream()
                .filter(n -> StringUtils.hasText(n))
                .collect(Collectors.toList());

        return networks;
    }

    private RowMapper<SimDetails> getSimDetailsRowMapper()
    {
        return (rs, rownum) -> {

            SimDetails details = new SimDetails(
                    rs.getLong(SIMID_CONST),
                    rs.getString(ICCID_CONST),
                    MiuId.valueOf(getIntegerIfNotNull(rs, MIUID_CONST)),
                    rs.getString(LIFE_CYCLE_STATE_CONST),
                    getInstant(rs, CREATION_DATE_CONST),
                    rs.getString(NETWORK_CONST),
                    rs.getString(MSISDN_CONST),
                    rs.getString(IMSI_CONST));

            return details;

        };

    }

    private RowMapper<SimAndModemDetails> getSimAndModemDetails()
    {
        return (rs, rownum) -> {

            SimAndModemDetails details = new SimAndModemDetails(
                    MiuId.valueOf(getIntegerIfNotNull(rs, MIUID_CONST)),
                    rs.getString(ICCID_CONST),
                    rs.getString(IMEI_CONST));
            return details;
        };
    }


    private String getOptionOrDefault(Map<String, String> options, String option, String defaultValue)
    {
        final String optionValue = getOption(options, option);
        if( StringUtils.hasText(optionValue) )
        {
            return optionValue;
        }
        return defaultValue;
    }

    private String getOption(Map<String, String> options, String option)
    {
        return options.get(option);
    }

    private static Integer getIntegerIfNotNull(ResultSet rs, String column) throws SQLException
    {
        int value = rs.getInt(column);

        if (!rs.wasNull())
        {
            return Integer.valueOf(value);
        }
        else
        {
            return null;
        }
    }

}
