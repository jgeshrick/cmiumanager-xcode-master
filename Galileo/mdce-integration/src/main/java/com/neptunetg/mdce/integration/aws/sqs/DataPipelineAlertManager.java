/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.aws.sqs;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Monitor incoming data pipeline alerts (from SQS) and transfer into mdce alert management system.
 */
@Service
public class DataPipelineAlertManager implements AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(CloudWatchMessageManager.class);

    private final MdceIpcSubscribeService awsQueueService;

    private final String topic = MdceIpcSubscribeService.DATA_PIPELINE;

    private final AlertService alertService;

    @Autowired
    public DataPipelineAlertManager(MdceIpcSubscribeService awsQueueService, AlertService alertService)
    {
        this.awsQueueService = awsQueueService;
        this.alertService = alertService;

        //create an observer to listen to AWS SQS for <env>-cloudwatch message send from cloudwatch alarm through SNS
        awsQueueService.register(this, topic, Duration.ofSeconds(1L), 10);
    }

    /**
     * Handler when SQS message to the registered SQS queue name has been received.
     * @param message the SQS message JSON body that is sent from SNS. Should contains Subject and Message field
     * @param messageId the message id
     * @param messageReceipt message receipt
     * @param cancellationToken Ignored as this is a short-running callback
     * @return true to delete the message from SQS
     */
    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        try
        {
            log.debug("Adding Alert To Database. Message: {}", message);

            ObjectMapper mapper = new ObjectMapper();
            JsonNode snsMessage = mapper.readValue(message, JsonNode.class);

            raiseDataPipelineAlertMessage(snsMessage.get("Message").textValue());

            return true;    //return true for SQS message to be removed
        }
        catch (Exception e)
        {
            log.error("Error processing received message", e);
        }

        return false;   //SQS message will not be removed from queue
    }

    /**
     *
     * @param message alarm message.
     * @throws IOException
     * @throws ParseException
     * @throws InternalApiException
     */
    private void raiseDataPipelineAlertMessage(String message) throws IOException, ParseException, InternalApiException
    {
        final AlertSource alertSource = AlertSource.AWS_DYNAMODB_BACKUP;
        final AlertDetails alertDetails = new AlertDetails();
        alertDetails.setAlertLevel(AlertLevel.ERROR);
        alertDetails.setAlertSource(alertSource.getAlertId());
        alertDetails.setAlertState(AlertState.NEW);
        alertDetails.setRecentMessage(message);
        alertDetails.setAlertCreationDate(ZonedDateTime.now());
        alertService.addAlertAndSendEmail(alertDetails);
    }


}
