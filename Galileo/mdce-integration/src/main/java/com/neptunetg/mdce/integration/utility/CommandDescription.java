/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.utility;

import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.mdce.common.internal.InternalApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommandDescription implements CommandDescriptionService {

    @Autowired
    private L900CommandDescription l900CommandDescription;

    @Autowired
    private CmiuCommandDescription cmiuCommandDescription;

    private static final Logger log = LoggerFactory.getLogger(CommandDescription.class);

    /**
     * Inspect command packet and generate a description of the command for human consumption.
     *
     * @param command command packet
     * @return A summary of the command for display purpose.
     */
    public String getCommandDetails(MiuCommand command) {

        try
        {
            return getService(command).getCommandDetails(command);
        }
        catch(InternalApiException e)
        {
            return "Error getting command details: " + e.getMessage();
        }

    }


    private CommandDescriptionService getService(MiuCommand command) throws InternalApiException
    {


        // Inspect packet type and call correct service
        if( MiuType.L900.equals(command.getTargetDeviceType()) )
        {
            return l900CommandDescription;

        } else if( MiuType.CMIU.equals(command.getTargetDeviceType()) )
        {
            return cmiuCommandDescription;

        } else
        {
            throw new InternalApiException("Unrecognized MIU type");
        }

    }


}
