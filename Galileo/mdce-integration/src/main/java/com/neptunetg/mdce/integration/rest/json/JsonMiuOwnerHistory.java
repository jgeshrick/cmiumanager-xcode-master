/*
 *  Neptune Technology Group
 *  Copyright 2017 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

/**
 * Represents a change to an MIU site ID.
 */
public class JsonMiuOwnerHistory
{
    @JsonProperty("sequence_id")
    private final int sequenceId;

    @JsonProperty("miu_id")
    private final int miuId;

    @JsonProperty("site_id")
    private final int siteId;

    @JsonProperty("timestamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private final Instant timestamp;

    public JsonMiuOwnerHistory(int sequenceId, int miuId, int siteId, Instant timestamp)
    {
        this.sequenceId = sequenceId;
        this.miuId = miuId;
        this.siteId = siteId;
        this.timestamp = timestamp;
    }

    public int getSequenceId()
    {
        return sequenceId;
    }

    public int getMiuId()
    {
        return miuId;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public Instant getTimestamp()
    {
        return timestamp;
    }
}
