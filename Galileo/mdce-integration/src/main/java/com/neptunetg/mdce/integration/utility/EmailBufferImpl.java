/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.utility;

import com.neptunetg.mdce.common.internal.email.model.EmailMessage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * When used in an integration test setting, allows a tester to view the most recently sent email message.
 */
@Component
@Scope("singleton")
public class EmailBufferImpl implements EmailBuffer
{
    private EmailMessage emailMessage;

    public EmailBufferImpl()
    {
        emailMessage = new EmailMessage();
    }

    /**
     * Gets the email message currently stored in the buffer.
     * @return The buffered email message.
     */
    @Override
    public EmailMessage getEmailMessage()
    {
        return emailMessage;
    }

    /**
     * Sets the currently buffered email message.
     */
    @Override
    public void setEmailMessage(EmailMessage emailMessage)
    {
        this.emailMessage = emailMessage;
    }
}
