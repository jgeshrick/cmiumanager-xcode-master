/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.integration.data.refdata;


import com.neptunetg.mdce.common.internal.site.model.MdceReferenceData;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataDistributer;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataUtility;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.rest.json.refdata.JsonReferenceData;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface RefDataRepository
{

    int insertRefData(JsonReferenceData referenceData, RefDataWriteMode writeMode);

    MdceReferenceData getRefData();

    List<String> getRegionalManagers();

    List<String> getStates();

    List<String> getMiuTypes();

    List<SiteDetails> getSitesForRegionalManager(String regionalManager);

    /**
     *
     * @param state the name of the state
     * @return a list of all the details for all the sites within that state
     */
    List<SiteDetails> getSitesForState(String state);

    List<String> getSimNetworks(Map<String,String> options);

    List<String> getSimLifeCycleStates(Map<String, String> options);

    Optional<String> lookupDistributerIdFromSiteId(SiteId siteId);

    MdceReferenceDataUtility getSiteReferenceData(SiteId siteId);

    MdceReferenceDataDistributer getDistributerReferenceData(String distributerId);
}