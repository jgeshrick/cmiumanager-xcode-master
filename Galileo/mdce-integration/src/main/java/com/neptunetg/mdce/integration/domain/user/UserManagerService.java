/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain.user;

import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.common.internal.site.model.SiteForUser;
import com.neptunetg.mdce.common.internal.user.model.LoginResult;
import com.neptunetg.mdce.common.internal.user.model.ModifiedUserDetails;
import com.neptunetg.mdce.common.internal.user.model.NewUserDetails;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequest;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequestClaim;
import com.neptunetg.mdce.common.internal.user.model.UserAlertSettings;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;

import java.util.List;
import java.util.Map;

/**
 * Service for managing users
 */
public interface UserManagerService
{
    /**
     * add a new user
     * @param newUserDetails a object containing the new users details
     * @return
     */
    boolean addUser(NewUserDetails newUserDetails);

    /**
     * Remove a user
     * @param modifiedUserDetails    form contain the user id to remove
     * @return
     */
    boolean removeUser(ModifiedUserDetails modifiedUserDetails);

    /**
     * Set a new password for a user
     * @param modifiedUserDetails    form containing the user id and new password
     * @return
     */
    boolean setUserPassword(ModifiedUserDetails modifiedUserDetails);

    boolean setUserLevel(ModifiedUserDetails modifiedUserDetails);

    boolean setUserEmail(ModifiedUserDetails modifiedUserDetails);

    boolean setUserName(ModifiedUserDetails modifiedUserDetails);

    boolean setUserDetails(String userName, ModifiedUserDetails modifiedUserDetails);

    /**
     * Used to verify a users login details
     * @param userName username in plain text
     * @param password the entered password in plain text
     * @return a value indicating whether the user can be logged in with the supplied credentials.
     */
    LoginResult verifyUser(String userName, String password);

    /**
     * Get a list of users
     * @return a list of user details objects
     */
    List<UserDetails> getUserList();

    /**
     * Get a users details by the users name
     * @param userName the users username
     * @return the users details
     */
    UserDetails getUser(String userName);

    /**
     * Get sites that the user has access to
     * @param userId the users ID
     * @return a list of site details objects
     */
    List<SiteDetails> getSitesForUser(long userId);

    /**
     * Get all the sites a user doesn't have access to
     * @param userId the users ID
     * @return a list of site details objects
     */
    List<SiteDetails> getSitesNotForUser(long userId);

    /**
     * Give a user access to a site
     * @param siteForUser user and site information
     * @return true if successful
     */
    boolean addSiteToUser(SiteForUser siteForUser);

    /**
     * Remove a users access to a site
     * @param siteForUser user and site information
     * @return true is successful
     */
    boolean removeSiteFromUser(SiteForUser siteForUser);

    /**
     * Get the alert settings for a user
     * @param userId the users ID
     * @return a user alert settings object
     */
    UserAlertSettings getUserAlertSettings(long userId);

    /**
     * Set a users alert settings
     * @param userAlertSettings a user alert settings object
     * @return true if successful
     */
    boolean setUserAlertSettings(UserAlertSettings userAlertSettings);

    /**
     * Adds a password reset request.
     * @param passwordResetRequest The request to add.
     * @return true if the operation succeeds, otherwise false.
     */
    boolean addPasswordResetRequest(PasswordResetRequest passwordResetRequest);

    /**
     * Modifies an existing password reset request.
     * @param token The token of the password reset request.
     * @param passwordResetRequestClaim The request to modify.
     * @return true if the operation succeeds, otherwise false.
     */
    boolean claimPasswordResetRequest(String token, PasswordResetRequestClaim passwordResetRequestClaim);

    /**
     * Expires any active password reset requests for the specified user.
     * @param userId the target user ID.
     * @return The number of affected rows.
     */
    int expirePasswordResetRequestsByUser(long userId);

    /**
     * Gets the password reset request with the specified token.
     * @param token the token to find.
     * @return the PasswordResetRequest with the matching token, or null if not found.
     */
    PasswordResetRequest getPasswordResetRequestByToken(String token);

    /**
     * Find users by the supplied criteria.
     * @param options search parameters
     * @return UserDetails for any users matching the search criteria.
     */
    List<UserDetails> findUsers(Map<String, Object> options);


    /**
     * Find user by email.
     * @param email user email
     * @return UserDetails to find any entry matching email
     */
    List<UserDetails> getUserDetailsByEmail(String email);
}
