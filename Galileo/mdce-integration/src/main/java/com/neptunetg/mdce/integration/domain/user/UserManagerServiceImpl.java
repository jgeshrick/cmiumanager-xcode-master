/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain.user;

import com.neptunetg.mdce.common.internal.settings.service.MdceSettingsService;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.common.internal.site.model.SiteForUser;
import com.neptunetg.mdce.common.internal.user.model.LoginResult;
import com.neptunetg.mdce.common.internal.user.model.ModifiedUserDetails;
import com.neptunetg.mdce.common.internal.user.model.NewUserDetails;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequest;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequestClaim;
import com.neptunetg.mdce.common.internal.user.model.UserAlertSettings;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import com.neptunetg.mdce.integration.audit.AuditService;
import com.neptunetg.mdce.integration.data.settings.MdceSettingsRepository;
import com.neptunetg.mdce.integration.data.user.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Class to provide the user manager service, allowing users to be created and deleted,
 * as well as allowing user details to be verified.
 */
@Service
public class UserManagerServiceImpl implements UserManagerService
{
    public static final int SALT_BYTE_SIZE = 32;
    public static final int HASH_BYTE_SIZE = 32;
    public static final int PBKDF2_ITERATIONS = 1000;
    private static final Logger logger = LoggerFactory.getLogger(UserManagerServiceImpl.class);
    private final UserRepository userRepo;

    private MdceSettingsRepository settingsRepository;
    private final AuditService auditService;


    @Autowired
    public UserManagerServiceImpl(UserRepository userRepo, MdceSettingsRepository settingsRepository, AuditService auditService)
    {
        this.userRepo = userRepo;
        this.settingsRepository = settingsRepository;
        this.auditService = auditService;
    }

    /**
     * Private method to generate a random byte array
     * @param saltLength The number of random bytes to generate
     * @return The random byte array
     */
    private static byte[] generateSalt(int saltLength)
    {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[saltLength];
        random.nextBytes(salt);

        return salt;
    }

    /**
     * Generate PDKDF2 (Password-Based Key Derivation Function 2)
     * Uses the PBKDF2 hashing algorithm which accepts an 'iterations' parameter to control
     * the computational expense of running it. In the future the number of iterations can be
     * increased inline with general increases in computing power so that the passwords don't
     * become easier to crack with brute force over time.
     * <p>
     * This function appends the salt to the password, and then passes it through the
     * PBKDF2 hashing algorithm.
     *
     * @param password   The users plain text password (as a char array)
     * @param salt       A random byte array
     * @param iterations The number of iterations the PBKDF2 algorithm should use
     * @param bytes      The number of bytes to generate for the hash
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    private static byte[] generatePbkdf2(char[] password, byte[] salt, int iterations, int bytes)
            throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        return skf.generateSecret(spec).getEncoded();
    }

    /**
     * Function to add a new user
     * @param userDetails the details for the new user
     * @return returns false if there is a failure to add the user ( i.e. the user already exists)
     */
    @Override
    public boolean addUser(NewUserDetails userDetails)
    {
        if (null == userRepo.getUserDetailsByUserName(userDetails.getUserName()))
        {
            byte[] salt = generateSalt(SALT_BYTE_SIZE);
            try
            {
                byte[] hash = generatePbkdf2(userDetails.getPassword().toCharArray(),
                        salt,
                        PBKDF2_ITERATIONS,
                        HASH_BYTE_SIZE);

                final boolean ret = userRepo.addUser(userDetails.getUserName(), userDetails.getEmail(), userDetails.getUserLevel(), hash, salt, PBKDF2_ITERATIONS);

                logger.debug("New user added: name: {}, level: {}", userDetails.getUserName(), userDetails.getUserLevel());
                auditService.logUserAdd(userDetails);
                return ret;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Error in adding user", e); //TODO better exception class
            }
        }

        return false;
    }

    @Override
    public boolean removeUser(ModifiedUserDetails modifiedUserDetails)
    {
        final UserDetails deletedUserDetails = userRepo.getUserDetailsById(modifiedUserDetails.getUserId());
        final boolean ret = userRepo.removeUser(modifiedUserDetails.getUserId());

        final String operationName = "User removed";

        auditService.logUserRemoved(modifiedUserDetails, deletedUserDetails, operationName);

        return ret;
    }

    @Override
    public boolean setUserPassword(ModifiedUserDetails modifiedUserDetails)
    {
        byte[] salt = generateSalt(SALT_BYTE_SIZE);
        try
        {
            byte[] hash = generatePbkdf2(modifiedUserDetails.getPassword().toCharArray(),
                    salt,
                    PBKDF2_ITERATIONS,
                    HASH_BYTE_SIZE);

            boolean ret = userRepo.setUserPassword(modifiedUserDetails.getUserId(), hash, salt, PBKDF2_ITERATIONS);

            final String operationName = "User password changed.";

            auditService.logUserAccountChange(modifiedUserDetails, operationName);

            return ret;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Error setting user password", e);
        }
    }

    @Override
    public boolean setUserLevel(ModifiedUserDetails modifiedUserDetails)
    {
        final boolean ret = userRepo.setUserLevel(modifiedUserDetails.getUserId(), modifiedUserDetails.getUserRole());
        final String operationName = "User level changed";
        auditService.logUserAccountChange(modifiedUserDetails, operationName + ": " + modifiedUserDetails.getUserRole());
        return ret;
    }

    @Override
    public boolean setUserEmail(ModifiedUserDetails modifiedUserDetails)
    {
        final boolean ret = userRepo.setUserEmail(modifiedUserDetails.getUserId(), modifiedUserDetails.getEmail());

        final String operationName = "User email changed";
        auditService.logUserAccountChange(modifiedUserDetails, operationName + ": " + modifiedUserDetails.getEmail());

        return ret;
    }

    @Override
    public boolean setUserName(ModifiedUserDetails modifiedUserDetails)
    {
        final boolean ret = userRepo.setUserName(modifiedUserDetails.getUserId(), modifiedUserDetails.getUserName());

        final String operationName = "Username changed";
        auditService.logUserAccountChange(
                modifiedUserDetails,
                operationName + ": from '" + modifiedUserDetails.getPreviousUserName() +
                        "'; to: '" + modifiedUserDetails.getUserName() + "'");

        return ret;
    }

    @Override
    public boolean setUserDetails(String userName, ModifiedUserDetails modifiedUserDetails)
    {
        UserDetails existingUserDetails = userRepo.getUserDetailsById(modifiedUserDetails.getUserId());
        boolean ret = true;
        if (StringUtils.hasText(modifiedUserDetails.getUserName()))
        {
            ret &= userRepo.setUserName(modifiedUserDetails.getUserId(), modifiedUserDetails.getUserName());
        }

        if (ret && modifiedUserDetails.getUserRole() != null)
        {
            ret &= userRepo.setUserLevel(modifiedUserDetails.getUserId(), modifiedUserDetails.getUserRole());
        }

        if (ret && StringUtils.hasText(modifiedUserDetails.getEmail()))
        {
            ret &= userRepo.setUserEmail(modifiedUserDetails.getUserId(), modifiedUserDetails.getEmail());
        }

        if (ret && modifiedUserDetails.getAccountLockoutEnabled() != null)
        {
            ret &= userRepo.setUserAccountLockoutEnabled(modifiedUserDetails.getUserId(), modifiedUserDetails.getAccountLockoutEnabled());
        }

        if (ret && existingUserDetails.isCurrentlyDisabled() && modifiedUserDetails.getCurrentlyDisabled() != null)
        {
            // Can only remove disabled status
            if (modifiedUserDetails.getCurrentlyDisabled() == false)
            {
                // Remove disabled status by changing disabledUntil to now.
                ret &= userRepo.setUserDisabledUntil(modifiedUserDetails.getUserId(), Instant.now());
            }
        }

        if (ret && modifiedUserDetails.getAllowConcurrentSessions() != null)
        {
            ret &= userRepo.setAllowConcurrentSessions(modifiedUserDetails.getUserId(), modifiedUserDetails.getAllowConcurrentSessions());
        }

        return ret;
    }

    @Override
    public LoginResult verifyUser(String userName, String password)
    {
        UserDetails details = userRepo.getUserDetailsByUserName(userName);

        // Check that the username is recognized and that the account is not disabled.
        LoginResult loginResult = isLoginRecognizedAndEnabled(userName, details);
        if(loginResult != LoginResult.OK)
        {
            return loginResult;
        }

        // If the password isn't correct, record the number of consecutive incorrect attempts. The count is reset
        // following a correct password. If account lockout is enabled for the user, the account is disabled when a
        // threshold is reached. The threshold and disabled duration are configured in the MDCE settings.
        boolean isUserVerified = isPasswordCorrect(password, details);

        if (!isUserVerified)
        {
            loginResult = LoginResult.LOGIN_FAILED;

            long failedAttempts = 1 + details.getFailedLoginAttempts();
            long maxLoginAttempts = settingsRepository.getIntSetting(
                    MdceSettingsService.MAX_LOGIN_ATTEMPTS_KEY,
                    MdceSettingsService.MAX_LOGIN_ATTEMPTS_DEFAULT_VALUE);

            if (failedAttempts >= maxLoginAttempts && details.isAccountLockoutEnabled())
            {
                loginResult = LoginResult.ACCOUNT_DISABLED;

                long lockedAccountTimeoutMinutes = settingsRepository.getIntSetting(
                        MdceSettingsService.LOCKED_ACCOUNT_TIMEOUT_MINUTES_KEY,
                        MdceSettingsService.LOCKED_ACCOUNT_TIMEOUT_MINUTES_DEFAULT_VALUE);

                Instant disabledUntil = Instant.now().plus(lockedAccountTimeoutMinutes, ChronoUnit.MINUTES);

                userRepo.setUserDisabledUntil(details.getUserId(), disabledUntil);
                auditService.logVerifyUserFailed(details.getUserName(), "disabledUntil = " + disabledUntil.toString());

                userRepo.setUserFailedLoginAttempts(details.getUserId(), 0);
                auditService.logVerifyUserFailed(details.getUserName(), "failedLoginAttempts = 0");

                logger.info("verifyUser: " +
                        "Disabling user after " + failedAttempts + " consecutive failed login attempt(s). " +
                        "Username \"" + details.getUserName() + "\", " +
                        "disabled until " + disabledUntil.toString() + ".");
            }
            else
            {
                userRepo.setUserFailedLoginAttempts(details.getUserId(), failedAttempts);

                auditService.logVerifyUserFailed(details.getUserName(), "failedLoginAttempts = " + failedAttempts);
                logger.info("verifyUser: Authentication was requested with an incorrect password. " +
                        "Username \"" + details.getUserName() + "\", " +
                        "consecutive failed login attempts: " + failedAttempts + ".");
            }
        }
        else if (details.getFailedLoginAttempts() != 0)
        {
            userRepo.setUserFailedLoginAttempts(details.getUserId(), 0);
        }

        return loginResult;
    }

    @Override
    public List<UserDetails> getUserList()
    {
        return userRepo.getUserList();
    }

    @Override
    public UserDetails getUser(String userName)
    {
        return userRepo.getUserDetailsByUserName(userName);
    }

    @Override
    public List<SiteDetails> getSitesForUser(long userId)
    {
        return userRepo.getSiteListForUser(userId);
    }

    @Override
    public List<SiteDetails> getSitesNotForUser(long userId)
    {
        return userRepo.getSiteListNotForUser(userId);
    }

    @Override
    public boolean addSiteToUser(SiteForUser siteForUser)
    {
        final boolean ret = userRepo.addSiteToUser(siteForUser.getUserId(), siteForUser.getSiteId());

        logger.debug("user site added. user id {}", siteForUser.getUserId());
        final String operationName = "Site added";

        auditService.logUserSiteChange(siteForUser, operationName + ": " + siteForUser.getSiteId());

        return ret;

    }

    @Override
    public boolean removeSiteFromUser(SiteForUser siteForUser)
    {
        final boolean ret = userRepo.removeSiteFromUser(siteForUser.getUserId(), siteForUser.getSiteId());
        logger.debug("user site removed. user id {}", siteForUser.getUserId());
        final String operationName = "Site removed";

        auditService.logUserSiteChange(siteForUser, operationName + ": " + siteForUser.getSiteId());

        return ret;
    }

    @Override
    public UserAlertSettings getUserAlertSettings(long userId)
    {
        return userRepo.getUserAlertSettings(userId);
    }

    @Override
    public boolean setUserAlertSettings(UserAlertSettings userAlertSettings)
    {
        return userRepo.setUserAlertSettings(userAlertSettings);
    }

    @Override
    public boolean addPasswordResetRequest(PasswordResetRequest passwordResetRequest)
    {
        return userRepo.addPasswordResetRequest(
                passwordResetRequest.getToken(),
                passwordResetRequest.getUserId(),
                passwordResetRequest.getMatchedUserName(),
                passwordResetRequest.getEnteredEmailAddress(),
                passwordResetRequest.getValidUntilTimestamp(),
                passwordResetRequest.getRequestorIpAddress());
    }

    @Override
    public boolean claimPasswordResetRequest(String token, PasswordResetRequestClaim passwordResetRequestClaim)
    {
        return userRepo.claimPasswordResetRequest(
                passwordResetRequestClaim.getPasswordResetRequestId(),
                token,
                passwordResetRequestClaim.getClaimantIpAddress());
    }

    @Override
    public int expirePasswordResetRequestsByUser(long userId)
    {
        return userRepo.expirePasswordResetRequestsByUser(userId);
    }

    @Override
    public PasswordResetRequest getPasswordResetRequestByToken(String token)
    {
        return userRepo.getPasswordResetRequestByToken(token);
    }

    @Override
    public List<UserDetails> findUsers(Map<String, Object> options)
    {
        return userRepo.findUsers(options);
    }

    /**
     * Find user by email.
     * @param email user email
     * @return UserDetails to find any entry matching email
     */
    @Override
    public List<UserDetails> getUserDetailsByEmail(String email)
    {
        return userRepo.getUserDetailsByUserEmail(email);
    }

    private LoginResult isLoginRecognizedAndEnabled(String userName, UserDetails details)
    {
        if (details == null)
        {
            logger.info("verifyUser: Authentication was requested for an unrecognized username: \"" + userName + "\".");

            return LoginResult.LOGIN_FAILED;
        }
        else if (details.isCurrentlyDisabled())
        {
            logger.info("verifyUser: Authentication was requested for a disabled account. " +
                    "Username: \"" + userName + "\".");

            return LoginResult.ACCOUNT_DISABLED;
        }

        return LoginResult.OK;
    }

    private boolean isPasswordCorrect(String suppliedPassword, UserDetails userDetails)
    {
        boolean isPasswordCorrect;

        try
        {
            byte[] testHash = generatePbkdf2(suppliedPassword.toCharArray(),
                    userDetails.getSalt(),
                    userDetails.getSaltIterations(),
                    HASH_BYTE_SIZE);

            isPasswordCorrect = Arrays.equals(testHash, userDetails.getHash());
        } catch (Exception e)
        {
            isPasswordCorrect = false;

            logger.debug("Exception while verifying password for user: " + userDetails.getUserName() + ".", e);
        }

        return isPasswordCorrect;
    }
}
