/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.data.audit;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.ConfigSet;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.mdce.common.internal.audit.model.AuditLog;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;
import com.neptunetg.mdce.common.internal.site.model.SiteForUser;
import com.neptunetg.mdce.common.internal.user.model.ModifiedUserDetails;
import com.neptunetg.mdce.common.internal.user.model.NewUserDetails;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import com.neptunetg.mdce.integration.data.site.SiteId;

import java.util.List;

/**
 * Handle audit logs for JDBC
 */
public interface AuditRepository
{
    void logMiuOwnershipChanged(MiuId miuId, SiteId oldSiteId, SiteId newSiteId, String requester);

    void logRefusedMiuOwnershipChanged(MiuId miuId, SiteId newSiteId, SiteId currentMiuOwner);

    void logMiuCommand(MiuCommand miuCommand);

    void logUserAccountChange(ModifiedUserDetails modifiedUserDetails, String operationDescription);

    void logUserRemoved(ModifiedUserDetails modifiedUserDetails, UserDetails deletedUserDetails, String operationDescription);

    void logUserSiteChange(SiteForUser siteForUser, String operationDescription);

    void logNewUser(NewUserDetails userDetails);

    List<AuditLog> getAuditLog(Long miuId, String userName, int daysAgo, Integer page);

    Integer getAuditLogCount(Long miuId, String userName, int daysAgo);

    void logSettingChanged(String key, String oldVal, String newVal, String requester);

    void logVerifyUserFailed(String userName, String description);

    void logAccessDenied(String userName, String description);
}
