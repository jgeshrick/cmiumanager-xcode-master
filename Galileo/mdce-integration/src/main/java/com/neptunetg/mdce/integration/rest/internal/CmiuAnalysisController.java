/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.anaylsis.service.CmiuAnalysisService;
import com.neptunetg.mdce.common.internal.anaylsis.model.CmiuTimingAnalysisDetails;
import com.neptunetg.mdce.integration.data.analysis.JdbcCmiuAnalysisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * CMIU Analysis Controller.
 */
@RestController
@RequestMapping("/service")
public class CmiuAnalysisController implements CmiuAnalysisService {

    @Autowired
    private JdbcCmiuAnalysisRepository cmiuAnalysisRepository;

    @Override
    @RequestMapping(value = URL_CMIU_TIMING_LIST, method = RequestMethod.GET)
    public List<CmiuTimingAnalysisDetails> getCmiuTimingInfo(@RequestParam(value = PARAM_CONNECTION_THRESHOLD, required = false) Long connectionThreshold,
                                                             @RequestParam(value = PARAM_CMIU_MODE, required = false) String cmiuMode,
                                                             @RequestParam(value = PARAM_CMIU_MNO, required = false) String mno,
                                                             @RequestParam(value = PARAM_CMIU_STATE, required = false) String state,
                                                             @RequestParam(value = PARAM_CMIU_SITE_ID, required = false) String siteId) throws InternalApiException {
        return this.cmiuAnalysisRepository.getTimingInfo(connectionThreshold, cmiuMode, mno, state, siteId);
    }
}
