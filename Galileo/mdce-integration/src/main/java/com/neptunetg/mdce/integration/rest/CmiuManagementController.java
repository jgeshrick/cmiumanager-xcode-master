/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.common.cns.model.UsageHistory;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.integration.cns.CellularNetworkServiceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

/**
 * Expose web service for managing CMIU cellular.
 */
@RestController
@RequestMapping(value = "/mdce/api/v1/miu/{miuId}/")
public class CmiuManagementController //TODO: this could be better named e.g. CmiuCnsRestController
{
    private static final String STATE_ACTIVATE_LOWERCASE = "activate";
    private static final String STATE_ACTIVATE_SUSPEND = "suspend";
    private static final String STATE_RESTORE_LOWERCASE = "restore";
    private static final String STATE_DEACTIVATE_LOWERCASE = "deactivate";

    @Autowired
    private CellularNetworkServiceManager cellularNetworkServiceManager;

    @Autowired
    private CmiuRepository cmiuRepository;

    /**
     * Trigger CNS provider to start calculating the usage data. The usage data will be available from callback, which will
     * be recorded to the RDS.
     *
     * TODO: This is not an API listed in ETI 48-02.  Either this needs to become an official API (which seems unlikely) or it should be an internal API.  As of Aug 8, 2015 this is only called from the integration test.
     *
     * @param miuId
     * @param startDate
     * @param endDate
     * @return the request id for debug purpose
     */
    @RequestMapping(value = "request-usage-data", method = RequestMethod.GET)
    public String requestUsageData(@PathVariable("miuId") MiuId miuId,
                                   @RequestParam("start-date") String startDate,
                                   @RequestParam("end-date") String endDate) throws InternalApiException
    {
        ZonedDateTime fromDate = (startDate == null)?
                ZonedDateTime.now():
                ZonedDateTime.parse(startDate, DateTimeFormatter.ISO_DATE_TIME);

        ZonedDateTime toDate = (startDate == null)?
                fromDate.plus(1, ChronoUnit.DAYS):
                ZonedDateTime.parse(endDate, DateTimeFormatter.ISO_DATE_TIME);

        try
        {
            final CellularDeviceDetail miuCellularDetails = cmiuRepository.getCellularConfig(miuId);
            final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(miuCellularDetails.getMno());

            final String requestId = cellularNetworkService.getBillingCycleUsageData(fromDate, toDate, miuCellularDetails.getIccid());

            return requestId;
        }
        catch (Exception e)
        {
            throw new InternalApiException("Can't get usage data for MIU " + miuId, e);
        }
    }

    /**
     * Change the cellular state of a CMIU.
     *
     * TODO: This is not an API listed in ETI 48-02.  Either this needs to become an official API (which seems unlikely) or it should be an internal API.  As of Aug 8, 2015 this is only called from the integration test.
     *
     * @param miuId miu id
     * @param newState new cellular state: suspend, restore, deactivate
     * @return request id
     */
    @RequestMapping(value = "change-state", method = RequestMethod.POST)
    public String changeCmiuProvisioningState(@PathVariable("miuId") MiuId miuId,
                                                       @RequestParam("new-state") String newState) throws InternalApiException
    {
        try
        {
            final CellularDeviceDetail miuCellularDetails = cmiuRepository.getCellularConfig(miuId);
            final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(miuCellularDetails.getMno());
            final String iccid = miuCellularDetails.getIccid();

            switch(newState.toLowerCase())
            {
                case STATE_ACTIVATE_LOWERCASE:
                    return cellularNetworkService.activateDevice(iccid, miuCellularDetails.getModemConfig().getImei());

                case STATE_ACTIVATE_SUSPEND:
                    return cellularNetworkService.suspendDevice(iccid);

                case STATE_RESTORE_LOWERCASE:
                    return cellularNetworkService.restoreDevice(iccid);

                case STATE_DEACTIVATE_LOWERCASE:
                    return cellularNetworkService.deactivateDevice(iccid);

                default:
                    throw new IllegalArgumentException("Desired provision state not recognised " + newState);
            }
        }
        catch (Exception e)
        {
            throw new InternalApiException("Can't change provisioning state for MIU " + miuId, e);
        }

    }

    /**
     * TODO: This is not an API listed in ETI 48-02.  Either this needs to become an official API (which seems unlikely) or it should be an internal API.  As of Aug 8, 2015 this is only called from the integration test.
     */
    @RequestMapping(value = "cellular-details", method = RequestMethod.GET)
    public DeviceCellularInformation getCmiuCellularDetails(@PathVariable("miuId") MiuId miuId) throws InternalApiException
    {
        try
        {
            final CellularDeviceDetail miuCellularDetails = cmiuRepository.getCellularConfig(miuId);
            final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(miuCellularDetails.getMno());
            final String iccid = miuCellularDetails.getIccid();
            return cellularNetworkService.getDeviceInformation(iccid);
        }
        catch (Exception e)
        {
            throw new InternalApiException("Can't change provisioning state for MIU " + miuId, e);
        }
    }

    /**
     * TODO: This is not an API listed in ETI 48-02.  Either this needs to become an official API (which seems unlikely) or it should be an internal API.  As of Aug 8, 2015 this is only called from the integration test.
     */

    @RequestMapping(value = "usage", method = RequestMethod.GET)
    public List<UsageHistory> getBillingData(@PathVariable("miuId") MiuId miuId,
                                 @RequestParam("start-date") String startDate,
                                 @RequestParam("end-date") String endDate) throws InternalApiException
    {
        try
        {
            final CellularDeviceDetail miuCellularDetails = cmiuRepository.getCellularConfig(miuId);
            final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(miuCellularDetails.getMno());
            final String iccid = miuCellularDetails.getIccid();
            if (iccid == null || iccid.isEmpty())
            {
                return Collections.emptyList();
            }

            DateTimeFormatter dtf = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
            ZonedDateTime fromLocalDate = ZonedDateTime.parse(startDate, dtf);
            ZonedDateTime toLocalDate = ZonedDateTime.parse(endDate, dtf);

            return cellularNetworkService.getDeviceUsageHistory(iccid,
                    fromLocalDate,
                    toLocalDate);
        }
        catch (Exception e)
        {
            throw new InternalApiException("Can't change provisioning state for MIU " + miuId, e);
        }
    }

}
