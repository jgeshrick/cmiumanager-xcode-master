/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleStateTransistion;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.integration.domain.alert.AlertEmail;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import com.neptunetg.mdce.integration.scheduled.lifecycle.alert.service.CmiuLifecycleAlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by WJD1 on 24/05/2016.
 * A class to check CMIU lifecycle state and raise an alert if necessary
 */
@Service
public class CmiuLifecycleStateChecker implements AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(CmiuLifecycleStateChecker.class);

    private final CmiuLifecycleAlertService miuLifecycleAlertService;

    private final AlertService alertService;

    @Value("#{envProperties['alert.lifecycle.stuck.trigger']}")
    private boolean stuckAlertIsOn;

    @Autowired
    public CmiuLifecycleStateChecker(MdceIpcSubscribeService awsQueueService, CmiuLifecycleAlertService miuLifecycleAlertService, AlertService alertService)
    {
        this.miuLifecycleAlertService = miuLifecycleAlertService;
        this.alertService = alertService;
        awsQueueService.register(this, MdceIpcSubscribeService.CHECK_CMIU_LIFECYCLE_STATE, Duration.ofSeconds(15L), 1);
    }

    /**
     * Handler when SQS message to the registered SQS queue name has been received.
     * @param message the message body
     * @param messageId the message id
     * @param messageReceipt message receipt
     * @return true to delete the message from SQS
     */
    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        log.debug("SQS message received to check for CMIU lifecycle state issues");

        try
        {
            raiseAlertForCmiuStillUnclaimedAfterXDays();
            raiseAlertForCmiuStuckAfterXDays();
            String[] receivedValues = message.split(",");
            if( receivedValues.length == 2 )
            {
                // 0 = cmiu, 1 = new lifecycle state
                raiseAlertForUnexpectedLifecycleStateChange(MiuId.fromString(receivedValues[0]), receivedValues[1]);

                return true;
            }
            else
            {
                log.error("Failed to check for Unexpected Lifecycle State Changes:");
                return false;
            }
        }
        catch (InternalApiException e)
        {
            log.error("Error checking for, or raising CMIU lifecycle state alerts",e);
        }

        return true;
    }

    private void raiseAlertForCmiuStillUnclaimedAfterXDays() throws InternalApiException
    {
        final List<MiuLifecycleStateTransistion> cmiusForAlert =
                miuLifecycleAlertService.getCmiusNotClaimedInGracePeriod();

        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

        final AlertEmail email = alertService.initAlertEmail();

        for(MiuLifecycleStateTransistion cmiuForAlert : cmiusForAlert)
        {
            final MiuLifecycleState currentState = cmiuForAlert.getCurrentMiuLifecycleState();
            final MiuLifecycleState previousState = cmiuForAlert.getPreviousMiuLifecycleState();
            final MiuId miuId = currentState.getMiuId();
            final AlertSource alertSource = AlertSource.cmiuLifecycleStateStuck(miuId.numericValue());

            AlertDetails alertDetails = new AlertDetails();
            alertDetails.setAlertLevel(AlertLevel.WARNING);
            alertDetails.setAlertState(AlertState.NEW);
            alertDetails.setAlertSource(alertSource.getAlertId());
            alertDetails.setRecentMessage("CMIU "
                    + miuId
                    + " still in state'"
                    + currentState.getLifecycleState()
                    + "' since "
                    + dateTimeFormatter.withZone(ZoneId.of("US/Central")).format(
                        currentState.getTransitionInstant())
                    + " and it was set to state '"
                    + previousState.getLifecycleState()
                    + "' at "
                    + dateTimeFormatter.withZone(ZoneId.of("US/Central")).format(
                        previousState.getTransitionInstant()));
            alertDetails.setAlertCreationDate(ZonedDateTime.now());
            alertService.addAlert(alertDetails, email);
        }
        alertService.sendEmail(email);
    }

    private void raiseAlertForCmiuStuckAfterXDays() throws InternalApiException
    {
        if(stuckAlertIsOn)
        {
            final List<MiuLifecycleState> cmiusForAlert =
                    miuLifecycleAlertService.getCmiusStuckInLifecycleStateForAlerts();

            final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

            final AlertEmail email = alertService.initAlertEmail();

            for (MiuLifecycleState cmiuForAlert : cmiusForAlert)
            {
                final MiuId miuId = cmiuForAlert.getMiuId();
                final AlertSource alertSource = AlertSource.cmiuLifecycleStateStuck(miuId.numericValue());
                final AlertDetails alertDetails = new AlertDetails();
                alertDetails.setAlertLevel(AlertLevel.WARNING);
                alertDetails.setAlertState(AlertState.NEW);
                alertDetails.setAlertSource(alertSource.toString());
                alertDetails.setRecentMessage("CMIU "
                        + miuId
                        + " still in state '"
                        + cmiuForAlert.getLifecycleState()
                        + "' since "
                        + dateTimeFormatter.withZone(ZoneId.of("US/Central")).format(
                        cmiuForAlert.getTransitionInstant()));
                alertDetails.setAlertCreationDate(ZonedDateTime.now());
                alertService.addAlert(alertDetails, email);
            }
            alertService.sendEmail(email);
        }
    }

    private void raiseAlertForUnexpectedLifecycleStateChange(MiuId miuId, String newLifecycleState) throws InternalApiException
    {
        final MiuLifecycleStateEnum currentLifecycleState =
                miuLifecycleAlertService.getMiuCurrentLifecycleState(miuId).getLifecycleState();

        final AlertEmail email = alertService.initAlertEmail();

        final AlertSource alertSource = AlertSource.cmiuLifecycleUnexpectedTransition(miuId.numericWrapperValue());
        final AlertDetails alertDetails = new AlertDetails();
        alertDetails.setAlertLevel(AlertLevel.WARNING);
        alertDetails.setAlertState(AlertState.NEW);
        alertDetails.setAlertSource(alertSource.toString());
        alertDetails.setRecentMessage("CMIUID "
                + miuId.numericWrapperValue()
                + " encountered an unexpected lifecycle state transition from "
                + currentLifecycleState
                + " to "
                + newLifecycleState
                + ".");
        alertDetails.setAlertCreationDate(ZonedDateTime.now());
        alertService.addAlert(alertDetails, email);
        alertService.sendEmail(email);
    }
}
