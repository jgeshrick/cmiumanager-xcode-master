/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.rest.json.JsonMiuActiveFlag;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by WJD1 on 18/12/2015.
 * Class to assist in performing a batch insert for miu details to SQL
 * The batch insert is necessary for performance
 * Not thread safe
 */
public class InsertMiuDetailsBpss implements BatchPreparedStatementSetter
{
    private int batchSize = 0;
    private final long[] miuId;
    private final SiteId siteId;
    private final int[] typeId;

    InsertMiuDetailsBpss(SiteId siteId, int maxBatchSize)
    {
        this.siteId = siteId;
        this.miuId = new long[maxBatchSize];
        this.typeId = new int[maxBatchSize];
    }

    public void addMiu(JsonMiuActiveFlag miuJson)
    {
        miuId[batchSize] = miuJson.getMiuId();

        if (miuJson.getType() != null)
        {
            typeId[batchSize] = MiuType.valueOf(miuJson.getType().trim()).numericValue();
        }
        else
        {
            MiuId miuId = new MiuId(miuJson.getMiuId());
            if(miuId.isInCmiuIdRange())
            {
                typeId[batchSize] = MiuType.CMIU.numericValue();
            }
            else
            {
                typeId[batchSize] = MiuType.L900.numericValue();
            }
        }

        batchSize++;
    }

    public void clearBatch()
    {
        batchSize = 0;
    }

    @Override
    public int getBatchSize()
    {
        return batchSize;
    }

    @Override
    public void setValues(PreparedStatement preparedStatement, int i) throws SQLException
    {
        preparedStatement.setLong(1, miuId[i]);
        preparedStatement.setInt(2, siteId.numericValue());
        preparedStatement.setLong(3, typeId[i]);
    }

    public SiteId getSiteId()
    {
        return this.siteId;
    }

    public long[] getMiuIds()
    {
        return this.miuId;
    }
}
