/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.utility;

import com.neptunetg.mdce.common.internal.email.model.EmailMessage;

/**
 * An interface to provide local buffering of email messages.
 */
public interface EmailBuffer
{
    /**
     * Gets the email message currently stored in the buffer.
     * @return The buffered email message.
     */
    EmailMessage getEmailMessage();

    /**
     * Sets the currently buffered email message.
     */
    void setEmailMessage(EmailMessage emailMessage);
}
