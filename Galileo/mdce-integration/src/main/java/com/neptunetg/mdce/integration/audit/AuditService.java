/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.audit;

import com.neptunetg.common.data.audit.CmiuConfigAuditLogRepository;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.ConfigSet;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.mdce.common.internal.audit.model.AuditLog;
import com.neptunetg.mdce.common.internal.site.model.SiteForUser;
import com.neptunetg.mdce.common.internal.user.model.ModifiedUserDetails;
import com.neptunetg.mdce.common.internal.user.model.NewUserDetails;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import com.neptunetg.mdce.integration.data.audit.AuditRepository;
import com.neptunetg.mdce.integration.data.refdata.RefDataRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Exposes services related to auditing to other beans related.
 */
@Service
public class AuditService
{
    private final AuditRepository auditRepository;

    private final CmiuConfigAuditLogRepository cmiuConfigAuditRepository;

    private final RefDataRepository refDataRepository;

    @Autowired
    public AuditService(AuditRepository auditRepository, CmiuConfigAuditLogRepository cmiuConfigAuditRepository, RefDataRepository refDataRepository)
    {
        this.auditRepository = auditRepository;
        this.cmiuConfigAuditRepository = cmiuConfigAuditRepository;
        this.refDataRepository = refDataRepository;
    }

    public void logMiuOwnershipChanged(MiuId miuId, SiteId oldSiteId, SiteId newSiteId, String requester)
    {
        auditRepository.logMiuOwnershipChanged(miuId, oldSiteId, newSiteId, requester);
    }

    public void logRefusedMiuOwnershipUpdate(MiuId miuId, SiteId siteId, SiteId currentMiuOwner)
    {
        auditRepository.logRefusedMiuOwnershipChanged(miuId, siteId, currentMiuOwner);
    }

    public void logMiuChangeConfigSet(MiuId miuId, ConfigSet oldConfigSet, ConfigSet newConfigSet, String requester)
    {
        cmiuConfigAuditRepository.logMiuChangeConfigSet(miuId, oldConfigSet, newConfigSet, requester);
    }

    public void logConfigSetCreated(ConfigSet configSet, String requester)
    {
        cmiuConfigAuditRepository.logConfigSetCreated(configSet, requester);
    }

    public void logConfigSetChanged(ConfigSet oldConfigSet, ConfigSet newConfigSet, String requester)
    {
        cmiuConfigAuditRepository.logConfigSetChange(oldConfigSet, newConfigSet, requester);
    }

    public void logSendMiuCommand(MiuCommand miuCommand)
    {
        auditRepository.logMiuCommand(miuCommand);
    }

    public void logUserAccountChange(ModifiedUserDetails modifiedUserDetails, String operationDescription)
    {
        auditRepository.logUserAccountChange(modifiedUserDetails, operationDescription);
    }

    public void logUserRemoved(ModifiedUserDetails modifiedUserDetails, UserDetails deletedUserDetails, String operationDescription)
    {
        auditRepository.logUserRemoved(modifiedUserDetails, deletedUserDetails, operationDescription);
    }

    public void logUserSiteChange(SiteForUser siteForUser, String operationDescription)
    {
        auditRepository.logUserSiteChange(siteForUser, operationDescription);
    }

    public void logUserAdd(NewUserDetails userDetails)
    {
        auditRepository.logNewUser(userDetails);
    }

    public void logSettingChanged(String key, String oldValue, String newValue, String requester)
    {
        auditRepository.logSettingChanged(key, oldValue, newValue, requester);
    }

    public List<AuditLog> getAuditLog(long miuId, String user, int daysAgo, Integer page)
    {
        return auditRepository.getAuditLog(miuId, user, daysAgo, page);
    }

    public int getAuditLogCount(long miuId, String user, int daysAgo)
    {
        return auditRepository.getAuditLogCount(miuId, user, daysAgo);
    }

    public void logVerifyUserFailed(String userName, String description)
    {
        auditRepository.logVerifyUserFailed(userName, description);
    }

    public void logAccessDenied(String userName, String description)
    {
        auditRepository.logAccessDenied(userName, description);
    }


    /**
     * Generate a descriptor to identify the originator of a command or change for audit purpose.
     * @param siteId Utility ID
     * @return
     */
    public String getRequesterFromSiteId(SiteId siteId)
    {
        Optional<String> distributerId = refDataRepository.lookupDistributerIdFromSiteId(siteId);

        String auditLogUser = "Site id: " + siteId;

        if (distributerId.isPresent())
        {
            auditLogUser += " Distributer: " + distributerId.get();
        }

        return auditLogUser;
    }

}
