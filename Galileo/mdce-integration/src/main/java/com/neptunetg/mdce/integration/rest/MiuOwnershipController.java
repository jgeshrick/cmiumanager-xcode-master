/*
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleService;
import com.neptunetg.mdce.integration.data.miu.MiuOwnerHistory;
import com.neptunetg.mdce.integration.data.miu.MiuOwnershipRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.data.site.SiteRepository;
import com.neptunetg.mdce.integration.domain.MiuService;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.ForbiddenException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import com.neptunetg.mdce.integration.rest.json.JsonMiuActiveFlag;
import com.neptunetg.mdce.integration.rest.json.JsonMiuOwnerHistoryCollection;
import com.neptunetg.mdce.integration.rest.json.JsonMiuOwnership;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * IF29
 */
@RestController
public class MiuOwnershipController extends BaseTokenAuthenticatingRestController
{
    private static final int miuOwnerHistoryPageSize = 1000;

    private static final Logger logger = LoggerFactory.getLogger(MiuOwnershipController.class);

    @Autowired
    private SiteRepository siteRepository;

    @Autowired
    private MiuService miuService;

    @Autowired
    private MiuLifecycleService miuLifecycleService;

    @Autowired
    private MiuOwnershipRepository miuOwnershipRepository;

    /**
     * used by N_SIGHT to claim an MIU for a utility
     *
     * @param token        token for this session
     * @param miuOwnership miu ownership json
     * @param request      http request
     * @throws NotAuthorizedException
     * @throws BadRequestException
     * @throws ForbiddenException
     */
    @RequestMapping(value = "/mdce/api/v1/miu/ownership", method = RequestMethod.PUT)
    public JsonMiuOwnership setMiuOwnership(@RequestParam("token") String token,
                                @RequestBody JsonMiuOwnership miuOwnership,
                                HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException, ForbiddenException, InterruptedException
    {
        final SiteId siteId = getClientSiteId(request);

        //check site id in json matches site id in token
        checkRequestSiteIdMatchesJsonSiteIdOrThrowForbidden(siteId, miuOwnership.getSiteId());

        this.siteRepository.ensureSiteExists(siteId);

        int count = 0;

        while (count < 3)
        {
            count++;

            try
            {
                miuService.updateMiuOwnerDetailsBatch(miuOwnership.getMiuList(), siteId);

                for(JsonMiuActiveFlag mius : miuOwnership.getMiuList())
                {
                    if(SiteId.valueOf(miuOwnership.getSiteId()) == SiteId.UNIVERSAL)
                    {
                        miuLifecycleService.setMiuLifecycleState(MiuId.valueOf(mius.getMiuId()),
                                MiuLifecycleStateEnum.UNCLAIMED);
                    }
                    else
                    {
                        miuLifecycleService.setMiuLifecycleState(MiuId.valueOf(mius.getMiuId()),
                                MiuLifecycleStateEnum.CLAIMED);
                    }
                }

                break;
            }
            catch (Exception e)
            {
                if(count == 3)
                {
                    logger.error("Update Miu Owner Details Failed 3 times in a row", e);
                }
                else
                {
                    logger.debug("Exception occured while attempting to set MIU ownership, and was retried", e);
                }
            }

            //This increases the delay before a retry each time it fails (2 seconds first time, then 4, then 6)
            Thread.sleep(2000 * count);
        }

        return miuOwnership;
    }


    @RequestMapping(value = "/mdce/api/v1/miu/owner_history", method = RequestMethod.GET)
    public JsonMiuOwnerHistoryCollection getMiuOwnerHistory(
            @RequestParam("token") String token,
            @RequestParam(value = "last_sequence", required = false) Integer lastSequenceParameter,
            HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException
    {
        // Only accessible to the universal site ID.
        final SiteId siteId = getClientSiteId(request);
        if (siteId.doesNotMatch(0))
        {
            throw new NotAuthorizedException("This API is not available to the current site ID.");
        }

        final int lastSequence = lastSequenceParameter == null ? -1 : lastSequenceParameter.intValue();

        List<MiuOwnerHistory> miuOwnerHistory = this.miuOwnershipRepository.getMiuOwnerHistory(lastSequence, miuOwnerHistoryPageSize);

        JsonMiuOwnerHistoryCollection jsonHistory = new JsonMiuOwnerHistoryCollection(miuOwnerHistory);

        return jsonHistory;
    }
}
