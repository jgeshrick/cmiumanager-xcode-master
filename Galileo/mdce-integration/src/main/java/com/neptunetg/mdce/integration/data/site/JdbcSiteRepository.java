/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.site;


import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

/**
 * Repository for MySql database - sites
 */
@Repository
public class JdbcSiteRepository implements SiteRepository
{

    private static final Logger logger = LoggerFactory.getLogger(JdbcSiteRepository.class);

    private static final int maxResults = 100;
    private final JdbcTemplate db;

    @Autowired
    public JdbcSiteRepository(JdbcTemplate db)
    {
        this.db = db;
    }


    @Override
    public List<SiteDetails> getSiteList()
    {
        String sql = "SELECT sl.site_id, sl.name, rdi.account_name, rdi.regional_manager FROM site_list sl " +
                "LEFT JOIN ref_data_utilities rdu ON rdu.site_id = sl.site_id " +
                "LEFT JOIN ref_data_info rdi ON rdi.ref_data_info_id = rdu.info_id " +
                "ORDER BY sl.name ASC";
        return this.db.query(sql,
                (rs, rownum) ->
                        new SiteDetails(
                                rs.getInt(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getString(4)
                        ));
    }


    @Override
    public SiteDetails getSiteDetails(SiteId siteId)
    {
        String sql = "SELECT sl.name, rdi.account_name, rdi.regional_manager FROM site_list sl " +
                "LEFT JOIN ref_data_utilities rdu ON rdu.site_id = sl.site_id " +
                "LEFT JOIN ref_data_info rdi ON rdi.ref_data_info_id = rdu.info_id " +
                "WHERE sl.site_id = ?";
        return this.db.queryForObject(sql,
                (rs, rownum) ->
                        new SiteDetails(
                                siteId.numericValue(),
                                rs.getString(1),
                                rs.getString(2),
                                rs.getString(3)
                        )
                , siteId.numericWrapperValue());
    }


    @Override
    public boolean hasSite(SiteId siteId)
    {
        String sql = "SELECT COUNT(*) FROM mdce.site_list WHERE site_id = ? ";
        Integer result = this.db.queryForObject(sql, new Object[]{siteId.numericWrapperValue()}, Integer.class);

        return result > 0;
    }

    @Override
    public int createSite(SiteId siteId, String siteName)
    {
        return this.db.update("INSERT INTO mdce.site_list (site_id, name) VALUES (?, ?)", siteId.numericWrapperValue(), siteName);
    }

    @Override
    public boolean ensureSiteExists(SiteId siteId)
    {
        return ensureSiteExists(siteId, "Site " + siteId + " placeholder (" + LocalDate.now() + ")");
    }

    @Override
    public boolean ensureSiteExists(SiteId siteId, String siteName)
    {
        //check site id exist in database, if not create
        if (hasSite(siteId))
        {
            return false;
        }

        createSite(siteId, siteName);
        return true;

    }


    @Override
    public void updateLastDataRequest(SiteId siteId, Instant queryTime)
    {
        final String sql = "UPDATE site_list SET last_data_request = FROM_UNIXTIME(?) WHERE site_id=?";

        try
        {
            this.db.update(sql, queryTime.getEpochSecond(), siteId.numericWrapperValue());
        }
        catch (Exception e)
        {
            logger.error("Could not update last data request time for site " + siteId, e);
        }
    }

    /**
     * Finds sites that have not contacted mdce between a configurable amount of time.
     *
     * @param siteConnectionCheckHours max amount of hours an site should be talking
     *        to mdce anything after that is an issue.
     * @param siteConnectionIgnoreAfterHours max amount of hours a site can be inactive
     *        before we declare inactive and ignore.
     * @return list of sites
     */
    @Override
    public List<SiteDetails> getSitesNotRequestingDataWithinXHours(int siteConnectionCheckHours, int siteConnectionIgnoreAfterHours) throws InternalApiException
    {
        String sql = "SELECT * FROM site_list " +
                "WHERE last_data_request IS NOT NULL " +
                "AND last_data_request <= NOW() - INTERVAL ? HOUR " +
                "AND last_data_request >= NOW() - INTERVAL ? HOUR";

        return this.db.query(sql,
                (rs, rownum) ->
                        new SiteDetails(
                                rs.getInt(1),
                                rs.getString(2),
                                rs.getString(3)
                        ), siteConnectionCheckHours, siteConnectionIgnoreAfterHours);
    }
}
