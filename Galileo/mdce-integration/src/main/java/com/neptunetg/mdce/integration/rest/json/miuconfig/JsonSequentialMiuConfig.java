/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.json.miuconfig;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.mdce.common.internal.miu.model.SequentialCmiuConfig;

import java.time.Instant;

/**
 * Contains details of a single CMIU mode change event with a sequence ID for change tracking.
 */
public class JsonSequentialMiuConfig
{
    @JsonProperty("sequence_id")
    private int sequenceId;

    @JsonProperty("miu_id")
    private int miuId;

    @JsonProperty("received_from_miu")
    private boolean receivedFromMiu;

    @JsonProperty("timestamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant timestamp;

    @JsonProperty("site_id")
    private int siteId;

    @JsonProperty("recording_interval_minutes")
    private Integer recordingIntervalMinutes;

    @JsonProperty("reporting_interval_minutes")
    private Integer reportingIntervalMinutes;

    public JsonSequentialMiuConfig(int sequenceId, int miuId, boolean receivedFromMiu, Instant timestamp, int siteId,
                                   Integer recordingIntervalMinutes, Integer reportingIntervalMinutes)
    {
        this.sequenceId = sequenceId;
        this.miuId = miuId;
        this.receivedFromMiu = receivedFromMiu;
        this.timestamp = timestamp;
        this.siteId = siteId;
        this.recordingIntervalMinutes = recordingIntervalMinutes;
        this.reportingIntervalMinutes = reportingIntervalMinutes;
    }

    public static JsonSequentialMiuConfig from(SequentialCmiuConfig config)
    {
        return new JsonSequentialMiuConfig(
                config.getSequenceId(),
                config.getMiuId(),
                config.isReceivedFromMiu(),
                config.getTimestamp(),
                config.getSiteId(),
                config.getRecordingIntervalMinutes(),
                config.getReportingIntervalMinutes());
    }

    public int getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(int sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public int getMiuId()
    {
        return miuId;
    }

    public void setMiuId(int miuId)
    {
        this.miuId = miuId;
    }

    public boolean isReceivedFromMiu()
    {
        return receivedFromMiu;
    }

    public void setReceivedFromMiu(boolean receivedFromMiu)
    {
        this.receivedFromMiu = receivedFromMiu;
    }

    public Instant getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public Integer getRecordingIntervalMinutes()
    {
        return recordingIntervalMinutes;
    }

    public void setRecordingIntervalMinutes(Integer recordingIntervalMinutes)
    {
        this.recordingIntervalMinutes = recordingIntervalMinutes;
    }

    public Integer getReportingIntervalMinutes()
    {
        return reportingIntervalMinutes;
    }

    public void setReportingIntervalMinutes(Integer reportingIntervalMinutes)
    {
        this.reportingIntervalMinutes = reportingIntervalMinutes;
    }
}
