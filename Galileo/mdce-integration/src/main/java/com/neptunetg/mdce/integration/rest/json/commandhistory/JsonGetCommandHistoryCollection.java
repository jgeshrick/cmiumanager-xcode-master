/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.commandhistory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.common.data.miu.command.MiuCommandDetail;

import java.util.List;
import java.util.stream.Collectors;

/**
    JSON object for Get command history IF32
*/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonGetCommandHistoryCollection
{
    @JsonProperty("miu_id")
    private long miuId;

    @JsonProperty("packets")
    private List<JsonGetCommandHistoryPacket> getCommandPackets;

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public List<JsonGetCommandHistoryPacket> getGetCommandPackets()
    {
        return getCommandPackets;
    }

    public void setGetCommandPackets(List<JsonGetCommandHistoryPacket> getCommandPackets)
    {
        this.getCommandPackets = getCommandPackets;
    }

    /**
     * Build a JsonGetCommandHistoryCollection class from MiuCommand list, assuming the list contains command for one miu
     * @param miuCommandList    Command list for ONE miu id.
     */
    public static JsonGetCommandHistoryCollection from(List<MiuCommandDetail> miuCommandList)
    {
        JsonGetCommandHistoryCollection commandHistoryCollection = new JsonGetCommandHistoryCollection();
        List<JsonGetCommandHistoryPacket> packetList = miuCommandList.stream()
                .map(JsonGetCommandHistoryPacket::from)
                .collect(Collectors.toList());

        if (packetList.size() > 0)
        {
            commandHistoryCollection.setMiuId(miuCommandList.get(0).getMiuId().numericValue());
            commandHistoryCollection.setGetCommandPackets(packetList);
        }

        return commandHistoryCollection;

    }

}
