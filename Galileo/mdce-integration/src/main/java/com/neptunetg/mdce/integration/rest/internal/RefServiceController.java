/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataDistributer;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataUtility;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.common.internal.site.service.RefService;
import com.neptunetg.mdce.integration.data.refdata.RefDataRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/service")
public class RefServiceController implements RefService
{
    @Autowired
    private RefDataRepository refDataRepository;

    @Override
    public List<String> getRegionalManagers() throws InternalApiException
    {
        return refDataRepository.getRegionalManagers();
    }

    @Override
    public List<String> getStates() throws InternalApiException
    {
        return refDataRepository.getStates();
    }

    @Override
    public List<String> getMiuTypes() throws InternalApiException
    {
        return refDataRepository.getMiuTypes();
    }

    @Override
    public List<SiteDetails> getSitesForRegionalManager(@RequestParam(value = RefService.REGIONAL_MANAGER) String regionalManager) throws InternalApiException
    {
        return refDataRepository.getSitesForRegionalManager(regionalManager);
    }

    @Override
    public List<SiteDetails> getSitesForState(@RequestParam(value = RefService.STATE) String state) throws InternalApiException
    {
        return refDataRepository.getSitesForState(state);
    }

    @Override
    public MdceReferenceDataUtility getSiteReferenceData(@RequestParam(RefService.SITE_ID) int siteId) throws InternalApiException
    {
        return refDataRepository.getSiteReferenceData(SiteId.valueOf(siteId));
    }

    @Override
    public MdceReferenceDataDistributer getDistributerReferenceData(@RequestParam(RefService.DISTRIBUTER_ID) String distributerId) throws InternalApiException
    {
        return refDataRepository.getDistributerReferenceData(distributerId);
    }

    @Override
    public List<String> getSimNetworks(@RequestParam Map<String, String> options) throws InternalApiException
    {
        return refDataRepository.getSimNetworks(options);
    }

    @Override
    public List<String> getSimLifeCycleStates(@RequestParam Map<String, String> options) throws InternalApiException
    {
        return refDataRepository.getSimLifeCycleStates(options);
    }


}
