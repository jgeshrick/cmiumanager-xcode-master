/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.integration.utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Stream wrapper that saves all bytes read to a file
 */
public class SaveToFileStream extends InputStream
{
    private final FileOutputStream fileOut;
    private final InputStream wrappedStream;

    /**
     * Set to true when the wrapped stream is done
     */
    private boolean underlyingStreamExhausted = false;


    /**
     * Create an instance
     * @param is Raw bytes input stream
     * @param filepath Path to save data
     * @throws FileNotFoundException if path cannot be accessed
     */
    public SaveToFileStream(InputStream is, File filepath) throws FileNotFoundException
    {
        this.fileOut = new FileOutputStream(filepath, false);
        this.wrappedStream = is;
    }

    /**
     * Read wrapped stream to end then close
     * @throws IOException If something goes wrong during close
     */
    @Override
    public void close() throws IOException
    {
        byte[] buffer = new byte[4096];

        while (!this.underlyingStreamExhausted)
        {
            this.read(buffer); //copy all remaining bytes
        }

        fileOut.close();
        wrappedStream.close();
    }

    /**
     * Read into an array of bytes
     * @param b buffer into which to read bytes
     * @return Number of bytes read, between 0 and the size of the buffer
     * @throws IOException If bytes could not be read
     */
    @Override
    public int read(byte[] b) throws IOException
    {
        return this.read(b, 0, b.length);
    }

    /**
     * Read into an array of bytes
     * @param b Array
     * @param off Offset in array
     * @param len Max bytes to read
     * @return Number of bytes read
     * @throws IOException If bytes could not be read
     */
    @Override
    public int read(byte[] b, int off, int len) throws IOException
    {
        if (this.underlyingStreamExhausted)
        {
            return -1;
        }
        int bytesRead = wrappedStream.read(b, off, len);
        if (bytesRead > 0)
        {
            fileOut.write(b, off, bytesRead);
        }
        else
        {
            this.underlyingStreamExhausted = true;
        }
        return bytesRead;
    }

    /**
     * Read a single byte
     * @return unsigned byte value or -1 if end of stream
     * @throws IOException If byte could not be read
     */
    @Override
    public int read() throws IOException
    {
        if (this.underlyingStreamExhausted)
        {
            return -1;
        }
        int ret = wrappedStream.read();
        if (ret >= 0)
        {
            fileOut.write(ret);
        }
        else
        {
            this.underlyingStreamExhausted = true;
        }
        return ret;
    }

    /**
     * Skip past some bytes - reading and saving to file at same time
     * @param n Bytes to skip
     * @return Number of bytes skipped
     * @throws IOException If bytes could not be skipped
     */
    @Override
    public long skip(long n) throws IOException
    {
        if (underlyingStreamExhausted)
        {
            return 0L;
        }
        byte[] buffer = new byte[4096];
        long bytesToSkip = n;
        long totalBytesSkipped = 0L;
        while (bytesToSkip > 0L)
        {
            long bytesSkipped = this.read(buffer, 0, (int)Math.min(bytesToSkip, buffer.length));
            if (bytesSkipped <= 0L)
            {
                break;
            }
            bytesToSkip -= bytesSkipped;
            totalBytesSkipped += bytesSkipped;
        }
        return totalBytesSkipped;
    }

    /**
     * Get number of available bytes in underlying stream
     * @return Number of bytes
     * @throws IOException If could not be done
     */
    @Override
    public int available() throws IOException
    {
        return wrappedStream.available();
    }

    /**
     * Not supported
     * @param readlimit ignored
     * @throws UnsupportedOperationException always
     */
    @Override
    public synchronized void mark(int readlimit)
    {
        throw new UnsupportedOperationException("Can't mark");
    }

    /**
     * Not supported
     * @throws IOException not thrown
     * @throws UnsupportedOperationException always
     */
    @Override
    public synchronized void reset() throws IOException
    {
        throw new UnsupportedOperationException("Can't reset");
    }

    /**
     * Returns false, indicating stream cannot be marked
     * @return false
     */
    @Override
    public boolean markSupported()
    {
        return false;
    }

}