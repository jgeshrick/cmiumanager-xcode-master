/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.cellular.*;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.data.miu.lora.LoraDeviceDetail;
import com.neptunetg.common.data.miu.lora.LoraRepository;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleService;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.audit.model.AuditLog;
import com.neptunetg.mdce.common.internal.audit.model.AuditLogPagedResult;
import com.neptunetg.mdce.common.internal.miu.model.MiuDataUsage;
import com.neptunetg.mdce.common.internal.miu.model.MiuDisplayDetails;
import com.neptunetg.mdce.common.internal.miu.model.MiuDisplayDetailsPagedResult;
import com.neptunetg.mdce.common.internal.miu.service.MiuService;
import com.neptunetg.mdce.integration.audit.AuditService;
import com.neptunetg.mdce.integration.data.miu.MiuDetails;
import com.neptunetg.mdce.integration.data.miu.MiuOwnershipRepository;
import com.neptunetg.mdce.integration.data.miu.MiuSearchRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.rest.BaseTokenAuthenticatingRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/service")
public class MiuDetailsInternalController extends BaseTokenAuthenticatingRestController implements MiuService
{
    /**
     * The reading interval is currently fixed at 15 minutes
     */
    private static final int READING_INTERVAL_MINS = 15;

    @Autowired
    private MiuSearchRepository miuSearchRepository;

    @Autowired
    private CmiuRepository cmiuRepository;

    @Autowired
    private MiuOwnershipRepository miuOwnershipRepository;

    @Autowired
    private AuditService auditService;

    @Autowired
    private MiuLifecycleService miuLifecycleService;

    @Autowired
    private CmiuConfigRepository cmiuConfigRepository;

    @Autowired
    private LoraRepository loraRepository;

    @Override
    @ResponseBody
    @RequestMapping(value = MiuService.URL_MIU_LIST, method = RequestMethod.GET)
    public MiuDisplayDetailsPagedResult getMiuList(@RequestParam Map<String,String> options) throws InternalApiException
    {
        final List<MiuDetails> miusForUtility =  miuSearchRepository.getMius(options);

        Integer count = miuSearchRepository.getMiusCount(options);

        //save a db call by correcting for miuId not found
        if (count == 1 && miusForUtility.isEmpty())
        {
            count = 0;
        }

        final List<MiuDisplayDetails> miuDisplayDetails = toMiuDetailsList(miusForUtility);

        try
        {
            ifRequestingSingleMiuThenPopulateExtraDetails(options, miuDisplayDetails);
        }
        catch (MiuDataException e)
        {
            throw new InternalApiException("Error getting MIU List for options " + options.toString(), e);
        }

        final String page = options.get("page");

        final Integer pageNum = page == null ? 0 : Integer.parseInt(page);

        return new MiuDisplayDetailsPagedResult(count, miuDisplayDetails.size(), pageNum, miuDisplayDetails);
    }

    private void ifRequestingSingleMiuThenPopulateExtraDetails(Map<String, String> options, List<MiuDisplayDetails> miuDisplayDetails) throws MiuDataException
    {
        if (options.containsKey("miuId") && miuDisplayDetails.size() == 1)
        {
            fetchExtraDetails(options, miuDisplayDetails.get(0));
        }
    }

    private void fetchExtraDetails(Map<String, String> options, MiuDisplayDetails miuDisplayDetails) throws MiuDataException
    {
        final MiuId miuId = MiuId.fromString(options.get("miuId"));

        final AuditLogPagedResult auditLogForMiu = getAuditLogs(options, miuId);
        miuDisplayDetails.setAuditLog(auditLogForMiu);

        final MiuDataUsage latestMiuDataUsage = convertDataUsage(cmiuRepository.getLatestMiuDataUsage(miuId));
        miuDisplayDetails.setMiuDataUsage(latestMiuDataUsage);

        if( MiuType.displayString(MiuType.CMIU).equals(miuDisplayDetails.getType()) )
        {
            fetchExtraCmiuDetails(miuId, miuDisplayDetails);
        }
        else if( MiuType.displayString(MiuType.L900).equals(miuDisplayDetails.getType()) )
        {
            fetchExtraLmiuDetails(miuId, miuDisplayDetails);
        }

        MiuLifecycleState miuLifecycleState = miuLifecycleService.getMiuLifecycleState(miuId);

        if(miuLifecycleState.getLifecycleState() != null)
        {
            miuDisplayDetails.setLifecycleState(miuLifecycleState.getLifecycleState().getStringValue());
        }
    }

    private void fetchExtraCmiuDetails(MiuId miuId, MiuDisplayDetails miuDisplayDetails) throws MiuDataException
    {
        final CellularDeviceDetail cellularConfig = cmiuRepository.getCellularConfig(miuId);

        if (cellularConfig != null)
        {
            miuDisplayDetails.setMsisdn(cellularConfig.getMsisdn());
            miuDisplayDetails.setIccid(cellularConfig.getIccid());
            final CellularDeviceDetail.ModemConfig modemConfig = cellularConfig.getModemConfig();
            miuDisplayDetails.setNetworkProvider(cellularConfig.getSimMno());

            if (modemConfig != null)
            {
                miuDisplayDetails.setImei(modemConfig.getImei());
                miuDisplayDetails.setTelitFirmwareRevision(modemConfig.getFirmwareRevision());
                miuDisplayDetails.setModemMno(modemConfig.getMno());
                miuDisplayDetails.setModemVendor(modemConfig.getVendor());
            }

        }

        final CmiuRevisionSet cmiuRevisionSet = cmiuRepository.getCurrentRevisionSet(miuId);

        if(cmiuRevisionSet != null)
        {
            miuDisplayDetails.setCmiuFirmwareRevision(cmiuRevisionSet.getFirmwareRevision());
            miuDisplayDetails.setCmiuConfigRevision(cmiuRevisionSet.getConfigRevision());
            miuDisplayDetails.setCmiuArbRevision(cmiuRevisionSet.getArbRevision());
            miuDisplayDetails.setCmiuBleRevision(cmiuRevisionSet.getBleRevision());
            miuDisplayDetails.setCmiuEncryptionRevision(cmiuRevisionSet.getEncryptionRevision());
            miuDisplayDetails.setTelitFirmwareRevision(cmiuRevisionSet.getTelitFirmwareRevision());
        }

        // Get CMIU config set details
        final CmiuConfigSetManagement cmiuConfigMgmt = cmiuConfigRepository.getCmiuConfigMgmt(miuId);
        if (cmiuConfigMgmt != null)
        {
            final ConfigSet currentCmiuConfig = cmiuConfigMgmt.getCurrentConfig();
            if (currentCmiuConfig != null)
            {
                miuDisplayDetails.setCmiuConfigSetName(currentCmiuConfig.getName());
                miuDisplayDetails.setReportingIntervalMins(currentCmiuConfig.getReportingIntervalMins());
                miuDisplayDetails.setRecordingIntervalMins(currentCmiuConfig.getRecordingIntervalMins());
                miuDisplayDetails.setReadingIntervalMins(READING_INTERVAL_MINS);
            }

            final ConfigSet reportedCmiuConfig = cmiuConfigMgmt.getReportedConfig();
            if (reportedCmiuConfig != null)
            {
                miuDisplayDetails.setReportedCmiuConfigSetName(reportedCmiuConfig.getName());
                miuDisplayDetails.setReportedReportingIntervalMins(reportedCmiuConfig.getReportingIntervalMins());
                miuDisplayDetails.setReportedRecordingIntervalMins(reportedCmiuConfig.getRecordingIntervalMins());
                miuDisplayDetails.setReportedReadingIntervalMins(READING_INTERVAL_MINS);
            }
        }
    }

    private void fetchExtraLmiuDetails(MiuId miuId, MiuDisplayDetails miuDisplayDetails)
    {
        LoraDeviceDetail loraDeviceDetail = loraRepository.getLoraDeviceDetailByMiuId(miuId);

        if(loraDeviceDetail != null)
        {
            if(loraDeviceDetail.getEui() != null)
            {
                miuDisplayDetails.setEui(loraDeviceDetail.getEui().toString());
            }

            if(loraDeviceDetail.getLoraNetwork() != null)
            {
                miuDisplayDetails.setNetworkProvider(loraDeviceDetail.getLoraNetwork().getStringValue());
            }

            if(loraDeviceDetail.getLoraAntennaType() != null)
            {
                miuDisplayDetails.setAntennaType(loraDeviceDetail.getLoraAntennaType().getStringValue());
            }
        }
    }

    private AuditLogPagedResult getAuditLogs(Map<String, String> options, MiuId miuId)
    {
        final String user = options.get("user");
        final int daysAgo = getOptionOrDefault(options, "daysAgo", 2);
        final Integer page = getOptionOrDefault(options, "page", 0);

        return getAuditLogForMiu(miuId, user, daysAgo, page);
    }

    private int getOptionOrDefault(Map<String, String> options, String option, int defaultValue)
    {
        if (options.containsKey(option))
        {
            final String daysAgo = options.get(option);
            if (!daysAgo.isEmpty())
            {
                return Integer.parseInt(daysAgo);
            }
        }
        return defaultValue;
    }

    private AuditLogPagedResult getAuditLogForMiu(MiuId miuId, String user, int daysAgo, Integer page)
    {
        final List<AuditLog> auditLog = auditService.getAuditLog(miuId.numericValue(), user, daysAgo, page);

        final int auditLogCount = auditService.getAuditLogCount(miuId.numericValue(), user, daysAgo);

        return new AuditLogPagedResult(Integer.valueOf(auditLogCount),
                Integer.valueOf(auditLog.size()),
                Integer.valueOf(page),
                auditLog);
    }

    private List<MiuDisplayDetails> toMiuDetailsList(List<MiuDetails> miusForUtility)
    {
        return miusForUtility
                .stream()
                .map(this::getMiuDisplayDetails)
                .collect(Collectors.toList());
    }

    private MiuDisplayDetails getMiuDisplayDetails(MiuDetails miu)
    {
        final MiuId id = miu.getId();

        final MiuDisplayDetails miuDisplayDetails = new MiuDisplayDetails();
        miuDisplayDetails.setMiuId(id.numericValue());
        miuDisplayDetails.setSiteId(miu.getSiteId().numericValue());
        miuDisplayDetails.setMeterActive(miu.getMeterActive());
        miuDisplayDetails.setLastHeardTimeToInstant(miu.getLastHeardTime());
        miuDisplayDetails.setType(MiuType.displayString(miu.getType()));

        // Populate type specific fields
        if( MiuType.CMIU.equals(miu.getType()) )
        {
            getCmiuDisplayDetails(miuDisplayDetails, miu);
        }
        else if( MiuType.L900.equals(miu.getType()))
        {
            getL900DisplayDetails(miuDisplayDetails, miu);
        }

        miuDisplayDetails.setAuditLog(new AuditLogPagedResult(0, 0, 0, Collections.EMPTY_LIST));

        return miuDisplayDetails;
    }

    private void getL900DisplayDetails(MiuDisplayDetails miuDisplayDetails, MiuDetails miu)
    {
        miuDisplayDetails.setNetworkProvider(miu.getNetworkProvider());
    }

    private void getCmiuDisplayDetails(MiuDisplayDetails miuDisplayDetails, MiuDetails miu)
    {
        miuDisplayDetails.setNetworkProvider(miu.getNetworkProvider());

        if (miu.getRecordingIntervalMinutes() != null)
        {
            miuDisplayDetails.setRecordingIntervalMins(miu.getRecordingIntervalMinutes().intValue());
        }

        if (miu.getReportingIntervalMinutes() != null)
        {
            miuDisplayDetails.setReportingIntervalMins(miu.getReportingIntervalMinutes().intValue());
        }

        miuDisplayDetails.setCmiuModeName(miu.getCmiuModeName());
        miuDisplayDetails.setReportedCmiuModeName(miu.getReportedCmiuModeName());
    }

    @Override
    @ResponseBody
    @RequestMapping(value = MiuService.URL_MIU_CLAIM, method = RequestMethod.POST)
    public void claimMiu(
            @RequestParam(value = MiuService.REQUEST_PARAM_MIU_ID) long miuId,
            @RequestParam(value = MiuService.REQUEST_PARAM_NEW_SITE_ID) int newSiteId,
            @RequestParam(value = MiuService.REQUEST_PARAM_USER) String requester) throws InternalApiException
    {
        final MiuId miuIdToClaim = MiuId.valueOf(miuId);

        final SiteId oldSiteId = miuOwnershipRepository.getMiuOwner(miuIdToClaim);

        if(newSiteId == SiteId.UNIVERSAL.numericValue())
        {
            miuLifecycleService.setMiuLifecycleState(miuIdToClaim, MiuLifecycleStateEnum.UNCLAIMED);
        }
        else
        {
            miuLifecycleService.setMiuLifecycleState(miuIdToClaim, MiuLifecycleStateEnum.CLAIMED);
        }

        final SiteId siteId = new SiteId(newSiteId);
        updateMiuOwnerDetails(miuIdToClaim, siteId);

        auditService.logMiuOwnershipChanged(miuIdToClaim, oldSiteId, siteId, requester);

    }

    @Override
    public List<String> getSetableLifecycleStates(@RequestParam("miuId") long miuId,
                                                  @RequestParam("miuType") String miuType) throws InternalApiException
    {
        List<String> states = new ArrayList<>();

        //Valid for both CMIU and L900
        states.add(MiuLifecycleStateEnum.DEVELOPMENT.getStringValue());
        states.add(MiuLifecycleStateEnum.RMAED.getStringValue());
        states.add(MiuLifecycleStateEnum.ROGUE.getStringValue());
        states.add(MiuLifecycleStateEnum.DEAD.getStringValue());

        return states;
    }

    @Override
    public void setLifecycleState(@RequestParam("miuId") long miuId,
                                  @RequestParam("lifecycleState") String lifecycleState) throws InternalApiException
    {
        MiuId miu = MiuId.valueOf(miuId);

        //Should work for both CMIU and L900. Differentiates between the two based on the MIU ID
        miuLifecycleService.setMiuLifecycleState(miu, MiuLifecycleStateEnum.fromStringValue(lifecycleState));
    }

    @Override
    public List<String> getAllCmiuModes() throws InternalApiException
    {
        return cmiuRepository.getAllCmiuModes();
    }

    private void updateMiuOwnerDetails(MiuId miuId, SiteId siteId)
    {
        final String active = miuOwnershipRepository.isMiuBillable(miuId) ? "Y" : "N";

        this.miuOwnershipRepository.updateMiuOwnerDetails(miuId, siteId, active, null);
    }


    private static MiuDataUsage convertDataUsage(CmiuDataUsage dataUsage)
    {
        if(dataUsage == null)
        {
            return null;
        }

        final MiuDataUsage ret = new MiuDataUsage();
        ret.setMiuId(dataUsage.getMiuId().numericValue());
        ret.setDataUsageDate(dataUsage.getDataUsageDate());
        ret.setDataUsageDatetime(dataUsage.getDataUsageDatetime());
        ret.setByteUsedToday(dataUsage.getByteUsedToday());
        ret.setByteUsedMonthToDate(dataUsage.getByteUsedMonthToDate());
        return ret;
    }

}
