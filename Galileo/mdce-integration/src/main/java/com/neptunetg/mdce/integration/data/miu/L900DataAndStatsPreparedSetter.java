/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.data.miu;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.util.JdbcDateUtil;
import com.neptunetg.common.lora.json.ActilityUplinkJson;
import com.neptunetg.common.lora.json.SenetForwardPacketJson;
import com.neptunetg.common.lora.parser.LoraPacketParser;
import com.neptunetg.common.lora.pdu.BasicConfigPacket;
import com.neptunetg.common.lora.pdu.L900Packet;
import com.neptunetg.common.lora.pdu.PacketId;
import com.neptunetg.mdce.integration.utility.L900BatteryVoltage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.PreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.time.Instant;

/**
 * Created by WJD1 on 28/07/2016.
 * A class to facilitate pushing L900 stats into mySQL, by representing the data.
 */
public class L900DataAndStatsPreparedSetter implements PreparedStatementSetter
{
    private static final Logger log = LoggerFactory.getLogger(L900DataAndStatsPreparedSetter.class);

    private final long miuId;
    private final Instant heardTime;
    private final int miuPacketCount;
    private final double rssi;
    private final double snr;
    private final int datarate;
    private final float txpower;
    private final float freq;
    private Double batteryVoltage;

    //Create prepared setter from actility object
    public L900DataAndStatsPreparedSetter(MiuId miuId, ActilityUplinkJson actilityJson)
    {
        this.miuId = miuId.numericValue();
        this.heardTime = actilityJson.getTxTimeInstant();
        this.miuPacketCount = 1;
        this.rssi = actilityJson.getRssi();
        this.snr = actilityJson.getSnr();
        this.datarate = 0;
        this.txpower = 0;
        this.freq = 0;
        this.batteryVoltage = null;

        Double batteryVoltageFromPdu = null;
        try
        {
            byte[] pdu = actilityJson.getPduBytes();
            this.batteryVoltage = L900BatteryVoltage.extractBatteryVoltageFromPdu(pdu);
        }
        catch (Exception e)
        {
            log.warn("Error while attempting to extract voltage from Actility JSON for MIU " + miuId + " (PDU = " + actilityJson.getPdu() + ")", e);
        }
    }


    //Create prepared setter from senet object
    public L900DataAndStatsPreparedSetter(MiuId miuId, SenetForwardPacketJson senetJson)
    {
        this.miuId = miuId.numericValue();
        this.heardTime = senetJson.getTxTimeInstant();
        this.miuPacketCount = 1;
        this.rssi = senetJson.getRssi();
        this.snr = senetJson.getSnr();
        this.datarate = senetJson.getDataRate();
        this.txpower = senetJson.getTxPower();
        this.freq = senetJson.getFreq();
        this.batteryVoltage = null;

        Double batteryVoltageFromPdu = null;
        try
        {
            byte[] pdu = senetJson.getPduBytes();
            this.batteryVoltage = L900BatteryVoltage.extractBatteryVoltageFromPdu(pdu);
        }
        catch (Exception e)
        {
            log.warn("Error while attempting to extract voltage from Senet JSON for MIU " + miuId + " (PDU = " + senetJson.getPdu() + ")", e);
        }
    }

    @Override
    public void setValues(PreparedStatement s) throws SQLException
    {
        s.setLong(1, miuId);
        s.setObject(2, JdbcDateUtil.bindDateWithDefault(heardTime, Instant.now()));
        s.setInt(3, miuPacketCount);

        //Set all RSSI values to RSSI.  The columns are for average, min and max RSSI to accommodate
        //a multi-endpoint packet, but for L900 we hear from endpoints individually.
        s.setDouble(4, rssi);
        s.setDouble(5, rssi);
        s.setDouble(6, rssi);

        //Set ber, rsrq, processor_reset_count to null
        s.setNull(7, Types.DOUBLE);
        s.setNull(8, Types.DOUBLE);
        s.setNull(9, Types.INTEGER);

        //Set cellular timing to null
        s.setNull(10, Types.INTEGER);
        s.setNull(11, Types.INTEGER);
        s.setNull(12, Types.INTEGER);
        s.setNull(13, Types.INTEGER);
        s.setNull(14, Types.INTEGER);

        //Set mag swipe and battery capacity to null
        s.setNull(15, Types.INTEGER);
        s.setNull(16, Types.DOUBLE);

        //Set battery voltage
        if (this.batteryVoltage == null)
        {
            s.setNull(17, Types.DOUBLE);
        }
        else
        {
            s.setDouble(17, this.batteryVoltage.doubleValue());
        }
        //Set temperature to null
        s.setNull(18, Types.DOUBLE);

        s.setDouble(19, snr);
        s.setInt(20, datarate);
        s.setDouble(21, txpower);
        s.setDouble(22, freq);
    }
}
