/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.gui.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * Bean interface for holding environment data
 */
@Component("EnvProperties")
public class EnvProperties {

    final private String envName;

    @Autowired
    public EnvProperties(@Value("#{envProperties['env.name']}") String envName) {
        this.envName = envName;
    }

    public String getEnvName() {
        return envName;
    }

}


