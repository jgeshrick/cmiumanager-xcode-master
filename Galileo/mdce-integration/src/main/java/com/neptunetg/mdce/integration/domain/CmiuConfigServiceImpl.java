/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.*;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigHistory;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigMgmt;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;
import com.neptunetg.mdce.common.internal.miu.model.SequentialCmiuConfig;
import com.neptunetg.mdce.integration.audit.AuditService;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.rest.json.miuconfig.JsonMiuConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.TemporalAccessor;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service for dealing with CMIU configuration (3Rs and windows).
 */
//Note: code pulled out of CmiuConfigController and CmiuConfigServiceController into service layer
@Service
@Transactional
public class CmiuConfigServiceImpl implements CmiuConfigService
{

    private final CmiuConfigRepository configRepo;

    private final AuditService auditService;

    @Autowired
    public CmiuConfigServiceImpl(CmiuConfigRepository configRepo, AuditService auditService)
    {
        this.configRepo = configRepo;
        this.auditService = auditService;
    }



    @Override
    public List<CmiuConfigSet> getConfigSetList()
    {
        return this.configRepo.getConfigSetList()
                .stream().map(cs -> convertConfigSetFromDBToApi(cs, null, null, null)).collect(Collectors.toList());
    }

    @Override
    public CmiuConfigSet getConfigSetForApiById(long configSetId)
    {
        final ConfigSet configSetFromDB = this.configRepo.getConfigSetFromId(configSetId);
        return convertConfigSetFromDBToApi(configSetFromDB, null, null, null);
    }

    @Override
    public ConfigSet getConfigSetFromDBById(long configSetId)
    {
        return this.configRepo.getConfigSetFromId(configSetId);
    }

    @Override
    public void addNewConfigSet(CmiuConfigSet cmiuConfigSet, String userName)
    {
        final ConfigSet dbConfigSet = convertConfigSetFromApiToDB(cmiuConfigSet);

        this.configRepo.addNewConfigSet(dbConfigSet);

        this.auditService.logConfigSetCreated(dbConfigSet, userName);
    }

    @Override
    public void updateConfigSetName(CmiuConfigSet cmiuConfigSet, String userName)
    {
        final ConfigSet dbConfigSet = convertConfigSetFromApiToDB(cmiuConfigSet);

        final ConfigSet oldConfigSet = this.configRepo.getConfigSetFromId(dbConfigSet.getId());

        this.configRepo.updateConfigSetName(dbConfigSet);

        this.auditService.logConfigSetChanged(oldConfigSet, dbConfigSet, userName);


    }


    @Override
    public CmiuConfigMgmt getCmiuConfigMgmt(MiuId miuId)
    {
        return convertConfigMgmtFromDBToAPI(this.configRepo.getCmiuConfigMgmt(miuId));
    }


    @Override
    public ConfigSet setCurrentCmiuConfig(MiuId miuId, ConfigSet configSet, String userName, String changeNote)
    {
        ConfigSet oldConfigSet = null;

        try
        {
            oldConfigSet = this.configRepo.getCmiuConfigMgmt(miuId).getCurrentConfig();
        }
        catch (Exception e)
        {
            //never mind, just for logging anyway
        }

        final ConfigSet newConfigSet = this.configRepo.updateCmiuConfig(miuId, configSet, oldConfigSet, false, changeNote);

        return newConfigSet;
    }

    @Override
    public List<CmiuConfigMgmt> getCmiuConfigChangeInProcess()
    {
        return this.configRepo.getCmiuConfigChangeInProcess().stream().map(CmiuConfigServiceImpl::convertConfigMgmtFromDBToAPI).collect(Collectors.toList());
    }

    @Override
    public List<CmiuConfigMgmt> getCmiuConfigChangeCompleted(int changedDaysAgo)
    {
        return this.configRepo.getCmiuConfigChangeCompleted(changedDaysAgo).stream().map(CmiuConfigServiceImpl::convertConfigMgmtFromDBToAPI).collect(Collectors.toList());
    }


    /**
     * Map DB object to API object
     * @param managementInfoFromDB DB object
     * @return API object
     */
    private static CmiuConfigMgmt convertConfigMgmtFromDBToAPI(CmiuConfigSetManagement managementInfoFromDB)
    {
        final CmiuConfigMgmt ret = new CmiuConfigMgmt();
        ret.setId(managementInfoFromDB.getId());
        final MiuId miuId = managementInfoFromDB.getCmiuId();
        ret.setCmiuId(miuId.numericValue());
        ret.setCurrentConfig(convertConfigSetFromDBToApi(managementInfoFromDB.getCurrentConfig(), miuId, null, null));
        ret.setCurrentConfigApplyDate(managementInfoFromDB.getCurrentConfigApplyDate());
        ret.setDefaultConfig(convertConfigSetFromDBToApi(managementInfoFromDB.getDefaultConfig(), miuId, null, null));
        ret.setPlannedConfig(convertConfigSetFromDBToApi(managementInfoFromDB.getPlannedConfig(), miuId, null, null));
        ret.setPlannedConfigApplyDate(managementInfoFromDB.getPlannedConfigApplyDate());
        ret.setPlannedConfigRevertDate(managementInfoFromDB.getPlannedConfigRevertDate());
        ret.setReportedConfig(convertConfigSetFromDBToApi(managementInfoFromDB.getReportedConfig(), miuId, null, null));
        ret.setReportedConfigDate(managementInfoFromDB.getReportedConfigDate());
        return ret;
    }


    /**
     * Map DB object to API object
     * @param configFromDB DB object
     * @return API object - no MIU information
     */
    private static CmiuConfigSet convertConfigSetFromDBToApi(ConfigSet configFromDB, MiuId miuId, SiteId siteId, Boolean billable)
    {
        final CmiuConfigSet ret = new CmiuConfigSet();
        ret.setId(configFromDB.getId());
        ret.setName(configFromDB.getName());
        ret.setCmiuModeName(configFromDB.getCmiuModeName());
        ret.setReportingStartMins(configFromDB.getReportingStartMins());
        ret.setReportingIntervalMins(configFromDB.getReportingIntervalMins());
        ret.setReportingTransmitWindowMins(configFromDB.getReportingTransmitWindowMins());
        ret.setReportingQuietStartMins(configFromDB.getReportingQuietStartMins());
        ret.setReportingQuietEndMins(configFromDB.getReportingQuietEndMins());
        ret.setReportingNumberOfRetries(configFromDB.getReportingNumberOfRetries());
        ret.setReportingSyncWarningMins(configFromDB.getReportingSyncWarningMins());
        ret.setReportingSyncErrorMins(configFromDB.getReportingSyncErrorMins());
        ret.setRecordingStartTimeMins(configFromDB.getRecordingStartTimeMins());
        ret.setRecordingIntervalMins(configFromDB.getRecordingIntervalMins());
        ret.setOfficialMode(configFromDB.getOfficialMode());
        if (miuId != null)
        {
            ret.setMiuId(miuId.numericValue());
        }
        if (siteId != null)
        {
            ret.setSiteId(siteId.numericValue());
        }
        ret.setBillable(billable);
        return ret;
    }


    /**
     * Map API object to DB object
     * @param configFromApi DB object
     * @return DB object
     */
    private static ConfigSet convertConfigSetFromApiToDB(CmiuConfigSet configFromApi)
    {
        final ConfigSet ret = new ConfigSet();
        ret.setId(configFromApi.getId());
        ret.setName(configFromApi.getName());
        ret.setCmiuModeName(configFromApi.getCmiuModeName());
        ret.setReportingStartMins(configFromApi.getReportingStartMins());
        ret.setReportingIntervalMins(configFromApi.getReportingIntervalMins());
        ret.setReportingTransmitWindowMins(configFromApi.getReportingTransmitWindowMins());
        ret.setReportingQuietStartMins(configFromApi.getReportingQuietStartMins());
        ret.setReportingQuietEndMins(configFromApi.getReportingQuietEndMins());
        ret.setReportingNumberOfRetries(configFromApi.getReportingNumberOfRetries());
        ret.setReportingSyncWarningMins(configFromApi.getReportingSyncWarningMins());
        ret.setReportingSyncErrorMins(configFromApi.getReportingSyncErrorMins());
        ret.setRecordingStartTimeMins(configFromApi.getRecordingStartTimeMins());
        ret.setRecordingIntervalMins(configFromApi.getRecordingIntervalMins());
        ret.setOfficialMode(configFromApi.getOfficialMode());

        return ret;
    }

    /**
     * Get the config history in a date range
     * @param queryOptions Defines options for filtering the results.
     * @return List of config history that applied in the given date range
     */
    @Override
    public List<CmiuConfigHistory> getConfigHistory(MiuConfigHistoryQueryOptions queryOptions)
    {
        return this.configRepo.getConfigHistory(queryOptions)
                .stream().map(CmiuConfigServiceImpl::convertConfigHistoryFromDBToApi).collect(Collectors.toList());
    }

    /**
     * Map DB object to API object
     * @param historyEntryFromDB DB object
     * @return API object - no MIU information
     */
    private static CmiuConfigHistory convertConfigHistoryFromDBToApi(CmiuConfigHistoryEntry historyEntryFromDB)
    {
        final MiuId miuId = historyEntryFromDB.getMiuId();
        final SiteId siteId = SiteId.valueOf(historyEntryFromDB.getSiteId());
        final CmiuConfigHistory ret = new CmiuConfigHistory();
        ret.setMiuId(miuId.numericValue());
        ret.setMiuType(historyEntryFromDB.getMiuType() != null ? historyEntryFromDB.getMiuType().toString() : null);
        ret.setMiuTypeId(historyEntryFromDB.getMiuType() != null ? historyEntryFromDB.getMiuType().numericValue() : null);
        ret.setDateTime(historyEntryFromDB.getDateTime());
        ret.setReceivedFromCmiu(historyEntryFromDB.isReceivedFromCmiu());
        ret.setChangeNote(historyEntryFromDB.getChangeNote());
        ret.setSiteId(historyEntryFromDB.getSiteId());
        ret.setNetworkProvider(historyEntryFromDB.getNetworkProvider());
        ret.setMsisdn(historyEntryFromDB.getMsisdn());
        ret.setIccid(historyEntryFromDB.getIccid());
        ret.setEui(historyEntryFromDB.getEui());
        ret.setLastHeardTime(historyEntryFromDB.getLastHeardTime());
        ret.setCmiuConfigSet(convertConfigSetFromDBToApi(historyEntryFromDB.getCmiuConfigSet(), miuId, siteId,
                Boolean.valueOf(historyEntryFromDB.isBillable())));
        ret.setMeterActive(historyEntryFromDB.isMeterActive());
        return ret;
    }

    @Override
    public List<CmiuConfigSetAssociation> getCurrentCmiuConfigCollection()
    {
        return this.configRepo.getCurrentCmiuConfigCollection();
    }

    @Override
    public List<CmiuConfigSetAssociation> getCurrentCmiuConfigCollection(SiteId siteId)
    {
        return this.configRepo.getCurrentCmiuConfigCollection(siteId.numericValue());
    }

    @Override
    public CmiuConfigSetAssociation getCurrentCmiuConfig(MiuId miuId) throws MiuDataException
    {
        return this.configRepo.getCurrentCmiuConfig(miuId);
    }

    @Override
    public List<SequentialCmiuConfig> getSequentialConfigHistory(Integer lastSequenceId)
    {
        return this.configRepo.getSequentialConfigHistory(lastSequenceId)
                .stream()
                .map(CmiuConfigServiceImpl::convertSequentialConfigHistoryFromDbToApi)
                .collect(Collectors.toList());
    }

    /**
     * Map DB object to API object
     * @param dbEntry DB object
     * @return API object
     */
    private static SequentialCmiuConfig convertSequentialConfigHistoryFromDbToApi(SequentialCmiuConfigHistoryEntry dbEntry)
    {
        final SequentialCmiuConfig ret = new SequentialCmiuConfig(
                dbEntry.getSequenceId(),
                dbEntry.getMiuId(),
                dbEntry.isReceivedFromMiu(),
                dbEntry.getTimestamp(),
                dbEntry.getSiteId(),
                dbEntry.getRecordingIntervalMinutes(),
                dbEntry.getReportingIntervalMinutes()
        );

        return ret;
    }
}
