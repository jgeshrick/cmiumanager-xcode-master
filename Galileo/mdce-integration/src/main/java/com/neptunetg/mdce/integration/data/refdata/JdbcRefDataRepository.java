/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.integration.data.refdata;


import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceData;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataDistributer;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataInfo;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataInfoContact;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataUtility;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.data.site.SiteRepository;
import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import com.neptunetg.mdce.integration.rest.json.refdata.JsonReferenceData;
import com.neptunetg.mdce.integration.rest.json.refdata.JsonReferenceDataDistributer;
import com.neptunetg.mdce.integration.rest.json.refdata.JsonReferenceDataInfo;
import com.neptunetg.mdce.integration.rest.json.refdata.JsonReferenceDataInfoContact;
import com.neptunetg.mdce.integration.rest.json.refdata.JsonReferenceDataUtility;
import com.neptunetg.mdce.integration.utility.SqlBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


/**
 * Repository for MySql database
 */
@Repository
public class JdbcRefDataRepository implements RefDataRepository
{
    private static final Logger log = LoggerFactory.getLogger(JdbcRefDataRepository.class);

    private static final String INSERT_UTILITY = "INSERT into mdce.ref_data_utilities (site_id, info_id) VALUES (?, ?)";
    private static final String INSERT_DISTRIBUTER = "INSERT into mdce.ref_data_distributer (customer_number, info_id) VALUES (?, ?)";
    private static final String INSERT_CONTACT_SQL = "INSERT into mdce.ref_data_info_contact (title, contact_name, work_phone) VALUES (?, ?, ?)";
    private static final String INSERT_MDCE_REF_DATA_SECONDARY_CONTACT = "INSERT into mdce.ref_data_secondary_contact_info (contact_id, info_id) VALUES (?, ?)";
    private static final String INSERT_CUSTOMER_NUMBERS = "INSERT into mdce.ref_data_customer_number (customer_number, info_id) VALUES (?, ?)";

    private static final String SELECT_CUSTOMER_NUMBERS = "SELECT customer_number from mdce.ref_data_customer_number WHERE info_id = ?";
    private static final String SELECT_DISTRIBUTER_ID = "SELECT info.parent_customer_number from mdce.ref_data_info as info, mdce.ref_data_utilities as util WHERE util.site_id = ? AND util.info_id = info.ref_data_info_id";

    private static final String PRIMARY_CONTACT_ID = "primary_contact_id";
    private static final String SITE_ID = "site_id";
    private static final String INFO_ID = "info_id";
    private static final String CONTACT_ID = "contact_id";
    private static final String REF_DATA_INFO_ID = "ref_data_info_id";

    private static final String SYSTEM_ID = "system_id";
    private static final String REF_DATA_INFO_CONTACT_ID = "ref_data_info_contact_id";

    private static final String CUSTOMER_NUMBER = "customer_number";
    private static final String PARENT_CUSTOMER_NUMBER = "parent_customer_number";

    private static final String ACCOUNT_NAME = "account_name";
    private static final String ACCOUNT_ID = "account_id";
    private static final String TITLE = "title";

    private static final String CONTACT_NAME = "contact_name";
    private static final String WORK_PHONE = "work_phone";
    private static final String ADDRESS_1 = "address1";
    private static final String ADDRESS_2 = "address2";
    private static final String ADDRESS_3 = "address3";
    private static final String CITY = "city";
    private static final String STATE = "state";

    private static final String POSTAL_CODE = "postal_code";
    private static final String COUNTRY = "country";

    private static final String MAIN_PHONE = "main_phone";
    private static final String FAX = "fax";
    private static final String SALES_TERRITORY_OWNER = "sales_territory_owner";

    private static final String REGIONAL_MANAGER = "regional_manager";
    private static final String ACCOUNT_MANAGER = "account_manager";
    private static final String STATUS = "status";
    private static final String TYPE = "type";

    private static final String SUB_TYPE = "sub_type";

    private static final String DELETE_FROM_MDCE = "DELETE FROM mdce.";
    private static final String REF_DATA_DISTRIBUTER = "ref_data_distributer";
    private static final String REF_DATA_UTILITIES = "ref_data_utilities";
    private static final String REF_DATA_SECONDARY_CONTACT_INFO = "ref_data_secondary_contact_info";
    private static final String REF_DATA_CUSTOMER_NUMBER = "ref_data_customer_number";
    private static final String REF_DATA_INFO = "ref_data_info";
    private static final String REF_DATA_INFO_CONTACT = "ref_data_info_contact";
    private static final String NULL = "null";
    private static final String NULL_NULL = "null, null";
    private static final String EMPTY_STRING = "";

    private static final String NETWORK = "network";
    private static final String LIFECYCLESTATE = "lifeCycleState";
    private static final String ICCID = "iccid";

    private static final String NETWORK_DB_COL = "sim_cellular_network";
    private static final String LIFECYCLESTATE_DB_COL = "state";
    private static final String ICCID_DB_COL = "iccid";

    final private JdbcTemplate db;

    final private SiteRepository siteRepository;
    final private KeyHolder keyHolder;

    @Autowired
    public JdbcRefDataRepository(JdbcTemplate db, SiteRepository siteRepository)
    {
        this.db = db;
        this.siteRepository = siteRepository;
        this.keyHolder = new GeneratedKeyHolder();
    }

    @Override
    @Transactional
    public int insertRefData(JsonReferenceData referenceData, RefDataWriteMode writeMode)
    {
        if (writeMode == RefDataWriteMode.ReplaceAll)
        {
            clearReferenceDataTables();
        }

        List<JsonReferenceDataUtility> utilities = insertUtilities(referenceData.getUtilities());

        List<JsonReferenceDataDistributer> distributers = insertDistributers(referenceData.getDistributers());

        return utilities.size() + distributers.size();
    }

    private void clearReferenceDataTables()
    {
        deleteTableContents(REF_DATA_DISTRIBUTER);
        deleteTableContents(REF_DATA_UTILITIES);
        deleteTableContents(REF_DATA_SECONDARY_CONTACT_INFO);
        deleteTableContents(REF_DATA_CUSTOMER_NUMBER);
        deleteTableContents(REF_DATA_INFO);
        deleteTableContents(REF_DATA_INFO_CONTACT);
    }

    private void deleteTableContents(String tableName)
    {
        this.db.update(connection -> {
            return connection.prepareStatement(DELETE_FROM_MDCE + tableName);
        });
    }

    private List<JsonReferenceDataDistributer> insertDistributers(List<JsonReferenceDataDistributer> distributers)
    {
        for (JsonReferenceDataDistributer distributer : distributers)
        {
            insertEachDistributer(distributer);
        }
        return distributers;
    }

    private int insertEachDistributer(JsonReferenceDataDistributer distributer)
    {
        JsonReferenceDataInfo info = distributer.getInfo();

        if (info == null)
        {
            return 0;
        }

        final int infoId = insertInfo(info);

        return insertDistributer(distributer, infoId);

    }

    private int insertDistributer(JsonReferenceDataDistributer distributer, int infoId)
    {
        return this.db.update(connection -> {
                    PreparedStatement ps = connection.prepareStatement(INSERT_DISTRIBUTER);
                    ps.setString(1, sanitiseInput(distributer.getCustomerNumber()));
                    ps.setInt(2, infoId);
                    return ps;
                }
        );
    }

    private List<JsonReferenceDataUtility> insertUtilities(List<JsonReferenceDataUtility> utilities)
    {
        for (JsonReferenceDataUtility utility : utilities)
        {
            insertEachUtility(utility);
        }
        return utilities;
    }

    private void insertEachUtility(JsonReferenceDataUtility utility)
    {
        JsonReferenceDataInfo info = utility.getInfo();

        if (info == null)
        {
            return;
        }

        final int infoId = insertInfo(info);

        insertUtility(utility, infoId);
    }

    private int insertInfo(JsonReferenceDataInfo info)
    {
        final Integer contactId = insertContactReturnId(info.getPrimaryContact());

        final int infoId = insertInfoReturnId(info, contactId);

        insertAnySecondaryContacts(infoId, info.getActiveSecondaryContacts());

        insertAnyCustomerNumbers(infoId, info.getCustomerNumbers());

        return infoId;
    }

    private void insertAnyCustomerNumbers(int infoId, List<String> customerNumbers)
    {
        for (String customerNumber : customerNumbers)
        {
            if (!sanitiseInput(customerNumber).equals(EMPTY_STRING))
            {
                insertCustomerNumber(infoId, customerNumber);
            }
        }
    }

    private void insertCustomerNumber(int infoId, String customerNumber)
    {
        this.db.update(connection -> {
                    PreparedStatement ps = connection.prepareStatement(INSERT_CUSTOMER_NUMBERS);
                    ps.setString(1, sanitiseInput(customerNumber));
                    ps.setInt(2, infoId);
                    return ps;
                }
        );
    }

    private void insertAnySecondaryContacts(int infoId, List<JsonReferenceDataInfoContact> activeSecondaryContacts)
    {
        for (JsonReferenceDataInfoContact secondaryContact : activeSecondaryContacts)
        {
            Integer contactId = insertContactReturnId(secondaryContact);
            if (contactId != null)
            {
                insertSecondaryContactIdIntoLinkTable(contactId, infoId);
            }
        }
    }

    private int insertSecondaryContactIdIntoLinkTable(int contactId, int infoId)
    {
        return this.db.update(connection -> {
                    PreparedStatement ps = connection.prepareStatement(INSERT_MDCE_REF_DATA_SECONDARY_CONTACT);
                    ps.setInt(1, contactId);
                    ps.setInt(2, infoId);
                    return ps;
                }
        );
    }

    private int insertUtility(JsonReferenceDataUtility jsonReferenceDataUtility, int infoId)
    {
        int siteId = jsonReferenceDataUtility.getSiteId();

        checkSiteIdExistsOrCreate(siteId, jsonReferenceDataUtility.getInfo().getAccountName());

        return insertUtilEntry(infoId, siteId);

    }

    private int insertUtilEntry(int infoId, int siteId)
    {
        return this.db.update(connection -> {
                    PreparedStatement ps = connection.prepareStatement(INSERT_UTILITY);
                    ps.setInt(1, siteId);
                    ps.setInt(2, infoId);
                    return ps;
                }
        );
    }

    private void checkSiteIdExistsOrCreate(int siteId, String accountName)
    {
        siteRepository.ensureSiteExists(new SiteId(siteId), accountName);
    }

    private int insertInfoReturnId(JsonReferenceDataInfo info, Integer contactId)
    {
        String insertInfoSql = "INSERT into mdce.ref_data_info (" +
                "system_id, account_id, account_name, " +
                "address1, address2, address3, " +
                "city, state, postal_code, " +
                "country, main_phone, fax, " +
                "sales_territory_owner, account_manager, regional_manager, " +
                "type, sub_type, status, " +
                "parent_customer_number, " +
                "customer_number " +
                (contactId == null ? EMPTY_STRING : ", primary_contact_id ") +
                ") VALUES (" +
                "?, ?, ?, " +
                "?, ?, ?, " +
                "?, ?, ?, " +
                "?, ?, ?, " +
                "?, ?, ?, " +
                "?, ?, ?, " +
                "?, " +
                (contactId == null ? EMPTY_STRING : "?, ") +
                " ?)";
        this.db.update(connection -> {
                    PreparedStatement ps =
                            connection.prepareStatement(insertInfoSql, new String[]{REF_DATA_INFO_ID});
                    return prepareStatementWithInfoDetails(info, contactId, ps);
                },
                keyHolder);
        return keyHolder.getKey().intValue();
    }

    private PreparedStatement prepareStatementWithInfoDetails(JsonReferenceDataInfo info, Integer contactId, PreparedStatement ps) throws SQLException
    {
        ps.setString(1, sanitiseInput(info.getSystemId()));
        ps.setString(2, sanitiseInput(info.getAccountId()));
        ps.setString(3, sanitiseInput(info.getAccountName()));

        ps.setString(4, sanitiseInput(info.getAddress1()));
        ps.setString(5, sanitiseInput(info.getAddress2()));
        ps.setString(6, sanitiseInput(info.getAddress3()));

        ps.setString(7, sanitiseInput(info.getCity()));
        ps.setString(8, sanitiseInput(info.getState()));
        ps.setString(9, sanitiseInput(info.getPostalCode()));
        ps.setString(10, sanitiseInput(info.getCountry()));

        ps.setString(11, sanitiseInput(info.getMainPhone()));
        ps.setString(12, sanitiseInput(info.getFax()));

        ps.setString(13, sanitiseInput(info.getSalesTerritoryOwner()));
        ps.setString(14, sanitiseInput(info.getAccountManager()));
        ps.setString(15, sanitiseInput(info.getRegionalManager()));

        ps.setString(16, sanitiseInput(info.getType()));
        ps.setString(17, sanitiseInput(info.getSubType()));
        ps.setString(18, sanitiseInput(info.getStatus()));
        ps.setString(19, sanitiseInput(info.getParentCustomerNumber()));
        ps.setString(20, sanitiseInput(info.getCustomerNumber()));

        if (contactId != null)
        {
            ps.setInt(21, contactId);
        }

        return ps;
    }

    private String sanitiseInput(String stringToSanitise)
    {
        if (stringToSanitise == null || stringToSanitise.equalsIgnoreCase(NULL) || stringToSanitise.equalsIgnoreCase(NULL_NULL))
        {
            return EMPTY_STRING;
        }
        return stringToSanitise;
    }

    private Integer insertContactReturnId(JsonReferenceDataInfoContact contact)
    {

        if (checkContactIsPopulated(contact))
        {
            return null;
        }

        this.db.update(connection -> {
                    PreparedStatement ps =
                            connection.prepareStatement(INSERT_CONTACT_SQL, new String[]{REF_DATA_INFO_CONTACT_ID});
                    ps.setString(1, sanitiseInput(contact.getTitle()));
                    ps.setString(2, sanitiseInput(contact.getName()));
                    ps.setString(3, sanitiseInput(contact.getWorkPhone()));
                    return ps;
                },
                keyHolder);

        return keyHolder.getKey().intValue();
    }

    private boolean checkContactIsPopulated(JsonReferenceDataInfoContact contact)
    {
        return (sanitiseInput(contact.getTitle()).equals(EMPTY_STRING)
                && sanitiseInput(contact.getName()).equals(EMPTY_STRING)
                && sanitiseInput(contact.getWorkPhone()).equals(EMPTY_STRING));
    }

    @Override
    public MdceReferenceData getRefData()
    {
        try
        {
            MdceReferenceData mdceReferenceData = new MdceReferenceData();

            List<MdceReferenceDataUtility> utilities = getUtilities();

            mdceReferenceData.setUtilities(utilities);

            List<MdceReferenceDataDistributer> distributers = getDistributers();

            mdceReferenceData.setDistributers(distributers);

            return mdceReferenceData;

        } catch (Exception e)
        {
            throw new MdceRestException("Exception thrown retrieving RefData " + e.getClass().getSimpleName() + " " + e.getMessage(), e);
        }
    }

    /**
     * Get list of names of the regional managers
     */
    @Override
    public List<String> getRegionalManagers()
    {
        String sql = "SELECT DISTINCT regional_manager FROM mdce.ref_data_info " +
                "ORDER BY regional_manager ASC";

        return this.db.query(sql, this::regionalManagerMapper);
    }

    /**
     * Get a list of all states that are listed in ref_data_info table
     */
    @Override
    public List<String> getStates()
    {
        String sql = "SELECT DISTINCT state FROM mdce.ref_data_info " +
                "ORDER BY state ASC";

        return this.db.query(sql, this::stateMapper);
    }

    /**
     * Get a list of possible MIU type names
     */
    @Override
    public List<String> getMiuTypes()
    {
        List<String> miuTypes = new ArrayList<String>();

        for (MiuType miuType : MiuType.values())
        {
            miuTypes.add(miuType.toString());
        }

        return miuTypes;
    }

    @Override
    public List<SiteDetails> getSitesForRegionalManager(String regionalManager)
    {
        String sql = "SELECT site_list.site_id, site_list.name FROM mdce.site_list INNER JOIN " +
                "(SELECT site_id FROM mdce.ref_data_utilities INNER JOIN " +
                "(SELECT ref_data_info_id FROM mdce.ref_data_info WHERE regional_manager=?) AS table1 " +
                "ON table1.ref_data_info_id=ref_data_utilities.info_id) AS table2 " +
                "ON table2.site_id = site_list.site_id ORDER BY site_list.name ASC;";

        return this.db.query(sql, this::siteMapper, regionalManager);
    }

    @Override
    public List<SiteDetails> getSitesForState(String state)
    {
        String sql = "SELECT site_list.site_id, site_list.name FROM mdce.site_list INNER JOIN " +
                "(SELECT site_id FROM mdce.ref_data_utilities INNER JOIN " +
                "(SELECT ref_data_info_id FROM mdce.ref_data_info WHERE state=?) AS table1 " +
                "ON table1.ref_data_info_id=ref_data_utilities.info_id) AS table2 " +
                "ON table2.site_id = site_list.site_id ORDER BY site_list.name ASC;";

        return this.db.query(sql, this::siteMapper, state);
    }

    private String regionalManagerMapper(ResultSet rs, int rownumber) throws SQLException
    {
        return rs.getString("regional_manager");
    }

    private String stateMapper(ResultSet rs, int rownumber) throws SQLException
    {
        return rs.getString("state");
    }
    private SiteDetails siteMapper(ResultSet rs, int rownumber) throws SQLException
    {
        SiteDetails siteDetails = new SiteDetails();
        siteDetails.setSiteId(rs.getInt("site_id"));
        siteDetails.setSiteName(rs.getString("name"));

        return siteDetails;
    }

    @Override
    public List<String> getSimNetworks(Map<String,String> options)
    {
        SqlBuilder sql = new SqlBuilder("SELECT DISTINCT sim_cellular_network FROM mdce.sim_details");

        Map<String,String> criteria = new HashMap<>();

        criteria.put(LIFECYCLESTATE, LIFECYCLESTATE_DB_COL);
        criteria.put(ICCID, ICCID_DB_COL);

        sql.addCriteriaIfExists(SqlBuilder.decodeNullValues(options), criteria);

        sql.append(" ORDER BY sim_cellular_network ASC");

        return this.db.query(sql.getSQL(), (ResultSet rs, int rowNumber) -> { return rs.getString("sim_cellular_network"); },
                (Object[])sql.getArguments());
    }

    @Override
    public List<String> getSimLifeCycleStates(Map<String,String> options)
    {
        SqlBuilder sql = new SqlBuilder("SELECT DISTINCT state FROM mdce.sim_details");

        Map<String,String> criteria = new HashMap<>();

        criteria.put(NETWORK, NETWORK_DB_COL);
        criteria.put(ICCID, ICCID_DB_COL);

        sql.addCriteriaIfExists(SqlBuilder.decodeNullValues(options), criteria);

        return this.db.query(sql.getSQL(), (ResultSet rs, int rowNumber) -> { return rs.getString("state"); },
                (Object[])sql.getArguments());
    }

    @Override
    public Optional<String> lookupDistributerIdFromSiteId(SiteId siteId)
    {
        List<Optional<String>> result = this.db.query(SELECT_DISTRIBUTER_ID, new Object[]{siteId.numericValue()}, (rs, rowNum) -> {
            return Optional.ofNullable(rs.getString(PARENT_CUSTOMER_NUMBER));
        });
        if (result.isEmpty())
        {
            return Optional.empty();
        }
        if (result.size() == 1)
        {
            return result.get(0);
        }
        throw new MdceRestException("Error: multiple results (" + result.size() + ") returned when single result required");
    }

    private List<MdceReferenceDataDistributer> getDistributers()
    {
        List<MdceReferenceDataDistributer> distributers = getMdceReferenceDataDistributers();
        for (MdceReferenceDataDistributer distributer : distributers)
        {
            MdceReferenceDataInfo info = getPopulatedInfo(distributer.getInfoId());

            distributer.setInfo(info);
        }
        return distributers;
    }

    private List<MdceReferenceDataUtility> getUtilities() throws SQLException
    {
        List<MdceReferenceDataUtility> utilities = getMdceReferenceDataUtilities();
        for (MdceReferenceDataUtility utility : utilities)
        {
            MdceReferenceDataInfo info = getPopulatedInfo(utility.getInfoId());

            utility.setInfo(info);
        }
        return utilities;
    }

    private MdceReferenceDataInfo getPopulatedInfo(final int infoId)
    {
        MdceReferenceDataInfo info = getReferenceDataInfoFromId(infoId);

        int primaryContactId = info.getPrimaryContactId();

        info.setPrimaryContact(getContactFromId(primaryContactId));

        info.setActiveSecondaryContacts(getSecondaryContacts(infoId));

        info.setCustomerNumbers(getCustomerNumbers(infoId));

        return info;
    }

    private List<String> getCustomerNumbers(final int infoId)
    {
        return this.db.query(SELECT_CUSTOMER_NUMBERS, (rs, rownum) -> getCustomerNumber(rs), infoId);
    }

    private String getCustomerNumber(ResultSet rs) throws SQLException
    {
        return rs.getString(CUSTOMER_NUMBER);
    }

    private List<MdceReferenceDataInfoContact> getSecondaryContacts(int infoId)
    {
        List<Integer> ids = getSecondaryContactsFromId(infoId);

        return getSecondaryContacts(ids);
    }

    private List<MdceReferenceDataInfoContact> getSecondaryContacts(List<Integer> ids)
    {
        List<MdceReferenceDataInfoContact> contacts = new ArrayList<>();
        for (Integer id : ids)
        {
            contacts.add(getContactFromId(id));
        }
        return contacts;
    }

    private List<Integer> getSecondaryContactsFromId(final int infoId)
    {
        String selectInfos = "SELECT contact_id from mdce.ref_data_secondary_contact_info WHERE info_id = ?";
        return this.db.query(selectInfos, (rs, rownum) -> mapContactIdResult(rs),
                infoId
        );
    }

    private MdceReferenceDataInfoContact getContactFromId(final int contactId)
    {
        String selectInfos = "SELECT * from mdce.ref_data_info_contact WHERE ref_data_info_contact_id = ?";
        List<MdceReferenceDataInfoContact> result = this.db.query(selectInfos, new Object[]{contactId}, (rs, rowNum) -> {
            return getMdceReferenceDataInfoContact(rs);
        });
        if (result.isEmpty())
        {
            return new MdceReferenceDataInfoContact();
        }
        if (result.size() == 1)
        {
            return result.get(0);
        }
        throw new MdceRestException("Error: multiple results returned when single result required");
    }

    private MdceReferenceDataInfoContact getMdceReferenceDataInfoContact(ResultSet rs) throws SQLException
    {
        final MdceReferenceDataInfoContact referenceData = new MdceReferenceDataInfoContact();

        referenceData.setContactName(rs.getString(CONTACT_NAME));
        referenceData.setTitle(rs.getString(TITLE));
        referenceData.setWorkPhone(rs.getString(WORK_PHONE));

        return referenceData;
    }

    private MdceReferenceDataInfo getReferenceDataInfoFromId(final int infoId)
    {
        String selectInfos = "SELECT * from mdce.ref_data_info WHERE ref_data_info_id = ?";
        return this.db.queryForObject(selectInfos, new Object[]{infoId}, (rs, i) -> {
            return populateInfoFromResultSet(rs);
        });
    }

    private MdceReferenceDataInfo populateInfoFromResultSet(ResultSet rs) throws SQLException
    {
        MdceReferenceDataInfo referenceData = new MdceReferenceDataInfo();

        referenceData.setAccountName(rs.getString(ACCOUNT_NAME));
        referenceData.setAccountId(rs.getString(ACCOUNT_ID));
        referenceData.setSystemId(rs.getString(SYSTEM_ID));

        referenceData.setCustomerNumber(rs.getString(CUSTOMER_NUMBER));

        referenceData.setMainPhone(rs.getString(MAIN_PHONE));
        referenceData.setFax(rs.getString(FAX));

        referenceData.setAddress1(rs.getString(ADDRESS_1));
        referenceData.setAddress2(rs.getString(ADDRESS_2));
        referenceData.setAddress3(rs.getString(ADDRESS_3));
        referenceData.setCity(rs.getString(CITY));
        referenceData.setState(rs.getString(STATE));
        referenceData.setPostalCode(rs.getString(POSTAL_CODE));
        referenceData.setCountry(rs.getString(COUNTRY));

        referenceData.setSalesTerritoryOwner(rs.getString(SALES_TERRITORY_OWNER));
        referenceData.setRegionalManager(rs.getString(REGIONAL_MANAGER));
        referenceData.setAccountManager(rs.getString(ACCOUNT_MANAGER));

        referenceData.setStatus(rs.getString(STATUS));

        referenceData.setType(rs.getString(TYPE));
        referenceData.setSubType(rs.getString(SUB_TYPE));

        referenceData.setPrimaryContactId(rs.getInt(PRIMARY_CONTACT_ID));
        referenceData.setParentCustomerNumber(rs.getString(PARENT_CUSTOMER_NUMBER));

        return referenceData;
    }

    private List<MdceReferenceDataUtility> getMdceReferenceDataUtilities() throws SQLException
    {
        final String selectUtilities = "SELECT * FROM mdce.ref_data_utilities";
        return this.db.query(selectUtilities, (rs, rownum) ->
                        mapUtilitiesResult(rs)
        );
    }

    private MdceReferenceDataUtility mapUtilitiesResult(ResultSet rs) throws SQLException
    {
        MdceReferenceDataUtility utility = new MdceReferenceDataUtility();

        utility.setSiteId(rs.getInt(SITE_ID));
        utility.setInfoId(rs.getInt(INFO_ID));

        return utility;
    }

    private MdceReferenceDataDistributer mapDistributersResult(ResultSet rs) throws SQLException
    {
        MdceReferenceDataDistributer utility = new MdceReferenceDataDistributer();

        utility.setCustomerNumber(rs.getString(CUSTOMER_NUMBER));
        utility.setInfoId(rs.getInt(INFO_ID));

        return utility;
    }

    private Integer mapContactIdResult(ResultSet rs) throws SQLException
    {
        return rs.getInt(CONTACT_ID);

    }

    public List<MdceReferenceDataDistributer> getMdceReferenceDataDistributers()
    {
        final String query = "SELECT * FROM mdce.ref_data_distributer";
        return this.db.query(
                query,
                (rs, rownum) -> mapDistributersResult(rs)
        );
    }

    @Override
    public MdceReferenceDataUtility getSiteReferenceData(SiteId siteId)
    {
        final String query = "SELECT * FROM mdce.ref_data_utilities WHERE site_id=?";
        try
        {
            return this.db.queryForObject(
                    query,
                    (rs, rownum) -> mapUtilitiesResult(rs),
                    siteId.numericWrapperValue()
            );
        }
        catch (EmptyResultDataAccessException e)
        {
            return null;
        }

    }

    @Override
    public MdceReferenceDataDistributer getDistributerReferenceData(String distributerId)
    {
        final String query = "SELECT * FROM mdce.ref_data_distributer WHERE customer_number=?";
        try
        {
            return this.db.queryForObject(
                    query,
                    (rs, rownum) -> mapDistributersResult(rs),
                    distributerId
            );
        }
        catch (EmptyResultDataAccessException e)
        {
            return null;
        }
    }

}
