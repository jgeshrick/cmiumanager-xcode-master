/*
 *  Neptune Technology Group
 *  Copyright 2017 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.mdce.integration.data.miu.MiuOwnerHistory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The JSON container for MIU owner history.
 */
public class JsonMiuOwnerHistoryCollection
{
    @JsonProperty("owner_history")
    private List<JsonMiuOwnerHistory> ownerHistory;

    /**
     * Constructs a new JsonMiuOwnerHistoryCollection using the supplied list.
     * @param historyList The internal MIU owner history list.
     */
    public JsonMiuOwnerHistoryCollection(List<MiuOwnerHistory> historyList)
    {
        ownerHistory = historyList
                .stream()
                .map(h -> new JsonMiuOwnerHistory(
                        h.getMiuOwnerHistoryId(),
                        h.getMiuId().numericValue(),
                        h.getSiteId().numericValue(),
                        h.getTimestamp()))
                .collect(Collectors.toList());
    }

    public List<JsonMiuOwnerHistory> getOwnerHistory()
    {
        return ownerHistory;
    }

    public void setOwnerHistory(List<JsonMiuOwnerHistory> ownerHistory)
    {
        this.ownerHistory = ownerHistory;
    }
}
