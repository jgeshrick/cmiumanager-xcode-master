/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;


import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.command.CommandLifeCycle;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.common.data.miu.command.MiuCommandDetail;
import com.neptunetg.common.data.miu.command.MiuCommandRepository;
import com.neptunetg.common.data.miu.lora.JdbcLoraRepository;
import com.neptunetg.common.data.miu.lora.LoraDeviceDetail;
import com.neptunetg.common.data.miu.lora.LoraNetwork;
import com.neptunetg.mdce.common.data.PacketRepository;
import com.neptunetg.mdce.common.data.model.DynamoItemPacketType;
import com.neptunetg.mdce.common.data.model.MiuPacketSentDynamoItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * For publishing and retrieving L900 commands to/from the provider.
 * The class constantly polls the RDS database to check whether commands need to be published.
 */
@Service
public class L900CommandMonitor implements AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(L900CommandMonitor.class);

    private final MiuCommandRepository miuCommandRepository;

    private final PacketRepository packetRepo;

    private final MdceIpcSubscribeService awsQueueService;

    private final SenetDownlinkService senetDownlinkService;

    private final ActilityDownlinkService actilityDownlinkService;

    private final JdbcLoraRepository loraRepository;


    @Autowired
    public L900CommandMonitor(
            MdceIpcSubscribeService awsQueueService,
            MiuCommandRepository miuCommandRepo,
            PacketRepository packetRepo,
            SenetDownlinkService senetDownlinkService,
            JdbcLoraRepository loraRepository,
            ActilityDownlinkService actilityDownlinkService)
    {
        this.miuCommandRepository = miuCommandRepo;
        this.packetRepo = packetRepo;
        this.awsQueueService = awsQueueService;
        this.senetDownlinkService = senetDownlinkService;
        this.loraRepository = loraRepository;
        this.actilityDownlinkService = actilityDownlinkService;

        //create a send command observer to listen to AWS SQS for send command message sent from mdce-integration
        awsQueueService.register(this, MdceIpcSubscribeService.LORA_PACKETS_TO_SEND, Duration.ofSeconds(1L), 10);
    }

    /**
     * Publish a MIU configuration message to the downlink URL
     * @throws
     */
    public void publishCommand(MiuCommand command) throws IOException
    {
        byte[] dataToSend = command.getCommandParams();

        if(dataToSend != null)
        {
            log.info("Sending to L900: {}, content:{}", command.getMiuId().numericValue(), dataToSend);
            LoraDeviceDetail loraDeviceDetail = loraRepository.getLoraDeviceDetailByMiuId(command.getMiuId());

            if(loraDeviceDetail != null && loraDeviceDetail.getLoraNetwork() == LoraNetwork.SENET)
            {
                senetDownlinkService.sendMessageToSenetDevice(loraDeviceDetail.getEui(),
                        command.getCommandParams());
            }
            else if(loraDeviceDetail != null && loraDeviceDetail.getLoraNetwork() == LoraNetwork.ACTILITY)
            {
                actilityDownlinkService.sendMessageToActilityDevice(loraDeviceDetail.getEui(),
                        command.getCommandParams());
            }

            //record to dynamoDb
            try
            {
                final Instant now = Instant.now();

                final MiuPacketSentDynamoItem miuPacket = new MiuPacketSentDynamoItem();
                miuPacket.setMiuId(command.getMiuId().numericValue());
                miuPacket.setPacketData(command.getCommandParams());
                miuPacket.setDateBuilt(now.toEpochMilli());
                miuPacket.setInsertDate(now.toEpochMilli());
                miuPacket.setSourceDeviceType("MDCE");
                miuPacket.setTargetDeviceType(MiuType.L900.toString());
                miuPacket.setPacketType(DynamoItemPacketType.L900_APP_COMMAND_PACKET_SENET.toString());

                packetRepo.insertMiuPacketSent(miuPacket);
            }
            catch (Exception e)
            {
                log.error("Cannot insert packet received for " + command.getMiuId(), e);
            }
        }
    }

    /**
     * Check whether there is any new commands to need to be published to an L900 device
     */
    public boolean checkAndPublishNewCommandTask(AtomicBoolean cancellationToken)
    {
        log.trace("checkAndPublishNewCommandTask for L900...");

        // get miu commands that are created and waiting to be published to Senet
        final List<MiuCommandDetail> publishCommandList = miuCommandRepository.getMiuCommandsByState(CommandLifeCycle.CREATED, MiuType.L900);

        for (MiuCommand c : publishCommandList)
        {
            if (cancellationToken.get())
            {
                return false;
            }
            try
            {
                this.publishCommand(c);

                //if publish is successful, update state of the command from CREATED to QUEUED
                miuCommandRepository.updateMiuCommandsState(c.getCommandId(), CommandLifeCycle.QUEUED);
            }
            catch (IOException e)
            {
                log.error("Error occured while trying to publish command for MIU ID" + c.getMiuId(), e);
            }
        }
        return true;
    }


    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        log.trace("SQS message received: {} - {}", messageId, message);
        return this.checkAndPublishNewCommandTask(cancellationToken);
    }
}
