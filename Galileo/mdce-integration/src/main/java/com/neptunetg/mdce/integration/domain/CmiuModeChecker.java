/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.mode.MiuMode;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.integration.domain.alert.AlertEmail;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import com.neptunetg.mdce.integration.scheduled.cmiumode.CmiuModeCheckerAlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by dcloud on 24/05/2017.
 * A class to check CMIU modes to ensure what it's reporting is correct
 */
@Service
public class CmiuModeChecker implements AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(CmiuModeChecker.class);

    private final CmiuModeCheckerAlertService miuModeAlertService;

    private final AlertService alertService;

    @Autowired
    public CmiuModeChecker(MdceIpcSubscribeService awsQueueService, CmiuModeCheckerAlertService miuModeAlertService, AlertService alertService)
    {
        this.miuModeAlertService = miuModeAlertService;
        this.alertService = alertService;
        awsQueueService.register(this, MdceIpcSubscribeService.CHECK_CMIU_MODE, Duration.ofSeconds(15L), 1);
    }

    /**
     * Handler when SQS message to the registered SQS queue name has been received.
     * @param message the message body
     * @param messageId the message id
     * @param messageReceipt message receipt
     * @return true to delete the message from SQS
     */
    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        log.debug("SQS message received to check for Current CMIU Mode");

        try
        {
            String[] receivedValues = message.split(",");
            if( receivedValues.length == 3 )
            {
                // 0 = cmiu, 1 = recordingInterval (in minutes), 2 = reportingInterval (in hours)
                raiseAlertForNonOfficialCmiuMode((MiuId.fromString(receivedValues[0])), receivedValues[1], receivedValues[2]);

                return true;
            }
            else
            {
                log.error("Failed to extract arguments from message for Cmiu Mode Alert.");
                return false;
            }
        }
        catch (InternalApiException e)
        {
            log.error("Error checking for, or raising CMIU Mode alerts",e);
        }

        return true;
    }

    private void raiseAlertForNonOfficialCmiuMode(MiuId miuId, String recordingInt, String reportingInt) throws InternalApiException
    {
        MiuMode reportedMode = new MiuMode();
        reportedMode.setRecordingInteval(Integer.parseInt(recordingInt));
        reportedMode.setReportingInterval(Integer.parseInt(reportingInt));
        final MiuMode currentKnownModeInfo = miuModeAlertService.checkForOfficialMode(miuId, reportedMode);

        if(!currentKnownModeInfo.isOfficialMode())
        {
            final AlertEmail email = alertService.initAlertEmail();

            final AlertSource alertSource = AlertSource.cmiuReportsNonOfficialMode(miuId.numericValue());

            AlertDetails alertDetails = new AlertDetails();
            alertDetails.setAlertLevel(AlertLevel.WARNING);
            alertDetails.setAlertState(AlertState.NEW);
            alertDetails.setAlertSource(alertSource.getAlertId());
            alertDetails.setRecentMessage("CMIU "
                    + miuId
                    + " is reporting a non-official mode: "
                    + currentKnownModeInfo.getReportedMode()
                    + ".");
            alertDetails.setAlertCreationDate(ZonedDateTime.now());
            alertService.addAlert(alertDetails, email);

            alertService.sendEmail(email);
        }
    }

}
