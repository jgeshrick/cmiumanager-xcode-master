/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.data.audit;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.cellular.CmiuConfigRepository;
import com.neptunetg.common.data.miu.cellular.ConfigSet;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.mdce.common.internal.audit.AuditEventType;
import com.neptunetg.mdce.common.internal.audit.model.AuditLog;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest;
import com.neptunetg.mdce.common.internal.site.model.SiteForUser;
import com.neptunetg.mdce.common.internal.user.model.ModifiedUserDetails;
import com.neptunetg.mdce.common.internal.user.model.NewUserDetails;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.data.user.UserRepository;
import com.neptunetg.mdce.integration.utility.CommandDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.neptunetg.common.data.util.JdbcDateUtil.getInstant;

/**
 * Handle audit logs for JDBC
 */
@Repository
public class JdbcAuditRepository implements AuditRepository
{
    public static final int MAX_RESULTS = 100;
    private static final String AUDIT_LOG = " FROM mdce.audit_list WHERE audit_time > DATE_SUB(NOW(), INTERVAL ? DAY ) ";
    private static final String AND_MIU_ID = " AND miu_id = ? ";
    private static final String AND_AUDIT_USER_NAME = " AND audit_user_name = ? ";
    private static final String ORDER_BY_AUDIT_TIME_DESC = " ORDER BY audit_time DESC";
    private static final int MAX_STRING_LENGTH = 1024; //this is the size of the varchar(1024) in audit_list.old_values and audit_list.new_values


    private final UserRepository userRepository;
    private final JdbcTemplate db;
    private final CommandDescription commandDescription;

    @Autowired
    public JdbcAuditRepository(JdbcTemplate db, UserRepository userRepository, CommandDescription commandDescription)
    {
        this.db = db;
        this.userRepository = userRepository;
        this.commandDescription = commandDescription;
    }


    /**
     * Ensure String length is not longer than requested size.
     *
     * @param value  the string to be truncated.  May be null
     * @param length max allowable length of the string, the rest will be truncated off.
     * @return the truncated string
     */
    private static String truncate(String value, int length)
    {
        if (value == null)
        {
            return null;
        }
        else if (value.length() > length)
        {
            return value.substring(0, length);
        }
        else
        {
            return value;
        }
    }

    private static AuditLog mapAuditLogRow(ResultSet rs, int rowNum) throws SQLException
    {
        AuditLog auditLog = new AuditLog();
        auditLog.setDateTime(getInstant(rs, "audit_time"));
        auditLog.setMiuId(rs.getLong("miu_id"));
        auditLog.setUserName(rs.getString("audit_user_name"));

        AuditEventType auditEventType = AuditEventType.fromId(rs.getInt("audit_type"));

        auditLog.setAuditType(auditEventType.getName());

        auditLog.setOldValue(rs.getString("old_values"));
        auditLog.setNewValue(rs.getString("new_values"));

        return auditLog;
    }

    @Override
    public void logMiuOwnershipChanged(MiuId miuId, SiteId oldSiteId, SiteId newSiteId, String requester)
    {
        addAuditEntry(AuditEventType.AUDIT_EVENT_TYPE_MIU_SET_OWNERSHIP,
                miuId,
                SiteId.getStringValueAllowNull(oldSiteId),
                SiteId.getStringValueAllowNull(newSiteId),
                requester);
    }

    @Override
    public void logRefusedMiuOwnershipChanged(MiuId miuId, SiteId newSiteId, SiteId currentMiuOwner)
    {
        addAuditEntry(AuditEventType.AUDIT_EVENT_TYPE_MIU_SET_OWNERSHIP_REFUSED,
                miuId,
                SiteId.getStringValueAllowNull(currentMiuOwner),
                SiteId.getStringValueAllowNull(newSiteId));
    }


    @Override
    public void logMiuCommand(MiuCommand miuCommand)
    {
        String desc = commandDescription.getCommandDetails(miuCommand);
        String commandType;

        if(MiuType.L900.equals(miuCommand.getTargetDeviceType()))
        {
            commandType = CommandRequest.CommandTypeL900.fromCommandNumber(miuCommand.getCommandType()).getCommandDescription();
        }
        else
        {
            commandType = CommandRequest.CommandTypeCmiu.fromCommandNumber(miuCommand.getCommandType()).getCommandDescription();
        }

        addAuditEntry(AuditEventType.AUDIT_EVENT_TYPE_MIU_SEND_COMMAND,
                miuCommand.getMiuId(),
                "-",
                commandType + ";" + desc,
                miuCommand.getUserName());
    }

    @Override
    public void logUserAccountChange(ModifiedUserDetails modifiedUserDetails, String operationDescription)
    {
        UserDetails userDetails = userRepository.getUserDetailsById(modifiedUserDetails.getUserId());

        addAuditEntry(AuditEventType.AUDIT_EVENT_TYPE_USER_EDIT,
                null,
                userDetails.toString(),
                "User name: " + userDetails.getUserName() + "; Operation:" + operationDescription,
                modifiedUserDetails.getRequester());
    }

    @Override
    public void logUserRemoved(ModifiedUserDetails modifiedUserDetails, UserDetails deletedUserDetails, String operationDescription)
    {
        addAuditEntry(AuditEventType.AUDIT_EVENT_TYPE_USER_REMOVED,
                null,
                deletedUserDetails.toString(),
                "User name: " + deletedUserDetails.getUserName() + "; Operation:" + operationDescription,
                modifiedUserDetails.getRequester());
    }

    @Override
    public void logUserSiteChange(SiteForUser siteForUser, String operationDescription)
    {
        UserDetails userDetails = userRepository.getUserDetailsById(siteForUser.getUserId());

        addAuditEntry(AuditEventType.AUDIT_EVENT_TYPE_USER_EDIT,
                null,
                userDetails.toString(),
                "User name: " + userDetails.getUserName() + "; Operation:" + operationDescription,
                siteForUser.getRequester());
    }



    @Override
    public void logNewUser(NewUserDetails userDetails)
    {
        addAuditEntry(AuditEventType.AUDIT_EVENT_TYPE_USER_CREATED,
                null, "", "user: " + userDetails.getUserName() + ", access: " + userDetails.getUserLevel(), userDetails.getRequester());
    }


    @Override
    public void logSettingChanged(String key, String oldVal, String newVal, String requester)
    {
        final String oldValToLog;
        if (StringUtils.hasText(oldVal))
        {
            oldValToLog = key + " = " + oldVal;
        }
        else
        {
            oldValToLog = null;
        }

        addAuditEntry(AuditEventType.AUDIT_EVENT_TYPE_MDCE_SETTING_CHANGE,
                null, oldValToLog, key + " = " + newVal, requester);

    }

    /**
     * Retrieve audit log based on filters
     * @param itemId if defined, retrieve audit log associated with the itemId (MIU ID or SIM ID)
     * @param userName if defined, retrieve audit log associate with the user
     * @param daysAgo if defined, retrieve audit logs up to the number of days.
     * @return audit log list.
     */
    @Override
    public List<AuditLog> getAuditLog(Long itemId, String userName, int daysAgo, Integer page)
    {
        List<Object> args = new ArrayList<>();

        //Get rows with audit_time later (greater) than the specific number of days ago.
        String sql = "SELECT * " + getAuditLogSql(itemId, userName, daysAgo, args);

        sql += ORDER_BY_AUDIT_TIME_DESC;

        if (page != null)
        {
            sql += " LIMIT ?, " + MAX_RESULTS;
            args.add(page * MAX_RESULTS);
        }

        List<AuditLog> result = this.db.query(sql, args.toArray(), JdbcAuditRepository::mapAuditLogRow);

        return result;
    }

    @Override
    public Integer getAuditLogCount(Long itemId, String userName, int daysAgo)
    {
        List<Object> args = new ArrayList<>();

        //Get rows with audit_time later (greater) than the specific number of days ago.
        String sql = "SELECT COUNT(*)" + getAuditLogSql(itemId, userName, daysAgo, args);

        return this.db.queryForObject(sql, args.toArray(), Integer.class);
    }

    @Override
    public void logVerifyUserFailed(String userName, String description)
    {
        addAuditEntry(AuditEventType.AUDIT_EVENT_TYPE_VERIFY_USER_FAILED,
                null,
                null,
                description,
                userName);
    }

    private String getAuditLogSql(Long itemId, String userName, int daysAgo, List<Object> args)
    {
        String sql = AUDIT_LOG;
        args.add(daysAgo);

        if (itemId != null)
        {
            sql += AND_MIU_ID;
            args.add(itemId);
        }

        if (userName != null && !userName.isEmpty())
        {
            sql += AND_AUDIT_USER_NAME;
            args.add(userName);
        }
        return sql;
    }

    private void addAuditEntry(AuditEventType eventType, MiuId miuId, String oldValue, String newValue)
    {
        final String insertAuditSql = "INSERT INTO mdce.audit_list " +
                "(audit_time, audit_type, miu_id, old_values, new_values) VALUES (NOW(),?,?,?,?)";

        Integer miuIdParameterValue = miuId == null ? null : miuId.numericValue();

        this.db.update(insertAuditSql, eventType.getId(), miuIdParameterValue,
                truncate(oldValue, MAX_STRING_LENGTH), truncate(newValue, MAX_STRING_LENGTH));

    }

    private void addAuditEntry(AuditEventType eventType, MiuId miuId, String oldValue, String newValue, String userName)
    {
        final String insertAuditSql = "INSERT INTO mdce.audit_list " +
                "(audit_time, audit_type, miu_id, old_values, new_values, audit_user_name) VALUES (NOW(),?,?,?,?,?)";

        Integer miuIdParameterValue = miuId == null ? null : miuId.numericValue();

        this.db.update(insertAuditSql, eventType.getId(), miuIdParameterValue,
                truncate(oldValue, MAX_STRING_LENGTH), truncate(newValue, MAX_STRING_LENGTH), userName);

    }

    @Override
    public void logAccessDenied(String userName, String description)
    {
        final String insertAuditSql = "INSERT INTO mdce.audit_list " +
                "(audit_time, audit_type, miu_id, old_values, new_values, audit_user_name) VALUES (NOW(),?,?,?,?,?)";

        this.db.update(insertAuditSql, AuditEventType.AUDIT_EVENT_TYPE_ACCESS_DENIED.getId(),
                null,
                null,
                description,
                userName);

    }
}
