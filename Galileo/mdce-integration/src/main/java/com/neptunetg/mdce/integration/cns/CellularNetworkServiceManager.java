/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.cns;

import com.neptunetg.common.cns.CellularNetworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Provides access to CNS instances for each supported network
 */
@Service
public class CellularNetworkServiceManager
{
    public static final String ATT = "ATT";
    public static final String VZW = "VZW";

    @Autowired
    @Qualifier("vzwCns")
    private CellularNetworkService vzwCns;

    @Autowired
    @Qualifier("attCns")
    private CellularNetworkService attCns;

    public CellularNetworkService getCns(String networkIdentifier)
    {
        if (ATT.equalsIgnoreCase(networkIdentifier))
        {
            return attCns;
        }
        else if (VZW.equalsIgnoreCase(networkIdentifier))
        {
            return vzwCns;
        }
        else
        {
            throw new IllegalArgumentException("Unknown network operator: " + networkIdentifier);
        }
    }

}
