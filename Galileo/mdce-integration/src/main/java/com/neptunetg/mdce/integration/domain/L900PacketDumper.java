/*
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 */

package com.neptunetg.mdce.integration.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lora.LoraDeviceDetail;
import com.neptunetg.common.data.miu.lora.LoraNetwork;
import com.neptunetg.common.data.miu.lora.LoraRepository;
import com.neptunetg.common.lora.json.ActilityForwardPacketJson;
import com.neptunetg.common.lora.json.ActilityUplinkJson;
import com.neptunetg.common.lora.json.SenetForwardPacketJson;
import com.neptunetg.common.lora.pdu.*;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletResponse;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Writes packets to output stream.  Not thread-safe
 */
public class L900PacketDumper implements MiuDumper
{
    private static final Logger logger = LoggerFactory.getLogger(CmiuPacketDumper.class);

    private static ObjectMapper mapper = new ObjectMapper();

    private PrintWriter writer;
    private OutputStream outputStream;
    private LoraRepository loraRepository;

    private boolean reportedFirstError = false;

    public L900PacketDumper(OutputStream outputStream, LoraRepository loraRepository)
    {
        this.outputStream = outputStream; //we store the response so we can call getWriter as late as possible
        this.loraRepository = loraRepository;
    }


    private void writeHeaderToCsv()
    {

        final String headerStr = "Insert Date,Insert Time (UTC),L900 ID,Packet Type" + //From DynamoDB
                ",EUI,Gateway,RSSI,SnR,Data Rate,TX Power,Frequency,Sequence Number,Port,TX Date,TX Time" + //Senet JSON Info
                ",Network Time Loss Flags,L900 Error Flags,antenna Type,Battery Swipe Flags,Mag Swipe Flag" + //11 Byte Config Packet
                ",FW Version,Device Type,Device ID,Reverse Flow 35 Day,Reverse Flow 24 Hour,Leak State,Battery Life" +
                ",Empty Pipe,Excessive Flow,Tamper,Attached Device Alarm Mask" +
                ",11 Byte Read 1,2,3,Major Reverse Flow, Continuous Leak" + //11 Byte Read Packet
                ",53 Byte Read | rev flow | leak flag 1,2,3,4,5,6,7,8,9,10,11,12,Time Since Last Read" + //53 Byte Read + flag Packet
                ",Time & Date,Time update #" + //Time & Date update packet
                ",Command,Meta Date"; //Command Response Packet

        synchronized (writer)
        {
            writer.println(headerStr);
        }
    }

    @Override
    public void writePacketToCsv(MiuPacketReceivedDynamoItem miuPacket)
    {
        ensureWriterAndHeader();

        //SimpleDateFormat used for formatting unix timestamp to date and time

        //construct each row for the packet
        String rowForPacket;

        try
        {
            rowForPacket = formatPacket(miuPacket);
        }
        catch (Exception e)
        {
            rowForPacket = "Error occurred while formatting packet: " + e.toString() + "; raw packet data was " +
                    (new String(miuPacket.getPacketData().array(), StandardCharsets.UTF_8));

            if (!reportedFirstError)
            {
                logger.warn("Exception in packet dump: " + rowForPacket, e);
                reportedFirstError = true;
            }

        }
        synchronized (writer)
        {
            writer.println(rowForPacket);
        }
    }

    private void ensureWriterAndHeader()
    {
        if (this.writer == null)
        {
            this.writer = new PrintWriter(this.outputStream);
            writeHeaderToCsv();
        }
    }

    private String formatPacket(MiuPacketReceivedDynamoItem miuPacket) throws IOException
    {
        StringBuilder packetsStr = new StringBuilder();

        SimpleDateFormat dateTimeFormat =
                new SimpleDateFormat("yyyy/MM/dd,HH:mm:ss");
        dateTimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = new Date(miuPacket.getInsertDate());

        packetsStr.append(dateTimeFormat.format(date));
        packetsStr.append(String.format(", %06d ", miuPacket.getMiuId()));

        //Parse packet
        String jsonString = new String(miuPacket.getPacketData().array(), StandardCharsets.UTF_8);
        PacketId packetId;
        byte[] pduBytes;

        final LoraDeviceDetail loraDeviceDetail = loraRepository.getLoraDeviceDetailByMiuId(MiuId.valueOf(miuPacket.getMiuId()));
        if (loraDeviceDetail == null)
        {
            packetsStr.append(",[L900 with ID ").append(miuPacket.getMiuId()).append(" not found in lora_device_details in database!]");
            packetsStr.append(",not available");
            packetsStr.append(",not available");
            packetsStr.append(",not available");
            packetsStr.append(",not available");
            packetsStr.append(",not available");
            packetsStr.append(",not available");
            packetsStr.append(",not available");
            packetsStr.append(",not available");
            packetsStr.append(",not available");
            packetsStr.append(",not available");

            packetId = null;
            pduBytes = new byte[0];
        }
        else if(loraDeviceDetail.getLoraNetwork() == LoraNetwork.ACTILITY)
        {
            ActilityForwardPacketJson actilityJsonFull = mapper.readValue(jsonString, ActilityForwardPacketJson.class);
            ActilityUplinkJson actilityJson = actilityJsonFull.getDevEuiUplink();
            pduBytes = actilityJson.getPduBytes();

            L900Packet l900Packet = new L900Packet(actilityJson.getPduBytes());
            packetId = l900Packet.getPacketId();

            packetsStr.append(",").append(packetId.getId());
            packetsStr.append(",").append(actilityJson.getEui());
            packetsStr.append(",").append(actilityJson.getGw());
            packetsStr.append(",").append(actilityJson.getRssi());
            packetsStr.append(",").append(actilityJson.getSnr());
            packetsStr.append(", not available from Actility");
            packetsStr.append(", not available from Actility");
            packetsStr.append(", not available from Actility");
            packetsStr.append(",").append(actilityJson.getSeqNo());
            packetsStr.append(",").append(actilityJson.getPort());
            packetsStr.append(",").append(dateTimeFormat.format(Date.from(actilityJson.getTxTimeInstant())));
        }
        else
        {
            SenetForwardPacketJson senetJson = mapper.readValue(jsonString, SenetForwardPacketJson.class);
            pduBytes = senetJson.getPduBytes();

            L900Packet l900Packet = new L900Packet(senetJson.getPduBytes());
            packetId = l900Packet.getPacketId();

            packetsStr.append(",").append(packetId.getId());
            packetsStr.append(",").append(senetJson.getEui());
            packetsStr.append(",").append(senetJson.getGw());
            packetsStr.append(",").append(senetJson.getRssi());
            packetsStr.append(",").append(senetJson.getSnr());
            packetsStr.append(",").append(senetJson.getDataRate());
            packetsStr.append(",").append(senetJson.getTxPower());
            packetsStr.append(",").append(senetJson.getFreq());
            packetsStr.append(",").append(senetJson.getSeqNo());
            packetsStr.append(",").append(senetJson.getPort());
            packetsStr.append(",").append(dateTimeFormat.format(Date.from(senetJson.getTxTimeInstant())));
        }

        if(PacketId.BASIC_CONFIG_PACKET.equals(packetId))
        {
            BasicConfigPacket basicConfigPacket = new BasicConfigPacket(pduBytes);
            packetsStr.append(",").append(basicConfigPacket.getNetworkTimeLoss());
            packetsStr.append(",").append(basicConfigPacket.getL900Errors());
            packetsStr.append(",").append(basicConfigPacket.getAntennaType());
            packetsStr.append(",").append(basicConfigPacket.getBatteryStatus());
            packetsStr.append(",").append(basicConfigPacket.getMagSwipe());
            packetsStr.append(",").append(basicConfigPacket.getFirmwareVersion());
            packetsStr.append(",").append(basicConfigPacket.getDeviceType());
            packetsStr.append(",").append(basicConfigPacket.getDeviceId());
            packetsStr.append(",").append(basicConfigPacket.getReverseFlow_35Day());
            packetsStr.append(",").append(basicConfigPacket.getReverseFlow_24Hour());
            packetsStr.append(",").append(basicConfigPacket.getLeakState());
            packetsStr.append(",").append(basicConfigPacket.getBatteryLife());
            packetsStr.append(",").append(basicConfigPacket.getEmptyPipe());
            packetsStr.append(",").append(basicConfigPacket.getExcessiveFlow());
            packetsStr.append(",").append(basicConfigPacket.getTamper());
            packetsStr.append(",").append(basicConfigPacket.getAttachedDeviceAlarmMask());
        }
        else
        {
            packetsStr.append(",,,,,,,,,,,,,,,,");
        }

        if(PacketId.BASIC_READ_PACKET.equals(packetId))
        {
            BasicReadPacket basicReadPacket = new BasicReadPacket(pduBytes);
            packetsStr.append(",").append(basicReadPacket.getRead1());
            packetsStr.append(",").append(basicReadPacket.getRead2());
            packetsStr.append(",").append(basicReadPacket.getRead3());
            packetsStr.append(",").append(basicReadPacket.getReverseFlowFlag());
            packetsStr.append(",").append(basicReadPacket.getContinuousLeakFlag());
        }
        else
        {
            packetsStr.append(",,,,,");
        }

        if(PacketId.FULL_READ_PACKET.equals(packetId))
        {
            FullReadPacket fullReadPacket = new FullReadPacket(pduBytes);

            for(int i=0; i<12; i++)
            {
                packetsStr.append(",").append(fullReadPacket.getReading(i).getReading()).append("|")
                        .append(fullReadPacket.getReading(i).getRevFlowFlag()).append("|")
                        .append(fullReadPacket.getReading(i).getLeakFlag());
            }

            packetsStr.append(",").append(fullReadPacket.getTimeSinceLastRead());
        }
        else
        {
            packetsStr.append(",,,,,,,,,,,,,");
        }

        if(PacketId.TIME_AND_DATE.equals(packetId))
        {
            TimeAndDatePacket timeAndDatePacket = new TimeAndDatePacket(pduBytes);
            packetsStr.append(",").append(timeAndDatePacket.getTimeAndDate().getEpochSecond());
            packetsStr.append(",").append(timeAndDatePacket.getUpdateIdentifier());
        }
        else
        {
            packetsStr.append(",,");
        }

        if(PacketId.COMMAND_RESPONSE_PACKET.equals(packetId))
        {
            //Todo - replace with parsed info
            packetsStr.append(",,");
        }
        else
        {
            packetsStr.append(",,");
        }

        return packetsStr.toString();
    }

    @Override
    public void close() throws IOException
    {
        if (this.writer != null)
        {
            this.writer.close();
        }
    }
}

