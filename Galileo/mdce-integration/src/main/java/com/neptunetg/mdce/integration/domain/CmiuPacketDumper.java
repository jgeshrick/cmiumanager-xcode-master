/*
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.*;
import com.neptunetg.common.packet.model.taggeddata.types.CharArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.ImageVersionInfoData;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerData;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.common.util.HexUtils;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

/**
 * Writes packets to output stream.  Not thread-safe
 */
public class CmiuPacketDumper implements MiuDumper
{
    private static final Logger logger = LoggerFactory.getLogger(CmiuPacketDumper.class);

    private static final String QUOTE = "\"";

    private PrintWriter writer;
    private OutputStream outputStream;
    private boolean reportedFirstError = false;
    private boolean writtenHeader = false;

    public CmiuPacketDumper(OutputStream outputStream)
    {
        this.outputStream = outputStream;
    }


    private void writeHeaderToCsv()
    {

        final String headerStr = "Insert Date,Insert Time (UTC),MIU ID" + //packet info CSV headers
                ",CMIU ID,Sequence Number,Key Info" + //Cmiu Packet Header CSV headers
                ",Encryption Method,Token (AES CRC),Network Flags" +
                ",RSSI (dBm),BER range min (%),BER range max (%)" +
                ",Date of Packet Creation,Time of Packet Creation (UTC)" +
                ",Cmiu Type,Device HW Revision,Firmware Revision" + //Cmiu Configuration CSV headers
                ",Bootloader Version,Manufacture Date,Manufacture Time (UTC)" +
                ",Initial Call in Time (UTC) [Reporting start time]" +
                ",Call in Interval [Reporting interval hours],Call in Window [Reporting window],Reporting Window Number of Retries" +
                ",Minutes to Reporting Windows Quiet Time" +
                ",Minutes to Reporting Windows Quiet Time End" +
                ",Number of Attached Devices,Date of Installation,Time of Installation (UTC OLD)" +
                ",Date of Last Mag Swipe (OLD),Time of Last Mag Swipe (UTC OLD),Mag Swipe Count (OLD)" +
                ",Estimated Battery capacity Remaining (OLD)" +
                ",Time Error on Last Network Time Access (OLD)" +
                ",Network Operator Response" + //Network operators response string CSV header
                ",Network Performance" + // Network performance response string CSV header
                ",Registration Status" + //Registration Status response string CSV Header
                ",Host IP Address" + //Host IP Address CSV Header
                ",Fallback Host IP Address" + //Fallback Host IP Address CSV Header
                ",Assigned CMIU IP Address" + //Assigned CMIU IP Address CSV Header
                ",Module Fota FTP URL" + //Module Fota FTP Url CSV Header
                ",Module Fota User Id" + //Module Fota User Id CSV Header
                ",Module Fota FTP Password" + //Module Fota FTP Password CSV Header
                ",Modem Hardware Revision Number" + //Modem Hardware Revision Number CSV Header
                ",Device Model ID Code" + //Device Model ID Code CSV Header
                ",Device Manufacturer ID Code" + //Device Manufacturer ID Code CSV Header
                ",Device Software Revision Number" + //Device Software Revision Number CSV Header
                ",Pin Puk Puk2 Request Data" + //Pin Puk Puk2 Request Data CSV Header
                ",Last BLE User ID" + //Last BLE user ID CSV Header
                ",Error Log Messages" + //Error Log Messages CSV Header
                ",Sim IMSI Number" + //Sim IMSI Number CSV Header
                ",Sim Card ID" + //Sim Card ID Number CSV Header
                ",Diagnostics Flags,Processor Reset Count" + //Cmiu Diagnostics CSV Headers
                ",BLE Last User Logged in Date,BLE Last user Logged in Time (UTC)" + //Last BLE user Logged in Date / Time CSV Headers
                ",Date of connection,Time of Connection (UTC)" + //Connection Timing Log CSV Headers
                ",Time From Power on to Network Registration" +
                ",Time From Network Registration To Context Activation" +
                ",Time From Context Activation to Server Connection" +
                ",Time to Transfer a Message to the Server" +
                ",Time to Disconnect and Shutdown" +
                ",Number of Devices,CMIU Flags" + //CMIU Status
                ",Device Number,Attached Device ID" + //Reported Device Config
                ",Device Type,Current Device Data,Device Flags" +
                ",R900 Interval Data" + //R900 Interval Data
                ",Device ID number,Interval Format" + //Interval recording config
                ",Device Reading Interval,Device Logging Interval,Start Time of Interval" +
                ",Time and Date of most recent reading in packet" +
                ",Firmware Revision" + //Firmware revision
                ",Bootloader Revision" + //Bootloader revision
                ",Config Revision" + //Config Revision
                ",Reporting Start Mins,Reporting Interval" + //Device Config and Recording and Reporting Interval
                ",Reporting Retries,Reporting Transmit Windows,Reporting Quiet Start,Reporting Quiet End" +
                ",Number Of Attached Devices,Event Mask For Device" +
                ",Recording Start Time,Recording Interval" +
                ",Battery Voltage (mV)" + //Battery Voltage
                ",Temperature (deg C)" +
                ",Manufacture Date,Date Of Installation,Date of last Mag Swipe,Mag Swipe Counter" + //CMIU Information
                ",Estimated Battery Capacity Remaining,Time Error on Last Network Time Access" +
                ",Command Id" + //Command
                ",IMEI" ; //The reported IMEI

        synchronized (writer)
        {
            writer.println(headerStr);
        }
    }

    @Override
    public void writePacketToCsv(MiuPacketReceivedDynamoItem miuPacket)
    {
        ensureWriterAndHeader();

        //SimpleDateFormat used for formatting unix timestamp to date and time

        //construct each row for the packet
        String rowForPacket;

        try
        {
            rowForPacket = formatPacket(miuPacket);
        }
        catch (Exception e)
        {
            rowForPacket = "Error occurred while formatting packet: " + e.toString() + "; raw packet data was " + HexUtils.byteBufferToHex(miuPacket.getPacketData());
            if (!reportedFirstError)
            {
                logger.warn("Exception in packet dump: " + rowForPacket, e);
                reportedFirstError = true;
            }
        }
        synchronized (writer)
        {
            writer.println(rowForPacket);
        }
    }

    private void ensureWriterAndHeader()
    {
        if (this.writer == null)
        {
            this.writer = new PrintWriter(this.outputStream);
            writeHeaderToCsv();
        }
    }

    private String formatPacket(MiuPacketReceivedDynamoItem miuPacket)
    {
        StringBuilder packetsStr = new StringBuilder();

        SimpleDateFormat tagDateTimeFormat =
                new SimpleDateFormat("yyyy/MM/dd,HH:mm:ss");
        tagDateTimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = new Date();
        date.setTime(miuPacket.getInsertDate());

        packetsStr.append(tagDateTimeFormat.format(date));
        packetsStr.append(String.format(", %06d ", miuPacket.getMiuId()));

        //Parse packet and get as iterator
        byte[] packetData = miuPacket.getPacketData().array();
        TaggedPacket tp = null;
        tp = PacketParser.parseTaggedPacket(packetData);

        Iterator<TaggedData> tagIterator = tp.iterator();

        //Put tags into a treemap so they can be searched
        TreeMap<Integer, TaggedData> tpMap = new TreeMap<>();
        while(tagIterator.hasNext())
        {
            TaggedData tag = tagIterator.next();
            tpMap.put(tag.getTagId().getId(), tag);
        }

        //Decode secure data (if it exists) and add tagged data to tpMap
        Map<Integer, TaggedData> secureTpMap = PacketParser.parseSecureTag(tp.findTag(SecureBlockArrayData.class));
        tpMap.putAll(secureTpMap);

        //Variable used to temporarily store tagged data
        TaggedData tag;

        //Go through possible tags and print
        if((tag = (TaggedData)tpMap.get(TagId.CmiuPacketHeader.getId())) != null)
        {
            CmiuPacketHeaderData cmiuPacketHeader = (CmiuPacketHeaderData) tag;
            packetsStr.append(",").append(cmiuPacketHeader.getCmiuId());
            packetsStr.append(",").append(cmiuPacketHeader.getSequenceNumber());
            packetsStr.append(",").append(cmiuPacketHeader.getKeyInfo());
            packetsStr.append(",").append(cmiuPacketHeader.getEncryptionMethod());
            packetsStr.append(",").append(cmiuPacketHeader.getToken());
            packetsStr.append(",").append(cmiuPacketHeader.getNetworkFlags());

            int rssi = cmiuPacketHeader.getRssi();

            if(rssi >= 0 && rssi <= 31)
            {
                packetsStr.append(",").append((-113 + (rssi * 2)));
            }
            else
            {
                packetsStr.append(",OUT OF RANGE"); //To indicate an error
            }

            int ber = cmiuPacketHeader.getBer();

            switch (ber)
            {
                case 0:
                    packetsStr.append(",0.0,0.2");
                    break;
                case 1:
                    packetsStr.append(",0.2,0.4");
                    break;
                case 2:
                    packetsStr.append(",0.4,0.8");
                    break;
                case 3:
                    packetsStr.append(",0.8,1.6");
                    break;
                case 4:
                    packetsStr.append(",1.6,3.2");
                    break;
                case 5:
                    packetsStr.append(",3.2,6.4");
                    break;
                case 6:
                    packetsStr.append(",6.4,12.8");
                    break;
                case 7:
                    packetsStr.append(",12.8,100.0");
                    break;
                default:
                    packetsStr.append(",OUT OF RANGE,OUT OF RANGE"); //To indicate an error
                    break;
            }
            packetsStr.append(",").append(tagDateTimeFormat.format(cmiuPacketHeader.getCurrentTimeAndDate().asDate()));
        }
        else
        {
            packetsStr.append(",,,,,,,,,");
        }

        if((tag = (TaggedData)tpMap.get(TagId.CmiuConfiguration.getId())) != null)
        {
            CmiuBasicConfigurationData cmiuConfiguration = (CmiuBasicConfigurationData) tag;
            packetsStr.append(","); //CMIU type
            packetsStr.append(","); //getDeviceHwRevision
            packetsStr.append(","); //getFirmwareRevision
            packetsStr.append(","); //cmiuConfiguration.getBootloaderVersion());
            packetsStr.append(",,"); // + tagDateTimeFormat.format(cmiuConfiguration.getManufactureDate().asDate()));
            packetsStr.append(",").append(cmiuConfiguration.getReportingStartTime());
            packetsStr.append(",").append(cmiuConfiguration.getReportingIntervalHours());
            packetsStr.append(",").append(cmiuConfiguration.getReportingWindow());
            packetsStr.append(","); // + cmiuConfiguration.getReportingWindowNumberOfRetries());
            packetsStr.append(","); // + cmiuConfiguration.getMinsToReportingWindowQuietTime());
            packetsStr.append(","); // + cmiuConfiguration.getMinsToReportingWindowQuietTimeEnd());
            packetsStr.append(","); // + cmiuConfiguration.getNumberOfAttachedDevices());
            packetsStr.append(",,"); // + tagDateTimeFormat.format(cmiuConfiguration.getDateOfInstallation().asDate()));
            packetsStr.append(",,"); // + tagDateTimeFormat.format(cmiuConfiguration.getDateOfLastMagSwipe().asDate()));
            packetsStr.append(","); // + cmiuConfiguration.getMagSwipeCounter());
            packetsStr.append(","); // + cmiuConfiguration.getEstimateBatteryCapacityRemaining());
            packetsStr.append(","); // + cmiuConfiguration.getTimeErrorOnLastNetworkTimeAccess());
        }
        else if((tag = (TaggedData)tpMap.get(TagId.DetailedCmiuConfiguration.getId())) != null)
        {
            DetailedCmiuConfigurationData cmiuConfiguration = (DetailedCmiuConfigurationData) tag;
            packetsStr.append(",").append(cmiuConfiguration.getCmiuType());
            packetsStr.append(",").append(cmiuConfiguration.getDeviceHwRevision());
            packetsStr.append(",").append(cmiuConfiguration.getFirmwareRevision());
            packetsStr.append(",").append(cmiuConfiguration.getBootloaderVersion());
            packetsStr.append(",").append(tagDateTimeFormat.format(cmiuConfiguration.getManufactureDate().asDate()));
            packetsStr.append(",").append(cmiuConfiguration.getReportingStartTime());
            packetsStr.append(",").append(cmiuConfiguration.getReportingInterval());
            packetsStr.append(",").append(cmiuConfiguration.getReportingWindow());
            packetsStr.append(",").append(cmiuConfiguration.getReportingWindowNumberOfRetries());
            packetsStr.append(",").append(cmiuConfiguration.getMinsToReportingWindowQuietTime());
            packetsStr.append(",").append(cmiuConfiguration.getMinsToReportingWindowQuietTimeEnd());
            packetsStr.append(",").append(cmiuConfiguration.getNumberOfAttachedDevices());
            packetsStr.append(",").append(tagDateTimeFormat.format(cmiuConfiguration.getDateOfInstallation().asDate()));
            packetsStr.append(",").append(tagDateTimeFormat.format(cmiuConfiguration.getDateOfLastMagSwipe().asDate()));
            packetsStr.append(",").append(cmiuConfiguration.getMagSwipeCounter());
            packetsStr.append(",").append(cmiuConfiguration.getEstimateBatteryCapacityRemaining());
            packetsStr.append(",").append(cmiuConfiguration.getTimeErrorOnLastNetworkTimeAccess());
        }
        else
        {
            packetsStr.append(",,,,,,,,,,,,,,,,,,,,");
        }

        if((tag = (TaggedData)tpMap.get(TagId.NetworkOperators.getId())) != null)
        {
            NetworkOperatorsData networkOperator = (NetworkOperatorsData) tag;
            appendStringItem(packetsStr, networkOperator.getNetworkOperatorResponseString());
        }
        else
        {
            packetsStr.append(",");
        }

        if((tag = (TaggedData)tpMap.get(TagId.NetworkPerformance.getId())) != null)
        {
            NetworkPerformanceData networkPerformance = (NetworkPerformanceData) tag;
            appendStringItem(packetsStr, networkPerformance.getNetworkPerformanceString());
        }
        else
        {
            packetsStr.append(",");
        }

        if((tag = (TaggedData)tpMap.get(TagId.RegistrationStatus.getId())) != null)
        {
            RegistrationStatusData registrationStatus = (RegistrationStatusData) tag;
            appendStringItem(packetsStr, registrationStatus.getRegistrationStatusString());
        }
        else
        {
            packetsStr.append(",");
        }

        //Get tag IDs for the char array tagged data used in packet dump tool
        int[] charArrayTagNumbers = new int[13];
        charArrayTagNumbers[0] = TagId.MQTTBrokerIPAddress.getId();
        charArrayTagNumbers[1] = TagId.FallbackMQTTBrokerIPAddress.getId();
        charArrayTagNumbers[2] = TagId.AssignedCmiuIPAddress.getId();
        charArrayTagNumbers[3] = TagId.ModuleFotaFtpUrl.getId();
        charArrayTagNumbers[4] = TagId.ModuleFotaFtpUsername.getId();
        charArrayTagNumbers[5] = TagId.ModuleFotaFtpPassword.getId();
        charArrayTagNumbers[6] = TagId.ModemHardwareRevision.getId();
        charArrayTagNumbers[7] = TagId.ModemModelIdentificationCode.getId();
        charArrayTagNumbers[8] = TagId.ModemManufacturerIdentificationCode.getId();
        charArrayTagNumbers[9] = TagId.ModemSoftwareRevisionNumber.getId();
        charArrayTagNumbers[10] = TagId.PinPukPukTwoRequestData.getId();
        charArrayTagNumbers[11] = TagId.LastBleUserId.getId();
        charArrayTagNumbers[12] = TagId.ErrorLog.getId();

        //Search for each tagid and fill in values
        for (int charArrayTagNumber : charArrayTagNumbers)
        {
            if ((tag = (TaggedData) tpMap.get(charArrayTagNumber)) != null)
            {
                CharArrayData charArrayTag = (CharArrayData) tag;
                appendStringItem(packetsStr, new String(charArrayTag.getData(), StandardCharsets.UTF_8));
            }
            else
            {
                packetsStr.append(",");
            }
        }

        if((tag = (TaggedData)tpMap.get(TagId.SimImsi.getId())) != null)
        {
            SimImsiData simImsiData = (SimImsiData) tag;
            appendStringItem(packetsStr, simImsiData.getAsString());
        }
        else
        {
            packetsStr.append(",");
        }

        if((tag = (TaggedData)tpMap.get(TagId.SimCardId.getId())) != null)
        {
            SimCardIdData simCardIdData = (SimCardIdData) tag;
            appendStringItem(packetsStr, simCardIdData.getAsString());
        }
        else
        {
            packetsStr.append(",");
        }

        if((tag = (TaggedData)tpMap.get(TagId.CmiuHwDiagnostics.getId())) != null)
        {
            CmiuDiagnosticsData cmiuDiagnostics = (CmiuDiagnosticsData) tag;
            packetsStr.append(",").append(cmiuDiagnostics.getDiagnosticFlags());
            packetsStr.append(",").append(cmiuDiagnostics.getProcessorResetCount());
        }
        else
        {
            packetsStr.append(",,");
        }

        if((tag = (TaggedData)tpMap.get(TagId.LastBleUserLoginDate.getId())) != null)
        {
            IntegerData lastBleUserDateTag = (IntegerData) tag;
            Date lastBleUserDate = new Date(lastBleUserDateTag.getValue() * 1000);
            packetsStr.append(",").append(tagDateTimeFormat.format(lastBleUserDate));
        }
        else
        {
            packetsStr.append(",,");
        }

        if((tag = (TaggedData)tpMap.get(TagId.ConnectionTimingLog.getId())) != null)
        {
            ConnectionTimingLogData connectionTimingLog = (ConnectionTimingLogData) tag;
            packetsStr.append(",").append(tagDateTimeFormat.format(connectionTimingLog.getDateOfConnection().asDate()));
            packetsStr.append(",").append(connectionTimingLog.getTimeFromPowerOnToNetworkRegistration());
            packetsStr.append(",").append(connectionTimingLog.getTimeFromNetworkRegistrationToContextActivation());
            packetsStr.append(",").append(connectionTimingLog.getTimeFromContextActivationToServerConnection());
            packetsStr.append(",").append(connectionTimingLog.getTimeToTransferMessageToServer());
            packetsStr.append(",").append(connectionTimingLog.getTimeToDisconnectAndShutdown());
        }
        else
        {
            packetsStr.append(",,,,,,,");
        }

        if((tag = (TaggedData)tpMap.get(TagId.CmiuStatus.getId())) != null)
        {
            CmiuStatusData cmiuStatus = (CmiuStatusData) tag;
            packetsStr.append(",").append(cmiuStatus.getNumberOfDevices());
            packetsStr.append(",").append(cmiuStatus.getCmiuFlags());
        }
        else
        {
            packetsStr.append(",,");
        }

        if((tag = (TaggedData)tpMap.get(TagId.ReportedDeviceConfiguration.getId())) != null)
        {
            ReportedDeviceConfigurationData reportedDeviceConfiguration = (ReportedDeviceConfigurationData) tag;
            packetsStr.append(",").append(reportedDeviceConfiguration.getDeviceNumber());
            packetsStr.append(",").append(reportedDeviceConfiguration.getAttachedDeviceId());
            packetsStr.append(",").append(reportedDeviceConfiguration.getDeviceType());
            packetsStr.append(",").append(reportedDeviceConfiguration.getCurrentDeviceData());
            packetsStr.append(",").append(reportedDeviceConfiguration.getDeviceFlags());
        }
        else
        {
            packetsStr.append(",,,,");
        }

        if((tag = (TaggedData)tpMap.get(TagId.R900IntervalData.getId())) != null)
        {
            IntegerArrayData r900Interval = (IntegerArrayData) tag;
            packetsStr.append(",").append("'").append(Hex.encodeHexString(r900Interval.getDataBuffer().array()));
        }
        else
        {
            packetsStr.append(",");
        }

        if((tag = tpMap.get(TagId.IntervalRecording.getId())) != null)
        {
            IntervalRecordingConfigurationData intervalRecordingConfig = (IntervalRecordingConfigurationData) tag;
            packetsStr.append(",").append(intervalRecordingConfig.getDeviceNumber());
            packetsStr.append(",").append(intervalRecordingConfig.getIntervalFormat());
            packetsStr.append(",").append(intervalRecordingConfig.getDeviceRecordingInterval());
            packetsStr.append(",").append(intervalRecordingConfig.getReportingIntervalHours());
            packetsStr.append(",").append(intervalRecordingConfig.getStartTimeOfRecordingInterval());
            packetsStr.append(",").append(intervalRecordingConfig.getTimeOfMostRecentReading());
        }
        else
        {
            packetsStr.append(",,,,,,");
        }

        //Start of Support For 39, 40 and 41
        if((tag = tpMap.get(TagId.FirmwareRevision.getId())) != null)
        {
            ImageVersionInfoData firmwareRevision = (ImageVersionInfoData) tag;
            packetsStr.append(",").append(firmwareRevision.getVersion());
        }
        else
        {
            packetsStr.append(",");
        }

        if((tag = tpMap.get(TagId.BootloaderVersion.getId())) != null)
        {
            ImageVersionInfoData bootloaderRevision = (ImageVersionInfoData) tag;
            packetsStr.append(",").append(bootloaderRevision.getVersion());
        }
        else
        {
            packetsStr.append(",");
        }

        if((tag = tpMap.get(TagId.ConfigRevision.getId())) != null)
        {
            ImageVersionInfoData configRevision = (ImageVersionInfoData) tag;
            packetsStr.append(",").append(configRevision.getVersion());
        }
        else
        {
            packetsStr.append(",");
        }
        //End of Support For 39, 40 and 41

        //Start of Support For 48, 62 and 63
        if((tag = tpMap.get(TagId.RecordingandReportingInterval.getId())) != null)
        {
            RecordingAndReportingIntervalData recordingAndReportingInterval = (RecordingAndReportingIntervalData) tag;
            packetsStr.append(",").append(recordingAndReportingInterval.getReportingStartMins());
            packetsStr.append(",").append(recordingAndReportingInterval.getReportingIntervalHours());
            packetsStr.append(",").append(recordingAndReportingInterval.getReportingNumberOfRetries());
            packetsStr.append(",").append(recordingAndReportingInterval.getReportingTransmitWindowsMins());
            packetsStr.append(",").append(recordingAndReportingInterval.getReportingQuietStartMins());
            packetsStr.append(",").append(recordingAndReportingInterval.getReportingQuietEndMins());
            packetsStr.append(",").append(recordingAndReportingInterval.getRecordingStartTimeMins());
            packetsStr.append(",").append(recordingAndReportingInterval.getRecordingIntervalMins());
            packetsStr.append(",").append(recordingAndReportingInterval.getNumberOfAttachedDevices());
            packetsStr.append(",").append(recordingAndReportingInterval.getEventMaskForDevice());
        }
        else
        {
            packetsStr.append(",,,,,,,,,,");
        }

        if((tag = tpMap.get(TagId.BatteryVoltage.getId())) != null)
        {
            BatteryVoltage batteryVoltage = (BatteryVoltage) tag;
            packetsStr.append(",").append(batteryVoltage.getMillivolts());
        }
        else
        {
            packetsStr.append(",");
        }

        if((tag = tpMap.get(TagId.Temperature.getId())) != null)
        {
            IntegerData temperature = (IntegerData) tag;
            packetsStr.append(",").append((byte) temperature.getValue());
        }
        else
        {
            packetsStr.append(",");
        }
        //End of Support For 48, 62 and 63

        //Start of Support For 44
        if((tag = tpMap.get(TagId.CmiuInformation.getId())) != null)
        {
            CmiuInformationData cmiuInformationData = (CmiuInformationData) tag;
            packetsStr.append(",").append(cmiuInformationData.getManufactureDate().asDate().toString());
            packetsStr.append(",").append(cmiuInformationData.getDateOfInstallation().asDate().toString());
            packetsStr.append(",").append(cmiuInformationData.getDateOfLastMagSwipe().asDate().toString());
            packetsStr.append(",").append(cmiuInformationData.getMagSwipeCounter());
            packetsStr.append(",").append(cmiuInformationData.getBatteryCapacityMah());
            packetsStr.append(",").append(cmiuInformationData.getTimeErrorOnLastNetworkAccess());
        }
        else
        {
            packetsStr.append(",,,,,,");
        }
        //End of Support For 44

        if((tag = tpMap.get(TagId.Command.getId())) != null)
        {
            CommandData commandData = (CommandData) tag;
            packetsStr.append(",").append(commandData.getCommandId());
        }
        else
        {
            packetsStr.append(",");
        }

        if((tag = tpMap.get(TagId.ModemSerialNumber.getId())) != null)
        {
            CharArrayData charArrayData = (CharArrayData) tag;
            packetsStr.append(",").append(charArrayData.getAsString());
        }
        else
        {
            packetsStr.append(",");
        }

        return packetsStr.toString();
    }

    private void appendStringItem(StringBuilder sb, String s)
    {
        sb.append(',');

        if (s != null)
        {
            if (!s.equals("null"))
            {
                if (s.contains(QUOTE) || s.contains(",") || s.contains("\r") || s.contains("\n"))
                {
                    s = s.trim();
                    sb.append(QUOTE).append(s.replace(QUOTE, QUOTE + QUOTE)).append(QUOTE); //replace " with ""
                }
                else
                {
                    sb.append(s);
                }
            }
        }
    }

    @Override
    public void close() throws IOException
    {
        if (this.writer != null)
        {
            this.writer.close();
        }
    }
}

