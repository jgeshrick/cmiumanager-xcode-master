/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.utility;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.Converter;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Utility to convert Date to formatted string.
 */
public class DateTimeUtility
{

    private final static DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME  // yyyy-MM-dd'T'HH:mm:ss.SSSZ, e.g. 2000-10-31 01:30:00.000-05:00.
            .withZone(ZoneId.of("US/Central"));

    private final static DateTimeFormatter instantToStringFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME  // yyyy-MM-dd'T'HH:mm:ss.SSSZ, e.g. 2000-10-31 01:30:00.000-05:00.
            .withZone(ZoneId.of("US/Central"));

    /**
     * Given epochSeconds, return a Date time string formatted to 2011-12-03T10:15:30+01:00
     * @param epochSeconds epoch seconds
     * @return format DateTime string eg 2011-12-03T10:15:30+01:00
     */
    public static String getUsCentralDateForEpochSecond(long epochSeconds)
    {
        return formatter.format(Instant.ofEpochSecond(epochSeconds));
    }

    /**
     * Given epochMilli, return a Date time string formatted to 2011-12-03T10:15:30+01:00
     * @param epochMilli epoch ms
     * @return format DateTime string eg 2011-12-03T10:15:30+01:00
     */
    public static String getUsCentralDateForEpochMilli(long epochMilli)
    {
        return formatter.format(Instant.ofEpochMilli(epochMilli));
    }



    /**
     * For json serialiser to convert from a java instant to string output of format 2000-10-31T14:30:00-05:00
     */
    public static class InstantToStringConverter implements Converter<Instant, String>
    {
        @Override
        public String convert(Instant arg0)
        {
            return instantToStringFormatter.format(arg0);
        }

        @Override
        public JavaType getInputType(TypeFactory arg0)
        {
            return arg0.constructType(Instant.class);
        }

        @Override
        public JavaType getOutputType(TypeFactory arg0)
        {
            return arg0.constructType(String.class);
        }

    }

    /**
     * For json deserialiser to convert from a string representation of date time to java.time.Instant
     */
    public static class DateTimeToInstantConverter implements com.fasterxml.jackson.databind.util.Converter<String, Instant>
    {
        private final static DateTimeFormatter stringToInstantFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

        @Override
        public Instant convert(String arg0)
        {
            ZonedDateTime zdt = ZonedDateTime.parse(arg0, stringToInstantFormatter);
            return zdt.toInstant();
        }

        @Override
        public JavaType getInputType(TypeFactory arg0)
        {
            return arg0.constructType(String.class);
        }

        @Override
        public JavaType getOutputType(TypeFactory arg0)
        {
            return arg0.constructType(Instant.class);
        }

    }


}
