/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;


import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.integration.audit.AuditService;
import com.neptunetg.mdce.integration.data.gateway.GatewayDetails;
import com.neptunetg.mdce.integration.data.gateway.GatewayId;
import com.neptunetg.mdce.integration.data.gateway.GatewayRepository;
import com.neptunetg.mdce.integration.data.refdata.RefDataRepository;
import com.neptunetg.mdce.integration.data.site.SiteRepository;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Provides Miu Service for controllers and other services to access mysql database.
 *
 */
@Service
public class GatewayServiceImpl implements GatewayService
{
    private static final Logger logger = LoggerFactory.getLogger(GatewayServiceImpl.class);

    private static final String GATEWAY_OWNERSHIP_PATH = "Gateway/Ownership/";
    private static final String CONFLICT_PATH = "/Conflict/";
    private static final String FOR_SITE = " for site ";
    private static final String FROM = " from ";
    private static final String UNABLE_TO_RAISE_ALERT = "Unable to raise alert! ";

    @Autowired
    private GatewayRepository gatewayRepo;

    @Autowired
    private SiteRepository siteRepository;

    @Autowired
    private AuditService auditService;

    @Autowired
    private RefDataRepository refDataRepository;

    @Autowired
    private AlertService alertService;

    @Override
    public List<GatewayDetails> listGatewayDetails()
    {
        return gatewayRepo.getGatewaysForAllUtility();
    }

    @Override
    public GatewayDetails getGatewayDetails(GatewayId gatewayId)
    {
        //Todo Add actual data
        return null;
    }

    @Override
    public List<SiteDetails> getUtilityList()
    {
        return siteRepository.getSiteList();
    }

    private static MiuType inferMiuTypeFromMiuId(MiuId miuId)
    {
        if (miuId.isInCmiuIdRange())
        {
            return MiuType.CMIU;
        }
        return MiuType.R900;
    }
}
