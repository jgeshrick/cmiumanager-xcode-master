/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.rest.json.packets;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * List of packet from a same packets id for constructing part of getPacket JSON response.
 */
public class JsonPacketListForMiu
{
    @JsonProperty("miu_id")
    final private int miuId;
    final private List<JsonPacketData> packets;

    /**
     * Getter for miuid
     */
    public int getMiuId()
    {
        return miuId;
    }

    /**
     * Setter for packets
     */
    public List<JsonPacketData> getPackets()
    {
        return packets;
    }

    /**
     * Constructor for creating a new miuPacket
     */
    public JsonPacketListForMiu(int miuid, List<JsonPacketData> packets)
    {
        this.miuId = miuid;
        this.packets = packets;
    }
}
