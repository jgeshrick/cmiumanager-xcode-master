/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.mdce.common.internal.servicestatus.model.ServiceStatus;
import com.neptunetg.mdce.common.internal.servicestatus.service.InternalApiLibraryVersionInfo;
import com.neptunetg.mdce.common.internal.servicestatus.service.ServiceStatusService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Provides MSR statistics
 */
@RestController
public class ServiceStatusServiceController implements ServiceStatusService
{
    private static final String INTEGRATION_URL_SERVICE_STATUS = "/service/" + URL_SERVICE_STATUS;

    @Value("#{envProperties['env.name']}")
    String envName;

    @RequestMapping(INTEGRATION_URL_SERVICE_STATUS)
    @Override
    public ServiceStatus getServiceStatus()
    {
        ServiceStatus ret = new ServiceStatus();

        ret.setLibraryVersion(InternalApiLibraryVersionInfo.LIBRARY_VERSION);
        ret.setEnvironmentName(envName);

        return ret;
    }
}
