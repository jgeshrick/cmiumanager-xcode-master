/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.miulifecycle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.List;

/**
 * The root for a collection of MIU lifecycle histories.
 */
public class JsonMiuLifecycleHistoryCollection
{
    @JsonProperty("mius")
    private List<JsonMiuLifecycleHistory> miuLifecycleHistoryList;

    public List<JsonMiuLifecycleHistory> getMiuLifecycleHistoryList()
    {
        return miuLifecycleHistoryList;
    }

    public void setMiuLifecycleHistoryList(List<JsonMiuLifecycleHistory> miuLifecycleHistoryList)
    {
        this.miuLifecycleHistoryList = miuLifecycleHistoryList;
    }

    /**
     * Builds an instance of JsonMiuLifecycleHistoryCollection using the supplied list of MiuLifecycleStates.
     * @param lifecycleStates The list of MiuLifecycleStates from which to build the collection.
     * @return A new JsonMiuLifecycleHistoryCollection, containing lifecycle history by MIU ID.
     */
    public static JsonMiuLifecycleHistoryCollection from(List<MiuLifecycleState> lifecycleStates)
    {
        JsonMiuLifecycleHistoryCollection result = new JsonMiuLifecycleHistoryCollection();
        List<JsonMiuLifecycleHistory> miuHistoryList = new ArrayList<>();
        result.setMiuLifecycleHistoryList(miuHistoryList);
        MultiValueMap<MiuId, JsonMiuLifecycleState> lifecycleStateMultiMap = new LinkedMultiValueMap<>();

        // Use a multi-map to group the results by MIU ID.
        for (MiuLifecycleState lifecycleState : lifecycleStates)
        {
            JsonMiuLifecycleState jsonLifecycleState = new JsonMiuLifecycleState(
                    lifecycleState.getLifecycleState(), lifecycleState.getTransitionInstant());

            lifecycleStateMultiMap.add(lifecycleState.getMiuId(), jsonLifecycleState);
        }

        // Convert the multi-map for use in the JsonMiuLifecycleHistoryCollection.
        for (MiuId miuId : lifecycleStateMultiMap.keySet())
        {
            JsonMiuLifecycleHistory miuHistory = new JsonMiuLifecycleHistory();
            miuHistory.setMiuId(miuId.numericValue());
            miuHistory.setMiuLifecycleStates(lifecycleStateMultiMap.get(miuId));
            miuHistoryList.add(miuHistory);
        }

        return result;
    }
}
