/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.json.commandhistory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.mdce.common.data.model.DynamoItemPacketType;
import com.neptunetg.mdce.integration.utility.DateTimeUtility;

import javax.xml.bind.DatatypeConverter;
import java.time.Instant;

/**
    JSON object for a representing a single packet for get command history.
*/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonGetCommandHistoryPacket
{
    @JsonProperty("send_date")
    private Instant sendDate;

    @JsonProperty("insert_date")
    private Instant insertDate;

    @JsonProperty("source_device_type")
    private String sourceDeviceType;

    @JsonProperty("target_device_type")
    private String targetDeviceType;

    @JsonProperty("packet_type")
    private int packetType;

    @JsonProperty("packet")
    private String packet;

    public static JsonGetCommandHistoryPacket from(MiuCommand miuCommand)
    {
        JsonGetCommandHistoryPacket result = new JsonGetCommandHistoryPacket();
        result.setSendDate(miuCommand.getCommandFulfilledTime());
        result.setInsertDate(miuCommand.getCommandEnteredTime());
        result.setSourceDeviceType("MDCE");     //fixme: get this value from packet
        result.setTargetDeviceType("CMIU");     //fixme: get this value from packet
        result.setPacketType(DynamoItemPacketType.CMIU_COMMAND_CONFIGURATION.getId());              //fixme Packet type ID according to the target device type, need to parse packet data to get packet type
        result.setPacket(DatatypeConverter.printHexBinary(miuCommand.getCommandParams()));

        return result;
    }

    @JsonSerialize(converter=DateTimeUtility.InstantToStringConverter.class)
    public Instant getSendDate()
    {
        return sendDate;
    }

    @JsonDeserialize(converter=DateTimeUtility.DateTimeToInstantConverter.class)
    public void setSendDate(Instant sendDate)
    {
        this.sendDate = sendDate;
    }

    @JsonSerialize(converter=DateTimeUtility.InstantToStringConverter.class)
    public Instant getInsertDate()
    {
        return insertDate;
    }

    @JsonDeserialize(converter=DateTimeUtility.DateTimeToInstantConverter.class)
    public void setInsertDate(Instant insertDate)
    {
        this.insertDate = insertDate;
    }

    public String getSourceDeviceType()
    {
        return sourceDeviceType;
    }

    public void setSourceDeviceType(String sourceDeviceType)
    {
        this.sourceDeviceType = sourceDeviceType;
    }

    public String getTargetDeviceType()
    {
        return targetDeviceType;
    }

    public void setTargetDeviceType(String targetDeviceType)
    {
        this.targetDeviceType = targetDeviceType;
    }

    public int getPacketType()
    {
        return packetType;
    }

    public void setPacketType(int packetType)
    {
        this.packetType = packetType;
    }

    public String getPacket()
    {
        return packet;
    }

    public void setPacket(String packet)
    {
        this.packet = packet;
    }
}
