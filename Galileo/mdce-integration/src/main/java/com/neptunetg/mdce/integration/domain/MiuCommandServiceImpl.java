/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.common.data.miu.command.MiuCommandDetail;
import com.neptunetg.common.data.miu.command.MiuCommandRepository;
import com.neptunetg.mdce.integration.audit.AuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of command service
 */
@Service
@Transactional
public class MiuCommandServiceImpl implements MiuCommandService
{
    private final MiuCommandRepository commandRepository;

    private final AuditService auditService;

    @Autowired
    public MiuCommandServiceImpl(MiuCommandRepository commandRepository, AuditService auditService)
    {
        this.commandRepository = commandRepository;
        this.auditService = auditService;
    }

    @Override
    public List<MiuCommandDetail> getCommandHistory(MiuId miuId)
    {
        return commandRepository.getCommandHistory(miuId);
    }

    @Override
    public List<MiuCommandDetail> getInProgressCommand()
    {
        return commandRepository.getInProgressCommand();
    }

    @Override
    public List<MiuCommandDetail> getReceivedCommand()
    {
        return commandRepository.getReceivedCommand();
    }

    @Override
    public List<MiuCommandDetail> getCompletedCommand(int numberOfDaysAgo)
    {
        return commandRepository.getCompletedCommand(numberOfDaysAgo);
    }

    @Override
    public List<MiuCommandDetail> getRejectedCommand(int numberOfDaysAgo)
    {
        return commandRepository.getRejectedCommand(numberOfDaysAgo);
    }

    @Override
    public int createMiuCommand(MiuCommand command)
    {
        final int ret = commandRepository.createMiuCommand(command);
        auditService.logSendMiuCommand(command);
        return ret;

    }

    @Override
    public int recallCreatedAndQueuedMiuCommand(MiuId miuId)
    {
        return commandRepository.recallCreatedAndQueuedMiuCommand(miuId);
    }

    @Override
    public int rejectAcceptedMiuCommand(long commandId)
    {
        return commandRepository.rejectAcceptedMiuCommand(commandId);
    }

}