/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.scheduled.lifecycle.alert.service;

import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.EnumMap;
import java.util.Properties;

/**
 * Created by WJD1 on 23/05/2016.
 * A class to hold the settings for the lifecycle state alerts
 */
@Component
public class CmiuLifecycleAlertServiceThresholds
{
    //An EnumMap to hold the maximum time a CMIU should stay in the defined lifecycle states
    private final EnumMap<MiuLifecycleStateEnum, Duration> maximumTimeInState =
            new EnumMap<MiuLifecycleStateEnum, Duration>(MiuLifecycleStateEnum.class);

    //An EnumMap to hold the maximum time a CMIU can stay in a state since Activation
    private final EnumMap<MiuLifecycleStateEnum, Duration> maximumTimeSinceActivatedInState =
            new EnumMap<MiuLifecycleStateEnum, Duration>(MiuLifecycleStateEnum.class);

    @Autowired
    public CmiuLifecycleAlertServiceThresholds(@Qualifier("envProperties") Properties envProperties)
    {
        Duration duration = Duration.ofDays(Integer.parseInt(envProperties.getProperty("alert.lifecycle.stuck.duration.days")));
        maximumTimeInState.put(MiuLifecycleStateEnum.PREACTIVATED, duration);
        maximumTimeInState.put(MiuLifecycleStateEnum.ACTIVATED, duration);
        maximumTimeInState.put(MiuLifecycleStateEnum.HEARD, duration);
        maximumTimeInState.put(MiuLifecycleStateEnum.PREPREPOT, duration);
        maximumTimeInState.put(MiuLifecycleStateEnum.PREPOT, duration);
        maximumTimeInState.put(MiuLifecycleStateEnum.POSTPOT, duration);
        maximumTimeInState.put(MiuLifecycleStateEnum.CONFIRMED, duration);
        maximumTimeInState.put(MiuLifecycleStateEnum.SCANNED, Duration.ofDays(1));
        maximumTimeInState.put(MiuLifecycleStateEnum.SHIPPED, Duration.ofDays(7));
        maximumTimeInState.put(MiuLifecycleStateEnum.RMAED, Duration.ofDays(30));

        maximumTimeSinceActivatedInState.put(MiuLifecycleStateEnum.UNCLAIMED, Duration.ofDays(180));
    }

    public EnumMap<MiuLifecycleStateEnum, Duration> getMaximumTimeInState()
    {
        return maximumTimeInState;
    }

    public EnumMap<MiuLifecycleStateEnum, Duration> getMaximumTimeSinceActivatedInState()
    {
        return maximumTimeSinceActivatedInState;
    }
}
