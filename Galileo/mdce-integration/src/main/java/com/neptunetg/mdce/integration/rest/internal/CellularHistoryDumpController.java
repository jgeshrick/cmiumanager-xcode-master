/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.cns.model.ConnectionEventDetail;
import com.neptunetg.common.cns.model.UsageHistory;
import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.common.data.miu.cellular.CellularDeviceId;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.diagnostics.service.CnsDumpService;
import com.neptunetg.mdce.integration.cns.CellularNetworkServiceManager;
import com.neptunetg.mdce.integration.data.miu.MiuSearchRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.domain.CellularDeviceMonitoringService;
import com.neptunetg.mdce.integration.gui.common.EnvProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * GUI interface for downloading Cellular event file as CSV
 */
@Controller
@RequestMapping(value = "/service")
public class CellularHistoryDumpController
{
    private static final Logger log = LoggerFactory.getLogger(PacketDumpController.class);

    @Autowired
    private CellularNetworkServiceManager cellularNetworkServiceManager;

    @Autowired
    private CmiuRepository cmiuRepository;

    @Autowired
    private CellularDeviceMonitoringService cellularDeviceMonitoringService;

    @Autowired
    private MiuSearchRepository miuSearchRepository;

    @Autowired
    private EnvProperties envProperties;


    private final List<CellularDeviceDetail> getDeviceListFromParams(
            Integer miuIdParam,
            String iccidParam,
            String imeiParam,
            Integer siteIdParam
            ) throws MiuDataException
    {
        final List<CellularDeviceDetail> deviceDetails = new ArrayList<>();

        if (miuIdParam != null)
        {
            final MiuId miuId = MiuId.valueOf(miuIdParam);
            final CellularDeviceDetail cellularDeviceDetail = this.cmiuRepository.getCellularConfig(miuId);
            if (cellularDeviceDetail == null)
            {
                throw new MiuDataException("No device found with MIUID " + miuIdParam);
            }
            deviceDetails.add(cellularDeviceDetail);
        }
        else if (StringUtils.hasText(iccidParam))
        {
            final CellularDeviceDetail cellularDeviceDetail = cmiuRepository.getCellularConfigFromCellularDeviceIdentifier(CellularDeviceId.iccid(iccidParam));
            if (cellularDeviceDetail == null)
            {
                throw new MiuDataException("No device found with ICCID " + iccidParam);
            }
            deviceDetails.add(cellularDeviceDetail);
        }
        else if (StringUtils.hasText(imeiParam))
        {
            final CellularDeviceDetail cellularDeviceDetail = cmiuRepository.getCellularConfigFromCellularDeviceIdentifier(CellularDeviceId.imei(imeiParam));
            if (cellularDeviceDetail == null)
            {
                throw new MiuDataException("No device found with IMEI " + imeiParam);
            }
            deviceDetails.add(cellularDeviceDetail);
        }
        else if (siteIdParam != null)
        {
            miuSearchRepository.getMiusForUtility(SiteId.valueOf(siteIdParam)).forEach(md ->
                    {
                        try
                        {
                            final CellularDeviceDetail cellularDeviceDetail = this.cmiuRepository.getCellularConfig(md.getId());
                            if (cellularDeviceDetail != null)
                            {
                                deviceDetails.add(cellularDeviceDetail);
                            }
                        }
                        catch (Exception e)
                        {
                            log.error("Error looking up MIUs for site " + siteIdParam + ", MIU ID " + md.getId(), e);
                        }
                    }

            );
        }

        return deviceDetails;

    }

    private final String getDumpDescFromParams(
            Integer miuIdParam,
            String iccidParam,
            String imeiParam,
            Integer siteIdParam
    ) throws MiuDataException
    {
        String dumpDesc = "no_mius";

        if (miuIdParam != null)
        {
            dumpDesc = "miu_" + miuIdParam;
        }
        else if (StringUtils.hasText(iccidParam))
        {
            dumpDesc = "iccid_" + iccidParam;
        }
        else if (StringUtils.hasText(imeiParam))
        {
            dumpDesc = "imei_" + imeiParam;
        }
        else if (siteIdParam != null)
        {
            dumpDesc = "site_" + siteIdParam;
        }

        return dumpDesc;

    }

    /**
     * Get CMIU connection history from two date range. Assumes time start at 0:00 hour.
     * This request is recommended for getting long histories as it split record retrievals into multiple requests of
     * records on per day basis, and streaming the results out while retrieving the next day of records.
     *
     * @param response handler to response where CSV will be stream out
     * @param fromDate date of start of record in the format yyyy-mm-dd eg 2015-05-30
     * @param toDate   date (inclusive) of end of record in the format yyyy-mm-dd eg 2015-05-30
     * @throws IOException
     */
    @RequestMapping(value = CnsDumpService.URL_GET_CELLULAR_CONNECTION_HISTORY_WITH_DATE_RANGE, method = RequestMethod.GET)
    public void getCellularConnectionHistoryWithDateRange(@PathVariable(CnsDumpService.MNO) String mno,
                                                          @RequestParam(value = CnsDumpService.MIU_ID, required = false, defaultValue = "") Integer miuIdParam,
                                                          @RequestParam(value = CnsDumpService.ICCID, required = false, defaultValue = "") String iccidParam,
                                                          @RequestParam(value = CnsDumpService.IMEI, required = false, defaultValue = "") String imeiParam,
                                                          @RequestParam(value = CnsDumpService.SITE_ID, required = false, defaultValue = "") Integer siteIdParam,
                                                          HttpServletResponse response,
                                                          @RequestParam(value = CnsDumpService.FROM) String fromDate,
                                                          @RequestParam(value = CnsDumpService.TO) String toDate) throws IOException, InternalApiException, MiuDataException
    {

        final List<CellularDeviceDetail> deviceDetails = getDeviceListFromParams(miuIdParam, iccidParam, imeiParam, siteIdParam);

        final String dumpDesc = getDumpDescFromParams(miuIdParam, iccidParam, imeiParam, siteIdParam);

        //get connection history on per day basis
        final DateTimeFormatter dtf = DateTimeFormatter.ISO_ZONED_DATE_TIME;

        final ZonedDateTime startDate = ZonedDateTime.parse(fromDate, dtf);
        final ZonedDateTime endDate = ZonedDateTime.parse(toDate, dtf);

        final PrintWriter writer = response.getWriter();
        response.setContentType("text/csv");

        //Print first line: header
        writer.println(ConnectionEventDetail.getCsvHeader());

        final int maxDayPerRequest = 1; //use for date range per request

        for (CellularDeviceDetail device : deviceDetails)
        {
            //if the date range is big, we want to split that into multiple requests
            ZonedDateTime tempStartDate = startDate;
            ZonedDateTime tempEndDate = startDate.plusDays(maxDayPerRequest);

            final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(mno);

            do
            {
                if (tempEndDate.isAfter(endDate))
                {
                    tempEndDate = endDate;
                }

                log.debug("Retrieving connect events for date range {} - {}", tempStartDate, tempEndDate);
                try
                {
                    final List<ConnectionEventDetail> connectionEventDetailList = cellularNetworkService.getDeviceConnectionEvents(device.getIccid(),
                            Date.from(tempStartDate.toInstant()),
                            Date.from(tempEndDate.toInstant()));

                    if (connectionEventDetailList != null)
                    {
                        connectionEventDetailList.forEach(i -> writer.println(i.toCsv()));
                    }
                }
                catch (Exception e)
                {
                    writer.println("Error: " + e.getClass() + ": " + e.getMessage());
                }

                tempStartDate = tempEndDate.plusDays(1);    //set next start date to 1 day ahead since the usage data granularity is per day
                tempEndDate = tempStartDate.plusDays(maxDayPerRequest);

            }
            while (tempStartDate.isBefore(endDate));
        }

        writer.close();
    }


    @RequestMapping(value = CnsDumpService.URL_DOWNLOAD_CELLULAR_CONNECTION_HISTORY_BY_DATE_RANGE, method = RequestMethod.GET)
    public void downloadCellularUsageHistoryByDateRange(@PathVariable(CnsDumpService.MNO) String mno,
                                                        @RequestParam(value = CnsDumpService.MIU_ID, required = false, defaultValue = "") Integer miuIdParam,
                                                        @RequestParam(value = CnsDumpService.ICCID, required = false, defaultValue = "") String iccidParam,
                                                        @RequestParam(value = CnsDumpService.IMEI, required = false, defaultValue = "") String imeiParam,
                                                        @RequestParam(value = CnsDumpService.SITE_ID, required = false, defaultValue = "") Integer siteIdParam,
                                                        HttpServletResponse response,
                                                        @RequestParam(value = CnsDumpService.FROM) String fromDate,
                                                        @RequestParam(value = CnsDumpService.TO) String toDate) throws IOException, InternalApiException, MiuDataException
    {
        final int MAX_DAY_PER_REQUEST = 30; //request in 30 days block

        final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(mno);

        final List<CellularDeviceDetail> deviceDetails = getDeviceListFromParams(miuIdParam, iccidParam, imeiParam, siteIdParam);

        final String dumpDesc = getDumpDescFromParams(miuIdParam, iccidParam, imeiParam, siteIdParam);


        //get usage history on per day basis
        DateTimeFormatter dtf = DateTimeFormatter.ISO_ZONED_DATE_TIME;

        final ZonedDateTime startDate = ZonedDateTime.parse(fromDate, dtf);
        final ZonedDateTime endDate = ZonedDateTime.parse(toDate, dtf);

        final PrintWriter writer = response.getWriter();
        response.setContentType("text/csv");

        //Print first line: header
        writer.println(UsageHistory.getCsvHeader());

        for (CellularDeviceDetail device : deviceDetails)
        {
            //if the date range is big, we want to split that into multiple requests
            ZonedDateTime tempStartDate = startDate;
            ZonedDateTime tempEndDate = startDate.plusDays(MAX_DAY_PER_REQUEST);

            do
            {
                if (tempEndDate.isAfter(endDate))
                {
                    tempEndDate = endDate;
                }

                log.debug("Retrieving usage data for date range {} - {}", tempStartDate, tempEndDate);
                try
                {
                    List<UsageHistory> usageHistoryList = cellularNetworkService.getDeviceUsageHistory(device.getIccid(),
                            tempStartDate,
                            tempEndDate);

                    if (usageHistoryList != null)
                    {
                        usageHistoryList.forEach(i -> writer.println(i.toCsv()));
                    }
                }
                catch (Exception e)
                {
                    writer.println("Error: " + e.getClass() + ": " + e.getMessage());
                }

                tempStartDate = tempEndDate.plusDays(1);    //set next start date to 1 day ahead since the usage data granularity is per day
                tempEndDate = tempEndDate.plusDays(MAX_DAY_PER_REQUEST);

            }
            while (tempStartDate.isBefore(endDate));
        }

        writer.close();
    }

    /**
     * Attaches environment properties bean to model
     */
    @ModelAttribute("envProperties")
    public EnvProperties envProperties() {

        return envProperties;

    }


}
