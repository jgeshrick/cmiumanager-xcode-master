/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.miulifecycle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Contains the lifecycle history for an MIU.
 */
public class JsonMiuLifecycleHistory
{
    @JsonProperty("miu_id")
    private int miuId;

    @JsonProperty("lifecycle_history")
    private List<JsonMiuLifecycleState> miuLifecycleStates;

    public int getMiuId()
    {
        return miuId;
    }

    public void setMiuId(int miuId)
    {
        this.miuId = miuId;
    }

    public List<JsonMiuLifecycleState> getMiuLifecycleStates()
    {
        return miuLifecycleStates;
    }

    public void setMiuLifecycleStates(List<JsonMiuLifecycleState> miuLifecycleStates)
    {
        this.miuLifecycleStates = miuLifecycleStates;
    }

    /**
     * Returns a single MIU lifecycle history from the supplied list of MiuLifecycleStates.
     * @param miuId The MIU ID whose lifecycle history is supplied in the miuLifecycleStateList parameter.
     * @param miuLifecycleStateList The lifecycle history of the MIU.
     * @return A JsonMiuLifecycleHistory instance for the supplied MIU history information.
     */
    public static JsonMiuLifecycleHistory from(MiuId miuId, List<MiuLifecycleState> miuLifecycleStateList)
    {
        JsonMiuLifecycleHistory jsonMiuLifecycleHistory = new JsonMiuLifecycleHistory();
        jsonMiuLifecycleHistory.setMiuId(miuId.numericValue());

        List<JsonMiuLifecycleState> jsonMiuLifecycleStates = miuLifecycleStateList
                .stream()
                .map(miuLifecycleState -> new JsonMiuLifecycleState(
                    miuLifecycleState.getLifecycleState(), miuLifecycleState.getTransitionInstant()))
                .collect(Collectors.toList());

        jsonMiuLifecycleHistory.setMiuLifecycleStates(jsonMiuLifecycleStates);

        return jsonMiuLifecycleHistory;
    }
}
