/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.scheduled.r900.service;

import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;

import java.io.File;
import java.time.Instant;

public abstract class BaseR900TarService
{
    protected static final String LOCK = ".lock";

    protected static final String PROCESSED = "processed";
    protected static final String UNPROCESSED = "unprocessed";
    protected static final String ERROR = "error";

    protected File unProcessedFolder;
    protected File processedFolder;
    protected File errorFolder;

    public BaseR900TarService(String tarballDumpFolder)
    {
        this.processedFolder = ensureDirectoryExists(new File(tarballDumpFolder, PROCESSED));
        this.unProcessedFolder = ensureDirectoryExists(new File(tarballDumpFolder, UNPROCESSED));
        this.errorFolder = ensureDirectoryExists(new File(tarballDumpFolder, ERROR));
    }


    protected Instant getNow()
    {
        return Instant.now();
    }

    protected File createLockFile(File file)
    {
        final File lockFile = new File(file.getParent(), file.getName() + ".lock");
        try
        {
            lockFile.createNewFile();
        } catch (Exception e)
        {
            throw new MdceRestException("Error creating lock file " + lockFile.getAbsolutePath(), e);
        }

        return lockFile;
    }

    protected File ensureDirectoryExists(File folder)
    {
        if (!folder.exists())
        {
            folder.mkdirs();
        }
        return folder;
    }

    protected File ensureFileDoesNotExist(File file)
    {
        if (file.exists())
        {
            file.delete();
        }
        return file;
    }
}
