/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.scheduled.r900.service;

import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.heard.MiuUpdateRepository;
import com.neptunetg.common.data.util.JdbcDateUtil;
import com.neptunetg.common.packet.model.builder.R900MiuRssiSummary;
import com.neptunetg.mdce.integration.data.gateway.GatewayId;
import com.neptunetg.mdce.integration.data.gateway.GatewayRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Collects updates for R900 packets heard, and batch-updates them into the repository.
 *
 * Not multi-threaded or thread-safe
 */
public class R900RelationalDataSpooler implements AutoCloseable
{
    private static final Logger logger = LoggerFactory.getLogger(R900RelationalDataSpooler.class);

    private static final Set<String> GATEWAY_CONFIG_HEADER_NAMES = new HashSet<>(Arrays.asList(
            "MinimumStorageTime", "TransferInterval", "TransferOffsetTime",
            "FirstReportingStartTime", "FirstReportingEndTime", "FirstReportingTimeInterval",
            "SecondReportingStartTime", "SecondReportingEndTime", "SecondReportingTimeInterval",
            "ThirdReportingStartTime", "ThirdReportingEndTime", "ThirdReportingTimeInterval"));

    private static final int MAX_BATCH_SIZE = 200; //insert / update 200 rows at once

    private final MiuUpdateRepository miuUpdateRepository;

    private final GatewayRepository gatewayRepository;

    private final MiuLastHeardTimeBpss miuLastHeardTimeBpss = new MiuLastHeardTimeBpss();
    private final MiuLastHeardTimeRssiBpss miuLastHeardTimeRssiBpss = new MiuLastHeardTimeRssiBpss();

    private int[] miuBatch = new int[MAX_BATCH_SIZE];
    private Instant[] heardTimeBatch = new Instant[MAX_BATCH_SIZE];
    private R900MiuRssiSummary[] rssiSummaries = new R900MiuRssiSummary[MAX_BATCH_SIZE];

    private int batchSize = 0;

    private SiteId gatewayConfigSiteId = null;
    private String gatewayConfigName;
    private final Map<String, Integer> gatewayConfigNumericValues = new HashMap<>(); //keys are defined in GATEWAY_CONFIG_HEADER_NAMES


    public R900RelationalDataSpooler(MiuUpdateRepository miuUpdateRepository, GatewayRepository gatewayRepository)
    {
        this.miuUpdateRepository = miuUpdateRepository;
        this.gatewayRepository = gatewayRepository;
    }

    public void recordMiuHeard(int miuId, Instant heardTime, R900MiuRssiSummary rssiSummary)
    {
        miuBatch[batchSize] = miuId;
        heardTimeBatch[batchSize] = heardTime;
        rssiSummaries[batchSize] = rssiSummary;
        batchSize++;
        if (batchSize >= MAX_BATCH_SIZE)
        {
            flush();
        }
    }

    private void flush()
    {
        try
        {
            miuUpdateRepository.updateMiuHeardTimeAndRssiBatch(miuLastHeardTimeBpss,
                    miuLastHeardTimeRssiBpss,
                    MiuType.R900);
        }
        catch (Exception e)
        {
            logger.error("Error recording last heard time and RSSI from " + batchSize + " MIUs", e);
        }
        finally
        {
            batchSize = 0;
        }
    }

    @Override
    public void close() throws Exception
    {
        flush();


    }

    public void recordGatewayHeader(String headerKey, String headerValue)
    {
        try
        {
            if ("SiteID".equals(headerKey))
            {
                gatewayConfigSiteId = SiteId.valueOf(headerValue);
            }
            else if ("Name".equals(headerKey))
            {
                gatewayConfigName = headerValue;
            }
            else if (GATEWAY_CONFIG_HEADER_NAMES.contains(headerKey))
            {
                gatewayConfigNumericValues.put(headerKey, Integer.valueOf(headerValue));
            }
        }
        catch (NumberFormatException e)
        {
            logger.error("Invalid number in gateway header with key " + headerKey + ", value " + headerValue, e);
        }
    }

    public void writeGatewayHeardStatsAndWriteConfig(GatewayId gatewayId, Instant minInsertDate, Instant maxInsertDate,
                                                     int miusHeard, int ookPacketsHeard, int fskPacketsHeard,
                                                     int configPacketsHeard, boolean storeR900Packets)
    {

        final Integer gwDbId = gatewayRepository.updateGatewayConfig(gatewayId, minInsertDate,
                gatewayConfigSiteId, gatewayConfigName, gatewayConfigNumericValues);

        Integer transferInterval = gatewayConfigNumericValues.get("TransferInterval");
        if (transferInterval == null)
        {
            transferInterval = Integer.valueOf(120); //2h by default
        }
        gatewayRepository.updateGatewayHeardTimeAndStats(gatewayId, gwDbId, minInsertDate, maxInsertDate, miusHeard,
                ookPacketsHeard, fskPacketsHeard, configPacketsHeard, transferInterval, storeR900Packets);

    }

    private class MiuLastHeardTimeBpss implements BatchPreparedStatementSetter
    {
        @Override
        public int getBatchSize()
        {
            return batchSize;
        }

        @Override
        public void setValues(PreparedStatement preparedStatement, int i) throws SQLException
        {
            final Object lastHeardTimeToBind = JdbcDateUtil.bindDate(heardTimeBatch[i]);
            preparedStatement.setObject(1, lastHeardTimeToBind);
            preparedStatement.setObject(2, lastHeardTimeToBind);
            preparedStatement.setObject(3, lastHeardTimeToBind);

            preparedStatement.setInt(4, miuBatch[i]);
        }
    }


    private class MiuLastHeardTimeRssiBpss implements BatchPreparedStatementSetter
    {
        @Override
        public int getBatchSize()
        {
            return batchSize;
        }

        @Override
        public void setValues(PreparedStatement preparedStatement, int i) throws SQLException
        {
            preparedStatement.setInt(1, miuBatch[i]);
            preparedStatement.setObject(2, JdbcDateUtil.bindDate(heardTimeBatch[i]));
            final R900MiuRssiSummary rssiSummary = rssiSummaries[i];
            preparedStatement.setInt(3, rssiSummary.getPacketCount());
            preparedStatement.setDouble(4, rssiSummary.getRssiAverage());
            preparedStatement.setDouble(5, rssiSummary.getRssiMin());
            preparedStatement.setDouble(6, rssiSummary.getRssiMax());

        }
    }

}
