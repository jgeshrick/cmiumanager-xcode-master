/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
    JSON object for a single MIU configuration
 *  "{\"site_id\":" + siteId + ",\"mius\":
 *      [{\"miu_id\":"+  testCmiuId+"}]}"
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonMiuOwnership
{
    @JsonProperty("site_id")
    private long siteId;

    @JsonProperty("mius")
    private List<JsonMiuActiveFlag> miuList;

    public long getSiteId()
    {
        return siteId;
    }

    public void setSiteId(long siteId)
    {
        this.siteId = siteId;
    }

    public List<JsonMiuActiveFlag> getMiuList()
    {
        return miuList;
    }

    public void setMiuList(List<JsonMiuActiveFlag> miuList)
    {
        this.miuList = miuList;
    }
}
