/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.miu;


import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.domain.InsertMiuDetailsBpss;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;

import java.util.List;
import java.util.Map;

public interface MiuSearchRepository
{

    List<MiuDetails> getMius(Map<String, String> options);

    int getMiusCount(Map<String, String> options);

    List<MiuDetails> getAllMius();

    /**
     * Get a list of miu details for a given utility id (site id)
     * @param utilId the utility id
     * @return list of MIU
     */
    List<MiuDetails> getMiusForUtility(SiteId utilId);


    /**
     * Get the miu details for a given utility id (site id) and miu ID
     * @param siteId the id of the site
     * @param miuId the MIU ID to search for
     * @return MIU Details, or null if no MIU found for that site
     */
    MiuDetails getMiuIfInUtility(SiteId siteId, MiuId miuId);

    /**
     * Get the miu details for a given miu ID
     * @param miuId the MIU ID to search for
     * @return MIU Details, or null if no MIU found for that site
     */
    MiuDetails getMiu(MiuId miuId);


}