/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.verizon.callback.CallbackRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A temporary service to store Verizon WNS callback events in a string list.
 * This provides a in memory storage for any WNS callbacks for debug purpose. Will be retried once the proper backend
 * implementation to the callback has been implemented.
 */
@Service
public class WnsCallbackEventsService
{
    final private List<CallbackRequest> callbackEvents = new ArrayList<>();

    private static final Logger log = LoggerFactory.getLogger(WnsCallbackEventsService.class);

    /**
     * Add callback objects to the in memory list.
     * @param request callbackrequest data received when Verizon UWS callback the server.
     */
    public synchronized void addEvents(CallbackRequest request)
    {
        this.callbackEvents.add(request);
    }

    /**
     * Return a list of callback events for examination.
     * @return list of callback.
     */
    public synchronized List<CallbackRequest> getCallbackEvents()
    {
        List<CallbackRequest> clonedCopy = new ArrayList<>(this.callbackEvents.size());
        Collections.copy(clonedCopy, this.callbackEvents);

        return clonedCopy;
    }

    public synchronized void clearEvents()
    {
        callbackEvents.clear();
    }
}
