/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.rest.json.refdata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * JsonReferenceDataInfo
 *
 * "info": {
 *  "account_id": "A6UJ9A000RJV",
 *  "account_name": "Hidden Valley Ranch Assoc.",
 *  "address1": "P.O. Box 4490",
 *  "address2": "NULL",
 *  "address3": "NULL",
 *  "city": "Pagosa Springs",
 *  "state": "CO",
 *  "postal_code": "81157",
 *  "country": "Usa",
 *  "main_phone": "NULL",
 *  "fax": "NULL",
 *  "owner": "37062",
 *  "account_manager": "Mike Lavin",
 *  "regional_manager": "Jeff Olsen",
 *  "type": "Private - Non-Water",
 *   "sub_type": "NULL",
 *   "status": "Active",
 *   "primary_contact": {
 *    "name": "NULL, NULL",
 *    "work_phone": "NULL",
 *    "title": "NULL"
 *   },
 *  "parent_customer_number": "7277400"
 * }
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonReferenceDataInfo
{

    @JsonProperty("account_id")
    private String accountId;

    @JsonProperty("system_id")
    private String systemId;

    @JsonProperty("customer_number")
    private String customerNumber;

    @JsonProperty("account_name")
    private String accountName;

    @JsonProperty("address1")
    private String address1;

    @JsonProperty("address2")
    private String address2;

    @JsonProperty("address3")
    private String address3;

    @JsonProperty("city")
    private String city;

    @JsonProperty("state")
    private String state;

    @JsonProperty("postal_code")
    private String postalCode;

    @JsonProperty("country")
    private String country;

    @JsonProperty("main_phone")
    private String mainPhone;

    @JsonProperty("fax")
    private String fax;

    @JsonProperty("sales_territory_owner")
    private String salesTerritoryOwner;

    @JsonProperty("account_manager")
    private String accountManager;

    @JsonProperty("regional_manager")
    private String regionalManager;

    @JsonProperty("type")
    private String type;

    @JsonProperty("sub_type")
    private String subType;

    @JsonProperty("status")
    private String status;

    @JsonProperty("primary_contact")
    private JsonReferenceDataInfoContact primaryContact = new JsonReferenceDataInfoContact();

    @JsonProperty("active_secondary_contacts")
    private List<JsonReferenceDataInfoContact> activeSecondaryContacts = new ArrayList<>();

    @JsonProperty("customer_numbers")
    private List<String> customerNumbers = new ArrayList<>();

    @JsonProperty("parent_customer_number")
    private String parentCustomerNumber;

    public JsonReferenceDataInfo()
    {

    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getSystemId()
    {
        return systemId;
    }

    public void setSystemId(String systemId)
    {
        this.systemId = systemId;
    }

    public String getCustomerNumber()
    {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber)
    {
        this.customerNumber = customerNumber;
    }

    public String getAccountName()
    {
        return accountName;
    }

    public void setAccountName(String accountName)
    {
        this.accountName = accountName;
    }

    public String getAddress1()
    {
        return address1;
    }

    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    public String getAddress2()
    {
        return address2;
    }

    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    public String getAddress3()
    {
        return address3;
    }

    public void setAddress3(String address3)
    {
        this.address3 = address3;
    }

    public String getMainPhone()
    {
        return mainPhone;
    }

    public void setMainPhone(String mainPhone)
    {
        this.mainPhone = mainPhone;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax = fax;
    }

    public String getSalesTerritoryOwner()
    {
        return salesTerritoryOwner;
    }

    public void setSalesTerritoryOwner(String salesTerritoryOwner)
    {
        this.salesTerritoryOwner = salesTerritoryOwner;
    }

    public String getAccountManager()
    {
        return accountManager;
    }

    public void setAccountManager(String accountManager)
    {
        this.accountManager = accountManager;
    }

    public String getRegionalManager()
    {
        return regionalManager;
    }

    public void setRegionalManager(String regionalManager)
    {
        this.regionalManager = regionalManager;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getSubType()
    {
        return subType;
    }

    public void setSubType(String subType)
    {
        this.subType = subType;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public JsonReferenceDataInfoContact getPrimaryContact()
    {
        return primaryContact;
    }

    public void setPrimaryContact(JsonReferenceDataInfoContact primaryContact)
    {
        this.primaryContact = primaryContact;
    }

    public List<JsonReferenceDataInfoContact> getActiveSecondaryContacts()
    {
        return activeSecondaryContacts;
    }

    public void setActiveSecondaryContacts(List<JsonReferenceDataInfoContact> activeSecondaryContacts)
    {
        this.activeSecondaryContacts = activeSecondaryContacts;
    }

    public List<String> getCustomerNumbers()
    {
        return customerNumbers;
    }

    public void setCustomerNumbers(List<String> customerNumbers)
    {
        this.customerNumbers = customerNumbers;
    }

    public String getParentCustomerNumber()
    {
        return parentCustomerNumber;
    }

    public void setParentCustomerNumber(String parentCustomerNumber)
    {
        this.parentCustomerNumber = parentCustomerNumber;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }


    public static JsonReferenceDataInfo from(MdceReferenceDataInfo info)
    {
        JsonReferenceDataInfo jsonReferenceDataInfo = new JsonReferenceDataInfo();

        jsonReferenceDataInfo.setParentCustomerNumber(info.getParentCustomerNumber());
        jsonReferenceDataInfo.setAccountName(info.getAccountName());
        jsonReferenceDataInfo.setAccountId(info.getAccountId());
        jsonReferenceDataInfo.setSystemId(info.getSystemId());

        jsonReferenceDataInfo.setAccountManager(info.getAccountManager());
        jsonReferenceDataInfo.setRegionalManager(info.getRegionalManager());
        jsonReferenceDataInfo.setSalesTerritoryOwner(info.getSalesTerritoryOwner());

        jsonReferenceDataInfo.setMainPhone(info.getMainPhone());
        jsonReferenceDataInfo.setFax(info.getFax());

        jsonReferenceDataInfo.setAddress1(info.getAddress1());
        jsonReferenceDataInfo.setAddress2(info.getAddress2());
        jsonReferenceDataInfo.setAddress3(info.getAddress3());
        jsonReferenceDataInfo.setCity(info.getCity());
        jsonReferenceDataInfo.setState(info.getState());
        jsonReferenceDataInfo.setPostalCode(info.getPostalCode());
        jsonReferenceDataInfo.setCountry(info.getCountry());

        jsonReferenceDataInfo.setType(info.getType());
        jsonReferenceDataInfo.setSubType(info.getSubType());
        jsonReferenceDataInfo.setStatus(info.getStatus());

        jsonReferenceDataInfo.setCustomerNumbers(info.getCustomerNumbers());

        jsonReferenceDataInfo.setPrimaryContact(JsonReferenceDataInfoContact.from(info.getPrimaryContact()));
        jsonReferenceDataInfo.setActiveSecondaryContacts(JsonReferenceDataInfoContact.from(info.getActiveSecondaryContacts()));

        return jsonReferenceDataInfo;
    }

}