/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.rest.json.refdata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * JsonReferenceDataUtility
 *
 *  "site_id": 32,
 *   "info": {
 *     ...
 *   }
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonReferenceDataUtility
{

    @JsonProperty("site_id")
    private int siteId;

    @JsonProperty("info")
    private JsonReferenceDataInfo info;

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public JsonReferenceDataInfo getInfo()
    {
        return info;
    }

    public void setInfo(JsonReferenceDataInfo info)
    {
        this.info = info;
    }


    public static List<JsonReferenceDataUtility> from(List<MdceReferenceDataUtility> utilities)
    {
        List<JsonReferenceDataUtility> jsonReferenceDataUtilities = new ArrayList<>();

        for (MdceReferenceDataUtility utility : utilities) {
            jsonReferenceDataUtilities.add(from(utility));
        }

        return jsonReferenceDataUtilities;
    }

    private static JsonReferenceDataUtility from(MdceReferenceDataUtility utility)
    {
        JsonReferenceDataUtility jsonReferenceDataUtility = new JsonReferenceDataUtility();

        jsonReferenceDataUtility.setSiteId(utility.getSiteId());
        jsonReferenceDataUtility.setInfo(JsonReferenceDataInfo.from(utility.getInfo()));

        return jsonReferenceDataUtility;
    }
}
