/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonMiuActiveFlag
{

    public static final String ALREADY_OWNED = "already_owned";
    public static final String SUCCESSFULLY_CLAIMED = "successfully_claimed";
    public static final String CONFLICT = "conflict";

    @JsonProperty("miu_id")
    private long miuId;

    @JsonProperty("active")
    private String active;

    @JsonProperty("type")
    private String type;

    @JsonProperty("owned")
    private String owned;

    @JsonProperty("set_ownership_result")
    private String setOwnershipResult;

    @JsonIgnore
    private Boolean isClaimable;

    @JsonIgnore
    private Boolean alreadyExists;

    @JsonIgnore
    private Integer oldSiteId;

    public Boolean getAlreadyExists()
    {
        return alreadyExists;
    }

    public void setAlreadyExists(Boolean alreadyExists)
    {
        this.alreadyExists = alreadyExists;
    }

    public Integer getOldSiteId()
    {
        return oldSiteId;
    }

    public void setOldSiteId(Integer oldSiteId)
    {
        this.oldSiteId = oldSiteId;
    }

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public String getActive()
    {
        return active;
    }

    public void setActive(String active)
    {
        this.active = active;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Boolean getIsClaimable()
    {
        return isClaimable;
    }

    public void setIsClaimable(Boolean isClaimable)
    {
        this.isClaimable = isClaimable;
    }

    public String getOwned()
    {
        return owned;
    }

    public void setOwned(String owned)
    {
        this.owned = owned;
    }

    public String getSetOwnershipResult()
    {
        return setOwnershipResult;
    }

    public void setSetOwnershipResult(String setOwnershipResult)
    {
        this.setOwnershipResult = setOwnershipResult;
    }
}
