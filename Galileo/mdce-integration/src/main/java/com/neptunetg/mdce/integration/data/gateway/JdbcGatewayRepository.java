/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.gateway;

import com.neptunetg.mdce.common.internal.gateway.model.GatewayHeardPackets;
import com.neptunetg.mdce.common.internal.gateway.model.GatewayMonthlyStats;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.data.site.SiteRepository;
import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

import static com.neptunetg.common.data.util.JdbcDateUtil.bindDate;
import static com.neptunetg.common.data.util.JdbcDateUtil.getInstant;

/**
 * Repository for MySql database - gateways
 */
@Repository
public class JdbcGatewayRepository implements GatewayRepository
{
    private static final String GATEWAY_ID = "gateway_id";
    public static final String SITE_ID = "siteId";
    public static final String REGIONAL_MANAGER = "regionalManager";
    public static final String LOCATION = "location";

    public static final DateTimeFormatter DTO_TIMESTAMP_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss zzz").withZone(ZoneId.systemDefault());

    private final JdbcTemplate db;
    private final SiteRepository siteRepository;
    private final int maxResults = 100;


    @Autowired
    public JdbcGatewayRepository(JdbcTemplate db, SiteRepository siteRepository)
    {
        this.db = db;
        this.siteRepository = siteRepository;
    }


    @Override
    public void updateGatewayHeardTimeAndStats(GatewayId gatewayId, Integer gatewayDbId, Instant minInsertDate, Instant maxInsertDate,
                                               int miusHeard, int ookPacketsHeard, int fskPacketsHeard, int configPacketsHeard,
                                               int transferInterval, boolean storeR900Packets)
    {
        String query = "INSERT INTO gateway_heard_packets (gateway_id, heard_time, mius_heard, ook_packets_heard, fsk_packets_heard, config_packets_heard) " +
                "VALUES (?, FROM_UNIXTIME(?), ?, ?, ?, ?)";
        this.db.update(query, gatewayId.toString(), maxInsertDate.getEpochSecond(), miusHeard, ookPacketsHeard, fskPacketsHeard, configPacketsHeard);

        if(storeR900Packets)
        {
            query = "UPDATE gateway_details SET last_insert_date=FROM_UNIXTIME(?), first_insert_date=IFNULL(first_insert_date,FROM_UNIXTIME(?)), last_heard_time=FROM_UNIXTIME(?), " +
                    "next_sync_time=FROM_UNIXTIME(?), next_sync_time_warning=FROM_UNIXTIME(?), next_sync_time_error=FROM_UNIXTIME(?) " +
                    "WHERE id=?";

            final Long minInsertDateEpochSecond = Long.valueOf(minInsertDate.getEpochSecond());
            final Long maxInsertDateEpochSecond = Long.valueOf(maxInsertDate.getEpochSecond());
            this.db.update(query, maxInsertDateEpochSecond, minInsertDateEpochSecond, maxInsertDateEpochSecond,
                    Long.valueOf(minInsertDate.plus(transferInterval, ChronoUnit.MINUTES).getEpochSecond()),
                    Long.valueOf(minInsertDate.plus(transferInterval * 2L, ChronoUnit.MINUTES).getEpochSecond()), //TODO - check on error / warning periods
                    Long.valueOf(minInsertDate.plus(transferInterval * 5L, ChronoUnit.MINUTES).getEpochSecond()), //TODO - check on error / warning periods
                    gatewayDbId
            );
        }
        else
        {
             query = "UPDATE gateway_details SET last_heard_time=FROM_UNIXTIME(?), " +
                    "next_sync_time=FROM_UNIXTIME(?), next_sync_time_warning=FROM_UNIXTIME(?), next_sync_time_error=FROM_UNIXTIME(?) " +
                    "WHERE id=?";

            final Long maxInsertDateEpochSecond = Long.valueOf(maxInsertDate.getEpochSecond());
            this.db.update(query, maxInsertDateEpochSecond,
                    Long.valueOf(minInsertDate.plus(transferInterval, ChronoUnit.MINUTES).getEpochSecond()),
                    Long.valueOf(minInsertDate.plus(transferInterval * 2L, ChronoUnit.MINUTES).getEpochSecond()), //TODO - check on error / warning periods
                    Long.valueOf(minInsertDate.plus(transferInterval * 5L, ChronoUnit.MINUTES).getEpochSecond()), //TODO - check on error / warning periods
                    gatewayDbId
            );
        }

    }

    public List<GatewayDetails> getGatewaysForUtilityPaged(SiteId siteId, int page)
    {
        final String sql = "SELECT gateway_id, first_insert_date, last_heard_time, " +
                "next_sync_time, next_sync_time_warning, next_sync_time_error, " +
                "gateway_active FROM mdce.gateway_details WHERE site_id=?" +
                " LIMIT ?," + maxResults;

        return this.db.query(sql,
                (rs, rownum) ->
                        new GatewayDetails(
                                GatewayId.valueOf(rs.getString(GATEWAY_ID)),
                                siteId,
                                getTime(rs, "first_insert_date"),
                                getTime(rs, "last_heard_time"),
                                getTime(rs, "next_sync_time"),
                                getTime(rs, "next_sync_time_warning"),
                                getTime(rs, "next_sync_time_error"),
                                rs.getBoolean("gateway_active")
                        )
                ,
                siteId.numericWrapperValue(),
                page * maxResults);
    }

    @Override
    public List<GatewayDetails> getGatewaysForUtility(SiteId siteId)
    {
        final String sql = "SELECT gateway_id, first_insert_date, last_heard_time, next_sync_time, " +
                "next_sync_time_warning, next_sync_time_error, gateway_active " +
                "FROM mdce.gateway_details WHERE site_id=?";

        return this.db.query(sql,
                (rs, rownum) ->
                        new GatewayDetails(
                                GatewayId.valueOf(rs.getString(GATEWAY_ID)),
                                siteId,
                                getTime(rs, "first_insert_date"),
                                getTime(rs, "last_heard_time"),
                                getTime(rs, "next_sync_time"),
                                getTime(rs, "next_sync_time_warning"),
                                getTime(rs, "next_sync_time_error"),
                                rs.getBoolean("gateway_active")
                        )
                ,
                siteId.numericWrapperValue());
    }

    private List<GatewayDetails> getGatewaysForRegionalManager(String regionalManager)
    {
        String sql = "SELECT gateway_id, gateway_details.site_id, first_insert_date, last_heard_time, next_sync_time, " +
                "next_sync_time_warning, next_sync_time_error, gateway_active " +
                "FROM mdce.gateway_details INNER JOIN " +
                "(SELECT site_list.site_id FROM mdce.site_list INNER JOIN " +
                "(SELECT site_id FROM mdce.ref_data_utilities INNER JOIN " +
                "(SELECT ref_data_info_id FROM mdce.ref_data_info WHERE regional_manager=?) AS table1 " +
                "ON table1.ref_data_info_id = ref_data_utilities.info_id) AS table2 " +
                "ON table2.site_id = site_list.site_id) AS table3 " +
                "ON table3.site_id = gateway_details.site_id";

        return this.db.query(sql, (rs, rownum) ->
                new GatewayDetails(
                        GatewayId.valueOf(rs.getString(GATEWAY_ID)),
                        new SiteId(rs.getInt("site_id")),
                        getTime(rs, "first_insert_date"),
                        getTime(rs, "last_heard_time"),
                        getTime(rs, "next_sync_time"),
                        getTime(rs, "next_sync_time_warning"),
                        getTime(rs, "next_sync_time_error"),
                        rs.getBoolean("gateway_active"))
                        , regionalManager);
    }

    private List<GatewayDetails> getGatewaysForLocation(String location)
    {
        String sql = "SELECT gateway_id, gateway_details.site_id, first_insert_date, last_heard_time, next_sync_time, " +
                "next_sync_time_warning, next_sync_time_error, gateway_active " +
                "FROM mdce.gateway_details INNER JOIN " +
                "(SELECT site_list.site_id FROM mdce.site_list INNER JOIN " +
                "(SELECT site_id FROM mdce.ref_data_utilities INNER JOIN " +
                "(SELECT ref_data_info_id FROM mdce.ref_data_info WHERE state=?) AS rdi " +
                "ON rdi.ref_data_info_id = ref_data_utilities.info_id) AS rdu " +
                "ON rdu.site_id = site_list.site_id) AS sl " +
                "ON sl.site_id = gateway_details.site_id";

        return this.db.query(sql, (rs, rownum) ->
                new GatewayDetails(
                        GatewayId.valueOf(rs.getString(GATEWAY_ID)),
                        new SiteId(rs.getInt("site_id")),
                        getTime(rs, "first_insert_date"),
                        getTime(rs, "last_heard_time"),
                        getTime(rs, "next_sync_time"),
                        getTime(rs, "next_sync_time_warning"),
                        getTime(rs, "next_sync_time_error"),
                        rs.getBoolean("gateway_active"))
                        , location);
    }

    private List<GatewayDetails> getGatewayByGatewayId(GatewayId gatewayId)
    {
        final String sql = "SELECT gateway_id, site_id, first_insert_date, " +
                "last_heard_time, next_sync_time, next_sync_time_warning, " +
                "next_sync_time_error, gateway_active " +
                "FROM mdce.gateway_details WHERE gateway_id=?";

        return this.db.query(sql,
                (rs, rownum) ->
                        new GatewayDetails(
                                GatewayId.valueOf(rs.getString(GATEWAY_ID)),
                                new SiteId(rs.getInt("site_id")),
                                getTime(rs, "first_insert_date"),
                                getTime(rs, "last_heard_time"),
                                getTime(rs, "next_sync_time"),
                                getTime(rs, "next_sync_time_warning"),
                                getTime(rs, "next_sync_time_error"),
                                rs.getBoolean("gateway_active")
                        )
                ,
                gatewayId.toString());
    }

    private String getTime(ResultSet rs, String columnTitle) throws SQLException
    {
        return formatInstant(getInstant(rs, columnTitle));
    }

    private String formatInstant(Instant instant)
    {
        if (instant == null)
        {
            return null;
        }
        else
        {
            return DTO_TIMESTAMP_FORMATTER.format(instant);
        }
    }

    @Override
    public List<GatewayDetails> getGateways(Map<String, String> options)
    {
        final String gatewayIdIn = getOption(options, "gatewayId");

        if (gatewayIdIn != null && !gatewayIdIn.equals(""))
        {
            final GatewayId gatewayId = new GatewayId(gatewayIdIn);
            return getGatewayByGatewayId(gatewayId);
        }

        final int page = Integer.parseInt(getOptionOrDefault(options, "page", "0"));
        final String siteId = getOption(options, SITE_ID);

        if(siteId != null && !siteId.equals(""))
        {
            return getGatewaysForUtilityPaged(new SiteId(Integer.parseInt(siteId)), page);
        }

        final String regionalmanager = getOption(options, REGIONAL_MANAGER);

        if(regionalmanager != null && !regionalmanager.equals(""))
        {
            return getGatewaysForRegionalManager(regionalmanager);
        }

        final String location = getOption(options, LOCATION);

        if(location != null && !location.equals(""))
        {
            return getGatewaysForLocation(location);
        }

        return getGatewaysForAllUtilityPaged(page);
    }

    @Override
    public Integer getGatewaysCount(Map<String, String> options)
    {
        final String gatewayIdIn = getOption(options, GATEWAY_ID);
        if(gatewayIdIn != null && !gatewayIdIn.equals(""))
        {
            return 1;
        }

        final String siteId = getOption(options, SITE_ID);
        if(siteId != null && !siteId.equals(""))
        {
            return getMiusForUtilityCount(Integer.parseInt(siteId));
        }

        return getMiusForAllUtilityCount();
    }

    private Integer getMiusForUtilityCount(int siteId)
    {
        return this.db.queryForObject("SELECT COUNT(*) FROM mdce.gateway_details WHERE site_id=?",
                new Object[]{siteId},
                Integer.class
        );
    }

    private Integer getMiusForAllUtilityCount()
    {
        return this.db.queryForObject("SELECT COUNT(*) FROM mdce.gateway_details",
                Integer.class
        );
    }

    private List<GatewayDetails> getGatewaysForAllUtilityPaged(int page)
    {
        final String sql = "SELECT gateway_id, site_id, first_insert_date, " +
                "last_heard_time, next_sync_time, next_sync_time_warning, " +
                "next_sync_time_error, gateway_active FROM mdce.gateway_details" +
                " LIMIT ?," + maxResults;

        return this.db.query(sql,
                (rs, rownum) ->
                        new GatewayDetails(
                                GatewayId.valueOf(rs.getString(GATEWAY_ID)),
                                SiteId.valueOf(rs.getInt("site_id")),
                                getTime(rs, "first_insert_date"),
                                getTime(rs, "last_heard_time"),
                                getTime(rs, "next_sync_time"),
                                getTime(rs, "next_sync_time_warning"),
                                getTime(rs, "next_sync_time_error"),
                                rs.getBoolean("gateway_active")
                        ), page * maxResults);
    }

    @Override
    public List<GatewayDetails> getGatewaysForAllUtility()
    {
        final String sql = "SELECT gateway_id, site_id, first_insert_date, " +
                "last_heard_time, next_sync_time, next_sync_time_warning, " +
                "next_sync_time_error, gateway_active FROM mdce.gateway_details";

        return this.db.query(sql,
                (rs, rownum) ->
                        new GatewayDetails(
                                GatewayId.valueOf(rs.getString(GATEWAY_ID)),
                                SiteId.valueOf(rs.getInt("site_id")),
                                getTime(rs, "first_insert_date"),
                                getTime(rs, "last_heard_time"),
                                getTime(rs, "next_sync_time"),
                                getTime(rs, "next_sync_time_warning"),
                                getTime(rs, "next_sync_time_error"),
                                rs.getBoolean("gateway_active")));
    }

    @Override
    public GatewayHeardPackets getLatestGatewayHeardPackets(GatewayId gatewayId)
    {
        String sql = "SELECT * FROM mdce.gateway_heard_packets WHERE gateway_id=? " +
                "ORDER BY heard_time DESC LIMIT 1";

        try
        {
            return this.db.queryForObject(sql, (rs, rownum) -> {
                GatewayHeardPackets gatewayHeardPackets = new GatewayHeardPackets();
                gatewayHeardPackets.setGatewayId(gatewayId.toString());
                gatewayHeardPackets.setHeardTime(getTime(rs, "heard_time"));
                gatewayHeardPackets.setMiusHeard(rs.getInt("mius_heard"));
                gatewayHeardPackets.setOokPacketsHeard(rs.getInt("ook_packets_heard"));
                gatewayHeardPackets.setFskPacketsHeard(rs.getInt("fsk_packets_heard"));
                gatewayHeardPackets.setConfigPacketsHeard(rs.getInt("config_packets_heard"));
                return gatewayHeardPackets;
            }, gatewayId.toString());
        }
        catch (EmptyResultDataAccessException ex)
        {
            return null;
        }
    }

    @Override
    public GatewayMonthlyStats getLatestGatewayMonthlyStats(GatewayId gatewayId)
    {
        String sql = "SELECT * FROM mdce.gateway_monthly_stats WHERE gateway_id=? " +
                "ORDER BY 'year' DESC, 'month' DESC LIMIT 1";

        try
        {
            return this.db.queryForObject(sql, (rs, rownum) -> {
                GatewayMonthlyStats gatewayMonthlyStats = new GatewayMonthlyStats();
                gatewayMonthlyStats.setGatewayId(rs.getString(GATEWAY_ID));
                gatewayMonthlyStats.setNumExpected(rs.getInt("num_expected"));
                gatewayMonthlyStats.setAvgMiusHeard(rs.getFloat("avg_mius_heard"));
                gatewayMonthlyStats.setMinMiusHeard(rs.getInt("min_mius_heard"));
                gatewayMonthlyStats.setMaxMiusHeard(rs.getInt("max_mius_heard"));
                gatewayMonthlyStats.setAvgOokPacketsHeard(rs.getFloat("avg_ook_packets_heard"));
                gatewayMonthlyStats.setMinOokPacketsHeard(rs.getInt("min_ook_packets_heard"));
                gatewayMonthlyStats.setMaxOokPacketsHeard(rs.getInt("max_ook_packets_heard"));
                gatewayMonthlyStats.setAvgFskPacketsHeard(rs.getFloat("avg_fsk_packets_heard"));
                gatewayMonthlyStats.setMinFskPacketsHeard(rs.getInt("min_fsk_packets_heard"));
                gatewayMonthlyStats.setMaxFskPacketsHeard(rs.getInt("max_fsk_packets_heard"));
                gatewayMonthlyStats.setAvgConfigPacketsHeard(rs.getFloat("avg_config_packets_heard"));
                gatewayMonthlyStats.setMinConfigPacketsHeard(rs.getInt("min_config_packets_heard"));
                gatewayMonthlyStats.setMaxConfigPacketsHeard(rs.getInt("max_config_packets_heard"));
                return gatewayMonthlyStats;
            }, gatewayId.toString());
        }
        catch (EmptyResultDataAccessException ex)
        {
            return null;
        }
    }

    /**
     * Get owner of Gateway
     *
     * @param gatewayId gateway ID
     * @return Site ID owning this gateway, or null if not claimed
     */
    @Override
    public SiteId getGatewayOwner(GatewayId gatewayId)
    {
        String sql = "SELECT site_id FROM mdce.gateway_details WHERE gateway_id = ?";

        final List<Integer> siteIds = this.db.query(sql, (rs, rownum) -> {
            return rs.getInt("site_id");
        }, new Object[]{gatewayId.toString()});

        if (siteIds.size() == 1)
        {
            return SiteId.valueOf(siteIds.get(0));
        }
        if (siteIds.size() > 1)
        {
            throw new MdceRestException("Duplicate siteIds found for gateway_id " + gatewayId.toString());
        }
        return null;
    }

    @Override
    public int updateGatewayOwnerDetails(GatewayId gatewayId, SiteId newSiteId)
    {

        siteRepository.ensureSiteExists(newSiteId);

        String updateSql = "UPDATE mdce.gateway_details SET site_id = ? WHERE gateway_id = ?";

        int rowsUpdated = this.db.update(updateSql, newSiteId.numericWrapperValue(), gatewayId.toString());

        if (rowsUpdated == 0) //non-existent Gateway
        {

            //add to gateway_details
            String sql = "INSERT INTO mdce.gateway_details (gateway_id, site_id) VALUES (?,?)";

            rowsUpdated = this.db.update(sql, gatewayId.toString(), newSiteId.numericWrapperValue());

        }

        return rowsUpdated;

    }

    @Override
    public Integer updateGatewayConfig(GatewayId gatewayId, Instant gatewayConfigDate, SiteId gatewayConfigSiteId, String gatewayConfigName, Map<String, Integer> gatewayConfigNumericValues)
    {
        String query = "SELECT id FROM gateway_details WHERE gateway_id = ?";

        Integer gwDbId;
        try
        {
            gwDbId = this.db.queryForObject(query, Integer.class, gatewayId.toString());
        }
        catch (EmptyResultDataAccessException e)
        {
            gwDbId = null;
        }
        if (gwDbId == null)
        {
            updateGatewayOwnerDetails(gatewayId, gatewayConfigSiteId);
            gwDbId = this.db.queryForObject(query, Integer.class, gatewayId.toString());
        }
        final Map<String, Integer> gwn = gatewayConfigNumericValues;
        final Object[] headerValuesForBinding = {gatewayConfigName, gatewayConfigSiteId.numericWrapperValue(),
                gwn.get("MinimumStorageTime"), gwn.get("TransferInterval"), gwn.get("TransferOffsetTime"),
                gwn.get("FirstReportingStartTime"), gwn.get("FirstReportingEndTime"), gwn.get("FirstReportingTimeInterval"),
                gwn.get("SecondReportingStartTime"), gwn.get("SecondReportingEndTime"), gwn.get("SecondReportingTimeInterval"),
                gwn.get("ThirdReportingStartTime"), gwn.get("ThirdReportingEndTime"), gwn.get("ThirdReportingTimeInterval")};

        //check for matching current config
        query = "SELECT count(*) FROM gateway_config gc " +
                "WHERE gc.gateway_db_id = ? AND gc.from_date = (select max(g2.from_date) from gateway_config g2 where g2.gateway_db_id = gc.gateway_db_id) " +
                "AND IFNULL(gc.name,'')=IFNULL(?,'') AND IFNULL(gc.site_id,-1)=IFNULL(?,-1) " +
                "AND IFNULL(gc.MinimumStorageTime,-1)=IFNULL(?,-1) AND IFNULL(gc.TransferInterval,-1)=IFNULL(?,-1) AND IFNULL(gc.TransferOffsetTime,-1)=IFNULL(?,-1) " +
                "AND IFNULL(gc.FirstReportingStartTime,-1)=IFNULL(?,-1) AND IFNULL(gc.FirstReportingEndTime,-1)=IFNULL(?,-1) AND IFNULL(gc.FirstReportingTimeInterval,-1)=IFNULL(?,-1) " +
                "AND IFNULL(gc.SecondReportingStartTime,-1)=IFNULL(?,-1) AND IFNULL(gc.SecondReportingEndTime,-1)=IFNULL(?,-1) AND IFNULL(gc.SecondReportingTimeInterval,-1)=IFNULL(?,-1) " +
                "AND IFNULL(gc.ThirdReportingStartTime,-1)=IFNULL(?,-1) AND IFNULL(gc.ThirdReportingEndTime,-1)=IFNULL(?,-1) AND IFNULL(gc.ThirdReportingTimeInterval,-1)=IFNULL(?,-1)";

        Object[] bindvars = new Object[headerValuesForBinding.length + 1];
        bindvars[0] = gwDbId;
        System.arraycopy(headerValuesForBinding, 0, bindvars, 1, headerValuesForBinding.length);

        final Integer currentConfigMatches = this.db.queryForObject(query, Integer.class, bindvars);

        if (currentConfigMatches.intValue() == 0)
        {
            //insert new row with latest config
            query = "INSERT INTO gateway_config (gateway_db_id, from_date, name, site_id, " +
                    "MinimumStorageTime, TransferInterval, TransferOffsetTime, " +
                    "FirstReportingStartTime, FirstReportingEndTime, FirstReportingTimeInterval, " +
                    "SecondReportingStartTime, SecondReportingEndTime, SecondReportingTimeInterval, " +
                    "ThirdReportingStartTime, ThirdReportingEndTime, ThirdReportingTimeInterval) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            bindvars = new Object[headerValuesForBinding.length + 2];
            bindvars[0] = gwDbId;
            bindvars[1] = bindDate(gatewayConfigDate);
            System.arraycopy(headerValuesForBinding, 0, bindvars, 2, headerValuesForBinding.length);

            this.db.update(query, bindvars);
        }

        return gwDbId;
    }

    private String getOptionOrDefault(Map<String, String> options, String option, String defaultValue)
    {
        final String optionValue = getOption(options, option);
        if (StringUtils.hasText(optionValue))
        {
            return optionValue;
        }
        return defaultValue;
    }

    private String getOption(Map<String, String> options, String option)
    {
        if (options.containsKey(option))
        {
            return options.get(option);
        }
        return null;
    }

}
