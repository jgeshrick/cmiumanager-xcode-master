/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.integration.utility;


import com.neptunetg.common.lora.parser.LoraPacketParser;
import com.neptunetg.common.lora.pdu.BasicConfigPacket;
import com.neptunetg.common.lora.pdu.L900Packet;
import com.neptunetg.common.lora.pdu.PacketId;

/**
 * Utility for extracting battery voltage from L900 PDU
 */
public class L900BatteryVoltage
{
    private static final Double VOLTAGE_3_6 = Double.valueOf(3.6);
    private static final Double VOLTAGE_3_3 = Double.valueOf(3.3);
    private static final Double VOLTAGE_3_1 = Double.valueOf(3.1);
    private static final Double VOLTAGE_3_0 = Double.valueOf(3.0);

    public static Double extractBatteryVoltageFromPdu(byte[] pdu)
    {
        final LoraPacketParser packetParser = new LoraPacketParser();
        final L900Packet packet = packetParser.parsePacket(pdu);
        final PacketId packetId = packet.getPacketId();

        if (PacketId.BASIC_CONFIG_PACKET.equals(packetId))
        {
            final BasicConfigPacket configPacket = (BasicConfigPacket)packet;
            switch (configPacket.getBatteryStatus())
            {
                    /*
13.1.1.1 Battery status bits (Battery voltage= (ADCVAL / 4096 ) * 2.5 * 3 )) FUTURE
Status bits Meaing Detailed description
00 Battery >3.4V (Default value if not enabled)
01 Between 3.2V-3.39V Battery is below normal voltage
10 Between 3.0V-3.19V. Battery is well below normal voltage.
11 Below 3.0V Battery voltage below 3.0V and may soon fail.

Value to store for each of these:

00 = 3.6V
01 = 3.3V
10 = 3.1V
11 = 3.0V
                     */
                case 0:
                {
                    return VOLTAGE_3_6;
                }
                case 1:
                {
                    return VOLTAGE_3_3;
                }
                case 2:
                {
                    return VOLTAGE_3_1;
                }
                case 3:
                {
                    return VOLTAGE_3_0;
                }

            }
        }
        else if (PacketId.DETAILED_CONFIG_PACKET.equals(packetId))
        {
            //TODO: detailed config packet
            throw new UnsupportedOperationException("L900 detailed config packet stats TODO!");
        }

        return null;
        //otherwise, no battery information in the packet
    }
}
