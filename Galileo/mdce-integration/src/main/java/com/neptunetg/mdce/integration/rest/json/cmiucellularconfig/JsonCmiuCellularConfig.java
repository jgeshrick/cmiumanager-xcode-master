/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.rest.json.cmiucellularconfig;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;

import java.time.Instant;

/**
 * JsonCmiuCellularConfig
 *  "{\"miu_id\":" + testCmiuId + ", "
 * + "\"modem_config\": {\"vendor\":\"Telit\", \"firmware_revision\": \"0.0.0.0\",\"imei\":\"123456789101112\"}, "
 * + "\"sim_config\":{\"iccid\":0, \"imsi\":15, \"msidn\":0}}"
 */
public class JsonCmiuCellularConfig
{
    @JsonProperty("modem_config")
    public JsonCmiuCellularConfigModemConfig modemConfig = new JsonCmiuCellularConfigModemConfig();
    @JsonProperty("sim_config")
    public JsonCmiuCellularConfigSimConfig simConfig = new JsonCmiuCellularConfigSimConfig();
    @JsonProperty("miu_id")
    private long miuId = 0;

    /**
     * Construct a JSON representation class from RDB model of cellular device details
     *
     * @param cellularDeviceDetail
     * @return
     */
    public static JsonCmiuCellularConfig from(CellularDeviceDetail cellularDeviceDetail)
    {
        JsonCmiuCellularConfig cellularConfig = new JsonCmiuCellularConfig();

        //todo add code that gets miu details from the database
        cellularConfig.setMiuId(cellularDeviceDetail.getMiuId().numericValue());

        cellularConfig.modemConfig.setFirmwareRevision(cellularDeviceDetail.getModemConfig().getFirmwareRevision());
        cellularConfig.modemConfig.setImei(cellularDeviceDetail.getModemConfig().getImei());
        cellularConfig.modemConfig.setVendor(cellularDeviceDetail.getModemConfig().getVendor());

        cellularConfig.simConfig.setIccid(cellularDeviceDetail.getIccid());
        cellularConfig.simConfig.setImsi(cellularDeviceDetail.getImsi());
        cellularConfig.simConfig.setMsidn(cellularDeviceDetail.getMsisdn());

        return cellularConfig;
    }

    public static CellularDeviceDetail from(JsonCmiuCellularConfig cellularConfig, CellularDeviceDetail cellularDeviceDetail)
    {
        cellularDeviceDetail.setMiuId(MiuId.valueOf(cellularConfig.getMiuId()));
        cellularDeviceDetail.setIccid(cellularConfig.simConfig.getIccid());
        cellularDeviceDetail.setImsi(cellularConfig.simConfig.getImsi());
        cellularDeviceDetail.setMsisdn(cellularConfig.simConfig.getMsidn());

        cellularDeviceDetail.getModemConfig().setVendor(cellularConfig.modemConfig.getVendor());
        cellularDeviceDetail.getModemConfig().setFirmwareRevision(cellularConfig.modemConfig.getFirmwareRevision());
        cellularDeviceDetail.getModemConfig().setImei(cellularConfig.modemConfig.getImei());

        cellularDeviceDetail.setLastUpdateTime(Instant.now());

        return cellularDeviceDetail;
    }

    public long getMiuId()
    {
        return this.miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }
}
