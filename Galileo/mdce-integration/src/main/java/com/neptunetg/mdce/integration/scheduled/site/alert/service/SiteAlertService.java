/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.scheduled.site.alert.service;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;

import java.util.List;

/**
 * Created by ibarron on 7/24/17.
 */
public interface SiteAlertService
{
    /**
     * Finds sites that have not contacted mdce between a configurable amount of time.
     *
     * @param siteConnectionCheckHours max amount of hours an site should be talking
     *        to mdce anything after that is an issue.
     * @param siteConnectionIgnoreAfterHours max amount of hours a site can be inactive
     *        before we declare inactive and ignore.
     * @return list of sites
     */
    List<SiteDetails> getSitesNotRequestingDataWithinXHours(
            int siteConnectionCheckHours,
            int siteConnectionIgnoreAfterHours) throws InternalApiException;
}
