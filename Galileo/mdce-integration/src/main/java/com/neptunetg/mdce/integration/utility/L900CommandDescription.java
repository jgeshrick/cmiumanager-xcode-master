/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */
package com.neptunetg.mdce.integration.utility;

import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.common.lora.parser.LoraPacketParser;
import com.neptunetg.common.lora.pdu.CommandPacket;
import com.neptunetg.common.lora.pdu.L900Packet;
import com.neptunetg.common.lora.pdu.TimeOffsetCommandPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class L900CommandDescription implements CommandDescriptionService {

        private static final Logger log = LoggerFactory.getLogger(L900CommandDescription.class);

        /**
         * Inspect command packet and generate a description of the command for human consumption.
         *
         * @param command command packet
         * @return A summary of the command for display purpose.
         */
        public String getCommandDetails(MiuCommand command) {

            byte[] rawPacket = command.getCommandParams();

            String description = "Unknown L900 Packet";

            LoraPacketParser parser = new LoraPacketParser();

            // Parse command
            L900Packet packet = parser.parsePacket(rawPacket);

            // Generate description
            if (packet instanceof CommandPacket)
            {

                if (packet instanceof TimeOffsetCommandPacket)
                {
                    TimeOffsetCommandPacket timeOffsetCommandPacket = (TimeOffsetCommandPacket) packet;

                    description = String.format("Time offset command packet: %d",
                            timeOffsetCommandPacket.getTimeOffset().getSeconds());

                }
                else
                {
                    description = "Generic L900 command packet";
                }


            }

            return description;

        }

}
