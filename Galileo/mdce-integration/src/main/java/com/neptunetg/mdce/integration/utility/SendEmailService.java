/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.utility;

import com.neptunetg.mdce.common.internal.email.model.EmailMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by WJD1 on 11/09/2015.
 * Class for sending alert emails to recipients
 */
@Service
public class SendEmailService
{
    private static final Logger log = LoggerFactory.getLogger(SendEmailService.class);

    @Value("#{envProperties['mail.smtp.host']}")
    private String host;

    @Value("#{envProperties['mail.smtp.port']}")
    private String port;

    @Value("#{envProperties['mail.from']}")
    private String from;

    @Value("#{envProperties['mail.redirectToLocalBuffer']}")
    private boolean redirectToLocalBuffer;

    /**
     * The email buffer to which messages are redirected when redirectToLocalBuffer is true.
     */
    private final EmailBuffer emailBuffer;

    @Autowired
    public SendEmailService(EmailBuffer emailBuffer)
    {
        this.emailBuffer = emailBuffer;
    }

    /**
     * Function for sending email
     * The send function does not indicate success or failure
     * @param recipientAddresses Email addresses to send email to
     * @param subject Email subject
     * @param contents The contents of the email
     */
    public void sendEmail(String[] recipientAddresses,
                                 String subject,
                                 String contents)
    {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);

        Session session = Session.getInstance(properties);

        try
        {
            InternetAddress[] recipientInternetAddressList = new InternetAddress[recipientAddresses.length];

            for(int i=0; i<recipientAddresses.length; i++)
            {
                recipientInternetAddressList[i] = (InternetAddress.parse(recipientAddresses[i])[0]);
            }

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, recipientInternetAddressList);
            message.setSubject(subject);
            message.setText(contents);

            if (redirectToLocalBuffer)
            {
                // Send to local buffer.
                EmailMessage emailMessage = new EmailMessage(recipientAddresses, subject, contents);
                emailBuffer.setEmailMessage(emailMessage);
            }
            else
            {
                // Send to SMTP service.
                Transport.send(message);
            }
        }
        catch (MessagingException e)
        {
            log.error("Error sending email", e);
            throw new RuntimeException(e);
        }
        catch (Exception e)
        {
            log.error("Error sending email", e);
            throw new RuntimeException(e);
        }
    }

}
