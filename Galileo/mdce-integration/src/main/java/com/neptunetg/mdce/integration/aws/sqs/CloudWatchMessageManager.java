/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.aws.sqs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Monitor incoming cloud watch alerts (from SQS) and transfer into mdce alert management system.
 */
@Service
public class CloudWatchMessageManager implements AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(CloudWatchMessageManager.class);

    private final MdceIpcSubscribeService awsQueueService;

    @Autowired
    private AlertService alertService;

    @Autowired
    public CloudWatchMessageManager(MdceIpcSubscribeService awsQueueService)
    {
        this.awsQueueService = awsQueueService;

        //create an observer to listen to AWS SQS for <env>-cloudwatch message sent from cloudwatch alarm through SNS
        awsQueueService.register(this, MdceIpcSubscribeService.CLOUDWATCH_ALARMS, Duration.ofSeconds(10L), 10);
        //also listen for internal MDCE alerts
        awsQueueService.register(this, MdceIpcSubscribeService.ALERTS, Duration.ofSeconds(10L), 10);
    }

    /**
     * Handler when SQS message to the registered SQS queue name has been received.
     * @param message the message body
     * @param messageId the message id
     * @param messageReceipt message receipt
     * @param cancellationToken Ignored as this is not a long-running callback
     * @return true to delete the message from SQS
     */
    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        try
        {
            log.debug("Adding Alert To Database. Message: {}", message);

            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> sqsMessage = mapper.readValue(message, Map.class);

            if(sqsMessage.containsKey("Subject") &&
                    (sqsMessage.get("Subject").toString().contains("ALARM") ||
                            sqsMessage.get("Subject").toString().contains("OK")))
            {
                processCloudWatchMessage(sqsMessage.get("Message").toString());
            }
            else
            {
                processMiscAlertMessage(message);
            }

            log.debug("Alert successfully added");

            return true;    //return true for SQS message to be removed
        }
        catch (Exception e)
        {
            log.error("Error processing received message", e);
        }

        return false;   //SQS message will not be removed from queue
    }

    /**
     *
     * @param message
     * @throws IOException
     * @throws ParseException
     * @throws InternalApiException
     */
    private void processCloudWatchMessage(String message) throws IOException, ParseException, InternalApiException
    {
        ObjectMapper mapper = new ObjectMapper();

        Map<String, Object> snsMessage = mapper.readValue(message, Map.class);

        //Todo Decide if the alert is a warning or an error

        final AlertSource alertSource = AlertSource.cloudwatchAlarm(snsMessage.get("AlarmName").toString());

        AlertDetails alertDetails = new AlertDetails();
        alertDetails.setAlertLevel(AlertLevel.WARNING);
        alertDetails.setAlertSource(alertSource.getAlertId());

        if(snsMessage.get("NewStateValue").toString().contains("OK"))
        {
            alertDetails.setAlertState(AlertState.STALE);
        }
        else
        {
            alertDetails.setAlertState(AlertState.NEW);
        }

        ZonedDateTime stateChangeTime = ZonedDateTime.parse(snsMessage.get("StateChangeTime").toString(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));

        alertDetails.setAlertCreationDate(stateChangeTime);
        alertDetails.setRecentMessage(snsMessage.get("NewStateReason").toString());

        alertService.addAlertAndSendEmail(alertDetails);
    }

    /**
     * Processes messages from non-cloudwatch sources
     * @param message a JSON string containing alert Source, Level and Message
     * @throws IOException
     * @throws InternalApiException
     */
    private void processMiscAlertMessage(String message) throws IOException, InternalApiException
    {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> parsedMessage = mapper.readValue(message, Map.class);

        if(parsedMessage.containsKey("Source") &&
                parsedMessage.containsKey("Level") &&
                parsedMessage.containsKey("Message"))
        {
            AlertDetails alertDetails = new AlertDetails();

            if(parsedMessage.get("Level").toString().contains("Error"))
            {
                alertDetails.setAlertLevel(AlertLevel.ERROR);
                alertDetails.setAlertState(AlertState.NEW);
            }
            else if(parsedMessage.get("Level").toString().contains("Warning"))
            {
                alertDetails.setAlertLevel(AlertLevel.WARNING);
                alertDetails.setAlertState(AlertState.NEW);
            }
            else
            {
                alertDetails.setAlertLevel(AlertLevel.WARNING);
                alertDetails.setAlertState(AlertState.STALE);
            }

            alertDetails.setAlertSource(parsedMessage.get("Source").toString());
            alertDetails.setAlertCreationDate(ZonedDateTime.now());
            alertDetails.setRecentMessage(parsedMessage.get("Message").toString());

            alertService.addAlertAndSendEmail(alertDetails);
        }
    }
}


