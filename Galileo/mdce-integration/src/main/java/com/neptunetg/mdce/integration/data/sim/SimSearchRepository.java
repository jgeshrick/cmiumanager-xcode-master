/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.data.sim;

import java.util.List;
import java.util.Map;
import com.neptunetg.common.data.miu.MiuId;

public interface SimSearchRepository {

    List<SimAndModemDetails> getCanByMiuid(MiuId miu_id);

    List<SimAndModemDetails> getCanByIccid(String iccid);

    List<SimAndModemDetails> getCanByImei(String imei);

    List<SimDetails> getSims(Map<String, String> options);

    int getSimCount(Map<String, String> options);

    List<String> getSimIccids(Map<String, String> options);

    /**
     * Gets all distinct SIM cellular networks.
     * @return A list of all cellular networks.
     */
    List<String> getSimMnos();
}
