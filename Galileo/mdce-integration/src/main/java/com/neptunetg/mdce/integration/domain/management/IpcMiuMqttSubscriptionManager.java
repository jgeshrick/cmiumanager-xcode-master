/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.domain.management;

import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.domain.mqtt.MiuMqttSubscriptionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Uses IPC (SQS) to manage MQTT subscriptions
 */
@Service
public class IpcMiuMqttSubscriptionManager implements MiuMqttSubscriptionManager
{
    private final MdceIpcPublishService ipc;

    @Autowired
    public IpcMiuMqttSubscriptionManager(MdceIpcPublishService ipc)
    {
        this.ipc = ipc;
    }

    /**
     * If this is a CMIU, send a message for it to be subscribed on all brokers
     *
     * @param miuId MIU ID
     * @param type  Type of MIU
     */
    @Override
    public void subscribeMiuMqttIfApplicable(MiuId miuId, MiuType type)
    {
        if (MiuType.CMIU.equals(type))
        {
            ipc.sendMessage(MdceIpcPublishService.SUBSCRIBE_MIU_ALL_BROKERS, miuId.toString() +
                    " , " + type.toString());
        }
    }

}
