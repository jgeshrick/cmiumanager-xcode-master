/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.miu;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.domain.mqtt.MiuMqttSubscriptionManager;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.data.site.SiteRepository;
import com.neptunetg.mdce.integration.domain.AddMiuOwnerHistoryBpss;
import com.neptunetg.mdce.integration.domain.InsertMiuDetailsBpss;
import com.neptunetg.mdce.integration.domain.UpdateMiuSiteIdBpss;
import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import com.neptunetg.mdce.integration.rest.json.JsonMiuActiveFlag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static com.neptunetg.common.data.util.JdbcDateUtil.getInstant;

/**
 * MIU ownership Repository for MySql database
 */
@Repository
public class JdbcMiuOwnershipRepository implements MiuOwnershipRepository
{
    public static final String MIU_OWNER_HISTORY_ID_COLUMN = "miu_owner_history_id";
    public static final String MIU_ID_COLUMN = "miu_id";
    public static final String SITE_ID_COLUMN = "site_id";
    public static final String TIMESTAMP_COLUMN = "timestamp";
    public static final String CREATED_COLUMN = "created";

    private static final Logger log = LoggerFactory.getLogger(JdbcMiuOwnershipRepository.class);
    // String Constants
    private static final String SELECT_SITE_ID_FROM_MDCE_MIU_DETAILS_WHERE_MIU_ID = "SELECT site_id FROM mdce.miu_details WHERE miu_id = ?";
    private static final String INSERT_MIU_DETAILS = "INSERT INTO mdce.miu_details (miu_id, site_id, miu_type) VALUES (?,?,?)";
    private static final String UPDATE_MIU_DETAILS = "UPDATE mdce.miu_details SET site_id = ?, meter_active = ? WHERE miu_id = ?";

    private static final String INSERT_CMIU_CONFIG_HISTORY_FOR_OWNER_CHANGE =
            "INSERT INTO cmiu_config_history (miu_id, cmiu_config, timestamp, received_from_cmiu, change_note, site_id, billable) " +
            "            (SELECT miu_id, current_config, now(), FALSE, 'CMIU owner changed', ?, ? FROM cmiu_config_mgmt WHERE miu_id=?)";

    /**
     * Appends the site_id to the site_id history.
     * Parameters: miu_id, site_id, timestamp.
     */
    private static final String INSERT_MIU_OWNER_HISTORY =
            "INSERT INTO mdce.miu_owner_history (`miu_id`, `site_id`, `timestamp`, `created`) " +
            "VALUES (?, ?, ?, NOW())";

    private static final String SELECT_MIU_OWNER_HISTORY_PAGED =
            "SELECT `miu_owner_history_id`, `miu_id`, `site_id`, `timestamp`, `created` " +
            "FROM mdce.miu_owner_history " +
            "WHERE miu_owner_history_id > ? " +
            "ORDER BY `miu_owner_history_id` ASC " +
            "LIMIT ? ";

    // fields
    private final JdbcTemplate db;
    private final CmiuRepository cmiuRepository;
    private final MiuMqttSubscriptionManager mqttSubscriptionManager;
    private final SiteRepository siteRepository;


    @Autowired
    public JdbcMiuOwnershipRepository(JdbcTemplate db, CmiuRepository cmiuRepository, MiuMqttSubscriptionManager mqttSubscriptionManager,
                                      SiteRepository siteRepository)
    {
        this.db = db;
        this.cmiuRepository = cmiuRepository;
        this.mqttSubscriptionManager = mqttSubscriptionManager;
        this.siteRepository = siteRepository;
    }


    @Override
    public boolean isMiuBillable(MiuId miuId)
    {
        String sql = "SELECT meter_active FROM mdce.miu_details WHERE miu_id=?";
        final String isBillable = this.db.queryForObject(sql, new Object[]{miuId.numericValue()}, String.class);

        return !isBillable.isEmpty() && isBillable.equals("Y");
    }


    /**
     * Update meter active flag for miu
     * @param miuId
     * @param isBillable
     * @return
     */
    @Override
    public int updateBilling(MiuId miuId, boolean isBillable)
    {
        String query = "UPDATE mdce.miu_details SET meter_active = ? WHERE miu_id=?";
        return this.db.update(query, isBillable ? "Y" : "N", miuId.numericValue());
    }


    /**
     * Get owner of MIU
     *
     * @param miuId miu id
     * @return Site ID owning this MIU, or null if not claimed
     */
    @Override
    public SiteId getMiuOwner(MiuId miuId)
    {
        final List<Integer> siteIds = this.db.queryForList(SELECT_SITE_ID_FROM_MDCE_MIU_DETAILS_WHERE_MIU_ID, new Object[]{miuId.numericWrapperValue()}, Integer.class);

        if (siteIds.isEmpty())
        {
            // unclaimed
            return null;
        }
        else if (siteIds.size() == 1)
        {
            Integer sid = siteIds.get(0);

            if (sid == null)
            {
                log.error("Query of miu {} returns NULL site id!!", miuId);
                return null;
            }
            else
            {
                return new SiteId(sid);
            }
        }

        throw new MdceRestException("Duplicate miuId's found: " + miuId.numericValue());

    }


    /**
     * Function to populate a list of MIU Json objects with the existing owners of each MIU
     * @param miuJsonList List of MIU IDs to get site IDs for
     */
    @Override
    public void populateExistingMiuOwnerBatch(List<JsonMiuActiveFlag> miuJsonList)
    {
        for(JsonMiuActiveFlag miuJson : miuJsonList)
        {
            final List<Integer> siteIds = this.db.queryForList(SELECT_SITE_ID_FROM_MDCE_MIU_DETAILS_WHERE_MIU_ID, new Object[]{miuJson.getMiuId()}, Integer.class);

            if (siteIds.isEmpty())
            {
                // unclaimed
                miuJson.setOldSiteId(null);
            }
            else if (siteIds.size() == 1)
            {
                Integer sid = siteIds.get(0);

                if (sid == null)
                {
                    log.error("Query of miu {} returns NULL site id!!", miuJson.getMiuId());
                    miuJson.setOldSiteId(null);
                } else
                {
                    miuJson.setOldSiteId(sid);
                }
            }
            else
            {
                throw new MdceRestException("Duplicate miuId's found: " + miuJson.getMiuId());
            }
        }
    }


    @Override
    public void insertMiuSetOwnerBatch(InsertMiuDetailsBpss insertMiuDetailsBpss)
    {
        final AddMiuOwnerHistoryBpss addMiuOwnerHistoryBpss = new AddMiuOwnerHistoryBpss(
                insertMiuDetailsBpss.getSiteId(), Instant.now(), insertMiuDetailsBpss.getBatchSize(), insertMiuDetailsBpss.getMiuIds());

        this.db.batchUpdate(INSERT_MIU_DETAILS, insertMiuDetailsBpss);

        this.db.batchUpdate(INSERT_MIU_OWNER_HISTORY, addMiuOwnerHistoryBpss);
    }


    @Override
    public int updateMiuOwnerDetails(MiuId miuId, SiteId newSiteId, String isMeterActive, MiuType miuTypeIfCreate)
    {
        siteRepository.ensureSiteExists(newSiteId);

        int rowsUpdated = this.db.update(UPDATE_MIU_DETAILS, newSiteId.numericWrapperValue(), isMeterActive, miuId.numericWrapperValue());

        if (rowsUpdated == 0) //non-existent MIU
        {
            final MiuType type;
            if (miuTypeIfCreate != null)
            {
                type = miuTypeIfCreate;
            }
            else
            {
                type = inferMiuTypeFromMiuId(miuId); //derive from MIU ID range
            }

            this.mqttSubscriptionManager.subscribeMiuMqttIfApplicable(miuId, type);
            //add to miu_details
            rowsUpdated = this.db.update(INSERT_MIU_DETAILS, miuId.numericWrapperValue(), newSiteId.numericWrapperValue(), type.numericWrapperValue());

        }
        this.db.update(INSERT_CMIU_CONFIG_HISTORY_FOR_OWNER_CHANGE, newSiteId.numericWrapperValue(), isMeterActive, miuId.numericWrapperValue());

        addMiuOwnerHistory(miuId, newSiteId, Instant.now());

        return rowsUpdated;
    }


    @Override
    public int[] updateMiuOwnerBatch(UpdateMiuSiteIdBpss updateMiuSiteIdBpss)
    {
        final AddMiuOwnerHistoryBpss addMiuOwnerHistoryBpss = new AddMiuOwnerHistoryBpss(
                updateMiuSiteIdBpss.getSiteId(), Instant.now(), updateMiuSiteIdBpss.getBatchSize(), updateMiuSiteIdBpss.getMiuIds());

        final int[] ret = this.db.batchUpdate(UPDATE_MIU_DETAILS, updateMiuSiteIdBpss);

        this.db.batchUpdate(INSERT_CMIU_CONFIG_HISTORY_FOR_OWNER_CHANGE, updateMiuSiteIdBpss);

        this.db.batchUpdate(INSERT_MIU_OWNER_HISTORY, addMiuOwnerHistoryBpss);

        return ret;
    }


    private static MiuType inferMiuTypeFromMiuId(MiuId miuId)
    {
        if (miuId.isInCmiuIdRange())
        {
            return MiuType.CMIU;
        }
        return MiuType.R900;
    }

    /**
     * Appends the supplied MIU site_id change to the miu_owner_history table.
     * @param miuId
     * @param siteId
     * @param timestamp
     * @return
     */
    private int addMiuOwnerHistory(MiuId miuId, SiteId siteId, Instant timestamp)
    {
        final int count = this.db.update(INSERT_MIU_OWNER_HISTORY,
                miuId.numericWrapperValue(), siteId.numericWrapperValue(), Timestamp.from(timestamp));

        return count;
    }

    @Override
    public List<MiuOwnerHistory> getMiuOwnerHistory(int lastSequenceId, int pageSize)
    {
        return this.db.query(SELECT_MIU_OWNER_HISTORY_PAGED, getMiuOwnerHistoryRowMapper(), lastSequenceId, pageSize);
    }

    private RowMapper<MiuOwnerHistory> getMiuOwnerHistoryRowMapper()
    {
        return (rs, rownum) ->
                new MiuOwnerHistory(
                        rs.getInt(MIU_OWNER_HISTORY_ID_COLUMN),
                        MiuId.valueOf(rs.getLong(MIU_ID_COLUMN)),
                        SiteId.valueOf(rs.getInt(SITE_ID_COLUMN)),
                        getInstant(rs, TIMESTAMP_COLUMN),
                        getInstant(rs, CREATED_COLUMN));
    }

}
