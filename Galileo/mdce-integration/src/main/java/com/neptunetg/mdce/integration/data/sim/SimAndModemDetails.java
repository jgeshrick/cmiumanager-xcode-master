/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2017 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.data.sim;

import com.neptunetg.common.data.miu.MiuId;

import java.io.Serializable;


/*
 * Class to store data to uniquely identify a SIM card and Modem Board pairing.
 */
public class SimAndModemDetails implements Serializable {

    private final MiuId miuId;
    private final String iccid;
    private final String imei;


    public SimAndModemDetails(MiuId miuId, String iccid, String imei)
    {
        super();
        this.miuId = miuId;
        this.iccid = iccid;
        this.imei = imei;
    }


    public String getIccid() { return iccid; }

    public MiuId getMiuId() { return miuId; }

    public String getImei() { return imei; }

}