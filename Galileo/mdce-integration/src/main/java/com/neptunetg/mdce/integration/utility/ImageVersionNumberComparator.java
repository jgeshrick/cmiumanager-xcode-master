/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.utility;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Compares image version numbers, newest first
 */
public class ImageVersionNumberComparator implements Comparator<String>
{
    private static final Pattern VERSION_NUMBER_PATTERN = Pattern.compile("([0-9]+)(?:\\.([0-9]+)(?:\\.([0-9]+)(?:\\.([0-9]+))?)?)?"); // ([0-9]+)(?:\.([0-9]+)(?:\.([0-9]+)(?:\.([0-9]+))?)?)?

    /**
     * Returns <0 if v2 < v1
     * @param v1
     * @param v2
     * @return
     */
    @Override
    public int compare(String v1, String v2)
    {

        final Matcher m1 = VERSION_NUMBER_PATTERN.matcher(v1);
        final Matcher m2 = VERSION_NUMBER_PATTERN.matcher(v2);

        if (m1.find())
        {
            if (m2.find()) {

                //version number found in both
                int ret = v2.substring(0, m2.start()).compareTo(v1.substring(0, m1.start()));
                if (ret != 0)
                {
                    return ret; //different preamble to version number
                }
                for (int part = 0; part < m1.groupCount(); part++)
                {
                    final String versionPart1 = m1.group(part + 1);
                    final String versionPart2 = m2.group(part + 1);
                    if (versionPart1 == null)
                    {
                        if (versionPart2 == null)
                        {
                            break;
                        }
                        else
                        {
                            return 1;
                        }
                    }
                    else if (versionPart2 == null)
                    {
                        return -1;
                    }
                    else
                    {
                        ret = Integer.compare(Integer.parseInt(versionPart2), Integer.parseInt(versionPart1));
                        if (ret != 0)
                        {
                            return ret;
                        }
                    }
                }
            } else {
                return -1;  //strings with version numbers before strings without version numbers
            }
        } else if (m2.find()) {
            return 1; //strings with version numbers before strings without version numbers
        }
        return v2.compareTo(v1);


    }

}
