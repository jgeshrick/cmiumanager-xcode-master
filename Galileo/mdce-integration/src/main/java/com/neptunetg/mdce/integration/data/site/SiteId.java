/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.data.site;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;

/**
 * Represents a site ID
 */
public class SiteId
{
    public static final SiteId UNIVERSAL = new SiteId(0);

    private final int id;

    public SiteId(int id)
    {
        this.id = id;
    }

    public SiteId(long id)
    {
        this.id = Long.valueOf(id).intValue();
    }

    /**
     * String-arg constructor supports Spring MVC URL binding
     * @param id number as string
     */
    public SiteId(String id)
    {
        this.id = Integer.parseInt(id);
    }

    public static SiteId valueOf(int id)
    {
        return new SiteId(id);
    }

    public static SiteId valueOf(long id)
    {
        return new SiteId(id);
    }

    public static SiteId valueOf(Integer id)
    {
        return new SiteId(id.intValue());
    }

    public static SiteId valueOf(String id)
    {
        return new SiteId(Integer.parseInt(id));
    }

    public int numericValue()
    {
        return id;
    }


    public Integer numericWrapperValue()
    {
        return Integer.valueOf(id);
    }

    public boolean isUniversalSite()
    {
        return id == 0;
    }

    public boolean isValid()
    {
        return id >= 0;
    }

    public boolean doesNotMatch(int otherNumericSiteId)
    {
        return id != otherNumericSiteId;
    }

    public boolean doesNotMatch(long otherNumericSiteId)
    {
        return id != otherNumericSiteId;
    }


    @Override
    public int hashCode()
    {
        return id;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        SiteId other = (SiteId) obj;
        return id == other.id;
    }

    @Override
    public String toString()
    {
        return Integer.toString(id);
    }

    /**
     * Get the site ID as a string if argument not null, otherwise return null
     * @param siteId Site ID, may be null
     * @return Site ID string or null
     */
    public static String getStringValueAllowNull(SiteId siteId)
    {
        if (siteId == null)
        {
            return null;
        }
        else
        {
            return Integer.toString(siteId.id);
        }
    }


    public static class ToIntConverter implements com.fasterxml.jackson.databind.util.Converter<SiteId, Integer>
    {

        @Override
        public Integer convert(SiteId arg0)
        {
            return Integer.valueOf(arg0.id);
        }

        @Override
        public JavaType getInputType(TypeFactory arg0)
        {
            return arg0.constructType(SiteId.class);
        }

        @Override
        public JavaType getOutputType(TypeFactory arg0)
        {
            return arg0.constructType(Integer.class);
        }

    }

}
