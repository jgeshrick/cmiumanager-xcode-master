/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.utility;


/* Class for generating SQL queries */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SqlBuilder {

    private static final String DB_NULL = "-";

    StringBuilder sqlBuilder;
    List<String> arguments = new ArrayList<>();

    int criteriaCount = 0;

    public SqlBuilder()
    {
        sqlBuilder = new StringBuilder();
    }

    public SqlBuilder(String query)
    {
        sqlBuilder = new StringBuilder(query);
    }

    public SqlBuilder addCriteriaIfExists(Map<String, String> options, Map<String, String> criteria)
    {

        criteria.forEach((k, v) -> {

            if (options.containsKey(k)) {

                addCriterion(v, options.get(k));

            }
        });

        return this;

    }

    public SqlBuilder addCriterion(String criterion, String arg)
    {

        if( criteriaCount > 0 )
        {
            sqlBuilder.append(" AND ");
        }
        else
        {
            sqlBuilder.append(" WHERE ");
        }

        if (arg != null)
        {
            sqlBuilder.append(criterion).append(" = ?");
            arguments.add(arg);
        }
        else
        {
            sqlBuilder.append(criterion).append(" IS NULL");
        }

        criteriaCount++;

        return this;
    }

    public void append(String string, Object... args)
    {
        sqlBuilder.append(" ");
        sqlBuilder.append(string);

        for (Object arg : args)
        {
            arguments.add(String.valueOf(arg));
        }

    }

    public String getSQL()
    {
        return sqlBuilder.toString();
    }


    public String[] getArguments() {
        String[] args = new String[arguments.size()];

        return arguments.toArray(args);
    }

    /* Decodes null special character to java null */
    public static Map<String, String> decodeNullValues(Map<String,String> options)
    {
        Map<String,String> decodedOptions = new HashMap<>();

        options.forEach( (k,v) -> {

            if (v.equals(DB_NULL))
            {
                decodedOptions.put(k, null);
            }
            else
            {
                decodedOptions.put(k,v);
            }

        });

        return decodedOptions;
    }

}
