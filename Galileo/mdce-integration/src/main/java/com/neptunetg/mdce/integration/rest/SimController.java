/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.sim.service.SimService;
import com.neptunetg.mdce.integration.data.miu.JdbcMiuSearchRepository;
import com.neptunetg.mdce.integration.data.miu.MiuSearchRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import com.neptunetg.mdce.integration.rest.json.JsonMiuListCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

@RestController
public class SimController
{
    @Autowired
    private SimService simService;

    /**
     * Update or insert a SIM.
     *
     * TODO: This is a temporary API that is not listed in ETI 48-02. Its purpose is to allow testing of the
     *       SimUpdateRepository implementation until we have fake CellularNetworkService services that can test
     *       the SIM registration process with better fidelity.
     *
     * @param iccid ICCID
     * @param mno Network operator (e.g. VZN/ATT)
     * @param msisdn MSISDN associated with the SIM
     * @return request id
     */
    @RequestMapping(value = "/mdce/api/v1/sim/details", method = RequestMethod.POST)
    public ResponseEntity updateOrInsertSimDetails(@RequestParam("iccid") String iccid,
                                         @RequestParam("mno") String mno,
                                         @RequestParam("msisdn") String msisdn) throws InternalApiException
    {
        Map<String, String> options = new HashMap<>();
        if (StringUtils.hasText(msisdn))
        {
            options.put("msisdn", msisdn);
        }

        simService.updateOrInsertSimDetails(options, iccid, mno);

        //return 204 (empty response body)
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
