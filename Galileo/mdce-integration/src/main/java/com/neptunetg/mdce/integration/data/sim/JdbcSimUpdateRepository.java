/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.sim;


import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.mdce.common.api.rest.exception.NotFoundException;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.common.internal.sim.model.SimLifecycleState;
import com.neptunetg.mdce.integration.domain.alert.AlertEmail;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

/**
 * SIM update repository
 */
@Repository
public class JdbcSimUpdateRepository implements SimUpdateRepository
{
    private static final Logger log = LoggerFactory.getLogger(JdbcSimUpdateRepository.class);

    // fields
    private final JdbcTemplate db;

    @Autowired
    private AlertService alertService;

    @Autowired
    public JdbcSimUpdateRepository(JdbcTemplate db)
    {
        this.db = db;
    }

    /**
     * Updates or inserts the supplied SIM details, and inserts a record of the SIM lifecycle state if it has changed.
     * @param sim DeviceCellularInformation object containing details of SIM
     * @param mno Mobile network operator name
     */
    @Override
    public void updateOrInsertSimDetails(DeviceCellularInformation sim, String mno)
    {
        if (sim == null)
        {
            throw new IllegalArgumentException("sim cannot be null");
        }
        if (!StringUtils.hasText(sim.getIccid()))
        {
            throw new IllegalArgumentException("ICCID is null in cellular information! MNO = " + mno + ", cellular information = " + sim);
        }

        // Search for any existing record
        SimDetails simDetails = getSimDetailsWithoutCreationDate(sim.getIccid());

        SimLifecycleState newLifecycleState = null;
        long simDetailsId;

        if (simDetails == null)
        {
            // New record - insert
            newLifecycleState = SimLifecycleState.UNASSIGNED;
            simDetailsId = addNewSimDetails(sim.getIccid(), sim.getMsisdn(), mno, newLifecycleState);
        }
        else
        {
            // Existing record - update.
            simDetailsId = simDetails.getSimId();
            SimLifecycleState existingLifecycleState = SimLifecycleState.fromString(simDetails.getLifeCycleState());

            if(simDetails.getMsisdn() != null && !simDetails.getMsisdn().equals(sim.getMsisdn()))
            {
                //Throw an alert if the SIM details are being updated to ones which don't match the CAN
                final AlertSource alertSource = AlertSource.simMsisdnMismatch(simDetails.getIccid());
                AlertDetails alert = new AlertDetails();
                alert.setAlertCreationDate(ZonedDateTime.now());
                alert.setAlertSource(alertSource.getAlertId());
                alert.setAlertLevel(AlertLevel.WARNING);
                alert.setAlertState(AlertState.NEW);
                alert.setRecentMessage("MSISDN: " + sim.getMsisdn() + "from the MNO does not match the MSISDN: " +
                        simDetails.getMsisdn() + " currently stored for SIM with ICCID: " + simDetails.getIccid());

                AlertEmail alertEmail = alertService.initAlertEmail();

                try
                {
                    alertService.addAlert(alert, alertEmail);
                }
                catch (InternalApiException e)
                {
                    log.error("Error while attempting to raise alert when updating sim details", e);
                }
            }

            // Default SIM lifecycle state to Unassigned if null.
            if (existingLifecycleState == null)
            {
                existingLifecycleState = SimLifecycleState.UNASSIGNED;
                newLifecycleState = SimLifecycleState.UNASSIGNED;
            }

            // Only allow transition from Unassigned to Assigned here.
            if (simDetails.getMiuId() != null && existingLifecycleState == SimLifecycleState.UNASSIGNED)
            {
                newLifecycleState = SimLifecycleState.ASSIGNED;
            }

            // Update sim_details.
            String sql = "UPDATE mdce.sim_details " +
                    "SET msisdn = IFNULL(?, msisdn), " +
                    "sim_cellular_network = IFNULL(?, sim_cellular_network), " +
                    "state = IFNULL(?, state) " +
                    "WHERE sim_details_id = ?";
            this.db.update(sql, sim.getMsisdn(), mno, newLifecycleState, simDetailsId);
        }

        // If there is a new lifecycle state, record it in mdce.sim_lifecycle_state.
        if (newLifecycleState != null)
        {
            addNewSimLifecycleStatus(simDetailsId, newLifecycleState);
        }
    }

    /**
     * Updates the lifecycle state of a SIM.
     * @param iccid The ICCID identifying the SIM.
     * @param lifecycleState The new lifecycle state of the SIM.
     */
    @Override
    public int updateLifecycleState(String iccid, SimLifecycleState lifecycleState)
    {
        if (iccid == null)
        {
            throw new IllegalArgumentException("iccid cannot be null");
        }
        if (lifecycleState == null)
        {
            throw new IllegalArgumentException("lifecycleState cannot be null");
        }

        // Get existing record
        SimDetails simDetails = getSimDetailsWithoutCreationDate(iccid);

        int result = 0;
        if (simDetails == null)
        {
            throw new NotFoundException();
        }
        else
        {
            if (!lifecycleState.getStringValue().equalsIgnoreCase(simDetails.getLifeCycleState()))
            {
                // Update mdce.sim_details.
                final String sql = "UPDATE mdce.sim_details " +
                        "SET state = ? " +
                        "WHERE sim_details_id = ? ";

                result = this.db.update(sql, lifecycleState.getStringValue(), simDetails.getSimId());

                // Insert into history table.
                addNewSimLifecycleStatus(simDetails.getSimId(), lifecycleState);
            }
        }

        return result;
    }

    /**
     * Gets the SimDetails instance identified by the provided ICCID.
     * @param iccid The ICCID that identifies the SIM.
     * @return If the ICCID exists, returns the SimDetails; otherwise, returns null.
     */
    private SimDetails getSimDetailsWithoutCreationDate(String iccid)
    {
        String findSql =
                "SELECT " +
                "sim_details_id, miu_id, iccid, msisdn, imsi, sim_cellular_network, state, NULL " +
                "FROM mdce.sim_details " +
                "WHERE iccid = ?";

        List<SimDetails> results = this.db.query(findSql, getSimDetailsRowMapper(), iccid);
        if (results.isEmpty())
        {
            return null;
        }
        else
        {
            return results.get(0);
        }
    }

    private int addNewSimDetails(String iccid, String msisdn, String network, SimLifecycleState lifecycleState)
    {
        assert (iccid != null);
        String insertSql = "INSERT INTO sim_details (iccid, msisdn, sim_cellular_network, state) VALUES (?, ?, ?, ?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.db.update(connection ->
                {
                    PreparedStatement ps = connection.prepareStatement(insertSql, new String[]{"sim_details_id"});
                    ps.setString(1, iccid);
                    ps.setString(2, msisdn);
                    ps.setString(3, network);
                    ps.setString(4, lifecycleState.getStringValue());

                    return ps;
                },
                keyHolder);

        return keyHolder.getKey().intValue();
    }

    private void addNewSimLifecycleStatus(long simDetailsId, SimLifecycleState lifecycleState)
    {
        assert (lifecycleState != null);

        String historySql =
                "INSERT INTO mdce.sim_lifecycle_state (`sim_details_id`, `timestamp`, `state`) VALUES (?, ?, ?)";

        this.db.update(historySql, simDetailsId, Date.from(Instant.now()), lifecycleState.getStringValue());
    }

    /**
     * Maps the record set row to an instance of SimDetails, excluding the creationDate.
     * @return the mapped SimDetails instance.
     */
    private RowMapper<SimDetails> getSimDetailsRowMapper()
    {
        return (rs, rownum) -> {

            SimDetails details = new SimDetails(
                    rs.getLong(JdbcSimAndModemSearchRepository.SIMID_CONST),
                    rs.getString(JdbcSimAndModemSearchRepository.ICCID_CONST),
                    MiuId.valueOf(getIntegerIfNotNull(rs, JdbcSimAndModemSearchRepository.MIUID_CONST)),
                    rs.getString(JdbcSimAndModemSearchRepository.LIFE_CYCLE_STATE_CONST),
                    null,
                    rs.getString(JdbcSimAndModemSearchRepository.NETWORK_CONST),
                    rs.getString(JdbcSimAndModemSearchRepository.MSISDN_CONST),
                    rs.getString(JdbcSimAndModemSearchRepository.IMSI_CONST));

            return details;

        };
    }

    private Integer getIntegerIfNotNull(ResultSet rs, String column) throws SQLException
    {
        int value = rs.getInt(column);

        if (!rs.wasNull())
        {
            return Integer.valueOf(value);
        }
        else
        {
            return null;
        }
    }

}
