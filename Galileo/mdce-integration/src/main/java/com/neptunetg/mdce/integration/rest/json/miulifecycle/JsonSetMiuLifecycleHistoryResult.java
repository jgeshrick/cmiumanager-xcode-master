/*
 *  Neptune Technology Group
 *  Copyright 2017 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.miulifecycle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.mdce.integration.rest.json.JsonErrorResponse;

import java.util.List;

/**
 * Models the response to a request to add a batch of MIU lifecycle events.
 */
public class JsonSetMiuLifecycleHistoryResult
{
    @JsonProperty("succeeded")
    private int succeededCount;

    @JsonProperty("failed")
    private int errorCount;

    @JsonProperty("error_details")
    private List<JsonErrorResponse> errorDetails;

    public int getSucceededCount()
    {
        return succeededCount;
    }

    public void setSucceededCount(int succeededCount)
    {
        this.succeededCount = succeededCount;
    }

    public int getErrorCount()
    {
        return errorCount;
    }

    public void setErrorCount(int errorCount)
    {
        this.errorCount = errorCount;
    }

    public List<JsonErrorResponse> getErrorDetails()
    {
        return errorDetails;
    }

    public void setErrorDetails(List<JsonErrorResponse> errorDetails)
    {
        this.errorDetails = errorDetails;
    }
}
