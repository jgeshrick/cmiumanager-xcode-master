/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.scheduled.cmiumode;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.mode.MiuMode;
import com.neptunetg.common.service.miu.mode.MiuModeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by dcloud on 20/05/2017.
 * A class to check if an alert needs to be raised for a CMIU given its mode
 */
@Service
public class CmiuModeCheckerAlertServiceImpl implements CmiuModeCheckerAlertService
{
    private final MiuModeService miuModeService;

    @Autowired
    public CmiuModeCheckerAlertServiceImpl(MiuModeService miuModeService)
    {
        this.miuModeService = miuModeService;
    }

    @Override
    public MiuMode checkForOfficialMode(MiuId miuId, MiuMode reportedMode)
    {
        MiuMode cmiuMode = new MiuMode();

        cmiuMode.setMiuId(miuId);
        cmiuMode.setReportingInterval(reportedMode.getReportingInterval());
        cmiuMode.setRecordingInteval(reportedMode.getRecordingInteval());

        cmiuMode.setCurrentMode(miuModeService.getMiuCurrentMode(miuId));
        cmiuMode.setReportedMode(miuModeService.getMiuReportedMode(miuId, reportedMode));

        return cmiuMode;
    }
}

