/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.integration.utility;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Utility for doing GET request with timeout
 */
public class HttpGetUtility
{
    /**
     * Do a HTTP GET request on a server.
     * @param uri the url for the HTTP GET.
     * @return the get response content
     * @throws IOException
     */
    public static String doHttpGetRequest(String uri) throws IOException
    {
        String responseBody = "";

        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(30000)
                .setConnectionRequestTimeout(30000)
                .build();

        HttpClient client = HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .build();
        HttpGet getRequest = new HttpGet(uri);
        HttpResponse response = client.execute(getRequest);
        int responseStatusCode = response.getStatusLine().getStatusCode();

        if (responseStatusCode >= 200 && responseStatusCode < 300)
        {
            responseBody = EntityUtils.toString(response.getEntity());

            Header contentTypeHeader = response.getFirstHeader("Content-Type");
            if (contentTypeHeader != null)
            {
                String contentType = contentTypeHeader.getValue();

                //to nothing to response content and header for the moment
            }

        }
        //TODO process other response status
//        else
//        {

            /*switch (responseStatusCode)
            {
                case 401:
                case 404:
                default:
            }*/
//        }

        return responseBody;
    }
}
