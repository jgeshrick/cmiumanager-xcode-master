/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.rest.json.JsonMiuActiveFlag;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to assist in performing a batch update for MIU owner history
 * The batch insert is necessary for performance
 * Intended to work with JdbcMiuOwnershipRepository.INSERT_MIU_OWNER_HISTORY.
 */
public class AddMiuOwnerHistoryBpss implements BatchPreparedStatementSetter
{
    private final SiteId siteId;
    private Instant timestamp;
    private final long[] miuIds;
    private final int batchSize;

    public AddMiuOwnerHistoryBpss(SiteId siteId, Instant timestamp, int batchSize, long[] miuIds)
    {
        this.siteId = siteId;
        this.timestamp = timestamp;
        this.batchSize = batchSize;
        this.miuIds = miuIds;
    }

    @Override
    public int getBatchSize()
    {
        return batchSize;
    }

    @Override
    public void setValues(PreparedStatement preparedStatement, int i) throws SQLException
    {
        preparedStatement.setLong(1, miuIds[i]);
        preparedStatement.setInt(2, siteId.numericValue());
        preparedStatement.setTimestamp(3, Timestamp.from(timestamp));
    }
}
