/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughputExceededException;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.mdce.common.data.PacketRepository;
import com.neptunetg.mdce.common.data.model.GatewayPacketReceivedDynamoItem;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import com.neptunetg.mdce.integration.data.gateway.GatewayDetails;
import com.neptunetg.mdce.integration.data.gateway.GatewayRepository;
import com.neptunetg.mdce.integration.data.miu.MiuDetails;
import com.neptunetg.mdce.integration.data.miu.MiuSearchRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.data.site.SiteRepository;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import com.neptunetg.mdce.integration.rest.json.packets.JsonPacketData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * IF15
 */
@RestController
public class PacketListController extends BaseTokenAuthenticatingRestController
{
    private static final Logger logger = LoggerFactory.getLogger(PacketListController.class);
    //allow one minute jitter for last insert date searching for miu packets within a date range
    private static final int LAST_INSERT_DATE_TIME_JITTER_MINUTES = 1;

    private final ObjectMapper jackson = new ObjectMapper();

    @Autowired
    private SiteRepository siteRepo;

    @Autowired
    private MiuSearchRepository miuSearchRepo;

    @Autowired
    private GatewayRepository gatewayRepo;


    @Autowired
    private PacketRepository packetRepository;

    /**
     * This web service will be used by external systems to retrieve packets received from endpoints corresponding
     * to the site id that the token represents.
     *
     * @param minInsertDate the earliest date when the packet is inserted to the database, 2015-01-01T14:01:24-05:00
     * @param maxInsertDate the latest date when the packet is inserted to the database, 2015-01-01T14:01:24-05:00
     * @return object to be serialised as JSON output
     */
    @RequestMapping(value = "/mdce/api/v1/packet", method = RequestMethod.GET)
    public void getPacketsForMiu(
            @RequestParam(value = "min_insert_date", required = true) String minInsertDate,
            @RequestParam(value = "max_insert_date", required = false) String maxInsertDate,
            @RequestParam(value = "miu_id", required = false) MiuId miuId,
            @RequestParam(value = "include_gateway_packets", required = false, defaultValue = "false") boolean includeGatewayPackets,
            HttpServletRequest request, HttpServletResponse response) throws NotAuthorizedException, BadRequestException
    {
        final ZonedDateTime fromDate = parseMinimumQueryDateMandatory(minInsertDate);
        final Instant fromTimestamp = fromDate.toInstant();
        final ZoneId timeZone = fromDate.getZone();

        final ZonedDateTime currentTimeAtStartOfQuery = ZonedDateTime.now();
        final ZonedDateTime toDate = parseMaximumQueryDateOptional(maxInsertDate, currentTimeAtStartOfQuery.plus(1, ChronoUnit.SECONDS));
        final Instant toTimestamp = toDate.toInstant();

        final SiteId siteId = getClientSiteId(request);

        if (siteId.isUniversalSite())
        {
            writeJsonToResponseOut(fromTimestamp, toTimestamp, timeZone, request, response, Collections.<MiuDetails>emptyList(), Collections.<GatewayDetails>emptyList());
        }
        else
        {
            List<MiuDetails> mius = new ArrayList<MiuDetails>();

            if(miuId != null)
            {
                MiuDetails miuDetails = miuSearchRepo.getMiuIfInUtility(siteId, miuId);

                if(miuDetails != null)
                {
                    mius.add(miuDetails);
                }
                else
                {
                    // If nothing was returned for valid SiteId, Check unclaimed units
                    MiuDetails universalMiuDetails = miuSearchRepo.getMiuIfInUtility(SiteId.UNIVERSAL, miuId);

                    if(universalMiuDetails != null)
                    {
                        mius.add(universalMiuDetails);
                    }
                }
            }
            else
            {
                mius = miuSearchRepo.getMiusForUtility(siteId);
            }

            final List<GatewayDetails> gateways = getGatewaysOrNull(includeGatewayPackets, siteId);

            logger.debug("Received packet API request with valid token for site ID " + siteId +
                    "; querying " + mius.size() + " MIUs, " +
                    (gateways == null ? " gateways omitted" : gateways.size() + " gateways") +
                    " since " + fromDate);

            writeResponseHeaderAndOkStatus(response);

            writeJsonToResponseOut(fromTimestamp, toTimestamp, timeZone, request, response, mius, gateways);

            siteRepo.updateLastDataRequest(siteId, currentTimeAtStartOfQuery.toInstant());
        }
    }

    private void writeResponseHeaderAndOkStatus(HttpServletResponse response)
    {
        response.setHeader("Content-Type", "application/json");
        response.setStatus(HttpStatus.OK.value()); //think we're okay, start spooling data
    }

    private void writeJsonToResponseOut(Instant minInsertDate, Instant maxInsertDate, ZoneId timeZone,
                                        HttpServletRequest request, HttpServletResponse response,
                                        List<MiuDetails> mius, List<GatewayDetails> gateways)
    {

        try (final Writer responseOut = response.getWriter())
        {
            writeJsonToResponseOut(mius, gateways, minInsertDate, maxInsertDate, timeZone, request.getRequestURI(), responseOut);
        }
        catch (Exception e)
        {
            //As we have already opened the writer, we can't fix the response code!
            logger.error("Error occurred while writing packet response JSON", e);
        }

    }

    private void writeJsonToResponseOut(List<MiuDetails> mius, List<GatewayDetails> gateways,
                                        Instant minInsertDate, Instant maxInsertDate, ZoneId timeZone,
                                        String requestUri, Writer responseOut) throws IOException
    {
        try (JsonGenerator jsonOut = getJsonOut(responseOut))
        {
            jsonOut.writeStartObject(); //  {

            writeServerTimeToJsonOut(jsonOut, timeZone);

            writeBodyToJsonOut(mius, gateways, minInsertDate, maxInsertDate, jsonOut);

        }
        catch (Exception e)
        {
            //as we have started writing to the output stream, we can't change the HTTP status
            logger.error("Error while answering " + requestUri + " occurred after returning status 200 and some data", e);
            responseOut.write("**** Error occurred in the middle of writing the response. Please check the logs");
        }
    }

    private void writeBodyToJsonOut(List<MiuDetails> mius, List<GatewayDetails> gateways,
                                    Instant minInsertDate, Instant maxInsertDate, JsonGenerator jsonOut) throws IOException
    {
        writeMiuPacketsToJsonOut(gateways != null, mius, minInsertDate, maxInsertDate, jsonOut);

        writeGatewayPacketsToJsonOut(gateways, minInsertDate, maxInsertDate, jsonOut);
    }

    private void writeServerTimeToJsonOut(JsonGenerator jsonOut, ZoneId timeZone) throws IOException
    {
        final Instant queryTime = Instant.now();

        jsonOut.writeStringField("server_time", queryTime.atZone(timeZone).format(DateTimeFormatter.ISO_DATE_TIME));
    }


    private JsonGenerator getJsonOut(Writer responseOut) throws IOException
    {
        return new JsonFactory().createGenerator(responseOut); // or, for data binding, org.codehaus.jackson.mapper.MappingJsonFactory
    }

    private void writeMiuPacketsToJsonOut(boolean includeGatewayPackets, List<MiuDetails> mius, Instant fromTimestamp, Instant toTimestamp, JsonGenerator jsonOut) throws IOException
    {
        jsonOut.writeArrayFieldStart("mius");

        final List<Table> tables = packetRepository.getRelevantTablesForMiuPacketQuery(fromTimestamp, toTimestamp);
        if (tables == null || tables.isEmpty())
        {
            logger.error("No tables found for MIU packet query");
        }
        else
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Query covers " + tables.size() + " tables" + " in packet list, last is " + tables.get(tables.size() - 1).getTableName());
            }

            for (MiuDetails miu : mius)
            {
                if (includeGatewayPackets || MiuType.CMIU.equals(miu.getType()) || MiuType.L900.equals(miu.getType()))
                {
                    // Skip DynamoDb query if the lastInsertDate is before the date range of the search.
                    if (miu.getLastInsertDate() != null &&
                            miu.getLastInsertDate().plus(LAST_INSERT_DATE_TIME_JITTER_MINUTES, ChronoUnit.MINUTES).isAfter(fromTimestamp))
                    {
                        final int miuId = miu.getId().numericValue();

                        writeAllPacketsForMiuToJson(fromTimestamp, toTimestamp, jsonOut, tables, miuId);

                        if (logger.isTraceEnabled())
                        {
                            logger.trace("Including " + miuId + " in packet list results because last insert date = " +
                                    miu.getLastInsertDate().atOffset(ZoneOffset.UTC) + " + " +
                                    LAST_INSERT_DATE_TIME_JITTER_MINUTES + "m is after " + fromTimestamp.atOffset(ZoneOffset.UTC));
                        }
                    }
                }
                else if (logger.isTraceEnabled())
                {
                    final String lastInsertDateDisplay;
                    if (miu != null && miu.getLastInsertDate() != null)
                    {
                        lastInsertDateDisplay = miu.getLastInsertDate().atOffset(ZoneOffset.UTC).toString();
                    }
                    else
                    {
                        lastInsertDateDisplay = "null";
                    }
                    logger.trace("Skipping" + (miu == null ? null : miu.getId()) + " in packet results because last insert date = " +
                            lastInsertDateDisplay + " + " +
                            LAST_INSERT_DATE_TIME_JITTER_MINUTES + "m is before " + (fromTimestamp == null ? null : fromTimestamp.atOffset(ZoneOffset.UTC)));
                }


            }
        }

        jsonOut.writeEndArray(); //end "mius"
    }

    private void writeAllPacketsForMiuToJson(Instant fromTimestamp, Instant toTimestamp, JsonGenerator jsonOut, Collection<Table> tables, int miuId) throws IOException
    {
        Iterable<MiuPacketReceivedDynamoItem> packets = null;
        for (int attempt = 0; attempt < 3; attempt++)
        {
            try
            {
                packets = packetRepository.iterateMiuPacketsInsertedInDateRange(miuId, fromTimestamp, toTimestamp, tables);
                break;
            }
            catch (ProvisionedThroughputExceededException e)
            {
                //hitting Dynamo too hard
                if (logger.isWarnEnabled())
                {
                    final StringBuilder tableNames = new StringBuilder();
                    tables.forEach(t -> {tableNames.append(tableNames.length() == 0 ? "" : ", ").append(t.getTableName());});
                    logger.warn("Provisioned throughput exceeded on attempt " + (attempt + 1) + " / 3, tables are: " + tableNames.toString() + ", response = " + e.getRawResponseContent(), e);
                }
                //TODO: can programmatically increase provisioned throughput here
                if (attempt == 2)
                {
                    throw e;
                }
                try
                {
                    Thread.sleep(5000L);
                }
                catch (InterruptedException e2)
                {
                    //just stop sleeping
                }

            }
        }
        boolean first = true;
        for (MiuPacketReceivedDynamoItem miuPacketReceivedDynamoItem : packets)
        {
            if (first)
            {
                jsonOut.writeStartObject();
                jsonOut.writeNumberField("miu_id", miuId);
                jsonOut.writeArrayFieldStart("packets");
                first = false;
            }

            jackson.writeValue(jsonOut, JsonPacketData.fromMiuPacket(miuPacketReceivedDynamoItem));
        }

        if (!first)
        {
            jsonOut.writeEndArray();
            jsonOut.writeEndObject();
        }
    }

    private void writeGatewayPacketsToJsonOut(List<GatewayDetails> gateways, Instant fromTimestamp, Instant toTimestamp, JsonGenerator jsonOut) throws IOException
    {
        if (gateways == null)
        {
            return;
        }

        final Collection<Table> tables = packetRepository.getRelevantTablesForGatewayPacketQuery(fromTimestamp, toTimestamp);

        jsonOut.writeArrayFieldStart("gateways");

        for (GatewayDetails gateway : gateways)
        {
            final String gatewayId = gateway.getGatewayId().toString();

            boolean first = true;

            final Iterable<GatewayPacketReceivedDynamoItem> packets = packetRepository.iterateGatewayPacketsInsertedInDateRange(gatewayId,
                    fromTimestamp, toTimestamp, tables);
            for (GatewayPacketReceivedDynamoItem gatewayPacketReceivedDynamoItem : packets)
            {
                if (first)
                {
                    jsonOut.writeStartObject();
                    jsonOut.writeStringField("gw_id", gatewayId);
                    jsonOut.writeArrayFieldStart("packets");
                    first = false;
                }
                jackson.writeValue(jsonOut, JsonPacketData.fromGatewayPacket(gatewayPacketReceivedDynamoItem));
            }
            if (!first)
            {
                jsonOut.writeEndArray();
                jsonOut.writeEndObject();
            }
        }
        jsonOut.writeEndArray(); //end "gateways"

    }

    private List<GatewayDetails> getGatewaysOrNull(boolean includeGatewayPackets, SiteId siteId)
    {
        if (includeGatewayPackets)
        {
            return gatewayRepo.getGatewaysForUtility(siteId);
        } else
        {
            return null;
        }
    }


}
