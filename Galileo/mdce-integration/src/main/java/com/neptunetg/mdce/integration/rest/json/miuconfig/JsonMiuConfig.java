/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.json.miuconfig;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.cellular.CmiuConfigSetAssociation;
import com.neptunetg.common.data.miu.cellular.ConfigSet;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;

import java.util.Optional;

/**
 *   JSON object for a single MIU configuration
**/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonMiuConfig
{
    @JsonProperty("miu_id")
    private long miuId;

    @JsonProperty("config")
    private JsonMiuConfigDetail miuConfigDetail;

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public JsonMiuConfigDetail getMiuConfigDetail()
    {
        return miuConfigDetail;
    }

    public void setMiuConfigDetail(JsonMiuConfigDetail miuConfigDetail)
    {
        this.miuConfigDetail = miuConfigDetail;
    }

    //TODO revisit this.
    public static JsonMiuConfig from(CmiuConfigSetAssociation configSet)
    {
        Optional<String> aWrappedNull = Optional.empty();
        return from(configSet, aWrappedNull);
    }

    public static JsonMiuConfig from(CmiuConfigSetAssociation configSet, Optional<String> distributerId)
    {
        JsonMiuConfig result = new JsonMiuConfig();
        result.setMiuId(configSet.getMiuId().numericValue());
        JsonMiuConfigDetail configDetail = JsonMiuConfigDetail.from(configSet, distributerId);
        result.setMiuConfigDetail(configDetail);

        return result;
    }


    /**
     * Convert to a new CmiuConfigSet object.
     * @return
     */
    public CmiuConfigSet toCmiuConfigSet()
    {
        CmiuConfigSet configSet = new CmiuConfigSet();
        configSet.setName("Custom configSet from cmiu " + String.valueOf(this.miuId));
        configSet.setReportingStartMins(this.miuConfigDetail.getReporting().getStartTimeMins());
        configSet.setReportingIntervalMins(this.miuConfigDetail.getReporting().getIntervalMins());
        configSet.setReportingNumberOfRetries(this.miuConfigDetail.getReporting().getRetries());
        configSet.setReportingTransmitWindowMins(this.miuConfigDetail.getReporting().getTransmitWindowMins());
        configSet.setReportingQuietStartMins(this.miuConfigDetail.getQuietTime().getQuietStartMins());
        configSet.setReportingQuietEndMins(this.miuConfigDetail.getQuietTime().getQuietEndMins());

        configSet.setRecordingStartTimeMins(this.miuConfigDetail.getRecording().getStartTimeMins());
        configSet.setRecordingIntervalMins(this.miuConfigDetail.getRecording().getIntervalMins());

        return configSet;
    }

    public ConfigSet toDBConfigSet()
    {
        ConfigSet configSet = new ConfigSet();
        configSet.setName("Custom configSet from cmiu " + String.valueOf(this.miuId));

        if(this.miuConfigDetail.getRecording() != null)
        {
            configSet.setRecordingStartTimeMins(this.miuConfigDetail.getRecording().getStartTimeMins());
            configSet.setRecordingIntervalMins(this.miuConfigDetail.getRecording().getIntervalMins());
        }

        if(this.miuConfigDetail.getReporting() != null)
        {
            configSet.setReportingStartMins(this.miuConfigDetail.getReporting().getStartTimeMins());
            configSet.setReportingIntervalMins(this.miuConfigDetail.getReporting().getIntervalMins());
            configSet.setReportingNumberOfRetries(this.miuConfigDetail.getReporting().getRetries());
            configSet.setReportingTransmitWindowMins(this.miuConfigDetail.getReporting().getTransmitWindowMins());
        }

        if(this.miuConfigDetail.getQuietTime() != null)
        {
            configSet.setReportingQuietStartMins(this.miuConfigDetail.getQuietTime().getQuietStartMins());
            configSet.setReportingQuietEndMins(this.miuConfigDetail.getQuietTime().getQuietEndMins());
        }

        return configSet;
    }
}
