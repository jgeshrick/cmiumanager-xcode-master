/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.rest.json.refdata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceDataInfoContact;

import java.util.ArrayList;
import java.util.List;

/**
 * JsonReferenceDataInfoContact
 *
 * "primary_contact": {
 *    "name": "Moore, William",
 *    "work_phone": "5612432084",
 *    "title": "Superintendent"
 * }
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonReferenceDataInfoContact
{

    @JsonProperty("name")
    private String name;

    @JsonProperty("work_phone")
    private String workPhone;

    @JsonProperty("title")
    private String title;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getWorkPhone()
    {
        return workPhone;
    }

    public void setWorkPhone(String workPhone)
    {
        this.workPhone = workPhone;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }


    public static JsonReferenceDataInfoContact from(MdceReferenceDataInfoContact primaryContact)
    {
        JsonReferenceDataInfoContact contact = new JsonReferenceDataInfoContact();

        if (primaryContact != null)
        {
            contact.setTitle(primaryContact.getTitle());
            contact.setName(primaryContact.getContactName());

            contact.setWorkPhone(primaryContact.getWorkPhone());
        }

        return contact;
    }

    public static List<JsonReferenceDataInfoContact> from(List<MdceReferenceDataInfoContact> activeSecondaryContacts)
    {
        List<JsonReferenceDataInfoContact> jsonContacts = new ArrayList<>();

        for (MdceReferenceDataInfoContact contact : activeSecondaryContacts) {
            jsonContacts.add(from(contact));
        }

        return jsonContacts;
    }
}
