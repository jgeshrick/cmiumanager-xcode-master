/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.audit.model.AuditLog;
import com.neptunetg.mdce.common.internal.audit.model.AuditLogPagedResult;
import com.neptunetg.mdce.common.internal.audit.service.AuditLogService;
import com.neptunetg.mdce.integration.data.audit.AuditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Internal REST controller implementing Audit log service
 */
@RestController
@RequestMapping("/service")
public class AuditLogServiceController implements AuditLogService
{
    @Autowired
    private AuditRepository auditRepository;

    @Override
    @RequestMapping(value= URL_AUDIT_LOG, method= RequestMethod.GET)
    public AuditLogPagedResult getAuditLog(@RequestParam(value = REQUEST_PARAM_AUDIT_FILTER_MIU, required = false) Long miuId,
                                      @RequestParam(value = REQUEST_PARAM_AUDIT_FILTER_USERNAME, required = false) String userName,
                                      @RequestParam(value = REQUEST_PARAM_AUDIT_LOG_DAYS_AGO, required = true, defaultValue = "1") Integer daysAgo,
                                           @RequestParam(value = REQUEST_PARAM_AUDIT_LOG_PAGE, required = false) Integer page
    ) throws InternalApiException
    {
        final Integer auditLogCount = auditRepository.getAuditLogCount(miuId, userName, daysAgo);

        final List<AuditLog> auditLog = auditRepository.getAuditLog(miuId, userName, daysAgo, page);

        final AuditLogPagedResult auditLogPagedResult = new AuditLogPagedResult(auditLogCount, auditLog.size(), page, auditLog);

        return auditLogPagedResult;
    }

    @Override
    @RequestMapping(value = URL_AUDIT_LOG_ACCESS_DENIED, method = RequestMethod.GET)
    public void logAccessDenied(@RequestParam(value = REQUEST_PARAM_AUDIT_USERNAME) String userName,
                                @RequestParam(value = REQUEST_PARAM_AUDIT_DESCRIPTION) String description) throws InternalApiException
    {
        this.auditRepository.logAccessDenied(userName, description);
    }
}
