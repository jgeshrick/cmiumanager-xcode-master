/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;


import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.integration.data.miu.MiuDetails;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.rest.json.JsonMiuActiveFlag;

import java.util.List;

/**
* Service interface for CRUD operation on the MySql database.
 * Used to claim MIUs
 */
public interface MiuService
{

    /**
     * Updates the MIU owner details (which is the site ID) for a list of MIUs
     * They all have to be updated to the same site ID
     * @param miuJsonList The list of MIUs to have their owner changed
     * @param siteId The site ID that the MIUs should now be owned by
     */
    void updateMiuOwnerDetailsBatch(List<JsonMiuActiveFlag> miuJsonList, SiteId siteId);

}
