/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.sim;

import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.cns.CellularStatus;
import com.neptunetg.common.cns.CnsException;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleService;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.sim.model.SimDeactivationDetails;
import com.neptunetg.mdce.common.internal.sim.model.SimDeactivationDisplayDetails;
import com.neptunetg.mdce.common.internal.sim.service.SimDeactivationService;
import com.neptunetg.mdce.integration.cns.CellularNetworkServiceManager;
import com.neptunetg.mdce.integration.data.miu.MiuDetails;
import com.neptunetg.mdce.integration.data.miu.MiuSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
@RequestMapping("/service")
public class SimDeactivationController implements SimDeactivationService
{
    @Autowired
    private MiuSearchRepository miuSearchRepo;

    @Autowired
    private CmiuRepository cmiuRepository;

    @Autowired
    private MiuLifecycleService miuLifecycleService;

    @Autowired
    private CellularNetworkServiceManager cellularNetworkServiceManager;

    private static final Logger log = LoggerFactory.getLogger(SimDeactivationController.class);

    @Override
    @RequestMapping(value = SimDeactivationService.URL_SIM_DETAILS_BY_MIUID, method = RequestMethod.GET)
    public SimDeactivationDisplayDetails getDeactivationDetails(@RequestParam(REQUEST_PARAM_MIU_ID_FOR_DEACTIVATION) String miuID) throws InternalApiException {
        SimDeactivationDisplayDetails results = new SimDeactivationDisplayDetails();

        MiuDetails details = miuSearchRepo.getMiu(MiuId.fromString(miuID));

        results.setCarrier(details.getNetworkProvider());
        results.setCurrentLifecycleState(details.getLifecycleState().toString());
        results.setIccid(details.getIccid());
        results.setImei(details.getImei());
        results.setMiuId(details.getId().numericWrapperValue());
        if (details.getLastHeardTime() != null) {
            results.setLastTimeHeard(details.getLastHeardTime().toString()); //TODO
        }
        final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(details.getNetworkProvider());

        try
        {
            //TODO Change this back
            DeviceCellularInformation cellularDeviceInformation = cellularNetworkService.getDeviceInformation(details.getIccid());
            if (cellularDeviceInformation.getLastConnectionDate() != null) {
                results.setLastCommunicationTime(cellularDeviceInformation.getLastConnectionDate().toInstant().toString());
            }
        }
        catch (CnsException e)
        {
            log.error("Error contacting CNS for device information: ", e);
        }


        return results;
    }

    @Override
    @RequestMapping(value = SimDeactivationService.URL_SIM_DETAILS_BY_MIUID, method = RequestMethod.POST)
    public SimDeactivationDetails deactivateSims(@RequestParam(REQUEST_PARAM_MIU_ID_FOR_DEACTIVATION) String miuIDString) throws InternalApiException {
        SimDeactivationDetails result = new SimDeactivationDetails();

        MiuId miuId = MiuId.fromString(miuIDString);

        try
        {
            result.setMiuId(miuId.numericWrapperValue());
            final CellularDeviceDetail miuCellularDetails = cmiuRepository.getCellularConfig(miuId);
            if(miuCellularDetails != null)
            {
                final CellularNetworkService cellularNetworkService = cellularNetworkServiceManager.getCns(miuCellularDetails.getMno());
                final String iccid = miuCellularDetails.getIccid();

                MiuDetails details = miuSearchRepo.getMiu(miuId);

                result.setOldState(details.getLifecycleState().toString());

                String cnsResult = "";

                //TODO AD ENUM VALUES TO DB
                switch (details.getLifecycleState()) {
                    case DEAD:
                    case DEATHBED:
                    case PRESUSPENDED:
                        result.setResult("No Action");
                        break;

                    case SUSPENDED:
                        if(cellularNetworkService.getAllowedLifecycleState(CellularStatus.DE_ACTIVE))
                        {
                            cnsResult = cellularNetworkService.deactivateDevice(iccid);
                            miuLifecycleService.setMiuLifecycleState(miuId, MiuLifecycleStateEnum.DEATHBED);
                            result.setNewState(MiuLifecycleStateEnum.DEATHBED.toString());
                            result.setResult("Success");
                        }
                        else
                        {
                            result.setResult("Error: MIU cannot be set to Deactivated.");
                        }

                        break;

                    default:
                        cnsResult = cellularNetworkService.suspendDevice(iccid);
                        miuLifecycleService.setMiuLifecycleState(miuId, MiuLifecycleStateEnum.PRESUSPENDED);
                        result.setNewState(MiuLifecycleStateEnum.PRESUSPENDED.toString());
                        result.setResult("Success");
                }

                log.debug("CNS Result: " + cnsResult);
            }
            else
            {
                result.setResult("Error: MIU not found.");
            }
        }
        catch (Exception e)
        {
            result.setResult("Error: " + e.toString());
            log.error("Error: ", e);
        }
        return result;
    }
}
