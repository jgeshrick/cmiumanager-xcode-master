/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.common.internal.site.model.SiteForUser;
import com.neptunetg.mdce.common.internal.user.model.LoginCredentials;
import com.neptunetg.mdce.common.internal.user.model.LoginResult;
import com.neptunetg.mdce.common.internal.user.model.ModifiedUserDetails;
import com.neptunetg.mdce.common.internal.user.model.NewUserDetails;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequest;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequestClaim;
import com.neptunetg.mdce.common.internal.user.model.UserAlertSettings;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;
import com.neptunetg.mdce.common.internal.user.service.UserService;
import com.neptunetg.mdce.integration.domain.user.UserManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/service")
public class UserServiceController implements UserService
{
    @Autowired
    private UserManagerService userManagerService;

    @Override
    @ResponseBody
    @RequestMapping(value=URL_USER_ADD, method = RequestMethod.POST)
    public boolean addUser(@RequestBody NewUserDetails newUserDetails) throws InternalApiException
    {
        return userManagerService.addUser(newUserDetails);
    }

    @Override
    @RequestMapping(value=URL_USER_REMOVE, method = RequestMethod.POST)
    public void removeUser(@RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException
    {
        userManagerService.removeUser(modifiedUserDetails);
    }

    @Override
    @RequestMapping(value=URL_USER_LIST, method = RequestMethod.GET)
    public List<UserDetails> listUsers() throws InternalApiException
    {
        return userManagerService.getUserList();
    }

    @Override
    @RequestMapping(value=URL_USER_PASSWORD, method = RequestMethod.POST)
    public void resetPassword(@RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException
    {
        userManagerService.setUserPassword(modifiedUserDetails);
    }

    @Override
    @RequestMapping(value=URL_USER_SITE_LIST, method=RequestMethod.POST)
    public List<SiteDetails> getSitesForUser(@RequestBody int userId) throws InternalApiException
    {
        return userManagerService.getSitesForUser(userId);
    }

    @Override
    @RequestMapping(value=URL_USER_NOT_SITE_LIST, method=RequestMethod.POST)
    public List<SiteDetails> getSitesNotForUser(@RequestBody int userId) throws InternalApiException
    {
        return userManagerService.getSitesNotForUser(userId);
    }

    @Override
    @RequestMapping(value=URL_USER_ADD_SITE, method=RequestMethod.POST)
    public boolean addSiteToUser(@RequestBody SiteForUser siteForUser) throws InternalApiException
    {
        return userManagerService.addSiteToUser(siteForUser);
    }

    @Override
    @RequestMapping(value=URL_USER_REMOVE_SITE, method=RequestMethod.POST)
    public boolean removeSiteFromUser(@RequestBody SiteForUser siteForUser) throws InternalApiException
    {
        return userManagerService.removeSiteFromUser(siteForUser);
    }

    @Override
    @RequestMapping(value=URL_USER_VERIFY, method=RequestMethod.POST)
    public LoginResult verifyUser(@RequestBody LoginCredentials loginCredentials) throws InternalApiException
    {
        return userManagerService.verifyUser(loginCredentials.getUserName(), loginCredentials.getPassword());
    }

    @Override
    @RequestMapping(value=URL_USER_DETAIL + "/{username}", method=RequestMethod.GET)
    public UserDetails getUserDetails(@PathVariable("username") String userName) throws InternalApiException
    {
        return userManagerService.getUser(userName);
    }

    @Override
    @RequestMapping(value=URL_USER_FIND, method=RequestMethod.POST)
    public List<UserDetails> findUsers(@RequestBody Map<String, Object> options) throws InternalApiException
    {
        return userManagerService.findUsers(options);
    }

    @Override
    @RequestMapping(value=URL_USER_DETAIL + "/{username}", method=RequestMethod.POST)
    public boolean setUserDetails(@PathVariable("username") String userName,
                                      @RequestBody ModifiedUserDetails modifiedUserDetails) throws InternalApiException
    {
        return userManagerService.setUserDetails(userName, modifiedUserDetails);
    }

    @Override
    @RequestMapping(value=URL_USER_ALERT_SETTINGS + "/{userid}", method = RequestMethod.GET)
    public UserAlertSettings getUserAlertSettings(@PathVariable("userid") int userId) throws InternalApiException
    {
        return userManagerService.getUserAlertSettings(userId);
    }

    @Override
    @RequestMapping(value=URL_USER_ALERT_SETTINGS, method = RequestMethod.POST)
    public boolean setUserAlertSettings(@RequestBody UserAlertSettings userAlertSettings) throws InternalApiException
    {
        return userManagerService.setUserAlertSettings(userAlertSettings);
    }

    @Override
    @RequestMapping(value = URL_USER_PASSWORD_RESET_REQUEST, method = RequestMethod.POST)
    public boolean addPasswordResetRequest(@RequestBody PasswordResetRequest passwordResetRequest) throws InternalApiException
    {
        return userManagerService.addPasswordResetRequest(passwordResetRequest);
    }

    @Override
    @RequestMapping(value = URL_USER_PASSWORD_RESET_REQUEST + "/{token}/claim", method = RequestMethod.POST)
    public boolean claimPasswordResetRequest(@PathVariable("token") String token,
                                             @RequestBody PasswordResetRequestClaim passwordResetRequestClaim
            ) throws InternalApiException
    {
        return userManagerService.claimPasswordResetRequest(token, passwordResetRequestClaim);
    }

    @Override
    @RequestMapping(value = URL_USER_EXPIRE_PASSWORD_RESET_REQUESTS, method = RequestMethod.POST)
    public int expirePasswordResetRequestsByUser(@RequestParam(USER_ID_PARAM) long userId) throws InternalApiException
    {
        return userManagerService.expirePasswordResetRequestsByUser(userId);
    }

    @Override
    @RequestMapping(value = URL_USER_PASSWORD_RESET_REQUEST + "/{token}", method = RequestMethod.GET)
    public PasswordResetRequest getPasswordResetRequestByToken(@PathVariable("token") String token) throws InternalApiException
    {
        return userManagerService.getPasswordResetRequestByToken(token);
    }

    @Override
    @RequestMapping(value = URL_USER_FIND_EMAIL, method = RequestMethod.GET)
    public List<UserDetails> getUserDetailsByEmail(@RequestParam("email") String email) throws InternalApiException
    {
        return this.userManagerService.getUserDetailsByEmail(email);
    }
}
