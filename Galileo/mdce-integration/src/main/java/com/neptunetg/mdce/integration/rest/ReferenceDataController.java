/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.mdce.common.internal.site.model.MdceReferenceData;
import com.neptunetg.mdce.integration.data.refdata.RefDataRepository;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import com.neptunetg.mdce.integration.data.refdata.RefDataWriteMode;
import com.neptunetg.mdce.integration.rest.json.refdata.JsonReferenceData;
import com.neptunetg.mdce.integration.rest.json.refdata.JsonReferenceDataDistributer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/*
* IF42 as defined in ETI 48-02
*/
@RestController
public class ReferenceDataController extends BaseTokenAuthenticatingRestController
{

    private static Logger logger = LoggerFactory.getLogger(ReferenceDataController.class);

    @Autowired
    private RefDataRepository refDataRepository;

    @RequestMapping(value = "/mdce/api/v1/refdata", method = RequestMethod.GET)
    public JsonReferenceData getReferenceData(@RequestParam("token") String token,
                                              HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException
    {
        //on top of validating the token, which is performed by ApiRequestInterceptor, check the token is from a valid BPC
        validateIsBusinessProcessConnector(token, request);

        final MdceReferenceData mdceReferenceData = refDataRepository.getRefData();

        return JsonReferenceData.from(mdceReferenceData);
    }

    @RequestMapping(value = "/mdce/api/v1/refdata", method = RequestMethod.PUT)
    public ResponseEntity setReferenceData(@RequestParam("token") String token,
                                           @RequestBody JsonReferenceData referenceData,
                                           HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException
    {
        //on top of validating the token, which is performed by ApiRequestInterceptor, check the token is from a valid BPC
        validateIsBusinessProcessConnector(token, request);

        //only BPC can call this function: BPC: SiteId = 0, Partner id = 1
        refDataRepository.insertRefData(referenceData, RefDataWriteMode.ReplaceAll);

        //return 204 (empty response body)
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/mdce/api/v1/refdata", method = RequestMethod.POST)
    public ResponseEntity appendReferenceData(@RequestParam("token") String token,
                                           @RequestBody JsonReferenceData referenceData,
                                           HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException
    {
        //on top of validating the token, which is performed by ApiRequestInterceptor, check the token is from a valid BPC
        validateIsBusinessProcessConnector(token, request);

        //only BPC can call this function: BPC: SiteId = 0, Partner id = 1
        refDataRepository.insertRefData(referenceData, RefDataWriteMode.Append);

        //return 204 (empty response body)
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
