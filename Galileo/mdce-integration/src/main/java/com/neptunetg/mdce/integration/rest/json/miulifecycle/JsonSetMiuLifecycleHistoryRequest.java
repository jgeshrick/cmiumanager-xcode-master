/*
 *  Neptune Technology Group
 *  Copyright 2017 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.miulifecycle;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Models a client request to add MIU lifecycle events to the history.
 */
public class JsonSetMiuLifecycleHistoryRequest
{
    @JsonProperty("lifecycle_states")
    private List<JsonHistoricMiuLifecycleState> lifecycleStates;

    public List<JsonHistoricMiuLifecycleState> getLifecycleStates()
    {
        return lifecycleStates;
    }

    public void setLifecycleStates(List<JsonHistoricMiuLifecycleState> lifecycleStates)
    {
        this.lifecycleStates = lifecycleStates;
    }
}
