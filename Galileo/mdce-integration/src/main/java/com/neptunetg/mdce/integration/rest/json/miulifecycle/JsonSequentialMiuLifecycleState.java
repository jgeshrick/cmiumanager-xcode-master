/*
 *  Neptune Technology Group
 *  Copyright 2017 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.miulifecycle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;

import java.time.Instant;

/**
 * A record of a transition to an MIU lifecycle state with sequence ID.
 */
public class JsonSequentialMiuLifecycleState
{
    @JsonProperty("sequence_id")
    private int sequenceId;

    @JsonProperty("miu_id")
    private int miuId;

    @JsonProperty("timestamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant timestamp;

    @JsonProperty("state")
    private MiuLifecycleStateEnum state;

    public JsonSequentialMiuLifecycleState()
    {
    }

    public JsonSequentialMiuLifecycleState(MiuLifecycleState state)
    {
        this.sequenceId = state.getSequenceId();
        this.miuId = state.getMiuId().numericValue();
        this.timestamp = state.getTransitionInstant();
        this.state = state.getLifecycleState();
    }

    public int getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(int sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public int getMiuId()
    {
        return miuId;
    }

    public void setMiuId(int miuId)
    {
        this.miuId = miuId;
    }

    public Instant getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp)
    {
        this.timestamp = timestamp;
    }

    public MiuLifecycleStateEnum getState()
    {
        return state;
    }

    public void setState(MiuLifecycleStateEnum state)
    {
        this.state = state;
    }
}
