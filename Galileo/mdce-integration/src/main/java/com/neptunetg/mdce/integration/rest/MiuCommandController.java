/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.amazonaws.services.s3.internal.RestUtils;
import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.common.data.miu.command.MiuCommandDetail;
import com.neptunetg.mdce.integration.audit.AuditService;
import com.neptunetg.mdce.integration.data.refdata.RefDataRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.domain.MiuCommandService;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.ForbiddenException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import com.neptunetg.mdce.integration.rest.json.JsonSendCommand;
import com.neptunetg.mdce.integration.rest.json.commandhistory.JsonGetCommandHistoryCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

/**
 * IF17 and IF32 Used by external systems to send and retrieve MIU commands.
 */
@RestController
public class MiuCommandController extends BaseTokenAuthenticatingRestController
{

    @Autowired
    private AuditService auditService;

    @Autowired
    private MiuCommandService miuCommandService;

    @Autowired
    private MdceIpcPublishService awsQueueService;

    @Autowired
    private RefDataRepository refDataRepository;

    /**
     * This web service is a more general form of Set MIU Config which can send an arbitrary command.
     * mdce-integration will insert the command into the miu_commands table. No data is send to MQTT broker at this point.
     * mdce-mqtt is responsible for monitoring the table and set commands to mqtt broker.
     * then all MIU’s in the MDCE will be returned.
     * @param miuId     the numeric ID of the MIU
     * @param miuCommand    commands to be send to the miu
     * @param request
     * @return
     * @throws NotAuthorizedException
     * @throws BadRequestException
     */
    @RequestMapping(value = "/mdce/api/v1/miu/{miuId}/command", method = RequestMethod.POST)
    public ResponseEntity sendCommand(
            @PathVariable("miuId") MiuId miuId,
            @RequestBody JsonSendCommand miuCommand,
            HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException, ForbiddenException
    {
        final SiteId siteId = getClientSiteId(request);

        checkSiteIdHasAccessToCmiuOrThrowForbidden(miuId, siteId);

        final MiuCommand command = miuCommand.toMiuCommand();

        if(command.getTargetDeviceType() == null)
        {
            throw new BadRequestException("Bad request, target device type not recognised");
        }

        command.setUserName(this.auditService.getRequesterFromSiteId(siteId));

        int result = this.miuCommandService.createMiuCommand(command);

        if(command.getTargetDeviceType() == MiuType.L900)
        {
            //send SQS to inform mdce-lora application to check message for pending commands to be processed
            awsQueueService.sendMessage(MdceIpcPublishService.LORA_PACKETS_TO_SEND,
                    String.format("%s: %d - %s",
                            LocalDateTime.now(), command.getCommandType(), "MIU Command"));
        }
        else
        {
            //send SQS to inform mdce-mqtt application to check message for pending commands to be processed
            awsQueueService.sendMessage(MdceIpcPublishService.SEND_COMMAND_QUEUE,
                    String.format("%s: %d - %s",
                            LocalDateTime.now(), command.getCommandType(), "MIU Command"));
        }

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * Retrieve command packets sent from MDCE to endpoints for the site id that the token represents.
     * @param miuId     the numeric ID of the MIU for which to get the command history
     * @param minInsertDate start date to retrieve data from, including time and TZ offset (so UTC can be calculated) – e.g. 2015-01-01T14:01:24-05:00.  Ignored.
     * @param maxInsertDate end date to retrieve data for.  If not supplied will default to current date + 1.  Ignored
     * @return command history list
     * @throws NotAuthorizedException
     * @throws BadRequestException
     */
    @RequestMapping(value = "/mdce/api/v1/miu/{miuId}/command-history", method = RequestMethod.GET)
    public JsonGetCommandHistoryCollection getCommandHistory(
            @PathVariable("miuId") MiuId miuId,
            @RequestParam(value = "min_insert_date") String minInsertDate,
            @RequestParam(value = "max_insert_date", required = false) String maxInsertDate,
            HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException, ForbiddenException
    {
        final SiteId siteId = getClientSiteId(request);

        checkSiteIdHasAccessToCmiuOrThrowForbidden(miuId, siteId);

        final List<MiuCommandDetail> miuCommandHistory = this.miuCommandService.getCommandHistory(miuId);

        return JsonGetCommandHistoryCollection.from(miuCommandHistory);

    }

}
