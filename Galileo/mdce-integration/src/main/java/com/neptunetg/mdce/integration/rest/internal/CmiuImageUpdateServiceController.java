/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.neptunetg.common.packet.model.taggeddata.tags.ImageMetaData;
import com.neptunetg.mdce.common.aws.S3Manager;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import com.neptunetg.mdce.common.internal.miu.service.CmiuImageUpdateService;
import com.neptunetg.mdce.integration.data.miu.MiuImageRepository;
import com.neptunetg.mdce.integration.utility.CmiuImageDistributionUtility;
import com.neptunetg.mdce.integration.utility.ImageVersionNumberComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Provides image update services and image registration services
 */
@RestController
@RequestMapping("/service")
public class CmiuImageUpdateServiceController implements CmiuImageUpdateService
{
    private static final Logger log = LoggerFactory.getLogger(CmiuImageUpdateServiceController.class);
    private final String IMAGE_DISTRIBUTION_BUCKET = "neptune-image-distribution";

    private static final Comparator<String> IMAGE_VERSION_NUMBER_COMPARATOR = new ImageVersionNumberComparator();

    @Autowired
    private MiuImageRepository miuImageRepo;

    @Autowired
    private S3Manager s3Manager;
    /**
     * Get a list of image description based on the image type
     * imageType the type of image to get the list
     * @return list of image
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(value = URL_CMIU_IMAGE, params = REQUEST_PARAM_IMAGE_TYPE)
    public List<ImageDescription> getAvailableImageList(ImageDescription.ImageType imageType) throws InternalApiException
    {
        List<ImageDescription> result = miuImageRepo.getRegisteredImageList(imageType);

        return result;
    }

    /**
     * Get a list of all available image from the RDS
     *
     * @return
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(URL_CMIU_IMAGE)
    public List<ImageDescription> getAvailableImageList() throws InternalApiException
    {
        List<ImageDescription> result = miuImageRepo.getRegisteredImageList(null);
        return result;
    }

    /**
     * Get a list of available build artifact (from S3)for purpose of selection/importing into the RDS
     *
     * @param imageType the type of image to retrieve, if null, all imageTypes are returned
     * @return a list of available artifacts grouped by imageType
     */
    @Override
    @RequestMapping(value = URL_CMIU_BUILD_ARTIFACT)
    public Map<ImageDescription.ImageType, List<String>> getBuildArtifactList(
            @RequestParam(REQUEST_PARAM_IMAGE_TYPE) ImageDescription.ImageType imageType) throws InternalApiException
    {
        List<S3ObjectSummary> s3ObjectSummaries = s3Manager.getItemsInFolder(IMAGE_DISTRIBUTION_BUCKET, "");

        //Sort the artifact, latest version at the top.

        final Map<ImageDescription.ImageType, List<String>> ret = s3ObjectSummaries.stream()
                .filter(s -> !s.getKey().endsWith("/"))      //ignore folder
                .filter(s -> s.getKey().toLowerCase().endsWith(".bin"))      //only include files ending with '.bin'
                .map(S3ObjectSummary::getKey)
                .sorted(IMAGE_VERSION_NUMBER_COMPARATOR)
                .collect(Collectors.groupingBy(CmiuImageDistributionUtility::s3ImageFolderToImageType));

        return ret;
    }



    /**
     * Register a build artifact to be used in future image update.
     *
     * @param imageType type if image
     * @param filePath path to s3 storage.
     */
    @Override
    @RequestMapping(value = URL_CMIU_BUILD_ARTIFACT, method=RequestMethod.POST)
    public void registerBuildArtifact(@RequestParam(REQUEST_PARAM_IMAGE_TYPE) ImageDescription.ImageType imageType, @RequestParam(REQUEST_PARAM_NAME) String filePath) throws InternalApiException
    {
        //get corresponding image meta data from s3 memory map file.
        InputStream memoryMapStream = s3Manager.getObjectAsStream(IMAGE_DISTRIBUTION_BUCKET, CmiuImageDistributionUtility.getMemoryMapPropertyKey(filePath));

        int imageSize;
        int startAddress;

        if (memoryMapStream != null)
        {
            final Properties memoryMapProperties = CmiuImageDistributionUtility.getMemoryMap(memoryMapStream);

            try
            {
                imageSize = CmiuImageDistributionUtility.getImageSizeFromImageType(imageType, memoryMapProperties);
            }
            catch (Exception ex)
            {
                imageSize = CmiuImageDistributionUtility.getDefaultImageSizeFromImageType(imageType);
            }

            try
            {
                startAddress = CmiuImageDistributionUtility.getImageSizeFromImageType(imageType, memoryMapProperties);
            }
            catch (Exception ex)
            {
                startAddress = CmiuImageDistributionUtility.getDefaultStartAddressFromImageType(imageType);
            }

        }
        else
        {
            log.error("Cannot get file: {} from s3, using default definition for image size and start address", filePath);
            imageSize = CmiuImageDistributionUtility.getDefaultImageSizeFromImageType(imageType);
            startAddress = CmiuImageDistributionUtility.getDefaultStartAddressFromImageType(imageType);
        }

        miuImageRepo.addRegisteredImage(filePath, filePath, imageSize, startAddress);

    }

    /**
     * A mapper to convert internal-api imageType to packetUtil image type
     * @param imageType image type
     * @return
     */
    private static ImageMetaData.ImageType toPacketUtilImageType(ImageDescription.ImageType imageType)
    {
        ImageMetaData.ImageType result;

        switch (imageType)
        {
            case FirmwareImage:
                result = ImageMetaData.ImageType.FIRMWARE_IMAGE;
                break;
            case BootloaderImage:
                result = ImageMetaData.ImageType.BOOTLOADER_IMAGE;
                break;
            case ConfigImage:
                result = ImageMetaData.ImageType.CONFIG_IMAGE;
                break;
            case TelitModuleImage:
                result = ImageMetaData.ImageType.TELIT_MODULE_IMAGE;
                break;
            case BleConfigImage:
                result = ImageMetaData.ImageType.BLE_CONFIG_IMAGE;
                break;
            case ArbConfigImage:
                result = ImageMetaData.ImageType.ARB_CONFIG_IMAGE;
                break;
            case EncryptionImage:
                result = ImageMetaData.ImageType.ENCRYPTION_IMAGE;
                break;
            default:
                throw new IllegalArgumentException("Unknown image type");
        }

        return result;
    }
}
