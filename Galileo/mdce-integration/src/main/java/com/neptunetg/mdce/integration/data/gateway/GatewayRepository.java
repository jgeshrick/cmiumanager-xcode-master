/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.gateway;


import com.neptunetg.mdce.common.internal.gateway.model.GatewayHeardPackets;
import com.neptunetg.mdce.common.internal.gateway.model.GatewayMonthlyStats;
import com.neptunetg.mdce.integration.data.site.SiteId;

import java.time.Instant;
import java.util.List;
import java.util.Map;

public interface GatewayRepository
{

    /**
     * Update the heard time and statistics for the gateway
     * @param gatewayDbId
     * @param minInsertDate
     * @param maxInsertDate
     * @param miusHeard
     * @param ookPacketsHeard
     * @param fskPacketsHeard
     * @param configPacketsHeard
     * @param transferInterval
     */
    void updateGatewayHeardTimeAndStats(GatewayId gatewayId, Integer gatewayDbId, Instant minInsertDate, Instant maxInsertDate,
                                        int miusHeard, int ookPacketsHeard, int fskPacketsHeard, int configPacketsHeard,
                                        int transferInterval, boolean storeR900Packets);

    /**
     * Get a list of all the gateways for various inputs
     * @param options
     * @return list of gateways
     */
    List<GatewayDetails> getGateways(Map<String, String> options);

    /**
     * Get a list of gateways for a specific site ID
     * @param siteId
     * @return
     */
    List<GatewayDetails> getGatewaysForUtility(SiteId siteId);

    /**
     * Get a list of all gateways
     * @return
     */
    List<GatewayDetails> getGatewaysForAllUtility();

    /**
     * Used to get the details of the most recently heard packet
     * @param gatewayId
     * @return
     */
    GatewayHeardPackets getLatestGatewayHeardPackets(GatewayId gatewayId);

    /**
     * Used to get the details of the most recently monthly heard packet stats
     * @param gatewayId
     * @return
     */
    GatewayMonthlyStats getLatestGatewayMonthlyStats(GatewayId gatewayId);

    /**
     * Gets a count of all the gateways selected by a specific option
     * @param options
     * @return
     */
    Integer getGatewaysCount(Map<String, String> options);

    /**
     * Get the current owning site of the gateway
     * @param  gatewayId ID of gateway
     * @return Owning site ID, or null if no owner
     */
    SiteId getGatewayOwner(GatewayId gatewayId);

    /**
     * Update gateway details with new site id and meter active status
     * @param gatewayId gateway to update
     * @param newSiteId new site id
     * @return number of rows updated
     */
    int updateGatewayOwnerDetails(GatewayId gatewayId, SiteId newSiteId);

    /**
     * Update gateway configuration details as per gateway headers
     * @param gatewayId Gateway to update
     * @param gatewayConfigDate Date from which config applies
     * @param gatewayConfigSiteId Site ID - from headers
     * @param gatewayConfigName Name of gateway - from headers
     * @param gatewayConfigNumericValues Numeric values from headers
     * @return  ID of GW in DB
     */
    Integer updateGatewayConfig(GatewayId gatewayId, Instant gatewayConfigDate, SiteId gatewayConfigSiteId, String gatewayConfigName, Map<String, Integer> gatewayConfigNumericValues);
}