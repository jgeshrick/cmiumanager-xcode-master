/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.site;




import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;

import java.time.Instant;
import java.util.List;

public interface SiteRepository
{

    /**
     * Get a list of all sites
     * @return site list
     */
    List<SiteDetails> getSiteList();

    /**
     * Get info about a site
     * @param siteId ID of site
     * @return Site, or null if not found
     */
    SiteDetails getSiteDetails(SiteId siteId);

    /**
     * Check whether a site id has been defined in the database
     * @param siteId the id of the site
     * @return true if the site id has been entered into the database
     */
    boolean hasSite(SiteId siteId);

    /**
     * Create a new site
     * @param siteId new site nd
     * @param siteName new site name
     * @return rows updated
     */
    int createSite(SiteId siteId, String siteName);

    /**
     * Ensure site exists in DB
     * @param siteId Site ID
     * @return true if site is newly created
     */
    boolean ensureSiteExists(SiteId siteId);

    /**
     * Ensure site exists in DB
     * @param siteId Site ID
     * @param siteName if doesn't exist
     * @return true if site is newly created
     */
    boolean ensureSiteExists(SiteId siteId, String siteName);


    /**
     * Update the record of when the site requested packets
     * @param siteId Site
     * @param queryTime Time
     */
    void updateLastDataRequest(SiteId siteId, Instant queryTime);

    /**
     * Finds sites that have not contacted mdce between a configurable amount of time.
     *
     * @param siteConnectionCheckHours max amount of hours an site should be talking
     *        to mdce anything after that is an issue.
     * @param siteConnectionIgnoreAfterHours max amount of hours a site can be inactive
     *        before we declare inactive and ignore.
     * @return list of sites
     */
    List<SiteDetails> getSitesNotRequestingDataWithinXHours(int siteConnectionCheckHours, int siteConnectionIgnoreAfterHours) throws InternalApiException;

}