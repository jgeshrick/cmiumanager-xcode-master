/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.command.MiuCommand;

import javax.xml.bind.DatatypeConverter;
import java.time.Instant;

/**
    JSON object for send command IF17
*/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonSendCommand
{
    @JsonProperty("miu_id")
    private long miuId;

    @JsonProperty("source_device_type")
    private String sourceDeviceType;

    @JsonProperty("target_device_type")
    private String targetDeviceType;

    @JsonProperty("command_type")
    private int commandType;

    @JsonProperty("packet")
    private String packet;

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public String getSourceDeviceType()
    {
        return sourceDeviceType;
    }

    public void setSourceDeviceType(String sourceDeviceType)
    {
        this.sourceDeviceType = sourceDeviceType;
    }

    public String getTargetDeviceType()
    {
        return targetDeviceType;
    }

    public void setTargetDeviceType(String targetDeviceType)
    {
        this.targetDeviceType = targetDeviceType;
    }

    public int getCommandType()
    {
        return commandType;
    }

    public void setCommandType(int commandType)
    {
        this.commandType = commandType;
    }

    public String getPacket()
    {
        return packet;
    }

    public void setPacket(String packet)
    {
        this.packet = packet;
    }

    public MiuCommand toMiuCommand()
    {
        //TODO: source device type, target device type, packet type input fields not recorded!!
        MiuCommand command = new MiuCommand();
        command.setMiuId(MiuId.valueOf(this.getMiuId()));
        command.setCommandType(this.getCommandType());
        command.setCommandEnteredTime(Instant.now());
        command.setCommandParams(DatatypeConverter.parseHexBinary(this.getPacket()));
        command.setSourceDeviceType(this.getSourceDeviceType());
        command.setTargetDeviceType(this.getTargetDeviceType());

        return command;
    }
}
