/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.scheduled.lifecycle.alert.service;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleStateTransistion;

import java.util.List;

/**
 * Created by WJD1 on 23/05/2016.
 * A class to check if an alert needs to be raised for a CMIU given its lifecycle state
 */
public interface CmiuLifecycleAlertService
{
    /**
     * Get a list of CMIUs stuck in a state for too long
     * @return A list of CmiuLifecycleState objects, which contain the CMIU ID, state, and time the state was entered
     * return an empty list if no CMIUs to raise alerts for
     */
    List<MiuLifecycleState> getCmiusStuckInLifecycleStateForTooLong();

    /**
     * Get a list of CMIUs that haven't reached the Claimed state for over
     * 6 months since transitioning into the Activated state
     * @return A list of objects containing the current state, and the previous Activated state
     */
    List<MiuLifecycleStateTransistion> getCmiusNotClaimedInGracePeriod();

    /**
     * Get a list of CMIUs stuck in a state for too long
     * @return A list of CmiuLifecycleState objects, which contain the CMIU ID, state, and time the state was entered
     * return an empty list if no CMIUs to raise alerts for
     */
    List<MiuLifecycleState> getCmiusStuckInLifecycleStateForAlerts();

    /**
     * Get a list of CMIUs that have performed an unexpected lifecycle state transition
     * @return List of CMIUs that performed an unexpected lifecycle state transition
     */
    MiuLifecycleState getMiuCurrentLifecycleState(MiuId miuId);
}
