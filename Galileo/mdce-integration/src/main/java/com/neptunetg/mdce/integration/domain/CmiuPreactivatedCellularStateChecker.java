/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.cns.CellularStatus;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleService;
import com.neptunetg.mdce.integration.cns.CellularNetworkServiceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by WJD1 on 24/05/2016.
 * A class to check the cellular state of CMIUs in the unactivated lifecycle state
 */
@Service
public class CmiuPreactivatedCellularStateChecker implements AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(CmiuPreactivatedCellularStateChecker.class);


    @Autowired
    private MiuLifecycleService miuLifecycleService;

    @Autowired
    private CellularNetworkServiceManager cellularNetworkServiceManager;

    @Autowired
    private CmiuRepository cmiuRepository;

    @Autowired
    public CmiuPreactivatedCellularStateChecker(MdceIpcSubscribeService awsQueueService)
    {
        awsQueueService.register(this, MdceIpcSubscribeService.CHECK_PENDING_CELLULAR_STATE, Duration.ofSeconds(5L), 100);
    }

    /**
     * Handler when SQS message to the registered SQS queue name has been received.
     * @param message the message body
     * @param messageId the message id
     * @param messageReceipt message receipt
     * @param cancellationToken abort flag
     * @return true to delete the message from SQS
     */
    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        log.debug("Checking the cellular status of CMIUs in the PreActivated lifecycle state");
        if(!this.checkMnoState(MiuLifecycleStateEnum.PREACTIVATED, CellularStatus.ACTIVE, MiuLifecycleStateEnum.ACTIVATED, cancellationToken))
        {
            return false;
        }
        // Retired maybe
        if(!this.checkMnoState(MiuLifecycleStateEnum.PRESUSPENDED, CellularStatus.RETIRED, MiuLifecycleStateEnum.SUSPENDED, cancellationToken))
        {
            return false;
        }
        if(!this.checkMnoState(MiuLifecycleStateEnum.DEATHBED, CellularStatus.DE_ACTIVE, MiuLifecycleStateEnum.DEAD, cancellationToken))
        {
            return false;
        }
        return true;
    }

    public boolean checkMnoState(MiuLifecycleStateEnum lookUpmiuLifecycleState, CellularStatus mnoLifecycleState,
                                 MiuLifecycleStateEnum newMuiLifeCycleState, AtomicBoolean cancellationToken)
    {
        final List<MiuLifecycleState> preactivatedCmius =
                miuLifecycleService.getCmiusInLifecycleStateBefore(lookUpmiuLifecycleState, Instant.now());

        for(MiuLifecycleState cmiu : preactivatedCmius)
        {
            if (cancellationToken.get())
            {
                return false;
            }
            try
            {
                CellularDeviceDetail cellularDeviceDetail = cmiuRepository.getCellularConfig(cmiu.getMiuId());

                if (cellularDeviceDetail != null)
                {
                    //device details exist in DB
                    final CellularNetworkService cellularNetworkService =
                            cellularNetworkServiceManager.getCns(cellularDeviceDetail.getMno());

                    final String provisioningStatus = cellularNetworkService.getDeviceProvisioningStatus(
                            cellularDeviceDetail.getIccid());
                    if(provisioningStatus != null)
                    {
                        if (mnoLifecycleState.toString().equalsIgnoreCase(provisioningStatus)) //note: provisioningStatus could be null if device not found
                        {
                            miuLifecycleService.setMiuLifecycleState(cmiu.getMiuId(), newMuiLifeCycleState);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                log.error("Error while trying to get cellular config for CMIU: " + cmiu.getMiuId(), e);
            }
        }

        return true;
    }
}
