/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.miu;

import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import com.neptunetg.mdce.integration.utility.CmiuImageDistributionUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * MIU Repository for MySql database
 */
@Repository
public class JdbcMiuImageRepository implements MiuImageRepository
{
    private static final String INSERT_REGISTERED_IMAGES = "INSERT INTO mdce.cmiu_registered_images (name, s3_path, image_size, start_address, type, register_date) " +
            " VALUES (?, ?, ?, ?, ?, NOW())";
    private static final int maxResults = 100;

    // fields
    private final JdbcTemplate db;

    @Autowired
    public JdbcMiuImageRepository(JdbcTemplate db)
    {
        this.db = db;
    }


    /**
     * Retrieve all entries in the cmiu_registered_images table
     *
     * @return image description list
     */
    @Override
    public List<ImageDescription> getRegisteredImageList(ImageDescription.ImageType desiredType)
    {
        final List<ImageDescription> result;
        if (desiredType == null)
        {
            String sql = "SELECT cmiu_image_id,\n" +
                    "    name,\n" +
                    "    s3_path,\n" +
                    "    register_date,\n" +
                    "    type,\n" +
                    "    image_size,\n" +
                    "    start_address\n" +
                    "FROM cmiu_registered_images";
            result = this.db.query(sql, JdbcMiuImageRepository::mapCmiuRegisteredImage);
        }
        else
        {
            String sql = "SELECT cmiu_image_id,\n" +
                    "    name,\n" +
                    "    s3_path,\n" +
                    "    register_date,\n" +
                    "    type,\n" +
                    "    image_size,\n" +
                    "    start_address\n" +
                    "FROM cmiu_registered_images " +
                    "WHERE type=?";
            result = this.db.query(sql, JdbcMiuImageRepository::mapCmiuRegisteredImage, desiredType.getValue());
        }
        return result;
    }

    private static ImageDescription mapCmiuRegisteredImage(ResultSet rs, int rowNum) throws SQLException
    {
        ImageDescription mappedObject = new ImageDescription();
        mappedObject.setFileName(rs.getString("s3_path"));
        mappedObject.setImageType(ImageDescription.ImageType.fromValue(rs.getInt("type")));
        mappedObject.setSizeBytes(rs.getInt("image_size"));
        mappedObject.setStartAddress(rs.getInt("start_address"));
        mappedObject.setRevision(CmiuImageDistributionUtility.getVersionInformation(rs.getString("s3_path")));

        return mappedObject;
    }

    /**
     * Register a new image
     *
     * @param name     filename
     * @param imageUrl s3 url
     * @return number of rows added, expected 1 unless the image has already been registered
     */
    @Override
    public int addRegisteredImage(String name, String imageUrl, int imageSize, int startAddress)
    {
        String sql = "SELECT count(*) FROM cmiu_registered_images WHERE name=? AND s3_path=?";
        final Integer existingMatchingImageCount = this.db.queryForObject(sql, Integer.class, name, imageUrl);
        if (existingMatchingImageCount != null && existingMatchingImageCount.intValue() == 0)
        {
            return this.db.update(INSERT_REGISTERED_IMAGES, "",
                    imageUrl, imageSize, startAddress, CmiuImageDistributionUtility.s3ImageFolderToImageType(imageUrl).getValue());
        }
        else
        {
            return 0; //already there
        }
    }




}
