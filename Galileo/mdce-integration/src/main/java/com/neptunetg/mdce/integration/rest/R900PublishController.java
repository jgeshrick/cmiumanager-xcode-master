/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import com.neptunetg.mdce.integration.utility.SaveToFileStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;


/**
 * Implements MDCE interface PUT data for receiving R900 gateway data file, as detailed in ETI 48-02
 * Currently uploads tar file, then kicks off processor; this will be done by scheduler in time
 */
@Controller
public class R900PublishController
{
    public static final String UNPROCESSED = "unprocessed";
    private static final Logger logger = LoggerFactory.getLogger(R900PublishController.class);
    private final File toProcessFolder;

    /**
     * Constructor for controller
     * @param tarballDumpFolder Folder to dump copies of the PUT tarballs
     */
    @Autowired
    public R900PublishController(@Value("#{envProperties['gateway.data.dump.folder']?:'/'}") String tarballDumpFolder)
    {
        this.toProcessFolder = new File(tarballDumpFolder);
        logger.info("Tarball dump folder: " + toProcessFolder.getAbsolutePath());
    }

    /**
     * Upload a tar ball to mdce-integration via HTTP PUT. Note the request stream must not be form encoded.
     * @param request http request object
     * @param siteId the site id that owns the collector
     * @param collectorName name of the collector.  Can contain spaces.
     * @param fileName The file name of the contents.
     * @return temporary return an OK text string to signify success of operation.
     * @throws Exception
     */
    @RequestMapping(value="/4/data", method=RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity handleFileUpload(HttpServletRequest request,
                                                 @RequestParam("site_id") SiteId siteId,
                                                 @RequestParam("collectorname") String collectorName,
                                                 @RequestParam("filename") String fileName) throws Exception
    {
        try
        {
            ensureSiteIdIsFirstBitOfFilename(siteId, fileName);

            final InputStream tarDataIn = request.getInputStream();

            final File tarballDumpFileUnprocessed = ensureDirExistsButFileDoesnt(UNPROCESSED, fileName);

            createLockFileAndUpload(fileName, tarDataIn, tarballDumpFileUnprocessed);

            if (logger.isDebugEnabled())
            {
                logger.debug("Successfully completed upload of gateway tarball " + fileName + " and siteId of " + siteId.numericValue()  + " that was PUT to " + request.getRequestURI());
            }
        }
        catch (Exception e)
        {
            logger.error("Exception occurred while uploading tarball " + fileName + " in request " + request.getRequestURI(), e);
            throw new MdceRestException("Exception occurred uploading tar file " + fileName + " in request " + request.getRequestURI(), e);
        }

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    private File createLockFileAndUpload(String fileName, InputStream tarDataIn, File tarballDumpFileUnprocessed) throws IOException
    {
        final File lockFile = createLockFile(tarballDumpFileUnprocessed);
        try
        {
            return saveTarFileToDirectory(tarDataIn, tarballDumpFileUnprocessed, fileName);
        }
        finally
        {
            lockFile.delete();
        }
    }

    private void ensureSiteIdIsFirstBitOfFilename(SiteId siteId, String fileName)
    {
        final SiteId siteIdInFilename = new SiteId(Integer.valueOf(fileName.substring(0, fileName.indexOf("_"))));
        if (!siteIdInFilename.equals(siteId))
        {
            throw new MdceRestException("Site id in filename does not equal supplied siteId in url " + fileName + " in request: " + siteId);
        }
    }

    private File saveTarFileToDirectory(InputStream tarDataIn, File dirName, String fileName) throws IOException
    {
        //TODO
        try (final SaveToFileStream saveToFileStream = new SaveToFileStream(tarDataIn, dirName))
        {
            return new File(dirName, fileName);
        }
    }

    private File ensureDirExistsButFileDoesnt(String dirName, String fileName)
    {
        final File folder = new File(toProcessFolder, dirName);
        ensureDirectoryExists(folder);

        ensureLockFileDoesNotExist(folder, fileName);

        final File file = new File(folder, fileName);
        ensureFileDoesNotExist(file);

        return file;
    }

    private void ensureLockFileDoesNotExist(File folder, String fileName)
    {
        final File lockFile = new File(folder, fileName + ".lock");

        if (lockFile.exists()) {
            throw new MdceRestException("Exception uploading file - file already exists with lock");
        }

    }

    private File createLockFile(File tarballDumpFileUnprocessed)
    {
        final File lockFile = new File(tarballDumpFileUnprocessed.getParent(), tarballDumpFileUnprocessed.getName() + ".lock");
        try
        {
            lockFile.createNewFile();
        } catch (Exception e) {
            throw new MdceRestException("Error creating lock file " + lockFile.getAbsolutePath(), e);
        }

        return lockFile;
    }

    private void ensureFileDoesNotExist(File file)
    {
        if (file.exists())
        {
            file.delete();
        }
    }

    private void ensureDirectoryExists(File folder)
    {
        if (!folder.exists())
        {
            folder.mkdirs();
        }
    }
}