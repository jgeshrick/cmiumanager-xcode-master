/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.integration.gui.debug;

import com.neptunetg.mdce.integration.domain.WnsCallbackEventsService;
import com.verizon.callback.CallbackRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by SML1 on 22/04/2015.
 */
@RestController
public class WnsCallbackHistoryController
{
    @Autowired
    WnsCallbackEventsService wnsCallbackEventsService;

    /**
     * A debug page to display all Verizon callbacks that is received since the server started.
     * @return
     */
    @RequestMapping(value="/pages/debug/verizon-callback-events", method = RequestMethod.GET)
    public List<CallbackRequest> getVerizonCallbackEvents()
    {
        return wnsCallbackEventsService.getCallbackEvents();
    }

    /**
     * A hack to clear events
     */
    @RequestMapping(value="/pages/debug/verizon-callback-events/clear", method = RequestMethod.GET)
    public void clearVerizonCallbackEvents()
    {
        wnsCallbackEventsService.clearEvents();
    }

}
