/*
 *  Neptune Technology Group
 *  Copyright 2017 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.miulifecycle;

import com.neptunetg.common.data.miu.lifecycle.model.HistoricMiuLifecycleState;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;

import java.time.Instant;

/**
 * Models an MIU lifecycle state that is to be added to the MIU lifecycle history.
 */
public class JsonHistoricMiuLifecycleState
{
    /**
     * A client-supplied identity used to track the status of the operation.
     */
    @JsonProperty("ref_id")
    private int referenceId;

    @JsonProperty("miu_id")
    private MiuId miuId;

    @JsonProperty("lifecycle_state")
    private String lifecycleState;

    @JsonProperty("timestamp")
    private Instant timestamp;

    public JsonHistoricMiuLifecycleState(){}

    public JsonHistoricMiuLifecycleState(int referenceId, MiuId miuId, String lifecycleState, Instant timestamp){

        this.referenceId = referenceId;
        this.miuId = miuId;
        this.lifecycleState = lifecycleState;
        this.timestamp = timestamp;
    }

    public int getReferenceId()
    {
        return referenceId;
    }

    public void setReferenceId(int referenceId)
    {
        this.referenceId = referenceId;
    }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public void setMiuId(MiuId miuId)
    {
        this.miuId = miuId;
    }

    public String getLifecycleState()
    {
        return lifecycleState;
    }

    public void setLifecycleState(String lifecycleState)
    {
        this.lifecycleState = lifecycleState;
    }

    public Instant getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp)
    {
        this.timestamp = timestamp;
    }

    public HistoricMiuLifecycleState toServiceModel()
    {
        HistoricMiuLifecycleState serviceModel = new HistoricMiuLifecycleState();
        serviceModel.setReferenceId(referenceId);
        serviceModel.setMiuId(miuId);
        serviceModel.setLifecycleState(MiuLifecycleStateEnum.fromStringValue(lifecycleState));
        serviceModel.setTransitionInstant(timestamp);

        return serviceModel;
    }
}
