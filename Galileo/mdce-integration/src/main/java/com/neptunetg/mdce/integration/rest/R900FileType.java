/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.rest;


public enum R900FileType
{

    OOK_READING("[READINGS OOK]"),
    FSK_READING("[READINGS FSK]"),
    FSK_CONFIG("[CONFIG FSK]"),
    FSK_RATE("[RATE FSK]"),
    FSK_UNKNOWN("[UNKNOWN FSK]");

    private String descriptor;

    R900FileType(String descriptor)
    {
        this.descriptor = descriptor;
    }

    public static R900FileType getR900FileType(String fromString)
    {
        for (R900FileType r900FileType : values())
        {
            if (r900FileType.getDescriptor().equals(fromString)
                    || r900FileType.getName().equals(fromString))
            {
                return r900FileType;
            }
        }
        return null;
    }

    public String getDescriptor()
    {
        return descriptor;
    }

    public String getName()
    {
        return name();
    }

}