/*
 *  Neptune Technology Group
 *  Copyright 2016 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.miulifecycle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;

import java.time.Instant;

/**
 * A record of a transition to an MIU lifecycle state.
 */
public class JsonMiuLifecycleState
{
    @JsonProperty("timestamp")
    private String timestamp;

    @JsonProperty("state")
    private MiuLifecycleStateEnum state;

    public JsonMiuLifecycleState()
    {
    }

    public JsonMiuLifecycleState(MiuLifecycleStateEnum state, Instant timestamp)
    {
        this.state = state;
        this.timestamp = timestamp.toString();
    }

    public String getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(String timestamp)
    {
        this.timestamp = timestamp;
    }

    public MiuLifecycleStateEnum getState()
    {
        return state;
    }

    public void setState(MiuLifecycleStateEnum state)
    {
        this.state = state;
    }
}
