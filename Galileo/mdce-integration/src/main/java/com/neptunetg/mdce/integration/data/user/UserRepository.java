/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.data.user;

import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.common.internal.user.model.MdceUserRole;
import com.neptunetg.mdce.common.internal.user.model.PasswordResetRequest;
import com.neptunetg.mdce.common.internal.user.model.UserAlertSettings;
import com.neptunetg.mdce.common.internal.user.model.UserDetails;

import java.time.Instant;
import java.util.List;
import java.util.Map;

/**
 * Created by WJD1 on 31/07/2015.
 * Interface to define the functions the user repository implementation should present
 */
public interface UserRepository
{
    boolean addUser(String userName, String email, MdceUserRole userLevel, byte[] hash, byte[] salt, int iterations);

    boolean removeUser(long userId);

    boolean setUserPassword(long userId, byte[] hash, byte[] salt, int iterations);

    boolean setUserLevel(long userId, MdceUserRole userLevel);

    boolean setUserEmail(long userId, String email);

    boolean setUserName(long userId, String userName);

    UserDetails getUserDetailsByUserName(String userName);

    UserDetails getUserDetailsById(long userId);

    List<UserDetails> getUserDetailsByUserEmail(String userEmail);

    List<UserDetails> getUserList();

    /**
     * Gets users matching the supplied search options.
     * @param options The search criteria to use for the operation.
     * @return A list of users who match all of the supplied search criteria.
     */
    List<UserDetails> findUsers(Map<String, Object> options);

    List<SiteDetails> getSiteListForUser(long userId);

    List<SiteDetails> getSiteListNotForUser(long userId);

    boolean addSiteToUser(long userId, long siteId);

    boolean removeSiteFromUser(long userId, long siteId);

    UserAlertSettings getUserAlertSettings(long userId);

    /**
     * Set the alert settings for a user, such as which alerts to email them on
     * @param userAlertSettings a class to hold the settings
     * @return true is successful
     */
    boolean setUserAlertSettings(UserAlertSettings userAlertSettings);

    boolean setUserAccountLockoutEnabled(long userId, boolean value);

    boolean setUserFailedLoginAttempts(long userId, long failedLoginAttempts);

    boolean setUserDisabledUntil(long userId, Instant disabledUntil);

    boolean setAllowConcurrentSessions(long userId, boolean allowConcurrentSessions);

    /**
     * Adds a new password reset request for a user.
     * @param token The unique, private token that allows the reset request to be used by the requestor.
     * @param userId The user ID of the user.
     * @param matchedUserName The userName of the user.
     * @param enteredEmailAddress The email address, exactly as entered by the requestor.
     * @param validUntilTimestamp The instant until which the request will remain valid.
     * @param requestorIpAddress The IP address from which the request originated.
     * @return A boolean value indicating whether the request was successfully created.
     */
    boolean addPasswordResetRequest(String token, long userId, String matchedUserName,
                                    String enteredEmailAddress, Instant validUntilTimestamp,
                                    String requestorIpAddress);

    /**
     * Marks a password reset request as having been claimed.
     * @param passwordResetRequestId The password reset request ID.
     * @param token The token associated with the password reset request.
     * @param claimantIpAddress The IP address from which the claim originated.
     * @return A boolean value indicating whether the operation was successful.
     */
    boolean claimPasswordResetRequest(long passwordResetRequestId, String token, String claimantIpAddress);

    /**
     * Immediately expires any unexpired password reset requests for a specific user.
     * @param userId The user ID.
     * @return The number of password reset requests that were expired by the operation.
     */
    int expirePasswordResetRequestsByUser(long userId);

    /**
     * Gets a password reset request by its token.
     * @param token The token associated with the password reset request.
     * @return If the token is found, returns the associated password reset request, even if it is claimed or expired;
     *         otherwise, returns null.
     */
    PasswordResetRequest getPasswordResetRequestByToken(String token);
}
