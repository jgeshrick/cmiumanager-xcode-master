/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.audit.model.AuditLog;
import com.neptunetg.mdce.common.internal.audit.model.AuditLogPagedResult;
import com.neptunetg.mdce.common.internal.sim.model.SimDisplayDetails;
import com.neptunetg.mdce.common.internal.sim.model.SimDisplayDetailsPagedResult;
import com.neptunetg.mdce.common.internal.sim.model.SimLifecycleState;
import com.neptunetg.mdce.common.internal.sim.service.SimService;
import com.neptunetg.mdce.integration.audit.AuditService;
import com.neptunetg.mdce.integration.data.sim.SimDetails;
import com.neptunetg.mdce.integration.data.sim.SimSearchRepository;
import com.neptunetg.mdce.integration.data.sim.SimUpdateRepository;
import com.neptunetg.mdce.integration.rest.BaseTokenAuthenticatingRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/service")
public class SimDetailsInternalController extends BaseTokenAuthenticatingRestController implements SimService
{
    @Autowired
    private SimSearchRepository simSearchRepository;

    @Autowired
    private SimUpdateRepository simUpdateRepository;

    @Autowired
    private AuditService auditService;

    @Override
    @ResponseBody
    @RequestMapping(value = SimService.URL_SIM_LIST, method = RequestMethod.GET)
    public SimDisplayDetailsPagedResult getSimList(@RequestParam Map<String,String> options) throws InternalApiException
    {
        final List<SimDetails> simDetails =  simSearchRepository.getSims(options);

        Integer count = simSearchRepository.getSimCount(options);

        final List<SimDisplayDetails> simDisplayDetails = toSimDisplayDetailsList(simDetails);

        final String page = options.get("page");

        final Integer pageNum = page == null ? 0 : Integer.parseInt(page);

        return new SimDisplayDetailsPagedResult(count, simDisplayDetails.size(), pageNum, simDisplayDetails);
    }

    @Override
    @RequestMapping(value = SimService.URL_DEVICE_DETAILS, method = RequestMethod.POST)
    public void updateOrInsertSimDetails(
            @RequestBody Map<String, String> options,
            @RequestParam(SimService.ICCID) String iccid,
            @RequestParam(SimService.MNO) String mno) throws InternalApiException
    {
        if (options == null)
        {
            throw new IllegalArgumentException("options parameter is null");
        }

        if (!StringUtils.hasText(iccid))
        {
            throw new IllegalArgumentException("iccid parameter is null or empty");
        }

        if (!StringUtils.hasText(mno))
        {
            throw new IllegalArgumentException("mno parameter is null or empty");
        }

        DeviceCellularInformation deviceCellularInfo = new DeviceCellularInformation();
        deviceCellularInfo.setIccid(iccid);
        if (options.containsKey(SimService.MSISDN))
        {
            deviceCellularInfo.setMsisdn(options.get(SimService.MSISDN));
        }

        simUpdateRepository.updateOrInsertSimDetails(deviceCellularInfo, mno);
    }

    @Override
    @RequestMapping(value = SimService.URL_SIM_LIFECYCLESTATE, method = RequestMethod.POST)
    public int updateSimLifecycleState(@RequestBody Map<String,String> options) throws InternalApiException
    {
        String iccid = options.get(SimService.ICCID);
        SimLifecycleState lifecycleState = SimLifecycleState.fromString(options.get(SimService.SIM_LIFECYCLE_STATE));
        if (!StringUtils.hasText(iccid))
        {
            throw new IllegalArgumentException("options parameter does not contain an ICCID");
        }

        if (lifecycleState == null)
        {
            throw new IllegalArgumentException("options parameter does not contain a valid SimLifecycleState string value");
        }

        return simUpdateRepository.updateLifecycleState(iccid, lifecycleState);
    }

    @Override
    @ResponseBody
    @RequestMapping(value = SimService.URL_SIM_DETAILS, method = RequestMethod.GET)
    public SimDisplayDetails getSimDetails(@RequestParam Map<String,String> options, @RequestParam(value = SimService.REQUEST_PARAM_ICCIID) String iccid) throws InternalApiException
    {

        options.putIfAbsent("iccid", iccid);

        final List<SimDetails> simDetailsList =  simSearchRepository.getSims(options);

        final List<SimDisplayDetails> simDisplayDetails = toSimDisplayDetailsList(simDetailsList);

        if (simDisplayDetails.size() > 1)
        {
            throw new InternalApiException("Multiple SIMs with same ICCID");
        }
        else if(simDisplayDetails.size() < 1)
        {
            throw new InternalApiException("No SIM with corresponding ICCID found");
        }

        SimDisplayDetails simDetails = simDisplayDetails.get(0);

        fetchAuditLog(options, simDetails);

        return simDetails;

    }

    @Override
    public List<String> getSimIccids(@RequestParam Map<String, String> options) throws InternalApiException
    {
        return simSearchRepository.getSimIccids(options);
    }

    @Override
    public List<String> getSimMnoList() throws InternalApiException
    {
        return simSearchRepository.getSimMnos();
    }


    private void fetchAuditLog(Map<String, String> options, SimDisplayDetails simDisplayDetails)
    {
        final long simId = simDisplayDetails.getSimId();

        final String user = options.get("user");
        final int daysAgo = getOptionOrDefault(options, "daysAgo", 2);
        final Integer page = getOptionOrDefault(options, "page", 0);

        final AuditLogPagedResult auditLogForMiu = getAuditLogForSim(simId, user, daysAgo, page);
        simDisplayDetails.setAuditLog(auditLogForMiu);

    }


    private int getOptionOrDefault(Map<String, String> options, String option, int defaultValue)
    {
        if (options.containsKey(option))
        {
            final String daysAgo = options.get(option);
            if (!daysAgo.isEmpty())
            {
                return Integer.parseInt(daysAgo);
            }
        }
        return defaultValue;
    }

    private AuditLogPagedResult getAuditLogForSim(long simId, String user, int daysAgo, Integer page)
    {
        final List<AuditLog> auditLog = auditService.getAuditLog(simId, user, daysAgo, page);

        final int auditLogCount = auditService.getAuditLogCount(simId, user, daysAgo);

        return new AuditLogPagedResult(Integer.valueOf(auditLogCount),
                Integer.valueOf(auditLog.size()),
                Integer.valueOf(page),
                auditLog);
    }

    private List<SimDisplayDetails> toSimDisplayDetailsList(List<SimDetails> simDetails)
    {
        return simDetails
                .stream()
                .map(this::getSimDisplayDetails)
                .collect(Collectors.toList());
    }

    private SimDisplayDetails getSimDisplayDetails(SimDetails sim)
    {

        String miuId = (sim.getMiuId() != null) ? sim.getMiuId().toString() : null;

        final SimDisplayDetails simDisplayDetails = new SimDisplayDetails();
        simDisplayDetails.setSimId(sim.getSimId());
        simDisplayDetails.setIccid(sim.getIccid());
        simDisplayDetails.setMiuId(miuId);
        simDisplayDetails.setNetwork(sim.getNetwork());
        simDisplayDetails.setState(sim.getLifeCycleState());
        simDisplayDetails.setCreationDate(sim.getCreationDate());

        simDisplayDetails.setAuditLog(new AuditLogPagedResult(0, 0, 0, Collections.EMPTY_LIST));

        return simDisplayDetails;
    }

}
