/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.common.data.miu.cellular.CmiuConfigRepository;
import com.neptunetg.common.data.miu.cellular.CmiuConfigSetAssociation;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.common.data.miu.command.MiuCommandDetail;
import com.neptunetg.common.lora.pdu.DeactivateCommandPacket;
import com.neptunetg.common.lora.pdu.ResetCommandPacket;
import com.neptunetg.common.lora.pdu.TimeOffsetCommandPacket;
import com.neptunetg.common.packet.model.builder.CommandPacketBuilder;
import com.neptunetg.common.packet.model.builder.ImageUpdateCommandBuilder;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.ImageMetaData;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CmiuDescriptionViewModel;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandRequest;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.CommandSummary;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import com.neptunetg.mdce.common.internal.miu.service.MiuCommandControlService;
import com.neptunetg.mdce.integration.domain.MiuCommandService;
import com.neptunetg.mdce.integration.utility.CmiuImageDistributionUtility;
import com.neptunetg.mdce.integration.utility.CommandDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Internal REST controller implementing CMIU command and control service
 */
@RestController
@RequestMapping("/service")
public class MiuCommandControlServiceController implements MiuCommandControlService
{
    private static final Logger log = LoggerFactory.getLogger(MiuCommandControlServiceController.class);

    @Value("#{envProperties['image.download.url']}")
    private String imageDownloadUrl;

    @Value("#{envProperties['cmiu.modem.firmware.image.ftp.url']}")
    private String fotaFtpUrl;

    @Value("#{envProperties['cmiu.modem.firmware.image.ftp.port']}")
    private int fotaFtpPort;

    @Value("#{envProperties['cmiu.modem.firmware.image.ftp.username']}")
    private String fotaFtpUsername;

    @Value("#{envProperties['cmiu.modem.firmware.image.ftp.password']}")
    private String fotaFtpPassword;

    @Autowired
    private MiuCommandService miuCommandService;

    @Autowired
    private CmiuRepository cmiuRepo;

    @Autowired
    private CmiuConfigRepository configRepo;

    @Autowired
    private MdceIpcPublishService awsQueueService;

    @Autowired
    private CommandDescription commandDescription;

    private static ImageMetaData.ImageType toPacketUtilImageType(ImageDescription.ImageType imageType)
    {
        ImageMetaData.ImageType result;

        switch (imageType)
        {
            case FirmwareImage:
                result = ImageMetaData.ImageType.FIRMWARE_IMAGE;
                break;
            case BootloaderImage:
                result = ImageMetaData.ImageType.BOOTLOADER_IMAGE;
                break;
            case ConfigImage:
                result = ImageMetaData.ImageType.CONFIG_IMAGE;
                break;
            case TelitModuleImage:
                result = ImageMetaData.ImageType.TELIT_MODULE_IMAGE;
                break;
            case BleConfigImage:
                result = ImageMetaData.ImageType.BLE_CONFIG_IMAGE;
                break;
            case ArbConfigImage:
                result = ImageMetaData.ImageType.ARB_CONFIG_IMAGE;
                break;
            case EncryptionImage:
                result = ImageMetaData.ImageType.ENCRYPTION_IMAGE;
                break;
            default:
                throw new IllegalArgumentException("Unknown image type");
        }

        return result;
    }

    /**
     * Send a command to the backend for processing and publishing to MQTT broker.
     *
     * @param commandRequest containing the details of the command to be send to the backend
     */
    @Override
    @RequestMapping(value=URL_CMIU_COMMAND, method=RequestMethod.POST)
    public void sendCommand(@RequestBody CommandRequest commandRequest)
    {
        //Check if L900 command, otherwise assume CMIU command
        if(commandRequest.getCommandL900() != null)
        {
            switch (commandRequest.getCommandL900())
            {
                case COMMAND_RESET:
                    buildAndSendResetCommandL900(commandRequest);
                    break;

                case COMMAND_TIME_OFFSET:
                    buildAndSendTimeOffsetCommandL900(commandRequest);
                    break;

                case COMMAND_DEACTIVATE:
                    buildAndSendDeactivateCommandL900(commandRequest);
                    break;
            }
        }
        else
        {
            switch (commandRequest.getCommandCmiu())
            {
                case COMMAND_UPDATE_IMAGE:
                    buildAndSendImageUpdateCommand(commandRequest);
                    break;

                case COMMAND_REBOOT_CMIU:
                    buildAndSendRebootCmiuCommand(commandRequest);
                    break;

                case COMMAND_MAG_SWIPE_EMULATION:
                    buildAndSendMagSwipeEmulationCommand(commandRequest);
                    break;

                case COMMAND_ERASE_DATALOG:
                    buildAndSendEraseDatalogCommand(commandRequest);
                    break;

                case COMMAND_SET_RECORDING_REPORTING_INTERVALS:
                    buildAndSendSetRecordingReportingIntervalsCommand(commandRequest);
                    break;

                case COMMAND_PUBLISH_REQUESTED_PACKET:
                    buildAndSendPublishRequestedPacketCommand(commandRequest);
                    break;

                case COMMAND_MODEM_FOTA:
                    buildAndSendUpdateModemFirmwareCommand(commandRequest);
                    break;
            }
        }
    }

    @Override
    @RequestMapping(value=URL_CMIU_COMMAND_REJECT, method=RequestMethod.POST)
    public void rejectCommand(@RequestParam(REQUEST_PARAM_COMMANDID) long commandId)
    {
        miuCommandService.rejectAcceptedMiuCommand(commandId);
    }

    @Override
    @RequestMapping(value=URL_CMIU_COMMAND_FLUSH, method=RequestMethod.POST)
    public int flushCommand(@RequestParam(REQUEST_PARAM_CMIUID) long cmiuId)
    {
        int commandsRecalled = miuCommandService.recallCreatedAndQueuedMiuCommand(MiuId.valueOf(cmiuId));

        if (commandsRecalled > 0)
        {
            //send SQS to inform mdce-mqtt application to check message for pending commands to be processed
            awsQueueService.sendMessage(MdceIpcPublishService.SEND_COMMAND_QUEUE,
                    String.format("%s: recall commands - %d", LocalDateTime.now(), cmiuId));
        }

        return commandsRecalled;
    }

    /**
     * AJAX request to retrieve CMIU description.
     * @param cmiuId the cmiu.
     * @return the cmiu description as json
     */
    @Override
    @RequestMapping(value=  URL_CMIU_METADATA, method=RequestMethod.GET, params = REQUEST_PARAM_CMIUID)
    public CmiuDescriptionViewModel getCmiuDescription(@RequestParam(REQUEST_PARAM_CMIUID) long cmiuId)
    {
        String description = "Cannot retrieve details for CMIU " + cmiuId;

        try
        {
            CmiuConfigSetAssociation config = configRepo.getCurrentCmiuConfig(MiuId.valueOf(cmiuId));
            description = config.toString();
        }
        catch (Exception ex)
        {
            log.warn("Exception occurred when getting CMIU description", ex);
            description = "No config entry for cmiu " + cmiuId + ". ";
        }

        try
        {
            CellularDeviceDetail cellularInfo = cmiuRepo.getCellularConfig(MiuId.valueOf(cmiuId));
            if (cellularInfo == null)
            {
                description += "; (no cellular config)";
            }
            else
            {
                description += "; " + cellularInfo.toString();

            }
        }
        catch (Exception ex)
        {
            final String desc = "No cellular entry for cmiu " + cmiuId + ". ";
            log.debug(desc);
            description += "; " + desc;
        }

        return new CmiuDescriptionViewModel(cmiuId, description);
    }

    @Override
    @RequestMapping(value=URL_COMMAND_IN_PROGRESS, method=RequestMethod.GET)
    public List<CommandSummary> getCommandsInProgress() throws InternalApiException
    {
        List<CommandSummary> result = miuCommandService.getInProgressCommand().stream()
                .map(this::miuCommandDetailMapper)
                .collect(Collectors.toList());

        return result;
    }

    @Override
    @RequestMapping(value=URL_COMMAND_RECEIVED, method=RequestMethod.GET)
    public List<CommandSummary> getCommandsReceived() throws InternalApiException
    {
        List<CommandSummary> result = miuCommandService.getReceivedCommand().stream()
                .map(this::miuCommandDetailMapper)
                .collect(Collectors.toList());
        return result;
    }

    @Override
    @RequestMapping(value=URL_COMMAND_COMPLETED, method=RequestMethod.GET)
    public List<CommandSummary> getCommandsCompleted(@RequestParam(REQUEST_PARAM_DAYS_AGO) int daysAgo) throws InternalApiException
    {
        List<CommandSummary> result = miuCommandService.getCompletedCommand(daysAgo).stream()
                .map(this::miuCommandDetailMapper)
                .collect(Collectors.toList());
        return result;
    }

    @Override
    @RequestMapping(value=URL_COMMAND_REJECTED, method=RequestMethod.GET)
    public List<CommandSummary> getCommandsRejected(@RequestParam(REQUEST_PARAM_DAYS_AGO) int daysAgo) throws InternalApiException
    {
        List<CommandSummary> result = miuCommandService.getRejectedCommand(daysAgo).stream()
                .map(this::miuCommandDetailMapper)
                .collect(Collectors.toList());
        return result;
    }

    private void buildAndSendTimeOffsetCommandL900(CommandRequest commandRequest)
    {
        TimeOffsetCommandPacket packet = new TimeOffsetCommandPacket(Duration.ofMillis(commandRequest.getTimeOffset()),
                1);

        this.sendCommandL900(commandRequest, packet.getBytes());
    }

    private void buildAndSendResetCommandL900(CommandRequest commandRequest)
    {
        ResetCommandPacket packet = new ResetCommandPacket();

        this.sendCommandL900(commandRequest, packet.getBytes());
    }

    private void buildAndSendDeactivateCommandL900(CommandRequest commandRequest)
    {
        DeactivateCommandPacket packet = new DeactivateCommandPacket(1); //Todo - set sync byte correctly

        this.sendCommandL900(commandRequest, packet.getBytes());
    }

    private void buildAndSendSetRecordingReportingIntervalsCommand(CommandRequest commandRequest)
    {
        final TaggedPacket packet = new CommandPacketBuilder()
                .reportingInterval(
                        commandRequest.getReportingStartMins(),
                        commandRequest.getReportingIntervalHours(),
                        commandRequest.getReportingRetries(),
                        commandRequest.getReportingTransmitWindows(),
                        commandRequest.getReportingQuietStart(),
                        commandRequest.getReportingQuietEnd())
                .recordingInterval(commandRequest.getRecordingStartTime(), commandRequest.getRecordingInterval())
                .buildSetRecordingReportingIntervalsCommandPacket(0);

        this.sendCommand(commandRequest, packet, commandRequest.getCommandCmiu());
    }

    private void buildAndSendPublishRequestedPacketCommand(CommandRequest commandRequest)
    {
        final TaggedPacket packet = new CommandPacketBuilder()
                .packetType(TaggedDataPacketType.getPacketType((byte)commandRequest.getPacketType().getPacketNumber()))
                .buildPublishRequestedPacketCommandPacket(0);

        this.sendCommand(commandRequest, packet, commandRequest.getCommandCmiu());
    }

    private void buildAndSendUpdateModemFirmwareCommand(CommandRequest commandRequest)
    {
        final TaggedPacket packet = new CommandPacketBuilder()
                .fotaDetails(fotaFtpUrl, fotaFtpPort, fotaFtpUsername, fotaFtpPassword,
                        commandRequest.getFotaFilename(), commandRequest.getFotaFileImageDescription())
                .buildUpdateModemFirmwareCommandPacket(0);

        this.sendCommand(commandRequest, packet, commandRequest.getCommandCmiu());
    }

    private void buildAndSendEraseDatalogCommand(CommandRequest commandRequest)
    {
        final TaggedPacket packet = new CommandPacketBuilder()
                .buildEraseDatalogCommandPacket(0);

        this.sendCommand(commandRequest, packet, commandRequest.getCommandCmiu());
    }

    private void buildAndSendMagSwipeEmulationCommand(CommandRequest commandRequest)
    {
        final TaggedPacket packet = new CommandPacketBuilder()
                .buildMagSwipeEmulationCommandPacket(0);

        this.sendCommand(commandRequest, packet, commandRequest.getCommandCmiu());
    }

    private void buildAndSendImageUpdateCommand(CommandRequest commandRequest)
    {
        ImageUpdateCommandBuilder[] imageBuilders = commandRequest.getImageDescription()
                .stream()
                .map(id -> new ImageUpdateCommandBuilder(toPacketUtilImageType(id.getImageType()))
                        .withFilename(CmiuImageDistributionUtility.filenameToDownloadUrl(id.getFileName(), imageDownloadUrl))
                        .withMetaData(id.getStartAddress(), id.getSizeBytes())
                        .withRevision(id.getRevisionMajor(), id.getRevisionMinor(), id.getRevisionDate(), id.getRevisionBuildNum()))
                .toArray(ImageUpdateCommandBuilder[]::new);

        TaggedPacket packet = new CommandPacketBuilder()
                .executionTime(commandRequest.getExecutionTimeEpochSecond())
                .buildImageUpdateCommandPacket(0, imageBuilders);

        this.sendCommand(commandRequest, packet, commandRequest.getCommandCmiu());
    }

    private void buildAndSendRebootCmiuCommand(CommandRequest commandRequest)
    {
        final TaggedPacket packet = new CommandPacketBuilder()
                .buildRebootCmiuCommandPacket(0);

        this.sendCommand(commandRequest, packet, commandRequest.getCommandCmiu());
    }

    private void sendCommandL900(CommandRequest commandRequest, byte[] packet)
    {
        final List<Long> l900List = commandRequest.getMiuList();

        l900List.forEach(l900Id -> {
            try
            {
                log.debug("Sending commands to l900 {}", l900Id);

                final MiuCommand command = new MiuCommand();
                command.setMiuId(MiuId.valueOf(l900Id));
                command.setCommandEnteredTime(Instant.now());
                command.setCommandParams(packet);
                command.setCommandType(commandRequest.getCommandL900().getCommandNumber());
                command.setTargetDeviceType(MiuType.L900.toString());
                command.setUserName(commandRequest.getUser());

                this.miuCommandService.createMiuCommand(command);
            } catch (Exception ex)
            {
                log.error("Error sending commands to l900 {}, Error: {}", l900Id, ex);
            }
        });


        //send SQS to inform mdce-mqtt application to check message for pending commands to be processed
        awsQueueService.sendMessage(MdceIpcPublishService.LORA_PACKETS_TO_SEND,
                String.format("%s: %d - %s",
                        LocalDateTime.now(), commandRequest.getCommandL900().getCommandNumber(),
                        commandRequest.getCommandL900().getCommandDescription())
        );
    }

    private void sendCommand(CommandRequest commandRequest, final TaggedPacket packet, final CommandRequest.CommandTypeCmiu commandType)
    {
        final List<Long> cmiuList = commandRequest.getMiuList();
        final byte[] packetData = packet.toByteSequence();

        cmiuList.forEach(cmiuId -> {

            try
            {
                log.debug("Sending commands to cmiu {}", cmiuId);

                final MiuCommand command = new MiuCommand();
                command.setMiuId(MiuId.valueOf(cmiuId));
                command.setCommandEnteredTime(Instant.now());
                command.setCommandParams(packetData.clone());
                command.setCommandType(commandType.getCommandNumber());
                command.setTargetDeviceType(MiuType.CMIU.toString());
                command.setUserName(commandRequest.getUser());

                this.miuCommandService.createMiuCommand(command);
            }
            catch (Exception ex)
            {
                log.error("Error sending commands to cmiu {}, Error: {}", cmiuId, ex);
            }
        });


        //send SQS to inform mdce-mqtt application to check message for pending commands to be processed
        awsQueueService.sendMessage(MdceIpcPublishService.SEND_COMMAND_QUEUE,
                String.format("%s: %d - %s",
                        LocalDateTime.now(),commandType.getCommandNumber(), commandType.getCommandDescription())
        );
    }


    private CommandSummary miuCommandDetailMapper(MiuCommandDetail miuCommandDetail)
    {

        CommandSummary commandSummary = new CommandSummary();

        commandSummary.setCommandId(miuCommandDetail.getCommandId());
        commandSummary.setMiuId(miuCommandDetail.getMiuId().numericValue());
        commandSummary.setUser(miuCommandDetail.getUserName());
        commandSummary.setCommandEnteredTime(miuCommandDetail.getCommandEnteredTime());
        commandSummary.setCommandFulfilledTime(miuCommandDetail.getCommandFulfilledTime());
        commandSummary.setDetails(commandDescription.getCommandDetails(miuCommandDetail));
        commandSummary.setNetworkProvider(miuCommandDetail.getNetworkProvider());

        Integer targetDeviceType = null;

        if( miuCommandDetail.getTargetDeviceType() != null )
        {
            targetDeviceType = miuCommandDetail.getTargetDeviceType().numericWrapperValue();
        }

        commandSummary.setCommandType(
                CommandRequest.CommandType.fromCommandNumber(targetDeviceType, miuCommandDetail.getCommandType())
                        .getCommandDescription()
        );

        return commandSummary;

    }

}




