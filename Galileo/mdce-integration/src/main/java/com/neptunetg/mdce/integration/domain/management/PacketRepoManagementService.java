/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain.management;

import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.mdce.common.data.DynamoPacketRepositoryMaintenanceService;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.integration.data.alert.AlertRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * DynamoDb packet repo management service listener to AWS SQS message for notification to perform database management service.
 * Current service is changing IOPS of hot/cold table.
 */
@Service
public class PacketRepoManagementService implements AwsQueueReceiver
{
    private static final Logger logger = LoggerFactory.getLogger(PacketRepoManagementService.class);


    private final DynamoPacketRepositoryMaintenanceService dynamoMaintainer;
    private final AlertRepository alertRepository;

    @Autowired
    public PacketRepoManagementService(DynamoPacketRepositoryMaintenanceService dynamoMaintainer,
                                       MdceIpcSubscribeService ipcSub,
                                       AlertRepository alertRepository)
    {
        this.dynamoMaintainer = dynamoMaintainer;
        this.alertRepository = alertRepository;
        ipcSub.register(this, MdceIpcSubscribeService.MANAGE_NOSQL_QUEUE, Duration.ofMinutes(10L), 1);
    }

    /**
     * Handler called by MdceIpcSubscribeService to process a SQS message
     * @param message the message body
     * @param messageId the message id
     * @param messageReceipt message receipt
     * @param cancellationToken Canceller.  Ignored as hourly maintenance cannot be interrupted.
     * @return if return true, the message will be deleted, else message will not be deleted from SQS.
     */
    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {

        try
        {
            dynamoMaintainer.performHourlyMaintenance();
            return true;
        }
        catch (Exception e)
        {
            logger.error("Exception occurred when maintaining Dynamo.", e);

            final AlertSource alertSource = AlertSource.AWS_DYNAMODB_MAINTENANCE_ERROR;
            AlertDetails dynamoMaintenanceAlert = new AlertDetails();
            dynamoMaintenanceAlert.setAlertCreationDate(ZonedDateTime.from(Instant.now()));
            dynamoMaintenanceAlert.setAlertState(AlertState.NEW);
            dynamoMaintenanceAlert.setAlertLevel(AlertLevel.ERROR);
            dynamoMaintenanceAlert.setAlertSource(alertSource.getAlertId());
            dynamoMaintenanceAlert.setRecentMessage("Error while maintaining DynamoDB: " + e);

            alertRepository.addAlert(dynamoMaintenanceAlert);

            return false;
        }
    }
}
