/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.sim;

import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.mdce.common.internal.sim.model.SimLifecycleState;

public interface SimUpdateRepository
{

    /**
     * Fills in missing (null) SIM detail fields from data provided. If SIM iccid not found new record is created.
     * @param sim DeviceCellularInformation object containing details of SIM
     * @param mno Mobile network operator name
     */
    void updateOrInsertSimDetails(DeviceCellularInformation sim, String mno);

    /**
     * Updates the lifecycle state of a SIM.
     * @param iccid The ICCID by which the SIM is identified.
     * @param lifecycleState The new lifecycle state of the SIM.
     */
    int updateLifecycleState(String iccid, SimLifecycleState lifecycleState);
}
