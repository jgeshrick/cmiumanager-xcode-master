/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;


import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.integration.data.gateway.GatewayDetails;
import com.neptunetg.mdce.integration.data.gateway.GatewayId;

import java.util.List;

/**
* Service interface for CRUD operation on the MySql database.
 * Used to claim MIUs
 */
public interface GatewayService
{
    List<GatewayDetails> listGatewayDetails();

    GatewayDetails getGatewayDetails(GatewayId gatewayId);

    List<SiteDetails> getUtilityList();
}
