/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.rest.json.packets;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * List of packet from a same gateway id for constructing part of getPacket JSON response.
 */
public class JsonPacketListForGateway
{
    @JsonProperty("gw_id")
    final private String gwId;
    final private List<JsonPacketData> packets;

    public String getGwId()
    {
        return gwId;
    }

    public List<JsonPacketData> getPackets()
    {
        return packets;
    }

    public JsonPacketListForGateway(String gwid, List<JsonPacketData> packets)
    {
        this.gwId = gwid;
        this.packets = packets;
    }
}
