/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.analysis;

import com.neptunetg.mdce.common.internal.anaylsis.model.CmiuTimingAnalysisDetails;

import java.util.List;

/**
 * Cmiu Analysis Repository.
 */
public interface CmiuAnalysisRepository
{
    List<CmiuTimingAnalysisDetails> getTimingInfo(Long connectionThreshold, String cmiuMode, String mno, String state, String siteId);
}
