/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.mdce.common.api.rest.exception.NotFoundException;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.email.model.EmailMessage;
import com.neptunetg.mdce.common.internal.email.service.EmailService;
import com.neptunetg.mdce.integration.utility.EmailBuffer;
import com.neptunetg.mdce.integration.utility.SendEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Properties;

/**
 * REST controller for the SendEmailService interface.
 */
@RestController
@RequestMapping("/service")
public class EmailServiceController implements EmailService
{
    private final SendEmailService sendEmailService;
    private final Properties envProperties;
    private final EmailBuffer emailBuffer;

    @Autowired
    public EmailServiceController(EmailBuffer emailBuffer, SendEmailService sendEmailService,
                                  @Qualifier("envProperties") Properties envProperties)
    {
        this.emailBuffer = emailBuffer;
        this.sendEmailService = sendEmailService;
        this.envProperties = envProperties;
    }


    @Override
    @RequestMapping(value = URL_EMAIL_SERVICE_SEND_EMAIL, method = RequestMethod.POST)
    public void sendEmail(@RequestBody EmailMessage emailMessage) throws InternalApiException
    {
        sendEmailService.sendEmail(emailMessage.getRecipientAddresses(), emailMessage.getSubject(), emailMessage.getContents());
    }

    @Override
    @RequestMapping(value = URL_EMAIL_SERVICE_GET_DEBUG_MODE_EMAIL, method = RequestMethod.GET)
    public EmailMessage getDebugModeEmail() throws InternalApiException
    {
        final boolean redirectToLocalBuffer = Boolean.parseBoolean(envProperties.getProperty("mail.redirectToLocalBuffer"));

        if (!redirectToLocalBuffer)
        {
            throw new NotFoundException();
        }

        // Get the last message and then clear it from the buffer.
        EmailMessage lastMessage = emailBuffer.getEmailMessage();
        emailBuffer.setEmailMessage(new EmailMessage(new String[]{}, "", ""));

        return lastMessage;
    }
}
