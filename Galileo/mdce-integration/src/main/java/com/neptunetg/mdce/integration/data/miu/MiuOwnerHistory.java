/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.miu;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.mdce.integration.data.site.SiteId;

import java.time.Instant;

/**
 * A record of the site_id being set for an MIU.
 */
public class MiuOwnerHistory
{
    private final int miuOwnerHistoryId;
    private final MiuId miuId;
    private final SiteId siteId;
    private final Instant timestamp;
    private final Instant created;

    public MiuOwnerHistory(int miuOwnerHistoryId, MiuId miuId, SiteId siteId, Instant timestamp, Instant created)
    {
        this.miuOwnerHistoryId = miuOwnerHistoryId;
        this.miuId = miuId;
        this.siteId = siteId;
        this.timestamp = timestamp;
        this.created = created;
    }

    public int getMiuOwnerHistoryId()
    {
        return miuOwnerHistoryId;
    }

    public MiuId getMiuId()
    {
        return miuId;
    }

    public SiteId getSiteId()
    {
        return siteId;
    }

    public Instant getTimestamp()
    {
        return timestamp;
    }

    public Instant getCreated()
    {
        return created;
    }
}
