/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;


import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.cns.CnsException;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.integration.cns.CellularNetworkServiceManager;
import com.neptunetg.mdce.integration.data.sim.SimUpdateRepository;
import com.neptunetg.mdce.integration.domain.alert.AlertEmail;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class SimMonitoringServiceImpl implements AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(SimMonitoringServiceImpl.class);

    @Autowired
    private CellularNetworkServiceManager cellularNetworkServiceManager;

    @Autowired
    private SimUpdateRepository simUpdateRepo;

    @Autowired
    private AlertService alertService;

    @Autowired
    public SimMonitoringServiceImpl(MdceIpcSubscribeService awsQueueService)
    {
        awsQueueService.register(this, MdceIpcSubscribeService.CHECK_SIM_LIST, Duration.ofMinutes(1L), 1);
    }

    private boolean updateSimList(AtomicBoolean cancellationToken) throws InternalApiException
    {
        boolean result = true;

        try
        {
            // Update Verizon devices
            result = updateSimListForNetwork(cellularNetworkServiceManager.VZW, "VZW", cancellationToken);
        }
        catch (Exception e)
        {
            result = false;
            log.error("Exception while updating SIM list for VZW: ", e);
        }

        try
        {
            // Update ATT
            result &= updateSimListForNetwork(cellularNetworkServiceManager.ATT, "ATT", cancellationToken);
        }
        catch (Exception e)
        {
            result = false;
            log.error("Exception while updating SIM list for ATT: ", e);
        }

        return result;
    }


    private boolean updateSimListForNetwork(String networkId, String mno, AtomicBoolean cancellationToken) throws InternalApiException
    {
        if (cancellationToken.get())
        {
            return false;
        }

        final CellularNetworkService networkService = cellularNetworkServiceManager.getCns(networkId);
        final List<DeviceCellularInformation> devices;

        try
        {
            devices = networkService.getDeviceList();
        }
        catch (CnsException e)
        {
            log.error("Exception connecting to " + networkId, e);
            return false;
        }

        for (DeviceCellularInformation device : devices)
        {
            try
            {
                simUpdateRepo.updateOrInsertSimDetails(device, mno);
            }
            catch (Exception e)
            {
                log.error("Error updating SIM information for device " + device + " on network " + mno, e);

                final AlertSource alertSource = AlertSource.SIM_DETAILS_UPDATE_UKNOWN_ERROR;
                AlertDetails alert = new AlertDetails();
                alert.setAlertCreationDate(ZonedDateTime.now());
                alert.setAlertSource(alertSource.getAlertId());
                alert.setAlertLevel(AlertLevel.WARNING);
                alert.setAlertState(AlertState.NEW);
                alert.setRecentMessage("Cellular information from MNO clashed with data already in the MDCE mySQL " +
                        "database: " + e);

                AlertEmail alertEmail = alertService.initAlertEmail();

                alertService.addAlert(alert, alertEmail);
            }
            if (cancellationToken.get())
            {
                return false; //stop processing if cancelled
            }
        }

        return true;

    }

    /**
     * Process incoming request for retrieving SIM data.  This is a long-running callback.
     */
    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        try
        {
            return updateSimList(cancellationToken);  // remove message from SQS queue if returns true
        }
        catch (InternalApiException e)
        {
            log.error("Exception thrown while updating sim list:", e);
        }

        return false;
    }


}
