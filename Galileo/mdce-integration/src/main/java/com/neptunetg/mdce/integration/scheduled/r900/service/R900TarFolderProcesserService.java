/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.scheduled.r900.service;

import com.neptunetg.mdce.integration.scheduled.ScheduledProcessor;
import com.neptunetg.mdce.integration.scheduled.r900.AsyncFileTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Future;

/**
 * Scans folder containing received R900 gateway data file, as detailed in ETI 48-02
 * This level checks upload directory for any unlocked files, locks them and passes them to the handler
 * On return the file is moved to the relevant folder: error or processed - as a fallback it appends .failed
 */
@Service
public class R900TarFolderProcesserService extends BaseR900TarService implements ScheduledProcessor
{
    private static final Logger logger = LoggerFactory.getLogger(R900TarFolderProcesserService.class);

    private final AsyncFileTask taskHandler;

    private Instant instantOfPreviousOldTarballCheck = Instant.ofEpochSecond(0);
    private int maximumProcessedTarAgeDays = 14;


    @Autowired
    public R900TarFolderProcesserService(AsyncFileTask taskHandler,
                                         @Value("#{envProperties['gateway.data.dump.folder']?:'/'}") String tarballDumpFolder)
    {
        super(tarballDumpFolder);
        this.taskHandler = taskHandler;
    }

    @Override
    public void process()
    {
        String[] listOfTars = getListOfTarsPresent();

        for (String filename : listOfTars)
        {
            ifFileNotLockedThenProcess(filename);
        }

        if(instantOfPreviousOldTarballCheck.plus(1, ChronoUnit.DAYS).isBefore(Instant.now()) == true)
        {
            for (String filename : getListOfProcessedTars())
            {
                removeOldProcessedTars(filename);
            }

            instantOfPreviousOldTarballCheck = Instant.now();
        }
    }

    private void ifFileNotLockedThenProcess(String filename)
    {
        final File file = new File(unProcessedFolder, filename);
        final File lockFile = new File(unProcessedFolder, filename + LOCK);

        if (!checkValidLockFileExists(lockFile) && file.exists())
        {
            logger.debug("Found unlocked file " + filename + " in " + unProcessedFolder);
            final Future<File> fileFuture = taskHandler.process(file);
        }
    }

    private void removeOldProcessedTars(String filename)
    {
        File tarFile = new File(processedFolder, filename);
        Instant lastModifiedInstant = Instant.ofEpochMilli(tarFile.lastModified());

        if(lastModifiedInstant.plus(maximumProcessedTarAgeDays, ChronoUnit.DAYS).isBefore(Instant.now()))
        {
            tarFile.delete();
        }
    }

    private boolean checkValidLockFileExists(File lockFile)
    {
        if (!lockFile.exists())
        {
            return false;
        }

        return checkLockFileIsNotOverAnHourOld(lockFile);
    }

    private boolean checkLockFileIsNotOverAnHourOld(File lockFile)
    {
        try
        {
            final Path lockFilePath = lockFile.toPath();

            BasicFileAttributes attr = Files.readAttributes(lockFilePath, BasicFileAttributes.class);
            final Instant fileCreationTime = attr.creationTime().toInstant();

            if (Instant.now().minus(1, ChronoUnit.HOURS).isAfter(fileCreationTime))
            {
                lockFile.delete();
            }
        }
        catch (Exception e)
        {
            logger.warn("Exception thrown trying to validate age of " + lockFile.getName(), e);
        }

        return lockFile.exists();
    }

    private String[] getListOfTarsPresent()
    {
        final FilenameFilter filter = (directory, fileName) -> fileName.endsWith(".tar");
        return unProcessedFolder.list(filter);
    }

    private String[] getListOfProcessedTars()
    {
        final FilenameFilter filter = (directory, fileName) -> fileName.endsWith(".tar");
        return processedFolder.list(filter);
    }

}
