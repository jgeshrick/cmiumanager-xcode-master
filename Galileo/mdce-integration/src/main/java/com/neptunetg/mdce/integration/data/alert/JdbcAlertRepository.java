/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.data.alert;


import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.neptunetg.common.data.util.JdbcDateUtil.getInstant;

/**
 * Class to allow access to the alerts section of the SQL database
 */

@Repository
public class JdbcAlertRepository implements AlertRepository
{
    /**
     * The maximum number of alert logs to retain per alert.
     */
    public static final int ALERT_LOG_WINDOW_SIZE = 10;

    private JdbcTemplate db;

    @Autowired
    public JdbcAlertRepository(JdbcTemplate db)
    {
        this.db = db;
    }

    @Override
    public List<AlertDetails> getNewAlertList(String alertSource)
    {
        String sql = "SELECT * FROM mdce.alert_alerts WHERE alert_state='new' AND alert_source LIKE ? " +
                "ORDER BY alert_level DESC, alert_creation_date DESC";

        return this.db.query(sql, this::AlertDetailsMapper, alertSource);
    }

    @Override
    public List<AlertDetails> getHandledAlertList(String alertSource)
    {
        String sql = "SELECT * FROM mdce.alert_alerts " +
                "WHERE alert_state='handled' AND alert_source LIKE ? " +
                "ORDER BY alert_level DESC, alert_creation_date DESC";

        return this.db.query(sql, this::AlertDetailsMapper, alertSource);
    }

    @Override
    public List<AlertDetails> getStaleAlertList(String alertSource)
    {
        String sql = "SELECT * FROM mdce.alert_alerts " +
                "WHERE alert_state='stale' AND alert_source LIKE ? " +
                "ORDER BY alert_level DESC, alert_creation_date DESC";

        return this.db.query(sql, this::AlertDetailsMapper, alertSource);
    }

    @Override
    public List<AlertDetails> getClearedAlertList(String alertSource)
    {
        String sql = "SELECT * FROM mdce.alert_alerts " +
                "WHERE alert_state='cleared' AND alert_source LIKE ? " +
                "ORDER BY alert_level DESC, alert_creation_date DESC";

        return this.db.query(sql, this::AlertDetailsMapper, alertSource);
    }

    /**
     * Alert alert and log
     * @param alertDetails
     * @return
     */
    @Override
    @Transactional
    public boolean addAlert(AlertDetails alertDetails)
    {
        String alertSql = "INSERT INTO mdce.alert_alerts " +
                     "(alert_source, alert_creation_date, " +
                     "alert_state, alert_level, alert_last_update_date, alert_latest_message " +
                     ") VALUES (?,?,?,?,?,?)";
        final Date date = new Date();

        this.db.update(alertSql,
                alertDetails.getAlertSource(),
                Date.from(alertDetails.getAlertCreationDate().toInstant()),
                alertDetails.getAlertState().getState(),
                alertDetails.getAlertLevel().getLevel(),
                date,
                alertDetails.getRecentMessage());

        alertDetails.setAlertId(lastInsertId());

        String alertLogSql = "INSERT INTO mdce.alert_log (alert_log_date, alert_log_message, alert_alerts_id) " +
                "VALUES (?,?,LAST_INSERT_ID())";

        this.db.update(alertLogSql,                date,
                alertDetails.getRecentMessage());

        this.trimAlertLogs(alertDetails.getAlertId());

        return true;
    }

    /**
     * get Alert by alertId
     * @param alertId the ID of the alert
     * @return AlertDetails object, or null if none found with specified alert ID
     */
    @Override
    public AlertDetails getAlert(long alertId)
    {
        String sql = "SELECT * FROM mdce.alert_alerts WHERE alert_alerts_id = ? ";

        try
        {
            return this.db.queryForObject(sql, this::AlertDetailsMapper, alertId);
        }
        catch (EmptyResultDataAccessException e)
        {
            return null;
        }
    }

    /**
     * Get alert matching exactly the alert source
     * Cleared alerts are not included as there could be multiple
     * @param alertSource alert source path
     * @return alert detail, null if no matching alert source
     */
    @Override
    public AlertDetails getAlertNotCleared(String alertSource)
    {
        String sql = "SELECT * FROM mdce.alert_alerts WHERE alert_source = ? " +
                    "AND alert_state != 'cleared'";

        try
        {
            return this.db.queryForObject(sql, this::AlertDetailsMapper, alertSource);
        }
        catch (EmptyResultDataAccessException e)
        {
            return null;
        }
    }

    @Override
    public boolean setAlertStateToNew(long alertId)
    {
        return setAlertStateHelper(alertId, "new");
    }

    @Override
    public boolean setAlertStateToHandled(long alertId)
    {
        return setAlertStateHelper(alertId, "handled");
    }

    @Override
    public boolean setAlertStateToStale(long alertId)
    {
        return setAlertStateHelper(alertId, "stale");
    }

    @Override
    public boolean setAlertStateToCleared(long alertId)
    {
        return setAlertStateHelper(alertId, "cleared");
    }

    private boolean setAlertStateHelper(long alertId, String state)
    {
        String sql = "UPDATE mdce.alert_alerts SET alert_state = ?" +
                "WHERE alert_alerts_id = ?";

        this.db.update(sql,
                state,
                alertId);

        return true;
    }

    @Override
    public boolean setAlertLevelToWarning(long alertId)
    {
        String sql = "UPDATE mdce.alert_alerts SET alert_level = ?" +
                "WHERE alert_alerts_id = ?";

        this.db.update(sql,
                "warning",
                alertId);

        return true;
    }

    @Override
    public boolean setAlertLevelToError(long alertId)
    {
        String sql = "UPDATE mdce.alert_alerts SET alert_level = ?" +
                "WHERE alert_alerts_id = ?";

        this.db.update(sql,
                "error",
                alertId);

        return true;
    }

    @Override
    public boolean setAlertTicketId(AlertDetails alertDetails)
    {
        String sql = "UPDATE mdce.alert_alerts SET alert_ticket_id = ?" +
                "WHERE alert_alerts_id = ?";

        this.db.update(sql,
                alertDetails.getAlertTicketId(),
                alertDetails.getAlertId());

        return true;
    }

    /**
     * Update alert status and alert logs
     * @param alertDetails
     * @return
     */
    @Override
    @Transactional
    public boolean addAlertLogMessage(AlertDetails alertDetails)
    {
        //insert into log
        String updateAlertLogSql = "INSERT INTO mdce.alert_log (alert_log_date, alert_log_message, alert_alerts_id) VALUES (?,?,?)";

        Date date = new Date();
        this.db.update(updateAlertLogSql,
                date,
                alertDetails.getRecentMessage(),
                alertDetails.getAlertId());

        //update alert latest message and last update date
        String updateAlertSql = "UPDATE mdce.alert_alerts SET " +
                " alert_latest_message = ?, alert_last_update_date = ? " +
                "WHERE alert_alerts_id = ?";

        this.db.update(updateAlertSql,
                alertDetails.getRecentMessage(),
                date,
                alertDetails.getAlertId());

        this.trimAlertLogs(alertDetails.getAlertId());

        return true;
    }

    @Override
    public boolean removeAlertsLogs(long alertId)
    {
        String sql = "DELETE FROM mdce.alert_log WHERE mdce.alert_log.alert_alerts_id = ?";

        this.db.update(sql, alertId);

        return true;
    }

    @Override
    public boolean removeAlert(long alertId)
    {
        String sql = "DELETE FROM mdce.alert_alerts WHERE mdce.alert_alerts.alert_alerts_id = ?";

        this.db.update(sql, alertId);

        return true;
    }

    @Override
    public List<AlertLog> getAlertLog(long alertId)
    {
        String sql = "SELECT * FROM mdce.alert_log WHERE " +
                "alert_alerts_id = ? ORDER BY alert_log_date DESC";

        return this.db.query(sql, this::AlertLogMapper, Long.toString(alertId));
    }

    /**
     * Get the last insert id.
     *
     * @return the last insert id.
     */
    @Override
    public Long lastInsertId()
    {
        String sql = "SELECT last_insert_id()";

        Long lastInsertId = this.db.queryForObject(sql, Long.class);

        if (lastInsertId != null)
        {
            return lastInsertId;
        }

        return null;
    }

    /**
     * Gets any alerts whose logs need to be trimmed.
     * @return A list of matching alerts.
     */
    @Override
    public List<AlertDetails> getAlertsExceedingLogWindowSize()
    {
        String sql =
                "SELECT alert_alerts_id " +
                        "FROM alert_log " +
                        "GROUP BY alert_alerts_id " +
                        "HAVING COUNT(*) > ?";

        return this.db.query(sql, this::AlertDetailsMapper, ALERT_LOG_WINDOW_SIZE);
    }

    /**
     * Trims the alert logs in a given alert record. When the number of alert logs for the supplied alertId exceeds
     * ALERT_LOG_WINDOW_SIZE, the oldest alert logs are deleted.
     * @param alertId The alert record ID.
     * @return The number of rows deleted.
     */
    @Override
    @Transactional
    public int trimAlertLogs(long alertId)
    {
        int deletedCount = 0;
        final String querySql =
                "SELECT alert_log_id FROM alert_log WHERE alert_alerts_id = ? ORDER BY alert_log_date DESC ";

        final List<Long> alertLogIdList = this.db.queryForList(querySql, Long.TYPE, alertId);

        if (alertLogIdList.size() > ALERT_LOG_WINDOW_SIZE)
        {
            final String deleteSql = "DELETE FROM alert_log WHERE alert_log_id = ? ";

            for (int i = ALERT_LOG_WINDOW_SIZE; i < alertLogIdList.size(); i++)
            {
                deletedCount += this.db.update(deleteSql, alertLogIdList.get(i));
            }
        }

        return deletedCount;
    }

    private AlertLog AlertLogMapper(ResultSet rs, int rownumber) throws SQLException
    {
        AlertLog alertLog = new AlertLog();
        alertLog.setLogDate(getInstant(rs, "alert_log_date"));
        alertLog.setLogMessage(rs.getString("alert_log_message"));

        return alertLog;
    }

    private AlertDetails AlertDetailsMapper(ResultSet rs, int rownumber) throws SQLException
    {
        AlertDetails alertDetails = new AlertDetails();
        alertDetails.setAlertId(rs.getLong("alert_alerts_id"));
        alertDetails.setAlertSource(rs.getString("alert_source"));
        alertDetails.setAlertCreationDate(getUtc(rs, "alert_creation_date"));
        alertDetails.setLatestAlertDate(getUtc(rs, "alert_last_update_date"));
        alertDetails.setAlertState(rs.getString("alert_state"));
        alertDetails.setAlertLevel(rs.getString("alert_level"));
        alertDetails.setRecentMessage(rs.getString("alert_latest_message"));
        alertDetails.setAlertTicketId(rs.getString("alert_ticket_id"));

        return alertDetails;
    }

    private ZonedDateTime getUtc(ResultSet rs, String alert_creation_date) throws SQLException
    {
        return ZonedDateTime.ofInstant(getInstant(rs, alert_creation_date), ZoneId.of("UTC"));
    }
}
