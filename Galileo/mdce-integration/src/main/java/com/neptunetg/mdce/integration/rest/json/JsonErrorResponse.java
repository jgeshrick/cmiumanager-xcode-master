/*
 *  Neptune Technology Group
 *  Copyright 2017 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Used to convey failure messages in batch operations.
 */
public class JsonErrorResponse
{
    @JsonProperty("ref_id")
    private int referenceId;

    @JsonProperty("error")
    private String error;

    public int getReferenceId()
    {
        return referenceId;
    }

    public void setReferenceId(int referenceId)
    {
        this.referenceId = referenceId;
    }

    public String getError()
    {
        return error;
    }

    public void setError(String error)
    {
        this.error = error;
    }
}
