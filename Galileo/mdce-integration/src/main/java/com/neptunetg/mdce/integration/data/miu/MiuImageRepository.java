/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.miu;


import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;

import java.util.List;

public interface MiuImageRepository
{

    /**
     * Get a list of registered image in RDS
     * @return description of the image
     */
    List<ImageDescription> getRegisteredImageList(ImageDescription.ImageType imageType);

    /**
     * Add a new image metadata to the RDS
     * @param name entry name
     * @param imageUrl image url pointing to the S3 location
     * @param imageSize the size of the image in bytes
     * @param startAddress start address for the image in bytes
     * @return number of rows updated
     */
    int addRegisteredImage(String name, String imageUrl, int imageSize, int startAddress);

}