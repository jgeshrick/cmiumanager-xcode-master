/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.ConfigSet;
import com.neptunetg.common.data.miu.cellular.MiuConfigHistoryQueryOptions;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigChange;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigHistory;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigMgmt;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;
import com.neptunetg.mdce.common.internal.miu.service.CmiuConfigMgmtService;
import com.neptunetg.mdce.common.internal.miu.service.CmiuConfigSetService;
import com.neptunetg.mdce.integration.domain.CmiuConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controllers for providing internal rest service for mdce-web. Implements command and control backend.
 */
@RestController
@RequestMapping("/service")
public class CmiuConfigServiceController implements CmiuConfigMgmtService, CmiuConfigSetService
{

    private static final Logger logger = LoggerFactory.getLogger(CmiuConfigServiceController.class);

    @Autowired
    private CmiuConfigService configService;

    /**
     * Get the CMIU config managment details for a cmiu. This includes the current config, planned config, reported config etc.
     *
     * @param cmiuId the cmiu id
     * @return Status counts
     * @throws InternalApiException If something goes wrong in the request
     */
    @Override
    @RequestMapping(URL_CMIU_CONFIG)
    public CmiuConfigMgmt getCmiuConfigMgmt(@RequestParam(PARAM_CMIU_ID) long cmiuId) throws InternalApiException
    {
        return this.configService.getCmiuConfigMgmt(MiuId.valueOf(cmiuId));
    }

    /**
     * Mass commit a list of new CMIU configuration changes for different CMIUs.
     *
     * @param cmiuConfigChangeList list of config set for each cmiu id
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(value = URL_CMIU_CONFIG, method = RequestMethod.POST)
    public CmiuConfigMgmt addCmiuConfig(@RequestParam(value = CmiuConfigMgmtService.PARAM_USER) String userName, @RequestBody List<CmiuConfigChange> cmiuConfigChangeList)
            throws InternalApiException, JsonProcessingException
    {
        final Map<Long, ConfigSet> configSetLookup = new HashMap<>();
        for (CmiuConfigChange configChange: cmiuConfigChangeList)
        {
            final MiuId miuId = MiuId.valueOf(configChange.getCmiuId());
            final Long desiredConfigSetId = Long.valueOf(configChange.getConfigSetId());

            ConfigSet configSet = configSetLookup.get(desiredConfigSetId);
            if (configSet == null)
            {
                configSet = this.configService.getConfigSetFromDBById(desiredConfigSetId.longValue());
                if (configSet == null)
                {
                    logger.warn("Can't apply config set with ID " + desiredConfigSetId + " to " + miuId + " because the config set does not exist in the DB");
                }
            }

            if (configSet != null)
            {
                this.configService.setCurrentCmiuConfig(miuId, configSet, userName, configChange.getChangeNote());
            }
        }

        return null;
    }

    /**
     * Get a list of cmiu config mgmt entries representing CMIU config commands that are being schedule for sending to CMIU.
     * This is done by comparing the configSet value of current and reported config set regardless of the config set id. If they are different,
     * then a config change has been made and is awaiting confirmation from the CMIU that it has been implemented in the hardware.
     *
     * @return list of CMIU which are awaiting confirmation of changes to their config.
     */
    @Override
    @RequestMapping(value = URL_CMIU_CONFIG_CHANGE_IN_PROCESS)
    public List<CmiuConfigMgmt> getCmiuConfigChangeInProcessList() throws InternalApiException
    {
        return this.configService.getCmiuConfigChangeInProcess();
    }

    /**
     * Get a list of cmiu config mgmt entries representing CMIU config commands that has been changed and acknowledged (reported) by the CMIU
     * for the last X days.
     * This filters the Cmiu config mgmt table to find reported config which has been changed in the last X days and matches planned config.
     *
     * @param changedDaysAgo the number of days ago the changes has been made to the reported config
     * @return filtered list of cmiu mgmt table
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(value = URL_CMIU_CONFIG_CHANGE_COMPLETED)
    public List<CmiuConfigMgmt> getCmiuConfigChangeCompletedList(
            @RequestParam(PARAM_CONFIG_SET_LAST_CHANGED_DAYS_AGO) int changedDaysAgo) throws InternalApiException
    {
        return this.configService.getCmiuConfigChangeCompleted(changedDaysAgo);
    }

    @Override
    @RequestMapping(value = URL_CMIU_CONFIG_HISTORY)
    public List<CmiuConfigHistory> getCmiuConfigHistory(@RequestParam(PARAM_CMIU_ID) long cmiuId) throws InternalApiException
    {
        MiuConfigHistoryQueryOptions queryOptions = new MiuConfigHistoryQueryOptions();
        queryOptions.setMiuId(MiuId.valueOf(cmiuId));
        List<CmiuConfigHistory> ret = this.configService.getConfigHistory(queryOptions);
        Collections.reverse(ret); //seems we want this in descending date order
        return ret;
    }

    /**
     * Get a list of config sets
     *
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(URL_CMIU_CONFIG_SET_LIST)
    public List<CmiuConfigSet> getCmiuConfigSetList() throws InternalApiException
    {
        return this.configService.getConfigSetList();
    }

    /**
     * Get a config set from id
     *
     * @param configSetId the configset id
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(URL_CMIU_CONFIG_SET)
    public CmiuConfigSet getCmiuConfigSet(@RequestParam(value = PARAM_CONFIG_SET_ID) Long configSetId) throws InternalApiException
    {
        if (configSetId == null)
        {
            return null;
        }
        return this.configService.getConfigSetForApiById(configSetId.longValue());
    }

    /**
     * Add a new CMIU config set to database
     *
     * @param cmiuConfigSet new config set definition
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(value = URL_CMIU_CONFIG_SET, method = RequestMethod.POST)
    public void addCmiuConfigSet(@RequestParam(value = CmiuConfigSetService.PARAM_USER) String userName, @RequestBody CmiuConfigSet cmiuConfigSet) throws InternalApiException, JsonProcessingException
    {
        this.configService.addNewConfigSet(cmiuConfigSet, userName);
    }

    /**
     * Edit an existing CMIU config set
     *
     * @param cmiuConfigSet new config set definition
     * @throws InternalApiException
     */
    @Override
    @RequestMapping(value = URL_CMIU_CONFIG_SET_EDIT, method = RequestMethod.POST)
    public void editCmiuConfigSet(@RequestParam(value = CmiuConfigSetService.PARAM_USER) String userName, @RequestBody CmiuConfigSet cmiuConfigSet) throws InternalApiException, JsonProcessingException
    {
        //currently only name field can be changed for the exist config set.

        this.configService.updateConfigSetName(cmiuConfigSet, userName);
    }
}
