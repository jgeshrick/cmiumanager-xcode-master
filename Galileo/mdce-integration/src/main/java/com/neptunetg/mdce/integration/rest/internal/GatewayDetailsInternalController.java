/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.gateway.model.GatewayDisplayDetails;
import com.neptunetg.mdce.common.internal.gateway.model.GatewayDisplayDetailsPagedResult;
import com.neptunetg.mdce.common.internal.gateway.model.GatewayHeardPackets;
import com.neptunetg.mdce.common.internal.gateway.model.GatewayMonthlyStats;
import com.neptunetg.mdce.common.internal.gateway.service.GatewayService;
import com.neptunetg.mdce.integration.data.gateway.GatewayDetails;
import com.neptunetg.mdce.integration.data.gateway.GatewayId;
import com.neptunetg.mdce.integration.data.gateway.GatewayRepository;
import com.neptunetg.mdce.integration.rest.BaseTokenAuthenticatingRestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/service")
public class GatewayDetailsInternalController extends BaseTokenAuthenticatingRestController implements GatewayService
{
    private static final Logger logger = LoggerFactory.getLogger(GatewayDetailsInternalController.class);

    private static final String UTC = "UTC";

    @Autowired
    private GatewayRepository gatewayRepo;

    @Override
    @ResponseBody
    @RequestMapping(value = GatewayService.URL_GATEWAY_LIST, method = RequestMethod.GET)
    public GatewayDisplayDetailsPagedResult getGatewayList(@RequestParam Map<String, String> options) throws InternalApiException
    {
        final List<GatewayDetails> gatewaysForUtility = gatewayRepo.getGateways(options);

        Integer count = gatewayRepo.getGatewaysCount(options);

        //save a db call by correcting for gatewayId not found
        if (count == 1 && gatewaysForUtility.isEmpty())
        {
            count = 0;
        }

        final List<GatewayDisplayDetails> gatewayDisplayDetails = toGatewayDetailsList(gatewaysForUtility);

        ifRequestingSingleGatewayThenPopulateExtraDetails(options, gatewayDisplayDetails);

        final String page = options.get("page");

        final Integer pageNum = page == null ? 0 : Integer.parseInt(page);

        return new GatewayDisplayDetailsPagedResult(count, gatewayDisplayDetails.size(), pageNum, gatewayDisplayDetails);
    }

    private void ifRequestingSingleGatewayThenPopulateExtraDetails(Map<String, String> options, List<GatewayDisplayDetails> gatewayDisplayDetails)
    {
        if (options.containsKey("gatewayId") && gatewayDisplayDetails.size() == 1)
        {
            fetchExtraDetails(options, gatewayDisplayDetails.get(0));
        }
    }

    private void fetchExtraDetails(Map<String, String> options, GatewayDisplayDetails gatewayDisplayDetails)
    {
        final String gatewayId = options.get("gatewayId");

        GatewayHeardPackets gatewayHeardPackets = gatewayRepo.getLatestGatewayHeardPackets(new GatewayId(gatewayId));
        GatewayMonthlyStats gatewayMonthlyStats = gatewayRepo.getLatestGatewayMonthlyStats(new GatewayId(gatewayId));

        if(gatewayHeardPackets != null)
        {
            gatewayDisplayDetails.setGatewayHeardPackets(gatewayHeardPackets);
        }

        if(gatewayMonthlyStats != null)
        {
            gatewayDisplayDetails.setGatewayMonthlyStats(gatewayMonthlyStats);
        }
    }

    private List<GatewayDisplayDetails> toGatewayDetailsList(List<GatewayDetails> gatewaysForUtility)
    {
        return gatewaysForUtility
                .stream()
                .map(this::getGatewayDisplayDetails)
                .collect(Collectors.toList());
    }

    private GatewayDisplayDetails getGatewayDisplayDetails(GatewayDetails gateway)
    {
        final GatewayDisplayDetails gatewayDisplayDetails = new GatewayDisplayDetails();
        gatewayDisplayDetails.setSiteId(gateway.getSiteId().numericValue());
        gatewayDisplayDetails.setCreationDate(gateway.getFirstHeardTime());
        gatewayDisplayDetails.setGatewayId(gateway.getGatewayId().toString());
        gatewayDisplayDetails.setLastHeardTime(gateway.getLastHeardTime());
        gatewayDisplayDetails.setGateway_active(gateway.isGateway_active());

        return gatewayDisplayDetails;
    }

}

