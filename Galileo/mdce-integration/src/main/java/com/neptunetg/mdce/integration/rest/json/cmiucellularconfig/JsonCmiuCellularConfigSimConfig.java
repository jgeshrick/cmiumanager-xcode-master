/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.cmiucellularconfig;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by WJD1 on 24/06/2015.
 */
public class JsonCmiuCellularConfigSimConfig
{
    @JsonProperty("iccid")
    private String iccid = "123456789101112";

    @JsonProperty("imsi")
    private String imsi = "123456789101112";

    @JsonProperty("msidn")
    private String msidn = "123456789101112";

    public String getIccid()
    {
        return this.iccid;
    }

    public void setIccid(String iccid)
    {
        this.iccid = iccid;
    }

    public String getImsi()
    {
        return this.imsi;
    }

    public void setImsi(String imsi)
    {
        this.imsi = imsi;
    }

    public String getMsidn()
    {
        return this.msidn;
    }

    public void setMsidn(String msidn)
    {
        this.msidn = msidn;
    }
}
