/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CmiuConfigSetAssociation;
import com.neptunetg.common.data.miu.cellular.ConfigSet;
import com.neptunetg.common.data.miu.cellular.MiuConfigHistoryQueryOptions;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigHistory;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigMgmt;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;
import com.neptunetg.mdce.common.internal.miu.model.SequentialCmiuConfig;
import com.neptunetg.mdce.integration.data.site.SiteId;

import java.time.temporal.TemporalAccessor;
import java.util.List;

/**
 * Created by ABH1 on 12/09/2016.
 */
public interface CmiuConfigService
{
    CmiuConfigMgmt getCmiuConfigMgmt(MiuId miuId);

    ConfigSet setCurrentCmiuConfig(MiuId miuId, ConfigSet configSet, String userName, String changeNote);

    List<CmiuConfigMgmt> getCmiuConfigChangeInProcess();

    List<CmiuConfigMgmt> getCmiuConfigChangeCompleted(int changedDaysAgo);

    /**
     * Get the config history in a date range
     * @param queryOptions Defines options for filtering the results.
     * @return List of config history that applied in the given date range
     */
    List<CmiuConfigHistory> getConfigHistory(MiuConfigHistoryQueryOptions queryOptions);

    List<CmiuConfigSet> getConfigSetList();

    CmiuConfigSet getConfigSetForApiById(long configSetId);

    ConfigSet getConfigSetFromDBById(long configSetId);

    void addNewConfigSet(CmiuConfigSet cmiuConfigSet, String userName);

    void updateConfigSetName(CmiuConfigSet cmiuConfigSet, String userName);

    List<CmiuConfigSetAssociation> getCurrentCmiuConfigCollection();

    List<CmiuConfigSetAssociation> getCurrentCmiuConfigCollection(SiteId siteId);

    CmiuConfigSetAssociation getCurrentCmiuConfig(MiuId miuId) throws MiuDataException;

    List<SequentialCmiuConfig> getSequentialConfigHistory(Integer lastSequenceId);

}
