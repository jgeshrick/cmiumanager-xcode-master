/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Scheduler to handle received R900 gateway data file, as detailed in ETI 48-02
 * This level just starts the process
 */
@Service
@EnableScheduling
public class R900SchedulerService extends AbstractScheduler
{
    private static final Logger logger = LoggerFactory.getLogger(R900SchedulerService.class);
    private static final Lock lockObject = new ReentrantLock();

    @Autowired
    public R900SchedulerService(ScheduledProcessor processor)
    {
        super(processor);
        logger.debug("Creating new R900SchedulerService");
    }

    /**
     * Run every x minutes (tbd)
     * Seconds=0, Minutes=every x, Hours=*, Day of month=*, Month=*, Day of week=*
     * Timezone = US Central, not that this is relevant
     */
    @Scheduled(cron = "*/20 * * * * *")
    public void process()
    {
        if(lockObject.tryLock())
        {
            try
            {
                logger.debug("Running the scheduled R900 tarball checker process");
                runScheduledProcess();
            }
            finally
            {
                lockObject.unlock();
            }
        }
        else
        {
            logger.error("Attempted to process R900 tar files but already performing action");
        }
    }
}