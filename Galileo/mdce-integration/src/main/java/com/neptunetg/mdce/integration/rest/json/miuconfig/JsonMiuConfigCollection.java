/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.miuconfig;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.cellular.CmiuConfigSetAssociation;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;

import java.util.List;
import java.util.stream.Collectors;

/**
    JSON object for return multiple MIUs' configuration.
*/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonMiuConfigCollection
{
    @JsonProperty("mius")
    private List<JsonMiuConfig> miuConfigList;

    public List<JsonMiuConfig> getMiuConfigList()
    {
        return miuConfigList;
    }

    public void setMiuConfigList(List<JsonMiuConfig> miuConfigList)
    {
        this.miuConfigList = miuConfigList;
    }

    public static JsonMiuConfigCollection from(List<CmiuConfigSetAssociation> configSetList)
    {
        JsonMiuConfigCollection mcc = new JsonMiuConfigCollection();
        List<JsonMiuConfig> configList = configSetList.stream()
                .map(JsonMiuConfig::from)
                .collect(Collectors.toList());

        mcc.setMiuConfigList(configList);

        return mcc;
    }

}
