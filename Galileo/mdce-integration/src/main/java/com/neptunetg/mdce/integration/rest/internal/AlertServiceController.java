/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLog;
import com.neptunetg.mdce.common.internal.alert.service.AlertRestService;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/service")
public class AlertServiceController implements AlertRestService
{
    @Autowired
    AlertService alertService;

    @Override
    @RequestMapping(value=URL_ALERT_LIST_NEW, method= RequestMethod.POST)
    public List<AlertDetails> getNewAlertList(@RequestBody String alertSource) throws InternalApiException
    {
        List<AlertDetails> ret = alertService.getNewAlertList(alertSource);

        return ret;
    }

    @Override
    @RequestMapping(value=URL_ALERT_LIST_HANDLED, method= RequestMethod.POST)
    public List<AlertDetails> getHandledAlertList(@RequestBody String alertSource) throws InternalApiException
    {
        return alertService.getHandledAlertList(alertSource);
    }

    @Override
    @RequestMapping(value=URL_ALERT_LIST_STALE, method= RequestMethod.POST)
    public List<AlertDetails> getStaleAlertList(@RequestBody String alertSource) throws InternalApiException
    {
        return alertService.getStaleAlertList(alertSource);
    }

    @Override
    @RequestMapping(value=URL_ALERT_LIST_CLEARED, method= RequestMethod.POST)
    public List<AlertDetails> getClearedAlertList(@RequestBody String alertSource) throws InternalApiException
    {
        return alertService.getClearedAlertList(alertSource);
    }

    @Override
    @RequestMapping(value=URL_ALERT_SINGLE, method= RequestMethod.POST)
    public AlertDetails getAlert(@RequestBody Long alertId) throws InternalApiException
    {
        return alertService.getAlert(alertId);
    }

    @Override
    @RequestMapping(value=URL_ALERT_ADD, method= RequestMethod.POST)
    public boolean addAlert(@RequestBody AlertDetails alertDetails) throws InternalApiException
    {
        return alertService.addAlertAndSendEmail(alertDetails);
    }

    @Override
    @RequestMapping(value=URL_ALERT_SET_NEW, method= RequestMethod.POST)
    public boolean setAlertStateToNew(@RequestBody Long alertId) throws InternalApiException
    {
        return alertService.setAlertStateToNew(alertId);
    }

    @Override
    @RequestMapping(value=URL_ALERT_SET_HANDLED, method= RequestMethod.POST)
    public boolean setAlertStateToHandled(@RequestBody Long alertId) throws InternalApiException
    {
        return alertService.setAlertStateToHandled(alertId);
    }

    @Override
    @RequestMapping(value=URL_ALERT_SET_STALE, method= RequestMethod.POST)
    public boolean setAlertStateToStale(@RequestBody Long alertId) throws InternalApiException
    {
        return alertService.setAlertStateToStale(alertId);
    }

    @Override
    @RequestMapping(value=URL_ALERT_SET_CLEARED, method= RequestMethod.POST)
    public boolean setAlertStateToCleared(@RequestBody Long alertId) throws InternalApiException
    {
        return alertService.setAlertStateToCleared(alertId);
    }

    @Override
    @RequestMapping(value=URL_ALERT_SET_WARNING, method= RequestMethod.POST)
    public boolean setAlertLevelToWarning(@RequestBody Long alertId) throws InternalApiException
    {
        return alertService.setAlertLevelToWarning(alertId);
    }

    @Override
    @RequestMapping(value=URL_ALERT_SET_ERROR, method = RequestMethod.POST)
    public boolean setAlertLevelToError(@RequestBody Long alertId) throws InternalApiException
    {
        return alertService.setAlertLevelToError(alertId);
    }

    @Override
    public boolean setAlertTicketId(@RequestBody AlertDetails alertDetails) throws InternalApiException
    {
        return alertService.setAlertTicketId(alertDetails);
    }

    @Override
    public boolean addAlertLogMessage(@RequestBody AlertDetails alertDetails) throws InternalApiException
    {
        return alertService.addAlertLogMessage(alertDetails);
    }

    @Override
    public List<AlertLog> getAlertLog(@RequestBody Long alertId) throws InternalApiException
    {
        return alertService.getAlertLog(alertId);
    }

}
