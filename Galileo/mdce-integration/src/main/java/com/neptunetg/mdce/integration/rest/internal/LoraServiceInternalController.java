/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.internal;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lora.LoraDeviceDetail;
import com.neptunetg.common.data.miu.lora.LoraNetwork;
import com.neptunetg.common.data.miu.lora.LoraRepository;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.lora.model.LoraDisplayDetails;
import com.neptunetg.mdce.common.internal.lora.service.LoraService;
import com.neptunetg.mdce.integration.rest.BaseTokenAuthenticatingRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/service")
public class LoraServiceInternalController extends BaseTokenAuthenticatingRestController implements LoraService
{
    @Autowired
    private LoraRepository loraRepository;

    @Override
    @ResponseBody
    @RequestMapping(value = URL_LORA_DETAILS, method = RequestMethod.GET)
    public LoraDisplayDetails getLoraDetails(@RequestParam(value = "miuId") int miuId) throws InternalApiException
    {
        LoraDeviceDetail loraDeviceDetail = loraRepository.getLoraDeviceDetailByMiuId(MiuId.valueOf(miuId));

        LoraDisplayDetails loraDisplayDetails = new LoraDisplayDetails(loraDeviceDetail.getMiuId().toString(),
                loraDeviceDetail.getEui().toString(),
                loraDeviceDetail.getLoraNetwork().toString(),
                loraDeviceDetail.getLastUpdateTime().toString());

        return loraDisplayDetails;
    }

    @Override
    public List<String> getLoraNetworkList() throws InternalApiException
    {
        List<String> networks = new ArrayList<>();

        for(LoraNetwork networkEnum : LoraNetwork.values())
        {
            networks.add(networkEnum.getStringValue());
        }

        return networks;
    }
}
