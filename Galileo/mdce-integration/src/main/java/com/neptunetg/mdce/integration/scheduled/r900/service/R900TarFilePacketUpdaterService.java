/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.scheduled.r900.service;

import com.neptunetg.common.data.miu.heard.MiuUpdateRepository;
import com.neptunetg.mdce.common.data.DynamoPacketRepository;
import com.neptunetg.mdce.integration.data.gateway.GatewayRepository;
import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import com.neptunetg.mdce.integration.scheduled.r900.FileProcessor;
import com.neptunetg.mdce.integration.scheduled.r900.analysis.FileAnalyzer;
import com.neptunetg.mdce.integration.scheduled.r900.analysis.R900TarFileAnalyzer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;


/**
 * Handles R900 gateway data files, as detailed in ETI 48-02
 * Is passed a file for processing, constructs a processor, gets results and updates dynamo and db
 */
@Service
public class R900TarFilePacketUpdaterService implements FileProcessor
{
    private static final Logger logger = LoggerFactory.getLogger(R900TarFilePacketUpdaterService.class);

    private final DynamoPacketRepository dynamoRepo;

    private final MiuUpdateRepository miuRepo;
    private final GatewayRepository gatewayRepo;

    private static final String STORE_R900_PACKETS_PROPERTY_NAME = "gateway.data.store.R900.packets";
    private boolean storeR900Packets;

    /**
     * Constructor for R900TarFileHandler
     *
     * @param dynamoRepo Dynamo repository
     * @param miuRepo    MiuUpdateRepository
     */
    @Autowired
    public R900TarFilePacketUpdaterService(DynamoPacketRepository dynamoRepo,
                                           MiuUpdateRepository miuRepo,
                                           GatewayRepository gatewayRepo,
                                           @Value("#{envProperties['" + STORE_R900_PACKETS_PROPERTY_NAME + "']}") Boolean storeR900Packets)
    {
        this.dynamoRepo = dynamoRepo;
        this.miuRepo = miuRepo;
        this.gatewayRepo = gatewayRepo;
        if (storeR900Packets == null) {
            logger.warn("Environment property " + STORE_R900_PACKETS_PROPERTY_NAME + " not set in mdce-env.properties, defaulting to true (packets from tarballs will be stored in Dynamo).");
            this.storeR900Packets = true;
        }
        else
        {
            this.storeR900Packets = storeR900Packets.booleanValue();
            logger.info("Environment property " + STORE_R900_PACKETS_PROPERTY_NAME + " is set to " + storeR900Packets + " in mdce-env.properties (packets from tarballs " + (this.storeR900Packets ? "will" : "will not") + " be stored).");

        }
    }

    @Override
    public File process(File tarFileToProcess)
    {
        try (final R900RelationalDataSpooler dataSaver = new R900RelationalDataSpooler(miuRepo, gatewayRepo))
        {
            final R900DynamoPacketSpooler dynamoSaver = new R900DynamoPacketSpooler(this.dynamoRepo);

            final FileAnalyzer tarFileProcesser = new R900TarFileAnalyzer(tarFileToProcess,
                    dynamoSaver, dataSaver, storeR900Packets);
            tarFileProcesser.processFile();

        } catch (Exception e)
        {
            throw new MdceRestException("Exception thrown processing " + tarFileToProcess.getName(), e);
        }

        return tarFileToProcess;
    }
}