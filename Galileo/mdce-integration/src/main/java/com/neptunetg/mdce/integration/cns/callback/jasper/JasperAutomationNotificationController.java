/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.cns.callback.jasper;

import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.cns.jasper.JasperAutomationNotificationControllerBase;
import com.neptunetg.common.cns.jasper.JasperPushNotificationException;
import com.neptunetg.common.cns.jasper.PushForm;
import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.cellular.CellularDeviceDetail;
import com.neptunetg.common.data.miu.cellular.CellularDeviceId;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.integration.cns.CnsUtils;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import com.neptunetg.mdce.integration.domain.CellularDeviceMonitoringService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.ZonedDateTime;

/**
 * Rest controller for handling Jasper Control Center Automation API Push event.
 */
@RestController
@RequestMapping(value = "cns/jasper")
public class JasperAutomationNotificationController extends JasperAutomationNotificationControllerBase
{
    private static final Logger logger = LoggerFactory.getLogger(JasperAutomationNotificationController.class);
    private static final String NO_CONNECTION_WITHIN_LAST_24_HOUR = "No connection within last 24 hour";

    @Autowired
    CmiuRepository cmiuRepo;

    @Autowired
    AlertService alertService;

    @Autowired
    CellularDeviceMonitoringService cellularDeviceMonitoringService;

    public JasperAutomationNotificationController()
    {
        //setup callback handler
        setDeviceStateChangeHandler((deviceIdentifierKind, deviceIdentifierValue, state) ->
        {
            //value: "Restore", "Suspend", from callback results. Other values, according to doc, is "Activate" and "Deactivate"
            try
            {
                cmiuRepo.changeCellularDeviceState(CnsUtils.cellularDeviceId(deviceIdentifierKind, deviceIdentifierValue), state);
                return true;
            }
            catch (MiuDataException ex)
            {
                logger.error("Error saving device state change to repo", ex);
                return false;
            }
        });

        setDeviceUsageNotificationHandler(this::checkDeviceMonthlyUseLimit);

        //set handler for pooled data usage limit handler
        setPooledDataUsageLimitHandler(this::raisePooledDataExceedLimit);

        //set handler for no device connection
        setNoConnectionNotificationHandler(this::raiseNoDeviceConnectionAlert);

        //daily usage limit exceeded
        setDeviceDailyUsageLimitHandler((iccid, byteUsed) -> {
            //TODO: implement 24h data usage limit alert
            return true;
        });

    }

    @Override
    @RequestMapping(value = "sim-state-change", method = RequestMethod.POST)
    public void notifySimStateChange(PushForm pushForm) throws JasperPushNotificationException
    {
        logger.debug("Received device state notification {}", pushForm);

        super.notifySimStateChange(pushForm);
    }

    @Override
    @RequestMapping(value = "usage-monitoring", method = RequestMethod.POST)
    public void notifyUsageDataExceed(PushForm pushForm) throws Exception
    {
        logger.debug("Received usage notification {}",
                pushForm);

        super.notifyUsageDataExceed(pushForm);

    }

    @Override
    @RequestMapping(value = "session-monitoring", method = RequestMethod.POST)
    public void notifySession(PushForm pushForm) throws JasperPushNotificationException
    {
        logger.debug("Received session notification {}", pushForm);

        //do nothing, we just want to proof that the event is received and deserialised by the request handler.
        super.notifySession(pushForm);
    }

    /**
     * This is a simple echo test for checking the request can be handled.
     */
    @RequestMapping(value="echo", method=RequestMethod.GET)
    public String echoTest()
    {
        return Instant.now().toString();
    }

    /**
     * Used to raise alert if the pooled data usage has exceeded limit.
     * @param usedBytes total used bytes
     * @return true if execution success
     */
    private boolean raisePooledDataExceedLimit(final long usedBytes)
    {
        //TODO: Pooled data alert has not been implemented yet
        return true;
    }

    /**
     * Raise an error to notify that the device has not had any data connection for the past 24hrs
     * @param iccid device iccid
     * @return true if success
     */
    private boolean raiseNoDeviceConnectionAlert(final String iccid)
    {
        CellularDeviceDetail cellularDevice = null;
        try
        {
            cellularDevice = cmiuRepo.getCellularConfigFromCellularDeviceIdentifier(CellularDeviceId.iccid(iccid));
        } catch (MiuDataException e)
        {
            logger.error("Cannot add alert", e);
            return false;
        }

        if (cellularDevice != null)
        {
            final AlertSource alertSource = AlertSource.cellularNoConnection(cellularDevice.getMiuId().numericValue());
            AlertDetails alertDetails = new AlertDetails();
            alertDetails.setAlertLevel(AlertLevel.WARNING);
            alertDetails.setAlertSource(alertSource.getAlertId());
            alertDetails.setAlertState(AlertState.NEW);

            alertDetails.setRecentMessage(NO_CONNECTION_WITHIN_LAST_24_HOUR);
            alertDetails.setAlertCreationDate(ZonedDateTime.now());

            try
            {
                alertService.addAlertAndSendEmail(alertDetails);
                return true;
            }
            catch (InternalApiException e)
            {
                logger.error("Cannot add alert", e);
            }
        }

        return false;
    }

    /**
     * Check device data usage against monthly quota and raise warning or alert if exceeded
     * @param iccid iccid as reported by Jasper
     * @param byteUsed byte use cycle to date.
     * @return always true when then there is no error.
     */
    private boolean checkDeviceMonthlyUseLimit(final String iccid, final long byteUsed)
    {
        CellularDeviceDetail cellularDevice = null;
        try
        {
            cellularDevice = cmiuRepo.getCellularConfigFromCellularDeviceIdentifier(CellularDeviceId.iccid(iccid));
        } catch (MiuDataException e)
        {
            logger.error("Cannot check use limit for ICCID " + iccid, e);
            return false;
        }

        cellularDeviceMonitoringService.checkMonthlyUseAllowanceAndRaiseAlertIfExceeded(ZonedDateTime.now(), cellularDevice, byteUsed);

        return true;
    }

}
