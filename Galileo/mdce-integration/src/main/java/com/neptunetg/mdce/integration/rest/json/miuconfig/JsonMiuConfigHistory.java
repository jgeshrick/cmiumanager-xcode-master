/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest.json.miuconfig;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigHistory;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonMiuConfigHistory
{
    @JsonProperty("miu_id")
    private long miuId;

    @JsonProperty("distributer_id")
    private String distributerId;

    @JsonProperty("site_id")
    private int siteId;

    @JsonProperty("device_type")
    private String deviceType;

    @JsonIgnore
    private Integer deviceTypeId;

    @JsonProperty("billable")
    private String billable;

    @JsonProperty("configuration_history")
    private List<JsonMiuConfigHistoryEntry> configurationHistories;

    public static JsonMiuConfigHistory from(List<CmiuConfigHistory> miuConfigHistoryList, long cmiuId, Optional<String> distributerId, ZoneId timezone)
    {
        return from(miuConfigHistoryList, MiuId.valueOf(cmiuId), distributerId, timezone);
    }

    public static JsonMiuConfigHistory from(List<CmiuConfigHistory> miuConfigHistoryList, int cmiuId, Optional<String> distributerId, ZoneId timezone)
    {
        return from(miuConfigHistoryList, MiuId.valueOf(cmiuId), distributerId, timezone);
    }

    /**
     * Populate config history json object from data obtained from RDS.
     *
     * If there are multiple changes on the same day, only the last is included
     * @param sortedMiuConfigHistoryList the miu config history list obtained from RDS, sorted by field dateTime in decreasing order, latest first
     * @param cmiuId The MIU ID.
     * @param distributerId distributed id if required
     * @param timezone the local timezone to use for startDate and endDate.
     * @return Json object containing config history for a CMIU in descending date order
     */
    public static JsonMiuConfigHistory from(List<CmiuConfigHistory> sortedMiuConfigHistoryList, MiuId cmiuId, Optional<String> distributerId, ZoneId timezone)
    {
        JsonMiuConfigHistory result = new JsonMiuConfigHistory();

        //populate miu details
        Optional<CmiuConfigHistory> miuConfigHistory = sortedMiuConfigHistoryList.stream()
                .filter(c -> cmiuId.matches(c.getMiuId()))    //only process those matching the cmiuId
                .findAny();

        if (!miuConfigHistory.isPresent())
        {
            return null;
        }

        //populate miu details
        final CmiuConfigHistory configHistoryExample = miuConfigHistory.get();
        result.setSiteId(configHistoryExample.getSiteId());
        result.setMiuId(configHistoryExample.getMiuId());
        result.setDeviceType(configHistoryExample.getMiuType());
        result.setDeviceTypeId(configHistoryExample.getMiuTypeId());
        result.setDistributerId(distributerId.isPresent() ? distributerId.get() : null);
        result.setBillable(configHistoryExample.getCmiuConfigSet().isBillableAsString());

        //get the history entries for this CMIU

        final List<JsonMiuConfigHistoryEntry> jsonConfigurationHistoryListForCmiu = new ArrayList<>(sortedMiuConfigHistoryList.size());
        JsonMiuConfigHistoryEntry previousEntry = null; //keep track of the entry which is earlier in the list but later in time
        Instant previousEntryPreciseDateTime = null;

        for (CmiuConfigHistory ch : sortedMiuConfigHistoryList)
        {
            if (cmiuId.matches(ch.getMiuId()))
            {
                JsonMiuConfigHistoryEntry newEntry = JsonMiuConfigHistoryEntry.from(ch, distributerId, timezone);

                if (previousEntry == null)
                {
                    jsonConfigurationHistoryListForCmiu.add(newEntry);
                    previousEntry = newEntry;
                    previousEntryPreciseDateTime = ch.getDateTime();
                }
                else if (newEntry.startDate.isBefore(previousEntry.startDate))
                {
                    //only record if it starts on a different day to the preceding entry
                    newEntry.endDate = previousEntry.startDate.minusDays(1L);
                    jsonConfigurationHistoryListForCmiu.add(newEntry);
                    previousEntry = newEntry;
                    previousEntryPreciseDateTime = ch.getDateTime();
                }
                else if (ch.getDateTime().isAfter(previousEntryPreciseDateTime))
                {
                    throw new IllegalArgumentException("sortedMiuConfigHistoryList must be sorted in reverse date order, but contains date sequence " +
                            previousEntryPreciseDateTime + ", " + ch.getDateTime() + " for MIU " + cmiuId);
                }
            }
        }

        result.setConfigurationHistories(jsonConfigurationHistoryListForCmiu);

        return result;
    }

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public String getDistributerId()
    {
        return distributerId;
    }

    public void setDistributerId(String distributerId)
    {
        this.distributerId = distributerId;
    }

    public String getDeviceType()
    {
        return deviceType;
    }

    public void setDeviceType(String deviceType)
    {
        this.deviceType = deviceType;
    }

    public String getBillable()
    {
        return billable;
    }

    public void setBillable(String billable)
    {
        this.billable = billable;
    }

    public List<JsonMiuConfigHistoryEntry> getConfigurationHistories()
    {
        return configurationHistories;
    }

    public void setConfigurationHistories(List<JsonMiuConfigHistoryEntry> configurationHistories)
    {
        this.configurationHistories = configurationHistories;
    }

    public Integer getDeviceTypeId()
    {
        return deviceTypeId;
    }

    public void setDeviceTypeId(Integer deviceTypeId)
    {
        this.deviceTypeId = deviceTypeId;
    }

    public static class JsonMiuConfigHistoryEntry
    {
        @JsonProperty("config")
        private JsonMiuConfigDetail miuConfigDetail;

        /**
         * The date in the current time zone from which the config applies.
         */
        @JsonIgnore
        private LocalDate startDate;

        /**
         * The date in the current time zone until which the config applies.
         */
        @JsonIgnore
        private LocalDate endDate;

        @JsonProperty("start_date")
        @JsonInclude(JsonInclude.Include.ALWAYS)
        public String getStartDateAsString()
        {
            if (startDate == null)
            {
                return null;
            }
            else
            {
                return startDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            }
        }

        @JsonProperty("end_date")
        @JsonInclude(JsonInclude.Include.ALWAYS)
        public String getEndDateAsString()
        {
            if (endDate == null)
            {
                return null;
            }
            else
            {
                return endDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            }
        }

        /**
         * The UTC timestamp from which the configuration applies.
         */
        @JsonIgnore
        private Instant timestamp;

        /**
         * Indicates whether the configuration history entry was an MDCE request or a confirmation from the MIU.
         */
        @JsonIgnore
        private String receivedFromMiu;

        public static JsonMiuConfigHistoryEntry from(CmiuConfigHistory cmiuConfigHistory, Optional<String> distributerId, ZoneId timezone)
        {
            JsonMiuConfigHistoryEntry configurationHistory = new JsonMiuConfigHistoryEntry();
            configurationHistory.setMiuConfigDetail(JsonMiuConfigDetail.from(cmiuConfigHistory.getCmiuConfigSet(), distributerId, cmiuConfigHistory.getMiuTypeId()));
            configurationHistory.setStartDate(ZonedDateTime.ofInstant(cmiuConfigHistory.getDateTime(), timezone).toLocalDate());
            configurationHistory.setReceivedFromMiu(cmiuConfigHistory.isReceivedFromCmiu() ? "Y" : "N");

            return configurationHistory;
        }

        public LocalDate getStartDate()
        {
            return startDate;
        }

        public void setStartDate(LocalDate startDate)
        {
            this.startDate = startDate;
        }

        public LocalDate getEndDate()
        {
            return endDate;
        }

        public void setEndDate(LocalDate endDate)
        {
            this.endDate = endDate;
        }

        public JsonMiuConfigDetail getMiuConfigDetail()
        {
            return miuConfigDetail;
        }

        public void setMiuConfigDetail(JsonMiuConfigDetail miuConfigDetail)
        {
            this.miuConfigDetail = miuConfigDetail;
        }

        public String getReceivedFromMiu()
        {
            return receivedFromMiu;
        }

        public void setReceivedFromMiu(String receivedFromMiu)
        {
            this.receivedFromMiu = receivedFromMiu;
        }
    }
}
