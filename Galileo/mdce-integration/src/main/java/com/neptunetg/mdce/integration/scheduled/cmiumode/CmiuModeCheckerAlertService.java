/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.scheduled.cmiumode;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.mode.MiuMode;

/**
 * Created by dcloud on 20/05/2017.
 * A class to check if an alert needs to be raised for a CMIU given its mode
 */
public interface CmiuModeCheckerAlertService
{
    /**
     * Get reported mode and check if it is an official mode
     * @return A MiuMode object with official_mode db field
     */
    MiuMode checkForOfficialMode(MiuId miuId, MiuMode miuMode);
}
