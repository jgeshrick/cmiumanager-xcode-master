/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.rest.json.refdata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.mdce.common.internal.site.model.MdceReferenceData;

import java.util.List;

/**
 * MdceReferenceData
 *
 * {
 *   "distributers": [
 *     ...
 *   ],
 *   "utilities": [
 *     ...
 *   ]}
 * }
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonReferenceData
{

    @JsonProperty("distributers")
    private List<JsonReferenceDataDistributer> distributers;

    @JsonProperty("utilities")
    private List<JsonReferenceDataUtility> utilities;

    public List<JsonReferenceDataUtility> getUtilities()
    {
        return utilities;
    }

    public void setUtilities(List<JsonReferenceDataUtility> utilities)
    {
        this.utilities = utilities;
    }

    public List<JsonReferenceDataDistributer> getDistributers()
    {
        return distributers;
    }

    public void setDistributers(List<JsonReferenceDataDistributer> distributers)
    {
        this.distributers = distributers;
    }


    public static JsonReferenceData from(MdceReferenceData mdceReferenceData)
    {
        JsonReferenceData jsonReferenceData = new JsonReferenceData();

        jsonReferenceData.setUtilities(JsonReferenceDataUtility.from(mdceReferenceData.getUtilities()));
        jsonReferenceData.setDistributers(JsonReferenceDataDistributer.from(mdceReferenceData.getDistributers()));

        return jsonReferenceData;
    }
}

