/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.rest.json.packets;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.mdce.common.data.model.DynamoItemPacketType;
import com.neptunetg.mdce.common.data.model.GatewayPacketReceivedDynamoItem;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import com.neptunetg.mdce.integration.utility.DateTimeUtility;

import javax.xml.bind.DatatypeConverter;

/**
 An individual MIU or GW packet, with metadata
 */
public class JsonPacketData
{
    @JsonProperty("built_date")
    private String builtDate;

    @JsonProperty("insert_date")
    private String insertDate;

    @JsonProperty("device_type")
    private String deviceType;

    @JsonProperty("packet_type")
    private int packetType;

    private String packet;

    public String getBuiltDate()
    {
        return builtDate;
    }

    public void setBuiltDate(String builtDate)
    {
        this.builtDate = builtDate;
    }

    public String getInsertDate()
    {
        return insertDate;
    }

    public void setInsertDate(String insertDate)
    {
        this.insertDate = insertDate;
    }

    public String getDeviceType()
    {
        return deviceType;
    }

    public void setDeviceType(String deviceType)
    {
        this.deviceType = deviceType;
    }

    public int getPacketType()
    {
        return packetType;
    }

    /**
     * Given the packet type in the format of CMIU:1, store only the id
     * @param packetType
     */
    public void setPacketType(int packetType)
    {

        this.packetType = packetType;

    }

    public String getPacket()
    {
        return packet;
    }

    public void setPacket(String packet)
    {
        this.packet = packet;
    }

    /**
     * Build a packets packet from DynamoDb data model
     * @param miuPacket data model from Dynamo
     * @return JsonPacketData object
     */
    public static JsonPacketData fromMiuPacket(MiuPacketReceivedDynamoItem miuPacket)
    {
        JsonPacketData packet = new JsonPacketData();
        packet.setBuiltDate(DateTimeUtility.getUsCentralDateForEpochMilli(miuPacket.getDateBuilt()));
        packet.setInsertDate(DateTimeUtility.getUsCentralDateForEpochMilli(miuPacket.getInsertDate()));
        packet.setPacket(DatatypeConverter.printHexBinary(miuPacket.getPacketData().array()));

        final DynamoItemPacketType type = DynamoItemPacketType.fromString(miuPacket.getPacketType());

        packet.setPacketType(type.getId());
        packet.setDeviceType(type.getSystemName());

        return packet;
    }

    /**
     * Build a gateway packet from DynamoDb data model
     * @param gatewayPacket data model from Dynamo
     * @return JsonPacketData object
     */
    public static JsonPacketData fromGatewayPacket(GatewayPacketReceivedDynamoItem gatewayPacket)
    {
        JsonPacketData packet = new JsonPacketData();
        packet.setBuiltDate(DateTimeUtility.getUsCentralDateForEpochMilli(gatewayPacket.getDateBuilt()));
        packet.setInsertDate(DateTimeUtility.getUsCentralDateForEpochMilli(gatewayPacket.getInsertDate()));
        packet.setPacket(DatatypeConverter.printHexBinary(gatewayPacket.getPacketData().array()));

        final DynamoItemPacketType type = DynamoItemPacketType.fromString(gatewayPacket.getPacketType());

        packet.setPacketType(type.getId());
        packet.setDeviceType(type.getSystemName());

        return packet;
    }

}
