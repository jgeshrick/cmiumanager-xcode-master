/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.miuconfig;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.common.data.miu.cellular.CmiuConfigSetAssociation;
import com.neptunetg.common.data.miu.cellular.ConfigSet;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigSet;

import java.util.Optional;

/**
 * Represent a json object for a single MIU config.
 * Used for REST API GetMiuConfig (IF16) and Set Miu Config (IF38).
 *  {\"billing\":
 *      {\"site_id\":"+ siteId +",\"distributer_id\":999,\"billable\":\"N\"},
 * "{\"miu_id\":" + testCmiuId + ",\"config\":
 *      \"recording\":{\"start_time_mins\":0,\"interval_mins\":15},\"reporting\":
 *          {\"start_time_mins\":0,\"interval_mins\":60,\"num_retries\":0,\"transmit_window_mins\":15},
 *      \"quiet_time\":{\"quiet_start_mins\":0,\"quiet_end_mins\":0}}}"
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonMiuConfigDetail
{
    @JsonProperty("CMIU_type")
    private Integer cmiuType;

    @JsonProperty("billing")
    private Billing billing;

    @JsonProperty("recording")
    private Recording recording;

    @JsonProperty("reporting")
    private Reporting reporting;

    @JsonProperty("quiet_time")
    private QuietTime quietTime;

    public Integer getCmiuType()
    {
        return cmiuType;
    }

    public void setCmiuType(Integer cmiuType)
    {
        this.cmiuType = cmiuType;
    }

    public Billing getBilling()
    {
        return billing;
    }

    public void setBilling(Billing billing)
    {
        this.billing = billing;
    }

    public Recording getRecording()
    {
        return recording;
    }

    public void setRecording(Recording recording)
    {
        this.recording = recording;
    }

    public Reporting getReporting()
    {
        return reporting;
    }

    public void setReporting(Reporting reporting)
    {
        this.reporting = reporting;
    }

    public QuietTime getQuietTime()
    {
        return quietTime;
    }

    public void setQuietTime(QuietTime quietTime)
    {
        this.quietTime = quietTime;
    }

    public static JsonMiuConfigDetail from(CmiuConfigSetAssociation configSetAssociation, Optional<String> distributerId)
    {
        JsonMiuConfigDetail configDetail = new JsonMiuConfigDetail();
        Billing billing = new JsonMiuConfigDetail.BillingWithBillableFlag();
        configDetail.setBilling(billing);
        billing.setSiteId(configSetAssociation.getSiteId());
        billing.setBillable(configSetAssociation.isBillableAsString());
        if (distributerId.isPresent())
        {
            billing.setDistributerId(distributerId.get());
        }

        final ConfigSet configSet = configSetAssociation.getConfigSet();

        Recording recording = new Recording();
        configDetail.setRecording(recording);
        recording.setIntervalMins(configSet.getRecordingIntervalMins());
        recording.setStartTimeMins(configSet.getRecordingStartTimeMins());

        Reporting reporting = new JsonMiuConfigDetail.Reporting();
        configDetail.setReporting(reporting);
        reporting.setStartTimeMins(configSet.getReportingStartMins());
        reporting.setIntervalMins(configSet.getReportingIntervalMins());
        reporting.setRetries(configSet.getReportingNumberOfRetries());
        reporting.setTransmitWindowMins(configSet.getReportingTransmitWindowMins());

        QuietTime quietTime = new JsonMiuConfigDetail.QuietTime();
        configDetail.setQuietTime(quietTime);

        quietTime.setQuietStartMins(configSet.getReportingQuietStartMins());
        quietTime.setQuietEndMins(configSet.getReportingQuietEndMins());

        return configDetail;
    }


    public static JsonMiuConfigDetail from(CmiuConfigSet configSet, Optional<String> distributerId, Integer cmiuType)
    {
        return fromVersion(1, configSet, distributerId, cmiuType);
    }

    public static JsonMiuConfigDetail fromV2(CmiuConfigSet configSet, Optional<String> distributerId, Integer cmiuType)
    {
        return fromVersion(2, configSet, distributerId, cmiuType);
    }

    private static JsonMiuConfigDetail fromVersion(int apiVersion, CmiuConfigSet configSet, Optional<String> distributerId, Integer cmiuType)
    {
        JsonMiuConfigDetail configDetail = new JsonMiuConfigDetail();
        configDetail.setCmiuType(cmiuType);
        Billing billing;

        if (apiVersion == 1)
        {
            billing = new BillingWithBillableFlag();
        }
        else if (apiVersion == 2)
        {
            billing = new BillingWithoutBillableFlag();
        }
        else
        {
            throw new IllegalArgumentException("Unsupported apiVersion.");
        }

        configDetail.setBilling(billing);
        billing.setSiteId(configSet.getSiteId());
        billing.setBillable(configSet.isBillableAsString());
        if (distributerId.isPresent())
        {
            billing.setDistributerId(distributerId.get());
        }

        Recording recording = new Recording();
        configDetail.setRecording(recording);
        recording.setIntervalMins(configSet.getRecordingIntervalMins());
        recording.setStartTimeMins(configSet.getRecordingStartTimeMins());

        Reporting reporting = new JsonMiuConfigDetail.Reporting();
        configDetail.setReporting(reporting);
        reporting.setStartTimeMins(configSet.getReportingStartMins());
        reporting.setIntervalMins(configSet.getReportingIntervalMins());
        reporting.setRetries(configSet.getReportingNumberOfRetries());
        reporting.setTransmitWindowMins(configSet.getReportingTransmitWindowMins());

        QuietTime quietTime = new JsonMiuConfigDetail.QuietTime();
        configDetail.setQuietTime(quietTime);

        quietTime.setQuietStartMins(configSet.getReportingQuietStartMins());
        quietTime.setQuietEndMins(configSet.getReportingQuietEndMins());

        return configDetail;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public abstract static class Billing
    {
        @JsonProperty("site_id")
        private int siteId;

        @JsonProperty("distributer_id")
        private String distributerId;

        @JsonIgnore
        private String billable;

        public int getSiteId()
        {
            return siteId;
        }

        public void setSiteId(int siteId)
        {
            this.siteId = siteId;
        }

        public String getBillable()
        {
            return billable;
        }

        public void setBillable(String billable)
        {
            this.billable = billable;
        }

        public String getDistributerId()
        {
            return distributerId;
        }

        public void setDistributerId(String distributerId)
        {
            this.distributerId = distributerId;
        }
    }

    /**
     * API v1 implementation that includes the Billable flag in its JSON output.
     */
    public static class BillingWithBillableFlag extends Billing
    {
        @JsonProperty("billable")
        public String getBillable()
        {
            if (super.getBillable() == null)
            {
                return null;
            }
            else
            {
                return super.getBillable();
            }
        }
    }

    /**
     * API v2 implementation that does not include the Billable flag.
     */
    public static class BillingWithoutBillableFlag extends Billing
    {
    }

    @JsonInclude(JsonInclude.Include.ALWAYS)
    public static class Recording
    {
        @JsonProperty("start_time_mins")
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Integer startTimeMins;

        @JsonProperty("interval_mins")
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Integer intervalMins;

        public Integer getStartTimeMins()
        {
            return startTimeMins;
        }

        public void setStartTimeMins(Integer startTimeMins)
        {
            this.startTimeMins = startTimeMins;
        }

        public Integer getIntervalMins()
        {
            return intervalMins;
        }

        public void setIntervalMins(Integer intervalMins)
        {
            this.intervalMins = intervalMins;
        }
    }

    @JsonInclude(JsonInclude.Include.ALWAYS)
    public static class Reporting
    {
        @JsonProperty("start_time_mins")
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Integer startTimeMins;

        @JsonProperty("interval_mins")
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Integer intervalMins;

        @JsonProperty("num_retries")
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Integer retries;

        @JsonProperty("transmit_window_mins")
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Integer transmitWindowMins;

        public Integer getStartTimeMins()
        {
            return startTimeMins;
        }

        public void setStartTimeMins(Integer startTimeMins)
        {
            this.startTimeMins = startTimeMins;
        }

        public Integer getIntervalMins()
        {
            return intervalMins;
        }

        public void setIntervalMins(Integer intervalMins)
        {
            this.intervalMins = intervalMins;
        }

        public Integer getRetries()
        {
            return retries;
        }

        public void setRetries(Integer retries)
        {
            this.retries = retries;
        }

        public Integer getTransmitWindowMins()
        {
            return transmitWindowMins;
        }

        public void setTransmitWindowMins(Integer transmitWindowMins)
        {
            this.transmitWindowMins = transmitWindowMins;
        }
    }

    @JsonInclude(JsonInclude.Include.ALWAYS)
    public static class QuietTime
    {
        @JsonProperty("quiet_start_mins")
        private Integer quietStartMins;

        @JsonProperty("quiet_end_mins")
        private Integer quietEndMins;

        public Integer getQuietStartMins()
        {
            return quietStartMins;
        }

        public void setQuietStartMins(Integer quietStartMins)
        {
            this.quietStartMins = quietStartMins;
        }

        public Integer getQuietEndMins()
        {
            return quietEndMins;
        }

        public void setQuietEndMins(Integer quietEndMins)
        {
            this.quietEndMins = quietEndMins;
        }
    }
}
