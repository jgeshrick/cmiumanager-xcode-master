/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.common.data.miu.Eui;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.heard.MiuUpdateRepository;
import com.neptunetg.common.data.miu.lora.*;
import com.neptunetg.common.lora.json.ActilityForwardPacketJson;
import com.neptunetg.common.lora.json.ActilityUplinkJson;
import com.neptunetg.common.lora.json.SenetForwardPacketJson;
import com.neptunetg.common.lora.parser.LoraPacketParser;
import com.neptunetg.common.lora.pdu.BasicConfigPacket;
import com.neptunetg.common.lora.pdu.L900Packet;
import com.neptunetg.common.lora.pdu.PacketId;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.integration.data.miu.L900DataAndStatsPreparedSetter;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by WJD1 on 24/05/2016.
 * A class to receive SQS messages from MDCE-LoRa and store information on packets received from L900 devices in SQL
 */
@Service
public class L900ProcessReceivedPacketService implements AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(L900ProcessReceivedPacketService.class);

    private final MiuUpdateRepository miuUpdateRepository;

    private final LoraRepository loraRepository;

    private final AlertService alertService;

    private final LoraPacketParser loraPacketParser;

    @Autowired
    public L900ProcessReceivedPacketService(MdceIpcSubscribeService awsQueueService,
                                            MiuUpdateRepository miuUpdateRepository,
                                            AlertService alertService,
                                            LoraRepository loraRepository)
    {
        awsQueueService.register(this, MdceIpcSubscribeService.LORA_PACKETS_RECEIVED, Duration.ofSeconds(1L), 100);

        this.miuUpdateRepository = miuUpdateRepository;
        this.loraRepository = loraRepository;
        this.alertService = alertService;
        this.loraPacketParser = new LoraPacketParser();
    }

    /**
     * Handler when SQS message to the registered SQS queue name has been received.
     * @param message the message body
     * @param messageId the message id
     * @param messageReceipt message receipt
     * @param cancellationToken cancel token, ignored because this processing is quick
     * @return true to delete the message from SQS
     */
    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        log.debug("SQS message received for L900 packet");

        try
        {
            processL900Packet(message);
        }
        catch (InternalApiException e)
        {
            log.error("Error processing L900 packet info into SQL",e);
        }

        return true;
    }

    private void processL900Packet(String message) throws InternalApiException
    {
        String[] topLineAndMessage = message.split("\n", 2);

        if(topLineAndMessage.length < 2)
        {
            throw new InternalApiException("Expected header followed by JSON, only one line found");
        }

        String header = topLineAndMessage[0];
        String providerJson = topLineAndMessage[1];

        String[] splitHeader = header.split(":", 2);

        if (splitHeader.length < 2)
        {
            throw new InternalApiException("Could not split SQS message header to find LoRa provider");
        }
        final String providerName = splitHeader[1].trim();

        if (providerName.contains("Senet"))
        {
            processL900PacketSenet(providerJson);
        }
        else if (providerName.contains("Actility"))
        {
            processL900PacketActility(providerJson);
        }
        else
        {
            throw new InternalApiException("Unrecognised LoRa provider: " + splitHeader[1]);
        }
    }

    private void processL900PacketSenet(String senetJson) throws InternalApiException
    {
        final ObjectMapper mapper = new ObjectMapper();

        try
        {
            final SenetForwardPacketJson senetPacket = mapper.readValue(senetJson, SenetForwardPacketJson.class);

            final Eui eui = Eui.valueOf(senetPacket.getEui());
            final MiuId miuId = eui.extractMiuId();
            final L900DataAndStatsPreparedSetter l900PreparedSetter = new L900DataAndStatsPreparedSetter(miuId, senetPacket);

            miuUpdateRepository.updateMiuHeardAllStats(eui.extractMiuId(),
                    MiuType.L900,
                    senetPacket.getTxTimeInstant(),
                    Instant.now(),
                    l900PreparedSetter);

            LoraDeviceDetail loraDeviceDetail = loraRepository.getLoraDeviceDetailByMiuId(eui.extractMiuId());

            if(loraDeviceDetail == null)
            {
                loraDeviceDetail = new LoraDeviceDetail(eui.extractMiuId(), LoraNetwork.SENET, Instant.now(),
                        eui, null, null, null);
                loraRepository.insertOrOverwriteLoraDeviceDetail(loraDeviceDetail);
            }
            else if (loraDeviceDetail.getLoraNetwork() != LoraNetwork.SENET)
            {
                final AlertSource alertSource = AlertSource.l900NetworkMismatch(miuId.numericValue());
                final AlertDetails alertDetails = new AlertDetails();
                alertDetails.setAlertLevel(AlertLevel.ERROR);
                alertDetails.setAlertState(AlertState.NEW);
                alertDetails.setAlertSource(alertSource.getAlertId());
                alertDetails.setRecentMessage("L900: " + miuId.toString() + " called in on network (SENET) different " +
                        "to previous network: " + loraDeviceDetail.getLoraNetwork().toString());
                alertDetails.setAlertCreationDate(ZonedDateTime.now());
                alertService.addAlertAndSendEmail(alertDetails);
            }

            if(!loraDeviceDetail.getEui().equals(eui))
            {
                final AlertSource alertSource = AlertSource.l900EuiMismatch(miuId.numericValue());
                final AlertDetails alertDetails = new AlertDetails();
                alertDetails.setAlertLevel(AlertLevel.ERROR);
                alertDetails.setAlertState(AlertState.NEW);
                alertDetails.setAlertSource(alertSource.toString());
                alertDetails.setRecentMessage("L900: " + miuId.toString() + " called in with EUI (" + eui.toString() +
                        ") different to previous EUI: " + loraDeviceDetail.getEui().toString());
                alertDetails.setAlertCreationDate(ZonedDateTime.now());
                alertService.addAlertAndSendEmail(alertDetails);
            }

            processL900Pdu(senetPacket.getPduBytes(), loraDeviceDetail);
        }
        catch (IOException e)
        {
            log.error("Could not parse JSON from Senet", e);
        }
    }

    private void processL900PacketActility(String actilityJson) throws InternalApiException
    {
        final ObjectMapper mapper = new ObjectMapper();

        try
        {
            final ActilityForwardPacketJson actilityPacketFull = mapper.readValue(actilityJson, ActilityForwardPacketJson.class);
            final ActilityUplinkJson actilityPacket = actilityPacketFull.getDevEuiUplink();

            final Eui eui = Eui.valueOf(actilityPacket.getEui());
            final MiuId miuId = eui.extractMiuId();
            final L900DataAndStatsPreparedSetter l900PreparedSetter = new L900DataAndStatsPreparedSetter(miuId, actilityPacket);

            miuUpdateRepository.updateMiuHeardAllStats(eui.extractMiuId(),
                    MiuType.L900,
                    actilityPacket.getTxTimeInstant(),
                    Instant.now(),
                    l900PreparedSetter);

            LoraDeviceDetail loraDeviceDetail = loraRepository.getLoraDeviceDetailByMiuId(eui.extractMiuId());

            if(loraDeviceDetail == null)
            {
                loraDeviceDetail = new LoraDeviceDetail(eui.extractMiuId(), LoraNetwork.ACTILITY, Instant.now(),
                        eui, null, null, null);
                loraRepository.insertOrOverwriteLoraDeviceDetail(loraDeviceDetail);
            }
            else if (loraDeviceDetail.getLoraNetwork() != LoraNetwork.ACTILITY)
            {
                final AlertSource alertSource = AlertSource.l900NetworkMismatch(miuId.numericValue());
                final AlertDetails alertDetails = new AlertDetails();
                alertDetails.setAlertLevel(AlertLevel.ERROR);
                alertDetails.setAlertState(AlertState.NEW);
                alertDetails.setAlertSource(alertSource.getAlertId());
                alertDetails.setRecentMessage("L900: " + miuId.toString() + " called in on network (Actility) different " +
                        "to previous network: " + loraDeviceDetail.getLoraNetwork().toString());
                alertDetails.setAlertCreationDate(ZonedDateTime.now());
                alertService.addAlertAndSendEmail(alertDetails);
            }

            if(!loraDeviceDetail.getEui().equals(eui))
            {
                final AlertSource alertSource = AlertSource.l900EuiMismatch(miuId.numericValue());
                AlertDetails alertDetails = new AlertDetails();
                alertDetails.setAlertLevel(AlertLevel.ERROR);
                alertDetails.setAlertState(AlertState.NEW);
                alertDetails.setAlertSource(alertSource.getAlertId());
                alertDetails.setRecentMessage("L900: " + miuId.toString() + " called in with EUI (" + eui.toString() +
                        ") different to previous EUI: " + loraDeviceDetail.getEui().toString());
                alertDetails.setAlertCreationDate(ZonedDateTime.now());
                alertService.addAlertAndSendEmail(alertDetails);
            }

            processL900Pdu(actilityPacket.getPduBytes(), loraDeviceDetail);
        }
        catch (IOException e)
        {
            log.error("Could not parse JSON from Actility", e);
        }
    }

    private void processL900Pdu(byte[] pdu, LoraDeviceDetail currentLoraDeviceDetails)
    {
        L900Packet packet = loraPacketParser.parsePacket(pdu);

        if(packet.getPacketId() == PacketId.BASIC_CONFIG_PACKET)
        {
            BasicConfigPacket basicConfigPacket = (BasicConfigPacket) packet;
            LoraAntennaType antennaTypeEnum = LoraAntennaType.fromIntValue(basicConfigPacket.getAntennaType());

            if(!antennaTypeEnum.equals(currentLoraDeviceDetails.getLoraAntennaType()))
            {
                currentLoraDeviceDetails.setLoraAntennaType(antennaTypeEnum);
                loraRepository.insertOrOverwriteLoraDeviceDetail(currentLoraDeviceDetails);
            }
        }
    }
}
