/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.utility;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility to derive useful information from the s3 image repository
 */
public class CmiuImageDistributionUtility
{
    private static final Logger log = LoggerFactory.getLogger(CmiuImageDistributionUtility.class);

    //the suffix key defined in the memory map.txt for start address.
    private static final String START_ADDRESS_KEY = "_START_ADDRESS";

    //the suffix key defined in the memory map.txt for image size
    private static final String IMAGE_SIZE_KEY = "_SIZE";

    // Regex pattern to capture text between final two slashes
    private static final String s3ImageFolderPattern = "([^/]*)/(?:[^/]*)$";

    /**
     * From the s3 key, get image type by parsing from the content of the key
     *
     * @param s3Key s3 key
     * @return image type
     */
    public static ImageDescription.ImageType s3ImageFolderToImageType(String s3Key)
    {
        final Pattern p = Pattern.compile(s3ImageFolderPattern);

        Matcher m = p.matcher(s3Key);

        if (m.find() && m.groupCount() == 1)
        {
            final String group = m.group(1);

            return ImageDescription.ImageType.fromS3FolderName(group);
        }

        return null;
    }

    /**
     * Derive the s3 key to the memory map file from the s3 key of the image file
     *
     * @param imageS3Key s3 key to the image file
     * @return s3 key to the memory map file
     */
    public static String getMemoryMapPropertyKey(String imageS3Key)
    {
        return imageS3Key.replaceAll("(?i)\\.bin$", "_MemoryMap.properties");
    }

    /**
     * Construct a download url based on the relative filepath and supplied url.
     *
     * @param fileName the relative filepath as stored in RDS.
     * @param url      the URL where the file download server is exposed to CMIU
     * @return the fully qualified url
     */
    public static String filenameToDownloadUrl(String fileName, String url)
    {
        if (!(url.startsWith("http://") || url.startsWith("https://")))
        {
            url = "http://" + url;
        }

        if (!url.endsWith("/"))
        {
            url += "/";
        }

        return url + fileName;
    }

    /**
     * Extract version information from s3 filename
     *
     * @param s3Key s3 key to the image file
     * @return version information derived from the file string
     */
    public static String getVersionInformation(String s3Key)
    {
        final Pattern pattern = Pattern.compile(".+_v(.+)\\.bin");
        Matcher matcher = pattern.matcher(s3Key);

        String result = "";
        if (matcher.find())
        {
            result = matcher.group(1);
        }

        return result;
    }

    /**
     * Extract filename information from s3 filename
     *
     * @param s3Key string in format xxxxxxx/filename_vxxxxxxxx
     * @return
     */
    public static String getFileName(String s3Key)
    {
        final Pattern pattern = Pattern.compile("/([A-Za-z]+)([A-Za-z_]+)v[.\\d]+\\.bin");
        Matcher matcher = pattern.matcher(s3Key);

        String result = "";
        if (matcher.find())
        {
            result = matcher.group(1);
        }

        return result;
    }

    /**
     * Get start address from image type based on http://gontg/ntgprojects/10065-1/Project%20Documents/CMIU/Firmware%20and%20Software%20design/CMIU_Memory_Organization.xlsx
     *
     * @param imageType image type
     * @return start address
     */
    public static int getStartAddressFromImageType(ImageDescription.ImageType imageType, Properties memoryMapProperties) throws InternalApiException
    {
        String key = ImageMetaDictionary.fromImageType(imageType).getMemoryMapKey() + START_ADDRESS_KEY;

        return getValueFromPropertyStream(memoryMapProperties, key);

    }

    /**
     * Get statically defined start address
     *
     * @throws InternalApiException
     */
    public static int getDefaultStartAddressFromImageType(ImageDescription.ImageType imageType) throws InternalApiException
    {
        return ImageMetaDictionary.fromImageType(imageType).getStartAddress();
    }

    /**
     * Get image size from type based on http://gontg/ntgprojects/10065-1/Project%20Documents/CMIU/Firmware%20and%20Software%20design/CMIU_Memory_Organization.xlsx
     *
     * @return image size in bytes
     */
    public static int getImageSizeFromImageType(ImageDescription.ImageType imageType, Properties memoryMapProperties) throws InternalApiException
    {
        String key = ImageMetaDictionary.fromImageType(imageType).getMemoryMapKey() + IMAGE_SIZE_KEY;

        return getValueFromPropertyStream(memoryMapProperties, key);
    }

    /**
     * Get statically defined image size
     */
    public static int getDefaultImageSizeFromImageType(ImageDescription.ImageType imageType) throws InternalApiException
    {
        return ImageMetaDictionary.fromImageType(imageType).getSize();  //return default
    }


    /**
     * Given an inputstream for a property file, get the value of a key-value pair
     *
     * @param memoryMapProperties    memory map loaded from s3 input stream
     * @param key property key
     * @return value of the property
     */
    private static Integer getValueFromPropertyStream(final Properties memoryMapProperties, final String key) throws InternalApiException
    {
        String value = memoryMapProperties.getProperty(key);

        if (value == null)
        {
            throw new InternalApiException("Cannot get value from key");
        }

        return Integer.parseInt(value.replaceAll("^0[xX]", ""), 16); //remove 0x or 0X from start of string, if present
    }

    public static Properties getMemoryMap(InputStream is)
    {
        try
        {
            Properties memoryMapProperty = new Properties();
            memoryMapProperty.load(is);
            return memoryMapProperty;
        }
        catch (IOException e)
        {
            log.warn("Exception thrown loading memory map ", e);
            return null;
        }
    }

    /**
     * Enum for pointing to default image size and start address values and key to the memory map key value pair.
     */
    private enum ImageMetaDictionary
    {
        Firmware("APPLICATION", ImageDescription.ImageType.FirmwareImage, 0x8000, 229376),
        Config("CONFIGURATION", ImageDescription.ImageType.ConfigImage, 0x6800, 0x800),
        Telit("TELIT", ImageDescription.ImageType.TelitModuleImage, 0, 0),    //fixme
        BleConfig("BLE_CONFIG", ImageDescription.ImageType.BleConfigImage, 0x7800, 0x800),
        ArbConfig("ARB", ImageDescription.ImageType.ArbConfigImage, 0, 0),    //fixme
        Encryption("KEY_TABLE", ImageDescription.ImageType.EncryptionImage, 0x7000, 0x800),
        Bootloader("BOOTLOADER", ImageDescription.ImageType.BootloaderImage, 0, 0x6800);

        private final ImageDescription.ImageType imageType;
        private final int startAddress;
        private final int size;
        private final String memoryMapKey;

        private ImageMetaDictionary(String memoryMapKey, ImageDescription.ImageType imageType, int startAddress, int size)
        {
            this.memoryMapKey = memoryMapKey;
            this.startAddress = startAddress;
            this.imageType = imageType;
            this.size = size;
        }

        public static ImageMetaDictionary fromImageType(ImageDescription.ImageType imageType) throws InternalApiException
        {
            for (ImageMetaDictionary imageMetaDictionary : ImageMetaDictionary.values())
            {
                if (imageMetaDictionary.getImageType() == imageType)
                {
                    return imageMetaDictionary;
                }
            }

            throw new InternalApiException("Unmapped image type");
        }

        public String getMemoryMapKey()
        {
            return memoryMapKey;
        }

        public int getStartAddress()
        {
            return startAddress;
        }

        public int getSize()
        {
            return size;
        }

        public ImageDescription.ImageType getImageType()
        {
            return imageType;
        }
    }

}
