/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.common.internal.site.model.SiteDetails;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.domain.alert.AlertEmail;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import com.neptunetg.mdce.integration.scheduled.site.alert.service.SiteAlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by ibarron on 7/18/17.
 * Check that site has communication
 */
@Service
public class SiteCommunicationChecker implements AwsQueueReceiver
{

    private static final Logger log = LoggerFactory.getLogger(SiteCommunicationChecker.class);

    private final AlertService alertService;

    private final SiteAlertService siteAlertService;

    @Value("#{envProperties['alert.site.connection.check.hours']}")
    private int siteConnectionCheckHours;

    @Value("#{envProperties['alert.site.communication.site.connection.ignore.after.hours']}")
    private int siteConnectionIgnoreAfterHours;

    @Autowired
    public SiteCommunicationChecker(MdceIpcSubscribeService awsQueueService, AlertService alertService, SiteAlertService siteAlertService)
    {
        this.alertService = alertService;
        this.siteAlertService = siteAlertService;
        awsQueueService.register(this, MdceIpcSubscribeService.CHECK_SITE_COMMUNICATION, Duration.ofSeconds(15L), 1);
    }

    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        log.debug("SQS message received to check site states");

        try
        {
           raiseAlertForSitesNotCommunicatingWithinXHours(this.siteConnectionCheckHours, this.siteConnectionIgnoreAfterHours);
        }
        catch (InternalApiException e)
        {
            log.error("Error checking site connections", e);
        }

        return true;
    }


    private void raiseAlertForSitesNotCommunicatingWithinXHours(int siteConnectionCheckHours, int siteConnectionIgnoreAfterHours) throws InternalApiException
    {
        final List<SiteDetails> sitesDetailsForAlerts =
                this.siteAlertService.getSitesNotRequestingDataWithinXHours(siteConnectionCheckHours, siteConnectionIgnoreAfterHours);

        final AlertEmail email = alertService.initAlertEmail();

        for(SiteDetails siteDetail : sitesDetailsForAlerts)
        {
            final SiteId siteId = new SiteId(siteDetail.getSiteId());
            final AlertSource alertSource = AlertSource.siteConnectionIssue(siteId.numericWrapperValue());

            AlertDetails alertDetails = new AlertDetails();
            alertDetails.setAlertLevel(AlertLevel.ERROR);
            alertDetails.setAlertState(AlertState.NEW);
            alertDetails.setAlertSource(alertSource.getAlertId());
            alertDetails.setRecentMessage("Site "
                    + siteId.numericValue()
                    + " has not requested data for "
                    + getAmountOfTimeSiteHasNotCommunicatedHours(siteDetail)
                    + " hours");
            alertDetails.setAlertCreationDate(ZonedDateTime.now());
            alertService.addAlert(alertDetails, email);
        }
        alertService.sendEmail(email);
    }

    private long getAmountOfTimeSiteHasNotCommunicatedHours(SiteDetails siteDetails) throws InternalApiException
    {
        try
        {
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    .parse(siteDetails.getLastHeardDate());

            Duration communicationDownTime = Duration.between(date.toInstant(), Instant.now());
            return communicationDownTime.toHours();
        }
        catch (ParseException e)
        {
            throw new InternalApiException("Cant Parse Date", e);
        }
    }
}
