/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.common.data.miu.command.MiuCommandDetail;

import java.util.List;

/**
 * Wraps MiuCommandRepository and provides audit logging
 */
public interface MiuCommandService
{

    /**
     * Get a list of command performed on a cmiu
     * @param miuId the miu to retrieve histroy
     * @return list of command performed on the miu
     */
    List<MiuCommandDetail> getCommandHistory(MiuId miuId);

    /**
     * Get a list of entry in the miu command table where the state has been created or queued, but not ack/nack by cmiu
     * @return miu command list
     */
    List<MiuCommandDetail> getInProgressCommand();

    /**
     * Get a list of entry in the miu command table where the state has been ACKed but not confirmed by a the correct
     * firmware version in a detailed config packet
     * @return miu command list
     */
    List<MiuCommandDetail> getReceivedCommand();

    /**
     * Get a list of completed commands limited to a number of days ago
     * @param numberOfDaysAgo list only commands up to the specified days ago
     * @return list of commands
     */
    List<MiuCommandDetail> getCompletedCommand(int numberOfDaysAgo);

    /**
     * Get a list of entry in the miu command table where the state has been marked as rejected
     * @return miu command list
     */
    List<MiuCommandDetail> getRejectedCommand(int numberOfDaysAgo);

    /**
     * Add a new entry to the miu command table and write to the audit log
     * @param command the new command
     * @return number of rows updated, expect 1
     */
    int createMiuCommand(MiuCommand command);

    /**
     * Set commands issued for a miu which has not been acknowledged (i.e. state created or queued) to state recalled
     * @param miuId the miu to apply the command state change
     * @return number of rows changed
     */
    int recallCreatedAndQueuedMiuCommand(MiuId miuId);

    /**
     * Set command listed as Accepted by an MIU as Rejected
     * @param commandId the command to apply the command state change
     * @return number of rows changed
     */
    int rejectAcceptedMiuCommand(long commandId);

}
