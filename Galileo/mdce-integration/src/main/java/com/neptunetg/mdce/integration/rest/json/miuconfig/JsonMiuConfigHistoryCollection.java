/*
 *  Neptune Technology Group
 *  Copyright 2017 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.miuconfig;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neptunetg.mdce.common.internal.miu.model.CmiuConfigHistory;

import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
    JSON object for return multiple MIUs' configuration.
*/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonMiuConfigHistoryCollection
{
    @JsonProperty("mius")
    private List<JsonMiuConfigHistory> miuConfigHistoryList;

    public List<JsonMiuConfigHistory> getMiuConfigHistoryList()
    {
        return miuConfigHistoryList;
    }

    public void setMiuConfigHistoryList(List<JsonMiuConfigHistory> miuConfigHistoryList)
    {
        this.miuConfigHistoryList = miuConfigHistoryList;
    }

    /**
     * Given a 1D list of CmiuConfigHistory containing 1 or more CMIU config history, organised into Json response
     * for get configuration history
     * @param configHistoryList config history list for 1 or more CMIU
     * @param distributerId
     * @return JSON format corresponding to IF18
     */
    public static JsonMiuConfigHistoryCollection from(List<CmiuConfigHistory> configHistoryList, Optional<String> distributerId, ZoneId timezone)
    {
        JsonMiuConfigHistoryCollection collection = new JsonMiuConfigHistoryCollection();

        //group config history list by CMIU first
        TreeMap<Integer, List<CmiuConfigHistory>> configHistoryMapByCmiu = configHistoryList
                .stream()
                .collect(Collectors.groupingBy(CmiuConfigHistory::getMiuId,
                        TreeMap::new,
                        Collectors.toList()));

        //process each cmiu group config history list to get JsonMiuConfigHistory respectively
        List<JsonMiuConfigHistory> jsonMiuConfigHistoryListByCmiu = configHistoryMapByCmiu
                .entrySet()
                .stream()
                .map(c -> JsonMiuConfigHistory.from(c.getValue(), c.getKey(), distributerId, timezone))
                .collect(Collectors.toList());

        collection.setMiuConfigHistoryList(jsonMiuConfigHistoryListByCmiu);

        return collection;
    }

}
