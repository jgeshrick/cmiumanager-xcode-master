/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.rest;

import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.common.data.miu.lifecycle.model.HistoricMiuLifecycleState;
import com.neptunetg.common.data.miu.lifecycle.model.AddMiuLifecycleStateResult;
import com.neptunetg.common.service.miu.lifecycle.MiuLifecycleService;
import com.neptunetg.mdce.common.api.rest.exception.BadRequestException;
import com.neptunetg.mdce.common.api.rest.exception.NotAuthorizedException;
import com.neptunetg.mdce.integration.data.miu.MiuSearchRepository;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.rest.json.JsonErrorResponse;
import com.neptunetg.mdce.integration.rest.json.miulifecycle.JsonHistoricMiuLifecycleState;
import com.neptunetg.mdce.integration.rest.json.miulifecycle.JsonSequentialMiuLifecycleState;
import com.neptunetg.mdce.integration.rest.json.miulifecycle.JsonSetMiuLifecycleHistoryRequest;
import com.neptunetg.mdce.integration.rest.json.miulifecycle.JsonSetMiuLifecycleHistoryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * IF46 Used by external systems to retrieve configuration history about an MIU or list of MIUs.
 */
@RestController
public class MiuLifecycleHistoryController extends BaseTokenAuthenticatingRestController
{
    private MiuLifecycleService miuLifecycleService;

    private MiuSearchRepository miuSearchRepository;

    @Autowired
    private MdceIpcPublishService awsQueueService;

    @Autowired
    public MiuLifecycleHistoryController(MiuLifecycleService miuLifecycleService,
                                         MiuSearchRepository miuSearchRepository)
    {
        this.miuLifecycleService = miuLifecycleService;
        this.miuSearchRepository = miuSearchRepository;
    }

    /**
     * Returns a page of lifecycle history entries for all MIUs starting from the supplied sequence ID.
     * @param lastSequenceId Optional parameter for the last sequence ID known to the client.
     * @throws NotAuthorizedException Thrown when the caller's siteId is not 0.
     * @throws BadRequestException
     */
    @RequestMapping(value = "/mdce/api/v1/miu/lifecycle_history", method = RequestMethod.GET)
    @ResponseBody
    public List<JsonSequentialMiuLifecycleState> getMiuLifecycleHistoryBySite(
            @RequestParam(value = "last_sequence", required = false) Integer lastSequenceId,
            HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException
    {
        final SiteId siteId = getClientSiteId(request);
        if (siteId.doesNotMatch(0))
        {
            throw new NotAuthorizedException("This API is not available to the current site ID.");
        }

        final List<MiuLifecycleState> states = this.miuLifecycleService.getMiuLifecycleHistory(lastSequenceId);

        final List<JsonSequentialMiuLifecycleState> result = states
                .stream()
                .map(state -> new JsonSequentialMiuLifecycleState(state))
                .collect(Collectors.toList());

        return result;
    }

    @RequestMapping(value = "/mdce/api/v1/miu/lifecycle_history", method = RequestMethod.POST)
    @ResponseBody
    public JsonSetMiuLifecycleHistoryResult addMiuLifecycleHistory(
            @RequestBody JsonSetMiuLifecycleHistoryRequest jsonRequest,
            HttpServletRequest request)
            throws NotAuthorizedException, BadRequestException
    {
        final SiteId siteId = getClientSiteId(request);
        if (siteId.doesNotMatch(0))
        {
            throw new NotAuthorizedException("This API is not available to the current site ID.");
        }

        // Identify any unrecognized lifecycle states
        List<JsonErrorResponse> jsonErrors = jsonRequest.getLifecycleStates()
                .stream()
                .filter(s -> MiuLifecycleStateEnum.fromStringValue(s.getLifecycleState()) == null)
                .map(s -> jsonErrorResponseFromJsonModel(s, "Invalid lifecycle state."))
                .collect(Collectors.toList());

        List<HistoricMiuLifecycleState> validatedLifecycleStates = jsonRequest.getLifecycleStates()
                .stream()
                .filter(s -> MiuLifecycleStateEnum.fromStringValue(s.getLifecycleState()) != null)
                .map(JsonHistoricMiuLifecycleState::toServiceModel)
                .collect(Collectors.toList());

        // Send validated entries to the MiuLifecycleService
        List<AddMiuLifecycleStateResult> serviceResult;

        if (validatedLifecycleStates.size() != 0)
        {
            for (HistoricMiuLifecycleState miu : validatedLifecycleStates)
            {
                if(!miuLifecycleService.checkMiuBpcLifecycleTransition(miu.getMiuId(), miu.getLifecycleState()))
                {
                    //Raise alert
                    awsQueueService.sendMessage(MdceIpcPublishService.CHECK_CMIU_LIFECYCLE_STATE, miu.getMiuId().toString()+ "," + miu.getLifecycleState().toString());
                }
            }
            serviceResult = miuLifecycleService.addMiuLifecycleStates(validatedLifecycleStates);
        }
        else
        {
            serviceResult = new ArrayList<>();
        }

        // Add any service-side failures to the error list
        serviceResult
                .stream()
                .filter(s -> !s.isOk())
                .map(MiuLifecycleHistoryController::jsonErrorResponseFromServiceModel)
                .forEach(e -> jsonErrors.add(e));

        int okCount = (int)serviceResult.stream().filter(r -> r.isOk()).count();

        JsonSetMiuLifecycleHistoryResult jsonResult = new JsonSetMiuLifecycleHistoryResult();
        jsonResult.setErrorDetails(jsonErrors);
        jsonResult.setErrorCount(jsonErrors.size());
        jsonResult.setSucceededCount(okCount);

        return jsonResult;
    }

    private static JsonErrorResponse jsonErrorResponseFromJsonModel(JsonHistoricMiuLifecycleState jsonState, String error)
    {
        JsonErrorResponse errorResponse = new JsonErrorResponse();
        errorResponse.setReferenceId(jsonState.getReferenceId());
        errorResponse.setError(error);

        return errorResponse;
    }

    private static JsonErrorResponse jsonErrorResponseFromServiceModel(AddMiuLifecycleStateResult serviceResult)
    {
        JsonErrorResponse errorResponse = new JsonErrorResponse();
        errorResponse.setReferenceId(serviceResult.getReferenceId());
        errorResponse.setError(serviceResult.getError());

        return errorResponse;
    }

}
