/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.scheduled.r900.service;


import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import com.neptunetg.mdce.integration.scheduled.r900.AsyncFileTask;
import com.neptunetg.mdce.integration.scheduled.r900.FileProcessor;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.concurrent.Future;

/**
 * Asynchoronously handles the task of processing a received R900 gateway data file, as detailed in ETI 48-02
 * Creates lock file and calls Handler service
 * On return the file is moved to the relevant folder: error or processed, and the lock file is deleted
 * Returns a Future-wrapped file
 */
@Service
public class R900TarAsyncTaskManagerService extends BaseR900TarService implements AsyncFileTask
{

    private static final Logger logger = LoggerFactory.getLogger(R900TarAsyncTaskManagerService.class);

    private static final String FAILED = ".failed";

    private final FileProcessor fileProcessor;

    @Autowired
    public R900TarAsyncTaskManagerService(FileProcessor fileProcessor,
                                          @Value("#{envProperties['gateway.data.dump.folder']?:'/'}") String tarballDumpFolder)
    {
        super(tarballDumpFolder);
        this.fileProcessor = fileProcessor;
    }

    @Override
    @Async("r900executor")
    // r900executor configured, including size of threadpool in servlet-context.xml
    public Future<File> process(File fileToProcess)
    {
        logger.info("Starting to process " + fileToProcess + " in new thread " + Thread.currentThread().getName());
        final File file = lockFileAndProcess(fileToProcess);

        return new AsyncResult<>(file);
    }

    private File lockFileAndProcess(File fileToProcess)
    {
        final File lockFile = createLockFile(fileToProcess);

        try
        {
            return processLockedFileMovingOnCompletion(fileToProcess);
        } finally
        {
            lockFile.delete();
        }
    }

    private File processLockedFileMovingOnCompletion(File fileToProcess)
    {
        try
        {
            final File processedFileTarget = processFile(fileToProcess);

            FileUtils.moveFile(fileToProcess, ensureFileDoesNotExist(processedFileTarget));

            return processedFileTarget;
        } catch (Exception e)
        {
            moveFileToErrorFolder(fileToProcess);
            throw new MdceRestException("Error caught processing file " + fileToProcess.getName() + " moved to error folder", e);
        }
    }

    private void moveFileToErrorFolder(File fileToProcess)
    {
        try
        {
            final File failureFileTarget = new File(ensureDirectoryExists(errorFolder), fileToProcess.getName());

            FileUtils.moveFile(fileToProcess, ensureFileDoesNotExist(failureFileTarget));
        } catch (Exception e)
        {
            logger.error("*** Error caught moving failed tar file to ERROR folder " + fileToProcess.getName() + " moved to error folder ***", e);
            renameErroredFileAsLastResort(fileToProcess);
            //If this doesn't work what do we do????
            throw new MdceRestException("Error caught moving file to ERROR folder " + fileToProcess.getName() + " moved to error folder", e);
        }
    }

    private void renameErroredFileAsLastResort(File fileToProcess)
    {
        try
        {
            FileUtils.moveFile(fileToProcess, new File(fileToProcess.getParent(), fileToProcess.getName() + FAILED));
        } catch (Exception e)
        {
            logger.error("*** Couldn't change its name at all " + fileToProcess.getName(), e);
        }
    }

    private File processFile(File fileToProcess)
    {
        final File processedFileTarget = new File(ensureDirectoryExists(processedFolder), fileToProcess.getName());

        fileProcessor.process(fileToProcess);
        return processedFileTarget;
    }

}