/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2017 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain;

import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.rest.json.JsonMiuActiveFlag;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by WJD1 on 18/12/2015.
 * Class to assist in performing a batch update for owner of MIU (site ID)
 * The batch insert is necessary for performance
 * Example SQL this works with:
 *
 * UPDATE mdce.miu_details SET site_id = ?, meter_active = ? WHERE miu_id = ?
 *
 */
public class UpdateMiuSiteIdBpss implements BatchPreparedStatementSetter
{
    private final SiteId siteId;
    private final String[] meterActive;
    private final long[] miuId;

    private int batchSize = 0;

    public UpdateMiuSiteIdBpss(SiteId siteId, int maxBatchSize)
    {
        this.siteId = siteId;
        this.meterActive = new String[maxBatchSize];
        this.miuId = new long[maxBatchSize];

    }

    public void addMiu(JsonMiuActiveFlag miuJson)
    {
        meterActive[batchSize] = miuJson.getActive();
        miuId[batchSize] = miuJson.getMiuId();

        batchSize++;
    }

    public void clearBatch()
    {
        batchSize = 0;
    }

    @Override
    public int getBatchSize()
    {
        return batchSize;
    }

    @Override
    public void setValues(PreparedStatement preparedStatement, int i) throws SQLException
    {
        preparedStatement.setInt(1, siteId.numericValue());
        preparedStatement.setString(2, meterActive[i]);
        preparedStatement.setLong(3, miuId[i]);
    }

    public SiteId getSiteId()
    {
        return this.siteId;
    }

    public long[] getMiuIds()
    {
        return this.miuId;
    }
}
