/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2017 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.data.miu;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.mdce.integration.data.site.SiteId;

import java.io.Serializable;
import java.time.Instant;

public class MiuDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    private final MiuId id;
    private final SiteId siteId;
    private final Instant lastHeardTime;
    private final Instant lastInsertDate; //TODO: this is redundant as always identical to lastHeardTime
    private final Instant firstInsertDate;
    private final MiuType type;
    private final Boolean meterActive;
    private Integer recordingIntervalMinutes;
    private Integer reportingIntervalMinutes;
    private String cmiuModeName;
    private String reportedCmiuModeName;
    private String networkProvider;
    private String iccid;
    private String imei;
    private String eui;
    private String msisdn;
    private MiuLifecycleStateEnum lifecycleState;

    public MiuDetails(MiuId id,
                      SiteId siteId,
                      Instant lastHeardTime,
                      Instant lastInsertDate,
                      Instant firstInsertDate,
                      MiuType type,
                      Boolean meterActive)
    {
        super();
        this.id = id;
        this.siteId = siteId;
        this.lastHeardTime = lastHeardTime;
        this.lastInsertDate = lastInsertDate;
        this.firstInsertDate = firstInsertDate;
        this.type = type;
        this.meterActive = meterActive;
        this.recordingIntervalMinutes = null;
        this.reportingIntervalMinutes = null;
        this.cmiuModeName = null;
        this.reportedCmiuModeName = null;
        this.msisdn = null;
    }

    public MiuDetails(MiuId id,
                      SiteId siteId,
                      Instant lastHeardTime,
                      Instant lastInsertDate,
                      Instant firstInsertDate,
                      MiuType type,
                      Boolean meterActive,
                      Integer recordingIntervalMinutes,
                      Integer reportingIntervalMinutes,
                      String cmiuModeName,
                      String reportedCmiuModeName,
                      String networkProvider,
                      String iccid,
                      String imei,
                      String eui,
                      String msisdn,
                      MiuLifecycleStateEnum lifecycleState)
    {
        super();
        this.id = id;
        this.siteId = siteId;
        this.lastHeardTime = lastHeardTime;
        this.lastInsertDate = lastInsertDate;
        this.firstInsertDate = firstInsertDate;
        this.type = type;
        this.meterActive = meterActive;
        this.recordingIntervalMinutes = recordingIntervalMinutes;
        this.reportingIntervalMinutes = reportingIntervalMinutes;
        this.cmiuModeName = cmiuModeName;
        this.reportedCmiuModeName = reportedCmiuModeName;
        this.networkProvider = networkProvider;
        this.iccid = iccid;
        this.imei = imei;
        this.eui = eui;
        this.msisdn = msisdn;
        this.lifecycleState = lifecycleState;
    }

    public MiuLifecycleStateEnum getLifecycleState() {
        return lifecycleState;
    }

    public void setLifecycleState(MiuLifecycleStateEnum lifecycleState) {
        this.lifecycleState = lifecycleState;
    }

    @JsonSerialize(converter = MiuId.ToStringConverter.class)
    public MiuId getId() {
        return id;
    }

    public Instant getLastHeardTime()
    {
        return lastHeardTime;
    }

    public Instant getLastInsertDate() {
        return lastInsertDate;
    }
    public Instant getFirstInsertDate() {
        return firstInsertDate;
    }

    @JsonSerialize(converter=SiteId.ToIntConverter.class)
    public SiteId getSiteId()
    {
        return siteId;
    }

    public MiuType getType()
    {
        return type;
    }

    public String getMeterActive()
    {
        if (meterActive == null)
        {
            return "";
        }
        return meterActive ? "Y" : "N";
    }

    public Integer getRecordingIntervalMinutes()
    {
        return recordingIntervalMinutes;
    }

    public void setRecordingIntervalMinutes(Integer recordingIntervalMinutes)
    {
        this.recordingIntervalMinutes = recordingIntervalMinutes;
    }

    public Integer getReportingIntervalMinutes()
    {
        return reportingIntervalMinutes;
    }

    public void setReportingIntervalMinutes(Integer reportingIntervalMinutes)
    {
        this.reportingIntervalMinutes = reportingIntervalMinutes;
    }

    public String getCmiuModeName()
    {
        return cmiuModeName;
    }

    public void setCmiuModeName(String cmiuModeName)
    {
        this.cmiuModeName = cmiuModeName;
    }

    public String getReportedCmiuModeName()
    {
        return reportedCmiuModeName;
    }

    public void setReportedCmiuModeName(String reportedCmiuModeName)
    {
        this.reportedCmiuModeName = reportedCmiuModeName;
    }

    public String getNetworkProvider()
    {
        return networkProvider;
    }

    public void setNetworkProvider(String networkProvider)
    {
        this.networkProvider = networkProvider;
    }

    public String getIccid()
    {
        return iccid;
    }

    public void setIccid(String iccid)
    {
        this.iccid = iccid;
    }

    public String getImei()
    {
        return imei;
    }

    public void setImei(String imei)
    {
        this.imei = imei;
    }

    public String getEui()
    {
        return eui;
    }

    public void setEui(String eui)
    {
        this.eui = eui;
    }

    public String getMsisdn()
    {
        return msisdn;
    }

    public void setMsisdn(String msisdn)
    {
        this.msisdn = msisdn;
    }
}