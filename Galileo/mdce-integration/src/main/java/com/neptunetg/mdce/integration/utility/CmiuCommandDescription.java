/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.utility;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.cellular.CmiuRepository;
import com.neptunetg.common.data.miu.cellular.CmiuRevisionSet;
import com.neptunetg.common.data.miu.command.MiuCommand;
import com.neptunetg.common.data.miu.command.MiuCommandRepository;
import com.neptunetg.common.packet.model.taggeddata.tags.RecordingAndReportingIntervalData;
import com.neptunetg.mdce.common.internal.miu.model.imageupdate.ImageDescription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.neptunetg.common.packet.model.CmdId;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.CommandData;
import com.neptunetg.common.packet.model.taggeddata.tags.SecureBlockArrayData;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.PacketParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Utility for generating description from command param bytes
 */
@Component
public class CmiuCommandDescription implements CommandDescriptionService
{
    private static final Logger log = LoggerFactory.getLogger(CmiuCommandDescription.class);

    @Autowired
    private CmiuRepository cmiuRepository;

    @Autowired
    private MiuCommandRepository miuCommandRepository;

    /**
     * Inspect command packet and generate a description of the command for human consumption.
     *
     * @param command command packet
     * @return A summary of the command for display purpose.
     */
    public String getCommandDetails(MiuCommand command)
    {
        String description = "Unknown";
        final MiuId commandMiuId = command.getMiuId();

        try
        {
            TaggedPacket tp = PacketParser.parseTaggedPacket(command.getCommandParams());

            //check this is a command config packet
            if (tp.getPacketType() == TaggedDataPacketType.CommandConfiguration)
            {
                //get tags in encrypted data
                List<TaggedData> tpList = PacketParser.parseNonUniqueSecureTag(tp.findTag(SecureBlockArrayData.class));

                CommandData commandTag = (CommandData) getTagWithId(tpList, TagId.Command);

                long commandId = commandTag.getCommandId();

                //image udpate
                if (commandId == CmdId.UpdateImage.getId())
                {
                    // Get past version information
                    CmiuRevisionSet revisionSet = cmiuRepository.getRevisionSetAtTime(commandMiuId, command.getCommandEnteredTime());

                    if( revisionSet != null )
                    {
                        //get filename and version number
                        description = tpList.stream()
                                .filter(taggedData -> taggedData.getTagId() == TagId.Image)
                                .map(taggedData -> createImageUpdateTaggedDataSummary(taggedData, revisionSet))
                                .collect(Collectors.joining("\n"));
                    }
                    else
                    {
                        //get filename and version number
                        description = tpList.stream()
                                .filter(taggedData -> taggedData.getTagId() == TagId.Image)
                                .map(taggedData -> createImageUpdateTaggedDataSummary(taggedData))
                                .collect(Collectors.joining("\n"));

                    }


                }
                else if (commandId == CmdId.RebootCmiu.getId() ||
                        commandId == CmdId.MapSwipeEmulation.getId() ||
                        commandId == CmdId.EraseDatalog.getId())
                {
                    //no detail description
                    description = "Unsupported command";
                }
                else if (commandId == CmdId.SetRecordingReportingInterval.getId())
                {
                    RecordingAndReportingIntervalData address = (RecordingAndReportingIntervalData) getTagWithId(tpList, TagId.RecordingandReportingInterval);

                    // Format record
                    StringBuilder commandSummary = new StringBuilder("<table>");
                    commandSummary.append("<tr><td>").append("Reporting Start Time (mins): ").append("</td><td>").append(address.getReportingStartMins()).append("</td></tr>");
                    commandSummary.append("<tr><td>").append("Reporting Interval (hours): ").append("</td><td>").append(address.getReportingIntervalHours()).append("</td></tr>");
                    commandSummary.append("<tr><td>").append("Reporting Retries: ").append("</td><td>").append(address.getReportingNumberOfRetries()).append("</td></tr>");
                    commandSummary.append("<tr><td>").append("Reporting Transmit Window (mins): ").append("</td><td>").append(address.getReportingTransmitWindowsMins()).append("</td></tr>");
                    commandSummary.append("<tr><td>").append("Reporting Quiet Start (mins): ").append("</td><td>").append(address.getReportingQuietStartMins()).append("</td></tr>");
                    commandSummary.append("<tr><td>").append("Reporting Quiet End (mins): ").append("</td><td>").append(address.getReportingQuietEndMins()).append("</td></tr>");
                    commandSummary.append("<tr><td>").append("Recording Start Time (mins): ").append("</td><td>").append(address.getRecordingStartTimeMins()).append("</td></tr>");
                    commandSummary.append("<tr><td>").append("Recording Interval (mins): ").append("</td><td>").append(address.getRecordingIntervalMins()).append("</td></tr>");
                    commandSummary.append("</table>");

                    description = commandSummary.toString();

                }
                else if(commandId == CmdId.ModemFota.getId())
                {
                    // Get past version information
                    CmiuRevisionSet revisionSet = cmiuRepository.getRevisionSetAtTime(commandMiuId,
                            command.getCommandEnteredTime());

                    // Extract firmware version
                    String firmwareVersion = getTagWithId(tpList, TagId.NewImageVersionInformation).toString();

                    if (revisionSet != null)
                    {
                        description = "Update from " + revisionSet.getTelitFirmwareRevision() + " to " + firmwareVersion;
                    }
                    else
                    {
                        description = "Update to " + firmwareVersion;
                    }
                }
                else
                {
                    description = "No description for command ID: " + commandId;
                    log.warn(description);
                }
            }
        }
        catch (Exception ex)
        {
            description = ex.getMessage();
            log.warn(description, ex);

        }

        return description;
    }

    private static String createImageUpdateTaggedDataSummary(TaggedData taggedData, CmiuRevisionSet revisionSet)
    {
        return CmiuImageDistributionUtility.getFileName(taggedData.toString()) + " from " +
                getRevisionForImageType(CmiuImageDistributionUtility.s3ImageFolderToImageType(taggedData.toString()), revisionSet) + " to " +
                CmiuImageDistributionUtility.getVersionInformation(taggedData.toString());
    }

    private static String createImageUpdateTaggedDataSummary(TaggedData taggedData) {

        return CmiuImageDistributionUtility.getFileName(taggedData.toString()) + " to " + CmiuImageDistributionUtility.getVersionInformation(taggedData.toString());

    }


    private static String getRevisionForImageType(ImageDescription.ImageType type, CmiuRevisionSet revisionSet)
    {
        switch( type )
        {
            case FirmwareImage:
                return revisionSet.getFirmwareRevision();

            case ConfigImage:
                return revisionSet.getConfigRevision();

            case TelitModuleImage:
                return revisionSet.getTelitFirmwareRevision();

            case BleConfigImage:
                return revisionSet.getBleRevision();

            case ArbConfigImage:
                return revisionSet.getArbRevision();

            case EncryptionImage:
                return revisionSet.getEncryptionRevision();

            default:
                return "";

        }
    }


    /**
     * Retrieve the first tag from the tag id
     *
     * @param taggedDataList the list of tagged data to search (the haystack)
     * @param expectedTagId  the tag id to search for, the needle
     * @return return the tagged data object matching the expected tag id, else return null
     */
    private TaggedData getTagWithId(final List<TaggedData> taggedDataList, TagId expectedTagId)
    {
        Optional<TaggedData> result = taggedDataList.stream().filter(t -> t.getTagId() == expectedTagId).findFirst();

        return result.orElse(null);
    }
}
