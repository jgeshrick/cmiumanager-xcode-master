/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.miu;

import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.common.data.miu.lifecycle.model.MiuLifecycleStateEnum;
import com.neptunetg.mdce.integration.data.site.SiteId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.neptunetg.common.data.util.JdbcDateUtil.getInstant;

/**
 * MIU search repository for MySql database
 */
@Repository
public class JdbcMiuSearchRepository implements MiuSearchRepository
{
    public static final String MIU_ID = "miuId";
    public static final String SITE_ID = "siteId";
    public static final String REGIONAL_MANAGER = "regionalManager";
    public static final String LOCATION = "location";
    public static final String MIU_TYPE_OPTION = "miuType";
    public static final String IMEI_OPTION = "imei";
    public static final String ICCID_OPTION = "iccid";
    public static final String CMIU_MODE_OPTION = "cmiuMode";
    public static final String NETWORK_PROVIDER_OPTION = "networkProvider";
    public static final String FIND_ID_OPTION = "findId";
    // String Constants
    private static final String METER_ACTIVE = "meter_active";
    private static final String MIU_ID_CONST = "miu_id";
    private static final String SITE_ID_CONST = "site_id";
    private static final String LAST_HEARD_TIME = "last_heard_time";
    private static final String LAST_INSERT_DATE = "last_insert_date";
    private static final String FIRST_INSERT_DATE = "first_insert_date";
    private static final String MIU_TYPE_COLUMN = "miu_type";
    private static final String CMIU_REPORTING_INTERVAL_MINS = "reporting_interval_mins";
    private static final String CMIU_RECORDING_INTERVAL_MINS = "recording_interval_mins";
    private static final String CMIU_MODE_NAME_COLUMN = "cmiu_mode_name";
    private static final String REPORTED_CMIU_MODE_NAME_COLUMN = "reported_cmiu_mode_name";
    private static final String NETWORK_PROVIDER_COLUMN = "network_provider";
    private static final String ICCID_COLUMN = "iccid";
    private static final String IMEI_COLUMN = "imei";
    private static final String EUI_COLUMN = "eui";
    private static final String MSISDN_COLUMN = "msisdn";
    private static final String LIFECYCLE_STATE = "state";

    private static final String SQL_SELECT_ROWS =
            "SELECT md.miu_id, md.miu_type, md.last_heard_time, md.last_insert_date, md.first_insert_date, " +
            "md.site_id, md.meter_active, md.state, csr.reporting_interval_mins, csr.recording_interval_mins, csr.cmiu_mode_name, " +
            "COALESCE(sd.sim_cellular_network, ld.lora_network) AS network_provider , " +
            "csr.cmiu_mode_name AS reported_cmiu_mode_name, sd.iccid, sd.msisdn, ld.eui, " +
            "cdd.imei ";

    private static final String SQL_SELECT_COUNT =
            "SELECT COUNT(*) ";

    private static final String SQL_FROM =
            "FROM mdce.miu_details md " +
            "LEFT OUTER JOIN mdce.sim_details AS sd ON sd.miu_id = md.miu_id " +
            "LEFT OUTER JOIN mdce.lora_device_details AS ld ON ld.miu_id = md.miu_id " +
            "LEFT OUTER JOIN mdce.cellular_device_details AS cdd ON cdd.miu_id = md.miu_id ";

    private static final String SQL_LEFT_JOIN_CMIU_CONFIG_SET =
            "LEFT OUTER JOIN mdce.cmiu_config_mgmt AS cm ON cm.miu_id = md.miu_id " +
            "LEFT OUTER JOIN mdce.cmiu_config_set AS csr ON cm.reported_config = csr.id ";

    private static final String SQL_LEFT_JOIN_SITE_LIST =
            "LEFT OUTER JOIN mdce.site_list AS sl ON md.site_id = sl.site_id ";

    private static final String SQL_LEFT_JOIN_REF_DATA_INFO =
            "LEFT OUTER JOIN mdce.ref_data_utilities AS rdu ON rdu.site_id = sl.site_id " +
            "LEFT OUTER JOIN mdce.ref_data_info AS rdi ON rdu.info_id = rdi.ref_data_info_id ";

    private static final String WHERE_MIU_ID = "md.miu_id = ? ";
    private static final String WHERE_SITE_ID = "md.site_id = ? ";
    private static final String WHERE_REGIONAL_MANAGER = "rdi.regional_manager = ? ";
    private static final String WHERE_LOCATION = "rdi.state = ? ";
    private static final String WHERE_MIU_TYPE = "miu_type = ? ";
    private static final String WHERE_IMEI = "cdd.imei = ? ";
    private static final String WHERE_ICCID = "sd.iccid = ? ";
    private static final String WHERE_CMIU_MODE_NAME = "csr.cmiu_mode_name = ? ";
    private static final String WHERE_NETWORK_PROVIDER = "(? IN (sd.sim_cellular_network, ld.lora_network)) ";

    // Supply as int (default to 0 if non-numeric chars included) and string to ensure the index on miu_id is used.
    // See: http://dev.mysql.com/doc/refman/5.7/en/type-conversion.html
    private static final String WHERE_FIND_ID = "(md.miu_id != 0 AND md.miu_id = ? OR ? IN (cdd.imei, sd.iccid, ld.eui)) ";

    private static final int maxResults = 100;
    private static final String LIMIT = "LIMIT ?," + maxResults;
    // fields
    private final JdbcTemplate db;

    @Autowired
    public JdbcMiuSearchRepository(JdbcTemplate db)
    {
        this.db = db;
    }

    @Override
    public List<MiuDetails> getMius(Map<String, String> options)
    {
        final MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        final String miuIdIn = getOption(options, MIU_ID);

        if (StringUtils.hasText(miuIdIn))
        {
            final Integer miuId = new MiuId(Integer.parseInt(miuIdIn)).numericWrapperValue();
            parameters.add(WHERE_MIU_ID, miuId);
        }

        putCommonSearchParameters(options, parameters);

        final int page = Integer.parseInt(getOptionOrDefault(options, "page", "0"));

        return getMiuDetails(parameters, page);
    }

    @Override
    public int getMiusCount(Map<String, String> options)
    {
        final String miuIdIn = getOption(options, MIU_ID);

        if (miuIdIn != null && !miuIdIn.equals(""))
        {
            return 1;
        }

        final MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();

        putCommonSearchParameters(options, parameters);

        return getMiuCount(parameters);
    }

    @Override
    public List<MiuDetails> getMiusForUtility(SiteId siteId)
    {
        final MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        parameters.add(WHERE_SITE_ID, siteId.numericWrapperValue());

        return getMiuDetails(parameters);
    }

    @Override
    public MiuDetails getMiu(MiuId miuId)
    {
        final MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        parameters.add(WHERE_MIU_ID, miuId.numericWrapperValue());

        List<MiuDetails> miuDetailsList = getMiuDetails(parameters);

        if(miuDetailsList.isEmpty())
        {
            return null;
        }

        return miuDetailsList.get(0);
    }

    @Override
    public MiuDetails getMiuIfInUtility(SiteId siteId, MiuId miuId)
    {
        final MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        parameters.add(WHERE_SITE_ID, siteId.numericWrapperValue());
        parameters.add(WHERE_MIU_ID, miuId.numericWrapperValue());

        List<MiuDetails> miuDetailsList = getMiuDetails(parameters);

        if(miuDetailsList.isEmpty())
        {
            return null;
        }

        return miuDetailsList.get(0);
    }

    @Override
    public List<MiuDetails> getAllMius()
    {
        return getMiuDetails(null);
    }

    /**
     * Appends search options that are common to both 'list' and 'count' queries to the supplied query parameter map.
     * @param fromOptions The search options supplied to the search repo.
     * @param toQueryParameters The Map into which the query parameters will be added.
     */
    private void putCommonSearchParameters(Map<String, String> fromOptions, MultiValueMap<String, Object> toQueryParameters)
    {
        final String siteId = getOption(fromOptions, SITE_ID);

        if (StringUtils.hasText(siteId))
        {
            final Integer siteIdValue = new SiteId(Integer.parseInt(siteId)).numericWrapperValue();
            toQueryParameters.add(WHERE_SITE_ID, siteIdValue);
        }

        final String regionalManager = getOption(fromOptions, REGIONAL_MANAGER);

        if (StringUtils.hasText(regionalManager))
        {
            toQueryParameters.add(WHERE_REGIONAL_MANAGER, regionalManager);
        }

        final String location = getOption(fromOptions, LOCATION);

        if (StringUtils.hasText(location))
        {
            toQueryParameters.add(WHERE_LOCATION, location);
        }

        final String miuType = getOption(fromOptions, MIU_TYPE_OPTION);

        if (StringUtils.hasText(miuType))
        {
            toQueryParameters.add(WHERE_MIU_TYPE, getMiuTypeNumber(miuType));
        }

        final String imei = getOption(fromOptions, IMEI_OPTION);

        if (StringUtils.hasText(imei))
        {
            toQueryParameters.add(WHERE_IMEI, imei);
        }

        final String iccid = getOption(fromOptions, ICCID_OPTION);

        if (StringUtils.hasText(iccid))
        {
            toQueryParameters.add(WHERE_ICCID, iccid);
        }

        final String cmiuMode = getOption(fromOptions, CMIU_MODE_OPTION);

        if (StringUtils.hasText(cmiuMode))
        {
            toQueryParameters.add(WHERE_CMIU_MODE_NAME, cmiuMode);
        }

        final String networkProvider = getOption(fromOptions, NETWORK_PROVIDER_OPTION);

        if (StringUtils.hasText(networkProvider))
        {
            toQueryParameters.add(WHERE_NETWORK_PROVIDER, networkProvider);
        }

        final String findId = getOption(fromOptions, FIND_ID_OPTION);

        if (StringUtils.hasText(findId))
        {
            int integerFindId = tryParseToInt(findId, 0);
            toQueryParameters.add(WHERE_FIND_ID, integerFindId);
            toQueryParameters.add(WHERE_FIND_ID, findId);
        }
    }

    private String getMiuQueryWithoutSelect(MultiValueMap<String, Object> parameters)
    {
        String sql = SQL_FROM + SQL_LEFT_JOIN_CMIU_CONFIG_SET;

        // Join onto site_list if site_list and/or ref_data_info are being queried
        if (parameters.containsKey(WHERE_SITE_ID) || parameters.containsKey(WHERE_LOCATION) ||
                parameters.containsKey(WHERE_REGIONAL_MANAGER))
        {
            sql += SQL_LEFT_JOIN_SITE_LIST;
        }

        // Join onto ref_data_info for location/regional_manager searches
        if (parameters.containsKey(WHERE_LOCATION) || parameters.containsKey(WHERE_REGIONAL_MANAGER))
        {
            sql += SQL_LEFT_JOIN_REF_DATA_INFO;
        }

        boolean isFirstClause = true;

        for (Map.Entry<String, List<Object>> entryList : parameters.entrySet())
        {
            sql += isFirstClause ? "WHERE " : "AND ";
            isFirstClause = false;
            sql += entryList.getKey();
        }

        return sql;
    }

    private List<MiuDetails> getMiuDetails(MultiValueMap<String, Object> parameters)
    {
        if (parameters == null)
        {
            parameters = new LinkedMultiValueMap<>();
        }

        String sql = SQL_SELECT_ROWS + getMiuQueryWithoutSelect(parameters);
        final List<Object> parameterValues = new ArrayList<>();
        parameters.values().forEach(parameterValues::addAll);

        return this.db.query(sql, getMiuDetailsRowMapper(), parameterValues.toArray());
    }

    private List<MiuDetails> getMiuDetails(MultiValueMap<String, Object> parameters, int page)
    {
        String sql = SQL_SELECT_ROWS + getMiuQueryWithoutSelect(parameters) + LIMIT;
        final List<Object> parameterValues = new ArrayList<>();
        parameters.values().forEach(parameterValues::addAll);
        parameterValues.add(page * maxResults);

        return this.db.query(sql, getMiuDetailsRowMapper(), parameterValues.toArray());
    }

    private Integer getMiuCount(MultiValueMap<String, Object> parameters)
    {
        String sql = SQL_SELECT_COUNT + getMiuQueryWithoutSelect(parameters);
        final List<Object> parameterValues = new ArrayList<>();
        parameters.values().forEach(parameterValues::addAll);

        return this.db.queryForObject(sql, parameterValues.toArray(), Integer.class);
    }

    private RowMapper<MiuDetails> getMiuDetailsRowMapper()
    {
        return (rs, rownum) ->
                new MiuDetails(
                        MiuId.valueOf(rs.getLong(MIU_ID_CONST)),
                        SiteId.valueOf(rs.getInt(SITE_ID_CONST)),
                        getInstant(rs, LAST_HEARD_TIME),
                        getInstant(rs, LAST_INSERT_DATE),
                        getInstant(rs, FIRST_INSERT_DATE),
                        MiuType.valueOf(rs.getInt(MIU_TYPE_COLUMN)),
                        rs.getBoolean(METER_ACTIVE),
                        rs.getObject(CMIU_RECORDING_INTERVAL_MINS) != null ? rs.getInt(CMIU_RECORDING_INTERVAL_MINS) : null,
                        rs.getObject(CMIU_REPORTING_INTERVAL_MINS) != null ? rs.getInt(CMIU_REPORTING_INTERVAL_MINS) : null,
                        rs.getString(CMIU_MODE_NAME_COLUMN),
                        rs.getString(REPORTED_CMIU_MODE_NAME_COLUMN),
                        rs.getString(NETWORK_PROVIDER_COLUMN),
                        rs.getString(ICCID_COLUMN),
                        rs.getString(IMEI_COLUMN),
                        rs.getString(EUI_COLUMN),
                        rs.getString(MSISDN_COLUMN),
                        MiuLifecycleStateEnum.fromStringValue(rs.getString(LIFECYCLE_STATE)));
    }

    private String getOptionOrDefault(Map<String, String> options, String option, String defaultValue)
    {
        final String optionValue = getOption(options, option);

        if (StringUtils.hasText(optionValue))
        {
            return optionValue;
        }

        return defaultValue;
    }

    private String getOption(Map<String, String> options, String option)
    {
        if (options.containsKey(option))
        {
            return options.get(option);
        }

        return null;
    }

    private int getMiuTypeNumber(String miuTypeString)
    {
        int miuTypeNumber = -1;
        final MiuType miuType = MiuType.fromString(miuTypeString);

        if (miuType != null)
        {
            miuTypeNumber = miuType.numericValue();
        }

        return miuTypeNumber;
    }

    private static int tryParseToInt(String text, int defaultValue)
    {
        Integer result;

        try
        {
            result = Integer.parseInt(text);
        }
        catch (NumberFormatException e)
        {
            result = defaultValue;
        }

        return result.intValue();
    }
}
