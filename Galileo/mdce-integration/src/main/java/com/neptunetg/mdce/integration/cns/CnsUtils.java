/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.cns;

import com.neptunetg.common.cns.DeviceIdentifierKind;
import com.neptunetg.common.data.miu.MiuDataException;
import com.neptunetg.common.data.miu.cellular.CellularDeviceId;

/**
 * Utilities for cellular network services
 */
public class CnsUtils
{

    private CnsUtils()
    {}

    public static CellularDeviceId cellularDeviceId(DeviceIdentifierKind kind, String value) throws MiuDataException
    {
        switch (kind)
        {
            case MSISDN: return CellularDeviceId.msisdn(value);
            case ICCID: return CellularDeviceId.iccid(value);
            case IMEI: return CellularDeviceId.imei(value);
            default: throw new MiuDataException("Invalid cellular device ID " + kind + " " + value);
        }
    }
}
