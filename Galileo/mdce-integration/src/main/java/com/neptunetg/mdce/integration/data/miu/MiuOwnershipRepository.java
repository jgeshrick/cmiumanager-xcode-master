/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.data.miu;


import com.neptunetg.common.data.miu.MiuId;
import com.neptunetg.common.data.miu.MiuType;
import com.neptunetg.mdce.integration.data.site.SiteId;
import com.neptunetg.mdce.integration.domain.InsertMiuDetailsBpss;
import com.neptunetg.mdce.integration.domain.UpdateMiuSiteIdBpss;
import com.neptunetg.mdce.integration.rest.json.JsonMiuActiveFlag;

import java.util.List;

public interface MiuOwnershipRepository
{

    boolean isMiuBillable(MiuId miuId);

    int updateBilling(MiuId miuId, boolean isBillable);

    /**
     * Get the current owning site of the MIU
     * @param miuId miu id
     * @return Owning site ID, or null if no owner
     */
    SiteId getMiuOwner(MiuId miuId);

    /**
     * Get the current owning site of the MIU
     * @param miuJsonList List of MIU IDs to get site IDs for
     */
    void populateExistingMiuOwnerBatch(List<JsonMiuActiveFlag> miuJsonList);

    int[] updateMiuOwnerBatch(UpdateMiuSiteIdBpss updateMiuSiteIdBpss);

    void insertMiuSetOwnerBatch(InsertMiuDetailsBpss insertMiuDetailsBpss);


    /**
     * Update miu details with new site id and meter active status
     * @param miuId miu to update
     * @param newSiteId new site id
     * @param isMeterActive true to set meter to active
     * @param type type of MIU.  Only used if creating the MIU.  Existing MIU's type is not changed by this command.
     * @return number of rows updated
     */
    int updateMiuOwnerDetails(MiuId miuId, SiteId newSiteId, String isMeterActive, MiuType type);

    /**
     * Gets MIU ownership history starting after the supplied ID.
     * @param lastSequenceId Return the next records after this miu_owner_history_id.
     * @param pageSize Sets the maximum number of results to return.
     * @return Any MIU owner history records matching the supplied criteria.
     */
    List<MiuOwnerHistory> getMiuOwnerHistory(int lastSequenceId, int pageSize);
}