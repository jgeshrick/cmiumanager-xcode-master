/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.cmiudeactivation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
    JSON object for return multiple MIUs' configuration.
*/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonDeactivationDetails
{
    @JsonProperty("miu_id")
    private long miuId;

    @JsonProperty("iccid")
    private String iccid;

    @JsonProperty("imei")
    private String imei;

    @JsonProperty("last_heard_time")
    private String lastTimeHeard;

    @JsonProperty("last_communication_time")
    private String lastCommunicationTime;

    @JsonProperty("current_lifecycle_state")
    private String currentLifecycleState;

    @JsonProperty("carrier")
    private String carrier;

    public long getMiuId()
    {
        return miuId;
    }

    public void setMiuId(long miuId)
    {
        this.miuId = miuId;
    }

    public String getIccid()
    {
        return iccid;
    }

    public void setIccid(String iccid)
    {
        this.iccid = iccid;
    }

    public String getImei()
    {
        return imei;
    }

    public void setImei(String imei)
    {
        this.imei = imei;
    }

    public String getLastTimeHeard()
    {
        return lastTimeHeard;
    }

    public void setLastTimeHeard(String lastTimeHeard)
    {
        this.lastTimeHeard = lastTimeHeard;
    }

    public String getLastCommunicationTime()
    {
        return lastCommunicationTime;
    }

    public void setLastCommunicationTime(String lastCommunicationTime)
    {
        this.lastCommunicationTime = lastCommunicationTime;
    }

    public String getCurrentLifecycleState()
    {
        return currentLifecycleState;
    }

    public void setCurrentLifecycleState(String currentLifecycleState)
    {
        this.currentLifecycleState = currentLifecycleState;
    }

    public String getCarrier()
    {
        return carrier;
    }

    public void setCarrier(String carrier)
    {
        this.carrier = carrier;
    }
}
