/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015, 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.integration.domain.alert;

import com.neptunetg.common.aws.AwsQueueReceiver;
import com.neptunetg.common.aws.MdceIpcSubscribeService;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertLog;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.integration.data.alert.AlertRepository;
import com.neptunetg.mdce.integration.domain.user.UserManagerService;
import com.neptunetg.mdce.integration.rest.internal.MiuCommandControlServiceController;
import com.neptunetg.mdce.integration.utility.SendEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Provides service for creating and dealing with alerts
 *
 */
@Service
public class AlertServiceImpl implements AlertService, AwsQueueReceiver
{
    private static final Logger log = LoggerFactory.getLogger(AlertServiceImpl.class);

    @Value("#{envProperties['alert.keep.cleared.days']?:1}")
    private int daysToKeepClearedAlerts;
    @Value("#{envProperties['server.domain.url']}")
    public String domainUrl;
    @Autowired
    private AlertRepository alertRepository;
    @Autowired
    private UserManagerService userManagerService;
    @Autowired
    private SendEmailService emailUtility;

    @Autowired
    public AlertServiceImpl(MdceIpcSubscribeService awsQueueService)
    {
        awsQueueService.register(this, MdceIpcSubscribeService.MANAGE_OLD_ALERT, Duration.ofMinutes(1L), 10);
    }


    @Override
    public List<AlertDetails> getNewAlertList(String alertSource)
    {
        return alertRepository.getNewAlertList(alertSource);
    }

    @Override
    public List<AlertDetails> getHandledAlertList(String alertSource)
    {
        return alertRepository.getHandledAlertList(alertSource);
    }

    @Override
    public List<AlertDetails> getStaleAlertList(String alertSource)
    {
        return alertRepository.getStaleAlertList(alertSource);
    }

    @Override
    public List<AlertDetails> getClearedAlertList(String alertSource)
    {
        return alertRepository.getClearedAlertList(alertSource);
    }

    @Override
    public AlertDetails getAlert(long alertId)
    {
        return alertRepository.getAlert(alertId);
    }

    @Override
    public AlertDetails getAlertNotCleared(String alertSource)
    {
        return alertRepository.getAlertNotCleared(alertSource);
    }

    @Override
    public boolean addAlertAndSendEmail(AlertDetails alertDetails) throws InternalApiException {
        final AlertEmail email = initAlertEmail();

        final boolean ret = addAlert(alertDetails, email);

        if (email.isAnyContent()) {
            email.send(userManagerService, emailUtility);
        }

        return ret;
    }

    @Override
    public boolean addAlert(AlertDetails alertDetails, AlertEmail email) throws InternalApiException
    {
        if (alertDetails.getAlertLevel() == null || alertDetails.getAlertState() == null)
        {
            throw new IllegalArgumentException("AlertDetails: Alert level and State must be set");
        }

        AlertDetails lastAlert = alertRepository.getAlertNotCleared(alertDetails.getAlertSource());
        if (lastAlert == null)
        {
            if (AlertState.NEW == alertDetails.getAlertState())
            {
                alertRepository.addAlert(alertDetails);
                email.addAlert(alertDetails, AlertLevel.WARNING == alertDetails.getAlertLevel());

                return true;
            }
        }
        else
        {
            alertDetails.setAlertId(lastAlert.getAlertId());

            if (lastAlert.getAlertLevel() == AlertLevel.WARNING &&
                    alertDetails.getAlertLevel() == AlertLevel.ERROR &&
                    alertDetails.getAlertState() != AlertState.STALE)
            {
                alertRepository.setAlertLevelToError(alertDetails.getAlertId());
                email.addAlert(alertDetails, false);
            }

            if (lastAlert.getAlertState() == AlertState.NEW &&
                    alertDetails.getAlertState() == AlertState.STALE)
            {
                alertRepository.setAlertStateToStale(alertDetails.getAlertId());
            }

            if (lastAlert.getAlertState() == AlertState.STALE &&
                    alertDetails.getAlertState() != AlertState.STALE )
            {
                if(lastAlert.getAlertTicketId() == null)
                {
                    alertRepository.setAlertStateToNew(alertDetails.getAlertId());
                }
                else
                {
                    alertRepository.setAlertStateToHandled(alertDetails.getAlertId());
                }

                email.addAlert(alertDetails, false);
            }

            return addAlertLogMessage(alertDetails);
        }

        return false;
    }


    @Override
    public void sendEmail(AlertEmail email) {
        if (email != null)
        {
            email.send(this.userManagerService, this.emailUtility);
        }
    }

    @Override
    public boolean setAlertStateToNew(long alertId)
    {
        return alertRepository.setAlertStateToNew(alertId);
    }

    @Override
    public boolean setAlertStateToHandled(long alertId)
    {
        return alertRepository.setAlertStateToHandled(alertId);
    }

    @Override
    public boolean setAlertStateToStale(long alertId)
    {
        return alertRepository.setAlertStateToStale(alertId);
    }

    @Override
    public boolean setAlertStateToCleared(long alertId)
    {
        return alertRepository.setAlertStateToCleared(alertId);
    }

    @Override
    public boolean setAlertLevelToWarning(long alertId)
    {
        return alertRepository.setAlertLevelToWarning(alertId);
    }

    @Override
    public boolean setAlertLevelToError(long alertId)
    {
        return alertRepository.setAlertLevelToError(alertId);
    }

    @Override
    public boolean setAlertTicketId(AlertDetails alertDetails)
    {
        return alertRepository.setAlertTicketId(alertDetails);
    }

    @Override
    public boolean addAlertLogMessage(AlertDetails alertDetails)
    {
        return  alertRepository.addAlertLogMessage(alertDetails);
    }

    @Override
    public List<AlertLog> getAlertLog(long alertId)
    {
        return  alertRepository.getAlertLog(alertId);
    }

    @Override
    public AlertEmail initAlertEmail()
    {
        return new AlertEmail(domainUrl);
    }

    /**
     * Performs housekeeping on the alerts.
     * Note: This loop may run for some time if there are large numbers of alerts and logs.
     * @param message
     * @param messageId
     * @param messageReceipt
     * @param cancellationToken
     * @return
     */
    @Override
    public boolean processReceivedMessage(String message, String messageId, String messageReceipt, AtomicBoolean cancellationToken)
    {
        // Remove cleared alerts after daysToKeepClearedAlerts days.
        List<AlertDetails> alertDetailsList = alertRepository.getClearedAlertList("%");
        final Instant threshold = Instant.now().minus(daysToKeepClearedAlerts, ChronoUnit.DAYS);

        for(AlertDetails alertDetails : alertDetailsList )
        {
            if (cancellationToken.get())
            {
                break;
            }

            final Instant time = alertDetails.getLatestAlertDate().toInstant();
            if (time.isBefore(threshold))
            {
                alertRepository.removeAlertsLogs(alertDetails.getAlertId());
                alertRepository.removeAlert(alertDetails.getAlertId());
            }
        }

        // Trim alert logs exceeding window size
        alertDetailsList = alertRepository.getAlertsExceedingLogWindowSize();
        for(AlertDetails alertDetails : alertDetailsList)
        {
            if (cancellationToken.get())
            {
                break;
            }

            int trimCount = alertRepository.trimAlertLogs(alertDetails.getAlertId());
            if (trimCount != 0)
            {
                log.info(String.format("Trimmed %d log(s) from alert ID: %d.", trimCount, alertDetails.getAlertId()));
            }
        }

        return true;
    }

    @Override
    public Long lastInsertId()
    {
        return alertRepository.lastInsertId();
    }
}
