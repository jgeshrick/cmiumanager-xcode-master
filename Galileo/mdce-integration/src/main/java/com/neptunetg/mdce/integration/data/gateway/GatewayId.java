/*
 *  ***************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * ***************************************************************************
 */

package com.neptunetg.mdce.integration.data.gateway;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.Converter;
import org.springframework.util.StringUtils;

/**
 * Represents a Gateway ID
 */
public class GatewayId
{
    private final String id;

    public GatewayId(String id)
    {
        if (!StringUtils.hasText(id))
        {
            throw new IllegalArgumentException("Empty gateway ID");
        }
        this.id = id.trim();
    }


    public static GatewayId valueOf(String id)
    {
        return new GatewayId(id);
    }


    @Override
    public int hashCode()
    {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        GatewayId other = (GatewayId) obj;
        return id.equals(other.id);
    }

    @Override
    public String toString()
    {
        return id;
    }


    public static class ToStringConverter implements Converter<GatewayId, String>
    {

        @Override
        public String convert(GatewayId arg0)
        {
            return arg0.toString();
        }

        @Override
        public JavaType getInputType(TypeFactory arg0)
        {
            return arg0.constructType(GatewayId.class);
        }

        @Override
        public JavaType getOutputType(TypeFactory arg0)
        {
            return arg0.constructType(String.class);
        }

    }

}
