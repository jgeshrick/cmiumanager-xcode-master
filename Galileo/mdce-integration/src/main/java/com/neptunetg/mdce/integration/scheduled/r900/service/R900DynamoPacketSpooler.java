/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.scheduled.r900.service;

import com.neptunetg.mdce.common.data.DynamoPacketRepository;
import com.neptunetg.mdce.common.data.MdceDataException;
import com.neptunetg.mdce.common.data.model.GatewayPacketReceivedDynamoItem;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/**
 * Asynchronously inserts packets into DynamoDB
 */
public class R900DynamoPacketSpooler implements AutoCloseable
{
    private static final Logger logger = LoggerFactory.getLogger(R900DynamoPacketSpooler.class);

    private final DynamoPacketRepository dynamoRepo;

    private final ExecutorService insertThread = Executors.newFixedThreadPool(1);

    private final List<FutureTask<Long>> insertBacklog = new LinkedList<>();

    public R900DynamoPacketSpooler(DynamoPacketRepository dynamoRepo)
    {
        this.dynamoRepo = dynamoRepo;
    }

    /**
     * Operation to insert a batch of miuPackets into dynamo
     *
     * @param miuPacketBatch list of miuPacket to be inserted into DynamoDb
     * @throws MdceDataException
     */
    public void insertMiuPacketsIntoDynamoAsync(List<MiuPacketReceivedDynamoItem> miuPacketBatch) throws MdceDataException
    {
        clearDoneTasksFromBacklog();

        if (logger.isDebugEnabled())
        {
            logger.debug("Dynamo insert task backlog size is " + insertBacklog.size() + "; scheduling new batch of " + miuPacketBatch.size() + " for insert");
        }
        final List<MiuPacketReceivedDynamoItem> packetsToInsert = new ArrayList<>(miuPacketBatch); //copy as the argument list could change
        final FutureTask<Long> task = new FutureTask<>(() -> insertMiuPacketsIntoDynamoSync(packetsToInsert));
        insertBacklog.add(task);
        insertThread.execute(task);
    }

    private void clearDoneTasksFromBacklog() throws MdceDataException
    {
        for (Iterator<FutureTask<Long>> taskBacklogIterator = insertBacklog.iterator(); taskBacklogIterator.hasNext(); )
        {
            final FutureTask<Long> t = taskBacklogIterator.next();
            if (t.isDone())
            {
                taskBacklogIterator.remove();
                try
                {
                    t.get(); //get the result to check for exception
                }
                catch (InterruptedException e)
                {
                    throw new MdceDataException("Dynamo insert result retrieval interrupted!", e);
                }
                catch (ExecutionException e)
                {
                    final Throwable cause = e.getCause();
                    if (cause != null && cause instanceof MdceDataException)
                    {
                        throw (MdceDataException)cause;
                    }
                    else
                    {
                        throw new MdceDataException("Error during Dyanmo insert!", e);
                    }
                }
            }
        }
    }

    private Long insertMiuPacketsIntoDynamoSync(List<MiuPacketReceivedDynamoItem> miuPacketBatch) throws MdceDataException, InterruptedException
    {
        long startedTime = 0L;
        if (logger.isDebugEnabled())
        {
            startedTime = System.currentTimeMillis();
            logger.debug("                          Starting insert of {} MIU packets", miuPacketBatch.size());
        }

        dynamoRepo.insertMiuPacketsReceived(miuPacketBatch);

        if (logger.isDebugEnabled())
        {
            final Long duration = Long.valueOf(System.currentTimeMillis() - startedTime);
            logger.debug("                          Insert completed in {} ms", duration);
            return duration;
        }
        else
        {
            return Long.valueOf(0L);
        }
    }

    /**
     * Operation to insert a gateway packet into dynamo
     *
     * @throws MdceDataException
     */
    public void insertGatewayPacketIntoDynamoSync(GatewayPacketReceivedDynamoItem gatewayPacket) throws MdceDataException, InterruptedException
    {
        dynamoRepo.insertR900GatewayPacketReceived(gatewayPacket);

    }

    public void waitForCompletion() throws MdceDataException
    {
        clearDoneTasksFromBacklog();
        logger.debug("Awaiting completion of " + insertBacklog.size() + " asynchronous Dynamo batch inserts");
        this.insertThread.shutdown();
        try
        {
            this.insertThread.awaitTermination(15L, TimeUnit.MINUTES);
        }
        catch (InterruptedException e)
        {
            logger.warn("Interrupted while waiting for completion of asynchronous Dynamo batch inserts");
        }
        clearDoneTasksFromBacklog();
    }

    @Override
    public void close() throws Exception
    {
        this.insertThread.shutdown();
    }
}
