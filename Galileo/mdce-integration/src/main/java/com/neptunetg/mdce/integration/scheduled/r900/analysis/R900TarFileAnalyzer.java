/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.integration.scheduled.r900.analysis;

import com.neptunetg.common.packet.model.builder.R900MiuReadingsPacketBuilder;
import com.neptunetg.common.packet.model.builder.R900MiuRssiSummary;
import com.neptunetg.common.packet.model.taggeddata.packets.R900MiuReadingsPacket;
import com.neptunetg.mdce.common.data.MdceDataException;
import com.neptunetg.mdce.common.data.model.DynamoItemPacketType;
import com.neptunetg.mdce.common.data.model.GatewayPacketReceivedDynamoItem;
import com.neptunetg.mdce.common.data.model.MiuPacketReceivedDynamoItem;
import com.neptunetg.mdce.integration.data.gateway.GatewayId;
import com.neptunetg.mdce.integration.rest.R900FileType;
import com.neptunetg.mdce.common.api.rest.exception.MdceRestException;
import com.neptunetg.mdce.integration.scheduled.r900.service.R900DynamoPacketSpooler;
import com.neptunetg.mdce.integration.scheduled.r900.service.R900RelationalDataSpooler;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Stand alone (POJO) file wrapper that processes as detailed in ETI 48-02
 * Extracts lists of updates to be made, with results available from public getters
 */
public class R900TarFileAnalyzer implements FileAnalyzer
{
    private static final Logger logger = LoggerFactory.getLogger(R900TarFileAnalyzer.class);

    private static final Pattern INNER_FILENAME_PATTERN = Pattern.compile("(\\d+)_(.*)_(\\d+)_(\\d+)_?(.*)\\.(.*)");
    private static final Pattern IMAGE_NAME_PATTERN = Pattern.compile("ImageName=\"(.*)\"");
    private static final Pattern GW_HEADER_ENTRY_PATTERN = Pattern.compile("([A-Za-z\\-0-9]+)=(.*)");

    private static final int ASCII_LF = 10;
    private static final String MANIFEST = "Manifest";
    private static final int DYNAMO_BATCH_TASK_SIZE = 200;

    private final File tarFileToProcess;

    private GatewayId gatewayId;
    private int configFskCount;
    final private boolean storeR900Packets;

    private final Map<String, byte[]> tarEntryData = new HashMap<>();

    /**
     * The data spooler stores up MySQL updates for the R900 MIUs and performs them in batches.
     * It also collects the config and summary data for the gateway
     */
    private final R900RelationalDataSpooler dataSaver;
    private final R900DynamoPacketSpooler packetSaver;


    private final int seqNumber;

    public R900TarFileAnalyzer(File tarFileToProcess,
                               R900DynamoPacketSpooler packetSaver,
                               R900RelationalDataSpooler dataSaver,
                               boolean storeR900Packets)
    {
        this.tarFileToProcess = tarFileToProcess;
        this.seqNumber = (int)System.currentTimeMillis(); //useful to identify the tarball, otherwise fake
        this.dataSaver = dataSaver;
        this.packetSaver = packetSaver;
        this.storeR900Packets = storeR900Packets;
        populateTarEntryMap();
    }

    @Override
    public void processFile()
    {
        final Instant insertDate = Instant.now();
        extractTarFileContents(insertDate);
    }

    private void extractTarFileContents(Instant insertDate)
    {
        try
        {
            final Map<Integer, R900MiuReadingsPacketBuilder> miuPacketBuilderMap = new HashMap<>();

            if (tarEntryData.containsKey(MANIFEST))
            {
                final byte[] buf = tarEntryData.get(MANIFEST);

                try (final BufferedReader manifestReader = getBufferedReaderFromBytes(buf))
                {
                    iterateThroughManifestEntries(insertDate, manifestReader, miuPacketBuilderMap);
                }

                if(storeR900Packets)
                {
                    insertR900MiuDynamoPackets(miuPacketBuilderMap, insertDate);
                }
                else
                {
                    insertR900GatewayStats(miuPacketBuilderMap, insertDate);
                }
            }
            else
            {
                throw new MdceRestException("No manifest in tar file " + tarFileToProcess.getName());
            }

        } catch (Exception e)
        {
            throw new MdceRestException("Exception thrown processing " + tarFileToProcess.getName(), e);
        }
    }

    /**
     * updates the last heard time and RSSI values of the R900 packets.
     * @param miuPacketBuilderMap
     * @param insertTimestamp
     */
    private void insertR900GatewayStats(Map<Integer, R900MiuReadingsPacketBuilder> miuPacketBuilderMap, Instant insertTimestamp) throws MdceDataException
    {
        int miuCount = 0;
        int ookPacketCount = 0;
        int fskPacketCount = 0;
        logger.debug("Begin processing MIU data from tarball " + this.tarFileToProcess.getName());

        for (final Iterator<Map.Entry<Integer, R900MiuReadingsPacketBuilder>> it = miuPacketBuilderMap.entrySet().iterator(); it.hasNext();)
        {
            final Map.Entry<Integer, R900MiuReadingsPacketBuilder> m = it.next();
            it.remove();
            miuCount++;
            final R900MiuReadingsPacketBuilder builder = m.getValue();
            final R900MiuReadingsPacket packet = builder.build();

            if (packet.getPacketCount() > 0)
            {
                ookPacketCount += builder.getOokPacketCount();
                fskPacketCount += builder.getFskPacketCount();
            }
        }

        logger.debug("Inserting gateway stats for tarball " + this.tarFileToProcess.getName() + " into MySQL");

        dataSaver.writeGatewayHeardStatsAndWriteConfig(gatewayId, insertTimestamp, insertTimestamp, miuCount,
                ookPacketCount, fskPacketCount, this.configFskCount, storeR900Packets);

        logger.debug("Finished processing tarball " + this.tarFileToProcess.getName());
    }

    /**
     * Clears the map of R900 MIU packets and inserts the packets into Dynamo.
     * Also updates the last heard time and RSSI values of the R900 packets.
     * @param miuPacketBuilderMap
     * @param insertTimestamp
     */
    private void insertR900MiuDynamoPackets(Map<Integer, R900MiuReadingsPacketBuilder> miuPacketBuilderMap, Instant insertTimestamp) throws MdceDataException
    {
        int miuCount = 0;
        int ookPacketCount = 0;
        int fskPacketCount = 0;
        final List<MiuPacketReceivedDynamoItem> dynamoBatch = new ArrayList<>(DYNAMO_BATCH_TASK_SIZE);
        logger.debug("Begin inserting MIU data from tarball " + this.tarFileToProcess.getName() + " into Dynamo and MySQL");

        for (final Iterator<Map.Entry<Integer, R900MiuReadingsPacketBuilder>> it = miuPacketBuilderMap.entrySet().iterator(); it.hasNext();)
        {
            final Map.Entry<Integer, R900MiuReadingsPacketBuilder> m = it.next();
            it.remove();
            miuCount++;
            final Integer miuId = m.getKey();
            final R900MiuReadingsPacketBuilder builder = m.getValue();
            final R900MiuReadingsPacket packet = builder.build();

            if (packet.getPacketCount() > 0)
            {
                final MiuPacketReceivedDynamoItem dynamoItem = buildMiuPacketReceivedDynamoItem(miuId.intValue(), packet, insertTimestamp);
                dynamoBatch.add(dynamoItem);

                if (dynamoBatch.size() >= DYNAMO_BATCH_TASK_SIZE)
                {
                    this.packetSaver.insertMiuPacketsIntoDynamoAsync(dynamoBatch);
                    dynamoBatch.clear();
                }
                final R900MiuRssiSummary rssiSummary = builder.getRssiSummary();

                dataSaver.recordMiuHeard(miuId, insertTimestamp, rssiSummary);

                ookPacketCount += builder.getOokPacketCount();
                fskPacketCount += builder.getFskPacketCount();
            }
        }

        if (dynamoBatch.size() > 0)
        {
            this.packetSaver.insertMiuPacketsIntoDynamoAsync(dynamoBatch);
            dynamoBatch.clear();
        }

        logger.debug("Inserting gateway stats for tarball " + this.tarFileToProcess.getName() + " into MySQL");

        dataSaver.writeGatewayHeardStatsAndWriteConfig(gatewayId, insertTimestamp, insertTimestamp, miuCount,
                ookPacketCount, fskPacketCount, this.configFskCount, storeR900Packets);

        logger.debug("Waiting for Dynamo inserts to finish for tarball " + this.tarFileToProcess.getName());

        this.packetSaver.waitForCompletion();

        logger.debug("Finished processing tarball " + this.tarFileToProcess.getName());
    }

    private BufferedReader getBufferedReaderFromBytes(byte[] buf)
    {
        return new BufferedReader(new InputStreamReader(new ByteArrayInputStream(buf)));
    }

    private void iterateThroughManifestEntries(Instant insertTimestamp, BufferedReader manifestReader, Map<Integer, R900MiuReadingsPacketBuilder> miuPacketBuilderMap) throws IOException
    {
        String line;
        List<String> lines = new ArrayList();
        boolean gotOok = false;
        boolean gotFsk = false;
        boolean gotConfig = false;

        while ((line = manifestReader.readLine()) != null)
        {
            lines.add(line);

            final R900FileType r900FileType = R900FileType.getR900FileType(line);
            // a line of comment will return null so skip it
            if (r900FileType != null)
            {
                if(R900FileType.OOK_READING.equals(r900FileType))
                {
                    gotOok = true;
                }
                else if(R900FileType.FSK_READING.equals(r900FileType))
                {
                    gotFsk = true;
                }
                else if(R900FileType.FSK_CONFIG.equals(r900FileType))
                {
                    gotConfig = true;
                }

                final String filename = getFilenameFromManifest(manifestReader);
                final byte[] bytes = tarEntryData.get(filename);

                extractFromFile(filename, bytes, r900FileType, insertTimestamp, miuPacketBuilderMap);
            }
        }

        for(String manifestLine : lines)
        {
            Matcher fileDetails = IMAGE_NAME_PATTERN.matcher(manifestLine);

            Pattern ookMatcher = Pattern.compile("_.+\\.dat");

            if(fileDetails.find())
            {
                String filename = fileDetails.group(1);
                String filenameLC = filename.toLowerCase();
                Matcher ookMatches = ookMatcher.matcher(filenameLC);

                if(filenameLC.contains("fsk.dat"))
                {
                    if(!gotFsk)
                    {
                        gotFsk = true;
                        final byte[] bytes = tarEntryData.get(filename);
                        extractFromFile(filename, bytes, R900FileType.FSK_READING, insertTimestamp, miuPacketBuilderMap);
                    }
                }
                else if(filenameLC.contains("config.dat"))
                {
                    if(!gotConfig)
                    {
                        gotConfig = true;
                        final byte[] bytes = tarEntryData.get(filename);
                        extractFromFile(filename, bytes, R900FileType.FSK_CONFIG, insertTimestamp, miuPacketBuilderMap);
                    }
                }
                else if(ookMatches.find())
                {
                    if(!gotOok)
                    {
                        gotOok = true;
                        final byte[] bytes = tarEntryData.get(filename);
                        extractFromFile(filename, bytes, R900FileType.OOK_READING, insertTimestamp, miuPacketBuilderMap);
                    }
                }
            }
        }

        if(!gotOok && !gotFsk)
        {
            logger.warn("Couldn't find OOK or FSK readings in: " + this.tarFileToProcess.getName());
        }
    }

    private void extractFromFile(String filename, byte[] bytes, R900FileType r900FileType, Instant insertTimestamp,
                                 Map<Integer, R900MiuReadingsPacketBuilder> miuPacketBuilderMap)
    {
        try
        {
            if (R900FileType.OOK_READING.equals(r900FileType) || R900FileType.FSK_READING.equals(r900FileType))
            {
                extractPacketsFromReadingsFile(filename, bytes, r900FileType, insertTimestamp, miuPacketBuilderMap);
            }
            else
            {
                final int lines = handleFskFiletype(bytes, filename, insertTimestamp, r900FileType);

                if (R900FileType.FSK_CONFIG.equals(r900FileType))
                {
                    this.configFskCount = lines;
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Error while processing sub-file " + filename + " in tarball " + tarFileToProcess.getName() + "; continuing with other sub-files", e);
        }
    }

    /**
     *
     * @param bytes
     * @param filename
     * @param insertTimestamp
     * @param fskConfig
     * @return Number of lines in the file
     * @throws IOException
     */
    private int handleFskFiletype(final byte[] bytes, String filename, Instant insertTimestamp, R900FileType fskConfig) throws IOException, MdceDataException, InterruptedException
    {
        if (bytes.length > 0)
        {
            return processFskFile(filename, new ByteArrayInputStream(bytes), insertTimestamp, fskConfig);
        }
        else
        {
            return 0;
        }
    }

    private String getFilenameFromManifest(BufferedReader manifestReader) throws IOException
    {
        String manifestLine = manifestReader.readLine();
        Matcher fileDetails = IMAGE_NAME_PATTERN.matcher(manifestLine);
        fileDetails.find();

        return fileDetails.group(1);
    }

    private void populateTarEntryMap()
    {
        try (final InputStream fileInputStream = new FileInputStream(tarFileToProcess))
        {
            try (final TarArchiveInputStream untarredIn = new TarArchiveInputStream(fileInputStream))
            {
                handleEachTarEntry(untarredIn);
            }
        } catch (Exception e)
        {
            throw new MdceRestException("Exception thrown opening Tar file " + tarFileToProcess.getName(), e);
        }
    }

    private void handleEachTarEntry(TarArchiveInputStream untarredIn) throws IOException
    {
        while (untarredIn.getNextTarEntry() != null)
        {
            TarArchiveEntry entry = untarredIn.getCurrentEntry();
            final byte[] bytes = getBytes(untarredIn, entry.getSize());
            tarEntryData.put(entry.getName(), bytes);
        }
    }

    private byte[] getBytes(InputStream dataStream, long len) throws IOException
    {
        return getBytes(dataStream, Long.valueOf(len).intValue());
    }

    /**
     * Process a READINGS_OOK or READINGS_FSK file and insert the readings into a packet builder map
     *
     * @param filename        of READINGS_OOK or READINGS_FSK file
     * @param rawFileData     Raw data for the file in the memory
     * @param insertTimestamp insertion timestamp for these records
     * @param miuPacketBuilderMap Packet builder map to populate
     * @throws IOException       If something goes wrong reading the stream
     */
    private void extractPacketsFromReadingsFile(String filename, byte[] rawFileData, R900FileType fileType,
                                                Instant insertTimestamp, Map<Integer,
                                                R900MiuReadingsPacketBuilder> miuPacketBuilderMap) throws IOException, MdceDataException, InterruptedException
    {
        final ByteBuffer dataIn = ByteBuffer.wrap(rawFileData).order(ByteOrder.BIG_ENDIAN);
        extractGatewayHeaderFromReadingsFile(filename, dataIn, fileType, insertTimestamp);

        switch (fileType)
        {
            case OOK_READING:
                extractMiuPacketsFromOokReadingsFile(dataIn, miuPacketBuilderMap);
                break;
            case FSK_READING:
                extractMiuPacketsFromFskReadingsFile(dataIn, miuPacketBuilderMap);
                break;
            default:
                //not expected, do nothing
        }
    }

    private void extractMiuPacketsFromOokReadingsFile(ByteBuffer dataIn, Map<Integer, R900MiuReadingsPacketBuilder> miuPacketBuilderMap) throws IOException
    {
        //Add each 25 byte entry into DynamoDB
        while (dataIn.remaining() >= 25)
        {
            final Integer miuId = Integer.valueOf(R900MiuReadingsPacketBuilder.getMiuIdFromOokPacketInBigEndianByteBuffer(dataIn));
            R900MiuReadingsPacketBuilder miuPackets = miuPacketBuilderMap.get(miuId);
            if (miuPackets == null)
            {
                miuPackets = new R900MiuReadingsPacketBuilder(this.seqNumber);
                miuPacketBuilderMap.put(miuId, miuPackets);
            }
            miuPackets.addOokPacket(dataIn);
        }
    }

    private void extractMiuPacketsFromFskReadingsFile(ByteBuffer dataIn, Map<Integer, R900MiuReadingsPacketBuilder> miuPacketBuilderMap) throws IOException
    {
        //Add each 25 byte entry into DynamoDB
        while (dataIn.remaining() >= 25)
        {
            final Integer miuId = Integer.valueOf(R900MiuReadingsPacketBuilder.getMiuIdFromFskPacketInBigEndianByteBuffer(dataIn));
            R900MiuReadingsPacketBuilder miuPackets = miuPacketBuilderMap.get(miuId);
            if (miuPackets == null)
            {
                miuPackets = new R900MiuReadingsPacketBuilder(this.seqNumber);
                miuPacketBuilderMap.put(miuId, miuPackets);
            }
            miuPackets.addFskPacket(dataIn);
        }
    }
    private void extractGatewayHeaderFromReadingsFile(String filename, ByteBuffer dataIn, R900FileType fileType, Instant insertTimestamp) throws IOException, MdceDataException, InterruptedException
    {
        final ByteArrayOutputStream gatewayHeader = readGatewayHeaderAndAppendToFileInfo(filename, dataIn, insertTimestamp);

        final GatewayPacketReceivedDynamoItem gatewayPacket = buildGatewayPacketReceivedDynamoItem(filename, fileType, insertTimestamp, gatewayHeader);

        if(storeR900Packets)
        {
            logger.debug("Inserting Gateway packet " + filename + " into Dynamo as " + gatewayPacket.getCollectorId() + ":" + gatewayPacket.getInsertDate());
            this.packetSaver.insertGatewayPacketIntoDynamoSync(gatewayPacket);
        }
    }

    private ByteArrayOutputStream readGatewayHeaderAndAppendToFileInfo(String filename, ByteBuffer dataIn, Instant insertDate) throws IOException
    {
        final ByteArrayOutputStream gatewayHeader = new ByteArrayOutputStream();

        appendFileInfoLine(filename, gatewayHeader);

        final ByteArrayOutputStream currentGatewayConfigInfoLine = new ByteArrayOutputStream();

        while (readEachValueOfHeaderFromInputStreamReturnNotFinished(dataIn, gatewayHeader, currentGatewayConfigInfoLine, insertDate))
        {
            // this body deliberately left blank
        }

        return gatewayHeader;
    }

    /**
     * Insert the line File=<filename of file in tar><CR><LF> required at the start of all
     * gateway header packets
     * @param filename Filename to insert
     * @param outputStream Output stream to write the filename to
     */
    private void appendFileInfoLine(String filename, ByteArrayOutputStream outputStream)
    {
        final String initLine = "File=" + filename + "\r\n";
        try
        {
            outputStream.write(initLine.getBytes(StandardCharsets.ISO_8859_1));
        }
        catch (IOException e)
        {
            throw new RuntimeException("Unexpected IO exeption writing file info line in packet representation of " + filename);
        }

    }

    private boolean readEachValueOfHeaderFromInputStreamReturnNotFinished(ByteBuffer dataIn, ByteArrayOutputStream gatewayHeader,
                     ByteArrayOutputStream currentGatewayConfigInfoLine, Instant insertDate) throws IOException
    {
        final int dataByte = dataIn.get();

        if (dataByte < 0)
        {
            throw new MdceRestException("Unexpected end of READINGS OOK file while parsing header");
        }

        if (dataByte == ASCII_LF)
        {
            gatewayHeader.write(dataByte);
            final String gatewayHeaderLine = currentGatewayConfigInfoLine.toString("ISO-8859-1");
            if (gatewayHeaderLine.contains("DATA BEGINS HERE"))
            {
                return false;
            }
            else
            {
                final Matcher headerParser = GW_HEADER_ENTRY_PATTERN.matcher(gatewayHeaderLine);
                if (headerParser.matches())
                {
                    final String headerKey = headerParser.group(1);
                    final String headerValue = headerParser.group(2);
                    dataSaver.recordGatewayHeader(headerKey, headerValue);
                }
                else
                {
                    logger.warn("Malformed header in gateway tarball file? " + gatewayHeaderLine);
                }
            }
            currentGatewayConfigInfoLine.reset();
        }
        else if (dataByte < 32)
        {
            gatewayHeader.write(dataByte); //not interested in control codes in the header lines
        }
        else
        {
            gatewayHeader.write(dataByte);
            currentGatewayConfigInfoLine.write(dataByte);
        }
        return true;
    }

    /**
     *
     * @param miuId
     * @param packet Must not be null or contain no MIU packets
     * @param packetReceivedTime
     * @return Item to insert in Dynamo
     */
    private static MiuPacketReceivedDynamoItem buildMiuPacketReceivedDynamoItem(int miuId, R900MiuReadingsPacket packet, Instant packetReceivedTime)
    {

        MiuPacketReceivedDynamoItem miuPacket = new MiuPacketReceivedDynamoItem();

        miuPacket.setMiuId(miuId);
        miuPacket.setPacketData(packet.toByteSequence());
        miuPacket.setPacketType(DynamoItemPacketType.fromSystemAndId(packet.getPacketType().getId(), "R900").toString());
        miuPacket.setDateBuilt(packet.getNewestPacketBuiltDate().toEpochMilli());
        miuPacket.setSourceDeviceType("GW");
        miuPacket.setTargetDeviceType("MDCE");

        miuPacket.setInsertDate(packetReceivedTime.toEpochMilli());
        miuPacket.setDateReceived(packetReceivedTime.toEpochMilli());

        return miuPacket;
    }

    /**
     * Process a non-reading FSK file and store it
     *
     * @param filename Filename of file
     * @param dataIn   Input for data
     * @return Number of lines in FSK file
     * @throws IOException       If something goes wrong reading the stream
     */
    private int processFskFile(String filename, InputStream dataIn, Instant insertTimestamp, R900FileType fileType) throws IOException, MdceDataException, InterruptedException
    {
        DynamoItemPacketType packetType = DynamoItemPacketType.GW_FSK_MIU_CONFIGURATION;
        if (fileType == R900FileType.FSK_RATE)
        {
            packetType = DynamoItemPacketType.GW_FSK_MIU_RATE;
        } else if (fileType == R900FileType.FSK_UNKNOWN)
        {
            packetType = DynamoItemPacketType.GW_FSK_UNKNOWN;
        }

        final ByteArrayOutputStream packetData = new ByteArrayOutputStream(dataIn.available() + filename.length() + 7);
        appendFileInfoLine(filename, packetData);

        int lineCount = 0;
        while (true)
        {
            int b = dataIn.read();
            if (b < 0)
            {
                break;
            }
            if (b == ASCII_LF)
            {
                lineCount++;
            }
            packetData.write(b);
        }

        final GatewayPacketReceivedDynamoItem gatewayPacket = buildGatewayPacketReceivedDynamoItem(filename, packetType, insertTimestamp, packetData);

        logger.debug("Inserting Gateway packet " + filename + " into Dynamo as " + gatewayPacket.getCollectorId() + ":" + gatewayPacket.getInsertDate());
        this.packetSaver.insertGatewayPacketIntoDynamoSync(gatewayPacket);

        return lineCount;
    }


    private GatewayPacketReceivedDynamoItem buildGatewayPacketReceivedDynamoItem(String filename, R900FileType fileType, Instant insertTimestamp, ByteArrayOutputStream gatewayHeader)
    {
        DynamoItemPacketType packetType = DynamoItemPacketType.GW_OOK_GW_CONFIGURATION;
        if (fileType == R900FileType.FSK_READING)
        {
            packetType = DynamoItemPacketType.GW_FSK_GW_CONFIGURATION;
        }

        return buildGatewayPacketReceivedDynamoItem(filename, packetType, insertTimestamp, gatewayHeader);
    }

    private GatewayPacketReceivedDynamoItem buildGatewayPacketReceivedDynamoItem(String filename, DynamoItemPacketType packetType, Instant insertTimestamp, ByteArrayOutputStream gatewayFileData)
    {
        final Matcher fileDetails = getFileDetailsFromFilename(filename);

        this.gatewayId = GatewayId.valueOf(fileDetails.group(1) + "_" + fileDetails.group(2));

        final Calendar buildDate = getBuildDate(fileDetails);

        final GatewayPacketReceivedDynamoItem gatewayPacket = new GatewayPacketReceivedDynamoItem();

        gatewayPacket.setPacketType(packetType.toString());
        gatewayPacket.setCollectorId(gatewayId.toString());
        gatewayPacket.setPacketData(ByteBuffer.wrap(gatewayFileData.toByteArray()));
        gatewayPacket.setDateBuilt(buildDate.getTime().getTime() / 1000);
        gatewayPacket.setSourceDeviceType("GW");
        gatewayPacket.setTargetDeviceType("MDCE");
        gatewayPacket.setInsertDate(insertTimestamp.toEpochMilli() + packetType.getId()); //nudge to avoid collisions

        return gatewayPacket;
    }


    private Matcher getFileDetailsFromFilename(String filename)
    {
        Matcher fileDetails = INNER_FILENAME_PATTERN.matcher(filename);
        fileDetails.find(); // 3 and 4 for data/time

        return fileDetails;
    }

    private Calendar getBuildDate(Matcher fileDetails)
    {
        Calendar buildDate = Calendar.getInstance();
        buildDate.setTimeZone(TimeZone.getTimeZone("GMT"));
        buildDate.set(1899, Calendar.DECEMBER, 30);
        buildDate.add(Calendar.DATE, Integer.parseInt(fileDetails.group(3)));
        buildDate.add(Calendar.MINUTE, Integer.parseInt(fileDetails.group(4)));
        return buildDate;
    }


    protected byte[] getBytes(InputStream dataStream, int len)
    {
        final byte[] data = new byte[len];

        int writePos = 0;
        while (writePos < len)
        {
            int bytesRead = getBytesRead(dataStream, len, data, writePos);
            if (bytesRead < 0)
            {
                throw new MdceRestException("Unexpected end of stream");
            }
            writePos += bytesRead;
        }
        return data;
    }

    private int getBytesRead(InputStream dataStream, int len, byte[] data, int writePos)
    {
        try
        {
            return dataStream.read(data, writePos, len - writePos);
        } catch (IOException e)
        {
            throw new MdceRestException("IOException reading tar file bytes", e);
        }
    }
}