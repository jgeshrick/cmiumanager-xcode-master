/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.scheduled.mqttStatusCheck;

import com.neptunetg.common.alert.AlertSource;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.model.AlertLevel;
import com.neptunetg.mdce.common.internal.alert.model.AlertState;
import com.neptunetg.mdce.integration.domain.alert.AlertEmail;
import com.neptunetg.mdce.integration.domain.alert.AlertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.time.ZonedDateTime;

/**
 * Created by WJD1 on 02/03/2017.
 * A class to check that the MQTT status directory still exists
 */
@Service
@EnableScheduling
public class MqttStatusCheck
{
    public static final String MQTT_RESTARTED_FILENAME = ".mqtt-restarted";
    private final AlertService alertService;

    private static boolean directoryAlreadyCreated = false;

    @Value("#{envProperties['mqtt.tmpDir']}")
    String tmpFolder;

    @Autowired
    public MqttStatusCheck(AlertService alertService)
    {
        this.alertService = alertService;
    }

    @Scheduled(fixedDelay = 1000*60*5) //Run every 5 minutes
    private void checkMqttStatusDirectoryExists() throws InternalApiException
    {
        File folder = new File(tmpFolder);
        File[] listOfFiles = folder.listFiles();

        boolean containsMqttFolder = false;

        if (listOfFiles != null)
        {
            for (File file : listOfFiles)
            {
                if (file.isDirectory() && file.getName().contains("mqtt"))
                {
                    directoryAlreadyCreated = true;
                    containsMqttFolder = true;
                }
            }
        }

        if(directoryAlreadyCreated)
        {
            final AlertSource alertSource = AlertSource.MQTT_MISSING_DIRECTORY;
            final AlertDetails alertDetails = new AlertDetails();

            alertDetails.setAlertLevel(AlertLevel.ERROR);
            alertDetails.setAlertState(AlertState.NEW);
            alertDetails.setAlertSource(alertSource.toString());
            alertDetails.setRecentMessage("Cannot find MQTT directory in " +
                    folder.getAbsolutePath() + " , commands cannot be sent to CMIUs until this is fixed");
            alertDetails.setAlertCreationDate(ZonedDateTime.now());

            if (!containsMqttFolder)
            {
                alertDetails.setAlertState(AlertState.NEW);
            } else if (containsMqttFolder)
            {
                alertDetails.setAlertState(AlertState.STALE);
            }

            alertService.addAlertAndSendEmail(alertDetails);
        }
    }

    /**
     * The mqtt process creates a file ".mqtt-restarted" if it found that the working directory was deleted. This
     * method creates an alert if it finds ".mqtt-restarted" in the temp directory.
     * @throws InternalApiException
     */
    @Scheduled(fixedDelay = 1000*60*5) //Run every 5 minutes
    private void checkMqttRestartedBreadcrumb() throws InternalApiException
    {
        boolean wasMqttRestarted = false;

        String path = tmpFolder + (tmpFolder.endsWith(File.separator) ? "" : File.separator) + MQTT_RESTARTED_FILENAME;
        File breadcrumb = new File(path);

        if (breadcrumb.exists())
        {
            breadcrumb.delete();
            wasMqttRestarted = true;
        }

        if(wasMqttRestarted)
        {
            final AlertEmail email = alertService.initAlertEmail();
            final AlertSource alertSource = AlertSource.MQTT_MISSING_DIRECTORY;
            final AlertDetails alertDetails = new AlertDetails();

            alertDetails.setAlertLevel(AlertLevel.ERROR);
            alertDetails.setAlertState(AlertState.NEW);
            alertDetails.setAlertSource(alertSource.toString());
            alertDetails.setRecentMessage("MQTT directory has been recreated by the mqtt process.");
            alertDetails.setAlertCreationDate(ZonedDateTime.now());

            alertService.addAlert(alertDetails, email);
            alertService.sendEmail(email);
        }
    }
}
