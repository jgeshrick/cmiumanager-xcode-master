/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.rest.json.cmiucellularconfig;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JsonCmiuCellularConfigModemConfig
{
    @JsonProperty("vendor")
    private String vendor = "Telit";

    @JsonProperty("firmware_revision")
    private String firmwareRevision = "0.0.0.0";

    @JsonProperty("imei")
    private String imei = "123456789101112";

    public String getVendor()
    {
        return this.vendor;
    }

    public void setVendor(String vendor)
    {
        this.vendor = vendor;
    }

    public String getFirmwareRevision()
    {
        return this.firmwareRevision;
    }

    public void setFirmwareRevision(String firmwareRevision)
    {
        this.firmwareRevision = firmwareRevision;
    }

    public String getImei()
    {
        return this.imei;
    }

    public void setImei(String imei)
    {
        this.imei = imei;
    }
}
