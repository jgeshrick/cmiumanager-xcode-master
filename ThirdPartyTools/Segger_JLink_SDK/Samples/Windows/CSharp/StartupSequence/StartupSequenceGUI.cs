﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using U8  = System.Byte;   // unsigned char
using U16 = System.UInt16; // unsigned short
using U32 = System.UInt32; // unsigned int
using U64 = System.UInt64; // unsigned long long

using I8  = System.SByte; // signed char
using I16 = System.Int16; // signed short
using I32 = System.Int32; // signed int
using I64 = System.Int64; // signed long long

namespace WindowsFormsApplication1
{
    public partial class StartupSequenceGUI : Form
    {   
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void JLINKARM_LOG(sbyte[] s);

        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_Open", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_Open();
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_SelectIP", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_SelectIP(string sHost, int Port);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_EMU_SelectByUSBSN", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_EMU_SelectByUSBSN(int SerilaNo);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_OpenEx", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_OpenEx(JLINKARM_LOG pfLog, JLINKARM_LOG pfErrorOut);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_ExecCommand", CallingConvention = CallingConvention.Cdecl)]
        unsafe public extern static Int32 JLINKARM_ExecCommand(sbyte[] pIn, out sbyte[] pOut, int BufferSize);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_TIF_Select", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_TIF_Select(int Interface);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_SetSpeed", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_SetSpeed(int Speed);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_Connect", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_Connect();
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_Close", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_Close();
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_ReadMemU32", CallingConvention = CallingConvention.Cdecl)]
        unsafe public extern static int JLINKARM_ReadMemU32(U32 Addr, U32 NumItems, U32[] pData, U8[] pStatus);

        
        private void Log(String Text)
        {
            TxtLog.Text += Text;
            TxtLog.SelectionStart = TxtLog.Text.Length;
            TxtLog.ScrollToCaret();
        }

        private static void JLINKARM_LOG_callback_pfErrorOut(sbyte[] s)
        {
            
        }

        private static JLINKARM_LOG pJLINKARM_LOG_callback_pfErrorOut = JLINKARM_LOG_callback_pfErrorOut;

        public StartupSequenceGUI()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int Result;
            sbyte[] acOut = new sbyte[256];
            sbyte[] acIn  = new sbyte[256];
            U32[] acData = new U32[16];
            U8[] acTmp = new U8[100];
            U32 NumItems;
            U32 Addr;

            if (RdoIFUSB.Checked == true)      // Interface USB selected?
            { 
                Log("USB Selected\n");
                if (TxtUSB.Text != "0") 
                {
                    Result = JLINKARM_EMU_SelectByUSBSN(Convert.ToInt32(TxtUSB.Text));
                    if (Result < 0)
                    {
                        Log("Failed to select J-Link via USB serial number.\n");
                        goto Done;
                    }
                
                }             
            }
            else if (RdoIFIP.Checked == true)    // Interface ethernet selected?
            {
                Log("IP Selected\n");
                if (TxtIP.Text != "")
                {
                    Result = JLINKARM_SelectIP(TxtIP.Text, 0);
                    if (Result < 0)
                    {
                        Log("Failed to select J-Link via IP.\n");
                        goto Done;
                    }
                }
            }
            else                                // No interface selected?
            {
                Log("Failed to get J-Link connection.\n" );
                goto Done;
            }
            //
            // Open connection to J-Link
            //
            Log("Connecting to J-Link...");

            Result = JLINKARM_OpenEx(null, pJLINKARM_LOG_callback_pfErrorOut);
            if (Result < 0)
            {
              Log("Failed.\n");
              goto Done;
            }
            Log("Successfully.\n");
            //
            // Setup settings file
            //
            acIn = Array.ConvertAll((Encoding.ASCII.GetBytes("ProjectFile = " + TxtPrjFile.Text)), q => Convert.ToSByte(q));
            JLINKARM_ExecCommand(acIn, out acOut, 256);
            //
            // Select device
            //
            acIn = Array.ConvertAll((Encoding.ASCII.GetBytes("Device = " + TxtDevice.Text)), q => Convert.ToSByte(q));
            JLINKARM_ExecCommand(acIn, out acOut, 256);
            //
            // Select target interface (JTAG / SWD /...)
            //
            if (CmbTIF.Text == "JTAG")
            {
              JLINKARM_TIF_Select(0);           // JTAG
            }
            else {         
              JLINKARM_TIF_Select(1);           // SWD
            }
            //
            // Select target interface speed
            //
            JLINKARM_SetSpeed(Convert.ToInt32(NumTIFSpeed.Value));
            //
            // Connect to the target device
            //
            Result = JLINKARM_Connect();
            Log("Connecting to the target...");
            if (Result < 0)
            {
              Log("Failed.\n");
              goto Close;
            }
            Log("Successfully.\n");
            //
            // Read 16 bytes from the target
            //
            NumItems = 16;                                  // Number of items to read
            Addr = Convert.ToUInt32(TxtAddr.Text, 16);          // Get Addr
            Result = JLINKARM_ReadMemU32(Addr, NumItems, acData, acTmp);
            Log("Trying to read " + NumItems + " bytes...");
            if (Result < 0)                                 // Error occured
            {
              Log("Failed.\n");
              goto Close;
            }
            else 
            {
                Log("Successfully.\n");
                //
                // Output bytes
                //
                for (int i = 0; i < NumItems; i += 4)
                {
                    TxtReadMem.Text += String.Format("{0:X8}", acData[i + 0]) + " ";
                    TxtReadMem.Text += String.Format("{0:X8}", acData[i + 1]) + " ";
                    TxtReadMem.Text += String.Format("{0:X8}", acData[i + 2]) + " ";
                    TxtReadMem.Text += String.Format("{0:X8}", acData[i + 3]) + "\n";
                }
            }
            //
            // Close connection
            //
        Close:
            JLINKARM_Close();
            Log("Disconnected from target and J-Link.\n");
        Done:
            ;
        }
    }
}
