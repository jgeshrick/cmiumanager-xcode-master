/*********************************************************************
*              SEGGER MICROCONTROLLER SYSTEME GmbH                   *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*          (c) 2006 SEGGER Microcontroller Systeme GmbH              *
*                                                                    *
*       Internet: www.segger.com Support: support@segger.com         *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File    : Main.c
Purpose : JLinkDCCSample
---------------------------END-OF-HEADER------------------------------
*/

#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include "JLinkARMDLL_Lib.h"

/*********************************************************************
*
*       Defines, configurable
*
**********************************************************************
*/

#define DCC_TIMEOUT       10                  // Timeout [ms] for a single data item.
#define USE_DCC_FAST       0

/*********************************************************************
*
*       Defines
*
**********************************************************************
*/

#define COUNTOF(a)        (sizeof(a)/sizeof(a[0]))
#define CMD_READ           2                          // Read data from target
#define CMD_WRITE          1                          // Write data to target

/*********************************************************************
*
*       Types
*
**********************************************************************
*/

typedef struct {
  int           (*pfCmd)    (const char* s);
  const char *  sName;
} COMMAND;

/*********************************************************************
*
*       _EatWhite
*/
static void _EatWhite(const char** ps) {
  const char* s = *ps;
  while ((*s == ' ') || (*s == '\t') || (*s == '\r') || (*s == '\n')) {
    s++;
  }
  *ps = s;
}

/*********************************************************************
*
*       _CompareCmd
*
*  Return value
*    0    Equal
*    1    Not equal
*/
static char _CompareCmd(const char ** ps, const char * sCmd) {
  const char *s;
  s = *ps;
  do {
    char c;
    char c1;

    c  = toupper(*sCmd++);
    c1 = toupper(*s);
    if (c == 0) {
      if ((c1 == 0x20) || (c1 == 0x3D) || (c1 == 0)) {
        *ps = s;
        return 0;       // Command found
      }
      return 1;         // Command not found
    }
    if (c != c1) {
      return 1;         // Command not found
    }
    s++;
  } while (1);
}

/*********************************************************************
*
*       _TestDCCRead
*
*  Parameters
*    NumItems  Number of 32-bit items to write
*
*  Return values
*    0 O.K.
*    1 Error
*/
static int _TestDCCRead(U32 NumItems, int TimeOut) {
  U32           r;
  U32           Cmd;
  int           NumItemsRead = 0;
  int           i;
  int           ProgSpeed;
  unsigned      Time;
  unsigned      StartTime;
  U32 *         pData;

  pData = malloc(NumItems * 4);
  Cmd = CMD_READ;
  if (USE_DCC_FAST) {
    JLINKARM_WriteDCCFast(&Cmd, 1);
    JLINKARM_WriteDCCFast(&NumItems, 1);
  } else {
    r = JLINKARM_WriteDCC(&Cmd, 1, TimeOut);
    if(r < 1) {
      goto Failed;
    }
    r = JLINKARM_WriteDCC(&NumItems, 1, TimeOut);
    if(r < 1) {
      goto Failed;
    }
  }
  printf("Reading %d items (%d KB)... ", NumItems, ((NumItems * 4) / 1024));
  StartTime = GetTickCount();
  if (USE_DCC_FAST) {
    JLINKARM_ReadDCCFast(pData, NumItems);
  } else {
    NumItemsRead = JLINKARM_ReadDCC(pData, NumItems, TimeOut);
    if ((U32)NumItemsRead < NumItems) {
      printf("ERROR: %d of %d items read successfully.\n", NumItemsRead, NumItems);
      free(pData);
      return 1;
    }
  }
  Time = GetTickCount() - StartTime;
  ProgSpeed = (Time) ? ((NumItems * 4) / Time) : 0;
  for (i = 0; i < (int)NumItems; i++) {
    if (*(pData + i) != (U32)i) {
      printf("ERROR: Expected 0x%.8X, got: %.8X\n", i, *(pData + i)); 
      free(pData);
      return 1;
    }
  }
  printf("O.K. - (%d.%.3d sec) (%d KB/sec)\n", Time / 1000, Time % 1000, ProgSpeed);
  free(pData);
  return 0;
Failed:
  printf("ERROR: Could not write DCC data.\n");
  return 1;
}

/*********************************************************************
*
*       _TestDCCWrite
*
*  Parameters
*    NumItems  Number of 32-bit items to write
*
*  Return values
*    0 O.K.
*    1 Error
*/
static int _TestDCCWrite(U32 NumItems, int TimeOut) {
  U32           u;
  U32           r;
  U32           Sum;
  U32           Data;
  int           ProgSpeed;
  unsigned      Time;
  unsigned      StartTime;
  U32 *         pData;

  pData = malloc(NumItems * 4);
  Data = CMD_WRITE;
  if (USE_DCC_FAST) {
    JLINKARM_WriteDCCFast(&Data, 1);
    JLINKARM_WriteDCCFast(&NumItems, 1);
  } else {
    r = JLINKARM_WriteDCC(&Data, 1, TimeOut);
    r = JLINKARM_WriteDCC(&NumItems, 1, TimeOut);
  }
  Sum = 0;
  for (u = 0; u < NumItems; u++) {
    *(pData + u) = u;
    Sum += u;
  }
  printf("Writing %d items (%d KB)... ", NumItems, ((NumItems * 4) / 1024));
  StartTime = GetTickCount();
  if (USE_DCC_FAST) {
    JLINKARM_WriteDCCFast(pData, NumItems);
    JLINKARM_ReadDCCFast(&Data, 1);
  } else {
    r = JLINKARM_WriteDCC(pData, NumItems, TimeOut);
    if(r < NumItems) {
      printf("ERROR: %d of %d items written successfully.\n", r, NumItems);
      return 1;
    }
    r = JLINKARM_ReadDCC(&Data, 1, TimeOut);
    if(r < 1) {
      printf("ERROR: Could not read DCC data.\n");;
    }
  }
  if (Data != Sum) {
    printf(" ERROR: Expected: %.8X, got: %.8X\n", Sum, Data);
    return 1;
  }
  Time = GetTickCount() - StartTime;
  ProgSpeed = (Time) ? ((NumItems * 4) / Time) : 0;
  printf(" O.K. - %d.%.3d sec (%d KB/sec)\n", Time / 1000, Time % 1000, ProgSpeed);
  return 0;
}


/*********************************************************************
*
*       _ExecTestDCCReadTimeOut
*
*  Return values
*    0 O.K.
*    1 Error
*/
static int _ExecTestDCCReadTimeOut(const char * s) {
  U32 aCmd[2] = {CMD_READ, 0x100};
  U32 NumItemsExpected;
  U32 NumItemsRequested;
  U32 * pData;
  int r = 0;
  int NumItemsRead;

  printf("Testing DDC read timeout behaviour...");
  r = JLINKARM_WriteDCC(aCmd, COUNTOF(aCmd), DCC_TIMEOUT);
  if(r != COUNTOF(aCmd)) {
    printf("Could not write DCC data.\n");
    return 1;
  }
  NumItemsExpected = aCmd[1];
  NumItemsRequested = NumItemsExpected + 50;
  pData = malloc(NumItemsRequested * 4);
  NumItemsRead = JLINKARM_ReadDCC(pData, NumItemsRequested, DCC_TIMEOUT);
  if ((U32)NumItemsRead != NumItemsExpected) {
    printf("\nERROR: Expected %d items read successfully, got %d\n", NumItemsExpected, NumItemsRead);
    free(pData);
    return 1;
  }
  printf(" O.K.\n");
  free(pData);
  return 0;  
}

/*********************************************************************
*
*       _ExecTestDCCWriteTimeOut
*
*  Return values
*    0 O.K.
*    1 Error
*/
static int _ExecTestDCCWriteTimeOut(const char * s) {
  U32 aData[10];
  U32 ac[10] = {CMD_READ, 10, 0, 0, 0, 0, 0, 0, 0, 0};
  int r = 0;
  const int NumItemsExpected = 3;
  
  printf("Testing DCC write timeout behaviour...");
  r = JLINKARM_WriteDCC(ac, COUNTOF(ac), DCC_TIMEOUT);
  if (r != NumItemsExpected) {
    printf("\nERROR: Expected %d NumItems written successfully, got %d\n", NumItemsExpected, r);
//    printf("ERROR: %d of %d items written successfully.\n", r, COUNTOF(ac));
    JLINKARM_ReadDCC(aData, 10, DCC_TIMEOUT);
    return 1;
  } else {
    JLINKARM_ReadDCC(aData, 10, DCC_TIMEOUT);
  }
  printf(" O.K.\n");
  return 0;
}

/*********************************************************************
*
*       _ExecTestDCCRead
*
*  Return values
*    0 O.K.
*    1 Error
*/
static int _ExecTestDCCRead(const char * s) {
  int r = 0;

  r = _TestDCCRead(0x800, DCC_TIMEOUT);         // 0x00800 * 4 bytes = 0x02000  bytes (8 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCRead(0x1000, DCC_TIMEOUT);        // 0x01000 * 4 bytes = 0x04000  bytes (16 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCRead(0x2000, DCC_TIMEOUT);        // 0x02000 * 4 bytes = 0x08000  bytes (32 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCRead(0x4000, DCC_TIMEOUT);        // 0x04000 * 4 bytes = 0x10000  bytes (64 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCRead(0x8000, DCC_TIMEOUT);        // 0x08000 * 4 bytes = 0x20000  bytes (128 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCRead(0x10000, DCC_TIMEOUT);       // 0x10000 * 4 bytes = 0x40000  bytes (256 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCRead(0x20000, DCC_TIMEOUT);       // 0x20000 * 4 bytes = 0x80000  bytes (512 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCRead(0x40000, DCC_TIMEOUT);       // 0x40000 * 4 bytes = 0x100000  bytes (1024 KB)
  if (r) {
    return 1;
  }
  return 0;
}

/*********************************************************************
*
*       _ExecTestDCCWrite
*
*  Return values
*    0 O.K.
*    1 Error
*/
static int _ExecTestDCCWrite(const char * s) {
  int r;

  r = _TestDCCWrite(0x800, DCC_TIMEOUT);        // 0x00800 * 4 bytes = 0x02000  bytes (8 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCWrite(0x1000, DCC_TIMEOUT);       // 0x01000 * 4 bytes = 0x04000  bytes (16 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCWrite(0x2000, DCC_TIMEOUT);       // 0x02000 * 4 bytes = 0x08000  bytes (32 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCWrite(0x4000, DCC_TIMEOUT);       // 0x04000 * 4 bytes = 0x10000  bytes (64 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCWrite(0x8000, DCC_TIMEOUT);       // 0x08000 * 4 bytes = 0x20000  bytes (128 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCWrite(0x10000, DCC_TIMEOUT);      // 0x10000 * 4 bytes = 0x40000  bytes (256 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCWrite(0x20000, DCC_TIMEOUT);      // 0x20000 * 4 bytes = 0x80000  bytes (512 KB)
  if (r) {
    return 1;
  }
  r = _TestDCCWrite(0x40000, DCC_TIMEOUT);      // 0x40000 * 4 bytes = 0x100000  bytes (1024 KB)
  if (r) {
    return 1;
  }
  return 0;
}

/*********************************************************************
*
*       Command list
*/
static const COMMAND _aCmd[] = {
  { _ExecTestDCCRead,         "r"   },
  { _ExecTestDCCWrite,        "w"   },
  { _ExecTestDCCReadTimeOut,  "rt"  },
  { _ExecTestDCCWriteTimeOut, "wt"  }
};

/*********************************************************************
*
*       _ExecCommandLine
*/
static char _ExecCommandLine(const char* s) {
  int i;
  //
  // Find and execute command
  //
  _EatWhite(&s);
  for (i = 0; i < COUNTOF(_aCmd); i++) {
    if (_CompareCmd(&s, _aCmd[i].sName) == 0) {
      _aCmd[i].pfCmd(s);
      return 1;
    }
  }
  //
  // Catch quit
  //
  if (_CompareCmd(&s, "q") == 0) {
    return 0;
  }
  printf(
         "r    Test DCC read communication.\n"
         "w    Test DCC write communication.\n"
         "rt   Test DCC read communication timeout.\n"
         "wt   Test DCC write communication timeout.\n"
        );
  return 1;
}

/*********************************************************************
*
*       _ClrDCCBuffer
*/
static void _ClrDCCBuffer(void) {
  U32 Data;

  JLINKARM_Halt();
  if (USE_DCC_FAST) {
    JLINKARM_ReadDCCFast(&Data, 1);                      // Dummy read to be sure that DCC reg is empty
  } else {
    JLINKARM_ReadDCC(&Data, 1, DCC_TIMEOUT);             // Dummy read to be sure that DCC reg is empty
  }
  JLINKARM_Go();
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
/*********************************************************************
*
*       main
*/
void main(void) {
  char ac[128];
  const char * sErr;
  
  printf("SEGGER J-Link DCC communication sample.\n");
  printf("Compiled " __DATE__ " " __TIME__ "\n");
  printf("(c) 2009 SEGGER Microcontroller GmbH & Co. KG, www.segger.com\n");
  printf("         Solutions for real time microcontroller applications\n\n");
  //
  // Connect to J-Link
  //
  printf("Connecting...\n");
  sErr = JLINKARM_Open();
  if (sErr) {
    printf("%s\n", sErr);
    goto Cleanup;
  }
  JLINKARM_SetSpeed(12000);
  printf("JTAG speed: %d kHz\n", JLINKARM_GetSpeed());
  _ClrDCCBuffer();
  do {
    printf("J-Link>");
    fgets(&ac[0], sizeof(ac), stdin);
  } while (_ExecCommandLine(&ac[0]) == 1);
Cleanup:
  JLINKARM_Close();
  Sleep(2000);
}

/*************************** end of file ****************************/
