/*********************************************************************
*              SEGGER MICROCONTROLLER SYSTEME GmbH                   *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2002-2005 SEGGER Microcontroller Systeme GmbH           *
*                                                                    *
* Internet: www.segger.com Support: support@segger.com               *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File    : main.c
Purpose : Jlink main program
---------------------------END-OF-HEADER------------------------------
*/

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <limits.h>
#include "JLinkARMDLL.h"
#include "Version.h"
#include "main.h"

#ifdef WIN32
  #include <conio.h>
  #include <windows.h>
#else
  #include <stdlib.h>
  #include <termios.h>
  #include <sys/ioctl.h>
  #include <fcntl.h>
  #include <limits.h>
  #include <time.h>
  #include <unistd.h>
  #include <ctype.h>
#endif

#ifdef linux
  #include <dlfcn.h>
#endif

#ifdef __APPLE__
  #include <CoreServices/CoreServices.h>
  #include <mach/mach.h>
  #include <mach/mach_time.h>
  #include <readline/readline.h>
  #include <readline/history.h>
#endif

/*********************************************************************
*
*       Defines, configurable
*
**********************************************************************
*/

#ifdef _DEBUG
  #define _PRINT_RETVAL(a) //printf a
  #define MEASURE_DLL_INIT_TIME  0 // 1
#else
  #define _PRINT_RETVAL(a)
  #define MEASURE_DLL_INIT_TIME  0
#endif

#define MAX_TIF_SPEED_KHZ_SUPPORTED (50000)

#define DEFAULT_TIF_SPEED_KHZ       4000

#define DEFAULT_FAST_SPEED          100
#define DEFAULT_SLOW_SPEED          4
#define DEFAULT_RAMADDR             0x0
#define MAX_NUM_WRITE_ITEMS         32
#define MAX_NUM_READ_DESCS          8
#define MAX_NUM_DEVICES             16
#define MAX_BLOCK_SIZE_FILE_IO      0x100000
#define UDP_DISCOVER_PORT           19020
#define SCRIPTFILE_DEFAULT_NAME     "Default.c"
#define SETTINGSFILE_DEFAULT_NAME   "Default.ini"
#define POWERTRACE_SWO_DEFAULT_SPEED 500000
#ifdef WIN32
  #define LOGFILE_NAME              "C:\\Work\\JLink.log"
  #define FILE_NAME_POWERTRACE_TEST "C:\\Work\\POWERTRACE_Test.txt"
#else
  #define LOGFILE_NAME              "JLink.log"
  #define FILE_NAME_POWERTRACE_TEST "POWERTRACE_Test.txt"
#endif

#define FLASH_DL_INTERVAL       5
#define FLASH_DL_NUM_INTERVALS  (100/FLASH_DL_INTERVAL)

/*********************************************************************
*
*       Defines
*
**********************************************************************
*/

#define JLINK_DEFAULT_SIZEOF_CONFIG       (0x100)

#define EMU_CAP_EX_TEST_NET                   (36)              // Supports command "EMU_CMD_TEST_NET"
#define EMU_CMD_TEST_NET                      239               // 0xEF
#define MAX_NUM_BYTES_TEST_NET                (4100)

#define JLINK_NOERROR                     0
#define JLINK_ERROR_UNKNOWN              -1
#define JLINK_ERROR_SYNTAX               -2
#define JLINK_ERROR_CANCELED_BY_USER     -3
#define JLINK_ERROR_WARNING              -4
#define JLINK_ERROR_INFO                 -5
#define JLINK_ERROR_ALL                  -5

#define HW_INFO_POWER_ENABLED             0
#define HW_INFO_POWER_OVERCURRENT         1
#define HW_INFO_ITARGET                   2
#define HW_INFO_ITARGET_PEAK              3
#define HW_INFO_ITARGET_PEAK_OPERATION    4
#define HW_INFO_ITARGET_MAX_TIME0        10
#define HW_INFO_ITARGET_MAX_TIME1        11
#define HW_INFO_ITARGET_MAX_TIME2        12
#define HW_INFO_ITARGET_NCAL_MA          13
#define HW_INFO_IP_ADDR                  16
#define HW_INFO_IP_MASK                  17
#define HW_INFO_IP_GW                    18
#define HW_INFO_IP_DNS0                  19
#define HW_INFO_IP_DNS1                  20

#define UDP_DISCOVER_OFF_IP_ADDR         (0x10)
#define UDP_DISCOVER_OFF_HW_ADDR         (0x20)
#define UDP_DISCOVER_OFF_SN              (0x30)
#define UDP_DISCOVER_OFF_HW_VERSION      (0x34)
#define UDP_DISCOVER_OFF_PRODUCT_NAME    (0x40)
#define UDP_DISCOVER_OFF_NICKNAME        (0x60)

#define EMU_IP_ERROR                     -1
#define EMU_IP_FIXED                      0
#define EMU_IP_DHCP_NO_CONFIG             1
#define EMU_IP_DHCP_CONFIGURED            2

#define MIN(a, b)     (((a) < (b)) ? (a) : (b))
#define MAX(a, b)     (((a) > (b)) ? (a) : (b))

#define FILE_FLAG_READ          (1 << 0)                    // Open the file for reading.
#define FILE_FLAG_SHARE_READ    (1 << 2)                    // Allow other processes to open the same file for reading.
#define FILE_FLAG_SHARE_WRITE   (1 << 3)                    // Allow other processes to open the same file for writing.
#define FILE_FLAG_CREATE        (1 << 4)                    // Create new file. If the file already exist, open it.
#define FILE_FLAG_TRUNC         (1 << 5)                    // Truncate the file to zero length.
#define FILE_FLAG_WRITE         (1 << 1)                    // Open the file for writing.
#define FILE_FLAG_APPEND        (1 << 6)                    // Set file pointer to end of file.
#define FILE_FLAG_EXCL          (1 << 7)                    // Can be specified in combination with FILE_FLAG_CREATE. Open fails if the specified file already exists.

#ifndef WIN32
  #define MAX_PATH              PATH_MAX
  #define getch                 getchar
  #define _getch                getchar
  #define INVALID_HANDLE_VALUE  (-1)
  #define _MAX_PATH             PATH_MAX
  #define HANDLE                int
#endif

#define ITARGET_MAX_FLUCTUATION         100000  // Max. fluctuation is app. 100 mA due to 1% fluctuation of the resistors
#define ITARGET_SAMPLES                 20
#define ITARGET_OFFSET_FLAG             (1 << 31)
#define ITARGET_OFFSET_ADDR             0x80

#define DP_REG_IDCODE      0      // On read
#define DP_REG_ABORT       0      // On write
#define DP_REG_CTRL_STAT   1
#define DP_REG_WCR         1
#define DP_REG_RESEND      2
#define DP_REG_SELECT      2
#define DP_REG_RD_BUF      3      // Read buffer register

#define DP_ABORT_MASK_ORUNERRCLR        (1<<4)
#define DP_ABORT_MASK_WDERRCLR          (1<<3)
#define DP_ABORT_MASK_STKERRCLR         (1<<2)
#define DP_ABORT_MASK_STKCMPCLR         (1<<1)
#define DP_ABORT_MASK_DAPABORT          (1<<0)

#define DP_CTRLSTAT_MASK_CSYSPWRUPREQ   (1 << 30)            // System power up
#define DP_CTRLSTAT_MASK_CDBGPWRUPREQ   (1 << 28)            // Debug power up
#define DP_CTRLSTAT_MASK_STICKYERR      (1 << 5)             // Sticky error

#define ACK_OK                          1
#define ACK_WAIT                        2
#define ACK_FAULT                       4

#define POWERTRACE_REFC_NO_REFC                  0
#define POWERTRACE_REFC_SWO_NUMBYTES_TRANSMITTED 1

#define S32_CALLH(Off, NumParas)              (3 | ((NumParas) << 4) | (0 << 7) | ((Off) << 8))
#define S32_MOVI7(iRegDest, v)                (6 | ((iRegDest) << 12) | ((v) << 5))
#define S32_MOVI32(iRegDest, v)               (22| ((iRegDest) << 12)), (((U32)(v) << 16) >> 16), ((U32)(v) >> 16)
#define S32_MOV(iRegDest, iRegSrc, Off)       (5 | ((Off) << 4) | ((iRegDest) << 8) | ((iRegSrc) << 12))
#define S32_PUSH(iRegFirst, NumRegs)          (7 | ((iRegFirst) << 8) | ((NumRegs) << 12))
#define S32_POP(iRegFirst, NumRegs)           (23| ((iRegFirst) << 8) | ((NumRegs) << 12))
#define S32_BRK                               0
#define S32_BNZ(Disp)                         (0x02 | ((Disp) << 8))
#define S32_BZ(Disp)                          (0x12 | ((Disp) << 8))
#define S32_ADD(iRegDest, iRegSrc)            (0x01 | ((iRegDest) << 8) | ((iRegSrc) << 12))
#define S32_ADDI4(iRegDest, UImm4)            (0x11 | ((iRegDest) << 8) | ((UImm4) << 12))
#define S32_ADDI7_NO_FLAG(iRegDest, UImm7)    (0x09 | ((iRegDest) <<12) | ((UImm7) << 5))
#define S32_SUB(iRegDest, iRegSrc)            (0x21 | ((iRegDest) << 8) | ((iRegSrc) << 12))
#define S32_SUBI4(iRegDest, UImm4)            (0x31 | ((iRegDest) << 8) | ((UImm4) << 12))
#define S32_AND(iRegDest, iRegSrc)            (0x41 | ((iRegDest) << 8) | ((iRegSrc) << 12))
#define S32_ANDI4(iRegDest, UImm4)            (0x51 | ((iRegDest) << 8) | ((UImm4) << 12))
#define S32_LOADB(iRegVal, iRegAddr)          (8 | (0 << 4) | (1 << 7) | ((iRegVal) << 8) | ((iRegAddr) << 12))
#define S32_LOADH(iRegVal, iRegAddr)          (8 | (1 << 4) | (1 << 7) | ((iRegVal) << 8) | ((iRegAddr) << 12))
#define S32_LOADW(iRegVal, iRegAddr)          (8 | (2 << 4) | (1 << 7) | ((iRegVal) << 8) | ((iRegAddr) << 12))
#define S32_LOADWBITPOS(iRegVal, iRegBitPos)  (8 | (3 << 4) | (1 << 7) | ((iRegVal) << 8) | ((iRegBitPos) << 12))
#define S32_STOREB(iRegVal, iRegAddr)         (8 | (0 << 4) | (0 << 7) | ((iRegVal) << 8) | ((iRegAddr) << 12))
#define S32_STOREH(iRegVal, iRegAddr)         (8 | (1 << 4) | (0 << 7) | ((iRegVal) << 8) | ((iRegAddr) << 12))
#define S32_STOREW(iRegVal, iRegAddr)         (8 | (2 << 4) | (0 << 7) | ((iRegVal) << 8) | ((iRegAddr) << 12))
#define S32_DBNZ(iReg, Disp)                  (10 | ((-(Disp)) << 5) | ((iReg) << 12))

#define VIRTUAL_COM_PORT_OFFSET   (0x89)
#define CONFIG_OFF_HW_FEATURES    (0x8E)
#define CONFIG_OFF_LIC_AREA       (0xB0)

/*********************************************************************
*
*       Pragmas
*
**********************************************************************
*/

#pragma warning(disable : 4761)

/*********************************************************************
*
*       Types
*
**********************************************************************
*/

typedef struct {
  int          (*pfCmd)    (const char* s);
  const char*  sLongName;
  const char*  sShortName;
  char         NeedsJLinkConnection;
  char         NeedsTargetConnection;
} _COMMAND_INFO;

typedef struct {
  U16          ManufacturerID;
  const char * sName;
} DEVICE_MANUFACTURER;

typedef struct {
  U32 StackPtr;
  U16 aCode[100];
  U8 aDataIn[0x100];
  U8 aDataDir[0x100];
  U8 aDataOut[0x100];
  U32 aStack[20];
} PCODE_PROGRAM;

typedef struct {
  U32  InitTIFSpeedkHz;
  U32  InitTIFSpeedkHzSet;
  U32  TargetIF;
  U32  HostIF;
  U32  EmuSN;                        // EmuSN 0-3 are mapped to USB 0-3 connection method. This method is no longer recommended since it limits the number of connected J-Links to 4.
  int  JTAGConfIRPre;
  int  JTAGConfDRPre;
  U8   ScriptModeActive;             // Commander script passed to J-Link Commander. Will be automatically executed.
  U8   TargetIFSet;
  U8   CloseRequested;
  U8   DoAutoConnect;
  U8   JTAGConfSet;
  char acIPAddr[128];
  char acDeviceName[128];
  char acJLinkScriptFile[512];
  char acCommanderScriptFile[512];
  char acSettingsFile[512];
  struct {
    unsigned ConnectedToJLink;
    unsigned ConnectedToTarget;
  } Status;
} COMMANDER_SETTINGS;

typedef struct {
  const char* sName;
  int (*pfCmd)(COMMANDER_SETTINGS* pCommanderSettings, const char* sVal);
} CMDLINE_OPTION;

typedef struct {
  const char* sName;
  char Shortcut;
  U32  TIFVal;
  const U32* paSupportedCores;
} _TIF_LIST;

typedef struct {
  U32 DevFamily;
  U32 iRegPC;
} _DEV_INFO;

typedef struct {
  const char* sAction;
  const char* sDescription;
} FLASHDL_ACTION;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/

static U32                _RAMAddr = DEFAULT_RAMADDR;
static U32                _ResetType;
static U64                _Var;
static const char*        _pLoop;
static char               _ExitTerminal;
static COMMANDER_SETTINGS _CommanderSettings;
static U32                _Polynom;
static int                _ExitOnErrorLvl = 0;
static int                _FlashDLCurPercentage = FLASH_DL_NUM_INTERVALS;
static U32                _FlashDLCurStep = 0;
static U32                _FlashProgBarOverrideSet;

/*********************************************************************
*
*       Static const data
*
**********************************************************************
*/

static const DEVICE_MANUFACTURER _aManufacturer[] = {
   { 0x001F, "Atmel"   }
  ,{ 0x0223, "Renesas" }
  ,{ 0x023B, "ARM"     }
};

static const char* _aIPSR2Str[32] = {
  "NoException",  "Reset",     "NMI",        "HardFault"
  "MemManage",    "BusFault",  "UsageFault", "Reserved", 
  "Reserved",     "Reserved",  "Reserved",   "SVCall",
  "DebugMonitor", "Reserved",  "PendSV",     "SysTick",
  "INTISR0",      "INTISR1",   "INTISR2",    "INTISR3",
  "INTISR4",      "INTISR5",   "INTISR6",    "INTISR7",
  "INTISR8",      "INTISR9",   "INTISR10",   "INTISR11",
  "INTISR12",     "INTISR13",  "INTISR14",   "INTISR15"
};

static const _DEV_INFO _aTblDevInfo[] = {
                                          { JLINKARM_DEV_FAMILY_CM1,        JLINKARM_CM3_REG_R15       },
                                          { JLINKARM_DEV_FAMILY_CF,         JLINK_CF_REG_PC            },
                                          { JLINKARM_DEV_FAMILY_CM3,        JLINKARM_CM3_REG_R15       },
                                          { JLINKARM_DEV_FAMILY_CM0,        JLINKARM_CM3_REG_R15       },
                                          { JLINKARM_DEV_FAMILY_ARM7,       ARM_REG_R15                },
                                          { JLINKARM_DEV_FAMILY_CORTEX_A8,  JLINKARM_CORTEX_R4_REG_R15 },
                                          { JLINKARM_DEV_FAMILY_ARM9,       ARM_REG_R15                },
                                          { JLINKARM_DEV_FAMILY_ARM10,      ARM_REG_R15                },
                                          { JLINKARM_DEV_FAMILY_ARM11,      ARM_REG_R15                },
                                          { JLINKARM_DEV_FAMILY_CORTEX_R4,  JLINKARM_CORTEX_R4_REG_R15 },
                                          { JLINKARM_DEV_FAMILY_RX,         JLINKARM_RX_REG_PC         },
                                          { JLINKARM_DEV_FAMILY_CM4,        JLINKARM_CM3_REG_R15       },
                                          { JLINKARM_DEV_FAMILY_CORTEX_A5,  JLINKARM_CORTEX_R4_REG_R15 },
                                          { JLINKARM_DEV_FAMILY_POWER_PC,   JLINK_POWER_PC_REG_PC      },
                                          { JLINK_DEV_FAMILY_MIPS,          JLINK_MIPS_REG_PC          },
                                          { JLINK_DEV_FAMILY_EFM8,          JLINK_EFM8_PC              }
                                        };

static const U32 _aSuppDevFamiliesJTAG[] = {
                                            JLINK_CORE_ANY,
                                            JLINK_CORE_CORTEX_M1,
                                            JLINK_CORE_COLDFIRE,
                                            JLINK_CORE_CORTEX_M3,
                                            JLINK_CORE_CORTEX_M3_R1P0,
                                            JLINK_CORE_CORTEX_M3_R1P1,
                                            JLINK_CORE_CORTEX_M3_R2P0,
                                            JLINK_CORE_SIM,
                                            JLINK_CORE_XSCALE,
                                            JLINK_CORE_CORTEX_M0,
                                            JLINK_CORE_ARM7,
                                            JLINK_CORE_ARM7TDMI,
                                            JLINK_CORE_ARM7TDMI_R3,
                                            JLINK_CORE_ARM7TDMI_R4,
                                            JLINK_CORE_ARM7TDMI_S,
                                            JLINK_CORE_ARM7TDMI_S_R3,
                                            JLINK_CORE_ARM7TDMI_S_R4,
                                            JLINK_CORE_CORTEX_A8,
                                            JLINK_CORE_CORTEX_A7,
                                            JLINK_CORE_CORTEX_A9,
                                            JLINK_CORE_CORTEX_A12,
                                            JLINK_CORE_CORTEX_A15,
                                            JLINK_CORE_CORTEX_A17,
                                            JLINK_CORE_ARM9,
                                            JLINK_CORE_ARM9TDMI_S,
                                            JLINK_CORE_ARM920T,
                                            JLINK_CORE_ARM922T,
                                            JLINK_CORE_ARM926EJ_S,
                                            JLINK_CORE_ARM946E_S,
                                            JLINK_CORE_ARM966E_S,
                                            JLINK_CORE_ARM968E_S,
                                            JLINK_CORE_ARM11,
                                            JLINK_CORE_ARM1136,
                                            JLINK_CORE_ARM1136J,
                                            JLINK_CORE_ARM1136J_S,
                                            JLINK_CORE_ARM1136JF,
                                            JLINK_CORE_ARM1136JF_S,
                                            JLINK_CORE_ARM1156,
                                            JLINK_CORE_ARM1176,
                                            JLINK_CORE_ARM1176J,
                                            JLINK_CORE_ARM1176J_S,
                                            JLINK_CORE_ARM1176JF,
                                            JLINK_CORE_ARM1176JF_S,
                                            JLINK_CORE_CORTEX_R4,
                                            JLINK_CORE_CORTEX_R5,
                                            JLINK_CORE_RX,
                                            JLINK_CORE_RX610,
                                            JLINK_CORE_RX62N,
                                            JLINK_CORE_RX62T,
                                            JLINK_CORE_RX63N,
                                            JLINK_CORE_RX630,
                                            JLINK_CORE_RX63T,
                                            JLINK_CORE_RX621,
                                            JLINK_CORE_RX62G,
                                            JLINK_CORE_RX631,
                                            JLINK_CORE_RX64M,
                                            JLINK_CORE_RX71M,
                                            JLINK_CORE_CORTEX_M4,
                                            JLINK_CORE_CORTEX_M7,
                                            JLINK_CORE_CORTEX_A5,
                                            JLINK_CORE_POWER_PC,
                                            JLINK_CORE_POWER_PC_N1,
                                            JLINK_CORE_POWER_PC_N2,
                                            JLINK_CORE_MIPS,
                                            JLINK_CORE_MIPS_M4K,
                                            JLINK_CORE_MIPS_MICROAPTIV,
                                            0
                                           };

static const U32 _aSuppDevFamiliesSWD[]  = {
                                            JLINK_CORE_ANY,
                                            JLINK_CORE_CORTEX_M1,
                                            JLINK_CORE_CORTEX_M3,
                                            JLINK_CORE_CORTEX_M3_R1P0,
                                            JLINK_CORE_CORTEX_M3_R1P1,
                                            JLINK_CORE_CORTEX_M3_R2P0,
                                            JLINK_CORE_CORTEX_M0,
                                            JLINK_CORE_CORTEX_A8,
                                            JLINK_CORE_CORTEX_A7,
                                            JLINK_CORE_CORTEX_A9,
                                            JLINK_CORE_CORTEX_A12,
                                            JLINK_CORE_CORTEX_A15,
                                            JLINK_CORE_CORTEX_A17,
                                            JLINK_CORE_CORTEX_R4,
                                            JLINK_CORE_CORTEX_R5,
                                            JLINK_CORE_CORTEX_M4,
                                            JLINK_CORE_CORTEX_M7,
                                            JLINK_CORE_CORTEX_A5,
                                            0
                                           };

static const U32 _aSuppDevFamiliesFINE[] = {
                                            JLINK_CORE_ANY,
                                            JLINK_CORE_RX,
                                            JLINK_CORE_RX610,
                                            JLINK_CORE_RX62N,
                                            JLINK_CORE_RX62T,
                                            JLINK_CORE_RX63N,
                                            JLINK_CORE_RX630,
                                            JLINK_CORE_RX63T,
                                            JLINK_CORE_RX621,
                                            JLINK_CORE_RX62G,
                                            JLINK_CORE_RX631,
                                            JLINK_CORE_RX210,
                                            JLINK_CORE_RX21A,
                                            JLINK_CORE_RX220,
                                            JLINK_CORE_RX230,
                                            JLINK_CORE_RX231,
                                            JLINK_CORE_RX23T,
                                            JLINK_CORE_RX111,
                                            JLINK_CORE_RX110,
                                            JLINK_CORE_RX113,
                                            JLINK_CORE_RX64M,
                                            JLINK_CORE_RX71M,
                                            0
                                           };

static const U32 _aSuppDevFamiliesICSP[] = {
                                            JLINK_CORE_ANY,
                                            JLINK_CORE_MIPS,
                                            JLINK_CORE_MIPS_M4K,
                                            JLINK_CORE_MIPS_MICROAPTIV,
                                            0
                                           };

static const U32 _aSuppDevFamiliesC2[]   = {
                                            JLINK_CORE_ANY,
                                            JLINK_CORE_EFM8_UNSPEC,
                                            JLINK_CORE_CIP51,
                                            0
                                           };

static const _TIF_LIST _aTIFList[] = {
  {"JTAG", 'J', JLINKARM_TIF_JTAG,  _aSuppDevFamiliesJTAG  },
  {"SWD",  'S', JLINKARM_TIF_SWD,   _aSuppDevFamiliesSWD   },
  {"FINE", 'F', JLINKARM_TIF_FINE,  _aSuppDevFamiliesFINE  },
  {"ICSP", 'I', JLINKARM_TIF_ICSP,  _aSuppDevFamiliesICSP  },
  {"C2",   'C', JLINKARM_TIF_C2,    _aSuppDevFamiliesC2    }
};

static const FLASHDL_ACTION _aAction[] = {
  {"", ""},
  {"Compare", "Comparing flash   [000%%]"},
  {"Erase",   "Erasing flash     [000%%]"},
  {"Program", "Programming flash [000%%]"},
  {"Verify",  "Verifying flash   [000%%]"}
};

static const char* _sDone = " Done.\n";

static const U8 _acInSWDSwitchingSeq[33]  = {
                                              255, 255, 255, 255, 255, 255, 255,
                                              0x9e,0xe7,                             // Switching sequence STM32
                                              255, 255, 255, 255, 255, 255, 255,
                                              0xb6, 0xeD,                            // Switching sequence Luminary (deprecated)
                                              255, 255, 255, 255, 255, 255, 255,
                                              0,0,                                   // Make sure SWD is ready for a start bit
                                              0xa5,                                  // Read ID (DP:00)
                                              0,0,0,0,0                              // Input
                                            };
static const U8 _acDirSWDSwitchingSeq[33] = {
                                              255, 255, 255, 255, 255, 255, 255,
                                              255, 255,
                                              255, 255, 255, 255, 255, 255, 255,
                                              255, 255,
                                              255, 255, 255, 255, 255, 255, 255,
                                              255,255,                               // Make sure SWD is ready for a start bit
                                              0xFF,                                  // Read ID (DP:00)
                                              0,0,0,0,0xF0                           // Input
                                            };

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _ReportOut
*/
static void _ReportOut(const char* s) {
  printf(s);
#ifndef WIN32
  fflush(stdout);  // Linux / OS X buffers the output until the next new line
#endif
}

/*********************************************************************
*
*       _ReportOutf
*/
static void _ReportOutf(const char* sFormat, ...) {
  char ac[2048];
  va_list ParamList;

  if (sFormat) {
    if (strlen(sFormat) < 4096) {
      va_start(ParamList, sFormat);
      vsprintf(&ac[0], sFormat, ParamList);
      fputs(ac, stdout);
#ifndef WIN32
      fflush(stdout);  // Linux / OS X buffers the output until the next new line
#endif
    }
  }
}

/*********************************************************************
*
*       _cbErrorOut()
*/
static void _cbErrorOut(const char *s) {
  _ReportOutf("\n****** Error: %s\n", s);
}

/*********************************************************************
*
*       _cbWarnOut()
*/
static void _cbWarnOut(const char *s) {
  _ReportOutf("\n**************************\nWARNING: %s\n**************************\n\n", s);
}

/*********************************************************************
*
*       _cbLog()
*/
static void _cbLog(const char *s) {
  _ReportOutf("Info: %s", s);
  if (*(s + strlen(s) - 1) != '\n') {
    _ReportOutf("\n");
  }
}

/*********************************************************************
*
*       _Log
*/
void _Log(const char *s) {
  _ReportOutf(s);
  if (*(s + strlen(s) - 1) != '\n') {
    _ReportOutf("\n");
  }
}

/*********************************************************************
*
*       _GetTickCount
*/
#ifdef WIN32
static int _GetTickCount(void) {
  return GetTickCount();
}
#endif

/*********************************************************************
*
*       _GetTickCount
*/
#if __APPLE__
static int _GetTickCount(void) {
  static int _t0 = 0;
  int        t;
  U64        AbsTime;

  AbsTime = mach_absolute_time();
  t       = AbsTime / 1000000;
  if (_t0 == 0) {
    _t0 = t;
  }
  return t - _t0;
}
#endif

/*********************************************************************
*
*       _GetTickCount
*/
#if linux
static int _GetTickCount(void) {
  static int _t0 = 0;
  int t;
  struct timespec Time;

  clock_gettime(CLOCK_MONOTONIC, &Time);
  t = ((int)Time.tv_sec * 1000) + ((int)Time.tv_nsec / 1000000);
  if (_t0 == 0) {
    _t0 = t;
  }
  return t - _t0;
}
#endif

/*********************************************************************
*
*       _Sleep
*/
static void _Sleep(int ms) {
#ifdef WIN32
  Sleep(ms);
#else
  usleep(ms * 1000);
#endif
}

/*********************************************************************
*
*       _InitSysTimer
*/
static void _InitSysTimer(void) {
#ifdef WIN32 
  timeBeginPeriod(1);
#endif
}

/*********************************************************************
*
*       _DeInitSysTimer
*/
static void _DeInitSysTimer(void) {
#ifdef WIN32
  timeEndPeriod(1);
#endif
}

/*********************************************************************
*
*       _kbhit
*
*    Function description
*      Linux (POSIX) implementation of _kbhit().
*/
#ifndef WIN32
static int _kbhit(void) {
  static int Inited = 0;

  if (Inited == 0) {
        //
        // Use termios to turn off line buffering
        //
        struct termios Term;
    tcgetattr(STDIN_FILENO, &Term);
        Term.c_lflag &= ~ICANON;
    tcsetattr(STDIN_FILENO, TCSANOW, &Term);
        setbuf(stdin, NULL);
    Inited = 1;
    }

    int BytesInBuffer;
 ioctl(STDIN_FILENO, FIONREAD, &BytesInBuffer);
    return BytesInBuffer;
}
#endif

#if linux

typedef char * (READ_LINE)  (const char *);
typedef int    (ADD_HISTORY)(const char *);

/*********************************************************************
*
*       _OpenLibrary
*/
static void * _OpenLibrary(void) {
  char         acPath [PATH_MAX];
  ssize_t      NumBytes;
  char       * p;  
  const char * pSoName;
  void       * pLib;

  pLib    = NULL;
  pSoName = "libedit.so.0";
  //
  // Get the path to the application which uses the shared library.
  //
  NumBytes = readlink("/proc/self/exe", acPath, sizeof(acPath));
  if (NumBytes > 0) {
    //
    // Create the path to shared library.
    //
    p = strrchr(acPath, '/');
    if (p) {
      ++p;
    } else {
      p = acPath;
    }
    strcpy(p, "ThirdParty/");
    strcat(acPath, pSoName);
    //
    // Try to open the shared libray stored in the directory where the application is stored.
    //
    pLib = dlopen(acPath, RTLD_LAZY);
    if (pLib == NULL) {
      //
      // Shared library not found. Try to load it from the installation path.
      //
      sprintf(acPath, "/opt/SEGGER/JLink/ThirdParty/ThirdParty/%s", pSoName);
      pLib = dlopen(acPath, RTLD_LAZY);
      if (pLib == NULL) {
        //
        // Shared library not found. Try to load it from the system path.
        //
        pLib = dlopen(pSoName, RTLD_LAZY);
      }
    }
  }
  return pLib;
}

/*********************************************************************
*
*       _GetSymbol
*/
static void * _GetSymbol(void * pLib, const char * pName) {
  void * pSymbol;
  char * pError;

  dlerror();
  pSymbol = dlsym(pLib, pName);
  pError = dlerror();
  if (pError) {
    pSymbol = NULL;
  }
  return pSymbol;
}

#endif

/*********************************************************************
*
*       _ConsoleGetString
*/
static char * _ConsoleGetString(const char * pPrompt, char * pBuffer, U32 BufferSize) {
#ifdef WIN32
  char* p;

  if (pPrompt) {
  	_ReportOutf(pPrompt);
  }
  fgets(pBuffer, BufferSize, stdin);
  //
  // Remove newline and carriage return characters 
  //  from the end of the string.
  //
  p = pBuffer + strlen(pBuffer) - 1;
  while ((*p == '\n') || (*p  == '\r')) {
    *p = '\0';
    p--;
  }
  return pBuffer;
#endif

#if linux
  char * s;
  U32    Len;
  char * pError;
  static READ_LINE   * _pfReadLine;
  static ADD_HISTORY * _pfAddHistory;
  static int           _Inited;

  if (_Inited == 0) {
    void * pLib;

    pLib = _OpenLibrary();
    if (pLib) {
      _pfReadLine   = (READ_LINE   *)_GetSymbol(pLib, "readline");
      _pfAddHistory = (ADD_HISTORY *)_GetSymbol(pLib, "add_history");
      _Inited = 1;
    }
  }
  if (_pfReadLine && _pfAddHistory) {
    //
    // Found the library. Use the functions implemented in it.
    //
    while (1) {
      s = (*_pfReadLine)(pPrompt);
      if (s != NULL) {
        Len = strlen(s);
        Len = MIN(Len, BufferSize - 1);
        strncpy(pBuffer, s, Len);
        pBuffer[Len] = '\0';                
        if (Len) {
          (*_pfAddHistory)(s);
        } else {
          free(s);
        }
        break;
      }
    }
  } else {
    //
    // libedit library not found. Use standard I/O functions.
    //
    if (pPrompt) {
      _ReportOutf(pPrompt);
    }
    s   = fgets(pBuffer, BufferSize, stdin);
    Len = strlen(s);
    if (s && Len) {
      pBuffer[Len - 1] = '\0';    // Remove the new line character
    }
  }
  return s;
#endif

#if __APPLE__
  U32    Len;
  char * s;

  while (1) {
  s = readline(pPrompt);
  if (s != NULL) {
    Len = strlen(s);
    Len = MIN(Len, BufferSize - 1);
    strncpy(pBuffer, s, Len);
    pBuffer[Len] = '\0';                
      if (Len) {
        add_history(s);
      } else {
        free(s);
  }
      break;
    }
  }
  return pBuffer;
#endif
}

/*********************************************************************
*
*       _OpenFile
*/
static HANDLE _OpenFile(const char * sFile, U32 Flags) {
#ifdef WIN32
  HANDLE hFile;
  DWORD  AccessFlags = 0;
  DWORD  ShareFlags  = 0;
  DWORD  CreateFlags = 0;

  AccessFlags |= (Flags & FILE_FLAG_READ)        ? GENERIC_READ      : 0;
  AccessFlags |= (Flags & FILE_FLAG_WRITE)       ? GENERIC_WRITE     : 0;
  ShareFlags  |= (Flags & FILE_FLAG_SHARE_READ)  ? FILE_SHARE_READ   : 0;
  ShareFlags  |= (Flags & FILE_FLAG_SHARE_WRITE) ? FILE_SHARE_WRITE  : 0;
  if (Flags & FILE_FLAG_CREATE) {
    if        (Flags & FILE_FLAG_EXCL) {
      CreateFlags = CREATE_NEW;
    } else if (Flags & FILE_FLAG_TRUNC) {
      CreateFlags = CREATE_ALWAYS;
    } else {
      CreateFlags = OPEN_ALWAYS;
    }
  } else if (Flags & FILE_FLAG_TRUNC) {
    CreateFlags = TRUNCATE_EXISTING;
  } else {
    CreateFlags = OPEN_EXISTING;
  }
  hFile = CreateFile(sFile, AccessFlags, ShareFlags, NULL, CreateFlags, FILE_ATTRIBUTE_NORMAL, NULL);
  if (hFile != INVALID_HANDLE_VALUE) {
    if (Flags & FILE_FLAG_APPEND) {
      SetFilePointer(hFile, 0, 0, FILE_END);
    }
  }
  return hFile;
#else
  HANDLE hFile;
  U32 AccessFlags = 0;
  U32 CreateFlags = 0;

  AccessFlags |= (Flags & FILE_FLAG_READ)        ? O_RDONLY     : 0;
  AccessFlags |= (Flags & FILE_FLAG_WRITE)       ? O_WRONLY     : 0;
  if (Flags & FILE_FLAG_CREATE) {
    if        (Flags & FILE_FLAG_EXCL) {
      CreateFlags = O_CREAT | O_EXCL;
    } else if (Flags & FILE_FLAG_TRUNC) {
      CreateFlags = O_CREAT | O_TRUNC;
    } else {
      CreateFlags = O_CREAT;
    }
  } else if (Flags & FILE_FLAG_TRUNC) {
    CreateFlags = O_TRUNC;
  }
  hFile = (HANDLE)open(sFile, AccessFlags | CreateFlags, 0644); // 0644 = -rw-r--r-- mode
  if (hFile != INVALID_HANDLE_VALUE) {
    if (Flags & FILE_FLAG_APPEND) {
      lseek(hFile, 0, SEEK_END);
    }
  }
  return hFile;
#endif
}

/*********************************************************************
*
*       _CloseFile
*/
static void _CloseFile(HANDLE hFile) {
#ifdef WIN32  
  CloseHandle((HANDLE)hFile);
#else
  close(hFile);
#endif
}

/*********************************************************************
*
*       _ReadFile
*/
static int _ReadFile(HANDLE hFile, void * pData, U32 NumBytes) {
#ifdef WIN32
  DWORD NumBytesRead;
  if (ReadFile((HANDLE)hFile, pData, NumBytes, &NumBytesRead, NULL) == 0) {
    return -1;                    // Error
  }
  return (int)NumBytesRead;       // O.K.
#else
  int NumBytesRead;

  NumBytesRead = read((int)hFile, pData, NumBytes);
  return NumBytesRead;
#endif
}

/*********************************************************************
*
*       _WriteFile
*/
static int _WriteFile(HANDLE hFile, const void* pData, U32 NumBytes) {
#ifdef WIN32
  DWORD NumBytesWritten;
  if (WriteFile((HANDLE)hFile, pData, NumBytes, &NumBytesWritten, NULL) == 0) {
    return -1;                    // Error
  }
  return (int)NumBytesWritten;    // O.K.
#else
  int NumBytesWritten;

  NumBytesWritten = write((int)hFile, pData, NumBytes);
  return NumBytesWritten;
#endif
}

/*********************************************************************
*
*       _GetFileSize
*/
static U32 _GetFileSize(HANDLE hFile) {
#ifdef WIN32
  DWORD FileSize;
  FileSize = GetFileSize((HANDLE)hFile, NULL);
  return (U32)FileSize;
#else
  U32   FileSize;
  off_t CurrentPos;

  CurrentPos = lseek((int)hFile, 0, SEEK_CUR);
  FileSize   = lseek((int)hFile, 0, SEEK_END);
  lseek((int)hFile, CurrentPos, SEEK_SET);
  return FileSize;
#endif
}

/*********************************************************************
*
*       _CRC_CalcFast
*/
static U32 _CRC_CalcFast(const U8* pData, unsigned NumBytes, U32 crc, U32 Polynom) {
  static U32 _aCRC8[256] = {0};
  //
  // Build small CRC table if required
  //
  if ((_aCRC8[1] == 0) || (Polynom != _Polynom)) {
    U32 i, n, v;
    for (n = 0; n < 256; n++) {
      v = n;
      i = 8;
      do {
        if (v & 1) {
          v = (v >> 1) ^ Polynom;
        } else {
          v >>= 1;
        }
      } while (--i);
      _aCRC8[n] = v;
    }
  }
  _Polynom = Polynom;
  //
  // Calculate CRC in units of 8 bytes
  //
  if (NumBytes >= 8) {
    U32* p;
    p = (U32*)pData;
    do {
      crc ^= *p++;
      crc  = _aCRC8[crc & 0xFF] ^ (crc >> 8);
      crc  = _aCRC8[crc & 0xFF] ^ (crc >> 8);
      crc  = _aCRC8[crc & 0xFF] ^ (crc >> 8);
      crc  = _aCRC8[crc & 0xFF] ^ (crc >> 8);
      crc ^= *p++;
      crc  = _aCRC8[crc & 0xFF] ^ (crc >> 8);
      crc  = _aCRC8[crc & 0xFF] ^ (crc >> 8);
      crc  = _aCRC8[crc & 0xFF] ^ (crc >> 8);
      crc  = _aCRC8[crc & 0xFF] ^ (crc >> 8);
      NumBytes -= 8;
    } while (NumBytes >= 8);
    pData = (const U8*)p;
  }
  //
  // Calculate CRC in units of bytes
  //
  if (NumBytes) {
    do {
      crc ^= *pData++;
      crc  = _aCRC8[crc & 0xFF] ^ (crc >> 8);
    } while (--NumBytes);
  }
  return crc;
}

/*********************************************************************
*
*       _CRC_Calc32Fast
*/
static U32 _CRC_Calc32Fast(const U8* pData, unsigned NumBytes, U32 crc) {
  U32 Polynom;
  Polynom = 0xEDB88320;    // Normal form is 0x04C11DB7
  return _CRC_CalcFast(pData, NumBytes, crc, Polynom);
}

/*********************************************************************
*
*       _Load8LE
*/
static U32 _Load8LE(const U8* p) {
  U32 r;
  r = *p;
  return r;
}

/*********************************************************************
*
*       _Load16LE
*/
static U32 _Load16LE(const U8* p) {
  U32 r;
  r = (*(p + 0) <<  0) | (*(p + 1) <<  8);
  return r;
}

/*********************************************************************
*
*       _Load32LE
*/
static U32 _Load32LE(const U8* p) {
  U32 r;
  r = (*(p + 0) <<  0) | (*(p + 1) <<  8) | (*(p + 2) << 16) | (*(p + 3) << 24);
  return r;
}

/*********************************************************************
*
*       _Load64LE
*/
static U64 _Load64LE(const U8* p) {
  U64 r;
  r  = (*(p + 4) <<  0) | (*(p + 5) <<  8) | (*(p + 6) << 16) | (*(p + 7) << 24);
  r <<= 16;
  r <<= 16;
  r |= (*(p + 0) <<  0) | (*(p + 1) <<  8) | (*(p + 2) << 16) | (*(p + 3) << 24);
  return r;
}

/*********************************************************************
*
*       _PrintTime
*/
static void _PrintTime(U64 Time, char* pBuffer, U32 BufferSize) {
  int hh;
  int mm;
  int ss;
  int ms;
  int us;
#ifdef WIN32
  hh = (int) (Time / (U64)3600000000uL);
#else
  hh = (int) (Time / (U64)3600000000uLL); // GCC under Linux generates a warning if 64-bit constants are not explicitly defined as 64-bit values (long long int)
#endif
  mm = (int)((Time /        60000000) % 60);
  ss = (int)((Time /         1000000) % 60);
  ms = (int)((Time /            1000) % 1000);
  us = (int)((Time)                   % 1000);
  if (hh) {
    sprintf(pBuffer, "%.1d:%.2d:%.2d.%.3d %.3d", hh, mm, ss, ms, us);
  } else if (mm) {
    sprintf(pBuffer, "%.1d:%.2d.%.3d %.3d", mm, ss, ms, us);
  } else { //if (ss) {
    sprintf(pBuffer, "%.1d.%.3d %.3d", ss, ms, us);
  }
}

/*********************************************************************
*
*       _Cmd2String
*/
static const char* _Cmd2String(U8 Cmd) {
  switch (Cmd) {
  case 2:  return "SCAN_N";
  case 4:  return "RESTART";
  case 8:  return "ABORT";
  case 10: return "DPACC";
  case 11: return "APACC";
  case 12: return "INTEST";
  case 14: return "IDCODE";
  case 15: return "BYPASS";
  }
  return "Unknown JTAG instruction";
}

/*********************************************************************
*
*       _MakeLowercase
*/
static int _MakeLowercase(int c) {
  if ((c >= 'A') && (c <= 'Z')) {
    c += 0x20;
  }
  return c;
}

/*********************************************************************
*
*       _Hex2Dec
*/
static int _Hex2Dec(U8 c) {
  if ((c >='0') && (c <= '9')) {
    return c - '0';
  }
  if ((c >='a') && (c <= 'f')) {
    return c - 'a' + 10;
  }
  if ((c >= 'A') && (c <= 'F')) {
    return c - 'A' + 10;
  }
  return -1;
}

/*********************************************************************
*
*       _EatWhite
*/
static void _EatWhite(const char** ps) {
  const char* s = *ps;
  while ((*s == ' ') || (*s == '\t') || (*s == '\r') || (*s == '\n')) {
    s++;
  }
  *ps = s;
}

/*********************************************************************
*
*       _EatWhiteAndSeparator
*/
static void _EatWhiteAndSeparator(const char** ps) {
  const char* s;

  _EatWhite(ps);
  s = *ps;
  if (*s == ',') {
    s++;
    *ps = s;
    _EatWhite(ps);
  }
}

/*********************************************************************
*
*       _ParseHex
*/
static const char* _ParseHex(const char** ps, U32* pData) {
  U32 Data = 0;
  int NumDigits = 0;
  char c;

  _EatWhite(ps);
  if (**ps == '0') {
    c = *(*ps + 1);
    if (c == 'x') {
      (*ps) += 2;
    } else if (c == 'X') {
      (*ps) += 2;
    }
  }
  do {
    int v =  _Hex2Dec(**ps);
    if (v >= 0) {
      Data = (Data << 4) | v;
      (*ps)++;
      NumDigits++;
    } else {
      if (NumDigits == 0) {
        return "Expected a hex value";
      }
      *pData = Data;
      return NULL;
    }
  } while (1);
}

/*********************************************************************
*
*       _TryParseMemZone
*
*  Function description
*    Tries to parse a memory zone, if specified for the current command.
*    In general, zones are specified by "<Zone>:" right before the address.
*
*  Notes
*    (1) If no zone specifier is found, we just fill the psZone buffer with 0, indicating an empty zone.
*/
static void _TryParseMemZone(const char** ps, char* psZone, int SizeOfBuffer) {
  const char* s;
  char* psZoneOrg;
  int ZoneFound;
  int NumBytesRem;
  //
  // Parse until ':' or end of string is hit
  //
  _EatWhite(ps);
  NumBytesRem = SizeOfBuffer;
  ZoneFound = 0;
  s = *ps;
  psZoneOrg = psZone;
  do {
    //
    // End of main string reached? We are done
    //
    if (*s == 0) {
      break;
    }
    //
    // End of zone description found? We are done
    //
    if (*s == ':') {
      s++;             // Skip ':'
      *psZone = 0;     // Terminate zone string
      ZoneFound = 1;
      break;
    }
    if (NumBytesRem > 1) {
      *psZone++ = *s;
      NumBytesRem--;
    }
    s++;
  } while (1);
  if (ZoneFound) {
    *ps = s;
  } else {
    memset(psZoneOrg, 0, SizeOfBuffer);
  }
}

/*********************************************************************
*
*       _ParseHexString
*/
static const char* _ParseHexString(const char** ps, U8* pData, int NumBits) {
  U32 Data = 0;
  int NumDigits = 0;
  int i;
  int Off;
  const char * s;

  _EatWhite(ps);
  if (**ps == '0') {
    if (*(*ps + 1) == 'x') {
      (*ps) += 2;
    }
  }
  s = *ps;
  for (i = 0; i < NumBits; i+=4) {
    int v;
    Off = (NumBits - 1 - i) / 4;
    v =  _Hex2Dec(*(s + Off));
    if (v < 0) {
      return "Unexpected character in hex string";
    }
    if ((i & 4) == 0) {
      *pData = v;
    } else {
      *pData |= (v << 4);
      *pData++;
    }
  }
  *ps += (NumBits + 3) / 4;
  return 0;
}

/*********************************************************************
*
*       _ParseChar
*/
static const char* _ParseChar(const char** ps, char c) {
  _EatWhite(ps);
  if (**ps != c) {
    return "Unexpected character";
  }
  (*ps)++;
  return NULL;
}

/*********************************************************************
*
*       _ParseHex64
*/
static const char* _ParseHex64(const char** ps, U64* pData) {
  U64 Data = 0;
  int NumDigits = 0;
  _EatWhite(ps);
  if (**ps == '0') {
    if (*(*ps + 1) == 'x') {
      (*ps) += 2;
    }
  }
  do {
    int v =  _Hex2Dec(**ps);
    if (v >= 0) {
      Data = (Data << 4) | v;
      (*ps)++;
      NumDigits++;
    } else {
      if (NumDigits == 0) {
        return "Expected a hex value";
      }
      *pData = Data;
      return NULL;
    }
  } while (1);
}

/*********************************************************************
*
*       _ParseDec
*/
static const char* _ParseDec(const char** ps, U32* pData) {
  U32 Data = 0;
  int NumDigits = 0;
  int IsNegative;
  _EatWhite(ps);
  //
  // Negative sign?
  //
  IsNegative = 0;
  if (**ps == '-') {
    (*ps)++;
    IsNegative = 1;
  }
  //
  // Parse decimal integer
  //
  do {
    int v;
    char c;

    c = **ps;
    v =  ((c >= '0') && (c <= '9')) ? c - '0' : -1;
    if (v >= 0) {
      Data = (Data * 10) + v;
      (*ps)++;
      NumDigits++;
    } else {
      if (NumDigits == 0) {
        return "Expected a dec value";
      }
      if (IsNegative) {
        Data *= -1;
      }
      *pData = Data;
      return NULL;
    }
  } while (1);
}

/*********************************************************************
*
*       _ParseString
*/
static const char* _ParseString(const char** ps, char* pBuffer, unsigned BufferSize) {
  if (pBuffer && BufferSize) {
    const char *s = *ps;
    char QuotationMarks = 0;
    *pBuffer = 0;
    _EatWhite(&s);
    if (*s == '"') {
      QuotationMarks++;
      s++;
    }
    while (*s && (*s != '\r') && (*s != '\n') && (((*s != ',') && (*s != ' ')) || QuotationMarks) && --BufferSize) {
      if (*s == '"') {
        s++;
        break;
      }
      *pBuffer++ = *s++;
      
    }
    *pBuffer = 0;
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    *ps = s;
  }
  return *ps;
}

/*********************************************************************
*
*       _JLinkstricmp
*
*  Notes
*    Call different than _stricmp to avoid linker errors with MinGW
*    which also defines this function non-static and gets a name conflict.
*/
static int _JLinkstricmp(const char * dst, const char * src) {
  int f;
  int l;

  do {
    if (((f = (unsigned char)(*(dst++))) >= 'A') && (f <= 'Z')) {
      f -= ('A' - 'a');
    }
    if (((l = (unsigned char)(*(src++))) >= 'A') && (l <= 'Z')) {
      l -= ('A' - 'a');
    }
  } while (f && (f == l));
  return (f - l);
}

/*********************************************************************
*
*       _JLinkstrnicmp
*
*  Notes
*    Call different than _stricmp to avoid linker errors with MinGW
*    which also defines this function non-static and gets a name conflict.
*/
static int _JLinkstrnicmp(const char * s0, const char * s1, size_t NumBytes) {
  char c0;
  char c1;

  while (NumBytes-- > 0) {
    c0 = *s0++;
    c1 = *s1++;
    c0 = _MakeLowercase(c0);
    c1 = _MakeLowercase(c1);
    if (c0 != c1) {
      return 1;
    }
  }
  return 0;
}

/*********************************************************************
*
*       _cbFlashProgProgress
*
*    Function description
*      This call back function is called by the J-Link DLL during flash programming.
*      It is used to show the progress of an ongoing flash programming procedure on OS without GUI support.
*
*/
#ifndef WIN32
static void _cbFlashProgProgress(const char * sAction, const char * sProg, int Percentage) {
  U32 Step;
  int CurPercentage;
  char ac[128];

  if (sAction == NULL) {  // Sanity checks. Action has to be given
    return;
  }
  //
  // Check which step is reported
  //
  Step = 1;
  do {
    if (_JLinkstrnicmp(sAction, _aAction[Step].sAction, strlen(_aAction[Step].sAction)) == 0) {
      break;
    }
    Step++;
  } while (Step < (COUNTOF(_aAction)));
  memset(ac,0, sizeof(ac));
  //
  // Check if it is a "dummy step" (Previous step, 100%)
  //
  if ((Step < _FlashDLCurStep) && (Percentage == 100)) {
    return;
  }
  //
  // Check if it is a new step
  //
  if ((Step != _FlashDLCurStep)) {
    //
    // Check if current step is not finished yet
    //
    if (_FlashDLCurPercentage < FLASH_DL_NUM_INTERVALS) {
      _ReportOutf("\b\b\b\b\b100%%]");   // Format is "000%]", so we have to make 5 backspaces to overrite the current percentage
      _ReportOutf(_sDone);
    }
    _FlashDLCurStep = Step;
    _FlashDLCurPercentage = 0;
    _ReportOutf(_aAction[Step].sDescription);
  }
  //
  // Check if percentage needs an update
  //
  Percentage = (Percentage/FLASH_DL_INTERVAL);
  if (_FlashDLCurPercentage == FLASH_DL_NUM_INTERVALS) {
    return;
  };
  CurPercentage = Percentage;
  _ReportOutf("\b\b\b\b\b%.3d%%]", Percentage * FLASH_DL_INTERVAL);   // Format is "000%]", so we have to make 5 backspaces to overrite the current percentage
  if ((_FlashDLCurPercentage < FLASH_DL_NUM_INTERVALS) && (Percentage == FLASH_DL_NUM_INTERVALS)) {
    _ReportOutf(_sDone);
  }
  _FlashDLCurPercentage = CurPercentage;
}
#endif

/*********************************************************************
*
*       _OverrideFlashProgProgressbarIfNoGui
*
*    Function description
*      For MAC/Linux, we set a call back function for the flash programming progress bar,
*      which is used by the J-Link DLL.
*/
static void _OverrideFlashProgProgressbarIfNoGui(void) {
#ifndef WIN32
  if (_FlashProgBarOverrideSet == 0) {
    JLINKARM_ExecCommand("DisableInfoWinFlashDL", NULL, 0);
    JLINKARM_ExecCommand("DisableInfoWinFlashBPs", NULL, 0);
    JLINK_SetFlashProgProgressCallback (_cbFlashProgProgress);
    _FlashProgBarOverrideSet = 1;
  }
#endif
}

/*********************************************************************
*
*       _memcmp
*
*  Function description
*    Compares memory areas
*
*  Return value
*    Offset of first different byte; -1 if all bytes are identical
*/
static int _memcmp(const U8 * p0, const U8 * p1, int NumBytes) {
  int i;
  for (i = 0; i < NumBytes; i++) {
    if (*p0++ != *p1++) {
      return i;
    }
  }
  return -1;
}

/*********************************************************************
*
*       _MeasureITarget()
*/
static void _MeasureITarget(int *pAvg, int *pMin, int *pMax) {
  U32 HWInfo[32];
  int ISum;
  int i;
  int IMin;
  int IMax;
  int ICur;
  int uA = 1000;

  ISum = 0;
  IMin = INT_MAX;
  IMax = INT_MIN;
  for (i = 0; i < ITARGET_SAMPLES; ++i) {
    memset(HWInfo, 0, sizeof HWInfo);
    JLINKARM_GetHWInfo(0xFFFFFFFF, HWInfo);
    //
    // Third word is the power consumption in mA
    //
    ICur = (int)HWInfo[2];
    ISum += ICur;
    if (ICur < IMin) {
      IMin = ICur;
    }
    if (ICur > IMax) {
      IMax = ICur;
    }
  }
  //
  // Return the measured current in uA
  //
  if (pAvg != NULL) {
    *pAvg = ISum * uA / i;
  }
  if (pMin != NULL) {
    *pMin = IMin * uA;
  }
  if (pMax != NULL) {
    *pMax = IMax * uA;
  }
}

/*********************************************************************
*
*       _EatEndMultiLineComment
*/
static void _EatEndMultiLineComment(const char** ppText) {
  const char* s = *ppText;
  char c;
  int  c16;
  while (1) {
    c   = *s;
    c16 = c | (*(s + 1) << 8);
    if (c == 0) {
      break;
    } else if (c16 == ('*' | ('/' << 8))) {             // End of multiline comment ?
      s += 2;
      break;
    }
    s++;
  }
  *ppText = s;
}

/*********************************************************************
*
*       _EatLine
*/
static void _EatLine(const char** ppText) {
  const char* s = *ppText;
  while (*s && (*s != 0x0A)) {
    s++;
  }
  if (*s == 0x0A) {
    s++;
  }
  *ppText = s;
}

/*********************************************************************
*
*       _EatWhiteAndComments
*/
static void _EatWhiteAndComments(const char** ppText) {
  const char* s = *ppText;
  while (1) {
    _EatWhite(&s);
    if (*s == ';') {
      _EatLine(&s);
    } else if (*s != '/') {
      break;
    } else {
      char c = *(s + 1);
      if (c == '/') {
        _EatLine(&s);
      } else if (c == '*') {
        s += 2;
        _EatEndMultiLineComment(&s);
      } else {
        break;
      }
    }
  }
  *ppText = s;
}

/*********************************************************************
*
*       _GetLine
*/
static char _GetLine(const char** ppText, char* pBuffer, unsigned BufferSize) {
  const char* s = *ppText;
  _EatWhiteAndComments(&s);
  /* Check for end of string */
  if (*s == 0) {
    return 0;
  }
  /* Search for end of line */
  while ((*s != '\r') && (*s != '\n') && (*s != 0)) {
    if (--BufferSize) {
      *pBuffer++ = *s;
    }
    s++;
  }
  *pBuffer = 0;
  *ppText = s;
  return 1;
}

/*********************************************************************
*
*       _PrintNumBytes
*/
static void _PrintNumBytes(U32 NumBytes) {
  if ((NumBytes & 1023) == 0) {
    _ReportOutf("%d KBytes", NumBytes / 1024);
  } else {
    _ReportOutf("%d Bytes", NumBytes);
  }
}

/*********************************************************************
*
*       _PrintId
*/
static void _PrintId(U32 Data32) {
  int IsValid = Data32 & 1;
  int ManId   = (Data32 >>  1) & 0x7FF;
  int PartNo  = (Data32 >> 12) & 0xFFFF;
  int Version = (Data32 >> 28) & 0xF;
  if (IsValid) {
    _ReportOutf("JTAG Id: 0x%.8X  Version: 0x%X Part no: 0x%x Man. Id: %.4X \n", Data32, Version, PartNo, ManId);
  } else {
    _ReportOutf("JTAG Id: 0x%.8X: INVALID\n", Data32);
  }
}

/*********************************************************************
*
*       _PrintCPSR
*/
static const char* _PrintCPSR(U32 CPSR) {
  static char ac[255];      // Note: This is not thread safe ! (Which is o.k. for this small program)
  const char * sMode;
  U8 Mode = (U8)(CPSR & 0x1f);
  switch (Mode) {
  case 0x10: sMode = "User mode";   break;
  case 0x11: sMode = "FIQ mode";    break;
  case 0x12: sMode = "IRQ mode";    break;
  case 0x13: sMode = "SVC mode";    break;
  case 0x17: sMode = "ABORT mode";  break;
  case 0x1B: sMode = "UNDEF mode";  break;
  case 0x1f: sMode = "System mode"; break;
  default:
  sMode = "Unknown mode";
  }
  sprintf(ac, "%s%s%s%s", sMode
          ,CPSR & (1 << 5) ? ", THUMB" : ", ARM"
          ,CPSR & (1 << 6) ? " FIQ dis." : ""
          ,CPSR & (1 << 7) ? " IRQ dis." : ""
          );
  return ac;
}

/*********************************************************************
*
*       _PrintMACAddr
*/
static void _PrintMACAddr(U64 MACAddr) {
  U32 SerialNo;   // It is important that serial number is unsigned, otherwise modulo operation will possibly lead to unexpected results
  int HWVersion;
  int HWVersionMajor;
  int ProdId;
  U8  MACHWIdent;

  if (((unsigned)MACAddr) != 0xFFFFFFFF) {
    _ReportOutf("MAC-Addr:       %.2X:%.2X:%.2X:%.2X:%.2X:%.2X (user assigned)\n", (unsigned) (MACAddr        & 0xFF),
                                                                                  (unsigned)((MACAddr >>  8) & 0xFF),
                                                                                  (unsigned)((MACAddr >> 16) & 0xFF),
                                                                                  (unsigned)((MACAddr >> 24) & 0xFF),
                                                                                  (unsigned)((MACAddr >> 32) & 0xFF),
                                                                                  (unsigned)((MACAddr >> 40) & 0xFF));
  } else {
    HWVersion = JLINKARM_GetHardwareVersion();
    HWVersionMajor = HWVersion / 10000 % 100;
    SerialNo  = (U32)JLINKARM_GetSN();
    ProdId    = JLINKARM_EMU_GetProductId();
    switch (ProdId) {
    case JLINK_EMU_PRODUCT_ID_FLASHER_ARM:
      if (HWVersionMajor == 4) {
        MACHWIdent = 0x09;
      } else if (HWVersionMajor == 3) {
        MACHWIdent = 0x04;
      } else {
        MACHWIdent = 0xFF;
        _ReportOutf("WARNING: Unknown hardware version of this product. MAC address can not be displayed properly.\n");
      }
      break;
    case JLINK_EMU_PRODUCT_ID_JLINK_PRO:
      if (HWVersionMajor < 3) {
        MACHWIdent = 0x01;
      } else if (HWVersionMajor == 3) {
        MACHWIdent = 0x02;
      } else if (HWVersionMajor == 4) {
        MACHWIdent = 0x07;
      } else {
        MACHWIdent = 0xFF;
        _ReportOutf("WARNING: Unknown hardware version of this product. MAC address can not be displayed properly.\n");
      }
      break;
    case JLINK_EMU_PRODUCT_ID_FLASHER_PPC:
      MACHWIdent = 0x06;
      break;
    case JLINK_EMU_PRODUCT_ID_FLASHER_RX:
      MACHWIdent = 0x05;
      break;
    case JLINK_EMU_PRODUCT_ID_FLASHER_PRO:
      MACHWIdent = 0x0A;
      break;
    default:
      _ReportOutf("Unknown debug probe sub-family. Please get in touch with SEGGER.\n");
      MACHWIdent = 0xFF;
      break;
    }
    if (MACHWIdent != 0xFF) {
      _ReportOutf("MAC-Addr:       00:22:C7:%.2X:%.2X:%.2X (Default)\n", (unsigned char) (MACHWIdent),
                                                                        (unsigned char) ((SerialNo % 100000) >> 8),
                                                                        (unsigned char) ((SerialNo % 100000) >> 0));
    }
  }
}

/*********************************************************************
*
*       _ShowFirmwareInfo
*/
void _ShowFirmwareInfo(void) {
  char ac[256];
  int  Version;
  JLINKARM_GetFirmwareString(ac, sizeof(ac));
  if (ac[0]) {
    _ReportOutf("Firmware: %s\n", ac);
    Version = JLINKARM_GetHardwareVersion();
    if (Version) {
      _ReportOutf("Hardware: V%d.%.2d\n", Version / 10000 % 100, Version / 100 % 100);
    }
  } else {
    _ReportOutf("Unable to retrieve firmware info !\n");
  }
}

/*********************************************************************
*
*       _ShowDLLInfo
*/
void _ShowDLLInfo(void) {
  char acRev[8] = {0};
  int Revision;
  int Version;
  Version  = JLINKARM_GetDLLVersion();
  Revision = Version % 100;
  if (Revision > 26) {
    Revision -= 26;
    acRev[0] = 'z';
    acRev[1] = '0' + (Revision / 10);
    acRev[2] = '0' + (Revision % 10);
  } else if (Revision > 0) {
    acRev[0] = 'a' + Revision - 1;
  }
  _ReportOutf("DLL version V%d.%.2d%s, compiled %s\n", Version / 10000, Version / 100 % 100, acRev, JLINKARM_GetCompileDateTime()); 
}

/*********************************************************************
*
*       _ShowETMConfig
*/
static U32 _ShowETMConfig(void) {
  //
  // Identify ETM
  //
  U32 ETMVersion;
  ETMVersion = JLINKARM_ETM_IsPresent();
  if (ETMVersion) {
    U32 ConfigCode;
    ConfigCode = JLINKARM_ETM_ReadReg(1);
    _ReportOutf("  ETM V%d.%d: %d pairs addr.comp, %d data comp, %d MM decs, %d counters%s\n"
               ,(ETMVersion >> 4) & 15
               ,(ETMVersion >> 0) & 15
               ,ConfigCode & 15
               ,(ConfigCode >> 4) & 15
               ,(ConfigCode >> 8) & 31
               ,(ConfigCode >> 13) & 7
               ,((ConfigCode >> 16) & 1) ? ", sequencer" : "");
  }
  return ETMVersion;
}

/*********************************************************************
*
*       _ShowETBConfig
*/
static void _ShowETBConfig(void) {
  if (JLINKARM_ETB_IsPresent()) {
    U32 ETMControl;
    U32 Id;
    U32 RAMDepth;
    U32 RAMWidth;

    ETMControl = JLINKARM_ETM_ReadReg(0);
    if (ETMControl & 1) {
      JLINKARM_ETM_WriteReg(0, (ETMControl & ~1), 1);
    }

    Id       = JLINKARM_ETB_ReadReg(0);
    RAMDepth = JLINKARM_ETB_ReadReg(1);
    RAMWidth = JLINKARM_ETB_ReadReg(2);
    _ReportOutf("  ETB V%d.0: %dx%d bit RAM\n"
              ,(Id >> 28) & 15
              ,RAMDepth
              ,RAMWidth
             );

    if (ETMControl & 1) {
      JLINKARM_ETM_WriteReg(0, ETMControl, 0);
    }
  }
}

/*********************************************************************
*
*       _ShowETMState
*/
static void _ShowETMState(void) {
  U32 ETMVersion;
  U32 CtrlReg;
  U32 StatusReg;
  U32 v;

  ETMVersion = _ShowETMConfig();
  if (ETMVersion) {
    CtrlReg   = JLINKARM_ETM_ReadReg(0);
    StatusReg = JLINKARM_ETM_ReadReg(4);
    if (CtrlReg & 1) {
      _ReportOutf("Powered down\n");
    } else {
      if (CtrlReg & (1 << 1)) {
        _ReportOutf(" MonitorCPRTs\n");
      }
      switch ((CtrlReg >> 2) & 3) {
        case 0: _ReportOutf("    Data trace: OFF (default)\n");         break;
        case 1: _ReportOutf("    Data trace: DATA only\n");   break;
        case 2: _ReportOutf("    Data trace: ADDR only\n");   break;
        case 3: _ReportOutf("    Data trace: Data & Addr\n"); break;
      }
      if (CtrlReg & (1 << 20)) {
        _ReportOutf("    Instruction trace disabled\n");
      }
      v = ((CtrlReg >> 4) & 7) | ((CtrlReg >> 18) & (1 << 3));    // Bits are in 21, 6:4;
      switch (v) {
        case  0: _ReportOutf("    Port size: 4-bit\n");         break;
        case  1: _ReportOutf("    Port size: 8-bit\n");         break;
        case  2: _ReportOutf("    Port size: 16-bit\n");         break;
        case  3: _ReportOutf("    Port size: 24-bit\n");         break;
        case  4: _ReportOutf("    Port size: 32-bit\n");         break;
        case  5: _ReportOutf("    Port size: 48-bit\n");         break;
        case  6: _ReportOutf("    Port size: 64-bit\n");         break;
        case  7: _ReportOutf("    Port size: reserved (7)\n");         break;
        case  8: _ReportOutf("    Port size: 1-bit\n");         break;
        case  9: _ReportOutf("    Port size: 2-bit\n");         break;
        case 10: _ReportOutf("    Port size: reserved (10)\n");         break;
        case 11: _ReportOutf("    Port size: reserved (11)\n");         break;
        case 12: _ReportOutf("    Port size: reserved (12)\n");         break;
        case 13: _ReportOutf("    Port size: reserved (13)\n");         break;
        case 14: _ReportOutf("    Port size: User defined 1\n");         break;
        case 15: _ReportOutf("    Port size: User defined 2\n");         break;
      }
      if (CtrlReg & (1 << 7)) {
        _ReportOutf("    Enable FIFOFULL (Stall CPU)\n");
      }
      if (CtrlReg & (1 << 8)) {
        _ReportOutf("    BranchOutput: Output all branch addresses\n");
      }
      if (CtrlReg & (1 << 9)) {
        _ReportOutf("    DebugRequestControl: Halt CPU on trigger\n");
      }
      if (CtrlReg & (1 << 10)) {
        _ReportOutf("    ETM Programming\n");
      }
      if (CtrlReg & (1 << 11)) {
        _ReportOutf("    ETM Port selection (bit 11) = 1:  Trace port pins enabled\n");
      } else {
        _ReportOutf("    ETM Port selection (bit 11) = 0:  Trace port pins disabled\n");
      }
      if (CtrlReg & (1 << 12)) {
        _ReportOutf("    Cycle accurate tracing\n");
      }
      v = ((CtrlReg >> 16) & 3) |  (((CtrlReg >> 13) & 1)  << 2);
      _ReportOutf("    Mode, Clocking: ");
      switch (v) {
        case 0: _ReportOutf("Normal, rising edge\n");   break;
        case 1: _ReportOutf("Multiplexed, both edges\n");   break;
        case 2: _ReportOutf("Demultiplexed, rising edge\n");   break;
        case 3: _ReportOutf("Illegal (3)\n");   break;
        case 4: _ReportOutf("Normal, Half-rate (Data on both clock edges, DDR)\n");   break;
        case 5: _ReportOutf("Illegal (5)\n");   break;
        case 6: _ReportOutf("Demultiplexed, Half-rate (Data on both clock edges, DDR)\n");   break;
        case 7: _ReportOutf("Illegal (7)\n");   break;
      }
      v = (CtrlReg >> 14) & 3;
      switch (v) {
        case 1: _ReportOutf("    ContextId: 8-bit\n");   break;
        case 2: _ReportOutf("    ContextId: 16-bit\n");   break;
        case 3: _ReportOutf("    ContextId: 32-bit\n");   break;
      }
      if (CtrlReg & (1 << 22)) {
        _ReportOutf("    ETM writes by debugger disabled\n");
      }
      if (ETMVersion >= 0x32) {
        if (CtrlReg & (1 << 23)) {
          _ReportOutf("    ETM writes by software disabled\n");
        }
      }
      //
      // Handle status register
      //
      if (ETMVersion >= 0x11) {
        if (StatusReg & (1 << 0)) {
          _ReportOutf("    Status: Overflow\n");
        }
      }
      if (ETMVersion >= 0x12) {
        if (StatusReg & (1 << 1)) {
          _ReportOutf("    Status: Programming\n");
        }
        if (StatusReg & (1 << 2)) {
          _ReportOutf("    Status: Trace start/stop is ON\n");
        } else {
          _ReportOutf("    Status: Trace start/stop is OFF\n");
        }
      }
      _ReportOutf("    Sequencer state: %d (1..3)\n", (JLINKARM_ETM_ReadReg(0x67) & 3) + 1);
    }
  }
}

/*********************************************************************
*
*       _ShowDebugState
*/
static void _ShowDebugState(void) {
  U32 DevFamily;
  DevFamily = JLINKARM_GetDeviceFamily();
  if ((DevFamily == 7) || (DevFamily == 9)) {
    U32 Data;
    Data = JLINKARM_ReadICEReg(0);
    _ReportOutf("DebugControl      (Ice[0x00]) :       %.2X (%s %s %s)\n"
      ,Data
      ,(Data & (1 << 2)) ? "INTDIS" : "      "
      ,(Data & (1 << 1)) ? "DBGRQ " : "      "
      ,(Data & (1 << 0)) ? "DBGACK" : "      "
      );
    Data = JLINKARM_ReadICEReg(1);
    _ReportOutf("DebugStatus       (Ice[0x01]) :       %.2X (%s %s %s %s %s)\n"
      ,Data 
      ,(Data & (1 << 4)) ? "THUMB " : "ARM   "
      ,(Data & (1 << 3)) ? "nMREQ " : "      "
      ,(Data & (1 << 2)) ? "IFEN  " : "      "
      ,(Data & (1 << 1)) ? "DBGRQ " : "      "
      ,(Data & (1 << 0)) ? "DBGACK" : "      "
      );
    Data = JLINKARM_ReadICEReg(4);
    _ReportOutf("DCC Control       (Ice[0x04]) : %.8X\n", Data);
    Data = JLINKARM_ReadICEReg(5);
    _ReportOutf("DCC Data          (Ice[0x05]) : %.8X\n", Data);

    Data = JLINKARM_ReadICEReg(8);
    _ReportOutf("WP0 Addr          (Ice[0x08]) : %.8X\n", Data);
    Data = JLINKARM_ReadICEReg(9);
    _ReportOutf("WP0 Addr Mask     (Ice[0x09]) : %.8X\n", Data);
    Data = JLINKARM_ReadICEReg(0xa);
    _ReportOutf("WP0 Data          (Ice[0x0a]) : %.8X\n", Data);
    Data = JLINKARM_ReadICEReg(0xb);
    _ReportOutf("WP0 Data Mask     (Ice[0x0b]) : %.8X\n", Data);
    Data = JLINKARM_ReadICEReg(0xc);
    _ReportOutf("WP0 Control       (Ice[0x0c]) :      %.3X\n", Data & ((1<<9) - 1));
    Data = JLINKARM_ReadICEReg(0xd);
    _ReportOutf("WP0 Control Mask  (Ice[0x0d]) :       %.2X\n", Data & ((1<<8) - 1));

    Data = JLINKARM_ReadICEReg(0x10);
    _ReportOutf("WP1 Addr          (Ice[0x10]) : %.8X\n", Data);
    Data = JLINKARM_ReadICEReg(0x11);
    _ReportOutf("WP1 Addr Mask     (Ice[0x11]) : %.8X\n", Data);
    Data = JLINKARM_ReadICEReg(0x12);
    _ReportOutf("WP1 Data          (Ice[0x12]) : %.8X\n", Data);
    Data = JLINKARM_ReadICEReg(0x13);
    _ReportOutf("WP1 Data Mask     (Ice[0x13]) : %.8X\n", Data);
    Data = JLINKARM_ReadICEReg(0x14);
    _ReportOutf("WP1 Control       (Ice[0x14]) :      %.3X\n", Data & ((1<<9) - 1));
    Data = JLINKARM_ReadICEReg(0x15);
    _ReportOutf("WP1 Control Mask  (Ice[0x15]) :       %.2X\n", Data  & ((1<<8) - 1));
  }
}

/*********************************************************************
*
*       _ShowETBState
*/
static void _ShowETBState(void) {
  if (JLINKARM_ETB_IsPresent()) {
    U32 Data;
    _ReportOutf("ETB is present.\n");
    Data = JLINKARM_ETB_ReadReg(0);
    _ReportOutf("ID register       (ETB[0x00]) : %.8X\n", Data);
    Data = JLINKARM_ETB_ReadReg(1);
    _ReportOutf("RAM depth         (ETB[0x01]) : %.8X\n", Data);
    Data = JLINKARM_ETB_ReadReg(2);
    _ReportOutf("RAM width         (ETB[0x02]) : %.8X\n", Data);
    Data = JLINKARM_ETB_ReadReg(3);
    _ReportOutf("Status            (ETB[0x03]) : %.8X\n", Data);
    Data = JLINKARM_ETB_ReadReg(4);
    _ReportOutf("RAM data          (ETB[0x04]) : %.8X\n", Data);
    Data = JLINKARM_ETB_ReadReg(5);
    _ReportOutf("RAM read pointer  (ETB[0x05]) : %.8X\n", Data);
    Data = JLINKARM_ETB_ReadReg(6);
    _ReportOutf("RAM write pointer (ETB[0x06]) : %.8X\n", Data);
    Data = JLINKARM_ETB_ReadReg(7);
    _ReportOutf("Trigger counter   (ETB[0x07]) : %.8X\n", Data);
    Data = JLINKARM_ETB_ReadReg(8);
    _ReportOutf("Control           (ETB[0x08]) : %.8X\n", Data);
  } else {
    _ReportOutf("ETB is not present.\n");
  }
}

/*********************************************************************
*
*       _GetNumRegs
*/
static int _GetNumRegs(void) {
  U32 DevFamily;
  int NumRegs;

  DevFamily = JLINKARM_GetDeviceFamily();
  switch (DevFamily) {
  case JLINKARM_DEV_FAMILY_CM0:       NumRegs = JLINKARM_CM3_NUM_REGS;       break;
  case JLINKARM_DEV_FAMILY_CM1:       NumRegs = JLINKARM_CM3_NUM_REGS;       break;
  case JLINKARM_DEV_FAMILY_CM3:       NumRegs = JLINKARM_CM3_NUM_REGS;       break;
  case JLINKARM_DEV_FAMILY_CM4:       NumRegs = JLINKARM_CM4_NUM_REGS;       break;
  case JLINKARM_DEV_FAMILY_CORTEX_R4: NumRegs = JLINKARM_CORTEX_R4_NUM_REGS; break;
  case JLINKARM_DEV_FAMILY_RX:        NumRegs = JLINKARM_RX_NUM_REGS;        break;
  case JLINKARM_DEV_FAMILY_POWER_PC:  NumRegs = JLINK_POWER_PC_NUM_REGS;     break;
  case JLINK_DEV_FAMILY_MIPS:         NumRegs = JLINK_MIPS_NUM_REGS;         break;
  default:                            NumRegs = ARM_NUM_REGS;                break;
  }
  return NumRegs;
}

/*********************************************************************
*
*       _PrintAPSR
*/
static void _PrintAPSR(U8* pBuffer, int BufferSize, U32 Value) {
  U8 ac[16];
  int Tmp;
  int i;

  i = 0;
  ac[i++] = ((Value >> 31) & 1) ? 'N' : 'n';
  ac[i++] = ((Value >> 30) & 1) ? 'Z' : 'z';
  ac[i++] = ((Value >> 29) & 1) ? 'C' : 'c';
  ac[i++] = ((Value >> 28) & 1) ? 'V' : 'v';
  ac[i++] = ((Value >> 27) & 1) ? 'Q' : 'q';
  ac[i] = '\0';
  Tmp = MIN(i, BufferSize - 1);
  memcpy(pBuffer, ac, Tmp);
  *(pBuffer + Tmp) = '\0';
}

/*********************************************************************
*
*       _PrintIPSR
*/
static void _PrintIPSR(U8* pBuffer, int BufferSize, U32 Value) {
  U8 ac[128];
  int Tmp;
  int Len;

  Value &= 0x1F;
  Len = sprintf(&ac[0], "%.3X (%s)", Value, _aIPSR2Str[Value]);
  Tmp = MIN(Len, BufferSize - 1);
  memcpy(pBuffer, ac, Tmp);
  *(pBuffer + Tmp) = '\0';
}

/*********************************************************************
*
*       _IsValidCPUMode_ARM79
*/
static int _IsValidCPUMode_ARM79(U32 Mode) {
  if (
      (Mode == 0x10) ||               // User mode
      (Mode == 0x11) ||               // FIQ mode
      (Mode == 0x12) ||               // IRQ mode
      (Mode == 0x13) ||               // Supervisor mode
      (Mode == 0x17) ||               // Abort mode
      (Mode == 0x1B) ||               // Undefined mode
      (Mode == 0x1F)                  // System mode
     )
  {
    return 1;
  }
  return 0;
}

/*********************************************************************
*
*       _ShowRegs
*/
static void  _ShowRegs(void) {
  U32 DevFamily;
  U32 cpacr;
  U32 aRegData[65];
  U8 acAPSR[64];
  U8 acIPSR[64];

  DevFamily = JLINKARM_GetDeviceFamily();
  if ((DevFamily == JLINKARM_DEV_FAMILY_CM0) || (DevFamily == JLINKARM_DEV_FAMILY_CM1) || (DevFamily == JLINKARM_DEV_FAMILY_CM3) || (DevFamily == JLINKARM_DEV_FAMILY_CM4)) {
    U32 aRegIndex[] = {
      JLINKARM_CM3_REG_R0,  JLINKARM_CM3_REG_R1,  JLINKARM_CM3_REG_R2,  JLINKARM_CM3_REG_R3,
      JLINKARM_CM3_REG_R4,  JLINKARM_CM3_REG_R5,  JLINKARM_CM3_REG_R6,  JLINKARM_CM3_REG_R7,
      JLINKARM_CM3_REG_R8,  JLINKARM_CM3_REG_R9,  JLINKARM_CM3_REG_R10, JLINKARM_CM3_REG_R11,
      JLINKARM_CM3_REG_R12, JLINKARM_CM3_REG_R13, JLINKARM_CM3_REG_MSP, JLINKARM_CM3_REG_PSP,
      JLINKARM_CM3_REG_R14, JLINKARM_CM3_REG_R15,
      JLINKARM_CM3_REG_XPSR, JLINKARM_CM3_REG_APSR, JLINKARM_CM3_REG_EPSR, JLINKARM_CM3_REG_IPSR,
      JLINKARM_CM3_REG_CFBP, JLINKARM_CM3_REG_CONTROL, JLINKARM_CM3_REG_FAULTMASK, JLINKARM_CM3_REG_BASEPRI, JLINKARM_CM3_REG_PRIMASK,
      JLINKARM_CM3_REG_DWT_CYCCNT
    };
    JLINKARM_ReadRegs(&aRegIndex[0], &aRegData[0], NULL, COUNTOF(aRegIndex));
    _ReportOutf("PC = %.8X, CycleCnt = %.8X\n", aRegData[17], aRegData[27]);
    _ReportOutf("R0 = %.8X, R1 = %.8X, R2 = %.8X, R3 = %.8X\n", aRegData[0],  aRegData[1],  aRegData[2],  aRegData[3]);
    _ReportOutf("R4 = %.8X, R5 = %.8X, R6 = %.8X, R7 = %.8X\n", aRegData[4],  aRegData[5],  aRegData[6],  aRegData[7]);
    _ReportOutf("R8 = %.8X, R9 = %.8X, R10= %.8X, R11= %.8X\n", aRegData[8],  aRegData[9],  aRegData[10], aRegData[11]);
    _ReportOutf("R12= %.8X\n", aRegData[12]);
    _ReportOutf("SP(R13)= %.8X, MSP= %.8X, PSP= %.8X, R14(LR) = %.8X\n", aRegData[13], aRegData[14], aRegData[15], aRegData[16]);
    _PrintAPSR(acAPSR, sizeof(acAPSR), aRegData[19]);
    _PrintIPSR(acIPSR, sizeof(acIPSR), aRegData[21]);
    _ReportOutf("XPSR = %.8X: APSR = %s, EPSR = %.8X, IPSR = %s\n", aRegData[18], acAPSR, aRegData[20], acIPSR);
    _ReportOutf("CFBP = %.8X, CONTROL = %.2X, FAULTMASK = %.2X, BASEPRI = %.2X, PRIMASK = %.2X\n", aRegData[22], aRegData[23] >> 24, aRegData[24] >> 16, aRegData[25] >> 8, aRegData[26] >> 0);

    if (DevFamily == JLINKARM_DEV_FAMILY_CM4) {
      U32 aRegIndex[] = {
        JLINKARM_CM4_REG_FPS0,  JLINKARM_CM4_REG_FPS1,  JLINKARM_CM4_REG_FPS2,  JLINKARM_CM4_REG_FPS3,
        JLINKARM_CM4_REG_FPS4,  JLINKARM_CM4_REG_FPS5,  JLINKARM_CM4_REG_FPS6,  JLINKARM_CM4_REG_FPS7,
        JLINKARM_CM4_REG_FPS8,  JLINKARM_CM4_REG_FPS9,  JLINKARM_CM4_REG_FPS10, JLINKARM_CM4_REG_FPS11,
        JLINKARM_CM4_REG_FPS12, JLINKARM_CM4_REG_FPS13, JLINKARM_CM4_REG_FPS14, JLINKARM_CM4_REG_FPS15,
        JLINKARM_CM4_REG_FPS16, JLINKARM_CM4_REG_FPS17, JLINKARM_CM4_REG_FPS18, JLINKARM_CM4_REG_FPS19,
        JLINKARM_CM4_REG_FPS20, JLINKARM_CM4_REG_FPS21, JLINKARM_CM4_REG_FPS22, JLINKARM_CM4_REG_FPS23,
        JLINKARM_CM4_REG_FPS24, JLINKARM_CM4_REG_FPS25, JLINKARM_CM4_REG_FPS26, JLINKARM_CM4_REG_FPS27,
        JLINKARM_CM4_REG_FPS28, JLINKARM_CM4_REG_FPS29, JLINKARM_CM4_REG_FPS30, JLINKARM_CM4_REG_FPS31,
        JLINKARM_CM4_REG_FPSCR
      };
      JLINKARM_ReadMemU32(0xE000ED88, 1, &cpacr, NULL);   // Coprocessor Access Control Register
      if (((cpacr >> 20) & 0xF) != 0xF) {
        _ReportOutf("FPU regs: FPU not enabled / not implemented on connected CPU.\n");
      } else {
        JLINKARM_ReadRegs(&aRegIndex[0], &aRegData[0], NULL, COUNTOF(aRegIndex));
        _ReportOutf("\n");
        _ReportOutf("FPS0 = %.8X, FPS1 = %.8X, FPS2 = %.8X, FPS3 = %.8X\n", aRegData[0],  aRegData[1],  aRegData[2],  aRegData[3]);
        _ReportOutf("FPS4 = %.8X, FPS5 = %.8X, FPS6 = %.8X, FPS7 = %.8X\n", aRegData[4],  aRegData[5],  aRegData[6],  aRegData[7]);
        _ReportOutf("FPS8 = %.8X, FPS9 = %.8X, FPS10= %.8X, FPS11= %.8X\n", aRegData[8],  aRegData[9],  aRegData[10], aRegData[11]);
        _ReportOutf("FPS12= %.8X, FPS13= %.8X, FPS14= %.8X, FPS15= %.8X\n", aRegData[12], aRegData[13], aRegData[14], aRegData[15]);
        _ReportOutf("FPS16= %.8X, FPS17= %.8X, FPS18= %.8X, FPS19= %.8X\n", aRegData[16], aRegData[17], aRegData[18], aRegData[19]);
        _ReportOutf("FPS20= %.8X, FPS21= %.8X, FPS22= %.8X, FPS23= %.8X\n", aRegData[20], aRegData[21], aRegData[22], aRegData[23]);
        _ReportOutf("FPS24= %.8X, FPS25= %.8X, FPS26= %.8X, FPS27= %.8X\n", aRegData[24], aRegData[25], aRegData[26], aRegData[27]);
        _ReportOutf("FPS28= %.8X, FPS29= %.8X, FPS30= %.8X, FPS31= %.8X\n", aRegData[28], aRegData[29], aRegData[30], aRegData[31]);
        _ReportOutf("FPSCR= %.8X\n", aRegData[32]);
      }
    }
  } else if(DevFamily == JLINKARM_DEV_FAMILY_RX) {
    _ReportOutf("R0 = %.8X, R1 = %.8X, R2 = %.8X, R3 = %.8X\n",            JLINKARM_ReadReg(JLINKARM_RX_REG_R0), JLINKARM_ReadReg(JLINKARM_RX_REG_R1), JLINKARM_ReadReg(JLINKARM_RX_REG_R2), JLINKARM_ReadReg(JLINKARM_RX_REG_R3));
    _ReportOutf("R4 = %.8X, R5 = %.8X, R6 = %.8X, R7 = %.8X\n",            JLINKARM_ReadReg(JLINKARM_RX_REG_R4), JLINKARM_ReadReg(JLINKARM_RX_REG_R5), JLINKARM_ReadReg(JLINKARM_RX_REG_R6), JLINKARM_ReadReg(JLINKARM_RX_REG_R7));
    _ReportOutf("R8 = %.8X, R9 = %.8X, R10= %.8X, R11= %.8X\n",            JLINKARM_ReadReg(JLINKARM_RX_REG_R8), JLINKARM_ReadReg(JLINKARM_RX_REG_R9), JLINKARM_ReadReg(JLINKARM_RX_REG_R10), JLINKARM_ReadReg(JLINKARM_RX_REG_R11));
    _ReportOutf("R12= %.8X, R13= %.8X, R14= %.8X, R15= %.8X\n",            JLINKARM_ReadReg(JLINKARM_RX_REG_R12),JLINKARM_ReadReg(JLINKARM_RX_REG_R13), JLINKARM_ReadReg(JLINKARM_RX_REG_R14),JLINKARM_ReadReg(JLINKARM_RX_REG_R15));
    _ReportOutf("ISP %.8X, USP %.8X, INTB %.8X, PC %.8X,\n",               JLINKARM_ReadReg(JLINKARM_RX_REG_ISP), JLINKARM_ReadReg(JLINKARM_RX_REG_USP), JLINKARM_ReadReg(JLINKARM_RX_REG_INTB), JLINKARM_ReadReg(JLINKARM_RX_REG_PC));
    _ReportOutf("PSW %.8X, BPC %.8X, BPSW %.8X, FINTV %.8X, FPSW %.8X\n",  JLINKARM_ReadReg(JLINKARM_RX_REG_PSW), JLINKARM_ReadReg(JLINKARM_RX_REG_BPC), JLINKARM_ReadReg(JLINKARM_RX_REG_BPSW), JLINKARM_ReadReg(JLINKARM_RX_REG_FINTV), JLINKARM_ReadReg(JLINKARM_RX_REG_FPSW));
  } else if(DevFamily == JLINKARM_DEV_FAMILY_POWER_PC) {
    _ReportOutf("R0 = %.8X, R1 = %.8X, R2 = %.8X, R3 = %.8X\n",              JLINKARM_ReadReg(JLINK_POWER_PC_REG_R0),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_R1),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_R2),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_R3));
    _ReportOutf("R4 = %.8X, R5 = %.8X, R6 = %.8X, R7 = %.8X\n",              JLINKARM_ReadReg(JLINK_POWER_PC_REG_R4),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_R5),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_R6),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_R7));
    _ReportOutf("R8 = %.8X, R9 = %.8X, R10= %.8X, R11= %.8X\n",              JLINKARM_ReadReg(JLINK_POWER_PC_REG_R8),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_R9),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_R10), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R11));
    _ReportOutf("R12= %.8X, R13= %.8X, R14= %.8X, R15= %.8X\n",              JLINKARM_ReadReg(JLINK_POWER_PC_REG_R12), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R13), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R14), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R15));
    _ReportOutf("R16= %.8X, R17= %.8X, R18= %.8X, R19= %.8X\n",              JLINKARM_ReadReg(JLINK_POWER_PC_REG_R16), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R17), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R18), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R19));
    _ReportOutf("R20= %.8X, R21= %.8X, R22= %.8X, R23= %.8X\n",              JLINKARM_ReadReg(JLINK_POWER_PC_REG_R20), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R21), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R22), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R23));
    _ReportOutf("R24= %.8X, R25= %.8X, R26= %.8X, R27= %.8X\n",              JLINKARM_ReadReg(JLINK_POWER_PC_REG_R24), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R25), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R26), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R27));
    _ReportOutf("R28= %.8X, R29= %.8X, R30= %.8X, R31= %.8X\n",              JLINKARM_ReadReg(JLINK_POWER_PC_REG_R28), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R29), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R30), JLINKARM_ReadReg(JLINK_POWER_PC_REG_R31));
    _ReportOutf("CR = %.8X, CTR= %.8X, LR = %.8X, XER= %.8X\n",              JLINKARM_ReadReg(JLINK_POWER_PC_REG_CR),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_CTR), JLINKARM_ReadReg(JLINK_POWER_PC_REG_LR),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_XER));
    _ReportOutf("PC = %.8X\n\n",                                             JLINKARM_ReadReg(JLINK_POWER_PC_REG_PC));
    _ReportOutf("PVR   =%.8X, PIR   =%.8X, SVR   =%.8X\n",                   JLINKARM_ReadReg(JLINK_POWER_PC_REG_PVR),   JLINKARM_ReadReg(JLINK_POWER_PC_REG_PIR),    JLINKARM_ReadReg(JLINK_POWER_PC_REG_SVR));
    _ReportOutf("MSR   =%.8X, HID0  =%.8X, HID1  =%.8X\n",                   JLINKARM_ReadReg(JLINK_POWER_PC_REG_MSR),   JLINKARM_ReadReg(JLINK_POWER_PC_REG_HID0),   JLINKARM_ReadReg(JLINK_POWER_PC_REG_HID1));
    _ReportOutf("SPRG0 =%.8X, SPRG1 =%.8X, SRR0  =%.8X, SRR1  =%.8X\n",      JLINKARM_ReadReg(JLINK_POWER_PC_REG_SPRG0), JLINKARM_ReadReg(JLINK_POWER_PC_REG_SPRG1),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_SRR0),   JLINKARM_ReadReg(JLINK_POWER_PC_REG_SRR1));
    _ReportOutf("CSRR0 =%.8X, CSRR1 =%.8X, DSRR0 =%.8X, DSRR1 =%.8X\n",      JLINKARM_ReadReg(JLINK_POWER_PC_REG_CSRR0), JLINKARM_ReadReg(JLINK_POWER_PC_REG_CSRR1),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_DSRR0),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_DSRR1));
    _ReportOutf("ESR   =%.8X, MCSR  =%.8X, DEAR  =%.8X, IVPR  =%.8X\n",      JLINKARM_ReadReg(JLINK_POWER_PC_REG_ESR),   JLINKARM_ReadReg(JLINK_POWER_PC_REG_MCSR),   JLINKARM_ReadReg(JLINK_POWER_PC_REG_DEAR),   JLINKARM_ReadReg(JLINK_POWER_PC_REG_IVPR));
    _ReportOutf("PID0  =%.8X, MMUCFG=%.8X, L1CFG0=%.8X, BUCSR =%.8X\n",      JLINKARM_ReadReg(JLINK_POWER_PC_REG_PID0),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_MMUCFG), JLINKARM_ReadReg(JLINK_POWER_PC_REG_L1CFG0), JLINKARM_ReadReg(JLINK_POWER_PC_REG_BUCSR));
    _ReportOutf("DBCR0 =%.8X, DBCR1 =%.8X, DBCR2 =%.8X, DBSR  =%.8X\n",      JLINKARM_ReadReg(JLINK_POWER_PC_REG_DBCR0), JLINKARM_ReadReg(JLINK_POWER_PC_REG_DBCR1),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_DBCR2),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_DBSR));
    _ReportOutf("IAC1  =%.8X, IAC2  =%.8X, IAC3  =%.8X, IAC4  =%.8X\n",      JLINKARM_ReadReg(JLINK_POWER_PC_REG_IAC1),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_IAC2),   JLINKARM_ReadReg(JLINK_POWER_PC_REG_IAC3),   JLINKARM_ReadReg(JLINK_POWER_PC_REG_IAC4));
    _ReportOutf("DAC1  =%.8X, DAC2  =%.8X\n",                                JLINKARM_ReadReg(JLINK_POWER_PC_REG_DAC1),  JLINKARM_ReadReg(JLINK_POWER_PC_REG_DAC2));
  } else if(DevFamily == JLINK_DEV_FAMILY_MIPS) {
    U32 aRegIndex[] = {
                        JLINK_MIPS_REG_R0,  JLINK_MIPS_REG_R1,  JLINK_MIPS_REG_R2,  JLINK_MIPS_REG_R3,                                 // 0-3
                        JLINK_MIPS_REG_R4,  JLINK_MIPS_REG_R5,  JLINK_MIPS_REG_R6,  JLINK_MIPS_REG_R7,                                 // 4-7
                        JLINK_MIPS_REG_R8,  JLINK_MIPS_REG_R9,  JLINK_MIPS_REG_R10, JLINK_MIPS_REG_R11,                                // 8-11
                        JLINK_MIPS_REG_R12, JLINK_MIPS_REG_R13, JLINK_MIPS_REG_R14, JLINK_MIPS_REG_R15,                                // 12-15
                        JLINK_MIPS_REG_R16, JLINK_MIPS_REG_R17, JLINK_MIPS_REG_R18, JLINK_MIPS_REG_R19,                                // 16-19
                        JLINK_MIPS_REG_R20, JLINK_MIPS_REG_R21, JLINK_MIPS_REG_R22, JLINK_MIPS_REG_R23,                                // 20-23
                        JLINK_MIPS_REG_R24, JLINK_MIPS_REG_R25, JLINK_MIPS_REG_R26, JLINK_MIPS_REG_R27,                                // 24-27
                        JLINK_MIPS_REG_R28, JLINK_MIPS_REG_R29, JLINK_MIPS_REG_R30, JLINK_MIPS_REG_R31,                                // 28-31
                        JLINK_MIPS_REG_HWRENA, JLINK_MIPS_REG_BADVADDR, JLINK_MIPS_REG_COUNT, JLINK_MIPS_REG_COMPARE,                  // 32-35
                        JLINK_MIPS_REG_STATUS, JLINK_MIPS_REG_INTCTL, JLINK_MIPS_REG_SRSCTL, JLINK_MIPS_REG_SRSMAP,                    // 36-39
                        JLINK_MIPS_REG_CAUSE, JLINK_MIPS_REG_EPC, JLINK_MIPS_REG_PRID, JLINK_MIPS_REG_EBASE,                           // 40-43
                        JLINK_MIPS_REG_CONFIG, JLINK_MIPS_REG_CONFIG1, JLINK_MIPS_REG_CONFIG2, JLINK_MIPS_REG_CONFIG3,                 // 44-47
                        JLINK_MIPS_REG_DEBUG, JLINK_MIPS_REG_TRACECONTROL, JLINK_MIPS_REG_TRACECONTROL2, JLINK_MIPS_REG_USERTRACEDATA, // 48-51
                        JLINK_MIPS_REG_TRACEBPC, JLINK_MIPS_REG_DEBUG2, JLINK_MIPS_REG_PC, JLINK_MIPS_REG_ERROR_PC,                    // 52-55
                        JLINK_MIPS_REG_DESAVE, JLINK_MIPS_REG_HI, JLINK_MIPS_REG_LO                                                    // 56-58
                      };
    JLINKARM_ReadRegs(&aRegIndex[0], &aRegData[0], NULL, COUNTOF(aRegIndex));
    _ReportOutf("PC = %.8X, Status = %.8X, Count = %.8X\n",                  aRegData[54], aRegData[36], aRegData[34]);
    _ReportOutf("R0  = %.8X, R1  = %.8X, R2  = %.8X, R3  = %.8X,\n",         aRegData[0],   aRegData[1],   aRegData[2],  aRegData[3]);
    _ReportOutf("R4  = %.8X, R5  = %.8X, R6  = %.8X, R7  = %.8X,\n",         aRegData[4],   aRegData[5],   aRegData[6],  aRegData[7]);
    _ReportOutf("R8  = %.8X, R9  = %.8X, R10 = %.8X, R11 = %.8X,\n",         aRegData[8],   aRegData[9],   aRegData[10], aRegData[11]);
    _ReportOutf("R12 = %.8X, R13 = %.8X, R14 = %.8X, R15 = %.8X,\n",         aRegData[12],  aRegData[13],  aRegData[14], aRegData[15]);
    _ReportOutf("R16 = %.8X, R17 = %.8X, R18 = %.8X, R19 = %.8X,\n",         aRegData[16],  aRegData[17],  aRegData[18], aRegData[19]);
    _ReportOutf("R20 = %.8X, R21 = %.8X, R22 = %.8X, R23 = %.8X,\n",         aRegData[20],  aRegData[21],  aRegData[22], aRegData[23]);
    _ReportOutf("R24 = %.8X, R25 = %.8X, R26 = %.8X, R27 = %.8X,\n",         aRegData[24],  aRegData[25],  aRegData[26], aRegData[27]);
    _ReportOutf("R28 = %.8X, R29 = %.8X, R30 = %.8X, R31 = %.8X,\n",         aRegData[28],  aRegData[29],  aRegData[30], aRegData[31]);
    _ReportOutf("HWREna = %.8X, BadVAddr = %.8X,\n"              ,           aRegData[32],  aRegData[33]);
    _ReportOutf("Compare = %.8X, IntCtl = %.8X,\n",                          aRegData[35],  aRegData[37]);
    _ReportOutf("SRSCtl = %.8X, SRSMap = %.8X, Cause = %.8X,\n",             aRegData[38],  aRegData[39],  aRegData[40]);
    _ReportOutf("EPC = %.8X, PrID = %.8X, EBASE = %.8X,\n",                  aRegData[41],  aRegData[42],  aRegData[43]);
    _ReportOutf("Config = %.8X, Config1 = %.8X, Config2 = %.8X,\n",          aRegData[44],  aRegData[45],  aRegData[46]);
    _ReportOutf("Config3 = %.8X, Debug = %.8X, TraceCtl = %.8X,\n",          aRegData[47],  aRegData[48],  aRegData[49]);
    _ReportOutf("TraceCtl2 = %.8X, UsrTraceData = %.8X, TraceBPC = %.8X,\n", aRegData[50],  aRegData[51],  aRegData[52]);
    _ReportOutf("Debug2 = %.8X, ErrorPC = %.8X,\n",                          aRegData[53],  aRegData[55]);
    _ReportOutf("HI = %.8X, LO = %.8X\n",                                    aRegData[57],  aRegData[58]);
  } else if (DevFamily == JLINK_DEV_FAMILY_EFM8) {
    U32 aRegIndexSFRs[] = {
                            JLINK_EFM8_PC, JLINK_EFM8_PSW, JLINK_EFM8_SP, JLINK_EFM8_A, JLINK_EFM8_B, JLINK_EFM8_DPTR, 
                            JLINK_EFM8_R0, JLINK_EFM8_R1, JLINK_EFM8_R2, JLINK_EFM8_R3, JLINK_EFM8_R4, JLINK_EFM8_R5, JLINK_EFM8_R6, JLINK_EFM8_R7
                          };
    JLINKARM_ReadRegs(&aRegIndexSFRs[0], &aRegData[0], NULL, COUNTOF(aRegIndexSFRs));
    _ReportOutf("PC = %.4X, PSW = %.2X, SP = %.2X, A = %.2X, B = %.2X, DPTR = %.4X\n",                          aRegData[0], aRegData[1], aRegData[2], aRegData[3], aRegData[4], aRegData[5]);
    _ReportOutf("R0 = %.2X, R1 = %.2X, R2 = %.2X, R3 = %.2X, R4 = %.2X, R5 = %.2X, R6 = %.2X, R7 = %.2X\n",     aRegData[6],   aRegData[7],   aRegData[8],  aRegData[9], aRegData[10],   aRegData[11],   aRegData[12],  aRegData[13]);
  } else {
    U32 CPSR = JLINKARM_ReadReg(ARM_REG_CPSR);
    _ReportOutf("PC: (R15) = %.8X, CPSR = %.8X (%s)\n", JLINKARM_ReadReg(ARM_REG_R15), CPSR, _PrintCPSR(CPSR));
    // R0..R7
    _ReportOutf("Current:\n");
    _ReportOutf("     R0 =%.8X, R1 =%.8X, R2 =%.8X, R3 =%.8X\n"
           "     R4 =%.8X, R5 =%.8X, R6 =%.8X, R7 =%.8X\n", JLINKARM_ReadReg(ARM_REG_R0), JLINKARM_ReadReg(ARM_REG_R1), JLINKARM_ReadReg(ARM_REG_R2), JLINKARM_ReadReg(ARM_REG_R3)
                                                          , JLINKARM_ReadReg(ARM_REG_R4), JLINKARM_ReadReg(ARM_REG_R5), JLINKARM_ReadReg(ARM_REG_R6), JLINKARM_ReadReg(ARM_REG_R7));
    // Current
    if (_IsValidCPUMode_ARM79(CPSR & 0x1F)) {
      _ReportOutf("     R8 =%.8X, R9 =%.8X, R10=%.8X, R11=%.8X, R12=%.8X\n"
             "     R13=%.8X, R14=%.8X" 
             , JLINKARM_ReadReg(ARM_REG_R8),  JLINKARM_ReadReg(ARM_REG_R9),  JLINKARM_ReadReg(ARM_REG_R10), JLINKARM_ReadReg(ARM_REG_R11)
             , JLINKARM_ReadReg(ARM_REG_R12), JLINKARM_ReadReg(ARM_REG_R13), JLINKARM_ReadReg(ARM_REG_R14));
      if (((CPSR & 0x1F) != 0x10) && ((CPSR & 0x1F) != 0x1F)) {
        _ReportOutf(", SPSR=%.8X\n", JLINKARM_ReadReg(ARM_REG_SPSR));
      } else {
        _ReportOutf("\n");
      }
    }
    // USR
    _ReportOutf("USR: R8 =%.8X, R9 =%.8X, R10=%.8X, R11=%.8X, R12=%.8X\n" 
           , JLINKARM_ReadReg(ARM_REG_R8_USR), JLINKARM_ReadReg(ARM_REG_R9_USR), JLINKARM_ReadReg(ARM_REG_R10_USR), JLINKARM_ReadReg(ARM_REG_R11_USR) , JLINKARM_ReadReg(ARM_REG_R12_USR));
    _ReportOutf("     R13=%.8X, R14=%.8X\n" 
           , JLINKARM_ReadReg(ARM_REG_R13_USR), JLINKARM_ReadReg(ARM_REG_R14_USR));
    // FIQ
    _ReportOutf("FIQ: R8 =%.8X, R9 =%.8X, R10=%.8X, R11=%.8X, R12=%.8X\n" 
           , JLINKARM_ReadReg(ARM_REG_R8_FIQ), JLINKARM_ReadReg(ARM_REG_R9_FIQ), JLINKARM_ReadReg(ARM_REG_R10_FIQ), JLINKARM_ReadReg(ARM_REG_R11_FIQ), JLINKARM_ReadReg(ARM_REG_R12_FIQ));
    _ReportOutf("     R13=%.8X, R14=%.8X, SPSR=%.8X\n" 
           , JLINKARM_ReadReg(ARM_REG_R13_FIQ), JLINKARM_ReadReg(ARM_REG_R14_FIQ), JLINKARM_ReadReg(ARM_REG_SPSR_FIQ));
    _ReportOutf("IRQ: R13=%.8X, R14=%.8X, SPSR=%.8X\n", JLINKARM_ReadReg(ARM_REG_R13_IRQ), JLINKARM_ReadReg(ARM_REG_R14_IRQ), JLINKARM_ReadReg(ARM_REG_SPSR_IRQ));
    _ReportOutf("SVC: R13=%.8X, R14=%.8X, SPSR=%.8X\n", JLINKARM_ReadReg(ARM_REG_R13_SVC), JLINKARM_ReadReg(ARM_REG_R14_SVC), JLINKARM_ReadReg(ARM_REG_SPSR_SVC));
    _ReportOutf("ABT: R13=%.8X, R14=%.8X, SPSR=%.8X\n", JLINKARM_ReadReg(ARM_REG_R13_ABT), JLINKARM_ReadReg(ARM_REG_R14_ABT), JLINKARM_ReadReg(ARM_REG_SPSR_ABT));
    _ReportOutf("UND: R13=%.8X, R14=%.8X, SPSR=%.8X\n", JLINKARM_ReadReg(ARM_REG_R13_UND), JLINKARM_ReadReg(ARM_REG_R14_UND), JLINKARM_ReadReg(ARM_REG_SPSR_UND));
  }
}

/*********************************************************************
*
*       _ShowHWStatus
*/
static void _ShowHWStatus(void) {
  JLINKARM_HW_STATUS Stat;
  U32 ITarget;
  JLINKARM_SPEED_INFO SpeedInfo;

  JLINKARM_GetHWStatus(&Stat);
  JLINKARM_GetHWInfo((1 << 2), &ITarget);
  SpeedInfo.SizeOfStruct = sizeof(SpeedInfo);
  JLINKARM_GetSpeedInfo(&SpeedInfo);

  _ReportOutf("VTarget=%d.%.3dV\n", Stat.VTarget / 1000, Stat.VTarget % 1000);
  if (ITarget != 0xFFFFFFFF) {
    _ReportOutf("ITarget=%dmA\n", ITarget);
  }
  if (Stat.tck == 255) {
    _ReportOutf("TCK=? ");
  } else {
    _ReportOutf("TCK=%d ", Stat.tck);
  }
  if (Stat.tdi == 255) {
    _ReportOutf("TDI=? ");
  } else {
    _ReportOutf("TDI=%d ", Stat.tdi);
  }
  if (Stat.tdo == 255) {
    _ReportOutf("TDO=? ");
  } else {
    _ReportOutf("TDO=%d ", Stat.tdo);
  }
  if (Stat.tms == 255) {
    _ReportOutf("TMS=? ");
  } else {
    _ReportOutf("TMS=%d ", Stat.tms);
  }
  if (Stat.tres == 255) {
    _ReportOutf("TRES=? ");
  } else {
    _ReportOutf("TRES=%d ", Stat.tres);
  }
  if (Stat.trst == 255) {
    _ReportOutf("TRST=?\n");
  } else {
    _ReportOutf("TRST=%d\n", Stat.trst);
  }
  //
  // Show supported target interface speeds
  //
  _ReportOutf("Supported target interface speeds:\n");
  if (SpeedInfo.BaseFreq > 1000) {
    _ReportOutf(" - %d MHz/n, (n>=%d). => %dkHz, %dkHz, %dkHz, ...\n", SpeedInfo.BaseFreq / 1000000, SpeedInfo.MinDiv,
                                                                  SpeedInfo.BaseFreq / 1000 / (SpeedInfo.MinDiv + 0),
                                                                  SpeedInfo.BaseFreq / 1000 / (SpeedInfo.MinDiv + 1),
                                                                  SpeedInfo.BaseFreq / 1000 / (SpeedInfo.MinDiv + 2));
  }
  if (SpeedInfo.SupportAdaptive) {
    _ReportOutf(" - Adaptive clocking\n");
  }
}

/*********************************************************************
*
*       _ShowCurrentSpeed
*/
static void _ShowCurrentSpeed(void) {
  U16 Speed;
  Speed = JLINKARM_GetSpeed();
  if (Speed == JLINKARM_SPEED_ADAPTIVE) {
    _ReportOutf("Using adaptive clocking instead of fixed JTAG speed.\n");
  } else {
    _ReportOutf("Target interface speed: %d kHz\n", Speed);
  }
}

/*********************************************************************
*
*       _ShowHWInfo
*/
static void _ShowHWInfo(void) {
  U32 aInfo[32];
  int i;

  JLINKARM_GetHWInfo(0xFFFFFFFF, &aInfo[0]);
  
  for (i = 0; i < 32; i++) {
    if (aInfo[i] != 0xFFFFFFFF) {
      switch (i) {
      case HW_INFO_POWER_ENABLED:
        _ReportOutf("HWInfo[%.2d] = Target power is %s\n", i, aInfo[i] ? "enabled" : "disabled");
        break;
      case HW_INFO_POWER_OVERCURRENT:
        switch (aInfo[i]) {
        case 0:                                                                                          break;
        case 1:          _ReportOutf("HWInfo[%.2d] = OverCurrent (2ms @ 3000mA)\n", i);                  break;
        case 2:          _ReportOutf("HWInfo[%.2d] = OverCurrent (10ms @ 1000mA)\n", i);                 break;
        case 3:          _ReportOutf("HWInfo[%.2d] = OverCurrent (40ms @ 400mA)\n", i);                  break;
        default:         _ReportOutf("HWInfo[%.2d] = OverCurrent (Unknown reason: %d)\n", i, aInfo[i]);  break;
        }
        break;
      case HW_INFO_ITARGET:
        _ReportOutf("HWInfo[%.2d] = %dmA\t(ITarget)\n", i, aInfo[i]);
        break;
      case HW_INFO_ITARGET_PEAK:
        _ReportOutf("HWInfo[%.2d] = %dmA\t(ITargetPeak)\n", i, aInfo[i]);
        break;
      case HW_INFO_ITARGET_PEAK_OPERATION:
        _ReportOutf("HWInfo[%.2d] = %dmA\t(ITargetPeakOperation)\n", i, aInfo[i]);
        break;
      case HW_INFO_ITARGET_MAX_TIME0:
        _ReportOutf("HWInfo[%.2d] = %dms\t(ITargetMaxTime0)\n", i, aInfo[i]);
        break;
      case HW_INFO_ITARGET_MAX_TIME1:
        _ReportOutf("HWInfo[%.2d] = %dms\t(ITargetMaxTime1)\n", i, aInfo[i]);
        break;
      case HW_INFO_ITARGET_MAX_TIME2:
        _ReportOutf("HWInfo[%.2d] = %dms\t(ITargetMaxTime2)\n", i, aInfo[i]);
        break;
      default:
        _ReportOutf("HWInfo[%.2d] = 0x%.8X\n", i, aInfo[i]);
      }
    }
  }
}

/*********************************************************************
*
*       _ShowMemZones
*
*  Function description
*    Some devices support different memory zones. For such devices, we output the information about the memory zones, if available
*
*  Notes
*    (1) May only be called after a target connection has been established successfully
*/
static int _ShowMemZones(void) {
  JLINK_MEM_ZONE_INFO aZoneInfo[16];
  int r;
  int i;

  r = JLINK_GetMemZones(&aZoneInfo[0], COUNTOF(aZoneInfo));
  if (r > 0) {
    _ReportOutf("Memory zones:\n");
    for (i = 0; i < r; i++) {
      _ReportOutf("  %s  %s\n", aZoneInfo[i].sName, aZoneInfo[i].sDesc);
    }
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _TestNet
*
*  Function description
*    Used to test host communication, especially when bringing up new hardware.
*    Basically, the data received is echoed.
*    For each packet sent by host, one response is expected.
*    The size of the packets varies, as well as the delay used by the target.
*
*  Communication format
*    -> U8  0xEF             // Cmd EMU_CMD_TEST_NET (Already swallowed by caller)
*    -> U8  SubCmd           // b[3..0]: Specifies the delay in repsonse per byte. Delay: 2 ^ (n-1) us. 0: 0, 1: 1, 2: 2, 3: 4, 4: 8, ... 15:16384
*                            // b[7..4]: Type of transfer. 0: Use overlap
*    -> U16 NumBytes
*    -> U8  Data[NumBytes]
*    <- U8  Data[NumBytes]
*    <- U8  Status           // 0
*/
static int _TestNet(int SizeInc, int DelayInc, int DelayMax) {
  U32 AllocSize;
  U8* pWrite;
  U8* pRead;
  U32 NumBytes;
  U32 NumBytesRead;
  char Response;
  int Delay;
  int i;
  int r = -1;
  //
  // Allocate memory
  //
  AllocSize = (MAX_NUM_BYTES_TEST_NET * 2) + 16;
  pWrite = malloc(AllocSize);
  if (pWrite == NULL) {
    _ReportOutf("ERROR: Could not alloc memory.\n");
    return -1;        // Error
  }
  pRead = pWrite + (AllocSize / 2);
  //
  // Fill memory with test pattern
  //
  for (i = 0; i < MAX_NUM_BYTES_TEST_NET; i++) {
    *(pWrite + i + 4) = i % 255;
  }
  //
  // Perform test in a loop
  //
  for (Delay = 0; Delay <= DelayMax; Delay += DelayInc) {
    for (NumBytes = 1; NumBytes <= MAX_NUM_BYTES_TEST_NET; NumBytes += SizeInc) {
      if (Delay || (NumBytes > 1)) {
        _ReportOutf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
      }
      _ReportOutf("  %4d bytes with delay %4d", NumBytes, Delay);
      *(pWrite + 0) = EMU_CMD_TEST_NET;
      *(pWrite + 1) = Delay;
      *(pWrite + 2) = (NumBytes >> 0) & 0xFF;
      *(pWrite + 3) = (NumBytes >> 8) & 0xFF;
      JLINKARM_Lock();
      NumBytesRead = JLINKARM_CommunicateEx(pWrite, (NumBytes + 4), pRead, (NumBytes + 1), 1);
      JLINKARM_Unlock();
      if (NumBytesRead != (NumBytes + 1)) {
        _ReportOutf("ERROR: Communication error (Expected %d bytes, received %d)!\n", (NumBytes + 1), NumBytesRead);
        r = JLINK_ERROR_UNKNOWN;
        goto Done;
      }
      Response = *(pRead + NumBytes);
      if (Response != 0) {
        if (Response == 6) {
          _ReportOutf("ERROR: J-Link is out of memory!\n");
        } else {
          _ReportOutf("ERROR: Failed to transfer data!\n");
        }
        r = JLINK_ERROR_UNKNOWN;
        goto Done;
      }
      if (memcmp(pWrite + 4, pRead, NumBytes) != 0) {
        _ReportOutf("ERROR: Verification of data failed!\n");
        r = JLINK_ERROR_UNKNOWN;
        goto Done;
      }
      if (_kbhit() != 0) {
        _getch();
        _ReportOutf("\nTest canceled by user!\n");
        r = JLINK_ERROR_CANCELED_BY_USER;
        goto Done;    // Canceled by user
      }
    }
  }
  _ReportOutf("\nTest completed successfully!\n");
  r = 0;              // O.K.
  //
  // Free the allocated memory
  //
Done:
  if (pWrite) {
    free(pWrite);
  }
  return r;
}

/*********************************************************************
*
*       _ExecTestNet
*/
static int _ExecTestNet(const char* s) {
  int r = 0;

  if (JLINKARM_EMU_HasCapEx(EMU_CAP_EX_TEST_NET) == 0) {
    _ReportOutf("Connected J-Link does not support EMU_CMD_TEST_NET.\n");
    return JLINK_NOERROR;
  }
  if (r == 0) {
    _ReportOutf("Performing quick test...(press any key to cancel)\n");
    r = _TestNet(47, 5, 5);
    r = _TestNet(255, 500, 1000);
  }
  if (r == 0) {
    _ReportOutf("Performing intensive test...(press any key to cancel)\n");
    r = _TestNet(1, 2, 10);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTestNRWSpeed
*
*  Function description
*    Tests the network speed for read or write
*/
static int _ExecTestNRWSpeed(const char* s, int IsWrite) {
  U32 NumReps;
  U32 NumRepsRem;
  U32 NumBytes;
  U32 NumRepsAtOnce;
  U32 NumRepsAtOnceMax;
  float Speed;
  int t;
  int r;

  NumReps  = 0;
  NumBytes = 0x1000;
  _ReportOutf("Syntax: TestN%sspeed [<NumBytes> [<NumReps>]]\n", IsWrite ? "W" : "R");
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (!*s) {
    _ReportOutf("Using defaults\n");
  } else {
    if (_ParseDec(&s, &NumBytes)) {
      return JLINK_ERROR_SYNTAX;
    }
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (*s) {
    if (_ParseDec(&s, &NumReps)) {
      return JLINK_ERROR_SYNTAX;
    }
  }
  //
  // Check parameters
  //
  if (NumBytes > 0x2000) {
    _ReportOutf("\nNumBytes may not exceed 8192.\n");
    return JLINK_NOERROR;
  }

  //
  // Compute NumReps in case it has not been specified
  //
  if (NumReps == 0) {
    if (NumBytes >= 256) {
      NumReps = (2048 * 1024) / NumBytes;
    } else {
      NumReps = 8 * 1024;
    }
  }
  NumRepsAtOnceMax = (512 * 1024) / NumBytes;     // make sure we do not transfer more than 512KB at once
  _ReportOutf("\nTransfering "),
  _PrintNumBytes(NumReps * NumBytes);
  _ReportOutf(" (%d * ", NumReps);
  _PrintNumBytes(NumBytes);
  _ReportOutf(")\n");
  //
  // Transfer data
  //
  NumRepsRem = NumReps;
  t = _GetTickCount();
  do {
    NumRepsAtOnce = MIN(NumRepsAtOnceMax, NumReps);
    if (IsWrite) {
      r = JLINKARM_EMU_TestNWSpeed(NumRepsAtOnce, NumBytes);
    } else {
      r = JLINKARM_EMU_TestNRSpeed(NumRepsAtOnce, NumBytes);
    }
    _ReportOutf(".");
    NumRepsRem -= NumRepsAtOnce;
  } while (NumRepsRem);
  t = _GetTickCount() - t;
  if (t) {
    Speed = (NumBytes * NumReps) / (float)t;
    _ReportOutf("\n%.1f KBytes/sec", Speed);
    Speed = (1000 * NumReps) / (float)t;
    _ReportOutf("\n%d Blocks/sec\n", (int)Speed);
  } else {
    _ReportOutf("WARNING: Measured time is 0ms\n");
  }

  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTestNRSpeed
*/
static int _ExecTestNRSpeed(const char* s) {
  return _ExecTestNRWSpeed(s, 0);
}

/*********************************************************************
*
*       _ExecTestNWSpeed
*/
static int _ExecTestNWSpeed(const char* s) {
  return _ExecTestNRWSpeed(s, 1);
}


/*********************************************************************
*
*       _ExecTestWSpeed
*/
static int _ExecTestWSpeed(const char* s) {
  int i;
  int t;
  int r;
  int BlockSize;
  U16 Speed;
  void * p;
  void * pRef;
  int NumReps;
  U32 Addr = _RAMAddr;
  //
  // Determine block size
  //
  Speed   = JLINKARM_GetSpeed();
  NumReps = 8;
  if (Speed < 1000) {
    BlockSize = 0x2000;
  } else {
    BlockSize = 0x20000;
  }
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseHex(&s, &Addr)) {
      return JLINK_ERROR_SYNTAX;
    }
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (*s) {
    if (_ParseHex(&s, &BlockSize)) {
      return JLINK_ERROR_SYNTAX;
    }
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (*s) {
    if (_ParseDec(&s, &NumReps)) {
      return JLINK_ERROR_SYNTAX;
    }
  }  
  //
  // Alloc memory
  //
  _ReportOutf("Speed test: Writing %d * %dkb into memory @ address 0x%.8X ", NumReps, BlockSize >> 10, Addr);
  p = malloc(BlockSize);
  pRef = malloc(BlockSize);
  if ((p == NULL) || (pRef == NULL)) {
    _ReportOutf("Could not alloc memory\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Fill memory with pattern
  //
  for (i = 0; i < BlockSize; i++) {
    *((U8*)p + i) = i % 255;
  }
  //
  // Transfer data
  //
  if (JLINKARM_IsHalted() == 0) {
//    JLINKARM_Halt();
  }
  t = _GetTickCount();
  for (i =0; i < NumReps; i++) {
    r = JLINKARM_WriteMemEx(Addr, BlockSize, p, 0);
    if (r != BlockSize) {
      _ReportOutf("\nCould not write memory.\n");
      return JLINK_ERROR_UNKNOWN;
    }
    _ReportOutf(".");
  }
  t = _GetTickCount() - t;
  if (t < 1) {
    t = 1;
  }
  //
  // Perform check after write
  //
  JLINKARM_ReadMemEx(Addr, BlockSize, pRef, 0);
  r = _memcmp(p, pRef, BlockSize);
  if (r < 0) {
    _ReportOutf("\n%d kByte written in %dms ! (%.1f KByte/sec)\n", BlockSize >> 10, t / NumReps, (float)NumReps * BlockSize / t);
  } else {
    _ReportOutf("\nWrite test failed. %d bytes written successfully.\n", r);
  }
  free(pRef);
  free(p);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTestRSpeed
*/
static int _ExecTestRSpeed(const char* s) {
  int i;
  int t;
  int r;
  U32 BlockSize;
  U16 Speed;
  void * p;
  int NumReps;
  U32 Addr = _RAMAddr;
  //
  // Determine block size
  //
  Speed   = JLINKARM_GetSpeed();
  NumReps = 8;
  if (Speed < 1000) {
    BlockSize = 0x2000;
  } else {
    BlockSize = 0x20000;
  }
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseHex(&s, &Addr)) {
      return JLINK_ERROR_SYNTAX;
    }
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (*s) {
    if (_ParseHex(&s, &BlockSize)) {
      return JLINK_ERROR_SYNTAX;
    }
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (*s) {
    if (_ParseDec(&s, &NumReps)) {
      return JLINK_ERROR_SYNTAX;
    }
  }  
  //
  // Test speed
  //
  _ReportOutf("Speed test: Reading %d * %dkb from address 0x%.8X ", NumReps, BlockSize >> 10, Addr);
  p = malloc(BlockSize);
  if (p == NULL) {
    _ReportOutf("Could not alloc memory\n");
    return JLINK_ERROR_UNKNOWN;
  }
  t = _GetTickCount();
  for (i =0; i < NumReps; i++) {
    r = JLINKARM_ReadMemEx(Addr, BlockSize, p, 0);
    if (r != (int)BlockSize) {
      _ReportOutf("\nCould not read memory.\n");
      return JLINK_ERROR_UNKNOWN;
    }
    _ReportOutf(".");
  }
  t = _GetTickCount() - t;
  if (t < 1) {
    t = 1;
  }
  _ReportOutf("\n%d kByte read in %dms ! (%.1f KByte/sec)\n", BlockSize >> 10, t / NumReps, (float)NumReps * BlockSize / t);
  free(p);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTestCSpeed
*/
static int _ExecTestCSpeed(const char* s) {
  int Freq;
  U32 RAMAddr  = _RAMAddr;
  U32 RepCount = 1;
  U32 Data1, Data2, Data3;

  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseHex(&s, &RAMAddr)) {
      return JLINK_ERROR_SYNTAX;
    }
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  _EatWhite(&s);
  if (*s) {
    if (_ParseHex(&s, &RepCount)) {
      return JLINK_ERROR_SYNTAX;
    }
  }
  //
  // Determine CPU clock frequency
  //
  _ReportOutf("Testing CPU clock frequency @ address 0x%.8X...\n", RAMAddr);
  JLINKARM_ReadMemHW (RAMAddr, 4, &Data1);
  Data3 = Data1;
  Data2 = (Data1 + 0x00010001) & 0x000F000F;
  JLINKARM_WriteMemHW(RAMAddr, 4, &Data2);
  JLINKARM_ReadMemHW (RAMAddr, 4, &Data1);
  JLINKARM_WriteMemHW(RAMAddr, 4, &Data3);
  if (Data1 != Data2) { 
    _ReportOutf("No RAM available @ address 0x%.1X. Could not determine CPU clock\n", RAMAddr);
    return JLINK_ERROR_UNKNOWN;
  }
  while (RepCount--) {
    Freq = JLINKARM_MeasureCPUSpeed(RAMAddr, 1);
    if (Freq <= 0) {
      _ReportOutf("Could not determine CPU clock\n");
      break;
    } else {
      _ReportOutf("CPU running at %d kHz\n", (U32)Freq / 1000);
    }
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecExportDeviceList
*/
static int _ExecExportDeviceList(const char* s) {
  char ac[MAX_PATH];
  char acOut[512];
  HANDLE hFile;
  int NumDevices;
  int NumBytesWritten;
  int Len;
  int i;
  int j;
  JLINKARM_DEVICE_INFO DeviceInfo;
  //
  // Parse parameters
  //
  hFile = INVALID_HANDLE_VALUE;
  ac[0] = 0;
  _ParseString(&s, ac, sizeof(ac));
  if (ac[0] == 0) {
    _ReportOutf("Syntax: exportdevicelist <filename>\n");
    return JLINK_ERROR_SYNTAX;
  }
  //
  // Open file for writing
  //
  _ReportOutf("Opening text file for writing... [%s]\n", ac);
  hFile = _OpenFile(ac, FILE_FLAG_WRITE | FILE_FLAG_CREATE | FILE_FLAG_TRUNC);
  if (hFile == INVALID_HANDLE_VALUE) {
    _ReportOutf("Could not open file for writing.\n");
    return JLINK_ERROR_SYNTAX;
  }
  //
  // Output table header
  //
  Len = sprintf(ac, "\"Manufacturer\", \"Device\", \"Core\", {Flash areas}, {RAM areas}\r\n");
  NumBytesWritten = _WriteFile(hFile, ac, Len);
  if (NumBytesWritten != Len) {
    _ReportOutf("Could not write to file.\n");
    goto Done;
  }
  //
  // Export device list from DLL
  //
  NumDevices = JLINKARM_DEVICE_GetInfo(-1, NULL);
  i = 0;
  do {
    DeviceInfo.SizeOfStruct = sizeof(JLINKARM_DEVICE_INFO);
    JLINKARM_DEVICE_GetInfo(i, &DeviceInfo);
    if (DeviceInfo.sName == NULL) {   // Hidden devices are never output by the DLL. Hidden devices are always at the end of the list
      break;
    }
    //
    // Output manufacturer & device name
    //
    JLINKARM_Core2CoreName(DeviceInfo.Core, acOut, sizeof(acOut));
    sprintf(ac, "\"%s\", \"%s\", \"%s\", ", DeviceInfo.sManu, DeviceInfo.sName, acOut);
    Len = strlen(ac);
    _WriteFile(hFile, ac, Len);
    //
    // Output flash region info
    //
    if (DeviceInfo.aFlashArea[1].Size) {  // More than one region?
      _WriteFile(hFile, "{ ", 2);
    }
    j = 0;
    do {
      Len = sprintf(ac, "{0x%.8X, 0x%.8X}", DeviceInfo.aFlashArea[j].Addr, DeviceInfo.aFlashArea[j].Size);
      j++;
      _WriteFile(hFile, ac, Len);
      if (DeviceInfo.aFlashArea[j].Size == 0) {   // Reached end of list?
        if (j > 1) {
          _WriteFile(hFile, " }", 2);
        }
        break;
      }
      _WriteFile(hFile, ", ", 2);
    } while(1);
    _WriteFile(hFile, ", ", 2);
    //
    // Output RAM region info
    //
    if (DeviceInfo.aRAMArea[1].Size) {  // More than one region?
      _WriteFile(hFile, "{ ", 2);
    }
    j = 0;
    do {
      Len = sprintf(ac, "{0x%.8X, 0x%.8X}", DeviceInfo.aRAMArea[j].Addr, DeviceInfo.aRAMArea[j].Size);
      j++;
      _WriteFile(hFile, ac, Len);
      if (DeviceInfo.aRAMArea[j].Size == 0) {   // Reached end of list?
        if (j > 1) {
          _WriteFile(hFile, " }", 1);
        }
        break;
      }
      _WriteFile(hFile, ", ", 2);
    } while(1);
    _WriteFile(hFile, "\r\n", 2);
  } while(++i < NumDevices);
Done:
  if (hFile != INVALID_HANDLE_VALUE) {
    _CloseFile(hFile);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecLoadBin
*/
static int _ExecLoadBin(const char* s) {
  char ac[MAX_PATH];
  U32 Addr;
  int r;
  //
  // Parse parameters
  //
  Addr = 0;
  ac[0] = 0;
  _ParseString(&s, ac, sizeof(ac));
  if (_ParseHex(&s, &Addr) || (ac[0] == 0)) {
    _ReportOutf("Syntax: loadbin <filename>, <addr>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _OverrideFlashProgProgressbarIfNoGui();
  //
  // Open file for reading
  //
  if (JLINKARM_IsHalted() == 0) {
    _ReportOutf("Halting CPU for downloading file.\n");
    JLINKARM_Halt();
  }
  _ReportOutf("Downloading file [%s]...", ac);
  r = JLINK_DownloadFile(ac, Addr);
  if (r < 0) {
    switch (r) {
    case JLINK_ERR_FLASH_PROG_COMPARE_FAILED:  _ReportOutf("Error while programming flash: Comparing flash contens failed.\n"); break;
    case JLINK_ERR_FLASH_PROG_PROGRAM_FAILED:  _ReportOutf("Error while programming flash: Programming failed.\n");             break;
    case JLINK_ERR_FLASH_PROG_VERIFY_FAILED:   _ReportOutf("Error while programming flash: Verify failed.\n");                  break;
    case JLINK_ERR_OPEN_FILE_FAILED:           _ReportOutf("Failed to open file.\n");                                           break;
    case JLINK_ERR_UNKNOWN_FILE_FORMAT:        _ReportOutf("File is of unknown / supported format.\n");                         break;
    case JLINK_ERR_WRITE_TARGET_MEMORY_FAILED: _ReportOutf("Writing target memory failed.\n");                                  break;
    default:                                   _ReportOutf("Unspecified error %d\n", r);                                        break;
    }
    return JLINK_ERROR_UNKNOWN;
  } else {
    _ReportOutf("O.K.\n");
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecLoadFile
*/
static int _ExecLoadFile(const char* s) {
  char ac[MAX_PATH];
  U32 Addr;
  int r;
  //
  // Parse parameters
  //
  Addr = 0;
  ac[0] = 0;
  _ParseString(&s, ac, sizeof(ac));
  if (ac[0] == 0) {
    _ReportOutf("Syntax: loadfile <filename> [<addr>]\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
    _EatWhite(&s);
  }
  if (*s) {
    if (_ParseHex(&s, &Addr)) {
      _ReportOutf("Syntax: loadfile <filename> [<addr>]\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  _OverrideFlashProgProgressbarIfNoGui();
  //
  // Open file for reading
  //
  if (JLINKARM_IsHalted() == 0) {
    JLINKARM_Halt();
  }
  _ReportOutf("Downloading file [%s]...\n", ac);
  r = JLINK_DownloadFile(ac, Addr);
  if (r < 0) {
    switch (r) {
    case JLINK_ERR_FLASH_PROG_COMPARE_FAILED:  _ReportOutf("Error while programming flash: Comparing flash contens failed.\n"); break;
    case JLINK_ERR_FLASH_PROG_PROGRAM_FAILED:  _ReportOutf("Error while programming flash: Programming failed.\n");             break;
    case JLINK_ERR_FLASH_PROG_VERIFY_FAILED:   _ReportOutf("Error while programming flash: Verify failed.\n");                  break;
    case JLINK_ERR_OPEN_FILE_FAILED:           _ReportOutf("Failed to open file.\n");                                           break;
    case JLINK_ERR_UNKNOWN_FILE_FORMAT:        _ReportOutf("File is of unknown / supported format.\n");                         break;
    case JLINK_ERR_WRITE_TARGET_MEMORY_FAILED: _ReportOutf("Writing target memory failed.\n");                                  break;
    default:                                   _ReportOutf("Unspecified error %d\n", r);                                        break;
    }
    return JLINK_ERROR_UNKNOWN;
  } else {
    _ReportOutf("O.K.\n");
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecVerifyBin
*/
static int _ExecVerifyBin(const char* s) {
  HANDLE hFile;
  U32 Addr;
  U32 FileSize;
  U8* pBinData;
  U8* pTargetData;
  char ac[MAX_PATH];
  char acErr[512];
  int r;
  int NumBytesRead;
  //
  // Parse parameters: <filename>, <addr>0
  //
  ac[0] = 0;
  acErr[0] = 0;
  _ParseString(&s, ac, sizeof(ac));
  if (_ParseHex(&s, &Addr) || (ac[0] == 0)) {
    _ReportOutf("Syntax: verifybin <filename>, <addr>\n");
    return JLINK_ERROR_SYNTAX;
  }
  //
  // Open file for reading
  //
  _ReportOutf("Loading binary file %s\n", ac);
  hFile = _OpenFile(ac, FILE_FLAG_READ | FILE_FLAG_SHARE_READ);
  if (hFile != INVALID_HANDLE_VALUE) {
    FileSize = _GetFileSize(hFile);
    pBinData  = (char*) malloc(FileSize);
    if (pBinData) {
      NumBytesRead = _ReadFile(hFile, pBinData, FileSize);
      if (NumBytesRead >= 0) {
        if (NumBytesRead == (int)FileSize) {
          pTargetData = (char*) malloc(FileSize);
          _ReportOutf("Reading %i byte%s data from target memory @ 0x%.8X.\n", NumBytesRead,(NumBytesRead >= 1) ? "s" : "", Addr);
          if (JLINKARM_IsHalted() == 0) {
            JLINKARM_Halt();
          }
          JLINKARM_ReadMem(Addr, FileSize, pTargetData);
          //
          //  Compare bin file and target data
          // 
          r = _memcmp(pTargetData, pBinData, FileSize);
          if (r >= 0) {
            _ReportOutf("Verify failed @ address 0x%.8X.\nExpected %.2X read %.2X", Addr + r, *(pBinData + r), *(pTargetData + r));
          } else {                                 // Compared data are identical       
            _ReportOutf("Verify successful.\n");
          }
        } else {
          strcpy(acErr, "ERROR: Could not read file.\n");
        }
      } else {
        strcpy(acErr, "ERROR: Could not read file.\n");
      }
      free(pBinData);
    } else {
      strcpy(acErr, "ERROR: Could not malloc file buffer.\n");
    }
    _CloseFile(hFile);
  } else {
    strcpy(acErr, "ERROR: Could not open file.\n");
  }
  if (acErr[0]) {
    _ReportOutf(acErr);
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSaveBin
*
*  Function description
*    Saves target memory into binary file
*/
static int _ExecSaveBin(const char* s) {
  char ac[MAX_PATH];
  HANDLE hFile;
  int NumBytesWritten;
  U32 Addr;
  char* pBuffer;
  U32 NumBytes;
  int r;
  //
  // Parse parameters
  //
  _ParseString(&s, ac, sizeof(ac));
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: savebin <filename>, <addr>, <NumBytes> (hex)\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &NumBytes)) {
    _ReportOutf("Syntax: savebin <filename>, <addr>, <NumBytes> (hex)\n");
    return JLINK_ERROR_SYNTAX;
  }
  //
  // Open file for writing
  //
  _ReportOutf("Opening binary file for writing... [%s]\n", ac);
  hFile = _OpenFile(ac, FILE_FLAG_WRITE | FILE_FLAG_CREATE | FILE_FLAG_TRUNC);
  if (hFile == INVALID_HANDLE_VALUE) {
    _ReportOutf("Could not open file for writing.\n");
    return JLINK_ERROR_SYNTAX;
  }
  pBuffer  = (char*) malloc(NumBytes);
  if(pBuffer == NULL) {
    _ReportOutf("Could not allocate memory for reading %d bytes at addr 0x%.8X.\n", NumBytes, Addr);
    r = JLINK_ERROR_UNKNOWN;
    goto Cleanup;
  }
  _ReportOutf("Reading %d bytes from addr 0x%.8X into file...", NumBytes, Addr);
  r = JLINKARM_ReadMemEx(Addr, NumBytes, pBuffer, 0);
  if (r < 0) {
    _ReportOutf("Could not read memory.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Cleanup;
  }
  if ((U32)r != NumBytes) {
    _ReportOutf("Not all bytes could be read from target. 0x%.8X bytes successfully read.\n", r);
  }
  NumBytesWritten = _WriteFile(hFile, pBuffer, r);
  if (NumBytesWritten != r) {
    _ReportOutf("Could not write to file.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Cleanup;
  }
  _ReportOutf("O.K.\n");
  r = JLINK_NOERROR;
Cleanup:
  _CloseFile(hFile);
  if(pBuffer) {
    free(pBuffer);
  }
  return r;
}

/*********************************************************************
*
*       _ExecWriteRaw
*/
static int _ExecWriteRaw(const char* s) {
  int NumBits;
  U8 abTDI[1024];
  U8 abTMS[1024];
  U8 abTDO[1024];
  const char * sErr;
  int i;

  if (_ParseDec(&s, &NumBits)) {
    _ReportOutf("Expected number of bits\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  sErr = _ParseHexString(&s, abTMS, NumBits);
  if (sErr) {
    _ReportOutf(sErr);
    _ReportOutf("\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  sErr = _ParseHexString(&s, abTDI, NumBits);
  if (sErr) {
    _ReportOutf(sErr);
    _ReportOutf("\n");
    return JLINK_ERROR_SYNTAX;
  }

  JLINKARM_JTAG_StoreGetRaw(abTDI, abTDO, abTMS, NumBits);
  _ReportOutf(" TDO: ");
  for (i = 0; i < NumBits; i += 8) {
    int Off = (NumBits - 1 - i) / 8;
    _ReportOutf("%.1X", abTDO[Off]);
  }
  _ReportOutf("\n");
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _CalcParity
*
*  Function description
*    Computes the (even) parity of the 32-bit value passed as parameter
*
*  Return value
*    0:   Number of 1s in Data is even
*    1:   Number of 1s in Data is odd
*
*  Examples
*    0 => 0
*    1 => 1
*    2 => 1
*    3 => 0
*    4 => 1
*    ...
*/
static int _CalcParity(U32 Data) {
  Data = (Data >> 16) ^ Data;           // Reduce 32 bits to 16 bits
  Data = (Data >>  8) ^ Data;           // Reduce 16 bits to 8 bits
  Data = (Data >>  4) ^ Data;           // Reduce 8 bits to 4 bits
  Data = (Data >>  2) ^ Data;           // Reduce 4 bits to 2 bits
  Data = (Data >>  1) ^ Data;           // Reduce 2 bits to 1 bit
  return Data & 1;
}

/*********************************************************************
*
*       _ClrDPError
*/
static void _ClrDPError(void) {
  U8 aIn[6]  = { 0x81, 0xC0, 0x03, 0x00, 0x00, 0x00 };
  U8 aDir[6] = { 0xFF, 0xE0, 0xFF, 0xFF, 0xFF, 0xFF };

/*
  aData[0]  = 1      << 0;   // START bit
  aData[0] |= 0      << 1;   // APnDP
  aData[0] |= 0      << 2;   // Write
  aData[0] |= 0      << 3;   // ADDR[2:3]
  aData[0] |= 0      << 5;   // PARITY
  aData[0] |= 0      << 6;   // STOP
  aData[0] |= 1      << 7;   // PARK

  v |= DP_ABORT_MASK_ORUNERRCLR;            // Clear STICKYORUN flag in Ctrl/Stat register
  v |= DP_ABORT_MASK_WDERRCLR;              // Clear WDATAERR   flag in Ctrl/Stat register
  v |= DP_ABORT_MASK_STKERRCLR;             // Clear STICKYERR  flag in Ctrl/Stat register
  v |= DP_ABORT_MASK_STKCMPCLR;             // Clear STICKYCMP  flag in Ctrl/Stat register

*/

  //
  // Clear DP error flags. Does the same as typing:
  //   SWDWriteDP 0,1E
  //
  JLINKARM_SWD_StoreRaw(&aDir[0], &aIn[0], 48);
  JLINKARM_SWD_SyncBits();
}

/*********************************************************************
*
*       _TIF_SWD_ReadU32
*/
static int _TIF_SWD_ReadU32(U32 Index, U32* pData) {
  int BitPos;
  int Status;
  U32 Data;
  U8 aIn[6];
  U8 aDir[6];
  U8 Parity;
  U8 APnDP;

  APnDP   = (U8)(Index >> 2) & 1;
  Index  &= 3;
  //
  // Calc parity of command
  //
  Parity  = 1;                  // Read
  Parity ^= APnDP;              // APnDP
  Parity ^= (Index >> 0) & 1;   // Addr[0]
  Parity ^= (Index >> 1) & 1;   // Addr[1]
  //
  // Send Command
  //
  aIn[0]  = 1      << 0;   // START bit
  aIn[0] |= APnDP  << 1;   // APnDP
  aIn[0] |= 1      << 2;   // Read
  aIn[0] |= Index  << 3;   // ADDR[2:3]
  aIn[0] |= Parity << 5;   // PARITY
  aIn[0] |= 0      << 6;   // STOP
  aIn[0] |= 1      << 7;   // PARK
  aDir[0] = 0xFF;          // Output
  JLINKARM_SWD_StoreRaw(&aDir[0], &aIn[0], 8);
  //
  // Receive ACK[0:2]
  //
  aIn [0]  = 0;
  aDir[0]  = 0;
  BitPos = JLINKARM_SWD_StoreRaw(&aDir[0], &aIn[0], 3);
  //
  // Read data
  //
  memset(aIn,  0, 5);
  memset(aDir, 0, 4);      // Read 32 bits
  aDir[4] = 0xFC;          // Read 1 bit, 7 clocks
  JLINKARM_SWD_StoreRaw(&aDir[0], &aIn[0], 40);
  Status = JLINKARM_SWD_GetU8(BitPos) & 7;
  Data   = JLINKARM_SWD_GetU32(BitPos + 3);
  //
  // Check result
  //
  if (Status == ACK_OK) {
    Parity  = JLINKARM_SWD_GetU8(BitPos + 35);
    Parity &= 1;
    *pData = Data;
    if (_CalcParity(Data) != Parity) {
      _ReportOutf("ERROR: Wrong parity (Data = 0x%.8X, ReceivedParity = %d)\n", Data, Parity);
      return -1;      // Error;
    }
  }
  return Status;      // O.K.
}

/*********************************************************************
*
*       _ReadUntilOK
*/
static int _ReadUntilOK(U32 Index, U32* pData) {
  int TryCnt = 0;
  int Status;
  //
  // Continue reading until we get "OK", fault or timeout
  //
  while (1) {
    Status = _TIF_SWD_ReadU32(Index, pData);
    if (Status == ACK_OK) {
      break;
    }
    if (Status < 0) {
      return -1;      // Error (Parity)
    }
    _ClrDPError();
    if (Status != ACK_WAIT) {
      _ReportOutf("ERROR: Read from DP/AP register failed!\n");
      return -1;      // Error (Fault)
    }
    if (TryCnt++ == 10) {
      _ReportOutf("ERROR: Timeout while reading from DP/AP register!\n");
      return -1;      // Error (Timeout)
    }
  }
  return 0;           // O.K.
}

/*********************************************************************
*
*       _TIF_SWD_WriteU32
*/
static int _TIF_SWD_WriteU32(U32 Index, U32 Data) {
  int BitPos;
  int Status;
  U8 aIn[6];
  U8 aDir[6];
  U8 Parity;
  U8 APnDP;

  APnDP   = (U8)(Index >> 2) & 1;
  Index  &= 3;
  //
  // Calc parity of command
  //
  Parity  = 0;                  // Write
  Parity ^= APnDP;              // APnDP
  Parity ^= (Index >> 0) & 1;   // Addr[0]
  Parity ^= (Index >> 1) & 1;   // Addr[1]
  //
  // Send Command
  //
  aIn[0]  = 1      << 0;    // START bit
  aIn[0] |= APnDP  << 1;    // APnDP
  aIn[0] |= 0      << 2;    // Write
  aIn[0] |= Index  << 3;    // ADDR[2:3]
  aIn[0] |= Parity << 5;    // PARITY
  aIn[0] |= 0      << 6;    // STOP
  aIn[0] |= 1      << 7;    // PARK
  aDir[0] = 0xFF;           // Output
  JLINKARM_SWD_StoreRaw(&aDir[0], &aIn[0], 8);
  //
  // Receive ACK[0:2]
  //
  aIn [0]  = 0;
  aDir[0]  = 0;
  BitPos = JLINKARM_SWD_StoreRaw(&aDir[0], &aIn[0], 3);
  //
  // TURNAROUND
  //
  aIn [0]  = 0;
  aDir[0]  = 0;
  JLINKARM_SWD_StoreRaw(&aDir[0], &aIn[0], 2);
  //
  // Write data
  //
  Parity = _CalcParity(Data);
  *(U32*)aIn  = Data;
  aIn[4]      = Parity;
  *(U32*)aDir = 0xFFFFFFFF;
  aDir[4]     = 0xFF;
  JLINKARM_SWD_StoreRaw(&aDir[0], &aIn[0], 40);
  //
  // Check result
  //
  Status = JLINKARM_SWD_GetU8(BitPos) & 7;
  return Status;      // O.K.
}

/*********************************************************************
*
*       _JLINK_CORESIGHT_TriggerReadAPDPReg
*
*  Function description
*    Only triggers an AP/DP read by sending the read request and waiting until it has been accepted.
*    But it does not make sure that the actual read data is returned. Depending on the interface,
*    an additional read request may be required to get the actual read data from the first one.
*    This function does *not* perform this additional read request internally.
*    For a function that performs this additional read request internally and always returns the actual read data
*    use JLINKARM_CORESIGHT_ReadAPDPReg()
*
*    JTAG:
*      DP read accesses return the read data from the previous read request. The actual read data is returned on the next read request.
*      AP read accesses return the read data from the previous read request. The actual read data is returned on the next read request.
*
*    SWD:
*      DP read accesses immediately return the read data
*      AP read accesses return the read data from the previous read request. The actual read data is returned on the next read request.
*
*  Return value
*    >= 0  O.K.
*     < 0  Error
*/
static int _JLINK_CORESIGHT_TriggerReadAPDPReg(U8 RegIndex, U8 APnDP, U32* pData) {
  JLINK_FUNC_CORESIGHT_TRIGGER_READ_APDP_REG* pf;
  int r;

  if (pData) {     // Make sure that pData is set to some default value, even in case old DLL is used that does not support this function
    *pData = 0xFFFFFFFF;
  }
  r = JLINK_ERR_EMU_FEATURE_NOT_SUPPORTED;
  pf = (JLINK_FUNC_CORESIGHT_TRIGGER_READ_APDP_REG*)JLINK_GetpFunc(JLINK_IFUNC_CORESIGHT_TRIGGER_READ_APDP_REG);
  if (pf) {
    r = pf(RegIndex, APnDP, pData);
  }
  return r;
}

/*********************************************************************
*
*       _TIF_SWD_Select
*/
static void _TIF_SWD_Select(void) {
  U32 Id;
  U32 CtrlStat;
  U32 v;

  JLINKARM_TIF_Select(JLINKARM_TIF_SWD);
  JLINKARM_CORESIGHT_Configure("");      // Output JTAG -> SWD switching sequence
  _JLINK_CORESIGHT_TriggerReadAPDPReg(DP_REG_IDCODE, 0, &Id);
  if ((Id & 0x0F000FFF) == 0x0B000477) {
    _ReportOutf("Found SWD-DP with ID 0x%.8X\n", Id);
  }
  //
  // Clear error flags if any are set
  //
  _JLINK_CORESIGHT_TriggerReadAPDPReg(DP_REG_CTRL_STAT, 0, &CtrlStat);
  v = 0
    | (1 << 1) // STICKYORUN
    | (1 << 4) // STICKYCMP
    | (1 << 5) // STICKYERR
    | (1 << 7) // WDATAERR
    ;
  if (CtrlStat & v) {
    JLINKARM_CORESIGHT_WriteAPDPReg(DP_REG_ABORT, 0, 0x1E);  // Clear DP errors
  }
  CtrlStat = 0
           | DP_CTRLSTAT_MASK_CSYSPWRUPREQ      // System power up
           | DP_CTRLSTAT_MASK_CDBGPWRUPREQ      // Debug power up
           ;
  JLINKARM_CORESIGHT_WriteAPDPReg(DP_REG_CTRL_STAT, 0, CtrlStat);
}

/*********************************************************************
*
*       _ExecSWDReadAP
*/
static int _ExecSWDReadAP(const char* s) {
  U32 Index;
  U32 Data;
  int r;
  //
  // Parse parameters
  //
  if (_ParseHex(&s, &Index)) {
    _ReportOutf("Syntax: SWDReadAP <RegIndex>\n"
           "Value of the AP register is returned on the NEXT transfer.\n"
           "In order to read an AP register, the following sequence is recommended:\n"
           "SWDReadAP <RegIndex>\n"
           "SWDReadDP 3\n");
    return JLINK_ERROR_SYNTAX;
  }
  r = _JLINK_CORESIGHT_TriggerReadAPDPReg((U8)Index, 1, &Data);
  if (r >= 0) {
    _ReportOutf("Read AP register %d = 0x%.8X\n", Index, Data);
  } else {
    _ReportOutf("Read AP register %d = ERROR\n", Index);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSWDReadDP
*/
static int _ExecSWDReadDP(const char* s) {
  U32 Index;
  U32 Data;
  int r;
  //
  // Parse parameters
  //
  if (_ParseHex(&s, &Index)) {
    _ReportOutf("Syntax: SWDReadDP <RegIndex>\n");
    return JLINK_ERROR_SYNTAX;
  }
  r = _JLINK_CORESIGHT_TriggerReadAPDPReg((U8)Index, 0, &Data);
  if (r >= 0) {
    _ReportOutf("Read DP register %d = 0x%.8X\n", Index, Data);
  } else {
    _ReportOutf("Read DP register %d = ERROR\n", Index);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSWDWriteAP
*/
static int _ExecSWDWriteAP(const char* s) {
  U32 Index;
  U32 Data;
  int r;
  //
  // Parse parameters
  //
  if (_ParseHex(&s, &Index)) {
    _ReportOutf("Syntax: SWDWriteAP <RegIndex> <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &Data)) {
    _ReportOutf("Syntax: SWDWriteAP <RegIndex> <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  r = JLINKARM_CORESIGHT_WriteAPDPReg(Index, 1, Data);
  _ReportOutf("Write AP register %d = 0x%.8X%s\n", Index, Data, (r < 0) ? " ***ERROR" : "");
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSWDWriteDP
*/
static int _ExecSWDWriteDP(const char* s) {
  U32 Index;
  U32 Data;
  int r;
  //
  // Parse parameters
  //
  if (_ParseHex(&s, &Index)) {
    _ReportOutf("Syntax: SWDWriteDP <RegIndex> <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &Data)) {
    _ReportOutf("Syntax: SWDWriteDP <RegIndex> <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  r = JLINKARM_CORESIGHT_WriteAPDPReg(Index, 0, Data);
  _ReportOutf("Write DP register %d = 0x%.8X%s\n", Index, Data, (r < 0) ? " ***ERROR" : "");
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSWDSelect
*/
static int _ExecSWDSelect(const char* s) {
  U32 Id;
  U32 CtrlStat;
  U32 v;
  //
  // Perform operation
  //
  _ReportOutf("Select SWD by sending SWD switching sequence.\n");
  JLINKARM_TIF_Select(JLINKARM_TIF_SWD);
  JLINKARM_CORESIGHT_Configure("");      // Output JTAG -> SWD switching sequence
  _JLINK_CORESIGHT_TriggerReadAPDPReg(DP_REG_IDCODE, 0, &Id);
  if ((Id & 0x0F000FFF) == 0x0B000477) {
    _ReportOutf("Found SWD-DP with ID 0x%.8X\n", Id);
  }
  //
  // Clear error flags if any are set
  //
  _JLINK_CORESIGHT_TriggerReadAPDPReg(DP_REG_CTRL_STAT, 0, &CtrlStat);
  v = 0
    | (1 << 1) // STICKYORUN
    | (1 << 4) // STICKYCMP
    | (1 << 5) // STICKYERR
    | (1 << 7) // WDATAERR
    ;
  if (CtrlStat & v) {
    JLINKARM_CORESIGHT_WriteAPDPReg(DP_REG_ABORT, 0, 0x1E);  // Clear DP errors
  }
  CtrlStat = 0
           | DP_CTRLSTAT_MASK_CSYSPWRUPREQ      // System power up
           | DP_CTRLSTAT_MASK_CDBGPWRUPREQ      // Debug power up
           ;
  JLINKARM_CORESIGHT_WriteAPDPReg(DP_REG_CTRL_STAT, 0, CtrlStat);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteAPDP
*/
static int _ExecWriteAPDP(const char* s, U8 APnDP) {
  U32 Index;
  U32 Data;
  int r;
  //
  // Parse parameters
  //
  if (_ParseHex(&s, &Index)) {
    _ReportOutf("Syntax: Write%s <RegIndex> <Data>\n", APnDP ? "AP" : "DP");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &Data)) {
    _ReportOutf("Syntax: Write%s <RegIndex> <Data>\n", APnDP ? "AP" : "DP");
    return JLINK_ERROR_SYNTAX;
  }
  //
  // Perform operation
  //
  r = JLINKARM_CORESIGHT_WriteAPDPReg(Index, APnDP, Data);
  if (r >= 0) {
    _ReportOutf("Writing %s register %d = 0x%.8X (%d write repetitions needed)\n", APnDP ? "AP" : "DP", Index, Data, r);
  } else {
    _ReportOutf("Writing failed.\n");
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteAP
*/
static int _ExecWriteAP(const char* s) {
  return _ExecWriteAPDP(s, 1);
}

/*********************************************************************
*
*       _ExecWriteDP
*/
static int _ExecWriteDP(const char* s) {
  return _ExecWriteAPDP(s, 0);
}

/*********************************************************************
*
*       _ExecReadAPDP
*/
static int _ExecReadAPDP(const char* s, U8 APnDP) {
  U32 Index;
  U32 Data;
  int r;
  //
  // Parse parameters
  //
  if (_ParseHex(&s, &Index)) {
    _ReportOutf("Syntax: Read%s <RegIndex>\n", APnDP ? "AP" : "DP");
    return JLINK_ERROR_SYNTAX;
  }
  //
  // Perform operation
  //
  r = JLINKARM_CORESIGHT_ReadAPDPReg(Index, APnDP, &Data);
  if (r >= 0) {
    _ReportOutf("Reading %s register %d = 0x%.8X (%d read repetitions needed)\n", APnDP ? "AP" : "DP", Index, Data, r);
  } else {
    _ReportOutf("Reading failed.\n");
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecReadAP
*/
static int _ExecReadAP(const char* s) {
  return _ExecReadAPDP(s, 1);
}

/*********************************************************************
*
*       _ExecReadDP
*/
static int _ExecReadDP(const char* s) {
  return _ExecReadAPDP(s, 0);
}

/*********************************************************************
*
*       _CompareCmd
*
*  Return value
*    0    Equal
*    1    Not equal
*/
static char _CompareCmd(const char ** ps, const char * sCmd) {
  const char *s;
  s = *ps;
  do {
    char c;
    char c1;

    c  = toupper(*sCmd++);
    c1 = toupper(*s);
    if (c == 0) {
      if ((c1 == 0x20) || (c1 == 0x3D) || (c1 == 0x2C) || (c1 == 0)) {
        *ps = s;
        return 0;       // Command found
      }
      return 1;         // Command not found
    }
    if (c != c1) {
      return 1;         // Command not found
    }
    s++;
  } while (1);
}

/*********************************************************************
*
*       _GetManufacturer
*/
static const char * _GetManufacturer(U32 DeviceID) {
  int i;
  U16 ManufacturerID;
    
  ManufacturerID = (DeviceID >> 1) & 0x7FF;
  for (i = 0; i < COUNTOF(_aManufacturer); i++) {
    if (_aManufacturer[i].ManufacturerID == ManufacturerID) {
      return _aManufacturer[i].sName;
    }
  }
  return NULL;
}

/*********************************************************************
*
*       _ShowCoreID
*/
static char _ShowCoreID(COMMANDER_SETTINGS* pCommanderSettings) {
  char acTmp[256];
  JLINKARM_JTAG_DEVICE_INFO DeviceInfo;
  JTAG_ID_DATA IdData = {0};
  const char * sManu;
  U32 Core = 0;
  U32 DevFamily;
  U32 Id;
  U32 InterfaceMask;
  U32 SelectedTIF;
  int r;
  //
  // If the emulator supports multiple interfaces, check which ones are supported,
  // since there are some J-Links out there which do not support JTAG (because they have been designed for some other interfaces only)
  // For J-Links which do not support the select interface command, we can assume JTAG is available.
  //
  SelectedTIF = JLINKARM_TIF_JTAG;
  if (JLINKARM_GetEmuCaps() & JLINKARM_EMU_CAP_SELECT_IF) {
    JLINKARM_TIF_GetAvailable(&InterfaceMask);
  } else {
    InterfaceMask = (1 << JLINKARM_TIF_JTAG);
  }
  //
  // If no interface has been explicitly set, leave auto-connect as it is to avoid problems when starting J-Link Commander in parallel to IAR
  //
  if (pCommanderSettings->TargetIFSet) {                                   // Specific interface pre-selected?
    if ((InterfaceMask & (1 << pCommanderSettings->TargetIF)) == 0) {      // Selected interface not supported?
      _ReportOutf("Selected interface (%d) is not supported by connected debug probe.\n", pCommanderSettings->TargetIF);
      return 1;
    }
    JLINKARM_TIF_Select(pCommanderSettings->TargetIF);
    SelectedTIF = pCommanderSettings->TargetIF;
    //
    // If interface has been selected explicitly , do NOT perform auto connect sequence of Commander
    //
    r = JLINKARM_Connect();   // Establish target connection
    JLINKARM_GetIdData(&IdData);
    if ((IdData.NumDevices == 0) || r) {
      _ReportOutf("Can not connect to target.\n");
      return 1;
    }
    if (r == 0) {
      _ShowMemZones();
    }
  } else {
    if ((InterfaceMask & (1 << JLINKARM_TIF_JTAG)) || (InterfaceMask & (1 << JLINKARM_TIF_SWD))) {
      SelectedTIF = JLINKARM_TIF_JTAG;
      r = JLINKARM_Connect();       // Establish target connection
      JLINKARM_GetIdData(&IdData);
      if ((IdData.NumDevices == 0) || r) {
        if((InterfaceMask & (1 << JLINKARM_TIF_SWD))) {    // Check if SWD is supported
          _ReportOutf("No devices found on JTAG chain. Trying to find device on SWD.\n");
          JLINKARM_ClrError();
          JLINKARM_TIF_Select(JLINKARM_TIF_SWD);
          SelectedTIF = JLINKARM_TIF_SWD;
          r = JLINKARM_Connect();   // Establish target connection. JLINKARM_TIF_Select will reset the "connected to target / target identified" status of the DLL.
          Core = JLINKARM_CORE_GetFound();
          if ((Core == 0) || r) {
            _ReportOutf("No device found on SWD.\n");
            if (InterfaceMask & (1 << JLINKARM_TIF_JTAG)) {  // Do not try to select JTAG if we have a SWD-only J-Link
              JLINKARM_TIF_Select(JLINKARM_TIF_JTAG);
            }
          }
        } else {
          _ReportOutf("No devices found on JTAG chain. SWD is not supported by this emulator.\n");
        }
      }
      if (r == 0) {
        _ShowMemZones();
      }
    }
    if (InterfaceMask & (1 << JLINKARM_TIF_FINE)) {
      if ((IdData.NumDevices == 0) && Core == 0) {   // If no device has been found so far, try to find device on UART interface
        _ReportOutf("Trying to find device on FINE interface.\n");
        JLINKARM_TIF_Select(JLINKARM_TIF_FINE);
        SelectedTIF = JLINKARM_TIF_FINE;
        r = JLINKARM_Connect();   // Establish target connection
        JLINKARM_GetIdData(&IdData);
        if ((IdData.aId[0] == 0) || r) {
          _ReportOutf("No device found on FINE interface.\n");
        }
        if (r == 0) {
          _ShowMemZones();
        }
      }
    }
  }
  if ((SelectedTIF == JLINKARM_TIF_JTAG) || (SelectedTIF == JLINKARM_TIF_FINE)) {   // Only available for JTAG & SWD
    if (IdData.NumDevices > 0) {
      int i;
      _ReportOutf("Found %d JTAG device%s, Total IRLen = %d:\n", IdData.NumDevices, (IdData.NumDevices > 1) ? "s" : "", IdData.ScanLen);
      for (i = 0; i < IdData.NumDevices; i++) {
        Id = JLINKARM_JTAG_GetDeviceId(i);
        if (Id) {
          sManu = _GetManufacturer(Id);
          r = JLINKARM_JTAG_GetDeviceInfo(i, &DeviceInfo);
          if (r == 0) {
            _ReportOutf(" #%d Id: 0x%.8X, IRLen: %.2d, IRPrint: 0x%X, %s", i, Id, DeviceInfo.IRLen, DeviceInfo.IRPrint, DeviceInfo.sName);
          } else if (r > 0) {
            _ReportOutf(" #%d Id: 0x%.8X, IRLen: %.2d, %s", i, Id, DeviceInfo.IRLen, DeviceInfo.sName);
          } else {
            _ReportOutf(" #%d Id: 0x%.8X", i, Id);
          }
          if (sManu) {
            _ReportOutf(" (%s)\n", sManu);
          } else {
            _ReportOutf("\n");
          }
        }
      }
    }
  }
  if (JLINKARM_HasError() == 0) {
    Core = JLINKARM_CORE_GetFound();
    JLINKARM_Core2CoreName(Core, acTmp, sizeof(acTmp));
    _ReportOutf("%s identified.\n", acTmp);
    DevFamily = (Core >> 24) & 0xFF;
    if ((DevFamily == JLINKARM_DEV_FAMILY_ARM7) || (DevFamily == JLINKARM_DEV_FAMILY_ARM9)) {
      if (JLINKARM_ETM_IsPresent()) {
        _ShowETMConfig();
        _ShowETBConfig();
      }
    }
    return 0;   // O.K.
  }
  return 1;       // Error
}

/*********************************************************************
*
*       _HasEmuEthernet
*
*  Return value
*     0     Emulator has no ethernet interface
*     1     Emulator has ethernet interface
*/
static int _HasEmuEthernet(void) {
  char acFW[256];
  int ProdId;
  int HWVersion;
  int HWVersionMajor;
  int r;

  if (JLINKARM_EMU_HasCapEx(JLINKARM_EMU_CAP_EX_ETHERNET)) {
    return 1;
  }
  r = 0;
  ProdId         = JLINKARM_EMU_GetProductId();
  HWVersion      = JLINKARM_GetHardwareVersion();
  HWVersionMajor = HWVersion / 10000 % 100;

  if (ProdId < 0) {  // On error
    return r;
  }
  switch(ProdId) {
  case JLINK_EMU_PRODUCT_ID_FLASHER_ARM:
    if (HWVersionMajor >= 3) {
      r = 1;
    }
    break;
  case JLINK_EMU_PRODUCT_ID_JLINK_PRO:
  case JLINK_EMU_PRODUCT_ID_FLASHER_PPC:
  case JLINK_EMU_PRODUCT_ID_FLASHER_RX:
    r = 1;
    break;
  default:
    JLINKARM_GetFirmwareString(acFW,256);
    if (memcmp(acFW, "Ember ", 6) == 0) {  // Ember hardware features TCP/IP functionality but does not have a J-Link compliant product ID
      r = 1;
    }
  }
  return r;
}

/*********************************************************************
*
*       _GetEmuIPAddr
*
*  Return value
*    -1     Error
*     0     O.K.: IP addr. is fixed
*     1     O.K.: IP addr. is assigned by DHCP but not yet ready
*     2     O.K.: IP addr. is assigned by DHCP
*/
static int _GetEmuIPAddr(U32* pIPAddr) {
  int r;
  U32 IPByte[4];
  
  if (_HasEmuEthernet()) {
    r = JLINKARM_ReadEmuConfigMem((unsigned char*)pIPAddr, 0x20, 4);
    if (r != 0) {    // Testing successfully reading has to be done only once
      if (r == 2) {
        _ReportOutf("This feature is not supported by connected J-Link.\n");
      } else {
        _ReportOutf("Failed to read configuration.\n");
      }
      return -1;  // Error
    } else {
      //
      // Read rest of configuration
      //
      if (*pIPAddr != 0xFFFFFFFF) {  // DHCPc is not active, we have set IP addr. manually
        return 0;  // IP addr. fixed
      } else {
        JLINKARM_GetHWInfo((1 << HW_INFO_IP_ADDR), pIPAddr);
        if (*pIPAddr == 0) {
          return 1;  // IP addr. is assigned by DHCP but not yet ready
        } else {
          IPByte[0]   =  *pIPAddr             >> 24;
          IPByte[1]   = (*pIPAddr & 0xFF0000) >> 16;
          IPByte[2]   = (*pIPAddr & 0x00FF00) >> 8;
          IPByte[3]   =  *pIPAddr & 0x0000FF;
          *pIPAddr    = (IPByte[0] << 24) | (IPByte[1] << 16) | (IPByte[2] <<  8) | (IPByte[3]);
          return 2;  // IP addr. has been assigned by DHCP
        }
      }
    }
  } else {
    _ReportOutf("This hardware does not support ethernet.\n");
    return -1;  // Error
  }
}

/*********************************************************************
*
*       _DisconnectFromTarget
*
*  Function description
*    Closes connection to target without closing connection to J-Link.
*
*  Return value
*    >= 0  O.K.
*     < 0  Error
*/
static int _DisconnectFromTarget(COMMANDER_SETTINGS* pCommanderSettings) {
  _ReportOutf("Disconnecting from J-Link...");
  //
  // TBD: Create extra API function that only disconnects from the target
  //
  pCommanderSettings->Status.ConnectedToTarget = 0;
  _ReportOutf("O.K.\n");
  return 0;
}

/*********************************************************************
*
*       _ConnectToTarget
*
*  Function description
*    Establish connection to target device by specifying device, target interface etc.
*
*  Return value
*    >= 0  O.K.
*     < 0  Error
*/
static int _ConnectToTarget(COMMANDER_SETTINGS* pCommanderSettings) {
  char ac[1024];
  char acDevName[256];
  char c0;
  char* ps;
  const U32* paSuppCores;
  int NumEntries;
  int i;
  int r;
  int Found;
  U32 SelectedCore;
  JLINK_FUNC_MRU_GETLIST* pfGetMRU;
  JLINKARM_DEVICE_SELECT_INFO DeviceSelectInfo;
  JLINKARM_DEVICE_INFO DevInfo;

  pCommanderSettings = &_CommanderSettings;
  //
  // Ask for device / core name, if not already specified
  //
  if (pCommanderSettings->acDeviceName[0] == 0) {
    //
    // If the DLL provides the possibility to get the MRU device names, we show the latest one
    //
    acDevName[0] = 0;
    pfGetMRU = (JLINK_FUNC_MRU_GETLIST*)JLINK_GetpFunc(JLINK_IFUNC_MRU_GETLIST);
    if (pfGetMRU) {
      r = pfGetMRU(JLINK_MRU_GROUP_DEVICE, ac, sizeof(ac));
      if (r >= 0) {
        strcpy(acDevName, ac);
      }
    }
    //
    // No MRU device present, assume "Unspecified" as device
    //
    if (acDevName[0] == 0) {
      strcpy(acDevName, "Unspecified");
    }
    //
    // Get user input, if any
    //
    _ReportOutf("Please specify device / core. <Default>: %s\n", acDevName);
    _ReportOut("Type '?' for selection dialog\n");
    ps = _ConsoleGetString("Device>", ac, sizeof(ac));
    if (*ps) {
      _ParseString(&ps, acDevName, sizeof(acDevName));
      if (acDevName[0] == '?') {
        DeviceSelectInfo.SizeOfStruct = sizeof(JLINKARM_DEVICE_SELECT_INFO);
        r = JLINKARM_DEVICE_SelectDialog(NULL, 0, &DeviceSelectInfo);
        if (r >= 0) {
          memset(&DevInfo, 0, sizeof(DevInfo));
          DevInfo.SizeOfStruct = sizeof(JLINKARM_DEVICE_INFO);
          JLINKARM_DEVICE_GetInfo(r, &DevInfo);
          strcpy(acDevName, DevInfo.sName);
        }
      }
    }
    //
    // Remember selection in commander settings
    //
    strcpy(pCommanderSettings->acDeviceName, acDevName);
  }
  //
  // Ask for target interface to use, if not already specified
  //
  if (pCommanderSettings->TargetIFSet == 0) {
    _ReportOut("Please specify target interface:\n");
    //
    // Show TIF list, prefiltered by selected core so we do not show interfaces
    // that are not supported by the currently selected device
    // If we cannot retrieve any information about the currently selected device, show all TIFs
    //
    r = JLINKARM_DEVICE_GetIndex(pCommanderSettings->acDeviceName);
    SelectedCore = JLINK_CORE_ANY;
    if (r >= 0) {
      memset(&DevInfo, 0, sizeof(DevInfo));
      DevInfo.SizeOfStruct = sizeof(JLINKARM_DEVICE_INFO);
      JLINKARM_DEVICE_GetInfo(r, &DevInfo);
      SelectedCore = DevInfo.Core;
    }
    //
    // Go through TIF list
    //
    NumEntries = COUNTOF(_aTIFList);
    r = 0;
    for (i = 0; i < NumEntries; i++) {
      Found = 0;
      //
      // Go through core filter for each TIF
      // 0-entry marks end of core filter list
      //
      paSuppCores = _aTIFList[i].paSupportedCores;
      if (paSuppCores) {
        do {
          if (*paSuppCores == 0) {
            break;
          }
          if (*paSuppCores == SelectedCore) {
            Found = 1;
            break;
          }
          paSuppCores++;
        } while (1);
      } else {
        Found = 1;
      }
      if (Found) {
        //
        // Select first supoported interface as default
        //
        if (r == 0) {
          pCommanderSettings->TargetIF = _aTIFList[i].TIFVal;
          pCommanderSettings->TargetIFSet = 1;
        }
        _ReportOutf("  %c) %s%s\n", _aTIFList[i].Shortcut, _aTIFList[i].sName, (r == 0) ? " (Default)" : "");
        r++;
      }
    }
    //
    // Evaluate selection
    //
    do {
      ps = _ConsoleGetString("TIF>", ac, sizeof(ac));
      if (*ps) {
        Found = 0;
        for (i = 0; i < NumEntries; i++) {
          c0 = toupper(ac[0]);
          if (c0 == _aTIFList[i].Shortcut) {
            Found = 1;
            break;
          }
        }
        if (Found) {
          pCommanderSettings->TargetIF = _aTIFList[i].TIFVal;
          pCommanderSettings->TargetIFSet = 1;
          break;
        }
      } else {
        //
        // Use default
        //
        break;
      }
    } while (1);
  }
  //
  // Special for JTAG: Ask for position of device in JTAG chain, if not already specified by user
  //
  if (pCommanderSettings->TargetIF == JLINKARM_TIF_JTAG) {
    if (pCommanderSettings->JTAGConfSet == 0) {
      //
      // Default: Take first supported core in JTAG chain
      //
      pCommanderSettings->JTAGConfIRPre = -1;
      pCommanderSettings->JTAGConfDRPre = -1;
      _ReportOut("Device position in JTAG chain (IRPre,DRPre) <Default>: -1,-1 => Auto-detect\n");
      ps = _ConsoleGetString("JTAGConf>", ac, sizeof(ac));
      if (*ps) {
        if (_ParseDec(&ps, &pCommanderSettings->JTAGConfIRPre)) {
          _ReportOutf("ERROR while parsing value for IRPre. Using default: %d.\n", pCommanderSettings->JTAGConfIRPre);
        }
        if (*ps == ',') {     // Skip comma between JTAG config params
          ps++;
        }
        if (_ParseDec(&ps, &pCommanderSettings->JTAGConfDRPre)) {
          _ReportOutf("ERROR while parsing value for DRPre. Using default: %d.\n", pCommanderSettings->JTAGConfDRPre);
        }
      }
      pCommanderSettings->JTAGConfSet = 1;
    }
    JLINKARM_ConfigJTAG(pCommanderSettings->JTAGConfIRPre, pCommanderSettings->JTAGConfDRPre);
  }
  //
  // Ask for interface speed, if not already specified
  //
  if (pCommanderSettings->InitTIFSpeedkHzSet == 0) {
    _ReportOutf("Specify target interface speed [kHz]. <Default>: %d kHz\n", DEFAULT_TIF_SPEED_KHZ);
    pCommanderSettings->InitTIFSpeedkHz = DEFAULT_TIF_SPEED_KHZ;
    pCommanderSettings->InitTIFSpeedkHzSet = 1;
    ps = _ConsoleGetString("Speed>", ac, sizeof(ac));
    if (ac[0]) {
      _EatWhite(&ps);
      //
      // Special speeds: "auto" and "adaptive"
      //
      if (_JLinkstricmp(ps, "adaptive") == 0) {
        pCommanderSettings->InitTIFSpeedkHz = JLINKARM_SPEED_ADAPTIVE;
      } else if (_JLinkstricmp(ps, "auto") == 0) {
        pCommanderSettings->InitTIFSpeedkHz = JLINKARM_SPEED_AUTO;
      } else {
        if (_ParseDec(&ps, &pCommanderSettings->InitTIFSpeedkHz)) {
          _ReportOutf("ERROR while parsing value for speed. Using default: %d kHz.\n", DEFAULT_TIF_SPEED_KHZ);
        }
      }
    }
  }
  //
  // Set settings file
  //
  if (pCommanderSettings->acSettingsFile[0]) {
    char acSettingsFile[_MAX_PATH];

    sprintf(acSettingsFile, "ProjectFile = %s", pCommanderSettings->acSettingsFile);
    JLINKARM_ExecCommand(acSettingsFile, NULL, 0);
  }
  //
  // Pass device selection to DLL
  //
  if (pCommanderSettings->acDeviceName[0]) {
    sprintf(ac, "device=%s", pCommanderSettings->acDeviceName);
    JLINKARM_ExecCommand(ac, NULL, 0);
  }
  //
  // Set J-Link script file
  //
  if (pCommanderSettings->acJLinkScriptFile[0]) {
    char acScriptFile[_MAX_PATH];

    strcpy(acScriptFile, "scriptfile = ");
    strcpy(acScriptFile + strlen(acScriptFile), pCommanderSettings->acJLinkScriptFile);
    ac[0] = 0;
    JLINKARM_ExecCommand(acScriptFile, &ac[0], sizeof(ac));
  }
  //
  // Connect to device
  //
  JLINKARM_SetSpeed(pCommanderSettings->InitTIFSpeedkHz);
  JLINKARM_EnableLog(_Log);
  _ReportOut("\n\n");
  _ShowCoreID(pCommanderSettings);
  pCommanderSettings->Status.ConnectedToTarget = 1;
  return 0;
}

/*********************************************************************
*
*       _DisconnectFromJLink
*
*  Function description
*    Closes connection to J-Link
*
*  Return value
*    >= 0  O.K.
*     < 0  Error
*/
static int _DisconnectFromJLink(COMMANDER_SETTINGS* pCommanderSettings) {
  _ReportOutf("Disconnecting from J-Link...");
  JLINKARM_Close();
  pCommanderSettings->Status.ConnectedToJLink = 0;
  _ReportOutf("O.K.\n");
  return 0;
}

/*********************************************************************
*
*       _ConnectToJLink
*
*  Function description
*    Establish connection to J-Link *without* connecting to the target device.
*
*  Return value
*    >= 0  O.K.
*     < 0  Error
*
*  Notes
*    (1) No connection to target is established
*/
static int _ConnectToJLink(COMMANDER_SETTINGS* pCommanderSettings) {
  JLINKARM_HW_STATUS Stat;
  const char *s;
  char ac[1024];
  U32 IPAddr;
  int v;
  int r;
  int VerHW;
  int t;
  //
  // Setup host interface (HIF)
  //
  _ReportOutf("Connecting to J-Link via %s...", (pCommanderSettings->HostIF == JLINKARM_HOSTIF_IP) ? "IP" : "USB");
  r = 0;
  if (pCommanderSettings->HostIF == JLINKARM_HOSTIF_IP) {
    r = JLINKARM_SelectIP(pCommanderSettings->acIPAddr, 0);
    r = (r != 0) ? -1 : 0;                                      // Convert error to standard commander internal error value (< 0)
  } else {
    //
    // Check if we need to connect to a specific J-Link (S/N specified or old USB0-3 enumeration method)
    // S/N 0-3 are interpreted as old USB0-3 connection method
    // In case nothing has been specified, we pass 0 to the DLL which means, we want to see a selection dialog in case multiple J-Links are connected
    //
    if (pCommanderSettings->EmuSN > 3) {
      r = JLINKARM_EMU_SelectByUSBSN(pCommanderSettings->EmuSN);
    } else {
      r = JLINKARM_SelectUSB(pCommanderSettings->EmuSN);
      r = (r != 0) ? -1 : 0;                                      // Convert error to standard commander internal error value (< 0)
    }
  }
  //
  // If DLL already reported an error during HIF setup, we are done
  //
  if (r < 0) {
    _ReportOut("FAILED\n");
    return -1;
  }
  //
  // Open actual connection to J-Link (not to target yet!)
  //
  JLINKARM_SetWarnOutHandler(_cbWarnOut);
  t = _GetTickCount();
  s = JLINKARM_OpenEx(_Log, _cbErrorOut);
  t = _GetTickCount() - t;
  if (s) {
    _ReportOutf("FAILED: %s\n", s);
    return -1;
  }
  _ReportOut("O.K.\n");
#if MEASURE_DLL_INIT_TIME
  _ReportOutf("DEBUG: Init DLL (%dms)\n", t);
#else
  (void)t;
#endif
  //
  // Show J-Link info (S/N, IP, OEM etc.)
  //
  JLINKARM_GetFirmwareString(ac, sizeof(ac));
  _ReportOutf("Firmware: %s\n", ac);
  VerHW = JLINKARM_GetHardwareVersion();
  _ReportOutf("Hardware version: V%d.%.2d\n", VerHW / 10000 % 100, VerHW / 100 % 100);
  _ReportOutf("S/N: %d\n", JLINKARM_GetSN());
  JLINKARM_GetFeatureString(&ac[0]);
  if (strlen(ac)) {
  	_ReportOutf("License(s): %s\n", ac);
  }
  JLINKARM_GetOEMString(&ac[0]);
  if (strlen(ac)) {
  	_ReportOutf("OEM: %s\n", ac);
  }
  if (_HasEmuEthernet()) {
    v = _GetEmuIPAddr(&IPAddr);
    switch(v) {
    case 0:  _ReportOutf("IP-Addr: %d.%d.%d.%d\n", (IPAddr >> 24) & 0xFF, (IPAddr >> 16) & 0xFF, (IPAddr >>  8) & 0xFF, (IPAddr >>  0) & 0xFF);            break;
    case 1:  _ReportOutf("IP-Addr: DHCP (no addr. received yet)\n");                                                                                       break;
    case 2:  _ReportOutf("IP-Addr: %d.%d.%d.%d (DHCP)\n", (IPAddr >> 24) & 0xFF, (IPAddr >> 16) & 0xFF, (IPAddr >>  8) & 0xFF, (IPAddr >>  0) & 0xFF);     break;
    default: _ReportOutf("IP-Addr: INVALID\n");                                                                                                            break;
    }
  }
  if (JLINKARM_GetEmuCaps() & JLINKARM_EMU_CAP_TRACE) {
   	_ReportOutf("Emulator has Trace capability\n");
  } else if (JLINKARM_GetEmuCaps() & JLINKARM_EMU_CAP_RAWTRACE) {    // Due to compatibility reasons, some probes need to report both caps, therefore else if ()
   	_ReportOutf("Emulator has RAWTRACE capability\n");
  }
  //
  // Show target voltage
  //
  JLINKARM_GetHWStatus(&Stat);
  _ReportOutf("VTref = %d.%.3dV\n", Stat.VTarget / 1000, Stat.VTarget % 1000);
  pCommanderSettings->Status.ConnectedToJLink = 1;
  return 0;
}

/*********************************************************************
*
*       _GetEmuIPMask
*
*  Return value
*    -1     Error
*     0     O.K.: IP mask is fixed
*     1     O.K.: IP mask is assigned by DHCP but not yet ready
*     2     O.K.: IP mask is assigned by DHCP
*/
static int _GetEmuIPMask(U32* IPMask) {
  int r;
  U32 v;
  
  if (_HasEmuEthernet()) {
    r = JLINKARM_ReadEmuConfigMem((unsigned char*)&v, 0x20, 4);
    if (r != 0) {    // Testing successfully reading has to be done only once
      if (r == 2) {
        _ReportOutf("This feature is not supported by connected J-Link.\n");
      } else {
        _ReportOutf("Failed to read configuration.\n");
      }
      return -1;  // Error
    } else {
      //
      // Read rest of configuration
      //
      if (v != 0xFFFFFFFF) {  // DHCPc is not active, we have set IP addr. manually
        JLINKARM_ReadEmuConfigMem((unsigned char*)IPMask, 0x24, 4);
        return 0;  // Addr. fixed
      } else {
        JLINKARM_GetHWInfo((1 << HW_INFO_IP_MASK), IPMask);
        if (*IPMask == 0) {
          return 1;  // IP mask is assigned by DHCP but not yet ready
        } else {
          return 2;  // IP mask has been assigned by DHCP
        }
      }
    }
  } else {
    _ReportOutf("This hardware does not support ethernet.\n");
    return -1;  // Error
  }
}

/*********************************************************************
*
*       _GetEmuIPGateway
*
*  Return value
*    -1     Error
*     0     O.K.: GW is fixed
*     1     O.K.: GW is assigned by DHCP but not yet ready
*     2     O.K.: GW is assigned by DHCP
*/
static int _GetEmuIPGateway(U32* Gateway) {
  int r;
  U32 v;
  
  if (_HasEmuEthernet()) {
    r = JLINKARM_ReadEmuConfigMem((unsigned char*)&v, 0x20, 4);
    if (r != 0) {    // Testing successfully reading has to be done only once
      if (r == 2) {
        _ReportOutf("This feature is not supported by connected J-Link.\n");
      } else {
        _ReportOutf("Failed to read configuration.\n");
      }
      return -1;  // Error
    } else {
      //
      // Read rest of configuration
      //
      if (v != 0xFFFFFFFF) {  // DHCPc is not active, we have set IP addr. manually
        JLINKARM_ReadEmuConfigMem((unsigned char*)&v, 0x40, 4);
        if (v == 0xFFFFFFFF) {
          *Gateway = 0;
        } else {
          *Gateway = v;
        }
        return 0;  // Addr. fixed
      } else {
        JLINKARM_GetHWInfo((1 << HW_INFO_IP_GW), Gateway);
        if (*Gateway == 0) {
          return 1;  // IP mask is assigned by DHCP but not yet ready
        } else {
          return 2;  // IP mask has been assigned by DHCP
        }
      }
    }
  } else {
    _ReportOutf("This hardware does not support ethernet.\n");
    return -1;  // Error
  }
}

/*********************************************************************
*
*       _GetEmuIPDNS0
*
*  Return value
*    -1     Error
*     0     O.K.: GW is fixed
*     1     O.K.: GW is assigned by DHCP but not yet ready
*     2     O.K.: GW is assigned by DHCP
*/
static int _GetEmuIPDNS0(U32* DNSServer) {
  int r;
  U32 v;
  
  if (_HasEmuEthernet()) {
    r = JLINKARM_ReadEmuConfigMem((unsigned char*)&v, 0x20, 4);
    if (r != 0) {    // Testing successfully reading has to be done only once
      if (r == 2) {
        _ReportOutf("This feature is not supported by connected J-Link.\n");
      } else {
        _ReportOutf("Failed to read configuration.\n");
      }
      return -1;  // Error
    } else {
      //
      // Read rest of configuration
      //
      if (v != 0xFFFFFFFF) {  // DHCPc is not active, we have set IP addr. manually
        JLINKARM_ReadEmuConfigMem((unsigned char*)&v, 0x48, 4);
        if (v == 0xFFFFFFFF) {
          *DNSServer = 0;
        } else {
          *DNSServer = v;
        }
        return 0;  // Addr. fixed
      } else {
        JLINKARM_GetHWInfo((1 << HW_INFO_IP_DNS0), DNSServer);
        if (*DNSServer == 0) {
          return 1;  // IP mask is assigned by DHCP but not yet ready
        } else {
          return 2;  // IP mask has been assigned by DHCP
        }
      }
    }
  } else {
    _ReportOutf("This hardware does not support ethernet.\n");
    return -1;  // Error
  }
}

/*********************************************************************
*
*       _GetModulePath
* Purpose:
*   gets the full path of the current application
*
* Parameters:
*   sBuffer  : Pointer to  output buffer
*   MaxLen   : size of the output buffer
* 
* Example:  
*   WIN32_GetModulePath(ac, sizeof(ac));
*   ---> ac[] = "C:\work\"

*/
static void _GetModulePath(char* sBuffer, int MaxLen) {
#ifdef WIN32
  char acPath [_MAX_PATH ];
  char acDrive[_MAX_DRIVE];
  char acDir  [_MAX_DIR  ];
  char acFName[_MAX_FNAME];
  char acExt  [_MAX_EXT  ];
  GetModuleFileName(NULL, acPath, sizeof(acPath));
  _splitpath(acPath, acDrive, acDir, acFName, acExt);
  _makepath(sBuffer, acDrive, acDir, 0, 0);
#else
  memset(sBuffer, 0, MaxLen);	// The directory where the executable is stored has read-only attributes for the normal user 
#endif
}

/*********************************************************************
*
*       _ExecSelectUSB
*/
static int _ExecSelectUSB(const char* s) {
  U32 Port = 0;
  int r;
  unsigned WasConnectedToJLink;
  unsigned WasConnectedToTarget;
  COMMANDER_SETTINGS* pCommanderSettings;

  pCommanderSettings = &_CommanderSettings;
  _EatWhite(&s);
  if (*s) {
    if (_ParseDec(&s, &Port)) {
      _ReportOutf("Syntax: usb <port>, where port is 0..3\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  //
  // Disconnect from target + J-Link
  // Update commander settings
  // Reconnect to J-Link + target
  //
  WasConnectedToJLink = pCommanderSettings->Status.ConnectedToJLink;
  WasConnectedToTarget = pCommanderSettings->Status.ConnectedToTarget;
  if (WasConnectedToTarget) {
    _DisconnectFromTarget(pCommanderSettings);
  }
  if (WasConnectedToJLink) {
    _DisconnectFromJLink(pCommanderSettings);
  }
  pCommanderSettings->HostIF      = JLINKARM_HOSTIF_USB;
  pCommanderSettings->acIPAddr[0] = 0;
  pCommanderSettings->EmuSN       = Port;
  r = _ConnectToJLink(pCommanderSettings);
  if (WasConnectedToTarget && (r >= 0)) {
    r = _ConnectToTarget(pCommanderSettings);
  }
  return (r < 0) ? JLINK_ERROR_UNKNOWN : JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSelectIP
*/
static int _ExecSelectIP(const char* s) {
  char acHost[256];
  int r;
  unsigned WasConnectedToJLink;
  unsigned WasConnectedToTarget;
  COMMANDER_SETTINGS* pCommanderSettings;

  pCommanderSettings = &_CommanderSettings;
  _ParseString(&s, acHost, sizeof(acHost));
  if (acHost[0]) {
    _ReportOutf("Connecting to %s\n", acHost);
  } else {
    _ReportOutf("Connecting via TCP/IP\n");
  }
  //
  // Disconnect from target + J-Link
  // Update commander settings
  // Reconnect to J-Link + target
  //
  WasConnectedToJLink = pCommanderSettings->Status.ConnectedToJLink;
  WasConnectedToTarget = pCommanderSettings->Status.ConnectedToTarget;
  if (WasConnectedToTarget) {
    _DisconnectFromTarget(pCommanderSettings);
  }
  if (WasConnectedToJLink) {
    _DisconnectFromJLink(pCommanderSettings);
  }
  pCommanderSettings->HostIF = JLINKARM_HOSTIF_IP;
  pCommanderSettings->EmuSN    = 0;
  strcpy(&_CommanderSettings.acIPAddr[0], acHost);
  r = _ConnectToJLink(pCommanderSettings);
  if (WasConnectedToTarget && (r >= 0)) {
    r = _ConnectToTarget(pCommanderSettings);
  }
  return (r < 0) ? JLINK_ERROR_UNKNOWN : JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSleep
*/
static int _ExecSleep(const char* s) {
  U32 ms;
  if (_ParseDec(&s, &ms)) {
    _ReportOutf("Syntax: Sleep <ms>\n");
  } else {
    _ReportOutf("Sleep(%d)\n", ms);
    _Sleep(ms);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecMACAddressSyntaxError
*/
static void _ExecMACAddressSyntaxError(void) {
  _ReportOutf("Syntax: macaddr = <MAC address>\n");
  _ReportOutf("Sample: macaddr = 00:0C:29:0A:48:1F\n");
}

/*********************************************************************
*
*       _ExecMACAddress
*/
static int _ExecMACAddress(const char* s) {
  U64 MACAddr;
  U32 MACByte[6];
  U16 USBConfig;
  int r;
  int i;

  _EatWhite(&s);
  if (*s) {
    if (*s == '=') {
      s++;
    }
    for (i = 0; i <= 5; i++) {
      if (_ParseHex(&s, &MACByte[i])) {
        _ExecMACAddressSyntaxError();
        return JLINK_ERROR_SYNTAX;
      }
      if (i < 5) {
        if (*s == ':') {
          s++;
        } else {
          _ExecMACAddressSyntaxError();
          return JLINK_ERROR_SYNTAX;
        }
      }
    }
    MACAddr = ((U64)MACByte[5]) << 40
            | ((U64)MACByte[4]  << 32)
            |  (MACByte[3]      << 24)
            |  (MACByte[2]      << 16)
            |  (MACByte[1]      <<  8)
            |   MACByte[0]
            ;
    r = JLINKARM_WriteEmuConfigMem((const unsigned char*)&MACAddr, 0x30, 6);
    if (r != 0) {
      if (r == 2) {
        _ReportOutf("MAC address is not supported by connected J-Link.\n");
      } else {
        _ReportOutf("Failed to write MAC address.\n");
        return JLINK_ERROR_UNKNOWN;
      }
    } else {
      _ReportOutf("MAC address successfully changed to '%.2X:%.2X:%.2X:%.2X:%.2X:%.2X'.\n", MACByte[0], MACByte[1], MACByte[2], MACByte[3], MACByte[4], MACByte[5]);
      _ReportOutf("Please unplug the device, then plug it back in.\n");
    }
  } else {
    r = JLINKARM_ReadEmuConfigMem((unsigned char*)&USBConfig, 0x00, 2);
    if (r != 0) {    // Testing successfully reading has to be done only once
      if (r == 2) {
        _ReportOutf("This feature is not supported by connected J-Link.\n");
      } else {
        _ReportOutf("Failed to read configuration.\n");
      }
    } else {
      //
      // Read rest of configuration
      //
      JLINKARM_ReadEmuConfigMem((unsigned char*)&MACAddr, 0x30, 6);
      _PrintMACAddr(MACAddr);
    }
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecIPAddressSyntaxError
*/
static void _ExecIPAddressSyntaxError(void) {
  _ReportOutf("Syntax: ipaddr = <ip> [<subnetmask>]\n");
  _ReportOutf("Sample: ipaddr = 192.168.0.10 255.255.255.0\n");
}

/*********************************************************************
*
*       _ExecIPAddress
*/
static int _ExecIPAddress(const char* s) {
  U64  ConfigData;
  U32  IPAddr;
  U32  IPByte[4];
  U32  IPMask;
  U32  MaskByte[4] = {0};
  int  r;
  int  i;

  _EatWhite(&s);
  if (*s) {
    if (*s == '=') {
      s++;
    }
    if (_CompareCmd(&s, "dhcp") == 0) {
      for (i = 0; i <= 3; i++) {
        IPByte[i] = 0xFF;
      }
    } else {
      for (i = 0; i <= 3; i++) {
        if (_ParseDec(&s, &IPByte[i])) {
          _ExecIPAddressSyntaxError();
          return JLINK_ERROR_SYNTAX;
        }
        if (i < 3) {
          if (*s == '.') {
            s++;
          } else {
            _ExecIPAddressSyntaxError();
            return JLINK_ERROR_SYNTAX;
          }
        }
      }
      _EatWhite(&s);
      if (*s) {
        for (i = 0; i <= 3; i++) {
          if (_ParseDec(&s, &MaskByte[i])) {
            _ExecIPAddressSyntaxError();
            return JLINK_ERROR_SYNTAX;
          }
          if (i < 3) {
            if (*s == '.') {
              s++;
            } else {
              _ExecIPAddressSyntaxError();
              return JLINK_ERROR_SYNTAX;
            }
          }
        }
      }
      for (i = 0; i <= 3; i++) {
        if ((IPByte[i] < 0) || (IPByte[i] > 255) || (MaskByte[i] < 0) || (MaskByte[i] > 255)) {
          _ReportOutf("Please input a valid IP address and subnetmask!\n");
          return JLINK_ERROR_SYNTAX;
        }
      }
    }
    IPAddr = (IPByte[0] << 24)
           | (IPByte[1] << 16)
           | (IPByte[2] <<  8)
           | (IPByte[3])
           ;
    IPMask = (MaskByte[0] << 24)
           | (MaskByte[1] << 16)
           | (MaskByte[2] <<  8)
           | (MaskByte[3])
           ;
    ConfigData = IPAddr;
    if (IPMask == 0) {
      IPMask = 0xFFFF0000;
      MaskByte[0] = 255;
      MaskByte[1] = 255;
    }
    ConfigData =  (U64)ConfigData
               | ((U64)IPMask << 32);
    r = JLINKARM_WriteEmuConfigMem((const unsigned char*)&ConfigData, 0x20, 8);
    if (r != 0) {
      if (r == 2) {
        _ReportOutf("IP address and subnetmask is not supported by connected J-Link.\n");
      } else {
        _ReportOutf("Failed to write IP address and subnetmask.\n");
        return JLINK_ERROR_UNKNOWN;
      }
    } else {
      if (IPAddr != 0xFFFFFFFF) {
        _ReportOutf("IP address successfully changed to '%d.%d.%d.%d'.\n", IPByte[0], IPByte[1], IPByte[2], IPByte[3]);
        if (IPMask != 0) {
          _ReportOutf("Subnetmask successfully changed to '%d.%d.%d.%d'.\n", MaskByte[0], MaskByte[1], MaskByte[2], MaskByte[3]);
        }
      } else {
        _ReportOutf("Configuration successfully changed to DHCP.\n");
      }
    }
  } else {
    r = _GetEmuIPAddr(&IPAddr);
    if (r == EMU_IP_ERROR) {  // IP addr could not be read
      return JLINK_NOERROR;
    } else {
      _GetEmuIPMask(&IPMask);
      IPByte[0]   =  IPAddr             >> 24;
      IPByte[1]   = (IPAddr & 0xFF0000) >> 16;
      IPByte[2]   = (IPAddr & 0x00FF00) >> 8;
      IPByte[3]   =  IPAddr & 0x0000FF;
      MaskByte[0] =  IPMask & 0x0000FF;
      MaskByte[1] = (IPMask & 0x00FF00) >> 8;
      MaskByte[2] = (IPMask & 0xFF0000) >> 16;
      MaskByte[3] =  IPMask             >> 24;
      if (r == EMU_IP_DHCP_NO_CONFIG) {
        _ReportOutf("DHCP active but no configuration received\n");
      } else {
        if (r == EMU_IP_FIXED) {
          _ReportOutf("User assigned network configuration\n");
        } else if (r == EMU_IP_DHCP_CONFIGURED) {  // DHCP is active and assigned
          _ReportOutf("DHCP assigned network configuration\n");
        }
        _ReportOutf("IP-Addr:    %d.%d.%d.%d\n"
               "Subnetmask: %d.%d.%d.%d\n", IPByte[0]  , IPByte[1]  , IPByte[2]  , IPByte[3],
                                            MaskByte[3], MaskByte[2], MaskByte[1], MaskByte[0]);
      }
    }
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecGWAddressSyntaxError
*/
static void _ExecGWAddressSyntaxError(void) {
  _ReportOutf("Syntax: gwaddr = <ip>\n");
  _ReportOutf("Sample: gwaddr = 192.168.0.1\n");
}

/*********************************************************************
*
*       _ExecGWAddress
*/
static int _ExecGWAddress(const char* s) {
  U32 IPAddr;
  U32 GWAddr;
  U32 IPByte[4];
  int r;
  int i;

  _EatWhite(&s);
  if (*s) {
    if (*s == '=') {
      s++;
    }
    for (i = 0; i <= 3; i++) {
      if (_ParseDec(&s, &IPByte[i])) {
        _ExecGWAddressSyntaxError();
        return JLINK_ERROR_SYNTAX;
      }
      if (i < 3) {
        if (*s == '.') {
          s++;
        } else {
          _ExecGWAddressSyntaxError();
          return JLINK_ERROR_SYNTAX;
        }
      }
    }
    for (i = 0; i <= 3; i++) {
      if ((IPByte[i] < 0) || (IPByte[i] > 255)) {
        _ReportOutf("Please input a valid gateway address!\n");
        return JLINK_ERROR_SYNTAX;
      }
    }
    GWAddr = (IPByte[0] << 24)
           | (IPByte[1] << 16)
           | (IPByte[2] <<  8)
           | (IPByte[3])
           ;
    r = JLINKARM_WriteEmuConfigMem((const unsigned char*)&GWAddr, 0x40, 4);
    if (r != 0) {
      if (r == 2) {
        _ReportOutf("Gateway address is not supported by connected J-Link.\n");
      } else {
        _ReportOutf("Failed to write gateway address.\n");
        return JLINK_ERROR_UNKNOWN;
      }
    } else {
      _ReportOutf("Gateway successfully changed to '%d.%d.%d.%d'.\n", IPByte[0], IPByte[1], IPByte[2], IPByte[3]);
    }
  } else {
    r = _GetEmuIPAddr(&IPAddr);
    if (r == EMU_IP_ERROR) {  // IP addr could not be read
      return JLINK_NOERROR;
    } else {
      _GetEmuIPGateway(&GWAddr);
      IPByte[0]   =  GWAddr             >> 24;
      IPByte[1]   = (GWAddr & 0xFF0000) >> 16;
      IPByte[2]   = (GWAddr & 0x00FF00) >> 8;
      IPByte[3]   =  GWAddr & 0x0000FF;
      if (r == EMU_IP_DHCP_NO_CONFIG) {
        _ReportOutf("DHCP active but no configuration received\n");
      } else {
        if (r == EMU_IP_FIXED) {
          _ReportOutf("User assigned network configuration\n");
        } else if (r == EMU_IP_DHCP_CONFIGURED) {
          _ReportOutf("DHCP assigned network configuration\n");
        }
        _ReportOutf("Gateway address: %d.%d.%d.%d\n", IPByte[0], IPByte[1], IPByte[2], IPByte[3]);
      }
    }
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecDNSAddressSyntaxError
*/
static void _ExecDNSAddressSyntaxError(void) {
  _ReportOutf("Syntax: dnsaddr = <ip>\n");
  _ReportOutf("Sample: dnsaddr = 192.168.0.1\n");
}

/*********************************************************************
*
*       _ExecDNSAddress
*/
static int _ExecDNSAddress(const char* s) {
  U32 IPAddr;
  U32 DNSAddr;
  U32 IPByte[4];
  int r;
  int i;

  _EatWhite(&s);
  if (*s) {
    if (*s == '=') {
      s++;
    }
    for (i = 0; i <= 3; i++) {
      if (_ParseDec(&s, &IPByte[i])) {
        _ExecDNSAddressSyntaxError();
        return JLINK_ERROR_SYNTAX;
      }
      if (i < 3) {
        if (*s == '.') {
          s++;
        } else {
          _ExecDNSAddressSyntaxError();
          return JLINK_ERROR_SYNTAX;
        }
      }
    }
    for (i = 0; i <= 3; i++) {
      if ((IPByte[i] < 0) || (IPByte[i] > 255)) {
        _ReportOutf("Please input a valid DNS server address!\n");
        return JLINK_ERROR_SYNTAX;
      }
    }
    DNSAddr = (IPByte[0] << 24)
            | (IPByte[1] << 16)
            | (IPByte[2] <<  8)
            | (IPByte[3])
            ;
    r = JLINKARM_WriteEmuConfigMem((const unsigned char*)&DNSAddr, 0x48, 4);
    if (r != 0) {
      if (r == 2) {
        _ReportOutf("DNS server address is not supported by connected J-Link.\n");
      } else {
        _ReportOutf("Failed to write DNS server address.\n");
        return JLINK_ERROR_UNKNOWN;
      }
    } else {
      _ReportOutf("DNS server successfully changed to '%d.%d.%d.%d'.\n", IPByte[0], IPByte[1], IPByte[2], IPByte[3]);
    }
  } else {
    r = _GetEmuIPAddr(&IPAddr);
    if (r == EMU_IP_ERROR) {  // IP addr could not be read
      return JLINK_NOERROR;
    } else {
      _GetEmuIPDNS0(&DNSAddr);
      IPByte[0]   =  DNSAddr             >> 24;
      IPByte[1]   = (DNSAddr & 0xFF0000) >> 16;
      IPByte[2]   = (DNSAddr & 0x00FF00) >> 8;
      IPByte[3]   =  DNSAddr & 0x0000FF;
      if (r == EMU_IP_DHCP_NO_CONFIG) {
        _ReportOutf("DHCP active but no configuration received\n");
      } else {
        if (r == EMU_IP_FIXED) {
          _ReportOutf("User assigned network configuration\n");
        } else if (r == EMU_IP_DHCP_CONFIGURED) {
          _ReportOutf("DHCP assigned network configuration\n");
        }
        _ReportOutf("DNS-Addr: %d.%d.%d.%d\n", IPByte[0], IPByte[1], IPByte[2], IPByte[3]);
      }
    }
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecShowConfig
*/
static int _ExecShowConfig(const char* s) {
  U16  USBConfig;
  U8   USBAddr;
  U8   EnumType;
  U32  KSPower;
  int  SN;
  U32  FakeSN;
  U32  IPAddr;
  U32  SubnetMask;
  U32  GWAddr;
  U32  DNSAddr;
  U64  MACAddr;
  char acOEM[256];    
  char acFW[256];
  int  r;
  int  v;
  U8   DHCPcActivated = 0;

  r = JLINKARM_ReadEmuConfigMem((unsigned char*)&USBConfig, 0x00, 2);
  if (r != 0) {    // Testing successfully reading has to be done only once
    if (r == 2) {
      _ReportOutf("This feature is not supported by connected J-Link.\n");
    } else {
      _ReportOutf("Failed to read configuration.\n");
      return JLINK_ERROR_UNKNOWN;
    }
  } else {
    //
    // Read rest of configuration
    //
    JLINKARM_ReadEmuConfigMem((unsigned char*)&KSPower,    0x04, 4);
    JLINKARM_ReadEmuConfigMem((unsigned char*)&FakeSN,     0x08, 4);
    JLINKARM_ReadEmuConfigMem((unsigned char*)&MACAddr,    0x30, 6);
    JLINKARM_GetFirmwareString(acFW,256);
    JLINKARM_GetOEMString(&acOEM[0]);
    SN = JLINKARM_GetSN();
    //
    // Show USB address
    //
    USBAddr = USBConfig & 0xFF;
    if (USBAddr == 0xFF) {
      _ReportOutf("USB-Address: Default\n");
    } else {
      _ReportOutf("USB-Address: %d\n", USBAddr);
    }
    //
    // Show enum. type
    //
    EnumType = USBConfig >> 8;
    if ((EnumType == 0) || (EnumType == 0xFF)) {
      _ReportOutf("Enum. type:  USB-Address is used\n");
    } else if (EnumType == 1) {
      _ReportOutf("Enum. type:  Real-SN is used\n");
    } else if (EnumType == 2) {
      _ReportOutf("Enum. type:  Fake-SN is used\n");
      _ReportOutf("Fake-SN:     %d", FakeSN);
    }
    //
    // Show KS-Power
    //
    if ((KSPower == -1) && (strcmp(&acOEM[0], "IARKS") == 0)) {
      _ReportOutf("KS-Power:    On (Default)\n");
    } else if (KSPower == -1) {
      _ReportOutf("KS-Power:    Off (Default)\n");
    } else if (KSPower == 0) {
      _ReportOutf("KS-Power:    Off\n");
    } else if (KSPower == 1) {
      _ReportOutf("KS-Power:    On\n");
    }
    //
    // Show ethernet configuration
    //
    if (_HasEmuEthernet()) {
      v = _GetEmuIPAddr(&IPAddr);
      _GetEmuIPMask(&SubnetMask);
      if (v == EMU_IP_DHCP_NO_CONFIG) {
        _ReportOutf("No DHCP configuration received.\n");
      } else {
        if (v == EMU_IP_FIXED) {
          _ReportOutf("User assigned network configuration\n");
        } else if (v == EMU_IP_DHCP_CONFIGURED) {
          _ReportOutf("DHCP assigned network configuration\n");
        }
        _ReportOutf("IP-Addr:        %d.%d.%d.%d\n",    IPAddr                  >> 24,
                                                  (IPAddr & 0xFF0000)      >> 16,
                                                  (IPAddr & 0x00FF00)      >> 8,
                                                   IPAddr & 0x0000FF);
      }
      if ((v == EMU_IP_FIXED) || (v == EMU_IP_DHCP_CONFIGURED)) {  // Either addresses are fixed or DHCPc has received configuration
        _ReportOutf("Subnetmask:     %d.%d.%d.%d\n",    SubnetMask              >> 24,
                                                  (SubnetMask & 0xFF0000)  >> 16,
                                                  (SubnetMask & 0x00FF00)  >> 8,
                                                   SubnetMask & 0x0000FF);
      }
      v = _GetEmuIPGateway(&GWAddr);
      if (((v == EMU_IP_FIXED) && (GWAddr != 0xFFFFFFFF))|| (v == EMU_IP_DHCP_CONFIGURED)) {  // Either addresses are fixed and have been set or DHCPc has received configuration
        _ReportOutf("Gateway-Addr:   %d.%d.%d.%d\n",    GWAddr                  >> 24,
                                                  (GWAddr & 0xFF0000)      >> 16,
                                                  (GWAddr & 0x00FF00)      >> 8,
                                                   GWAddr & 0x0000FF);
      }
      v = _GetEmuIPDNS0(&DNSAddr);
      if (((v == EMU_IP_FIXED) && (GWAddr != 0xFFFFFFFF))|| (v == EMU_IP_DHCP_CONFIGURED)) {  // Either addresses are fixed and have been set or DHCPc has received configuration
        _ReportOutf("DNS-Addr:       %d.%d.%d.%d\n",    DNSAddr                 >> 24,
                                                  (DNSAddr & 0xFF0000)     >> 16,
                                                  (DNSAddr & 0x00FF00)     >> 8,
                                                   DNSAddr & 0x0000FF);
      }
      _PrintMACAddr(MACAddr);
    }
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSetJTAGConfig
*/
static int _ExecSetJTAGConfig(const char* s) {
	U32 IRPre, DRPre;
	if (_ParseDec(&s, &IRPre)) {
    _ReportOutf("Syntax: Config <IRPre>, <DRPre>\n");
    return JLINK_ERROR_SYNTAX;
	} 
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseDec(&s, &DRPre)) {
    _ReportOutf("Syntax: Config <IRPre>, <DRPre>\n");
    return JLINK_ERROR_SYNTAX;
	}
  JLINKARM_ConfigJTAG(IRPre, DRPre);
  _ShowCoreID(&_CommanderSettings);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecMSDConfig
*/
static int _ExecMSDConfig(int Enable) {
  U8 ConfigByte;
  int r;

  r = JLINKARM_ReadEmuConfigMem(&ConfigByte, CONFIG_OFF_HW_FEATURES, 1);
  if (r) {
    _ReportOutf("Failed to read config area of connected probe.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  if (Enable) {
    ConfigByte |= (1uL << 1);
  } else {
    ConfigByte &= ~(1uL << 1);
  }
  r = JLINKARM_WriteEmuConfigMem(&ConfigByte, CONFIG_OFF_HW_FEATURES, 1);
  if (r) {
    _ReportOutf("Failed to write config area of connected probe.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  _ReportOutf("Probe configured successfully.\n");
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecMSDDisble
*/
static int _ExecMSDDisble(const char* s) {
  return _ExecMSDConfig(0);
}

/*********************************************************************
*
*       _ExecMSDEnable
*/
static int _ExecMSDEnable(const char* s) {
  return _ExecMSDConfig(1);
}

/*********************************************************************
*
*       _ExecVCOM
*/
static int _ExecVCOM(const char* s) {
  U8 NewConfigByte;
  U8 OldConfigByte;
  int r;
  //
  // Check if emulator supports CDC
  //
  if (JLINKARM_EMU_HasCapEx(JLINKARM_EMU_CAP_EX_CDC_EXEC) == 0) {
    _ReportOutf("The connected emulator does not support VCOM functionality.\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if(_CompareCmd(&s, "Enable") == 0) {
    NewConfigByte = 1;

  } else if(_CompareCmd(&s, "Disable") == 0) {
    NewConfigByte = 0;
  } else {
    _ReportOutf("Syntax: VCOM <State>\n");
    return JLINK_ERROR_SYNTAX;
  }
  r = JLINKARM_ReadEmuConfigMem(&OldConfigByte, VIRTUAL_COM_PORT_OFFSET, 1);
  if (r != 0) {
    OldConfigByte = -1;          // Force writing the config byte if we were not able to check if it changed.
  }
  //
  // Check if virtual com port config byte has changed
  //
  if (OldConfigByte ^ NewConfigByte) {
    r = JLINKARM_WriteEmuConfigMem(&NewConfigByte, VIRTUAL_COM_PORT_OFFSET, 1);
    if (r != 0) {
      if (r == 2) {
        _ReportOutf("Write config data is not supported by connected J-Link.\n");
      } else {
        _ReportOutf("Failed to write config data\n");
        return JLINK_ERROR_UNKNOWN;
      }
    }
    _ReportOutf("The new configuration applies after power cycling the debug probe.\n");
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecResetConfig
*/
static int _ExecResetConfig(const char* s) {
  int r;
  int ConfigSize;
  U8* pConfigData;
  U8 Dummy;

  r = JLINKARM_ReadEmuConfigMem(&Dummy, 0, 1);
  if (r != 0) {
    if (r == 2) {
      _ReportOutf("This feature is not supported by connected J-Link.\n");
    } else {
      _ReportOutf("Failed to reset config data. Error code %d\n", r);
    }
    return JLINK_ERROR_UNKNOWN;
  }
  ConfigSize = 0x100;  // Default config size supported by all models
  //
  // New models support a dynamic size of the config area so determine config area size (which is always a multiple of 256 bytes)
  //
  do {
    r = JLINKARM_ReadEmuConfigMem(&Dummy, ConfigSize, 1);
    if (r != 0) {
      break;
    }
    ConfigSize += 256;
  } while (1);
  pConfigData = (U8*)malloc(ConfigSize);
  memset(pConfigData, 0xFF, ConfigSize);
  r = JLINKARM_WriteEmuConfigMem(pConfigData, 0, ConfigSize);
  free(pConfigData);
  if (r != 0) {
    if (r == 2) {
      _ReportOutf("Write config data is not supported by connected J-Link.\n");
    } else {
      _ReportOutf("Failed to reset config data. Error code %d\n", r);
      return JLINK_ERROR_UNKNOWN;
    }
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteConfig
*/
static int _ExecWriteConfig(const char* s) {
  U32 Offset, Data;
  U8  ConfigByte;
  int r;
  if (_ParseHex(&s, &Offset)) {
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &Data)) {
    return JLINK_ERROR_SYNTAX;
  }
  ConfigByte = (U8)Data;
  r = JLINKARM_WriteEmuConfigMem(&ConfigByte, Offset, 1);
  if (r != 0) {
    if (r == 2) {
      _ReportOutf("Write config data is not supported by connected J-Link.\n");
    } else {
      _ReportOutf("Failed to write config data\n");
      return JLINK_ERROR_UNKNOWN;
    }
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecReadConfig
*/
static int _ExecReadConfig(const char* s) {
  U8* pData;
  U8* pBase;
  U32 NumBytes;
  U32 Offset;
  int r;
  int ConfigSize;
  U8 Dummy;

  pBase = NULL;
  pData = pBase;
  //
  // Default
  //
  NumBytes = JLINK_DEFAULT_SIZEOF_CONFIG;
  Offset   = 0;
  //
  // Any parameters given?
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseHex(&s, &Offset)) {
      _ReportOutf("Syntax: mem <Addr>, <NumBytes>\n");
      return JLINK_ERROR_SYNTAX;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (_ParseHex(&s, &NumBytes)) {
      _ReportOutf("Syntax: mem <Addr>, <NumBytes>\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  pBase = malloc(NumBytes);
  pData = pBase;
  if (pData == NULL) {
    _ReportOutf("Failed to allocate memory for temporary buffer.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  r = JLINKARM_ReadEmuConfigMem(pData, Offset, NumBytes);
  if (r != 0) {
    if (r == 2) {
      _ReportOutf("Read config data is not supported by connected J-Link.\n");
      return JLINK_NOERROR;
    } else {
      _ReportOutf("Failed to read config data.\n");
      return JLINK_ERROR_UNKNOWN;
    }
  }
  //
  // New models support a dynamic size of the config area so determine config area size (which is always a multiple of 256 bytes)
  //
  ConfigSize = 0x100;  // Default config size supported by all models
  do {
    r = JLINKARM_ReadEmuConfigMem(&Dummy, ConfigSize, 1);
    if (r != 0) {
      break;
    }
    ConfigSize += 256;
  } while (1);
  _ReportOutf("Total size of config area: 0x%X bytes\n\n", ConfigSize);
  while (NumBytes > 0) {
    int NumBytesPerLine;
    _ReportOutf("%.8X = ", Offset);
    NumBytesPerLine = (NumBytes > 16) ? 16 : NumBytes;
    NumBytes -= NumBytesPerLine;
    Offset   += NumBytesPerLine;
    for (; NumBytesPerLine > 0; NumBytesPerLine--) {
      _ReportOutf("%.2X ", *pData++);
    }
    _ReportOutf("\n");
  }
  if (pBase) {
    free(pBase);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteConfigVMSD
*/
static int _ExecWriteConfigVMSD(const char* s) {
  char ac[MAX_PATH];
  HANDLE hFile;
  U8* pData;
  int Result;
  int r;
  U32 FileSize;
  //
  // Parse filename of VMSD config file for J-Link
  //
  pData = NULL;
  Result = JLINK_NOERROR;
  ac[0] = 0;
  _ParseString(&s, ac, sizeof(ac));
  //
  // Read VMSD config file into tmeporary buffer
  //
  hFile = _OpenFile(ac, FILE_FLAG_READ | FILE_FLAG_SHARE_READ);
  if (hFile == INVALID_HANDLE_VALUE) {
    _ReportOutf("Failed to open file %s\n", ac[0] ? ac : "[NULL]");
    Result = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  FileSize = _GetFileSize(hFile);
  if ((FileSize == 0) || (FileSize == 0xFFFFFFFF)) {
    _ReportOutf("Invalid FileSize: %d\n", FileSize);
    Result = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  pData = (U8*)malloc(FileSize + sizeof(FileSize));  // Reserve some extra space to be able to store the filesize/streamsize at the beginning of the buffer
  if (pData == NULL) {
    _ReportOutf("Failed to allocate buffer for reading file.\n");
    Result = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  memcpy(pData, &FileSize, sizeof(FileSize));
  _ReadFile(hFile, pData + sizeof(FileSize), FileSize);
  _CloseFile(hFile);
  //
  // Download VMSD config file into config area of J-Link
  // Offset 0x800: Size of VMSD configuration downloaded
  // Offset 0x804: Start of VMSD configuration file
  //
  _ReportOutf("Downloading VMSD config...");
  r = JLINKARM_WriteEmuConfigMem(pData, 0x800, FileSize + sizeof(FileSize));
  if (r == 0) {
    _ReportOutf("O.K.\n");
  } else {
    if (r == 1) {
      _ReportOutf("ERROR: Configuration area of J-Link too small.\n");
      Result = JLINK_ERROR_UNKNOWN;
    } else {
      _ReportOutf("ERROR: Error while writing VMSD config.\n");
      Result = JLINK_ERROR_UNKNOWN;
    }
  }
  //
  // Maintenance
  //
Done:
  if (pData) {
    free(pData);
  }
  return Result;
}

/*********************************************************************
*
*       _ExecSelectFamily
*/
static int _ExecSelectFamily(const char* s) {
  U32 Family;
  if (_ParseHex(&s, &Family)) {
    _ReportOutf("Parameters: <3:Cortex-M3 / 5:XScale / 7:ARM7 / 9:ARM9 / 11:ARM11>\n");
    return JLINK_ERROR_SYNTAX;
  } else {
    JLINKARM_SelectDeviceFamily(Family);
    _ReportOutf("Device family has been set to %d\n", Family);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSelectCore
*/
static int _ExecSelectCore(const char* s) {
  U32 Core;
  if (_ParseHex(&s, &Core)) {
    _ReportOutf("Syntax: SelectCore <Core>\n");
    return JLINK_ERROR_SYNTAX;
  } else {
    JLINKARM_CORE_Select(Core);
    _ReportOutf("Core has been set to 0x%.8X\n", Core);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _TIF2Name
*/
static const char* _TIF2Name(int Interface) {
  static char _acTIFName[16];
  switch (Interface) {
  case JLINKARM_TIF_JTAG:              strcpy(_acTIFName, "JTAG"); break;
  case JLINKARM_TIF_SWD:               strcpy(_acTIFName, "SWD");  break;
  case JLINKARM_TIF_BDM3:              strcpy(_acTIFName, "BDM3"); break;
  case JLINKARM_TIF_FINE:              strcpy(_acTIFName, "FINE"); break;
  case JLINKARM_TIF_2_WIRE_JTAG_PIC32: strcpy(_acTIFName, "ICSP"); break;
  case JLINKARM_TIF_SPI:               strcpy(_acTIFName, "SPI");  break;
  case JLINKARM_TIF_C2:                strcpy(_acTIFName, "C2");   break;
  default:
    sprintf(_acTIFName, "Interface %d", Interface);
  }
  return _acTIFName;
}

/*********************************************************************
*
*       _Name2TIF
*/
static int _Name2TIF(const char* s) {
  if (_JLinkstricmp(s, "JTAG") == 0) {
    return JLINKARM_TIF_JTAG;
  } else if (_JLinkstricmp(s, "SWD") == 0) {
    return JLINKARM_TIF_SWD;
  } else if (_JLinkstricmp(s, "BDM3") == 0) {
    return JLINKARM_TIF_BDM3;
  } else if (_JLinkstricmp(s, "UART") == 0) {                // Formerly name of FINE interface
    return JLINKARM_TIF_FINE;
  } else if (_JLinkstricmp(s, "FINE") == 0) {
    return JLINKARM_TIF_FINE;
  } else if (_JLinkstricmp(s, "2-wire-JTAG-PIC32") == 0) {   // Compatible reasons. Official interface name is: ICSP
    return JLINKARM_TIF_2_WIRE_JTAG_PIC32;
  } else if (_JLinkstricmp(s, "ICSP") == 0) {
    return JLINKARM_TIF_2_WIRE_JTAG_PIC32;
  } else if (_JLinkstricmp(s, "SPI") == 0) {
    return JLINKARM_TIF_SPI;
  } else if (_JLinkstricmp(s, "C2") == 0) {
    return JLINKARM_TIF_C2;
  } else {
    _ReportOutf("ERROR while parsing value for -if.\n");
    return -1;
  }
}

/*********************************************************************
*
*       _ExecSelectInterface
*/
static int _ExecSelectInterface(const char* s) {
  U32 Interface;
  U32 IFMask;
  char ac[MAX_PATH+10];
  int Result;
  unsigned WasConnectedToTarget;
  COMMANDER_SETTINGS* pCommanderSettings;
  //
  // Obsolete method: Customers can pass the interface number to select the interface
  // New method: Customers can pass the interface name (e.g. SWD, JTAG, etc...) to select the interface
  //
  pCommanderSettings = &_CommanderSettings;
  if (_ParseHex(&s, &Interface)) {                             // No hex index parsed? --> Parse the string
    _ParseString(&s, ac, sizeof(ac));
    if (ac[0] == 0) {                                          // No string to Parsed? --> Error
      _ReportOutf("Syntax: SelectInterface <interface>\n");
      return JLINK_ERROR_SYNTAX;
    } else {
      Interface = _Name2TIF(ac);
      if (Interface < 0) {
        _ReportOutf("Syntax: Unknown interface entered: %s\n", ac);
        return JLINK_ERROR_SYNTAX;
      }
    }
  } 
  //
  // Disconnect from target => Target connection will be re-established on next command that needs it
  // Update commander settings
  //
  WasConnectedToTarget = pCommanderSettings->Status.ConnectedToTarget;
  if (WasConnectedToTarget) {
    _DisconnectFromTarget(pCommanderSettings);
  }
  IFMask = 0;
  JLINKARM_TIF_GetAvailable(&IFMask);
  Result = JLINK_NOERROR;
  if ((IFMask & (1uL << Interface)) == 0) {
    _ReportOutf("%s is not supported by connected emulator.\n", _TIF2Name(Interface));
    Result = JLINK_ERROR_UNKNOWN;
  } else {
    _ReportOutf("Selecting %s as current target interface.\n", _TIF2Name(Interface));
    pCommanderSettings->TargetIF = Interface;
    pCommanderSettings->TargetIFSet = 1;
  }
  return Result;
}

/*********************************************************************
*
*       _ExecTraceClear
*/
static int _ExecTraceClear(const char* s) {
  JLINKARM_TRACE_Control(JLINKARM_TRACE_CMD_FLUSH, NULL);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTraceSetFormat
*/
static int _ExecTraceSetFormat(const char* s) {
  U32 Size;
  U32 Format;
  if (_ParseHex(&s, &Size)) {
    _ReportOutf("Parameters: <4/8/16>\n");
    return JLINK_ERROR_SYNTAX;
  } else {
    if (Size == 4) {
      Format = JLINKARM_TRACE_FORMAT_4BIT;
    } else if (Size == 8) {
      Format = JLINKARM_TRACE_FORMAT_8BIT;
    } else if (Size == 16) {
      Format = JLINKARM_TRACE_FORMAT_16BIT;
    } else {
      _ReportOutf("Invalid trace format\n");
      return JLINK_ERROR_UNKNOWN;
    }
    JLINKARM_TRACE_Control(JLINKARM_TRACE_CMD_SET_FORMAT, &Format);
    _ReportOutf("Trace format has been set to %d bits\n", Size);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTraceStart
*/
static int _ExecTraceStart(const char* s) {
  U32 TFreq;
  JLINKARM_TRACE_Control(JLINKARM_TRACE_CMD_START, &TFreq);
  if (TFreq) {
    _ReportOutf("Trace started, Trace frequency is %dkHz\n", TFreq / 1000);
  } else {
    _ReportOutf("Trace could not be started, no trace clock.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTraceStop
*/
static int _ExecTraceStop(const char* s) {
  U32 TFreq;
  JLINKARM_TRACE_Control(JLINKARM_TRACE_CMD_STOP, &TFreq);
  _ReportOutf("Trace stopped.\n");
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTraceSetSize
*/
static int _ExecTraceSetSize(const char* s) {
  U32 Size;
  if (_ParseHex(&s, &Size)) {
    _ReportOutf("Syntax: TSetSize <Size(hex)>\n");
    return JLINK_ERROR_SYNTAX;
  } else {
    JLINKARM_TRACE_Control(JLINKARM_TRACE_CMD_SET_CAPACITY, (U32*)&Size);
    _ReportOutf("Trace Buffer has been set to 0x%X Bytes (%dkBytes)\n", Size, Size >> 10);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTraceAddInst
*/
static int _ExecTraceAddInst(const char* s) {
  // No longer used
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTraceAddBranch
*/
static int _ExecTraceAddBranch(const char* s) {
  // No longer used
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecResetSetType
*/
static int _ExecResetSetType(const char* s) {
  const char* sName;
  const char* sDesc;
  const char* sErr;
  int NumTypes, i;
  U32 Type;

  sErr = _ParseDec(&s, &Type);
  if (sErr) {
    _ReportOutf("Syntax: RSetType <type>\n\n");
    _ReportOutf("        Types:\n");
    NumTypes = JLINKARM_GetResetTypeDesc(0, 0, 0);
    for (i = 0; i < NumTypes; i++) {
      JLINKARM_GetResetTypeDesc(i, &sName, &sDesc);
      _ReportOutf("        %d = %s (%s)\n", i, sName, sDesc);
    }
    return JLINK_ERROR_SYNTAX;
  } else {
    _ResetType = Type;
    JLINKARM_SetResetType(Type);
    JLINKARM_GetResetTypeDesc(Type, &sName, &sDesc);
    _ReportOutf("Reset type %s: %s\n", sName, sDesc);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSetWP
*/
static int _ExecSetWP(const char* s) {
  JLINKARM_DATA_EVENT Event;
  const char* sErr;
  U32 Handle;
  U32 Addr;
  U32 AddrMask;
  U32 Data;
  U32 DataMask;
  U8  Access;
  U8  AccessMask;
  int r;

  Addr       = 0;
  AddrMask   = 0;              // exact match
  Data       = 0;
  DataMask   = 0xffffffff;     // don't care
  Access     = 0;
  AccessMask = JLINKARM_WP_MASK_PRIV;
  //
  // Parse Addr
  //
  sErr = _ParseHex(&s, &Addr);
  if (sErr) {
    goto SyntaxError;
  }
  //
  // Parse optional <accesstype>
  //
  _EatWhiteAndSeparator(&s);
  if        (_CompareCmd(&s, "W") == 0) {
    Access |= JLINKARM_WP_DIR_WR;
  } else if (_CompareCmd(&s, "R") == 0) {
    Access |= JLINKARM_WP_DIR_RD;
  } else {
    AccessMask |= JLINKARM_WP_MASK_DIR;
  }
  //
  // Parse optional <size>
  //
  _EatWhiteAndSeparator(&s);
  if        (_CompareCmd(&s, "S8")  == 0) {
    Access |= JLINKARM_WP_SIZE_8BIT;
  } else if (_CompareCmd(&s, "S16") == 0) {
    Access |= JLINKARM_WP_SIZE_16BIT;
  } else if (_CompareCmd(&s, "S32") == 0) {
    Access |= JLINKARM_WP_SIZE_32BIT;
  } else {
    AccessMask |= JLINKARM_WP_MASK_SIZE;
  }
  //
  // Parse optional <Data>, <DataMask>
  //
  _EatWhiteAndSeparator(&s);
  if (*s) {
    sErr = _ParseHex(&s, &Data);
    if (sErr) {
      goto SyntaxError;
    }
    DataMask = 0;     // Exact match
    //
    // Parse optional <DataMask>
    //
    _EatWhiteAndSeparator(&s);
    if (*s) {
      sErr = _ParseHex(&s, &DataMask);
      if (sErr) {
        goto SyntaxError;
      }
      //
      // Parse optional <AddrMask>
      //
      _EatWhiteAndSeparator(&s);
      if (*s) {
        sErr = _ParseHex(&s, &AddrMask);
        if (sErr) {
          goto SyntaxError;
        }
      }
    }
  }
  //
  // Set event
  //
  memset(&Event, 0, sizeof(JLINKARM_DATA_EVENT));
  Event.SizeOfStruct = sizeof(JLINKARM_DATA_EVENT);
  Event.Type         = JLINKARM_EVENT_TYPE_DATA_BP;
  Event.Addr         = Addr;
  Event.AddrMask     = AddrMask;
  Event.Data         = Data;
  Event.DataMask     = DataMask;
  Event.Access       = Access;
  Event.AccessMask   = AccessMask;
  r = JLINKARM_SetDataEvent(&Event, &Handle);
  if (r < 0) {
    r = JLINKARM_SetDataEvent(&Event, &Handle);
  }
  if (Handle) {
    _ReportOutf("Watchpoint set @ addr 0x%.8X (Handle = 0x%.8X)", Addr, Handle);
    if ((AccessMask & JLINKARM_WP_MASK_DIR) == 0) {
      if ((Access & JLINKARM_WP_MASK_DIR) == JLINKARM_WP_DIR_WR) {
        _ReportOutf(" Write");
      } else {
        _ReportOutf(" Read");
      }
    }
    if ((AccessMask & JLINKARM_WP_MASK_SIZE) == 0) {
      if        ((Access & JLINKARM_WP_MASK_SIZE) == JLINKARM_WP_SIZE_8BIT) {
        _ReportOutf(" 8-Bit");
      } else if ((Access & JLINKARM_WP_MASK_SIZE) == JLINKARM_WP_SIZE_16BIT) {
        _ReportOutf(" 16-Bit");
      } else if ((Access & JLINKARM_WP_MASK_SIZE) == JLINKARM_WP_SIZE_32BIT) {
        _ReportOutf(" 32-Bit");
      }
    }
    if (DataMask != 0xFFFFFFFF) {
      _ReportOutf(" 0x%.8X", Data);
      if (DataMask != 0) {
        _ReportOutf(" Data mask: 0x%.8X", DataMask);
      }
    }
    if (AddrMask != 0) {
      _ReportOutf(" Addr mask: 0x%.8X", AddrMask);
    }
  } else {
    _ReportOutf("Could not set watchpoint @ addr 0x%.8X\n", Addr);
    return JLINK_ERROR_WARNING;
  }
  _ReportOutf("\n");
  return JLINK_NOERROR;
SyntaxError:
  _ReportOutf("Syntax: <addr> [<accesstype>] [<size>] [<data> [<data mask> [<addr mask>]]]\n\n"
         "        <accesstype> = R | W\n"
         "        <size>       = S8 | S16 | S32\n"
         "        <data>       = Data value that should be read/written\n"
         "                       to trigger watchpoint\n"
         "        <data mask>  = Bits set to 1 are \"do not care\"\n"
         "        <addr mask>  = Bits set to 1 are \"do not care\"\n"
         "Example:\n"
         "Stop if value 0x1000 is written to addr. 0x20000000 (32-bit access):\n"
         "setwp 0x20000000 W S32 0x1000 0 0\n"
        );
  return JLINK_ERROR_SYNTAX;
}

/*********************************************************************
*
*       _ExecClrWP
*/
static int _ExecClrWP(const char* s) {
  const char * sErr;
  int WPHandle;
  //
  // Parse handle identifier
  //
  sErr = _ParseHex(&s, &WPHandle);
  if (sErr) {
    goto Err;
  }
  if (JLINKARM_ClrWP(WPHandle)) {
    _ReportOutf("Could not clear watchpoint 0x%.8X\n", WPHandle);
    return JLINK_ERROR_WARNING;
  } else {
    _ReportOutf("Watchpoint cleared\n");
  }
  return JLINK_NOERROR;
Err:
  _ReportOutf("Parameters: <handle>\n");
  return JLINK_ERROR_SYNTAX;
}

/*********************************************************************
*
*       _ExecSetBP
*/
static int _ExecSetBP(const char* s) {
  const char * sErr;
  char c;
  U32 Addr;
  U32 BPType;
  int BPHandle;
  U32 DevFamily;
  
  BPType = JLINKARM_BP_TYPE_ARM;
  DevFamily = JLINKARM_GetDeviceFamily();
  //
  // Parse Addr
  //
  sErr = _ParseHex(&s, &Addr);
  if (sErr) {
    goto Err;
  }
  //
  // Check breakpoint mode (ARM/THUMB) only for ARM7/9/11 and Cortex-R4
  //
  if (DevFamily == JLINKARM_DEV_FAMILY_ARM7 || DevFamily == JLINKARM_DEV_FAMILY_ARM9 || DevFamily == JLINKARM_DEV_FAMILY_ARM7 || DevFamily == JLINKARM_DEV_FAMILY_ARM11 || DevFamily == JLINKARM_DEV_FAMILY_CORTEX_R4) {
    //
    // Parse optional <A/T>
    //
    _EatWhite(&s);
    c = tolower(*s);
    if (c == 'a') {
      s++;
      BPType = JLINKARM_BP_TYPE_ARM;      // ARM mode
    } else if (c == 't') {
      s++;
      BPType = JLINKARM_BP_TYPE_THUMB;    // THUMB mode
    } else {
      _ReportOutf("Syntax: setbp <Addr> <A/T> [S/H]\n");
      return JLINK_ERROR_SYNTAX;
    }
  } else if (DevFamily == JLINK_DEV_FAMILY_MIPS) {
    //
    // For MIPS, breakpoint size has to be defined
    // W = Word => MIPS32 mode
    // H = Half-word => MIPS16e mode
    //
    // Parse optional <W/H>
    //
    _EatWhite(&s);
    c = tolower(*s);
    if (c == 'w') {
      s++;
      BPType = JLINKARM_BP_MODE1;      // MIPS32 mode
    } else if (c == 'h') {
      s++;
      BPType = JLINKARM_BP_MODE2;      // MIPS16e mode
    } else {
      _ReportOutf("Syntax: setbp <Addr> <W/H> [S/H]\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  //
  // Parse optional <S/H>
  //
  _EatWhite(&s);
  c = tolower(*s);
  if (c == 's') {
    s++;
    BPType |= JLINKARM_BP_TYPE_SW;      // Software breakpoint
  }
  if (c == 'h') {
    s++;
    BPType |= JLINKARM_BP_TYPE_HW;      // Hardware breakpoint
  }
  BPHandle = JLINKARM_SetBPEx(Addr, BPType);
  if (BPHandle) {
    _ReportOutf("Breakpoint set @ addr 0x%.8X (Handle = %d)\n", Addr, BPHandle);
  } else {
    _ReportOutf("Could not set breakpoint @ addr 0x%.8X\n", Addr);
    return JLINK_ERROR_WARNING;
  }
  return JLINK_NOERROR;
Err:
  switch (DevFamily) {
  case JLINKARM_DEV_FAMILY_ARM7:
  case JLINKARM_DEV_FAMILY_ARM9:
  case JLINKARM_DEV_FAMILY_ARM10:
  case JLINKARM_DEV_FAMILY_ARM11:
  case JLINKARM_DEV_FAMILY_CORTEX_R4:
  case JLINKARM_DEV_FAMILY_CORTEX_A5:
  case JLINKARM_DEV_FAMILY_CORTEX_A8:
    _ReportOutf("Parameters: <addr> <A/T> [S/H]\n");
    break;
  case JLINKARM_DEV_FAMILY_CM1:
  case JLINKARM_DEV_FAMILY_CM3:
  case JLINKARM_DEV_FAMILY_CM0:
  case JLINKARM_DEV_FAMILY_RX:
  case JLINKARM_DEV_FAMILY_CM4:
    _ReportOutf("Parameters: <Addr> [S/H]\n");
    break;
  case JLINK_DEV_FAMILY_MIPS:
    _ReportOutf("Parameters: <Addr> <W/H> [S/H]\n");
    break;
  }
  return JLINK_ERROR_SYNTAX;
}

/*********************************************************************
*
*       _Reset
*/
static void _Reset(int Delay) {
  const char* sName;
  const char* sDesc;
  JLINKARM_GetResetTypeDesc(_ResetType, &sName, &sDesc);
  _ReportOutf("Reset delay: %d ms\n", Delay);
  _ReportOutf("Reset type %s: %s\n", sName, sDesc);
  JLINKARM_SetResetDelay(Delay);
  JLINKARM_Reset();
}

/*********************************************************************
*
*       _ExecTraceShowRegions
*/
static int _ExecTraceShowRegions(const char* s) {
  TRACE_ShowRegions();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSetLittleEndian
*/
static int _ExecSetLittleEndian(const char* s) {
  JLINKARM_SetEndian(ARM_ENDIAN_LITTLE);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSetBigEndian
*/
static int _ExecSetBigEndian(const char* s) {
  JLINKARM_SetEndian(ARM_ENDIAN_BIG);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecStartTrace
*/
static int _ExecStartTrace(const char* s) {
  JLINKARM_ETM_StartTrace();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecGo
*/
static int _ExecGo(const char* s) {
  U32 NumSteps = 0;
  U32 Flags = JLINKARM_GO_OVERSTEP_BP;

  _EatWhite(&s);
  if (*s) {
    if (_ParseDec(&s, &NumSteps)) {
      _ReportOutf("Syntax: g [<NumSteps> [<Flags>]]\n");
      return JLINK_ERROR_SYNTAX;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (*s) {
      if (_ParseHex(&s, &Flags)) {
        _ReportOutf("Syntax: g [<NumSteps> [<Flags>]]\n");
        return JLINK_ERROR_SYNTAX;
      }
    }
  }
  JLINKARM_GoEx(NumSteps, Flags);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecHalt
*/
static int _ExecHalt(const char* s) {
  if (!JLINKARM_Halt()) {
    _ShowRegs();
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecClock
*/
static int _ExecClock(const char* s) {
  U32 RepCount = 1;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseDec(&s, &RepCount)) {
      return JLINK_ERROR_SYNTAX;
    }
  }
  while (RepCount--) {
    JLINKARM_Clock();
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecC00
*/
static int _ExecC00(const char* s) {
  U32 RepCount = 1;
  U8 tdi = 0;
  U8 tms = 0;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseDec(&s, &RepCount)) {
      return JLINK_ERROR_SYNTAX;
    }
  }
  while (RepCount--) {
    JLINKARM_JTAG_StoreRaw(&tdi, &tms, 1);
  }
  JLINKARM_JTAG_SyncBits();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecClrTRST
*/
static int _ExecClrTRST(const char* s) {
  JLINKARM_ClrTRST();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSetTRST
*/
static int _ExecSetTRST(const char* s) {
  JLINKARM_SetTRST();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecClrRESET
*/
static int _ExecClrRESET(const char* s) {
  JLINKARM_ClrRESET();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSetRESET
*/
static int _ExecSetRESET(const char* s) {
  JLINKARM_SetRESET();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecClrTMS
*/
static int _ExecClrTMS(const char* s) {
  JLINKARM_ClrTMS();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSetTMS
*/
static int _ExecSetTMS(const char* s) {
  JLINKARM_SetTMS();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecClrTCK
*/
static int _ExecClrTCK(const char* s) {
  int r;

  r = JLINKARM_ClrTCK();
  if (r < 0) {
    _ReportOutf("Firmware of connected emulator does not support this feature.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSetTCK
*/
static int _ExecSetTCK(const char* s) {
  int r;

  r = JLINKARM_SetTCK();
  if (r < 0) {
    _ReportOutf("Firmware of connected emulator does not support this feature.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecClrTDI
*/
static int _ExecClrTDI(const char* s) {
  JLINKARM_ClrTDI();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSetTDI
*/
static int _ExecSetTDI(const char* s) {
  JLINKARM_SetTDI();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecResetTRST
*/
static int _ExecResetTRST(const char* s) {
  JLINKARM_ResetTRST();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecResetTAP
*/
static int _ExecResetTAP(const char* s) {
  JLINKARM_StoreBits(0x1F, 0, 6);
  JLINKARM_JTAG_SyncBytes();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecIdentifySCSRLen
*/
static int _ExecIdentifySCSRLen(const char* s) {
  _ReportOutf("JTAG scan length: %d\n", JLINKARM_GetScanLen());
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecIdentify
*/
static int _ExecIdentify(const char* s) {
  _PrintId(JLINKARM_GetId());
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecShowHWStatus
*/
static int _ExecShowHWStatus(const char* s) {
  _ShowHWStatus();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecShowHWInfo
*/
static int _ExecShowHWInfo(const char* s) {
  _ShowHWInfo();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecShowFirmwareInfo
*/
static int _ExecShowFirmwareInfo(const char* s) {
  _ShowFirmwareInfo();
  _ReportOutf("\n");
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecShowICERegs
*/
static int _ExecShowICERegs(const char* s) {
  _ShowDebugState();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecShowETMState
*/
static int _ExecShowETMState(const char* s) {
  _ShowETMState();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteETMReg
*/
static int _ExecWriteETMReg(const char* s) {
  U32 Addr, Data;
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: we <Addr>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  s++;
  if (_ParseHex(&s, &Data)) {
    _ReportOutf("Syntax: we <Addr>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _ReportOutf("ETMreg[%.8X] = %.8X\n", Addr, Data);
  JLINKARM_ETM_WriteReg(Addr, Data, 0);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecReadETMReg
*/
static int _ExecReadETMReg(const char* s) {
  U32 Addr, Data;
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: re <Addr>\n");
    return JLINK_ERROR_SYNTAX;
  }
  Data = JLINKARM_ETM_ReadReg(Addr);
  _ReportOutf("ETMreg[%.8X] = %.8X\n", Addr, Data);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecShowETBState
*/
static int _ExecShowETBState(const char* s) {
  _ShowETBState();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteETBReg
*/
static int _ExecWriteETBReg(const char* s) {
  U32 Addr, Data;
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: wb <Addr>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  s++;
  if (_ParseHex(&s, &Data)) {
    _ReportOutf("Syntax: wb <Addr>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _ReportOutf("ETBreg[%.2X] = %.8X\n", Addr, Data);
  JLINKARM_ETB_WriteReg(Addr, Data, 0);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecReadETBReg
*/
static int _ExecReadETBReg(const char* s) {
  U32 Addr, Data;
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: rb <Addr>\n");
    return JLINK_ERROR_SYNTAX;
  }
  Data = JLINKARM_ETB_ReadReg(Addr);
  _ReportOutf("ETBreg[%.2X] = %.8X\n", Addr, Data);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _GetCurrentPC
*/
static int _GetCurrentPC(U32* pCurrentPC) {
  U32 DevFamily;
  int RegIndexPC;
  
  DevFamily  = JLINKARM_GetDeviceFamily();
  switch (DevFamily) {
  case JLINKARM_DEV_FAMILY_CF:        RegIndexPC = JLINK_CF_REG_PC;            break;
  case JLINKARM_DEV_FAMILY_CM0:       RegIndexPC = JLINKARM_CM3_REG_R15;       break;
  case JLINKARM_DEV_FAMILY_CM1:       RegIndexPC = JLINKARM_CM3_REG_R15;       break;
  case JLINKARM_DEV_FAMILY_CM3:       RegIndexPC = JLINKARM_CM3_REG_R15;       break;
  case JLINKARM_DEV_FAMILY_CM4:       RegIndexPC = JLINKARM_CM3_REG_R15;       break;
  case JLINKARM_DEV_FAMILY_CORTEX_R4: RegIndexPC = JLINKARM_CORTEX_R4_REG_R15; break;
  case JLINKARM_DEV_FAMILY_RX:        RegIndexPC = JLINKARM_RX_REG_PC;         break;
  case JLINKARM_DEV_FAMILY_POWER_PC:  RegIndexPC = JLINK_POWER_PC_REG_PC;      break;
  case JLINKARM_DEV_FAMILY_CORTEX_A5: RegIndexPC = JLINKARM_CORTEX_R4_REG_R15; break;
  case JLINKARM_DEV_FAMILY_CORTEX_A8: RegIndexPC = JLINKARM_CORTEX_R4_REG_R15; break;
  case JLINK_DEV_FAMILY_MIPS:         RegIndexPC = JLINK_MIPS_REG_PC;          break;
  case JLINKARM_DEV_FAMILY_ARM7:      RegIndexPC = ARM_REG_R15;                break;
  case JLINKARM_DEV_FAMILY_ARM9:      RegIndexPC = ARM_REG_R15;                break;
  case JLINKARM_DEV_FAMILY_ARM11:     RegIndexPC = ARM_REG_R15;                break;
  case JLINK_DEV_FAMILY_EFM8:         RegIndexPC = JLINK_EFM8_PC;              break;
  default:                            return -1;
  }
  *pCurrentPC = JLINKARM_ReadReg(RegIndexPC);
  return 0;
}

/*********************************************************************
*
*       _DisassembleInst
*/
static int _DisassembleInst(char* pBuffer, U32 Addr, const JLINK_DISASSEMBLY_INFO* pInfo) {
  char acDA[64];
  U8   abData[32];
  U8*  p;
  int  InstSize;
  int  Offset;
  int  NumBytesAtOnce;
  int  r;

  if (pInfo == NULL) {
    InstSize = JLINKARM_DisassembleInst(acDA, sizeof(acDA), Addr);
    if (InstSize < 0) {
      sprintf(pBuffer, "%.8X:  ???                ???\n", Addr);
      return -1;
    }
  } else {
    InstSize = JLINKARM_DisassembleInstEx(acDA, sizeof(acDA), Addr, pInfo);
    if (InstSize < 0) {
      sprintf(pBuffer, "%.8X:  ???                ???\n", Addr);
      return -1;
    }
  }
  //
  // For 8051 devices, we need to make sure that we read from the CODE area
  //
  if (JLINKARM_GetDeviceFamily() == JLINK_DEV_FAMILY_EFM8) {
    r = JLINKARM_ReadMemEx(Addr | JLINK_EFM8_START_ADDR_CODE, InstSize, abData, 0);
  } else {
    r = JLINKARM_ReadMemEx(Addr, InstSize, abData, 0);
  }
  if (r != InstSize) {
    sprintf(pBuffer, "%.8X:  ???                %s\n", Addr, acDA);
    return -1;
  }
  switch (InstSize) {
  case 0:   sprintf(pBuffer, "%.8X:  -                  %s\n",              Addr,                                                                   acDA);  break;
  case 1:   sprintf(pBuffer, "%.8X:  %.2X                 %s\n",            Addr, abData[0],                                                        acDA);  break;
  case 2:   sprintf(pBuffer, "%.8X:  %.2X %.2X              %s\n",          Addr, abData[0], abData[1],                                             acDA);  break;
  case 3:   sprintf(pBuffer, "%.8X:  %.2X %.2X %.2X           %s\n",        Addr, abData[0], abData[1], abData[2],                                  acDA);  break;
  case 4:   sprintf(pBuffer, "%.8X:  %.2X %.2X %.2X %.2X        %s\n",      Addr, abData[0], abData[1], abData[2], abData[3],                       acDA);  break;
  case 5:   sprintf(pBuffer, "%.8X:  %.2X %.2X %.2X %.2X %.2X     %s\n",    Addr, abData[0], abData[1], abData[2], abData[3], abData[4],            acDA);  break;
  default:  sprintf(pBuffer, "%.8X:  %.2X %.2X %.2X %.2X %.2X %.2X  %s\n",  Addr, abData[0], abData[1], abData[2], abData[3], abData[4], abData[5], acDA);  break;
  }
  if (InstSize > 6) {
    Offset = 6;
    do {
      NumBytesAtOnce = MIN(InstSize - Offset, 6);
      p = &abData[Offset];
      switch (NumBytesAtOnce) {
      case 1:   sprintf(pBuffer, "%s           %.2X\n",                          pBuffer, *(p+0));                                         break;
      case 2:   sprintf(pBuffer, "%s           %.2X %.2X\n",                     pBuffer, *(p+0), *(p+1));                                 break;
      case 3:   sprintf(pBuffer, "%s           %.2X %.2X %.2X\n",                pBuffer, *(p+0), *(p+1), *(p+2));                         break;
      case 4:   sprintf(pBuffer, "%s           %.2X %.2X %.2X %.2X\n",           pBuffer, *(p+0), *(p+1), *(p+2), *(p+3));                 break;
      case 5:   sprintf(pBuffer, "%s           %.2X %.2X %.2X %.2X %.2X\n",      pBuffer, *(p+0), *(p+1), *(p+2), *(p+3), *(p+4));         break;
      default:  sprintf(pBuffer, "%s           %.2X %.2X %.2X %.2X %.2X %.2X\n", pBuffer, *(p+0), *(p+1), *(p+2), *(p+3), *(p+4), *(p+5)); break;
      }
      Offset += NumBytesAtOnce;
    } while (Offset < InstSize);
  }

  return InstSize;
}

/*********************************************************************
*
*       _ExecStep
*/
static int _ExecStep(const char* s) {
  char ac[256];
  U32 CurrentPC;
  int r;

  if (JLINKARM_IsHalted() == 0) {
    _cbErrorOut("CPU is not halted");
    return JLINK_ERROR_UNKNOWN;
  }
  r = _GetCurrentPC(&CurrentPC);
  if (r >= 0) {
    r = _DisassembleInst(ac, CurrentPC, NULL);
    if (r >= 0) {
      _ReportOutf(ac);
    }
  }
  JLINKARM_Step();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecDisassemble
*/
static int _ExecDisassemble(const char* s) {
  char ac[256];
  U32 Addr;
  U32 NumInsts     = 10;
  int UseCurrentPC = 1;
  int r;
  JLINK_DISASSEMBLY_INFO DAInfo;

  DAInfo.SizeOfStruct = sizeof(JLINK_DISASSEMBLY_INFO);
  DAInfo.Mode = JLINK_DISASSEMBLY_MODE_CURRENT;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (*s == ',') {
      goto ParseNumInsts;
    }
    if (_JLinkstrnicmp(s, "PC", 2) == 0) {
      s += 2;
      goto ParseNumInsts;
    }
    if (_ParseHex(&s, &Addr)) {
      _ReportOutf("Syntax: disassemble <Addr> [<NumInsts>]\n");
      return JLINK_ERROR_SYNTAX;
    }
    UseCurrentPC = 0;
ParseNumInsts:
    _EatWhite(&s);
    if (*s) {
      if (*s == ',') {
        s++;
      }
      if (_ParseDec(&s, &NumInsts)) {
        _ReportOutf("Syntax: disassemble <Addr> [<NumInsts>]\n");
        return JLINK_ERROR_SYNTAX;
      }
    }
    _EatWhite(&s);
    if (*s) {
      if (_JLinkstrnicmp(s, "ARM", 3) == 0) {
        DAInfo.Mode = JLINK_DISASSEMBLY_MODE_ARM;
      } else if (_JLinkstrnicmp(s, "THUMB", 5) == 0) {
        DAInfo.Mode = JLINK_DISASSEMBLY_MODE_THUMB;
      }
    }
  }
  //
  // Get current PC if necessary
  //
  if (UseCurrentPC) {
    if (JLINKARM_IsHalted() == 0) {
      _cbErrorOut("CPU is not halted");
      return JLINK_ERROR_UNKNOWN;
    }
    r = _GetCurrentPC(&Addr);
    if (r < 0) {
      _cbErrorOut("Could not read current PC");
      return JLINK_ERROR_UNKNOWN;
    }
  }
  //
  // Disassemble instructions
  //
  while (NumInsts) {
    r = _DisassembleInst(ac, Addr, &DAInfo);
    _ReportOutf(ac);
    if (r < 0) {
      break;
    }
    Addr += r;
    NumInsts--;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecShowRegs
*/
static int _ExecShowRegs(const char* s) {
  if (JLINKARM_IsHalted() > 0) {
    _ShowRegs();
  } else {
    _ReportOutf("CPU is not halted !\n");
    return JLINK_ERROR_WARNING;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecLog
*/
static int _ExecLog(const char* s) {
  char ac[MAX_PATH+10];
  _ParseString(&s, ac, sizeof(ac));
  JLINKARM_SetLogFile(ac);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _UnlockLM3Sxxx
*
*    The internal flash of the device is erased via a special unlock sequence:
*     1. nRESET is pulled LOW and hold it LOW
*     2. Perform switching sequence JTAG->SWD then SWD->JTAG
*     3. Perform 2. again 4 times.
*     4. Release nRESET
*    JTAG-to-SWD Switching
*    -----------------------
*    1. Send at least 50 TCK/SWCLK cycles with TMS/SWDIO set to 1.
*       This ensures that both JTAG and SWD are in their reset/idle states.
*    2. Send the 16-bit JTAG-to-SWD switch sequence, 0xE79E. (Sending LSB first)
*    3. Send at least 50 TCK/SWCLK cycles with TMS/SWDIO set to 1.
*       This ensures that if SWJ-DP was already in SWD mode, before sending the switch sequence,
*       the SWD goes into the line reset state.
*    SWD-to-JTAG Switching
*    -----------------------
*    1. Send at least 50 TCK/SWCLK cycles with TMS/SWDIO set to 1. This ensures that both JTAG and
*       SWD are in their reset/idle states.
*    2. Send the 16-bit SWD-to-JTAG switch sequence, 0xE73C. (Sending LSB first)
*    3. Send at least 5 TCK/SWCLK cycles with TMS/SWDIO set to 1.
*       This ensures that if SWJ-DP was already in JTAG mode, before sending the switch sequence,
*       the JTAG goes into the Test Logic Reset state.
*/
static int _UnlockLM3Sxxx(const char * s) {
  int i;
  U8 aTDI[7] = {0};
  U8 aTMS[7] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x03};  // 50 clocks with TMS high to go to reset/idle
  U16 SwitchToSWD;
  U16 SwitchToJTAG;
  U32 DisInteraction;

  _ReportOutf("Be sure that nRESET is LOW before continuing.\n"
         "If you are working with an eval board,\n"
         "please press the RESET button and keep it pressed.\n"
        );
  _ParseDec(&s, &DisInteraction);
  if (DisInteraction != 1) {
    _ReportOutf("Press enter to start unlock sequence...");
    getchar();
  }
  _ReportOutf("Unlocking device...");
  SwitchToSWD  = 0xE79E;
  SwitchToJTAG = 0xE73C;
  //
  // Make sure that all JTAG sequences are output before we set RESET low.
  // Since while reset is active, the LM3S devices do not respond with
  // their IRPrint when sending a JTAG command.
  // This would cause a "Bad JTAG communication" error.
  //
  JLINKARM_JTAG_SyncBits();
  JLINKARM_ClrRESET();
  for (i = 0; i < 5; i++) {
    //
    // Switch to SWD
    //
    JLINKARM_JTAG_StoreRaw(&aTDI[0], &aTMS[0], 50);
    JLINKARM_JTAG_StoreRaw(&aTDI[0], (U8 *)&SwitchToSWD, 16);
    JLINKARM_JTAG_StoreRaw(&aTDI[0], &aTMS[0], 50);
    //
    // Switch back to JTAG
    //
    JLINKARM_JTAG_StoreRaw(&aTDI[0], &aTMS[0], 50);
    JLINKARM_JTAG_StoreRaw(&aTDI[0], (U8 *)&SwitchToJTAG, 16);
    JLINKARM_JTAG_StoreRaw(&aTDI[0], &aTMS[0], 50);
    JLINKARM_JTAG_SyncBits();
  }  
  JLINKARM_SetRESET();
  _Sleep(400);
  _ReportOutf("O.K.\n");
  _ReportOutf("Please power-cycle target hardware.\n");
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _UnlockKinetis
*
*  Function description
*    Freescale Kinetis K40 and K60 devices unlock
*    For information about the MDM-AP, please refer to
*    \\fileserver\techinfo\Company\Freescale\MCU\CortexM4_Kinetis\K40P144M100SF2RM_review.pdf
*/
static int _UnlockKinetis(const char * s) {
  int r;
  int Stat;
  U32 Data;
  int t;

  Stat = JLINK_ERROR_UNKNOWN;
  //
  // Freescale Kinetis K40 and K60 devices unlock
  // For information about the MDM-AP, please refer to
  // \\fileserver\techinfo\Company\Freescale\MCU\CortexM4_Kinetis\K40P144M100SF2RM_review.pdf
  //
  JLINKARM_TIF_Select(1);
  _TIF_SWD_Select();
  _ReportOutf("Unlocking device...");
  JLINKARM_ClrRESET();
  _Sleep(50);
  _TIF_SWD_WriteU32(DP_REG_ABORT, 0x1E);     // Clear all sticky error flags
  _TIF_SWD_WriteU32(DP_REG_SELECT, 1 << 24); // Select AP[1] which is the MDM-AP
  r = _ReadUntilOK(0 | (1 << 2), &Data);     // Trigger read of AP[1] bank 0, reg 0 which is the MDM-AP status register
  if (r < 0) {
    goto Done;
  }
  r = _ReadUntilOK(0 | (1 << 2), &Data);     // Trigger second read of AP[1] bank 0, reg 0 which is the MDM-AP status register. This time we will get valid data (from the read which was triggered first)
  if (r < 0) {
    goto Done;
  }
  //
  // In case of enabled system security
  // also check if mass erase is allowed
  // If the system security is not active, mass erase disabled set has no effect
  //
  if (Data & (1 << 2)) {
    if ((Data & (1 << 5)) == 0) {
      _ReportOutf("Unlock via debug port is disabled. Unlock failed.\n");
    }
  }
  _TIF_SWD_WriteU32(1 | (1 << 2), 1 << 0);   // Write AP[1] bank 0, reg 1 which is the MDM-AP control register. Bit 0 requests a mass erase of the flash memory. This will also disable the security.
  //
  // Wait until mass erase command has been accepted
  // by checking the "Flash Mass Erase Acknowledge"
  // bit in the status register
  //
  r = _ReadUntilOK(0 | (1 << 2), &Data);     // Trigger the first read outside the loop to make sure that we will always get valid data inside the loop
  if (r < 0) {
    goto Done;
  }
  t = _GetTickCount() + 3000;
  do {
    r = _ReadUntilOK(0 | (1 << 2), &Data);
    if (r < 0) {
      goto Done;
    }
    if (Data & (1 << 0)) {                   // "Flash Mass Erase Acknowledge" bit is set as soon as the mass erase command has been accepted
      break;
    }
    if ((t - _GetTickCount()) <= 0) {
      _ReportOutf("Timeout while unlocking device.\n");
      goto Done;
    }
  } while (1);
  //
  // Wait for mass erase to complete
  // Read AP[1] bank 0, reg 1 (MDM-AP control register)
  // in order to check if mass erase has been completed.
  //
  r = _ReadUntilOK(1 | (1 << 2), &Data);     // Trigger the first read outside the loop to make sure that we will always get valid data inside the loop
  if (r < 0) {
    goto Done;
  }
  t = _GetTickCount() + 3000;
  do {
    r = _ReadUntilOK(1 | (1 << 2), &Data);
    if (r < 0) {
      goto Done;
    }
    if ((Data & (1 << 0)) == 0) {            // Flash mass erase in Progress bit is cleared automatically when the mass operation completes.
      break;
    }
    if ((t - _GetTickCount()) <= 0) {
      _ReportOutf("Timeout while unlocking device.\n");
      goto Done;
    }
  } while (1);
  _ReportOutf("O.K.\n");
  Stat = JLINK_NOERROR;
Done:
  JLINKARM_SetRESET();
  _Sleep(200);
  return Stat;
}

/*********************************************************************
*
*       _UnlockEFM32Gxxx
*
*  Function description
*    For the EFM32Gxxx we use a J-Link PCode for the unlock prcedure.
*    PCode execution has to be supported by the connected emulator
*    in order to use this unlock feature
*/
static int _UnlockEFM32Gxxx(const char * s) {
  PCODE_PROGRAM Program = {
    sizeof(PCODE_PROGRAM),
    {
       //
       // Select interface
       //
       S32_MOVI7(0, 1)                                                                      // Move Interface (TIF_SWD == 1) to register 0
      ,S32_PUSH(0, 1)                                                                           // Push R0 onto the stack
      ,S32_CALLH(4, 1)                                                                          // CALLH(Off = 2) which is _TIF_Sel. 1 Parameter is used
       //
       // Set speed
       //
      ,S32_MOVI32(0, 0x3D0900)                                                              // Move Speed (4000000 Hz = 4 MHz) to register 0
      ,S32_PUSH(0, 1)                                                                           // Push R0 onto the stack
      ,S32_CALLH(8, 1)                                                                          // CALLH(Off = 4) which is _TIF_SetSpeed. 1 Parameter is used
       //
       // Prepare parameters for the unlock sequence, since these registers
       // are not affected by the reset-related operations
       // This saves some time between reset release and start of sending the sequence
       // Register usage:
       // R0 Used to setup speed & interface. Furthermore used for return values of host-functions
       // R1 Reset hold time
       // R2 pDataIn for _TIF_SWD_Transfer
       // R3 pDirection for _TIF_SWD_Transfer
       // R4 pDataOut for _TIF_SWD_Transfer
       // R5 NumBits for _TIF_SWD_Transfer
       // R6 TRY_CNT_MAX
       // R7 decrement for TRY_CNT_MAX
       // R8 Pointer to first byte of IDCode
       // R9 Reference value for IDCode
       // R10 Holds IDCode pointed to by R8
       // R11 pDataIn for remaining unlock sequence
       // R12 pDirection for remaining unlock sequence
       // R13 pDataOut for remaining unlock sequence
       // R14 NumBits for remaining unlock sequence
       //
      ,S32_MOVI32(1, 1000)                                                                                                                 // Initialize counter
      ,S32_MOVI32(2, (sizeof(Program.StackPtr) + sizeof(Program.aCode)))                                                                   // Move pDataIn
      ,S32_MOVI32(3, (sizeof(Program.StackPtr) + sizeof(Program.aCode) + sizeof(Program.aDataIn)))                                         // Move pDirection
      ,S32_MOVI32(4, (sizeof(Program.StackPtr) + sizeof(Program.aCode) + sizeof(Program.aDataIn) + sizeof(Program.aDataDir)))              // Move pDataOut
      ,S32_MOVI32(5, (39 * 8))                                                                                                             // Move NumBits (39 * 8)
      ,S32_MOVI32(6, 100000)                                                                                                               // Wait for erase time: 100ms
      ,S32_MOVI32(7, 10000)                                                                                                                // Reset hold time: 10ms
      ,S32_MOVI32(8, 8 * (sizeof(Program.StackPtr) + sizeof(Program.aCode) + sizeof(Program.aDataIn) + sizeof(Program.aDataDir) + 34) + 3) // Initialize bitpos of first AAP-IDCode byte
      ,S32_MOVI32(9, 0x16E60001)                                                                                                           // Load expected IDCode value into R9
      ,S32_MOVI32(11, (sizeof(Program.StackPtr) + sizeof(Program.aCode) + 39))                                                             // Move pDataIn
      ,S32_MOVI32(12, (sizeof(Program.StackPtr) + sizeof(Program.aCode) + sizeof(Program.aDataIn) + 39))                                   // Move pDirection
      ,S32_MOVI32(13, (sizeof(Program.StackPtr) + sizeof(Program.aCode) + sizeof(Program.aDataIn) + sizeof(Program.aDataDir)))             // Move pDataOut
      ,S32_MOVI32(14, (31 * 8))                                                                                                            // Move NumBits (31 * 8). Remaining 18 bytes of the unlock sequence
      ,S32_PUSH(7, 1)                                                                                                                      // PUSH R7. Used as Para0 by CALLH(10, 1)
       //
       // Perform reset
       //
      ,S32_CALLH(2, 0)                                                                                                                     // CALLH 1 which is _TIF_ClrRESET. No parameters are used
      ,S32_CALLH(10, 1)                                                                                                                    // CALLH(Off = 5) which is _Delay_us. 1 Parameter is used
      ,S32_CALLH(0, 0)                                                                                                                     // CALLH 0 which is _TIF_SetRESET. No parameters are used
       //
       // Send SWD switching sequence with DP-ID request as well as AAP-ID request
       // Repeat sequence until we get a valid AAP-ID
       // The AAP-ID is: 0x16E60001
       //
      ,S32_PUSH(11, 4)                                                                                                                     // R14 is pushed first
      ,S32_PUSH(2, 4)                                                                                                                      // PUSH R2-R5. R5 is pushed first
//Loop
      ,S32_CALLH(6, 0)                                                                                                                     // CALLH 3 which is _TIF_SWD_Transfer. 4 Parameters are used: pDataIn, pDirection, pDataOut, NumBits
      ,S32_LOADWBITPOS(0, 8)
      ,S32_SUB(0, 9)
      ,S32_BZ(3)
       //
       // Check if operation timed out
       //
      ,S32_DBNZ(1, -5)   // Decrement timeout counter
      ,S32_MOVI7(0, 1)                                                                                                                     // Return value: AAP not found
      ,S32_BRK                                                                                                                             // End of application
       //
       // Send the remaining bits of the unlock sequence
       //
      ,S32_ADDI7_NO_FLAG(15,16)
      ,S32_CALLH(6, 4)                                                                                                                     // CALLH 3 which is _TIF_SWD_Transfer. 4 Parameters are used: pDataIn, pDirection, pDataOut, NumBits
       //
       // Send another reset pulse after 100ms (the device should be erased now)
       // to make sure that unsecure has been recongized by the core
       //
      ,S32_PUSH(6, 2)                                                                                                                      // PUSH R6-R7
      ,S32_CALLH(10, 1)                                                                                                                    // CALLH(Off = 5) which is _Delay_us. 1 Parameter is used. Wait 100ms for erase to finish
      ,S32_CALLH(2, 0)                                                                                                                     // CALLH 1 which is _TIF_ClrRESET. No parameters are used
      ,S32_CALLH(10, 1)                                                                                                                    // CALLH(Off = 5) which is _Delay_us. 1 Parameter is used
      ,S32_CALLH(0, 0)                                                                                                                     // CALLH 0 which is _TIF_SetRESET. No parameters are used
       //
       // Program execution finished
       //
      ,S32_MOVI7(0, 0)                                                                                                                     // Return value
      ,S32_BRK                                                                                                                             // End of application
    },
    {
      //
      // Erase sequence
      //
      // First sequence. Output in a loop until we see the right AAP-ID
      0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
      0x00, 0x00,                               // Make sure SWD is ready for a start bit
      0xa5, 0x00, 0x00, 0x00, 0x00, 0x00,       // Read DP-ID - init sequence ends after this
      0xa9, 0x00, 0x00, 0x00, 0x00, 0x0a,       // ctrlstat = 0x50000000
      0xb1, 0x00, 0x1E, 0x00, 0x00, 0x00,       // Write DP2: 0x000000F0: Select AP0, Bank 15
      0x9F, 0x00, 0x00, 0x00, 0x00, 0x00,       // Read AP3
      0x9F, 0x00, 0x00, 0x00, 0x00, 0x00,       // Read AP3
      // Second sequence: Erase/Unlock
      0xb1, 0x00, 0x00, 0x00, 0x00, 0x00,       // Write DP2: 0x00000000: Select AP0, Bank 0
      0x8b, 0x00, 0x23, 0x98, 0xf5, 0x39,       // Write AP1: Unlock key
      0xa3, 0x20, 0x00, 0x00, 0x00, 0x20,       // Write AP0: AAP_CMD = 1;   // Set Deverase
      0x00,                                     // Make sure write is clocked through
      0xB7, 0x00, 0x00, 0x00, 0x00, 0x00,       // Read AP2
      0xB7, 0x00, 0x00, 0x00, 0x00, 0x00        // Read AP2
    },
    {
      //
      // Direction sequence
      //
      // First sequence. Output in a loop until we see the right AAP-ID
      0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
      0xff, 0xff,                               // Make sure SWD is ready for a start bit
      0xff, 0x00 ,0x00 ,0x00, 0x00, 0xf0,       // Read DP-ID
      0xff, 0xe0, 0xff, 0xff, 0xff, 0xff,       // Write DP1: CTRL/STAT
      0xff, 0xe0, 0xff, 0xff, 0xff, 0xff,       // Write DP2: SELAP
      0xff, 0x00 ,0x00 ,0x00, 0x00, 0xf0,       // Read AP3
      0xff, 0x00 ,0x00 ,0x00, 0x00, 0xf0,       // Read AP3
      // Second sequence: Erase/Unlock
      0xff, 0xe0, 0xff, 0xff, 0xff, 0xff,       // Write DP2
      0xff, 0xe0, 0xff, 0xff, 0xff, 0xff,       // Write AP1
      0xff, 0xe0, 0xff, 0xff, 0xff, 0xff,       // Write AP0: AAP_CMD
      0xff,                                     // Make sure write is clocked through
      0xff, 0x00 ,0x00 ,0x00, 0x00, 0xf0,       // Read AP2
      0xff, 0x00 ,0x00 ,0x00, 0x00, 0xf0        // Read AP2
    },
    {0}
  };
  JLINKARM_EMU_PCODE_STATUS_INFO PCodeStat;
  int r;

  _ReportOutf("Unlocking...");
  //
  // Check if emulator has PCode execution capability
  //
  if (JLINKARM_EMU_HasCapEx(JLINKARM_EMU_CAP_EX_PCODE_EXEC) == 0) {
    _ReportOutf("Unsecure requires PCode execution feature of J-Link.\nThe connected emulator does not support PCode execution.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  r = JLINKARM_PCODE_Exec((U8*)&Program, sizeof(Program) - sizeof(Program.aStack), &PCodeStat);
  if (r != 0) {  // Error occurred before PCode execution
    if (r == JLINK_ERR_EMU_COMM_ERROR) {
      _ReportOutf("Emulator communication error.\n");
    }
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // PCode related error occurred on J-Link during download of PCode (no memory for PCode execution)
  //
  switch (PCodeStat.PCodeStat) {
  case 1:    // BRK - This is the normal way to end a PCODE
    break;
  case 2:
    _ReportOutf("Undefined instruction in PCode. PC = 0x%.8X.\n", PCodeStat.S32_PC - 2);
    return JLINK_ERROR_UNKNOWN;
  case 0xFFFFFFFF:
    _ReportOutf("Emulator has no memory for PCode execution.\n");
    return JLINK_ERROR_UNKNOWN;
  default:
    _ReportOutf("Unknown error (%d) occurred during download of PCode.\n", PCodeStat.PCodeStat);
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Return value of PCode itself. It depends on the PCode what this value means
  //
  if (PCodeStat.S32_R0) {
    switch (PCodeStat.S32_R0) {
    case 1:
      _ReportOutf("Could not find AAP. Device does not seem to be secured.\n");
      break;
    default:
      _ReportOutf("PCode reported unknown error: 0x%.8X", PCodeStat.S32_R0);
    }
    return JLINK_ERROR_UNKNOWN;
  }
  _ReportOutf("O.K.\n");
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecUnlock
*
*  Function description
*    This function unlock a device which has
*    been accidentially locked by malfunction of user software.
*/
static int _ExecUnlock(const char * s) {
  char ac[64];
  int r;

  _ParseString(&s, ac, sizeof(ac));
  //
  // Luminary LM3S unlock
  //
  if (_JLinkstricmp(ac, "LM3Sxxx") == 0) {
    r = _UnlockLM3Sxxx(s);
  } else if (_JLinkstricmp(ac, "Kinetis") == 0) {
    r = _UnlockKinetis(s);
  } else if (_JLinkstricmp(ac, "EFM32Gxxx") == 0) {
    r = _UnlockEFM32Gxxx(s);
  } else {
    //
    // If device has not been found, print a list of all supported devices.
    //
    _ReportOutf("Syntax: unlock <DeviceName>\n");
    _ReportOutf("---Supported devices---\n"
           "  LM3Sxxx [<Auto>]\n"
           "  Kinetis\n"
           "  EFM32Gxxx\n"
          );
    r = JLINK_NOERROR;
  }
  return r;
}

/*********************************************************************
*
*       _ExecTestRecover
*
*  Function description
*    Checks if the connected emulator recovers from error scenarios such as
*      - Timeout on receive (Command takes too long to execute)
*      - Wrong command
*/
static int _ExecTestRecover(const char * s) {
  int r;
  U8 aCmd[4];
  U8 aRead[4];

  static char acData[0x1000];
  const char * sErr;

  //Cmd = 0x9F;  // Unknown command
  aCmd[0] = 0x0C; // PCode exec
  aCmd[1] = 0; // SubCmd: GetCaps
  r = JLINKARM_Communicate(&aCmd[0], 2, &aRead[0], 4);
  do {
    JLINKARM_ReadMem(0, 0x1000 , &acData[0]);
  } while (1);

  JLINKARM_Close();
  sErr = JLINKARM_Open();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecRunPCode
*
*  Function description
*    Loads a pcode assembler file, assembles it and performs the PCode.
*/
static int _ExecRunPCode(const char * s) {
  JLINKARM_EMU_PCODE_STATUS_INFO PCodeStat;
  char acASMFile[256];
  U8* pTmp;
  U8* pPCode;
  U32 PCodeSize;
  int v;
  int r;
  int Ret;
  HANDLE hFile;

  Ret = JLINK_ERROR_UNKNOWN;
  pTmp      = NULL;
  pPCode    = NULL;
  //
  // Check if emulator has PCode execution capability
  //
  if (JLINKARM_EMU_HasCapEx(JLINKARM_EMU_CAP_EX_PCODE_EXEC) == 0) {
    _ReportOutf("The connected emulator does not support PCode execution.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Parse parameters (path to ASM file)
  //
  _ParseString(&s, acASMFile, sizeof(acASMFile));
  if (acASMFile[0] == 0) {
    _ReportOutf("Syntax: runpcode <ASMFile>");
    Ret = JLINK_ERROR_SYNTAX;
    goto Done;
  }
  //
  // Read file contents
  //
  hFile = _OpenFile(acASMFile, FILE_FLAG_READ | FILE_FLAG_SHARE_READ);
  if (hFile == INVALID_HANDLE_VALUE) {
    _ReportOutf("Could not open PCode ASM file.\n");
    goto Done;
  }
  v = _GetFileSize(hFile);
  pTmp = malloc(v);
  _ReadFile(hFile, pTmp, v);
  _CloseFile(hFile);
  //
  // Assemble PCode
  //
  _ReportOutf("Assembling pcode [%s]...", acASMFile);
  r = JLINKARM_PCODE_Assemble(&pPCode, &PCodeSize, pTmp, _cbErrorOut);

  if (r < 0) {
    _ReportOutf("Error while assembling PCode.\n");
    goto Done;
  }
  _ReportOutf("O.K.\n");
  //
  // Output PCode information
  //
  _ReportOutf("Segment [0] size: %d bytes\n", *(U32*)(pPCode +  8));
  //
  // Execute PCode
  //
  r = JLINKARM_PCODE_Exec(pPCode + 16, PCodeSize - 16, &PCodeStat);
  //
  // Analyse PCode status
  switch (PCodeStat.PCodeStat) {
  case 1:  _ReportOutf("O.K. PC = 0x%.8X, R0 = 0x%.8X.\n", PCodeStat.S32_PC - 2, PCodeStat.S32_R0);              Ret = JLINK_NOERROR;       break;
  case 2:  _ReportOutf("Undefined instruction at addr 0x%.8X.\n", PCodeStat.S32_PC - 2);                         Ret = JLINK_ERROR_UNKNOWN; break;
  default: _ReportOutf("Unknown error (error %d) at addr 0x%.8X.\n", PCodeStat.S32_PC - 2, PCodeStat.PCodeStat); Ret = JLINK_ERROR_UNKNOWN; break;
  }
Done:
  if (pTmp) {
    free(pTmp);
  }
  if (pPCode) {
    JLINKARM_FreeMem(pPCode);
  }
  return Ret;
}

/*********************************************************************
*
*       _ExecTestPCode
*
*  Function description
*    Checks PCode performance.
*/
static int _ExecTestPCode(const char * s) {
  typedef struct {
    U32 StackPtr;
    U16 aCode[100];
  } PCODE_TEST_PROG;

  PCODE_TEST_PROG ProgramDBNZ = {
    sizeof(PCODE_TEST_PROG) + 32,  // Reserve 32 bytes stack
    {
      S32_MOVI32(0, 2000000),
      S32_DBNZ(0, -1),
      S32_BRK
    }
  };

  PCODE_TEST_PROG ProgramDEC = {
    sizeof(PCODE_TEST_PROG) + 32,  // Reserve 32 bytes stack
    {
      S32_MOVI32(0, 1000000),
      S32_SUBI4(0, 1),
      S32_BNZ(-2),
      S32_BRK
    }
  };

  PCODE_TEST_PROG ProgramCALLH = {
    sizeof(PCODE_TEST_PROG) + 32,  // Reserve 32 bytes stack
    {
      S32_MOVI32(1, 500000),
      S32_CALLH(18, 0),                                                                         // CALLH(Off = 9) which is _GetTimeMS
      S32_DBNZ(1, -2),
      S32_BRK
    }
  };
  PCODE_TEST_PROG ProgramDelayus = {
    sizeof(PCODE_TEST_PROG),  // Stack area is part of PCode to be downloaded, so at startup, stackpointer should point to the first location after the PCode.
    {
      S32_MOVI32(1, 1000000),  // Counter for HW_Delay_us: Wait 1 second
      S32_PUSH(1, 1),
      //
      // Wait until a systick happens
      //
      S32_CALLH(18, 0),       // _GetTimeMS
      S32_MOV(2, 0, 0),       // Remember systick reference value
      S32_CALLH(18, 0),       // _GetTimeMS
      S32_SUB(0, 2),
      S32_BZ(-3),             // Wait until a systick occurred.
      //
      // Wait 1000 ms using HW_Delay_us()
      // to check if routine is implemented correctly
      //
      S32_CALLH(10, 1),       // HW_Delay_us(int Delayus), pop Delayus automatically
      S32_CALLH(18, 0),       // _GetTimeMS => OS_Time is in R0
      S32_ADDI4(2, 1),        // Increment systick reference value since at startup we waited until 1 systick occurred
      S32_SUB(0, 2),
      S32_BRK
    }
  };

  JLINKARM_EMU_PCODE_STATUS_INFO PCodeStat;
  int r;
  int t;
  U32 Caps;
  U32 Version;
  //
  // Check if emulator has PCode execution capability
  //
  if (JLINKARM_EMU_HasCapEx(JLINKARM_EMU_CAP_EX_PCODE_EXEC) == 0) {
    _ReportOutf("The connected emulator does not support PCode execution.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  r = JLINKARM_PCODE_GetCaps(&Caps);
  if (r < 0) {
    return JLINK_ERROR_UNKNOWN;
  }
  r = JLINKARM_PCODE_GetS32Version(&Version);
  if (r < 0) {
    return JLINK_ERROR_UNKNOWN;
  }
  _ReportOutf("  Emulator supports PCODE. S32 Version: %d.%d\n", Version / 100, Version % 100);
  if (Caps) {
    if (Caps & (1 << 1)) { _ReportOutf("    GetAPIList\n"); }
    if (Caps & (1 << 2)) { _ReportOutf("    Download\n");   }
    if (Caps & (1 << 3)) { _ReportOutf("    Exec\n");       }
    if (Caps & (1 << 4)) { _ReportOutf("    Step\n");       }
    if (Caps & (1 << 5)) { _ReportOutf("    GetRegs\n");    }
    if (Caps & (1 << 6)) { _ReportOutf("    SetRegs\n");    }
  }
  _ReportOutf("  Checking PCode performance:\n");
  //
  // Check DBNZ Performance
  //
  t = _GetTickCount();
  r = JLINKARM_PCODE_Exec((U8*)&ProgramDBNZ, sizeof(ProgramDBNZ), &PCodeStat);
  t = _GetTickCount() - t;
  if (t) {
    t = 2000000 / t;
    _ReportOutf("    DBNZ : %dk Loops/Sec.\n", t);
  }
  //
  // Check DEC Performance
  //
  t = _GetTickCount();
  r = JLINKARM_PCODE_Exec((U8*)&ProgramDEC, sizeof(ProgramDEC), &PCodeStat);
  t = _GetTickCount() - t;
  if (t) {
    t = 1000000 / t;
    _ReportOutf("    SUB  : %dk Loops/Sec.\n", t);
  }
  //
  // Check CALLH Performance
  //
  t = _GetTickCount();
  r = JLINKARM_PCODE_Exec((U8*)&ProgramCALLH, sizeof(ProgramCALLH), &PCodeStat);
  t = _GetTickCount() - t;
  if (t) {
    t = 500000 / t;
    _ReportOutf("    CALLH: %dk Loops/Sec.\n", t);
  }
  //
  // Check functions implementation
  //
  _ReportOutf("  Checking implementation of J-Link functions:\n");
  //
  // Check implementation of HW_Delay_us()
  //
  _ReportOutf("    HW_Delay_us(1000000 us)...");
  if ((Version / 100) >= 2) {
    r = JLINKARM_PCODE_Exec((U8*)&ProgramDelayus, sizeof(ProgramDelayus), &PCodeStat);
    _ReportOutf("O.K.: %d Systicks (ms) elapsed.\n", PCodeStat.S32_R0);
  } else {
    _ReportOutf("ERROR: Min. PCode V2.0 required. Current version: V%d.%d\n", Version / 100, Version % 100);
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteTestPattern
*/
static int _ExecWriteTestPattern(const char* s) {
  JLINKARM_WriteMem(_RAMAddr + 1, 14, "1234567890ABCDEF");
  _ReportOutf("Writing Test pattern\n");
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecReset
*/
static int _ExecReset(const char* s) {
  _Reset(0);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecResetEx
*/
static int _ExecResetEx(const char* s) {
  U32 Delay;
  if (_ParseDec(&s, &Delay)) {
    _ReportOutf("Syntax: rx <DelayAfterReset>\n");
    return JLINK_ERROR_SYNTAX;
  }
  JLINKARM_SetInitRegsOnReset(0);
  _Reset(Delay);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecResetNoHalt
*/
static int _ExecResetNoHalt(const char* s) {
  JLINKARM_ResetNoHalt();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSelDevice
*/
static int _ExecSelDevice(const char* s) {
  U32 DevIndex;
  if (_ParseDec(&s, &DevIndex)) {
    _ReportOutf("Syntax: sel <DevIndex>\n");
    return JLINK_ERROR_SYNTAX;
  }
  JLINKARM_SelDevice((U16)DevIndex);
  _ReportOutf("Selected device with JTAG index %d\n", DevIndex);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSetSpeed
*/
static int _ExecSetSpeed(const char* s) {
  U32 Speed;
  int r;
  COMMANDER_SETTINGS* pCommanderSettings;
  
  pCommanderSettings = &_CommanderSettings;
  r = JLINK_NOERROR;
  _EatWhite(&s);
  if ((_CompareCmd(&s, "adaptive") == 0) || (_CompareCmd(&s, "a") == 0)) {
    JLINKARM_SPEED_INFO SpeedInfo;
    SpeedInfo.SizeOfStruct = sizeof(SpeedInfo);
    JLINKARM_GetSpeedInfo(&SpeedInfo);
    if (SpeedInfo.SupportAdaptive == 0) {
      _ReportOutf("The connected J-Link does not support adaptive clocking.\n");
      return JLINK_ERROR_UNKNOWN;
    }
    Speed = JLINKARM_SPEED_ADAPTIVE;
  } else if (_CompareCmd(&s, "auto") == 0) {
    Speed = JLINKARM_SPEED_AUTO;
  } else {
    if (_ParseDec(&s, &Speed)) {
      _ReportOutf("Syntax: speed <freq>|auto|adaptive\n");
      return JLINK_ERROR_SYNTAX;
    }
    if (Speed != JLINKARM_SPEED_ADAPTIVE) {   // If adaptive speed has been selected as numeric value (65535), we should not perform this max. speed check.
      if (Speed > MAX_TIF_SPEED_KHZ_SUPPORTED) {
        _ReportOutf("Maximum target interface speed is %d kHz.\n", MAX_TIF_SPEED_KHZ_SUPPORTED);
        r = JLINK_ERROR_WARNING;
        Speed = MAX_TIF_SPEED_KHZ_SUPPORTED;
      }
    }
  }
  pCommanderSettings->InitTIFSpeedkHz    = Speed;
  pCommanderSettings->InitTIFSpeedkHzSet = 1;
  JLINKARM_SetSpeed(Speed);
  _ShowCurrentSpeed();
  return r;
}

/*********************************************************************
*
*       _ExecSetPC
*/
static int _ExecSetPC(const char* s) {
  U32 Addr;
  U32 DevFamily;
  int i;

  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: SetPC <addr>\n");
    return JLINK_ERROR_SYNTAX;
  }
  if (JLINKARM_IsHalted()) {
    DevFamily = JLINKARM_GetDeviceFamily();
    for (i = 0; i < COUNTOF(_aTblDevInfo); i++) {
      if (_aTblDevInfo[i].DevFamily == DevFamily) {
        JLINKARM_WriteReg(_aTblDevInfo[i].iRegPC, Addr);
        break;
      }
    }
  } else {
    _ReportOutf("CPU is not halted !\n");
    return JLINK_ERROR_WARNING;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecClrBP
*/
static int _ExecClrBP(const char* s) {
  int BPHandle;
  if (_ParseDec(&s, &BPHandle)) {
    _ReportOutf("Syntax: ClrBP <BP_Handle>\n");
    return JLINK_ERROR_SYNTAX;
  }
  if (JLINKARM_ClrBPEx(BPHandle)) {
    _ReportOutf("Could not clear breakpoint.\n");
  } else {
    _ReportOutf("Breakpoint cleared\n");
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecMeasureSCLen
*/
static int _ExecMeasureSCLen(const char* s) {
  U32 SCLen, ScanChain;
  if (_ParseDec(&s, &ScanChain)) {
    _ReportOutf("Syntax: ms <scan chain>\n");
    return JLINK_ERROR_SYNTAX;
  }
  SCLen = JLINKARM_MeasureSCLen(ScanChain);
  _ReportOutf("Length of scan chain [%d] = %d\n", ScanChain, SCLen);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecMeasureRTCKReactTime
*/
static int _ExecMeasureRTCKReactTime(const char* s) {
  U32 RepCount = 1;
  int r;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseDec(&s, &RepCount)) {
      return JLINK_ERROR_SYNTAX;
    }
  }
  while (RepCount--) {
    JLINKARM_RTCK_REACT_INFO Info;
    Info.SizeOfStruct = sizeof(Info);
    r = JLINKARM_MeasureRTCKReactTime(&Info);
    if (r == 0) {
      _ReportOutf("Min: %dns, Max: %dns, Average: %dns\n", Info.Min, Info.Max, Info.Average);
    } else if (r == -1) {
      _ReportOutf("RTCK did not react\n");
    }
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecReadCP15
*/
static int _ExecReadCP15(const char* s) {
  U32 RegIndex, Data;
  int r;
  if (_ParseHex(&s, &RegIndex)) {
    _ReportOutf("Syntax: rcp15 <RegIndex>\n");
    return JLINK_ERROR_SYNTAX;
  }
  r = JLINKARM_CP15_ReadReg(RegIndex, &Data);
  if (r == 0) {
    _ReportOutf("CP15[%.2X] = 0x%.8X\n", RegIndex, Data);
  } else {
    _ReportOutf("Read failed\n");
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecReadCP15Ex
*/
static int _ExecReadCP15Ex(const char* s) {
  U32 CRn, CRm, Op1, Op2;
  U32 Data;
  int r;
  if (_ParseDec(&s, &Op1)) {
    _ReportOutf("Syntax: rcp15ex <Op1>, <CRn>, <CRm>, <Op2>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseDec(&s, &CRn)) {
    _ReportOutf("Syntax: rcp15ex <Op1>, <CRn>, <CRm>, <Op2>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseDec(&s, &CRm)) {
    _ReportOutf("Syntax: rcp15ex <Op1>, <CRn>, <CRm>, <Op2>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseDec(&s, &Op2)) {
    _ReportOutf("Syntax: rcp15ex <Op1>, <CRn>, <CRm>, <Op2>\n");
    return JLINK_ERROR_SYNTAX;
  }
  r = JLINKARM_CP15_ReadEx((U8)CRn, (U8)CRm, (U8)Op1, (U8)Op2, &Data);
  if (r == 0) {
    _ReportOutf("CP15[%d, %d, %d, %d] = 0x%.8X\n", Op1, CRn, CRm, Op2, Data);
  } else {
    _ReportOutf("Read failed\n");
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteCP15
*/
static int _ExecWriteCP15(const char* s) {
  U32 RegIndex, Data;
  if (_ParseHex(&s, &RegIndex)) {
    _ReportOutf("Syntax: wcp15 <RegIndex>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  s++;
  if (_ParseHex(&s, &Data)) {
    _ReportOutf("Syntax: wcp15 <RegIndex>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _ReportOutf("CP15[%.2X] = 0x%.8X\n", RegIndex, Data);
  JLINKARM_CP15_WriteReg(RegIndex, Data);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteCP15Ex
*/
static int _ExecWriteCP15Ex(const char* s) {
  U32 CRn, CRm, Op1, Op2;
  U32 Data;
  int r;
  if (_ParseDec(&s, &Op1)) {
    _ReportOutf("Syntax: wcp15ex <Op1>, <CRn>, <CRm>, <Op2>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseDec(&s, &CRn)) {
    _ReportOutf("Syntax: wcp15ex <Op1>, <CRn>, <CRm>, <Op2>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseDec(&s, &CRm)) {
    _ReportOutf("Syntax: wcp15ex <Op1>, <CRn>, <CRm>, <Op2>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseDec(&s, &Op2)) {
    _ReportOutf("Syntax: wcp15ex <Op1>, <CRn>, <CRm>, <Op2>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &Data)) {
    _ReportOutf("Syntax: wcp15ex <Op1>, <CRn>, <CRm>, <Op2>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  r = JLINKARM_CP15_WriteEx((U8)CRn, (U8)CRm, (U8)Op1, (U8)Op2, Data);
  _ReportOutf("CP15[%d, %d, %d, %d] = 0x%.8X\n", Op1, CRn, CRm, Op2, Data);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecExecCommand
*/
static int _ExecExecCommand(const char* s) {
  char ac[4000];
  ac[0] = 0;
  JLINKARM_ExecCommand(s, &ac[0], sizeof(ac));
  _ReportOutf(ac);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecDevice
*/
static int _ExecDevice(const char* s) {
  int r;
  unsigned WasConnectedToTarget;
  COMMANDER_SETTINGS* pCommanderSettings;

  pCommanderSettings = &_CommanderSettings;
  _EatWhite(&s);
  if (*s == '=') {
    s++;
    _EatWhite(&s);
  }
  //
  // No device given -> Show help
  //
  if (*s == 0) {
    _ReportOutf("Syntax: device <DeviceName>\n"
           "        Type \"device ?\" to show the device selection dialog.\n"
          );
    return JLINK_NOERROR;
  }
  //
  // Update commander settings
  //
  strcpy(pCommanderSettings->acDeviceName, s);
  //
  // Disconnect and re-connect to target
  //
  WasConnectedToTarget = pCommanderSettings->Status.ConnectedToTarget;
  if (WasConnectedToTarget) {
    _DisconnectFromTarget(pCommanderSettings);
  }
  r = _ConnectToTarget(pCommanderSettings);
  if (r == 0) {
    _ShowMemZones();
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecPower
*/
static int _ExecPower(const char* s) {
  U32 OnOff = 1;
  char ac[4000];

  _EatWhite(&s);
  if(_CompareCmd(&s, "On") == 0) {
    JLINKARM_ExecCommand("SupplyPower = 1", &ac[0], sizeof(ac));
    _EatWhite(&s);
    if(_CompareCmd(&s, "perm") == 0) {
      JLINKARM_ExecCommand("SupplyPowerDefault = 1", &ac[0], sizeof(ac));
    }
  } else if(_CompareCmd(&s, "Off") == 0) {
    JLINKARM_ExecCommand("SupplyPower = 0", &ac[0], sizeof(ac));
    _EatWhite(&s);
    if(_CompareCmd(&s, "perm") == 0) {
      JLINKARM_ExecCommand("SupplyPowerDefault = 0", &ac[0], sizeof(ac));
    }
  } else {
    _ReportOutf("Syntax: power <State> [perm]\n");
    return JLINK_ERROR_SYNTAX;
  }

  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTestHaltGo
*/
static int _ExecTestHaltGo(const char* s) {
  int i;
  int TickCnt = _GetTickCount();
  _ReportOutf("test halt/go \n");
  if (JLINKARM_IsHalted()) {
    JLINKARM_Go();
  }
  for (i = 0; i < 1000; i++) {
    if (JLINKARM_Halt()) {
      break;
    }
    _ReportOutf(".");
    JLINKARM_Go();
  }
  TickCnt = _GetTickCount() - TickCnt;
  _ReportOutf("\nCompleted. %dms required\n", TickCnt );
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTestSingleStep
*/
static int _ExecTestSingleStep(const char* s) {
  int i;
  int TickCnt = _GetTickCount();
  _ReportOutf("Test single step");
  JLINKARM_Halt();
  for (i = 0; i < 1000; i++) {
    JLINKARM_Step();
    _ReportOutf(".");
  }
  TickCnt = _GetTickCount() - TickCnt;
  _ReportOutf("\nCompleted. %dms required\n", TickCnt );
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSetRAMAddr
*/
static int _ExecSetRAMAddr(const char* s) {
  char acIn [128];
  char acOut[128];
  int r = JLINK_NOERROR;
  if (_ParseHex(&s, &_RAMAddr)) {
    _ReportOutf("Syntax: ram <RAMAddr>(hex)\n");
    _RAMAddr = DEFAULT_RAMADDR;
    r = JLINK_ERROR_SYNTAX;
  }
  sprintf(acIn, "map ram 0x%.8X - 0x%.8X", _RAMAddr, _RAMAddr + 0x100 - 1);
  JLINKARM_ExecCommand(acIn, acOut, sizeof(acOut));
  return r;
}

/*********************************************************************
*
*       _ExecTestRAM
*/
static int _ExecTestRAM(const char* s) {
  U32 Addr;
  U32 ExitOnError;
  U32 Pattern;
  U8* pDataIn;
  U8* pDataOut;
  int c;
  int i;
  int n;
  int NumBytes;
  int NumCycles;
  int r;
  int Percent;

  NumCycles   = 2;
  ExitOnError = 0;
  //
  // Parse options
  //
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: TestRAM <StartAddr (hex)>, <NumBytes (hex)>, [<NumCycles>], [<ExitOnError>]\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &NumBytes)) {
    _ReportOutf("Syntax: TestRAM <StartAddr (hex)>, <NumBytes (hex)>, [<NumCycles>], [<ExitOnError>]\n");
    return JLINK_ERROR_SYNTAX;
  }
  n = (NumBytes + NumBytes%4)/4;
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  //
  // Parse NumCycles if necessary
  //
  if (*s) {
    if (_ParseDec(&s, &NumCycles)) {
      _ReportOutf("Syntax: TestRAM <StartAddr (hex)>, <NumBytes (hex)>, [<NumCycles>], [<ExitOnError>]\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  //
  // Parse ExitOnError if necessary
  //
  if (*s) {
    if (_ParseDec(&s, &ExitOnError)) {
      _ReportOutf("Syntax: TestRAM <StartAddr (hex)>, <NumBytes (hex)>, [<NumCycles>], [<ExitOnError>]\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  //
  // Halt target for RAM test
  //
  _ReportOutf("Halting target for RAM test...");
  r = JLINKARM_Halt();  
  if (r) {
    return JLINK_ERROR_SYNTAX;
  }
  _ReportOutf("O.K.\n");
  //
  // Allocate buffers for RAM test
  //
  pDataIn  = (U8*)malloc(n * 4);                         // Make sure Buffer size is a multiple of 4, because CRC returns 4 bytes
  pDataOut = (U8*)malloc(n * 4);
  Pattern  = 0xAAAAAAAA;                                 // Pattern for CRC generation we start with
  //
  // Start RAM check
  //
  _ReportOutf("Starting RAM check @0x%.8X, 0x%.8X bytes, %d cycles.\n", Addr, NumBytes, NumCycles);
  _ReportOutf("Checking RAM...[000%%]");
  c = 1;
  do {
    Percent = (c * 100) / NumCycles;
    _ReportOutf("\b\b\b\b\b\b");
    _ReportOutf("[%3d%%]", Percent);
    if(c & 1) {
      //
      // Fill buffer with test pattern
      //
      i = 0;
      do {
        Pattern = _CRC_Calc32Fast((U8*)&Pattern, 4, 0);
        memcpy(pDataIn + (i * 4), &Pattern, 4);
        i++;
      } while(i < n);
    } else {
      //
      // Invert test pattern
      //
      i = 0;
      do {
        *(pDataIn + i) = ~(*(pDataIn + i));
        i++;
      } while(i < n);
    }
    //
    // Write & verify test pattern
    //
    r = JLINKARM_WriteMemEx(Addr, NumBytes, pDataIn, 0);
    if (r != NumBytes) {
      _ReportOutf("RAM check Failed.\n");
      r = JLINK_ERROR_UNKNOWN;
      goto Cleanup;
    }
    r = JLINKARM_ReadMemEx(Addr, NumBytes, pDataOut, 0);
    if((r != NumBytes) || memcmp(pDataIn, pDataOut, NumBytes)) {
      _ReportOutf("RAM check Failed.\n");
      r = JLINK_ERROR_UNKNOWN;
      goto Cleanup;
    }
    c++;
  } while(c <= NumCycles);
  _ReportOutf("\b\b\b\b\b\bdone. \n");
  r = JLINK_NOERROR;
Cleanup:  
  free(pDataIn);
  free(pDataOut);
  return r;
}


/*********************************************************************
*
*       _ExecWriteICEReg
*/
static int _ExecWriteICEReg(const char* s) {
  U32 Addr, Data;
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: wi <Addr>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  s++;
  if (_ParseHex(&s, &Data)) {
    _ReportOutf("Syntax: wi <Addr>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _ReportOutf("Icereg[%.8X] = %.8X\n", Addr, Data);
  JLINKARM_WriteICEReg(Addr, Data, 0);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecReadICEReg
*/
static int _ExecReadICEReg(const char* s) {
  U32 Addr, Data;
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: wi <Addr>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  Data = JLINKARM_ReadICEReg(Addr);
  _ReportOutf("Icereg[%.8X] = %.8X\n", Addr, Data);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteJTAGCommand
*/
static int _ExecWriteJTAGCommand(const char* s) {
  U32 Cmd;
  if (_ParseHex(&s, &Cmd)) {
    _ReportOutf("Syntax: wjc <Cmd(hex)>\n");
    return JLINK_ERROR_SYNTAX;
  }
  JLINKARM_JTAG_StoreInst((U8*)&Cmd, 4);
  JLINKARM_JTAG_SyncBits();
  _ReportOutf("Command 0x%X (%s) successfully written\n", Cmd, _Cmd2String((U8)Cmd));
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteJTAGData
*/
static int _ExecWriteJTAGData(const char* s) {
  U64 Data;
  int NumBits;
  int BitPos;
  U8  ac[8];
  if (_ParseHex64(&s, &Data)) {
    _ReportOutf("Syntax: wjd <Data(hex)>, <NumBits(dec)>\n");
    return JLINK_ERROR_SYNTAX;
  }
  s++;
  if (_ParseDec(&s, &NumBits)) {
    _ReportOutf("Syntax: wjd <Data(hex)>, <NumBits(dec)>\n");
    return JLINK_ERROR_SYNTAX;
  }
  ac[0] = (U8) (Data & 255);
  ac[1] = (U8)((Data >> 8) & 255);
  ac[2] = (U8)((Data >> 16) & 255);
  ac[3] = (U8)((Data >> 24) & 255);
  ac[4] = (U8)((Data >> 32) & 255);
  ac[5] = (U8)((Data >> 40) & 255);
  ac[6] = (U8)((Data >> 48) & 255);
  ac[7] = (U8)((Data >> 56) & 255);
  BitPos = JLINKARM_JTAG_StoreData(ac, NumBits);
  JLINKARM_JTAG_SyncBits();

  _Var = JLINKARM_GetU32(BitPos);
  if (NumBits > 32) {
    _Var |= (U64)JLINKARM_GetU32(BitPos + 32) << 32;
  }
  if        (NumBits < 32) {
    _Var &= (1U << NumBits) - 1;
  } else if (NumBits < 64) {
    U64 Mask;
    Mask   = ~(0);
    Mask >>= (64 - NumBits);
    _Var  &= Mask;
  }
#ifdef WIN32
  _ReportOutf("returns 0x%I64X\n", _Var);
#else
  _ReportOutf("returns 0x%llX\n", _Var);
#endif
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecReadReg
*/
static int _ExecReadReg(const char* s) {
  U32 RegIndex;
  U32 Data;

  if (_ParseDec(&s, &RegIndex)) {
    _ReportOutf("Syntax: rreg <RegIndex>\n");
    return JLINK_ERROR_SYNTAX;
  }
  Data = JLINKARM_ReadReg(RegIndex);
  _ReportOutf("%s = 0x%.8X\n", JLINKARM_GetRegisterName(RegIndex), Data);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteReg
*/
static int _ExecWriteReg(const char* s) {
  const char * sReg;
  char ac[32];
  U32 RegIndex;
  U32 Data;
  int NumRegs;
  int i;

  _ParseString(&s, ac, sizeof(ac));
  NumRegs = _GetNumRegs();
  for (i = 0; i < NumRegs; i++) {
    sReg = JLINKARM_GetRegisterName(i);
    if (_JLinkstricmp(ac, sReg) == 0) {
      RegIndex = i;
      goto FoundReg;
    } 
  }
  _ReportOutf("Illegal register name.\nSyntax: wreg <RegName>, <Data>\n\n");
  for (i = 0; i < NumRegs; i++) {
    sReg = JLINKARM_GetRegisterName(i);
    _ReportOutf("%s\n", sReg);
  }
  return JLINK_ERROR_SYNTAX;

FoundReg:
  _EatWhite(&s);
  if ((*s == ',') || (*s == '=')) {
    s++;
  }
  if (_ParseHex(&s, &Data)) {
    _ReportOutf("Syntax: wreg <RegName>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _ReportOutf("%s = 0x%.8X\n", sReg, Data);
  JLINKARM_WriteReg(RegIndex, Data);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecGetDebugInfo
*/
static int _ExecGetDebugInfo(const char* s) {
  U32 Index, Data;
  int r;

  r = JLINK_NOERROR;
  if (_ParseHex(&s, &Index)) {
    _ReportOutf("Syntax: GetDebugInfo <Index>\n");
    return JLINK_ERROR_SYNTAX;
  }
  r = JLINKARM_GetDebugInfo(Index, &Data);
  if (r == 0) {
    _ReportOutf("Index 0x%.4X = 0x%.8X\n", Index, Data);
  } else {
    _ReportOutf("No debug information for index 0x%.4X available on this CPU.\n", Index);
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecReadMem
*/
static int _ExecReadMem(const char* s) {
  U32 Addr, NumBytes;
  U8* pData0;
  U8* pData;
  char acZone[32];
  int ReadResult;
  int r;

  r = JLINK_NOERROR;
  _TryParseMemZone(&s, &acZone[0], sizeof(acZone));
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: mem <Addr>, <NumBytes>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &NumBytes)) {
    _ReportOutf("Syntax: mem <Addr>, <NumBytes>\n");
    return JLINK_ERROR_SYNTAX;
  }
  if (NumBytes > 0x100000) {
    _ReportOutf("NumBytes should be <= 0x100000\n");
    return JLINK_ERROR_UNKNOWN;
  }
  pData0 = pData = malloc(NumBytes);
  //
  // Read memory zoned, if necessary
  //
  if (acZone[0]) {
    ReadResult = JLINK_ReadMemZonedEx(Addr, NumBytes, pData, 0, acZone);
    if (ReadResult == (int)NumBytes) {
      ReadResult = 0;
    }
  } else {
    ReadResult = JLINKARM_ReadMem(Addr, NumBytes, pData);
  }
  if (ReadResult != 0) {
    _ReportOutf("Could not read memory.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  if (JLINKARM_HasError() == 0) {
    while (NumBytes > 0) {
      int NumBytesPerLine;
      _ReportOutf("%.8X = ", Addr);
      NumBytesPerLine = (NumBytes > 16) ? 16 : NumBytes;
      NumBytes -= NumBytesPerLine;
      Addr     += NumBytesPerLine;
      for (; NumBytesPerLine > 0; NumBytesPerLine--) {
        _ReportOutf("%.2X ", *pData++);
      }
      _ReportOutf("\n");
    }
  }
Done:
  free(pData0);
  return r;
}

/*********************************************************************
*
*       _ExecReadMem8
*/
static int _ExecReadMem8(const char* s) {
  U32 Addr, NumBytes;
  U8* pData0;
  U8* pData;
  int r;
  char acZone[32];

  r = JLINK_NOERROR;
  _TryParseMemZone(&s, &acZone[0], sizeof(acZone));
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: mem8 <Addr>, <NumBytes>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &NumBytes)) {
    _ReportOutf("Syntax: mem8 <Addr>, <NumBytes>\n");
    return JLINK_ERROR_SYNTAX;
  }
  if (NumBytes > 0x10000) {
    _ReportOutf("NumBytes should be <= 0x10000\n");
    return JLINK_ERROR_UNKNOWN;
  }
  pData0 = pData = malloc(NumBytes);
  //
  // Read memory zoned, if necessary
  //
  if (acZone[0]) {
    r = JLINK_ReadMemZonedEx(Addr, NumBytes, pData, 1, acZone);
  } else {
    r = JLINKARM_ReadMemU8(Addr, NumBytes, pData, NULL);
  }
  if (r < 0) {
    _ReportOutf("Could not read memory.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  if (JLINKARM_HasError() == 0) {
    NumBytes = (U32)r;
    while (NumBytes > 0) {
      int NumBytesPerLine;
      _ReportOutf("%.8X = ", Addr);
      NumBytesPerLine = (NumBytes > 16) ? 16 : NumBytes;
      NumBytes -= NumBytesPerLine;
      Addr     += NumBytesPerLine;
      for (; NumBytesPerLine > 0; NumBytesPerLine--) {
        _ReportOutf("%.2X ", *pData++);
      }
      _ReportOutf("\n");
    }
  }
Done:
  free(pData0);
  return r;
}

/*********************************************************************
*
*       _ExecReadMem16
*/
static int _ExecReadMem16(const char* s) {
  U32 Addr, NumItems;
  U16* pData0;
  U16* pData;
  int r;
  char acZone[32];

  r = JLINK_NOERROR;
  _TryParseMemZone(&s, &acZone[0], sizeof(acZone));
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: mem16 <Addr>, <NumItems>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &NumItems)) {
    _ReportOutf("Syntax: mem16 <Addr>, <NumItems>\n");
    return JLINK_ERROR_SYNTAX;
  }
  if (NumItems > 0x8000) {
    _ReportOutf("NumItems should be <= 0x8000\n");
    return JLINK_ERROR_UNKNOWN;
  }
  pData0 = pData = malloc(NumItems * sizeof(U16));
  //
  // Read memory zoned, if necessary
  //
  if (acZone[0]) {
    r = JLINK_ReadMemZonedEx(Addr, NumItems << 1, pData, 2, acZone);
    if (r > 0) {
      r >>= 1;
    }
  } else {
    r = JLINKARM_ReadMemU16(Addr, NumItems, pData, NULL);
  }
  if (r < 0) {
    _ReportOutf("Could not read memory.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  if (JLINKARM_HasError() == 0) {
    NumItems = (U32)r;
    while (NumItems > 0) {
      int NumItemsPerLine;
      _ReportOutf("%.8X = ", Addr);
      NumItemsPerLine = (NumItems > 8) ? 8 : NumItems;
      NumItems -= NumItemsPerLine;
      Addr     += NumItemsPerLine * 2;
      for (; NumItemsPerLine > 0; NumItemsPerLine--) {
        _ReportOutf("%.4X ", *pData++);
      }
      _ReportOutf("\n");
    }
  }
Done:
  free(pData0);
  return r;
}

/*********************************************************************
*
*       _ExecReadMem32
*/
static int _ExecReadMem32(const char* s) {
  U32 Addr, NumItems;
  U32* pData0;
  U32* pData;
  int r;
  char acZone[32];

  r = JLINK_NOERROR;
  _TryParseMemZone(&s, &acZone[0], sizeof(acZone));
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: mem32 <Addr>, <NumItems>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &NumItems)) {
    _ReportOutf("Syntax: mem32 <Addr>, <NumItems>\n");
    return JLINK_ERROR_SYNTAX;
  }
  if (NumItems > 0x4000) {
    _ReportOutf("NumItems should be <= 0x4000\n");
    return JLINK_ERROR_UNKNOWN;
  }
  pData0 = pData = malloc(NumItems * sizeof(U32));
  //
  // Read memory zoned, if necessary
  //
  if (acZone[0]) {
    r = JLINK_ReadMemZonedEx(Addr, NumItems << 2, pData, 4, acZone);
    if (r > 0) {
      r >>= 2;
    }
  } else {
    r = JLINKARM_ReadMemU32(Addr, NumItems, pData, NULL);
  }
  if (r < 0) {
    _ReportOutf("Could not read memory.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  if (JLINKARM_HasError() == 0) {
    NumItems = (U32)r;
    while (NumItems > 0) {
      int NumItemsPerLine;
      _ReportOutf("%.8X = ", Addr);
      NumItemsPerLine = (NumItems > 4) ? 4 : NumItems;
      NumItems -= NumItemsPerLine;
      Addr     += NumItemsPerLine * 4;
      for (; NumItemsPerLine > 0; NumItemsPerLine--) {
        _ReportOutf("%.8X ", *pData++);
      }
      _ReportOutf("\n");
    }
  }
Done:
  free(pData0);
  return r;
}

/*********************************************************************
*
*       _ExecReadMem64
*/
static int _ExecReadMem64(const char* s) {
  U32 Addr, NumItems;
  U64* pData0;
  U32* pData;
  int r;
  char acZone[32];

  r = JLINK_NOERROR;
  _TryParseMemZone(&s, &acZone[0], sizeof(acZone));
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: mem64 <Addr>, <NumItems>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &NumItems)) {
    _ReportOutf("Syntax: mem64 <Addr>, <NumItems>\n");
    return JLINK_ERROR_SYNTAX;
  }
  if (NumItems > 0x4000) {
    _ReportOutf("NumItems should be <= 0x4000\n");
    return JLINK_ERROR_UNKNOWN;
  }
  pData0 = malloc(NumItems * sizeof(U64));
  //
  // Read memory zoned, if necessary
  //
  if (acZone[0]) {
    r = JLINK_ReadMemZonedEx(Addr, NumItems << 3, pData0, 8, acZone);
    if (r > 0) {
      r >>= 3;
    }
  } else {
    r = JLINKARM_ReadMemU64(Addr, NumItems, pData0, NULL);
  }
  if (r < 0) {
    _ReportOutf("Could not read memory.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  if (JLINKARM_HasError() == 0) {
    pData = (U32*)pData0;
    NumItems = (U32)r;
    while (NumItems > 0) {
      int NumItemsPerLine;
      _ReportOutf("%.8X = ", Addr);
      NumItemsPerLine = (NumItems > 2) ? 2 : NumItems;
      NumItems -= NumItemsPerLine;
      Addr     += NumItemsPerLine * 8;
      for (; NumItemsPerLine > 0; NumItemsPerLine--) {
        _ReportOutf("%.8X%.8X ", *(pData + 1), *pData);
        pData += 2;
      }
      _ReportOutf("\n");
    }
  }
Done:
  free(pData0);
  return r;
}

/*********************************************************************
*
*       _ExecReadMemIndirect
*/
static int _ExecReadMemIndirect(const char* s) {
  U32 Addr, NumBytes;
  U8* pData0;
  U8* pData;
  int r;

  r = JLINK_NOERROR;
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: memi <Addr>, <NumBytes>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &NumBytes)) {
    _ReportOutf("Syntax: memi <Addr>, <NumBytes>\n");
    return JLINK_ERROR_SYNTAX;
  }
  if (NumBytes > 0x10000) {
    _ReportOutf("NumBytes should be <= 0x10000\n");
    return JLINK_ERROR_UNKNOWN;
  }
  if (JLINKARM_IsHalted() == 0) {
    _ReportOutf("Halting CPU in order to read memory...\n");
    JLINKARM_Halt();
  }
  pData0 = pData = malloc(NumBytes);
  if (JLINKARM_ReadMemIndirect(Addr, NumBytes, pData) != (int)NumBytes) {
    _ReportOutf("Could not read memory.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  if (JLINKARM_HasError() == 0) {
    while (NumBytes > 0) {
      int NumBytesPerLine;
      _ReportOutf("%.8X = ", Addr);
      NumBytesPerLine = (NumBytes > 16) ? 16 : NumBytes;
      NumBytes -= NumBytesPerLine;
      Addr     += NumBytesPerLine;
      for (; NumBytesPerLine > 0; NumBytesPerLine--) {
        _ReportOutf("%.2X ", *pData++);
      }
      _ReportOutf("\n");
    }
  }
Done:
  free(pData0);
  return r;
}

/*********************************************************************
*
*       _ExecWriteU8
*/
static int _ExecWriteU8(const char* s) {
  U32 aData[MAX_NUM_WRITE_ITEMS];
  U32 Addr;
  U32 NumItems, i;
  char acZone[32];

  _TryParseMemZone(&s, &acZone[0], sizeof(acZone));
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: w1 <Addr>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  NumItems = 0;
  do {
    if (NumItems > MAX_NUM_WRITE_ITEMS) {
      _ReportOutf("This command can only handle up to %d items\n", MAX_NUM_WRITE_ITEMS);
      return JLINK_ERROR_UNKNOWN;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (_ParseHex(&s, &aData[NumItems])) {
      if (NumItems == 0) {
        _ReportOutf("Syntax: w1 <Addr>, <Data>\n");
        return JLINK_ERROR_SYNTAX;
      }
      break;
    }
    aData[NumItems] &= 0xFF;
    NumItems++;
  } while (1);
  for (i = 0; i < NumItems; i++) {
    _ReportOutf("Writing %.2X -> %.8X\n", aData[i], Addr);
    if (acZone[0]) {  // Any zone specified?
      JLINK_WriteMemZonedEx(Addr, 1, &aData[i], 1, acZone);
    } else {
      JLINKARM_WriteU8(Addr, (U8)aData[i]);
    }
    Addr++;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteU16
*/
static int _ExecWriteU16(const char* s) {
  U32 aData[MAX_NUM_WRITE_ITEMS];
  U32 Addr;
  U32 NumItems, i;
  char acZone[32];

  _TryParseMemZone(&s, &acZone[0], sizeof(acZone));
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: w2 <Addr>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  NumItems = 0;
  do {
    if (NumItems > MAX_NUM_WRITE_ITEMS) {
      _ReportOutf("This command can only handle up to %d items\n", MAX_NUM_WRITE_ITEMS);
      return JLINK_ERROR_UNKNOWN;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (_ParseHex(&s, &aData[NumItems])) {
      if (NumItems == 0) {
        _ReportOutf("Syntax: w2 <Addr>, <Data>\n");
        return JLINK_ERROR_SYNTAX;
      }
      break;
    }
    aData[NumItems] &= 0xFFFF;
    NumItems++;
  } while (1);
  for (i = 0; i < NumItems; i++) {
    _ReportOutf("Writing %.4X -> %.8X\n", aData[i], Addr);
    if (acZone[0]) {  // Any zone specified?
      JLINK_WriteMemZonedEx(Addr, 2, &aData[i], 2, acZone);
    } else {
      JLINKARM_WriteU16(Addr, (U16)aData[i]);
    }
    Addr += 2;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteU32
*/
static int _ExecWriteU32(const char* s) {
  U32 aData[MAX_NUM_WRITE_ITEMS];
  U32 Addr;
  U32 NumItems, i;
  char acZone[32];

  _TryParseMemZone(&s, &acZone[0], sizeof(acZone));
  if (_ParseHex(&s, &Addr)) {
    _ReportOutf("Syntax: w4 <Addr>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  NumItems = 0;
  do {
    if (NumItems > MAX_NUM_WRITE_ITEMS) {
      _ReportOutf("This command can only handle up to %d items\n", MAX_NUM_WRITE_ITEMS);
      return JLINK_ERROR_UNKNOWN;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (_ParseHex(&s, &aData[NumItems])) {
      if (NumItems == 0) {
        _ReportOutf("Syntax: w4 <Addr>, <Data>\n");
        return JLINK_ERROR_SYNTAX;
      }
      break;
    }
    NumItems++;
  } while (1);
  for (i = 0; i < NumItems; i++) {
    _ReportOutf("Writing %.8X -> %.8X\n", aData[i], Addr);
    if (acZone[0]) {  // Any zone specified?
      JLINK_WriteMemZonedEx(Addr, 4, &aData[i], 4, acZone);
    } else {
      JLINKARM_WriteU32(Addr, aData[i]);
    }
    Addr += 4;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteMultiU32
*/
static int _ExecWriteMultiU32(const char* s) {
  JLINK_WRITE_MEM_DESC aDesc[MAX_NUM_WRITE_ITEMS];
  U32 aData[MAX_NUM_WRITE_ITEMS];
  U32 NumItems, i;
  int r;

  memset(aDesc, 0, sizeof(aDesc));
  NumItems = 0;
  do {
    if (NumItems > MAX_NUM_WRITE_ITEMS) {
      _ReportOutf("This command can only handle up to %d items\n", MAX_NUM_WRITE_ITEMS);
      return JLINK_ERROR_UNKNOWN;
    }
    if (_ParseHex(&s, &aDesc[NumItems].Addr)) {
      if (NumItems == 0) {
        _ReportOutf("Syntax: wm4 <Addr> <Data> [<Addr> <Data>...]\n");
        return JLINK_ERROR_SYNTAX;
      }
      break;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (_ParseHex(&s, &aData[NumItems])) {
      _ReportOutf("Syntax: wm4 <Addr> <Data> [<Addr> <Data>...]\n");
      return JLINK_ERROR_SYNTAX;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    aDesc[NumItems].NumBytes  = 4;
    aDesc[NumItems].pData     = (U8*)&aData[NumItems];
    NumItems++;
  } while (1);
  for (i = 0; i < NumItems; i++) {
    _ReportOutf("Writing %.8X -> %.8X\n", aData[i], aDesc[i].Addr);
  }
  r = JLINKARM_WriteMemMultiple(&aDesc[0], NumItems);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecErase
*/
static int _ExecErase(const char* s) {
  JLINKARM_DEVICE_INFO DeviceInfo;
  U32 d;
  int r;
  //
  // Get Flash Areas from Device Config
  //
  d = JLINKARM_DEVICE_GetIndex(NULL);
  //
  // Check if everything is O.K.
  //
  if(d == -1) {
    _ReportOutf("Please select a device with \"device <DeviceName>\" before using this command.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  if(JLINKARM_IsHalted() == 0) {
    JLINKARM_Halt();
  }
  DeviceInfo.SizeOfStruct = sizeof(JLINKARM_DEVICE_INFO);
  JLINKARM_DEVICE_GetInfo(d, &DeviceInfo);
  _OverrideFlashProgProgressbarIfNoGui();
  //
  // For each Flash area, 
  // fill an array with erase value (FF) 
  // and write it
  //
  _ReportOutf("Erasing device (%s)...\n", DeviceInfo.sName);
  r = JLINK_EraseChip();
  if (r < 0) {
    _ReportOutf("ERROR: Erase returned with error code %d.\n", r);
    return JLINK_ERROR_UNKNOWN;
  } else {
    _ReportOutf("Erasing done.\n");
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecConfigDevices
*/
static int _ExecConfigDevices(const char* s) {
  JLINKARM_JTAG_DEVICE_CONF aConf[MAX_NUM_DEVICES];
  U32 NumDevices;

  memset(aConf, 0, sizeof(aConf));
  aConf[0].SizeOfStruct = sizeof(JLINKARM_JTAG_DEVICE_CONF);
  if (_ParseDec(&s, &aConf[0].IRLen)) {
    _ReportOutf("Syntax: ConfigDevices <IRLen_0> [,<IRLen_1>,...,<IRLen_n>]\n");
    return JLINK_ERROR_SYNTAX;
  }
  NumDevices = 1;
  do {
    if (NumDevices >= MAX_NUM_DEVICES) {
      _ReportOutf("This command can only handle up to %d devices\n", MAX_NUM_DEVICES);
      return JLINK_ERROR_UNKNOWN;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    aConf[NumDevices].SizeOfStruct = sizeof(JLINKARM_JTAG_DEVICE_CONF);
    if (_ParseDec(&s, &aConf[NumDevices].IRLen)) {
      break;
    }
    NumDevices++;
  } while (1);

  JLINKARM_JTAG_ConfigDevices(NumDevices, aConf);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecWriteMultiple
*/
static int _ExecWriteMultiple(const char* s) {
  U32 NumWords;
  U8* pData;
  unsigned i;
  if (_ParseDec(&s, &NumWords)) {
    _ReportOutf("Syntax: wm <NumWords>, <NumReps>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _ReportOutf("Writing 100 * %d words -> %.8X ...\n", NumWords, _RAMAddr);
  pData = (U8*)malloc(NumWords << 2);
  for (i = 0; i < 100; i++) {
    JLINKARM_WriteMem(_RAMAddr, NumWords << 2, pData);
    if (JLINKARM_HasError()) {
      _ReportOutf("  Write cycles before error: %d\n", i);
      break;
    }
  }
  if (JLINKARM_HasError() == 0) {
    _ReportOutf("  O.K.\n");
  }
  free(pData);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecAND
*/
static int _ExecAND(const char* s) {
  U64 Data;
  if (_ParseHex64(&s, &Data)) {
    return JLINK_ERROR_SYNTAX;
  }
  _Var &= Data;
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecOR
*/
static int _ExecOR(const char* s) {
  U64 Data;
  if (_ParseHex64(&s, &Data)) {
    return JLINK_ERROR_SYNTAX;
  }
  _Var |= Data;
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecXOR
*/
static int _ExecXOR(const char* s) {
  U64 Data;
  if (_ParseHex64(&s, &Data)) {
    return JLINK_ERROR_SYNTAX;
  }
  _Var ^= Data;
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecShowEmuInfo
*/
static int _ExecShowEmuInfo(const char* s) {
  JLINKARM_EMU_INFO Info;
  int NumDevices, iDevice;
  NumDevices = JLINKARM_EMU_GetNumDevices();
  _ReportOutf("%d emulator%s detected.\n", NumDevices, (NumDevices == 1) ? "" : "s");
  for (iDevice = 0; iDevice < NumDevices; iDevice++) {
    Info.SizeOfStruct = sizeof(JLINKARM_EMU_INFO);
    JLINKARM_EMU_GetDeviceInfo(iDevice, &Info);
    _ReportOutf("  No.%d: USBAddr=%d, SN=%d\n", iDevice, Info.USBAddr, Info.SerialNo);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSelectEmuByUSBSN
*/
static int _ExecSelectEmuByUSBSN(const char* s) {
  U32 SerialNo;
  int r;
  unsigned WasConnectedToJLink;
  unsigned WasConnectedToTarget;
  COMMANDER_SETTINGS* pCommanderSettings;

  pCommanderSettings = &_CommanderSettings;
  if (_ParseDec(&s, &SerialNo)) {
    _ReportOutf("Syntax: SelectEmuBySN <SerialNo>\n");
    return JLINK_ERROR_SYNTAX;
  }
  //
  // Disconnect from target + J-Link
  // Update commander settings
  // Reconnect to J-Link + target
  //
  WasConnectedToJLink = pCommanderSettings->Status.ConnectedToJLink;
  WasConnectedToTarget = pCommanderSettings->Status.ConnectedToTarget;
  if (WasConnectedToTarget) {
    _DisconnectFromTarget(pCommanderSettings);
  }
  if (WasConnectedToJLink) {
    _DisconnectFromJLink(pCommanderSettings);
  }
  pCommanderSettings->HostIF      = JLINKARM_HOSTIF_USB;
  pCommanderSettings->acIPAddr[0] = 0;
  pCommanderSettings->EmuSN = SerialNo;
  r = _ConnectToJLink(pCommanderSettings);
  if (WasConnectedToTarget && (r >= 0)) {
    r = _ConnectToTarget(pCommanderSettings);
  }
  return (r < 0) ? JLINK_ERROR_UNKNOWN : JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecConfigEmu
*/
static int _ExecConfigEmu(const char* s) {
  U32 EnumMode;
  U32 USBAddr;
  U32 FakedSN;
  int r;

  if (JLINKARM_IsOpen() == 0) {
    _ReportOutf("No communication with J-Link !\n");
    return JLINK_ERROR_UNKNOWN;
  }
  if (JLINKARM_GetHardwareVersion() < 50000) {
    _ReportOutf("Command \"ConfigEmu\" is only supported by hardware version 5.00 and above.\n");
    return JLINK_NOERROR;
  }
  if (_ParseDec(&s, &EnumMode)) {
    _ReportOutf("Syntax: ConfigEmu <EnumMode> [<USBAddr>|<FakedSN>]\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  _EatWhite(&s);
  switch (EnumMode) {
  case 0:           // Old enumeration method (USBAddr: 0-3, SN: 123456)
    if (*s) {
      if (_ParseDec(&s, &USBAddr)) {
        _ReportOutf("Syntax: ConfigEmu 0 [<USBAddr>]\n");
        return JLINK_ERROR_SYNTAX;
      }
      JLINKARM_WriteEmuConfigMem((U8*)&USBAddr, 0, 1);
    }
    break;
  case 1:           // New enumeration method (use real SN)
    break;
  case 2:           // New enumeration method (use faked SN)
    if (*s) {
      if (_ParseDec(&s, &FakedSN)) {
        _ReportOutf("Syntax: ConfigEmu 2 [<FakedSN>]\n");
        return JLINK_ERROR_SYNTAX;
      }
      JLINKARM_WriteEmuConfigMem((U8*)&FakedSN, 8, 4);
    }
    break;
  default:
    _ReportOutf("ERROR: Unknown enumeration method specified.\n");
    return JLINK_ERROR_SYNTAX;
  }
  r = JLINKARM_WriteEmuConfigMem((U8*)&EnumMode, 1, 1);
  if (r != 0) {
    _ReportOutf("Failed  EMU configuration failed.\n");
    return JLINK_ERROR_UNKNOWN;
  } else {
    _ReportOutf("Please unplug the device, then plug it back in.\n");
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _cbCtrlHandler
*/
#ifdef WIN32
static BOOL WINAPI _cbCtrlHandler(DWORD Type) {
  switch (Type) {
  case CTRL_C_EVENT:
  case CTRL_BREAK_EVENT:
    _ExitTerminal = 1;
    return TRUE;
  }
  return FALSE;
}
#endif

/*********************************************************************
*
*       _ExecTerminal
*/
static int _ExecTerminal(const char* s) {
  U32 aData[0x100];
  U32 Data;
  int NumBytes;
  int iByte;
  int NumItems;
  int iItem;

#ifdef WIN32
  SetConsoleCtrlHandler(_cbCtrlHandler, TRUE);
#endif
  _ExitTerminal = 0;

  if (JLINKARM_EMU_HasCPUCap(JLINKARM_EMU_CPU_CAP_TERMINAL)) {
    U8 aBuffer[0x800];
    int r;
    int i;

    do {
      r = JLINKARM_ReadTerminal(&aBuffer[0], sizeof(aBuffer));
      if (r > 0) {
        for (i = 0; i < r; i++) {
          _ReportOutf("%c", aBuffer[i] & 0xFF);
        }
      }
      if (_kbhit()) {
       getch();
       _ExitTerminal = 1;
      }
    } while (_ExitTerminal == 0);
  } else {
    do {
      NumItems = JLINKARM_ReadDCC(&aData[0], COUNTOF(aData), 20);
      if (NumItems > 0) {
        for (iItem = 0; iItem < NumItems; iItem++) {
          U32 DCCHandlerVer;
 
          Data = aData[iItem];
          DCCHandlerVer = Data >> 24;
          if ((DCCHandlerVer & 0x93) == 0x93) {           // New DCC handler is running on target
            U32 PacketType;
            //
            // We have to check if the packet we received is a valid terminal packet
            //
            PacketType = (Data >> 16) & 0xFF;
            if (PacketType == 0x80) {                     // 1-byte terminal packet found
              NumBytes = 1;
            } else if (PacketType == 0xA0) {              // 2-byte terminal packet found
              NumBytes = 2;
            } else {                                      // No valid terminal packet found
              NumBytes = 0;
            }
          } else {                                        // Old DCC handler is running on target
            NumBytes = (Data >> 24) + 1;
          }
          for (iByte = 0; iByte < NumBytes; iByte++) {    // Display data
            _ReportOutf("%c", (Data & 0xFF));
            Data >>= 8;
          }
        }
      }
      if (_kbhit()) {
        getch();
        _ExitTerminal = 1;
      }
    } while (_ExitTerminal == 0);
  }
  _ReportOutf("\n");
 return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSTraceConfig
*/
static int _ExecSTraceConfig(const char* s) {
  int r;
  _EatWhite(&s);
  if (*s) {
    r = JLINK_STRACE_Config(s);
  }
  if (r < 0) {
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSTraceStart
*/
static int _ExecSTraceStart(const char* s) {
  int r;
  r = JLINK_STRACE_Start();
  if (r < 0) {
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSTraceStop
*/
static int _ExecSTraceStop(const char* s) {
  int r;
  r = JLINK_STRACE_Stop();
  if (r < 0) {
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSTraceRead
*/
static int _ExecSTraceRead(const char* s) {
  char ac[256];
  int NumItems = 0x40;
  U32* pData;
  int i;
  int r = JLINK_NOERROR;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseHex(&s, &NumItems)) {
      _ReportOutf("Syntax: STraceRead [<NumItems>]\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  if (NumItems > 0x10000) {
    _ReportOutf("NumItems should be <= 0x10000\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Read data
  //
  pData = malloc(NumItems * 4);
  NumItems = JLINK_STRACE_Read(pData, NumItems);
  if (NumItems < 0) {
    _ReportOutf("Could not read trace data.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  //
  // Display data
  //
  if (JLINKARM_HasError() == 0) {
    _ReportOutf("%d instructions read via STRACE.\n", NumItems);
    for (i = 0; i < NumItems; i++) {
      r = _DisassembleInst(ac, *(pData + i), NULL);
      if (r < 0) {
        //break;
      }
      _ReportOutf(ac);
    }
  }
Done:
  free(pData);
  return r;
}

/*********************************************************************
*
*       _ExecSWOSpeed
*/
static int _ExecSWOSpeed(const char* s) {
  JLINKARM_SWO_SPEED_INFO SpeedInfo = {0};
  U32 Interface = 0;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseDec(&s, &Interface)) {
      _ReportOutf("Syntax: SWOSpeed [<Interface>]\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  //
  // Read speed info
  //
  SpeedInfo.SizeofStruct = sizeof(SpeedInfo);
  SpeedInfo.Interface    = Interface;
  JLINKARM_SWO_Control(JLINKARM_SWO_CMD_GET_SPEED_INFO, &SpeedInfo);
  //
  // Show supported SWO speeds
  //
  _ReportOutf("Supported speeds:\n");
  if (SpeedInfo.BaseFreq > 1000) {
    _ReportOutf(" - %d kHz/n, (n>=%d). => %dkHz, %dkHz, %dkHz, ...\n", SpeedInfo.BaseFreq / 1000, SpeedInfo.MinDiv,
                                                                  SpeedInfo.BaseFreq / 1000 / (SpeedInfo.MinDiv + 0),
                                                                  SpeedInfo.BaseFreq / 1000 / (SpeedInfo.MinDiv + 1),
                                                                  SpeedInfo.BaseFreq / 1000 / (SpeedInfo.MinDiv + 2));
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSWOStart
*/
static int _ExecSWOStart(const char* s) {
  JLINKARM_SWO_START_INFO StartInfo = {0};
  U32 Interface = 0;
  U32 Speed     = 19200;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseDec(&s, &Speed)) {
      _ReportOutf("Syntax: SWOStart [<Speed> [<Interface>]]\n");
      return JLINK_ERROR_SYNTAX;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (*s) {
      if (_ParseDec(&s, &Interface)) {
        _ReportOutf("Syntax: SWOStart [<Speed> [<Interface>]]\n");
        return JLINK_ERROR_SYNTAX;
      }
    }
  }
  //
  // Start SWO
  //
  StartInfo.SizeofStruct = sizeof(StartInfo);
  StartInfo.Interface    = Interface;
  StartInfo.Speed        = Speed;
  JLINKARM_SWO_Control(JLINKARM_SWO_CMD_START, &StartInfo);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSWOStop
*/
static int _ExecSWOStop(const char* s) {
  JLINKARM_SWO_Control(JLINKARM_SWO_CMD_STOP, NULL);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSWOStat
*/
static int _ExecSWOStat(const char* s) {
  U32 NumBytesInBuffer;
  NumBytesInBuffer = JLINKARM_SWO_Control(JLINKARM_SWO_CMD_GET_NUM_BYTES, NULL);
  _ReportOutf("%d bytes in host buffer\n", NumBytesInBuffer);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSWORead
*/
static int _ExecSWORead(const char* s) {
  U32 NumBytesInBuffer;
  U32 NumBytes = 0x100;
  U32 Offset   = 0;
  U8* pData0;
  U8* pData;
  int r = JLINK_NOERROR;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseHex(&s, &NumBytes)) {
      _ReportOutf("Syntax: SWORead [<NumBytes> [<Offset>]]\n");
      return JLINK_ERROR_SYNTAX;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (*s) {
      if (_ParseHex(&s, &Offset)) {
        _ReportOutf("Syntax: SWORead [<NumBytes> [<Offset>]]\n");
        return JLINK_ERROR_SYNTAX;
      }
    }
  }
  if (NumBytes > 0x10000) {
    _ReportOutf("NumBytes should be <= 0x10000\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Read data
  //
  pData0 = pData = malloc(NumBytes);
  JLINKARM_SWO_Read(pData, Offset, &NumBytes);
  if (NumBytes < 0) {
    _ReportOutf("Could not read SWO data.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  //
  // Display data
  //
  if (JLINKARM_HasError() == 0) {
    NumBytesInBuffer = JLINKARM_SWO_Control(JLINKARM_SWO_CMD_GET_NUM_BYTES, NULL);
    _ReportOutf("%d bytes read (%d bytes in host buffer)\n", NumBytes, NumBytesInBuffer);
    while (NumBytes > 0) {
      int NumBytesPerLine;
      _ReportOutf("%.8X = ", Offset);
      NumBytesPerLine = (NumBytes > 16) ? 16 : NumBytes;
      NumBytes -= NumBytesPerLine;
      Offset   += NumBytesPerLine;
      for (; NumBytesPerLine > 0; NumBytesPerLine--) {
        _ReportOutf("%.2X ", *pData++);
      }
      _ReportOutf("\n");
    }
  }
Done:
  free(pData0);
  return r;
}

/*********************************************************************
*
*       _AnalyzeSWOPacket
*
*  Return value:
*    >0  Len of valid packet
*    =0  Not enough data in buffer to analyze packet
*    -1  Unknown packet
*    -2  Ignore packet (in case of "Idle packet")
*/
static int _AnalyzeSWOPacket(U8* pData, U32 NumBytes, char* pInfo) {
  int CmdLen, i;
  U8  Cmd, d;
  U32 Data;
  U32 Id;
  //
  // Early out
  //
  if (NumBytes == 0) {
    return 0;                   // Not enough data
  }
  //
  // Parse command
  //
  Cmd = *pData++;
  //
  // Idle / Sync
  //
  if      (Cmd == 0x00) {
    if (NumBytes >= 6) {
      Data = _Load32LE(pData);
      if ((Data == 0) && (*(pData + 4) == 0x80)) {
        sprintf(pInfo, "Sync");
        return 6;
      }
    }
    return -2;                  // Ignore packet
  }
  //
  // Sync
  //
  else if (Cmd == 0x80) {
    return -2;                  // Ignore packet, since we can not check for full sync packet
  } 
  //
  // Overflow
  //
  else if (Cmd == 0x70) {
    sprintf(pInfo, "Overflow");
    return 1;
  } 
  //
  // Timestamp
  //
  else if ((Cmd & 0x0F) == 0x00) {
    //
    // multi-byte packet
    //
    if (Cmd & (1 << 7)) {
      Cmd = (Cmd >> 4) & 7;
      CmdLen = 1;
      Data   = 0;
      for (i = 0; i < 4; i++) {
        CmdLen++;
        d = *pData++;
        Data |= (d & 0x7F) << (i * 7);
        if ((d & 0x80) == 0) {
          break;
        }
      }
      if (d & 0x80) {
        return -1;              // Unknown packet
      }
      if        (Cmd == 4) {
        sprintf(pInfo, "Timestamp sync. event (%d)", Data);
      } else if (Cmd == 5) {
        sprintf(pInfo, "Timestamp delayed event (%d)", Data);
      } else {
        return -1;              // Unknown packet
      }
      return CmdLen;
    }
    //
    // single-byte packet
    //
    else {
      sprintf(pInfo, "Timestamp sync. ITM/DWT (%d)", (Cmd >> 4) & 7);
      return 1;
    }
  }
  //
  // Reserved
  //
  else if ((Cmd & 0x0F) == 0x04) {
  } 
  //
  // Extension
  //
  else if ((Cmd & 0x0B) == 0x08) {
  } 
  //
  // Software/Hardware Source
  //
  else if ((Cmd & 0x03) != 0x00) {
    Id     = (Cmd & 0xF8) >> 3;
    CmdLen = (1 << ((Cmd & 3) - 1)) + 1;
    if ((int)NumBytes < CmdLen) {
      return 0;                 // Not enough data
    }
    switch (CmdLen) {
    case 2: Data = _Load8LE (pData);  break;
    case 3: Data = _Load16LE(pData);  break;
    case 5: Data = _Load32LE(pData);  break;
    }
    //
    // Hardware Source (Diagnostics)
    //
    if (Cmd & (1 << 2)) {
      switch (Id) {
      //
      // Event Packet
      //
      case 0:
        if (CmdLen != 2) {
          return -1;            // Unknown packet
        }
        sprintf(pInfo, "Event counter (");
        if (Data & (1 << 0)) { strcat(pInfo, "CPI,");   }
        if (Data & (1 << 1)) { strcat(pInfo, "Exc,");   }
        if (Data & (1 << 2)) { strcat(pInfo, "Sleep,"); }
        if (Data & (1 << 3)) { strcat(pInfo, "LSU,");   }
        if (Data & (1 << 4)) { strcat(pInfo, "Fold,");  }
        if (Data & (1 << 5)) { strcat(pInfo, "Cyc,");   }
        if (Data & 0x3F) {
          *(pInfo + strlen(pInfo) - 1) = 0;
        }
        strcat(pInfo, ")");
        return CmdLen;
      //
      // Exception Trace Packet
      //
      case 1:
        if (CmdLen != 3) {
          return -1;            // Unknown packet
        }
        sprintf(pInfo, "Exception %d (", (Data & 0x1FF));
        if (((Data >> 12) & 3) == 0) { strcat(pInfo, "Invalid"); }
        if (((Data >> 12) & 3) == 1) { strcat(pInfo, "Entry");   }
        if (((Data >> 12) & 3) == 2) { strcat(pInfo, "Exit");    }
        if (((Data >> 12) & 3) == 3) { strcat(pInfo, "Return");  }
        strcat(pInfo, ")");
        return CmdLen;
      //
      // PC Sample Packet
      //
      case 2:
        sprintf(pInfo, "PC = 0x%.8X", Data);
        return CmdLen;
      }
    }
    //
    // Software Source (Application)
    //
    else {
      switch (CmdLen) {
      case 2: sprintf(pInfo, "SWIT(%d): %.2X",                Id, (Data >> 0) & 0xFF);                                                                break;
      case 3: sprintf(pInfo, "SWIT(%d): %.2X %.2X",           Id, (Data >> 0) & 0xFF, (Data >> 8) & 0xFF);                                            break;
      case 5: sprintf(pInfo, "SWIT(%d): %.2X %.2X %.2X %.2X", Id, (Data >> 0) & 0xFF, (Data >> 8) & 0xFF, (Data >> 16) & 0xFF, (Data >> 24) & 0xFF);  break;
      }
      return CmdLen;
    }
  }
  return -1;                    // Unknown packet
}

/*********************************************************************
*
*       _AnalyzeSWOData
*/
static void _AnalyzeSWOData(U8* pData, U32 Offset, U32 NumBytes) {
  char  acInfo[256] = {0};
  int   Sync = 0;
  int   Len, i;
  //
  // Early out
  //
  if (NumBytes == 0) {
    return;
  }
  //
  // Display packets
  //
  _ReportOutf("Offset     Data               Meaning\n");
  _ReportOutf("-----------------------------------------------\n");
  while (1) {
    Len = _AnalyzeSWOPacket(pData, NumBytes, &acInfo[0]);
    //
    // Handle valid packet
    //
    if (Len > 0) {
      Sync = 1;
      _ReportOutf("%.4X-%.4X  ", Offset, Offset + Len - 1);
      for (i = 0; i < Len; i++) {
        _ReportOutf("%.2X ", *(pData + i));
      }
      for (i = Len; i < 6; i++) {
        _ReportOutf("   ");
      }
      _ReportOutf(" %s\n", acInfo);
    }
    //
    // Handle unknown packet
    //
    else if (Len < 0) {
      if ((Len == -1) && Sync) {
        _ReportOutf("%.4X-....  ", Offset);
        _ReportOutf("%.2X                 ", *pData);
        _ReportOutf("Unknown packet\n");
        break;
      }
      Len = 1;
    }
    //
    // Handle end of data
    //
    else {
      if (Sync == 0) {
        _ReportOutf("No synchronization...\n");
      }
      break;
    }
    //
    // Increment offset
    //
    pData    += Len;
    Offset   += Len;
    NumBytes -= Len;
  }
}

/*********************************************************************
*
*       _ExecSWOShow
*/
static int _ExecSWOShow(const char* s) {
  U32 NumBytesInBuffer;
  U32 NumBytes = 0x100;
  U32 Offset   = 0;
  U8* pData;
  int r = JLINK_NOERROR;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseHex(&s, &NumBytes)) {
      _ReportOutf("Syntax: SWOShow [<NumBytes> [<Offset>]]\n");
      return JLINK_ERROR_SYNTAX;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (*s) {
      if (_ParseHex(&s, &Offset)) {
        _ReportOutf("Syntax: SWOShow [<NumBytes> [<Offset>]]\n");
        return JLINK_ERROR_SYNTAX;
      }
    }
  }
  if (NumBytes > 0x10000) {
    _ReportOutf("NumBytes should be <= 0x10000\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Read data
  //
  pData = malloc(NumBytes);
  JLINKARM_SWO_Read(pData, Offset, &NumBytes);
  if (NumBytes < 0) {
    _ReportOutf("Could not read SWO data.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  //
  // Display data
  //
  if (JLINKARM_HasError() == 0) {
    NumBytesInBuffer = JLINKARM_SWO_Control(JLINKARM_SWO_CMD_GET_NUM_BYTES, NULL);
    _ReportOutf("%d bytes read (%d bytes in host buffer)\n", NumBytes, NumBytesInBuffer);
    _AnalyzeSWOData(pData, Offset, NumBytes);
  }
Done:
  free(pData);
  return r;
}

/*********************************************************************
*
*       _ExtractSWIT
*
*  Return value:
*    >0  Len of valid packet
*    =0  Not enough data in buffer to analyze packet
*    -1  Unknown packet
*    -2  Ignore packet (in case of "Idle packet")
*/
static int _ExtractSWIT(U8* pData, U32 NumBytes, U32 Port) {
  int CmdLen, i;
  U8  Cmd, d;
  U32 Data;
  U32 Id;
  //
  // Early out
  //
  if (NumBytes == 0) {
    return 0;                   // Not enough data
  }
  //
  // Parse command
  //
  Cmd = *pData++;
  //
  // Idle / Sync
  //
  if (Cmd == 0x00) {
    if (NumBytes >= 6) {
      Data = _Load32LE(pData);
      if ((Data == 0) && (*(pData + 4) == 0x80)) {
        return 6;
      }
    }
    return -2;                  // Ignore packet
  }
  //
  // Sync
  //
  else if (Cmd == 0x80) {
    return -2;                  // Ignore packet, since we can not check for full sync packet
  } 
  //
  // Overflow
  //
  else if (Cmd == 0x70) {
    return 1;
  } 
  //
  // Timestamp
  //
  else if ((Cmd & 0x0F) == 0x00) {
    //
    // multi-byte packet
    //
    if (Cmd & (1 << 7)) {
      Cmd = (Cmd >> 4) & 7;
      CmdLen = 1;
      Data   = 0;
      for (i = 0; i < 4; i++) {
        CmdLen++;
        d = *pData++;
        Data |= (d & 0x7F) << (i * 7);
        if ((d & 0x80) == 0) {
          break;
        }
      }
      if (d & 0x80) {
        return -1;              // Unknown packet
      }
      if        (Cmd == 4) {
      } else if (Cmd == 5) {
      } else {
        return -1;              // Unknown packet
      }
      return CmdLen;
    }
    //
    // single-byte packet
    //
    else {
      return 1;
    }
  }
  //
  // Reserved
  //
  else if ((Cmd & 0x0F) == 0x04) {
  } 
  //
  // Extension
  //
  else if ((Cmd & 0x0B) == 0x08) {
  } 
  //
  // Software/Hardware Source
  //
  else if ((Cmd & 0x03) != 0x00) {
    Id     = (Cmd & 0xF8) >> 3;
    CmdLen = (1 << ((Cmd & 3) - 1)) + 1;
    if ((int)NumBytes < CmdLen) {
      return 0;                 // Not enough data
    }
    switch (CmdLen) {
    case 2: Data = _Load8LE (pData);  break;
    case 3: Data = _Load16LE(pData);  break;
    case 5: Data = _Load32LE(pData);  break;
    }
    //
    // Hardware Source (Diagnostics)
    //
    if (Cmd & (1 << 2)) {
      switch (Id) {
      //
      // Event Packet
      //
      case 0:
        if (CmdLen != 2) {
          return -1;            // Unknown packet
        }
        return CmdLen;
      //
      // Exception Trace Packet
      //
      case 1:
        if (CmdLen != 3) {
          return -1;            // Unknown packet
        }
        return CmdLen;
      //
      // PC Sample Packet
      //
      case 2:
        return CmdLen;
      }
    }
    //
    // Software Source (Application)
    //
    else {
      if (Id == Port) {
        switch (CmdLen) {
        case 2:
          _ReportOutf("%c",       (Data >> 0) & 0xFF);
          break;
        case 3:
          _ReportOutf("%c%c",     (Data >> 0) & 0xFF, (Data >> 8) & 0xFF);
          break;
        case 5:
          _ReportOutf("%c%c%c%c", (Data >> 0) & 0xFF, (Data >> 8) & 0xFF, (Data >> 16) & 0xFF, (Data >> 24) & 0xFF);
          break;
        }
      }
      return CmdLen;
    }
  }
  return -1;                    // Unknown packet
}

/*********************************************************************
*
*       _ShowStimulusData
*/
static int _ShowStimulusData(U8* pData, U32 NumBytes, U32 Port) {
  U32 NumBytesRem;
  int Sync = 0;
  int Len;
  //
  // Early out
  //
  if (NumBytes == 0) {
    return 0;
  }
  //
  // Display packets
  //
  NumBytesRem = NumBytes;
  while (1) {
    Len = _ExtractSWIT(pData, NumBytesRem, Port);
    //
    // Handle valid packet
    //
    if (Len > 0) {
      Sync = 1;
    }
    //
    // Handle unknown packet
    //
    else if (Len < 0) {
      if ((Len == -1) && Sync) {
        break;
      }
      Len = 1;
    }
    //
    // Handle end of data
    //
    else {
      break;
    }
    //
    // Increment offset
    //
    pData       += Len;
    NumBytesRem -= Len;
  }
  return NumBytes - NumBytesRem;
}

/*********************************************************************
*
*       _DetermineSWOSpeed
*/
static int _DetermineSWOSpeed(U32 TargetFreq, U32* pTargetDiv, U32* pJLinkFreq, U32* pJLinkDiv) {
  JLINKARM_SWO_SPEED_INFO SpeedInfo = {0};
  U32 JLinkFreq;
  U32 JLinkDiv;
  U32 TargetDiv;
  float Deviation;

  SpeedInfo.SizeofStruct = sizeof(SpeedInfo);
  SpeedInfo.Interface    = 0;
  JLINKARM_SWO_Control(JLINKARM_SWO_CMD_GET_SPEED_INFO, &SpeedInfo);
  //
  // Find speed that matches for both: Emulator and debugger
  //
  JLinkFreq = SpeedInfo.BaseFreq;
  JLinkDiv  = SpeedInfo.MinDiv;
  TargetDiv = TargetFreq / (JLinkFreq / JLinkDiv); // Scale down target frequency to app. J-Link speed
  do {
    Deviation = ((float)JLinkFreq / (float)JLinkDiv) / ((float)TargetFreq / (float)TargetDiv);
    Deviation -= 1;
    Deviation *= 100;
    if ((Deviation <= 3.0) && (Deviation >= -3.0)) {
      break;
    }
    if (Deviation > 0.0) {   // Increment J-Link divider
      JLinkDiv++;
    } else {                 // Increment target divider
      TargetDiv++;
    }
  } while(1);
  if ((JLinkDiv > SpeedInfo.MaxDiv) || (TargetDiv > 0x2000)) {                     // Check if no matching frequency has been found. For the target we have a 13-bit prescaler which can be configured
    return -1;
  }
  *pJLinkFreq = JLinkFreq;
  *pJLinkDiv  = JLinkDiv;
  *pTargetDiv = TargetDiv;
  return 0;
}

/*********************************************************************
*
*       _ExecSWOView
*/
static int _ExecSWOView(const char* s) {
  JLINKARM_SWO_START_INFO StartInfo = {0};
  U32 NumBytes;
  U32 Interface  = 0;
  U32 Port       = 0;
  U32 TargetFreq = 0;
  U32 TargetDiv  = 1;
  U32 JLinkFreq  = 0;
  U32 JLinkDiv   = 1;
  U8* pData;
  int r = JLINK_NOERROR;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseDec(&s, &TargetFreq)) {
      _ReportOutf("Syntax: SWOView <TargetFreq>\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  //
  // Init SWO
  //
  if (TargetFreq == 0) {
    TargetFreq = JLINKARM_MeasureCPUSpeed(-1, 1);
  }
  if (TargetFreq <= 0) {
    _ReportOutf("ERROR: Could not measure target CPU speed.\n");
    _ReportOutf("Please select a device using the \"device\" command or enter\n");
    _ReportOutf("the target CPU speed manually using \"SWOView <TargetFreq>\".\n");
    return JLINK_ERROR_SYNTAX;
  }
  if (_DetermineSWOSpeed(TargetFreq, &TargetDiv, &JLinkFreq, &JLinkDiv) < 0) {
    _ReportOutf("ERROR: Can not determine SWO clock frequency.\n");
    return JLINK_ERROR_SYNTAX;
  }
  JLINKARM_WriteU32(0xE000EDFC, 0x01000000);      // Debug Exception and Monitor Control Register.
	JLINKARM_WriteU32(0xE0042004, 0x00000027);      //
  JLINKARM_WriteU32(0xE00400F0, 0x00000002);      // Selected Pin Protocol Register, Select NRZ mode
  JLINKARM_WriteU32(0xE0040010, TargetDiv - 1);   // Async Clock Prescaler Register (72/48 = 1,5 MHz)
  JLINKARM_WriteU32(0xE0000FB0, 0xC5ACCE55);      // Lock Access Register
  JLINKARM_WriteU32(0xE0000E80, 0x0001000D);      // Trace Control Register 
  JLINKARM_WriteU32(0xE0000E40, 0x0000000F);      // Trace Privilege Register
  JLINKARM_WriteU32(0xE0000E00, 0x00000001);      // Trace Enable Register
  JLINKARM_WriteU32(0xE0001000, 0x400003FE);      // DWT Control Register
  JLINKARM_WriteU32(0xE0040304, 0x00000100);      // Formatter and Flush Control Register
  //
  // Start SWO
  //
  StartInfo.SizeofStruct = sizeof(StartInfo);
  StartInfo.Interface    = Interface;
  StartInfo.Speed        = JLinkFreq / JLinkDiv;
  _ReportOutf("\nReceiving SWO data @ %d kHz.\n", StartInfo.Speed / 1000);
  JLINKARM_SWO_Control(JLINKARM_SWO_CMD_START, &StartInfo);
  //
  // Read and analyze data
  //
  _ReportOutf("Data from stimulus port %d:\n", Port);
  _ReportOutf("-----------------------------------------------\n");
#ifdef WIN32
  SetConsoleCtrlHandler(_cbCtrlHandler, TRUE);
#endif
  _ExitTerminal = 0;
  pData = malloc(0x800);
  do {
    NumBytes = 0x800;
    JLINKARM_SWO_Read(pData, 0, &NumBytes);
    if (NumBytes > 0) {
      NumBytes = _ShowStimulusData(pData, NumBytes, Port);
      JLINKARM_SWO_Control(JLINKARM_SWO_CMD_FLUSH, &NumBytes);
    }
    if (_kbhit()) {
      getch();
      _ExitTerminal = 1;
    }
  } while (_ExitTerminal == 0);
  free(pData);
  //
  // Stop SWO
  //
  JLINKARM_SWO_Control(JLINKARM_SWO_CMD_STOP, NULL);
  return r;
}

/*********************************************************************
*
*       _ExecSWOFlush
*/
static int _ExecSWOFlush(const char* s) {
  U32* pNumBytes = NULL;
  U32 NumBytes;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseHex(&s, &NumBytes)) {
      _ReportOutf("Syntax: SWOFlush [<NumBytes>]\n");
      return JLINK_ERROR_SYNTAX;
    }
    pNumBytes = &NumBytes;
  }
  //
  // Flush data
  //
  JLINKARM_SWO_Control(JLINKARM_SWO_CMD_FLUSH, pNumBytes);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSWOStabilityTest
*
*  Function description
*    Performs SWO stability test on Cortex-M target.
*    Generic RAMCode is downloaded which outputs a 4-byte unsigned counter via SWO,
*    We should ONLY receive SWIT packets. NO sync packets or anything else.
*    The SWIT packets will always have 4 bytes payload which describe a counter value that is incremented.
*    We check if we receive the correct counter value.
*
*  Notes
*    Stability test is always performed with the highest SWO speed that is supported by the CPU and J-Link.
*    Device needs to be selected to perform this test
*/
static int _ExecSWOStabilityTest(const char* s) {
  U32 Data;
  int HasError;
  U32 NumErrPackets = 0;
  U32 OldNumErrPackets = 0;
  U32 NumBytesRead;
  U32 NumBytesInBuff;
  U32 Cnt;
  U8 * pData;
  U8 * pAlloc;
  JLINKARM_SWO_START_INFO StartInfo = {0};
  JLINKARM_DEVICE_INFO DeviceInfo;
  int iDevice;
  U32 CPUSpeedHz;
  U32 MaxSWOSpeedHz;
  int r;
  U32 v;
  U8 acSWORAMCode[] = {
                        0x0A, 0x68, // ??main_0: LDR      R2,[R1, #+0]
                        0xD2, 0x07, // LSLS     R2,R2,#+31
                        0xFC, 0xD5, // BPL.N    ??main_0
                        0x08, 0x60, // STR      R0,[R1, #+0]
                        0x40, 0x1C, // ADDS     R0,R0,#+1
                        0xF9, 0xE7  // B.N      ??main_0
                      };
  //
  // Check if connected J-Link has SWO capabilities
  //
  if (JLINKARM_EMU_HasCapEx(JLINKARM_EMU_CAP_EX_SWO) == 0) {
    _ReportOutf("Connected J-Link does not support SWO.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Check if device is set and if it is a Cortex-M.
  // If not, we are done.
  //
  iDevice = JLINKARM_DEVICE_GetIndex(NULL);
  if(iDevice == -1) {
    _ReportOutf("Please select a device with \"device <DeviceName>\" before using this command.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  DeviceInfo.SizeOfStruct = sizeof(JLINKARM_DEVICE_INFO);
  JLINKARM_DEVICE_GetInfo(iDevice, &DeviceInfo);
  switch (DeviceInfo.Core >> 24) {
  case JLINKARM_DEV_FAMILY_CM1:
  case JLINKARM_DEV_FAMILY_CM3:
  case JLINKARM_DEV_FAMILY_CM0:
  case JLINKARM_DEV_FAMILY_CM4:
    break;
  default:
    _ReportOutf("This command is only supported for Cortex-M devices.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Make sure that SWD is selected as target interface
  // and remember current speed settings
  //
  _ReportOutf("Reconnecting to device via SWD interface...");
  v = JLINKARM_GetSpeed();
  JLINKARM_TIF_Select(JLINKARM_TIF_SWD);
  JLINKARM_SetSpeed(v);
  r = JLINKARM_Connect();
  if (r != 0) {
    _ReportOutf("Failed to reconnect to device.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Halt CPU
  //
  if(JLINKARM_IsHalted() == 0) {
    JLINKARM_Halt();
  }
  //
  // Determine max. SWO speed by measuring CPU speed and checking J-Link SWO speed capabilities
  //
  CPUSpeedHz = JLINKARM_MeasureCPUSpeed(-1, 1);
  if ((int)CPUSpeedHz <= 0) {
    _ReportOutf("Failed to measure CPU clock speed.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  r = JLINKARM_SWO_GetCompatibleSpeeds(CPUSpeedHz, 0, &MaxSWOSpeedHz, 1);
  if (r < 1) {
    _ReportOutf("Failed to calculate suitable SWO speed.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  _ReportOutf( "Performing SWO test with the following settings:\n"
          "Target: %s\n"
          "CPUSpeed: %d kHz\n"
          "SWOSpeed: %d kHz\n",
          DeviceInfo.sName,
          CPUSpeedHz / 1000,
          MaxSWOSpeedHz / 1000
        );
  //
  // R0 = 0
  // R1 = 0xE0000000 // ITM_STIM_U32
  //
  // int Cnt;
  // Cnt = 0;
  // do {
  //   while ((ITM_STIM_U32 & 1) == 0);
  //   ITM_STIM_U32 = Cnt++;
  // } while (1);
  //
  JLINKARM_WriteReg(JLINKARM_CM3_REG_R0, 0);
  JLINKARM_WriteReg(JLINKARM_CM3_REG_R1, 0xE0000000);
  JLINKARM_WriteReg(JLINKARM_CM3_REG_R15, DeviceInfo.RAMAddr);
  JLINKARM_WriteMem(DeviceInfo.RAMAddr, sizeof(acSWORAMCode), acSWORAMCode);
  JLINKARM_SWO_EnableTarget(CPUSpeedHz, MaxSWOSpeedHz, JLINKARM_SWO_IF_UART, 1); // Enable stimulus port 0
  JLINKARM_GoIntDis();
#ifdef WIN32
  SetConsoleCtrlHandler(_cbCtrlHandler, TRUE);
#endif
  //
  // Read and analyze SWO data
  // What we expect:
  // Target sends a 32-bit counter value with full speed.
  // We have no syncs etc enabled so we expect to only get SWIT packets with 1 byte header and 4 bytes payload.
  // Everything else is an error.
  //
  pAlloc = malloc(0x100000);
  NumBytesInBuff = 0;
  pData = pAlloc;
  Cnt = 0;
  HasError = 0;
  _ReportOutf("Counter: %9d, Errors: %9d", Cnt, NumErrPackets);
  do {
    NumBytesRead = 0x100000 - NumBytesInBuff;
    JLINKARM_SWO_Read(pData + NumBytesInBuff, 0, &NumBytesRead);
    if (NumBytesRead > 0) {
      JLINKARM_SWO_Control(JLINKARM_SWO_CMD_FLUSH, &NumBytesRead);
      NumBytesInBuff += NumBytesRead;
    }
    //
    // Analyze buffer content
    //
    do {
      if (NumBytesInBuff < 5) {                   // Too less data to analyze in the buffer?
        if (NumBytesInBuff) {                     // Any incomplete packets in buffer? move them to the start of the buffer
          memcpy(pAlloc, pData, NumBytesInBuff);
        }
        pData = pAlloc;
        break;
      }
      //
      // We expect data from stimulus port 0 which means the header[7:3] must be 0.
      // We expect 32-bit items to be send by the target, so header[1:0] must be 3.
      //
      if (*pData != 0x3) {
        if (HasError == 0) {
          HasError = 1;
          NumErrPackets++;
          Cnt++;
        }
        pData += 1;
        NumBytesInBuff -= 1;
        continue;
      }
      //
      // Extract data and check if counter value matches the expected one.
      //
      Data = _Load32LE(pData + 1);
      if (HasError) {
        HasError = 0;
        Cnt = Data;
      }
      if (Data != Cnt) {
        HasError = 1;
        NumErrPackets++;
      }
      if (((Cnt % 10000) == 0) || (OldNumErrPackets != NumErrPackets)) {
        OldNumErrPackets = NumErrPackets;
        _ReportOutf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\bCounter: %11d, Errors: %11d", Cnt, NumErrPackets);

      }
      Cnt++;
      pData += 5;
      NumBytesInBuff -= 5;
    } while(_ExitTerminal == 0);
    _Sleep(2);  // Give other threads some time
  } while (_ExitTerminal == 0);
  _ReportOutf("\n");
  free(pAlloc);
  JLINKARM_SWO_DisableTarget(1);
  _ExitTerminal = 0;
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       static code, HSS
*
**********************************************************************
*/
#define HSS_MAX_NUM_BLOCKS      (8)

static JLINK_HSS_MEM_BLOCK_DESC _HSS_aBlock[HSS_MAX_NUM_BLOCKS + 1];  // Last element will have <NumBytes> set to 0 to indicate end of list
static U32                      _HSS_IsRunning;

/*********************************************************************
*
*       _ExecHSSStart
*/
static int _ExecHSSStart(const char* s) {
  U32 Addr;
  U32 Period;
  int NumBlocks;
  int r;
  int i;
  JLINK_HSS_CAPS Caps = {0};
  //
  // Show capabilities
  //
  _HSS_IsRunning = 0;
  //
  // Parse parameters
  //
  Period         = 10000;
  NumBlocks      = 0;
  _HSS_IsRunning = 0;  // Initially, HSS is not running
  _EatWhite(&s);
  _ReportOutf("Syntax: HSSStart <Period_us> Block0 [Block1] [...]\n Blockn: <Addr> <NumBytes> <Flags>\n");
  if (*s == 0) {
    return JLINK_ERROR_SYNTAX;
  }
  //
  // Parse period first
  //
  if (_ParseDec(&s, &Period)) {
    _ReportOutf("Syntax error.\n");
    return JLINK_ERROR_SYNTAX;
  }
  //
  // Parse <Addr>, <NumBytes>, <Flags> parameters
  //
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (*s == 0) {
    _ReportOutf("Syntax error.\n");
    return JLINK_ERROR_SYNTAX;
  }
  do {
    _EatWhite(&s);
    if (*s == 0) {
      break;
    }
    if (NumBlocks >= HSS_MAX_NUM_BLOCKS) {
      _ReportOutf("This command can only handle up to %d blocks\n", HSS_MAX_NUM_BLOCKS);
      return JLINK_ERROR_UNKNOWN;
    }
    if (*s == ',') {
      s++;
    }
    if (_ParseHex(&s, &Addr)) {
      _ReportOutf("Syntax error.\n");
      return JLINK_ERROR_SYNTAX;
    }
    _HSS_aBlock[NumBlocks].Addr = Addr;
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (_ParseHex(&s, &_HSS_aBlock[NumBlocks].NumBytes)) {
      _ReportOutf("Syntax error.\n");
      return JLINK_ERROR_SYNTAX;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (_ParseHex(&s, &_HSS_aBlock[NumBlocks].Flags)) {
      _ReportOutf("Syntax error.\n");
      return JLINK_ERROR_SYNTAX;
    }
    NumBlocks++;
  } while (1);
  _HSS_aBlock[NumBlocks].NumBytes = 0;  // Indicate end of list
  _ReportOutf("Starting HSS with the following settings:\n  %d blocks\n", NumBlocks);
  for(i = 0; i < NumBlocks; i++) {
    _ReportOutf("    Block %d: %d bytes @ 0x%.8X\n", i, _HSS_aBlock[i].NumBytes, _HSS_aBlock[i].Addr);
  }
  _ReportOutf("  %d us period.\n", Period);
  //
  // Start HSS
  //
  r = JLINK_HSS_Start(&_HSS_aBlock[0], NumBlocks, Period, 0);
  if (r < 0) {
    _ReportOutf("Failed to start HSS. Error code: %d\n", r);
    return JLINK_ERROR_UNKNOWN;
  } else {
    _HSS_IsRunning = 1;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecHSSStop
*/
static int _ExecHSSStop(const char* s) {
  int r;
  r = JLINK_HSS_Stop();
  _HSS_IsRunning = 0;
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecHSSRead
*/
static int _ExecHSSRead(const char* s) {
  int NumSamplesRead;
  U32 NumSamples;
  U32 AllocSize;
  U32 Tmp;
  U8* pData0;
  U8* pData;
  int r;
  int i;
  int NumBlocks;

  pData0     = NULL;
  pData      = NULL;
  r          = JLINK_NOERROR;
  NumSamples = 10;
  _ReportOutf("Syntax: HSSRead [<NumSamples>]\n");
  //
  // Early out if HSS is not running
  //
  if (_HSS_IsRunning == 0) {
    _ReportOutf("HSS is not running.\n");
    return JLINK_NOERROR;
  }
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (*s) {
    if (_ParseHex(&s, &NumSamples)) {
      _ReportOutf("Syntax error\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  //
  // Determine NumBlocks of current config
  //
  NumBlocks = 0;
  AllocSize = 0;
  do {
    if (_HSS_aBlock[NumBlocks].NumBytes == 0) {
      break;
    }
    AllocSize += _HSS_aBlock[NumBlocks].NumBytes;
    NumBlocks++;
  } while (1);
  _ReportOutf("HSS config: %d blocks.\n", NumBlocks);
  for (i = 0; i < NumBlocks; i++) {
    _ReportOutf("  Block %d, %d bytes.\n", i, _HSS_aBlock[i].NumBytes);
  }
  //
  // Allocate memory for requested number of samples
  //
  pData = malloc(NumSamples * AllocSize);
  if (pData == NULL) {
    _ReportOutf("Failed to allocate memory for HSS buffer.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  pData0 = pData;
  NumSamplesRead = JLINK_HSS_Read(pData, NumSamples * AllocSize);
  if (NumSamplesRead < 0) {
    _ReportOutf("Error while reading HSS data.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  NumSamplesRead /= AllocSize;
  _ReportOutf("Read %d samples.\n", NumSamplesRead);
  //
  // Show data for each block
  //
  if (NumSamplesRead) {
    do {
      for (i = 0; i < NumBlocks; i++) {
        _ReportOutf("Block %d (@0x%.8X): ", i, _HSS_aBlock[i].Addr);
        Tmp = _HSS_aBlock[i].NumBytes;
        do {
          _ReportOutf("%.2X ", *pData++);
        } while (--Tmp);
        _ReportOutf("\n");
      }
    } while (--NumSamplesRead);
  }
Done:
  free(pData0);
  return r;
}

/*********************************************************************
*
*       static code, POWERTRACE
*
**********************************************************************
*/
/*********************************************************************
*
*       _ExecPowerTrace
*/
static int _ExecPowerTrace(const char* s) {
  JLINK_POWERTRACE_SETUP Setup;
  JLINK_POWERTRACE_CAPS  Caps;
  JLINK_POWERTRACE_DATA_ITEM * pData;
  JLINK_POWERTRACE_DATA_ITEM * pTemp;
  JLINK_POWERTRACE_CHANNEL_CAPS_IN  ChannelCapsIn;
  JLINK_POWERTRACE_CHANNEL_CAPS_OUT ChannelCapsOut;
  JLINKARM_SWO_START_INFO SWOStartInfo;
  const char* sErr;
  HANDLE hFile;
  U32 ChannelMask;
  int NumItemsRead;
  int NumChannelsActive;
  U32 NumItemsReadTotal;
  U8  ac[512];
  U8 acFile[MAX_PATH];
  int r;
  int i;
  U32 Data;

  hFile = INVALID_HANDLE_VALUE;
  pData = NULL;
  memset(&Setup, 0, sizeof(Setup));
  memset(&Caps, 0, sizeof(Caps));
  memset(&SWOStartInfo, 0, sizeof(SWOStartInfo));
  //
  // Select defaults for powertrace parameters
  //
  strcpy(acFile, FILE_NAME_POWERTRACE_TEST);
  Setup.ChannelMask = 1;  // 
  Setup.RefSelect   = POWERTRACE_REFC_NO_REFC;  // No reference counter
  Setup.SampleFreq  = 0;  // Max.
  Setup.EnableCond  = 0;  // Non-stop collecting of data
  //
  // Parse parameters
  //
  //
  // Para <LogFile>
  //
  _EatWhite(&s);
  if (*s) {
    _ParseString(&s, acFile, sizeof(acFile));
  }
  //
  // Para <ChannelMask>
  //
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  _EatWhite(&s);
  if (*s) {
    sErr = _ParseHex(&s, &Setup.ChannelMask);
    if (sErr) {
      _ReportOutf("ERROR: %s\n", sErr);
      r = JLINK_ERROR_SYNTAX;
      goto Done;
    }
  }
  //
  // Para <RefCountSel>
  //
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  _EatWhite(&s);
  if (*s) {
    sErr = _ParseHex(&s, &Setup.RefSelect);
    if (sErr) {
      _ReportOutf("ERROR: %s\n", sErr);
      r = JLINK_ERROR_SYNTAX;
      goto Done;
    }
  }
  //
  // Check J-Link caps
  //
  if (JLINKARM_EMU_HasCapEx(JLINKARM_EMU_CAP_EX_POWERTRACE) == 0) {
    _ReportOutf("ERROR: POWERTRACE is not supported by connected J-Link.\n");
    r = JLINK_ERROR_SYNTAX;
    goto Done;
  }
  //
  // Print selected settings
  //
  _ReportOutf("---Selected settings---\n\nLogFile: %s\nChannelMask: 0x%.8X\nSampling frequency: %d %s\nReference counter selection: %d\n\n",
         acFile,
         Setup.ChannelMask,
         Setup.SampleFreq,
         Setup.SampleFreq ? "Hz" : "(Max)",
         Setup.RefSelect
        );
  //
  // Print generic J-Link POWERTRACE capabilities
  //
  Caps.SizeOfStruct = sizeof(JLINK_POWERTRACE_CAPS);
  JLINK_POWERTRACE_Control(JLINK_POWERTRACE_CMD_GET_CAPS, NULL, &Caps);
  _ReportOutf("---J-Link capabilities---\n\n");
  _ReportOutf("Channels supported by connected emulator:\n");
  ChannelMask = Caps.ChannelMask;
  i = 0;
  while (ChannelMask) {
    if (ChannelMask & 1) {
      _ReportOutf(" - %s%d\n", (i < 8) ? "Int" : "Ext", (i < 8) ? i : i - 8);
    }
    ChannelMask >>= 1;
    i++;
  }
  if ((Setup.ChannelMask & Caps.ChannelMask) != Setup.ChannelMask) {
    _ReportOutf("ERROR: Connected J-Link does not support selected channel mask.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  NumChannelsActive = 0;
  ChannelMask = Setup.ChannelMask;
  while (ChannelMask) {
    if (ChannelMask & 1) {
      NumChannelsActive++;
    }
    ChannelMask >>= 1;
  }
  //
  // Check if selected channels & frequency are supported by emulator
  //
  ChannelCapsIn.SizeOfStruct  = sizeof(JLINK_POWERTRACE_CHANNEL_CAPS_IN);
  ChannelCapsIn.ChannelMask = Setup.ChannelMask;
  ChannelCapsOut.SizeOfStruct = sizeof(JLINK_POWERTRACE_CHANNEL_CAPS_OUT);
  JLINK_POWERTRACE_Control(JLINK_POWERTRACE_CMD_GET_CHANNEL_CAPS, &ChannelCapsIn, &ChannelCapsOut);
  if (Setup.SampleFreq > (ChannelCapsOut.BaseFreq / ChannelCapsOut.MinDiv)) {
    _ReportOutf("Selected sampling frequency is not supported for selected setup, by connected J-Link.\n");
  }
  //
  // Setup J-Link for POWERTRACE
  //
  hFile = _OpenFile(acFile, FILE_FLAG_WRITE | FILE_FLAG_CREATE | FILE_FLAG_TRUNC);   // Create logfile for POWERTRACE data
  if (hFile == INVALID_HANDLE_VALUE) {
    _ReportOutf("ERROR: Could not open file for writing.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  //
  // Setup emulator for POWERTRACE and print the setup information
  //
  Setup.SizeOfStruct = sizeof (JLINK_POWERTRACE_SETUP);
  if (Setup.SampleFreq == 0) {                                              // Max. frequency selected?
    Setup.SampleFreq = ChannelCapsOut.BaseFreq / ChannelCapsOut.MinDiv;
  }
  r = JLINK_POWERTRACE_Control(JLINK_POWERTRACE_CMD_SETUP, &Setup, NULL);
  if (r < 0) {
    _ReportOutf("ERROR: Unspecified error occurred while setting up POWERTRACE\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  _ReportOutf(" ChannelMask: 0x%.8X\n Sampling frequency: %d Hz\n RefValue select: %d\n EnableCond: %d\n", Setup.ChannelMask, r, Setup.RefSelect, Setup.EnableCond);
  sprintf(ac, "POWERTRACE Test\n--Settings--\nChannelMask: 0x%.8X\nSampling frequency: %d Hz\nRefValue select: %d\nEnableCond: %d\n", Setup.ChannelMask, r, Setup.RefSelect, Setup.EnableCond);
  _WriteFile(hFile, ac, strlen(ac));
  //
  // Select SWD as target interface in order to be able to use SWO
  // in case reference counter type 1 has been selected
  //
  if (Setup.RefSelect == POWERTRACE_REFC_SWO_NUMBYTES_TRANSMITTED) {
    if (JLINKARM_TIF_Select(JLINKARM_TIF_SWD) != 0) {
      _ReportOutf("SWO byte count selected as reference count but SWD/SWO is not supported by connected emulator.\n");
      r = JLINK_ERROR_UNKNOWN;
      goto Done;
    }
    JLINKARM_SetSpeed(4000);
    //
    // Reset target to get it in a defined state
    //
    JLINKARM_Reset();
    JLINKARM_WriteU32(0xE000EDFC, 0x01000000);    // Debug Exception and Monitor Control Register.
   	JLINKARM_WriteU32(0xE0042004, 0x00000027);    //
    JLINKARM_WriteU32(0xE00400F0, 0x00000002);    // "Selected PIN Protocol Register": Select which protocol to use for trace output (2: SWO)
    Data = (72000000 / POWERTRACE_SWO_DEFAULT_SPEED) - 1;
    JLINKARM_WriteU32(0xE0040010, Data);          // Async Clock Prescaler Register (72/144 = 500 kHz)
    JLINKARM_WriteU32(0xE0000FB0, 0xC5ACCE55);    // ITM Lock Access Register, 0xC5ACCE55 enables more write access to Control Register 0xE00 :: 0xFFC. Invalid write removes write access
    JLINKARM_WriteU32(0xE0000E80, 0x0001000D);    // ITM Trace Control Register. Configures and controls ITM transfers
    JLINKARM_WriteU32(0xE0000E40, 0x0000000F);    // ITM Trace Privilege Register
    JLINKARM_WriteU32(0xE0000E00, 0x00000001);    // ITM Trace Enable Register. Enable tracing on stimulus ports. One bit per stimulus port.
    JLINKARM_WriteU32(0xE0001000, 0x400003FE);    // DWT Control Register
    JLINKARM_WriteU32(0xE0040304, 0x00000100);    // Formatter and Flush Control Register
    //
    // Start SWO
    //
    SWOStartInfo.SizeofStruct = sizeof(SWOStartInfo);
    SWOStartInfo.Interface    = JLINKARM_SWO_IF_UART;
    SWOStartInfo.Speed        = POWERTRACE_SWO_DEFAULT_SPEED;
    JLINKARM_SWO_Control(JLINKARM_SWO_CMD_START, &SWOStartInfo);
  }
  //
  // Allocate buffer for POWERTRACE data
  //
  pData = malloc(sizeof(JLINK_POWERTRACE_DATA_ITEM) * 500);
  if (pData == NULL) {
    _ReportOutf("ERROR: Could not allocate buffer for POWERTRACE data.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  //
  // Start POWERTRACE
  //
  NumItemsReadTotal = 0;
  _ReportOutf("Press any key to start POWERTRACE (press any key to stop)...");
  getch();
  _ReportOutf("\n");
  JLINK_POWERTRACE_Control(JLINK_POWERTRACE_CMD_START, NULL, NULL);
  //
  // Let the CPU run
  //
  if (Setup.RefSelect == POWERTRACE_REFC_SWO_NUMBYTES_TRANSMITTED) {
    JLINKARM_Go();
  }
  //
  // Get POWERTRACE data periodically
  //
  while (1) {
    NumItemsRead = JLINK_POWERTRACE_Read(pData, 500);
    if (NumItemsRead < 0) {
      _ReportOutf("Error while reading POWERTRACE data.\n");
      break;
    }
    NumItemsReadTotal += NumItemsRead;
    _ReportOutf("\r%d items read total", NumItemsReadTotal);
    sprintf(ac, "%d items read:\n", NumItemsRead);
    _WriteFile(hFile, ac, strlen(ac));
    pTemp = pData;
    while (NumItemsRead) {
      ChannelMask = Setup.ChannelMask;
      i = 0;
      while (ChannelMask) {
        if (ChannelMask & 1) {
          sprintf(&ac[0], "Channel %d: Data: %d, RefVal: 0x%.4X\n", i, pTemp->Data, pTemp->RefValue);
          _WriteFile(hFile, ac, strlen(ac));
          pTemp++;
          NumItemsRead--;
        }
        i++;
        ChannelMask >>= 1;
      }
    }
    if (_kbhit() != 0) {
      _getch();
      break;
    }
    _Sleep(20);   // Give other threads some time to perform their action. This also allows our POWERTRACE thread in the DLL to collect some more data
  }
  //
  // Stop POWERTRACE
  //
  r = JLINK_POWERTRACE_Control(JLINK_POWERTRACE_CMD_STOP, NULL, NULL);
  if (r < 0) {
    _ReportOutf("\nFailed to stop POWERTRACE\n");
  } else {
    _ReportOutf("\nPOWERTRACE stopped!\n");
  }
  //
  // Stop SWO
  //
  if (Setup.RefSelect == POWERTRACE_REFC_SWO_NUMBYTES_TRANSMITTED) {
    JLINKARM_SWO_Control(JLINKARM_SWO_CMD_STOP, NULL);
    JLINKARM_SWO_Control(JLINKARM_SWO_CMD_FLUSH, NULL);
  }
  //
  // Close file handle and free POWERTRACE item buffer
  //
  r = JLINK_NOERROR;
Done:
  if (hFile != INVALID_HANDLE_VALUE) {
    _CloseFile(hFile);
  }
  if (pData) {
    free(pData);
  }
  return r;
}

/*********************************************************************
*
*       _ExecRawTrace
*/
static int _ExecRawTrace(const char* s) {
  RAWTRACE_Exec();
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecEMUCOMWrite
*/
static int _ExecEMUCOMWrite(const char* s) {
  U8  abData[MAX_NUM_WRITE_ITEMS];
  U32 Channel;
  U32 NumBytes;
  int NumBytesWritten;
  U32 Data;
  int Offset = 0;

  if (_ParseHex(&s, &Channel)) {
    _ReportOutf("Syntax: ECWrite <Channel>, <Data>\n");
    return JLINK_ERROR_SYNTAX;
  }
  NumBytes = 0;
  while (1) {
    if (NumBytes > MAX_NUM_WRITE_ITEMS) {
      _ReportOutf("This command can only handle up to %d bytes.\n", MAX_NUM_WRITE_ITEMS);
      return JLINK_ERROR_UNKNOWN;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (_ParseHex(&s, &Data)) {
      if (NumBytes == 0) {
        _ReportOutf("Syntax: ECWrite <Channel>, <Data>\n");
        return JLINK_ERROR_SYNTAX;
      }
      break;
    }
    abData[NumBytes] = Data & 0xFF;
    NumBytes++;
  }
  if (JLINKARM_EMU_COM_IsSupported() <= 0) {
    _ReportOutf("EMUCOM is not supported by connected emulator.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  NumBytesWritten = JLINKARM_EMU_COM_Write(Channel, NumBytes, &abData[0]);
  if (NumBytesWritten >= 0) {
    _ReportOutf("%d bytes written successfully to channel 0x%.2X.\n", NumBytesWritten, Channel);
    return JLINK_NOERROR;
  } else if (NumBytesWritten == (int)JLINKARM_EMUCOM_ERR_CHANNEL_NOT_SUPPORTED) {
    _ReportOutf("Channel 0x%.2X is not supported by connected emulator.\n", Channel);
  } else {
    _ReportOutf("Failed to write to channel 0x%.2X.\n", Channel);
  }
  return JLINK_ERROR_UNKNOWN;
}

/*********************************************************************
*
*       _ExecEMUCOMRead
*/
static int _ExecEMUCOMRead(const char* s) {
  U32 Channel;
  U32 NumBytes;
  int NumBytesRead;
  U8* pData0;
  U8* pData;
  int Offset = 0;
  int r = JLINK_ERROR_UNKNOWN;

  if (_ParseHex(&s, &Channel)) {
    _ReportOutf("Syntax: ECRead <Channel>, <NumBytes>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &NumBytes)) {
    _ReportOutf("Syntax: ECRead <Channel>, <NumBytes>\n");
    return JLINK_ERROR_SYNTAX;
  }
  if (NumBytes > 0x10000) {
    _ReportOutf("NumBytes should be <= 0x10000\n");
    return JLINK_ERROR_UNKNOWN;
  }
  if (JLINKARM_EMU_COM_IsSupported() <= 0) {
    _ReportOutf("EMUCOM is not supported by connected emulator.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  pData0 = pData = malloc(NumBytes);
  if (pData0 == NULL) {
    _ReportOutf("Failed to allocate memory.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  NumBytesRead = JLINKARM_EMU_COM_Read(Channel, NumBytes, pData);
  if (NumBytesRead >= 0) {
    _ReportOutf("%d bytes read successfully from channel 0x%.2X:\n", NumBytesRead, Channel);
    while (NumBytesRead > 0) {
      int NumBytesPerLine;
      _ReportOutf("%.8X = ", Offset);
      NumBytesPerLine = (NumBytesRead > 16) ? 16 : NumBytesRead;
      NumBytesRead   -= NumBytesPerLine;
      Offset         += NumBytesPerLine;
      for (; NumBytesPerLine > 0; NumBytesPerLine--) {
        _ReportOutf("%.2X ", *pData++);
      }
      _ReportOutf("\n");
    }
    r = JLINK_NOERROR;
  } else if (NumBytesRead == 0) {
    _ReportOutf("No bytes available on channel 0x%.2X.\n", Channel);
    r = JLINK_NOERROR;
  } else if (NumBytesRead == (int)JLINKARM_EMUCOM_ERR_CHANNEL_NOT_SUPPORTED) {
    _ReportOutf("Channel 0x%.2X is not supported by connected emulator.\n", Channel);
  } else if ((NumBytesRead & 0xFF000000) == JLINKARM_EMUCOM_ERR_BUFFER_TOO_SMALL) {
    _ReportOutf("Buffer too small. %d bytes available for reading from channel 0x%.2X.\n", NumBytesRead & 0xFFFFFF, Channel);
  } else {
    _ReportOutf("Failed to read from channel 0x%.2X.\n", Channel);
  }
  free(pData0);
  return r;
}

/*********************************************************************
*
*       _ExecEMUCOMIsSupported
*/
static int _ExecEMUCOMIsSupported(const char* s) {
  int r;

  r = JLINKARM_EMU_COM_IsSupported();
  if (r > 0) {
    _ReportOutf("EMUCOM is supported by connected emulator.\n");
  } else if (r == 0) {
    _ReportOutf("EMUCOM is not supported by connected emulator.\n");
    return JLINK_ERROR_UNKNOWN;
  } else {
    _ReportOutf("Failed to determine if EMUCOM is supported by emulator.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecFileWrite
*/
static int _ExecFileWrite(const char* s) {
  char acEmuFile[MAX_PATH];
  char acHostFile[MAX_PATH];
  char acErr[512];
  HANDLE hFile;
  U32 Offset   = 0;
  U32 NumBytes = 0;
  U32 NumBytesRead;
  U32 NumBytesWritten;
  U32 NumBytesAtOnce;
  U8* pData;
  int r;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if ((*s == 0) || (*s == '\r') || (*s == '\n') || (*s == ',')) {
    _ReportOutf("Syntax: fwrite <EmuFile> <HostFile> [<Offset> [<NumBytes>]]\n");
    return JLINK_ERROR_SYNTAX;
  }
  _ParseString(&s, acEmuFile, sizeof(acEmuFile));
  _EatWhite(&s);
  _ParseString(&s, acHostFile, sizeof(acHostFile));
  _EatWhite(&s);
  if (*s) {
    if (*s == ',') {
      s++;
    }
    if (_ParseHex(&s, &Offset)) {
      _ReportOutf("Syntax: fwrite <EmuFile> <HostFile> [<Offset> [<NumBytes>]]\n");
      return JLINK_ERROR_SYNTAX;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (*s) {
      if (_ParseHex(&s, &NumBytes)) {
        _ReportOutf("Syntax: fwrite <EmuFile> <HostFile> [<Offset> [<NumBytes>]]\n");
        return JLINK_ERROR_SYNTAX;
      }
    }
  }
  //
  // Check if file I/O is supported
  //
  if ((JLINKARM_GetEmuCaps() & JLINKARM_EMU_CAP_FILE_IO) == 0) {
    _ReportOutf("File I/O is not supported by connected emulator.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Read from source and write to destination file
  //
  acErr[0] = 0;
  NumBytesWritten = 0;
  hFile = _OpenFile(acHostFile, FILE_FLAG_READ | FILE_FLAG_SHARE_READ);
  if (hFile != INVALID_HANDLE_VALUE) {
    if (NumBytes == 0) {
      NumBytes = _GetFileSize(hFile);
    }
    JLINKARM_EMU_FILE_Write(acEmuFile, NULL, NumBytes, 0);
    pData = (char*)malloc(MAX_BLOCK_SIZE_FILE_IO);
    if (pData) {
      while (NumBytes) {
        NumBytesAtOnce = MIN(NumBytes, MAX_BLOCK_SIZE_FILE_IO);
        NumBytesRead   = _ReadFile(hFile, pData, NumBytesAtOnce);
        if (NumBytesRead == -1) {
          strcpy(acErr, "ERROR: Could not read from source file.\n");
          break;
        }
        if (NumBytesRead != NumBytesAtOnce) {
          strcpy(acErr, "ERROR: Could not read from source file.\n");
          break;
        }
        r = JLINKARM_EMU_FILE_Write(acEmuFile, pData, Offset, NumBytesAtOnce);
        if (r < 0) {
          strcpy(acErr, "ERROR: Could not write to destination file.\n");
          break;
        }
        NumBytesWritten += r;
        Offset          += r;
        NumBytes        -= r;
        if (r != (int)NumBytesAtOnce) {
          break;
        }
      }
      free(pData);
    } else {
      strcpy(acErr, "ERROR: Could not allocate file buffer.\n");
    }
    _CloseFile(hFile);
  } else {
    strcpy(acErr, "ERROR: Could not open source file.\n");
  }
  //
  // Display result
  //
  if (*acErr) {
    _ReportOutf(acErr);
    return JLINK_ERROR_UNKNOWN;
  }
  _ReportOutf("%d bytes written successfully.\n", NumBytesWritten);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecFileRead
*/
static int _ExecFileRead(const char* s) {
  char acEmuFile[MAX_PATH];
  char acHostFile[MAX_PATH];
  char acErr[512];
  HANDLE hFile;
  U32 Offset   = 0;
  U32 NumBytes = 0;
  U32 NumBytesRead;
  U32 NumBytesWritten;
  U32 NumBytesAtOnce;
  U8* pData;
  int r;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if ((*s == 0) || (*s == '\r') || (*s == '\n') || (*s == ',')) {
    _ReportOutf("Syntax: fread <EmuFile> <HostFile> [<Offset> [<NumBytes>]]\n");
    return JLINK_ERROR_SYNTAX;
  }
  _ParseString(&s, acEmuFile, sizeof(acEmuFile));
  _EatWhite(&s);
  _ParseString(&s, acHostFile, sizeof(acHostFile));
  _EatWhite(&s);
  if (*s) {
    if (*s == ',') {
      s++;
    }
    if (_ParseHex(&s, &Offset)) {
      _ReportOutf("Syntax: fread <EmuFile> <HostFile> [<Offset> [<NumBytes>]]\n");
      return JLINK_ERROR_SYNTAX;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (*s) {
      if (_ParseHex(&s, &NumBytes)) {
        _ReportOutf("Syntax: fread <EmuFile> <HostFile> [<Offset> [<NumBytes>]]\n");
        return JLINK_ERROR_SYNTAX;
      }
    }
  }
  //
  // Check if file I/O is supported
  //
  if ((JLINKARM_GetEmuCaps() & JLINKARM_EMU_CAP_FILE_IO) == 0) {
    _ReportOutf("File I/O is not supported by connected emulator.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Read from source and write to destination file
  //
  if (NumBytes == 0) {
    NumBytes = JLINKARM_EMU_FILE_GetSize(acEmuFile);
    if (NumBytes == 0xFFFFFFFF) {
      _ReportOutf("ERROR: Source file does not exist.\n");
      return JLINK_ERROR_UNKNOWN;
    }
  }
  acErr[0] = 0;
  NumBytesRead = 0;
  hFile = _OpenFile(acHostFile, FILE_FLAG_WRITE | FILE_FLAG_CREATE | FILE_FLAG_TRUNC);
  if (hFile != INVALID_HANDLE_VALUE) {
    pData = (char*)malloc(MAX_BLOCK_SIZE_FILE_IO);
    if (pData) {
      while (NumBytes) {
        NumBytesAtOnce = MIN(NumBytes, MAX_BLOCK_SIZE_FILE_IO);
        r = JLINKARM_EMU_FILE_Read(acEmuFile, pData, Offset, NumBytesAtOnce);
        if (r < 0) {
          strcpy(acErr, "ERROR: Could not read from source file.\n");
          break;
        }
        NumBytesWritten = _WriteFile(hFile, pData, NumBytes);
        if (NumBytesWritten == -1) {
          strcpy(acErr, "ERROR: Could not write to destination file.\n");
          break;
        }
        if (NumBytesWritten != (U32)r) {
          strcpy(acErr, "ERROR: Could not write to destination file.\n");
          break;
        }
        NumBytesRead += r;
        Offset       += r;
        NumBytes     -= r;
        if (r != (int)NumBytesAtOnce) {
          break;
        }
      }
      free(pData);
    } else {
      strcpy(acErr, "ERROR: Could not allocate file buffer.\n");
    }
    _CloseFile(hFile);
  } else {
    strcpy(acErr, "ERROR: Could not open destination file.\n");
  }
  //
  // Display result
  //
  if (*acErr) {
    _ReportOutf(acErr);
    return JLINK_ERROR_UNKNOWN;
  }
  _ReportOutf("%d bytes read successfully.\n", NumBytesRead);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecFileShow
*/
static int _ExecFileShow(const char* s) {
  char acFile[MAX_PATH];
  U32 Offset   = 0;
  U32 NumBytes = 0x100;
  int NumBytesRead;
  U8* pData;
  U8* pData0;
  int r = JLINK_NOERROR;
  int ShowText = 0;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if ((*s == 0) || (*s == '\r') || (*s == '\n') || (*s == ',')) {
    _ReportOutf("Syntax: fshow <FileName> [<Offset> [<NumBytes>]]\n");
    return JLINK_ERROR_SYNTAX;
  }
  if ((*s == '-') && (*(s+1) == 'a')) {
    s += 2;
    ShowText = 1;
    _EatWhite(&s);
  }
  _ParseString(&s, acFile, sizeof(acFile));
  _EatWhite(&s);
  if (*s) {
    if (*s == ',') {
      s++;
    }
    if (_ParseHex(&s, &Offset)) {
      _ReportOutf("Syntax: fshow <FileName> [<Offset> [<NumBytes>]]\n");
      return JLINK_ERROR_SYNTAX;
    }
    _EatWhite(&s);
    if (*s) {
      if (*s == ',') {
        s++;
      }
      if (_ParseHex(&s, &NumBytes)) {
        _ReportOutf("Syntax: fshow <FileName> [<Offset> [<NumBytes>]]\n");
        return JLINK_ERROR_SYNTAX;
      }
    }
  }
  if (NumBytes > 0x10000) {
    _ReportOutf("NumBytes should be <= 0x10000\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Check if file I/O is supported
  //
  if ((JLINKARM_GetEmuCaps() & JLINKARM_EMU_CAP_FILE_IO) == 0) {
    _ReportOutf("File I/O is not supported by connected emulator.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Read data
  //
  pData0 = pData = malloc(NumBytes);
  NumBytesRead = JLINKARM_EMU_FILE_Read(acFile, pData, Offset, NumBytes);
  if (NumBytesRead < 0) {
    _ReportOutf("ERROR: Could not read file.\n");
    r = JLINK_ERROR_UNKNOWN;
    goto Done;
  }
  //
  // Display data
  //
  if (JLINKARM_HasError() == 0) {
    _ReportOutf("%d bytes read successfully:\n", NumBytesRead);
    if (ShowText) {
      *(pData + NumBytes - 1) = 0;
      _ReportOutf(pData);
    } else {
      while (NumBytesRead > 0) {
        int NumBytesPerLine;
        _ReportOutf("%.8X = ", Offset);
        NumBytesPerLine = (NumBytesRead > 16) ? 16 : NumBytesRead;
        NumBytesRead -= NumBytesPerLine;
        Offset       += NumBytesPerLine;
        for (; NumBytesPerLine > 0; NumBytesPerLine--) {
          _ReportOutf("%.2X ", *pData++);
        }
        _ReportOutf("\n");
      }
    }
  } else {
    r = JLINK_ERROR_UNKNOWN;
  }
Done:
  free(pData0);
  return r;
}

/*********************************************************************
*
*       _ExecFileDelete
*/
static int _ExecFileDelete(const char* s) {
  char acFile[MAX_PATH];
  int r;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if ((*s == 0) || (*s == '\r') || (*s == '\n') || (*s == ',')) {
    _ReportOutf("Syntax: fdelete <FileName>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _ParseString(&s, acFile, sizeof(acFile));
  //
  // Check if file I/O is supported
  //
  if ((JLINKARM_GetEmuCaps() & JLINKARM_EMU_CAP_FILE_IO) == 0) {
    _ReportOutf("File I/O is not supported by connected emulator.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Delete file
  //
  r = JLINKARM_EMU_FILE_Delete(acFile);
  if (r != 0) {
    _ReportOutf("ERROR: Could not delete file.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  _ReportOutf("File deleted successfully.\n");
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecFileSize
*/
static int _ExecFileSize(const char* s) {
  char acFile[MAX_PATH];
  int FileSize;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if ((*s == 0) || (*s == '\r') || (*s == '\n') || (*s == ',')) {
    _ReportOutf("Syntax: fsize <FileName>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _ParseString(&s, acFile, sizeof(acFile));
  //
  // Check if file I/O is supported
  //
  if ((JLINKARM_GetEmuCaps() & JLINKARM_EMU_CAP_FILE_IO) == 0) {
    _ReportOutf("File I/O is not supported by connected probe.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Read file size
  //
  FileSize = JLINKARM_EMU_FILE_GetSize(acFile);
  if (FileSize < 0) {
    _ReportOutf("ERROR: Could not read file size.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  _ReportOutf("Size of file: %d bytes\n", FileSize);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecFileList
*/
static int _ExecFileList(const char* s) {
  char acBuffer[0x400];
  char acFile[MAX_PATH];
  int r;
  //
  // Parse parameters
  //
  acFile[0] = 0;
  _EatWhite(&s);
  if (*s) {
    _ParseString(&s, acFile, sizeof(acFile));
  }
  //
  // Read and display directory
  //
  r = JLINKARM_EMU_FILE_GetList(acFile, acBuffer, sizeof(acBuffer));
  if (r < 0) {
    if (r == JLINK_ERR_EMU_FEATURE_NOT_SUPPORTED) {
      _ReportOutf("This File I/O command is not supported by the connected probe.\n");
    } else {
      _ReportOutf("Unknown error (%d) occurred.\n", r);
    }
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Firmware has more bytes to send than our buffer can handle?
  //
  if (r > sizeof(acBuffer)) {
    U8* pTmp;

    pTmp = malloc(r);
    if (pTmp == NULL) {
      _ReportOutf("Failed to allocate temporary buffer (on PC side) for directory tree.\n");
      return JLINK_ERROR_UNKNOWN;
    }
    JLINKARM_EMU_FILE_GetList(acFile, pTmp, r);    // Very unlikely that it suddenly fails when called again. Just assume that it will work
    _ReportOutf("%s\n", pTmp);
    free(pTmp);
  } else {
    _ReportOutf("%s\n", acBuffer);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecFileSecureArea
*/
static int _ExecFileSecureArea(const char* s) {
  char acCommand[64];
  char acErr[128];
  int r;

  _ReportOutf("Syntax: SecureArea <Operation>\n<Operation>: Create / Remove\n");
  //
  // Parse parameters
  //
  acCommand[0] = 0;
  _EatWhite(&s);
  if (*s) {
    _ParseString(&s, acCommand, sizeof(acCommand));
  }
  //
  // Perform operation based on parameters parsed
  //
  if (_JLinkstricmp("Create", acCommand) == 0) {
    _ReportOutf("Creating secure area...");
    r = JLINKARM_ExecCommand("SetEmuSecureArea 0x4000000", acErr, sizeof(acErr));
    if (r < 0) {
      _ReportOutf("Failed with error code %d\n", r);
      return JLINK_ERROR_UNKNOWN;
    } else {
      _ReportOutf("O.K.\n");
    }
  } else if (_JLinkstricmp("Remove", acCommand) == 0) {
    _ReportOutf("Removing secure area...");
    r = JLINKARM_ExecCommand("SetEmuSecureArea 0", acErr, sizeof(acErr));
    if (r < 0) {
      _ReportOutf("Failed with error code %d\n", r);
      return JLINK_ERROR_UNKNOWN;
    } else {
      _ReportOutf("O.K.\n");
    }
  } else {
    _ReportOutf("Invalid value for <Operation>\n");
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecShowEmuStatus
*/
static int _ExecShowEmuStatus(const char* s) {
  U32 NumBytes;
  NumBytes = JLINKARM_EMU_GetMaxMemBlock();
  _ReportOutf("Emulator has currently %d bytes of free memory.\n", NumBytes);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecIndicator
*/
static int _ExecIndicator(const char* s) {
  JLINKARM_INDICATOR_CTRL Ctrl;
  U32 Id            = 0;
  U32 Override      = 0;
  U32 InitialOnTime = 0;
  U32 OnTime        = 0;
  U32 OffTime       = 0;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (_ParseDec(&s, &Id)) {
    _ReportOutf("Syntax: indi <Id> <Override> <InitialOnTime> <OnTime> <OffTime>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  _EatWhite(&s);
  if (*s) {
    if (_ParseDec(&s, &Override)) {
      _ReportOutf("Syntax: indi <Id> <Override> <InitialOnTime> <OnTime> <OffTime>\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  _EatWhite(&s);
  if (*s) {
    if (_ParseDec(&s, &InitialOnTime)) {
      _ReportOutf("Syntax: indi <Id> <Override> <InitialOnTime> <OnTime> <OffTime>\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  _EatWhite(&s);
  if (*s) {
    if (_ParseDec(&s, &OnTime)) {
      _ReportOutf("Syntax: indi <Id> <Override> <InitialOnTime> <OnTime> <OffTime>\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  _EatWhite(&s);
  if (*s) {
    if (_ParseDec(&s, &OffTime)) {
      _ReportOutf("Syntax: indi <Id> <Override> <InitialOnTime> <OnTime> <OffTime>\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  //
  // Update indicator
  //
  Ctrl.IndicatorId    = (U16)Id;
  Ctrl.Override       = (U16)Override;
  Ctrl.InitialOnTime  = (U16)InitialOnTime;
  Ctrl.OnTime         = (U16)OnTime;
  Ctrl.OffTime        = (U16)OffTime;
  JLINKARM_INDICATORS_SetState(1, &Ctrl);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecVCatch
*/
static int _ExecVCatch(const char* s) {
  U32 Value = 0;
  //
  // Parse parameters
  //
  _EatWhite(&s);
  if (_ParseHex(&s, &Value)) {
    _ReportOutf("Syntax: VCatch <Value>\n");
    return JLINK_ERROR_SYNTAX;
  }
  //
  // Write vector catch
  //
  JLINKARM_WriteVectorCatch(Value);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSetDebugUnitBlockMask
*/
static int _ExecSetDebugUnitBlockMask(const char* s) {
  U32 Mask;
  int Type;

  if (_ParseHex(&s, &Type)) {
    _ReportOutf("Syntax: sdubm <Type> <Mask>\n");
    return JLINK_ERROR_SYNTAX;
  }
  _EatWhite(&s);
  if (*s == ',') {
    s++;
  }
  if (_ParseHex(&s, &Mask)) {
    _ReportOutf("Syntax: sdubm <Type> <Mask>\n");
    return JLINK_ERROR_SYNTAX;
  }
  JLINKARM_SetDebugUnitBlockMask(Type, Mask);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ShowMOE
*/
static void _ShowMOE(void) {
  JLINKARM_MOE_INFO Info;
  JLINKARM_WP_INFO WPInfo;
  int NumWPs;
  int i;
  int r;

  if (JLINKARM_IsHalted() <= 0) {
    _ReportOutf("CPU is not halted.\n");
    return;
  }
  r = JLINKARM_GetMOEs(&Info, 1);
  if (r > 0) {
    if (Info.HaltReason == JLINKARM_HALT_REASON_DBGRQ) {
      _ReportOutf("CPU halted because DBGRQ was asserted.\n");
    } else if (Info.HaltReason == JLINKARM_HALT_REASON_VECTOR_CATCH) {
      _ReportOutf("CPU halted due to vector catch.\n");
    } else if (Info.HaltReason == JLINKARM_HALT_REASON_DATA_BREAKPOINT) {
      if (Info.Index >= 0) {
        _ReportOutf("CPU halted due to data breakpoint unit %d match.\n", Info.Index);
        WPInfo.SizeOfStruct = sizeof(JLINKARM_WP_INFO);
        NumWPs = JLINKARM_GetWPInfoEx(-1, &WPInfo);
        for (i = 0; i < NumWPs; i++) {
          JLINKARM_GetWPInfoEx(i, &WPInfo);
          if (WPInfo.UnitMask && (1 << Info.Index)) {
            _ReportOutf("Unit %d was used for WP with handlle 0x%.4X.\n", Info.Index, WPInfo.Handle);
            return;
          }
        }
      } else {
        _ReportOutf("CPU halted due to data breakpoint match.\n");
      }
    } else if (Info.HaltReason == JLINKARM_HALT_REASON_CODE_BREAKPOINT) {
      if (Info.Index >= 0) {
        _ReportOutf("CPU halted due to code breakpoint unit %d match.\n", Info.Index);
      } else {
        _ReportOutf("CPU halted due to code breakpoint match.\n");
      }
    } else {
      _ReportOutf("CPU halted for unknown reason.");
    }
  }
}

/*********************************************************************
*
*       _ExecShowMOE
*/
static int _ExecShowMOE(const char* s) {
  char ac[256];

  _EatWhite(&s);
  if (_CompareCmd(&s, "On") == 0) {
    JLINKARM_ExecCommand("EnableMOEHandling", &ac[0], sizeof(ac));
  } else if (_CompareCmd(&s, "Off") == 0) {
    JLINKARM_ExecCommand("DisableMOEHandling", &ac[0], sizeof(ac));
  } else {
    _ShowMOE();
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecGetCounters
*/
static int _ExecGetCounters(const char* s) {
  U32 aCnt[4];

  JLINKARM_EMU_GetCounters(15, &aCnt[0]);
  _ReportOutf("aCnt[0] = 0x%.8X\n", aCnt[0]);
  _ReportOutf("aCnt[1] = 0x%.8X\n", aCnt[1]);
  _ReportOutf("aCnt[2] = 0x%.8X\n", aCnt[2]);
  _ReportOutf("aCnt[3] = 0x%.8X\n", aCnt[3]);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTest
*/
static int _ExecTest(const char* s) {
#ifdef _DEBUG
  U32 Count = 0;

  while (Count < 1) {
    if (!JLINKARM_IsOpen()) {
      JLINKARM_Open();
    }
    if (JLINKARM_IsOpen()) {
      JLINKARM_Close();
    }
    Count++;
  }
#endif
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecCalibrate
*/
static int _ExecCalibrate(const char* s) {
  int IAvg;   // uA
  int IMin;   // uA
  int IMax;   // uA
  int IDiff;  // uA
  int IOffsNew;  // uA
  int IOffsOld;  // uA
  int ICur;   // uA
  int uA = 1000;
  int Tmp;
  char ac[256];

  _ReportOutf("Please disconnect emulator from target.\n");
	 _ReportOutf("Press any key to start the calibration...\n");
  getch();
  _ReportOutf("Calibrating...");
  JLINKARM_ExecCommand("SupplyPower = 1", &ac[0], sizeof(ac));
//  _Sleep(1000);
  //
  // Measure the actual current
  //
  _MeasureITarget(&IAvg, &IMin, &IMax);
  IDiff = abs(IMax - IMin);
  //
  // Fail if the current fluctuates too much
  //
  if (IDiff > (2 * ITARGET_MAX_FLUCTUATION)) {
    _ReportOutf("\nERROR: Current fluctuates too much Min=%gmA Max=%gmA Diff=%gmA!\n",
      (double)IMin / (double)uA, 
      (double)IMax / (double)uA, 
      (double)IDiff / (double)uA);
    goto OnError;
  }
  JLINKARM_ReadEmuConfigMem((U8*)&IOffsOld, ITARGET_OFFSET_ADDR, sizeof(IOffsOld));
  //
  // MSB is set if the target was *NOT* calibrated
  //
  if (IOffsOld & ITARGET_OFFSET_FLAG) {
    IOffsNew = 0;
  } else {
    _ReportOutf("Emulator already calibrated.\n");
    //
    // Extend the sign
    //
    IOffsNew = IOffsOld;
    IOffsNew <<= 1;
    IOffsNew >>= 1;
  }
  IOffsNew += IAvg;
  Tmp = IOffsNew & (~ITARGET_OFFSET_FLAG);  // Upper bit cleared means for J-Link: Offset is valid
  if (Tmp != IOffsOld) {
    _ReportOutf("Emulator calibrated.\n");
    //
    // Clear MSB to indicate that the offset is valid
    //
    JLINKARM_WriteEmuConfigMem((const U8*)&Tmp, ITARGET_OFFSET_ADDR, sizeof(Tmp));
//    _Sleep(1000);
  } else {
    _ReportOutf("No re-calibration necessary.\n");
  }
  //
  // Measure again the power to check if the calibration was OK
  //
  _MeasureITarget(&ICur, NULL, NULL);
  if (abs(ICur) > ITARGET_MAX_FLUCTUATION) {
    _ReportOutf("\nERROR: Current deviates too much from zero Min=%gmA Max=%gmA Cur=%gmA!\n",
      (double)IMin / (double)uA, 
      (double)IMax / (double)uA, 
      (double)ICur / (double)uA);
    goto OnError;
  }
  //
  // Sign extend IOffs again to be able to perform a correct output of the variable
  //
  _ReportOutf("Min=%gmA Max=%gmA Avg=%gmA Offs=%gmA...DONE\n", 
    (double)IMin / (double)uA, 
    (double)IMax / (double)uA,
    (double)IAvg / (double)uA,
    (double)IOffsNew / (double)uA);
OnError:
  JLINKARM_ExecCommand("SupplyPower = 0", &ac[0], sizeof(ac));
  return JLINK_NOERROR;
}


/*********************************************************************
*
*       _ShowAndSelectEmu
*
*  Function description
*    Searches for J-Links which are connected to the host PC
*    and prints a list of all emulators which have been found.
*    Depending on the value of "SelectEmu" the user can connect
*    to one of the emulators or just the list is shown.
*
*  Return value
*    0  O.K.
*   -1  Generic error
*   -2  Syntax error
*/
static int _ShowAndSelectEmu(const char * sIn, char SelectEmu) {
  int r;
  int i;
  char NeedDealloc;
  char ac[128];
  const char * s;
  U32 Index;
  U32 Interface;
  JLINKARM_EMU_CONNECT_INFO * paConnectInfo;
  JLINKARM_EMU_CONNECT_INFO aConnectInfo[50];
  unsigned WasConnectedToJLink;
  unsigned WasConnectedToTarget;
  COMMANDER_SETTINGS* pCommanderSettings;

  pCommanderSettings = &_CommanderSettings;
  Interface = 0;
  do {
    _ParseString(&sIn, ac, sizeof(ac));
    if (ac[0] == 0) {
      if (Interface == 0) {
        Interface = JLINKARM_HOSTIF_USB;   // If no interfaces are selected, select USB only.
      }
      break;
    } else if (_JLinkstricmp(ac, "usb") == 0) {
      Interface |= JLINKARM_HOSTIF_USB;
    } else if (_JLinkstricmp(ac, "ip") == 0) {
      Interface |= JLINKARM_HOSTIF_IP;
    } else {
      _ReportOutf("Unknown interface.\n");
      Interface = 0;
      break;
    }
  } while (1);
  if (Interface == 0) {
    return -2;  // Syntax error
  }
  //
  // Request emulator list
  //
  r = JLINKARM_EMU_GetList(Interface, &aConnectInfo[0], COUNTOF(aConnectInfo));
  //
  // Allocate memory for emulator info buffer if local buffer is not big enough
  //
  NeedDealloc = 0;
  if (r > COUNTOF(aConnectInfo)) {
    paConnectInfo = malloc(r * sizeof(JLINKARM_EMU_CONNECT_INFO));
    if (paConnectInfo == NULL) {
      _ReportOutf("Failed to allocate memory for emulator info buffer.\n");
      return -1;
    }
    NeedDealloc = 1;
    JLINKARM_EMU_GetList(Interface, paConnectInfo, r);
  } else {
    paConnectInfo = &aConnectInfo[0];
  }
  for (i = 0; i < r; i++) {
    if ((paConnectInfo + i)->Connection == JLINKARM_HOSTIF_USB) {
      _ReportOutf("J-Link[%d]: Connection: USB, Serial number: %d, ProductName: %s\n", i, (paConnectInfo + i)->SerialNumber, (paConnectInfo + i)->acProduct);
    } else {
      _ReportOutf("J-Link[%d]: Connection: IP,  Serial number: %d, IP: %d.%d.%d.%d, ProductName: %s\n", i, (paConnectInfo + i)->SerialNumber, (paConnectInfo + i)->aIPAddr[0], (paConnectInfo + i)->aIPAddr[1], (paConnectInfo + i)->aIPAddr[2], (paConnectInfo + i)->aIPAddr[3], (paConnectInfo + i)->acProduct);
    }
  }
  if (SelectEmu) {
    //
    // Select one emulator from the list in order to connect to it
    //
    s = _ConsoleGetString("Select emulator index: ", ac, sizeof(ac));
    s = _ParseDec(&s, &Index);
    if (s == NULL) {
      //
      // Disconnect from target + J-Link
      // Update commander settings
      // Reconnect to J-Link + target
      //
      WasConnectedToJLink = pCommanderSettings->Status.ConnectedToJLink;
      WasConnectedToTarget = pCommanderSettings->Status.ConnectedToTarget;
      if (WasConnectedToTarget) {
        _DisconnectFromTarget(pCommanderSettings);
      }
      if (WasConnectedToJLink) {
        _DisconnectFromJLink(pCommanderSettings);
      }
      if ((paConnectInfo + Index)->Connection == JLINKARM_HOSTIF_USB) {
        //
        // USB selected?
        //
        pCommanderSettings->HostIF      = JLINKARM_HOSTIF_USB;
        pCommanderSettings->acIPAddr[0] = 0;
        if ((paConnectInfo + Index)->SerialNumber == 123456) {     // Older J-Links enumerate with this serial number
          pCommanderSettings->EmuSN = (paConnectInfo + Index)->USBAddr;
        } else {
          pCommanderSettings->EmuSN = (paConnectInfo + Index)->SerialNumber;
        }
      } else {
        //
        // IP selected?
        //
        sprintf(ac, "%d.%d.%d.%d", (paConnectInfo + Index)->aIPAddr[0], (paConnectInfo + Index)->aIPAddr[1], (paConnectInfo + Index)->aIPAddr[2], (paConnectInfo + Index)->aIPAddr[3]);
        pCommanderSettings->HostIF = JLINKARM_HOSTIF_IP;
        pCommanderSettings->EmuSN    = 0;
        strcpy(&pCommanderSettings->acIPAddr[0], ac);
      }
      r = _ConnectToJLink(pCommanderSettings);
      if (WasConnectedToTarget && (r >= 0)) {
        r = _ConnectToTarget(pCommanderSettings);
      }
    } else {
      _ReportOutf("%s\n", s);
    }
  }
  if (NeedDealloc) {
    free(paConnectInfo);
  }
  return 0;
}

/*********************************************************************
*
*       _ExecShowEmuList
*
*  Function description
*    Searches for J-Links which are connected to the host PC
*    and gives the user a list of all J-Links which have been found,
*    from which he can select the one he wants to talk to.
*
*/
static int _ExecShowEmuList(const char* sIn) {
  int r;

  r = _ShowAndSelectEmu(sIn, 0);
  if (r < 0) {
    if (r == -1) {
      return JLINK_ERROR_UNKNOWN;
    } else if (r == -2) {
      _ReportOutf("Syntax: ShowEmuList [<Interface0> <Interface1> ...]\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSelectEmuFromList
*
*  Function description
*    Searches for J-Links which are connected to the host PC
*    and gives the user a list of all J-Links which have been found,
*    from which he can select the one he wants to talk to.
*
*/
static int _ExecSelectEmuFromList(const char* sIn) {
  int r;

  r = _ShowAndSelectEmu(sIn, 1);
  if (r < 0) {
    if (r == -1) {
      return JLINK_ERROR_UNKNOWN;
    } else if (r == -2) {
      _ReportOutf("Syntax: selemu [<Interface0> <Interface1> ...]\n");
      return JLINK_ERROR_SYNTAX;
    }
  }
  return JLINK_NOERROR;
}  

/*********************************************************************
*
*       _ExecCDCSetHookFuncs
*
*  Function description
*    Set custom handlers for CDC functions on the J-Link.
*    We get an image from the PC which contains a signature as well
*    as the hook functions for the CDC Tx & Rx functionality
*/
static int _ExecCDCSetHookFuncs(const char * sIn) {
  char ac[MAX_PATH];
  HANDLE hFile;
  U32 FileSize;
  char* pBuffer;
  int r;

  ac[0] = 0;
  _ParseString(&sIn, ac, sizeof(ac));
  hFile = _OpenFile(ac, FILE_FLAG_READ | FILE_FLAG_SHARE_READ);
  if (hFile == INVALID_HANDLE_VALUE) {
    _ReportOutf("Could not open file for reading.\n");
    return JLINK_ERROR_SYNTAX;
  }
  //
  // Read in image
  //
  FileSize = _GetFileSize(hFile);
  pBuffer  = (char*) malloc(FileSize);
  if (pBuffer == NULL) {
    _ReportOutf("Could not allocate memory for image file.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  _ReportOutf("Reading image... [%s]\n", ac);
  _ReadFile(hFile, pBuffer, FileSize);
  _CloseFile(hFile);
  _ReportOutf("O.K.\n");
  //
  // Transfer image to J-Link
  //
  _ReportOutf("Downloading image to J-Link...");
  r = JLINKARM_CDC_SetHookFuncs(pBuffer, FileSize);
  if (r >= 0) {
    _ReportOutf("O.K.\n");
  } else {
    switch(r) {
    case -2: _ReportOutf("Not enough RAM left on J-Link for image download.\n");                            break;
    case -3: _ReportOutf("Invalid data signature found in image.\n");                                       break;
    case JLINK_ERR_EMU_FEATURE_NOT_SUPPORTED: _ReportOutf("Feature not supported by connected J-Link.\n");  break;
    }
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecSetBMI
*
*  Function description
*    Set BMI mode of Infineon XMC1000 device.
*
*  Syntax
*    SetBMI <Mode>
*    Valid values for <Mode>:
*      0 ASC Bootstrap Load Mode (ASC_BSL)
*      1 User Mode (Productive)
*      2 User Mode (Debug) SWD0
*      3 User Mode (Debug) SWD1
*      4 User Mode (Debug) SPD0
*      5 User Mode (Debug) SPD1
*      6 User Mode (HAR) SWD0
*      7 User Mode (HAR) SWD1
*      8 User Mode (HAR) SPD0
*      9 User Mode (HAR) SPD1
*/
static int _ExecSetBMI(const char * sIn) {
  int r;
  U32 BMIMode;
  //
  // Parse parameter(s) and perform plausibility checks.
  //
  r = 0;
  if (*sIn == 0) {
    r = -1;
    goto Done;
  }
  if (_ParseDec(&sIn, &BMIMode)) {
    r = -1;
    goto Done;
  }
  //
  // Set BMI mode
  //
  _ReportOutf("Setting BMI mode %d...", BMIMode);
  r = JLINKARM_BMI_Set(BMIMode);
  if (r != 0) {
    _ReportOutf("Failed. ErrorCode: %d\n", r);
  } else {
    _ReportOutf("O.K.\n");
  }
Done:
  if (r < 0) {
    _ReportOutf( "Syntax: SetBMI <Mode>\n"
            "Valid values for <Mode>:\n"
            "  0 ASC Bootstrap Load Mode (ASC_BSL)\n"
            "  1 User Mode (Productive)\n"
            "  2 User Mode (Debug) SWD0\n"
            "  3 User Mode (Debug) SWD1\n"
            "  4 User Mode (Debug) SPD0\n"
            "  5 User Mode (Debug) SPD1\n"
            "  6 User Mode (HAR) SWD0\n"
            "  7 User Mode (HAR) SWD1\n"
            "  8 User Mode (HAR) SPD0\n"
            "  9 User Mode (HAR) SPD1\n"
          );
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecGetBMI
*
*  Function description
*    Get BMI mode of Infineon XMC1000 device.
*
*  Syntax
*    GetBMI
*    Valid modes:
*      0 ASC Bootstrap Load Mode (ASC_BSL)
*      1 User Mode (Productive)
*      2 User Mode (Debug) SWD0
*      3 User Mode (Debug) SWD1
*      4 User Mode (Debug) SPD0
*      5 User Mode (Debug) SPD1
*      6 User Mode (HAR) SWD0
*      7 User Mode (HAR) SWD1
*      8 User Mode (HAR) SPD0
*      9 User Mode (HAR) SPD1
*/
static int _ExecGetBMI(const char * sIn) {
  int r;
  U32 BMIMode;
  //
  // Get BMI mode
  //
  _ReportOutf("Current BMI mode: ");
  r = JLINKARM_BMI_Get(&BMIMode);
  if (r != 0) {
    _ReportOutf("Error, ErrorCode: %d.\n", r);
  } else {
    _ReportOutf("%d.\n", BMIMode);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecGPIOGetProps
*/
static int _ExecGPIOGetProps(const char* s) {
  JLINK_EMU_GPIO_DESC aDesc[32];
  int i, NumDesc;
  int r;
  //
  // Get BMI mode
  //
  r = JLINK_EMU_GPIO_GetProps(aDesc, COUNTOF(aDesc));
  if (r <= 0) {
    _ReportOutf("Connected emulator does not support GPIO commands.\n");
    return JLINK_NOERROR;
  }
  NumDesc = MIN(r, COUNTOF(aDesc));
  _ReportOutf("%d GPIOs are supported:\n", r);
  for (i = 0; i < NumDesc; i++) {
    _ReportOutf("%d: %s\n", i, aDesc[i].acName);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecGPIOSetState
*/
static int _ExecGPIOSetState(const char* s) {
  U8  aPort  [MAX_NUM_WRITE_ITEMS];
  U8  aState [MAX_NUM_WRITE_ITEMS];
  U8  aResult[MAX_NUM_WRITE_ITEMS];
  U32 NumPorts, i;
  U32 v;
  int r;

  NumPorts = 0;
  do {
    if (NumPorts > MAX_NUM_WRITE_ITEMS) {
      _ReportOutf("This command can only handle up to %d items\n", MAX_NUM_WRITE_ITEMS);
      return JLINK_ERROR_UNKNOWN;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (_ParseDec(&s, &v)) {
      if (NumPorts == 0) {
        _ReportOutf("Syntax: GPIOSetState <Port0> <State0> [<Port1> <State1>...]\n");
        return JLINK_ERROR_SYNTAX;
      }
      break;
    }
    aPort[NumPorts] = v;
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (_ParseDec(&s, &v)) {
      _ReportOutf("Syntax: GPIOSetState <Port0> <State0> [<Port1> <State1>...]\n");
      return JLINK_ERROR_SYNTAX;
    }
    aState[NumPorts] = v;
    NumPorts++;
  } while (1);

  r = JLINK_EMU_GPIO_SetState(aPort, aState, aResult, NumPorts);
  for (i = 0; i < NumPorts; i++) {
    _ReportOutf("%d -> Port %d (State: %d)\n", aState[i], aPort[i], aResult[i]);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecGPIOGetState
*/
static int _ExecGPIOGetState(const char* s) {
  U8  aPort  [MAX_NUM_WRITE_ITEMS];
  U8  aResult[MAX_NUM_WRITE_ITEMS];
  U32 NumPorts, i;
  U32 v;
  int r;

  NumPorts = 0;
  do {
    if (NumPorts > MAX_NUM_WRITE_ITEMS) {
      _ReportOutf("This command can only handle up to %d items\n", MAX_NUM_WRITE_ITEMS);
      return JLINK_ERROR_UNKNOWN;
    }
    _EatWhite(&s);
    if (*s == ',') {
      s++;
    }
    if (_ParseDec(&s, &v)) {
      if (NumPorts == 0) {
        _ReportOutf("Syntax: GPIOGetState <Port0> [<Port1>...]\n");
        return JLINK_ERROR_SYNTAX;
      }
      break;
    }
    aPort[NumPorts] = v;
    NumPorts++;
  } while (1);

  r = JLINK_EMU_GPIO_GetState(aPort, aResult, NumPorts);
  for (i = 0; i < NumPorts; i++) {
    _ReportOutf("Port %d (Read: %d)\n", aPort[i], aResult[i]);
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecLicenseAdd
*
*  Return value
*    1:   O.K.,  license already exist
*    0:   O.K.,  license was added successfully
*   -1:   Error, unspecified error occurred
*   -2:   Error, could not read or write config area
*   -3:   Error, not enough space for new license
*   -4:   Error, no license given
*/
static int _ExecLicenseAdd(const char* s) {
  int r;
  _EatWhite(&s);
  r = JLINK_EMU_AddLicense(s);
  //
  // Check result
  //
  switch (r) {
  case  0:  _ReportOutf("License \"%s\" added successfully.\n", s);         break;
  case  1:  _ReportOutf("License \"%s\" already exists.\n", s);             break;
  case -1:  _ReportOutf("Failed to add license \"%s\".\n", s);              break;
  case -2:  _ReportOutf("Failed to add License \"%s\".\n", s);              break;
  case -3:  _ReportOutf("Not enough space to add license \"%s\".\n", s);    break;
  case -4:  _ReportOutf("No license string has been commited\"%s\".\n", s); break;
  default:  _ReportOutf("Unknown error (Error code: %d)\n", r);             break;
  }
  return r;
}

/*********************************************************************
*
*       _ExecLicenseShowHelp
*
*  Return value
*    0:   O.K.
*/
static int _ExecLicenseShowHelp(const char* s) {
  _ReportOutf("Syntax:\n"
         "  license add   Store a custom license on J-Link. Syntax: LicAdd <LicName>\n"
         "  license erase Erase all custom licenses on J-Link.\n"
         "  license show  Show all licenses stored on J-Link.\n");
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecLicenseErase
*
*  Return value
*    0:   O.K.
*   -1:   Error
*/
static int _ExecLicenseErase(const char* s) {
  int r;

  r = JLINK_EMU_EraseLicenses();

  if (r < 0) {
    _ReportOutf("Failed to erase licenses.\n");
    return -1;                                    // Error, could not write config area
  }
  _ReportOutf("All licenses erased successfully.\n");
  return JLINK_NOERROR;                           // OK, licenses erased successfully
}

/*********************************************************************
*
*       _ExecLicenseShow
*/
static int _ExecLicenseShow(const char* s) {
  static const char* _aFeature[] = {"RDI","FlashBP","FlashDL","JFlash","GDB"};
  U8  ac[0x50 + 0x100];  // Enough space to support old J-Links (80 bytes in config for Installed lics) and new ones (additional 256 bytes available for storage)
  int r;

  //
  // Show built-in licenses
  //
  r = JLINK_GetAvailableLicense(&ac[0], sizeof(ac));
  if (r >= 0) {
    _ReportOutf("Built-in licenses: %s\n", ac);
  } else {
    _ReportOutf("Could not determine available licenses.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  //
  // Show installable licenses
  //
  r = JLINK_EMU_GetLicenses(ac, sizeof(ac));
  switch (r) {
  case 0:
    _ReportOutf("No installable licenses.\n");
    return JLINK_NOERROR;
  case -1:
    _ReportOutf("Failed to get licenses.\n");
    return JLINK_ERROR_UNKNOWN;
  case -2:
    _ReportOutf("Read config data is not supported by connected J-Link.\n");
    return JLINK_NOERROR;
  case sizeof(ac):
    r--;
    break;
  default:
    break;
  }
  ac[r] = 0;
  r--;
  if (ac[r] == ',') {
    ac[r] = 0;
  }
  _ReportOutf("Installable licenses: %s\n", ac);
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecRTTRead
*/
static int _ExecRTTRead(const char* s) {
  char ac[256];
  int r;

  r = JLINK_RTTERMINAL_Read(0, ac, sizeof(ac) - 1);
  if (r > 0) {
    ac[r] = 0;
    _ReportOutf("RTT: %s\n", ac);
  } else if (r == 0) {
    _ReportOutf("No more data in real time terminal.\n");
  } else {
    _ReportOutf("Could not read real time terminal.\n");
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecExitOnError
*/
static int _ExecExitOnError(const char* sIn) {
  int r;
  _ParseDec(&sIn, &r);
  switch (r) {
  case 0:
   _ExitOnErrorLvl = 0; 
    _ReportOutf("J-Link Commander will no longer exit on Error\n");
    break;
  case 1:
    _ReportOutf("J-Link Commander will now exit on Error\n");
    _ExitOnErrorLvl = JLINK_ERROR_SYNTAX;
    break;
  default:
  _ReportOutf("Syntax: ExitonError <1|0>\n");
  return JLINK_ERROR_SYNTAX;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecTestPINOverride
*/
static int _ExecTestPINOverride(const char* s) {
  JLINK_FUNC_PIN_OVERRIDE* pfPinOverride;
  U32 aMode[8];
  U32 aState[8];
  int r;

  pfPinOverride = (JLINK_FUNC_PIN_OVERRIDE*)JLINK_GetpFunc(JLINK_IFUNC_PIN_OVERRIDE);
  if (pfPinOverride) {
    aMode[0] = 1;
    aMode[1] = 1;
    aMode[2] = 1;
    aMode[3] = 1;
    aMode[4] = 1;
    aMode[5] = 1;
    aMode[6] = 1;
    aMode[7] = 5;
    r = pfPinOverride(&aMode[0], &aState[0]);
  }
  return JLINK_NOERROR;
}

#define JLINK_IFUNC_PIN_GET_CAPS    3
typedef int STDCALL JLINK_FUNC_PIN_GET_CAPS(U32* paCaps);                                                // JLINK_IFUNC_PIN_GET_CAPS

/*********************************************************************
*
*       _ExecGetPinCaps
*/
static int _ExecGetPinCaps(const char* s) {

  JLINK_FUNC_PIN_GET_CAPS* pfPINGetCaps;
  U32 aCaps[8];
  int r;
  int i;

  pfPINGetCaps = (JLINK_FUNC_PIN_GET_CAPS*)JLINK_GetpFunc(JLINK_IFUNC_PIN_GET_CAPS);
  if (pfPINGetCaps) {
    r = pfPINGetCaps(&aCaps[0]);
    for (i = 0; i < 8; i++) {
      _ReportOutf("Pin %d: 0x%x \n", i, aCaps[i]);
    }
  } else {
    _ReportOutf("Function not defined. \n");
  }
 
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecCDCWrite
*/
static int _ExecCDCWrite(const char* s) {
  int r;
  int len;

  r = 0;
  _EatWhite(&s); //Write start at first char != whitespace
  len = strlen(s);
  if (len) {
    r = JLINKARM_CDC_Write(s, len);
  } else {
    _ReportOutf("Syntax: CDCWrite <StringToWrite>\n");
    return JLINK_ERROR_SYNTAX;
  }
  if (r != len) {
    _ReportOutf("Error while writing data. Error: %d.\n", r);
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecCDCRead
*/
static int _ExecCDCRead(const char* s) {
  char acIn[256];
  int r;

  r = JLINKARM_CDC_Read(acIn, sizeof(acIn) - 1);
  if ((r >= 0) && (r < sizeof(acIn))) {
    acIn[r] = 0;
    _ReportOutf("%s\n", acIn);
  } else {
    _ReportOutf("Error while reading data. Error: %d.\n", r);
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecCDCSetBaudrate
*/
static int _ExecCDCSetBaudrate(const char* s) {
  int r;
  U32 Baudrate;
  const char* ps;

  r  = 0;
  ps = _ParseDec(&s, &Baudrate); //Write start at first char != whitespace
  if (ps == NULL) {
    r = JLINKARM_CDC_SetBaudrate(Baudrate);
  } else {
    _ReportOutf("Syntax: CDCBaudRate <Baudrate>\n");
    return JLINK_ERROR_SYNTAX;
  }
  if (r < 0) {
    _ReportOutf("Could not set Baudrate. Error: %d.\n", r);
    return JLINK_ERROR_UNKNOWN;
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecMRU
*/
static int _ExecMRU(const char* s) {
  char ac[512];
  char* p;
  int r;
  JLINK_FUNC_MRU_GETLIST* pFunc;

  pFunc = (JLINK_FUNC_MRU_GETLIST*)JLINK_GetpFunc(JLINK_IFUNC_MRU_GETLIST);
  if (pFunc) {
    r = pFunc(JLINK_MRU_GROUP_DEVICE, ac, sizeof(ac));
    if (r >= 0) {
      _ReportOutf("Most recently used devices:\n");
      p = ac;
      r = 0;
      while ((*p != 0) && (r < 10)) {
        _ReportOutf("#%d: %s\n", r, p);
        p += strlen(p) + 1;
        r++;
      }
    }
  }
  return JLINK_NOERROR;
}

/*********************************************************************
*
*       _ExecQuitCommander
*/
static int _ExecQuitCommander(const char* s) {
  COMMANDER_SETTINGS* pCommanderSettings;

  pCommanderSettings = &_CommanderSettings;
  pCommanderSettings->CloseRequested = 1;
  return 0;
}

/*********************************************************************
*
*       _ExecConnectToTarget
*
*  Function description
*    Establish connection to target device by specifying device, target interface etc.
*
*  Return value
*    >= 0  O.K.
*     < 0  Error
*/
static int _ExecConnectToTarget(const char* s) {
  COMMANDER_SETTINGS* pCommanderSettings;

  pCommanderSettings = &_CommanderSettings;
  (void)s;
  return _ConnectToTarget(pCommanderSettings);
}

/*********************************************************************
*
*       _ExecShowCommands
*
*  Function description
*    Show list of available commands.
*
*  Return value
*    >= 0  O.K.
*     < 0  Error
*/
static int _ExecShowCommands(const char* s) {
  (void)s;
  printf("\nAvailable commands are:\n"
           "----------------------\n"
           "f          Firmware info\n"
           "h          halt\n"
           "g          go\n"
           "Sleep      Waits the given time (in milliseconds). Syntax: Sleep <delay>\n"
           "s          Single step the target chip\n"
           "st         Show hardware status\n"
           "hwinfo     Show hardware info\n"
           "mem        Read memory. Syntax: mem  [<Zone>:]<Addr>, <NumBytes> (hex)\n"
           "mem8       Read  8-bit items. Syntax: mem8  [<Zone>:]<Addr>, <NumBytes> (hex)\n"
           "mem16      Read 16-bit items. Syntax: mem16 [<Zone>:]<Addr>, <NumItems> (hex)\n"
           "mem32      Read 32-bit items. Syntax: mem32 [<Zone>:]<Addr>, <NumItems> (hex)\n"
           "w1         Write  8-bit items. Syntax: w1 [<Zone>:]<Addr>, <Data> (hex)\n"
           "w2         Write 16-bit items. Syntax: w2 [<Zone>:]<Addr>, <Data> (hex)\n"
           "w4         Write 32-bit items. Syntax: w4 [<Zone>:]<Addr>, <Data> (hex)\n"
           "erase      Erase internal flash of selected device. Syntax: Erase\n"
           "wm         Write test words. Syntax: wm <NumWords>\n"
           "is         Identify length of scan chain select register\n"
           "ms         Measure length of scan chain. Syntax: ms <Scan chain>\n"
           "mr         Measure RTCK react time. Syntax: mr\n"
           "q          Quit\n"
           "qc         Close JLink connection and quit\n"
           "r          Reset target         (RESET)\n"
           "rx         Reset target         (RESET). Syntax: rx <DelayAfterReset>\n"
           "RSetType   Set the current reset type. Syntax: RSetType <type>\n"
           "Regs       Display contents of registers\n"
           "wreg       Write register.   Syntax: wreg <RegName>, <Value>\n"
           "moe        Shows mode-of-entry, meaning: Reason why CPU is halted\n"
           "SetBP      Set breakpoint.   Syntax: SetBP <addr> [A/T] [S/H]\n"
           "SetWP      Set Watchpoint. Syntax: <Addr> [R/W] [<Data> [<D-Mask>] [A-Mask]]\n"
           "ClrBP      Clear breakpoint. Syntax: ClrBP  <BP_Handle>\n"
           "ClrWP      Clear watchpoint. Syntax: ClrWP  <WP_Handle>\n"
           "VCatch     Write vector catch. Syntax: VCatch <Value>\n"
           "loadfile   Load data file into target memory.\n"
           "             Syntax: loadfile <filename>, [<addr>]\n"
           "             Supported extensions: *.bin, *.mot, *.hex, *.srec\n"
           "             <addr> is needed for bin files only.\n"
           "loadbin    Load *.bin file into target memory.\n"
           "             Syntax: loadbin <filename>, <addr>\n"
           "savebin    Saves target memory into binary file.\n"
           "             Syntax: savebin <filename>, <addr>, <NumBytes>\n"
           "verifybin  Verfies if the specified binary is already in the target memory at the specified address.\n"
           "             Syntax: verifybin <filename>, <addr>\n"
           "SetPC      Set the PC to specified value. Syntax: SetPC <Addr>\n"
           "le         Change to little endian mode\n"
           "be         Change to big endian mode\n"
           "log        Enables log to file.  Syntax: log <filename>\n"
           "unlock     Unlocks a device. Syntax: unlock <DeviceName>\n"
           "           Type unlock without <DeviceName> to get a list\n"
           "           of supported device names.\n"
           "           nRESET has to be connected\n"
           "term       Test command to visualize _ReportOutf output from the target device,\n"
           "           using DCC (SEGGER DCC handler running on target)\n"          
           "ReadAP     Reads a CoreSight AP register.\n"
           "           Note: First read returns the data of the previous read.\n"
           "           An additional read of DP reg 3 is necessary to get the data.\n"
           "ReadDP     Reads a CoreSight DP register.\n"
           "           Note: For SWD data is returned immediately.\n"
           "           For JTAG the data of the previous read is returned.\n"
           "           An additional read of DP reg 3 is necessary to get the data.\n"
           "WriteAP    Writes a CoreSight AP register.\n"
           "WriteDP    Writes a CoreSight DP register.\n"
           "SWDSelect  Selects SWD as interface and outputs\n"
           "           the JTAG -> SWD switching sequence.\n"
           "SWDReadAP  Reads a CoreSight AP register via SWD.\n"
           "           Note: First read returns the data of the previous read.\n"
           "           An additional read of DP reg 3 is necessary to get the data.\n"
           "SWDReadDP  Reads a CoreSight DP register via SWD.\n"
           "           Note: Correct data is returned immediately.\n"
           "SWDWriteAP Writes a CoreSight AP register via SWD.\n"
           "SWDWriteDP Writes a CoreSight DP register via SWD.\n"
           "Device     Selects a specific device J-Link shall connect to\n"
           "           and performs a reconnect.\n"
           "           In most cases explicit selection of the device is not necessary.\n"
           "           Selecting a device enables the user to make use of the J-Link\n"
           "           flash programming functionality as well as using unlimited\n"
           "           breakpoints in flash memory.\n"
           "           For some devices explicit device selection is mandatory in order\n"
           "           to allow the DLL to perform special handling needed by the device.\n"
           "ExpDevList Exports the device names from the DLL internal\n"
           "           device list to a text file\n"
           "             Syntax: ExpDevList <Filename>\n"
           "PowerTrace Perform power trace (not supported by all models)\n"
           "Syntax: PowerTrace <LogFile> [<ChannelMask> <RefCountSel>]\n"
           "<LogFile>: File to store power trace data to\n"
           "<ChannelMask>: 32-bit mask to specify what channels shall be enabled\n"
           "<SampleFreq>: Sampling frequency in Hz (0 == max)\n"
           "<RefCountSel>:       0: No reference count\n"
           "                     1: Number of bytes transmitted on SWO\n"
           "---- CP15 ------------\n"
           "rce        Read CP15.  Syntax: rce <Op1>, <CRn>, <CRm>, <Op2>\n"
           "wce        Write CP15. Syntax: wce <Op1>, <CRn>, <CRm>, <Op2>, <Data>\n"
           "---- ICE -------------\n"
           "Ice        Show state of the embedded ice macrocell (ICE breaker)\n"
           "ri         Read Ice reg.  Syntax: ri <RegIndex>(hex)\n"
           "wi         Write Ice reg. Syntax: wi <RegIndex>, <Data>(hex)\n"
           "---- TRACE -----------\n"
           "TClear     TRACE - Clear buffer\n"
           "TSetSize   TRACE - Set Size of trace buffer\n"
           "TSetFormat TRACE - SetFormat\n"
           "TSR        TRACE - Show Regions (and analyze trace buffer)\n"
           "TStart     TRACE - Start\n"
           "TStop      TRACE - Stop\n"
           "---- SWO -------------\n"
           "SWOSpeed   SWO - Show supported speeds\n"
           "SWOStart   SWO - Start\n"
           "SWOStop    SWO - Stop\n"
           "SWOStat    SWO - Display SWO status\n"
           "SWORead    SWO - Read and display SWO data\n"
           "SWOShow    SWO - Read and analyze SWO data\n"
           "SWOFlush   SWO - Flush data\n"
           "SWOView    SWO - View terminal data\n"
           "---- PERIODIC --------\n"
           "PERConf    PERIODIC - Configure\n"
           "PERStart   PERIODIC - Start\n"
           "PERStop    PERIODIC - Stop\n"
           "PERStat    PERIODIC - Display status\n"
           "PERRead    PERIODIC - Read and display data\n"
           "PERShow    PERIODIC - Read and analyze data\n"
           "---- File I/O --------\n"
           "fwrite     Write file to emulator\n"
           "fread      Read file from emulator\n"
           "fshow      Read and display file from emulator\n"
           "fdelete    Delete file on emulator\n"
           "fsize      Display size of file on emulator\n"
           "flist      List directory on emulator\n"
           "SecureArea Creates/Removes secure area on probe\n"
           "---- Test ------------\n"
           "TestHaltGo   Run go/halt 1000 times\n"
           "TestStep     Run step 1000 times\n"
           "TestCSpeed   Measure CPU speed.\n"
           "             Parameters: [<RAMAddr>]\n"
           "TestWSpeed   Measure download speed into target memory.\n"
           "             Parameters:  [<Addr> [<Size>]]\n"
           "TestRSpeed   Measure upload speed from target memory.\n"
           "             Parameters: [<Addr> [<Size>] [<NumBlocks>]]\n"
           "TestNWSpeed  Measure network download speed.\n"
           "             Parameters: [<NumBytes> [<NumReps>]]\n"
           "TestNRSpeed  Measure network upload speed.\n"
           "             Parameters: [<NumBytes> [<NumReps>]]\n"
           "---- JTAG ------------\n"
           "Config     Set number of IR/DR bits before ARM device.\n"
           "             Syntax: Config <IRpre>, <DRpre>\n"
           "speed      Set target interface speed. Syntax: speed <freq>|auto|adaptive, e.g. speed 2000, speed a\n"
           "i          Read JTAG Id (Host CPU)\n"
           "wjc        Write JTAG command (IR). Syntax: wjc <Data>(hex)\n"
           "wjd        Write JTAG data (DR). Syntax: wjd <Data64>(hex), <NumBits>(dec)\n"
           "RTAP       Reset TAP Controller using state machine (111110)\n"
           "wjraw      Write Raw JTAG data. Syntax: wjraw <NumBits(dec)>, <tms>, <tdi>\n"
           "rt         Reset TAP Controller (nTRST)\n"
           "---- JTAG-Hardware ---\n"
           "c00        Create clock with TDI = TMS = 0\n"
           "c          Clock\n"
           "tck0       Clear TCK\n"
           "tck1       Set   TCK\n"
           "0          Clear TDI\n"
           "1          Set   TDI\n"
           "t0         Clear TMS\n"
           "t1         Set   TMS\n"
           "trst0      Clear TRST\n"
           "trst1      Set   TRST\n"
           "r0         Clear RESET\n"
           "r1         Set   RESET\n"
           "---- Connection ------\n"
           "usb        Connect to J-Link via USB.  Syntax: usb <port>, where port is 0..3\n"
           "ip         Connect to J-Link ARM Pro or J-Link TCP/IP Server via TCP/IP.\n"
           "           Syntax: ip <ip_addr>\n"
           "---- Configuration ---\n"
           "si         Select target interface. Syntax: si <Interface>,\n"
           "           where <Interface> can be any supported target interface (e.g SWD, JTAG, ICSP, FINE, ...\n"
           "power      Switch power supply for target. Syntax: power <State> [perm],\n"
           "           where State is either On or Off. Example: power on perm\n"    // Enable power supply, save settings in non-volatile memory"
           "wconf      Write configuration byte. Syntax: wconf <offset>, <data>\n"
           "rconf      Read configuration bytes. Syntax: rconf\n"
           "license    Shows a list of all available license commands\n"
           "ipaddr     Show/Assign IP address and subnetmask of/to the connected J-Link.\n"
           "gwaddr     Show/Assign network gateway address of/to the connected J-Link.\n"
           "dnsaddr    Show/Assign network DNS server address of/to the connected J-Link.\n"
           "conf       Show configuration of the connected J-Link.\n"
           "ecp        Enable the  J-Link control panel.\n"
           "calibrate  Calibrate the target current measurement.\n"
           "selemu     Select a emulator to communicate with,\n"
           "           from a list of all emulators which are connected to the host\n"
           "           The interfaces to search on, can be specified\n"
           "             Syntax: selemu [<Interface0> <Interface1> ...]\n"
           "ShowEmuList Shows a list of all emulators which are connected to the host.\n"
           "            The interfaces to search on, can be specified.\n"
           "             Syntax: ShowEmuList [<Interface0> <Interface1> ...]\n"
           "----------------------\n"
           );
  return 0;
}

/*********************************************************************
*
*       Command list
*/
static const _COMMAND_INFO _aCmd[] = {
//  pfCmd                      sLongName              sShortName            NeedsJLinkConnection   NeedsTargetConnection
  { _ExecShowCommands,         "?",                   "?",                   0,                     0,    },
  { _ExecQuitCommander,        "q",                   "q",                   0,                     0,    },
  { _ExecQuitCommander,        "qc",                  "qc",                  0,                     0,    },
  { _ExecQuitCommander,        "exit",                "exit",                0,                     0,    },
  { _ExecConnectToTarget,      "connect",             "connect",             1,                     0,    },
  { _ExecShowFirmwareInfo,     "f",                   "f",                   1,                     0,    },
  { _ExecShowHWStatus,         "st",                  "st",                  1,                     0,    },
  { _ExecShowHWInfo,           "hwinfo",              "hwinfo",              1,                     0,    },
  { _ExecHalt,                 "halt",                "h",                   1,                     1,    },
  { _ExecGo,                   "go",                  "g",                   1,                     1,    },
  { _ExecSleep,                "Sleep",               "Sleep",               0,                     0,    },
  { _ExecStep,                 "step",                "s",                   1,                     1,    },
  { _ExecReadMem,              "mem",                 "mem",                 1,                     1,    },
  { _ExecReadMem8,             "mem8",                "mem8",                1,                     1,    },
  { _ExecReadMem16,            "mem16",               "mem16",               1,                     1,    },
  { _ExecReadMem32,            "mem32",               "mem32",               1,                     1,    },
  { _ExecReadMem64,            "mem64",               "mem64",               1,                     1,    },
  { _ExecWriteU8,              "w1",                  "w1",                  1,                     1,    },
  { _ExecWriteU16,             "w2",                  "w2",                  1,                     1,    },
  { _ExecWriteU32,             "w4",                  "w4",                  1,                     1,    },
  { _ExecWriteMultiU32,        "wm4",                 "wm4",                 1,                     1,    },
  { _ExecErase,                "erase",               "erase",               1,                     1,    },
  { _ExecWriteMultiple,        "wm",                  "wm",                  1,                     1,    },
  { _ExecIdentifySCSRLen,      "is",                  "is",                  1,                     1,    },
  { _ExecMeasureSCLen,         "ms",                  "ms",                  1,                     1,    },
  { _ExecMeasureRTCKReactTime, "mr",                  "mr",                  1,                     1,    },
  { _ExecReset,                "r",                   "r",                   1,                     1,    },
  { _ExecResetEx,              "rx",                  "rx",                  1,                     1,    },
  { _ExecResetNoHalt,          "rnh",                 "rnh",                 1,                     1,    },
  { _ExecResetSetType,         "RSetType",            "rst",                 1,                     1,    },
  { _ExecShowRegs,             "regs",                "regs",                1,                     1,    },
  { _ExecReadReg,              "rreg",                "rreg",                1,                     1,    },
  { _ExecWriteReg,             "wreg",                "wreg",                1,                     1,    },
  { _ExecSetBP,                "SetBP",               "SetBP",               1,                     1,    },
  { _ExecClrBP,                "ClrBP",               "ClrBP",               1,                     1,    },
  { _ExecSetWP,                "SetWP",               "SetWP",               1,                     1,    },
  { _ExecClrWP,                "ClrWP",               "ClrWP",               1,                     1,    },
  { _ExecVCatch,               "VCatch",              "vc",                  1,                     1,    },
  { _ExecLoadFile,             "loadfile",            "loadfile",            1,                     1,    },
  { _ExecLoadBin,              "loadbin",             "loadbin",             1,                     1,    },
  { _ExecSaveBin,              "savebin",             "savebin",             1,                     1,    },
  { _ExecVerifyBin,            "verifybin",           "verifybin",           1,                     1,    },
  { _ExecSetPC,                "setpc",               "setpc",               1,                     1,    },
  { _ExecSetLittleEndian,      "le",                  "le",                  1,                     0,    },
  { _ExecSetBigEndian,         "be",                  "be",                  1,                     0,    },
  { _ExecLog,                  "log",                 "log",                 1,                     0,    },
  { _ExecUnlock,               "unlock",              "unlock",              1,                     0,    },
  { _ExecGetDebugInfo,         "GetDebugInfo",        "gdi",                 1,                     1,    },
  { _ExecSetDebugUnitBlockMask,"sdubm",               "sdubm",               1,                     1,    },
  { _ExecShowMOE,              "moe",                 "moe",                 1,                     1,    },
  // ---- ICE -------------                                                 0,                     0,    
  { _ExecShowICERegs,          "ice",                 "ice",                 1,                     1,    },
  { _ExecReadICEReg,           "ri",                  "ri",                  1,                     1,    },
  { _ExecWriteICEReg,          "wi",                  "wi",                  1,                     1,    },
  // ---- ETM -------------                                                 0,                     0,    
  { _ExecShowETMState,         "etm",                 "etm",                 1,                     1,    },
  { _ExecReadETMReg,           "re",                  "re",                  1,                     1,    },
  { _ExecWriteETMReg,          "we",                  "we",                  1,                     1,    },
  { _ExecStartTrace,           "es",                  "es",                  1,                     1,    },
  // ---- ETB -------------                                                 0,                     0,    
  { _ExecShowETBState,         "etb",                 "etb",                 1,                     1,    },
  { _ExecReadETBReg,           "rb",                  "rb",                  1,                     1,    },
  { _ExecWriteETBReg,          "wb",                  "wb",                  1,                     1,    },
  // ---- TRACE -----------                                                 0,                     0,    
  { _ExecTraceAddBranch,       "TAddBranch",          "tab",                 1,                     1,    },
  { _ExecTraceAddInst,         "TAddInst",            "tai",                 1,                     1,    },
  { _ExecTraceClear,           "TClear",              "tc",                  1,                     1,    },
  { _ExecTraceSetSize,         "TSetSize",            "tss",                 1,                     1,    },
  { _ExecTraceSetFormat,       "TSetFormat",          "tsf",                 1,                     1,    },
  { _ExecTraceShowRegions,     "tsr",                 "tsr",                 1,                     1,    },
  { _ExecTraceStart,           "TStart",              "TStart",              1,                     1,    },
  { _ExecTraceStop,            "TStop",               "TStop",               1,                     1,    },
  // ---- STRACE ----------                                                 0,                     0,    
  { _ExecSTraceConfig,         "STraceConfig",        "STConfig",            1,                     1,    },
  { _ExecSTraceStart,          "STraceStart",         "STStart",             1,                     1,    },
  { _ExecSTraceStop,           "STraceStop",          "STStop",              1,                     1,    },
  { _ExecSTraceRead,           "STraceRead",          "STRead",              1,                     1,    },
  // ---- SWO -------------                                                 0,                     0,    
  { _ExecSWOSpeed,             "SWOSpeed",            "SWOSpeed",            1,                     1,    },
  { _ExecSWOStart,             "SWOStart",            "SWOStart",            1,                     1,    },
  { _ExecSWOStop,              "SWOStop",             "SWOStop",             1,                     1,    },
  { _ExecSWOStat,              "SWOStat",             "SWOStat",             1,                     1,    },
  { _ExecSWORead,              "SWORead",             "SWORead",             1,                     1,    },
  { _ExecSWOShow,              "SWOShow",             "SWOShow",             1,                     1,    },
  { _ExecSWOFlush,             "SWOFlush",            "SWOFlush",            1,                     1,    },
  { _ExecSWOView,              "SWOView",             "SWOView",             1,                     1,    },
  { _ExecSWOStabilityTest,     "SWOStability",        "SWOStability",        1,                     1,    },
  // ---- HSS -------------                                                 0,                     0,    
  { _ExecHSSStart,             "HSSStart",            "HSSStart",            1,                     1,    },
  { _ExecHSSStop,              "HSSStop",             "HSSStop",             1,                     1,    },
  { _ExecHSSRead,              "HSSRead",             "HSSRead",             1,                     1,    },
  // ---- POWERTRACE ------                                                 0,                     0,    
  { _ExecPowerTrace,           "PowerTrace",          "PowerTrace",          1,                     1,    },
  // ---- EMUCOM ----------                                                 0,                     0,    
  { _ExecEMUCOMWrite,          "ECWrite",             "ecw",                 1,                     0,    },
  { _ExecEMUCOMRead,           "ECRead",              "ecr",                 1,                     0,    },
  { _ExecEMUCOMIsSupported,    "ECIsSupported",       "ecis",                1,                     0,    },
  // ---- File I/O --------                                                 0,                     0,    
  { _ExecFileWrite,            "fwrite",              "fwr",                 1,                     0,    },
  { _ExecFileRead,             "fread",               "frd",                 1,                     0,    },
  { _ExecFileShow,             "fshow",               "fshow",               1,                     0,    },
  { _ExecFileDelete,           "fdelete",             "fdel",                1,                     0,    },
  { _ExecFileSize,             "fsize",               "fsz",                 1,                     0,    },
  { _ExecFileList,             "flist",               "flist",               1,                     0,    },
  { _ExecFileSecureArea,       "SecureArea",          "SecureArea",          1,                     0,    },
  // ----Test--------------                                                 0,                     0,    
  { _ExecTestHaltGo,           "TestHaltGo",          "thg",                 1,                     1,    },
  { _ExecTestSingleStep,       "TestStep",            "ts",                  1,                     1,    },
  { _ExecTestWSpeed,           "testwspeed",          "testw",               1,                     1,    },
  { _ExecTestRSpeed,           "testrspeed",          "testr",               1,                     1,    },
  { _ExecTestCSpeed,           "testcspeed",          "testc",               1,                     1,    },
  { _ExecTestPCode,            "testpcode",           "testp",               1,                     0,    },
  { _ExecRunPCode,             "runpcode",            "runp",                1,                     1,    },
  { _ExecTestRecover,          "testRecover",         "testrecover",         1,                     0,    },
  { _ExecTestNWSpeed,          "testnwspeed",         "testnw",              1,                     0,    },
  { _ExecTestNRSpeed,          "testnrspeed",         "testnr",              1,                     0,    },
  // ---- GPIO --------                                                     0,                     0,    
  { _ExecGPIOGetProps,         "GPIOGetProps",        "GPIOProps",           1,                     0,    },
  { _ExecGPIOSetState,         "GPIOSetState",        "GPIOSet",             1,                     0,    },
  { _ExecGPIOGetState,         "GPIOGetState",        "GPIOGet",             1,                     0,    },
  // ----JTAG--------------                                                 0,                     0,    
  { _ExecSetJTAGConfig,        "Config",              "Config",              1,                     0,    },
  { _ExecSetSpeed,             "speed",               "speed",               1,                     0,    },
  { _ExecIdentify,             "i",                   "i",                   1,                     0,    },
  { _ExecWriteJTAGCommand,     "wjc",                 "wjc",                 1,                     0,    },
  { _ExecWriteJTAGData,        "wjd",                 "wjd",                 1,                     0,    },
  { _ExecWriteRaw,             "wjraw",               "wjr",                 1,                     0,    },
  { _ExecResetTAP,             "RTAP",                "RTAP",                1,                     0,    },
  { _ExecResetTRST,            "rt",                  "rt",                  1,                     0,    },
  // ----JTAG-Hardware-----                                                 0,                     0,    
  { _ExecC00,                  "c00",                 "c00",                 1,                     0,    },
  { _ExecClock,                "c",                   "c",                   1,                     0,    },
  { _ExecClrTCK,               "ClrTCK",              "tck0",                1,                     0,    },
  { _ExecSetTCK,               "SetTCK",              "tck1",                1,                     0,    },
  { _ExecClrTDI,               "ClrTDI",              "0",                   1,                     0,    },
  { _ExecSetTDI,               "SetTDI",              "1",                   1,                     0,    },
  { _ExecClrTMS,               "ClrTMS",              "t0",                  1,                     0,    },
  { _ExecSetTMS,               "SetTMS",              "t1",                  1,                     0,    },
  { _ExecClrTRST,              "ClrTRST",             "trst0",               1,                     0,    },
  { _ExecSetTRST,              "SetTRST",             "trst1",               1,                     0,    },
  { _ExecClrRESET,             "ClrRESET",            "r0",                  1,                     0,    },
  { _ExecSetRESET,             "SetRESET",            "r1",                  1,                     0,    },
  // ----Connection--------                                                 0,                     0,    
  { _ExecSelectUSB,            "usb",                 "usb",                 0,                     0,    },
  { _ExecSelectIP,             "ip",                  "ip",                  0,                     0,    },
  // ----Configuration-----                                                 0,                     0,    
  { _ExecReadAP,               "ReadAP",              "ReadAP",              1,                     0,    },
  { _ExecWriteAP,              "WriteAP",             "WriteAP",             1,                     0,    },
  { _ExecReadDP,               "ReadDP",              "ReadDP",              1,                     0,    },
  { _ExecWriteDP,              "WriteDP",             "WriteDP",             1,                     0,    },
  { _ExecMSDDisble,            "MSDDisable",          "MSDDisable",          1,                     0,    },
  { _ExecMSDEnable,            "MSDEnable",           "MSDEnable",           1,                     0,    },
  { _ExecWriteConfigVMSD,      "VMSDDownloadConfig",  "VMSDDownloadConfig",  1,                     0,    },
  { _ExecCDCSetHookFuncs,      "CDCSetHooks",         "CDCSetHooks",         1,                     0,    },
  { _ExecSWDReadAP,            "SWDReadAP",           "srap",                1,                     0,    },
  { _ExecSWDWriteAP,           "SWDWriteAP",          "swap",                1,                     0,    },
  { _ExecSWDReadDP,            "SWDReadDP",           "srdp",                1,                     0,    },
  { _ExecSWDWriteDP,           "SWDWriteDP",          "swdp",                1,                     0,    },
  { _ExecSWDSelect,            "SWDSelect",           "SWDSelect",           1,                     0,    },
  { _ExecGetCounters,          "GetCounters",         "gc",                  1,                     0,    },
  { _ExecSelectInterface,      "SelectInterface",     "si",                  1,                     0,    },
  { _ExecSelectInterface,      "SelectInterface",     "if",                  1,                     0,    }, // obsolete
  { _ExecPower,                "power",               "power",               1,                     0,    },
  { _ExecVCOM,                 "VCOM",                "VCOM",                1,                     0,    },
  { _ExecWriteConfig,          "wconf",               "wconf",               1,                     0,    },
  { _ExecReadConfig,           "rconf",               "rconf",               1,                     0,    },
  { _ExecResetConfig,          "resetconf",           "resetconf",           1,                     0,    },
  { _ExecIPAddress,            "ipaddr",              "ipaddr",              1,                     0,    },
  { _ExecGWAddress,            "gwaddr",              "gwaddr",              1,                     0,    },
  { _ExecDNSAddress,           "dnsaddr",             "dnsaddr",             1,                     0,    },
  { _ExecMACAddress,           "macaddr",             "macaddr",             1,                     0,    },
  { _ExecShowConfig,           "showconf",            "conf",                1,                     0,    },
  { _ExecCalibrate,            "calibrate",           "calib",               1,                     0,    },
  { _ExecSelectEmuFromList,    "SelectEmulator",      "selemu",              0,                     0,    },
  { _ExecShowEmuList,          "ShowEmuList",         "ShowEmuList",         0,                     0,    },
  { _ExecDisassemble,          "disassemble",         "da",                  1,                     1,    },
  { _ExecRawTrace,             "rawtrace",            "rtr",                 1,                     1,    },
  { _ExecTestNet,              "testnet",             "tn",                  1,                     0,    },
  { _ExecShowEmuInfo,          "ShowEmus",            "emu",                 0,                     0,    },
  { _ExecShowEmuStatus,        "EmuStatus",           "est",                 1,                     0,    },
  { _ExecConfigEmu,            "ConfigEmu",           "ce",                  1,                     0,    },
  { _ExecSelectEmuByUSBSN,     "SelectEmuBySN",       "sesn",                0,                     0,    },
  { _ExecSelectFamily,         "SelectFamily",        "sf",                  1,                     0,    },
  { _ExecSelectCore,           "SelectCore",          "sc",                  1,                     0,    },
  { _ExecConfigDevices,        "ConfigDevices",       "cd",                  1,                     0,    },
  { _ExecReadMemIndirect,      "memi",                "memi",                1,                     1,    },
  { _ExecTest,                 "test",                "test",                1,                     0,    },
  { _ExecIndicator,            "indi",                "indi",                1,                     0,    },
  { _ExecTerminal,             "term",                "term",                1,                     1,    },
  { _ExecWriteTestPattern,     "w",                   "w",                   1,                     1,    },
  { _ExecSelDevice,            "sel",                 "sel",                 1,                     0,    },
  { _ExecReadCP15,             "rcp15",               "rcp15",               1,                     1,    },
  { _ExecWriteCP15,            "wcp15",               "wcp15",               1,                     1,    },
  { _ExecReadCP15Ex,           "rcp15ex",             "rce",                 1,                     1,    },
  { _ExecWriteCP15Ex,          "wcp15ex",             "wce",                 1,                     1,    },
  { _ExecExecCommand,          "Exec",                "Exec",                1,                     0,    },
  { _ExecExportDeviceList,     "ExpDevList",          "ExpDevList",          1,                     0,    },
  { _ExecDevice,               "Device",              "Device",              1,                     0,    },
  { _ExecSetRAMAddr,           "ram",                 "ram",                 1,                     0,    },
  { _ExecTestRAM,              "testram",             "testram",             1,                     1,    },
  { _ExecAND,                  "and",                 "and",                 1,                     1,    },
  { _ExecOR,                   "or",                  "or",                  1,                     1,    },
  { _ExecXOR,                  "xor",                 "xor",                 1,                     1,    },
  { _ExecSetBMI,               "SetBMI",              "SetBMI",              1,                     0,    },
  { _ExecGetBMI,               "GetBMI",              "GetBMI",              1,                     0,    },
  { _ExecLicenseAdd,           "license add",         "license add",         1,                     0,    },
  { _ExecLicenseShow,          "license show",        "license show",        1,                     0,    },
  { _ExecLicenseErase,         "license erase",       "license erase",       1,                     0,    },
  { _ExecLicenseShowHelp,      "license",             "license",             1,                     0,    },
  { _ExecRTTRead,              "rttread",             "rttread",             1,                     1,    },
  { _ExecExitOnError,          "exitonerror",         "eoe",                 0,                     0,    },
  { _ExecTestPINOverride,      "TestPinOverride",     "testpio",             1,                     0,    },
  { _ExecGetPinCaps,           "GetPinCaps",          "pincaps",             1,                     0,    },
  { _ExecCDCWrite,             "CDCWrite",            "cdcW",                1,                     0,    },
  { _ExecCDCRead,              "CDCRead",             "cdcR",                1,                     0,    },
  { _ExecCDCSetBaudrate,       "CDCBaudRate",         "cdcB",                1,                     0,    },
  { _ExecMRU,                  "MRU",                 "MRU",                 0,                     0,    },
};

/*********************************************************************
*
*       _ExecCommand
*
*  
*
*  Return value
*  >= 0: Continue execution of command line
*  <  0: Error occured. Upper level handles quit or continuation
*/

static char _ExecCommand(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  int i;
  int r;
  //
  // Go through command table and find & execute command
  //
  _EatWhite(&s);
  for (i = 0; i < COUNTOF(_aCmd); i++) {
    if ((_CompareCmd(&s, _aCmd[i].sLongName) == 0) || (_CompareCmd(&s, _aCmd[i].sShortName) == 0)) {   // Check short and long version of command
      //
      // Make sure that J-Link connection is established, if required by command
      // In script mode we do not automatically connect to J-Link to allow the user to specify connection settings via command file
      //
      if (_aCmd[i].NeedsJLinkConnection && (pCommanderSettings->Status.ConnectedToJLink == 0)) {
        _ReportOut("J-Link connection not established yet but required for command.\n");
        r = _ConnectToJLink(&_CommanderSettings);
        if (r < 0) {
          return r;
        }
      }
      //
      // Make sure that target connection is established, if required by command
      //
      if (_aCmd[i].NeedsTargetConnection && (pCommanderSettings->Status.ConnectedToTarget == 0)) {
        _ReportOut("Target connection not established yet but required for command.\n");
        r = _ConnectToTarget(&_CommanderSettings);
        if (r < 0) {
          return r;
        }
      }
      //
      // Perform command
      //
      r = _aCmd[i].pfCmd(s);
      _PRINT_RETVAL(("Command returned: %d\n", r));
      r = (r >= 0) ? 0 : r;
      return r;
    }
  }
  _ReportOutf("Unknown command. '?' for help.\n");
  return 0;
}

/*********************************************************************
*
*       _ParseScriptFile
*
*  Return value
*    >= 0: O.K.
*     < 0: Error happened
*/
static char _ParseScriptFile(COMMANDER_SETTINGS* pCommanderSettings, const char* pBuffer) {
  char ac[512];
  int r;
  while (_GetLine(&pBuffer, ac, sizeof(ac))) {
    const char* s = ac;
    _ReportOutf("\n");
    if        (_CompareCmd(&s, ":Loop") == 0) {
      _pLoop = pBuffer;
    } else if (_CompareCmd(&s, "BEQ") == 0) {
      if (_pLoop && (_Var == 0)) {
        pBuffer = _pLoop;
      }
    } else if (_CompareCmd(&s, "BNE") == 0) {
      if (_pLoop && (_Var != 0)) {
        pBuffer = _pLoop;
      }
    } else {
      r = _ExecCommand(pCommanderSettings, ac);
      //
      // If executed command set requested to close, the command returns O.K. but the upper layer have to handle the close request
      //
      if (pCommanderSettings->CloseRequested) {
        return 0;
      }
      if ((_ExitOnErrorLvl) && (r < 0)) {
        return JLINK_ERROR_SYNTAX;
      }  
    }
  }
  return 0;
}

/*********************************************************************
*
*       _InteractiveMode
*
*  Function description
*    Controls interactive mode of J-Link where user needs to enter commands.
*
*  Return value
*    >= 0  O.K.
*     < 0  Error
*
*  Notes
*    (1) Interactive mode may be cancelled by a "q" command or if exit on error level has been activated and an error occurred
*/
static int _InteractiveMode(void) {
  char ac[1024];
  char * s;
  int r;
  COMMANDER_SETTINGS* pCommanderSettings;
  //
  // Open connection to J-Link and show some information about J-Link + DLL
  //
  pCommanderSettings = &_CommanderSettings;
  r = 0;
  if (pCommanderSettings->Status.ConnectedToJLink == 0) {
    r = _ConnectToJLink(pCommanderSettings);
  }
  //
  // User specified to automatically establish a target connection on start of commander?
  //
  if (r >= 0) {
    r = 0;
    if (pCommanderSettings->DoAutoConnect) {
      if (pCommanderSettings->Status.ConnectedToTarget == 0) {
        r = _ConnectToTarget(pCommanderSettings);
      }
    } else {
      _ReportOut("\n\nType \"connect\" to establish a target connection, '?' for help\n");
    }
  }
  //
  // Wait for user input
  //
  do {
    s = _ConsoleGetString("J-Link>", ac, sizeof(ac));
    r = _ExecCommand(pCommanderSettings, s);
    if ((r < 0) && (r >= _ExitOnErrorLvl)) {      // Error occured and exit on error activated? => Quit
      break;
    }
    if (pCommanderSettings->CloseRequested) {      // Quit command received? => Quit
      break;
    }
  } while (1);
  return r;
}

/*********************************************************************
*
*       _ScriptMode
*
*  Function description
*    Controls script mode where user passes a command file to J-Link Commander and automates operations.
*
*  Return value
*    >= 0: O.K.
*    == 0: O.K., enter interactive mode
*    == 1: O.K., script closed connection with "qc" or "q" command
*     < 0: Error happened
*
*  Notes
*    (1) Interactive mode may be cancelled by a "q" command or if exit on error level has been activated and an error occurred
*/
static int _ScriptMode(COMMANDER_SETTINGS* pCommanderSettings) {
  char* pBuffer;
  char sErr[256] = {0};
  HANDLE hFile;
  U32 NumBytes;
  int NumBytesRead;
  int r;
  //
  // Load command file
  //
  r = 0;
  pBuffer = NULL;
  hFile = _OpenFile(pCommanderSettings->acCommanderScriptFile, FILE_FLAG_READ);
  if (hFile != INVALID_HANDLE_VALUE) {
    NumBytes = _GetFileSize(hFile);
    pBuffer = malloc(NumBytes + 1);
    if (pBuffer) {
      NumBytesRead = _ReadFile(hFile, pBuffer, NumBytes);
    } else {
      NumBytesRead = 0;
    }
    if (NumBytesRead == (int)NumBytes) {
      pBuffer[NumBytes] = 0;
      _ReportOut("\nScript file read successfully.\n");
    } else {
      _ReportOut("Failed to read command file\n");
      r = -1;
    }
    _CloseFile(hFile);
  } else {
    _ReportOutf(sErr, "Could not open script file '%s'\n", pCommanderSettings->acCommanderScriptFile);
    r = -1;
  }
  if (r < 0) {
    goto Done;
  }
  //
  // Process command file
  //
  _ReportOutf("Processing script file...\n");
  r = _ParseScriptFile(pCommanderSettings, pBuffer);
  _ReportOutf("\nScript processing completed.\n\n");
  //
  // Do cleanup
  //
Done:
  if (pBuffer) {
    free(pBuffer);
  }
  return r;
}

/*********************************************************************
*
*       _HandleCmdLineHelp
*
*  Function description
*    Parses specific command line option "?" which is used
*    to print all available command line options.
*
*  Return Value
*      0  O.K.
*    < 0  Error (Syntax error etc.)
*/
static int _HandleCmdLineHelp(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  return 0;
}

/*********************************************************************
*
*       _HandleCmdLineSI
*
*  Function description
*    Parses specific command line option -si which is used
*    to select the target interface J-Link shall use to connect to the target.
*
*  Return Value
*      0: O.K.
*    < 0: Error (Syntax error etc.)
*/
static int _HandleCmdLineSI(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  int r;

  r = _Name2TIF(s);
  if (r < 0) {
    _ReportOutf("ERROR while parsing value for -si.\n");
    return JLINK_ERROR_SYNTAX;
  }
  pCommanderSettings->TargetIF = r;
  pCommanderSettings->TargetIFSet = 1;
  return 0;
}

/*********************************************************************
*
*       _HandleCmdLineJTAGConf
*
*  Function description
*    Parses command line option -JTAGConf
*    which is used to setup the JTAG device selection inside a JTAG chain
*
*  Return Value
*      0: O.K.
*    < 0: Error (Syntax error etc.)
*/
static int _HandleCmdLineJTAGConf(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  if (_ParseDec(&s, &pCommanderSettings->JTAGConfIRPre)) {
    _ReportOutf("ERROR while parsing value for IRPre. Using default: %d.\n", pCommanderSettings->JTAGConfIRPre);
    return JLINK_ERROR_SYNTAX;
  }
  if (*s == ',') {     // Skip comma between JTAG config params
    s++;
  }
  if (_ParseDec(&s, &pCommanderSettings->JTAGConfDRPre)) {
    _ReportOutf("ERROR while parsing value for DRPre. Using default: %d.\n", pCommanderSettings->JTAGConfDRPre);
    return JLINK_ERROR_SYNTAX;
  }
  pCommanderSettings->JTAGConfSet = 1;
  pCommanderSettings->TargetIFSet = 1;
  return 0;
}

/*********************************************************************
*
*       _HandleCmdLineCommanderScript
*
*  Function description
*    Parses specific command line option -CommandFile which is used
*    to pass a J-Link commander script that is executed by J-Link Commander.
*    Do NOT confuse with J-Link script files.
*
*  Return Value
*      0: O.K.
*    < 0: Error (Syntax error etc.)
*/
static int _HandleCmdLineCommanderScript(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  strcpy(pCommanderSettings->acCommanderScriptFile, s);
  pCommanderSettings->ScriptModeActive = 1;
  return 0;
}

/*********************************************************************
*
*       _HandleCmdLineUSB
*
*  Function description
*    Parses specific command line option -usb which is used
*    to select the USB address to use to connec to the emulator.
*    Deprecated, use -SelectEmuBySN instead.
*
*  Return Value
*      0: O.K.
*    < 0: Error (Syntax error etc.)
*/
static int _HandleCmdLineUSB(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  U32 v;

  if (_ParseDec(&s, &v)) {
    _ReportOutf("ERROR while parsing value for usb.\n");
    return -1;
  }
  pCommanderSettings->HostIF = JLINKARM_HOSTIF_USB;
  pCommanderSettings->EmuSN = v;    // Will be 0-3 in this case
  return 0;
}

/*********************************************************************
*
*       _HandleCmdLineDevice
*
*  Function description
*    Parses specific command line option -device which is used
*    to select the device the emulator shall connect to.
*
*  Return Value
*      0: O.K.
*    < 0: Error (Syntax error etc.)
*/
static int _HandleCmdLineDevice(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  strcpy(pCommanderSettings->acDeviceName, s);
  return 0;
}

/*********************************************************************
*
*       _HandleCmdLineJLinkScriptFile
*
*  Function description
*    Parses specific command line option -JLinkScriptFile which is used
*    to select a J-Link script file to be used by the DLL for connect etc.
*
*  Return Value
*      0: O.K.
*    < 0: Error (Syntax error etc.)
*/
static int _HandleCmdLineJLinkScriptFile(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  strcpy(pCommanderSettings->acJLinkScriptFile, s);
  return 0;
}

/*********************************************************************
*
*       _HandleCmdLineJLinkSettingsFile
*
*  Function description
*    Parses specific command line option -settingsfile which is used
*    to select a settings file to be used by the DLL.
*
*  Return Value
*      0: O.K.
*    < 0: Error (Syntax error etc.)
*/
static int _HandleCmdLineJLinkSettingsFile(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  strcpy(pCommanderSettings->acSettingsFile, s);
  return 0;
}

/*********************************************************************
*
*       _HandleCmdLineSelectEmuBySN
*
*  Function description
*    Parses specific command line option -SelectEmuBySN which is used
*    to connect to an emulator via its serial number.
*
*  Return Value
*      0: O.K.
*    < 0: Error (Syntax error etc.)
*/
static int _HandleCmdLineSelectEmuBySN(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  U32 v;

  if (_ParseDec(&s, &v)) {
    _ReportOutf("ERROR while parsing value for -SelectEmuBySN.\n");
    return -1;
  }
  pCommanderSettings->EmuSN = v;
  return 0;
}

/*********************************************************************
*
*       _HandleCmdLineIP
*
*  Function description
*    Parses specific command line option -ip which is used
*    to connect to an emulator connected via IP.
*
*  Return Value
*      0: O.K.
*    < 0: Error (Syntax error etc.)
*/
static int _HandleCmdLineIP(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  strcpy(pCommanderSettings->acIPAddr, s);
  pCommanderSettings->HostIF = JLINKARM_HOSTIF_IP;
  return 0;
}
/*********************************************************************
*
*       _HandleCmdLineExitOnError
*
*  Function description
*    Sets _ExitOnErrorLvl to JLINK_ERROR_ALL, so JLinkCommander will break on all return values < 0.
*
*  Return Value
*      0: O.K.
*    < 0: Error (Syntax error etc.)
*/
static int _HandleCmdLineExitOnError(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  _ReportOutf("J-Link Commander will now exit on Error\n");
  _ExitOnErrorLvl = JLINK_ERROR_SYNTAX;
  return 0;
}

/*********************************************************************
*
*       _HandleCmdLineAutoConnect
*
*  Function description
*    Parses specific command line option -AutoConnect which is used
*    to set automatically start the connect sequence for connecting to the target.
*
*  Return Value
*      0: O.K.
*    < 0: Error (Syntax error etc.)
*/
static int _HandleCmdLineAutoConnect(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  U32 v;

  if (_ParseDec(&s, &v)) {
    _ReportOutf("ERROR while parsing value for -AutoConnect.\n");
    return -1;
  }
  pCommanderSettings->DoAutoConnect = v;
  return 0;
}

/*********************************************************************
*
*       _HandleCmdLineSpeed
*
*  Function description
*    Parses specific command line option -speed which is used
*    to set the default target interface speed used by the Commander.
*
*  Return Value
*      0: O.K.
*    < 0: Error (Syntax error etc.)
*/
static int _HandleCmdLineSpeed(COMMANDER_SETTINGS* pCommanderSettings, const char* s) {
  U32 v;

  if (_JLinkstricmp(s, "adaptive") == 0) {
    v = JLINKARM_SPEED_ADAPTIVE;
  } else if (_JLinkstricmp(s, "auto") == 0) {
    v = JLINKARM_SPEED_AUTO;
  } else {
    if (_ParseDec(&s, &v)) {
      _ReportOutf("ERROR while parsing value for -speed.\n");
      return -1;
    }
  }
  pCommanderSettings->InitTIFSpeedkHz = v;
  pCommanderSettings->InitTIFSpeedkHzSet = 1;
  return 0;
}

static const CMDLINE_OPTION _acCmdLineOption[] = {
  { "?",                _HandleCmdLineHelp              },
  { "IP",               _HandleCmdLineIP                },
  { "SelectEmuBySN",    _HandleCmdLineSelectEmuBySN     },
  { "USB",              _HandleCmdLineUSB               },
  { "-AutoConnect",     _HandleCmdLineAutoConnect       },
  { "-CommanderScript", _HandleCmdLineCommanderScript   },
  { "-CommandFile",     _HandleCmdLineCommanderScript   },
  { "-JTAGConf",        _HandleCmdLineJTAGConf          },
  { "-Device",          _HandleCmdLineDevice            },
  { "-If",              _HandleCmdLineSI                },
  { "-Si",              _HandleCmdLineSI                },
  { "-IP",              _HandleCmdLineIP                },
  { "-SelectEmuBySN",   _HandleCmdLineSelectEmuBySN     },
  { "-Speed",           _HandleCmdLineSpeed             },
  { "-JLinkScriptFile", _HandleCmdLineJLinkScriptFile   },
  { "-SettingsFile",    _HandleCmdLineJLinkSettingsFile },
  { "-ExitOnError",     _HandleCmdLineExitOnError       },
  { NULL, NULL }
};

/*********************************************************************
*
*       _ParseCmdLine
*
*  Function description
*    Parse all command line parameters and setup the commander options accordingly.
*
*  Return Value
*      0: O.K.
*    < 0: Error (Syntax error etc.)
*/
static int _ParseCmdLine(COMMANDER_SETTINGS* pCommanderSettings, int argc, char* argv[]) {
  HANDLE hFile;
  const char* s;
  int i;
  int j;
  int r;
  //
  // Parse option by option
  //
  i = 1;  // First command line parameter is always the application name
  while (i < argc) {
    //
    // Check if command is in command list
    //
    s = argv[i++];
    j = 0;
    do {
      if (_acCmdLineOption[j].sName == NULL) {  // End of list reached?
        //
        // Check if a J-Link Commander script file has been passed
        // In older versions of the commander it was allowed to pass a commander script file without specifying a specific command line option
        // We check if the current option is a valid commander file and if it is not, we just output that it is an unknown option
        //
        hFile = _OpenFile(s, FILE_FLAG_READ | FILE_FLAG_SHARE_READ);
        if (hFile == INVALID_HANDLE_VALUE) {
          _ReportOutf("Unknown command line option %s.\n", s);
          return -1;
        }
        _CloseFile(hFile);
        _HandleCmdLineCommanderScript(pCommanderSettings, s);
        break;
      }
      if (_JLinkstricmp(s, _acCmdLineOption[j].sName) == 0) {  // Cmd found?
        //
        // Execute command
        //
        if (i >= argc) {
          _ReportOutf("Missing command line parameter after command %s.\n", _acCmdLineOption[j].sName);
          return -1;
        }
        r = _acCmdLineOption[j].pfCmd(pCommanderSettings, argv[i++]);
        if (r < 0) {
          return -1;
        }
        break;
      }
      j++;
    } while(1);
  }
  return 0;
}

/*********************************************************************
*
*       main
*/
int main(int argc, char* argv[], char* envp[]) {
  char acRev[8] = {0};
  char ac[512];
  char * pBuffer;
  char SkipInit;
  int Revision;
  int r = 0;
  int RevDLL;
  int VerDLL;

  pBuffer          = NULL;
  SkipInit         = 0;
  _InitSysTimer();     // Initialize system timers for usage
  //
  // Init host-interface related settings of commander with good defaults
  //
  memset(&_CommanderSettings, 0, sizeof(_CommanderSettings));
  _CommanderSettings.EmuSN           = 0;                         // Default is: Simply connect to first J-Link that can be found
  _CommanderSettings.HostIF          = JLINKARM_HOSTIF_USB;       // Default interface is USB
  //
  // Init target-interface related settings of commander with good defaults
  //
  _CommanderSettings.TargetIF        = JLINKARM_TIF_JTAG;
  _CommanderSettings.InitTIFSpeedkHz = DEFAULT_FAST_SPEED;        // Default connect speed is 100 kHz
  //
  // Output commander version info
  //
  Revision = APP_VERSION % 100;
  if (Revision > 26) {
    Revision -= 26;
    acRev[0] = 'z';
    acRev[1] = '0' + (Revision / 10);
    acRev[2] = '0' + (Revision % 10);
    acRev[3] = 0;
  } else if (Revision > 0) {
    acRev[0] = 'a' + Revision - 1;
    acRev[1] = 0;
  } else {
    acRev[0] = 0;
  }
  _ReportOutf("SEGGER J-Link Commander V%d.%02d%s (Compiled %s %s)\n", APP_VERSION / 10000, (APP_VERSION / 100) % 100, acRev, __DATE__, __TIME__);
  //
  // Show DLL info
  //
  VerDLL   = JLINKARM_GetDLLVersion();
  RevDLL   = VerDLL % 100;
  if (RevDLL > 26) {
    RevDLL -= 26;
    ac[0] = 'z';
    ac[1] = '0' + (RevDLL / 10);
    ac[2] = '0' + (RevDLL % 10);
    ac[3] = 0;
  } else if (RevDLL > 0) {
    ac[0] = 'a' + RevDLL - 1;
    ac[1] = 0;
  } else {
    ac[0] = 0;
  }
  _ReportOutf("DLL version V%d.%.2d%s, compiled %s\n\n", VerDLL / 10000, VerDLL / 100 % 100, ac, JLINKARM_GetCompileDateTime()); 
  //
  // Parse command line options, if any
  //
  r = _ParseCmdLine(&_CommanderSettings, argc, argv);
  if (r < 0) {                                         // Command line error? => Quit
    r = -1;
    _Sleep(3000);
    goto Done;
  }
  //
  // Decide if we need to enter interactive or command-file based mode
  //
  if (_CommanderSettings.ScriptModeActive) {
    r = _ScriptMode(&_CommanderSettings);
    if (r == 0) {
      if (_CommanderSettings.CloseRequested) {
        goto Done;
      }
      r = _InteractiveMode();
    }
  } else {
    r = _InteractiveMode();
  }
Done:
  if (JLINKARM_IsOpen()) {
    JLINKARM_Close();
  }
  _DeInitSysTimer();
  r = (r < 0) ? 1 : 0;        // Map to Windows / Linux etc. exit code handling => 0 == O.K., else == error
  return r;
}

/*************************** end of file ****************************/
