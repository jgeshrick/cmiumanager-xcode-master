/*********************************************************************
*                SEGGER MICROCONTROLLER SYSTEME GmbH                 *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*      (C) 2008-2014    SEGGER Microcontroller Systeme GmbH          *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File    : SYS.c
Purpose : System routines
---------------------------END-OF-HEADER------------------------------
*/

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <mmsystem.h>
#include <stdlib.h>
#include <stdio.h>
#include <shlobj.h>
#include <sys\types.h>
#include <sys\stat.h>
#include <errno.h>
#include <direct.h>

#include "SYS.h"

/*********************************************************************
*
*       Types
*
**********************************************************************
*/

typedef struct {
  HANDLE hMap;
  void*  pView;
} FILE_MAP_INFO;

#define NUM_BYTES_SPARE_POST    32
#define ALLOC_SIGNATURE         0xDEADBEEF
#define MAX_NUM_FILE_MAPS       8

#define SYS_PATH_MAX_DEPTH      128

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/

static FILE_MAP_INFO  _aFileMap[MAX_NUM_FILE_MAPS];
static int            _NumFileMaps;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _FindFileMapByPointer
*/
static int _FindFileMapByPointer(void* p) {
  int i;
  for (i = 0; i < _NumFileMaps; i++) {
    if (p == _aFileMap[i].pView) {
      return i;
    }
  }
  return -1;
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       SYS_FILE_Open
*/
SYS_FILE_HANDLE SYS_FILE_Open(const char* sFile, U32 Flags) {
  SYS_FILE_HANDLE hFile;
  DWORD AccessFlags = 0;
  DWORD ShareFlags  = 0;
  DWORD CreateFlags = 0;

  AccessFlags |= (Flags & SYS_FILE_FLAG_READ)        ? GENERIC_READ      : 0;
  AccessFlags |= (Flags & SYS_FILE_FLAG_WRITE)       ? GENERIC_WRITE     : 0;
  ShareFlags  |= (Flags & SYS_FILE_FLAG_SHARE_READ)  ? FILE_SHARE_READ   : 0;
  ShareFlags  |= (Flags & SYS_FILE_FLAG_SHARE_WRITE) ? FILE_SHARE_WRITE  : 0;
  if (Flags & SYS_FILE_FLAG_CREATE) {
    if        (Flags & SYS_FILE_FLAG_EXCL) {
      CreateFlags = CREATE_NEW;
    } else if (Flags & SYS_FILE_FLAG_TRUNC) {
      CreateFlags = CREATE_ALWAYS;
    } else {
      CreateFlags = OPEN_ALWAYS;
    }
  } else if (Flags & SYS_FILE_FLAG_TRUNC) {
    CreateFlags = TRUNCATE_EXISTING;
  } else {
    CreateFlags = OPEN_EXISTING;
  }
  hFile = (SYS_FILE_HANDLE)CreateFile(sFile, AccessFlags, ShareFlags, NULL, CreateFlags, FILE_ATTRIBUTE_NORMAL, NULL);
  if (hFile != INVALID_HANDLE_VALUE) {
    if (Flags & SYS_FILE_FLAG_APPEND) {
      SetFilePointer((HANDLE)hFile, 0, 0, FILE_END);
    }
  }
  return hFile;
}

/*********************************************************************
*
*       SYS_FILE_Read
*/
int SYS_FILE_Read(SYS_FILE_HANDLE hFile, void* pData, U32 NumBytes) {
  DWORD NumBytesRead;
  if (ReadFile((HANDLE)hFile, pData, NumBytes, &NumBytesRead, NULL) == 0) {
    return -1;                    // Error
  }
  return (int)NumBytesRead;       // O.K.
}

/*********************************************************************
*
*       SYS_FILE_Write
*/
int SYS_FILE_Write(SYS_FILE_HANDLE hFile, const void* pData, U32 NumBytes) {
  DWORD NumBytesWritten;
  if (WriteFile((HANDLE)hFile, pData, NumBytes, &NumBytesWritten, NULL) == 0) {
    return -1;                    // Error
  }
  return (int)NumBytesWritten;    // O.K.
}

/*********************************************************************
*
*       SYS_FILE_Close
*/
void SYS_FILE_Close(SYS_FILE_HANDLE hFile) {
  CloseHandle((HANDLE)hFile);
}

/*********************************************************************
*
*       SYS_GetTickCount
*/
int SYS_GetTickCount(void) {
  static int _t0;
  int t;

  t = timeGetTime();
  if (_t0 == 0) {
    _t0 = t;
  }
  return t - _t0;
}

/*********************************************************************
*
*       SYS_Sleep
*/
void SYS_Sleep(int ms) {
  Sleep(ms);
}

/*************************** End of file ****************************/
