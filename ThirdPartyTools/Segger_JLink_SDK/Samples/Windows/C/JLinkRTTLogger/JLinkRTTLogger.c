/*********************************************************************
*               SEGGER MICROCONTROLLER GmbH & Co KG                  *
*       Solutions for real time microcontroller applications         *
**********************************************************************
*                                                                    *
*       (c) 2014         SEGGER Microcontroller GmbH & Co KG         *
*                                                                    *
*       www.segger.com     Support: support@segger.com               *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File    : main.c
Purpose : J-Link RTT Logger
---------------------------END-OF-HEADER------------------------------
*/

#include <stdio.h>
#include <string.h>
#include "JLinkARMDLL.h"
#include "SYS.h"

#ifdef WIN32
  #include <conio.h>
  #include <windows.h>
#else
  #include <termios.h>
  #include <limits.h>
  #include <unistd.h>
  #include <stdlib.h>
  #include <sys/ioctl.h>
#endif

#ifdef linux
  #include <dlfcn.h>
#endif

#ifdef __APPLE__
  #include <readline/readline.h>
  #include <readline/history.h>
#endif

void MAIN_ErrorOut(const char* sText);

/*********************************************************************
*
*       Defines
*
**********************************************************************
*/
#define APP_PURPOSE               "SEGGER J-Link RTT Logger"
#define USE_SAVED_SETTINGS        0

/*********************************************************************
*
*       Types
*
**********************************************************************
*/

typedef struct {
  char  acDevice[128];
  U32   TIF;
  U32   TIFSpeed;
  U32   RTTCBAddr;
  U32   RTTCBAddrValid;
  int   RTTChannel;
  char  acChannelName[32];
  
  char  acFileName[SYS_MAX_PATH];
} RTT_LOGGER_CONFIG;

typedef struct {
  U32 HostIF;                 // Host interface used to connect to J-Link. 0 = USB, 1 = IP
  U32 TargetIF;               // See JLINKARM_Const.h "Interfaces" for valid values
  U32 SerialNo;               // Serial number of J-Link we want to connect to via USB
  U32 Speed;                  // Interface speed in kHz
  const char* sHost;          // Points to the IPAddr / nickname of the J-Link we want to connect to.
  const char* sSettingsFile;
  const char* sDevice;        // Target device J-Link is connected to
} INIT_PARAS;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static RTT_LOGGER_CONFIG  _Config;
static char               _ExitTerminal;
static char               _abData[1024*1024];

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/
/*********************************************************************
*
*       _cbLogOutHandler
*
*  Function description
*    Call-back function used to output log messages from the J-Link DLL
*/
static void _cbLogOutHandler(const char* sLog) {
}

/*********************************************************************
*
*       _cbErrorOutHandler
*
*  Function description
*    Call-back function used to output error messages from the J-Link DLL
*/
static void _cbErrorOutHandler(const char* sError) {
  MAIN_ErrorOut(sError);
}
/*********************************************************************
*
*       _kbhit
*
*    Function description
*      Linux (POSIX) implementation of _kbhit().
*/
#ifndef WIN32
static int _kbhit(void) {
  static int Inited = 0;

  if (Inited == 0) {
        //
        // Use termios to turn off line buffering
        //
        struct termios Term;
    tcgetattr(STDIN_FILENO, &Term);
        Term.c_lflag &= ~ICANON;
    tcsetattr(STDIN_FILENO, TCSANOW, &Term);
        setbuf(stdin, NULL);
    Inited = 1;
    }

    int BytesInBuffer;
 ioctl(STDIN_FILENO, FIONREAD, &BytesInBuffer);
    return BytesInBuffer;
}
#endif

/*********************************************************************
*
*       _cbCtrlHandler
*/
#ifdef WIN32
static BOOL WINAPI _cbCtrlHandler(DWORD Type) {
  switch (Type) {
  case CTRL_C_EVENT:
  case CTRL_BREAK_EVENT:
    _ExitTerminal = 1;
    return TRUE;
  }
  return FALSE;
}
#endif

#if linux

typedef char * (READ_LINE)  (const char *);
typedef int    (ADD_HISTORY)(const char *);

/*********************************************************************
*
*       _OpenLibrary
*/
static void * _OpenLibrary(void) {
  char         acPath [PATH_MAX];
  ssize_t      NumBytes;
  char       * p;  
  const char * pSoName;
  void       * pLib;

  pLib    = NULL;
  pSoName = "libedit.so.0";
  //
  // Get the path to the application which uses the shared library.
  //
  NumBytes = readlink("/proc/self/exe", acPath, sizeof(acPath));
  if (NumBytes > 0) {
    //
    // Create the path to shared library.
    //
    p = strrchr(acPath, '/');
    if (p) {
      ++p;
    } else {
      p = acPath;
    }
    strcpy(p, "ThirdParty/");
    strcat(acPath, pSoName);
    //
    // Try to open the shared libray stored in the directory where the application is stored.
    //
    pLib = dlopen(acPath, RTLD_LAZY);
    if (pLib == NULL) {
      //
      // Shared library not found. Try to load it from the installation path.
      //
      sprintf(acPath, "/opt/segger/jlink/ThirdParty/%s", pSoName);
      pLib = dlopen(acPath, RTLD_LAZY);
      if (pLib == NULL) {
        //
        // Shared library not found. Try to load it from the system path.
        //
        pLib = dlopen(pSoName, RTLD_LAZY);
      }
    }
  }
  return pLib;
}

/*********************************************************************
*
*       _GetSymbol
*/
static void * _GetSymbol(void * pLib, const char * pName) {
  void * pSymbol;
  char * pError;

  dlerror();
  pSymbol = dlsym(pLib, pName);
  pError = dlerror();
  if (pError) {
    pSymbol = NULL;
  }
  return pSymbol;
}

#endif

/*********************************************************************
*
*       _ConsoleGetString
*/
static char * _ConsoleGetString(const char * pPrompt, char * pBuffer, U32 BufferSize) {
  if (pPrompt) {
  	printf(pPrompt);
  }
#ifdef _MSC_VER  
  fgets(pBuffer, BufferSize, stdin);
  if (*(pBuffer+strlen(pBuffer)-1) == '\n') {
    *(pBuffer+strlen(pBuffer)-1) = 0;
  }
  return pBuffer;
#else
  *pBuffer = BufferSize - 4;
  return _cgets(pBuffer);
#endif
}

/*********************************************************************
*
*       _InitDebugSession
*
*  Function description
*    Initializes the debug session by connecting to a J-Link,
*    setting up the J-Link DLL and J-Link and finally connecting to the target system
*
*  Return value
*      0 O.K.
*    < 0 Error
*/
static int _InitDebugSession(INIT_PARAS* pParas) {
  const char* sError;
  U8 acIn[0x400];
  U8 acOut[0x400];
  int r;
  //
  // Select and configure host interface (USB is default)
  //
  if (pParas->HostIF == 1) {  // Host interface == IP?
    //
    // If sHost is NULL, J-Link selection dialog will pop-up
    // on JLINKARM_Open() / JLINKARM_OpenEx(), showing all
    // emulators that have been found in the network    
    // Passing 0 as port selects the default port (19020).
    //
    r = JLINKARM_SelectIP(pParas->sHost, 0);
    if (r == 1) {
      return -1;  // Error occurred during configuration of IP interface
    }
  } else {        // Connect via USB
    //
    // Was a specific serial number set we shall to connect to?
    //
    if (pParas->SerialNo) {
      r = JLINKARM_EMU_SelectByUSBSN(pParas->SerialNo);
      if (r < 0) {
        return -1;    // Error: Specific serial number not found on USB
     }
    }
  }
  //
  // Open communication with J-Link
  //
  sError = JLINKARM_OpenEx(_cbLogOutHandler, _cbErrorOutHandler);
  if (sError) {                   // Error occurred while connecting to J-Link?
    MAIN_ErrorOut(sError);
    return -1;
  }
  //
  // Select settings file
  // Used by the control panel to store its settings and can be used by the user to
  // enable features like flash breakpoints in external CFI flash, if not selectable
  // by the debugger. There should be different settings files for different debug
  // configurations, for example different settings files for LEDSample_DebugFlash
  // and LEDSample_DebugRAM. If this file does not exist, the DLL will create one
  // with default settings. If the file is already present, it will be used to load
  // the control panel settings
  //
  if (pParas->sSettingsFile) {
    strcpy(acIn, "ProjectFile = ");
    strcat(acIn, pParas->sSettingsFile);
    JLINKARM_ExecCommand(acIn, acOut, sizeof(acOut));
    if (acOut[0]) {
      MAIN_ErrorOut(acOut);
      return -1;
    }
  }
  //
  // Select device or core
  //
  if (pParas->sDevice) {
    strcpy(acIn, "device = ");
    strcat(acIn, pParas->sDevice);
    JLINKARM_ExecCommand(acIn, &acOut[0], sizeof(acOut));
    if (acOut[0]) {
      MAIN_ErrorOut(acOut);
      return -1;
    }
  }
  //
  // Select and configure target interface
  // If not called, J-Link will use the interface which was configured before. If
  // J-Link is power-cycled, JTAG is the default target interface.
  // It is recommended to always select the interface at debug startup.
  //
  JLINKARM_TIF_Select(pParas->TargetIF);
  //
  // Select target interface speed which should be used by J-Link for target communication
  //
  JLINKARM_SetSpeed(pParas->Speed);
  //
  // Connect to target CPU
  //
  r = JLINKARM_Connect();
  if (r) {
    MAIN_ErrorOut("Could not connect to target.");
    return -1;
  }
  //
  // If this is the only connection to the J-Link, make sure that the CPU is running
  //
  if (JLINKARM_EMU_GetNumConnections() == 1) {
    if (JLINKARM_IsHalted() == 1) {
      JLINKARM_Go();
      SYS_Sleep(500);  // Give target some time for PLL init etc. since on some targets JLINKARM_Connect() causes the target to be reset and halted immediately after reset.
    }
  }
  return 0;
}

/*********************************************************************
*
*       _EatWhite
*/
static void _EatWhite(const char** ps) {
  const char* s = *ps;
  while ((*s == ' ') || (*s == '\t') || (*s == '\r') || (*s == '\n')) {
    s++;
  }
  *ps = s;
}

/*********************************************************************
*
*       _ParseDec
*/
static const char* _ParseDec(const char** ps, U32* pData) {
  U32 Data;
  int NumDigits;

  Data = 0;
  NumDigits = 0;
  
  _EatWhite(ps);
  do {
    int v;
    char c;

    c = **ps;
    v =  ((c >= '0') && (c <= '9')) ? c - '0' : -1;
    if (v >= 0) {
      Data = (Data * 10) + v;
      (*ps)++;
      NumDigits++;
    } else {
      if (NumDigits == 0) {
        return "Expected a dec value";
      }
      *pData = Data;
      return NULL;
    }
  } while (1);
}

/*********************************************************************
*
*       _ParseHex
*/
static const char* _ParseHex(const char** ps, U32* pData) {
  U32 Data;
  int NumDigits;

  Data = 0;
  NumDigits = 0;
  
  _EatWhite(ps);
  if ((**ps == '0') && ((*((*ps)+1) == 'x') || (*((*ps)+1) == 'X'))) {
    (*ps) += 2;
  }
  do {
    int v;
    char c;

    c = **ps;
    if ((c >= '0') && (c <= '9')) {
      v = c - '0';
    } else if ((c >= 'a') && (c <= 'f')) {
      v = c - 'a' + 10;
    } else if ((c >= 'A') && (c <= 'F')) {
      v = c - 'A' + 10;
    } else {
      v = -1;
    }
    if (v >= 0) {
      Data = (Data * 16) + v;
      (*ps)++;
      NumDigits++;
    } else {
      if (NumDigits == 0) {
        return "Expected a hex value";
      }
      *pData = Data;
      return NULL;
    }
  } while (1);
}


/*********************************************************************
*
*       _LoadMRUDevice
*/
static void _LoadMRUDevice(void) {
  int r;
  JLINK_FUNC_MRU_GETLIST* pFunc;

  pFunc = (JLINK_FUNC_MRU_GETLIST*)JLINK_GetpFunc(JLINK_IFUNC_MRU_GETLIST);
  if (pFunc) {
    r = pFunc(JLINK_MRU_GROUP_DEVICE, _Config.acDevice, sizeof(_Config.acDevice));
    if (r < 0) {
      _Config.acDevice[0] = 0;
    }
  }
}


/*********************************************************************
* 
*       _InitSettings
*/
static int _InitSettings(int argc, char* argv[]) {
  char  ac[1024];
  char* s;
  const char* sErr;
  U32   v;

  _LoadMRUDevice();
  if (_Config.acDevice[0] != 0) {
    printf("Enter the device name. Default: %s         ", _Config.acDevice);
  } else {
    printf("Enter the device name.                               ");
  }
  s = _ConsoleGetString("> ", ac, sizeof(ac));
  _EatWhite(((const char **)&s));
  if (*s) {
    sprintf(_Config.acDevice, "%s", s);
  } else if (_Config.acDevice[0] == 0) {
    MAIN_ErrorOut("Could not get device name.\n");
    return -1;
  }
  printf("Target interface. 0: JTAG, 1: SWD.      Default: SWD ");
  s = _ConsoleGetString("> ", ac, sizeof(ac));
  _EatWhite(((const char **)&s));
  if (*s) {
    switch (*s) {
    case '0':
      _Config.TIF = JLINKARM_TIF_JTAG;
      break;
    case '1':
      _Config.TIF = JLINKARM_TIF_SWD;
      break;
    default:
      MAIN_ErrorOut("Could not get target interface.\n");
      return -1;
      break;
    }
  } else {
    _Config.TIF = JLINKARM_TIF_SWD;
  }
  printf("Interface speed [kHz].             Default: 4000 kHz ");
  s = _ConsoleGetString("> ", ac, sizeof(ac));
  _EatWhite(((const char **)&s));
  if (*s) {
    sErr = _ParseDec((const char**)&s, &v);
    if (sErr) {
      MAIN_ErrorOut(sErr);
      MAIN_ErrorOut("Could not get interface speed.\n");
      return -1;
    }
    _Config.TIFSpeed = v;
  } else {
    _Config.TIFSpeed = 4000;
  }
  printf("RTT Control Block address.   Default: auto-detection ");
  s = _ConsoleGetString("> ", ac, sizeof(ac));
  _EatWhite(((const char **)&s));
  if (*s) {
    sErr = _ParseHex((const char**)&s, &v);
    if (sErr) {
      MAIN_ErrorOut(sErr);
      MAIN_ErrorOut("Could not get RTT address.\n");
      return -1;
    }
    _Config.RTTCBAddr = v;
    _Config.RTTCBAddrValid = 1;
  } else {
    _Config.RTTCBAddrValid = 0;
  }

  printf("RTT Channel name or index.          Default: Index 1 ");
  s = _ConsoleGetString("> ", ac, sizeof(ac));
  _EatWhite(((const char **)&s));
  if (*s) {
    sErr = _ParseDec((const char**)&s, &v);
    if (sErr) {
      _Config.RTTChannel = -1;
      sprintf(_Config.acChannelName, "%s", s);
    } else {
      _Config.RTTChannel = v;
      _Config.acChannelName[0] = 0;
    }
  } else {
    _Config.RTTChannel = 1;
    _Config.acChannelName[0] = 0;
  }


  printf("Output file.  If empty: RTT_<ChannelName>_<Time>.log ");
  s = _ConsoleGetString("> ", ac, sizeof(ac));
  _EatWhite(((const char **)&s));
  if (*s) {
    sprintf(_Config.acFileName, "%s", s);
  } else {
    _Config.acFileName[0] = 0;
  }
  
  return 0;
}

static int _Connect(void) {
  INIT_PARAS Paras;
  JLINK_RTTERMINAL_START Info;
  
  memset(&Paras, 0, sizeof(INIT_PARAS));
  Paras.TargetIF = _Config.TIF;
  Paras.Speed    = _Config.TIFSpeed;
  Paras.sDevice  = _Config.acDevice;
  
  if (_InitDebugSession(&Paras) != 0) {
    return -1;
  }

  if (_Config.RTTCBAddrValid == 1) {
    Info.ConfigBlockAddress = _Config.RTTCBAddr;
    JLINK_RTTERMINAL_Control(JLINKARM_RTTERMINAL_CMD_START, &Info);
  } else {
    JLINK_RTTERMINAL_Control(JLINKARM_RTTERMINAL_CMD_START, NULL);
  }

  return 0;
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
* 
*       MAIN_ErrorOut
*/
void MAIN_ErrorOut(const char* sText) {

  printf("\nERROR: ");
  printf(sText);
}

/*********************************************************************
*
*       main
*
*  Function description
*    main() function of command line version of SWOViewer.
*    This version does not remember any settings in a settings file.
*/
int main(int argc, char* argv[], char* envp[]) {
  int   r;
  char  c;
  int   NumBytesRead;
  U32   NumBytesTotal;
  U32   NumBytesPerSec;
  int   Time;
  int   TimeStart;
  int   Rate;
  SYS_FILE_HANDLE hFile;
  U32   SN;
  char  ac[256];
  JLINK_RTTERMINAL_BUFDESC Desc;
  int   Direction;
  int   NumChannels;
  
  SYS_TIME TS;

  hFile = SYS_INVALID_HANDLE;
  NumBytesTotal = 0;
  NumBytesPerSec = 0;
  //
  // Set output buffering to non-buffered
  // to allow directly getting output 
  // when redirecting stdout to somewhere else than terminal.
  // setvbuf has to be called before any use of the stream.
  //
  setvbuf(stdout, (char*)NULL, _IONBF, 0);

#ifdef WIN32
  SetConsoleCtrlHandler(_cbCtrlHandler, TRUE);
#endif
#if 0
  printf("************************************************************ \n");
  printf("*           SEGGER MICROCONTROLLER GmbH & Co KG            * \n");
  printf("*   Solutions for real time microcontroller applications   * \n");
  printf("************************************************************ \n");
  printf("*                                                          * \n");
  printf("*  (c) 2012 - 2014  SEGGER Microcontroller GmbH & Co KG    * \n");
  printf("*                                                          * \n");
  printf("*     www.segger.com     Support: support@segger.com       * \n");
  printf("*                                                          * \n");
  printf("************************************************************ \n");
  printf("*                                                          * \n");
  printf("* SEGGER J-Link RTT Logger   Compiled " __DATE__ " " __TIME__ " * \n");
  printf("*                                                          * \n");
  printf("************************************************************ \n\n");
#else
  printf(APP_PURPOSE);
  printf("\nCompiled " __DATE__ " " __TIME__ "\n");
  printf("(c) 2014 SEGGER Microcontroller GmbH & Co. KG, www.segger.com\n");
  printf("         Solutions for real time microcontroller applications\n\n");
#endif
  printf("------------------------------------------------------------ \n\n");
  TimeStart = SYS_GetTickCount();

  
  //
  // Initialize target settings
  //
  if (_InitSettings(argc, argv) != 0) {
    r = -1;
    goto Done;
  }
  //
  // Connect and set up RTT
  //
  if (_Connect() != 0) {
    r = -1;
    goto Done;
  }

  JLINKARM_EMU_GetProductName(ac, sizeof(ac));
  printf("\n------------------------------------------------------------ \n");
  printf("Connected to:\n  ");
  printf(ac);
  SN = JLINKARM_GetSN();
  if (SN >= 0) {
  	printf("\n  S/N: %d\n", SN);
  }
  //
  // Get the number of down buffers and the description of up buffer 1
  // Try until the the RTT CB is found
  //
  Direction = JLINKARM_RTTERMINAL_BUFFER_DIR_UP;
  printf("\nSearching for RTT Control Block...");
  do {
    NumChannels = JLINK_RTTERMINAL_Control(JLINKARM_RTTERMINAL_CMD_GETNUMBUF, &Direction);
    if (_kbhit()) {
#ifdef WIN32
      c = getch();
#else
      c = getchar();
#endif
      if (c) {
        NumChannels = -1;
        break;
      }
    }
    SYS_Sleep(2);
  } while (NumChannels == -2);
  if (NumChannels < 0) {
    printf("RTT Control Block not found. Cannot get data.\n");
    goto Done;
  }
  printf("OK. %d up-channels found.\n", NumChannels);

  if (_Config.RTTChannel >= 0) {
    if (_Config.RTTChannel >= NumChannels) {
      printf("RTT Channel %d not found. Cannot get data.\n", _Config.RTTChannel);
      goto Done;
    }
    Desc.BufferIndex = _Config.RTTChannel;
    Desc.Direction = JLINKARM_RTTERMINAL_BUFFER_DIR_UP;
    r = JLINK_RTTERMINAL_Control(JLINKARM_RTTERMINAL_CMD_GETDESC, &Desc);
    if (r >= 0) {
      sprintf(_Config.acChannelName, "%s", Desc.acName);
      printf("RTT Channel description: \n"
             "  Index: %d\n"
             "  Name:  %s\n"
             "  Size:  %d bytes.\n",
             Desc.BufferIndex, Desc.acName, Desc.SizeOfBuffer);
    } else {
      printf("RTT Channel description unknown.\n");
    }
  } else if (_Config.acChannelName[0] != 0) {
    int i;
    for (i = 0; i < NumChannels; i++) {
      memset(&Desc, 0, sizeof(Desc));
      Desc.BufferIndex = i;
      Desc.Direction = JLINKARM_RTTERMINAL_BUFFER_DIR_UP;
      r = JLINK_RTTERMINAL_Control(JLINKARM_RTTERMINAL_CMD_GETDESC, &Desc);
      if (r < 0) {
        printf("Failed to get RTT Channel description.\n");
        goto Done;
      }
      if (strcmp(_Config.acChannelName, Desc.acName) == 0) {
        _Config.RTTChannel = Desc.BufferIndex;
        sprintf(_Config.acChannelName, "%s", Desc.acName);
        printf("RTT Channel description: \n"
               "  Index: %d\n"
               "  Name:  %s\n"
               "  Size:  %d bytes.\n",
               Desc.BufferIndex, Desc.acName, Desc.SizeOfBuffer);
        break;
      }
    }
    if (_Config.RTTChannel < 0) {
      printf("Could not find RTT Channel %s. Cannot get data.\n", _Config.acChannelName);
    }
  } else {
    printf("RTT Channel not selected. Cannot get data.\n");
    goto Done;
  }

  TimeStart = SYS_GetTickCount();

  SYS_GetLocalTime(&TS);

  if (_Config.acFileName[0] == 0) {
    sprintf(_Config.acFileName, "RTT_%s_%.4d%.2d%.2d%.2d%.2d.log", _Config.acChannelName, TS.Year, TS.Month, TS.Day, TS.Hour, TS.Minute);
  }
  hFile = SYS_FILE_Open(_Config.acFileName, SYS_FILE_FLAG_WRITE | SYS_FILE_FLAG_SHARE_READ | SYS_FILE_FLAG_CREATE | SYS_FILE_FLAG_TRUNC);
  if (hFile == SYS_INVALID_HANDLE) {
    goto Done;
  }
  printf("\nOutput file: %s\n", _Config.acFileName);
  printf("\nGetting RTT data from target. Press any key to quit.\n");
  printf("------------------------------------------------------------ \n\n");
  _ExitTerminal = 0;
  do {
    //
    // Get RTT data and write them to the file
    //
    NumBytesRead = JLINK_RTTERMINAL_Read(_Config.RTTChannel, _abData, sizeof(_abData));
    if (NumBytesRead < 0) {
      r = 1;
      MAIN_ErrorOut("Failed to read data.\n");
      goto Done;
    }
    Time = SYS_GetTickCount() - TimeStart;
    
    SYS_FILE_Write(hFile, _abData, NumBytesRead);

    NumBytesPerSec += NumBytesRead;
    if (Time >= 500) {
      NumBytesTotal += NumBytesPerSec;
      Rate = (NumBytesPerSec*1000/Time);
      printf("\r");
      printf("Transfer rate: %d KByte/s ", Rate/1024);
      if (NumBytesTotal < 1024) {
        printf("Bytes written: %d Byte   ", NumBytesTotal);
      } else if (NumBytesTotal < 1024*1024) {
        printf("Bytes written: %d KByte  ", NumBytesTotal/1024);
      } else {
        printf("Bytes written: %d.%d MByte   ", NumBytesTotal/(1024*1024), ((NumBytesTotal/1024)%1000));
      }
      TimeStart += Time;
      NumBytesPerSec = 0;
    }
    //
    // Wait for closing event
    // Will be set by any key press or Ctrl-C 
    //
    if (_kbhit()) {
#ifdef WIN32
      c = getch();
#else
      c = getchar();
#endif
      switch (c) {
#if _DEBUG
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        c -= '0';
        if (JLINK_RTTERMINAL_Write(_Config.RTTChannel, &c, 1) < 1) {
          printf("\nFailed to write char.\n");
        }
        break;
      case 'Q':
        _ExitTerminal = 1;
        break;
#endif
      default:
        _ExitTerminal = 1;
        break;
      }
    }
    SYS_Sleep(2); // Sleep some time
  } while (_ExitTerminal == 0);
  r = 0;
Done:
  //
  // Shut down
  //
  printf("\n");
  printf(APP_PURPOSE);
  printf(" is shutting down...");
  JLINKARM_Close();

  if (hFile != SYS_INVALID_HANDLE) {
    SYS_FILE_Close(hFile);
  }
  //
  // Give the user some time to read the messages
  //
  SYS_Sleep(1000);
  return r;
}

/*************************** end of file ****************************/
