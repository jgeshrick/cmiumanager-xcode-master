/*********************************************************************
*                SEGGER MICROCONTROLLER SYSTEME GmbH                 *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*           (C) 2008    SEGGER Microcontroller Systeme GmbH          *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File    : SYS.h
Purpose : Interface definition for system specific routines
---------------------------END-OF-HEADER------------------------------
*/

#ifndef SYS_H                    // Avoid multiple inclusion
#define SYS_H

#include "TYPES.h"

#if defined(__cplusplus)         // Allow usage of this module from C++ files (disable name mangling)
  extern "C" {
#endif

/*********************************************************************
*
*       Defines
*
**********************************************************************
*/

#define SYS_FILE_FLAG_READ              (1 << 0)                    // Open the file for reading.
#define SYS_FILE_FLAG_WRITE             (1 << 1)                    // Open the file for writing.
#define SYS_FILE_FLAG_SHARE_READ        (1 << 2)                    // Allow other processes to open the same file for reading.
#define SYS_FILE_FLAG_SHARE_WRITE       (1 << 3)                    // Allow other processes to open the same file for writing.
#define SYS_FILE_FLAG_CREATE            (1 << 4)                    // Create new file. If the file already exist, open it.
#define SYS_FILE_FLAG_TRUNC             (1 << 5)                    // Truncate the file to zero length.
#define SYS_FILE_FLAG_APPEND            (1 << 6)                    // Set file pointer to end of file.
#define SYS_FILE_FLAG_EXCL              (1 << 7)                    // Can be specified in combination with SYS_FILE_FLAG_CREATE. Open fails if the specified file already exists.
#define SYS_FILE_FLAG_SHARE             (SYS_FILE_FLAG_SHARE_READ | SYS_FILE_FLAG_SHARE_WRITE)

#define SYS_FILE_SEEK_BEGIN             (0)
#define SYS_FILE_SEEK_CURRENT           (1)
#define SYS_FILE_SEEK_END               (2)

#define SYS_FILE_ATT_READONLY           (1)                         // Same value as FILE_ATTRIBUTE_READONLY

#define SYS_INVALID_HANDLE              (SYS_HANDLE)(-1)

#define SYS_ERROR_FILE_NOT_FOUND        (2)
#define SYS_ERROR_PATH_NOT_FOUND        (3)
#define SYS_ERROR_ACCESS_DENIED         (5)
#define SYS_ERROR_INVALID_DRIVE         (15)
#define SYS_ERROR_INVALID_NAME          (123)

#define SYS_MAX_PATH                    (260)                       // Max. length of full pathname
#define SYS_MAX_DRIVE                   (3)                         // Max. length of drive component
#define SYS_MAX_DIR                     (256)                       // Max. length of path component
#define SYS_MAX_FNAME                   (256)                       // Max. length of file name component
#define SYS_MAX_EXT                     (256)                       // Max. length of extension component

#ifndef NULL
  #define NULL  ((void*)0)
#endif

/*********************************************************************
*
*       Types
*
**********************************************************************
*/

typedef void* SYS_FILE_HANDLE;
typedef void* SYS_HANDLE;

/*********************************************************************
*
*       API, file I/O
*
**********************************************************************
*/

void              SYS_FILE_Close                  (SYS_FILE_HANDLE hFile);
SYS_FILE_HANDLE   SYS_FILE_Open                   (const char* sFile, U32 Flags);
int               SYS_FILE_Read                   (SYS_FILE_HANDLE hFile,       void* pData, U32 NumBytes);
int               SYS_FILE_Write                  (SYS_FILE_HANDLE hFile, const void* pData, U32 NumBytes);

/*********************************************************************
*
*       API, misc. functions
*
**********************************************************************
*/

int               SYS_GetTickCount                (void);
void              SYS_Sleep                       (int ms);

#if defined(__cplusplus)          // Allow usage of this module from C++ files (disable name mangling)
  }
#endif

#endif                            // Avoid multiple inclusion

/*************************** End of file ****************************/
