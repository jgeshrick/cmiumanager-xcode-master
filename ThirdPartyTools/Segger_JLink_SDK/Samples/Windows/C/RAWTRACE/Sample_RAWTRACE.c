/*********************************************************************
*              SEGGER MICROCONTROLLER SYSTEME GmbH                   *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*          (c) 2009 SEGGER Microcontroller Systeme GmbH              *
*                                                                    *
*       Internet: www.segger.com Support: support@segger.com         *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File    : Sample_RAWTRACE.c
Purpose : Raw trace sample program
---------------------------END-OF-HEADER------------------------------
*/

#include <stdio.h>
#ifdef WIN32
  #include <windows.h>
  #include <conio.h>
#else
  #include <unistd.h>
  #include <fcntl.h>
#endif
#include "JLinkARMDLL.h"
#include "main.h"

/*********************************************************************
*
*       Defines, configurable
*
**********************************************************************
*/

#define BLOCKSIZE_READ    (0x02000)
#define MAX_SIZE_READ     (0x100000)     // Max. number of bytes to read
#define TRACE_ID          (0x10)

/*********************************************************************
*
*       Defines, not configurable
*
**********************************************************************
*/

#define FILE_FLAG_READ          (1 << 0)                    // Open the file for reading.
#define FILE_FLAG_SHARE_READ    (1 << 2)                    // Allow other processes to open the same file for reading.
#define FILE_FLAG_SHARE_WRITE   (1 << 3)                    // Allow other processes to open the same file for writing.
#define FILE_FLAG_CREATE        (1 << 4)                    // Create new file. If the file already exist, open it.
#define FILE_FLAG_TRUNC         (1 << 5)                    // Truncate the file to zero length.
#define FILE_FLAG_WRITE         (1 << 1)                    // Open the file for writing.
#define FILE_FLAG_APPEND        (1 << 6)                    // Set file pointer to end of file.
#define FILE_FLAG_EXCL          (1 << 7)                    // Can be specified in combination with FILE_FLAG_CREATE. Open fails if the specified file already exists.

#ifndef WIN32
  #define getch                 getchar
  #define INVALID_HANDLE_VALUE  (-1)
  #define HANDLE                int
#endif 

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/

static const char*  _sFileRaw   = "JLink_RawTrace.log";
static const char*  _sFileTrace = "JLink_Trace.log";

static U8           _abFormatted[MAX_SIZE_READ];
static U8           _abRaw      [MAX_SIZE_READ];
static U32          _CurrentPC;
static int          _PCIsValid;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/
/*********************************************************************
*
*       _cbErrorOut
*/
static void _cbErrorOut(const char *s) {
  printf("\n ERROR: %s\n", s);
}

/*********************************************************************
*
*       _Sleep
*/
static void _Sleep(int ms) {
#ifdef WIN32
  Sleep(ms);
#else
  usleep(ms * 1000);
#endif
}

/*********************************************************************
*
*       _OpenFile
*/
static HANDLE _OpenFile(const char* sFile, U32 Flags) {
#ifdef WIN32
  HANDLE hFile;
  DWORD  AccessFlags = 0;
  DWORD  ShareFlags  = 0;
  DWORD  CreateFlags = 0;

  AccessFlags |= (Flags & FILE_FLAG_READ)        ? GENERIC_READ      : 0;
  AccessFlags |= (Flags & FILE_FLAG_WRITE)       ? GENERIC_WRITE     : 0;
  ShareFlags  |= (Flags & FILE_FLAG_SHARE_READ)  ? FILE_SHARE_READ   : 0;
  ShareFlags  |= (Flags & FILE_FLAG_SHARE_WRITE) ? FILE_SHARE_WRITE  : 0;
  if (Flags & FILE_FLAG_CREATE) {
    if        (Flags & FILE_FLAG_EXCL) {
      CreateFlags = CREATE_NEW;
    } else if (Flags & FILE_FLAG_TRUNC) {
      CreateFlags = CREATE_ALWAYS;
    } else {
      CreateFlags = OPEN_ALWAYS;
    }
  } else if (Flags & FILE_FLAG_TRUNC) {
    CreateFlags = TRUNCATE_EXISTING;
  } else {
    CreateFlags = OPEN_EXISTING;
  }
  hFile = CreateFile(sFile, AccessFlags, ShareFlags, NULL, CreateFlags, FILE_ATTRIBUTE_NORMAL, NULL);
  if (hFile != INVALID_HANDLE_VALUE) {
    if (Flags & FILE_FLAG_APPEND) {
      SetFilePointer(hFile, 0, 0, FILE_END);
    }
  }
  return hFile;
#else
  HANDLE hFile;
  U32    AccessFlags = 0;
  U32    CreateFlags = 0;

  AccessFlags |= (Flags & FILE_FLAG_READ)        ? O_RDONLY     : 0;
  AccessFlags |= (Flags & FILE_FLAG_WRITE)       ? O_WRONLY     : 0;
  if (Flags & FILE_FLAG_CREATE) {
    if        (Flags & FILE_FLAG_EXCL) {
      CreateFlags = O_CREAT | O_EXCL;
    } else if (Flags & FILE_FLAG_TRUNC) {
      CreateFlags = O_CREAT | O_TRUNC;
    } else {
      CreateFlags = O_CREAT;
    }
  } else if (Flags & FILE_FLAG_TRUNC) {
    CreateFlags = O_TRUNC;
  }
  hFile = (HANDLE)open(sFile, AccessFlags | CreateFlags);
  if (hFile != INVALID_HANDLE_VALUE) {
    if (Flags & FILE_FLAG_APPEND) {
      lseek(hFile, 0, SEEK_END);
    }
  }
  return hFile;
#endif
}

/*********************************************************************
*
*       _CloseFile
*/
static void _CloseFile(HANDLE hFile) {
#ifdef WIN32  
  CloseHandle((HANDLE)hFile);
#else
  close(hFile);
#endif
}

/*********************************************************************
*
*       _ReadFile
*/
static int _ReadFile(HANDLE hFile, void * pData, U32 NumBytes) {
#ifdef WIN32
  DWORD NumBytesRead;
  if (ReadFile((HANDLE)hFile, pData, NumBytes, &NumBytesRead, NULL) == 0) {
    return -1;                    // Error
  }
  return (int)NumBytesRead;       // O.K.
#else
  int NumBytesRead;

  NumBytesRead = read((int)hFile, pData, NumBytes);
  return NumBytesRead;
#endif
}

/*********************************************************************
*
*       _WriteFile
*/
static int _WriteFile(HANDLE hFile, const void* pData, U32 NumBytes) {
#ifdef WIN32
  DWORD NumBytesWritten;
  if (WriteFile((HANDLE)hFile, pData, NumBytes, &NumBytesWritten, NULL) == 0) {
    return -1;                    // Error
  }
  return (int)NumBytesWritten;    // O.K.
#else
  int NumBytesWritten;

  NumBytesWritten = write((int)hFile, pData, NumBytes);
  return NumBytesWritten;
#endif
}

/*********************************************************************
*
*       _GetFileSize
*/
static U32 _GetFileSize(HANDLE hFile) {
#ifdef WIN32
  DWORD FileSize;
  FileSize = GetFileSize((HANDLE)hFile, NULL);
  return (U32)FileSize;
#else
  U32   FileSize;
  off_t CurrentPos;

  CurrentPos = lseek((int)hFile, 0, SEEK_CUR);
  FileSize   = lseek((int)hFile, 0, SEEK_END);
  lseek((int)hFile, CurrentPos, SEEK_SET);
  return FileSize;
#endif
}

/*********************************************************************
*
*       _LoadU32Rev
*/
static U32 _LoadU32Rev(const U8* p) {
  U32 v;

  v  = *(p - 0) <<  0;
  v |= *(p - 1) <<  8;
  v |= *(p - 2) << 16;
  v |= *(p - 3) << 24;
  return v;
}

/*********************************************************************
*
*       _LoadU32
*/
static U32 _LoadU32(const U8* p) {
  U32 v;

  v  = *(p + 0) <<  0;
  v |= *(p + 1) <<  8;
  v |= *(p + 2) << 16;
  v |= *(p + 3) << 24;
  return v;
}

/*********************************************************************
*
*       _Analyze
*/
static int _Analyze(U8* p, int NumBytes, int NumPackets) {
  int NumBytesRem;
  U32 v;
  U8  Data;
  int Cnt = 0;
  int i;

  NumBytesRem = NumBytes;
  while (NumBytesRem && NumPackets) {

    Data = *p++;
    NumBytesRem--;

    if (Data == 0x00) {                     // A-Sync
      Cnt++;
      continue;
    } else if ((Cnt >= 5) && (Data == 0x80)) {
      printf("A-Sync\n");
      Cnt = 0;
      goto PacketDone;
    } else {
      Cnt = 0;
    }

    if (Data == 0x08) {                     // I-Sync
      printf("I-Sync");
      if (NumBytesRem < 5) {
        NumBytesRem = 0;
        goto AnalyzeDone;
      }
      Data = *p++;
      v = _LoadU32(p);
      p += 4;
      printf(", PC=0x%.8X\n", v);
      _CurrentPC = v;
      _PCIsValid = 1;
      NumBytesRem -= 5;
    } else if ((Data & 0x81) == 0x80) {     // P-Header
      printf("P-Header");
      if        ((Data & 0x83) == 0x80) {
        printf(" (format 1), %dxE %dxN\n", (Data >> 2) & 0xF, (Data >> 6) & 1);
      } else if ((Data & 0xF3) == 0x82) {
        printf(" (format 2),");
        printf(" 1x%s",   (Data & (1 << 3)) ? "N" : "E");
        printf(" 1x%s\n", (Data & (1 << 2)) ? "N" : "E");
      } else {
        printf(" (unknown format)\n");
      }
      NumBytesRem--;
    } else if ((Data & 0x01) == 0x01) {     // Branch
      U32 Addr;
      U8  CBit;
      int Bits = 7;

      Addr = (Data & 0x7E);
      for (i = 0; i < 3; i++) {
        CBit = (Data >> 7) & 1;
        if (CBit == 0) {
          break;
        }
        if (NumBytesRem == 0) {
          goto AnalyzeDone;
        }
        Data = *p++;
        NumBytesRem--;
        Addr |= (Data & 0x7F) << Bits;
        Bits += 7;
      }
      CBit = (Data >> 7) & 1;
      if (CBit) {
        if (NumBytesRem == 0) {
          goto AnalyzeDone;
        }
        Data = *p++;
        NumBytesRem--;
        Addr  |= (Data & 0x0F) << 28;
        Bits = 32;
        _PCIsValid = 1;
      }
      printf("Branch, Addr=0x%.2X, Bits=%d", Addr, Bits);
      if (_PCIsValid) {
        _CurrentPC &= (Bits == 32) ? (0) : (~(0) << Bits);
        _CurrentPC |= Addr;
        printf(", PC=0x%.8X\n", _CurrentPC);
      } else {
        printf("\n");
      }
    } else {
      printf("???\n");
    }
PacketDone:
    NumPackets--;
  }
AnalyzeDone:
  return NumBytes - NumBytesRem;
}

/*********************************************************************
*
*       _Unformat
*/
static int _Unformat(U8* pDataIn, U8* pDataOut, int Off) {
  U8* pIn;
  U8  Byte15;
  U8  Data;
  U8  Id;
  U8  CurrentId;
  U32 v;
  int NumBytes;
  int i;

  NumBytes = 0;
  while (1) {
    while (Off >= 4) {
      v = _LoadU32Rev(pDataIn + Off);
      if (v != 0x7FFFFFFF) {
        break;
      }
      Off -= 4;
    }
    if (Off < 15) {
      break;
    }
    pIn = pDataIn + Off;
    Byte15 = *(pIn - 15);
    for (i = 0; i < 15; i++) {
      Data = *(pIn - i);
      if ((i & 1) == 0) {
        if (Data & 1) {
          Id = Data >> 1;
          if (Id == 0x7F) {
            i++;
          } else {
            CurrentId = Id;
          }
          continue;
        } else {
          Data = Data | ((Byte15 >> (i >> 1)) & 1);
        }
      }
      if (CurrentId == 0x00) {        // Skipr dummy fill bytes
        continue;
      }
      if (CurrentId != TRACE_ID) {    // Skip non-trace data
        continue;
      }
      *(pDataOut + NumBytes) = Data;
      NumBytes++;
    }
    Off -= 16;
  }
  return NumBytes;
}


/*********************************************************************
*
*       _Log
*/
static void _Log(const char *s) {
  printf(s);
  if (*(s + strlen(s) - 1) != '\n') {
    printf("\n");
  }
}

/*********************************************************************
*
*       _Init
*/
static void _Init(void) {
  JLINKARM_TIF_Select(JLINKARM_TIF_SWD);
  JLINKARM_SetSpeed(4000);
  JLINKARM_ClrRESET();
  _Sleep(5);
  JLINKARM_WriteU32(0xE000EDFC, 0x01000000);    // DEMC_R : set TRCENA : otherwise, TPIU registers are not accessible
  JLINKARM_WriteU32(0xE00400F0, 0x00000000);    // TPIU/SPP : select SYNC PORT Mode
  JLINKARM_WriteU32(0xE0040004, 0x00000008);    // TPIU/CPS : select PORT SIZE=4
  JLINKARM_WriteU32(0xE0041FB0, 0xC5ACCE55);    // ETM Unlock Lock Access register
  JLINKARM_WriteU32(0xE0041000, 0x00003d00);    // Set Programming bit (b10)
  JLINKARM_WriteU32(0xE00411E0, 0x00000100);    // Outputs a sync every 256 cycles
  JLINKARM_WriteU32(0xE0041008, 0x0000006F);    // ETM Trigger Event Register, start trace unconditionally
  JLINKARM_WriteU32(0xE004101C, 0x00000000);    // ETM TraceEnable Control2 Register
  JLINKARM_WriteU32(0xE0041020, 0x0000006F);    // ETM TraceEnable Event Register
  JLINKARM_WriteU32(0xE0041024, 0x01000000);    // ETM TraceEnable Control1 Register
  JLINKARM_WriteU32(0xE00411F0, 0x00000001);    // ETM Trace Start/Stop
  JLINKARM_WriteU32(0xE00411F4, 0x00000001);    // ETM EmbeddedICE Behavior Register
  JLINKARM_WriteU32(0xE0041000, 0x00003c00);    // ETM Control Register: Port mode 001: 2:1; Cycle acurate; ETMEN, branch output; Portsize 4 bit; Access: No data tracing
  JLINKARM_WriteU32(0xE0041000, 0x00003800);    // ETM Control Register: Port mode 001: 2:1; Cycle acurate; ETMEN, branch output; Portsize 4 bit; Access: No data tracing
  JLINKARM_WriteU32(0xE0041200, TRACE_ID);      // CoreSight Trace ID register
  JLINKARM_WriteU32(0xE0042004, 0x000000E0);    // DBG_MCU_CR : assign SYNC TRACE I/Os for SIZE=4
  _Sleep(5);
  JLINKARM_SetRESET();
  JLINKARM_Halt();
}

/*********************************************************************
*
*       _WriteTraceDataToFile
*/
static void _WriteTraceDataToFile(const U8* pData, U32 NumBytes) {
  HANDLE hFile;
  U32 NumBytesWritten;

  printf("Writing unformatted trace data to file %s\n", _sFileTrace);
  //
  // Create file
  //
  hFile = _OpenFile(_sFileTrace, FILE_FLAG_CREATE | FILE_FLAG_WRITE | FILE_FLAG_SHARE_READ | FILE_FLAG_TRUNC);
  if (hFile == INVALID_HANDLE_VALUE) {
    printf("ERROR: Failed to create file \"%s\".\n", _sFileTrace);
    return;
  }
  //
  // Write trace data to file
  //
  NumBytesWritten = _WriteFile(hFile, pData, NumBytes);
  if (NumBytesWritten != NumBytes) {
    printf("ERROR: Failed to write file \"%s\".\n", _sFileTrace);
    goto Done;
  }
Done:
  _CloseFile(hFile);
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
#ifdef APP_RAWTRACE_SAMPLE
/*********************************************************************
*
*       main
*/
void main(void) {
#else
/*********************************************************************
*
*       RAWTRACE_Exec
*/
void RAWTRACE_Exec(void) {
#endif
  HANDLE hFile;
	const char* sErr = NULL;
  U8* p;
  int NumBytes;
  int NumBytesRem;
  int NumBytesAtOnce;
  int NumBytesWritten;
  int r, c;
  int Cnt;
  int Off;
  int OffSync;
  U32 Caps;
  U8  Data;

  printf("RAWTRACE Demonstration program for SEGGER J-Trace for Cortex-M3\n");
  printf("(c) 2010 SEGGER Microcontroller\n");
  //
  // Open conenction to emulator and check if it can perform RAWTrace
  //
  JLINKARM_Close();
  sErr = JLINKARM_OpenEx(_Log, _cbErrorOut);
  if (sErr) {
    goto OnError;
  }
  Caps = JLINKARM_GetEmuCaps();
  if ((Caps & JLINKARM_EMU_CAP_RAWTRACE) == 0) {
    printf("ERROR: Connected emulator does not support RAWTrace.\n");
    getch();
    return;    
  }
  printf("Found J-Link compatible emulator");
  printf(" with RAWTrace support\n");
  _Init();
  while (1) {
    //
    // Wait for key
    //
    printf("\n***********************************************************************\n");
    printf("Press any key to start trace !\n");
    getch();
    //
    // Start trace
    //
    r = JLINKARM_RAWTRACE_Control(JLINKARM_RAWTRACE_CMD_GET_TRACE_FREQ, NULL);
    if (r <= 0) {
      printf("ERROR: No trace frequency measured.\n");
      continue;
    }
    r = JLINKARM_RAWTRACE_Control(JLINKARM_RAWTRACE_CMD_START, NULL);
    if (r < 0) {
      printf("ERROR: Failed to start trace.\n");
      continue;
    }
    printf("Trace started. TraceFreq = %dHz\n", r);
    JLINKARM_Go();
    _Sleep(10);
    JLINKARM_Halt();
    //
    // Stop trace
    //
    NumBytes = JLINKARM_RAWTRACE_Control(JLINKARM_RAWTRACE_CMD_STOP, NULL);
    if (NumBytes < 0) {
      printf("ERROR: Failed to stop trace.\n");
      continue;
    }
    printf("Trace stopped. 0x%X bytes in trace buffer\n", NumBytes);
    //
    // Read and analyze trace data
    //
    if (NumBytes > 0) {
      NumBytes = MIN(NumBytes, MAX_SIZE_READ);
      printf("Reading data from trace buffer into file %s", _sFileRaw);
      //
      // Create file
      //
      hFile = _OpenFile(_sFileRaw, FILE_FLAG_CREATE | FILE_FLAG_WRITE | FILE_FLAG_SHARE_READ | FILE_FLAG_TRUNC);
      if (hFile == INVALID_HANDLE_VALUE) {
        printf("ERROR: Failed to create file \"%s\".\n", _sFileRaw);
        continue;
      }
      //
      // Read trace data and write it to file
      //
      p = &_abFormatted[0];
      NumBytesRem = NumBytes;
      while (NumBytesRem > 0) {
        NumBytesAtOnce = MIN(NumBytesRem, BLOCKSIZE_READ);
        r = JLINKARM_RAWTRACE_Read(p, NumBytesAtOnce);
        if (r != NumBytesAtOnce) {
          printf("ERROR: Failed to read trace data.\n");
          break;
        }
        NumBytesWritten = _WriteFile(hFile, p, NumBytesAtOnce);
        if (NumBytesWritten != NumBytesAtOnce) {
          printf("ERROR: Failed to write file \"%s\".\n", _sFileRaw);
          break;
        }
        printf(".");
        p           += NumBytesAtOnce;
        NumBytesRem -= NumBytesAtOnce;
      }
      _CloseFile(hFile);
      printf("\n");
      //
      // Find frame sync. packet
      //
      OffSync = -1;
      Cnt = 0;
      p = &_abFormatted[0];
      Off = NumBytes - 1;
      while (Off >= 0) {
        Data = *(p + Off);
        if (Data == 0xFF) {
          Cnt++;
        } else if ((Cnt >= 3) && (Data == 0x7F)) {
          OffSync = Off;
          break;
        } else {
          Cnt = 0;
        }
        Off--;
      }
      if (OffSync >= 0) {
        printf("Found frame-sync packet @ offset 0x%.2X.\n", OffSync);
        NumBytes = _Unformat(p, _abRaw, OffSync - 1);
        if (NumBytes >= 0) {
          printf("0x%.2X bytes trace data extracted.\n", NumBytes);
        } else {
          printf("ERROR: Failed to extract trace data.\n");
          continue;
        }
        _WriteTraceDataToFile(_abRaw, NumBytes);
      } else {
        printf("No frame-sync packet found.\n");
        continue;
      }
      //
      // Find A-sync packet
      //
      OffSync = -1;
      Cnt = 0;
      p = &_abRaw[0];
      Off = 0;
      while (Off < NumBytes) {
        Data = *p++;
        if (Data == 0x00) {
          Cnt++;
        } else if ((Cnt >= 5) && (Data == 0x80)) {
          OffSync = Off - 4;
          break;
        } else {
          Cnt = 0;
        }
        Off++;
      }
      if (OffSync >= 0) {
        printf("Found A-sync packet @ offset 0x%.2X.\n", OffSync);
      } else {
        printf("No A-sync packet found.\n");
        continue;
      }
      //
      // Analyze data
      //
      _PCIsValid = 0;
      Off = OffSync + 5;
      p = &_abRaw[0] + Off;
      NumBytes -= Off;
      while (NumBytes > 0) {
        r = _Analyze(p, NumBytes, 10);
        p += r;
        NumBytes -= r;
        if (NumBytes <= 0) {
          break;
        }
        printf("Press <n> for more data or <q> to end analyzing!\n");
        do {
          c = getch();
        } while ((c != 'n') && (c != 'q'));
        if (c == 'q') {
          break;
        }
        printf("\n");
      }
    }
  }
OnError:
  JLINKARM_Close();
}

/*************************** end of file ****************************/
