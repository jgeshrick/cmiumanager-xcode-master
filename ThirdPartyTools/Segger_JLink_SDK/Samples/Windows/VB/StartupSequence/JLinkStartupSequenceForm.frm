VERSION 5.00
Begin VB.Form JLinkStartupSequenceForm 
   BorderStyle     =   1  'Fest Einfach
   Caption         =   "J-Link Startup"
   ClientHeight    =   3600
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6015
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3600
   ScaleWidth      =   6015
   StartUpPosition =   3  'Windows-Standard
   Begin VB.Frame Frame1 
      Caption         =   "Info"
      Height          =   1335
      Left            =   1440
      TabIndex        =   8
      Top             =   2040
      Width           =   3135
      Begin VB.Label Label4 
         Caption         =   "DLL Version:"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   480
         Width           =   2535
      End
      Begin VB.Label Label5 
         Caption         =   "Hardware:"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   720
         Width           =   2415
      End
      Begin VB.Label Label6 
         Caption         =   "S/N:"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   960
         Width           =   2535
      End
      Begin VB.Label Label7 
         Caption         =   "Status: disconnected"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   240
         Width           =   2655
      End
   End
   Begin VB.TextBox PortNoIPTxt 
      Height          =   375
      Left            =   4200
      TabIndex        =   4
      Text            =   "19020"
      Top             =   1440
      Width           =   1575
   End
   Begin VB.TextBox HostNameTxt 
      Height          =   375
      Left            =   4200
      TabIndex        =   3
      Text            =   "localhost"
      Top             =   960
      Width           =   1575
   End
   Begin VB.TextBox PortNoTxt 
      Height          =   375
      Left            =   1440
      TabIndex        =   2
      Text            =   "0"
      Top             =   960
      Width           =   975
   End
   Begin VB.CommandButton IPConnectbtn 
      Caption         =   "Test J-Link connection via IP"
      Height          =   495
      Left            =   3120
      TabIndex        =   1
      Top             =   240
      Width           =   2655
   End
   Begin VB.CommandButton USBConnectbtn 
      Caption         =   "Test J-Link connection via USB"
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   2655
   End
   Begin VB.Label Label3 
      Caption         =   "PortNo."
      Height          =   255
      Left            =   3120
      TabIndex        =   7
      Top             =   1560
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Hostname/IP"
      Height          =   255
      Left            =   3120
      TabIndex        =   6
      Top             =   1080
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "JLink USB No."
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   1080
      Width           =   1215
   End
End
Attribute VB_Name = "JLinkStartupSequenceForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub TestConnection()
  Dim sErr As String
  Dim r As Long
  Dim aData(100) As Long
  Dim IDData As Long
  sErr = JLink_OpenEx(0&, AddressOf ErrorOut)
  If sErr = vbNullString Then
    IDData = JLINK_GetId
    'MsgBox "Connection to J-Link was successful." & vbNewLine & "ID of device = 0x" & Hex(IDData), , "VisualBasic J-Link Sample application"
    Label7.Caption = "Status: J-Link connection successful"
    JLINK_SetSpeed (30)
    GetInfo
    JLINK_Close
  Else
    'MsgBox "Connection to J-Link was not successful." & vbNewLine & "" & sErr, , "VisualBasic J-Link Sample application"
    Label4.Caption = "DLL Version:"
    Label5.Caption = "Hardware:"
    Label6.Caption = "S/N:"
    Label7.Caption = "Status: J-Link connection unsuccessful"
  End If
End Sub

Private Sub IPConnectbtn_Click()
  JLINK_Close
  If HostNameTxt.Text <> "" Then
    JLINK_SelectIP HostNameTxt.Text, CLng(PortNoIPTxt.Text)
  Else
    JLINK_SelectIP "", CLng(PortNoIPTxt.Text)
  End If
  TestConnection
End Sub

Private Sub USBConnectbtn_Click()
  JLINK_Close
  If PortNoTxt.Text <> "" Then
    JLINK_EMU_SelectByUSBSN (CLng(PortNoTxt.Text))
  End If
  TestConnection
End Sub

Private Sub GetInfo()
  Dim Ver As Long
  Dim Sn As Long

  Ver = JLINK_GetDLLVersion
  Label4.Caption = "DLL Version: " & CInt(Ver / 10000) & "." & (Ver / 100) Mod 100 & "." & (Ver Mod 100)
  Ver = JLINK_GetHardwareVersion
  Label5.Caption = "Hardware: V" & (Ver / 10000) Mod 100 & "." & (Ver / 100) Mod 100
  Sn = JLINK_GetSN
  Label6.Caption = "S/N: " & Sn
End Sub
