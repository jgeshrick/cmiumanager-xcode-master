<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~
  ~ ***************************************************************************
  ~
  ~     Neptune Technology Group
  ~     Copyright 2015, 2016 as unpublished work.
  ~     All rights reserved
  ~
  ~     The information contained herein is confidential
  ~     property of Neptune Technology Group. The use, copying, transfer
  ~     or disclosure of such information is prohibited except; by express
  ~     written agreement with Neptune Technology Group.
  ~
  ~ ***************************************************************************
  ~
  ~
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.neptunetg.mdce.integrationtest</groupId>
    <artifactId>mdce-integration-test</artifactId>
    <version>1.0-SNAPSHOT</version>


    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- Test -->
        <junit.version>4.12</junit.version>
        <selenium.version>3.0.1</selenium.version>

        <!-- MDCE dependencies -->
        <!--<common-packet-utils.version>1.0-SNAPSHOT</common-packet-utils.version>-->
        <common-packet-utils.version>1.6.170517.178</common-packet-utils.version>

        <!--<mdce-common-internal-api.version>1.0-SNAPSHOT</mdce-common-internal-api.version>-->
        <mdce-common-internal-api.version>1.6.170427.238</mdce-common-internal-api.version>

        <!-- common-aws-ipc.version>1.0-SNAPSHOT</common-aws-ipc.version -->
        <common-aws-ipc.version>1.5.170323.81</common-aws-ipc.version>

        <!--<common-cellular-network-interface.version>1.0-SNAPSHOT</common-cellular-network-interface.version>-->
        <common-cellular-network-interface.version>1.6.170626.80</common-cellular-network-interface.version>

        <!--Spring-->
        <spring-framework.version>4.2.2.RELEASE</spring-framework.version>

        <!-- AWS -->
        <aws.sdk.version>1.10.27</aws.sdk.version>

        <!-- Logging -->
        <logback.version>1.1.7</logback.version>
        <slf4j.version>1.7.5</slf4j.version>

    </properties>

    <dependencies>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring-framework.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${spring-framework.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- JNA allows Java to call code in DLLs -->
        <dependency>
            <groupId>net.java.dev.jna</groupId>
            <artifactId>jna</artifactId>
            <scope>test</scope>
            <version>4.1.0</version>
        </dependency>

        <!-- Selenium for mdce-web testing -->
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-chrome-driver</artifactId>
            <version>${selenium.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-support</artifactId>
            <version>${selenium.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.4</version>
            <scope>test</scope>
        </dependency>


        <!-- Jackson for JSON serialization and deserialization -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.5.0</version>
            <scope>test</scope>
        </dependency>


        <!-- JsonPacketData parser and builder -->
        <dependency>
            <groupId>com.neptunetg.common</groupId>
            <artifactId>common-packet-utils</artifactId>
            <version>${common-packet-utils.version}</version>
            <scope>test</scope>
        </dependency>


        <!--For accessing Amazon AWS services-->
        <dependency>
            <groupId>com.amazonaws</groupId>
            <artifactId>aws-java-sdk-s3</artifactId>
            <version>${aws.sdk.version}</version>
            <scope>test</scope>
        </dependency>

        <!--MDCE common internal api code-->
        <dependency>
            <groupId>com.neptunetg.mdce.common</groupId>
            <artifactId>mdce-common-internal-api</artifactId>
            <version>${mdce-common-internal-api.version}</version>
            <scope>test</scope>
        </dependency>

        <!--MDCE common aws ipc -->
        <dependency>
            <groupId>com.neptunetg.common</groupId>
            <artifactId>common-aws-ipc</artifactId>
            <version>${common-aws-ipc.version}</version>
        </dependency>

        <!-- Common network interface code.  Used for some data objects (DeviceCelullarInformation, UsageHistory) only. -->
        <dependency>
            <groupId>com.neptunetg.common</groupId>
            <artifactId>common-cellular-network-interface</artifactId>
            <version>${common-cellular-network-interface.version}</version>
        </dependency>

        <dependency>
            <groupId>com.jayway.jsonpath</groupId>
            <artifactId>json-path</artifactId>
            <version>2.0.0</version>
            <scope>test</scope>
        </dependency>

        <!-- Commons Compression - used to create tarballs for N_SIGHT -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-compress</artifactId>
            <version>1.9</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.jayway.jsonpath</groupId>
            <artifactId>json-path-assert</artifactId>
            <version>2.0.0</version>
            <scope>test</scope>
        </dependency>

        <!-- Commons Net FTP - used for FTP server test -->
        <dependency>
            <groupId>commons-net</groupId>
            <artifactId>commons-net</artifactId>
            <version>3.4</version>
        </dependency>

        <!-- Logging with SLF4J & LogBack -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback.version}</version>
            <scope>runtime</scope>
        </dependency>

    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.2</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>test-jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
                <checksumPolicy>warn</checksumPolicy>
            </releases>
            <snapshots>
                <enabled>false</enabled>
                <updatePolicy>never</updatePolicy>
                <checksumPolicy>fail</checksumPolicy>
            </snapshots>
            <id>neptune-nexus</id>
            <name>Neptune project Nexus server on lochness:8081</name>
            <url>http://10.3.1.46:8081/nexus/content/repositories/releases/</url>
            <layout>default</layout>
        </repository>
    </repositories>

</project>

