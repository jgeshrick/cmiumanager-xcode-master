/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.framework;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetPacketsResponse
{
    private List<MiuResponse> mius;

    private List<GatewayResponse> gateways;

    @JsonProperty("server_time")
    private String serverTime;

    public List<MiuResponse> getMius()
    {
        return this.mius;
    }

    public void setMius(List<MiuResponse> miuList)
    {
        this.mius = miuList;
    }

    public List<GatewayResponse> getGateways()
    {
        return gateways;
    }

    public void setGateways(List<GatewayResponse> gateways)
    {
        this.gateways = gateways;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }
}
