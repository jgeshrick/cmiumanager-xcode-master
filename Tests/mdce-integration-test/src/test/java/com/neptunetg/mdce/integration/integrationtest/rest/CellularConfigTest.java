/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.rest;

import com.jayway.jsonpath.JsonPath;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseSetupRestTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.HttpStatus;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test Interface: IF39, IF40
 */
public class CellularConfigTest extends BaseSetupRestTest
{

    public final String NEW_CONFIG_REQUEST_BODY = "{" +
            "\"miu_id\": " + getTestCmiuId() + "," +
            "\"modem_config\": " +
            "{" +
            "\"vendor\": \"new vendor\"," +
            "\"mno\": \"ATT\"," +
            "\"imei\": \"345634564567\"" +
            "}," +
            "\"sim_config\": " +
            "{" +
            "\"iccid\": \"5678123456781234\"" +
            "}" +
            "" +
            "}";

    public CellularConfigTest()
    {
        super(400000246); //AT&T CMIU defined in svn://redsea:3691/MSPD/trunk/Galileo/environments/aws/nonprod/srv/salt/galileo/auto-test/auto-test-test-data.sql
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testSetAndGetCellularConfig() throws Exception
    {
        final String tokenFpc = fetchTokenForFpc();

        String getOriginalConfigResponse = HttpUtility.httpGet(Constants.FPC_URL + "api/v1/miu/" +
                getTestCmiuId() + "/cellular-config?token=" + tokenFpc);

        //OK response is asserted in the post
        HttpUtility.httpJsonPost(Constants.FPC_URL + "api/v1/miu/" +
                getTestCmiuId() + "/cellular-config?token=" + tokenFpc + "&doRegisterWithMno=false", NEW_CONFIG_REQUEST_BODY);

        String getResponse = HttpUtility.httpGet(Constants.FPC_URL + "api/v1/miu/" +
                getTestCmiuId() + "/cellular-config?token=" + tokenFpc);

        assertEquals(getTestCmiuId(), (int) JsonPath.read(getResponse, "$.miu_id"));
        assertEquals("new vendor", JsonPath.read(getResponse, "$.modem_config.vendor"));
        assertEquals("345634564567", JsonPath.read(getResponse, "$.modem_config.imei"));

        assertEquals("5678123456781234", JsonPath.read(getResponse, "$.sim_config.iccid"));

        //set it back

        final String setCellularConfigToOriginal = constructNewConfigRequest(getOriginalConfigResponse);

        //OK response is asserted in the post
        String setBackResponse = HttpUtility.httpJsonPost(Constants.FPC_URL + "api/v1/miu/" +
                getTestCmiuId() + "/cellular-config?token=" + tokenFpc + "&doRegisterWithMno=false", setCellularConfigToOriginal);

        String getOriginalConfigResponse2 = HttpUtility.httpGet(Constants.FPC_URL + "api/v1/miu/" +
                getTestCmiuId() + "/cellular-config?token=" + tokenFpc);

        assertTrue(JsonPath.read(getOriginalConfigResponse, "$.miu_id").equals(
                JsonPath.read(getOriginalConfigResponse2, "$.miu_id")));
        assertTrue(JsonPath.read(getOriginalConfigResponse, "$.modem_config.vendor").equals(
                JsonPath.read(getOriginalConfigResponse2, "$.modem_config.vendor")));
        assertTrue(JsonPath.read(getOriginalConfigResponse, "$.modem_config.mno").equals(
                JsonPath.read(getOriginalConfigResponse2, "$.modem_config.mno")));
        assertTrue(JsonPath.read(getOriginalConfigResponse, "$.modem_config.imei").equals(
                JsonPath.read(getOriginalConfigResponse2, "$.modem_config.imei")));
        assertTrue(JsonPath.read(getOriginalConfigResponse, "$.sim_config.iccid").equals(
                JsonPath.read(getOriginalConfigResponse2, "$.sim_config.iccid")));
    }

    /**
     * Getting cellular config from a site id not owning the CMIU is forbidden.
     * @throws Exception
     */
    @Test
    public void testGetCellularConfigNotPermitted() throws Exception
    {
        //assume identity for site not owning the cmiu
        final String badToken = fetchTokenForSiteId(64);

        this.expectedException.expectMessage(containsString("Response code: 401"));

        HttpUtility.httpGet(Constants.FPC_URL + "/api/v1/miu/" +
                getTestCmiuId() + "/cellular-config?token=" + badToken);

    }

    /**
     * Setting cellular config from a site id not owning the CMIU is forbidden.
     */
    @Test
    public void testSetCellularConfigNotPermitted() throws Exception
    {
        //assume identity for site not owning the cmiu
        final String badToken = fetchTokenForSiteId(64);

        //setup expected exception
        this.expectedException.expectMessage(containsString("Response code: 401"));

        HttpUtility.httpJsonPost(Constants.FPC_URL + "/api/v1/miu/" +
                getTestCmiuId() + "/cellular-config?token=" + badToken + "&doRegisterWithMno=false", NEW_CONFIG_REQUEST_BODY);


    }

    private static String constructNewConfigRequest(String jsonGetConfigResponse )
    {
        return "{" +
                "\"miu_id\": " + JsonPath.read(jsonGetConfigResponse, "$.miu_id") + "," +
                "\"modem_config\": " +
                "{" +
                "\"vendor\": \"" + JsonPath.read(jsonGetConfigResponse, "$.modem_config.vendor") + "\"," +
                "\"mno\": \"" + JsonPath.read(jsonGetConfigResponse, "$.modem_config.mno") + "\"," +
                "\"imei\": \"" + JsonPath.read(jsonGetConfigResponse, "$.modem_config.imei") + "\"" +
                "}," +
                "\"sim_config\": " +
                "{" +
                "\"iccid\": \"" + JsonPath.read(jsonGetConfigResponse, "$.sim_config.iccid") + "\"" +
                "}" +
                "" +
                "}";
    }

}
