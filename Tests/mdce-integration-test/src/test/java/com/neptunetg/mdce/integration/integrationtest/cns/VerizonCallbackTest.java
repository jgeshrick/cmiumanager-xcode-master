/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.cns;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.StreamHelper;
import com.neptunetg.mdce.common.utility.TrustModifier;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.*;

/**
 * This is to ensure that the UWS SOAP end point is up and running
 */
public class VerizonCallbackTest
{
    private final String SOAP_URL = Constants.MDCE_INTEGRATION_URL + "/soap";

    @Test
    public void testCarrierServiceDeviceActivatedCallback() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException
    {
        String response = this.doSoapRequest("/VerizonUwsCallbackRequest/ChangeDeviceState_Activate.xml");
    }

    @Test
    public void testCarrierServiceDeviceSuspendedCallback() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException
    {
        String response = this.doSoapRequest("/VerizonUwsCallbackRequest/ChangeDeviceState_Suspend.xml");
    }

    @Test
    public void testCarrierServiceDeviceRestoredCallback() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException
    {
        String response = this.doSoapRequest("/VerizonUwsCallbackRequest/ChangeDeviceState_Restore.xml");
    }

    @Test
    public void testCarrierServiceDeviceDeactivatedCallback() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException
    {
        String response = this.doSoapRequest("/VerizonUwsCallbackRequest/ChangeDeviceState_Deactivate.xml");
    }

    @Test
    public void testExternalProvisioningDeviceActivatedCallback() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException
    {
        String response = this.doSoapRequest("/VerizonUwsCallbackRequest/ExternalProvisioningChanges_Activate.xml");
    }

    @Test
    public void testExternalProvisioningDeviceSuspendCallback() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException
    {
        String response = this.doSoapRequest("/VerizonUwsCallbackRequest/ExternalProvisioningChanges_Suspend.xml");
    }

    @Test
    public void testExternalProvisioningDeviceRestoreCallback() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException
    {
        String response = this.doSoapRequest("/VerizonUwsCallbackRequest/ExternalProvisioningChanges_Restore.xml");
    }

    @Test
    public void testExternalProvisioningDeviceDeactivatedCallback() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException
    {
        String response = this.doSoapRequest("/VerizonUwsCallbackRequest/ExternalProvisioningChanges_Deactivate.xml");
    }


    @Test
    public void testDeviceServiceDataUsageCallback() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException
    {
        String response = this.doSoapRequest("/VerizonUwsCallbackRequest/GetAggregateDeviceUsage.xml");
    }

    private String doSoapRequest(String resourcePath) throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException
    {
        final URL postUrl = new URL(SOAP_URL);

        //write request
        HttpURLConnection httpCon = TrustModifier.openConnectionIgnoreSelfSignCert(postUrl) ;

        httpCon.setDoOutput(true);
        httpCon.setRequestMethod("POST");
        httpCon.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
        httpCon.setRequestProperty("SOAPAction", "");

        String requestXml = StreamHelper.inputStreamToString(getClass().getResourceAsStream(resourcePath));

        OutputStream reqStream = httpCon.getOutputStream();
        reqStream.write(requestXml.getBytes());

        //get response
        String responseBody = "";
        try
        {
            InputStream responseStream = httpCon.getInputStream();
            responseBody = StreamHelper.inputStreamToString(responseStream);
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }

        if (httpCon.getResponseCode() != 200)
        {
            InputStream error = httpCon.getErrorStream();
            StreamHelper.copyStream(error, System.err);
        }

        assertEquals(200, httpCon.getResponseCode());


        System.out.println("Response: \n" + responseBody);
        assertTrue(responseBody.contains("RequestId"));
        assertFalse(responseBody.contains("Fault"));
        assertFalse(responseBody.contains("faultcode"));
        assertFalse(responseBody.contains("faultstring"));

        return  responseBody;
    }
}
