/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.framework;

import com.neptunetg.mdce.web.integrationtest.UserLogin;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Base class for selenium test
 */
public class SeleniumTestBase
{
    private final String webUrl;

    private final String testSuiteName;

    private String testName;

    private final AtomicInteger screenshotNumber = new AtomicInteger(1);

    public final String CHROME_DOWNLOAD_LOCATION = "C:/temp/selenium_chrome_downloads";

    public ChromeDriver driver;

    public SeleniumTestBase()
    {
        this(Constants.MDCE_WEB_URL);
    }

    public SeleniumTestBase(String webUrl)
    {
        this.webUrl = webUrl;

        String className = getClass().getName();
        int lastDot = className.lastIndexOf('.');
        int lastCp = className.length();
        if (className.endsWith("SeleniumTest"))
        {
            lastCp -= "SeleniumTest".length();
        }

        this.testSuiteName = className.substring(lastDot + 1, lastCp);

    }

    /**
     * See http://cwd.dhemery.com/2010/12/junit-rules/ for a good guide to how this works
     */
    @Rule
    public TestRule screenshotOnFailureRule = new ScreenshotOnFailureRule();


    private class ScreenshotOnFailureRule implements TestRule
    {

        public Statement apply(Statement statement, Description desc)
        {
            return new ScreenshotOnFailureStatement(statement, desc);
        }

    }

    private class ScreenshotOnFailureStatement extends Statement
    {

        private final Statement wrappedStatement;
        private final Description testDescription;

        public ScreenshotOnFailureStatement(Statement wrappedStatement, Description testDescription)
        {
            super();
            this.wrappedStatement = wrappedStatement;
            this.testDescription = testDescription;
            SeleniumTestBase.this.testName = SeleniumTestBase.this.testSuiteName + "_" + testDescription.getMethodName();
        }


        @Override
        public void evaluate() throws Throwable
        {
            System.out.println("**************  STARTING TEST: " + SeleniumTestBase.this.testName + " **********************");

            // Optional, if not specified, WebDriver will search your path for chromedriver.
            System.setProperty("webdriver.chrome.driver", getClass().getResource(Constants.SELENIUM_CHROME_DRIVER_PATH).getPath());

            Map<String, Object> chromePrefs = new HashMap<>();
            chromePrefs.put("download.default_directory", CHROME_DOWNLOAD_LOCATION);
            (new File(CHROME_DOWNLOAD_LOCATION)).mkdirs();

            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("prefs", chromePrefs);
            options.addArguments("no-sandbox");

            DesiredCapabilities capability = DesiredCapabilities.chrome();
            capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            capability.setCapability(ChromeOptions.CAPABILITY, options);

            driver = new ChromeDriver(capability);
            WebDriver.Options driverOptions = driver.manage();
//            driverOptions.window().maximize();
//            driverOptions.timeouts().implicitlyWait(3, TimeUnit.SECONDS);//set timeout

            driver.get(webUrl);


            if (driver.findElements(By.className("top-bar")).isEmpty())
            {

                driver.quit();

                Assert.fail("Cannot retrieve MDCE Web homepage at " + webUrl + ".  Check URL.  ");
            }

            try
            {
                doLogin(UserLogin.ADMIN_USER, UserLogin.ADMIN_PWD);
                wrappedStatement.evaluate();
            }
            catch (Throwable e)
            {

                Throwable cause = e.getCause();
                if (cause != null)
                {
                    cause.fillInStackTrace();
                }

                this.testDescription.getMethodName();
                PrintStream captionOut = takeScreenshotWithCaption(this.testDescription.getMethodName() + "_FAIL");
                if (driver == null)
                {
                    captionOut.println("driver is null");
                }
                else
                {
                    captionOut.println(driver.getCurrentUrl());
                }
                captionOut.println();
                e.printStackTrace(captionOut);
                captionOut.close();

                throw e;
            }
            finally
            {
                doLogout();

                if (driver != null)
                {
                    driver.quit();//Determines whether to close browser afterwards
                }
                ScreenshotManager.writeIndex();
                System.out.println("**************  FINISHED TEST: " + SeleniumTestBase.this.testName + " **********************");

            }
        }

        /**
         * Submit username and password in login page and check for page to be redirected
         */
        private void doLogin(String userName, String password)
        {
            final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("username")));
            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("login")));

            driver.findElement(By.id("username")).sendKeys(userName);

            driver.findElement(By.id("password")).sendKeys(password);

            takeScreenshot("Login as admin. " + Instant.now());

            driver.findElement(By.id("login")).submit();

            //check that we have navigated away from the login webpage
            try
            {
                webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("//h1[contains(text(), 'Login')]")));
                takeScreenshot("Redirect to target after login. " + Instant.now());
            }
            catch(TimeoutException ex)
            {
                //is there a login failure?
                if (driver.findElements(By.xpath("//div[contains(@class, 'error')]")).size() > 0)
                {
                    takeScreenshot("Login failure " + Instant.now());
                    throw new RuntimeException("Error logging to mdce, check user name/password");
                }

                //timeout waiting for webpage to process login
                throw new RuntimeException("Timeout waiting for webpage to login", ex);
            }
        }

        public void doLogout()
        {
            final String cmiuUserLoginUrl = Constants.MDCE_WEB_URL + "/pages/user/login";
            final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

            System.out.println("Logging out...");
            driver.get(cmiuUserLoginUrl);

            try
            {
                WebElement logoutSubmitButton = driver.findElement(By.xpath("//input[@value='Log out']"));
                logoutSubmitButton.click();

                webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Login')]")));

            }
            catch (Exception ex)
            {
                //user not login, so logout element is not displayed

            }

        }
    }

    /**
     * Save a screenshot
     *
     * @param testStep
     */
    public final void takeScreenshot(String testStep)
    {

        NumberFormat digits3 = new DecimalFormat("000");

        String filenameBase = testName + "_" + digits3.format(screenshotNumber) + "_" + stripIllegalCharacter(testStep);

        File screenshotTmp = driver.getScreenshotAs(OutputType.FILE);
        ScreenshotManager.moveScreenshot(testName, testStep, screenshotTmp, filenameBase);
        screenshotNumber.incrementAndGet();
    }

    /**
     * Save a screenshot
     *
     * @param testStep
     * @return File for a caption for the image, to be written by the calling code if required
     */
    public final PrintStream takeScreenshotWithCaption(String testStep)
    {

        String filenameBase = testName + "_" + stripIllegalCharacter(testStep);

        File screenshotTmp = driver.getScreenshotAs(OutputType.FILE);
        return ScreenshotManager.moveScreenshotAndCreateCaptionFile(testName, testStep, screenshotTmp, filenameBase);
    }

    /**
     * Save a screenshot with the supplied caption.
     *
     * @param testStep A descriptive name for the test step.
     * @param caption  The caption to save alongside the screenshot.
     */
    public void takeScreenshotWithCaption(String testStep, String caption)
    {
        PrintStream captionOut = takeScreenshotWithCaption(testStep);
        if (driver == null)
        {
            captionOut.println("driver is null");
        }
        else
        {
            captionOut.println(caption);
        }

        captionOut.close();
    }

    private static String stripIllegalCharacter(String filename)
    {
        return filename.replaceAll("[/:\\\\]", "");
    }

    public final void clearAndSend(String item)
    {
        final String CLEAR_FIELD = Keys.chord(Keys.CONTROL, "a") + Keys.DELETE;

        Actions actions = new Actions(driver);
        actions.sendKeys(CLEAR_FIELD).build().perform();
        actions.sendKeys(item).build().perform();
    }

    public final void send(String item)
    {
        Actions actions = new Actions(driver);
        actions.sendKeys(item).build().perform();
    }

    public final void sendAndTab(String item)
    {
        Actions actions = new Actions(driver);
        actions.sendKeys(item).build().perform();
        actions.sendKeys(Keys.TAB).build().perform();
    }

    public final void tab()
    {
        Actions actions = new Actions(driver);
        actions.sendKeys(Keys.TAB).build().perform();
    }


    public final void enter()
    {
        Actions actions = new Actions(driver);
        actions.sendKeys(Keys.ENTER).build().perform();
    }

    public final void select(String dropdown, String item)
    {
        new Select(driver.findElement(By.id(dropdown))).selectByVisibleText(item);
    }


    /**
     * Force the program to delay for certain web events to completed,
     * eg javascript show/hide
     *
     * @param milliseconds
     */
    public final void sleep(long milliseconds)
    {
        try
        {
            Thread.sleep(milliseconds);
        }
        catch (InterruptedException e)
        {
        }
    }


}
