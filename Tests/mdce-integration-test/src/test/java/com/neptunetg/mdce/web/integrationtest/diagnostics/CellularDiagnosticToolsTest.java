/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.diagnostics;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.*;

/**
 *
 */
public class CellularDiagnosticToolsTest extends MdceWebSeleniumTestBase
{
    private final String DIAGNOSTIC_TOOLS_PAGE_URL = Constants.MDCE_WEB_URL + "pages/diagnostics/diagnosticTools";
    private final int TEST_CMIU = 400000210;
    private final String TEST_CMIU_ICCID = "89148000002036030226";
    private final String TEST_CMIU_IMEI = "353238060161527";

    @Test
    public void testCellularUsageHistoryAndConnectionHistoryToolCmiu() throws Exception
    {
        wipeDownloadFolder();

        //Start up the web driver
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 60);

        //Get the diagnostic tools web page
        driver.get(DIAGNOSTIC_TOOLS_PAGE_URL);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("idValCnsDump")));

        //Create a map of filter names (from the drop down) and the value to for the filter
        Map<String, String> filterTypeDetails = new HashMap<>();
        filterTypeDetails.put("MIUID", Integer.toString(TEST_CMIU));
        filterTypeDetails.put("ICCID", TEST_CMIU_ICCID);
        filterTypeDetails.put("IMEI", TEST_CMIU_IMEI);

        //Create a map of buttons
        Map<String, String> buttonTypes = new HashMap<>();
        filterTypeDetails.put("UsageHistoryButton", "get-usage-history-button");
        filterTypeDetails.put("ConnectionHistoryButton", "get-connection-history-button");

        //Get all the files already in the download folder
        final Set<String> initialDownloadedFiles = getDownloadedFiles();

        for(String buttonType : buttonTypes.keySet())
        {
            for (String filterName : filterTypeDetails.keySet())
            {
                final String filterValue = filterTypeDetails.get(filterName);
                //Choose the filter method, enter the filter string, and download the CSV file
                selectOptionByText(driver.findElement(By.id("idTypeCnsDump")), filterName);
                final WebElement valPacketDump = driver.findElement(By.id("idValCnsDump"));
                valPacketDump.clear();
                valPacketDump.sendKeys(filterValue);
                driver.findElement(By.id(buttonTypes.get(filterName))).click();

                //Wait 3 seconds for the file to download
                Thread.sleep(3000);

                //Get a list of the files in the download folder, and check if there is a new one
                final Set<String> newDownloadedFiles = getDownloadedFiles();
                newDownloadedFiles.removeAll(initialDownloadedFiles);
                assertEquals(1, newDownloadedFiles.size());
                final String newFile = newDownloadedFiles.iterator().next();

                //Get the file as a string (Okay for a integration test, shouldn't ever exceed a few MBs)
                File file = new File(CHROME_DOWNLOAD_LOCATION + "/" + newFile);
                FileInputStream inputStream = new FileInputStream(file);
                byte[] fileData = new byte[(int) file.length()];
                inputStream.read(fileData);
                inputStream.close();
                String fileText = new String(fileData, StandardCharsets.UTF_8);

                //Check that the new file contains some of the strings we would expect
                assertThat("ICCID " + TEST_CMIU_ICCID + " present in CNS filtered by " + filterName + "=" + filterValue,
                        fileText, containsString(TEST_CMIU_ICCID));

                //Add the new file, to the initial files
                initialDownloadedFiles.add(newFile);
            }
        }
    }

    private void wipeDownloadFolder() throws IOException
    {
            FileUtils.cleanDirectory((new File(CHROME_DOWNLOAD_LOCATION)));
    }


    private Set<String> getDownloadedFiles()
    {
        File folder = new File(CHROME_DOWNLOAD_LOCATION);

        Set<String> filenames = new HashSet<>();

        for(File file : folder.listFiles())
        {
            filenames.add(file.getName());
        }

        return filenames;
    }
}
