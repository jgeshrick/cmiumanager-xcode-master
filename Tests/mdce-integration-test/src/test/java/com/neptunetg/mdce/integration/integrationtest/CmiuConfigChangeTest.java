/*
 * Neptune Technology Group
 *      Copyright 2016 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.integration.integrationtest;

import com.neptunetg.mdce.common.framework.BackendManager;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.alert.model.AlertDetails;
import com.neptunetg.mdce.common.internal.alert.service.AlertRestService;
import com.neptunetg.mdce.common.simulator.CmiuSimulatorLauncher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {BackendManager.class})
public class CmiuConfigChangeTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BackendManager backEndManager;

    @Test
    public void testCmiuConfigChange() throws Exception
    {
        final int cmiuId = 400000102;

        final String alertSource = "CMIU/" + cmiuId + "/Config/Mismatch";

        // Get alertService client
        AlertRestService alertRestService = backEndManager.createClient(AlertRestService.class);

        // Clear existing alerts
        List<AlertDetails> alertList = getUnclearedAlerts(alertSource);

        for( AlertDetails alertDetails : alertList )
        {
            alertRestService.setAlertStateToCleared(alertDetails.getAlertId());
        }

        // Make sure alerts have been cleared
        alertList = getUnclearedAlerts(alertSource);
        assertEquals(0, alertList.size());

        // Set up CMIU simulator
        final CmiuSimulatorLauncher cmiuSim = new CmiuSimulatorLauncher();

        logger.debug("Sending basic config packet: Recording - 35, Reporting - 1");

        // Send detailed config packet corresponding to first config set
        cmiuSim.sendRecordingReportingConfigPacket(cmiuId, Constants.BROKER_SERVER, "", 35, 1);

        logger.debug("Sending basic config packet: Recording - 40, Reporting - 2");

        // Send detailed config packet corresponding to second config set
        cmiuSim.sendRecordingReportingConfigPacket(cmiuId, Constants.BROKER_SERVER, "", 40, 2);

        // Check for alert status for either config set

        // Wait for packet to filter through
        int timeoutForPacketsToAppearInListSeconds = 30;
        long timeoutWaitingForPacketToFilterThroughUnixMilli = System.currentTimeMillis() + timeoutForPacketsToAppearInListSeconds * 1000L;

        boolean alertReceived = false;

        do
        {
            //Wait for a second while the MDCE stores the detailed config packet from the simulator
            Thread.sleep(1 * 1000);

            // Check for alert
            List<AlertDetails> newAlertList = alertRestService.getNewAlertList(alertSource);

            if( newAlertList.size() > 0 )
            {
                alertReceived = true;
                break;
            }

        } while (System.currentTimeMillis() < timeoutWaitingForPacketToFilterThroughUnixMilli);

        assertTrue("Config change alert not received", alertReceived);
    }

    private List<AlertDetails> getUnclearedAlerts(String alertSource) throws InternalApiException
    {
        AlertRestService alertRestService = backEndManager.createClient(AlertRestService.class);

        List<AlertDetails> alertList = alertRestService.getNewAlertList(alertSource);
        alertList.addAll(alertRestService.getStaleAlertList(alertSource));
        alertList.addAll(alertRestService.getHandledAlertList(alertSource));

        return alertList;
    }

}
