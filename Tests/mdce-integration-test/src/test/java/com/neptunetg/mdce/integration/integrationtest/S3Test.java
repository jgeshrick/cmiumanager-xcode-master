/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.S3Utility;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Test S3 setup.
 */
public class S3Test
{
    /**
     * Ensure that S3 bucket and required folders has been created.
     */
    @Test
    public void ensureS3BucketHasBeenCreated()
    {
        final String bucketName = "neptune-mdce-" + Constants.ENV_NAME + "-large-packets";
        final String s3Folder = "20150101/" + Constants.ENV_NAME + "-gateway-packets-received-20150101";

        System.out.println("Checking S3 bucket" + bucketName);
        S3Utility s3Util = new S3Utility();
        List<String> list = s3Util.getBucketList();

        assertTrue(s3Util.hasBucket(bucketName));
    }
}
