/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.alert;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

/**
 * Alert web page testing
 */
public class AlertTest extends MdceWebSeleniumTestBase
{
    private final String cmiuAlertUrl = Constants.MDCE_WEB_URL + "/pages/alert/list";

    /**
     * Simple test to ensure that the alert back-end is working.
     */
    @Test
    public void testAlertBackend()
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        driver.get(cmiuAlertUrl);

        //do a filter
        assertThat(driver.getTitle(), containsString("Alert"));

        driver.findElement(By.id("filterString")).sendKeys("CMIU");
        driver.findElement(By.id("filter-alert")).submit();
        assertThat(driver.getTitle(), containsString("Alert"));

        driver.findElementByLinkText("Clear Filter").click();
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Alerts')]")));


        //view first alert detail
        WebElement alertDetailView = driver.findElementByXPath("//td[contains(@class, 'list-content-note')]/a");
        alertDetailView.click();
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Alert From:')]")));
        assertThat(driver.getTitle(), containsString("Alert"));

        //navigate back
        driver.findElementByLinkText("Back to Alerts List").click();
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Alerts')]")));

        //todo: extend to move alerts to different states

    }


}
