/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.common.simulator;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.StreamHelper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Class for assisting in starting the CMIU Simulator written in Python
 */
public class CmiuSimulatorLauncher
{

    public static File getUnzippedPath()
    {
        return unzippedPath;
    }

    private static final File unzippedPath = new File(System.getProperty("java.io.tmpdir"), "cmiuSim");

    static
    {
        final String pathToCmiuSimulatorZip = Constants.CMIU_SIMULATOR_ZIP;

        System.out.println("Using Cmiu simulator: " + pathToCmiuSimulatorZip);

        try (final ZipFile zf = new ZipFile(pathToCmiuSimulatorZip))
        {
            unzippedPath.mkdirs();
            unzippedPath.deleteOnExit();

            System.out.println("Unzipping " + pathToCmiuSimulatorZip + " to " + unzippedPath);

            for (final Enumeration entriesEnumeration = zf.entries(); entriesEnumeration.hasMoreElements(); )
            {
                final ZipEntry entry = (ZipEntry) entriesEnumeration.nextElement();
                if (!entry.isDirectory())
                {
                    String filename = entry.getName();
                    final int slashPos = filename.lastIndexOf('/');
                    if (slashPos >= 0)
                    {
                        filename = filename.substring(slashPos + 1);
                    }
                    final File extractedFile = new File(unzippedPath, filename);
                    Files.copy(zf.getInputStream(entry), extractedFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    extractedFile.deleteOnExit();
                }
            }
        }
        catch (IOException e)
        {
            throw new RuntimeException("Can't extract ZIP file " + pathToCmiuSimulatorZip);
        }
    }

    /**
     * Trigger CMIU Simulator to send a packet
     * @param cmiuId start cmiu id to run the simulator
     * @param brokerServer broker server to connect to
     * @param instigator The instigator flag to use
     * @throws IOException
     * @throws InterruptedException
     */
    public int sendDetailedConfigPacket(int cmiuId, String brokerServer, String instigator) throws IOException, InterruptedException
    {
        ProcessBuilder pb = new ProcessBuilder("python", "CmiuSimManager.py",
                brokerServer,
                "-s " + cmiuId,
                "-u 0",
                "-r 1",
                "-p" + "detailedConfigPacket",
                instigator);

        pb.directory(unzippedPath);
        Process p = pb.start();
        int errorCode = p.waitFor();

        System.out.println(StreamHelper.inputStreamToString(p.getInputStream()));
        System.out.println(StreamHelper.inputStreamToString(p.getErrorStream()));

        return errorCode;
    }

    /**
     * Trigger CMIU Simulator to send a packet and report the specified recording and reporting intervals
     * @param cmiuId start cmiu id to run the simulator
     * @param brokerServer broker server to connect to
     * @param instigator The instigator flag to use
     * @throws IOException
     * @throws InterruptedException
     */
    public int sendRecordingReportingConfigPacket(int cmiuId, String brokerServer, String instigator, int recordingInterval, int reportingInterval) throws IOException, InterruptedException
    {
        ProcessBuilder pb = new ProcessBuilder("python", "CmiuSimManager.py",
                brokerServer,
                "-s " + cmiuId,
                "-u 0",
                "-r 1",
                "-p" + "detailedConfigPacket",
                "--recordingInterval=" + String.valueOf(recordingInterval),
                "--reportingInterval=" + String.valueOf(reportingInterval),
                instigator);

        pb.directory(unzippedPath);
        Process p = pb.start();
        int errorCode = p.waitFor();

        System.out.println(StreamHelper.inputStreamToString(p.getInputStream()));
        System.out.println(StreamHelper.inputStreamToString(p.getErrorStream()));

        return errorCode;
    }

    /**
     * Trigger CMIU Simulator to send a packet and set the reported revision build number
     * @param cmiuId start cmiu id to run the simulator
     * @param brokerServer broker server to connect to
     * @param packetType packet type to publish
     * @param revision the build revision to report
     * @throws IOException
     * @throws InterruptedException
     */
    public int sendPacketWithRevision(int cmiuId, String brokerServer, String packetType, int revision) throws IOException, InterruptedException
    {
        ProcessBuilder pb = new ProcessBuilder("python", "CmiuSimManager.py",
                brokerServer,
                "-s " + cmiuId,
                "-p" + packetType,
                "-u 0",
                "-N " + revision,
                "-r 1");

        pb.directory(unzippedPath);
        Process p = pb.start();
        int errorCode = p.waitFor();

        System.out.println(StreamHelper.inputStreamToString(p.getInputStream()));
        System.out.println(StreamHelper.inputStreamToString(p.getErrorStream()));

        return errorCode;
    }


    /**
     * Trigger CMIU Simulator to send several packets
     * @param startCmiuId start cmiu id to run the simulator
     * @param numberOfCmius the number of cmius to simulate
     * @param period time in minutes to simulate cmius
     * @param brokerServer broker server to connect to
     * @param packetType packet type to publish
     * @throws IOException
     * @throws InterruptedException
     */
    public String sendPackets(int startCmiuId, int numberOfCmius, double period,
                           String brokerServer, String packetType, int repeatCount) throws IOException, InterruptedException
    {
        ProcessBuilder pb = new ProcessBuilder("python", "CmiuSimManager.py",
                brokerServer,
                "-s " + startCmiuId,
                "-n " + numberOfCmius,
                "-p" + packetType,
                "-u " + Double.toString(period),
                "-r " + repeatCount,
                "-R");

        pb.directory(unzippedPath);
        Process p = pb.start();
        int errorCode = p.waitFor();

        String output =  StreamHelper.inputStreamToString(p.getInputStream());
        System.out.println(output);
        System.out.println(StreamHelper.inputStreamToString(p.getErrorStream()));

        return output;
    }
}
