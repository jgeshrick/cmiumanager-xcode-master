/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.rest;

import com.jayway.jsonpath.JsonPath;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseSetupRestTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test interface: IF 17 send command
 */
public class CmiuCommandTest extends BaseSetupRestTest
{

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    public CmiuCommandTest() {
        super(400001102);
    }

    @Test
    public void testSendCommandSuccess() throws Exception
    {
        final String token = fetchTokenForSiteId(32);
        ensureTestMiuExists(32, token);

        String jsonResponse = getCommandHistory(token);

        //OK response is asserted in the post
        sendCommand(token);

        String newJsonResponse = getCommandHistory(token);

        //command history packets should grow by 1 in size
        assertEquals(getJsonArrayOfPacketsOrNull(jsonResponse).size() + 1,
                getJsonArrayOfPacketsOrNull(newJsonResponse).size());

        //get last packet as json
        assertEquals(getTestCmiuId(), (int)JsonPath.read(newJsonResponse, "$.miu_id"));

        Object lastCommandPacketJson = JsonPath.read(newJsonResponse, "$.packets.[(@.length-1)]");

        assertEquals(5, (int)JsonPath.read(lastCommandPacketJson, "$.packet_type"));
        assertEquals("MDCE", JsonPath.read(lastCommandPacketJson, "$.source_device_type"));
        assertEquals("CMIU", JsonPath.read(lastCommandPacketJson, "$.target_device_type"));
        assertEquals(SEND_COMMAND_PACKET, JsonPath.read(lastCommandPacketJson, "$.packet"));

    }

    @Test
    public void testBadTokenInvalidLength() throws Exception
    {
        final String token = fetchTokenForSiteId(32);
        ensureTestMiuExists(32, token);

        int testCmiuId = getTestCmiuId();

        assertNotNull(token);

        //create a bad token
        String badToken = token + "BAD";

        final String requestBody = "{ \t\"miu_id\": \""+ testCmiuId + "\", \"source_device_type\": \"N_SIGHT\", \"target_device_type\": \"CMIU\", \t\t\"packet_type\": 4, \t\"packet\": \""+ SEND_COMMAND_PACKET +"\" } ";

        this.expectedException.expectMessage(containsString("Response code: 401"));
        String response =  HttpUtility.httpPost(urlCmiuConfigIntegration + testCmiuId + "/command?token=" + badToken, requestBody);

        badToken = "eb2fbcff2a4c64ba";  //assign a corrupted token

        this.expectedException.expectMessage(containsString("Response code: 401"));
        response =  HttpUtility.httpPost(urlCmiuConfigIntegration + testCmiuId + "/command?token=" + badToken, requestBody);

    }

    @Test
    public void testBadTokenCorrupted() throws Exception
    {
        final String token = fetchTokenForSiteId(32);
        ensureTestMiuExists(32, token);

        int testCmiuId = getTestCmiuId();

        assertNotNull(token);

        //create a bad token
        String badToken = "eb2fbcff2a4c64ba";  //assign a corrupted token
        final String requestBody = "{ \t\"miu_id\": \""+ testCmiuId + "\", \"source_device_type\": \"N_SIGHT\", \"target_device_type\": \"CMIU\", \t\t\"packet_type\": 4, \t\"packet\": \""+ SEND_COMMAND_PACKET +"\" } ";

        this.expectedException.expectMessage(containsString("Response code: 401"));
        String response =  HttpUtility.httpPost(urlCmiuConfigIntegration + testCmiuId + "/command?token=" + badToken, requestBody);
    }

    /**
     * Send commands to cmiu not belonging to the site throws error.
     * @throws Exception
     */
    @Test
    public void testSendCommandsToUnpermittedCmiu() throws Exception
    {
        final String token = fetchTokenForSiteId(32);
        ensureTestMiuExists(32, token);

        int testCmiuId = getTestCmiuId();

        final String site64Token = fetchTokenForSiteId(64);

        final String requestBody = "{ \t\"miu_id\": \""+ testCmiuId + "\", \"source_device_type\": \"N_SIGHT\", \"target_device_type\": \"CMIU\", \t\t\"packet_type\": 4, \t\"packet\": \""+ SEND_COMMAND_PACKET +"\" } ";

        this.expectedException.expectMessage(containsString("Response code: 403"));
        String response =  HttpUtility.httpPost(urlCmiuConfigIntegration + testCmiuId + "/command?token=" + site64Token, requestBody);
    }


}
