/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.framework;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by WJD1 on 12/05/2015.
 */
public class PacketResponse
{
    @JsonProperty("built_date")
    String builtDate;

    @JsonProperty("insert_date")
    String insertDate;

    String packet;

    @JsonProperty("packet_type")
    int packetType;

    @JsonProperty("device_type") //source_device_type for current system alpha code, as written for dev0
    String sourceDeviceType;

    public String getBuiltDate()
    {
        return builtDate;
    }

    public void setBuiltDate(String buildDate)
    {
        this.builtDate = buildDate;
    }

    public String getInsertDate()
    {
        return insertDate;
    }

    public void setInsertDate(String insertDate)
    {
        this.insertDate = insertDate;
    }

    public String getPacket()
    {
        return packet;
    }

    public void setPacket(String packet)
    {
        this.packet = packet;
    }

    public int getPacketType()
    {
        return packetType;
    }

    public void setPacketType(int packetType)
    {
        this.packetType = packetType;
    }

    public String getSourceDeviceType()
    {
        return sourceDeviceType;
    }

    public void setSourceDeviceType(String sourceDeviceType)
    {
        this.sourceDeviceType = sourceDeviceType;
    }

    @Override
    public String toString()
    {
        return "PacketResponse{" +
                "builtDate='" + builtDate + '\'' +
                ", insertDate='" + insertDate + '\'' +
                ", packet='" + packet + '\'' +
                ", packetType=" + packetType +
                ", sourceDeviceType='" + sourceDeviceType + '\'' +
                '}';
    }
}
