/*
 *  Neptune Technology Group
 *  Copyright 2015 as unpublished work.
 *  All rights reserved.
 *
 *  The information contained herein is confidential
 *  property of Neptune Technology Group. The use, copying, transfer
 *  or disclosure of such information is prohibited except by express
 *  written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.utility;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.util.List;
import java.util.stream.Collectors;

/**
 * A utility for accessing S3 store.
 */
public class S3Utility
{
    private final AmazonS3Client s3Client;

    public S3Utility()
    {
        AwsCredentialFactory awsCredentialProvider = new AwsCredentialFactory(true);
        this.s3Client = new AmazonS3Client(awsCredentialProvider.getCredentialProvider());
    }

    public List<String>  getBucketList()
    {
        return this.s3Client.listBuckets().stream().map(Bucket::getName).collect(Collectors.toList());
    }

    public List<S3ObjectSummary> getItemsInFolder(String bucketName, String folder)
    {
        ObjectListing listing = this.s3Client.listObjects(bucketName, folder);
        List<S3ObjectSummary> summaries = listing.getObjectSummaries();

        while (listing.isTruncated())
        {
            listing = this.s3Client.listNextBatchOfObjects(listing);
            summaries.addAll(listing.getObjectSummaries());
        }

        return summaries;
    }

    public boolean hasBucket(String bucketName)
    {
        return this.getBucketList().stream().filter(e -> e.equals(bucketName)).findFirst().isPresent();
    }

    public boolean getS3Object(String bucketName, String folder, String s3ObjectName)
    {
        return this.getItemsInFolder(bucketName, folder)
                .stream()
                .filter(e -> e.getKey().contains(s3ObjectName))
                .findFirst().isPresent();
    }
}
