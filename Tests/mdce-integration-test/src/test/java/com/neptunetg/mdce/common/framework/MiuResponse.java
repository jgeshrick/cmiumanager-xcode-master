/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.common.framework;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by WJD1 on 12/05/2015.
 */
public class MiuResponse
{
    @JsonProperty("miu_id")
    int miuId;
    List<PacketResponse> packets;

    public List<PacketResponse> getPackets()
    {
        return packets;
    }

    public void setPackets(List<PacketResponse> packets)
    {
        this.packets = packets;
    }

    public int getMiuId()
    {
        return miuId;
    }

    public void setMiuId(int miuId)
    {
        this.miuId = miuId;
    }

}
