/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.miudetails;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * Miu list page filtering.
 */
public class MiuListTest extends MdceWebSeleniumTestBase
{
    private static final String MIU_LIST_URL = Constants.MDCE_WEB_URL + "pages/miu/miulist";
    private static final String MIU_TYPE_CMIU = "CMIU";
    private static final String MIU_TYPE_L900 = "L900";
    private static final String MIU_TYPE_R900 = "R900";

    private static final String CMIU_MODES_XPATH = "//table[@id='table-miu-list']/tbody/tr/td[8]";

    @Test
    public void testMiuListPageFiltersByMuiTypeCmiu()
    {
        // Arrange: Open MIU List page and choose MIU type filter
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 60);

        openPageAndSelectFilterByMiuType(webDriverWait, MIU_TYPE_CMIU);

        // Assert: Only CMIU type records expected to be shown
        WebElement miuListTable = driver.findElement(By.id("table-miu-list"));
        int miuTypeColumnIndex = getTableHeaderIndexByText(miuListTable, "Type");
        Assert.assertTrue(doesTableContainTextAtColumnIndex(miuListTable, MIU_TYPE_CMIU, miuTypeColumnIndex));
        Assert.assertFalse(doesTableContainTextAtColumnIndex(miuListTable, MIU_TYPE_L900, miuTypeColumnIndex));
        Assert.assertFalse(doesTableContainTextAtColumnIndex(miuListTable, MIU_TYPE_R900, miuTypeColumnIndex));
        takeScreenshot("MIU list filtered by CMIU");
    }

    @Test
    public void testMiuListPageFiltersByMuiTypeL900()
    {
        // Arrange: Open MIU List page and choose MIU type filter
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 60);

        openPageAndSelectFilterByMiuType(webDriverWait, MIU_TYPE_L900);

        // Assert: Only L900 type records expected to be shown
        WebElement miuListTable = driver.findElement(By.id("table-miu-list"));
        int miuTypeColumnIndex = getTableHeaderIndexByText(miuListTable, "Type");
        Assert.assertFalse(doesTableContainTextAtColumnIndex(miuListTable, MIU_TYPE_CMIU, miuTypeColumnIndex));
        Assert.assertTrue(doesTableContainTextAtColumnIndex(miuListTable, MIU_TYPE_L900, miuTypeColumnIndex));
        Assert.assertFalse(doesTableContainTextAtColumnIndex(miuListTable, MIU_TYPE_R900, miuTypeColumnIndex));
    }

    @Test
    public void testMiuListPageFiltersByMuiTypeR900()
    {
        // Arrange: Open MIU List page and choose MIU type filter
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 60);

        openPageAndSelectFilterByMiuType(webDriverWait, MIU_TYPE_R900);

        // Assert: Only R900 type records expected to be shown
        WebElement miuListTable = driver.findElement(By.id("table-miu-list"));
        int miuTypeColumnIndex = getTableHeaderIndexByText(miuListTable, "Type");
        Assert.assertFalse(doesTableContainTextAtColumnIndex(miuListTable, MIU_TYPE_CMIU, miuTypeColumnIndex));
        Assert.assertFalse(doesTableContainTextAtColumnIndex(miuListTable, MIU_TYPE_L900, miuTypeColumnIndex));
        Assert.assertTrue(doesTableContainTextAtColumnIndex(miuListTable, MIU_TYPE_R900, miuTypeColumnIndex));
    }

    /**
     * Iterate over the available CMIU modes and verify that only matching records are shown.
     */
    @Test
    public void testMiuListPageFiltersByCmiuModeName()
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        driver.get(MIU_LIST_URL);

        WebElement cmiuModeElement = driver.findElement(By.id("selectCmiuMode"));
        Select cmiuModeSelectElement = new Select(cmiuModeElement);

        // Get all non-blank CMIU modes from the select element options.
        List<String> cmiuModeOptions = cmiuModeSelectElement
                .getOptions()
                .stream()
                .map(WebElement::getText)
                .filter(s -> StringUtils.hasText(s))
                .collect(Collectors.toList());

        for (String cmiuModeOption : cmiuModeOptions)
        {
            // Select the CMIU mode, using the timestamp to detect when the page has reloaded.
            final String timestamp = driver.findElement(By.id("timestamp")).getText();
            cmiuModeElement = driver.findElement(By.id("selectCmiuMode"));
            selectOptionByText(cmiuModeElement, cmiuModeOption);
            webDriverWait.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElementValue(By.id("timestamp"), timestamp)));
            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("selectCmiuMode")));
            takeScreenshot("Fitler by CMIU mode " + cmiuModeOption);

            // Make sure any matching elements display the selected CMIU mode.
            List<WebElement> cmiuModeTdElements = driver.findElements(By.xpath(CMIU_MODES_XPATH));

            for (WebElement cmiuModeTdElement : cmiuModeTdElements)
            {
                assertEquals(cmiuModeOption, cmiuModeTdElement.getText());
            }
        }
    }

    private void openPageAndSelectFilterByMiuType(WebDriverWait webDriverWait, String miuType) {
        //list miu details page
        driver.get(MIU_LIST_URL);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("selectMiuType")));
        final String timestamp = driver.findElement(By.id("timestamp")).getText();
        takeScreenshot("Miu list");

        // Filter by MIU Type: CMIU
        boolean wasCmiuOptionFound = selectOptionByText(driver.findElement(By.id("selectMiuType")), miuType);
        Assert.assertTrue(wasCmiuOptionFound);

        // Wait for the timestamp to change
        webDriverWait.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElementValue(By.id("timestamp"), timestamp)));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("selectMiuType")));
    }

    private int getTableHeaderIndexByText(WebElement table, String text)
    {
        final List<WebElement> headings = table.findElements(By.xpath("//th"));
        int index = 0;
        for (WebElement heading : headings)
        {
            if (heading.getText().equalsIgnoreCase(text))
            {
                return index;
            }
            index++;
        }

        return -1;
    }

    private boolean doesTableContainTextAtColumnIndex(WebElement table, String text, int columnIndex)
    {
        final int index = columnIndex + 1; // xpath indices start at 1
        final List<WebElement> cells = table.findElements(By.xpath("//tr/td[" + index + "]"));
        for (WebElement cell : cells)
        {
            if (cell.getText().equalsIgnoreCase(text))
            {
                return true;
            }
        }

        return false;
    }

}
