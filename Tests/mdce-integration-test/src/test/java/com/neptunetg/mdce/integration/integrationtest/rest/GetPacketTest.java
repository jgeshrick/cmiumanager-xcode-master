/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.rest;

import com.jayway.jsonpath.JsonPath;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseSetupRestTest;
import net.minidev.json.JSONArray;
import org.junit.Test;

import java.net.URLEncoder;
import java.time.Duration;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.*;

/**
 * Tests all scenarios of Get Packets
 * Test Interface:
 * - IF 15 Get Packets
 */
public class GetPacketTest extends BaseSetupRestTest
{

    public GetPacketTest()
    {
        super(400000102);
    }

    /**
     * Token that requested site id 0 will never return any packets using this
     */
    @Test
    public void testRequestFromSiteIdZeroReturnsNothing() throws Exception
    {
        final String token1 = fetchTokenForSiteId(1);
        ensureTestMiuExists(1, token1);

        final String token0 = fetchTokenForSiteId(0);

        String minInsertDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(OffsetDateTime.now().minusDays(1));
        String responseString = getPackets(minInsertDate, true, token0);

        //should return empty list.
        JSONArray mius = JsonPath.read(responseString, "$.mius");
        assertEquals(0, mius.size());

    }

    /**
     * Validate success content.
     * Validate content does not include info not belonging to the request sites.
     */
    @Test
    public void testRequestSuccess() throws Exception
    {
        final String token59111 = fetchTokenForSiteId(59111);

        //we should have some packet post by R900 tarball

        final Duration maxMaxPacketAge = Duration.ofDays(3L);

        for (Duration maxPacketAge = Duration.ofHours(2L); maxPacketAge.compareTo(maxMaxPacketAge) <= 0; maxPacketAge = maxPacketAge.plusHours(2L))
        {

            final String minInsertDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(OffsetDateTime.now().minus(maxPacketAge));
            final String getPacketResponse1 = getPackets(minInsertDate, true, token59111);

            Instant responseServerTime = DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(JsonPath.read(getPacketResponse1, "$.server_time"), Instant::from);
            assertThat(Duration.between(responseServerTime, Instant.now()).getSeconds(), lessThan(10L * 60L));  //server time is within 10 mins

            List<Integer> miusForSite59111 = JsonPath.read(getPacketResponse1, "$.mius..miu_id");

            if (miusForSite59111.size() > 0)
            {
                //login as another site id
                final String token1 = fetchTokenForSiteId(1);
                final String getPacketResponse2 = getPackets(minInsertDate, true, token1);
                List<Integer> miusForSite1 = JsonPath.read(getPacketResponse2, "$.mius..miu_id");

                //mius in first array should not appear on the second
                boolean foundMatchMiuFrom2Sites = miusForSite59111.stream()
                        .anyMatch(site59111Miu -> miusForSite1.stream().anyMatch(site1Miu -> site1Miu.equals(site59111Miu)));

                assertFalse("Found matching miu from 2 sites", foundMatchMiuFrom2Sites);

                break;
            }
            else
            {
                System.out.println("GetPacketTest.testRequestSuccess: No packets found within last " + maxPacketAge);
            }


        }
    }

    @Test
    public void testRequestExcludeGatewayPackets() throws Exception
    {
        final String token = fetchTokenForSiteId(1);  //assume site id credential

        String minInsertDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(OffsetDateTime.now().minusDays(1));
        boolean includeGatewayPackets = false;

        String getPacketResponse = HttpUtility.httpGet(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/packet?token=" + token
                + "&min_insert_date=" + URLEncoder.encode(minInsertDate, "UTF-8")
                + "&include_gateway_packets=" + includeGatewayPackets);

        //check we do not have gateway packets
        assertFalse(getPacketResponse.contains("gateways:"));
        assertTrue(getPacketResponse.contains("\"mius\":"));

    }

    @Test
    public void testRequestIncludeGatewayPackets() throws Exception
    {
        final String token = fetchTokenForSiteId(1);  //assume site id credential

        String minInsertDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(OffsetDateTime.now().minusDays(1));
        boolean includeGatewayPackets = true;

        String getPacketResponse = HttpUtility.httpGet(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/packet?token=" + token
                + "&min_insert_date=" + URLEncoder.encode(minInsertDate, "UTF-8")
                + "&include_gateway_packets=" + includeGatewayPackets);

        //check we do not have gateway packets
        assertTrue(getPacketResponse.contains("\"gateways\":["));
        assertTrue(getPacketResponse.contains("\"mius\":["));
    }


}
