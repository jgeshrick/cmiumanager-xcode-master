/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.rest;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseHttpSupportRestTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;


public class ReferenceDataControllerCsvTest extends BaseHttpSupportRestTest
{
    private final String urlRefData = Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/refdata?token=";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    public ReferenceDataControllerCsvTest()
    {
        super(400000102);
    }

    @Test
    public void testSetRefDataFromJson() throws Exception
    {
        final String tokenBpc = fetchTokenForBpc();

        final String url = urlRefData + tokenBpc;
        String responseString = HttpUtility.httpPutFile(url, "refdata/util.json");

        assertNotNull(responseString);
        assertEquals("", responseString);

        String responseStringForGet = HttpUtility.httpGet(urlRefData + tokenBpc);

        assertTrue(responseStringForGet.substring(0, 100), responseStringForGet.startsWith("{\"distributers\":[],\"utilities\":[{\"site_id\""));

    }

    @Test
    public void testSetRefDataFromJson2() throws Exception
    {
        final String tokenBpc = fetchTokenForBpc();

        final String url = urlRefData + tokenBpc;
        String responseString = HttpUtility.httpPutFile(url, "refdata/customer.json");

        assertNotNull(responseString);
        assertEquals("", responseString);

        String responseStringForGet = HttpUtility.httpGet(urlRefData + tokenBpc);

        //expecting no exception thrown and this function runs to completion
        assertNotNull(responseStringForGet);

        assertTrue(responseStringForGet.substring(0, 100), responseStringForGet.startsWith("{\"distributers\":[{\"customer_number\""));
    }

}
