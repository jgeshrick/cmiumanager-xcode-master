/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.common.framework;

/**
 * Constants used by the integration tests.  These should generally be defined in
 * environment variables, though some defaults will apply otherwise.
 */
public class Constants
{
    public static final String MDCE_SERVER = getVar("MDCE_SERVER", "localhost");
    public static final String MDCE_INTEGRATION_URL = getVarEnsureTrailingSlash("MDCE_INTEGRATION_URL", "http://auto-test.mdce-nonprod.neptunensight.com:8444");
    public static final String MDCE_SERVICE_URL = getVarEnsureTrailingSlash("MDCE_SERVICE_URL", "http://52.25.21.32:8081/service");
    public static final String MDCE_LORA_URL = getVarEnsureTrailingSlash("MDCE_LORA_URL", "https://auto-test.mdce-nonprod.neptunensight.com:8446/mdce-lora");
    public static final String FPC_URL = getVarEnsureTrailingSlash("FPC_URL", "https://auto-test.mdce-nonprod.neptunensight.com:8445/fpc/");

    public static final String MDCE_WEB_SALES_DISTRIBUTOR_URL = getVarEnsureTrailingSlash("MDCE_WEB_SALES_DISTRIBUTOR_URL", "https://" + MDCE_SERVER + "/mdce-web/");

    public static final String MDCE_WEB_URL = getVarEnsureTrailingSlash("MDCE_WEB_URL", "https://" + "auto-test.mdce-nonprod.neptunensight.com" + ":9444/mdce-web/");

    public static final String BROKER_SERVER = getVar("BROKER_SERVER", "localhost");

    public static final String ENV_NAME = getVar("ENV_NAME", "local-dev");

    public static final String SELENIUM_CHROME_DRIVER_PATH = getVar("SELENIUM_CHROME_DRIVER_PATH", "/Selenium/chromedriver.exe");

    public static final String CMIU_SIMULATOR_ZIP = getVar("CMIU_SIMULATOR_ZIP", null);

    public static final String L900_SIMULATOR_ZIP = getVar("L900_SIMULATOR_ZIP", null);


    private static String getVarEnsureTrailingSlash(String envVarName, String defaultValue)
    {
        final String ret = getVar(envVarName, defaultValue);
        if (ret.endsWith("/"))
        {
            return ret;
        }
        else
        {
            return ret + "/";
        }
    }

    private static String getVar(String envVarName, String defaultValue)
    {
        String envHost = System.getenv(envVarName);
        if (envHost == null)
        {
            if (defaultValue == null)
            {
                throw new RuntimeException("Env variable " + envVarName + " not defined!");
            }
            envHost = defaultValue;
        }
        System.out.println(envVarName + " = " + envHost);
        return envHost;
    }
}
