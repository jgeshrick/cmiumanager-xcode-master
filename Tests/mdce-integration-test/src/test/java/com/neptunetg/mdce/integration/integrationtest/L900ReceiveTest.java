/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.integration.integrationtest;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.simulator.L900SimulatorLauncher;
import com.neptunetg.mdce.common.utility.HttpUtility;
import org.junit.Ignore;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by WJD1 on 01/06/2016.
 * Class to test L900 from Command API IF17 to L900
 */
public class L900ReceiveTest extends BaseAuthenticatingRestTest
{
    final int l900IdHttpSenet = 700000206;
    final int l900IdHttpActility = 700000208;

    //Payload to send
    final String testPayload = "c023456789abcdefabcdef";

    L900SimulatorLauncher l900Sim = new L900SimulatorLauncher();


    private String getJson(int l900Id)
    {
        return  "{" +
                "\"miu_id\": " + l900Id + "," +
                "\"source_device_type\": \"N_SIGHT\"," +
                "\"target_device_type\": \"L900\"," +
                "\"packet_type\": 20," +
                "\"packet\": \"" + testPayload + "\"" +
                "}";
    }

    @Test
    public void testL900ReceiveEndToEndHttpSenet() throws Exception
    {
        l900ReceiveEndToEnd(l900IdHttpSenet, false);
    }

    @Test
    public void testL900ReceiveEndToEndHttpActility() throws Exception
    {
        l900ReceiveEndToEnd(l900IdHttpActility, true);
    }

    private void l900ReceiveEndToEnd(int l900Id, boolean useActility) throws Exception
    {
        System.out.println("Running L900 Simulator from folder: " + Constants.L900_SIMULATOR_ZIP);

        //Make sure the L900 is subscribed to the broker
        l900Sim.runL900Simulator(Constants.MDCE_LORA_URL, l900Id, 1, 2, 10000, false, useActility);

        Thread.sleep(1000);

        final String token = fetchTokenForSiteId(1);

        final String responseString = HttpUtility.httpJsonPost(Constants.MDCE_INTEGRATION_URL +
                "/mdce/api/v1/miu/" + l900Id + "/command?token=" + token, getJson(l900Id));

        System.out.println("Response from sending L900 command bytes:" + responseString);

        Thread.sleep(1000);

        String simOutput = l900Sim.runL900Simulator(Constants.MDCE_LORA_URL, l900Id, 1, 5, 10000, true, useActility);

        Pattern receivedHexPattern = Pattern.compile("Packet Received Hex: ([0-9abcdef]+)");
        Matcher m = receivedHexPattern.matcher(simOutput);

        assertTrue("could not find pattern /" + receivedHexPattern.pattern() + "/ in L900 sim output", m.find());

        assertEquals(testPayload, m.group(1));
    }
}
