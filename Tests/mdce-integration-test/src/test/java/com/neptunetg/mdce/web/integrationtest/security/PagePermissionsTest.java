/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.security;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.internal.user.model.LoginConstants;
import com.neptunetg.mdce.common.internal.user.model.MdceUserRole;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import com.neptunetg.mdce.web.integrationtest.UserLogin;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.rmi.runtime.Log;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertTrue;

/**
 * Created by MC5 on 10/11/2016.
 * Performs basic verification that pages on the MSOC-mode Web site are either accessible or inaccessible depending on
 * the roles of the logged-in user. These are not comprehensive tests.
 */
public class PagePermissionsTest extends MdceWebSeleniumTestBase
{
    private List<PageAccess> pageAccessMatrix;

    public PagePermissionsTest()
    {
        // Set up expected page permissions: path to page; expected element id on success; MSOC admin; MSOC operator; MSCOC viewer; site operator; distributor.
        pageAccessMatrix = new ArrayList<>();
        pageAccessMatrix.add(new PageAccess("/pages/miu/miulist",         "miu-list-title",        true, true,  true, false, false));
        pageAccessMatrix.add(new PageAccess("/pages/alert/list",          "alert-list-title",      true, true,  true, false,  false));
        pageAccessMatrix.add(new PageAccess("/pages/config",              "c2-interface-title",    true, true,  true, false,  false));
        pageAccessMatrix.add(new PageAccess("/pages/config-set",          "config-set-list-title", true, true,  true, false, false));
        pageAccessMatrix.add(new PageAccess("/pages/audit",               "audit-log-title",       true, true,  true, false,  false));
        pageAccessMatrix.add(new PageAccess("/pages/user/list",           "user-list-title",       true, true, false, false, false));
        pageAccessMatrix.add(new PageAccess("/pages/settings",            "mdce-settings-view",    true, false,  false, false,  false));
        pageAccessMatrix.add(new PageAccess("/pages/settings/modify",     "mdce-settings-modify",  true, false, false, false, false));
        pageAccessMatrix.add(new PageAccess("/pages/home",                "homepage-title",        true, true,  true, false,  false));
        pageAccessMatrix.add(new PageAccess("/pages/sim/simlist",         "sim-list-title",        true, true,  true, false, false));
        pageAccessMatrix.add(new PageAccess("/pages/gateway/gatewaylist", "gateway-list-title",    true, true,  true, false, false));
    }

    /**
     * Test various pages with each role type to verify expected permissions.
     * @throws InterruptedException
     */
    @Test
    public void testBasicPagePermissions() throws InterruptedException
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        final Map<String, String> userRoleMap = new HashMap<>();

        // Create users
        final String msocAdminUserName = "MsocAdmin-" + Instant.now().getEpochSecond();
        final String msocOperatorUserName = "MsocOperator-" + Instant.now().getEpochSecond();
        final String siteOperatorUserName = "SiteOperator-" + Instant.now().getEpochSecond();
        final String distributorUserName = "Distributor-" + Instant.now().getEpochSecond();

        addUser(webDriverWait, msocAdminUserName, MdceUserRole.MsocAdmin.getRoleDescription());
        addUser(webDriverWait, msocOperatorUserName, MdceUserRole.MsocOperator.getRoleDescription());
        addUser(webDriverWait, siteOperatorUserName, MdceUserRole.SiteOperator.getRoleDescription());
        addUser(webDriverWait, distributorUserName, MdceUserRole.Distributor.getRoleDescription());

        userRoleMap.put(LoginConstants.MSOC_ADMIN_ROLE, msocAdminUserName);
        userRoleMap.put(LoginConstants.MSOC_OPERATOR_ROLE, msocOperatorUserName);
        userRoleMap.put(LoginConstants.SITE_OPERATOR_ROLE, siteOperatorUserName);
        userRoleMap.put(LoginConstants.DISTRIBUTOR_ROLE, distributorUserName);

        // Iterate over the role types
        for (String role : userRoleMap.keySet())
        {
            assertTrue(tryLoginAs(webDriverWait, userRoleMap.get(role), "password"));

            // Test each page with the role type
            for (PageAccess page : pageAccessMatrix)
            {
                driver.get(Constants.MDCE_WEB_URL + page.getPath());

                try
                {
                    if (page.isAccessibleTo(role))
                    {
                        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id(page.getExpectedElementIdOnSuccess())));
                    }
                    else
                    {
                        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("access-not-allowed")));
                    }
                }
                catch (WebDriverException exception)
                {
                    String message =
                            "Exception while verifying that user with the \"" + role + "\" role " +
                            (page.isAccessibleTo(role) ? "can" : "cannot") + " access the Website path \"" +
                            page.getPath() + "\".";

                    throw new RuntimeException(message, exception);
                }
            }
        }

        // Tidy up.
        assertTrue(tryLoginAs(webDriverWait, UserLogin.ADMIN_USER, UserLogin.ADMIN_PWD));
        deleteUser(webDriverWait, distributorUserName);
        deleteUser(webDriverWait, siteOperatorUserName);
        deleteUser(webDriverWait, msocOperatorUserName);
        deleteUser(webDriverWait, msocAdminUserName);
    }

    /**
     * Private helper class for building the matrix of page accessibility per user role.
     */
    private class PageAccess
    {
        private String path;
        private String expectedElementIdOnSuccess;
        private Map<String, Boolean> permissionMap;

        public PageAccess(String path, String expectedElementIdOnSuccess, boolean isAccessibleToMsocAdmin,
                          boolean isAccessibleToMsocOperator, boolean isAccessibleToMsocViewer, boolean isAccessibleToSiteOperator,
                          boolean isAccessibleToDistributor)
        {
            this.path = path;
            this.expectedElementIdOnSuccess = expectedElementIdOnSuccess;
            permissionMap = new HashMap<>();
            permissionMap.put(LoginConstants.MSOC_ADMIN_ROLE, isAccessibleToMsocAdmin);
            permissionMap.put(LoginConstants.MSOC_OPERATOR_ROLE, isAccessibleToMsocOperator);
            permissionMap.put(LoginConstants.MSOC_VIEWER_ROLE, isAccessibleToMsocViewer);
            permissionMap.put(LoginConstants.SITE_OPERATOR_ROLE, isAccessibleToSiteOperator);
            permissionMap.put(LoginConstants.DISTRIBUTOR_ROLE, isAccessibleToDistributor);
        }

        public String getPath()
        {
            return path;
        }

        public boolean isAccessibleTo(String role)
        {
            return permissionMap.get(role);
        }

        public String getExpectedElementIdOnSuccess()
        {
            return expectedElementIdOnSuccess;
        }
    }
}
