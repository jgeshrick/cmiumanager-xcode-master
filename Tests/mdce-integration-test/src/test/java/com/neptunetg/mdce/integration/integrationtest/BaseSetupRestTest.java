/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.integration.integrationtest;

import com.jayway.jsonpath.JsonPath;

import java.time.Duration;
import java.util.List;

import static org.junit.Assert.*;

public abstract class BaseSetupRestTest extends BaseHttpSupportRestTest
{
    public static final String EMPTY_MIU_LIST = "{\"mius\":[]}";

    private final Duration miuConfigRequestSpacing;

    protected BaseSetupRestTest(int testCmiuId)
    {
        super(testCmiuId);
        this.miuConfigRequestSpacing = Duration.ZERO;
    }

    protected BaseSetupRestTest(int testCmiuId, Duration miuConfigRequestSpacing)
    {
        super(testCmiuId);
        this.miuConfigRequestSpacing = miuConfigRequestSpacing;
    }

    protected final void ensureTestMiuExists(int siteId, String token) throws Exception
    {
        String miuList = getMiuList(token);

        if (miuIsNotPresentInList(miuList))
        {
            createMiuAsSiteIdZero();

            Thread.sleep(this.miuConfigRequestSpacing.toMillis());

            assignMiuToSiteId(getTestCmiuId(), siteId, token);

            miuList = getMiuList(token);

            if (miuIsNotPresentInList(miuList))
            {

                assertFalse(miuList.equals(EMPTY_MIU_LIST));
                assertTrue(miuList.contains(String.valueOf(getTestCmiuId())));

            }
        }

        Thread.sleep(this.miuConfigRequestSpacing.toMillis());

        setTestMiuConfig(siteId, token);

    }

    protected final void ensureMiuExistsForSite(int siteId, int miuId) throws Exception
    {
        final String tokenForSiteId = fetchTokenForSiteId(siteId);

        final String miuList = getMiuList(tokenForSiteId);

        // search for miu_id matching the desired miuId, and get the site_id
        List<Object> foundMiuSite = JsonPath.read(miuList, "$..mius[?(@.miu_id==" + miuId + ")]");

        if (foundMiuSite.size() == 0)   //no miu found, create miu
        {
            final String tokenForFpc = fetchTokenForFpc();
            createTestMiu(miuId, tokenForFpc);

            Thread.sleep(this.miuConfigRequestSpacing.toMillis());

            assignMiuToSiteId(miuId, siteId, tokenForSiteId);

        }
        else
        {
            List<Integer> targetMiuSiteId = JsonPath.read(miuList, "$..mius[?(@.miu_id==" + miuId + ")].site_id");

            assertEquals(1, targetMiuSiteId.size());
            assertEquals(siteId, (int)targetMiuSiteId.get(0));
        }

        Thread.sleep(this.miuConfigRequestSpacing.toMillis());

        //set config for miu
        setMiuConfig(siteId, miuId, tokenForSiteId);

    }

    private boolean miuIsNotPresentInList(String miuList)
    {
        return miuList.equals(EMPTY_MIU_LIST) || !miuList.contains(String.valueOf(getTestCmiuId()));
    }

    protected void createMiuAsSiteIdZero() throws Exception
    {
        final int siteId = 0;
        final String tokenFpc = fetchTokenForFpc();
        final String token0 = fetchTokenForSiteId(siteId);

        final String miuList = getMiuList(token0);

        if (miuIsNotPresentInList(miuList))
        {
            createTestMiu(tokenFpc);
        }

        final String miuOwner = assignMiuToSiteId(getTestCmiuId(), siteId, token0);
        assertEquals("Unable to claim ownership of miu for site 0", "{\"site_id\":" + siteId + ",\"mius\":[{\"miu_id\":" + getTestCmiuId() + ",\"active\":\"Y\",\"owned\":\"true\",\"set_ownership_result\":\"successfully_claimed\"}]}", miuOwner);
    }

}
