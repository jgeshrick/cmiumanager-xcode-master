/* *****************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.common.r900;

import java.text.DateFormat;
import java.util.Calendar;

public class MeterReading
{


	private int miuId;

	private int rssi;

	private Calendar timestamp;
	
	private int value;

	private byte[] rawPacket;

	public int getMiuId() {
		return miuId;
	}

	public void setMiuId(int miuId) {
		this.miuId = miuId;
	}

	public int getRssi() {
		return rssi;
	}

	public void setRssi(int rssi) {
		this.rssi = rssi;
	}

	public Calendar getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Calendar timestamp) {
		this.timestamp = timestamp;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public byte[] getRawPacket()
	{
		return this.rawPacket;
	}

	public void setRawPacket(byte[] rawData)
	{
		this.rawPacket = rawData.clone();
	}

	public MeterReading(int miuId, int rssi, Calendar timestamp, int value)
	{
		this.miuId = miuId;
		this.rssi = rssi;
		this.timestamp = timestamp;
		this.value = value;
	}

	@Override
	public String toString()
	{
		return "MeterReading{" +
				"miuId=" + miuId +
				", rssi=" + rssi +
				", timestamp=" + DateFormat.getInstance().format(timestamp.getTime()) +
				", value=" + value +
				'}';
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MeterReading that = (MeterReading) o;

		if (miuId != that.miuId) return false;
		if (rssi != that.rssi) return false;
		if (value != that.value) return false;
		return !(timestamp != null ? !timestamp.equals(that.timestamp) : that.timestamp != null);

	}

	@Override
	public int hashCode()
	{
		int result = miuId;
		result = 31 * result + rssi;
		result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
		result = 31 * result + value;
		return result;
	}
}
